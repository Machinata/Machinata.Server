using System.Collections.Generic;
using System.Linq;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Module.Guides.Extensions;

namespace Machinata.Module.Finance.Handler {


    public class GuidesAdminHandler : AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("guides");
        }

        #endregion

        #region Menu

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menuBuilder) {
            menuBuilder.AddSection(new MenuSection {
                Icon = "document",
                Path = "/admin/guides",
                Title = "{text.guides}",
                Sort = "500"
            });



            // Menu: add sections as submenu
            {
                var maxItems = 5;
                

                // All first level nodes
                var root = menuBuilder.Handler.DB.ContentNodes().Include("Children").GetNodeByPath(Guides.Config.GuidesCMSPath,false);
                if (root != null) {
                    var nodes = root.GetGuidesNodes(true);

                    // Number of nodes
                    var count = nodes.Count();

                    // Add as items
                    foreach (var node in nodes.OrderBy(o => o.Sort).Take(maxItems)) {
                        menuBuilder.Item(node.Name, node.PublicId, "/admin/guides/guide/" + node.Name);
                    }

                    // Add Show all item
                    if (count > maxItems) {
                        menuBuilder.Item("{text.all}", "all", "/admin/guides", null, classes: "not-selectable");
                    }
                }
            }
        }

        #endregion

        [RequestHandler("/admin/guides")]
        public void List() {
            this.Template.InsertGuides(this.DB, "guides", onlyPublished: true);
     
            // Navigation
            this.AddNavigationForPath("/");

        }

      
        [RequestHandler("/admin/guides/guide/{path}")]
        public void Guide(string path) {

            // Insert Guid
            var node = this.Template.InsertGuide(this.DB, path);
            
            var navPath = node.Path.Replace(Module.Guides.Config.GuidesCMSPath, string.Empty);

            // Navigation
            this.AddNavigationForPath(navPath);
        }


        public void AddNavigationForPath(string path) {
            this.Template.InsertVariable("node.path", path);
            this.Navigation.Add("guides", "{text.guides}");
            if (path != null && path != ContentNode.NODE_PATH_SEP) {
                path = path.TrimStart(ContentNode.NODE_PATH_SEP[0]);
                var segs = path.Split(ContentNode.NODE_PATH_SEP[0]);
                for (int i = 0; i < segs.Length; i++) {
                    var seg = segs[i];
                    this.Navigation.Add((i == 0 ? "guide/" : "") + seg, seg);
                }
            }
        }

    }
}
