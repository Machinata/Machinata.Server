using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Util;
using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Reflection;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Guides.Extensions {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public static class PageTemplateSceneInserts {



        public static void InsertGuides(this PageTemplate parentTemplate, ModelContext db, string variableName, bool onlyPublished = false) {


            // Get node
            var node = db.ContentNodes().Include("Children").GetNodeByPath(Guides.Config.GuidesCMSPath);
                       
            // All root pages
            {
                List<PageTemplate> templates = CreateGuidesTemplates(parentTemplate, db, node, onlyPublished);
                if (templates.Any()) {
                    parentTemplate.InsertTemplates(variableName, templates);
                } else {
                    parentTemplate.InsertVariable(variableName, "{text.list-empty}");
                }
            }

            // Variables
            parentTemplate.InsertVariables("node", node);
        }

        /// <summary>
        /// Sub nodes/guides
        /// </summary>
        /// <param name="parentTemplate"></param>
        /// <param name="db"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        private static List<PageTemplate> CreateGuidesTemplates(PageTemplate parentTemplate, ModelContext db, ContentNode node, bool onlyPublished = false) {
            var templates = new List<PageTemplate>();

            IEnumerable<ContentNode> pages = GetGuidesNodes(node, onlyPublished);

            foreach (var page in pages.OrderBy(n => n.Sort)) {
                var template = parentTemplate.LoadTemplate("guides-list.item");
                template.InsertVariable("guide.name", page.Name);
                template.InsertVariable("guide.short-url", page.ShortURL);
                template.InsertVariable("guide.path", page.Path.Replace(Config.GuidesCMSPath, string.Empty));
                template.InsertVariable("guide.public-id", page.PublicId);
                template.InsertVariable("guide.icon", "folder-open");
                template.InsertVariable("guide.published", page.Published);
                template.InsertVariable("guide.subpages-count", db.ContentNodes().Count(n => n.ParentId == page.Id && n.NodeType == ContentNode.NODE_TYPE_NODE));
                templates.Add(template);
            }

            return templates;
        }

        public static IEnumerable<ContentNode> GetGuidesNodes(this ContentNode node, bool onlyPublished) {
            var pages = node.Children.Where(n => n.IsSystemNode == false && n.NodeType != ContentNode.NODE_TYPE_TRANSLATION);

            if (onlyPublished == true) {
                pages = pages.Where(p => p.Published == true);
            }

            return pages;
        }

        public static ContentNode InsertGuide(this PageTemplate parentTemplate, ModelContext db, string path) {
                      
            // Rebuild path
            path = Config.GuidesCMSPath + "/" +  path;

            ContentNode node = db.ContentNodes()
              .Include("Children")
              .Include("Children.Children")
              .GetNodeByPath(path);

            var children = node.ChildrenForType(ContentNode.NODE_TYPE_NODE);
            var contentNodes = node.Children.Where(n => n.NodeType != ContentNode.NODE_TYPE_NODE);

            // TODO BETTER SECURITY
            if (node.Published == false) {
                throw new Backend404Exception();
            }

            // Sub-pages
            {
                var templates = CreateGuidesTemplates(parentTemplate, db, node, onlyPublished: true);
                parentTemplate.InsertTemplates("guides", templates);
                parentTemplate.InsertVariable("guides.count", templates.Count);
            }

            // Variables
            parentTemplate.InsertVariable("guide.has-children", children.Count() > 0 ?  "true": "false");
            parentTemplate.InsertVariable("guide.has-content", contentNodes.Count() > 0 ? "true" : "false");

            // Variables
            //this.Template.InsertVariable("node.name", node.Name);
            //this.Template.InsertVariable("node.short-url", node.ShortURL);
            //this.Template.InsertVariable("node.public-id", node.PublicId);

            //var tranlsation = node.TranslationForLanguage(parentTemplate.Language);
            // parentTemplate.InsertLayoutedContent("content",node.Path, parentTemplate.Language);

            parentTemplate.InsertContent("content", node);

            return node;
        }

    }
}
