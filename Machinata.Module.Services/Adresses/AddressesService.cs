﻿using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Machinata.Module.Services.Weather.WeatherService;

namespace Machinata.Module.Services.Addresses {

    public class AddressesService {

       


        public static IEnumerable<AddressesDataEntry> GetAddresses(string where, string what) {
            var provider = new TelSearchAddressesProvider();
            var data = provider.GetAddresses(where, what);
            return data;
        }

       

        
    }


    public class AddressesDataEntry {

        public AddressesDataEntry() {
            this.Address = new Core.Model.Address();
        }

        public string Id { get; set; }

        public Core.Model.Address Address { get; set; }
        public string Occupation { get; set; }
        public IEnumerable<string> Categories { get; set; }
        public string Website { get; set; }
        public string Type { get; set; }
        public string Link { get; set; }
        public string Email { get; internal set; }
        public string Fax { get; internal set; }
        public string Copyright { get; internal set; }
    }
}
