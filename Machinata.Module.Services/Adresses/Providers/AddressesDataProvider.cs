﻿using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Machinata.Module.Services.Weather.WeatherDataProvider;

namespace Machinata.Module.Services.Addresses {

    public abstract class AddressesDataProvider {
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        public abstract IEnumerable<AddressesDataEntry> GetAddresses(string what, string where);

       

    }


   
}
