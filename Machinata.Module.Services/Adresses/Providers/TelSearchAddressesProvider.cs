using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Xml.Linq;
using Machinata.Core.Util;
using Machinata.Module.Services.Weather;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Machinata.Module.Services.Addresses {


    #region Notes
    //https://tel.search.ch/api/help

    #endregion


    public class TelSearchAddressesProvider : AddressesDataProvider {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        public const string SERVER_URL = "https://tel.search.ch/api/";


        public TelSearchAddressesProvider() {


        }

        public override IEnumerable<AddressesDataEntry> GetAddresses(string where, string what) {
        https://tel.search.ch/api/?was=%C3%A4rzte&wo=riehen&key=23292fa58d652e775a150b28d781e5a0
            var key = Core.Config.GetStringSetting("TelSearchAPIKey");

            var result = new List<AddressesDataEntry>();

            int startIndex = 0;
            int totalResults = 0;
            int itemsPerPage = 0;
            int index = 0;

            // Paging
            do {

                var url = $"https://tel.search.ch/api/?was={what}&wo={where}&key={key}&pos={index}";
                var xml = Core.Util.HTTP.GetXML(url);

                startIndex = int.Parse(xml.ElementAnyNS("startIndex")?.Value);
                itemsPerPage = int.Parse(xml.ElementAnyNS("itemsPerPage")?.Value);
                totalResults = int.Parse(xml.ElementAnyNS("totalResults")?.Value);
                index = startIndex + itemsPerPage;
                

                var addresses = _parseXMLResults(xml);
                result.AddRange(addresses);
            } while (startIndex + itemsPerPage <= totalResults);
            
            return result;
        }




        private IEnumerable<AddressesDataEntry> _parseXMLResults(XElement xml) {
            var result = new List<AddressesDataEntry>();

            var index = 0;
            foreach (var entry in xml.ElementsAnyNS("entry")) {

                AddressesDataEntry data = new AddressesDataEntry();
                var name = entry.ElementAnyNS("name")?.Value;
                var firstname = entry.ElementAnyNS("firstname")?.Value;
                var street = entry.ElementAnyNS("street")?.Value;
                var streetNo = entry.ElementAnyNS("streetno")?.Value;
                var zip = entry.ElementAnyNS("zip")?.Value;


                data.Address.Name = $"{firstname} {name}";
                data.Address.Address1 = $"{street} {streetNo}";
                data.Address.City = entry.ElementAnyNS("city")?.Value;
                data.Address.CountryCode = entry.ElementAnyNS("country")?.Value;
                data.Address.Phone = entry.ElementAnyNS("phone")?.Value;
                data.Address.ZIP = zip;

                var extras = entry.ElementsAnyNS("extra");


                var fax = extras.ElementWithAttribute("type", "Fax");
                var email = extras.ElementWithAttribute("type", "email");
                var website = extras.ElementWithAttribute("type", "website");


                data.Fax = fax?.Value;
                data.Website = website?.Value;
                data.Email = email?.Value;

                data.Link = entry.ElementAnyNS("link")?.FirstAttribute?.Value;
                data.Occupation = entry.ElementAnyNS("occupation")?.Value;
                data.Categories = entry.ElementsAnyNS("category").Select(c => c.Value);


                data.Copyright = entry.ElementAnyNS("copyright")?.Value;

                data.Id = entry.ElementAnyNS("id")?.Value;

                result.Add(data);
                index++;

            }


            return result;
        }

       
    }
}