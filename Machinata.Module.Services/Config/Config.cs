using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Services {
    public class Config {

        public static string FixerApiAccessKey = Core.Config.GetStringSetting("FixerApiAccessKey");

        
        public static string TelSearchAPIKey = Core.Config.GetStringSetting("TelSearchAPIKey");
        public static string OpenWeatherMapAPIKey = Core.Config.GetStringSetting("OpenWeatherMapAPIKey");
    }
}
