using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Services {

    public class GeoCoding {

        public static Providers.GeoCodingProvider Google = new Providers.GoogleGeoCodingProvider();
        public static Providers.GeoCodingProvider OpenStreetMap = new Providers.OpenStreetMapsGeoCodingProvider();
        public static Providers.GeoCodingProvider MapBox = new Providers.MapBoxGeoCodingProvider();

        public static Providers.GeoCodingProvider Default = Google;

    }
}
