using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Services.Providers {

    public class GeoCodingData {
        public string Lat;
        public string Lon;
        public string Formatted;
        public Address AddressLong;
        public Address AddressShort;
        
        public double LatAsDouble() {
            return double.Parse(Lat);
        }
        public double LonAsDouble() {
            return double.Parse(Lon);
        }
    }

    public abstract class GeoCodingProvider {

        /// <summary>
        /// Forward goecode an address to a coordinate.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="countryFilter">Optional, limit results to one or more countries. Permitted values are ISO 3166 alpha 2 country codes separated by commas. See implementations for more detail.</param>
        /// <returns>GeoCodingData</returns>
        public abstract GeoCodingData DataForAddress(string address, string countryFilter = null);

        public abstract GeoCodingData DataForLatLon(string lat, string lon);

    }
}
