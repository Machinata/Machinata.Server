using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Services.Providers {
    /// <summary>
    /// See https://nominatim.org/release-docs/develop/api/Search/
    /// Note: Please cache results!
    /// </summary>
    /// <seealso cref="Machinata.Module.Services.Providers.GeocodingProvider" />
    public class OpenStreetMapsGeoCodingProvider : GeoCodingProvider {

        public override GeoCodingData DataForAddress(string address, string countryFilter = null) {
            // Feature check
            if (countryFilter != null) throw new Exception("GoogleGeoCodingProvider does not support countryFilter!");


            // Documentation: https://nominatim.org/release-docs/develop/api/Search/
            // https://nominatim.openstreetmap.org/search.php?&format=json&q=QUERY
            /*
            [
               {
                  "place_id":42647671,
                  "licence":"Data � OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
                  "osm_type":"node",
                  "osm_id":3228436887,
                  "boundingbox":[
                     "47.5829713",
                     "47.5830713",
                     "7.6482042",
                     "7.6483042"
                  ],
                  "lat":"47.5830213",
                  "lon":"7.6482542",
                  "display_name":"12, Baselstrasse, Riehen, Basel-City, 4125, Switzerland",
                  "class":"place",
                  "type":"house",
                  "importance":0.4310000000000001
               }
            ]
            */
            var addressParam = address.Replace("\n", ",").Replace(" ", "+");
            var url = $"https://nominatim.openstreetmap.org/search.php?&format=json&q={addressParam}";
            var json = Core.Util.HTTP.GetJSON(url);
            return _parseJSONResults(json);
        }

        public override GeoCodingData DataForLatLon(string lat, string lon) {
            throw new Exception("Open Street Maps does not provide reverse geo coding...");
        }

        private GeoCodingData _parseJSONResults(JToken json) {
            if (json.Count() == 0) return null;

            GeoCodingData data = new GeoCodingData();
            //TODO: handle multiple results?
            data.Formatted = json[0]["display_name"].ToString();
            data.Lat = json["lat"].ToString();
            data.Lon = json["lon"].ToString();
            return data;
        }
    }
}
