using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Services.Providers {
    /// <summary>
    /// See https://docs.mapbox.com/api/search/geocoding/
    /// 
    /// Note: License only for use in displaying on a mapbox layer!
    /// Note: Please cache results!
    /// </summary>
    /// <seealso cref="Machinata.Module.Services.Providers.GeocodingProvider" />
    public class MapBoxGeoCodingProvider : GeoCodingProvider {

        /// <summary>
        /// Forward goecode an address to a coordinate.
        /// For country codes see: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
        /// </summary>
        /// <param name="address"></param>
        /// <param name="countryFilter">Optional, limit results to one or more countries. Permitted values are ISO 3166 alpha 2 country codes separated by commas.</param>
        /// <returns>GeoCodingData</returns>
        public override GeoCodingData DataForAddress(string address, string countryFilter = null) {
            // Documentation: https://docs.mapbox.com/api/search/geocoding/
            // https://api.mapbox.com/geocoding/v5/{endpoint}/{search_text}.json

            /*
            {
               "type":"FeatureCollection",
               "query":[
                  "muhlestalden"
               ],
               "features":[
                  {
                     "id":"address.1114103036472582",
                     "type":"Feature",
                     "place_type":[
                        "address"
                     ],
                     "relevance":1,
                     "properties":{
                        "accuracy":"street"
                     },
                     "text":"Mühlestalden",
                     "place_name":"Mühlestalden, 8824 Schönenberg, Switzerland",
                     "center":[
                        8.654207,
                        47.198938
                     ],
                     "geometry":{
                        "type":"Point",
                        "coordinates":[
                           8.654207,
                           47.198938
                        ]
                     },
                     "context":[
                        {
                           "id":"postcode.8567685210224870",
                           "text":"8824"
                        },
                        {
                           "id":"place.8567685211950730",
                           "text":"Schönenberg"
                        },
                        {
                           "id":"region.317199580698500",
                           "wikidata":"Q11943",
                           "short_code":"CH-ZH",
                           "text":"Zürich"
                        },
                        {
                           "id":"country.10641720978437020",
                           "wikidata":"Q39",
                           "short_code":"ch",
                           "text":"Switzerland"
                        }
                     ]
                  },
                  {
                     "id":"address.1446929898798964",
                     "type":"Feature",
                     "place_type":[
                        "address"
                     ],
                     "relevance":1,
                     "properties":{
                        "accuracy":"street"
                     },
                     "text":"Mühlestalden",
                     "place_name":"Mühlestalden, 3863 Gadmen, Switzerland",
                     "center":[
                        8.301906,
                        46.722303
                     ],
                     "geometry":{
                        "type":"Point",
                        "coordinates":[
                           8.301906,
                           46.722303
                        ]
                     },
                     "context":[
                        {
                           "id":"postcode.12818810205823990",
                           "text":"3863"
                        },
                        {
                           "id":"place.12818810205763950",
                           "text":"Gadmen"
                        },
                        {
                           "id":"region.7653181245960580",
                           "wikidata":"Q11911",
                           "short_code":"CH-BE",
                           "text":"Bern"
                        },
                        {
                           "id":"country.10641720978437020",
                           "wikidata":"Q39",
                           "short_code":"ch",
                           "text":"Switzerland"
                        }
                     ]
                  }
               ],
               "attribution":"NOTICE: © 2021 Mapbox and its suppliers. All rights reserved. Use of this data is subject to the Mapbox Terms of Service (https://www.mapbox.com/about/maps/). This response and the information it contains may not be retained. POI(s) provided by Foursquare."
            }            
            */
            var key = Core.Config.GetStringSetting("MapsAccessToken");
            var endpoint = "mapbox.places";
            var addressParam = Uri.EscapeDataString(address.Replace("\n", ",").Replace(";", " "));
            var url = $"https://api.mapbox.com/geocoding/v5/{endpoint}/{addressParam}.json?access_token={key}";
            if (countryFilter != null) url += "&country=" + Uri.EscapeDataString(countryFilter);
            var json = Core.Util.HTTP.GetJSON(url);
            return _parseJSONResults(json);
        }

        public override GeoCodingData DataForLatLon(string lat, string lon) {
            throw new Exception("Not yet implemented!");
        }

        private GeoCodingData _parseJSONResults(JToken json) {
            if (json["features"].Count() == 0) return null;

            GeoCodingData data = new GeoCodingData();
            //TODO: handle multiple results?
            data.Formatted = json["features"][0]["place_name"].ToString();
            data.Lat = json["features"][0]["center"][1].ToString();
            data.Lon = json["features"][0]["center"][0].ToString();
            return data;
        }
    }
}
