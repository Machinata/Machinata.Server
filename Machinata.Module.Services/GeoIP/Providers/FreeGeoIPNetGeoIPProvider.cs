using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Services.Providers {
    /// <summary>
    /// 
    /// See http://freegeoip.net/
    /// </summary>
    /// <seealso cref="Machinata.Module.Services.Providers.GeocodingProvider" />
    public class FreeGeoIPNetGeoIPProvider : GeoIPProvider {

        public override GeoIPData DataForIP(string ip) {
            // http://freegeoip.net/json/83.150.2.227
            /*
            {
              "ip": "83.150.2.227",
              "country_code": "CH",
              "country_name": "Switzerland",
              "region_code": "ZH",
              "region_name": "Zurich",
              "city": "Zurich",
              "zip_code": "8037",
              "time_zone": "Europe/Zurich",
              "latitude": 47.3667,
              "longitude": 8.55,
              "metro_code": 0
            }
            */
            var url = $"http://freegeoip.net/json/{ip}";
            var json = Core.Util.HTTP.GetJSON(url);
            GeoIPData data = new GeoIPData();
            data.IP = json["ip"].ToString();
            data.Lat = json["latitude"].ToString();
            data.Lon = json["longitude"].ToString();
            data.MetroCode = json["metro_code"].ToString();
            data.Timezone = json["time_zone"].ToString();
            data.Address = new Core.Model.Address();
            data.Address.CountryCode = json["country_code"].ToString();
            data.Address.City = json["city"].ToString();
            data.Address.ZIP = json["zip_code"].ToString();
            return data;
        }
        
    }
}
