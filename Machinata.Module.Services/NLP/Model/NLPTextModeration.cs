﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Machinata.Module.Services.NLP.Model {
    public class NLPTextModeration {


        //https://beta.openai.com/docs/api-reference/moderations/create
        //{
        //  "id": "modr-5MWoLO",
        //  "model": "text-moderation-001",
        //  "results": [
        //    {
        //      "categories": {
        //        "hate": false,
        //        "hate/threatening": true,
        //        "self-harm": false,
        //        "sexual": false,
        //        "sexual/minors": false,
        //        "violence": true,
        //        "violence/graphic": false
        //      },
        //      "category_scores": {
        //        "hate": 0.22714105248451233,
        //        "hate/threatening": 0.4132447838783264,
        //        "self-harm": 0.005232391878962517,
        //        "sexual": 0.01407341007143259,
        //        "sexual/minors": 0.0038522258400917053,
        //        "violence": 0.9223177433013916,
        //        "violence/graphic": 0.036865197122097015
        //      },
        //      "flagged": true
        //    }
        //  ]
        //}

        [Newtonsoft.Json.JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [Newtonsoft.Json.JsonProperty(PropertyName = "model")]
        public string Model { get; set; }

        [Newtonsoft.Json.JsonProperty(PropertyName = "results")]
        public List<NLPTextModerationData> Results { get; set; }

    }

    public class NLPTextModerationData {

        [Newtonsoft.Json.JsonProperty(PropertyName = "categories")]
        public NLPTextModerationDataCategories<bool> Categories { get; set; }

        [Newtonsoft.Json.JsonProperty(PropertyName = "category_scores")]
        public NLPTextModerationDataCategories<double> CategoryScores { get; set; }

        [Newtonsoft.Json.JsonProperty(PropertyName = "flagged")]
        public bool Flagged { get; set; }

    }

    public class NLPTextModerationDataCategories<T> {

        [Newtonsoft.Json.JsonProperty(PropertyName = "hate")]
        public T Hate { get; set; }
        [Newtonsoft.Json.JsonProperty(PropertyName = "hate/threatening")]
        public T HateThreatening { get; set; }
        [Newtonsoft.Json.JsonProperty(PropertyName = "self-harm")]
        public T SelfHarm { get; set; }
        [Newtonsoft.Json.JsonProperty(PropertyName = "sexual")]
        public T Sexual { get; set; }
        [Newtonsoft.Json.JsonProperty(PropertyName = "sexual/minors")]
        public T SexualMinors { get; set; }
        [Newtonsoft.Json.JsonProperty(PropertyName = "violence")]
        public T Violence { get; set; }
        [Newtonsoft.Json.JsonProperty(PropertyName = "violence/graphic")]
        public T ViolenceGraphic { get; set; }

    }
}
