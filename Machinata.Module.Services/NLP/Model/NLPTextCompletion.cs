﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Machinata.Module.Services.NLP.Model {
    public class NLPTextCompletion {

        /* {
                "id": "cmpl-GERzeJQ4lvqPk8SkZu4XMIuR",
                "object": "text_completion",
                "created": 1586839808,
                "model": "text-davinci:003",
                "choices": [
                    {
                        "text": "\n\nThis is indeed a test",
                        "index": 0,
                        "logprobs": null,
                        "finish_reason": "length"
                    }
                ],
                "usage": {
                    "prompt_tokens": 5,
                    "completion_tokens": 7,
                    "total_tokens": 12
                }
            }*/

        [Newtonsoft.Json.JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [Newtonsoft.Json.JsonProperty(PropertyName = "object")]
        public string Object { get; set; }

        [Newtonsoft.Json.JsonProperty(PropertyName = "created")]
        public int Created { get; set; }

        [Newtonsoft.Json.JsonProperty(PropertyName = "model")]
        public string Model { get; set; }

        [Newtonsoft.Json.JsonProperty(PropertyName = "choices")]
        public List<NLPTextCompletionChoice> Choices { get; set; }

    }

    public class NLPTextCompletionChoice {

        /*{
            "text": "\n\nThis is indeed a test",
            "index": 0,
            "logprobs": null,
            "finish_reason": "length"
        }*/

        [Newtonsoft.Json.JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

        [Newtonsoft.Json.JsonProperty(PropertyName = "index")]
        public int Index { get; set; }

        //[Newtonsoft.Json.JsonProperty(PropertyName = "logprobs")]
        //public string logprobs { get; set; }

        [Newtonsoft.Json.JsonProperty(PropertyName = "finish_reason")]
        public string FinishReason { get; set; }

    }
}
