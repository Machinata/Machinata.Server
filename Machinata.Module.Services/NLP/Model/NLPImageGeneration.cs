﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Machinata.Module.Services.NLP.Model {
    public class NLPImageGeneration {

        /* {
          "created": 1589478378,
          "data": [
            {
              "url": "https://..."
            },
            {
              "url": "https://..."
            }
          ]
        }*/

        [Newtonsoft.Json.JsonProperty(PropertyName = "created")]
        public int Created { get; set; }

        [Newtonsoft.Json.JsonProperty(PropertyName = "data")]
        public List<NLPImageGenerationData> Data { get; set; }

    }

    public class NLPImageGenerationData {

        [Newtonsoft.Json.JsonProperty(PropertyName = "url")]
        public string URL { get; set; }

        [Newtonsoft.Json.JsonProperty(PropertyName = "b64_json")]
        public string B64Json { get; set; }

    }
}
