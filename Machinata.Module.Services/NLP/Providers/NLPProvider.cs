﻿using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Services.NLP.Providers {
    public abstract class NLPProvider {

        public abstract int? GetDefaultMaxTokens();
        public abstract double? GetDefaultTemperature();
        public abstract string GetDefaultModel();

        public abstract Model.NLPTextCompletion GetTextCompletion(string prompt, string model = null, double ? temperature = null, int? maxTokens = null);
        public abstract Model.NLPImageGeneration CreateImage(string prompt, int? number = null, string size = null, string format = null);
        public abstract Model.NLPTextModeration CreateModeration(string input);
        public abstract Model.NLPImageGeneration CreateImageAndStoreInDataCenter(ModelContext db, string filename, string filesource, string prompt, int? number = null, string size = null);

        }
    }
