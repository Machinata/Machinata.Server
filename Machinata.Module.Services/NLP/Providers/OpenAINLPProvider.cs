﻿using Machinata.Core.Lifecycle;
using Machinata.Core.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Services.NLP.Providers {

    // https://beta.openai.com/docs/introduction/overview
    public class OpenAINLPProvider : NLPProvider {

        public string GetAPIKey() {
            return Core.Config.GetStringSetting("OpenAIAPIKey");
        }

        public string GetAuthorizationHeader() {
            return "Bearer " + GetAPIKey();
        }

        public override int? GetDefaultMaxTokens() {
            return 4000;
        }

        // https://platform.openai.com/docs/models/gpt-3-5
        // https://platform.openai.com/docs/deprecations
        // InstructGPT models
        // SHUTDOWN DATE   LEGACY MODEL    LEGACY MODEL PRICE RECOMMENDED REPLACEMENT
        // 2024-01-04	  text-ada-001      $0.0004 / 1K tokens gpt-3.5-turbo-instruct
        // 2024-01-04	  text-babbage-001	$0.0005 / 1K tokens gpt-3.5-turbo-instruct
        // 2024-01-04	  text-curie-001	$0.0020 / 1K tokens gpt-3.5-turbo-instruct
        // 2024-01-04	  text-davinci-001	$0.0200 / 1K tokens gpt-3.5-turbo-instruct
        // 2024-01-04	  text-davinci-002	$0.0200 / 1K tokens gpt-3.5-turbo-instruct
        // 2024-01-04	  text-davinci-003	$0.0200 / 1K tokens gpt-3.5-turbo-instruct
        public override string GetDefaultModel() {
            //return "text-davinci-003";
            return "gpt-3.5-turbo-instruct";
        }

        public override double? GetDefaultTemperature() {
            return 0.0;
        }

        public override Model.NLPTextCompletion GetTextCompletion(string prompt, string model = null, double? temperature = null, int? maxTokens = null) {
            var data = new JObject();
            data.Add("model", model);
            data.Add("prompt", prompt);
            if(temperature != null && temperature.HasValue) data.Add("temperature", temperature.Value);
            if(maxTokens != null && maxTokens.HasValue) data.Add("max_tokens", maxTokens.Value);
            var ret = Core.Util.HTTP.PostJSON("https://api.openai.com/v1/completions", data, Encoding.UTF8, GetAuthorizationHeader());
            return ret.ToObject<NLP.Model.NLPTextCompletion>();
        }

        public override Model.NLPImageGeneration CreateImage(string prompt, int? number = null, string size = null, string format = null) {
            if (format == null) format = "url";
            var data = new JObject();
            data.Add("prompt", prompt);
            data.Add("response_format", format);
            if (number != null && number.HasValue) data.Add("n", number.Value);
            if (size != null) data.Add("size", size);
            var ret = Core.Util.HTTP.PostJSON("https://api.openai.com/v1/images/generations", data, Encoding.UTF8, GetAuthorizationHeader());
            var returnObject = ret.ToObject<NLP.Model.NLPImageGeneration>();
            return returnObject;
        }

        public override Model.NLPImageGeneration CreateImageAndStoreInDataCenter(ModelContext db, string filename, string filesource, string prompt, int? number = null, string size = null) {
            var returnObject = CreateImage(prompt, number, size, "b64_json");
            foreach (var image in returnObject.Data) {
                // We have a base 64 representation of the image, now convert it and upload it to our own store
                var bytes = Convert.FromBase64String(image.B64Json);
                var contentFile = Core.Data.DataCenter.UploadFile(filename, bytes, filesource, null, null);
                db.ContentFiles().Add(contentFile);
                db.SaveChanges();
                image.B64Json = null;
                image.URL = contentFile.ContentURL;
            }
            return returnObject;
        }

        public override Model.NLPTextModeration CreateModeration(string input) {
            var data = new JObject();
            data.Add("input", input);
            var ret = Core.Util.HTTP.PostJSON("https://api.openai.com/v1/moderations", data, Encoding.UTF8, GetAuthorizationHeader());
            return ret.ToObject<NLP.Model.NLPTextModeration>();
        }
    }
}
