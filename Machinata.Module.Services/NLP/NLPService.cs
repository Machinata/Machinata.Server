﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Services.NLP {
    public class NLPService {

        public static NLP.Providers.NLPProvider Default { get {
                return new NLP.Providers.OpenAINLPProvider();
            } 
        }

    }
}
