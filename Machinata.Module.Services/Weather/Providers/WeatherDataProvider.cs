﻿using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Machinata.Module.Services.Weather.WeatherDataProvider;

namespace Machinata.Module.Services.Weather {

    public abstract class WeatherDataProvider {
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        public abstract IEnumerable<WeatherDataEntry> GetWeatherData(string location, DateTime start, DateTime end);

        public virtual List<WeatherDataEntry> GetWeatherDataDaily(string location, DateTime start, int days) {
            var data = this.GetWeatherData(location, start, start.AddDays(days));

            var startLocalDate = Core.Util.Time.ConvertToDefaultTimezone(start).Date;
            var startLocalDateUTC = Core.Util.Time.ConvertToUTCTimezone(startLocalDate);

            var currentWeather = data.FirstOrDefault();


            var result = new List<WeatherDataEntry>();
            result.Add(currentWeather);

            for (int i = 1; i < days; i++) {
                var startTmrw = startLocalDateUTC.AddDays(i);
                var endTmrw = startTmrw.AddDays(1).AddTicks(-1);


                var samples = data.Where(d => d.Date >= startTmrw && d.Date < endTmrw);

                var tempMin = samples.Select(s => s.MinTemperatur).Min();
                var tempMax = samples.Select(s => s.MaxTemperatur).Max();

                _logger.Info("weather min: "+ tempMin);
                _logger.Info("weather max: " + tempMax);

                var sampleMin = samples.FirstOrDefault(s => s.MinTemperatur == tempMin);
                var sampleMy = samples.FirstOrDefault(s => s.MaxTemperatur == tempMax);


                var weather = samples.GroupBy(s => s.Weather).OrderByDescending(d => d.Count()).First();
                var dailyWeather = new WeatherDataEntry();
                dailyWeather.Date = startTmrw;
                dailyWeather.EndDate = endTmrw;
                dailyWeather.MinTemperatur = tempMin;
                dailyWeather.MaxTemperatur = tempMax;
                dailyWeather.Source = samples.FirstOrDefault()?.Source;
                dailyWeather.Id = samples.FirstOrDefault()?.Id;
                dailyWeather.WeatherDescription = weather.First().WeatherDescription; 
                dailyWeather.WeatherID = weather.First().WeatherID; 

                dailyWeather.Weather = weather.Key;
                result.Add(dailyWeather);

            }

            return result;
        }


    }


   
}
