using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Machinata.Module.Services.Weather {


    #region Notes


    #endregion


    public class OpenWeatherMapProvider : WeatherDataProvider {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        public const string SERVER_URL = "https://api.openweathermap.org/";


        // 5 day 3 hour forecast
        // this call is avaiable free for minimal subscription
        //http://api.openweathermap.org/data/2.5/forecast?q=London,us&APPID=5927f5b84b9e740dfe6191f2cd0d2cf7
        public OpenWeatherMapProvider() {


        }

        public override IEnumerable<WeatherDataEntry> GetWeatherData(string location, DateTime start, DateTime end) {
            // http://api.openweathermap.org/data/2.5/forecast?q=London,us&APPID={key}
            var key = Core.Config.GetStringSetting("OpenWeatherMapAPIKey");
            var url = $"http://api.openweathermap.org/data/2.5/forecast?q={location}&APPID={key}&units=metric";
            var json = Core.Util.HTTP.GetJSON(url);
            return _parseJSONResults(json, url);
        }




        private IEnumerable<WeatherDataEntry> _parseJSONResults(JToken json, string url) {
            var result = new List<WeatherDataEntry>();

            var index = 0;
            foreach (var entry in json["list"]) {

                WeatherDataEntry data = new WeatherDataEntry();
                data.MaxTemperatur = entry["main"]["temp_max"].Value<decimal>();
                data.MinTemperatur = entry["main"]["temp_min"].Value<decimal>();
                data.Temperatur = entry["main"]["temp"].Value<decimal>();
                data.Weather = entry["weather"][0]["main"]?.ToString();
                data.WeatherDescription = entry["weather"][0]["description"]?.ToString();
                data.WeatherID = entry["weather"][0]["id"].Value<int>();
                data.Source = url;
                data.Date = DateTime.ParseExact(entry["dt_txt"]?.ToString(),"yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                data.EndDate = data.Date.Value.AddHours(3).AddSeconds(-1);
                data.Now = index == 0;
                data.Id = entry["dt_txt"]?.ToString();
                result.Add(data);
                index++;

            }


            return result;
        }
    }
}