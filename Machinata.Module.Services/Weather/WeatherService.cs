﻿using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Machinata.Module.Services.Weather.WeatherService;

namespace Machinata.Module.Services.Weather {

    public class WeatherService {

        public enum WeatherTypes { Clouds , Rainy}


        public static IEnumerable<WeatherDataEntry> GetWeatherData(string location, DateTime start, DateTime end) {
            var provider = new OpenWeatherMapProvider();
            var data = provider.GetWeatherData(location, start, end);
            return data;
        }

        /// <summary>
        /// Gets current weather and x days
        /// </summary>
        /// <param name="location"></param>
        /// <param name="start"></param>
        /// <param name="days"></param>
        /// <returns></returns>
        public static IEnumerable<WeatherDataEntry> GetWeatherDataDaily(string location, DateTime start, int days) {
            var provider = new OpenWeatherMapProvider();

            List<WeatherDataEntry> result = provider.GetWeatherDataDaily(location, start, days);

            return result;
        }

        
    }


    public class WeatherDataEntry {

        public DateTime? Date { get; set; }
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Marks the entry as the current weather
        /// </summary>
        public bool Now { get; set; }

        // Main weather category
        public string Weather { get; set; }

        // sub category/description of weather
        public string WeatherDescription { get; set; }

        /// <summary>
        /// proprietary icon from Provider
        /// </summary>
        public int WeatherID { get; set; }



        /// <summary>
        /// Min temperatur
        /// </summary>
        public decimal? MinTemperatur { get; set; }

        /// <summary>
        /// Max temperatur
        /// </summary>
        public decimal? MaxTemperatur { get; set; }

        /// <summary>
        /// temperatur
        /// </summary>
        public decimal? Temperatur { get; set; }


        /// <summary>
        /// URL of call
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// Id for this entry
        /// </summary>
        public string Id { get; set; }

        public override string ToString() {
            return $"{this.Date} : {MinTemperatur}-{MaxTemperatur}";
        }

    }
}
