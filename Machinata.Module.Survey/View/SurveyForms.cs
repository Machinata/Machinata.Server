﻿using Machinata.Core.Builder;
using Machinata.Module.Survey.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Survey.View {
    public class SurveyForms {

        public static FormBuilder GetEditForm(SurveyQuestion entity) {
            var form = new FormBuilder(Forms.Admin.EDIT);
            if (entity.Type == SurveyQuestion.QuestionTypes.MultipleChoice) {
                form.Exclude(nameof(entity.Required));
            }
            if (entity.Type == SurveyQuestion.QuestionTypes.TextInput) {
                form.Exclude(nameof(entity.DisplayMode));
            }

            return form;
        }

        public static FormBuilder GetAnswerForm(FormBuilder form, SurveyQuestion question, SurveyAnswer answer) {
           
            if (question.Type == SurveyQuestion.QuestionTypes.TextInput) {
                form.Exclude(nameof(answer.AllowTextInput));
            }

            if (question.Type != SurveyQuestion.QuestionTypes.MultipleChoice) {
                form.Exclude(nameof(answer.IsExclusive));
            }

            return form;
        }

    }
}
