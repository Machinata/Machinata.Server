using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;

using Newtonsoft.Json;

namespace Machinata.Module.Survey.Model {

    [Serializable()]
    [ModelClass] 
    public partial class SurveyQuestion : ModelObject,IPublishedModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enum
        public enum QuestionTypes {
            MultipleChoice,
            SingleChoice,
            TextInput,
        }

        public enum AnswerDisplayModes {
            Default,
            Vertical,
            Horizontal,
            
        }
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public SurveyQuestion() {
            this.Text = new TranslatableText();
            this.ShortURL = new TranslatableText();
          
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [Required]
        [FormBuilder(Forms.Admin.LISTING)]
      //  [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public TranslatableText Text { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        public bool Published { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
       // [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public bool Required { get; set; }

        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [Column]
        [Required]
        public TranslatableText ShortURL { get; set; }

        //[FormBuilder(Forms.Admin.EDIT)]
        //[FormBuilder(Forms.Admin.VIEW)]
        //[FormBuilder(Forms.Admin.LISTING)]
        [Column]
        [Required]
        public int Sort { get; set; }

        [Column]
        [Required]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public QuestionTypes Type { get; set; }


        [Column]
        [Required]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public AnswerDisplayModes DisplayMode { get; set; }

        // ALTER TABLE `surveyquestions` ADD COLUMN `DisplayMode` INT NOT NULL;
        // ALTER TABLE `surveyanswers` ADD COLUMN `AllowTextInput` TINYINT ( 1 ) NOT NULL;
        // ALTER TABLE `surveysurveys` ADD COLUMN `ShowContactForm` TINYINT ( 1 ) NOT NULL;




        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [Column]
        public virtual ICollection<SurveyAnswer> Answers { get; set; }

        [Column]
        public virtual SurveySurvey Survey { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////


        public string Summary {
            get {
                var text = this.Text?.GetDefault();
                if (string.IsNullOrWhiteSpace(text) == false) {
                    return Core.Util.String.CreateSummarizedText(text, 40, true);
                }
                return ""  +this.PublicId;
            }
        }


        public string TitleShort {
            get {
                var text = this.Text?.GetDefault();
                if (string.IsNullOrWhiteSpace(text) == false) {
                    return Core.Util.String.CreateSummarizedText(text, 100, true);
                }
                return "" + this.PublicId;
            }
        }


        public bool SupportsPie() {
            return this.Type != QuestionTypes.TextInput;
        }

        [FormBuilder(Forms.Admin.LISTING, Order = "0")]
        [FormBuilder(Forms.Admin.VIEW)]
        public int Number {
            get { return this.Sort + 1; }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////



        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override string ToString() {
            return this.Summary;
        }

        public override void OnDelete(ModelContext db) {
            base.OnDelete(db);
            // Questions and Answers
            var anwswers = db.SurveyAnswers()
               .Include(nameof(SurveyAnswer.Question)).Where(a => a.Question != null && a.Question.Id == this.Id).ToList();

            foreach (var answer in anwswers) {
                db.DeleteEntity(answer);
            }

          
        }

        public override void Validate() {
            base.Validate();

            if (this.Type == SurveyQuestion.QuestionTypes.MultipleChoice && this.Required == true) {
                throw new BackendException("not-supported", "Multiple Choice required questions are not supported yet.");

            }
        }


        #endregion

    }



    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextSurveyQuestionExtenions {

        public static DbSet<SurveyQuestion> SurveyQuestions(this Core.Model.ModelContext context) {
            return context.Set<SurveyQuestion>();
        }
    }

    #endregion
}
