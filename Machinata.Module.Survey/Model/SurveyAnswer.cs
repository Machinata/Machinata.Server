using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

using Machinata.Core.Builder;
using Machinata.Core.Model;

using Newtonsoft.Json;

namespace Machinata.Module.Survey.Model {

    [Serializable()]
    [ModelClass] 
    public partial class SurveyAnswer : ModelObject,IPublishedModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enum
       
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public SurveyAnswer() {
            this.Text = new TranslatableText();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [Required]
        [FormBuilder(Forms.Admin.LISTING)]
        //[FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public TranslatableText Text { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        public decimal? NumericalValue { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public bool Published { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public bool AllowTextInput { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public bool IsExclusive { get; set; }


        [Column]
        [Required]
        public int Sort { get; set; }



        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////



        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [Column]
        public virtual ICollection<SurveyAnswerParticipation> QuestionParticipations { get; set; }

        [Column]
        public virtual SurveySurvey Survey { get; set; }

        [Column]
        public virtual SurveyQuestion Question { get; set; }
        
        public string TitleShort {
            get {
                var text = this.Text?.GetDefault();
                if (string.IsNullOrWhiteSpace(text) == false) {
                    return Core.Util.String.CreateSummarizedText(text, 100, true);
                }
                return "" + this.PublicId;
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

       

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override string ToString() {
            var text = this.Text?.GetDefault();
            if (string.IsNullOrWhiteSpace(text)== false) {
                return text;
            }
            return base.ToString();
        }


        #endregion

    }
    


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextSurveyAnswerExtenions {

        public static DbSet<SurveyAnswer> SurveyAnswers(this Core.Model.ModelContext context) {
            return context.Set<SurveyAnswer>();
        }
    }

    #endregion
}
