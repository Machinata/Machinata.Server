using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Machinata.Core.Builder;
using Machinata.Core.Model;

using Newtonsoft.Json;

namespace Machinata.Module.Survey.Model {

    [Serializable()]
    [ModelClass] 
    public partial class SurveyAnswerParticipation : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enum
      
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public SurveyAnswerParticipation() {
            
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        

        [Column]
        public int? SessionId { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING, Order = "E")]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public string Text { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [Column]
       // [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [ForeignKey("SessionId")]
        public virtual SurveySession Session { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING, Order = "A")]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public virtual SurveySurvey Survey { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING, Order = "C")]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public virtual SurveyQuestion Question { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.LISTING, Order = "D")]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public virtual SurveyAnswer Answer { get; set; }



        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder(Forms.Admin.LISTING, Order = "F")]
        public DateTime Date {
            get {
                return this.Created;
            }
        }

        /// <summary>
        /// GUID from the Session object
        /// </summary>
        [FormBuilder(Forms.Admin.LISTING, Order = "G")]
        public string SessionID {
            get {
                return this.Session?.SessionId;
            }
        }


        [FormBuilder(Forms.System.LISTING)]
        public string SurveyId {
            get {
                return this.Survey?.PublicId;
            }
        }

        [FormBuilder(Forms.Admin.LISTING, Order = "B")]
        public int QuestionNumber {
            get {
                return this.Question?.Number??0;
            }
        }

        public string Summary {
            get {
                var text = this.Text;
                if (string.IsNullOrWhiteSpace(text) == false) {
                    return Core.Util.String.CreateSummarizedText(text, 40, true);
                }
                return "" + this.PublicId;
            }
        }


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////



        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override string ToString() {
            //return base.ToString();
            return $"{this.Session}: {this.Survey} - {this.Question}: {this.Answer}";


        }


        #endregion

    }
    


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextSSurveyUserParticipationExtenions {

        public static DbSet<SurveyAnswerParticipation> SurveyQuestionParticipations(this Core.Model.ModelContext context) {
            return context.Set<SurveyAnswerParticipation>();
        }

        public static DbQuery<SurveyAnswerParticipation> IncludeAll(this DbQuery<SurveyAnswerParticipation> participations) {
            participations
                 .Include(nameof(SurveyAnswerParticipation.Survey))
                 .Include(nameof(SurveyAnswerParticipation.Session) + "." + nameof(SurveySession.User))
                 .Include(nameof(SurveyAnswerParticipation.Answer))
                 .Include(nameof(SurveyAnswerParticipation.Question))
                   ;
            return participations;
        }
    }

    #endregion
}
