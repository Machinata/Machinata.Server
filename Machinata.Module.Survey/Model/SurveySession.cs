using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;

using Newtonsoft.Json;

namespace Machinata.Module.Survey.Model {

    /// <summary>
    /// A session can be re used to participate multiple times in a survey or surves
    /// </summary>
    [Serializable()]
    [ModelClass] 
    public partial class SurveySession : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enum
       
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public SurveySession() {
            this.Surveys = new List<SurveySurvey>();
            this.Infos = new Properties();
        }   

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
       // [Required]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Name { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Email { get; set; }

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string SessionId { get; set; }

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
       // [FormBuilder(Forms.Admin.LISTING)]
        public Properties Infos { get; set; }


        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////


        [Column]
        public virtual User User { get; set; }

        [Column]
        public virtual ICollection<SurveyAnswerParticipation> Participations { get; set; }

        [Column]
        public virtual ICollection<SurveySurvey> Surveys { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder(Forms.Admin.LISTING)]
        public DateTime Date {
            get {
                return this.Created;
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////



        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////


        public override string ToString() {
            return this.Name;
        }

        public override void OnDelete(ModelContext db) {
            base.OnDelete(db);

            // Participations
            var participations = db
               .SurveyQuestionParticipations()
               .IncludeAll()
               .Where(p =>p.Session != null && p.Session.Id == this.Id).ToList();
            foreach (var participation in participations) {
                db.DeleteEntity(participation);
            }


        }

        #endregion

    }
    


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextSurveyUserExtenions {

        public static DbSet<SurveySession> SurveySessions(this Core.Model.ModelContext context) {
            return context.Set<SurveySession>();
        }

        public static T GetByGuid<T>(this IQueryable<T> query, Core.Handler.Handler handler, string guid, bool throwExceptions = true) where T : Model.SurveySession {
            var ret = query.Where(e => e.SessionId == guid).SingleOrDefault();
            if (ret == null && throwExceptions) {
                throw new LocalizedException(handler, "session-not-found");
            }
            return ret;
        }

        public static DbQuery<SurveySession> IncludeAll(this DbQuery<SurveySession> sessions) {
         

            sessions
                 .Include(nameof(SurveySession.Surveys))
                
                 .Include(nameof(SurveySession.Participations) + "." + nameof(SurveyAnswerParticipation.Question))
                 .Include(nameof(SurveySession.Participations) + "." + nameof(SurveyAnswerParticipation.Answer))
                 .Include(nameof(SurveySession.User));
            return sessions;
        }


    }

    #endregion
}
