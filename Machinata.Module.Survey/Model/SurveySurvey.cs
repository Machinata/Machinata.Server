using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;

using Newtonsoft.Json;

namespace Machinata.Module.Survey.Model {

    [Serializable()]
    [ModelClass] 
    public partial class SurveySurvey : ModelObject,IPublishedModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public SurveySurvey() {
            this.ShortURL = new TranslatableText();
            this.Title = new TranslatableText();
            this.ThankYouText = new TranslatableText();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [Required]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public TranslatableText Title { get; set; }

        [Column]
        [Required]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public bool Published { get; set; }

        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [Column]
        [Required]
        public TranslatableText ShortURL { get; set; }

        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [Column]
        public TranslatableText ThankYouText { get; set; }


        [Column]
        [Required]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public bool ShowContactForm { get; set; }


        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [Column]
        public virtual ICollection<SurveyQuestion> Questions { get; set; }

        [Column]
        public ICollection<SurveyAnswerParticipation> Participations { get; set; }

        [Column]
        public ICollection<SurveySession> Sessions { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder(Forms.Admin.VIEW)]
        public string FrontendURL {
            get {
                var language = this.ContextLanguage();
                var url = Core.Localization.Text.GetTranslatedTextByIdForLanguage(Config.SurveyFrontendURLRoutesKey, language, $"No translation found for '{Config.SurveyFrontendURLRoutesKey}'");
                url = url.Replace("{shortUrl}", this.ShortURL.GetForLanguage(language));
                //var url = $"{prefix}{this.ShortURL.GetForLanguage(language)}";
                return url;
            }
        }

        public string TitleShort {
            get {
                var text = this.Title?.GetDefault();
                if (string.IsNullOrWhiteSpace(text) == false) {
                    return Core.Util.String.CreateSummarizedText(text, 100, true);
                }
                return "" + this.PublicId;
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////



        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override void OnDelete(ModelContext db) {
            base.OnDelete(db);

            // Participations
            var participations = db
               .SurveyQuestionParticipations()
               .IncludeAll()
               .Where(p => p.Survey.Id == this.Id).ToList();
            foreach(var participation in participations) {
                db.DeleteEntity(participation);
            }

            // Questions and Answers
            var questions = db.SurveyQuestions()
               .Include(nameof(SurveyQuestion.Answers)).Where(q => q.Survey.Id == this.Id).ToList();
            foreach (var question in questions) {
                db.DeleteEntity(question);
            }

        }

       

        public override string ToString() {
            return this.Title?.GetDefault();
        }

        #endregion

    }
    


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextSurveySurveysExtenions {

        public static DbSet<SurveySurvey> SurveySurveys(this Core.Model.ModelContext context) {
            return context.Set<SurveySurvey>();
        }

        public static DbQuery<SurveySurvey> IncludeQuestionsAndAnswers(this DbQuery<SurveySurvey> surveys) {
            var querychallenges = surveys.Include(nameof(SurveySurvey.Questions) + "." + nameof(SurveyQuestion.Answers));
            return querychallenges;
        }

        public static T GetByShortUrl<T>(this IQueryable<T> query, string shortUrl, Core.Handler.Handler handler, string errorId = "survey-not-found", bool throwExceptions = true) where T : Model.SurveySurvey {
            // Stored as JSON string (TranslatableText)
            // For example:
            // {"de":"mujingas-fit-list","default":"mujingas-fit-list","fr":"mujingas-fit-liste","it":"mujingas-fit-lista"}
            var searchParamA = $"\"{handler.Language}\":\"{shortUrl}\""; //TODO: escape JSON?
            var searchParamB = $"\"default\":\"{shortUrl}\""; //TODO: escape JSON?
            var result = query
                .Where(e => e.ShortURL.Data.Contains(searchParamA) || e.ShortURL.Data.Contains(searchParamB));
            if (result.Count() > 1) { throw new Exception($"more than one surveys found with '{shortUrl}'"); }
            var ret = result.FirstOrDefault();
            if (ret == null && throwExceptions) throw new LocalizedException(handler, errorId);
            return ret;
        }
    }

    #endregion
}
