﻿survey.surveys.en=Surveys
survey.survay.en=Survey
survey.questions.en=Questions
survey.question.en=Question
survey.add-question.en=Add Question
survey.add-answer.en=Add Answer
survey.answers.en=Answers
survey.participations.en=Participations
survey.sessions.en=Sessions
survey.session.en=Session

error.survey-not-found.en=The survey could not be found.
error.survey-not-found.de=Der survey konnte nicht gefunden werden.

error.user-not-found.en=The user could not be found.
error.user-not-found.de=Der User konnte nicht gefunden werden.

error.survey-submit.en=Unfortunately, an error occurred when submitting the survey.
error.survey-submit.de=Bei der Übermittlung der Umfrage ist leider ein Fehler aufgetreten.

survey.user.name.en=Name
survey.user.name.de=Name

survey.user.email.en=Email
survey.user.email.de=E-Mail

survey.your-details.en=Your Details
survey.your-details.de=Deine Angaben


survey.form.first-name.en=Firstname
survey.form.first-name.de=Vorname

survey.form.last-name.en=Lastname
survey.form.last-name.de=Nachname

survey.form.submit.en=Submit
survey.form.submit.de=Abschicken

survey.form.email.en=Email
survey.form.email.de=E-Mail

survey.success.title.de=Danke!
survey.success.title.en=Thank you.
survey.success.title.it=Grazie
survey.success.title.fr=Merci

survey.success.message.de=Vielen Dank, dass Sie an unserer Umfrage teilgenommen haben!
survey.success.message.en=Thank you for taking part in our survey!
survey.success.message.it=Grazie per aver partecipato al nostro sondaggio!
survey.success.message.fr=Merci d'avoir participé à notre enquête !