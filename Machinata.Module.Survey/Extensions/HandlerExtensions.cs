﻿using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Survey.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Survey.Extensions {


    public static class HandlerExtensions {

        /// <summary>
        /// Tries to retrieve the variable value from the Request Parameters and then Cookie
        /// </summary>
        /// <param name="handler"></param>
        /// <param name="sessionIdName"></param>
        /// <returns></returns>
        public static string GetOrCreateSessionId(this Core.Handler.Handler handler, string sessionIdName="session-id") {
            var sessionId = handler.Params.String(sessionIdName, null);
            if (string.IsNullOrWhiteSpace(sessionId) == true) {
                var cookie = handler.Context.Request.Cookies[sessionIdName];
                if (cookie != null && string.IsNullOrEmpty(cookie.Value) == false) {
                    sessionId = cookie.Value;
                }
            }

            // Still no session-id?
            if (string.IsNullOrWhiteSpace(sessionId) == true) {
                sessionId = Guid.NewGuid().ToString();
            }

            return sessionId;
        }

    }
}
