﻿using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Survey.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Survey.Extensions {
    public static class PageTemplateExtensions {

        public static void InsertSurvey(this PageTemplate template, PageTemplateHandler handler, SurveySurvey survey, string variableName, string templateName = "/default/survey-ui", string onSuccess = "Machinata.reload") {
            var apiCall = $"/api/surveys/survey/{survey.PublicId}/add-participation";
            var surveyTemplate = PageTemplate.LoadForTemplateContextAndName(template, templateName, false);
            surveyTemplate.InsertVariables("survey", survey);
            surveyTemplate.InsertVariable("survey.api-call", apiCall);
            surveyTemplate.InsertVariable("survey.on-success", onSuccess);
            var questionTemplates = new List<PageTemplate>();
            foreach (var question in survey.Questions.Published().OrderBy(q => q.Sort)) {
                var classes = new List<string>();
                classes.Add("display-mode-" + question.DisplayMode.ToString().ToLowerInvariant());
                var questionTemplate = surveyTemplate.LoadTemplate(templateName + ".question");

                questionTemplate.InsertVariable("question.classes", string.Join(" ", classes));

                questionTemplate.InsertVariables("question", question);


                var answerTemplates = new List<PageTemplate>();
                foreach (var answer in question.Answers.Published().OrderBy(q => q.Sort)) {
                    

                    var answerTemplate = questionTemplate.LoadTemplate(templateName + ".question.answer."+question.Type.ToString().ToLower());
                    answerTemplate.InsertVariables("answer", answer);
                    answerTemplate.InsertVariable("required", question.Required == true ? "required='required'": "");
                    answerTemplate.InsertVariables("question", question);
                  

                    answerTemplates.Add(answerTemplate);
                }
                questionTemplate.InsertTemplates("answers", answerTemplates);
                questionTemplates.Add(questionTemplate);
            }
            surveyTemplate.InsertTemplates("questions", questionTemplates);
            template.InsertTemplate(variableName, surveyTemplate);

            // Inject
            template.InsertVariable("session-id", handler.GetOrCreateSessionId());
        }
    }
}
