using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Survey {
    public class Config {

        public static string SurveyFrontendURLRoutesKey = Core.Config.GetStringSetting("SurveyFrontendURLRoutesKey", "routes.survey");

    }
}
