
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core;
using Machinata.Module.Survey.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Reporting;

namespace Machinata.Module.Survey.Handler {


    public class SurveyAdminHandler : AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("surveys");
        }

        #endregion

        #region Menu 

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "clipboard-list",
                Path = "/admin/surveys",
                Title = "{text.survey.surveys}",
                Sort = "500"
            });
        }

        #endregion

        public override void DefaultNavigation() {
            base.DefaultNavigation();
            // Navigation
            this.Navigation.Add("surveys", "{text.survey.surveys}");

        }

        [RequestHandler("/admin/surveys")]
        public void Default() {

            var entities = this.DB.SurveySurveys().AsQueryable();
            // Listing
            entities = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList(
                variableName: "entities",
                entities: entities,
                form: new Core.Builder.FormBuilder(Forms.Admin.LISTING),
                link: "admin/surveys/survey/{entity.public-id}"
            );


        }


        [RequestHandler("/admin/surveys/all")]
        public void All() {

            // Listing
            var entities = this.DB.SurveySurveys().AsQueryable();
            entities = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList(
                variableName: "entities",
                entities: entities,
                form: new Core.Builder.FormBuilder(Forms.Admin.LISTING),
                link: "admin/surveys/survey/{entity.public-id}"
            );

            // Navigation
            this.Navigation.Add("all");

        }

        [RequestHandler("/admin/surveys/create")]
        public void Create() {
            this.RequireWriteARN();


            var form = new FormBuilder(Forms.Admin.CREATE);
            form.Custom("title", "title", value: null, required: true, formType: "text");

            var entity = new SurveySurvey();

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: form,
               apiCall: "/api/admin/surveys/create",
               onSuccess: "/admin/surveys/survey/{entity.public-id}"
            );

            // Navigation
            this.Navigation.Add("create");
        }


        [RequestHandler("/admin/surveys/survey/{publicId}")]
        public void Survey(string publicId) {
            this.RequireWriteARN();

            // Init
            var entity = DB.SurveySurveys()
                .IncludeQuestionsAndAnswers()
                .Include(nameof(SurveySurvey.Sessions))
                .GetByPublicId(publicId);


            // Variables
            this.Template.InsertPropertyList(
               variableName: "entity",
               entity: entity,
               form: new FormBuilder(Forms.Admin.VIEW)
            );
            this.Template.InsertVariables("entity", entity);

            // Questions
            this.Template.InsertSortableList(
                "questions",
                entity.Questions.OrderBy(q => q.Sort).AsQueryable(),
                link: "/admin/surveys/survey/" + publicId + "/questions/question/{entity.public-id}",
                sortingAPICall: $"/api/admin/surveys/survey/{entity.PublicId}/questions/sort"
                );


            var sessions = entity.Sessions.AsQueryable();
            var sesionsPaged = this.Template.Paginate(sessions, this);

            this.Template.InsertEntityList(
                "sessions",
                sesionsPaged,
                link: "/admin/surveys/sessions/session/{entity.public-id}"
                );

            // Navigation
            this.Navigation.Add($"survey/{publicId}", entity.Title?.GetDefault());

        }



        [RequestHandler("/admin/surveys/survey/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();

            // Init
            var entity = DB.SurveySurveys()
           .Include(nameof(SurveySurvey.Questions) + "." + nameof(SurveyQuestion.Answers))
           .GetByPublicId(publicId);

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: "/api/admin/surveys/survey/" + entity.PublicId + "/edit",
               onSuccess: "/admin/surveys/survey/" + entity.PublicId
            );

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add($"survey/{publicId}", entity.Title?.GetDefault());
            this.Navigation.Add($"edit");
        }

        [RequestHandler("/admin/surveys/survey/{publicId}/add-question")]
        public void SurveyAddQuestion(string publicId) {
            this.RequireWriteARN();

            // Init
            var survey = DB.SurveySurveys()
           .Include(nameof(SurveySurvey.Questions) + "." + nameof(SurveyQuestion.Answers))
            .GetByPublicId(publicId);

            var entity = new SurveyQuestion();
            var form = new FormBuilder(Forms.Admin.CREATE);
            form.Custom("text", "text", value: null, required: true, formType: "text");

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: form,
               apiCall: "/api/admin/surveys/survey/" + survey.PublicId + "/add-question",
               onSuccess: "/admin/surveys/survey/" + survey.PublicId
            );

            // Variables
            this.Template.InsertVariables("entity", survey);

            // Navigation
            this.Navigation.Add($"survey/{publicId}", survey.Title?.GetDefault());
            this.Navigation.Add($"add-question", "{text.survey.add-question}");
        }

        [RequestHandler("/admin/surveys/survey/{surveyId}/questions/question/{publicId}")]
        public void Question(string surveyId, string publicId) {
            this.RequireWriteARN();

            // Grab entity
            var survey = DB.SurveySurveys()
                .IncludeQuestionsAndAnswers()
                .GetByPublicId(surveyId);

            var entity = survey.Questions.AsQueryable().GetByPublicId(publicId);


            this.Template.InsertPropertyList(
               variableName: "entity",
               entity: entity,
               form: new FormBuilder(Forms.Admin.VIEW)
            );


            // Answers
            this.Template.InsertSortableList(
               "answers",
               entity.Answers.OrderBy(q => q.Sort).AsQueryable(),
               link: "/admin/surveys/survey/" + surveyId + "/questions/question/" + publicId + "/answers/answer/{entity.public-id}",
               sortingAPICall: $"/api/admin/surveys/question/{entity.PublicId}/answers/sort"
            );

            // Sessions
            var participations = this.DB.SurveyQuestionParticipations()
                .IncludeAll()
                .Where(p => p.Survey.Id == survey.Id && p.Question.Id == entity.Id && p.Session != null);
            var sessions = participations.Select(p => p.Session).DistinctBy(p => p.Id).AsQueryable();
            var sesionsPaged = this.Template.Paginate(sessions, this);
            this.Template.InsertEntityList(
                "sessions",
                sesionsPaged,
                link: "/admin/surveys/sessions/session/{entity.public-id}"
                );

            // Variables
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertVariableBool("entity.supports-pie", entity.SupportsPie());
            this.Template.InsertVariables("survey", survey);

            // Navigation
            this.Navigation.Add($"survey/{surveyId}", survey.TitleShort);
            this.Navigation.Add($"questions/question/" + publicId, entity.TitleShort, entity.TitleShort);
        }


        [RequestHandler("/admin/surveys/survey/{surveyId}/participations/participation/{publicId}")]
        public void Participation(string surveyId, string publicId) {
            this.RequireWriteARN();

            // Grab entity
            var survey = DB.SurveySurveys()
                .Include(nameof(SurveySurvey.Participations) + "." + nameof(SurveyAnswerParticipation.Session))
                .IncludeQuestionsAndAnswers()
                .GetByPublicId(surveyId);

            var entity = survey.Participations.AsQueryable().GetByPublicId(publicId);

            this.Template.InsertPropertyList(
               variableName: "entity",
               entity: entity,
               form: new FormBuilder(Forms.Admin.VIEW)
            );

            // Variables
            this.Template.InsertVariables("entity", entity);


            // Navigation
            this.Navigation.Add($"survey/{surveyId}", survey.TitleShort);
            this.Navigation.Add($"participations/participation/" + publicId, entity.ToString());
        }

        [RequestHandler("/admin/surveys/survey/{surveyId}/participations/participation/{publicId}/edit")]
        public void ParticipationEdit(string surveyId, string publicId) {
            this.RequireWriteARN();

            //Grab entity
            var survey = DB.SurveySurveys()
                .Include(nameof(SurveySurvey.Participations))
                .IncludeQuestionsAndAnswers()
                .GetByPublicId(surveyId);

            var entity = survey.Participations.AsQueryable().GetByPublicId(publicId);

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: "/api/admin/surveys/participation/" + entity.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add($"survey/{surveyId}", survey.Title?.GetDefault());
            this.Navigation.Add($"participations/participation/" + publicId, entity.ToString());
            this.Navigation.Add($"edit");
        }



        [RequestHandler("/admin/surveys/survey/{surveyId}/questions/question/{publicId}/edit")]
        public void QuestionEdit(string surveyId, string publicId) {
            this.RequireWriteARN();

            //Grab entity
            var survey = DB.SurveySurveys()
                .Include(nameof(SurveySurvey.Questions) + "." + nameof(SurveyQuestion.Answers))
                .GetByPublicId(surveyId);

            var entity = survey.Questions.AsQueryable().GetByPublicId(publicId);
            FormBuilder form = View.SurveyForms.GetEditForm(entity);

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: form,
               apiCall: "/api/admin/surveys/question/" + entity.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add($"survey/{surveyId}", survey.Title?.GetDefault());
            this.Navigation.Add($"questions/question/" + publicId, entity.TitleShort, entity.Text?.GetDefault());
            this.Navigation.Add($"edit");
        }

      
        [RequestHandler("/admin/surveys/survey/{surveyId}/questions/question/{publicId}/add-answer")]
        public void QuestionAddAnswer(string surveyId, string publicId) {
            this.RequireWriteARN();

            //Grab entity
            var survey = DB.SurveySurveys()
                .Include(nameof(SurveySurvey.Questions) + "." + nameof(SurveyQuestion.Answers))
                .GetByPublicId(surveyId);

            var question = survey.Questions.AsQueryable().GetByPublicId(publicId);

            var entity = new SurveyAnswer();

            var form = new FormBuilder(Forms.Admin.CREATE);
            form.Exclude(nameof(entity.NumericalValue));
            form.Custom("text", "text", value: null, required: true, formType: "text");
            form.Custom("Numerical Value", "numericalvalue", value: null, required: false, formType: "text"); // hack to move it down in form
           

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: form,
               apiCall: "/api/admin/surveys/question/" + question.PublicId + "/add-answer",
               onSuccess: "{page.navigation.prev-path}"
            );

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add($"survey/{surveyId}", survey.Title?.GetDefault());
            this.Navigation.Add($"questions/question/" + publicId, question.TitleShort, question.Text?.GetDefault());
            this.Navigation.Add($"add-answer", "{text.survey.add-answer}");
        }


        [RequestHandler("/admin/surveys/survey/{surveyId}/questions/question/{questionId}/answers/answer/{publicId}")]
        public void Answer(string surveyId, string questionId, string publicId) {
            this.RequireWriteARN();

            // Grab entity
            var survey = DB.SurveySurveys()
                .Include(nameof(SurveySurvey.Questions) + "." + nameof(SurveyQuestion.Answers))
                .GetByPublicId(surveyId);

            var question = survey.Questions.AsQueryable().GetByPublicId(questionId);

            var entity = question.Answers.AsQueryable().GetByPublicId(publicId);

            this.Template.InsertPropertyList(
               variableName: "entity",
               entity: entity,
               form: View.SurveyForms.GetAnswerForm(new FormBuilder(Forms.Admin.VIEW), question, entity)
            );



            var participations = this.DB.SurveyQuestionParticipations()
                .IncludeAll()
                .Where(p => p.Survey.Id == survey.Id && p.Question.Id == question.Id && p.Answer.Id == entity.Id && p.Session != null);

            // Sessions
            var sessions = participations.Select(p => p.Session).DistinctBy(p => p.Id).AsQueryable();
            var sesionsPaged = this.Template.Paginate(sessions, this);
            this.Template.InsertEntityList(
                "sessions",
                sesionsPaged,
                link: "/admin/surveys/sessions/session/{entity.public-id}"
                );

            // Participations
            var participationListingForm = new FormBuilder(Forms.EMPTY);
            if (question.Type == SurveyQuestion.QuestionTypes.TextInput) {
                participationListingForm.Include(nameof(SurveyAnswerParticipation.Text));
            }
            participationListingForm.Include(nameof(SurveyAnswerParticipation.Created));
            participationListingForm.Include(nameof(SurveyAnswerParticipation.SessionID));
            this.Template.InsertEntityList(
               "participations",

               participations,
               link: "/admin/surveys/survey/" + surveyId + "/participations/participation/{entity.public-id}",
                form: participationListingForm


               );


            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add($"survey/{surveyId}", survey.Title?.GetDefault());
            this.Navigation.Add($"questions/question/" + questionId, question.Summary);
            this.Navigation.Add($"answers/answer/" + publicId, entity.Text?.GetDefault());
        }

        [RequestHandler("/admin/surveys/survey/{surveyId}/questions/question/{questionId}/answers/answer/{publicId}/edit")]
        public void AnswerEdit(string surveyId, string questionId, string publicId) {
            this.RequireWriteARN();

            // Grab entity
            var survey = DB.SurveySurveys()
                .Include(nameof(SurveySurvey.Questions) + "." + nameof(SurveyQuestion.Answers))
                .GetByPublicId(surveyId);

            var question = survey.Questions.AsQueryable().GetByPublicId(questionId);

            var entity = question.Answers.AsQueryable().GetByPublicId(publicId);

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: View.SurveyForms.GetAnswerForm(new FormBuilder(Forms.Admin.EDIT), question, entity),
               apiCall: "/api/admin/surveys/answer/" + entity.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add($"survey/{surveyId}", survey.Title?.GetDefault());
            this.Navigation.Add($"questions/question/" + questionId, question.Summary);
            this.Navigation.Add($"answers/answer/" + publicId, entity.Text?.GetDefault());
            this.Navigation.Add($"edit");

          
        }

       

        [RequestHandler("/admin/surveys/sessions")]
        public void Sessions() {
            this.RequireWriteARN();


            var sessions = DB.SurveySessions()
                .Include(nameof(Model.SurveySession.Participations));

            // var sessions = participations.Select(p => p.Session).DistinctBy(p => p.Id).AsQueryable();
            var sesionsPaged = this.Template.Paginate(sessions, this);
            this.Template.InsertEntityList(
                "entities",
                sesionsPaged,
                link: "/admin/surveys/sessions/session/{entity.public-id}"
                );



            // Navigation
            this.Navigation.Add("sessions", "{text.survey.sessions}");
        }


        [RequestHandler("/admin/surveys/sessions/session/{publicId}")]
        public void Session(string publicId) {

            var entity = DB.SurveySessions()
                .Include(nameof(Model.SurveySession.Participations) + "." + nameof(Model.SurveyAnswerParticipation.Survey)) // this is somehow needed EF bug if this is inside IncludeAll it does not work

                .IncludeAll()
                .GetByPublicId(publicId);

            this.Template.InsertPropertyList(
               variableName: "entity",
               entity: entity,
               form: new FormBuilder(Forms.Admin.VIEW)
            );

            // Participations
            var form = new FormBuilder(Forms.Admin.LISTING);
            form.Exclude(nameof(SurveyAnswerParticipation.Session));
            var participations = new List<SurveyAnswerParticipation>();
            if (entity.Participations != null) {
                participations = entity.Participations.ToList();
            }
            this.Template.InsertEntityList(
                "participations",
                participations.AsQueryable()
                ,
                link: "/admin/surveys/survey/{entity.survey-id}/participations/participation/{entity.public-id}",
                form: form
                );

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("sessions", "{text.survey.sessions}");
            this.Navigation.Add($"session/" + publicId, "{text.survey.session}: " + entity.ToString());
        }

        [RequestHandler("/admin/surveys/sessions/session/{publicId}/edit")]
        public void SessionEdit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.SurveySessions()
                .Include(nameof(Model.SurveySession.Participations)) // this is somehow needed EF bug if this is inside IncludeAll it does not work
                .IncludeAll()
                .GetByPublicId(publicId);


            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: "/api/admin/surveys/session/" + entity.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("sessions", "{text.survey.sessions}");
            this.Navigation.Add($"session/" + publicId, "{text.survey.session}: " + entity.ToString());
            this.Navigation.Add($"edit");
        }



        [RequestHandler("/admin/surveys/survey/{publicId}/report")]
        public void Report(string publicId) {

            this.RequireWriteARN();

            // Init
            var entity = DB.SurveySurveys()
                .IncludeQuestionsAndAnswers()
                .Include(nameof(SurveySurvey.Sessions))
                .GetByPublicId(publicId);


            // Variables
            this.Template.InsertPropertyList(
               variableName: "entity",
               entity: entity,
               form: new FormBuilder(Forms.Admin.VIEW)
            );
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add($"survey/{publicId}", entity.Title?.GetDefault());
            this.Navigation.Add($"report");
        }


        [RequestHandler("/admin/surveys/survey/{publicId}/export", null, null, Verbs.Get, ContentType.Binary)]
        public void ExportParticipations(string publicId) {

            // Init
            var entity = DB.SurveySurveys()
                .IncludeQuestionsAndAnswers()
                .Include(nameof(SurveySurvey.Participations))
                .Include(nameof(SurveySurvey.Sessions))
                .GetByPublicId(publicId);


            var lang = Core.Config.LocalizationDefaultLanguage;

            // Data
            var bytes = Reporting.Reports.Export(this.DB, entity, lang);

            // Filename
            var fileName = $"{Core.Config.ProjectName}_Survey_" + entity.Title?.GetForLanguage(lang) + "_" + Core.Util.Time.GetDefaultTimezoneNow().ToDefaultTimezoneDateString();

            // Send
            this.SendBinary(bytes, $"{fileName}.xlsx");
        }
    }
}


