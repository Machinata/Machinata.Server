﻿using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Module.Reporting.Model;
using Machinata.Module.Survey.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Survey.Handler.API.Admin {
    public class SurveyReportingAdminAPIHandler : Module.Admin.Handler.AdminAPIHandler {


        [RequestHandler("/api/admin/surveys/survey/{surveyId}/question/{publicId}")]
        public void DonutProjectsProjectBudget(string surveyId, string publicId) {
            // Init
            var survey = this.DB.SurveySurveys()
                .IncludeQuestionsAndAnswers()
                .GetByPublicId(surveyId);
            var question = survey.Questions.AsQueryable().GetByPublicId(publicId);

            // Init report
            Report report = new Report();
            report.Body.Theme = "default";
            var nodes = CreateQuestionNodes(question);
            foreach(var node in nodes) {
                report.AddToBody(node);
            }

            SendAPIMessage("chart-data", JObject.FromObject(report));
        }


        [RequestHandler("/api/admin/surveys/survey/{publicId}/report")]
        public void DonutProjectsProjectReport(string publicId) {
            // Init
            var survey = this.DB.SurveySurveys()
                .IncludeQuestionsAndAnswers()
                .GetByPublicId(publicId);

            // Init report
            Report report = new Report();
            report.Body.Theme = "default";
            foreach (var question in survey.Questions.Published().ToList()) {
                var chapter = report.NewChapter(question.TitleShort);
                chapter.Style = "W_H2";
                var nodes = CreateQuestionNodes(question);
                foreach (var node in nodes) {
                    chapter.AddChild(node);
                }
            }

            this.SendAPIMessage("chart-data", JObject.FromObject(report));
        }


        private List<Node> CreateQuestionNodes(SurveyQuestion question) {

            var result = new List<Node>();

            // Answers stats in percent
            if (question.Type == SurveyQuestion.QuestionTypes.TextInput) {
                var node = GetTextAnswerNode(question);
                if (node != null) {
                    result.Add(node);
                }
            } else if (question.SupportsPie() == false) {
                var infoNode = new InfoNode() { Message = $"{question.Type} does not support Donut" };
                //report.AddToBody(infoNode);
                result.Add(infoNode);
            } else {
                var donutNode = GetAnswersPie(question);

                //report.AddToBody(donutNode);
                if (donutNode != null) {
                    result.Add(donutNode);
                }

                // Any Text input
                if (question.Answers.Any(a => a.AllowTextInput == true)) {
                    var node = GetTextAnswerNode(question);
                    if (node != null) {
                        result.Add(node);
                    }
                }
            }

            // Numerical answers
            var numericalAnswersIds = question.Answers.Published().Where(a => a.NumericalValue != null).Select(a => a.Id).ToList();
            if (numericalAnswersIds.Count() > 0) {
                var participations = this.DB.SurveyQuestionParticipations()
                    .Include(nameof(SurveyAnswerParticipation.Answer))
                    .Where(p => numericalAnswersIds.Contains(p.Answer.Id));
                if (participations.Count() > 0) {
                    var sum = participations.Sum(p => p.Answer.NumericalValue.Value);
                    var average = participations.Average(p => p.Answer.NumericalValue.Value);

                    var metrics = new MetricsNode();
                    metrics.Screen = false;
                    metrics.SetTitle("Numerical Results");
                    metrics.Style = "Growing";
                    {
                        var group = metrics.AddGroup();
                        group.CreateItem().SetLabel("Sum").SetValue(sum.ToString("#.##"));
                        group.CreateItem().SetLabel("Average").SetValue(average.ToString("#.##"));

                    }
                    //report.AddToBody(metrics);
                    result.Add(metrics);
                }
            }

            if (result.Count() == 0) {
                result.Add(new InfoNode { Message = "No data yet." });
            }

            // return report;
            return result;
        }

        private Node GetAnswersPie(SurveyQuestion question) {
            var node = new DonutNode();
            node.Theme = "default";
            node.SetTitle("Answers");

            var maxCount = 0;
            var totalCount = 0;
            SurveyAnswer maxAnswer = null;
            foreach (var answer in question.Answers.Published()) {
                var participations = this.DB.SurveyQuestionParticipations()
                           .Include(nameof(SurveyAnswerParticipation.Answer))
                           .Where(p => p.Answer.Id == answer.Id);
                var count = participations.Count();
                totalCount += count;
                if (count > maxCount) {
                    maxCount = count;
                    maxAnswer = answer;
                }
                node.AddSliceNumber(count, answer.Text.GetDefault());
            }
            if(maxAnswer != null) {
                node.Status = new ChartStatus(maxAnswer.Text.GetForLanguage(this.Language), System.Math.Round(((double)maxCount / (double)totalCount) * 100)+"%");
            }
            node.LegendLabelsShowValue = true;
            if (totalCount == 0) {
                return null;
            }
           

            return node;
        }

        private VerticalTableNode GetTextAnswerNode(SurveyQuestion question) {
            var node = new VerticalTableNode();
            node.Theme = "default";
            node.SetTitle("Answers");
            var columns = new string[] {/*"Count", */"Text" };
            var columnTypes = new ValueTypes[] { /* ValueTypes.Count,*/ ValueTypes.Text };

            var group = node.AddColumns(question.Text?.GetForLanguage(this.Language), question.PublicId, 0, columns, columnTypes);

           
            var totalCount = 0;
          
            foreach (var answer in question.Answers.Published()) {
                var textGroups = this.DB.SurveyQuestionParticipations()
                           .Include(nameof(SurveyAnswerParticipation.Answer))
                           .Where(p => p.Answer.Id == answer.Id).ToList().GroupBy(p=>p.Text?.Trim());


                var rowGroup = node.AddRowGroup(answer.Text.GetForLanguage(this.Language), answer.PublicId, 0);

                foreach (var textGroup in textGroups.OrderByDescending(tx => tx.Count())) {
                    //var row = rowGroup.AddRow(textGroup.First().Summary + "");
                    var row = rowGroup.AddRow("");
                    //  row.AddRowValue(textGroup.Count(),textGroup.Count().ToString());

                    var text = textGroup.First().Text;
                    if (textGroup.Count() > 0) {
                        text += $" ({textGroup.Count()})";
                    }
                    row.AddRowValue(text, text);
                    totalCount++;
                }
                
            }
                     
            if (totalCount == 0) {
                return null;
            }


            return node;
        }
    }
}
