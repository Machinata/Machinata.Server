
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Module.Admin.Handler;
using Machinata.Core.Exceptions;
using Machinata.Module.Survey.Model;

namespace Machinata.Module.Survey.Handler {


    public class SurveyQuestionAdminAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Module.Survey.Model.SurveyQuestion> {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultRootPolicies("surveys");
        }

        #endregion

       
        [RequestHandler("/api/admin/surveys/question/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }

        [RequestHandler("/api/admin/surveys/question/{publicId}/edit")]
        public void Edit(string publicId) {
            var question = this.DB.SurveyQuestions().GetByPublicId(publicId);
            CRUDEdit(publicId,View.SurveyForms.GetEditForm(question));
        }

        [RequestHandler("/api/admin/surveys/question/{publicId}/add-answer")]
        public void AddAnswer(string publicId) {

            var question = DB.SurveyQuestions()
            .Include(nameof(SurveyQuestion.Survey))
            .Include(nameof(SurveyQuestion.Answers))
            .GetByPublicId(publicId);

            int sort = 0;
            if (question.Answers.Any() == true) {
                sort = question.Answers.Max(a => a.Sort) + 1;
            }

            var answer = new SurveyAnswer();
            var text = this.Params.String("text");
            answer.Survey = question.Survey;
            answer.Text.SetDefault(text);
            answer.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            answer.Sort = sort;
            answer.Validate();
            question.Answers.Add(answer);

            this.DB.SaveChanges();
            this.SendAPIMessage("success");
        }

        [RequestHandler("/api/admin/surveys/question/{publicId}/answers/sort")]
        public void SortTasks(string publicId) {

            this.RequireWriteARN();
            var question = DB.SurveyQuestions()
          .Include(nameof(SurveyQuestion.Answers) )
          .GetByPublicId(publicId);


            var orderIds = this.Params.StringArray("order", new string[] { });
            int order = 0;

            // Set the order/sort
            foreach (var id in orderIds) {
                var answer = question.Answers.AsQueryable().GetByPublicId(id);
                answer.Sort = order;
                order++;
            }

            // Save to DB...
            this.DB.SaveChanges();
            this.SendAPIMessage("success");

        }

    }
}
