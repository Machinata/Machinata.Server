
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Module.Admin.Handler;
using Machinata.Core.Exceptions;
using Machinata.Module.Survey.Model;

namespace Machinata.Module.Survey.Handler {


    public class SurveySessionAdminAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Module.Survey.Model.SurveySession> {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultRootPolicies("surveys");
        }

        #endregion

       
        [RequestHandler("/api/admin/surveys/session/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }

        [RequestHandler("/api/admin/surveys/session/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }

    

    }
}
