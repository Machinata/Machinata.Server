
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Module.Admin.Handler;
using Machinata.Core.Exceptions;
using Machinata.Module.Survey.Model;

namespace Machinata.Module.Survey.Handler {


    public class TemplatesAdminAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Module.Survey.Model.SurveySurvey> {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultRootPolicies("surveys");
        }

        #endregion

        protected override void CreatePopulate(SurveySurvey entity) {
            base.CreatePopulate(entity);
          
        }

        [RequestHandler("/api/admin/surveys/create")]
        public void Create() {
            //CRUDCreate("entity");
            var entity = new SurveySurvey();


            var title = this.Params.String("title");
            entity.Title.SetDefault(title);
            entity.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            var shortUrl = Core.Util.String.CreateShortURLForName(entity.Title.GetDefault());
            entity.ShortURL?.SetDefault(shortUrl);
            entity.Validate();

            this.DB.SurveySurveys().Add(entity);
            this.DB.SaveChanges();

            this.SendAPIMessage("success", new { Entity = new { PublicId = entity.PublicId } });
        }

        [RequestHandler("/api/admin/surveys/survey/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }

        [RequestHandler("/api/admin/surveys/survey/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }

        [RequestHandler("/api/admin/surveys/survey/{publicId}/add-question")]
        public void AddQuestion(string publicId) {

            var survey = DB.SurveySurveys()
            .Include(nameof(SurveySurvey.Questions) + "." + nameof(SurveyQuestion.Answers))
            .GetByPublicId(publicId);

            int sort = 0;
            if (survey.Questions.Any() == true) {
                sort = survey.Questions.Max(a => a.Sort) + 1;
            }

            var question = new SurveyQuestion();
            var text = this.Params.String("text");
            question.Text.SetDefault(text);
            var shortUrl = Core.Util.String.CreateShortURLForName(question.Summary);
            question.ShortURL.SetDefault(shortUrl);
            question.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            question.Sort = sort;
         

            question.Validate();
            survey.Questions.Add(question);

            this.DB.SaveChanges();
            this.SendAPIMessage("success");
        }


        [RequestHandler("/api/admin/surveys/survey/{publicId}/questions/sort")]
        public void SortTasks(string publicId) {

            this.RequireWriteARN();
            var survey = DB.SurveySurveys()
          .Include(nameof(SurveySurvey.Questions) + "." + nameof(SurveyQuestion.Answers))
          .GetByPublicId(publicId);


            var orderIds = this.Params.StringArray("order", new string[] { });
            int order = 0;

            // Set the order/sort
            foreach (var id in orderIds) {
                var question = survey.Questions.AsQueryable().GetByPublicId(id);
                question.Sort = order;
                order++;
            }

            // Save to DB...
            this.DB.SaveChanges();
            this.SendAPIMessage("success");

        }


        [RequestHandler("/api/admin/surveys/survey/{publicId}/reset-user-participations")]
        public void ResetUserParticipations(string publicId) {

            this.RequireWriteARN();
            var survey = DB.SurveySurveys()
                .Include(nameof(SurveySurvey.Sessions) + "." + nameof(SurveySession.Surveys))
                .GetByPublicId(publicId);


            // Delete participations
            var participations = this.DB.SurveyQuestionParticipations()
                .Include(nameof(SurveyAnswerParticipation.Survey))
                .Where(p => p.Survey != null && p.Survey.Id == survey.Id).ToList();

            foreach (var participation in participations) {
                this.DB.DeleteEntity(participation);
            }


            // Delete session with only this survey
            foreach (var session in survey.Sessions.ToList()) {
                if (session.Surveys.Count() == 1 && session.Surveys.Contains(survey)) {
                    this.DB.DeleteEntity(session);
                } else {
                    session.Surveys.Remove(survey); // only remove from this survey is used in others
                }
            }


            // Save to DB...
             this.DB.SaveChanges();
            this.SendAPIMessage("success");

        }

    }
}
