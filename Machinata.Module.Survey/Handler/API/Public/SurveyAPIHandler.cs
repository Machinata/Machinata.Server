
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Module.Admin.Handler;
using Machinata.Core.Exceptions;
using Machinata.Module.Survey.Model;
using Machinata.Module.Survey.Extensions;

namespace Machinata.Module.Survey.Handler {


    public class SurveyAPIHandler : APIHandler {
        private const string QUESTION_ID_PREFIX = "question-";
        private const int PUBLIC_ID_LENGHT = 6;
        private const string ANSWER_ID_PREFIX = "answer-";

        [RequestHandler("/api/surveys/survey/{publicId}/add-participation", AccessPolicy.PUBLIC_ARN)]
        public void AddParticipation(string publicId) {

            try {
                var sessionIdName = "session-id";
                var surveys = this.DB.SurveySurveys().IncludeQuestionsAndAnswers().AsQueryable();

                if (this.User?.IsAdminOrSuperUser == null || this.User?.IsAdminOrSuperUser == false) {
                    surveys = surveys.Published();
                }

                var survey = surveys.GetByPublicId(publicId);

                // Session
                string sessionId = this.GetOrCreateSessionId(sessionIdName);
                var userLastName = this.Params.String("user-lastname");
                var userFirsttName = this.Params.String("user-firstname");
                var userEmail = this.Params.String("user-email");
                var surveySession = this.DB.SurveySessions()
                    .Include(nameof(SurveySession.Surveys))
                    .GetByGuid(this, sessionId, false);
                if (surveySession == null) {
                    surveySession = new SurveySession();
                    surveySession.SessionId = sessionId;
                    this.DB.SurveySessions().Add(surveySession);
                }


                if (surveySession.Surveys.Contains(survey) == false) {
                    surveySession.Surveys.Add(survey);
                }

                // User infos 
                // We set new if same session overrides it
                surveySession.User = this.User;
                surveySession.Email = userEmail;
                surveySession.Name = $"{userFirsttName} {userLastName}".Trim();
                surveySession.Infos["FirstName"] = userFirsttName;
                surveySession.Infos["LastName"] = userLastName;


                // Questions
                var questions = this.Params.AllParamsAndValues().Where(k => k.Key.StartsWith(QUESTION_ID_PREFIX));
                var participations = new List<SurveyAnswerParticipation>();

                foreach (var questionAnswer in questions) {
                    var questionId = questionAnswer.Key.Substring(QUESTION_ID_PREFIX.Length, PUBLIC_ID_LENGHT);
                    var question = survey.Questions.AsQueryable().GetByPublicId(questionId);


                    // TODO REFACTOR TEXT PARSING QUESTION/ANSWERS
                    var answerIds = new List<string>();
                    if (question.Type != SurveyQuestion.QuestionTypes.TextInput) {
                        if (string.IsNullOrEmpty(questionAnswer.Value) == false) {
                            answerIds = questionAnswer.Value.Split(',').Select(v => v.ReplacePrefix(ANSWER_ID_PREFIX, "")).ToList();
                        }
                    } else {
                        answerIds.Add(questionAnswer.Value.ReplacePrefix(ANSWER_ID_PREFIX, ""));
                    }

                    foreach (var answerId in answerIds) {

                        var answerPublicId = answerId;
                        var textInput = false;

                        // Textinput
                        if (questionAnswer.Key.Length > QUESTION_ID_PREFIX.Length + PUBLIC_ID_LENGHT) {
                            try {
                                answerPublicId = questionAnswer.Key.Substring(questionAnswer.Key.Length - PUBLIC_ID_LENGHT);
                                textInput = true;
                                if (answerIds.Count() > 1) {
                                    throw new Exception("survey values key format incorrect: " + questionAnswer.Key);
                                }
                            } catch (Exception e) {
                                throw new Exception("survey values key format incorrect: " + questionAnswer.Key);
                            }
                        }



                        var surveyAnswer = question.Answers.AsQueryable().GetByPublicId(answerPublicId);


                        var participation = participations.FirstOrDefault(p => p.Answer == surveyAnswer);

                        // we already have one?
                        if (participation == null) {
                            participation = new SurveyAnswerParticipation();
                            participation.Session = surveySession;
                            participation.Question = surveyAnswer.Question;
                            participation.Survey = survey;
                            participation.Answer = surveyAnswer;
                            participations.Add(participation);
                            this.DB.SurveyQuestionParticipations().Add(participation);
                        } else {
                            //var asdf = "";
                        }

                        // Text input?
                        if (question.Type == SurveyQuestion.QuestionTypes.TextInput
                            || (question.Type == SurveyQuestion.QuestionTypes.MultipleChoice && surveyAnswer.AllowTextInput == true && textInput == true)
                            || (question.Type == SurveyQuestion.QuestionTypes.SingleChoice && surveyAnswer.AllowTextInput == true && textInput == true)
                            ) {
                            if (string.IsNullOrWhiteSpace(questionAnswer.Value) == true) {
                                continue; // dont save anything if we have no text
                            }
                            participation.Text = questionAnswer.Value;
                        }

                       
                    }


                }
                this.DB.SaveChanges();

                // Get thank you text
                var thankYouText = survey.ThankYouText?.GetForLanguage(this.Language);
                if (string.IsNullOrWhiteSpace(thankYouText)) thankYouText = Core.Localization.Text.GetTranslatedTextByIdForLanguage("survey.success.message", this.Language);

                // Send success response
                this.SendAPIMessage("success", new {
                    User = new { GUID = surveySession.SessionId },
                    ThankYouText = thankYouText
                });

            } catch (BackendException be) {
                throw be;
            } catch (Exception e) {
                Core.EmailLogger.SendMessageToAdminEmail("Error submitting a survey ", "Machinata.Module.Survey", this.Context, this, e);
                throw new LocalizedException(this, "survey-submit", e);
            }


        }
    }
}
