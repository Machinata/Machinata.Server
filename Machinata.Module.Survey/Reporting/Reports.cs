﻿using Machinata.Core.Util;
using Machinata.Module.Admin.Dashboard;
using Machinata.Module.Reporting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Survey.Reporting {
    public class Reports {

        public static byte[] Export(Core.Model.ModelContext dB, Survey.Model.SurveySurvey survey, string lang) {
            XLSX xlsx = new XLSX("Survey");
            var headerRow1 = new Dictionary<string, string>();
            headerRow1["Participants"] = ""; // to ensure first column
         
            // Header
            foreach (var question in survey.Questions.Published().OrderBy(q => q.Sort)) {
                var headerRowId = GetQuestionId(question);
                headerRow1[headerRowId] = question.Text?.GetForLanguage(lang);
                foreach (var answer in question.Answers.Published().OrderBy(q => q.Sort)) {
                    var headerAnswerRowId = GetAnswerId(answer);
                    var answerText = answer.Text?.GetForLanguage(lang);
                    if (answer.NumericalValue != null) {
                        answerText += " (" + answer.NumericalValue.Value.ToString("0.##") + ")";
                    }
                    headerRow1[headerAnswerRowId] = answerText;
                }
            }
            xlsx.AddRow(headerRow1);

            // Participants
            var participationsBySession = survey.Participations.Where(p=> p.Session != null).GroupBy(p => p.Session);
            foreach (var participationsSession in participationsBySession) {
                var dataRow = new Dictionary<string, string>();
                dataRow["Participants"] = GetParticipentId(participationsSession.Key);
                foreach (var participation in participationsSession) {
                    string value = participation.Text;
                    if (string.IsNullOrWhiteSpace(value)) {
                        value = "X";
                    }
                    dataRow[GetAnswerId(participation.Answer)] = value;
                }
                xlsx.AddRow(dataRow);
            }

            xlsx._compile();

            // Remove the 1st row (used row to genereate correct headings)
            xlsx.RemoveRowFromWorkSheet(1);
           

            return xlsx.GetContent();
        }

        private static string GetParticipentId(Model.SurveySession session) {
            if (string.IsNullOrWhiteSpace(session.Email) == false) {
                return session.Email;
            }
            if (string.IsNullOrWhiteSpace(session.Name) == false) {
                return session.Name;
            }

            return session.SessionId;
        }

        private static string GetQuestionId(Model.SurveyQuestion question, string lang) {
            //return "question-" + question.Sort;
            return question.Text?.GetForLanguage(lang);
        }

        private static string GetAnswerId(Model.SurveyQuestion question, Model.SurveyAnswer answer, string lang) {
            //return "question-" + question.Sort + "-answer-" + answer.Sort;
            var id = answer.Text?.GetForLanguage(lang);
            if (answer.NumericalValue != null) {
                id += " - " + answer.NumericalValue;
            }
            return id + " - " + answer.PublicId;
        }

        private static string GetAnswerId(Model.SurveyAnswer answer) {
            return "answer-" + answer.PublicId;
        }

        private static string GetQuestionId(Model.SurveyQuestion quesiton) {
            return "question-" + quesiton.PublicId;
        }
    }
}
