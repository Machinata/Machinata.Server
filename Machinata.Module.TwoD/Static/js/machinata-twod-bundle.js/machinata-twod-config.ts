
/// <type>class</type>
class TwoDCanvasConfig {

    rotateHandleSize: number = 5.0;
    rotateHandleLineSize: number = 2.0;
    rotateHandleColor: string = "red";

    selectedBorderSize: number = 2.0;
    selectedBorderColor: string = "red";

    hoveredBorderSize: number = 1.0;
    hoveredBorderColor: string = "gray";

    gridLineSize: number = 1.0;
    gridLineColor: string = "rgba(0,0,0,0.2)"; 

    snapPointSize: number = 8.0;
    snapPointSizeSnapped: number = 12.0;
    snapPointValidColor: string = "gray";
    snapPointInvalidColor: string = "gray";
    snapPointSnappingDistance: number = 20;
    snapPointAlwaysShowWhenSnapped: boolean = true;

    exportFitToBoundsPadding: number = 100;
    snapshotFitToBoundsPadding: number = 200;
    exportDocumentTitle: string = null;
    exportDocumentAuthor: string = null;
    exportDocumentSubject: string = null;

    allowCopyPaste: boolean = false;
    allowDoubleClickForSelectAllInSnapGroup: boolean = false;

    units: string = "mm";
    scale: number = 1 / 100;
    zoom: number = 1.0;

    defaultPanAmount: number = 100.0;

    promptBeforeLeavingPageOnDirty: boolean = false;
}
