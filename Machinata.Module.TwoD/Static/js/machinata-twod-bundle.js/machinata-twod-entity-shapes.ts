

/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityArrow extends TwoDEntity {

    dashed: boolean = false;

    constructor() {
        super("TwoDEntityArrow");
        let self = this;
    }

    saveAsJSON(): any {
        var json = super.saveAsJSON();
        json.dashed = this.dashed;
        return json;
    }

    loadFromJSON(json: any) {
        super.loadFromJSON(json);
        this.dashed = json.dashed;
    }

    setDashed(dashed: boolean): this {
        this.dashed = dashed;
        return this;
    }

    drawEntity(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        if (this.dashed) renderer.setDashedBorderStyle();
        else renderer.setSolidBorderStyle();
        // Arrow body (line)
        ctx.beginPath();
        ctx.moveTo(-this._size.w / 2, 0);
        ctx.lineTo(+this._size.w / 2, 0);
        ctx.stroke();
        // Arrow head
        renderer.setSolidBorderStyle();
        ctx.beginPath();
        ctx.moveTo(+this._size.w / 2 - this._size.h / 2, -this._size.h / 2);
        ctx.lineTo(+this._size.w / 2, 0);
        ctx.lineTo(+this._size.w / 2 - this._size.h / 2, +this._size.h / 2);
        ctx.stroke();
    }

}
Machinata.TwoD.EntityArrow = TwoDEntityArrow;


/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityCircle extends TwoDEntity {


    fillColor: string = "black";

    constructor() {
        super("TwoDEntityCircle");
        let self = this;
    }

    saveAsJSON(): any {
        var json = super.saveAsJSON();
        json.fillColor = this.fillColor;
        return json;
    }

    loadFromJSON(json: any) {
        super.loadFromJSON(json);
        this.fillColor = json.fillColor;
    }

    drawEntity(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        ctx.fillStyle = this.fillColor;
        ctx.beginPath();
        ctx.ellipse(0, 0, this._size.w / 2, this._size.h / 2, 0, 0, Math.PI * 2);
        ctx.closePath();
        ctx.fill();
    }

}
Machinata.TwoD.EntityCircle = TwoDEntityCircle;






/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityTriangle extends TwoDEntity {


    fillColor: string = "black";

    constructor() {
        super("TwoDEntityTriangle");
        let self = this;
    }

    saveAsJSON(): any {
        var json = super.saveAsJSON();
        json.fillColor = this.fillColor;
        return json;
    }

    loadFromJSON(json: any) {
        super.loadFromJSON(json);
        this.fillColor = json.fillColor;
    }

    drawEntity(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        ctx.fillStyle = this.fillColor;
        ctx.beginPath();
        // Bottom left
        ctx.moveTo(-this._size.w / 2, this._size.h / 2);
        // Bottom right
        ctx.lineTo(+this._size.w / 2, this._size.h / 2);
        // Top center
        ctx.lineTo(0, -this._size.h / 2);
        ctx.closePath();
        ctx.fill();
    }

}
Machinata.TwoD.EntityTriangle = TwoDEntityTriangle;
