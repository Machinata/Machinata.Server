

/// <summary>
/// 
/// </summary>
/// <type>class</type>
abstract class TwoDEntity {

    typeName: string;

    static _rid: number = 0;
    rid: number;

    guid: string;

    snapGroupId: string;

    canvas: TwoDCanvas = null;
    parent: TwoDEntity = null;
    children: TwoDEntity[] = [];

    props: TwoDEntityProp[] = [];
    snapPoints: TwoDEntitySnapPoint[] = [];
    acceptableSnapPointPartnerTypes: string[] = [];

    data: any = {};

    _enabled: boolean = true;
    _canRotate: boolean = true;
    _canMove: boolean = true;
    _canClick: boolean = false;
    _canSelect: boolean = true;
    _deletesParent: boolean = false;

    _grabOffset: gpos;
    _grabPos: gpos;
    _grabRotation: number;
    _label: string;
    _rot: number;
    _pos: gpos;
    //_worldRot: number; //TODO: track via parent and update always on set of _rot or _pos
    //_worldPos: gpos; //TODO: track via parent and update always on set of _rot or _pos
    _size: gsize;
    //_dirty: boolean = true;
    _group: boolean = false;
    _debugColor: string = "green";

    // Events
    onPropChanged: Function = null; // onPropChanged(entity,prop,val,eventData)


    constructor(typeName) {
        // Set runtime id
        this.typeName = typeName;
        this.rid = TwoDEntity._rid++;
        this._size = new gsize(0, 0);
        this._pos = new gpos(0, 0);
        this._rot = 0;
        this.guid = Machinata.guid();
    }

    saveAsJSON(): any {
        var self = this;

        // Init
        var json: any = {};

        // General props
        json.typeName = self.typeName;
        json.guid = self.guid;
        json.snapGroupId = self.snapGroupId;
        json._rot = self._rot;
        json._pos = self._pos.saveAsJSON();
        json._size = self._size.saveAsJSON();
        json._enabled = self._enabled;
        json._canRotate = self._canRotate;
        json._canMove = self._canMove;
        json._canSelect = self._canSelect;
        json._deletesParent = self._deletesParent;
        json._label = self._label;
        json._group = self._group;
        json._debugColor = self._debugColor;

        // Data object
        json.data = self.data;

        // Snap point data
        for (var i = 0; i < self.snapPoints.length; i++) {
            if (json.snapPoints == null) json.snapPoints = [];
            json.snapPoints.push(self.snapPoints[i].saveAsJSON());
        }

        // Children
        json.children = [];
        for (var i = 0; i < self.children.length; i++) {
            json.children.push(self.children[i].saveAsJSON());
        }
        return json;
    }

    

    loadFromJSON(json: any) {
        var self = this;

        // General props
        //self.typeName = json.typeName;
        self.guid = json.guid;
        self.snapGroupId = json.snapGroupId;
        self._rot = json._rot;
        self._pos = gpos.loadFromJSON(json._pos);
        self._size = gsize.loadFromJSON(json._size);
        self._enabled = json._enabled;
        self._canRotate = json._canRotate;
        self._canMove = json._canMove;
        self._canSelect = json._canSelect;
        self._deletesParent = json._deletesParent;
        self._label = json._label;
        self._group = json._group;
        self._debugColor = json._debugColor;

        // Data object
        self.data = json.data;

        // Snap point data
        // Since we can't yet resolve the snap point partner to future entities which might not be loaded yet, we
        // use a TwoDEntityWeakReference, which then later gets resolved via _resolveEntityWeakReferences
        if (json.snapPoints != null) {
            for (var i = 0; i < json.snapPoints.length; i++) {
                var jsonSP = json.snapPoints[i];
                if (jsonSP.partner != null) {
                    var sp = self.getSnapPointById(jsonSP.id);
                    if (sp == null) {
                        console.warn("TwoDEntity.loadFromJSON(): warning: the snap point with id " + jsonSP.id+" does not exist in the model, however it was saved in the JSON. The snap point has not be setup properly!");
                    } else {
                        sp._partner = new TwoDEntitySnapPoint(jsonSP.partner.label, jsonSP.partner.id);
                        sp._partner.entity = new TwoDEntityWeakReference(jsonSP.partner.entity_guid);
                    }
                }
            }
        }

        // Children
        for (var i = 0; i < json.children.length; i++) {
            var childJSON = json.children[i];
            //console.log("loadFromJSON", childJSON.typeName);
            var newEntity: TwoDEntity = null;
            try {
                newEntity = new window[(childJSON.typeName as string)]() as TwoDEntity;
            } catch (e) {
                console.error(e);
                throw "Could not create entity for type " + childJSON.typeName + ": " + e;
            }
            newEntity.guid = childJSON.guid; // we need this before addChild
            self._addChild(newEntity); // _addChild has no redraw
            newEntity.loadFromJSON(childJSON);
        }

    }

    getSnapPointById(id: string): TwoDEntitySnapPoint {
        for (var i = 0; i < this.snapPoints.length; i++) {
            if (this.snapPoints[i].id == id) return this.snapPoints[i];
        }
        return null;
    }

    getAllSnappedPartnerEntities(): TwoDEntity[] {
        let self = this;
        var ret = [];
        //TODO: is there a faster way to get these? Chaining via snap points?
        this.canvas.visitAllEntities(function (entity: TwoDEntity) {
            if (entity.guid != self.guid && entity.snapGroupId == self.snapGroupId) {
                ret.push(entity);
            }
        });
        return ret;
    }

    _resolveEntityWeakReferences() {
        // Resolves TwoDEntityWeakReference 
        var self = this;
        // Snap points
        // Here we have weak references to partner entities that might not have been loaded at the time they were needed,
        // Thus the snap point partners are resolved here.
        for (var i = 0; i < self.snapPoints.length; i++) {
            var sp = self.snapPoints[i];
            if (sp._partner != null && sp._partner.entity != null && sp._partner.entity instanceof TwoDEntityWeakReference) {
                var partnerGUID = (sp._partner.entity as TwoDEntityWeakReference).futureGUID;
                //console.log("_resolveEntityWeakReferences: got a weak ref, will resolve...", partnerGUID);
                var realPartnerEntity = self.canvas.getEntityByGUID(partnerGUID);
                if (realPartnerEntity == null) {
                    sp.setPartner(null);
                    console.warn("_resolveEntityWeakReferences: could not get realPartnerEntity");
                    continue;
                }
                var realPartnerSP = realPartnerEntity.getSnapPointById(sp._partner.id);
                if (realPartnerSP == null) {
                    sp.setPartner(null);
                    console.warn("_resolveEntityWeakReferences: could not get realPartnerSP");
                    continue;
                }
                sp.setPartner(realPartnerSP);
            }
        }
        // Children
        for (var i = 0; i < self.children.length; i++) {
            self.children[i]._resolveEntityWeakReferences();
        }
        // Call event
        this.loadFromJSONWeakEntitiesResolved();
    }

    loadFromJSONWeakEntitiesResolved() {
        // Implemented by subclass, called when all the weak entities have been resolved
    }

    setDirty() {
        if (this.canvas != null) this.canvas.setDirty();
    }

    getPropById(id: string): TwoDEntityProp {
        if (this.props == null) return null;
        for (var i = 0; i < this.props.length; i++) {
            if (this.props[i]._id == id) return this.props[i];
        }
        return null;
    }

    getPropValueById(id: string): any {
        var prop = this.getPropById(id);
        if (prop == null) return null;
        return prop.getValue(this);
    }

    updateForNewPropValue(prop: TwoDEntityProp, val: any, eventData?: any) {
        // Can be implemented by subclass
    }

    updateForAllPropValues() {
        for (var i = 0; i < this.props.length; i++) {
            var prop = this.props[i];
            var val = prop.getValue(this);
            this.updateForNewPropValue(prop, val);
        }
        if (this.canvas != null) this.canvas.requestRedraw();
        this.setDirty();
    }

    updateForAllPropUIs() {
        for (var i = 0; i < this.props.length; i++) {
            var prop = this.props[i];
            prop.updateUI();
        }
    }

    copyPartnerAlteringPropertiesToPartner(partner: TwoDEntity): boolean {
        // Get all partner props
        var didUpdateAProp = false;
        for (var i = 0; i < this.props.length; i++) {
            let myProp = this.props[i];
            // Does this affect our partners?
            if (myProp._altersPartners == true) {
                // Do we have this prop?
                var partnerProp = partner.getPropById(myProp._id);
                if (partnerProp != null) {
                    var partnerValue = partnerProp.getValue(partner);
                    var myValue = myProp.getValue(this);
                    if (partnerValue != myValue) {
                        partnerProp.setValueWithoutTriggers(partner, myValue);
                        didUpdateAProp = true;
                    }
                }
            }
        }
        return didUpdateAProp;
    }

    isSelected(): boolean {
        if (this.canvas == null) return false;
        if (this.canvas.entitySelection == null) return false;
        return this.canvas.entitySelection.isSelected(this);
    }

    isHovered(): boolean {
        if (this.canvas == null) return false;
        return this.canvas.hoveredEntity == this;
    }

    triggerAction(action: string): this {
        if (this.canvas == null) return this;
        if (this.canvas.onEntityAction == null) return this;
        this.canvas.onEntityAction(this.canvas,this,action);
        return this;
    }

    removeAllSnapPoints(): TwoDEntity {
        //TODO: we need to properly unhook the snap points
        this.snapPoints = [];
        this.setDirty();
        return this;
    }

    addSnapPoint(sp: TwoDEntitySnapPoint): TwoDEntity {
        sp.entity = this;
        this.snapPoints.push(sp);
        this.setDirty();
        return this;
    }

    updateSnapPoints() {
        // Should be implemented by subclass, called when resized (setSize)
    }

    snappedToPartner(partner: TwoDEntity, spSelf: TwoDEntitySnapPoint, spPartner: TwoDEntitySnapPoint, isOriginalPlacedEntity: boolean) {
        // Should be implemented by subclass, called when resized (setSize)
    }

    acceptsSnapPointPartner(partner: TwoDEntity): boolean {
        // Can be overridden by subclass

        // Check if the partner matches any in the list of other partner types
        if (this.acceptableSnapPointPartnerTypes != null && this.acceptableSnapPointPartnerTypes.length > 0) {
            for (var i = 0; i < this.acceptableSnapPointPartnerTypes.length; i++) {
                var typeName = this.acceptableSnapPointPartnerTypes[i];
                if (partner.typeName == typeName || typeName == "*") return true;
            }
            return false;
        } else {
            return false;
        }
    }

    // When onlyUnsnapped is true, we only check our snappoints that are unsnapped currently.
    // This is usefull if we just want to check if any snappoints.
    // When onlyOverlapping is true, we only check snappoints that are directly overlapping.
    // This can be usefull if we just want to reconnect snappoints that happen to overlap after moving and entity.
    snapToClosebySnapPoints(moveEntity: boolean = true, onlyUnsnapped: boolean = false, onlyOverlapping: boolean = false) {
        //console.log("TwoDEntity.snapToClosebySnapPoints()", this.typeName);
        var maxDistanceBetweenSnapPoints = this.canvas.config.snapPointSnappingDistance;
        
        let self = this;
        let a = this;
        if (a.snapPoints == null || a.snapPoints.length == 0) return;
        var finalSnapPointGroupId = null;

        for (var spai = 0; spai < a.snapPoints.length; spai++) {
            var closestMatchDistance = 9999999999999.0;
            let matchA: TwoDEntitySnapPoint = null;
            let matchB: TwoDEntitySnapPoint = null;

            let spa = a.snapPoints[spai];
            if (onlyUnsnapped == true && spa.partner() != null) continue; // 
            let spaPos = spa.worldPos();
            
            // Loop all children
            // TODO: what about nested children !?
            for (var ebi = 0; ebi < self.canvas.rootEntity.children.length; ebi++) {
                // Get b, the other entity
                var b = self.canvas.rootEntity.children[ebi];
                // Skip self, of course
                if (b.rid == a.rid) continue;
                // Skip if no snap points
                if (b.snapPoints == null || b.snapPoints.length == 0) continue;
                // Do we event accept this partner?
                var acceptPartner = self.acceptsSnapPointPartner(b);
                if (!acceptPartner) continue;
                // Skip if bounds not close
                //TODO
                // Check distance between each other b snappoint position
                for (var spbi = 0; spbi < b.snapPoints.length; spbi++) {
                    // Get distance to other snap point
                    var spb = b.snapPoints[spbi];
                    var spbPos = spb.worldPos();
                    var dist = spaPos.dist(spbPos);
                    if (onlyOverlapping == true && dist > 0.00000001) continue;
                    // Is it less than a max threshhold and closest match so far?
                    if (dist < maxDistanceBetweenSnapPoints && dist < closestMatchDistance) {
                        closestMatchDistance = dist;
                        matchA = spa; // self
                        matchB = spb; // other
                        //TODO: break if very close (note: can't break due to reset of all snap points)
                    }
                }
            }

            if (matchB != null) {
                // We got a match - snap to this!
                self.snapToSnapPoint(matchA, matchB, moveEntity);
                // Store the snap group id - we need this later to reset since another might undo this
                finalSnapPointGroupId = self.snapGroupId;
            } else {
                // No match - unsnap!
                self.snapToSnapPoint(spa, null, moveEntity); // reset
            }
        }

        // Overwrite the snap group id, since the nomatch-unsnap might have set this to null
        //console.log("Commit-4b81a9b8-BUG F overwrite the snap group id with finalSnapPointGroupId = " + finalSnapPointGroupId);
        // This is actually no longer needed, because TwoDEntitySnapPoint.setPartner now only unsets the snappoint group id if nothing else is connected anymore...
        //self.snapGroupId = finalSnapPointGroupId; //BUG: this will actually set to null since some snappoints checked can get unsnapped

        

    }

    snapToSnapPoint(spSelf: TwoDEntitySnapPoint, spOther: TwoDEntitySnapPoint, moveEntity: boolean = true) {
        //console.log("TwoDEntity.snapToSnapPoint()");
        if (spOther == null) {
            // Unlink spSelf
            //console.log("Commit-4b81a9b8-BUG A: spSelf.setPartner(null); via if (spOther == null)" + " (entity " + this.rid + ", snapid = " + this.snapGroupId + ")");
            spSelf.setPartner(null);
            // Event
            this.snappedToPartner(null, null, null, false);
            this.setDirty();
        } else {
            //TODO: validate that spSelf is really own
            //var newRot = spOther.worldRot() + spSelf.rot;
            //console.log("snapToSnapPoint", spSelf.id, spOther.id);
            if (moveEntity == true) {
                var newRot = spOther.worldRot() + Machinata.Math.degToRad(180) - spSelf.rot;
                var newPos = spOther.worldPos();
                newPos.x += -spSelf.pos.rot(newRot).x;
                newPos.y += -spSelf.pos.rot(newRot).y;
                this.setWorldRot(newRot);
                this.setWorldPos(newPos);
                // Check if our move broke another snappoint of ours?
                // We do this by checking all our other snappoints, and if we are snapped
                // but no longer overlapping then we break that snappoint
                for (var spi = 0; spi < this.snapPoints.length; spi++) {
                    var spSelfSibling = this.snapPoints[spi];
                    if (spSelfSibling == spSelf) continue;
                    if (spSelfSibling.isSnapped() && spSelfSibling.isSnappedAndOverlapping() == false) {
                        //console.warn("SP is no longer connected!!!!");
                        // Unlink spSelfSibling
                        //console.log("Commit-4b81a9b8-BUG B: spSelfSibling.setPartner(null); via Sibling no longer connected" + " (entity " + this.rid + ")");
                        spSelfSibling.setPartner(null);
                    }
                }
            }
            spSelf.setPartner(spOther);
            // Event
            this.snappedToPartner(spOther.entity, spSelf, spOther, true);
            spOther.entity.snappedToPartner(this, spOther, spSelf, false);
            this.setDirty();
            // Snap any additional snap points that resulted in this?
            this.snapToClosebySnapPoints(false, true, true); // only unsnapped, only overlapping
        }
        
    }

    setLabel(label: string): this {
        this._label = label;
        this.setDirty();
        return this;
    }

    getLabel() {
        return this._label;
    }

    getFullLabel() {
        return this._label;
    }

    setCanSelect(canSelect: boolean): this {
        this._canSelect = canSelect;
        return this;
    }

    setCanMove(canMove: boolean): this {
        this._canMove = canMove;
        return this;
    }

    setCanRotate(canRotate: boolean): this {
        this._canRotate = canRotate;
        return this;
    }

    addChild(child: TwoDEntity) {
        this._addChild(child);
        if (this.canvas != null) {
            this.canvas.requestRedraw();
        }
        this.setDirty();
        return this;
    }

    _addChild(child: TwoDEntity) {
        child.parent = this;
        child._setCanvas(this.canvas);
        this.children.push(child);
        return this;
    }

    _setCanvas(canvas: TwoDCanvas) {

        this.canvas = canvas;
        if (canvas != null) {
            canvas._guidToEntityMap[this.guid] = this;
        }
        // Make sure all child children are hooked on the canvas
        for (var i = 0; i < this.children.length; i++) {
            this.children[i]._setCanvas(canvas);
        }
    }

    addTo(parent: TwoDEntity) {
        parent.addChild(this);
        this.setDirty();
        return this;
    }

    addToCanvas(canvas: TwoDCanvas) {
        canvas.addEntity(this);
        this.setDirty();
        return this;
    }

    delete() {
        if (this.parent == null) {
            throw "Cannot delete as this item is not attached to any parent"
        }
        this.parent.deleteChild(this);
        this.setDirty();
        return this;
    }

    deleteChild(child: TwoDEntity) {
        // MASTER DELETE METHOD - ALL DELETES SHOULD GO THROUGH HERE

        // Remove from child list
        var index = this.children.indexOf(child);
        if (index > -1) {
            this.children.splice(index, 1);
        } else {
            throw "Cannot delete child as it is not a child of this item"
        }

        // Unlink any snappoints
        for (var spi = 0; spi < child.snapPoints.length; spi++) {
            var sp = child.snapPoints[spi];
            var partner = sp.partner();
            sp.setPartner(null);
            if (partner != null && partner.entity != null) {
                partner.entity.snappedToPartner(null, null, null, false);
            }
        }

        // Unregister from maps
        if (this.canvas != null) {
            delete this.canvas._guidToEntityMap[child.guid];
        }

        // Unselect
        if (child.isSelected()) {
            this.canvas.entitySelection.deselect(child); // de-select
            if (this.canvas.entitySelection.entities.length == 0) this.canvas.entitySelection = null;
        }

        // Unlink from parent and canvas
        child.parent = null;
        child.canvas = null;

        // Call on delted event
        if (this.canvas.onEntityDeleted) this.canvas.onEntityDeleted(this, child);

        // Request redraw
        this.canvas.requestRedraw();
        this.setDirty();
        return this;
    }

    setRot(rot: number): TwoDEntity {
        this._rot = rot;

        this.setDirty();
        return this;
    }

    // If true, the entity won't be drawn or impose its own bounds
    setGroup(group: boolean): TwoDEntity {
        this._group = group;

        this.setDirty();
        return this;
    }

    setPos(pos: gpos): TwoDEntity {
        this._pos = new gpos(pos.x,pos.y);

        this.setDirty();
        return this;
    }

    setSize(size: gsize): TwoDEntity {
        this._size = size;
        if (this.snapPoints != null && this.snapPoints.length > 0) this.updateSnapPoints();

        this.setDirty();
        return this;
    }
    //setSize(w: number, h: number): TwoDEntity {
    //    return this.setSize(new vec2(w,h));
    //}

    worldPos(): gpos {
        if (this.parent == null) return this._pos;
        let parentPos = this.parent.worldPos();
        let parentRot = this.parent.worldRot();

        return parentPos
            .add(this._pos)
            .rotAroundPivot(parentRot, parentPos);

    }

    worldRot(): number {
        if (this.parent == null) return this._rot;
        let parentRot = this.parent.worldRot();
        return (
            this._rot + parentRot
        );
    }

    setWorldRot(newRot: number): TwoDEntity {
        let parentRot = this.parent.worldRot();
        this._rot = newRot - parentRot;
        this.setDirty();
        return this;
    }

    setWorldPos(newPos: gpos): TwoDEntity {
        let parentPos = this.parent.worldPos();
        let parentRot = this.parent.worldRot();
        newPos = newPos.rotAroundPivot(-parentRot, parentPos);
        newPos = new gpos(
            newPos.x - parentPos.x,
            newPos.y - parentPos.y
        );
        this._pos = newPos;
        return this;
    }

    bounds(): gbounds {

        // Get self
        // P1---P2
        // |     |
        // P4---P3
        let myBoundsP1: gpos = new gpos( // upper left
            - this._size.w / 2,
            - this._size.h / 2
        ).rot(this._rot);
        let myBoundsP2: gpos = new gpos( // upper right
            + this._size.w / 2,
            - this._size.h / 2
        ).rot(this._rot);
        let myBoundsP3: gpos = new gpos( // lower right
            + this._size.w / 2,
            + this._size.h / 2
        ).rot(this._rot);
        let myBoundsP4: gpos = new gpos( // lower left
            - this._size.w / 2,
            + this._size.h / 2
        ).rot(this._rot);
        let myBounds: gbounds = new gbounds(
            new gpos(
                this._pos.x + (myBoundsP1.x + myBoundsP2.x + myBoundsP3.x + myBoundsP4.x) / 4.0,
                this._pos.y + (myBoundsP1.y + myBoundsP2.y + myBoundsP3.y + myBoundsP4.y) / 4.0
            ),
            new gsize(
                Math.max(myBoundsP1.x, myBoundsP2.x, myBoundsP3.x, myBoundsP4.x) - Math.min(myBoundsP1.x, myBoundsP2.x, myBoundsP3.x, myBoundsP4.x),
                Math.max(myBoundsP1.y, myBoundsP2.y, myBoundsP3.y, myBoundsP4.y) - Math.min(myBoundsP1.y, myBoundsP2.y, myBoundsP3.y, myBoundsP4.y)
            )
        );

        for (let child of this.children) {
            // Get child bounds, in our coord space
            let childBounds = child.bounds();
            childBounds.center.x += this._pos.x;
            childBounds.center.y += this._pos.y;
            myBounds.extend(childBounds);
        }

        return myBounds;
    }

    _calculateWorldBounds(): gbounds {
        // Get self
        // P1---P2
        // |     |
        // P4---P3
        let worldPos = this.worldPos();
        let wrot = this.worldRot();
        let p1: gpos = new gpos( // upper left
            - this._size.w / 2,
            - this._size.h / 2
        ).rot(wrot).add(worldPos);
        let p2: gpos = new gpos( // upper right
            + this._size.w / 2,
            - this._size.h / 2
        ).rot(wrot).add(worldPos);
        let p3: gpos = new gpos( // lower right
            + this._size.w / 2,
            + this._size.h / 2
        ).rot(wrot).add(worldPos);
        let p4: gpos = new gpos( // lower left
            - this._size.w / 2,
            + this._size.h / 2
        ).rot(wrot).add(worldPos);

        return new gbounds(
            new gpos(
                (p1.x + p2.x + p3.x + p4.x) / 4.0,
                (p1.y + p2.y + p3.y + p4.y) / 4.0
            ),
            new gsize(
                Math.max(p1.x, p2.x, p3.x, p4.x) - Math.min(p1.x, p2.x, p3.x, p4.x),
                Math.max(p1.y, p2.y, p3.y, p4.y) - Math.min(p1.y, p2.y, p3.y, p4.y)
            )
        );
    }

    worldBounds(): gbounds {
        var bounds: gbounds;
        // If group, then return the first childs bounds, otherwise the calculated version
        if (this._group == true) {
            bounds = new gbounds(new gpos(0, 0), new gsize(0, 0));
            if (this.children.length > 1) bounds = this.children[1].worldBounds();
        } else {
            bounds = this._calculateWorldBounds();
        }
        // Extend by child bounds
        for (let child of this.children) {
            let childBounds = child.worldBounds();
            bounds.extend(childBounds);
        }
        return bounds;
    }

    visualBoundsPadding(): number {
        // Can be implemented by subclass...
        return 0;
    }

    visualWorldBounds(): gbounds {
        // Same as worldBounds, but just with some added padding
        // This allows entities to have a logical size (for selecting, grabbing, etc), but
        // slight overhang draws...
        var bounds: gbounds;
        // If group, then return the first childs bounds, otherwise the calculated version
        if (this._group == true) {
            bounds = new gbounds(new gpos(0, 0), new gsize(0, 0));
            if (this.children.length > 1) bounds = this.children[1].worldBounds();
        } else {
            bounds = this._calculateWorldBounds();
            bounds.size.w += this.visualBoundsPadding() * 2;
            bounds.size.h += this.visualBoundsPadding() * 2;
        }
        // Extend by child bounds
        for (let child of this.children) {
            let childBounds = child.visualWorldBounds();
            bounds.extend(childBounds);
        }
        return bounds;
    }

    rotateHandlePos(): gpos {
        let handleOffset = this._size.h / 2 + (15.0 / this.canvas._previewRenderer.viewport.drawScale());
        var worldPos = this.worldPos();
        var worldRot = this.worldRot();
        //var handlePos = worldPos.rot(worldRot).add(new gpos(0, -handleOffset));
        var handlePos = worldPos.add(new gpos(0, -handleOffset).rot(worldRot));
        return handlePos;
    }

    drawBG(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;
    }

    drawEntity(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;
    }

    drawFG(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;
    }

    drawLabel(renderer: TwoDCanvasRenderer, label: string = null) {
        if (label == null) label = this.getFullLabel();
        let ctx = renderer.ctx;
        var rot = this.worldRot();
        ctx.rotate(-rot);
        renderer.drawLabel(this, label, 0, 0, "center");
        ctx.rotate(+rot);
    }

    getChildAtPoint(pos: gpos, onlyInteractable: boolean = false): TwoDEntity {
        for (var i = this.children.length-1; i >= 0; i--) {
            var child = this.children[i];
            if (!child._enabled) continue;
            if (onlyInteractable == true && (child._canSelect == false && child._canClick == false)) continue;
            if (child.worldBounds().containsPoint(pos)) {
                var grandchild = child.getChildAtPoint(pos);
                if (grandchild != null) return grandchild;
                return child;
            } 
        }
        return null;
    }

    onClick(): void {

    }
    onSelected(): void {
        
    }
    onDeselected(): void {

    }
    didMove(): void {

    }

    moveTo(newWorldPos: gpos, snapToGrid: boolean) {
        if (snapToGrid) {
            var snapSteps = 10; // fallback
            if (this.canvas != null) snapSteps = this.canvas._previewRenderer.viewport.gridSize;
            newWorldPos.x = Math.round(newWorldPos.x / snapSteps) * snapSteps;
            newWorldPos.y = Math.round(newWorldPos.y / snapSteps) * snapSteps;
        }
        var didChange = false;
        var previewPos = this._pos.clone();
        this.setWorldPos(newWorldPos);
        if (this._pos.x != previewPos.x || this._pos.y != previewPos.y) didChange = true;
        if (didChange == true) {
            this.didMove();
            this.setDirty();
        }
    }

    pointTo(targetWorldPos: gpos, snapToGrid: boolean) {
        let worldPos = this.worldPos();
        let newRot = Math.atan2(targetWorldPos.y - worldPos.y, targetWorldPos.x - worldPos.x);
        newRot += Math.PI / 2;
        if (snapToGrid) {
            var snapSteps = (Math.PI * 2) / 8; // 45 deg
            newRot = Math.round(newRot / snapSteps) * snapSteps;
        }
        this.setWorldRot(newRot);
        this.setDirty();
    }

    _drawBG(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        // Skip if disabled
        if (!this._enabled) return;

        // Skip if not in viewport
        if (!renderer.viewport.worldBounds().intersects(this.visualWorldBounds())) return;

        ctx.save(); {

            // Statis
            renderer.statsTotalDraws++;

            // Do entity transformation
            ctx.translate(this._pos.x, this._pos.y);
            ctx.rotate(this._rot);

            // Debug bg
            if (this.canvas.debug == true) {
                ctx.save(); {
                    ctx.globalAlpha = 0.2;
                    ctx.fillStyle = this._debugColor;
                    ctx.strokeStyle = null;
                    ctx.fillRect(
                        -this._size.w / 2,
                        -this._size.h / 2,
                        this._size.w,
                        this._size.h
                    );
                    if (this.isSelected()) {
                        ctx.strokeStyle = "red";
                        ctx.lineWidth = 2;
                        ctx.strokeRect(
                            -this._size.w / 2,
                            -this._size.h / 2,
                            this._size.w,
                            this._size.h
                        );
                    }
                    if (this == this.canvas.hoveredEntity) {
                        ctx.strokeStyle = "blue";
                        ctx.lineWidth = 2;
                        ctx.strokeRect(
                            -this._size.w / 2,
                            -this._size.h / 2,
                            this._size.w,
                            this._size.h
                        );
                    }
                    ctx.globalAlpha = 1.0;
                } ctx.restore();
            }

            // Call implementing draw
            this.drawBG(renderer);

            // Draw all children
            for (let child of this.children) {
                child._drawBG(renderer);
            }


        } ctx.restore();
    }

    _drawEntity(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        // Skip if disabled
        if (!this._enabled) return;

        // Skip if not in viewport
        if (!renderer.viewport.worldBounds().intersects(this.visualWorldBounds())) return;

        ctx.save(); {

            // Statis
            renderer.statsTotalDraws++;

            // Do entity transformation
            ctx.translate(this._pos.x, this._pos.y);
            ctx.rotate(this._rot);

            // Call implementing draw
            this.drawEntity(renderer);

            // Draw all children
            for (let child of this.children) {
                child._drawEntity(renderer);
            }


        } ctx.restore();
    }

    _drawFG(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        // Skip if disabled
        if (!this._enabled) return;

        // Skip if not in viewport
        if (!renderer.viewport.worldBounds().intersects(this.visualWorldBounds())) return;

        ctx.save(); {

            // Statis
            renderer.statsTotalDraws++;

            // Do entity transformation
            ctx.translate(this._pos.x, this._pos.y);
            ctx.rotate(this._rot);

            // Call implementing draw
            this.drawFG(renderer);

            // Draw all children
            for (let child of this.children) {
                child._drawFG(renderer);
            }


        } ctx.restore();
    }

    _drawUI(renderer: TwoDCanvasRenderer): void {
        //TODO: optimisze wordPos/worldRot access via caching!

        let ctx = renderer.ctx;

        // Skip if disabled
        if (!this._enabled) return;

        // Skip if not in viewport
        if (!renderer.viewport.worldBounds().intersects(this.visualWorldBounds())) return;

        ctx.save(); {

            var drawSnapPoints = false;
            if (this.canvas != null && this.canvas._interactionMode == TwoDCanvasInteractionMode.DRAG) drawSnapPoints = true;

            // Selected bounds
            if (this.isSelected()) {
                drawSnapPoints = true;
                var worldPos = this.worldPos();
                var worldRot = this.worldRot();

                // Draw selection border
                ctx.save(); {
                    ctx.translate(worldPos.x, worldPos.y);
                    ctx.rotate(worldRot);
                    ctx.strokeStyle = this.canvas.config.selectedBorderColor;
                    ctx.lineWidth = this.canvas.config.selectedBorderSize / renderer.viewport.drawScale();
                    ctx.strokeRect(-this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
                } ctx.restore();

                // Rotate handle?
                // Deprecated: now handled by selection
                /*if (this.canvas.entitySelection.canRotate()) {

                    var handlePos = this.rotateHandlePos();
                    if (this.canvas._interactionMode == TwoDCanvasInteractionMode.ROTATE) {
                        handlePos = this.canvas.cursorPos;
                    }
                    let handleSize = this.canvas.config.rotateHandleSize / renderer.viewport.drawScale();
                    //if (handlePos.dist(this.canvas.cursorPos) < (this.canvas.config.rotateHandleSize * 1.5) / 2.0) handleSize = this.canvas.config.rotateHandleSize * 1.5 / renderer.viewport.zoom;
                    ctx.save(); {
                        // Line
                        ctx.beginPath();
                        ctx.moveTo(worldPos.x, worldPos.y);
                        ctx.lineTo(handlePos.x, handlePos.y);
                        ctx.lineWidth = this.canvas.config.rotateHandleLineSize / renderer.viewport.drawScale();
                        ctx.strokeStyle = this.canvas.config.rotateHandleColor;
                        ctx.closePath();
                        ctx.stroke();
                        // Dot
                        ctx.translate(handlePos.x, handlePos.y);
                        ctx.fillStyle = this.canvas.config.rotateHandleColor;
                        ctx.beginPath();
                        ctx.arc(0, 0, handleSize, 0, 2 * Math.PI);
                        ctx.closePath();
                        ctx.fill();
                    } ctx.restore();
                }*/

            } else if (this.canvas.hoveredEntity == this) {
                drawSnapPoints = true;
                var worldPos = this.worldPos();
                var worldRot = this.worldRot();

                // Draw selection border
                ctx.save(); {
                    ctx.translate(worldPos.x, worldPos.y);
                    ctx.rotate(worldRot);
                    ctx.strokeStyle = this.canvas.config.hoveredBorderColor;
                    ctx.lineWidth = this.canvas.config.hoveredBorderSize / renderer.viewport.drawScale();
                    ctx.strokeRect(-this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
                } ctx.restore();

            }

            // Snap points?
            if (this.snapPoints != null && this.snapPoints.length > 0) {
                var worldPos = this.worldPos();
                var worldRot = this.worldRot();
                for (var i = 0; i < this.snapPoints.length; i++) {
                    var sp = this.snapPoints[i];
                    if (drawSnapPoints == true || (this.canvas.config.snapPointAlwaysShowWhenSnapped &&  sp.partner() != null)) {
                        var spSize = this.canvas.config.snapPointSize;
                        if (sp.partner() != null) spSize = this.canvas.config.snapPointSizeSnapped;
                        ctx.save(); {
                            // Move/rotate to snappoint pos
                            ctx.translate(worldPos.x, worldPos.y);
                            ctx.rotate(worldRot);
                            ctx.translate(sp.pos.x, sp.pos.y);
                            ctx.rotate(sp.rot);
                            // Draw half circl
                            ctx.beginPath();
                            ctx.arc(
                                0, // x
                                0, // y
                                spSize / renderer.viewport.drawScale(), // radius
                                0, // start angle
                                Math.PI, // end angle
                                false
                            );
                            ctx.closePath();
                            ctx.strokeStyle = null;
                            ctx.fillStyle = sp.valid ? this.canvas.config.snapPointValidColor : this.canvas.config.snapPointInvalidColor;
                            ctx.fill();
                            // Draw label
                            if (sp.label != null && (this.canvas.hoveredEntity == this || this.isSelected())) {
                                // Undo rotation from above
                                ctx.translate(0, (spSize / renderer.viewport.drawScale())*0.4);
                                ctx.rotate(-sp.rot);
                                ctx.rotate(-worldRot);
                                ctx.font = ((this.canvas.config.snapPointSize*0.9) / renderer.viewport.drawScale()) + "px " + renderer.viewport.labelFontName;
                                ctx.fillStyle = "white";//TODO
                                ctx.textAlign = "center";
                                ctx.textBaseline = "middle";
                                ctx.fillText(sp.label[0],0,0);
                            }
                        } ctx.restore();
                    }
                }
            }

            // Debug bounds
            if (this.canvas.debug == true) {
                //var bounds = this.bounds();
                var worldPos = this.worldPos();
                var bounds = this.worldBounds();
                var visualBounds = this.visualWorldBounds();
                ctx.save(); {
                    let lineSize = 1.0 / renderer.viewport.drawScale();
                    let dotSize = 2.0 / renderer.viewport.drawScale();
                    ctx.globalAlpha = 0.5;
                    if (true) {
                        ctx.fillStyle = null;
                        ctx.strokeStyle = this._debugColor;
                        ctx.lineWidth = lineSize;
                        ctx.strokeRect(
                            bounds.center.x - bounds.size.w / 2,
                            bounds.center.y - bounds.size.h / 2,
                            bounds.size.w,
                            bounds.size.h
                        );
                    }
                    if (true) {
                        ctx.fillStyle = null;
                        ctx.strokeStyle = this._debugColor;
                        ctx.lineWidth = lineSize;
                        ctx.strokeRect(
                            visualBounds.center.x - visualBounds.size.w / 2,
                            visualBounds.center.y - visualBounds.size.h / 2,
                            visualBounds.size.w,
                            visualBounds.size.h
                        );
                    }
                    if (true) {
                        ctx.strokeStyle = null;
                        ctx.fillStyle = "black";
                        ctx.fillRect(
                            worldPos.x - dotSize / 2,
                            worldPos.y - dotSize / 2,
                            dotSize,
                            dotSize
                        );
                    }
                    if (false) {
                        ctx.fillStyle = this._debugColor;
                        ctx.fillRect(
                            bounds.center.x - dotSize / 2,
                            bounds.center.y - dotSize / 2,
                            dotSize,
                            dotSize
                        );
                    }
                } ctx.restore();
            }

            // Draw children UI
            for (let child of this.children) {
                child._drawUI(renderer);
            }


        } ctx.restore();
    }

    toString() {
        var spPartners = "";
        for (var i = 0; i < this.snapPoints.length; i++) {
            if (this.snapPoints[i]._partner != null) {
                spPartners += ", partner with " + this.snapPoints[i]._partner.entity.guid;
            }
        }
        var fullLabel = this.getFullLabel() || "";
        return Machinata.String.replaceAll(fullLabel, "\n", "/") + " (rid=" + this.rid + ", guid=" + this.guid + ", snapid=" + this.snapGroupId + spPartners + ")";
    }

}
Machinata.TwoD.Entity = TwoDEntity;



/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityRoot extends TwoDEntity {
    constructor() {
        super("TwoDEntityRoot");
    }
}


/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityWeakReference extends TwoDEntity {

    futureGUID: string = null;

    constructor(futureGUID: string) {
        super("TwoDEntityWeakReference");
        this.futureGUID = futureGUID;
    }
}

/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityPlain extends TwoDEntity {
    constructor() {
        super("TwoDEntityPlain");
    }
}