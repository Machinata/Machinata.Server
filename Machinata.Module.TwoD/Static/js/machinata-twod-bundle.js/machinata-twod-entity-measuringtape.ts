

/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityMeasuringTape extends TwoDEntity {

    a: TwoDEntityMeasuringTapeEndPoint;
    b: TwoDEntityMeasuringTapeEndPoint;
    _tapeHandleSize: number = 100;

    constructor() {
        super("TwoDEntityMeasuringTape");
        let self = this;

        self._group = true;
        self._canRotate = false;
        self._canMove = false;

        var a = new TwoDEntityMeasuringTapeEndPoint();
        a.setSize(new gsize(this._tapeHandleSize, this._tapeHandleSize));
        a._canRotate = false;
        a._canMove = true;
        a.setPos(new gpos(-500, 0));
        a._deletesParent = true;


        var b = new TwoDEntityMeasuringTapeEndPoint();
        b.setSize(new gsize(this._tapeHandleSize, this._tapeHandleSize));
        b._canRotate = false;
        b._canMove = true;
        b.setPos(new gpos(500, 0));
        b._deletesParent = true;

        // Property
        var prop = new TwoDEntityProp();
        prop.id("TapeLength");
        prop.title("{text.length}");
        prop.uiType("text");
        prop.readonly(true);
        prop.getter(function (entity: TwoDEntity) {
            var measureTape = entity as TwoDEntityMeasuringTape;
            var dist = measureTape.getMeasureLength();
            return TwoDCanvas.getMetricUnitLabel(dist, 2);
        });
        this.props.push(prop);

        // Add as children
        self.a = a;
        self.b = b;
        this.addChild(a);
        this.addChild(b);
        this.bindABHandles();
    }

    saveAsJSON(): any {
        var json = super.saveAsJSON();
        return json;
    }

    loadFromJSON(json: any) {
        // Remove children, as these where saved
        this.children = [];
        // Reload children
        super.loadFromJSON(json);
        // Assign
        this.a = this.children[0] as TwoDEntityMeasuringTapeEndPoint;
        this.b = this.children[1] as TwoDEntityMeasuringTapeEndPoint;
        this.bindABHandles();
    }

    bindABHandles() {
        this.a.pointsTo = this.b;
        this.b.pointsTo = this.a;

        
    }

    getMeasureLength(): number {
        var dist = this.a._pos.dist(this.b._pos);
        return dist;
    }

    drawEntity(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        if (this.a == null || this.b == null) return;

        ctx.strokeStyle = "black";
        ctx.lineWidth = 2.0 / renderer.viewport.drawScale();
        ctx.beginPath();
        ctx.moveTo(this.a._pos.x, this.a._pos.y);
        ctx.lineTo(this.b._pos.x, this.b._pos.y);
        ctx.closePath();
        ctx.stroke();

        var dist = this.getMeasureLength();
        var labelPos = this.b._pos.add(this.a._pos).mul(0.5);
        renderer.drawLabel(this, TwoDCanvas.getMetricUnitLabel(dist,2), labelPos.x, labelPos.y, "center", true);
    }

}
Machinata.TwoD.EntityMeasuringTape = TwoDEntityMeasuringTape;





/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityMeasuringTapeEndPoint extends TwoDEntity {

    pointsTo: TwoDEntity;

    fillColor: string = "black";

    constructor() {
        super("TwoDEntityMeasuringTapeEndPoint");
        let self = this;
    }

    drawEntity(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        ctx.strokeStyle = this.fillColor;
        ctx.lineWidth = 2.0 / renderer.viewport.drawScale();
        // Rotate to partner
        var angleToPointsTo = Math.atan2(this.pointsTo._pos.y - this._pos.y, this.pointsTo._pos.x - this._pos.x);
        ctx.rotate(angleToPointsTo + (Math.PI/2));
        ctx.beginPath();
        // Bottom left
        ctx.moveTo(-this._size.w / 2, this._size.h / 2);
        // Top center
        ctx.lineTo(0, 0);
        // Bottom right
        ctx.lineTo(+this._size.w / 2, this._size.h / 2);
        ctx.stroke();
    }

    didMove() {
        this.parent.getPropById("TapeLength").updateUI();
    }

}