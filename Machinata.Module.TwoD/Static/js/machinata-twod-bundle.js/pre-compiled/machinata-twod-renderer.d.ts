declare abstract class TwoDCanvasRenderer {
    ctx: CanvasRenderingContext2D;
    viewport: TwoDCanvasViewport;
    statsStartDrawTime: Date;
    statsEndDrawTime: Date;
    statsTotalDrawTimeMS: number;
    statsTotalDrawTimeFPS: number;
    statsTotalDrawTimeFPSMin: number;
    statsTotalDrawTimeFPSMax: number;
    statsTotalDrawTimeFPSAverage: number;
    statsTotalDraws: number;
    debug: boolean;
    isExport: boolean;
    supportsTransformSetAndReset: boolean;
    drawLabels: boolean;
    drawMeasurements: boolean;
    drawUI: boolean;
    drawingCallCache: {};
    abstract createDrawingContext(options: any): any;
    prepareForDrawing(): void;
    resetDrawingCallCache(): void;
    drawWithCachedDrawCall(cacheId: string, x: number, y: number, w: number, h: number, drawCall: Function): void;
    setShapeStyle(): void;
    setLineStyle(): void;
    setDashedBorderStyle(): void;
    setSolidBorderStyle(): void;
    translatePointOnUnscaledContext(p: gpos): gpos;
    drawArc(x: any, y: any, radius: any, start: any, end: any, direction: any, moveTo: any): void;
    drawPixelPerfectLine(p1: gpos, p2: gpos, lineSize: number): void;
    drawMeasurement(p1: gpos, p2: gpos, offset?: number, label?: string): void;
    drawLabel(entity: TwoDEntity, text: string, x: number, y: number, alignment: CanvasTextAlign, forceDraw?: boolean): void;
}
declare class HTMLCanvasTwoDCanvasRenderer extends TwoDCanvasRenderer {
    canvas: HTMLCanvasElement;
    createDrawingContext(options: any): any;
}
declare class PDFCanvasTwoDCanvasRenderer extends TwoDCanvasRenderer {
    canvas: HTMLCanvasElement;
    createDrawingContext(options: any): any;
}
