declare abstract class TwoDEntityFoliage extends TwoDEntity {
    _seed: number;
    constructor(typeName: string, radius: number);
    saveAsJSON(): any;
    loadFromJSON(json: any): void;
    setRadius(r: number): TwoDEntityFoliage;
    getRadius(): number;
    drawOrganicRing(renderer: TwoDCanvasRenderer, r: number, steps: number, seed2: number, rJitter: number, aJitter: number): void;
    drawEntity(renderer: TwoDCanvasRenderer): void;
}
declare class TwoDEntityTree extends TwoDEntityFoliage {
    constructor();
    drawEntity(renderer: TwoDCanvasRenderer): void;
}
declare class TwoDEntityBush extends TwoDEntityFoliage {
    constructor();
    drawEntity(renderer: TwoDCanvasRenderer): void;
}
declare class TwoDEntityStone extends TwoDEntityFoliage {
    constructor();
    drawEntity(renderer: TwoDCanvasRenderer): void;
}
