var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityUIButton = /** @class */ (function (_super) {
    __extends(TwoDEntityUIButton, _super);
    function TwoDEntityUIButton() {
        var _this = _super.call(this, "TwoDEntityUIButton") || this;
        _this.borderColorNormal = "black";
        _this.backgroundColorNormal = "lightblue";
        _this.textColorNormal = "white";
        _this.borderColorHover = "black";
        _this.backgroundColorHover = "black";
        _this.textColorHover = "white";
        _this.textSize = 100;
        _this.action = null;
        var self = _this;
        self._canClick = true;
        self._canMove = false;
        self._canRotate = false;
        self._canSelect = false;
        self.setStyleFromBundleTheme();
        return _this;
    }
    TwoDEntityUIButton.prototype.saveAsJSON = function () {
        var json = _super.prototype.saveAsJSON.call(this);
        json.borderColorNormal = this.borderColorNormal;
        json.backgroundColorNormal = this.backgroundColorNormal;
        json.textColorNormal = this.textColorNormal;
        json.borderColorHover = this.borderColorHover;
        json.backgroundColorHover = this.backgroundColorHover;
        json.textColorHover = this.textColorHover;
        json.textSize = this.textSize;
        json.action = this.action;
        return json;
    };
    TwoDEntityUIButton.prototype.loadFromJSON = function (json) {
        _super.prototype.loadFromJSON.call(this, json);
        this.borderColorNormal = json.borderColorNormal;
        this.backgroundColorNormal = json.backgroundColorNormal;
        this.textColorNormal = json.textColorNormal;
        this.borderColorHover = json.borderColorHover;
        this.backgroundColorHover = json.backgroundColorHover;
        this.textColorHover = json.textColorHover;
        this.textSize = json.textSize;
        this.action = json.action;
    };
    TwoDEntityUIButton.prototype.setAction = function (action) {
        this.action = action;
        return this;
    };
    TwoDEntityUIButton.prototype.setLabel = function (label) {
        _super.prototype.setLabel.call(this, label);
        // Auto size to label
        var widthFactor = 6;
        if (label != null && label.length > 12)
            widthFactor = label.length * 0.5;
        this._size.h = this.textSize * 1.4;
        this._size.w = this._size.h * widthFactor;
        return this;
    };
    TwoDEntityUIButton.prototype.setStyle = function (borderColorNormal, backgroundColorNormal, textColorNormal, borderColorHover, backgroundColorHover, textColorHover) {
        this.borderColorNormal = borderColorNormal;
        this.backgroundColorNormal = backgroundColorNormal;
        this.textColorNormal = textColorNormal;
        this.borderColorHover = borderColorHover;
        this.backgroundColorHover = backgroundColorHover;
        this.textColorHover = textColorHover;
        return this;
    };
    TwoDEntityUIButton.prototype.setStyleFromBundleTheme = function () {
        this.borderColorNormal = "{theme.highlight-color}";
        this.backgroundColorNormal = "{theme.solid-color}";
        this.textColorNormal = "{theme.text-color}";
        this.borderColorHover = "{theme.highlight-color}";
        this.backgroundColorHover = "{theme.highlight-color}";
        this.textColorHover = "{theme.highlight-text-color}";
        return this;
    };
    TwoDEntityUIButton.prototype.drawEntity = function (renderer) {
        var ctx = renderer.ctx;
        var borderColor = this.isHovered() ? this.borderColorHover : this.borderColorNormal;
        var backgroundColor = this.isHovered() ? this.backgroundColorHover : this.backgroundColorNormal;
        var textColor = this.isHovered() ? this.textColorHover : this.textColorNormal;
        // BG Rect
        ctx.fillStyle = backgroundColor;
        ctx.strokeStyle = borderColor;
        ctx.lineWidth = 2 / renderer.viewport.drawScale();
        ;
        ctx.beginPath();
        ctx.rect(-this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
        ctx.closePath();
        ctx.fillAndStroke();
        // Label
        ctx.fillStyle = textColor;
        ctx.font = this.textSize + "px " + renderer.viewport.labelFontName;
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.fillText(this._label, 0, this.textSize * 0.075, this._size.w);
    };
    TwoDEntityUIButton.prototype.onClick = function () {
        // Should be implemented by base class
    };
    return TwoDEntityUIButton;
}(TwoDEntity));
Machinata.TwoD.EntityUIButton = TwoDEntityUIButton;
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityUILabel = /** @class */ (function (_super) {
    __extends(TwoDEntityUILabel, _super);
    function TwoDEntityUILabel() {
        var _this = _super.call(this, "TwoDEntityUILabel") || this;
        _this.textColor = "white";
        _this.backgroundColor = "white";
        _this.borderColor = "black";
        _this.textSize = 100;
        _this.lineHeight = 1.2;
        _this.padding = 40;
        _this.borderDashed = false;
        _this.textAlign = "center";
        _this._lines = null;
        var self = _this;
        self._canMove = false;
        self._canRotate = false;
        self._canSelect = false;
        self.setStyleFromBundleTheme();
        return _this;
    }
    TwoDEntityUILabel.prototype.saveAsJSON = function () {
        var json = _super.prototype.saveAsJSON.call(this);
        json.textColor = this.textColor;
        json.backgroundColor = this.backgroundColor;
        json.borderColor = this.borderColor;
        json.textSize = this.textSize;
        json.lineHeight = this.lineHeight;
        json.padding = this.padding;
        json.borderDashed = this.borderDashed;
        json.textAlign = this.textAlign;
        json._lines = this._lines;
        return json;
    };
    TwoDEntityUILabel.prototype.loadFromJSON = function (json) {
        _super.prototype.loadFromJSON.call(this, json);
        this.textColor = json.textColor;
        this.backgroundColor = json.backgroundColor;
        this.borderColor = json.borderColor;
        this.textSize = json.textSize;
        this.lineHeight = json.lineHeight;
        this.padding = json.padding;
        this.borderDashed = json.borderDashed;
        this.textAlign = json.textAlign;
        this._lines = json._lines;
    };
    TwoDEntityUILabel.prototype.setBorderDashed = function (borderDashed) {
        this.borderDashed = borderDashed;
        return this;
    };
    TwoDEntityUILabel.prototype.setTextColor = function (textColor) {
        this.textColor = textColor;
        return this;
    };
    TwoDEntityUILabel.prototype.setBackgroundColor = function (backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    };
    TwoDEntityUILabel.prototype.setBorderColor = function (borderColor) {
        this.borderColor = borderColor;
        return this;
    };
    TwoDEntityUILabel.prototype.setLabel = function (label) {
        _super.prototype.setLabel.call(this, label);
        // Cache lines
        this._lines = label.split("\n");
        // Update size
        var maxLineWidth = 0;
        for (var i = 0; i < this._lines.length; i++) {
            var metrics = TwoDCanvas.measureText(this._lines[i], this.textSize + "px " + TwoDCanvas.DEFAULT_LABEL_FONT);
            maxLineWidth = Math.max(metrics.width, maxLineWidth);
        }
        this._size.w = maxLineWidth + this.padding * 2;
        this._size.h = (this.textSize * this.lineHeight) * this._lines.length + this.padding * 2;
        return this;
    };
    TwoDEntityUILabel.prototype.setTextAlign = function (textAlign) {
        this.textAlign = textAlign;
        return this;
    };
    TwoDEntityUILabel.prototype.setStyleFromBundleTheme = function () {
        this.textColor = "{theme.solid-text-color}";
        this.backgroundColor = "{theme.solid-color}";
        this.borderColor = "{theme.solid-text-color}";
        return this;
    };
    TwoDEntityUILabel.prototype.drawEntity = function (renderer) {
        var ctx = renderer.ctx;
        // Setup ctx
        ctx.font = this.textSize + "px " + renderer.viewport.labelFontName;
        // Draw BG
        if (this.borderDashed)
            renderer.setDashedBorderStyle();
        ctx.fillStyle = this.backgroundColor;
        ctx.strokeStyle = this.borderColor;
        ctx.lineWidth = 1 / renderer.viewport.drawScale();
        ;
        ctx.beginPath();
        ctx.rect(-this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
        ctx.closePath();
        ctx.fillAndStroke();
        // Multiline label
        ctx.fillStyle = this.textColor;
        if (this.textAlign == "center") {
            ctx.textAlign = "center";
        }
        else if (this.textAlign == "left") {
            ctx.textAlign = "left";
            ctx.translate(-this._size.w / 2 + this.padding, 0);
        }
        else if (this.textAlign == "right") {
            ctx.textAlign = "right";
        }
        ctx.textBaseline = "top";
        for (var i = 0; i < this._lines.length; i++) {
            ctx.fillText(this._lines[i], 0, this.padding + (-this._size.h / 2) + (i * this.textSize * this.lineHeight) + this.textSize * 0.075);
        }
    };
    return TwoDEntityUILabel;
}(TwoDEntity));
Machinata.TwoD.EntityUILabel = TwoDEntityUILabel;
