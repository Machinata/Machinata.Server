var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityMeasuringTape = /** @class */ (function (_super) {
    __extends(TwoDEntityMeasuringTape, _super);
    function TwoDEntityMeasuringTape() {
        var _this = _super.call(this, "TwoDEntityMeasuringTape") || this;
        _this._tapeHandleSize = 100;
        var self = _this;
        self._group = true;
        self._canRotate = false;
        self._canMove = false;
        var a = new TwoDEntityMeasuringTapeEndPoint();
        a.setSize(new gsize(_this._tapeHandleSize, _this._tapeHandleSize));
        a._canRotate = false;
        a._canMove = true;
        a.setPos(new gpos(-500, 0));
        a._deletesParent = true;
        var b = new TwoDEntityMeasuringTapeEndPoint();
        b.setSize(new gsize(_this._tapeHandleSize, _this._tapeHandleSize));
        b._canRotate = false;
        b._canMove = true;
        b.setPos(new gpos(500, 0));
        b._deletesParent = true;
        // Property
        var prop = new TwoDEntityProp();
        prop.id("TapeLength");
        prop.title("{text.length}");
        prop.uiType("text");
        prop.readonly(true);
        prop.getter(function (entity) {
            var measureTape = entity;
            var dist = measureTape.getMeasureLength();
            return TwoDCanvas.getMetricUnitLabel(dist, 2);
        });
        _this.props.push(prop);
        // Add as children
        self.a = a;
        self.b = b;
        _this.addChild(a);
        _this.addChild(b);
        _this.bindABHandles();
        return _this;
    }
    TwoDEntityMeasuringTape.prototype.saveAsJSON = function () {
        var json = _super.prototype.saveAsJSON.call(this);
        return json;
    };
    TwoDEntityMeasuringTape.prototype.loadFromJSON = function (json) {
        // Remove children, as these where saved
        this.children = [];
        // Reload children
        _super.prototype.loadFromJSON.call(this, json);
        // Assign
        this.a = this.children[0];
        this.b = this.children[1];
        this.bindABHandles();
    };
    TwoDEntityMeasuringTape.prototype.bindABHandles = function () {
        this.a.pointsTo = this.b;
        this.b.pointsTo = this.a;
    };
    TwoDEntityMeasuringTape.prototype.getMeasureLength = function () {
        var dist = this.a._pos.dist(this.b._pos);
        return dist;
    };
    TwoDEntityMeasuringTape.prototype.drawEntity = function (renderer) {
        var ctx = renderer.ctx;
        if (this.a == null || this.b == null)
            return;
        ctx.strokeStyle = "black";
        ctx.lineWidth = 2.0 / renderer.viewport.drawScale();
        ctx.beginPath();
        ctx.moveTo(this.a._pos.x, this.a._pos.y);
        ctx.lineTo(this.b._pos.x, this.b._pos.y);
        ctx.closePath();
        ctx.stroke();
        var dist = this.getMeasureLength();
        var labelPos = this.b._pos.add(this.a._pos).mul(0.5);
        renderer.drawLabel(this, TwoDCanvas.getMetricUnitLabel(dist, 2), labelPos.x, labelPos.y, "center", true);
    };
    return TwoDEntityMeasuringTape;
}(TwoDEntity));
Machinata.TwoD.EntityMeasuringTape = TwoDEntityMeasuringTape;
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityMeasuringTapeEndPoint = /** @class */ (function (_super) {
    __extends(TwoDEntityMeasuringTapeEndPoint, _super);
    function TwoDEntityMeasuringTapeEndPoint() {
        var _this = _super.call(this, "TwoDEntityMeasuringTapeEndPoint") || this;
        _this.fillColor = "black";
        var self = _this;
        return _this;
    }
    TwoDEntityMeasuringTapeEndPoint.prototype.drawEntity = function (renderer) {
        var ctx = renderer.ctx;
        ctx.strokeStyle = this.fillColor;
        ctx.lineWidth = 2.0 / renderer.viewport.drawScale();
        // Rotate to partner
        var angleToPointsTo = Math.atan2(this.pointsTo._pos.y - this._pos.y, this.pointsTo._pos.x - this._pos.x);
        ctx.rotate(angleToPointsTo + (Math.PI / 2));
        ctx.beginPath();
        // Bottom left
        ctx.moveTo(-this._size.w / 2, this._size.h / 2);
        // Top center
        ctx.lineTo(0, 0);
        // Bottom right
        ctx.lineTo(+this._size.w / 2, this._size.h / 2);
        ctx.stroke();
    };
    TwoDEntityMeasuringTapeEndPoint.prototype.didMove = function () {
        this.parent.getPropById("TapeLength").updateUI();
    };
    return TwoDEntityMeasuringTapeEndPoint;
}(TwoDEntity));
