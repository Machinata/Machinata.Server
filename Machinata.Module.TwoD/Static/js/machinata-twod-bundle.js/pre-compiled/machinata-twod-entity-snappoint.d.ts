declare class TwoDEntitySnapPoint {
    id: string;
    pos: gpos;
    rot: number;
    label: string;
    valid: boolean;
    entity: TwoDEntity;
    _partner: TwoDEntitySnapPoint;
    constructor(label: string, id: string);
    saveAsJSON(): any;
    loadFromJSON(json: any): void;
    worldPos(): gpos;
    resnap(): void;
    unsnap(): void;
    setPartner(other: TwoDEntitySnapPoint): void;
    partner(): TwoDEntitySnapPoint;
    worldRot(): number;
    isSnapped(): boolean;
    isSnappedAndOverlapping(): boolean;
}
