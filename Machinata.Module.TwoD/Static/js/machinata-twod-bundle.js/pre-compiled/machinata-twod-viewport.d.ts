declare class TwoDCanvasViewport {
    pos: gpos;
    zoom: number;
    dpi: number;
    scale: number;
    units: string;
    viewSize: gsize;
    deviceSize: gsize;
    gridEnabled: boolean;
    gridSize: number;
    scaleEnabled: boolean;
    labelFontSize: number;
    labelFontName: string;
    setZoom(newZoom: number): void;
    fitBounds(bounds: gbounds): void;
    drawScale(): number;
    drawScaleForZoom(zoom: number): number;
    worldBounds(): gbounds;
    inToPx(inches: number): number;
    pxToIn(px: number): number;
    inToMM(inches: number): number;
    mmToIn(mm: number): number;
    mmToPDFPoints(mm: number): number;
    mmToPx(mm: number): number;
    mmToPxUnscaled(mm: number): number;
    pxToMM(px: number): number;
}
