/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntitySelection = /** @class */ (function () {
    function TwoDEntitySelection() {
        this.entities = [];
        this._grabWorldPos = null;
        this._grabRotateHandlePos = null;
        this._grabRotateHandleAngle = null;
    }
    TwoDEntitySelection.prototype.clone = function () {
        var clone = new TwoDEntitySelection();
        for (var i = 0; i < this.entities.length; i++) {
            clone.entities.push(this.entities[i]);
            clone._grabWorldPos = this._grabWorldPos; //TODO should be a clone
            clone._grabRotateHandleAngle = this._grabRotateHandleAngle; //TODO should be a clone
            clone._grabRotateHandleAngle = this._grabRotateHandleAngle;
        }
        return clone;
    };
    TwoDEntitySelection.prototype.setGrabBeginInformation = function (cursor) {
        // Sets the information needed to perform a grab move or rotation for a group of entities...
        this._grabWorldPos = this.worldPos();
        this._grabRotateHandlePos = this.rotateHandlePos();
        this._grabRotateHandleAngle = Math.atan2(this._grabRotateHandlePos.y - this._grabWorldPos.y, this._grabRotateHandlePos.x - this._grabWorldPos.x);
        for (var i = 0; i < this.entities.length; i++) {
            this.entities[i]._grabPos = this.entities[i].worldPos();
            this.entities[i]._grabOffset = this.entities[i]._grabPos.subtract(cursor);
            this.entities[i]._grabRotation = this.entities[i].worldRot();
        }
    };
    TwoDEntitySelection.prototype.moveTo = function (cursor, snapToGrid) {
        for (var i = 0; i < this.entities.length; i++) {
            //this.entities[i].setWorldPos(cursor.add(this.entities[i]._grabOffset));
            this.entities[i].moveTo(cursor.add(this.entities[i]._grabOffset), snapToGrid && this.entities.length == 1);
        }
    };
    TwoDEntitySelection.prototype.pointTo = function (cursor, snapToGrid) {
        if (this.entities.length == 1) {
            // Single entity
            this.entities[0].pointTo(cursor, snapToGrid);
        }
        else {
            // Group rotation
            // We get the relative angle offset since the user started the grab for rotate...
            var rotateHandleAngle = Math.atan2(cursor.y - this._grabWorldPos.y, cursor.x - this._grabWorldPos.x);
            if (snapToGrid) {
                var snapSteps = (Math.PI * 2) / 8; // 45 deg
                rotateHandleAngle = Math.round(rotateHandleAngle / snapSteps) * snapSteps;
            }
            var rotateHandleAngleDelta = rotateHandleAngle - this._grabRotateHandleAngle;
            // Now we rotate each element around that original group privot point
            // and add the relative rotation angle to each object thereafter
            for (var i = 0; i < this.entities.length; i++) {
                var newPos = this.entities[i]._grabPos.rotAroundPivot(rotateHandleAngleDelta, this._grabWorldPos);
                this.entities[i].setWorldPos(newPos);
                this.entities[i].setWorldRot(this.entities[i]._grabRotation + rotateHandleAngleDelta);
            }
        }
    };
    TwoDEntitySelection.prototype.snapToClosebySnapPoints = function () {
        //TODO: allow snapping of all?
        if (this.entities.length == 1) {
            this.entities[0].snapToClosebySnapPoints();
        }
    };
    TwoDEntitySelection.prototype.canMove = function () {
        return true; //TODO
    };
    TwoDEntitySelection.prototype.canRotate = function () {
        //TODO: support multiple rotation
        //if (this.entities.length != 1) return false; // currently only a single is supported
        for (var i = 0; i < this.entities.length; i++) {
            if (this.entities[i]._canRotate == false)
                return false;
        }
        return true;
    };
    TwoDEntitySelection.prototype.worldPos = function () {
        // Get average of all
        var average = new gpos(0, 0);
        for (var i = 0; i < this.entities.length; i++) {
            average = average.add(this.entities[i].worldPos());
        }
        return average.div(this.entities.length);
    };
    TwoDEntitySelection.prototype.rotateHandlePos = function () {
        // Get average of all
        var average = new gpos(0, 0);
        for (var i = 0; i < this.entities.length; i++) {
            average = average.add(this.entities[i].rotateHandlePos());
        }
        return average.div(this.entities.length);
    };
    TwoDEntitySelection.prototype.delete = function () {
        for (var i = 0; i < this.entities.length; i++) {
            if (this.entities[i]._deletesParent == true && this.entities[i].parent != null) {
                this.entities[i].parent.delete();
            }
            else {
                this.entities[i].delete();
            }
        }
    };
    TwoDEntitySelection.prototype.select = function (entity, cursor) {
        if (this.isSelected(entity))
            return;
        this.entities.push(entity);
        this.setGrabBeginInformation(cursor);
        entity.onSelected();
    };
    TwoDEntitySelection.prototype.deselect = function (entity) {
        var index = this.entities.indexOf(entity);
        if (index > -1) {
            this.entities.splice(index, 1);
        }
        entity.onDeselected();
    };
    TwoDEntitySelection.prototype.isSelected = function (entity) {
        for (var i = 0; i < this.entities.length; i++) {
            if (this.entities[i] == entity)
                return true;
        }
        return false;
    };
    TwoDEntitySelection.prototype.toString = function () {
        var ret = "";
        for (var i = 0; i < this.entities.length; i++) {
            if (ret != "")
                ret += ", ";
            ret += this.entities[i].toString();
        }
        return ret;
    };
    TwoDEntitySelection.prototype.saveAsJSON = function () {
        var ret = [];
        for (var i = 0; i < this.entities.length; i++) {
            ret.push(this.entities[i].saveAsJSON());
        }
        return ret;
    };
    return TwoDEntitySelection;
}());
