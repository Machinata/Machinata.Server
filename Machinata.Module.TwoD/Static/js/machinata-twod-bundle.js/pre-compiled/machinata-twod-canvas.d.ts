declare enum TwoDCanvasInteractionMode {
    NONE = "NONE",
    DRAG = "DRAG",
    PAN = "PAN",
    ROTATE = "ROTATE",
    SELECT = "SELECT",
    CLICK = "CLICK"
}
declare class TwoDCanvas {
    _previewCanvasElem: any;
    _previewCanvas: HTMLCanvasElement;
    _previewRenderer: TwoDCanvasRenderer;
    _isDrawing: boolean;
    _isDirty: boolean;
    _redrawRequested: boolean;
    debug: boolean;
    backgroundEntity: TwoDEntityRoot;
    rootEntity: TwoDEntityRoot;
    _guidToEntityMap: {
        [id: string]: TwoDEntity;
    };
    entitySelection: TwoDEntitySelection;
    config: TwoDCanvasConfig;
    data: any;
    hoveredEntity: TwoDEntity;
    onEntitySelected: Function;
    onEntityDeleted: Function;
    onEntityMoved: Function;
    onEntityRotated: Function;
    onEntityDropped: Function;
    onEntityAction: Function;
    onRequestSave: Function;
    onKeyPress: Function;
    maxZoom: number;
    minZoom: number;
    cursorPos: gpos;
    _mousePos: gpos;
    _lastMousePos: gpos;
    _lastCursorPos: gpos;
    _interactionMode: TwoDCanvasInteractionMode;
    _interactionPossibility: TwoDCanvasInteractionMode;
    _lockedInteractionMode: TwoDCanvasInteractionMode;
    _pasteBuffer: any[];
    _interactionStartCursorPos: gpos;
    _interactionEndCursorPos: gpos;
    _ctrlKeyDown: boolean;
    static DEFAULT_LABEL_SIZE: number;
    static DEFAULT_LABEL_FONT: string;
    constructor(canvasElem: any, config: TwoDCanvasConfig);
    getEntityByGUID(guid: string): TwoDEntity;
    addEntity(entity: TwoDEntity): TwoDCanvas;
    setDirty(): TwoDCanvas;
    setClean(): TwoDCanvas;
    mmToPx(mm: number): number;
    _debugLoadTestScene(): void;
    _debugDrawPoint(pos: gpos): void;
    _updateInteractionState(event: any): void;
    isScreencasting(): boolean;
    _updatePointer(): void;
    _updateCursor(event: MouseEvent): void;
    createEntityForType(typeName: string): TwoDEntity;
    bindUIEvents(): void;
    selectEntity(entity: TwoDEntity, addToCurrentSelection: boolean): void;
    selectAll(): void;
    selectAllInSnapGroup(snapGroupId: string): void;
    copySelection(): void;
    pasteSelection(): void;
    _rebuildNewGUIDsForJSON(json: any[], guidDictionary?: any): void;
    _rebuildNewGUIDsForJSONSnapPoints(json: any[], guidDictionary?: any): void;
    loadEntitiesFromJSON(json: any[], rebuildNewGUIDs: boolean, placeInCenterOfViewport: boolean, selectNewEntities: boolean): void;
    loadEntityFromJSON(json: any): TwoDEntity;
    lockInteractionMode(mode: TwoDCanvasInteractionMode): void;
    unlockInteractionMode(): void;
    setInteractionMode(mode: TwoDCanvasInteractionMode): void;
    setInteractionPossibility(mode: TwoDCanvasInteractionMode): void;
    pan(x: number, y: number, factor: number): void;
    panLeft(factor: number): void;
    panRight(factor: number): void;
    panUp(factor: number): void;
    panDown(factor: number): void;
    zoomIn(factor: number, fromCursor: boolean): void;
    zoomOut(factor: number, fromCursor: boolean): void;
    setPosition(newPosition: gpos): void;
    setZoom(newZoom: any, fromCursor: boolean): void;
    zoomFit(factor: number): void;
    resize(): void;
    getEntityAtPosition(pos: gpos, onlyInteractable?: boolean): TwoDEntity;
    redraw(): void;
    requestRedraw(): void;
    _renderWithRenderer(renderer: TwoDCanvasRenderer): void;
    drawUI(renderer: TwoDCanvasRenderer): void;
    drawMeasurements(renderer: TwoDCanvasRenderer): void;
    getFullSceneBounds(): gbounds;
    getExportBounds(): gbounds;
    exportPDF(opts: any): void;
    exportPNG(opts: any): void;
    getSnapshotForBoundsAsBase64(bounds: gbounds, width: number, height: number, drawUI: boolean, drawLabels: boolean, drawMeasurements: boolean): string;
    static _measureTextContext: CanvasRenderingContext2D;
    static measureText(text: string, font: string): TextMetrics;
    static getMetricUnitLabel(mm: number, precision?: number): string;
    clearChildren(): void;
    visitAllEntities(visit: Function): void;
    saveAsJSON(): any;
    loadFromJSON(json: any): void;
    getPreviewThumbnailAsImageData(): ImageData;
    getPreviewThumbnailAsBase64(): string;
    getAllSnapGroups(includedNonGrouped?: boolean): any;
    getAllEntitiesForSnapGroup(snapGroupId: string): TwoDEntity[];
}
