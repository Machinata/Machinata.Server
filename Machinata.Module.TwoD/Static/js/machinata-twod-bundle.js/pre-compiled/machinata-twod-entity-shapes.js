var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityArrow = /** @class */ (function (_super) {
    __extends(TwoDEntityArrow, _super);
    function TwoDEntityArrow() {
        var _this = _super.call(this, "TwoDEntityArrow") || this;
        _this.dashed = false;
        var self = _this;
        return _this;
    }
    TwoDEntityArrow.prototype.saveAsJSON = function () {
        var json = _super.prototype.saveAsJSON.call(this);
        json.dashed = this.dashed;
        return json;
    };
    TwoDEntityArrow.prototype.loadFromJSON = function (json) {
        _super.prototype.loadFromJSON.call(this, json);
        this.dashed = json.dashed;
    };
    TwoDEntityArrow.prototype.setDashed = function (dashed) {
        this.dashed = dashed;
        return this;
    };
    TwoDEntityArrow.prototype.drawEntity = function (renderer) {
        var ctx = renderer.ctx;
        if (this.dashed)
            renderer.setDashedBorderStyle();
        else
            renderer.setSolidBorderStyle();
        // Arrow body (line)
        ctx.beginPath();
        ctx.moveTo(-this._size.w / 2, 0);
        ctx.lineTo(+this._size.w / 2, 0);
        ctx.stroke();
        // Arrow head
        renderer.setSolidBorderStyle();
        ctx.beginPath();
        ctx.moveTo(+this._size.w / 2 - this._size.h / 2, -this._size.h / 2);
        ctx.lineTo(+this._size.w / 2, 0);
        ctx.lineTo(+this._size.w / 2 - this._size.h / 2, +this._size.h / 2);
        ctx.stroke();
    };
    return TwoDEntityArrow;
}(TwoDEntity));
Machinata.TwoD.EntityArrow = TwoDEntityArrow;
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityCircle = /** @class */ (function (_super) {
    __extends(TwoDEntityCircle, _super);
    function TwoDEntityCircle() {
        var _this = _super.call(this, "TwoDEntityCircle") || this;
        _this.fillColor = "black";
        var self = _this;
        return _this;
    }
    TwoDEntityCircle.prototype.saveAsJSON = function () {
        var json = _super.prototype.saveAsJSON.call(this);
        json.fillColor = this.fillColor;
        return json;
    };
    TwoDEntityCircle.prototype.loadFromJSON = function (json) {
        _super.prototype.loadFromJSON.call(this, json);
        this.fillColor = json.fillColor;
    };
    TwoDEntityCircle.prototype.drawEntity = function (renderer) {
        var ctx = renderer.ctx;
        ctx.fillStyle = this.fillColor;
        ctx.beginPath();
        ctx.ellipse(0, 0, this._size.w / 2, this._size.h / 2, 0, 0, Math.PI * 2);
        ctx.closePath();
        ctx.fill();
    };
    return TwoDEntityCircle;
}(TwoDEntity));
Machinata.TwoD.EntityCircle = TwoDEntityCircle;
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityTriangle = /** @class */ (function (_super) {
    __extends(TwoDEntityTriangle, _super);
    function TwoDEntityTriangle() {
        var _this = _super.call(this, "TwoDEntityTriangle") || this;
        _this.fillColor = "black";
        var self = _this;
        return _this;
    }
    TwoDEntityTriangle.prototype.saveAsJSON = function () {
        var json = _super.prototype.saveAsJSON.call(this);
        json.fillColor = this.fillColor;
        return json;
    };
    TwoDEntityTriangle.prototype.loadFromJSON = function (json) {
        _super.prototype.loadFromJSON.call(this, json);
        this.fillColor = json.fillColor;
    };
    TwoDEntityTriangle.prototype.drawEntity = function (renderer) {
        var ctx = renderer.ctx;
        ctx.fillStyle = this.fillColor;
        ctx.beginPath();
        // Bottom left
        ctx.moveTo(-this._size.w / 2, this._size.h / 2);
        // Bottom right
        ctx.lineTo(+this._size.w / 2, this._size.h / 2);
        // Top center
        ctx.lineTo(0, -this._size.h / 2);
        ctx.closePath();
        ctx.fill();
    };
    return TwoDEntityTriangle;
}(TwoDEntity));
Machinata.TwoD.EntityTriangle = TwoDEntityTriangle;
