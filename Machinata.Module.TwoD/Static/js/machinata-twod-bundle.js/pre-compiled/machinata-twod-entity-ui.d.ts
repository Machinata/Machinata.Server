declare class TwoDEntityUIButton extends TwoDEntity {
    borderColorNormal: string;
    backgroundColorNormal: string;
    textColorNormal: string;
    borderColorHover: string;
    backgroundColorHover: string;
    textColorHover: string;
    textSize: number;
    action: string;
    constructor();
    saveAsJSON(): any;
    loadFromJSON(json: any): void;
    setAction(action: string): this;
    setLabel(label: string): this;
    setStyle(borderColorNormal: string, backgroundColorNormal: string, textColorNormal: string, borderColorHover: string, backgroundColorHover: string, textColorHover: string): TwoDEntityUIButton;
    setStyleFromBundleTheme(): TwoDEntityUIButton;
    drawEntity(renderer: TwoDCanvasRenderer): void;
    onClick(): void;
}
declare class TwoDEntityUILabel extends TwoDEntity {
    textColor: string;
    backgroundColor: string;
    borderColor: string;
    textSize: number;
    lineHeight: number;
    padding: number;
    borderDashed: boolean;
    textAlign: string;
    _lines: string[];
    constructor();
    saveAsJSON(): any;
    loadFromJSON(json: any): void;
    setBorderDashed(borderDashed: boolean): this;
    setTextColor(textColor: string): this;
    setBackgroundColor(backgroundColor: string): this;
    setBorderColor(borderColor: string): this;
    setLabel(label: string): this;
    setTextAlign(textAlign: string): this;
    setStyleFromBundleTheme(): TwoDEntityUILabel;
    drawEntity(renderer: TwoDCanvasRenderer): void;
}
