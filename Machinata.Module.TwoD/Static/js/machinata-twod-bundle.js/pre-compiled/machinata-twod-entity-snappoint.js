/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntitySnapPoint = /** @class */ (function () {
    function TwoDEntitySnapPoint(label, id) {
        this.id = id;
        this.label = label;
        this.pos = new gpos(0, 0);
        this.rot = 0;
        this.valid = true;
    }
    TwoDEntitySnapPoint.prototype.saveAsJSON = function () {
        // {
        //     id: "a",
        //     entity_guid: "xyz-abc-012",
        //     partner: {
        //         id: "b",
        //         entity_guid: "xyz-abc-012"
        //     }
        // }
        var json = {
            id: this.id,
            label: this.label
        };
        if (this.entity != null)
            json.entity_guid = this.entity.guid;
        if (this._partner != null) {
            json.partner = {
                id: this._partner.id
            };
            if (this._partner.entity != null) {
                json.partner.entity_guid = this._partner.entity.guid;
            }
        }
        return json;
    };
    TwoDEntitySnapPoint.prototype.loadFromJSON = function (json) {
    };
    TwoDEntitySnapPoint.prototype.worldPos = function () {
        var parentPos = this.entity.worldPos();
        var parentRot = this.entity.worldRot();
        return parentPos
            .add(this.pos)
            .rotAroundPivot(parentRot, parentPos);
    };
    TwoDEntitySnapPoint.prototype.resnap = function () {
        this.entity.snapToSnapPoint(this, this.partner());
    };
    TwoDEntitySnapPoint.prototype.unsnap = function () {
        this.entity.snapToSnapPoint(this, null);
    };
    TwoDEntitySnapPoint.prototype.setPartner = function (other) {
        //if (other != null) console.log("Commit-4b81a9b8-BUG D: setPartner on " + this.label + " to " + other.entity.rid + " (entity " + this.entity.rid + ")");
        //else console.log("Commit-4b81a9b8-BUG D: setPartner on " + this.label + " to null (entity " + this.entity.rid + ")");
        // Link/unlink with partner snap point
        if (other == null) {
            // Unlink
            if (this._partner != null)
                this._partner._partner = null;
            this._partner = null;
        }
        else {
            // Link
            this._partner = other;
            other._partner = this;
        }
        // Update the entity snap group id
        if (other != null) {
            // Set snap group id
            // Create a new on other if not already...
            if (other.entity.snapGroupId == null)
                other.entity.snapGroupId = Machinata.guid();
            //console.log("Commit-4b81a9b8-BUG E: setting this.entity.snapGroupId to " + this.entity.snapGroupId)
            this.entity.snapGroupId = other.entity.snapGroupId;
        }
        else {
            // Check if none of our snappoints are snapped (ie everything is disconnected)
            var allSnappointsUnsnapped = true;
            for (var spi = 0; spi < this.entity.snapPoints.length; spi++) {
                if (this.entity.snapPoints[spi].isSnapped() == true) {
                    allSnappointsUnsnapped = false;
                    break; // if any is snapped, then allSnappointsUnsnapped is false and we no longer need to check
                }
            }
            if (allSnappointsUnsnapped == true) {
                //console.log("Commit-4b81a9b8-BUG C1: allSnappointsUnsnapped is true, setting snapGroupId to null" + " (entity " + this.entity.rid + ")");
                this.entity.snapGroupId = null; // set our snapgroup to null
            }
            else {
                //console.log("Commit-4b81a9b8-BUG C2: allSnappointsUnsnapped is false" + " (entity " + this.entity.rid + ")");
            }
        }
    };
    TwoDEntitySnapPoint.prototype.partner = function () {
        return this._partner;
    };
    TwoDEntitySnapPoint.prototype.worldRot = function () {
        return this.entity.worldRot() + this.rot;
    };
    TwoDEntitySnapPoint.prototype.isSnapped = function () {
        if (this._partner == null)
            return false;
        else
            return true;
    };
    TwoDEntitySnapPoint.prototype.isSnappedAndOverlapping = function () {
        if (this.isSnapped() == false)
            return false;
        if (this.worldPos().dist(this.partner().worldPos()) < 0.000001)
            return true;
        else
            return false;
    };
    return TwoDEntitySnapPoint;
}());
