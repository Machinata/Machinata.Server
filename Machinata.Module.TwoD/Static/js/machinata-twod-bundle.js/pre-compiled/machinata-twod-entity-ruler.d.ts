declare class TwoDEntityRuler extends TwoDEntity {
    constructor();
    setLength(length: number): void;
    drawEntity(renderer: TwoDCanvasRenderer): void;
}
