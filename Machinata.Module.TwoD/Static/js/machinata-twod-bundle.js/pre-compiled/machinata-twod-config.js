/// <type>class</type>
var TwoDCanvasConfig = /** @class */ (function () {
    function TwoDCanvasConfig() {
        this.rotateHandleSize = 5.0;
        this.rotateHandleLineSize = 2.0;
        this.rotateHandleColor = "red";
        this.selectedBorderSize = 2.0;
        this.selectedBorderColor = "red";
        this.hoveredBorderSize = 1.0;
        this.hoveredBorderColor = "gray";
        this.gridLineSize = 1.0;
        this.gridLineColor = "rgba(0,0,0,0.2)";
        this.snapPointSize = 8.0;
        this.snapPointSizeSnapped = 12.0;
        this.snapPointValidColor = "gray";
        this.snapPointInvalidColor = "gray";
        this.snapPointSnappingDistance = 20;
        this.snapPointAlwaysShowWhenSnapped = true;
        this.exportFitToBoundsPadding = 100;
        this.snapshotFitToBoundsPadding = 200;
        this.exportDocumentTitle = null;
        this.exportDocumentAuthor = null;
        this.exportDocumentSubject = null;
        this.allowCopyPaste = false;
        this.allowDoubleClickForSelectAllInSnapGroup = false;
        this.units = "mm";
        this.scale = 1 / 100;
        this.zoom = 1.0;
        this.defaultPanAmount = 100.0;
        this.promptBeforeLeavingPageOnDirty = false;
    }
    return TwoDCanvasConfig;
}());
