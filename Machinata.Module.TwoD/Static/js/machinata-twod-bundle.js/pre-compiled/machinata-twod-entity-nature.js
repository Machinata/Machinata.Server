var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityFoliage = /** @class */ (function (_super) {
    __extends(TwoDEntityFoliage, _super);
    function TwoDEntityFoliage(typeName, radius) {
        var _this = _super.call(this, typeName) || this;
        _this._seed = 0;
        var self = _this;
        self.setRadius(radius);
        self._seed = Math.round(Math.random() * 1000);
        // Properties
        _this.props.push(new TwoDEntityProp()
            .title("{text.twod.circumference}")
            .units("{canvas-units}")
            .uiType("number")
            .minValue(600)
            .maxValue(1000 * 6)
            .defaultValue(1000)
            .step(200)
            .getter(function (entity) { return entity._size.w; })
            .setter(function (entity, val) { entity.setRadius(val / 2); }));
        _this.props.push(new TwoDEntityProp()
            .title("{text.twod.shape}")
            .uiType("range")
            .minValue(0)
            .maxValue(1000)
            .defaultValue(Math.round(Math.random() * 1000))
            .step(1)
            .getter(function (entity) { return self._seed; })
            .setter(function (entity, val) { self._seed = val; }));
        return _this;
    }
    TwoDEntityFoliage.prototype.saveAsJSON = function () {
        var json = _super.prototype.saveAsJSON.call(this);
        json._seed = this._seed;
        return json;
    };
    TwoDEntityFoliage.prototype.loadFromJSON = function (json) {
        _super.prototype.loadFromJSON.call(this, json);
        this._seed = json._seed;
    };
    TwoDEntityFoliage.prototype.setRadius = function (r) {
        this.setSize(new gsize(r * 2, r * 2));
        return this;
    };
    TwoDEntityFoliage.prototype.getRadius = function () {
        return this._size.w / 2;
    };
    TwoDEntityFoliage.prototype.drawOrganicRing = function (renderer, r, steps, seed2, rJitter, aJitter) {
        var ctx = renderer.ctx;
        ctx.beginPath();
        var seedScaled = this._seed * 1000;
        var step = 360 / steps;
        var firstX;
        var firstY;
        for (var i = 0; i < 360; i += step) {
            var angleOffset = Machinata.Math.rndWithSeed(seedScaled + seed2 + steps + i, -step * aJitter, +step * aJitter);
            var rOffset = Machinata.Math.rndWithSeed(seedScaled + seed2 + steps + i * 2, -(r * rJitter), 0);
            var angle = i + angleOffset;
            var px = Math.sin(Machinata.Math.degToRad(angle)) * (r + rOffset);
            var py = Math.cos(Machinata.Math.degToRad(angle)) * (r + rOffset);
            if (i == 0) {
                ctx.moveTo(px, py);
                firstX = px;
                firstY = py;
            }
            else
                ctx.lineTo(px, py);
        }
        ctx.lineTo(firstX, firstY); // close
        ctx.closePath();
    };
    TwoDEntityFoliage.prototype.drawEntity = function (renderer) {
        var ctx = renderer.ctx;
        // Label
        if (true) {
            this.drawLabel(renderer);
        }
    };
    return TwoDEntityFoliage;
}(TwoDEntity));
Machinata.TwoD.EntityFoliage = TwoDEntityFoliage;
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityTree = /** @class */ (function (_super) {
    __extends(TwoDEntityTree, _super);
    function TwoDEntityTree() {
        var _this = _super.call(this, "TwoDEntityTree", 1500) || this;
        _this.setLabel("{text.twod.tree}");
        return _this;
    }
    TwoDEntityTree.prototype.drawEntity = function (renderer) {
        var ctx = renderer.ctx;
        // Init
        var r = this.getRadius();
        renderer.setShapeStyle();
        // Draw leaves border
        {
            this.drawOrganicRing(renderer, r, 20, 2323, 0.2, 0.3);
            ctx.fill();
            ctx.stroke();
        }
        // Draw trunk
        if (true) {
            this.drawOrganicRing(renderer, r * 0.1, 10, 654, 0.1, 0.1);
            ctx.stroke();
        }
        // Foliage
        _super.prototype.drawEntity.call(this, renderer);
    };
    return TwoDEntityTree;
}(TwoDEntityFoliage));
Machinata.TwoD.EntityTree = TwoDEntityTree;
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityBush = /** @class */ (function (_super) {
    __extends(TwoDEntityBush, _super);
    function TwoDEntityBush() {
        var _this = _super.call(this, "TwoDEntityBush", 600) || this;
        _this.setLabel("{text.twod.bush}");
        return _this;
    }
    TwoDEntityBush.prototype.drawEntity = function (renderer) {
        var ctx = renderer.ctx;
        // Init
        var r = this.getRadius();
        ctx.fillStyle = "white";
        ctx.strokeStyle = "black";
        ctx.lineWidth = 1 / renderer.viewport.drawScale();
        // Draw leaves border
        {
            this.drawOrganicRing(renderer, r, 40, 2323, 0.3, 0.4);
            ctx.fill();
            ctx.stroke();
        }
        // Foliage
        _super.prototype.drawEntity.call(this, renderer);
    };
    return TwoDEntityBush;
}(TwoDEntityFoliage));
Machinata.TwoD.EntityBush = TwoDEntityBush;
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityStone = /** @class */ (function (_super) {
    __extends(TwoDEntityStone, _super);
    function TwoDEntityStone() {
        var _this = _super.call(this, "TwoDEntityStone", 500) || this;
        _this.setLabel("{text.twod.stone}");
        return _this;
    }
    TwoDEntityStone.prototype.drawEntity = function (renderer) {
        var ctx = renderer.ctx;
        // Init
        var r = this.getRadius();
        ctx.fillStyle = "white";
        ctx.strokeStyle = "black";
        ctx.lineWidth = 1 / renderer.viewport.drawScale();
        // Draw leaves border
        {
            this.drawOrganicRing(renderer, r, 8, 2323, 0.3, 0.4);
            ctx.fill();
            ctx.stroke();
        }
        // Foliage
        _super.prototype.drawEntity.call(this, renderer);
    };
    return TwoDEntityStone;
}(TwoDEntityFoliage));
Machinata.TwoD.EntityStone = TwoDEntityStone;
