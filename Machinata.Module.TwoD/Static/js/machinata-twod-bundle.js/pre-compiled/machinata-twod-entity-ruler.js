var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityRuler = /** @class */ (function (_super) {
    __extends(TwoDEntityRuler, _super);
    function TwoDEntityRuler() {
        var _this = _super.call(this, "TwoDEntityRuler") || this;
        var self = _this;
        _this.setLength(1000);
        // Properties
        _this.props.push(new TwoDEntityProp()
            .title("{text.length}")
            .units("{canvas-units}")
            .uiType("buttons")
            .acceptedValuesAndTitles([1000, 2000, 5000, 10000, 20000], ["1m", "2m", "5m", "10m", "20m"])
            .defaultValue(1000)
            .getter(function (entity) { return entity._size.w; })
            .setter(function (entity, val) { entity.setLength(val); }));
        return _this;
    }
    TwoDEntityRuler.prototype.setLength = function (length) {
        this.setSize(new gsize(length, 1000 / 8));
    };
    TwoDEntityRuler.prototype.drawEntity = function (renderer) {
        var ctx = renderer.ctx;
        ctx.translate(-this._size.w / 2, -this._size.h / 2);
        var mmPerBlock = 1000; // 1 meter
        var widthInMM = this._size.w; //TODO: what if other units?
        var scaleBlocksToShow = Math.floor(widthInMM / mmPerBlock); // meters
        while (scaleBlocksToShow < 5) {
            mmPerBlock = mmPerBlock / 10;
            scaleBlocksToShow = Math.floor(widthInMM / mmPerBlock); // meters
        }
        var blockW = mmPerBlock;
        var blockH = this._size.h;
        for (var i = 0; i < scaleBlocksToShow; i++) {
            var blockX = (i * blockW);
            var blockY = 0;
            ctx.fillStyle = i % 2 == 0 ? "black" : "white";
            ctx.fillRect(blockX, blockY, blockW, blockH);
        }
        // Block label
        var fontSize = (renderer.viewport.labelFontSize / renderer.viewport.drawScale());
        ctx.font = fontSize + "px " + renderer.viewport.labelFontName;
        ctx.fillStyle = "black";
        ctx.fillText(TwoDCanvas.getMetricUnitLabel(mmPerBlock), 0, -fontSize / 2);
        ctx.textAlign = "right";
        ctx.fillText(TwoDCanvas.getMetricUnitLabel((mmPerBlock * scaleBlocksToShow)), blockW * scaleBlocksToShow, -fontSize / 2);
        // Border
        ctx.lineWidth = 1.0 / renderer.viewport.drawScale();
        ctx.strokeStyle = "black";
        ctx.strokeRect(0, 0, this._size.w, this._size.h);
    };
    return TwoDEntityRuler;
}(TwoDEntity));
Machinata.TwoD.EntityRuler = TwoDEntityRuler;
