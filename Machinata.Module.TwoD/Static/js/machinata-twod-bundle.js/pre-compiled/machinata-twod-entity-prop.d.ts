declare class TwoDEntityPropSetEvent {
    triggerEntityUpdateForNewPropValue: boolean;
    triggerEntityOnPropChanged: boolean;
    isUserTriggered: boolean;
    userData: any;
}
declare class TwoDEntityProp {
    static _rid: number;
    rid: number;
    _id: string;
    _title: string;
    _units: string;
    _tooltip: string;
    _uiType: string;
    _minValue: any;
    _maxValue: any;
    _defaultValue: any;
    _prevAcceptedValuesArray: any[];
    _acceptedValuesArray: any[];
    _acceptedTitlesArray: any[];
    _acceptedValuesAndTitlesCallback: Function;
    _propUIUpdatedCallback: Function;
    _step: any;
    _readonly: boolean;
    _visible: boolean;
    _altersPartners: boolean;
    _valueTips: {
        [name: string]: string;
    };
    _getter: Function;
    _setter: Function;
    data: any;
    constructor();
    id(id: string): TwoDEntityProp;
    title(title: string): TwoDEntityProp;
    units(units: string): TwoDEntityProp;
    tooltip(tooltip: string): TwoDEntityProp;
    uiType(uiType: string): TwoDEntityProp;
    step(step: any): TwoDEntityProp;
    readonly(readonly: boolean): TwoDEntityProp;
    visible(visible: boolean): TwoDEntityProp;
    altersPartners(altersPartners: boolean): TwoDEntityProp;
    minValue(minValue: any): TwoDEntityProp;
    maxValue(maxValue: any): TwoDEntityProp;
    defaultValue(defaultValue: any): TwoDEntityProp;
    acceptedValuesAndTitles(acceptedValues: any[], acceptedTitles: any[]): TwoDEntityProp;
    acceptedValuesAndTitlesViaCallback(callback: Function): TwoDEntityProp;
    valueTips(tips: {
        [name: string]: string;
    }): TwoDEntityProp;
    propUIUpdatedCallback(callback: Function): TwoDEntityProp;
    getter(getter: Function): TwoDEntityProp;
    setter(setter: Function): TwoDEntityProp;
    setVisible(visible: boolean): TwoDEntityProp;
    _setValue(entity: TwoDEntity, val: any): void;
    setValueWithoutTriggers(entity: TwoDEntity, val: any, eventData?: TwoDEntityPropSetEvent): TwoDEntityProp;
    setValueWithUserTrigger(entity: TwoDEntity, val: any, eventData?: TwoDEntityPropSetEvent): TwoDEntityProp;
    setValue(entity: TwoDEntity, val: any, eventData?: TwoDEntityPropSetEvent): TwoDEntityProp;
    updateUI(): void;
    getValue(entity: TwoDEntity): any;
    dataGetterSetter(key: string): void;
    _updateTip(entity: TwoDEntity, propElem: any): void;
    buildStandardEditUI(entity: TwoDEntity): any;
    getPropElemInDOM(entity: TwoDEntity): any;
    getAcceptedValuesAndTitles(entity: TwoDEntity): any[];
    updateAcceptedValues(entity: TwoDEntity): void;
    updateAcceptedValuesEditUI(entity: TwoDEntity, propElem: any): void;
}
