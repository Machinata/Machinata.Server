var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntity = /** @class */ (function () {
    function TwoDEntity(typeName) {
        this.canvas = null;
        this.parent = null;
        this.children = [];
        this.props = [];
        this.snapPoints = [];
        this.acceptableSnapPointPartnerTypes = [];
        this.data = {};
        this._enabled = true;
        this._canRotate = true;
        this._canMove = true;
        this._canClick = false;
        this._canSelect = true;
        this._deletesParent = false;
        //_dirty: boolean = true;
        this._group = false;
        this._debugColor = "green";
        // Events
        this.onPropChanged = null; // onPropChanged(entity,prop,val,eventData)
        // Set runtime id
        this.typeName = typeName;
        this.rid = TwoDEntity._rid++;
        this._size = new gsize(0, 0);
        this._pos = new gpos(0, 0);
        this._rot = 0;
        this.guid = Machinata.guid();
    }
    TwoDEntity.prototype.saveAsJSON = function () {
        var self = this;
        // Init
        var json = {};
        // General props
        json.typeName = self.typeName;
        json.guid = self.guid;
        json.snapGroupId = self.snapGroupId;
        json._rot = self._rot;
        json._pos = self._pos.saveAsJSON();
        json._size = self._size.saveAsJSON();
        json._enabled = self._enabled;
        json._canRotate = self._canRotate;
        json._canMove = self._canMove;
        json._canSelect = self._canSelect;
        json._deletesParent = self._deletesParent;
        json._label = self._label;
        json._group = self._group;
        json._debugColor = self._debugColor;
        // Data object
        json.data = self.data;
        // Snap point data
        for (var i = 0; i < self.snapPoints.length; i++) {
            if (json.snapPoints == null)
                json.snapPoints = [];
            json.snapPoints.push(self.snapPoints[i].saveAsJSON());
        }
        // Children
        json.children = [];
        for (var i = 0; i < self.children.length; i++) {
            json.children.push(self.children[i].saveAsJSON());
        }
        return json;
    };
    TwoDEntity.prototype.loadFromJSON = function (json) {
        var self = this;
        // General props
        //self.typeName = json.typeName;
        self.guid = json.guid;
        self.snapGroupId = json.snapGroupId;
        self._rot = json._rot;
        self._pos = gpos.loadFromJSON(json._pos);
        self._size = gsize.loadFromJSON(json._size);
        self._enabled = json._enabled;
        self._canRotate = json._canRotate;
        self._canMove = json._canMove;
        self._canSelect = json._canSelect;
        self._deletesParent = json._deletesParent;
        self._label = json._label;
        self._group = json._group;
        self._debugColor = json._debugColor;
        // Data object
        self.data = json.data;
        // Snap point data
        // Since we can't yet resolve the snap point partner to future entities which might not be loaded yet, we
        // use a TwoDEntityWeakReference, which then later gets resolved via _resolveEntityWeakReferences
        if (json.snapPoints != null) {
            for (var i = 0; i < json.snapPoints.length; i++) {
                var jsonSP = json.snapPoints[i];
                if (jsonSP.partner != null) {
                    var sp = self.getSnapPointById(jsonSP.id);
                    if (sp == null) {
                        console.warn("TwoDEntity.loadFromJSON(): warning: the snap point with id " + jsonSP.id + " does not exist in the model, however it was saved in the JSON. The snap point has not be setup properly!");
                    }
                    else {
                        sp._partner = new TwoDEntitySnapPoint(jsonSP.partner.label, jsonSP.partner.id);
                        sp._partner.entity = new TwoDEntityWeakReference(jsonSP.partner.entity_guid);
                    }
                }
            }
        }
        // Children
        for (var i = 0; i < json.children.length; i++) {
            var childJSON = json.children[i];
            //console.log("loadFromJSON", childJSON.typeName);
            var newEntity = null;
            try {
                newEntity = new window[childJSON.typeName]();
            }
            catch (e) {
                console.error(e);
                throw "Could not create entity for type " + childJSON.typeName + ": " + e;
            }
            newEntity.guid = childJSON.guid; // we need this before addChild
            self._addChild(newEntity); // _addChild has no redraw
            newEntity.loadFromJSON(childJSON);
        }
    };
    TwoDEntity.prototype.getSnapPointById = function (id) {
        for (var i = 0; i < this.snapPoints.length; i++) {
            if (this.snapPoints[i].id == id)
                return this.snapPoints[i];
        }
        return null;
    };
    TwoDEntity.prototype.getAllSnappedPartnerEntities = function () {
        var self = this;
        var ret = [];
        //TODO: is there a faster way to get these? Chaining via snap points?
        this.canvas.visitAllEntities(function (entity) {
            if (entity.guid != self.guid && entity.snapGroupId == self.snapGroupId) {
                ret.push(entity);
            }
        });
        return ret;
    };
    TwoDEntity.prototype._resolveEntityWeakReferences = function () {
        // Resolves TwoDEntityWeakReference 
        var self = this;
        // Snap points
        // Here we have weak references to partner entities that might not have been loaded at the time they were needed,
        // Thus the snap point partners are resolved here.
        for (var i = 0; i < self.snapPoints.length; i++) {
            var sp = self.snapPoints[i];
            if (sp._partner != null && sp._partner.entity != null && sp._partner.entity instanceof TwoDEntityWeakReference) {
                var partnerGUID = sp._partner.entity.futureGUID;
                //console.log("_resolveEntityWeakReferences: got a weak ref, will resolve...", partnerGUID);
                var realPartnerEntity = self.canvas.getEntityByGUID(partnerGUID);
                if (realPartnerEntity == null) {
                    sp.setPartner(null);
                    console.warn("_resolveEntityWeakReferences: could not get realPartnerEntity");
                    continue;
                }
                var realPartnerSP = realPartnerEntity.getSnapPointById(sp._partner.id);
                if (realPartnerSP == null) {
                    sp.setPartner(null);
                    console.warn("_resolveEntityWeakReferences: could not get realPartnerSP");
                    continue;
                }
                sp.setPartner(realPartnerSP);
            }
        }
        // Children
        for (var i = 0; i < self.children.length; i++) {
            self.children[i]._resolveEntityWeakReferences();
        }
        // Call event
        this.loadFromJSONWeakEntitiesResolved();
    };
    TwoDEntity.prototype.loadFromJSONWeakEntitiesResolved = function () {
        // Implemented by subclass, called when all the weak entities have been resolved
    };
    TwoDEntity.prototype.setDirty = function () {
        if (this.canvas != null)
            this.canvas.setDirty();
    };
    TwoDEntity.prototype.getPropById = function (id) {
        if (this.props == null)
            return null;
        for (var i = 0; i < this.props.length; i++) {
            if (this.props[i]._id == id)
                return this.props[i];
        }
        return null;
    };
    TwoDEntity.prototype.getPropValueById = function (id) {
        var prop = this.getPropById(id);
        if (prop == null)
            return null;
        return prop.getValue(this);
    };
    TwoDEntity.prototype.updateForNewPropValue = function (prop, val, eventData) {
        // Can be implemented by subclass
    };
    TwoDEntity.prototype.updateForAllPropValues = function () {
        for (var i = 0; i < this.props.length; i++) {
            var prop = this.props[i];
            var val = prop.getValue(this);
            this.updateForNewPropValue(prop, val);
        }
        if (this.canvas != null)
            this.canvas.requestRedraw();
        this.setDirty();
    };
    TwoDEntity.prototype.updateForAllPropUIs = function () {
        for (var i = 0; i < this.props.length; i++) {
            var prop = this.props[i];
            prop.updateUI();
        }
    };
    TwoDEntity.prototype.copyPartnerAlteringPropertiesToPartner = function (partner) {
        // Get all partner props
        var didUpdateAProp = false;
        for (var i = 0; i < this.props.length; i++) {
            var myProp = this.props[i];
            // Does this affect our partners?
            if (myProp._altersPartners == true) {
                // Do we have this prop?
                var partnerProp = partner.getPropById(myProp._id);
                if (partnerProp != null) {
                    var partnerValue = partnerProp.getValue(partner);
                    var myValue = myProp.getValue(this);
                    if (partnerValue != myValue) {
                        partnerProp.setValueWithoutTriggers(partner, myValue);
                        didUpdateAProp = true;
                    }
                }
            }
        }
        return didUpdateAProp;
    };
    TwoDEntity.prototype.isSelected = function () {
        if (this.canvas == null)
            return false;
        if (this.canvas.entitySelection == null)
            return false;
        return this.canvas.entitySelection.isSelected(this);
    };
    TwoDEntity.prototype.isHovered = function () {
        if (this.canvas == null)
            return false;
        return this.canvas.hoveredEntity == this;
    };
    TwoDEntity.prototype.triggerAction = function (action) {
        if (this.canvas == null)
            return this;
        if (this.canvas.onEntityAction == null)
            return this;
        this.canvas.onEntityAction(this.canvas, this, action);
        return this;
    };
    TwoDEntity.prototype.removeAllSnapPoints = function () {
        //TODO: we need to properly unhook the snap points
        this.snapPoints = [];
        this.setDirty();
        return this;
    };
    TwoDEntity.prototype.addSnapPoint = function (sp) {
        sp.entity = this;
        this.snapPoints.push(sp);
        this.setDirty();
        return this;
    };
    TwoDEntity.prototype.updateSnapPoints = function () {
        // Should be implemented by subclass, called when resized (setSize)
    };
    TwoDEntity.prototype.snappedToPartner = function (partner, spSelf, spPartner, isOriginalPlacedEntity) {
        // Should be implemented by subclass, called when resized (setSize)
    };
    TwoDEntity.prototype.acceptsSnapPointPartner = function (partner) {
        // Can be overridden by subclass
        // Check if the partner matches any in the list of other partner types
        if (this.acceptableSnapPointPartnerTypes != null && this.acceptableSnapPointPartnerTypes.length > 0) {
            for (var i = 0; i < this.acceptableSnapPointPartnerTypes.length; i++) {
                var typeName = this.acceptableSnapPointPartnerTypes[i];
                if (partner.typeName == typeName || typeName == "*")
                    return true;
            }
            return false;
        }
        else {
            return false;
        }
    };
    // When onlyUnsnapped is true, we only check our snappoints that are unsnapped currently.
    // This is usefull if we just want to check if any snappoints.
    // When onlyOverlapping is true, we only check snappoints that are directly overlapping.
    // This can be usefull if we just want to reconnect snappoints that happen to overlap after moving and entity.
    TwoDEntity.prototype.snapToClosebySnapPoints = function (moveEntity, onlyUnsnapped, onlyOverlapping) {
        if (moveEntity === void 0) { moveEntity = true; }
        if (onlyUnsnapped === void 0) { onlyUnsnapped = false; }
        if (onlyOverlapping === void 0) { onlyOverlapping = false; }
        //console.log("TwoDEntity.snapToClosebySnapPoints()", this.typeName);
        var maxDistanceBetweenSnapPoints = this.canvas.config.snapPointSnappingDistance;
        var self = this;
        var a = this;
        if (a.snapPoints == null || a.snapPoints.length == 0)
            return;
        var finalSnapPointGroupId = null;
        for (var spai = 0; spai < a.snapPoints.length; spai++) {
            var closestMatchDistance = 9999999999999.0;
            var matchA = null;
            var matchB = null;
            var spa = a.snapPoints[spai];
            if (onlyUnsnapped == true && spa.partner() != null)
                continue; // 
            var spaPos = spa.worldPos();
            // Loop all children
            // TODO: what about nested children !?
            for (var ebi = 0; ebi < self.canvas.rootEntity.children.length; ebi++) {
                // Get b, the other entity
                var b = self.canvas.rootEntity.children[ebi];
                // Skip self, of course
                if (b.rid == a.rid)
                    continue;
                // Skip if no snap points
                if (b.snapPoints == null || b.snapPoints.length == 0)
                    continue;
                // Do we event accept this partner?
                var acceptPartner = self.acceptsSnapPointPartner(b);
                if (!acceptPartner)
                    continue;
                // Skip if bounds not close
                //TODO
                // Check distance between each other b snappoint position
                for (var spbi = 0; spbi < b.snapPoints.length; spbi++) {
                    // Get distance to other snap point
                    var spb = b.snapPoints[spbi];
                    var spbPos = spb.worldPos();
                    var dist = spaPos.dist(spbPos);
                    if (onlyOverlapping == true && dist > 0.00000001)
                        continue;
                    // Is it less than a max threshhold and closest match so far?
                    if (dist < maxDistanceBetweenSnapPoints && dist < closestMatchDistance) {
                        closestMatchDistance = dist;
                        matchA = spa; // self
                        matchB = spb; // other
                        //TODO: break if very close (note: can't break due to reset of all snap points)
                    }
                }
            }
            if (matchB != null) {
                // We got a match - snap to this!
                self.snapToSnapPoint(matchA, matchB, moveEntity);
                // Store the snap group id - we need this later to reset since another might undo this
                finalSnapPointGroupId = self.snapGroupId;
            }
            else {
                // No match - unsnap!
                self.snapToSnapPoint(spa, null, moveEntity); // reset
            }
        }
        // Overwrite the snap group id, since the nomatch-unsnap might have set this to null
        //console.log("Commit-4b81a9b8-BUG F overwrite the snap group id with finalSnapPointGroupId = " + finalSnapPointGroupId);
        // This is actually no longer needed, because TwoDEntitySnapPoint.setPartner now only unsets the snappoint group id if nothing else is connected anymore...
        //self.snapGroupId = finalSnapPointGroupId; //BUG: this will actually set to null since some snappoints checked can get unsnapped
    };
    TwoDEntity.prototype.snapToSnapPoint = function (spSelf, spOther, moveEntity) {
        if (moveEntity === void 0) { moveEntity = true; }
        //console.log("TwoDEntity.snapToSnapPoint()");
        if (spOther == null) {
            // Unlink spSelf
            //console.log("Commit-4b81a9b8-BUG A: spSelf.setPartner(null); via if (spOther == null)" + " (entity " + this.rid + ", snapid = " + this.snapGroupId + ")");
            spSelf.setPartner(null);
            // Event
            this.snappedToPartner(null, null, null, false);
            this.setDirty();
        }
        else {
            //TODO: validate that spSelf is really own
            //var newRot = spOther.worldRot() + spSelf.rot;
            //console.log("snapToSnapPoint", spSelf.id, spOther.id);
            if (moveEntity == true) {
                var newRot = spOther.worldRot() + Machinata.Math.degToRad(180) - spSelf.rot;
                var newPos = spOther.worldPos();
                newPos.x += -spSelf.pos.rot(newRot).x;
                newPos.y += -spSelf.pos.rot(newRot).y;
                this.setWorldRot(newRot);
                this.setWorldPos(newPos);
                // Check if our move broke another snappoint of ours?
                // We do this by checking all our other snappoints, and if we are snapped
                // but no longer overlapping then we break that snappoint
                for (var spi = 0; spi < this.snapPoints.length; spi++) {
                    var spSelfSibling = this.snapPoints[spi];
                    if (spSelfSibling == spSelf)
                        continue;
                    if (spSelfSibling.isSnapped() && spSelfSibling.isSnappedAndOverlapping() == false) {
                        //console.warn("SP is no longer connected!!!!");
                        // Unlink spSelfSibling
                        //console.log("Commit-4b81a9b8-BUG B: spSelfSibling.setPartner(null); via Sibling no longer connected" + " (entity " + this.rid + ")");
                        spSelfSibling.setPartner(null);
                    }
                }
            }
            spSelf.setPartner(spOther);
            // Event
            this.snappedToPartner(spOther.entity, spSelf, spOther, true);
            spOther.entity.snappedToPartner(this, spOther, spSelf, false);
            this.setDirty();
            // Snap any additional snap points that resulted in this?
            this.snapToClosebySnapPoints(false, true, true); // only unsnapped, only overlapping
        }
    };
    TwoDEntity.prototype.setLabel = function (label) {
        this._label = label;
        this.setDirty();
        return this;
    };
    TwoDEntity.prototype.getLabel = function () {
        return this._label;
    };
    TwoDEntity.prototype.getFullLabel = function () {
        return this._label;
    };
    TwoDEntity.prototype.setCanSelect = function (canSelect) {
        this._canSelect = canSelect;
        return this;
    };
    TwoDEntity.prototype.setCanMove = function (canMove) {
        this._canMove = canMove;
        return this;
    };
    TwoDEntity.prototype.setCanRotate = function (canRotate) {
        this._canRotate = canRotate;
        return this;
    };
    TwoDEntity.prototype.addChild = function (child) {
        this._addChild(child);
        if (this.canvas != null) {
            this.canvas.requestRedraw();
        }
        this.setDirty();
        return this;
    };
    TwoDEntity.prototype._addChild = function (child) {
        child.parent = this;
        child._setCanvas(this.canvas);
        this.children.push(child);
        return this;
    };
    TwoDEntity.prototype._setCanvas = function (canvas) {
        this.canvas = canvas;
        if (canvas != null) {
            canvas._guidToEntityMap[this.guid] = this;
        }
        // Make sure all child children are hooked on the canvas
        for (var i = 0; i < this.children.length; i++) {
            this.children[i]._setCanvas(canvas);
        }
    };
    TwoDEntity.prototype.addTo = function (parent) {
        parent.addChild(this);
        this.setDirty();
        return this;
    };
    TwoDEntity.prototype.addToCanvas = function (canvas) {
        canvas.addEntity(this);
        this.setDirty();
        return this;
    };
    TwoDEntity.prototype.delete = function () {
        if (this.parent == null) {
            throw "Cannot delete as this item is not attached to any parent";
        }
        this.parent.deleteChild(this);
        this.setDirty();
        return this;
    };
    TwoDEntity.prototype.deleteChild = function (child) {
        // MASTER DELETE METHOD - ALL DELETES SHOULD GO THROUGH HERE
        // Remove from child list
        var index = this.children.indexOf(child);
        if (index > -1) {
            this.children.splice(index, 1);
        }
        else {
            throw "Cannot delete child as it is not a child of this item";
        }
        // Unlink any snappoints
        for (var spi = 0; spi < child.snapPoints.length; spi++) {
            var sp = child.snapPoints[spi];
            var partner = sp.partner();
            sp.setPartner(null);
            if (partner != null && partner.entity != null) {
                partner.entity.snappedToPartner(null, null, null, false);
            }
        }
        // Unregister from maps
        if (this.canvas != null) {
            delete this.canvas._guidToEntityMap[child.guid];
        }
        // Unselect
        if (child.isSelected()) {
            this.canvas.entitySelection.deselect(child); // de-select
            if (this.canvas.entitySelection.entities.length == 0)
                this.canvas.entitySelection = null;
        }
        // Unlink from parent and canvas
        child.parent = null;
        child.canvas = null;
        // Call on delted event
        if (this.canvas.onEntityDeleted)
            this.canvas.onEntityDeleted(this, child);
        // Request redraw
        this.canvas.requestRedraw();
        this.setDirty();
        return this;
    };
    TwoDEntity.prototype.setRot = function (rot) {
        this._rot = rot;
        this.setDirty();
        return this;
    };
    // If true, the entity won't be drawn or impose its own bounds
    TwoDEntity.prototype.setGroup = function (group) {
        this._group = group;
        this.setDirty();
        return this;
    };
    TwoDEntity.prototype.setPos = function (pos) {
        this._pos = new gpos(pos.x, pos.y);
        this.setDirty();
        return this;
    };
    TwoDEntity.prototype.setSize = function (size) {
        this._size = size;
        if (this.snapPoints != null && this.snapPoints.length > 0)
            this.updateSnapPoints();
        this.setDirty();
        return this;
    };
    //setSize(w: number, h: number): TwoDEntity {
    //    return this.setSize(new vec2(w,h));
    //}
    TwoDEntity.prototype.worldPos = function () {
        if (this.parent == null)
            return this._pos;
        var parentPos = this.parent.worldPos();
        var parentRot = this.parent.worldRot();
        return parentPos
            .add(this._pos)
            .rotAroundPivot(parentRot, parentPos);
    };
    TwoDEntity.prototype.worldRot = function () {
        if (this.parent == null)
            return this._rot;
        var parentRot = this.parent.worldRot();
        return (this._rot + parentRot);
    };
    TwoDEntity.prototype.setWorldRot = function (newRot) {
        var parentRot = this.parent.worldRot();
        this._rot = newRot - parentRot;
        this.setDirty();
        return this;
    };
    TwoDEntity.prototype.setWorldPos = function (newPos) {
        var parentPos = this.parent.worldPos();
        var parentRot = this.parent.worldRot();
        newPos = newPos.rotAroundPivot(-parentRot, parentPos);
        newPos = new gpos(newPos.x - parentPos.x, newPos.y - parentPos.y);
        this._pos = newPos;
        return this;
    };
    TwoDEntity.prototype.bounds = function () {
        // Get self
        // P1---P2
        // |     |
        // P4---P3
        var myBoundsP1 = new gpos(// upper left
        -this._size.w / 2, -this._size.h / 2).rot(this._rot);
        var myBoundsP2 = new gpos(// upper right
        +this._size.w / 2, -this._size.h / 2).rot(this._rot);
        var myBoundsP3 = new gpos(// lower right
        +this._size.w / 2, +this._size.h / 2).rot(this._rot);
        var myBoundsP4 = new gpos(// lower left
        -this._size.w / 2, +this._size.h / 2).rot(this._rot);
        var myBounds = new gbounds(new gpos(this._pos.x + (myBoundsP1.x + myBoundsP2.x + myBoundsP3.x + myBoundsP4.x) / 4.0, this._pos.y + (myBoundsP1.y + myBoundsP2.y + myBoundsP3.y + myBoundsP4.y) / 4.0), new gsize(Math.max(myBoundsP1.x, myBoundsP2.x, myBoundsP3.x, myBoundsP4.x) - Math.min(myBoundsP1.x, myBoundsP2.x, myBoundsP3.x, myBoundsP4.x), Math.max(myBoundsP1.y, myBoundsP2.y, myBoundsP3.y, myBoundsP4.y) - Math.min(myBoundsP1.y, myBoundsP2.y, myBoundsP3.y, myBoundsP4.y)));
        for (var _i = 0, _a = this.children; _i < _a.length; _i++) {
            var child = _a[_i];
            // Get child bounds, in our coord space
            var childBounds = child.bounds();
            childBounds.center.x += this._pos.x;
            childBounds.center.y += this._pos.y;
            myBounds.extend(childBounds);
        }
        return myBounds;
    };
    TwoDEntity.prototype._calculateWorldBounds = function () {
        // Get self
        // P1---P2
        // |     |
        // P4---P3
        var worldPos = this.worldPos();
        var wrot = this.worldRot();
        var p1 = new gpos(// upper left
        -this._size.w / 2, -this._size.h / 2).rot(wrot).add(worldPos);
        var p2 = new gpos(// upper right
        +this._size.w / 2, -this._size.h / 2).rot(wrot).add(worldPos);
        var p3 = new gpos(// lower right
        +this._size.w / 2, +this._size.h / 2).rot(wrot).add(worldPos);
        var p4 = new gpos(// lower left
        -this._size.w / 2, +this._size.h / 2).rot(wrot).add(worldPos);
        return new gbounds(new gpos((p1.x + p2.x + p3.x + p4.x) / 4.0, (p1.y + p2.y + p3.y + p4.y) / 4.0), new gsize(Math.max(p1.x, p2.x, p3.x, p4.x) - Math.min(p1.x, p2.x, p3.x, p4.x), Math.max(p1.y, p2.y, p3.y, p4.y) - Math.min(p1.y, p2.y, p3.y, p4.y)));
    };
    TwoDEntity.prototype.worldBounds = function () {
        var bounds;
        // If group, then return the first childs bounds, otherwise the calculated version
        if (this._group == true) {
            bounds = new gbounds(new gpos(0, 0), new gsize(0, 0));
            if (this.children.length > 1)
                bounds = this.children[1].worldBounds();
        }
        else {
            bounds = this._calculateWorldBounds();
        }
        // Extend by child bounds
        for (var _i = 0, _a = this.children; _i < _a.length; _i++) {
            var child = _a[_i];
            var childBounds = child.worldBounds();
            bounds.extend(childBounds);
        }
        return bounds;
    };
    TwoDEntity.prototype.visualBoundsPadding = function () {
        // Can be implemented by subclass...
        return 0;
    };
    TwoDEntity.prototype.visualWorldBounds = function () {
        // Same as worldBounds, but just with some added padding
        // This allows entities to have a logical size (for selecting, grabbing, etc), but
        // slight overhang draws...
        var bounds;
        // If group, then return the first childs bounds, otherwise the calculated version
        if (this._group == true) {
            bounds = new gbounds(new gpos(0, 0), new gsize(0, 0));
            if (this.children.length > 1)
                bounds = this.children[1].worldBounds();
        }
        else {
            bounds = this._calculateWorldBounds();
            bounds.size.w += this.visualBoundsPadding() * 2;
            bounds.size.h += this.visualBoundsPadding() * 2;
        }
        // Extend by child bounds
        for (var _i = 0, _a = this.children; _i < _a.length; _i++) {
            var child = _a[_i];
            var childBounds = child.visualWorldBounds();
            bounds.extend(childBounds);
        }
        return bounds;
    };
    TwoDEntity.prototype.rotateHandlePos = function () {
        var handleOffset = this._size.h / 2 + (15.0 / this.canvas._previewRenderer.viewport.drawScale());
        var worldPos = this.worldPos();
        var worldRot = this.worldRot();
        //var handlePos = worldPos.rot(worldRot).add(new gpos(0, -handleOffset));
        var handlePos = worldPos.add(new gpos(0, -handleOffset).rot(worldRot));
        return handlePos;
    };
    TwoDEntity.prototype.drawBG = function (renderer) {
        var ctx = renderer.ctx;
    };
    TwoDEntity.prototype.drawEntity = function (renderer) {
        var ctx = renderer.ctx;
    };
    TwoDEntity.prototype.drawFG = function (renderer) {
        var ctx = renderer.ctx;
    };
    TwoDEntity.prototype.drawLabel = function (renderer, label) {
        if (label === void 0) { label = null; }
        if (label == null)
            label = this.getFullLabel();
        var ctx = renderer.ctx;
        var rot = this.worldRot();
        ctx.rotate(-rot);
        renderer.drawLabel(this, label, 0, 0, "center");
        ctx.rotate(+rot);
    };
    TwoDEntity.prototype.getChildAtPoint = function (pos, onlyInteractable) {
        if (onlyInteractable === void 0) { onlyInteractable = false; }
        for (var i = this.children.length - 1; i >= 0; i--) {
            var child = this.children[i];
            if (!child._enabled)
                continue;
            if (onlyInteractable == true && (child._canSelect == false && child._canClick == false))
                continue;
            if (child.worldBounds().containsPoint(pos)) {
                var grandchild = child.getChildAtPoint(pos);
                if (grandchild != null)
                    return grandchild;
                return child;
            }
        }
        return null;
    };
    TwoDEntity.prototype.onClick = function () {
    };
    TwoDEntity.prototype.onSelected = function () {
    };
    TwoDEntity.prototype.onDeselected = function () {
    };
    TwoDEntity.prototype.didMove = function () {
    };
    TwoDEntity.prototype.moveTo = function (newWorldPos, snapToGrid) {
        if (snapToGrid) {
            var snapSteps = 10; // fallback
            if (this.canvas != null)
                snapSteps = this.canvas._previewRenderer.viewport.gridSize;
            newWorldPos.x = Math.round(newWorldPos.x / snapSteps) * snapSteps;
            newWorldPos.y = Math.round(newWorldPos.y / snapSteps) * snapSteps;
        }
        var didChange = false;
        var previewPos = this._pos.clone();
        this.setWorldPos(newWorldPos);
        if (this._pos.x != previewPos.x || this._pos.y != previewPos.y)
            didChange = true;
        if (didChange == true) {
            this.didMove();
            this.setDirty();
        }
    };
    TwoDEntity.prototype.pointTo = function (targetWorldPos, snapToGrid) {
        var worldPos = this.worldPos();
        var newRot = Math.atan2(targetWorldPos.y - worldPos.y, targetWorldPos.x - worldPos.x);
        newRot += Math.PI / 2;
        if (snapToGrid) {
            var snapSteps = (Math.PI * 2) / 8; // 45 deg
            newRot = Math.round(newRot / snapSteps) * snapSteps;
        }
        this.setWorldRot(newRot);
        this.setDirty();
    };
    TwoDEntity.prototype._drawBG = function (renderer) {
        var ctx = renderer.ctx;
        // Skip if disabled
        if (!this._enabled)
            return;
        // Skip if not in viewport
        if (!renderer.viewport.worldBounds().intersects(this.visualWorldBounds()))
            return;
        ctx.save();
        {
            // Statis
            renderer.statsTotalDraws++;
            // Do entity transformation
            ctx.translate(this._pos.x, this._pos.y);
            ctx.rotate(this._rot);
            // Debug bg
            if (this.canvas.debug == true) {
                ctx.save();
                {
                    ctx.globalAlpha = 0.2;
                    ctx.fillStyle = this._debugColor;
                    ctx.strokeStyle = null;
                    ctx.fillRect(-this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
                    if (this.isSelected()) {
                        ctx.strokeStyle = "red";
                        ctx.lineWidth = 2;
                        ctx.strokeRect(-this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
                    }
                    if (this == this.canvas.hoveredEntity) {
                        ctx.strokeStyle = "blue";
                        ctx.lineWidth = 2;
                        ctx.strokeRect(-this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
                    }
                    ctx.globalAlpha = 1.0;
                }
                ctx.restore();
            }
            // Call implementing draw
            this.drawBG(renderer);
            // Draw all children
            for (var _i = 0, _a = this.children; _i < _a.length; _i++) {
                var child = _a[_i];
                child._drawBG(renderer);
            }
        }
        ctx.restore();
    };
    TwoDEntity.prototype._drawEntity = function (renderer) {
        var ctx = renderer.ctx;
        // Skip if disabled
        if (!this._enabled)
            return;
        // Skip if not in viewport
        if (!renderer.viewport.worldBounds().intersects(this.visualWorldBounds()))
            return;
        ctx.save();
        {
            // Statis
            renderer.statsTotalDraws++;
            // Do entity transformation
            ctx.translate(this._pos.x, this._pos.y);
            ctx.rotate(this._rot);
            // Call implementing draw
            this.drawEntity(renderer);
            // Draw all children
            for (var _i = 0, _a = this.children; _i < _a.length; _i++) {
                var child = _a[_i];
                child._drawEntity(renderer);
            }
        }
        ctx.restore();
    };
    TwoDEntity.prototype._drawFG = function (renderer) {
        var ctx = renderer.ctx;
        // Skip if disabled
        if (!this._enabled)
            return;
        // Skip if not in viewport
        if (!renderer.viewport.worldBounds().intersects(this.visualWorldBounds()))
            return;
        ctx.save();
        {
            // Statis
            renderer.statsTotalDraws++;
            // Do entity transformation
            ctx.translate(this._pos.x, this._pos.y);
            ctx.rotate(this._rot);
            // Call implementing draw
            this.drawFG(renderer);
            // Draw all children
            for (var _i = 0, _a = this.children; _i < _a.length; _i++) {
                var child = _a[_i];
                child._drawFG(renderer);
            }
        }
        ctx.restore();
    };
    TwoDEntity.prototype._drawUI = function (renderer) {
        //TODO: optimisze wordPos/worldRot access via caching!
        var ctx = renderer.ctx;
        // Skip if disabled
        if (!this._enabled)
            return;
        // Skip if not in viewport
        if (!renderer.viewport.worldBounds().intersects(this.visualWorldBounds()))
            return;
        ctx.save();
        {
            var drawSnapPoints = false;
            if (this.canvas != null && this.canvas._interactionMode == TwoDCanvasInteractionMode.DRAG)
                drawSnapPoints = true;
            // Selected bounds
            if (this.isSelected()) {
                drawSnapPoints = true;
                var worldPos = this.worldPos();
                var worldRot = this.worldRot();
                // Draw selection border
                ctx.save();
                {
                    ctx.translate(worldPos.x, worldPos.y);
                    ctx.rotate(worldRot);
                    ctx.strokeStyle = this.canvas.config.selectedBorderColor;
                    ctx.lineWidth = this.canvas.config.selectedBorderSize / renderer.viewport.drawScale();
                    ctx.strokeRect(-this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
                }
                ctx.restore();
                // Rotate handle?
                // Deprecated: now handled by selection
                /*if (this.canvas.entitySelection.canRotate()) {

                    var handlePos = this.rotateHandlePos();
                    if (this.canvas._interactionMode == TwoDCanvasInteractionMode.ROTATE) {
                        handlePos = this.canvas.cursorPos;
                    }
                    let handleSize = this.canvas.config.rotateHandleSize / renderer.viewport.drawScale();
                    //if (handlePos.dist(this.canvas.cursorPos) < (this.canvas.config.rotateHandleSize * 1.5) / 2.0) handleSize = this.canvas.config.rotateHandleSize * 1.5 / renderer.viewport.zoom;
                    ctx.save(); {
                        // Line
                        ctx.beginPath();
                        ctx.moveTo(worldPos.x, worldPos.y);
                        ctx.lineTo(handlePos.x, handlePos.y);
                        ctx.lineWidth = this.canvas.config.rotateHandleLineSize / renderer.viewport.drawScale();
                        ctx.strokeStyle = this.canvas.config.rotateHandleColor;
                        ctx.closePath();
                        ctx.stroke();
                        // Dot
                        ctx.translate(handlePos.x, handlePos.y);
                        ctx.fillStyle = this.canvas.config.rotateHandleColor;
                        ctx.beginPath();
                        ctx.arc(0, 0, handleSize, 0, 2 * Math.PI);
                        ctx.closePath();
                        ctx.fill();
                    } ctx.restore();
                }*/
            }
            else if (this.canvas.hoveredEntity == this) {
                drawSnapPoints = true;
                var worldPos = this.worldPos();
                var worldRot = this.worldRot();
                // Draw selection border
                ctx.save();
                {
                    ctx.translate(worldPos.x, worldPos.y);
                    ctx.rotate(worldRot);
                    ctx.strokeStyle = this.canvas.config.hoveredBorderColor;
                    ctx.lineWidth = this.canvas.config.hoveredBorderSize / renderer.viewport.drawScale();
                    ctx.strokeRect(-this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
                }
                ctx.restore();
            }
            // Snap points?
            if (this.snapPoints != null && this.snapPoints.length > 0) {
                var worldPos = this.worldPos();
                var worldRot = this.worldRot();
                for (var i = 0; i < this.snapPoints.length; i++) {
                    var sp = this.snapPoints[i];
                    if (drawSnapPoints == true || (this.canvas.config.snapPointAlwaysShowWhenSnapped && sp.partner() != null)) {
                        var spSize = this.canvas.config.snapPointSize;
                        if (sp.partner() != null)
                            spSize = this.canvas.config.snapPointSizeSnapped;
                        ctx.save();
                        {
                            // Move/rotate to snappoint pos
                            ctx.translate(worldPos.x, worldPos.y);
                            ctx.rotate(worldRot);
                            ctx.translate(sp.pos.x, sp.pos.y);
                            ctx.rotate(sp.rot);
                            // Draw half circl
                            ctx.beginPath();
                            ctx.arc(0, // x
                            0, // y
                            spSize / renderer.viewport.drawScale(), // radius
                            0, // start angle
                            Math.PI, // end angle
                            false);
                            ctx.closePath();
                            ctx.strokeStyle = null;
                            ctx.fillStyle = sp.valid ? this.canvas.config.snapPointValidColor : this.canvas.config.snapPointInvalidColor;
                            ctx.fill();
                            // Draw label
                            if (sp.label != null && (this.canvas.hoveredEntity == this || this.isSelected())) {
                                // Undo rotation from above
                                ctx.translate(0, (spSize / renderer.viewport.drawScale()) * 0.4);
                                ctx.rotate(-sp.rot);
                                ctx.rotate(-worldRot);
                                ctx.font = ((this.canvas.config.snapPointSize * 0.9) / renderer.viewport.drawScale()) + "px " + renderer.viewport.labelFontName;
                                ctx.fillStyle = "white"; //TODO
                                ctx.textAlign = "center";
                                ctx.textBaseline = "middle";
                                ctx.fillText(sp.label[0], 0, 0);
                            }
                        }
                        ctx.restore();
                    }
                }
            }
            // Debug bounds
            if (this.canvas.debug == true) {
                //var bounds = this.bounds();
                var worldPos = this.worldPos();
                var bounds = this.worldBounds();
                var visualBounds = this.visualWorldBounds();
                ctx.save();
                {
                    var lineSize = 1.0 / renderer.viewport.drawScale();
                    var dotSize = 2.0 / renderer.viewport.drawScale();
                    ctx.globalAlpha = 0.5;
                    if (true) {
                        ctx.fillStyle = null;
                        ctx.strokeStyle = this._debugColor;
                        ctx.lineWidth = lineSize;
                        ctx.strokeRect(bounds.center.x - bounds.size.w / 2, bounds.center.y - bounds.size.h / 2, bounds.size.w, bounds.size.h);
                    }
                    if (true) {
                        ctx.fillStyle = null;
                        ctx.strokeStyle = this._debugColor;
                        ctx.lineWidth = lineSize;
                        ctx.strokeRect(visualBounds.center.x - visualBounds.size.w / 2, visualBounds.center.y - visualBounds.size.h / 2, visualBounds.size.w, visualBounds.size.h);
                    }
                    if (true) {
                        ctx.strokeStyle = null;
                        ctx.fillStyle = "black";
                        ctx.fillRect(worldPos.x - dotSize / 2, worldPos.y - dotSize / 2, dotSize, dotSize);
                    }
                    if (false) {
                        ctx.fillStyle = this._debugColor;
                        ctx.fillRect(bounds.center.x - dotSize / 2, bounds.center.y - dotSize / 2, dotSize, dotSize);
                    }
                }
                ctx.restore();
            }
            // Draw children UI
            for (var _i = 0, _a = this.children; _i < _a.length; _i++) {
                var child = _a[_i];
                child._drawUI(renderer);
            }
        }
        ctx.restore();
    };
    TwoDEntity.prototype.toString = function () {
        var spPartners = "";
        for (var i = 0; i < this.snapPoints.length; i++) {
            if (this.snapPoints[i]._partner != null) {
                spPartners += ", partner with " + this.snapPoints[i]._partner.entity.guid;
            }
        }
        var fullLabel = this.getFullLabel() || "";
        return Machinata.String.replaceAll(fullLabel, "\n", "/") + " (rid=" + this.rid + ", guid=" + this.guid + ", snapid=" + this.snapGroupId + spPartners + ")";
    };
    TwoDEntity._rid = 0;
    return TwoDEntity;
}());
Machinata.TwoD.Entity = TwoDEntity;
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityRoot = /** @class */ (function (_super) {
    __extends(TwoDEntityRoot, _super);
    function TwoDEntityRoot() {
        return _super.call(this, "TwoDEntityRoot") || this;
    }
    return TwoDEntityRoot;
}(TwoDEntity));
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityWeakReference = /** @class */ (function (_super) {
    __extends(TwoDEntityWeakReference, _super);
    function TwoDEntityWeakReference(futureGUID) {
        var _this = _super.call(this, "TwoDEntityWeakReference") || this;
        _this.futureGUID = null;
        _this.futureGUID = futureGUID;
        return _this;
    }
    return TwoDEntityWeakReference;
}(TwoDEntity));
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityPlain = /** @class */ (function (_super) {
    __extends(TwoDEntityPlain, _super);
    function TwoDEntityPlain() {
        return _super.call(this, "TwoDEntityPlain") || this;
    }
    return TwoDEntityPlain;
}(TwoDEntity));
