var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDEntityImage = /** @class */ (function (_super) {
    __extends(TwoDEntityImage, _super);
    function TwoDEntityImage() {
        var _this = _super.call(this, "TwoDEntityImage") || this;
        _this._imageURL = null;
        _this._imageLoaded = false;
        _this._autoSize = false;
        _this._autoSizeScale = 1;
        _this.border = false;
        var self = _this;
        _this._imageURL = null;
        _this._imageLoaded = false;
        _this.image = new Image();
        _this.image.onload = function () {
            self._imageLoaded = true;
            console.log("TwoDEntityImage: image loaded: " + self.image.src);
            if (self._autoSize == true) {
                // Note: setSize will mark the canvas as dirty - an unwanted sideeffect
                //self.setSize(new gsize(self.image.width * self._autoSizeScale, self.image.height * self._autoSizeScale));
                self._size = new gsize(self.image.width * self._autoSizeScale, self.image.height * self._autoSizeScale);
            }
            if (self.canvas != null)
                self.canvas.requestRedraw();
        };
        // Properties
        _this.props.push(new TwoDEntityProp()
            .title("{text.twod.image}")
            .uiType("image")
            .defaultValue(null)
            .getter(function (entity) { return entity._imageURL; })
            .setter(function (entity, val) { entity.setImage(val); }));
        return _this;
    }
    TwoDEntityImage.prototype.saveAsJSON = function () {
        var json = _super.prototype.saveAsJSON.call(this);
        json._imageURL = this._imageURL;
        json._autoSize = this._autoSize;
        json._autoSizeScale = this._autoSizeScale;
        json.border = this.border;
        return json;
    };
    TwoDEntityImage.prototype.loadFromJSON = function (json) {
        _super.prototype.loadFromJSON.call(this, json);
        this.setImage(json._imageURL);
        this._autoSize = json._autoSize;
        this._autoSizeScale = json._autoSizeScale || 1;
        this.border = json.border || false;
    };
    TwoDEntityImage.prototype.setAutoSize = function (autoSize, scale) {
        this._autoSize = autoSize;
        if (scale)
            this._autoSizeScale = scale;
        return this;
    };
    TwoDEntityImage.prototype.setBorder = function (border) {
        this.border = border;
        return this;
    };
    TwoDEntityImage.prototype.setImage = function (url) {
        this._imageLoaded = false;
        this._imageURL = url;
        this.image.src = url;
        return this;
    };
    TwoDEntityImage.prototype.drawEntity = function (renderer) {
        var ctx = renderer.ctx;
        if (this._imageLoaded == true) {
            ctx.drawImage(this.image, -this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
            if (this.border == true) {
                renderer.setSolidBorderStyle();
                ctx.beginPath();
                ctx.rect(-this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
                ctx.closePath();
                ctx.stroke();
            }
        }
    };
    return TwoDEntityImage;
}(TwoDEntity));
Machinata.TwoD.EntityImage = TwoDEntityImage;
