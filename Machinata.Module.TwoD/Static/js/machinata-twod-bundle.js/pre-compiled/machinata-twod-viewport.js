/// <type>class</type>
var TwoDCanvasViewport = /** @class */ (function () {
    function TwoDCanvasViewport() {
        this.pos = new gpos(0, 0);
        this.zoom = 1.0;
        this.dpi = 96;
        this.scale = 1 / 100;
        this.units = "mm";
        this.viewSize = new gsize(800, 800);
        this.deviceSize = new gsize(800, 800);
        this.gridEnabled = true;
        this.gridSize = 1000; // default 1m
        this.scaleEnabled = true;
        this.labelFontSize = TwoDCanvas.DEFAULT_LABEL_SIZE;
        this.labelFontName = TwoDCanvas.DEFAULT_LABEL_FONT;
    }
    TwoDCanvasViewport.prototype.setZoom = function (newZoom) {
        this.zoom = newZoom;
        this.viewSize = this.deviceSize.mul(1.0 / this.drawScale());
    };
    TwoDCanvasViewport.prototype.fitBounds = function (bounds) {
        // Set to center
        this.pos = new gpos(-bounds.center.x, -bounds.center.y);
        // Get zoom to fit
        var scaleFactor = this.drawScaleForZoom(1.0); // We need this factor since our actual draw scale is different from the zoom
        var newZoomX = (this.deviceSize.w / scaleFactor) / bounds.size.w;
        var newZoomY = (this.deviceSize.h / scaleFactor) / bounds.size.h;
        var newZoom = Math.min(newZoomX, newZoomY);
        this.setZoom(newZoom - (newZoom * 0.01));
    };
    TwoDCanvasViewport.prototype.drawScale = function () {
        return this.drawScaleForZoom(this.zoom);
    };
    TwoDCanvasViewport.prototype.drawScaleForZoom = function (zoom) {
        if (this.units == "mm") {
            return zoom * this.scale * (this.mmToIn(1) * this.dpi);
        }
        else {
            throw this.units + " units is not supported";
        }
    };
    TwoDCanvasViewport.prototype.worldBounds = function () {
        return new gbounds(new gpos(-this.pos.x, -this.pos.y), new gsize(this.viewSize.w, this.viewSize.h));
    };
    TwoDCanvasViewport.prototype.inToPx = function (inches) {
        // px = inches * dpi * physicalScale
        return inches * this.dpi * this.scale;
    };
    TwoDCanvasViewport.prototype.pxToIn = function (px) {
        // px = inches * dpi * physicalScale
        // inches = px/(dpi * physicalScale)
        return px / (this.dpi * this.scale);
    };
    TwoDCanvasViewport.prototype.inToMM = function (inches) {
        return inches * 25.4;
    };
    TwoDCanvasViewport.prototype.mmToIn = function (mm) {
        return mm / 25.4;
    };
    TwoDCanvasViewport.prototype.mmToPDFPoints = function (mm) {
        var inches = this.mmToIn(mm);
        return inches * 72;
    };
    TwoDCanvasViewport.prototype.mmToPx = function (mm) {
        var inches = this.mmToIn(mm);
        return this.inToPx(inches);
    };
    TwoDCanvasViewport.prototype.mmToPxUnscaled = function (mm) {
        var inches = this.mmToIn(mm);
        return this.inToPx(inches) / this.scale;
    };
    TwoDCanvasViewport.prototype.pxToMM = function (px) {
        var inches = this.pxToIn(px);
        return this.inToMM(inches);
    };
    return TwoDCanvasViewport;
}());
