declare class TwoDEntityArrow extends TwoDEntity {
    dashed: boolean;
    constructor();
    saveAsJSON(): any;
    loadFromJSON(json: any): void;
    setDashed(dashed: boolean): this;
    drawEntity(renderer: TwoDCanvasRenderer): void;
}
declare class TwoDEntityCircle extends TwoDEntity {
    fillColor: string;
    constructor();
    saveAsJSON(): any;
    loadFromJSON(json: any): void;
    drawEntity(renderer: TwoDCanvasRenderer): void;
}
declare class TwoDEntityTriangle extends TwoDEntity {
    fillColor: string;
    constructor();
    saveAsJSON(): any;
    loadFromJSON(json: any): void;
    drawEntity(renderer: TwoDCanvasRenderer): void;
}
