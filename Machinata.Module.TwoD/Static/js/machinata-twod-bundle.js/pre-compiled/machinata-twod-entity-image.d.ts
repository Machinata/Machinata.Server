declare class TwoDEntityImage extends TwoDEntity {
    image: HTMLImageElement;
    _imageURL: string;
    _imageLoaded: boolean;
    _autoSize: boolean;
    _autoSizeScale: number;
    border: boolean;
    constructor();
    saveAsJSON(): any;
    loadFromJSON(json: any): void;
    setAutoSize(autoSize: boolean, scale?: number): this;
    setBorder(border: boolean): this;
    setImage(url: string): this;
    drawEntity(renderer: TwoDCanvasRenderer): void;
}
