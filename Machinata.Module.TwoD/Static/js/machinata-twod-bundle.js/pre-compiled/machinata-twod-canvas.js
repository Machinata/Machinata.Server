/// <summary>
/// Copyright Dan Krusi 2020-2021
/// </summary>
/// <summary>
/// 
/// </summary>
var TwoDCanvasInteractionMode;
(function (TwoDCanvasInteractionMode) {
    TwoDCanvasInteractionMode["NONE"] = "NONE";
    TwoDCanvasInteractionMode["DRAG"] = "DRAG";
    TwoDCanvasInteractionMode["PAN"] = "PAN";
    TwoDCanvasInteractionMode["ROTATE"] = "ROTATE";
    TwoDCanvasInteractionMode["SELECT"] = "SELECT";
    TwoDCanvasInteractionMode["CLICK"] = "CLICK";
})(TwoDCanvasInteractionMode || (TwoDCanvasInteractionMode = {}));
/// <summary>
/// 
/// </summary>
/// <type>class</type>
var TwoDCanvas = /** @class */ (function () {
    function TwoDCanvas(canvasElem, config) {
        this._isDirty = false;
        this.debug = Machinata.DebugEnabled;
        this.backgroundEntity = null;
        this._guidToEntityMap = {};
        this.data = {}; // custom user data
        this.maxZoom = 10.0;
        this.minZoom = 0.1;
        this.cursorPos = new gpos(0, 0);
        this._mousePos = new gpos(0, 0);
        this._lastMousePos = new gpos(0, 0);
        this._lastCursorPos = new gpos(0, 0);
        this._interactionMode = TwoDCanvasInteractionMode.NONE;
        this._interactionPossibility = TwoDCanvasInteractionMode.NONE;
        this._lockedInteractionMode = null;
        this._pasteBuffer = null; // stores an array of JSON entities
        this._interactionStartCursorPos = null;
        this._interactionEndCursorPos = null;
        this._ctrlKeyDown = false;
        var self = this;
        Machinata.debug("TwoDCanvas.constructor()");
        if (config == null)
            config = new TwoDCanvasConfig();
        this.config = config;
        this._previewCanvasElem = canvasElem;
        this._previewCanvas = canvasElem[0];
        this._isDrawing = false;
        this._redrawRequested = false;
        // Create preview renderer
        this._previewRenderer = new HTMLCanvasTwoDCanvasRenderer();
        this._previewRenderer.ctx = this._previewCanvas.getContext("2d");
        this._previewRenderer.viewport.units = this.config.units;
        this._previewRenderer.viewport.scale = this.config.scale;
        this._previewRenderer.viewport.zoom = this.config.zoom;
        this._previewCanvasElem.attr("tabindex", "1");
        this.backgroundEntity = new TwoDEntityRoot();
        this.backgroundEntity.setGroup(true);
        this.backgroundEntity.setPos(new gpos(0, 0));
        this.backgroundEntity.canvas = this;
        this.backgroundEntity._debugColor = "yellow";
        this.rootEntity = new TwoDEntityRoot();
        this.rootEntity.setGroup(true);
        this.rootEntity.setPos(new gpos(0, 0));
        this.rootEntity.canvas = this;
        this.rootEntity._debugColor = "red";
        // TEST scene
        //this._debugLoadTestScene();
        // Bindings
        this.bindUIEvents();
        // Initial draw
        this.resize();
        this.redraw();
    }
    TwoDCanvas.prototype.getEntityByGUID = function (guid) {
        var ret = this._guidToEntityMap[guid];
        return ret;
    };
    TwoDCanvas.prototype.addEntity = function (entity) {
        this.rootEntity.addChild(entity);
        return this;
    };
    TwoDCanvas.prototype.setDirty = function () {
        var self = this;
        this._isDirty = true;
        if (this.config.promptBeforeLeavingPageOnDirty == true && window.onbeforeunload == null) {
            window.onbeforeunload = function () {
                return "Are you sure you want to leave? Changes will not be saved.";
            };
        }
        return this;
    };
    TwoDCanvas.prototype.setClean = function () {
        this._isDirty = false;
        window.onbeforeunload = null;
        return this;
    };
    TwoDCanvas.prototype.mmToPx = function (mm) {
        return this._previewRenderer.viewport.mmToPx(mm);
    };
    TwoDCanvas.prototype._debugLoadTestScene = function () {
        new TwoDEntityImage()
            .setAutoSize(true)
            .setImage("/static/file/images/burri_digitalworks_logo.png")
            .setPos(new gpos(0, 0))
            .addToCanvas(this);
        new TwoDEntityImage()
            .setAutoSize(true)
            .setImage("/static/file/images/burri_logo.svg")
            .setPos(new gpos(1000, 1000))
            .addToCanvas(this);
        new TwoDEntityRuler()
            .setPos(new gpos(2000, 1000))
            .setSize(new gsize(1000, 1000 / 4))
            .addToCanvas(this);
        new TwoDEntityRuler()
            .setPos(new gpos(1000, 2000))
            .setSize(new gsize(10000, 1000 / 4))
            .addToCanvas(this);
        new TwoDEntityMeasuringTape()
            .setPos(new gpos(3000, 3000))
            .addToCanvas(this);
        return;
        var mainEntity = new TwoDEntityPlain().setPos(new gpos(-10, 10)).setSize(new gsize(20, 20));
        mainEntity._debugColor = "blue";
        mainEntity.setRot(Machinata.Math.degToRad(33.0));
        this.rootEntity.addChild(mainEntity);
        this.rootEntity.addChild(new TwoDEntityPlain().setPos(new gpos(40, 50)).setSize(new gsize(30, 10)));
        var grandChild = new TwoDEntityPlain().setPos(new gpos(40, 50)).setSize(new gsize(30, 10));
        mainEntity.addChild(grandChild);
        var greatGrandChild = new TwoDEntityPlain().setPos(new gpos(20, 10)).setSize(new gsize(5, 8)).setRot(Machinata.Math.degToRad(256.0));
        grandChild.addChild(greatGrandChild);
        var greatGreatGrandChild = new TwoDEntityPlain().setPos(new gpos(20, 10)).setSize(new gsize(5, 8)).setRot(Machinata.Math.degToRad(-33.0));
        greatGrandChild.addChild(greatGreatGrandChild);
        // Random entities
        for (var i = 0; i < 1000; i++) {
            var newEntity = new TwoDEntityPlain()
                .setPos(new gpos(Machinata.Math.rnd(-1000, 1000), Machinata.Math.rnd(-1000, 1000)))
                .setRot(Machinata.Math.degToRad(Machinata.Math.rnd(-360, 360)))
                .setSize(new gsize(Machinata.Math.rnd(10, 100), Machinata.Math.rnd(10, 100)));
            this.rootEntity.addChild(newEntity);
        }
    };
    TwoDCanvas.prototype._debugDrawPoint = function (pos) {
        this._previewRenderer.ctx.save();
        {
            this._previewRenderer.ctx.fillStyle = "pink";
            this._previewRenderer.ctx.fillRect(pos.x - 1, pos.y - 1, 2, 2);
        }
        this._previewRenderer.ctx.restore();
    };
    TwoDCanvas.prototype._updateInteractionState = function (event) {
        var self = this;
        if (event == null)
            event = {}; // event shim
        // If we are locked, dont update
        {
            // Move entity
            if (self._interactionMode == TwoDCanvasInteractionMode.DRAG) {
                if (self.entitySelection != null && self.entitySelection.canMove()) {
                    //self.selectedEntity.setWorldPos(self.cursorPos.add(self.selectedEntityGrabOffset));
                    self.entitySelection.moveTo(self.cursorPos, event.shiftKey == true);
                }
            }
            // Rotate entity
            if (self._interactionMode == TwoDCanvasInteractionMode.ROTATE) {
                if (self.entitySelection != null && self.entitySelection.canRotate()) {
                    //self.selectedEntity.pointTo(self.cursorPos);
                    self.entitySelection.pointTo(self.cursorPos, event.shiftKey == true);
                }
            }
            // Pan
            if (self._interactionMode == TwoDCanvasInteractionMode.PAN) {
                self._previewRenderer.viewport.pos.x += (self._mousePos.x - self._lastMousePos.x) / self._previewRenderer.viewport.drawScale();
                self._previewRenderer.viewport.pos.y += (self._mousePos.y - self._lastMousePos.y) / self._previewRenderer.viewport.drawScale();
            }
            if (self._interactionMode == TwoDCanvasInteractionMode.NONE && self._lockedInteractionMode == null) {
                // Update hovered entity
                self.hoveredEntity = self.getEntityAtPosition(self.cursorPos, true); // only selectable
                var newInteractionPossibility = TwoDCanvasInteractionMode.NONE;
                // Update possibilities
                if (self.hoveredEntity != null) {
                    if (self.hoveredEntity.isSelected() == false && self.hoveredEntity._canSelect == true) {
                        newInteractionPossibility = TwoDCanvasInteractionMode.SELECT;
                    }
                    else if (self.hoveredEntity._canMove == true) {
                        newInteractionPossibility = TwoDCanvasInteractionMode.DRAG;
                    }
                    else if (self.hoveredEntity._canClick == true) {
                        newInteractionPossibility = TwoDCanvasInteractionMode.CLICK;
                    }
                }
                // Test for handles
                if (self.entitySelection != null && self.entitySelection.canRotate()) {
                    //let rotateHandle = self.selectedEntity.rotateHandlePos();
                    var rotateHandle = self.entitySelection.rotateHandlePos();
                    if (rotateHandle.dist(self.cursorPos) < self.config.rotateHandleSize / self._previewRenderer.viewport.drawScale()) {
                        newInteractionPossibility = TwoDCanvasInteractionMode.ROTATE;
                    }
                }
                self.setInteractionPossibility(newInteractionPossibility);
            }
        }
    };
    TwoDCanvas.prototype.isScreencasting = function () {
        var SCREENCAST_FLAG = Machinata.queryParameter("screencast") == "true" ? true : false;
        return SCREENCAST_FLAG;
    };
    TwoDCanvas.prototype._updatePointer = function () {
        var CROSSHAIR = this.isScreencasting() ? "move" : "crosshair";
        if (this._lockedInteractionMode == TwoDCanvasInteractionMode.PAN && this._interactionMode == TwoDCanvasInteractionMode.PAN) {
            this._previewCanvasElem.css("cursor", "grabbing");
        }
        else if (this._lockedInteractionMode == TwoDCanvasInteractionMode.PAN) {
            this._previewCanvasElem.css("cursor", "grab");
        }
        else if (this._interactionMode == TwoDCanvasInteractionMode.DRAG || this._interactionPossibility == TwoDCanvasInteractionMode.DRAG) {
            this._previewCanvasElem.css("cursor", "move");
        }
        else if (this._interactionMode == TwoDCanvasInteractionMode.ROTATE || this._interactionPossibility == TwoDCanvasInteractionMode.ROTATE) {
            this._previewCanvasElem.css("cursor", "cell");
        }
        else if (this._interactionMode == TwoDCanvasInteractionMode.PAN || this._interactionPossibility == TwoDCanvasInteractionMode.PAN) {
            this._previewCanvasElem.css("cursor", "grabbing");
        }
        else if (this._interactionMode == TwoDCanvasInteractionMode.CLICK || this._interactionPossibility == TwoDCanvasInteractionMode.CLICK) {
            this._previewCanvasElem.css("cursor", "pointer");
        }
        else {
            this._previewCanvasElem.css("cursor", CROSSHAIR); // crosshair is not supported by chrome screencast
        }
    };
    ;
    TwoDCanvas.prototype._updateCursor = function (event) {
        if (event != null) {
            this._lastMousePos.x = this._mousePos.x;
            this._lastMousePos.y = this._mousePos.y;
            this._mousePos = new gpos(event.offsetX, event.offsetY);
        }
        this._lastCursorPos.x = this.cursorPos.x;
        this._lastCursorPos.y = this.cursorPos.y;
        this.cursorPos = new gpos((this._mousePos.x - this._previewRenderer.viewport.deviceSize.w / 2) / this._previewRenderer.viewport.drawScale() - this._previewRenderer.viewport.pos.x, (this._mousePos.y - this._previewRenderer.viewport.deviceSize.h / 2) / this._previewRenderer.viewport.drawScale() - this._previewRenderer.viewport.pos.y);
    };
    TwoDCanvas.prototype.createEntityForType = function (typeName) {
        var newEntity = null;
        try {
            newEntity = new window[typeName]();
        }
        catch (e) {
            throw "Could not create entity for type " + typeName + ": " + e;
        }
        return newEntity;
    };
    TwoDCanvas.prototype.bindUIEvents = function () {
        var self = this;
        // Redraw for responsiveness...
        $(window).resize(function () {
            self.resize();
            self.redraw();
        });
        // Right click bind
        $(this._previewCanvas).on('contextmenu', this._previewCanvas, function (e) { return false; });
        // Drag n drop interactions - drag over
        this._previewCanvas.addEventListener("dragover", function (event) {
            event.preventDefault(); // allow
            // Update cursor
            self._updateCursor(event);
        });
        // Drag n drop interactions - drop
        this._previewCanvas.addEventListener("drop", function (event) {
            event.preventDefault();
            // Image drop
            var imageData = event.dataTransfer.getData("image");
            if (imageData != null && imageData != "") {
                new TwoDEntityImage()
                    .setAutoSize(true)
                    .setImage(imageData)
                    .setPos(self.cursorPos)
                    .addToCanvas(self);
                if (self.onEntityDropped != null)
                    self.onEntityDropped(self, newEntity, event.dataTransfer);
            }
            // Type drop
            var typeData = event.dataTransfer.getData("type");
            if (typeData != null && typeData != "") {
                var newEntity = self.createEntityForType(typeData);
                if (newEntity != null) {
                    newEntity.setLabel(event.dataTransfer.getData("name"));
                    newEntity.setPos(self.cursorPos);
                    newEntity.addToCanvas(self);
                    if (self.onEntityDropped != null)
                        self.onEntityDropped(self, newEntity, event.dataTransfer);
                }
            }
        });
        // Mouse interactions - mouse down
        this._previewCanvas.addEventListener("mousedown", function (event) {
            //event.preventDefault(); // this will prevent focus, we need focus for key events
            event.stopPropagation();
            // Update cursor
            self._updateCursor(event);
            // If we are locked, dont update
            if (event.which == 1) {
                if (self._lockedInteractionMode == null) {
                    // Left mouse
                    if (self._interactionPossibility != TwoDCanvasInteractionMode.NONE) {
                        self.setInteractionMode(self._interactionPossibility);
                        if (self._interactionMode == TwoDCanvasInteractionMode.SELECT) {
                            if (event.shiftKey == true) {
                                self.selectEntity(self.hoveredEntity, true);
                            }
                            else {
                                self.selectEntity(self.hoveredEntity, false);
                            }
                            self.setInteractionMode(TwoDCanvasInteractionMode.DRAG);
                        }
                        else if (self._interactionMode == TwoDCanvasInteractionMode.DRAG) {
                            if (self.entitySelection != null) {
                                self.entitySelection.setGrabBeginInformation(self.cursorPos);
                            }
                        }
                        else if (self._interactionMode == TwoDCanvasInteractionMode.ROTATE) {
                            if (self.entitySelection != null) {
                                self.entitySelection.setGrabBeginInformation(self.cursorPos);
                            }
                        }
                    }
                    else {
                        // Deselect
                        self.selectEntity(null, false);
                    }
                }
                else {
                    self.setInteractionMode(self._lockedInteractionMode);
                }
            }
            else if (event.which == 2) {
                // Middle mouse
                self.setInteractionMode(TwoDCanvasInteractionMode.PAN);
            }
            else if (event.which == 3) {
                // Right mouse
                self.setInteractionMode(TwoDCanvasInteractionMode.PAN);
            }
            else {
                // Unknown
                self.setInteractionMode(TwoDCanvasInteractionMode.NONE);
            }
            self.redraw();
        });
        // Mouse interactions - mouse up
        this._previewCanvas.addEventListener("mouseup", function (event) {
            event.preventDefault();
            event.stopPropagation();
            // Update cursor
            self._updateCursor(event);
            // End drag?
            if (self._interactionMode == TwoDCanvasInteractionMode.DRAG) {
                if (self.entitySelection != null) {
                    self.entitySelection.snapToClosebySnapPoints();
                    //TODO: allow snapping of all?
                    //self.selectedEntities[0].snapToClosebySnapPoints();
                }
            }
            // End rotate?
            if (self._interactionMode == TwoDCanvasInteractionMode.ROTATE) {
                if (self.entitySelection != null) {
                    self.entitySelection.snapToClosebySnapPoints();
                    //TODO: allow snapping of all?
                    //self.selectedEntities[0].snapToClosebySnapPoints();
                }
            }
            // Click?
            if (self._interactionMode == TwoDCanvasInteractionMode.CLICK) {
                if (self.hoveredEntity != null) {
                    self.hoveredEntity.onClick();
                }
            }
            self.setInteractionMode(TwoDCanvasInteractionMode.NONE);
            self._updatePointer();
            self.redraw();
        });
        // Mouse interactions - mouse move
        this._previewCanvas.addEventListener("mousemove", function (event) {
            event.preventDefault();
            if (!self.isScreencasting())
                event.stopPropagation();
            // Update cursor
            self._updateCursor(event);
            self._updateInteractionState(event);
            self._updatePointer();
            self.redraw();
        }, false);
        // Mouse interactions - double click
        this._previewCanvas.addEventListener("dblclick", function (event) {
            if (self.config.allowDoubleClickForSelectAllInSnapGroup == true) {
                if (self.hoveredEntity != null && self.hoveredEntity.snapGroupId != null) {
                    self.selectAllInSnapGroup(self.hoveredEntity.snapGroupId);
                    self.selectEntity(self.hoveredEntity, true);
                    self.requestRedraw();
                }
            }
        });
        // Mouse interactions - scroll wheel
        this._previewCanvas.addEventListener("wheel", function (event) {
            event.preventDefault();
            event.stopPropagation();
            var factor = 1.1;
            if (event.deltaY > 0)
                self.zoomOut(factor, true);
            else if (event.deltaY < 0)
                self.zoomIn(factor, true);
        }, false);
        // Focus event
        this._previewCanvasElem.on("focus", function () {
        });
        // Keyboard interactions - key down
        window.addEventListener("keydown", function (event) {
            //console.log("keydown", event.key, self._ctrlKeyDown);
            // Track some key states
            if (event.key == "Control" || event.key == "Cmd")
                self._ctrlKeyDown = true;
            // Ensure we have focus for interactions
            if (!self._previewCanvasElem.is(":focus"))
                return;
            // Ctrl+C
            if (self._ctrlKeyDown == true && event.key == "c") {
                self.copySelection();
            }
            // Ctrl+V
            if (self._ctrlKeyDown == true && event.key == "v") {
                self.pasteSelection();
            }
            // Ctrl+S
            if (self._ctrlKeyDown == true && event.key == "s") {
                if (self.onRequestSave) {
                    event.preventDefault();
                    event.stopPropagation();
                    self.onRequestSave(self);
                }
            }
            // Ctrl+A
            if (self._ctrlKeyDown == true && event.key == "a") {
                event.preventDefault();
                event.stopPropagation();
                self.selectAll();
            }
            // Zoom out
            if (event.key == "-") {
                self.zoomOut(1.2, false);
            }
            else if (event.key == "=" || event.key == "+") {
                self.zoomIn(1.2, false);
            }
            // Pan
            if (event.key == "ArrowRight") {
                self.panRight(1.2);
            }
            else if (event.key == "ArrowLeft") {
                self.panLeft(1.2);
            }
            else if (event.key == "ArrowUp") {
                self.panUp(1.2);
            }
            else if (event.key == "ArrowDown") {
                self.panDown(1.2);
            }
        });
        // Keyboard interactions - key up
        window.addEventListener("keyup", function (event) {
            // console.log("keyup", event.key, self._ctrlKeyDown);
            // Track some key states
            if (event.key == "Control" || event.key == "Cmd")
                self._ctrlKeyDown = false;
            // Ensure we have focus for interactions
            if (!self._previewCanvasElem.is(":focus"))
                return;
            // Prevent event propogation - we handle it from here
            event.preventDefault();
            event.stopPropagation();
            // Delete
            if (event.key == "Delete" || event.key == "Backspace") {
                if (self.entitySelection != null) {
                    self.entitySelection.delete();
                    self.entitySelection = null;
                }
                self.redraw();
            }
            // External handler?
            if (self.onKeyPress) {
                self.onKeyPress(self, event.key, event);
            }
        });
    };
    TwoDCanvas.prototype.selectEntity = function (entity, addToCurrentSelection) {
        var self = this;
        Machinata.debug("selectEntity: " + entity);
        if (entity != null) {
            if (addToCurrentSelection == false || self.entitySelection == null) {
                self.entitySelection = new TwoDEntitySelection();
            }
            self.entitySelection.select(entity, self.cursorPos);
            //self.selectedEntity = entity;
            //self.selectedEntityGrabOffset = self.selectedEntity.worldPos().subtract(self.cursorPos);
        }
        else {
            //self.selectedEntity = entity;
            self.entitySelection = null;
        }
        if (self.onEntitySelected)
            self.onEntitySelected(self, entity);
    };
    TwoDCanvas.prototype.selectAll = function () {
        var self = this;
        self.entitySelection = new TwoDEntitySelection();
        for (var i = 0; i < self.rootEntity.children.length; i++) {
            self.entitySelection.entities.push(self.rootEntity.children[i]);
        }
        self.requestRedraw();
    };
    TwoDCanvas.prototype.selectAllInSnapGroup = function (snapGroupId) {
        var self = this;
        var entities = self.getAllEntitiesForSnapGroup(snapGroupId);
        self.entitySelection = new TwoDEntitySelection();
        self.entitySelection.entities = entities;
        self.requestRedraw();
    };
    TwoDCanvas.prototype.copySelection = function () {
        var self = this;
        if (self.entitySelection != null) {
            var json = self.entitySelection.saveAsJSON();
            this._pasteBuffer = json;
        }
        else {
            this._pasteBuffer = null;
        }
    };
    TwoDCanvas.prototype.pasteSelection = function () {
        if (this._pasteBuffer != null) {
            this.loadEntitiesFromJSON(this._pasteBuffer, true, true, true);
        }
    };
    TwoDCanvas.prototype._rebuildNewGUIDsForJSON = function (json, guidDictionary) {
        if (guidDictionary === void 0) { guidDictionary = null; }
        if (guidDictionary == null)
            guidDictionary = {};
        for (var i = 0; i < json.length; i++) {
            var jsonEntity = json[i];
            // Main entity guid
            {
                var guid = jsonEntity.guid;
                if (guid != null) {
                    var guidRebuilt = guidDictionary[guid];
                    if (guidRebuilt == null) {
                        guidRebuilt = Machinata.guid();
                        guidDictionary[guid] = guidRebuilt;
                    }
                    jsonEntity.guid = guidRebuilt;
                }
            }
            // Entity snapGroupId
            {
                var guid = jsonEntity.snapGroupId;
                if (guid != null) {
                    var guidRebuilt = guidDictionary[guid];
                    if (guidRebuilt == null) {
                        guidRebuilt = Machinata.guid();
                        guidDictionary[guid] = guidRebuilt;
                    }
                    jsonEntity.snapGroupId = guidRebuilt;
                }
            }
            // Recurse through children
            if (jsonEntity.children != null)
                this._rebuildNewGUIDsForJSON(jsonEntity.children, guidDictionary);
        }
        // We do this only once we have resolved all the entity guids!
        this._rebuildNewGUIDsForJSONSnapPoints(json, guidDictionary);
    };
    TwoDCanvas.prototype._rebuildNewGUIDsForJSONSnapPoints = function (json, guidDictionary) {
        if (guidDictionary === void 0) { guidDictionary = null; }
        if (guidDictionary == null)
            throw "guidDictionary is not initialized!";
        for (var i = 0; i < json.length; i++) {
            var jsonEntity = json[i];
            if (jsonEntity.snapPoints != null) {
                for (var spi = 0; spi < jsonEntity.snapPoints.length; spi++) {
                    var sp = jsonEntity.snapPoints[spi];
                    if (sp.entity_guid != null) {
                        var guidFromDictionary = guidDictionary[sp.entity_guid];
                        if (guidFromDictionary == null) {
                            //TODO?
                            console.warn("_rebuildNewGUIDsForJSONSnapPoints snapPoint guid is not in guidDictionary!");
                        }
                        else {
                            sp.entity_guid = guidFromDictionary;
                        }
                    }
                    if (sp.partner != null && sp.partner.entity_guid != null) {
                        var guidFromDictionary = guidDictionary[sp.partner.entity_guid];
                        if (guidFromDictionary == null) {
                            // In this case we are most likely importing only a partial group or single element
                            // We might not have all the elements of the snapoint group so we just set it to null
                            // and the snap point snap to its partner will just break
                            sp.partner.entity_guid = null;
                        }
                        else {
                            sp.partner.entity_guid = guidFromDictionary;
                        }
                    }
                }
            }
            // Recurse through children
            if (jsonEntity.children != null)
                this._rebuildNewGUIDsForJSONSnapPoints(jsonEntity.children, guidDictionary);
        }
    };
    TwoDCanvas.prototype.loadEntitiesFromJSON = function (json, rebuildNewGUIDs, placeInCenterOfViewport, selectNewEntities) {
        // Init
        var newEntities = [];
        var jsonToUse = json;
        // Rebuild the GUIDs (since we might be copy/pasting, all the entities must have new guids, but of course mapped to each other)
        if (rebuildNewGUIDs == true) {
            console.log(JSON.stringify(jsonToUse, null, '  '));
            jsonToUse = JSON.parse(JSON.stringify(jsonToUse)); // make sure we work on a clone
            this._rebuildNewGUIDsForJSON(jsonToUse);
            console.log(JSON.stringify(jsonToUse, null, '  '));
        }
        // Create the new entities...
        // Here we create them and then resolve their weak references in two steps
        for (var i = 0; i < jsonToUse.length; i++) {
            var entityJSON = jsonToUse[i];
            var entity = this.loadEntityFromJSON(entityJSON);
            newEntities.push(entity);
        }
        for (var i = 0; i < newEntities.length; i++) {
            var entity = newEntities[i];
            entity._resolveEntityWeakReferences();
        }
        // Place in center of viewport
        if (placeInCenterOfViewport == true) {
            if (newEntities.length > 0) {
                // Calculate center of new entities
                var centerOfEntities = new gpos(0, 0);
                for (var i = 0; i < newEntities.length; i++) {
                    var entity = newEntities[i];
                    centerOfEntities = centerOfEntities.add(entity._pos);
                }
                centerOfEntities = centerOfEntities.div(newEntities.length);
                //console.log("centerOfEntities", centerOfEntities);
                // Offset to viewport
                var viewportOffset = this._previewRenderer.viewport.pos.mul(-1);
                var offsetForEntities = viewportOffset.subtract(centerOfEntities);
                //console.log("viewportOffset", viewportOffset);
                //console.log("offsetForEntities", offsetForEntities);
                // Apply offset
                for (var i = 0; i < newEntities.length; i++) {
                    var entity = newEntities[i];
                    entity._pos = entity._pos.add(offsetForEntities);
                }
            }
        }
        // Select all the new entities
        if (selectNewEntities == true) {
            this.entitySelection = new TwoDEntitySelection();
            for (var i = 0; i < newEntities.length; i++) {
                var entity = newEntities[i];
                this.entitySelection.entities.push(entity);
            }
        }
        // Schedule redraw
        this.requestRedraw();
    };
    TwoDCanvas.prototype.loadEntityFromJSON = function (json) {
        var newEntity = null;
        try {
            newEntity = new window[json.typeName]();
        }
        catch (e) {
            throw "Could not create entity for type " + json.typeName + ": " + e;
        }
        newEntity.guid = json.guid; // we need this before addChild
        this.rootEntity._addChild(newEntity); // _addChild has no redraw
        newEntity.loadFromJSON(json);
        return newEntity;
    };
    TwoDCanvas.prototype.lockInteractionMode = function (mode) {
        this._lockedInteractionMode = mode;
        this.setInteractionMode(TwoDCanvasInteractionMode.NONE);
    };
    TwoDCanvas.prototype.unlockInteractionMode = function () {
        this._lockedInteractionMode = null;
        this.setInteractionMode(TwoDCanvasInteractionMode.NONE);
    };
    TwoDCanvas.prototype.setInteractionMode = function (mode) {
        if (this._interactionMode != mode) {
            Machinata.debug("setInteractionMode: new mode: " + mode);
            this._interactionMode = mode;
            if (mode != TwoDCanvasInteractionMode.NONE) {
                this._interactionStartCursorPos = this.cursorPos.clone();
            }
            else {
                this._interactionEndCursorPos = this.cursorPos.clone();
            }
        }
    };
    TwoDCanvas.prototype.setInteractionPossibility = function (mode) {
        if (this._interactionPossibility != mode) {
            Machinata.debug("setInteractionPossibility: new possibility: " + mode);
            this._interactionPossibility = mode;
        }
    };
    TwoDCanvas.prototype.pan = function (x, y, factor) {
        this.setPosition(this._previewRenderer.viewport.pos.add(new gpos(x * factor, y * factor)));
    };
    TwoDCanvas.prototype.panLeft = function (factor) {
        this.pan(+this.config.defaultPanAmount, 0, factor);
    };
    TwoDCanvas.prototype.panRight = function (factor) {
        this.pan(-this.config.defaultPanAmount, 0, factor);
    };
    TwoDCanvas.prototype.panUp = function (factor) {
        this.pan(0, +this.config.defaultPanAmount, factor);
    };
    TwoDCanvas.prototype.panDown = function (factor) {
        this.pan(0, -this.config.defaultPanAmount, factor);
    };
    TwoDCanvas.prototype.zoomIn = function (factor, fromCursor) {
        this.setZoom(this._previewRenderer.viewport.zoom * factor, fromCursor);
    };
    TwoDCanvas.prototype.zoomOut = function (factor, fromCursor) {
        this.setZoom(this._previewRenderer.viewport.zoom / factor, fromCursor);
    };
    TwoDCanvas.prototype.setPosition = function (newPosition) {
        this._previewRenderer.viewport.pos = newPosition;
        this._updateCursor(null);
        this.requestRedraw();
    };
    TwoDCanvas.prototype.setZoom = function (newZoom, fromCursor) {
        if (newZoom > this.maxZoom)
            newZoom = this.maxZoom;
        if (newZoom < this.minZoom)
            newZoom = this.minZoom;
        var lastZoom = this._previewRenderer.viewport.zoom;
        var focalPos = fromCursor ? this.cursorPos : new gpos(0, 0);
        var mouseOffset = fromCursor ? this._mousePos : new gpos(0, 0);
        // Match cursor and canvas coord before and after zoom
        // Consider that the same page coordinates need to match the same canvas coordinates 
        // before and after the zoom.Then you can do some algebra starting from this equation:
        //   (pageCoords - translation) / scale = canvasCoords
        if (fromCursor) {
            var newZoomScaled = this._previewRenderer.viewport.drawScaleForZoom(newZoom);
            this._previewRenderer.viewport.pos = new gpos(-this.cursorPos.x + (this._mousePos.x - this._previewRenderer.viewport.deviceSize.w / 2) / newZoomScaled, -this.cursorPos.y + (this._mousePos.y - this._previewRenderer.viewport.deviceSize.h / 2) / newZoomScaled);
        }
        this._previewRenderer.viewport.setZoom(newZoom);
        this._updateCursor(null);
        this._updatePointer();
        this.redraw();
    };
    TwoDCanvas.prototype.zoomFit = function (factor) {
        var bounds = this.rootEntity.visualWorldBounds();
        // Set to center
        this._previewRenderer.viewport.pos = new gpos(-bounds.center.x, -bounds.center.y);
        // Get zoom to fit
        var scaleFactor = this._previewRenderer.viewport.drawScaleForZoom(1.0); // We need this factor since our actual draw scale is different from the zoom
        var newZoomX = (this._previewRenderer.viewport.deviceSize.w / scaleFactor) / bounds.size.w;
        var newZoomY = (this._previewRenderer.viewport.deviceSize.h / scaleFactor) / bounds.size.h;
        var newZoom = Math.min(newZoomX, newZoomY);
        this.setZoom(newZoom - (newZoom * 0.01), false);
        this._updateCursor(null);
        this._updatePointer();
        this.redraw();
    };
    TwoDCanvas.prototype.resize = function () {
        this._previewRenderer.viewport.deviceSize = new gsize(this._previewCanvas.width, this._previewCanvas.height);
        this._previewRenderer.viewport.viewSize = this._previewRenderer.viewport.deviceSize.mul(1.0 / this._previewRenderer.viewport.drawScale());
    };
    TwoDCanvas.prototype.getEntityAtPosition = function (pos, onlyInteractable) {
        if (onlyInteractable === void 0) { onlyInteractable = false; }
        if (this.rootEntity.worldBounds().containsPoint(pos)) {
            var child = this.rootEntity.getChildAtPoint(pos, onlyInteractable);
            if (child != null)
                return child;
            else
                return null;
        }
        return null;
    };
    TwoDCanvas.prototype.redraw = function () {
        Machinata.debug("TwoDCanvas.redraw()");
        // TESTING
        //if (this.selectedEntity) this.selectedEntity.setRot(this.selectedEntity._rot += 0.01);
        // Call render pipeline
        // First we mark as being drawn, and if someone has requested a redraw we set that to false since we are just about to do that...
        this._isDrawing = true;
        //if (this._redrawRequested == true) this._redrawRequested = false;
        this._renderWithRenderer(this._previewRenderer);
        this._isDrawing = false;
        // Did for some reason a side-effect cause another redraw?
        if (this._redrawRequested == true) {
            this.redraw();
        }
    };
    TwoDCanvas.prototype.requestRedraw = function () {
        if (this._redrawRequested == true)
            return;
        if (this._isDrawing == false) {
            this.redraw();
        }
        else {
            this._redrawRequested = true;
        }
    };
    TwoDCanvas.prototype._renderWithRenderer = function (renderer) {
        Machinata.debug("TwoDCanvas._drawWithRenderer()");
        // Prepare renderer
        renderer.prepareForDrawing();
        renderer.debug = this.debug;
        // Init
        var ctx = renderer.ctx;
        // Before we touch anything, we save the context so that at the very end we can reset it again
        ctx.save(); //TODO: why does the stack not reset on getcontext?
        // Clear bg
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, renderer.viewport.deviceSize.w, renderer.viewport.deviceSize.h);
        // Translate to center
        ctx.translate(Math.round(renderer.viewport.deviceSize.w / 2), Math.round(renderer.viewport.deviceSize.h / 2));
        // Background layer
        if (this.backgroundEntity != null && this.backgroundEntity.children.length > 0) {
            // Viewport transform
            ctx.save();
            {
                // Zoom
                ctx.scale(renderer.viewport.drawScale(), renderer.viewport.drawScale());
                // Translate (pos)
                ctx.translate(renderer.viewport.pos.x, renderer.viewport.pos.y);
                // Begin redraw chain
                ctx.save();
                {
                    this.backgroundEntity._drawBG(renderer);
                    this.backgroundEntity._drawEntity(renderer);
                    this.backgroundEntity._drawFG(renderer);
                }
                ctx.restore();
            }
            ctx.restore(); // end viewport transform
        }
        // Draw crosshair viewport
        if (this.debug == true) {
            var crossHairSize = 20.0;
            ctx.fillStyle = "black";
            ctx.fillRect(0, -crossHairSize / 2, 1, crossHairSize);
            ctx.fillRect(-crossHairSize / 2, 0, crossHairSize, 1);
        }
        // Draw grid
        if (renderer.viewport.gridEnabled) {
            // To ensure pixel-perfect grid lines, we draw the grid before the view transformation...
            ctx.strokeStyle = this.config.gridLineColor;
            ctx.lineWidth = this.config.gridLineSize;
            var gridSize = renderer.viewport.gridSize;
            var pixelOffset = 0.5;
            //if (renderer.viewport.zoom > 5) gridSize = gridSize / 10; //TODO: dynamic grid size multiples of 10
            ctx.save();
            {
                // Move according to viewport (x only)
                var vptransx = renderer.viewport.pos.x * renderer.viewport.drawScale();
                var vptransy = renderer.viewport.pos.y * renderer.viewport.drawScale();
                //ctx.translate(vptransx, 0);
                var vpxo = (Math.floor(-(renderer.viewport.pos.x + renderer.viewport.viewSize.w / 2) / gridSize) + 1) * gridSize;
                ctx.beginPath();
                for (var xview = 0 + vpxo; xview <= renderer.viewport.viewSize.w + vpxo; xview += gridSize) {
                    var xdevice = Math.round(vptransx + xview * renderer.viewport.drawScale());
                    ctx.moveTo(xdevice + pixelOffset, -renderer.viewport.deviceSize.h / 2);
                    ctx.lineTo(xdevice + pixelOffset, renderer.viewport.deviceSize.h / 2);
                }
                ctx.closePath();
                ctx.stroke();
                // Undo previous move (x), and move according to viewport (y only)
                //ctx.translate(-vptransx, vptransy);
                //ctx.translate(0, vptransy);
                var vpyo = (Math.floor(-(renderer.viewport.pos.y + renderer.viewport.viewSize.h / 2) / gridSize) + 1) * gridSize;
                ctx.beginPath();
                for (var yview = 0 + vpyo; yview <= renderer.viewport.viewSize.h + vpyo; yview += gridSize) {
                    var ydevice = Math.round(vptransy + yview * renderer.viewport.drawScale());
                    ctx.moveTo(-renderer.viewport.deviceSize.w / 2, ydevice + pixelOffset);
                    ctx.lineTo(renderer.viewport.deviceSize.w / 2, ydevice + pixelOffset);
                }
                ctx.closePath();
                ctx.stroke();
            }
            ctx.restore();
        }
        // Viewport transform
        ctx.save();
        {
            // Zoom
            ctx.scale(renderer.viewport.drawScale(), renderer.viewport.drawScale());
            // Translate (pos)
            ctx.translate(renderer.viewport.pos.x, renderer.viewport.pos.y);
            // Draw crosshair origin
            if (this.debug == true) {
                var crossHairSize = 20.0 / renderer.viewport.drawScale();
                var crossHairWidth = 1.0 / renderer.viewport.drawScale();
                ctx.lineWidth = 1.0 / renderer.viewport.drawScale();
                ctx.fillStyle = "blue";
                ctx.fillRect(0, -crossHairSize / 2, crossHairWidth, crossHairSize);
                ctx.fillRect(-crossHairSize / 2, 0, crossHairSize, crossHairWidth);
            }
            // Draw cursor
            if (this.debug == true) {
                var crossHairSize = 20.0 / renderer.viewport.drawScale();
                var crossHairWidth = 1.0 / renderer.viewport.drawScale();
                ctx.save();
                {
                    ctx.translate(this.cursorPos.x, this.cursorPos.y);
                    ctx.fillStyle = "red";
                    ctx.fillRect(0, -crossHairSize / 2, crossHairWidth, crossHairSize);
                    ctx.fillRect(-crossHairSize / 2, 0, crossHairSize, crossHairWidth);
                }
                ctx.restore();
            }
            // Draw viewport bounds
            if (this.debug == true) {
                ctx.save();
                {
                    var bounds = renderer.viewport.worldBounds();
                    bounds.size.w = bounds.size.w - 10.0 / renderer.viewport.drawScale();
                    bounds.size.h = bounds.size.h - 10.0 / renderer.viewport.drawScale();
                    ctx.lineWidth = 2.0 / renderer.viewport.drawScale();
                    ctx.fillStyle = "black";
                    ctx.strokeRect(bounds.center.x - bounds.size.w / 2, bounds.center.y - bounds.size.h / 2, bounds.size.w, bounds.size.h);
                }
                ctx.restore();
            }
            // Begin redraw chain
            ctx.save();
            {
                this.rootEntity._drawBG(renderer);
                this.rootEntity._drawEntity(renderer);
                this.rootEntity._drawFG(renderer);
            }
            ctx.restore();
            if (renderer.drawUI == true) {
                this.drawUI(renderer);
            }
            if (renderer.drawMeasurements == true) {
                this.drawMeasurements(renderer);
            }
        }
        ctx.restore(); // end viewport transform
        // Draw scale?
        if (renderer.viewport.scaleEnabled) {
            ctx.save();
            {
                // Get dimensions - first attempt
                var scaleBlocksToShow = 10; // 10 meters
                var mmPerBlock = 1000; // 1 meter
                var blockW = mmPerBlock * renderer.viewport.drawScale();
                var blockH = 10; // in pixels
                // Validate
                if (blockW * scaleBlocksToShow > renderer.viewport.deviceSize.w) {
                    // Get dimensions - second attempt
                    scaleBlocksToShow = 10; // 1 meter
                    mmPerBlock = 100; // 100 cm
                    blockW = mmPerBlock * renderer.viewport.drawScale();
                }
                // Perform draw
                ctx.translate(-renderer.viewport.deviceSize.w / 2, +renderer.viewport.deviceSize.h / 2);
                ctx.translate(blockH, -blockH * 2);
                for (var i = 0; i < scaleBlocksToShow; i++) {
                    var blockX = (i * blockW);
                    var blockY = 0;
                    ctx.fillStyle = i % 2 == 0 ? "black" : "white";
                    ctx.fillRect(blockX, blockY, blockW, blockH);
                }
                // Block label
                ctx.font = renderer.viewport.labelFontSize + "px " + renderer.viewport.labelFontName;
                ctx.textAlign = "left";
                ctx.fillStyle = "black";
                ctx.fillText(TwoDCanvas.getMetricUnitLabel(mmPerBlock), 0, -blockH / 2);
                ctx.textAlign = "right";
                ctx.fillText(TwoDCanvas.getMetricUnitLabel((mmPerBlock * scaleBlocksToShow)), blockW * scaleBlocksToShow, -blockH / 2);
                ctx.textAlign = "center";
                //ctx.fillText((renderer.viewport.physicalScale * 100) + ":100 @ " + renderer.viewport.dpi + "dpi", blockW * scaleBlocksToShow / 2, -blockH / 2);
                if (false) {
                    var viewportScaleB = 100;
                    var viewportScaleA = (renderer.viewport.scale * viewportScaleB) * renderer.viewport.zoom;
                    viewportScaleB = viewportScaleB / viewportScaleA;
                    viewportScaleA = (renderer.viewport.scale * viewportScaleB) * renderer.viewport.zoom;
                    viewportScaleA = Math.round(viewportScaleA);
                    viewportScaleB = Math.round(viewportScaleB);
                    ctx.fillText(viewportScaleA + ":" + viewportScaleB, blockW * scaleBlocksToShow / 2, -blockH / 2);
                }
                // Border
                ctx.lineWidth = 1.0;
                ctx.strokeStyle = "black";
                ctx.strokeRect(0, 0, blockW * scaleBlocksToShow, blockH);
            }
            ctx.restore();
        }
        // Final restore
        ctx.restore();
        // Stats
        renderer.statsEndDrawTime = new Date();
        renderer.statsTotalDrawTimeMS = renderer.statsEndDrawTime.getTime() - renderer.statsStartDrawTime.getTime();
        renderer.statsTotalDrawTimeFPS = Math.min(9999, 1000 / renderer.statsTotalDrawTimeMS);
        renderer.statsTotalDrawTimeFPSMin = renderer.statsTotalDrawTimeFPSMin == null ? renderer.statsTotalDrawTimeFPS : Math.min(renderer.statsTotalDrawTimeFPS, renderer.statsTotalDrawTimeFPSMin);
        renderer.statsTotalDrawTimeFPSMax = renderer.statsTotalDrawTimeFPSMax == null ? renderer.statsTotalDrawTimeFPS : Math.max(renderer.statsTotalDrawTimeFPS, renderer.statsTotalDrawTimeFPSMax);
        renderer.statsTotalDrawTimeFPSAverage = renderer.statsTotalDrawTimeFPSAverage == null ? renderer.statsTotalDrawTimeFPS : (renderer.statsTotalDrawTimeFPS + renderer.statsTotalDrawTimeFPSAverage * 100) / (1 + 100);
        if (renderer.debug) {
            console.log("  rendered in " + renderer.statsTotalDrawTimeMS + " ms (" + renderer.statsTotalDrawTimeFPS + " fps)");
        }
        // Debug overlay
        if (renderer.debug) {
            ctx.save();
            {
                var text = "INFO";
                text += "\n" + "cursor: " + this.cursorPos.x + " " + this.cursorPos.y;
                text += "\n" + "zoom: " + renderer.viewport.zoom;
                text += "\n" + "draw scale: " + renderer.viewport.drawScale();
                text += "\n" + "vp-tl: " + (renderer.viewport.pos.x - renderer.viewport.viewSize.w / 2) + " " + (renderer.viewport.pos.y - renderer.viewport.viewSize.h / 2);
                text += "\n" + "vp-tr: " + (renderer.viewport.pos.x + renderer.viewport.viewSize.w / 2) + " " + (renderer.viewport.pos.y - renderer.viewport.viewSize.h / 2);
                text += "\n" + "vp-bl: " + (renderer.viewport.pos.x - renderer.viewport.viewSize.w / 2) + " " + (renderer.viewport.pos.y + renderer.viewport.viewSize.h / 2);
                text += "\n" + "vp-br: " + (renderer.viewport.pos.x + renderer.viewport.viewSize.w / 2) + " " + (renderer.viewport.pos.y + renderer.viewport.viewSize.h / 2);
                text += "\n" + "i-mode: " + this._interactionMode;
                text += "\n" + "i-pos: " + this._interactionPossibility;
                text += "\n" + "selected: " + (this.entitySelection == null ? "" : this.entitySelection.toString());
                text += "\n" + "hover: " + (this.hoveredEntity == null ? "" : this.hoveredEntity.toString());
                text += "\n" + "";
                text += "\n" + "STATS";
                text += "\n" + "ms: " + renderer.statsTotalDrawTimeMS;
                text += "\n" + "fps: " + Math.round(renderer.statsTotalDrawTimeFPS);
                text += "\n" + "fps-min: " + Math.round(renderer.statsTotalDrawTimeFPSMin);
                text += "\n" + "fps-max: " + Math.round(renderer.statsTotalDrawTimeFPSMax);
                text += "\n" + "fps-avg: " + Math.round(renderer.statsTotalDrawTimeFPSAverage);
                text += "\n" + "draws: " + renderer.statsTotalDraws;
                ctx.textBaseline = "top";
                ctx.fillStyle = "black";
                ctx.font = "10px Helvetica";
                ctx.fillMultilineText(text, 10, 10);
            }
            ctx.restore();
        }
    };
    TwoDCanvas.prototype.drawUI = function (renderer) {
        var ctx = renderer.ctx;
        ctx.save();
        {
            this.rootEntity._drawUI(renderer);
            if (this.entitySelection != null && this.entitySelection.canRotate()) {
                var origin = this.entitySelection.worldPos();
                var handlePos = this.entitySelection.rotateHandlePos();
                if (this._interactionMode == TwoDCanvasInteractionMode.ROTATE) {
                    handlePos = this.cursorPos;
                }
                var handleSize = this.config.rotateHandleSize / renderer.viewport.drawScale();
                //if (handlePos.dist(this.canvas.cursorPos) < (this.canvas.config.rotateHandleSize * 1.5) / 2.0) handleSize = this.canvas.config.rotateHandleSize * 1.5 / renderer.viewport.zoom;
                ctx.save();
                {
                    // Line
                    ctx.beginPath();
                    ctx.moveTo(origin.x, origin.y);
                    ctx.lineTo(handlePos.x, handlePos.y);
                    ctx.lineWidth = this.config.rotateHandleLineSize / renderer.viewport.drawScale();
                    ctx.strokeStyle = this.config.rotateHandleColor;
                    ctx.closePath();
                    ctx.stroke();
                    // Dot
                    ctx.translate(handlePos.x, handlePos.y);
                    ctx.fillStyle = this.config.rotateHandleColor;
                    ctx.beginPath();
                    ctx.arc(0, 0, handleSize, 0, 2 * Math.PI);
                    ctx.closePath();
                    ctx.fill();
                }
                ctx.restore();
            }
        }
        ctx.restore();
    };
    TwoDCanvas.prototype.drawMeasurements = function (renderer) {
    };
    TwoDCanvas.prototype.getFullSceneBounds = function () {
        // Old method: only take into account the scene items (no background)
        //return this.rootEntity.visualWorldBounds().growBy(this.config.exportFitToBoundsPadding, this.config.exportFitToBoundsPadding);
        // New method: also take extra entities (such as background) into account
        var bounds = this.rootEntity.visualWorldBounds();
        if (this.backgroundEntity != null)
            bounds = bounds.extend(this.backgroundEntity.visualWorldBounds());
        return bounds;
    };
    TwoDCanvas.prototype.getExportBounds = function () {
        var bounds = this.getFullSceneBounds();
        bounds = bounds.growBy(this.config.exportFitToBoundsPadding, this.config.exportFitToBoundsPadding);
        return bounds;
    };
    TwoDCanvas.prototype.exportPDF = function (opts) {
        if (opts == null)
            opts = {};
        if (opts.size == null)
            opts.size = "A4";
        if (opts.orientation == null)
            opts.orientation = "landscape";
        if (opts.dpi == null)
            opts.dpi = 96;
        if (opts.filename == null)
            opts.filename = "export.pdf";
        // Page options
        var pdfMarginMM = 10; // mm
        // Create a HTMLCanvas renderer
        var renderer = new PDFCanvasTwoDCanvasRenderer();
        renderer.drawLabels = this._previewRenderer.drawLabels;
        renderer.drawMeasurements = this._previewRenderer.drawMeasurements;
        renderer.drawUI = false;
        renderer.isExport = true;
        renderer.supportsTransformSetAndReset = false; // This is impossible on a PDF...
        renderer.viewport.units = this.config.units;
        renderer.viewport.dpi = opts.dpi; //TODO
        // Get pdf points width/hight
        // Note we can't convert to mm automatically because of rounding errors
        // A3 = 297 x 420 mm
        // A4 = 210 x 297 mm
        //var pdfWidthPDFPoints = Math.round(renderer.viewport.mmToPDFPoints(pdfWidthMM)); // pdf points
        //var pdfHeightPDFPoints = Math.round(renderer.viewport.mmToPDFPoints(pdfHeightMM)); // pdf points
        var pdfWidthPDFPoints = 1190; //TODO: a3 hardcoded (seems they must match perfectly, must be rounded)
        var pdfHeightPDFPoints = 842;
        //alert(renderer.viewport.mmToPDFPoints(297));
        //alert(renderer.viewport.mmToPDFPoints(420));
        //alert(renderer.viewport.mmToPDFPoints(210));
        //alert(renderer.viewport.mmToPDFPoints(297));
        if (opts.size == "A3" && opts.orientation == "landscape") {
            // A3 landscape
            pdfWidthPDFPoints = 1190;
            pdfHeightPDFPoints = 842;
        }
        else if (opts.size == "A3" && opts.orientation == "portrait") {
            // A3 portrait
            pdfWidthPDFPoints = 842;
            pdfHeightPDFPoints = 1190;
        }
        else if (opts.size == "A4" && opts.orientation == "landscape") {
            // A4 landscape
            pdfWidthPDFPoints = 842;
            pdfHeightPDFPoints = 595;
        }
        else if (opts.size == "A4" && opts.orientation == "portrait") {
            // A4 portrait
            pdfWidthPDFPoints = 595;
            pdfHeightPDFPoints = 842;
        }
        renderer.viewport.deviceSize.w = pdfWidthPDFPoints; // Seems this must just match the pdf points
        renderer.viewport.deviceSize.h = pdfHeightPDFPoints;
        renderer.viewport.labelFontName = "Helvetica"; //TODO: only this works
        renderer.viewport.fitBounds(this.getExportBounds());
        // PDF options
        // See https://pdfkit.org/docs/getting_started.html#creating_a_document
        // Note PDF options units are PDF points (there are 72 per inch).
        var pdfOptions = {
            info: {
                "Title": this.config.exportDocumentTitle || "",
                "Author": this.config.exportDocumentAuthor || "",
                "Subject": this.config.exportDocumentSubject || "",
            },
            size: [pdfWidthPDFPoints, pdfHeightPDFPoints],
            //layout: 'landscape', // can be 'landscape'/'portrait'. Note: this will flip the document...
            margins: {
                top: Math.round(renderer.viewport.mmToPDFPoints(pdfMarginMM)),
                bottom: Math.round(renderer.viewport.mmToPDFPoints(pdfMarginMM)),
                left: Math.round(renderer.viewport.mmToPDFPoints(pdfMarginMM)),
                right: Math.round(renderer.viewport.mmToPDFPoints(pdfMarginMM))
            }
        };
        var ctx = renderer.createDrawingContext(pdfOptions);
        console.log("pdfOptions", pdfOptions);
        console.log("deviceSize", renderer.viewport.deviceSize);
        // Call render pipeline
        this._renderWithRenderer(renderer);
        // Setup a download for PDF
        var mimetype = "application/pdf";
        //convert your PDF to a Blob and save to file
        ctx.stream.on('finish', function () {
            var blob = ctx.stream.toBlob('application/pdf');
            Machinata.Data.exportBlob(blob, mimetype, opts.filename);
        });
        ctx.end();
    };
    TwoDCanvas.prototype.exportPNG = function (opts) {
        if (opts == null)
            opts = {};
        if (opts.size == null)
            opts.size = "A4";
        if (opts.orientation == null)
            opts.orientation = "landscape";
        if (opts.dpi == null)
            opts.dpi = 96;
        if (opts.filename == null)
            opts.filename = "export.png";
        // Create a HTMLCanvas renderer
        var renderer = new HTMLCanvasTwoDCanvasRenderer();
        renderer.drawLabels = this._previewRenderer.drawLabels;
        renderer.drawMeasurements = this._previewRenderer.drawMeasurements;
        renderer.drawUI = false;
        renderer.isExport = true;
        renderer.viewport.units = this.config.units;
        renderer.viewport.dpi = opts.dpi;
        // Get PNG pixel width/hight
        // A3 = 297 x 420 mm
        // A4 = 210 x 297 mm
        var pngWidthPixels = 800; // fallback
        var pngHeightPixels = 800; // fallback
        function mmToPx(mm) {
            var inches = mm / 25.4;
            var pixels = inches * opts.dpi;
            return Math.round(pixels);
        }
        ;
        if (opts.size == "A3" && opts.orientation == "landscape") {
            // A3 landscape
            pngWidthPixels = mmToPx(420);
            pngHeightPixels = mmToPx(297);
        }
        else if (opts.size == "A3" && opts.orientation == "portrait") {
            // A3 portrait
            pngWidthPixels = mmToPx(297);
            pngHeightPixels = mmToPx(420);
        }
        else if (opts.size == "A4" && opts.orientation == "landscape") {
            // A4 landscape
            pngWidthPixels = mmToPx(297);
            pngHeightPixels = mmToPx(210);
        }
        else if (opts.size == "A4" && opts.orientation == "portrait") {
            // A4 portrait
            pngWidthPixels = mmToPx(210);
            pngHeightPixels = mmToPx(297);
        }
        renderer.viewport.deviceSize.w = pngWidthPixels;
        renderer.viewport.deviceSize.h = pngHeightPixels;
        renderer.viewport.fitBounds(this.getExportBounds());
        renderer.createDrawingContext({});
        // Call render pipeline
        this._renderWithRenderer(renderer);
        // Setup a download for PNG
        var mimetype = "image/png";
        renderer.canvas.toBlob(function (blob) {
            Machinata.Data.exportBlob(blob, mimetype, opts.filename);
        }, mimetype);
    };
    TwoDCanvas.prototype.getSnapshotForBoundsAsBase64 = function (bounds, width, height, drawUI, drawLabels, drawMeasurements) {
        // Create a HTMLCanvas renderer
        var renderer = new HTMLCanvasTwoDCanvasRenderer();
        renderer.drawUI = drawUI;
        renderer.drawLabels = drawLabels;
        renderer.drawMeasurements = drawMeasurements;
        renderer.isExport = true;
        renderer.viewport.units = this.config.units;
        // Setup renderer
        renderer.viewport.deviceSize.w = width;
        renderer.viewport.deviceSize.h = height;
        renderer.viewport.fitBounds(bounds.growBy(this.config.snapshotFitToBoundsPadding, this.config.snapshotFitToBoundsPadding));
        renderer.createDrawingContext({});
        // Call render pipeline
        this._renderWithRenderer(renderer);
        // Get image data as base64
        var imageData = renderer.canvas.toDataURL("jpg", 0.9);
        return imageData;
    };
    TwoDCanvas.measureText = function (text, font) {
        if (TwoDCanvas._measureTextContext == null) {
            var canvas = document.createElement('canvas');
            canvas.width = 1;
            canvas.height = 1;
            TwoDCanvas._measureTextContext = canvas.getContext('2d');
        }
        TwoDCanvas._measureTextContext.font = font;
        return TwoDCanvas._measureTextContext.measureText(text);
    };
    TwoDCanvas.getMetricUnitLabel = function (mm, precision) {
        if (precision === void 0) { precision = null; }
        function applyPrecision(val) {
            if (precision == null)
                return val;
            return val.toFixed(precision);
        }
        // Convert to m
        var m = mm / 1000;
        if (m < 1.0) {
            var cm = mm / 10;
            if (cm < 1.0) {
                return applyPrecision(mm) + "mm";
            }
            return applyPrecision(cm) + "cm";
        }
        return applyPrecision(m) + "m";
    };
    TwoDCanvas.prototype.clearChildren = function () {
        var childrenToDelete = [];
        for (var i = 0; i < this.rootEntity.children.length; i++) {
            childrenToDelete.push(this.rootEntity.children[i]);
        }
        for (var i = 0; i < childrenToDelete.length; i++) {
            childrenToDelete[i].delete();
        }
    };
    TwoDCanvas.prototype.visitAllEntities = function (visit) {
        function visitChildren(entity, depth) {
            var ret = visit(entity, depth);
            if (ret == false)
                return;
            for (var i = 0; i < entity.children.length; i++) {
                var child = entity.children[i];
                visitChildren(child, depth + 1);
            }
        }
        visitChildren(this.rootEntity, 1);
    };
    TwoDCanvas.prototype.saveAsJSON = function () {
        var json = {};
        // Build info
        //TODO
        json.buildInfo = Machinata.buildInfo();
        // Canvas properties
        json.viewport = {};
        json.viewport.pos = this._previewRenderer.viewport.pos.saveAsJSON();
        json.viewport.zoom = this._previewRenderer.viewport.zoom;
        // Custom data
        json.data = this.data;
        // Entities
        json.backgroundEntity = this.backgroundEntity.saveAsJSON();
        json.rootEntity = this.rootEntity.saveAsJSON();
        return json;
    };
    TwoDCanvas.prototype.loadFromJSON = function (json) {
        try {
            // Canvas properties
            try {
                // Viewport info
                if (json.viewport != null) {
                    if (json.viewport.pos != null)
                        this._previewRenderer.viewport.pos = gpos.loadFromJSON(json.viewport.pos);
                    if (json.viewport.zoom != null)
                        this._previewRenderer.viewport.zoom = json.viewport.zoom;
                }
                // Custom data
                this.data = json.data;
            }
            catch (error) {
                console.log(error);
                throw "load canvas properties: " + error;
            }
            // Entities
            try {
                if (json.backgroundEntity != null) {
                    this.backgroundEntity.loadFromJSON(json.backgroundEntity);
                }
            }
            catch (error) {
                console.log(error);
                throw "backgroundEntity.loadFromJSON(): " + error;
            }
            try {
                this.rootEntity.loadFromJSON(json.rootEntity);
            }
            catch (error) {
                console.log(error);
                throw "rootEntity.loadFromJSON(): " + error;
            }
            try {
                this.rootEntity._resolveEntityWeakReferences();
            }
            catch (error) {
                console.log(error);
                throw "rootEntity._resolveEntityWeakReferences(): " + error;
            }
            // Make sure we mark as clean
            this.setClean();
            // Draw
            try {
                //this.requestRedraw();
                this.setZoom(this._previewRenderer.viewport.zoom, false); // will redraw
            }
            catch (error) {
                console.log(error);
                throw "TwoDCanvas.requestRedraw(): initial draw after load: " + error;
            }
        }
        catch (error) {
            console.log(error);
            throw "TwoDCanvas.loadFromJSON(): " + error;
        }
    };
    TwoDCanvas.prototype.getPreviewThumbnailAsImageData = function () {
        var imageData = this._previewRenderer.ctx.getImageData(0, 0, this._previewCanvas.width, this._previewCanvas.height);
        return imageData;
    };
    TwoDCanvas.prototype.getPreviewThumbnailAsBase64 = function () {
        var imageData = this._previewCanvas.toDataURL("jpg", 0.9);
        return imageData;
    };
    TwoDCanvas.prototype.getAllSnapGroups = function (includedNonGrouped) {
        var ret = {};
        this.visitAllEntities(function (entity) {
            var snapGroupId = entity.snapGroupId;
            if (snapGroupId == null && includedNonGrouped == true) {
                snapGroupId = entity.guid; // if no group, we use our own guid (we are a solo element)
            }
            if (snapGroupId != null) {
                if (ret[snapGroupId] == null) {
                    ret[snapGroupId] = [entity];
                }
                else {
                    ret[snapGroupId].push(entity);
                }
            }
        });
        return ret;
    };
    TwoDCanvas.prototype.getAllEntitiesForSnapGroup = function (snapGroupId) {
        var ret = [];
        //TODO: is there a faster way to get these? Chaining via snap points?
        this.visitAllEntities(function (entity) {
            if (entity.snapGroupId == snapGroupId) {
                ret.push(entity);
            }
        });
        return ret;
    };
    TwoDCanvas.DEFAULT_LABEL_SIZE = 10;
    TwoDCanvas.DEFAULT_LABEL_FONT = "Arial";
    TwoDCanvas._measureTextContext = null;
    return TwoDCanvas;
}());
Machinata.TwoD.Canvas = TwoDCanvas;
