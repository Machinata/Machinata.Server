declare class TwoDEntityMeasuringTape extends TwoDEntity {
    a: TwoDEntityMeasuringTapeEndPoint;
    b: TwoDEntityMeasuringTapeEndPoint;
    _tapeHandleSize: number;
    constructor();
    saveAsJSON(): any;
    loadFromJSON(json: any): void;
    bindABHandles(): void;
    getMeasureLength(): number;
    drawEntity(renderer: TwoDCanvasRenderer): void;
}
declare class TwoDEntityMeasuringTapeEndPoint extends TwoDEntity {
    pointsTo: TwoDEntity;
    fillColor: string;
    constructor();
    drawEntity(renderer: TwoDCanvasRenderer): void;
    didMove(): void;
}
