declare class TwoDEntitySelection {
    entities: TwoDEntity[];
    _grabWorldPos: gpos;
    _grabRotateHandlePos: gpos;
    _grabRotateHandleAngle: number;
    clone(): TwoDEntitySelection;
    setGrabBeginInformation(cursor: gpos): void;
    moveTo(cursor: gpos, snapToGrid: boolean): void;
    pointTo(cursor: gpos, snapToGrid: boolean): void;
    snapToClosebySnapPoints(): void;
    canMove(): boolean;
    canRotate(): boolean;
    worldPos(): gpos;
    rotateHandlePos(): gpos;
    delete(): void;
    select(entity: TwoDEntity, cursor: gpos): void;
    deselect(entity: TwoDEntity): void;
    isSelected(entity: TwoDEntity): boolean;
    toString(): string;
    saveAsJSON(): any[];
}
