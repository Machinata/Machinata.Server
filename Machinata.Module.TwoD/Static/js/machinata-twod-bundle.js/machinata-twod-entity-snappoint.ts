

/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntitySnapPoint {
    id: string;
    pos: gpos;
    rot: number;
    label: string;
    valid: boolean;
    entity: TwoDEntity;
    _partner: TwoDEntitySnapPoint;

    constructor(label: string, id: string) {
        this.id = id;
        this.label = label;
        this.pos = new gpos(0, 0);
        this.rot = 0;
        this.valid = true;
    }

    saveAsJSON(): any {
        // {
        //     id: "a",
        //     entity_guid: "xyz-abc-012",
        //     partner: {
        //         id: "b",
        //         entity_guid: "xyz-abc-012"
        //     }
        // }
        var json: any = {
            id: this.id,
            label: this.label
        };
        if (this.entity != null) json.entity_guid = this.entity.guid;
        if (this._partner != null) {
            json.partner = {
                id: this._partner.id
            };
            if (this._partner.entity != null) {
                json.partner.entity_guid = this._partner.entity.guid;
            }
        }
        return json;
    }

    loadFromJSON(json: any) {
        
    }

    worldPos(): gpos {
        let parentPos = this.entity.worldPos();
        let parentRot = this.entity.worldRot();
        return parentPos
            .add(this.pos)
            .rotAroundPivot(parentRot, parentPos);
    }

    resnap() {
        this.entity.snapToSnapPoint(this, this.partner());
    }

    unsnap() {
        this.entity.snapToSnapPoint(this, null);
    }

    setPartner(other: TwoDEntitySnapPoint) {
        //if (other != null) console.log("Commit-4b81a9b8-BUG D: setPartner on " + this.label + " to " + other.entity.rid + " (entity " + this.entity.rid + ")");
        //else console.log("Commit-4b81a9b8-BUG D: setPartner on " + this.label + " to null (entity " + this.entity.rid + ")");
        // Link/unlink with partner snap point
        if (other == null) {
            // Unlink
            if (this._partner != null) this._partner._partner = null;
            this._partner = null;
        } else {
            // Link
            this._partner = other;
            other._partner = this;
        }
        // Update the entity snap group id
        if (other != null) {
            // Set snap group id
            // Create a new on other if not already...
            if (other.entity.snapGroupId == null) other.entity.snapGroupId = Machinata.guid();
            //console.log("Commit-4b81a9b8-BUG E: setting this.entity.snapGroupId to " + this.entity.snapGroupId)
            this.entity.snapGroupId = other.entity.snapGroupId;
        } else {
            // Check if none of our snappoints are snapped (ie everything is disconnected)
            var allSnappointsUnsnapped = true;
            for (var spi = 0; spi < this.entity.snapPoints.length; spi++) {
                if (this.entity.snapPoints[spi].isSnapped() == true) {
                    allSnappointsUnsnapped = false;
                    break; // if any is snapped, then allSnappointsUnsnapped is false and we no longer need to check
                }
            }
            if (allSnappointsUnsnapped == true) {
                //console.log("Commit-4b81a9b8-BUG C1: allSnappointsUnsnapped is true, setting snapGroupId to null" + " (entity " + this.entity.rid + ")");
                this.entity.snapGroupId = null; // set our snapgroup to null
            } else {
                //console.log("Commit-4b81a9b8-BUG C2: allSnappointsUnsnapped is false" + " (entity " + this.entity.rid + ")");
            }
        }
    }
    partner(): TwoDEntitySnapPoint {
        return this._partner;
    }

    worldRot(): number {
        return this.entity.worldRot() + this.rot;
    }

    isSnapped(): boolean {
        if (this._partner == null) return false;
        else return true;
    }

    isSnappedAndOverlapping(): boolean {
        if (this.isSnapped() == false) return false;
        if (this.worldPos().dist(this.partner().worldPos()) < 0.000001) return true;
        else return false;
    }
}
