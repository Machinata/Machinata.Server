

/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityUIButton extends TwoDEntity {


    borderColorNormal: string = "black";
    backgroundColorNormal: string = "lightblue";
    textColorNormal: string = "white";

    borderColorHover: string = "black";
    backgroundColorHover: string = "black";
    textColorHover: string = "white";

    textSize: number = 100;

    action: string = null;

    constructor() {
        super("TwoDEntityUIButton");
        let self = this;

        self._canClick = true;
        self._canMove = false;
        self._canRotate = false;
        self._canSelect = false;

        self.setStyleFromBundleTheme();
    }

    saveAsJSON(): any {
        var json = super.saveAsJSON();
        json.borderColorNormal = this.borderColorNormal;
        json.backgroundColorNormal = this.backgroundColorNormal;
        json.textColorNormal = this.textColorNormal;
        json.borderColorHover = this.borderColorHover;
        json.backgroundColorHover = this.backgroundColorHover;
        json.textColorHover = this.textColorHover;
        json.textSize = this.textSize;
        json.action = this.action;
        return json;
    }

    loadFromJSON(json: any) {
        super.loadFromJSON(json);
        this.borderColorNormal = json.borderColorNormal;
        this.backgroundColorNormal = json.backgroundColorNormal;
        this.textColorNormal = json.textColorNormal;
        this.borderColorHover = json.borderColorHover;
        this.backgroundColorHover = json.backgroundColorHover;
        this.textColorHover = json.textColorHover;
        this.textSize = json.textSize;
        this.action = json.action;
    }

    setAction(action: string): this {
        this.action = action;
        return this;
    }

    setLabel(label: string): this {
        super.setLabel(label);

        // Auto size to label
        var widthFactor = 6;
        if (label != null && label.length > 12) widthFactor = label.length * 0.5;
        this._size.h = this.textSize * 1.4;
        this._size.w = this._size.h * widthFactor;

        return this;
    }

    setStyle(borderColorNormal: string, backgroundColorNormal: string, textColorNormal: string, borderColorHover: string, backgroundColorHover: string, textColorHover: string): TwoDEntityUIButton {

        this.borderColorNormal = borderColorNormal;
        this.backgroundColorNormal = backgroundColorNormal;
        this.textColorNormal = textColorNormal;

        this.borderColorHover = borderColorHover;
        this.backgroundColorHover = backgroundColorHover;
        this.textColorHover = textColorHover;

        return this;
    }

    setStyleFromBundleTheme(): TwoDEntityUIButton {

        this.borderColorNormal = "{theme.highlight-color}";
        this.backgroundColorNormal = "{theme.solid-color}";
        this.textColorNormal = "{theme.text-color}";

        this.borderColorHover = "{theme.highlight-color}";
        this.backgroundColorHover = "{theme.highlight-color}";
        this.textColorHover = "{theme.highlight-text-color}";

        return this;
    }

    drawEntity(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;


        let borderColor = this.isHovered() ? this.borderColorHover : this.borderColorNormal;
        let backgroundColor = this.isHovered() ? this.backgroundColorHover : this.backgroundColorNormal;
        let textColor = this.isHovered() ? this.textColorHover : this.textColorNormal;

        // BG Rect
        ctx.fillStyle = backgroundColor;
        ctx.strokeStyle = borderColor;
        ctx.lineWidth = 2 / renderer.viewport.drawScale();;
        ctx.beginPath();
        ctx.rect(-this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
        ctx.closePath();
        ctx.fillAndStroke();

        // Label
        ctx.fillStyle = textColor;
        ctx.font = this.textSize+ "px " + renderer.viewport.labelFontName;
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.fillText(this._label, 0, this.textSize*0.075, this._size.w);
    }

    onClick(): void {
        // Should be implemented by base class
    }

}
Machinata.TwoD.EntityUIButton = TwoDEntityUIButton;





/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityUILabel extends TwoDEntity {


    textColor: string = "white";
    backgroundColor: string = "white";
    borderColor: string = "black";

    textSize: number = 100;
    lineHeight: number = 1.2;
    padding: number = 40;
    borderDashed: boolean = false;
    textAlign: string = "center";

    _lines: string[] = null;

    constructor() {
        super("TwoDEntityUILabel");
        let self = this;

        self._canMove = false;
        self._canRotate = false;
        self._canSelect = false;

        self.setStyleFromBundleTheme();
    }

    saveAsJSON(): any {
        var json = super.saveAsJSON();
        json.textColor = this.textColor;
        json.backgroundColor = this.backgroundColor;
        json.borderColor = this.borderColor;
        json.textSize = this.textSize;
        json.lineHeight = this.lineHeight;
        json.padding = this.padding;
        json.borderDashed = this.borderDashed;
        json.textAlign = this.textAlign;
        json._lines = this._lines;
        return json;
    }

    loadFromJSON(json: any) {
        super.loadFromJSON(json);
        this.textColor = json.textColor;
        this.backgroundColor = json.backgroundColor;
        this.borderColor = json.borderColor;
        this.textSize = json.textSize;
        this.lineHeight = json.lineHeight;
        this.padding = json.padding;
        this.borderDashed = json.borderDashed;
        this.textAlign = json.textAlign;
        this._lines = json._lines;
    }

    setBorderDashed(borderDashed: boolean): this {
        this.borderDashed = borderDashed;
        return this;
    }

    setTextColor(textColor: string): this {
        this.textColor = textColor;
        return this;
    }

    setBackgroundColor(backgroundColor: string): this {
        this.backgroundColor = backgroundColor;
        return this;
    }

    setBorderColor(borderColor: string): this {
        this.borderColor = borderColor;
        return this;
    }

    setLabel(label: string): this {
        super.setLabel(label);

        // Cache lines
        this._lines = label.split("\n");

        // Update size
        var maxLineWidth = 0;
        for (let i = 0; i < this._lines.length; i++) {
            var metrics = TwoDCanvas.measureText(this._lines[i], this.textSize + "px " + TwoDCanvas.DEFAULT_LABEL_FONT);
            maxLineWidth = Math.max(metrics.width, maxLineWidth);
        }
        this._size.w = maxLineWidth + this.padding * 2;
        this._size.h = (this.textSize * this.lineHeight) * this._lines.length + this.padding * 2;


        return this;
    }

    setTextAlign(textAlign: string): this {
        this.textAlign = textAlign;
        return this;
    }

    setStyleFromBundleTheme(): TwoDEntityUILabel {

        this.textColor = "{theme.solid-text-color}";
        this.backgroundColor = "{theme.solid-color}";
        this.borderColor = "{theme.solid-text-color}";

        return this;
    }

    drawEntity(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        // Setup ctx
        ctx.font = this.textSize + "px " + renderer.viewport.labelFontName;

        // Draw BG
        if (this.borderDashed) renderer.setDashedBorderStyle();
        ctx.fillStyle = this.backgroundColor;
        ctx.strokeStyle = this.borderColor;
        ctx.lineWidth = 1 / renderer.viewport.drawScale();;
        ctx.beginPath();
        ctx.rect(-this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
        ctx.closePath();
        ctx.fillAndStroke();

        // Multiline label
        ctx.fillStyle = this.textColor;
        if (this.textAlign == "center") {
            ctx.textAlign = "center";
        } else if (this.textAlign == "left") {
            ctx.textAlign = "left";
            ctx.translate(-this._size.w / 2 + this.padding, 0);
        } else if (this.textAlign == "right") {
            ctx.textAlign = "right";
        }
        ctx.textBaseline = "top";
        for (let i = 0; i < this._lines.length; i++) {
            ctx.fillText(this._lines[i], 0, this.padding + (-this._size.h / 2) + (i * this.textSize * this.lineHeight) + this.textSize * 0.075);
        }
    }

}
Machinata.TwoD.EntityUILabel = TwoDEntityUILabel;


