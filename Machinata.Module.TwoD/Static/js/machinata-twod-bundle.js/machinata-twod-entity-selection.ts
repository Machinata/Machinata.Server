
/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntitySelection {
    entities: TwoDEntity[] = [];

    _grabWorldPos: gpos = null;
    _grabRotateHandlePos: gpos = null;
    _grabRotateHandleAngle: number = null;

    clone(): TwoDEntitySelection {
        var clone = new TwoDEntitySelection();
        for (var i = 0; i < this.entities.length; i++) {
            clone.entities.push(this.entities[i]);
            clone._grabWorldPos = this._grabWorldPos; //TODO should be a clone
            clone._grabRotateHandleAngle = this._grabRotateHandleAngle; //TODO should be a clone
            clone._grabRotateHandleAngle = this._grabRotateHandleAngle;
        }
        return clone;
    }

    setGrabBeginInformation(cursor: gpos) {
        // Sets the information needed to perform a grab move or rotation for a group of entities...
        this._grabWorldPos = this.worldPos();
        this._grabRotateHandlePos = this.rotateHandlePos();
        this._grabRotateHandleAngle = Math.atan2(this._grabRotateHandlePos.y - this._grabWorldPos.y, this._grabRotateHandlePos.x - this._grabWorldPos.x);
        for (var i = 0; i < this.entities.length; i++) {
            this.entities[i]._grabPos = this.entities[i].worldPos();
            this.entities[i]._grabOffset = this.entities[i]._grabPos.subtract(cursor);
            this.entities[i]._grabRotation = this.entities[i].worldRot();
        }
    }
    moveTo(cursor: gpos, snapToGrid: boolean) {
        for (var i = 0; i < this.entities.length; i++) {
            //this.entities[i].setWorldPos(cursor.add(this.entities[i]._grabOffset));
            this.entities[i].moveTo(cursor.add(this.entities[i]._grabOffset), snapToGrid && this.entities.length == 1);
        }
    }
    pointTo(cursor: gpos, snapToGrid: boolean) {
        if (this.entities.length == 1) {
            // Single entity
            this.entities[0].pointTo(cursor, snapToGrid);
        } else {
            // Group rotation
            // We get the relative angle offset since the user started the grab for rotate...
            let rotateHandleAngle = Math.atan2(cursor.y - this._grabWorldPos.y, cursor.x - this._grabWorldPos.x);
            if (snapToGrid) {
                var snapSteps = (Math.PI * 2) / 8; // 45 deg
                rotateHandleAngle = Math.round(rotateHandleAngle / snapSteps) * snapSteps;
            }
            let rotateHandleAngleDelta = rotateHandleAngle - this._grabRotateHandleAngle;
            // Now we rotate each element around that original group privot point
            // and add the relative rotation angle to each object thereafter
            for (var i = 0; i < this.entities.length; i++) {
                var newPos = this.entities[i]._grabPos.rotAroundPivot(rotateHandleAngleDelta, this._grabWorldPos);
                this.entities[i].setWorldPos(newPos);
                this.entities[i].setWorldRot(this.entities[i]._grabRotation + rotateHandleAngleDelta);
            }
        }
        
    }
    snapToClosebySnapPoints() {
        //TODO: allow snapping of all?
        if (this.entities.length == 1) {
            this.entities[0].snapToClosebySnapPoints();
        }
    }
    canMove(): boolean {
        return true; //TODO
    }
    canRotate(): boolean {
        //TODO: support multiple rotation
        //if (this.entities.length != 1) return false; // currently only a single is supported
        for (var i = 0; i < this.entities.length; i++) {
            if (this.entities[i]._canRotate == false) return false;
        }
        return true;
    }
    worldPos(): gpos {
        // Get average of all
        var average = new gpos(0, 0);
        for (var i = 0; i < this.entities.length; i++) {
            average = average.add(this.entities[i].worldPos());
        }
        return average.div(this.entities.length);
    }
    rotateHandlePos(): gpos {
        // Get average of all
        var average = new gpos(0, 0);
        for (var i = 0; i < this.entities.length; i++) {
            average = average.add(this.entities[i].rotateHandlePos());
        }
        return average.div(this.entities.length);
    }
    delete() {
        for (var i = 0; i < this.entities.length; i++) {
            if (this.entities[i]._deletesParent == true && this.entities[i].parent != null) {
                this.entities[i].parent.delete();
            } else {
                this.entities[i].delete();
            }
        }
    }
    select(entity: TwoDEntity, cursor: gpos) {
        if (this.isSelected(entity)) return;
        this.entities.push(entity);
        this.setGrabBeginInformation(cursor);
        entity.onSelected();
    }

    deselect(entity: TwoDEntity) {
        const index = this.entities.indexOf(entity);
        if (index > -1) {
            this.entities.splice(index, 1);
        }
        entity.onDeselected();
    }

    isSelected(entity: TwoDEntity): boolean {
        for (var i = 0; i < this.entities.length; i++) {
            if (this.entities[i] == entity) return true;
        }
        return false;
    }

    toString() {
        var ret = "";
        for (var i = 0; i < this.entities.length; i++) {
            if (ret != "") ret += ", ";
            ret += this.entities[i].toString();
        }
        return ret;
    }

    saveAsJSON(): any[] {
        var ret = [];
        for (var i = 0; i < this.entities.length; i++) {
            ret.push(this.entities[i].saveAsJSON());
        }
        return ret;
    }
}
