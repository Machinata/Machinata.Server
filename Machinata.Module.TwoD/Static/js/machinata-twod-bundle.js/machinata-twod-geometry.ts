
/// <summary>
/// 
/// </summary>
/// <type>class</type>
class gpos {

    x: number;
    y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    clone() {
        return new gpos(this.x, this.y);
    }

    log(label: string): void {
        console.log(label,this.x,this.y);
    }

    mul(factor: number): gpos {
        return new gpos(this.x * factor, this.y * factor);
    }

    div(divisor: number): gpos {
        return new gpos(this.x / divisor, this.y / divisor);
    }

    rot(angle: number): gpos {
        return this.rotAroundPivot(angle, new gpos(0, 0));
    }

    add(other: gpos): gpos {
        return new gpos(this.x + other.x, this.y + other.y);
    }

    subtract(other: gpos): gpos {
        return new gpos(this.x - other.x, this.y - other.y);
    }

    dist(other: gpos): number {
        return Math.sqrt((this.x - other.x) * (this.x - other.x) + (this.y - other.y) * (this.y - other.y));
    }

    static distanceBetween(a: gpos, b: gpos): number {
        return a.dist(b);
    }

    angleTo(other: gpos): number {
        return Math.atan2(other.y - this.y, other.x - this.x);
    }

    static angleBetween(a: gpos, b: gpos): number {
        return a.angleTo(b);
    }

    rotAroundPivot(angle: number, pivot: gpos): gpos {
        let cos = Math.cos(-angle);
        let sin = Math.sin(-angle);
        let run = this.x - pivot.x;
        let rise = this.y - pivot.y;
        let cx = (cos * run) + (sin * rise) + pivot.x;
        let cy = (cos * rise) - (sin * run) + pivot.y;
        return new gpos(
            cx,
            cy
        );
        /*
        let sinTheta = Math.sin(angle);
        let cosTheta = Math.cos(angle);

        return new gpos(
            (cosTheta * (this.x - pivot.x) - sinTheta * (this.x - pivot.y) + pivot.x),
            (sinTheta * (this.x - pivot.x) + cosTheta * (this.y - pivot.y) + pivot.y)
        );*/
    }

    saveAsJSON(): any {
        return { x: this.x, y: this.y };
    }

    static loadFromJSON(json: any): gpos {
        return new gpos(json.x, json.y);
    }

}
Machinata.TwoD.gpos = gpos;


/// <summary>
/// 
/// </summary>
/// <type>class</type>
class gsize {

    w: number;
    h: number;

    constructor(w: number, h: number) {
        this.w = w;
        this.h = h;
    }


    mul(factor: number): gsize {
        return new gsize(this.w * factor, this.h * factor);
    }

    saveAsJSON(): any {
        return { w: this.w, h: this.h };
    }

    static loadFromJSON(json: any): gsize {
        return new gsize(json.w, json.h);
    }
}
Machinata.TwoD.gsize = gsize;



/// <summary>
/// 
/// </summary>
/// <type>class</type>
class gbounds {

    center: gpos;
    size: gsize;

    constructor(center: gpos, size: gsize) {
        this.center = center;
        this.size = size;
    }

    topLeft(): gpos {
        return new gpos(this.x0(), this.y0());
    }
    topRight(): gpos {
        return new gpos(this.x1(), this.y0());
    }
    bottomLeft(): gpos {
        return new gpos(this.x0(), this.y1());
    }
    bottomRight(): gpos {
        return new gpos(this.x1(), this.y1());
    }

    x0(): number {
        return this.center.x - this.size.w / 2;
    }
    x1(): number {
        return this.center.x + this.size.w / 2;
    }

    y0(): number {
        return this.center.y - this.size.h / 2;
    }
    y1(): number {
        return this.center.y + this.size.h / 2;
    }

    extend(by: gbounds): gbounds {
        if (by.x0() < this.x0()) {
            this.setX0(by.x0());
        }
        if (by.x1() > this.x1()) {
            this.setX1(by.x1());
        }
        if (by.y0() < this.y0()) {
            this.setY0(by.y0());
        }
        if (by.y1() > this.y1()) {
            this.setY1(by.y1());
        }
        return this;
    }

    growBy(width: number, height: number): gbounds {
        this.size.w += width;
        this.size.h += height;
        return this;
    }

    setX0(newX0: number): gbounds {
        var delta = this.x0() - newX0;
        this.center.x -= delta / 2;
        this.size.w += delta;
        return this;
    }

    setX1(newX1: number): gbounds {
        var delta = newX1 - this.x1();
        this.center.x += delta / 2;
        this.size.w += delta;
        return this;
    }

    setY0(newY0: number): gbounds {
        var delta = this.y0() - newY0;
        this.center.y -= delta / 2;
        this.size.h += delta;
        return this;
    }

    setY1(newY1: number): gbounds {
        var delta = newY1 - this.y1();
        this.center.y += delta / 2;
        this.size.h += delta;
        return this;
    }

    containsPoint(pos: gpos): boolean {
        if (
            pos.x >= this.x0() && pos.x <= this.x1() &&
            pos.y >= this.y0() && pos.y <= this.y1()
        ) return true;
        else return false;
    }

    intersects(other: gbounds): boolean {
        if (other.x1() <= this.x0()) return false; // other is to left
        if (other.x0() >= this.x1()) return false; // other is to right
        if (other.y1() <= this.y0()) return false; // other is above
        if (other.y0() >= this.y1()) return false; // other is below
        return true;
    }


    saveAsJSON(): any {
        return { center: this.center.saveAsJSON(), size: this.size.saveAsJSON() };
    }

    static loadFromJSON(json: any): gbounds {
        return new gbounds(gpos.loadFromJSON(json.center), gsize.loadFromJSON(json.size));
    }
}
Machinata.TwoD.gbounds = gbounds;