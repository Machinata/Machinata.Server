
/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityImage extends TwoDEntity {

    image: HTMLImageElement;

    _imageURL: string = null;
    _imageLoaded: boolean = false;
    _autoSize: boolean = false;
    _autoSizeScale: number = 1;

    border: boolean = false;

    constructor() {
        super("TwoDEntityImage");
        let self = this;

        this._imageURL = null;
        this._imageLoaded = false;

        this.image = new Image();
        this.image.onload = function () {
            self._imageLoaded = true;
            console.log("TwoDEntityImage: image loaded: " + self.image.src);
            if (self._autoSize == true) {
                // Note: setSize will mark the canvas as dirty - an unwanted sideeffect
                //self.setSize(new gsize(self.image.width * self._autoSizeScale, self.image.height * self._autoSizeScale));
                self._size = new gsize(self.image.width * self._autoSizeScale, self.image.height * self._autoSizeScale);
            }
            if(self.canvas != null) self.canvas.requestRedraw();
        }

        // Properties
        this.props.push(
            new TwoDEntityProp()
                .title("{text.twod.image}")
                .uiType("image")
                .defaultValue(null)
                .getter(function (entity: TwoDEntityImage) { return entity._imageURL })
                .setter(function (entity: TwoDEntityImage, val: string) { entity.setImage(val) })
        );
    }

    saveAsJSON(): any {
        var json = super.saveAsJSON();
        json._imageURL = this._imageURL;
        json._autoSize = this._autoSize;
        json._autoSizeScale = this._autoSizeScale;
        json.border = this.border;
        return json;
    }

    loadFromJSON(json: any) {
        super.loadFromJSON(json);
        this.setImage(json._imageURL);
        this._autoSize = json._autoSize;
        this._autoSizeScale = json._autoSizeScale || 1;
        this.border = json.border || false;
    }

    setAutoSize(autoSize: boolean, scale?: number): this {
        this._autoSize = autoSize;
        if (scale) this._autoSizeScale = scale;
        return this;
    }

    setBorder(border: boolean): this {
        this.border = border;
        return this;
    }

    setImage(url: string): this {
        this._imageLoaded = false;
        this._imageURL = url;
        this.image.src = url;

        return this;
    }

    drawEntity(renderer: TwoDCanvasRenderer): void {
        let ctx = renderer.ctx;

        if (this._imageLoaded == true) {
            ctx.drawImage(this.image, -this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
            if (this.border == true) {
                renderer.setSolidBorderStyle();
                ctx.beginPath();
                ctx.rect(-this._size.w / 2, -this._size.h / 2, this._size.w, this._size.h);
                ctx.closePath();
                ctx.stroke();
            }
        }
    }

}
Machinata.TwoD.EntityImage = TwoDEntityImage;