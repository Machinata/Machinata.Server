
/// <summary>
/// 
/// </summary>
/// <type>class</type>
abstract class TwoDCanvasRenderer {

    ctx: CanvasRenderingContext2D;
    viewport: TwoDCanvasViewport = new TwoDCanvasViewport();

    statsStartDrawTime: Date;
    statsEndDrawTime: Date;
    statsTotalDrawTimeMS: number;
    statsTotalDrawTimeFPS: number;
    statsTotalDrawTimeFPSMin: number;
    statsTotalDrawTimeFPSMax: number;
    statsTotalDrawTimeFPSAverage: number;
    statsTotalDraws: number;

    debug: boolean = true;
    isExport: boolean = false;
    supportsTransformSetAndReset: boolean = true;

    drawLabels: boolean = true;
    drawMeasurements: boolean = false;
    drawUI: boolean = true;

    drawingCallCache = {};

    abstract createDrawingContext(options: any): any;

    prepareForDrawing() {
        // Stats
        this.statsStartDrawTime = new Date();
        this.statsTotalDraws = 0;
        // Cache
        this.resetDrawingCallCache();
    }

    resetDrawingCallCache() {
        this.drawingCallCache = {};
    }

    drawWithCachedDrawCall(cacheId: string, x: number, y: number, w: number, h: number, drawCall: Function) {
        // Note:
        // drawCall should be:
        // function (renderer: TwoDCanvasRenderer, ctx: CanvasRenderingContext2D)

        // Init
        let self = this;

        // Should we even do caching?
        if (self.isExport) {
            self.ctx.save(); {
                self.ctx.translate(x, y);
                drawCall(self, self.ctx);
            } self.ctx.restore();
            
            return;
        }

        // Find in cache
        var fromCache = self.drawingCallCache[cacheId];
        if (fromCache == null) {
            // No cache hit - create a temp canvas and do draw
            let canvasTemp = document.createElement("canvas");
            let ctxTemp = canvasTemp.getContext("2d");
            // Canvas must be scaled according to zoom for high-res
            canvasTemp.width = self.viewport.mmToPx(w) * self.viewport.zoom;
            canvasTemp.height = self.viewport.mmToPx(h) * self.viewport.zoom;
            // Set the scale
            var contextScale = self.viewport.drawScaleForZoom(self.viewport.zoom);
            ctxTemp.scale(contextScale, contextScale);
            // Do draw call
            drawCall(self, ctxTemp);
            // Register in cache
            fromCache = self.drawingCallCache[cacheId] = canvasTemp;
        }
        // Draw from cache...
        self.ctx.drawImage(fromCache, x, y, w, h);
    }

    setShapeStyle() {
        this.ctx.fillStyle = "white";
        this.ctx.strokeStyle = "black";
        this.ctx.lineWidth = 1 / this.viewport.drawScale();
    }

    setLineStyle() {
        this.ctx.strokeStyle = "black";
        this.ctx.fillStyle = null;
        this.ctx.lineWidth = 1 / this.viewport.drawScale();
    }

    setDashedBorderStyle() {
        this.ctx.setLineDash([5 / this.viewport.drawScale(), 5 / this.viewport.drawScale()]);
        this.ctx.strokeStyle = "black";
        this.ctx.lineWidth = 1 / this.viewport.drawScale();;
    }

    setSolidBorderStyle() {
        this.ctx.setLineDash([]);
        this.ctx.strokeStyle = "black";
        this.ctx.lineWidth = 1 / this.viewport.drawScale();;
    }

    translatePointOnUnscaledContext(p: gpos): gpos {
        var ret = p;
        // Do the same transform as a regular drawing context...
        // Move viewport
        ret = ret.add(new gpos(this.viewport.pos.x, this.viewport.pos.y));
        // Scale
        ret = ret.mul(this.viewport.drawScale());
        // Move to center of device (0,0 starts in center)
        ret = ret.add(new gpos(Math.round(this.viewport.deviceSize.w / 2), Math.round(this.viewport.deviceSize.h / 2)));
        return ret;
    }

    drawArc(x, y, radius, start, end, direction, moveTo) {
        // Custom arc method that is compatible with all underlying drawing contexts (PDF/browser canvas)
        // If connecting a path with multiple arcs, set moveTo to true to do an initial moveTo on the first arc line point (ie set true on the first arc, if the first path drawing op is an arc)
        // Based on:
        // https://stackoverflow.com/questions/46287423/custom-canvas-arc-method-implementation
        const PI = Math.PI;  // use PI and PI * 2 a lot so make them constants for easy reading
        const PI2 = PI * 2;
        // check radius is in range 
        if (radius < 0) { throw new Error(`Failed to execute 'arc' on 'CanvasRenderingContext2D': The radius provided (${radius}) is negative.`) }
        if (radius == 0) { this.ctx.lineTo(x, y) } // if zero radius just do a lineTo
        else {
            const angleDist = end - start; // get the angular distance from start to end;
            let step, i;
            let steps = radius;  // number of 6.28 pixel steps is radius
            // check for full CW or CCW circle depending on directio
            if ((direction !== true && angleDist >= PI2)) { // full circle
                step = PI2 / steps;
            } else if ((direction === true && angleDist <= -PI2)) { // full circle
                step = -PI2 / steps;
            } else {
                // normalise start and end angles to the range 0- 2 PI
                start = ((start % PI2) + PI2) % PI2;
                end = ((end % PI2) + PI2) % PI2;
                if (end < start) { end += PI2 }           // move end to be infront (CW) of start
                if (direction === true) { end -= PI2 }     // if CCW move end behind start
                steps *= (end - start) / PI2;            // get number of 2 pixel steps
                step = (end - start) / steps;            // convert steps to a step in radians
                if (direction === true) { step = -step; } // correct sign of step if CCW
                steps = Math.abs(steps);                 // ensure that the iteration is positive
            }
            // iterate circle
            if (moveTo == true) {
                this.ctx.moveTo(
                    Math.cos(start) * radius + x,
                    Math.sin(start) * radius + y
                );
            }
            for (i = 0; i < steps; i += 1) {
                this.ctx.lineTo(
                    Math.cos(start + step * i) * radius + x,
                    Math.sin(start + step * i) * radius + y
                );
            }
            this.ctx.lineTo( // do the last segment
                Math.cos(start + step * steps) * radius + x,
                Math.sin(start + step * steps) * radius + y
            );
        }
    }

    drawPixelPerfectLine(p1: gpos, p2: gpos, lineSize: number) {
        let ctx = this.ctx;
        
        function snapRound(num) {
            return Math.round(num) + 0.5;
        };
        ctx.save(); {
            if (this.supportsTransformSetAndReset == true) {
                // To get a pixel perfect line, we translate the points to a unscaled drawing 
                // context, then set the transform to the unit vector and draw...
                p1 = this.translatePointOnUnscaledContext(p1);
                p2 = this.translatePointOnUnscaledContext(p2);
                ctx.lineWidth = lineSize; // no need to scale
                ctx.setTransform(1, 0, 0, 1, 0, 0);
                ctx.beginPath();
                ctx.moveTo(snapRound(p1.x), snapRound(p1.y));
                ctx.lineTo(snapRound(p2.x), snapRound(p2.y));
                ctx.stroke();
            } else {
                // PDFs cannot setTransform - thus we just draw directly
                // ""cm" always concatenates the matrix to the current matrix and there is no operator to set it."
                // See https://stackoverflow.com/questions/34361609/is-it-possible-to-reinitialize-the-graphics-state-in-a-pdf-file
                ctx.lineWidth = lineSize / this.viewport.drawScale();
                ctx.beginPath();
                ctx.moveTo((p1.x), (p1.y));
                ctx.lineTo((p2.x), (p2.y));
                ctx.stroke();
            }
        } ctx.restore();
    }

    drawMeasurement(p1: gpos, p2: gpos, offset?: number, label?: string) {
        let self = this;
        let ctx = this.ctx;

        ctx.save(); {

            this.setLineStyle();

            // Init
            let offsetAmount = offset || 14;
            let tickSize = 6;
            let lineSize = 1;

            // Calculate angle and offset
            let angle = p1.angleTo(p2);
            let perpendicularOffsetAngle = angle - Machinata.Math.degToRad(90);
            let perpendicularOffsetAmount = offsetAmount / this.viewport.drawScale();
            let perpendicularOffset = new gpos(perpendicularOffsetAmount * Math.cos(perpendicularOffsetAngle), perpendicularOffsetAmount * Math.sin(perpendicularOffsetAngle))
            let p1o = p1.add(perpendicularOffset);
            let p2o = p2.add(perpendicularOffset);

            // P1 tick
            var tickSizeScaled = tickSize / this.viewport.drawScale();
            var tickAngle = angle;
            {
                var tickAngle1 = tickAngle + Machinata.Math.degToRad(-45);
                var tickAngle2 = tickAngle + Machinata.Math.degToRad(-45 + 180);
                self.drawPixelPerfectLine(
                    p1o.add(new gpos(tickSizeScaled * Math.cos(tickAngle1), tickSizeScaled * Math.sin(tickAngle1))),
                    p1o.add(new gpos(tickSizeScaled * Math.cos(tickAngle2), tickSizeScaled * Math.sin(tickAngle2))),
                    lineSize
                );
            }
            // P2 tick
            {
                var tickAngle1 = tickAngle + Machinata.Math.degToRad(-45);
                var tickAngle2 = tickAngle + Machinata.Math.degToRad(-45 + 180);
                self.drawPixelPerfectLine(
                    p2o.add(new gpos(tickSizeScaled * Math.cos(tickAngle1), tickSizeScaled * Math.sin(tickAngle1))),
                    p2o.add(new gpos(tickSizeScaled * Math.cos(tickAngle2), tickSizeScaled * Math.sin(tickAngle2))),
                    lineSize
                );
            }

            // Connecting line
            self.drawPixelPerfectLine(p1o, p2o, lineSize);

            // Label
            if (label == null) label = TwoDCanvas.getMetricUnitLabel(p1.dist(p2), 2);
            else if (label == "{units}") label = Math.round(p1.dist(p2)) + "" + self.viewport.units;
            var plabel = p1o.add(p2o).div(2);
            ctx.save(); {
                ctx.translate(plabel.x, plabel.y);
                //ctx.rotate(angle);
                self.drawLabel(null, label, 0, 0, "center", true);
            } ctx.restore();

        } ctx.restore();
    }

    drawLabel(entity: TwoDEntity, text: string, x: number, y: number, alignment: CanvasTextAlign, forceDraw?: boolean) {
        let drawLabel = true;
        if (this.drawLabels == false ) {
            // Only draw labels on hover/select
            drawLabel = false;
            if (entity != null && this.isExport == false) {
                if (entity.isSelected() == true) drawLabel = true;
                if (entity.canvas.hoveredEntity == entity) drawLabel = true;
            }
        }
        if (forceDraw == true) drawLabel = true;
        if (!drawLabel) return;
        

        let ctx = this.ctx;

        ctx.save(); {
            var labelSize = this.viewport.labelFontSize / this.viewport.drawScale();
            ctx.textAlign = alignment;
            ctx.textBaseline = "middle";
            ctx.font = labelSize + "px " + this.viewport.labelFontName;

            var lines = text.split("\n");
            for (var i = 0; i < lines.length; i++) {
                var line = lines[i];
                var meas = ctx.measureText(line);
                ctx.fillStyle = "rgba(0,0,0,0.6)";
                var padding = 2 / this.viewport.drawScale();
                var w = meas.width;
                var h = Math.floor(labelSize*1.2);
                var boxW = w + padding + padding;
                var boxH = h + padding + padding;
                var yOffset = -((boxH * lines.length) / 2) + boxH * i + boxH / 2;
                var boxXOffset = -boxW / 2; // center
                if (alignment == "left") boxXOffset = -padding; // left 
                ctx.fillRect(
                    x + boxXOffset,
                    y + yOffset - boxH/2,
                    boxW,
                    boxH);
                ctx.fillStyle = "white";
                ctx.fillText(line, x, y + yOffset);
            }
        } ctx.restore();
    }
}

/// <summary>
/// 
/// </summary>
/// <type>class</type>
class HTMLCanvasTwoDCanvasRenderer extends TwoDCanvasRenderer {

    canvas: HTMLCanvasElement;

    createDrawingContext(options: any): any {
        // Create the canvas
        var canvas = document.createElement('canvas');
        canvas.width = this.viewport.deviceSize.w;
        canvas.height = this.viewport.deviceSize.h;
        this.canvas = canvas;

        // Set the drawing context
        var ctx = canvas.getContext('2d');
        this.ctx = ctx;
        return ctx;
    }
}

/// <summary>
/// Calling fill and then stroke consecutively only executes fill
/// Some canvas 2d context methods are not implemented yet(e.g.setTransform and arcTo)
/// See https://github.com/dankrusi/canvas2pdf
/// </summary>
/// <type>class</type>
class PDFCanvasTwoDCanvasRenderer extends TwoDCanvasRenderer {

    canvas: HTMLCanvasElement;

    createDrawingContext(options: any): any {
        //Create a new PDF canvas context.
        var ctx = new canvas2pdf.PdfContext(blobStream(), options);
        this.ctx = ctx;
        return ctx;
    }
}