
// CanvasRenderingContext2D context extensions

interface CanvasRenderingContext2D {
    fillMultilineText(text: string, x: number, y: number): void;
    fillAndStroke(): void;
}
Object.defineProperty(CanvasRenderingContext2D.prototype, 'fillMultilineText', {
    value: function (text: string, x: number, y: number) {
        let lines = text.split("\n");
        let fontSize = parseInt(this.font.split("px")[0]);
        for (let i = 0; i < lines.length; i++) {
            this.fillText(lines[i], x, y + i * fontSize);
        }
    }
});
Object.defineProperty(CanvasRenderingContext2D.prototype, 'fillAndStroke', {
    value: function () {
        this.fill();
        this.stroke();
    }
});

/* 
//https://stackoverflow.com/questions/46287423/custom-canvas-arc-method-implementation
CanvasRenderingContext2D.prototype.arc = function (x, y, radius, start, end, direction) {
    const PI = Math.PI;  // use PI and PI * 2 a lot so make them constants for easy reading
    const PI2 = PI * 2;
    // check radius is in range 
    if (radius < 0) { throw new Error(`Failed to execute 'arc' on 'CanvasRenderingContext2D': The radius provided (${radius}) is negative.`) }
    if (radius == 0) { this.lineTo(x, y) } // if zero radius just do a lineTo
    else {
        const angleDist = end - start; // get the angular distance from start to end;
        let step, i;
        let steps = radius;  // number of 6.28 pixel steps is radius
        // check for full CW or CCW circle depending on directio
        if ((direction !== true && angleDist >= PI2)) { // full circle
            step = PI2 / steps;
        } else if ((direction === true && angleDist <= -PI2)) { // full circle
            step = -PI2 / steps;
        } else {
            // normalise start and end angles to the range 0- 2 PI
            start = ((start % PI2) + PI2) % PI2;
            end = ((end % PI2) + PI2) % PI2;
            if (end < start) { end += PI2 }           // move end to be infront (CW) of start
            if (direction === true) { end -= PI2 }     // if CCW move end behind start
            steps *= (end - start) / PI2;            // get number of 2 pixel steps
            step = (end - start) / steps;            // convert steps to a step in radians
            if (direction === true) { step = -step; } // correct sign of step if CCW
            steps = Math.abs(steps);                 // ensure that the iteration is positive
        }
        // iterate circle 
        for (i = 0; i < steps; i += 1) {
            this.lineTo(
                Math.cos(start + step * i) * radius + x,
                Math.sin(start + step * i) * radius + y
            );
        }
        this.lineTo( // do the last segment
            Math.cos(start + step * steps) * radius + x,
            Math.sin(start + step * steps) * radius + y
        );
    }
}*/



// canvas2pdf context extensions

Object.defineProperty(canvas2pdf.PdfContext.prototype, 'fillMultilineText', {
    value: function (text: string, x: number, y: number) {
        let lines = text.split("\n");
        let fontSize = parseInt(this.font.split("px")[0]);
        for (let i = 0; i < lines.length; i++) {
            this.fillText(lines[i], x, y + i * fontSize);
        }
    }
});