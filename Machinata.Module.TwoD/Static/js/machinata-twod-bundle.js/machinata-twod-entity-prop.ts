
/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityPropSetEvent {
    triggerEntityUpdateForNewPropValue: boolean = true;
    triggerEntityOnPropChanged: boolean = true;
    isUserTriggered: boolean = true;
    userData: any = null;
}

/// <summary>
/// 
/// </summary>
/// <type>class</type>
class TwoDEntityProp {

    static _rid: number = 0;
    rid: number;

    _id: string;
    _title: string;
    _units: string;
    _tooltip: string;
    _uiType: string;
    _minValue: any;
    _maxValue: any;
    _defaultValue: any;
    _prevAcceptedValuesArray: any[];
    _acceptedValuesArray: any[];
    _acceptedTitlesArray: any[];
    _acceptedValuesAndTitlesCallback: Function;
    _propUIUpdatedCallback: Function;
    _step: any;
    _readonly: boolean;
    _visible: boolean = true;
    _altersPartners: boolean; // indicates that this property affects snapped partners
    _valueTips: { [name: string]: string } = null; // null

    _getter: Function;
    _setter: Function;


    /// <summary>
    /// Custom store for custom model properties. Can store anything.
    /// </summary>
    data: any = {};


    constructor() {
        // Set runtime id
        this.rid = TwoDEntity._rid++;
    }

    id(id: string): TwoDEntityProp {
        this._id = id;
        return this;
    }
    title(title: string): TwoDEntityProp {
        this._title = title;
        return this;
    }
    units(units: string): TwoDEntityProp {
        this._units = units;
        return this;
    }
    tooltip(tooltip: string): TwoDEntityProp {
        this._tooltip = tooltip;
        return this;
    }
    uiType(uiType: string): TwoDEntityProp {
        this._uiType = uiType;
        return this;
    }
    step(step: any): TwoDEntityProp {
        this._step = step;
        return this;
    }
    readonly(readonly: boolean): TwoDEntityProp {
        this._readonly = readonly;
        return this;
    }
    visible(visible: boolean): TwoDEntityProp {
        this._visible = visible;
        return this;
    }
    altersPartners(altersPartners: boolean): TwoDEntityProp {
        this._altersPartners = altersPartners;
        return this;
    }
    minValue(minValue: any): TwoDEntityProp {
        this._minValue = minValue;
        return this;
    }
    maxValue(maxValue: any): TwoDEntityProp {
        this._maxValue = maxValue;
        return this;
    }
    defaultValue(defaultValue: any): TwoDEntityProp {
        this._defaultValue = defaultValue;
        return this;
    }
    acceptedValuesAndTitles(acceptedValues: any[], acceptedTitles: any[]): TwoDEntityProp {
        // Sanity
        if (acceptedValues.length != acceptedTitles.length) throw "TwoDProp.acceptedValues(): values and titles dont have the same length!";
        this._acceptedValuesArray = acceptedValues;
        this._acceptedTitlesArray = acceptedTitles;
        return this;
    }
    acceptedValuesAndTitlesViaCallback(callback: Function): TwoDEntityProp {
        this._acceptedValuesAndTitlesCallback = callback;
        return this;
    }
    valueTips(tips: { [name: string]: string}): TwoDEntityProp {
        this._valueTips = tips;
        return this;
    }
    // callback: function(entity, prop, propElem, propInputElem, entityVal){}
    propUIUpdatedCallback(callback: Function): TwoDEntityProp {
        this._propUIUpdatedCallback = callback;
        return this;
    }
    getter(getter: Function): TwoDEntityProp {
        this._getter = getter;
        return this;
    }
    setter(setter: Function): TwoDEntityProp {
        this._setter = setter;
        return this;
    }


    setVisible(visible: boolean): TwoDEntityProp {
        this._visible = visible;
        this.updateUI();
        return this;
    }

    _setValue(entity: TwoDEntity, val: any) {
        let self = this;
        if (this._uiType == "number") val = parseFloat(val);
        // Set value
        this._setter(entity, val);

        // Mark canvas dirty
        if (entity != null && entity.canvas != null) entity.canvas.setDirty();
    }

    setValueWithoutTriggers(entity: TwoDEntity, val: any, eventData?: TwoDEntityPropSetEvent): TwoDEntityProp {
        // Sanity
        if (eventData == null) eventData = new TwoDEntityPropSetEvent();

        // Turn off all trigers
        eventData.triggerEntityUpdateForNewPropValue = false;
        eventData.triggerEntityOnPropChanged = false;

        return this.setValue(entity, val, eventData);
    }

    setValueWithUserTrigger(entity: TwoDEntity, val: any, eventData?: TwoDEntityPropSetEvent): TwoDEntityProp {
        // Sanity
        if (eventData == null) eventData = new TwoDEntityPropSetEvent();

        // Turn off all trigers
        eventData.triggerEntityUpdateForNewPropValue = true;
        eventData.triggerEntityOnPropChanged = true;
        eventData.isUserTriggered = true;

        return this.setValue(entity, val, eventData);
    }

    setValue(entity: TwoDEntity, val: any, eventData?: TwoDEntityPropSetEvent): TwoDEntityProp {
        let self = this;
        // Sanity
        if (eventData == null) eventData = new TwoDEntityPropSetEvent();
        // Val conversion
        if (this._uiType == "number") val = parseFloat(val);
        // Set value
        this._setValue(entity, val);
        // Update entity
        if (eventData.triggerEntityUpdateForNewPropValue) entity.updateForNewPropValue(this, val, eventData);
        // Update event
        if (eventData.triggerEntityOnPropChanged && entity.onPropChanged) entity.onPropChanged(entity, this, val, eventData);
        this.updateUI();
        return this;
    }

    updateUI() {
        // UI
        $(".entity-prop[data-prop-id=" + this.rid + "]").each(function () {
            var propElem = $(this);
            propElem.trigger("entity-val-changed");
        });
    }

    getValue(entity: TwoDEntity): any {
        // Get value
        return this._getter(entity);
    }

    dataGetterSetter(key: string) {
        this.getter(function (entity: TwoDEntity) { return entity.data[key] });
        this.setter(function (entity: TwoDEntity, val: any) { entity.data[key] = val });
    }

    _updateTip(entity: TwoDEntity, propElem: any) {

        // Update tip?
        if (this._valueTips != null) {
            console.log("updateTip!!!!!!!!", this._valueTips);
            // Find the right tip...
            var tipToShow = null;
            var entityVal = this.getValue(entity);
            for (var key in this._valueTips) {
                if (entityVal == key) {
                    tipToShow = this._valueTips[key];
                    break;
                }
            }
            // If we got something, update the UI for the tip
            if (propElem == null || propElem.length == 0) console.warn("_updateTip: could not get prop elem");
            if (tipToShow != null) {
                propElem.find(".prop-tip").text(tipToShow).show();
            } else {
                propElem.find(".prop-tip").hide();
            }
        }
    }

    buildStandardEditUI(entity: TwoDEntity) {
        var self = this;

        // Init elem
        var propElem = $("<div class='ui-form-row entity-prop'><div class='ui-label'></div><div class='ui-input'></div></div>");
        propElem.attr("data-entity-id",entity.rid);
        propElem.attr("data-prop-id",self.rid);
        propElem.attr("id", "entity" + entity.rid+"prop"+self.rid);
        propElem.data("entity",entity);
        propElem.data("prop", self);

        // Visible?
        if (this._visible == false) {
            propElem.hide();
        }

        // Tip?
        if (this._valueTips != null) {
            propElem.append("<div class='prop-tip ui-info option-small'></div>");
        }

        // Label
        var label = self._title;
        if (self._units != null) {
            if (self._units == "{canvas-units}") label += " [" + entity.canvas.config.units + "]";
            else label += " [" + self._units + "]";
        }
        propElem.find(".ui-label").text(label);
        if (self._tooltip != null) propElem.attr("title", self._tooltip);

        // Create input element
        var propInputTag = "input";
        if (self._uiType == "select") propInputTag = "select";
        if (self._uiType == "textarea") propInputTag = "textarea";
        if (self._uiType == "json") propInputTag = "textarea";
        if (self._uiType == "warning") propInputTag = "div";
        if (self._uiType == "error") propInputTag = "div";
        if (self._uiType == "thumbnails" || self._uiType == "buttons") propInputTag = "div";
        var propInputElem = $("<" + propInputTag + " class='prop-input'/>").appendTo(propElem.find(".ui-input"));
        if (propInputTag != "div") propInputElem.attr("type", self._uiType);
        // Set attributes
        if (self._step) propInputElem.attr("step", self._step);
        if (self._minValue) propInputElem.attr("min", self._minValue);
        if (self._maxValue) propInputElem.attr("max", self._maxValue);
        
        // Thumbnails type
        if (self._uiType == "thumbnails" || self._uiType == "buttons") {
            propInputElem.addClass("ui-controlgroup");
        }
        // Warning/error type
        if (self._uiType == "warning" || self._uiType == "error") {
            propElem.hide(); // hide by default, will be shown on val-changed trigger
            propInputElem.append($("<div class='info-text prop-value' style='white-space: pre-line;'></div>"));
            propInputElem.addClass("ui-info");
            propInputElem.addClass("option-small");
            propInputElem.addClass("option-" + self._uiType);
            propElem.find(".ui-label").hide();
        }
        // Readonly?
        if (self._setter == null || self._readonly == true) propInputElem.attr("readonly", "readonly");
        // Set value
        if (self._uiType == "json") {
            propInputElem.val(JSON.stringify(self.getValue(entity)));
        } else {
            propInputElem.val(self.getValue(entity));
        }
        // Accepted values?
        self.updateAcceptedValuesEditUI(entity, propElem);
        // Tip?
        self._updateTip(entity, propElem);
        // Bind events
        propInputElem.on("change input", function () {
            // Set val
            var val = $(this).data("value") || $(this).val();
            if (self._uiType == "json") {
                self._setValue(entity, JSON.parse(val));
            } else {
                self._setValue(entity, val);
            }
            // Update values on all other?
            // We do this looking down in the list of props (ie the top drive the bottom)
            for (var i = entity.props.indexOf(self)+1; i < entity.props.length; i++) {
                var otherProp = entity.props[i];
                //console.log("will updateAcceptedValuesEditUI " + otherProp._title)
                if (otherProp == self) continue; // skip self
                if (otherProp) otherProp.updateAcceptedValues(entity);
                if (otherProp) otherProp.updateAcceptedValuesEditUI(entity, null);
            }
            // Update entity
            let eventData = new TwoDEntityPropSetEvent();
            eventData.isUserTriggered = true;
            entity.updateForNewPropValue(self, val, eventData);
            // Tip?
            self._updateTip(entity,propElem);
            // Update event
            if (entity.onPropChanged) entity.onPropChanged(entity, self, val);
            // Redraw
            entity.canvas.requestRedraw();
        });
        propElem.on("entity-val-changed", function () {
            var propElem = $(this);
            var entity = propElem.data("entity");
            var prop = propElem.data("prop") as TwoDEntityProp;
            var propInputElem = propElem.find(".prop-input");
            var entityVal = prop.getValue(entity);
            // Update value, depending on type
            if (prop._uiType == "warning" || prop._uiType == "error") {
                // Update the prop value elem as text
                propElem.find(".prop-value").text(entityVal);
            } else if (prop._uiType == "thumbnails" || prop._uiType == "buttons") {
                propInputElem.find(".ui-button").removeClass("selected");
                propInputElem.find(".ui-button").each(function () {
                    if ($(this).data("value") == entityVal) $(this).addClass("selected");
                    else $(this).removeClass("selected");
                });
            } else if (prop._uiType == "select") {
                throw "not yet supported!";
            } else if(prop._uiType == "json") {
                // Update input value
                propInputElem.val(JSON.stringify(entityVal));
            } else {
                // Update input value
                propInputElem.val(entityVal);
            }
            
            // Update visibility
            if (prop._uiType == "warning" || prop._uiType == "error") {
                // Update visibitlity - based on contents
                if (entityVal == "" || entityVal == null) {
                    propElem.hide();
                } else {
                    propElem.show();
                }
            } else {
                // Update visibility - regular property
                if (prop._visible == true) {
                    propElem.show();
                } else {
                    propElem.hide();
                }
            }

            // Updated callback?
            if (prop._propUIUpdatedCallback != null) prop._propUIUpdatedCallback(entity, prop, propElem, propInputElem, entityVal);
        });
        propInputElem.trigger("entity-val-changed");


        return propElem;
    }
    getPropElemInDOM(entity: TwoDEntity): any {
        var self = this;
        var propElem = $("#entity" + entity.rid + "prop" + self.rid);
        if (propElem.length == 0) console.warn("getPropElemInDOM: could not find prop elem (" + "#entity" + entity.rid + "prop" + self.rid+")");
        return propElem;
    }

    getAcceptedValuesAndTitles(entity: TwoDEntity): any[] {
        var self = this;
        var valuesTitles = []; //TODO: what is the best default?
        if (self._acceptedValuesArray != null) {
            for (var i = 0; i < self._acceptedValuesArray.length; i++) {
                valuesTitles.push({
                    val: self._acceptedValuesArray[i],
                    title: self._acceptedTitlesArray[i]
                });
            }
        } else if (self._acceptedValuesAndTitlesCallback != null) {
            valuesTitles = self._acceptedValuesAndTitlesCallback(entity, self);
            // Sanity
            if (valuesTitles == null) throw "TwoDEntityProp.getAcceptedValuesAndTitles(): callback returned null. Expected a array of val/title pairs!";
            if (valuesTitles.length > 0 && valuesTitles[0].val == null) throw "TwoDEntityProp.getAcceptedValuesAndTitles(): callback returned unknown object (no val). Expected a array of val/title pairs!";
            if (valuesTitles.length > 0 && valuesTitles[0].title == null) throw "TwoDEntityProp.getAcceptedValuesAndTitles(): callback returned unknown object (no title). Expected a array of val/title pairs!";
        }
        return valuesTitles;
    }

    updateAcceptedValues(entity: TwoDEntity) {
        var self = this;
        //console.log("updateAcceptedValues", self._title);
        
        // Prev value
        var prevValue = self.getValue(entity);
        // Is this a multi value prop that has accepted values?
        if (self._uiType == "select" || self._uiType == "thumbnails" || self._uiType == "buttons") {
            // Get new values
            var newValuesTitles = self.getAcceptedValuesAndTitles(entity);
            self._prevAcceptedValuesArray = newValuesTitles;
            // Calculate the new value (prioritizing the prev value)
            var newValue = null;
            for (var i = 0; i < newValuesTitles.length; i++) {
                if (newValuesTitles[i].val == prevValue) newValue = prevValue;
            }
            if (newValue == null) newValue = newValuesTitles[0].val;
            // Update value on entity (if changed)
            if (newValue != prevValue) self._setValue(entity, newValue);
        }
    }

    updateAcceptedValuesEditUI(entity: TwoDEntity, propElem: any) {
        var self = this;
        //console.log("updateAcceptedValuesEditUI", self._title);
        // Init
        if (propElem == null) propElem = self.getPropElemInDOM(entity);
        var propInputElem = propElem.find(".prop-input");
        if (propInputElem.length == 0) console.warn("updateAcceptedValuesEditUI: could not find input elem");
        // Prev value
        var propValue = self.getValue(entity);
        // Is this a multi value prop that has accepted values?
        if (self._uiType == "select" || self._uiType == "thumbnails" || self._uiType == "buttons") {
            // Get new values
            var newValuesTitles = self.getAcceptedValuesAndTitles(entity);
            self._prevAcceptedValuesArray = newValuesTitles;
            // Re-build UI
            if (self._uiType == "select") {
                propInputElem.empty();
                for (var i = 0; i < newValuesTitles.length; i++) {
                    var optionElem = $("<option class='prop-option'/>");
                    optionElem.text(newValuesTitles[i].title);
                    optionElem.attr("value", newValuesTitles[i].val);
                    if (newValuesTitles[i].val == propValue) optionElem.attr("selected", "selected");
                    propInputElem.append(optionElem);
                }
            } else if (self._uiType == "thumbnails" || self._uiType == "buttons") {
                propInputElem.empty();
                for (var i = 0; i < newValuesTitles.length; i++) {
                    var optionElemTag = "button";
                    if (self._uiType == "thumbnails") optionElemTag = "div";
                    var optionElem = $("<" + optionElemTag+" class='ui-button prop-option'/>");
                    if (self._uiType == "thumbnails") optionElem.addClass("option-thumbnail");
                    optionElem.text(newValuesTitles[i].title);
                    optionElem.data("entity", entity);
                    optionElem.data("prop", self);
                    optionElem.data("value", newValuesTitles[i].val);
                    if (newValuesTitles[i].image != null) {
                        var thumbnailElem = $("<div class='thumbnail magnification-image magnification-trigger material'></div>");
                        thumbnailElem.css("background-image", "url(" + newValuesTitles[i].image + ")");
                        thumbnailElem.appendTo(optionElem);
                        optionElem.addClass("option-magnify");
                    }
                    if (newValuesTitles[i].val == propValue) optionElem.addClass("selected");
                    optionElem.click(function () {
                        //$(this).data("prop").setValue($(this).data("entity"), $(this).data("value"));
                        propInputElem.find(".ui-button").removeClass("selected");
                        $(this).addClass("selected");
                        propInputElem.data("value", $(this).data("value"));
                        propInputElem.trigger("change");
                    });
                    propInputElem.append(optionElem);
                }
                if (Machinata.Responsive.currentLayout == "desktop") {
                    Machinata.UI.magnificationPopup(propInputElem.find(".option-magnify"), {
                        delay: 800
                    });
                }
            }
        }
    }
}

