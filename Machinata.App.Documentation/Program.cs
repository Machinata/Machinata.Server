using Machinata.Core.Documentation.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using static Machinata.Core.Documentation.Model.DocumentationPackage;

namespace Machinata.App.Documentation {
    class Program {
        static void Main(string[] args) {

            // E.g. for testing: C:\Data\Code\Nerves\Machinata.Product.NervesWebsite\Static\documentation\documentation.json C:\Data\Code\Nerves\Machinata.Product.NervesWebsite\\bin
            // Init
            if (args.Length == 0) {
                throw new ArgumentException("No config file defined");
            }
            var projectBinPath = Directory.GetCurrentDirectory();

            // Override bin path from command line
            if (args.Length >= 2) {
                projectBinPath = args[1];
            }
            var configFile = args[0];
            var projectBasePath = new DirectoryInfo(projectBinPath).Parent.FullName;

            // Load configuration
            var configuration = LoadPackageConfiguration(configFile);
            var destination =  (configuration["destination"] != null ? configuration["destination"] : "{project.base-path}\\Static\\documentation\\packages")?.ToString().ToString().Replace("{project.bin-path}", projectBinPath).Replace("{project.base-path}", projectBasePath); ;
            var clearDestinationFolder = configuration["clear-destination"]?.Value<bool>();
            var outputFormatString = configuration["output-format"]?.ToString();
            ExportFormat exportFormat = ExportFormat.JSON;

            if (clearDestinationFolder.HasValue == false) {
                clearDestinationFolder = false;
            }

            if (outputFormatString == "TypeScript") {
                exportFormat = ExportFormat.TypeScript;
            }

            var packages = new List<DocumentationPackage>();
         
            // Create Documentation Packgages
            foreach (var bundle in configuration["packages"].Children()) {
                // Init
                var path = bundle["path"]?.ToString().Replace("{project.bin-path}", projectBinPath).Replace("{project.base-path}", projectBasePath);
                var extension = bundle["extension"]?.ToString();
                var prefix = bundle["prefix"]?.ToString();
                var description = bundle["description"]?.ToString();
                var name = bundle["name"]?.ToString();
                var generateMissingNamespacesVal = bundle["generateMissingNamespaceItems"]?.Value<bool>();
                var includeSourceCodeVal = bundle["includeSourceCode"]?.Value<bool>();
                var privateMembersAreHidden = bundle["privateMembersAreHidden"]?.Value<bool>() == true ? true: false;

                //var published = bundle["includeSourceCode"]?.Value<bool>() == true ? true : false; // OLD

                var published = bundle["published"]?.Value<bool>() == true ? true : false;


                // Namespaces
                bool generateMissingNamespaces = false;
                if (generateMissingNamespacesVal != null && generateMissingNamespacesVal.HasValue) {
                    generateMissingNamespaces = generateMissingNamespacesVal.Value;
                }

                // Remove Code?
                bool includeSourceCode = false;
                if (includeSourceCodeVal != null && includeSourceCodeVal.HasValue) {
                    includeSourceCode = includeSourceCodeVal.Value;
                }


                Console.WriteLine("*****************************************");
                Console.WriteLine("Create Documentation Package: ");
                Console.WriteLine("Project Base Bath: " + projectBasePath);
                Console.WriteLine("Project Bin Bath: " + projectBinPath);
                Console.WriteLine("Path: " + path);
                Console.WriteLine("Extension: " + extension);
                Console.WriteLine("Prefix: " + prefix);
                Console.WriteLine("Description: " + description);
                Console.WriteLine("Published: " + published);

                // Package
                var package = DocumentationPackage.ParseFromDirectory(
                    path: path,
                    extension: extension,
                    prefix: prefix,
                    description: description,
                    generateMissingNamespaceItems: generateMissingNamespaces,
                    name: name,
                    includeSourceCode: includeSourceCode,
                    published: published,
                    privateMembersAreHidden: privateMembersAreHidden);
               
                packages.Add(package);

            }

            // Write Documentation Files
            Console.WriteLine("Writing Documentation Packages to " + destination + "...");
            Console.WriteLine($"\t export type is {outputFormatString}");
            DocumentationPackage.WriteDocumentations(packages, destination, clearDestinationFolder.Value, exportFormat: exportFormat);
            Console.WriteLine("done!");
        }




        private static JObject LoadPackageConfiguration(string path) {
           // var path = Core.Config.StaticPath + Core.Config.PathSep + "asset-packager" + Core.Config.PathSep + packageConfigurationName;
            if (File.Exists(path)) {
                return JObject.Parse(File.ReadAllText(path));
            }
            throw new FileNotFoundException(path);
        }




    }
}
