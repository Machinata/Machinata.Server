using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Module.Transfer.Logic;
using Machinata.Module.Transfer.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Machinata.Module.Transfer.Handler.Page {

    public class TransferAdminPageHandler : Admin.Handler.AdminPageTemplateHandler {


        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("transfer");
        }

        #region Menu Item
        
        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "upload-to-the-cloud",
                Path = "/admin/transfer",
                Title = "{text.transfer}",
                Sort = "500"
            });
        }

        #endregion

        [RequestHandler("/admin/transfer")]
        public void Default() {

            var entities = this.DB.Transmissions()
                            .Include(nameof(Model.Transmission.Files))
                            .Include(nameof(Model.Transmission.Recipients))
                            .Include(nameof(Model.Transmission.Sender))
                            .AsQueryable();

            entities = this.Template.Paginate(entities, this,nameof(ModelObject.Created),"desc",1,10);

            this.Template.InsertEntityList(
                variableName: "entities",
                entities: entities,
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/transfer/transmissions/transmission/{entity.public-id}"
                );

            // Navigation
            this.Navigation.Add("transfer", "Transfer","Transfer (Beta)");

        }
 

        [RequestHandler("/admin/transfer/transmissions/transmission/{publicId}")]
        public void Transmission(string publicId) {
            var entity = this.DB.Transmissions()
                            .Include(nameof(Model.Transmission.Files))
                            .Include(nameof(Model.Transmission.Recipients))
                            .Include(nameof(Model.Transmission.Sender))
                            .GetByPublicId(publicId);

            this.Template.InsertPropertyList(
                variableName: "entity",
                entity: entity,
                form: new FormBuilder(Forms.Admin.VIEW),
                loadFirstLevelReferences: true
             );

            this.Template.InsertVariables("entity", entity);

            // Files
            this.Template.InsertEntityList(
              variableName: "files",
              entities: entity.Files.AsQueryable(),
              form: new FormBuilder(Forms.Admin.LISTING),
              link: "/transfer/" + entity.ShareId + "/file/{entity.public-id}"
              );

            // Recipients
            this.Template.InsertEntityList(
              variableName: "recipients",
              entities: entity.Recipients.AsQueryable(),
              form: new FormBuilder(Forms.Admin.LISTING)
              );


            // Navigation
            this.Navigation.Add("transfer");
            this.Navigation.Add("transmissions/transmission/" + entity.PublicId, entity.ToString());
            
        }

     

        [RequestHandler("/admin/transfer/transmissions/transmission/{publicId}/edit")]
        public void Edit(string publicId) {

            var entity = this.DB.Transmissions().GetByPublicId(publicId);

            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/transfer/transmission/{publicId}/edit",
                onSuccess: $"/admin/transfer/transmissions/transmission/{publicId}"
            );

            // Navigation
            Navigation.Add("transfer");
            Navigation.Add("transmission/" + entity.PublicId, entity.ToString());
            Navigation.Add("edit");

        }


     



    }
}
