using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Module.Crawler.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using Machinata.Core.Util;
namespace Machinata.Module.Crawler {


    // Sample response with the siri and trias namspaces
    /*
    <? xml version="1.0" encoding="UTF-8"?>
<trias:Trias xmlns:trias="http://www.vdv.de/trias" xmlns:acsb="http://www.ifopt.org.uk/acsb" xmlns:datex2="http://datex2.eu/schema/1_0/1_0" xmlns:ifopt="http://www.ifopt.org.uk/ifopt" xmlns:siri="http://www.siri.org.uk/siri" version="1.1">
   <trias:ServiceDelivery>
      <siri:ResponseTimestamp>2020-07-01T08:50:05Z</siri:ResponseTimestamp>
      <siri:ProducerRef>EFAController10.4.5.58-OJP-EFA01-P</siri:ProducerRef>
      <siri:Status>true</siri:Status>
      <trias:Language>de</trias:Language>
      <trias:CalcTime>217</trias:CalcTime>
      <trias:DeliveryPayload>
         <trias:StopEventResponse>
            <trias:StopEventResponseContext>
               <trias:Situations />
            </trias:StopEventResponseContext>
            <trias:StopEventResult>
               <trias:ResultId>ID-4DEF93D4-8CF9-4F77-B89B-CC21387FB2B2</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T07:17:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:31:52518</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-03E1AAE2-BE02-46B2-A95E-650915145F60</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T07:39:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:6:52511</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-B7F3A1B4-003F-49E4-94BA-FC50D842A8A8</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T08:17:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:32:52522</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-A2F557C0-391E-48A6-9B73-E1192B4DC505</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T08:39:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:7:52513</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-1B66F6B8-0E78-47E6-9144-808654634FA6</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T09:17:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:33:52526</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-6976F62F-8843-4C00-9C5B-21DC473F042C</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T09:39:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:8:52515</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-2AA6E026-8EE2-40EE-8D81-5ED30C61FAB2</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T09:47:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:34:52530</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-FAE70E7A-77D4-4019-AF92-60AC6F8C5644</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T10:09:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:9:52517</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-3E92D7AB-D3A2-4D75-BF92-F5BBDB28EB53</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T10:17:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:35:52532</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-C5D9F689-4925-46E7-AB2E-11AF48A91D80</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T10:39:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:10:52519</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-E008FCDB-FDBF-403F-A395-95B73CB98A40</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T10:47:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:36:52536</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-0B6565AB-6046-4F09-ABA5-CC73C2E69A00</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T11:09:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:11:52521</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-DF86D196-121E-4269-9840-32DB3A58B755</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T11:17:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:37:52538</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-DBBF28C4-0B87-452F-B02A-C819945A4371</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T11:39:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:12:52523</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-A5295AD3-7533-426A-AE98-FCA76957BD19</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T12:17:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:38:52542</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-DDE1B790-7197-4E32-891D-7B3420C3E6CB</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T12:39:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:13:52525</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-F098CC2F-2A87-4EBB-8BA1-77B5D2C8935B</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T13:17:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:39:52546</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-C4B200EE-ECFD-4DB3-B49D-5A5D6D165C26</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T13:39:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:14:52527</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-EAEE0D5A-D403-430B-BDE9-8A5E667FEC69</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T14:17:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:40:52550</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-2881A457-03CE-4472-B339-5166DE4636B5</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T14:39:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:15:52529</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-ABCFFD71-7590-4851-B6BA-CF974E2A0C4F</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T14:47:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:41:52554</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-19ED0281-B7DD-4AC3-9605-222995B66C6C</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T15:09:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:16:52531</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-C6442A80-4913-4CD3-ADCB-68133A5370D6</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T15:17:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:42:52556</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-4C720531-1AB5-4E9A-BF25-D41D43B11D8F</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T15:39:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:17:52533</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-A9A20C9B-AADD-4189-B543-0FA4A850E06F</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T15:47:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:43:52560</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-FB3DCCF0-C059-47DF-9CC1-3A72EDDBE2D4</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T16:09:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:18:52535</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-2C16281F-747C-4700-B6DC-FA11994180B2</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T16:17:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:44:52562</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-4F734245-89D2-407D-AF8F-1A595901C8A6</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T16:39:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:19:52537</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-29B87617-91C7-4D33-8531-A0B3A8E9440A</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T16:47:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:45:52566</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-702E6FDB-A955-4784-B431-8C5B6430ADFF</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T17:09:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:20:52539</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-27E1E150-2AFA-4C4D-8B6B-8A88A41DB758</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T17:17:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:46:52568</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-92AC47C7-EE84-4499-8B5C-BA2200C5B49B</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T17:39:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:21:52541</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-E17ECA9E-BB75-4804-89A1-365C50B5FEC0</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T17:47:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:47:52572</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-FCCB06EB-F54B-4EEF-B448-53B5561DB9CF</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T18:09:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:22:52543</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-4040518A-A1DB-4DAD-8217-DFD7BCE77F36</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T18:17:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:48:52574</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-665F032A-604D-4F79-9870-A288FD2C7032</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T18:39:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:23:52545</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-CACE3C29-4CD6-42C3-88B8-392809D827D4</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T19:17:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:49:52578</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-1313C8A2-1826-43B3-BAEF-24321EF1E21F</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T19:39:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:24:52547</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-B808F902-A550-4D6E-A399-D438924F0E2F</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T20:17:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:50:52582</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-44263EE5-C5C9-4673-A29E-93141793AEA3</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-01T20:39:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-01</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:25:52549</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-12E50429-C246-4479-A27A-DD6B9DC965EA</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-02T04:17:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-02</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:26:52502</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-B50279F8-EAB4-48D1-B493-D688A5C4EC0D</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-02T04:39:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-02</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:1:52501</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-36834274-6187-4CF5-91D4-5CDEB410D5FE</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-02T04:47:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-02</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:27:52506</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-99DFECEB-4851-49A8-B4AE-16A031EB243A</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-02T05:09:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-02</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:2:52503</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-ACEFA963-7D6A-455B-9C6B-E3A94AF1F183</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-02T05:17:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-02</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:28:52508</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-DFB60379-505E-40A5-B3CD-563C5359B10E</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-02T05:39:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-02</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:3:52505</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-A28E9699-981B-46A0-83CC-8B506C0CAA90</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-02T05:47:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-02</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:29:52512</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-6A914F26-939B-41D1-8955-AAA1858B35BE</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-02T06:09:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-02</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:4:52507</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-8F907819-2FB8-4D74-A7EC-B1F863BA7B59</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-02T06:17:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>8</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-02</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:R:j20:30:52514</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:R</trias:LineRef>
                     <trias:DirectionRef>return</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8503640</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8574478</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
            <trias:StopEventResult>
               <trias:ResultId>ID-343810FC-13CE-4F2C-8516-B9F6E7F16F54</trias:ResultId>
               <trias:StopEvent>
                  <trias:ThisCall>
                     <trias:CallAtStop>
                        <trias:StopPointRef>8581710</trias:StopPointRef>
                        <trias:StopPointName>
                           <trias:Text>Lachen SZ, Eschenweg</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:StopPointName>
                        <trias:ServiceDeparture>
                           <trias:TimetabledTime>2020-07-02T06:39:00Z</trias:TimetabledTime>
                        </trias:ServiceDeparture>
                        <trias:StopSeqNumber>4</trias:StopSeqNumber>
                     </trias:CallAtStop>
                  </trias:ThisCall>
                  <trias:Service>
                     <trias:OperatingDayRef>2020-07-02</trias:OperatingDayRef>
                     <trias:JourneyRef>ojp:96242:5:H:j20:5:52509</trias:JourneyRef>
                     <trias:LineRef>ojp:96242:5:H</trias:LineRef>
                     <trias:DirectionRef>outward</trias:DirectionRef>
                     <trias:Mode>
                        <trias:PtMode>bus</trias:PtMode>
                        <trias:BusSubmode>localBus</trias:BusSubmode>
                        <trias:Name>
                           <trias:Text>Bus</trias:Text>
                           <trias:Language>de</trias:Language>
                        </trias:Name>
                     </trias:Mode>
                     <trias:PublishedLineName>
                        <trias:Text>525</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:PublishedLineName>
                     <trias:OperatorRef>ojp:801</trias:OperatorRef>
                     <trias:OriginStopPointRef>8574478</trias:OriginStopPointRef>
                     <trias:OriginText>
                        <trias:Text>Lachen SZ, Bahnhof</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:OriginText>
                     <trias:DestinationStopPointRef>8503640</trias:DestinationStopPointRef>
                     <trias:DestinationText>
                        <trias:Text>Siebnen, Schulhaus</trias:Text>
                        <trias:Language>de</trias:Language>
                     </trias:DestinationText>
                  </trias:Service>
               </trias:StopEvent>
            </trias:StopEventResult>
         </trias:StopEventResponse>
      </trias:DeliveryPayload>
   </trias:ServiceDelivery>
</trias:Trias>

        */



    /// <summary>
    /// This is the new Implementation for the new version of the TRIAS avaiable since July 2020
    /// Main changes from: OpenTransportDataTimetableWebCrawler -> new namespaces in xml elements and the Liniennamen
    /// 
    /// 
    /// Documentation
    /// https://opentransportdata.swiss/de/cookbook/abfahrts-ankunftsanzeiger/
    /// https://opentransportdata.swiss/de/cookbook/open-journey-planner-ojp/ojp-und-die-neue-trias-und-gtfs-schnittstelle/
    /// 
    /// Permalink to current DIdok file, udpated dailly:
    /// https://opentransportdata.swiss/de/dataset/bav_liste/permalink
    /// 
    /// 
    /// Email from SBB confirming changes:
    ///  
    /// 
    /// Lutz Richard SBB CFF FFS<richard.lutz@sbb.ch>
    /// 22.06.2020, 14:57 (vor 10 Tagen)
    /// an mich; Fachstelle

    /// Hallo Micha
    /// Danke für deine Anfrage
    /// Du solltest etwa 14:52 eine Anfrage von Trello erhalten haben.Ansonsten versuchs mit diesem Link: https://trello.com/invite/b/jiIZwszF/92193f66878ff6accfa575137c255699/open-journey-planner

    /// Ja, die …/trias wird ab August ausser Betrieb genommen und die neue …/trias2020 ist ab Juli verfügbar.Einzig die NamespacePräfix «trias:» und «siri:» werden noch neu verwendet. Ansonsten ist die Schnittstelle – nach aktuellem Kenntnisstand  - identisch.

    /// En Gruess Rich
    /// Hinweis micha: Die Liniennamen haben sich geändert. --> therefore  changes is in the ConvertODTXXXToZVV methods
    /// 
    /// 
    /// 
    /// 
    /// </summary>
    /// <seealso cref="Machinata.Module.Crawler.WebCrawler" />
    /// 
    public class OpenTransportDataTimetableWebCrawler2020 : TimetableCrawler {

    #region Class Logger
    /// <summary>
    /// The logger helper for this class. Use this logger instance anytime you want to
    /// log something from within this class.
    /// </summary>
    private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
    private static NLog.Logger _loggerSQL = NLog.LogManager.GetLogger("SQL");
        private string APIKey;
        #endregion


    public const string SERVER_URL = "https://api.opentransportdata.swiss";
    public const string TIMETABLE_REQUEST = "/trias2020";


    /// <summary>
    /// Generats the request XML.
    /// HINT: dont change white spaces etc...might make it incompatible with the Webservice
    /// </summary>
    /// <param name="stopPointref">The stop pointref.</param>
    /// <param name="time">The time.</param>
    /// <param name="maxConnections">The maximum connections.</param>
    /// <returns></returns>
    public static string GetRequestXML(string stopPointref, DateTime time, int maxConnections) {
        var request = string.Format(@"<?xml version=""1.0"" encoding=""UTF-8""?>
<Trias version=""1.1"" xmlns=""http://www.vdv.de/trias"" xmlns:siri=""http://www.siri.org.uk/siri"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
<ServiceRequest>
    <siri:RequestTimestamp>2016-06-27T13:34:00</siri:RequestTimestamp>
    <siri:RequestorRef>EPSa</siri:RequestorRef>
    <RequestPayload>
        <StopEventRequest>
            <Location>
                <LocationRef>
                    <StopPointRef>{0}</StopPointRef>
                </LocationRef>
<DepArrTime>{1}</DepArrTime>        

</Location>
            <Params><NumberOfResults>{2}</NumberOfResults><StopEventType>departure</StopEventType>
<IncludePreviousCalls>false</IncludePreviousCalls><IncludeOnwardCalls>false</IncludeOnwardCalls><IncludeRealtimeData>true</IncludeRealtimeData>  
          </Params>
        </StopEventRequest>
    </RequestPayload>
</ServiceRequest>
</Trias>
", stopPointref, time.ToString("yyyy-MM-ddTHH:mm:ss"),
        maxConnections);
        return request;
    }

        public override WebClient GetWebClient(Encoding encoding = null) {
            var client =  base.GetWebClient(encoding);
            client.Headers[HttpRequestHeader.ContentType] =  "text/xml";
                client.Headers[HttpRequestHeader.Authorization] = this.APIKey;
            return client;
        }

        public OpenTransportDataTimetableWebCrawler2020() : base(SERVER_URL) {
            SetCrawlerProfile(Crawler.WebCrawler.CRAWLER_PROFILE_GOOGLEBOT_SEARCH);
            this.APIKey = Config.OTDTimetableAPIKey;
        }


        /// <summary>
        /// Use custom API, not the one from the config
        /// </summary>
        /// <param name="apiKey"></param>
        public OpenTransportDataTimetableWebCrawler2020(string apiKey) : base(SERVER_URL) {
            SetCrawlerProfile(Crawler.WebCrawler.CRAWLER_PROFILE_GOOGLEBOT_SEARCH);
            this.APIKey = apiKey;
        }


        public string GetCopyright() {
            return $"© Open-Data-Plattform öV Schweiz {DateTime.Now.Year}";
        }

        /// <summary>
        /// Gets the timetable for station.
        /// </summary>
        /// <param name="stationId">The didok station identifier.</param>
        /// <param name="dateTime">datetime in utc</param>
        /// <param name="maxConnections">The maximum connections.</param>
        /// <returns></returns>
        public override Timetable GetTimetableForStation(string stationId, DateTime dateTime, int maxConnections = 8, DateTime? now= null, bool filterOutPast = true) {
        var ret = new Timetable();

        ret.TimetableDownload = "https://opentransportdata.swiss/de/";
        ret.TimetableURL = "https://opentransportdata.swiss/de/";
        ret.LegalText = this.GetCopyright();

        if (now == null) {
            now = DateTime.UtcNow;
        }

        var postDataString = GetRequestXML(stationId, dateTime, maxConnections);
        var xml = PostString(TIMETABLE_REQUEST, postDataString, Encoding.UTF8);

        // Parse out all data

        var data = XElement.Parse(xml);
        var stopEventResults = data.ElementAnyNS("ServiceDelivery").ElementAnyNS("DeliveryPayload").ElementAnyNS("StopEventResponse").ElementsAnyNS("StopEventResult");

        foreach (var stopEventResult in stopEventResults) {
            XElement stopEvent = null;
            try {

                stopEvent = stopEventResult.ElementAnyNS("StopEvent");

                var serviceElement = stopEvent.ElementAnyNS("Service");
                var thisCall = stopEvent.ElementAnyNS("ThisCall");
                var mode = serviceElement.ElementAnyNS("Mode").ElementAnyNS("Name").ElementAnyNS("Text").Value;
                var lineName = serviceElement.ElementAnyNS("PublishedLineName").ElementAnyNS("Text").Value;
                var type = serviceElement.ElementAnyNS("Mode").ElementAnyNS("PtMode").Value;

                var destination = serviceElement.ElementAnyNS("DestinationText").ElementAnyNS("Text").Value;
                var thisCallName = thisCall.ElementAnyNS("CallAtStop").ElementAnyNS("StopPointName").ElementAnyNS("Text").Value;

                if (string.IsNullOrEmpty(ret.Name)) {
                    ret.Name = thisCallName;
                }

                // Estimated only available if realtime data
                // TODO: show estimated or timetable time?
                var departure = thisCall.ElementAnyNS("CallAtStop").ElementAnyNS("ServiceDeparture").ElementAnyNS("TimetabledTime").Value;
                var departureDate = DateTime.ParseExact(departure, "yyyy-MM-ddTHH:mm:ssZ", System.Globalization.CultureInfo.InvariantCulture);
                string shortSymbol = ConvertToShortSymbol(mode, lineName);

                var platform = thisCall.ElementAnyNS("CallAtStop").ElementAnyNS("EstimatedBay")?.ElementAnyNS("Text")?.Value;
                if (string.IsNullOrEmpty(platform)== true) {
                    platform = thisCall.ElementAnyNS("CallAtStop").ElementAnyNS("PlannedBay")?.ElementAnyNS("Text")?.Value;
                }

                var connection = new TimetableConnection();
                connection.Name = "TODO";
                connection.ProductName = $"{mode}  {lineName}";
                connection.ProductLine = shortSymbol;
                connection.ProductType = type;
                connection.ProductIcon = type;
                connection.ProductColorBG = "#FFFFFF"; // TODO: not available from ODT
                connection.ProductColor = "#FFFFFF"; // TODO: not available from ODT
                connection.ProductColorFG = "#000000"; // TODO: not available from ODT
                connection.ProductDirection = destination;
                connection.LocationName = thisCallName;
                connection.LocationTime = departureDate.ToString("HH:mm");
                connection.LocationDate = departureDate.ToString("dd.MM.yy");
                connection.LocationDateTime = departureDate;
                connection.LocationPlatform = platform;
                connection.LocationCountdown = CalculateCountdownMinutes(departureDate, now.Value).ToString();
                connection.ProductIcon = ConvertODTIconToZVV(mode, type);
                connection.ProductType = ConvertODTypeToZVV(mode, type).ToString();
                if (connection.ProductLine != null) {
                    connection.ProductLine = connection.ProductLine.Replace(" ", "").Trim(' ', '_', '-');
                }


                // Suffix ", Bahnof" if its a train station and doesnt end with "
                if (connection.ProductType == "1") {
                    var trainStationSuffix = Core.Localization.Text.GetTranslatedTextByIdForLanguage("timetable.trainstation", Core.Config.LocalizationDefaultLanguage, "Bahnof");
                    if(connection.LocationName != null && connection.LocationName.EndsWith(trainStationSuffix) == false) {

                        var containsException = Config.AutoStationSuffixExceptions.Any(e => connection.LocationName.ToLower().EndsWith(e.ToLower()));
                        if (containsException == false) {
                            connection.LocationName += ", " + trainStationSuffix;
                        }
                    }
                }


                // Connection of the past
                if (filterOutPast == true && connection.LocationDateTime < now) {
                    continue;
                }

                ret.Connections.Add(connection);
            } catch (Exception e1) {
                _logger.Warn(e1, "Could not parse connection: " + stopEvent?.ToString());
                throw new BackendException("odt-error", $"Could not process data for station: {stationId}", e1);
            }
        }

        return ret;
    }

    private long CalculateCountdownMinutes(DateTime locationTime, DateTime dateTime) {
        var diff = locationTime - dateTime;
        return (long)System.Math.Round(diff.TotalMinutes);

    }

    private static string ConvertODTIconToZVV(string mode, string icon) {
        var result = icon.Trim(' ', '_', '-');
        if (icon == "rail") {
            result = "train";
        } else if (mode == "Schiff") {
            result = "ship";
        }
        return result;
    }

    private static int ConvertODTypeToZVV(string mode, string type ) {
        if (type == "bus") {
            return 6;
        }
        else if (type == "tram") {
            return 9;
        } else if (type == "rail") {
            if (mode!= null && mode.Contains("S-Bahn")) {
                return 5;
            }
            return 1;
        }
        // unknown/not mapped yet
        return -1;
    }

    /// <summary>
    /// Converts ODT Mode like (e.g. 'S-Bahn' ) and LineName (e.g. '7') to 'S7'
    /// </summary>
    public static string ConvertToShortSymbol(string mode, string lineName) {
        var shortSymbol = lineName;
            // This seems to have changed in Fall2020, the short symbol is already like S15
            if (mode != null) {
              
                //if (mode.Contains("S-Bahn")) {
                //    shortSymbol = "S" + shortSymbol;
                //} else if (mode.Contains("IR")) {
                //    shortSymbol = "IR" + shortSymbol;
                //} else if (mode.Contains( "IC")) {
                //    shortSymbol = "IC" + shortSymbol;
                //} else if (mode.Contains("RegioExpress")) {
                //    shortSymbol = "RE" + shortSymbol;
                //} else if (mode.Contains( "EC")) {
                //    shortSymbol = mode.Replace("EuroCity", "").Trim();
                //} else if (string.IsNullOrEmpty(shortSymbol) == true) {
                //    shortSymbol = mode; // e.g. ICE without lineName
                //}
            }

        return shortSymbol;
    }
}
}
