using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Module.Crawler.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using Machinata.Core.Util;
namespace Machinata.Module.Crawler {




    // Sample response Legacy as of july 2020
    /*
     <Trias xmlns="http://www.vdv.de/trias" version="1.1">
    <ServiceDelivery>
     <ResponseTimestamp xmlns="http://www.siri.org.uk/siri">2019-06-18T10:19:03Z</ResponseTimestamp>
     <ProducerRef xmlns="http://www.siri.org.uk/siri">EFAController10.2.9.62-WIN-G0NJHFUK71P</ProducerRef>
     <Status xmlns="http://www.siri.org.uk/siri">true</Status>
     <MoreData>false</MoreData>
     <Language>de</Language>
     <DeliveryPayload>
       <StopEventResponse>
         <StopEventResult>
           <ResultId>ID-38D93015-BB32-4975-B09C-86CE50437288</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503057</StopPointRef>
                 <StopPointName>
                   <Text>Uetliberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:05:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:05:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503056</StopPointRef>
                 <StopPointName>
                   <Text>Ringlikon</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:10:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:10:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:10:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:10:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503055</StopPointRef>
                 <StopPointName>
                   <Text>Uitikon Waldegg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:12:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:13:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:13:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:13:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:17:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:18:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:17:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:18:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:19:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:19:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:19:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:19:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:20:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:20:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:20:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:20:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:22:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:22:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>7</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:24:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:24:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:24:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:24:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>8</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:27:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:26:00Z</EstimatedTime>
                 </ServiceArrival>
                 <StopSeqNumber>9</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:26010:B:R:j19:12832:12832</JourneyRef>
               <LineRef>odp:26010:B:R</LineRef>
               <DirectionRef>return</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503057</OriginStopPointRef>
               <OriginText>
                 <Text>Uetliberg</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503088</DestinationStopPointRef>
               <DestinationText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-8E183612-008A-44A1-BEE9-848161593403</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:25:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:25:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:26:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:26:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:26:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:26:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:29:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:29:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:31:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:31:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:31:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:31:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:32:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:32:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:32:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:32:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:34:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:34:00Z</EstimatedTime>
                 </ServiceArrival>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:79010:B:H:j19:12835:12835</JourneyRef>
               <LineRef>odp:79010:B:H</LineRef>
               <DirectionRef>outward</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503088</OriginStopPointRef>
               <OriginText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503054</DestinationStopPointRef>
               <DestinationText>
                 <Text>Zürich Triemli</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-F366FCCE-4AD6-47AF-99EF-1078044A8E8F</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:35:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:35:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:36:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:36:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:36:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:36:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:39:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:39:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:41:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:41:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:41:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:41:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:42:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:42:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:42:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:42:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:44:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:44:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:44:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:44:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503055</StopPointRef>
                 <StopPointName>
                   <Text>Uitikon Waldegg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:48:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:48:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:48:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:48:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>7</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503056</StopPointRef>
                 <StopPointName>
                   <Text>Ringlikon</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:50:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:50:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:50:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:50:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>8</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503057</StopPointRef>
                 <StopPointName>
                   <Text>Uetliberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:55:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:54:00Z</EstimatedTime>
                 </ServiceArrival>
                 <StopSeqNumber>9</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:26010:B:H:j19:12837:12837</JourneyRef>
               <LineRef>odp:26010:B:H</LineRef>
               <DirectionRef>outward</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503088</OriginStopPointRef>
               <OriginText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503057</DestinationStopPointRef>
               <DestinationText>
                 <Text>Uetliberg</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-715D6410-E7D4-4E77-B9F7-632A4AB238C6</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:37:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:37:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:39:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:39:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:39:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:39:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:40:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:40:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:40:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:40:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:42:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:42:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:44:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:44:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:44:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:44:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:47:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:46:00Z</EstimatedTime>
                 </ServiceArrival>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:79010:B:R:j19:12836:12836</JourneyRef>
               <LineRef>odp:79010:B:R</LineRef>
               <DirectionRef>return</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503054</OriginStopPointRef>
               <OriginText>
                 <Text>Zürich Triemli</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503088</DestinationStopPointRef>
               <DestinationText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-4AB51BE1-E23E-4EBC-A7BF-4138FD475195</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503057</StopPointRef>
                 <StopPointName>
                   <Text>Uetliberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:35:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:35:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503056</StopPointRef>
                 <StopPointName>
                   <Text>Ringlikon</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:40:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:40:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:40:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:40:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503055</StopPointRef>
                 <StopPointName>
                   <Text>Uitikon Waldegg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:42:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:42:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:43:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:43:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:47:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:46:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:47:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:47:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:49:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:49:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:49:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:49:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:50:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:50:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:50:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:50:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:52:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:52:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>7</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:54:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:54:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:54:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:54:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>8</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:57:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:56:00Z</EstimatedTime>
                 </ServiceArrival>
                 <StopSeqNumber>9</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:26010:B:R:j19:12838:12838</JourneyRef>
               <LineRef>odp:26010:B:R</LineRef>
               <DirectionRef>return</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503057</OriginStopPointRef>
               <OriginText>
                 <Text>Uetliberg</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503088</DestinationStopPointRef>
               <DestinationText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-C40B5B55-D481-499F-806B-8F889395C577</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:55:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:55:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T10:56:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:56:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:56:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:56:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T10:59:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T10:59:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:01:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:01:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:01:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:01:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:02:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:02:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:02:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:02:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:04:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:04:00Z</EstimatedTime>
                 </ServiceArrival>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:79010:B:H:j19:12841:12841</JourneyRef>
               <LineRef>odp:79010:B:H</LineRef>
               <DirectionRef>outward</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503088</OriginStopPointRef>
               <OriginText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503054</DestinationStopPointRef>
               <DestinationText>
                 <Text>Zürich Triemli</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-0CB7B37B-A238-40AF-B2AC-0295581A3D01</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:05:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:05:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:06:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:06:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:06:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:06:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:09:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:09:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:11:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:11:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:11:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:11:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:12:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:12:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:12:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:12:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:14:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:14:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:14:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:14:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503055</StopPointRef>
                 <StopPointName>
                   <Text>Uitikon Waldegg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:18:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:18:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:18:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:18:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>7</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503056</StopPointRef>
                 <StopPointName>
                   <Text>Ringlikon</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:20:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:20:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:20:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:20:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>8</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503057</StopPointRef>
                 <StopPointName>
                   <Text>Uetliberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:25:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:24:00Z</EstimatedTime>
                 </ServiceArrival>
                 <StopSeqNumber>9</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:26010:B:H:j19:12843:12843</JourneyRef>
               <LineRef>odp:26010:B:H</LineRef>
               <DirectionRef>outward</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503088</OriginStopPointRef>
               <OriginText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503057</DestinationStopPointRef>
               <DestinationText>
                 <Text>Uetliberg</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-19363972-322E-4372-947A-CAD0297A85E4</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:07:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:07:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:09:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:09:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:09:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:09:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:10:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:10:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:10:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:10:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:12:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:12:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:14:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:14:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:14:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:14:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:17:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:16:00Z</EstimatedTime>
                 </ServiceArrival>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:79010:B:R:j19:12842:12842</JourneyRef>
               <LineRef>odp:79010:B:R</LineRef>
               <DirectionRef>return</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503054</OriginStopPointRef>
               <OriginText>
                 <Text>Zürich Triemli</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503088</DestinationStopPointRef>
               <DestinationText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-0C201828-68B9-47DE-9927-28EFF1A88BCD</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503057</StopPointRef>
                 <StopPointName>
                   <Text>Uetliberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:05:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:05:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503056</StopPointRef>
                 <StopPointName>
                   <Text>Ringlikon</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:10:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:10:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:10:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:10:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503055</StopPointRef>
                 <StopPointName>
                   <Text>Uitikon Waldegg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:12:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:12:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:13:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:13:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:17:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:16:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:17:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:17:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:19:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:19:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:19:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:19:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:20:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:20:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:20:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:20:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:22:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:22:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>7</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:24:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:24:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:24:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:24:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>8</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:27:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:26:00Z</EstimatedTime>
                 </ServiceArrival>
                 <StopSeqNumber>9</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:26010:B:R:j19:12844:12844</JourneyRef>
               <LineRef>odp:26010:B:R</LineRef>
               <DirectionRef>return</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503057</OriginStopPointRef>
               <OriginText>
                 <Text>Uetliberg</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503088</DestinationStopPointRef>
               <DestinationText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-C8110CDF-EAF0-4ADE-90F5-49343FAEF58D</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:25:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:25:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:26:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:26:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:26:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:26:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:29:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:29:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:31:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:31:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:31:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:31:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:32:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:32:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:32:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:32:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:34:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:34:00Z</EstimatedTime>
                 </ServiceArrival>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:79010:B:H:j19:12847:12847</JourneyRef>
               <LineRef>odp:79010:B:H</LineRef>
               <DirectionRef>outward</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503088</OriginStopPointRef>
               <OriginText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503054</DestinationStopPointRef>
               <DestinationText>
                 <Text>Zürich Triemli</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-DB2A9704-E1CA-4E98-A3BB-AE320C803D04</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:35:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:35:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:36:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:36:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:36:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:36:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:39:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:39:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:41:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:41:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:41:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:41:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:42:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:42:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:42:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:42:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:44:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:44:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:44:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:44:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503055</StopPointRef>
                 <StopPointName>
                   <Text>Uitikon Waldegg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:48:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:48:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:48:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:48:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>7</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503056</StopPointRef>
                 <StopPointName>
                   <Text>Ringlikon</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:50:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:50:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:50:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:50:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>8</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503057</StopPointRef>
                 <StopPointName>
                   <Text>Uetliberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:55:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:54:00Z</EstimatedTime>
                 </ServiceArrival>
                 <StopSeqNumber>9</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:26010:B:H:j19:12849:12849</JourneyRef>
               <LineRef>odp:26010:B:H</LineRef>
               <DirectionRef>outward</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503088</OriginStopPointRef>
               <OriginText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503057</DestinationStopPointRef>
               <DestinationText>
                 <Text>Uetliberg</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-824D462F-F6AE-4C46-B961-DE59CBA0E4EF</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:37:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:37:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:39:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:39:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:39:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:39:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:40:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:40:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:40:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:40:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:42:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:42:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:44:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:44:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:44:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:44:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:47:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:46:00Z</EstimatedTime>
                 </ServiceArrival>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:79010:B:R:j19:12848:12848</JourneyRef>
               <LineRef>odp:79010:B:R</LineRef>
               <DirectionRef>return</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503054</OriginStopPointRef>
               <OriginText>
                 <Text>Zürich Triemli</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503088</DestinationStopPointRef>
               <DestinationText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-78DEDB03-114E-4142-805D-3AFB3FF395C8</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503057</StopPointRef>
                 <StopPointName>
                   <Text>Uetliberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:35:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:35:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503056</StopPointRef>
                 <StopPointName>
                   <Text>Ringlikon</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:40:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:40:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:40:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:40:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503055</StopPointRef>
                 <StopPointName>
                   <Text>Uitikon Waldegg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:42:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:42:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:43:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:43:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:47:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:46:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:47:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:47:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:49:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:49:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:49:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:49:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:50:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:50:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:50:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:50:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:52:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:52:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>7</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:54:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:54:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:54:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:54:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>8</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:57:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:56:00Z</EstimatedTime>
                 </ServiceArrival>
                 <StopSeqNumber>9</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:26010:B:R:j19:12850:12850</JourneyRef>
               <LineRef>odp:26010:B:R</LineRef>
               <DirectionRef>return</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503057</OriginStopPointRef>
               <OriginText>
                 <Text>Uetliberg</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503088</DestinationStopPointRef>
               <DestinationText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-CE7B0FF0-1EA7-4C3D-9AD4-8FF89C1FA46B</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:55:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:55:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T11:56:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:56:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:56:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:56:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T11:59:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T11:59:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:01:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:01:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:01:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:01:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:02:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:02:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:02:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:02:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:04:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:04:00Z</EstimatedTime>
                 </ServiceArrival>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:79010:B:H:j19:12853:12853</JourneyRef>
               <LineRef>odp:79010:B:H</LineRef>
               <DirectionRef>outward</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503088</OriginStopPointRef>
               <OriginText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503054</DestinationStopPointRef>
               <DestinationText>
                 <Text>Zürich Triemli</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-8F8C6EA1-E92E-48F3-9B87-5013664D4C84</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:05:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:05:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:06:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:06:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:06:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:06:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:09:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:09:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:11:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:11:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:11:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:11:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:12:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:12:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:12:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:12:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:14:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:14:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:14:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:14:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503055</StopPointRef>
                 <StopPointName>
                   <Text>Uitikon Waldegg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:18:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:18:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:18:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:18:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>7</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503056</StopPointRef>
                 <StopPointName>
                   <Text>Ringlikon</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:20:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:20:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:20:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:20:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>8</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503057</StopPointRef>
                 <StopPointName>
                   <Text>Uetliberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:25:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:24:00Z</EstimatedTime>
                 </ServiceArrival>
                 <StopSeqNumber>9</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:26010:B:H:j19:12855:12855</JourneyRef>
               <LineRef>odp:26010:B:H</LineRef>
               <DirectionRef>outward</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503088</OriginStopPointRef>
               <OriginText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503057</DestinationStopPointRef>
               <DestinationText>
                 <Text>Uetliberg</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-18573F36-3807-4F14-843E-EF7A6353E173</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:07:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:07:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:09:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:09:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:09:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:09:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:10:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:10:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:10:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:10:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:12:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:12:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:14:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:14:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:14:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:14:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:17:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:16:00Z</EstimatedTime>
                 </ServiceArrival>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:79010:B:R:j19:12854:12854</JourneyRef>
               <LineRef>odp:79010:B:R</LineRef>
               <DirectionRef>return</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503054</OriginStopPointRef>
               <OriginText>
                 <Text>Zürich Triemli</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503088</DestinationStopPointRef>
               <DestinationText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-66E1BC41-8B23-4BA6-B21F-64A47BA0DAC9</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503057</StopPointRef>
                 <StopPointName>
                   <Text>Uetliberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:05:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:05:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503056</StopPointRef>
                 <StopPointName>
                   <Text>Ringlikon</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:10:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:10:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:10:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:10:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503055</StopPointRef>
                 <StopPointName>
                   <Text>Uitikon Waldegg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:12:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:12:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:13:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:13:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:17:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:16:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:17:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:17:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:19:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:19:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:19:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:19:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:20:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:20:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:20:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:20:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:22:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:22:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>7</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:24:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:24:00Z</EstimatedTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:24:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:24:00Z</EstimatedTime>
                 </ServiceDeparture>
                 <StopSeqNumber>8</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:27:00Z</TimetabledTime>
                   <EstimatedTime>2019-06-18T12:26:00Z</EstimatedTime>
                 </ServiceArrival>
                 <StopSeqNumber>9</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:26010:B:R:j19:12856:12856</JourneyRef>
               <LineRef>odp:26010:B:R</LineRef>
               <DirectionRef>return</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503057</OriginStopPointRef>
               <OriginText>
                 <Text>Uetliberg</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503088</DestinationStopPointRef>
               <DestinationText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-4095BFDC-1FC4-4CB5-89F2-873D1A75AD44</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:25:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:26:00Z</TimetabledTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:26:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:29:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:31:00Z</TimetabledTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:31:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:32:00Z</TimetabledTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:32:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:34:00Z</TimetabledTime>
                 </ServiceArrival>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:79010:B:H:j19:12859:12859</JourneyRef>
               <LineRef>odp:79010:B:H</LineRef>
               <DirectionRef>outward</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503088</OriginStopPointRef>
               <OriginText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503054</DestinationStopPointRef>
               <DestinationText>
                 <Text>Zürich Triemli</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-0471B789-F69D-4100-ADAD-E5B2999084F5</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:35:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:36:00Z</TimetabledTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:36:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:39:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:41:00Z</TimetabledTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:41:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:42:00Z</TimetabledTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:42:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:44:00Z</TimetabledTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:44:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503055</StopPointRef>
                 <StopPointName>
                   <Text>Uitikon Waldegg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:48:00Z</TimetabledTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:48:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>7</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503056</StopPointRef>
                 <StopPointName>
                   <Text>Ringlikon</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:50:00Z</TimetabledTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:50:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>8</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503057</StopPointRef>
                 <StopPointName>
                   <Text>Uetliberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:55:00Z</TimetabledTime>
                 </ServiceArrival>
                 <StopSeqNumber>9</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:26010:B:H:j19:12861:12861</JourneyRef>
               <LineRef>odp:26010:B:H</LineRef>
               <DirectionRef>outward</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503088</OriginStopPointRef>
               <OriginText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503057</DestinationStopPointRef>
               <DestinationText>
                 <Text>Uetliberg</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
         <StopEventResult>
           <ResultId>ID-ED19ECDA-38DC-4B1F-BDFD-315F3CDE76C3</ResultId>
           <StopEvent>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503054</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Triemli</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:37:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>1</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503053</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Schweighof</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:39:00Z</TimetabledTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:39:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>2</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <PreviousCall>
               <CallAtStop>
                 <StopPointRef>8503052</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Friesenberg</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:40:00Z</TimetabledTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:40:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>3</StopSeqNumber>
               </CallAtStop>
             </PreviousCall>
             <ThisCall>
               <CallAtStop>
                 <StopPointRef>8503051</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Binz</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:42:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>4</StopSeqNumber>
               </CallAtStop>
             </ThisCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503090</StopPointRef>
                 <StopPointName>
                   <Text>Zürich Selnau</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:44:00Z</TimetabledTime>
                 </ServiceArrival>
                 <ServiceDeparture>
                   <TimetabledTime>2019-06-18T12:44:00Z</TimetabledTime>
                 </ServiceDeparture>
                 <StopSeqNumber>5</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <OnwardCall>
               <CallAtStop>
                 <StopPointRef>8503088</StopPointRef>
                 <StopPointName>
                   <Text>Zürich HB SZU</Text>
                   <Language>de</Language>
                 </StopPointName>
                 <PlannedBay>
                   <Text>22</Text>
                   <Language>de</Language>
                 </PlannedBay>
                 <ServiceArrival>
                   <TimetabledTime>2019-06-18T12:47:00Z</TimetabledTime>
                 </ServiceArrival>
                 <StopSeqNumber>6</StopSeqNumber>
               </CallAtStop>
             </OnwardCall>
             <Service>
               <OperatingDayRef>2019-06-18</OperatingDayRef>
               <JourneyRef>odp:79010:B:R:j19:12860:12860</JourneyRef>
               <LineRef>odp:79010:B:R</LineRef>
               <DirectionRef>return</DirectionRef>
               <Mode>
                 <PtMode>rail</PtMode>
                 <RailSubmode>suburbanRailway</RailSubmode>
                 <Name>
                   <Text>S-Bahn</Text>
                   <Language>de</Language>
                 </Name>
               </Mode>
               <PublishedLineName>
                 <Text>10</Text>
                 <Language>de</Language>
               </PublishedLineName>
               <OperatorRef>odp:78</OperatorRef>
               <OriginStopPointRef>8503054</OriginStopPointRef>
               <OriginText>
                 <Text>Zürich Triemli</Text>
                 <Language>de</Language>
               </OriginText>
               <DestinationStopPointRef>8503088</DestinationStopPointRef>
               <DestinationText>
                 <Text>Zürich HB SZU</Text>
                 <Language>de</Language>
               </DestinationText>
             </Service>
           </StopEvent>
         </StopEventResult>
       </StopEventResponse>
     </DeliveryPayload>
    </ServiceDelivery>
    </Trias>

          */


    // Sample Error for Opfikon at midnight
    /*<?xml version="1.0" encoding="UTF-8"?>
    <Trias xmlns="http://www.vdv.de/trias" version="1.1"><ServiceDelivery><ResponseTimestamp xmlns="http://www.siri.org.uk/siri">2019-11-01T10:18:43Z</ResponseTimestamp><ProducerRef xmlns="http://www.siri.org.uk/siri">EFAController10.2.9.62-WIN-G0NJHFUK71P</ProducerRef><Status xmlns="http://www.siri.org.uk/siri">true</Status><MoreData>false</MoreData><Language>de</Language><DeliveryPayload><StopEventResponse><ErrorMessage><Code>-4030</Code><Text><Text>STOPEVENT_LOCATIONUNSERVED</Text><Language>de</Language></Text></ErrorMessage></StopEventResponse></DeliveryPayload></ServiceDelivery></Trias>
    */


    /// <summary>
    /// Hint: this service will be shutdown in August 2020, 
    /// Please use OpenTransportDataTimetableWebCrawler2020
    /// See https://opentransportdata.swiss/de/cookbook/abfahrts-ankunftsanzeiger/
    /// 
    /// Permalink to current DIdok file, udpated dailly: https://opentransportdata.swiss/de/dataset/bav_liste/permalink
    /// 
    /// </summary>
    /// <seealso cref="Machinata.Module.Crawler.WebCrawler" />
    /// 
    public class OpenTransportDataTimetableWebCrawler : TimetableCrawler {

    #region Class Logger
    /// <summary>
    /// The logger helper for this class. Use this logger instance anytime you want to
    /// log something from within this class.
    /// </summary>
    private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
    private static NLog.Logger _loggerSQL = NLog.LogManager.GetLogger("SQL");
        private string APIKey;
        #endregion


    public const string SERVER_URL = "https://api.opentransportdata.swiss";
    public const string TIMETABLE_REQUEST = "/trias";


    /// <summary>
    /// Generats the request XML.
    /// HINT: dont change white spaces etc...might make it incompatible with the Webservice
    /// </summary>
    /// <param name="stopPointref">The stop pointref.</param>
    /// <param name="time">The time.</param>
    /// <param name="maxConnections">The maximum connections.</param>
    /// <returns></returns>
    public static string GetRequestXML(string stopPointref, DateTime time, int maxConnections) {
        var request = string.Format(@"<?xml version=""1.0"" encoding=""UTF-8""?>
<Trias version=""1.1"" xmlns=""http://www.vdv.de/trias"" xmlns:siri=""http://www.siri.org.uk/siri"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
<ServiceRequest>
    <siri:RequestTimestamp>2016-06-27T13:34:00</siri:RequestTimestamp>
    <siri:RequestorRef>EPSa</siri:RequestorRef>
    <RequestPayload>
        <StopEventRequest>
            <Location>
                <LocationRef>
                    <StopPointRef>{0}</StopPointRef>
                </LocationRef>
<DepArrTime>{1}</DepArrTime>        

</Location>
            <Params><NumberOfResults>{2}</NumberOfResults><StopEventType>departure</StopEventType>
<IncludePreviousCalls>false</IncludePreviousCalls><IncludeOnwardCalls>false</IncludeOnwardCalls><IncludeRealtimeData>true</IncludeRealtimeData>  
          </Params>
        </StopEventRequest>
    </RequestPayload>
</ServiceRequest>
</Trias>
", stopPointref, time.ToString("yyyy-MM-ddTHH:mm:ss"),
        maxConnections);
        return request;
    }

    public override WebClient GetWebClient(Encoding encoding = null) {
        var client =  base.GetWebClient(encoding);
        client.Headers[HttpRequestHeader.ContentType] =  "text/xml";
            client.Headers[HttpRequestHeader.Authorization] = this.APIKey;
        return client;
    }

        public OpenTransportDataTimetableWebCrawler() : base(SERVER_URL) {
            SetCrawlerProfile(Crawler.WebCrawler.CRAWLER_PROFILE_GOOGLEBOT_SEARCH);
            this.APIKey = Config.OTDTimetableAPIKey;
        }


        /// <summary>
        /// Use custom API, not the one from the config
        /// </summary>
        /// <param name="apiKey"></param>
        public OpenTransportDataTimetableWebCrawler(string apiKey) : base(SERVER_URL) {
            SetCrawlerProfile(Crawler.WebCrawler.CRAWLER_PROFILE_GOOGLEBOT_SEARCH);
            this.APIKey = apiKey;
        }



        public string GetCopyright() {
            return $"© Open-Data-Plattform öV Schweiz {DateTime.Now.Year}";
        }

        /// <summary>
        /// Gets the timetable for station.
        /// </summary>
        /// <param name="stationId">The didok station identifier.</param>
        /// <param name="dateTime">datetime in utc</param>
        /// <param name="maxConnections">The maximum connections.</param>
        /// <returns></returns>
        public override Timetable GetTimetableForStation(string stationId, DateTime dateTime, int maxConnections = 8, DateTime? now= null, bool filterOutPast = true) {
        var ret = new Timetable();

        ret.TimetableDownload = "https://opentransportdata.swiss/de/";
        ret.TimetableURL = "https://opentransportdata.swiss/de/";
        ret.LegalText = this.GetCopyright();

        if (now == null) {
            now = DateTime.UtcNow;
        }

        var postDataString = GetRequestXML(stationId, dateTime, maxConnections);
        var xml = PostString(TIMETABLE_REQUEST, postDataString, Encoding.UTF8);

        // Parse out all data

        var data = XElement.Parse(xml);
        var stopEventResults = data.ElementAnyNS("ServiceDelivery").ElementAnyNS("DeliveryPayload").ElementAnyNS("StopEventResponse").Elements();

        foreach (var stopEventResult in stopEventResults) {
            XElement stopEvent = null;
            try {

                stopEvent = stopEventResult.ElementAnyNS("StopEvent");

                var serviceElement = stopEvent.ElementAnyNS("Service");
                var thisCall = stopEvent.ElementAnyNS("ThisCall");
                var mode = serviceElement.ElementAnyNS("Mode").ElementAnyNS("Name").ElementAnyNS("Text").Value;
                var lineName = serviceElement.ElementAnyNS("PublishedLineName").ElementAnyNS("Text").Value;
                var type = serviceElement.ElementAnyNS("Mode").ElementAnyNS("PtMode").Value;

                var destination = serviceElement.ElementAnyNS("DestinationText").ElementAnyNS("Text").Value;
                var thisCallName = thisCall.ElementAnyNS("CallAtStop").ElementAnyNS("StopPointName").ElementAnyNS("Text").Value;

                if (string.IsNullOrEmpty(ret.Name)) {
                    ret.Name = thisCallName;
                }

                // Estimated only available if realtime data
                // TODO: show estimated or timetable time?
                var departure = thisCall.ElementAnyNS("CallAtStop").ElementAnyNS("ServiceDeparture").ElementAnyNS("TimetabledTime").Value;
                var departureDate = DateTime.ParseExact(departure, "yyyy-MM-ddTHH:mm:ssZ", System.Globalization.CultureInfo.InvariantCulture);
                string shortSymbol = ConvertToShortSymbol(mode, lineName);

                var platform = thisCall.ElementAnyNS("CallAtStop").ElementAnyNS("EstimatedBay")?.ElementAnyNS("Text")?.Value;
                if (string.IsNullOrEmpty(platform)== true) {
                    platform = thisCall.ElementAnyNS("CallAtStop").ElementAnyNS("PlannedBay")?.ElementAnyNS("Text")?.Value;
                }

                var connection = new TimetableConnection();
                connection.Name = "TODO";
                connection.ProductName = $"{mode}  {lineName}";
                connection.ProductLine = shortSymbol;
                connection.ProductType = type;
                connection.ProductIcon = type;
                connection.ProductColorBG = "#FFFFFF"; // TODO: not available from ODT
                connection.ProductColor = "#FFFFFF"; // TODO: not available from ODT
                connection.ProductColorFG = "#000000"; // TODO: not available from ODT
                connection.ProductDirection = destination;
                connection.LocationName = thisCallName;
                connection.LocationTime = departureDate.ToString("HH:mm");
                connection.LocationDate = departureDate.ToString("dd.MM.yy");
                connection.LocationDateTime = departureDate;
                connection.LocationPlatform = platform;
                connection.LocationCountdown = CalculateCountdownMinutes(departureDate, now.Value).ToString();
                connection.ProductIcon = ConvertODTIconToZVV(mode, type);
                connection.ProductType = ConvertODTypeToZVV(mode, type).ToString();
                if (connection.ProductLine != null) connection.ProductLine = connection.ProductLine.Replace(" ", "").Trim(' ', '_', '-');
                ret.Connections.Add(connection);
            } catch (Exception e1) {
                _logger.Warn(e1, "Could not parse connection: " + stopEvent?.ToString());
                throw new BackendException("odt-error", $"Could not process data for station: {stationId}", e1);
            }
        }

        return ret;
    }

    private long CalculateCountdownMinutes(DateTime locationTime, DateTime dateTime) {
        var diff = locationTime - dateTime;
        return (long)System.Math.Round(diff.TotalMinutes);

    }

    private static string ConvertODTIconToZVV(string mode, string icon) {
        var result = icon.Trim(' ', '_', '-');
        if (icon == "rail") {
            result = "train";
        } else if (mode == "Schiff") {
            result = "ship";
        }
        return result;
    }

    private static int ConvertODTypeToZVV(string mode, string type ) {
        if (type == "bus") {
            return 6;
        }
        else if (type == "tram") {
            return 9;
        } else if (type == "rail") {
            if (mode == "S-Bahn") {
                return 5;
            }
            return 1;
        }
        // unknown/not mapped yet
        return -1;
    }

    /// <summary>
    /// Converts ODT Mode like (e.g. 'S-Bahn' ) and LineName (e.g. '7') to 'S7'
    /// </summary>
    public static string ConvertToShortSymbol(string mode, string lineName) {
        var shortSymbol = lineName;
        if (mode == "S-Bahn") {
            shortSymbol = "S" + shortSymbol;
        } else if (mode == "InterRegio") {
            shortSymbol = "IR" + shortSymbol;
        } else if (mode == "Intercity") {
            shortSymbol = "IC" + shortSymbol;
        } else if (mode == "RegioExpress") {
            shortSymbol = "RE" + shortSymbol;
        }else if (string.IsNullOrEmpty(shortSymbol) == true) {
            shortSymbol = mode; // e.g. ICE without lineName
        }

        return shortSymbol;
    }

       
    }
}
