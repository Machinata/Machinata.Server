﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Crawler.Model {
    public abstract class TimetableCrawler : WebCrawler {

        public abstract Timetable GetTimetableForStation(string stationId, DateTime dateTime, int maxConnections, DateTime? now, bool filterOutPast);


        public TimetableCrawler(string baseURL) :base (baseURL){
          
        }
    }
}
