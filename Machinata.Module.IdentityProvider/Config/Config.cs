using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.IdentityProvider {
    public class Config {
        //public static int YourModuleConfig = Core.Config.GetIntSetting("YourModuleConfig", 0);



        public static string IdentityProviderGoogleClientID = Core.Config.GetStringSetting("IdentityProviderGoogleClientID", null);
        public static string IdentityProviderMicrosoftClientID = Core.Config.GetStringSetting("IdentityProviderMicrosoftClientID", null);
        public static string IdentityProviderFacebookClientID = Core.Config.GetStringSetting("IdentityProviderFacebookClientID", null);
        public static string IdentityProviderAppleClientID = Core.Config.GetStringSetting("IdentityProviderAppleClientID", null);
        public static string IdentityProviderAppleClientSecret = Core.Config.GetStringSetting("IdentityProviderAppleClientSecret", null);
        public static string IdentityProviderAppleClientRedirectURL = Core.Config.GetStringSetting("IdentityProviderAppleClientRedirectURL", null);
        public static string IdentityProviderAppleClientKeyId = Core.Config.GetStringSetting("IdentityProviderAppleClientKeyId", null);
        public static string IdentityProviderAppleClientTeamId = Core.Config.GetStringSetting("IdentityProviderAppleClientTeamId", null);
       


    }
}
