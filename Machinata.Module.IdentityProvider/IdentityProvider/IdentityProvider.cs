﻿using Google.Apis.Auth;
using Machinata.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.IdentityProvider {
    
    public abstract class IdentityProvider {

        public const string LoginProviderRocketList = "RocketList";
        public const string LoginProviderGoogle = "Google";
        public const string LoginProviderFacebook = "Facebook";
        public const string LoginProviderMicrosoft = "Microsoft";
        public const string LoginProviderApple = "Apple";

        public class UserInformation {
            public string UserName { get; set; }
            public string Email { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string ProfileImage { get; set; }
            public string ProviderId { get; set; }
        }

        public abstract UserInformation ValidateTokenAndGetUserInformation(string token, System.Web.HttpContext context);

        public static IdentityProvider GetProviderByName(string name) {
            IdentityProvider provider = null;
            if (name == LoginProviderGoogle) provider = new Module.IdentityProvider.GoogleIdentityProvider();
            else if (name == LoginProviderFacebook) provider = new Module.IdentityProvider.FacebookIdentityProvider();
            else if (name == LoginProviderMicrosoft) provider = new Module.IdentityProvider.MicrosoftIdentityProvider();
            else if (name == LoginProviderApple) provider = new Module.IdentityProvider.AppleIdentityProvider();
            else throw new BackendException("invalid-identity-provider","The identity provider "+name+" is not valid!");
            return provider;
        }
    }
}
