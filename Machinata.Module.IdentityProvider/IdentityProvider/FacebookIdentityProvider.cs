﻿using Google.Apis.Auth;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.IdentityProvider {
    public class FacebookIdentityProvider : IdentityProvider {

        public override UserInformation ValidateTokenAndGetUserInformation(string token, System.Web.HttpContext context) {

            using (var client = new HttpClient()) {
                // Make request
                client.BaseAddress = new Uri("https://graph.facebook.com");
                HttpResponseMessage response = client.GetAsync($"me?fields=picture,first_name,last_name,email&access_token={token}").Result;
                response.EnsureSuccessStatusCode();
                string responseJSON = response.Content.ReadAsStringAsync().Result;
                var responseJObject = Core.JSON.ParseJsonAsJObject(responseJSON);

                // Repack as user info object and return
                var userInfo = new UserInformation();
                userInfo.FirstName = responseJObject.Value<string>("first_name");
                userInfo.LastName = responseJObject.Value<string>("last_name");
                userInfo.Email = responseJObject.Value<string>("email");
                userInfo.ProfileImage = responseJObject.Value<string>("picture");
                userInfo.ProviderId = responseJObject.Value<string>("id");
                return userInfo;
            }


        }
    }
}
