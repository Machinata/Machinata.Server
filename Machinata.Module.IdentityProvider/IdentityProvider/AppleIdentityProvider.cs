﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using Machinata.Core.Messaging;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Asn1;
using System.Security.Cryptography;

namespace Machinata.Module.IdentityProvider {


    /// <summary>
    ///  WIP yes tested
    ///  https://developer.apple.com/documentation/sign_in_with_apple/sign_in_with_apple_rest_api/verifying_a_user
    /// 
    /// </summary>
    public class AppleIdentityProvider : IdentityProvider {

        public const string APPLE_AUTH_ENDPOINT = "https://appleid.apple.com/auth/token";

        public const bool DEBUG_LOGGING = false;

        public const string PREFIX_APPLE_PARAMS = "LoginProviderApple-";

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

     

        public override UserInformation ValidateTokenAndGetUserInformation(string token, System.Web.HttpContext context) {
            try {
                using (var client = new HttpClient()) {

                    // Prepare the request
                    var clientSecret = createToken(); 

                    var dictionary = new[]{
                        new KeyValuePair<string, string>("client_id", Module.IdentityProvider.Config.IdentityProviderAppleClientID),      // Service ID com.rocket-list.SignInWithApple
                        new KeyValuePair<string, string>("client_secret", clientSecret),       // Secret (contains your Key ID, Team ID, Bundle ID, private key, and more)
                        new KeyValuePair<string, string>("code", token),
                        new KeyValuePair<string, string>("scope", "name email"),
                        new KeyValuePair<string, string>("grant_type", "authorization_code"),  // For exchanging an authorization code for tokens
                        new KeyValuePair<string, string>("redirect_uri", Module.IdentityProvider.Config.IdentityProviderAppleClientRedirectURL  /* "https://example.com/home"*/), // Redirect URL
                    };
                    var requestContent = new FormUrlEncodedContent(dictionary);

                    // Make request
                    var response = client.PostAsync(APPLE_AUTH_ENDPOINT, requestContent).Result;
                    // Check the response
                    if (response.IsSuccessStatusCode) {
                        var responseContent = response.Content.ReadAsStringAsync().Result;
                        var parstedResponse = JObject.Parse(responseContent);

                        _logger.Info("responseContent");
                        _logger.Info(responseContent);

                        // Sample response
                        //{
                        //   "access_token":"a39c040bcc3c247b8b0da66e25a9e0012.0.sxsr.gkgnMJDFqMgsLRKdK3EAog",
                        //   "token_type":"Bearer",
                        //   "expires_in":3600,
                        //   "refresh_token":"r9f37b7145a3d4235bb019836fd6379a8.0.sxsr.duTXXXVYEAbvb0iuk0CYPg",
                        //   "id_token":"eyJraWQiOiJZdXlYb1kiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwczovL2FwcGxlaWQuYXBwbGUuY29tIiwiYXVkIjoiY29tLnJvY2tldC1saXN0LlNpZ25JbldpdGhBcHBsZSIsImV4cCI6MTcwMjY0NTQwNywiaWF0IjoxNzAyNTU5MDA3LCJzdWIiOiIwMDA3MjEuZTU2YWQ4YWE3NjQwNDcwYWFkM2Y0MzE0Y2ZmMTU3NmYuMDgxNiIsImF0X2hhc2giOiJ4MjlQMmEyTGJ5aS1aQmV2Q1ZzODZ3IiwiZW1haWwiOiJtaWNoYUBuZXJ2ZXMuY2giLCJlbWFpbF92ZXJpZmllZCI6InRydWUiLCJhdXRoX3RpbWUiOjE3MDI1NTkwMDIsIm5vbmNlX3N1cHBvcnRlZCI6dHJ1ZX0.aFTtKACNnmfMy0TeExbmg1SosFPrd_sJK15wB5XXfgixSfLwZCTxUu0CUi2arY2QLd7FfaT59XLhqWd5HMCS-cGEjiJ7AERB04DAoMAej9yadrRhTYd7NKe1MRLOs-bC6rpwDtjezBSA5P59aptrLPhumh-hnx_28OvWJzVD5n3AsBEwPvguDj_zNLDgCl3qYaXoR_WJzwZvbweVPDPGKAk2ozw5qXTXQOHUmKkfx5RVSw9nu70_HNXJ0Bal-xvdIygR29Vvhds_f3fPY5gBQYf8dn9URd3mbqVOhbE1gJLZrji1pM_Fl39Lyg5CbncdbhdVdM4afCFW378GbOHP5A"
                        //}

                        var id_token = parstedResponse.Value<string>("id_token");
                        if (DEBUG_LOGGING) {
                            _logger.Info("id_token");
                            _logger.Info(id_token);
                        }

                        var jwtPayloadBase64 = id_token.Split('.').Skip(1).Take(1).First();
                        if (DEBUG_LOGGING) {
                            _logger.Info("jwtPayloadBase64");
                            _logger.Info(jwtPayloadBase64);
                        }

                        var jwtHeaderBase64 = id_token.Split('.').Skip(0).Take(1).First();
                        var jwtSignatureBase64 = id_token.Split('.').Skip(2).Take(1).First();
                        var bytesToSign = string.Join(".", id_token.Split('.').Take(2)).Select(s => (byte)s).ToArray();

                        var jwtPayloadJson = Base64UrlDecode(jwtPayloadBase64);
                        var jwtHeaderJson = Base64UrlDecode(jwtHeaderBase64);
                  
                        var jwtPayloadJobject = JObject.Parse(jwtPayloadJson);
                        var jwtHeaderJobject = JObject.Parse(jwtHeaderJson);
                        var serverKID = jwtHeaderJobject.Value<string>("kid");

                        if (DEBUG_LOGGING) {
                            _logger.Info("jwtHeaderJson");
                            _logger.Info(jwtHeaderJson);
                        }

                        string data = jwtHeaderBase64 + "." + jwtPayloadBase64;

                      

                        var publicKey = LoadPublicKey(serverKID);

                        //_logger.Info("publicKey");
                        //_logger.Info(publicKey);


                        // Sample payload
                        // {"iss":"https://appleid.apple.com","aud":"com.rocket-list.SignInWithApple","exp":1702645407,"iat":1702559007,"sub":"000721.e56ad8aa7640470aad3f4314cff1576f.0816","at_hash":"x29P2a2Lbyi-ZBevCVs86w","email":"micha@nerves.ch","email_verified":"true","auth_time":1702559002,"nonce_supported":true}
                        // Sample header
                        // { "kid":"YuyXoY","alg":"RS256"}

                        // IMPORTANT: ONLY THE FIRST TIME THE USER NAME IS AVAILABLE IN THE JAVASCRIPT

                        // verify the jws signature with the public key 
                        // https://developer.apple.com/documentation/sign_in_with_apple/sign_in_with_apple_rest_api/verifying_a_user
                        if (VerifyRS256Signature(publicKey, bytesToSign, jwtSignatureBase64) == false){
                            throw new Exception("Apple identity token validation failed! invalid signature");
                        }

                        // ISS is always https://appleid.apple.com
                        if (jwtPayloadJobject.Value<string>("iss") != "https://appleid.apple.com") {
                            throw new Exception("Apple identity token validation failed! iss");
                        }

                        // aud 
                        if (jwtPayloadJobject.Value<string>("aud") != Config.IdentityProviderAppleClientID) {
                            throw new Exception("Apple identity token validation failed! aud");
                        }

                        // Nonce
                        var nonce = jwtPayloadJobject.Value<string>("nonce");
                        if (string.IsNullOrWhiteSpace(nonce) == false) {
                            // TODO check if the nonce is same as from the js
                            if (context.Request.Params[PREFIX_APPLE_PARAMS + "nonce"]?.ToString() != nonce) {
                                throw new Exception("Apple identity token validation failed! nonce");
                            }
                        }

                        // State
                        var state = jwtPayloadJobject.Value<string>("state");
                        if (string.IsNullOrWhiteSpace(state) == false) {
                            if (context.Request.Params[PREFIX_APPLE_PARAMS + "state"]?.ToString() != state) {
                                throw new Exception("Apple identity token validation failed! state");
                            }
                        }

                        // Expiration
                        var expirationUnixSeconds = jwtPayloadJobject.Value<long>("exp");
                        {
                            //var expiration = Core.Util.Time.GetDateTimeFromUTCMilliseconds(expirationUnixSeconds);
                            var expiration = DateTimeOffset.FromUnixTimeSeconds(expirationUnixSeconds).UtcDateTime;
                            if (expiration <= DateTime.UtcNow) {
                                throw new Exception("Apple identity token validation failed! exp=" + expiration + " - " + expirationUnixSeconds);
                            }
                        }

                        // Repack as user info object and return
                        var userInfo = new UserInformation();
                      
                        userInfo.FirstName = context.Request.Params[PREFIX_APPLE_PARAMS + "firstname"];
                        userInfo.LastName = context.Request.Params[PREFIX_APPLE_PARAMS + "lastname"];
                        userInfo.Email = jwtPayloadJobject.Value<string>("email");
                        userInfo.ProfileImage = null;
                        return userInfo;
                    } else {
                        throw new Exception("Apple identity token validation failed!");
                    }

                }
            } catch (Exception e) {
             
                var report = Core.EmailLogger.CompileFullErrorReport(null, null, e);
                var contacts = new List<EmailContact> { new EmailContact() { Address = "micha@nerves.ch" } };
                Core.Messaging.MessageCenter.SendMessageToEmail(contacts, null, null, "Apple Sign in", report, "Package");

                throw e;
            }



        }

        /// <summary>
        ///  Verfiy the signature using the public key
        /// </summary>
        /// <param name="rsaparams"></param>
        /// <param name="bytesToSign"></param>
        /// <param name="jwtSignature"></param>
        /// <returns></returns>
        private bool VerifyRS256Signature(RSAParameters rsaparams, byte[] bytesToSign, string jwtSignature) {
            // Verify the signature
            using (var rsa = new RSACryptoServiceProvider()) {
                rsa.ImportParameters(rsaparams);
                var verified = rsa.VerifyData(
                    bytesToSign,
                    CryptoConfig.MapNameToOID("SHA256"),
                    Base64UrlDecodeBytes(jwtSignature)
                );
                return verified;
            }
        }

        public static string Base64UrlEncode(string str) {
            if (str == null || str == "") {
                return null;
            }
            byte[] bytesToEncode = System.Text.UTF8Encoding.UTF8.GetBytes(str);
            String returnVal = System.Convert.ToBase64String(bytesToEncode);
            return returnVal.TrimEnd('=').Replace('+', '-').Replace('/', '_');
        }

        public static string Base64UrlDecode(string st) {
            if (st == null || st == "") {
                return null;
            }
            st = st.PadRight(st.Length + (4 - st.Length % 4) % 4, '=');
            var returnValBytes = System.Convert.FromBase64String(st);
            var returnValString = System.Text.UTF8Encoding.UTF8.GetString(returnValBytes);
            //return returnVal.TrimEnd('=').Replace('+', '-').Replace('/', '_');

            return returnValString;

        }

        public static byte[] Base64UrlDecodeBytes(string base64Url) {
            string padded = base64Url.Length % 4 == 0 ? base64Url : base64Url + "====".Substring(base64Url.Length % 4);
            string base64 = padded.Replace("_", "/").Replace("-", "+");
            return Convert.FromBase64String(base64);
        }

        /// <summary>
        /// Create a token using the private key
        /// </summary>
        /// <returns></returns>
        // JWT token
        // https://developer.apple.com/documentation/accountorganizationaldatasharing/creating-a-client-secret
        // JWT https://stackoverflow.com/questions/42514289/how-to-use-apns-auth-key-p8-file-in-c
        public static string createToken() {

            var privateKey = Config.IdentityProviderAppleClientSecret;
            privateKey = privateKey .Replace("-----BEGIN PRIVATE KEY-----", "")
                                   .Replace("-----END PRIVATE KEY-----", "")
                                   .Replace("\n", "")
                                   .Replace("\r", "");

            var iss = Config.IdentityProviderAppleClientTeamId;// "5PRSUCCGW2";
            var sup = Config.IdentityProviderAppleClientID;

            // Structure of the token
            ///{
            //    "alg": "ES256",
            //    "kid": "ABC123DEFG" key id
            // }
            //{
            //    "iss": "DEF123GHIJ", team id
            //    "iat": 1437179036, time linux now
            //    "exp": 1493298100, time linux
            //    "aud": "https://appleid.apple.com",
            //    "sub": "com.mytest.app" client id
            //}

            var header = "{\"alg\":\"ES256\",\"kid\":\"" + Config.IdentityProviderAppleClientKeyId + "\"}";
            var payload = "{\"iss\":\"{iss}\",\"iat\":{iat},\"exp\":{exp},\"aud\":\"https://appleid.apple.com\",\"sub\":\"{sup}\"}";
            payload = payload.Replace("{iss}", iss).
                Replace("{iat}", DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString()).
                Replace("{exp}", DateTimeOffset.UtcNow.AddMinutes(15).ToUnixTimeSeconds().ToString())
               .Replace("{sup}", sup);

            // JWT is created like this:
            // token= base64URL(head) + "." + base64URL(payload) + "." + sign(base64URL(head) + "." + base64URL(payload))
            var toSign = Base64UrlEncode(header)+"." + Base64UrlEncode(payload); 
            var signed = SignECDSAP256(privateKey, toSign);
            var token = toSign + "." + signed;

            return token;
        }

        /// <summary>
        /// Signs the data with the private key using the Elliptic Curve Digital Signature Algorithm (ECDSA) with the P-256 curve
        /// </summary>
        /// <param name="privateKey">.p8 secret key file content</param>
        /// <param name="data"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public static string SignECDSAP256(string privateKey, string data) {
            // Convert base64 string to byte array
            byte[] pkcs8Bytes = Convert.FromBase64String(privateKey);

            // Wrap the bytes in an ASN.1 object
            Asn1Object asn1Object = Asn1Object.FromByteArray(pkcs8Bytes);

            // Create a PKCS8 object out of it
            PrivateKeyInfo privateKeyInfo = PrivateKeyInfo.GetInstance(asn1Object);

            // Get a key parameter object ouf of it
            AsymmetricKeyParameter keyParams = PrivateKeyFactory.CreateKey(privateKeyInfo);

            // Ensure it's an EC private key
            if (!(keyParams is ECPrivateKeyParameters)) {
                throw new InvalidOperationException("Key is not an EC private key");
            }

            // Use your AsymmetricKeyParameter object
            var result = SignDataWithPrivateKey(keyParams, data);
            return result;

        }

        public static string SignDataWithPrivateKey(AsymmetricKeyParameter privateKey, string data) {
            byte[] dataToSign = Encoding.UTF8.GetBytes(data);

            // Create a signer 
            // https://stackoverflow.com/questions/66651601/ecdsa-signature-with-c-sharp-and-bouncy-castle-does-not-match-ms-ecdsa-signature
            ISigner signer = SignerUtilities.GetSigner("SHA-256withPLAIN-ECDSA" /*"SHA-256withECDSA"*/);// "e problem in the BouncyCastle solution is probably that SignerUtilities.GetSigner(\"SHA-256withECDSA\") returns the signature in ASN.1 format, but in the JWT context the (r,s) format is used. Newer BC versions support this format, e.g. with SHA-256withPLAIN-ECDSA. If the problem persists, it would be helpful if you could complete both codes for a repro, i.e. sample keypairs, calling code and samples for the generated JWTs. – \r\nTopaco\r\n Mar 16, 2021 at 9:22

            // Initialise the signer
            signer.Init(true, privateKey);

            // Add the data to be signed
            signer.BlockUpdate(dataToSign, 0, dataToSign.Length);

            // Generate the signature
            byte[] signature = signer.GenerateSignature();
            String returnVal = System.Convert.ToBase64String(signature);

            return returnVal.TrimEnd('=').Replace('+', '-').Replace('/', '_');

        }


        /// <summary>
        /// Data structure for https://appleid.apple.com/auth/keys
        /// </summary>
        public class JWKS {
            public List<Key> keys { get; set; }
        }

        /// <summary>
        /// Data structure for https://appleid.apple.com/auth/keys
        /// </summary>
        public class Key {
            public string kty { get; set; }
            public string kid { get; set; }
            public string use { get; set; }
            public string alg { get; set; }
            public string n { get; set; }
            public string e { get; set; }
        }

        /// <summary>
        /// Load the public key from https://appleid.apple.com/auth/keys
        /// and fill into RSAParameters
        /// </summary>
        /// <param name="kid"></param>
        /// <returns></returns>
        public static RSAParameters LoadPublicKey(string kid) {
            using (HttpClient httpClient = new HttpClient()) {
                var response = httpClient.GetAsync("https://appleid.apple.com/auth/keys").Result;
                response.EnsureSuccessStatusCode();

                var responseBody = response.Content.ReadAsStringAsync().Result;
                if (DEBUG_LOGGING) {
                 _logger.Info("responseBody");
                 _logger.Info(responseBody);
                }

                var jsonWebKeySet = JObject.Parse(responseBody).ToObject<JWKS>();
                var jsonWebKey = jsonWebKeySet.keys.First(k => k.kid == kid);

                var n = Base64UrlDecodeBytes(jsonWebKey.n);
                var e = Base64UrlDecodeBytes(jsonWebKey.e);

                RSAParameters rsaParameters = new RSAParameters();

                rsaParameters.Modulus = n;
                rsaParameters.Exponent = e;

                return rsaParameters;
               
            }
        }

       



    }

    
}
