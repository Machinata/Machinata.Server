﻿using Google.Apis.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.IdentityProvider {
    public class GoogleIdentityProvider : IdentityProvider {

        public override UserInformation ValidateTokenAndGetUserInformation(string token, System.Web.HttpContext context) {

            // Make JWT request to unpack token and validate authenticity and get payload data...
            // See https://developers.google.com/identity/gsi/web/reference/js-reference#credential for details on response
            // header
            // {
            //   "alg": "RS256",
            //   "kid": "f05415b13acb9590f70df862765c655f5a7a019e", // JWT signature
            //   "typ": "JWT"
            // }
            // payload
            // {
            //   "iss": "https://accounts.google.com", // The JWT's issuer
            //   "nbf":  161803398874,
            //   "aud": "314159265-pi.apps.googleusercontent.com", // Your server's client ID
            //   "sub": "3141592653589793238", // The unique ID of the user's Google Account
            //   "hd": "gmail.com", // If present, the host domain of the user's GSuite email address
            //   "email": "elisa.g.beckett@gmail.com", // The user's email address
            //   "email_verified": true, // true, if Google has verified the email address
            //   "azp": "314159265-pi.apps.googleusercontent.com",
            //   "name": "Elisa Beckett",
            //                            // If present, a URL to user's profile picture
            //   "picture": "https://lh3.googleusercontent.com/a-/e2718281828459045235360uler",
            //   "given_name": "Elisa",
            //   "family_name": "Beckett",
            //   "iat": 1596474000, // Unix timestamp of the assertion's creation time
            //   "exp": 1596477600, // Unix timestamp of the assertion's expiration time
            //   "jti": "abc161803398874def"
            //}
            var settings = new GoogleJsonWebSignature.ValidationSettings() {
                Audience = new List<string>() { Machinata.Core.Config.GetStringSetting("IdentityProviderGoogleClientID") }
            };
            Task<GoogleJsonWebSignature.Payload> task = Task.Run<GoogleJsonWebSignature.Payload>(async () => await GoogleJsonWebSignature.ValidateAsync(token, settings));
            var result = task.Result;

            // Repack as user info object and return
            var userInfo = new UserInformation();
            userInfo.FirstName = result.GivenName;
            userInfo.LastName = result.FamilyName;
            userInfo.Email = result.Email;
            userInfo.ProfileImage = result.Picture;
            userInfo.ProviderId = result.Subject;
            return userInfo;
        }
    }
}
