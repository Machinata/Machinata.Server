﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.IdentityProvider {
    public class MicrosoftIdentityProvider : IdentityProvider {

        public override UserInformation ValidateTokenAndGetUserInformation(string token, System.Web.HttpContext context) {
            using (var client = new HttpClient()) {
                // Make request
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                client.BaseAddress = new Uri("https://graph.microsoft.com");
                HttpResponseMessage response = client.GetAsync($"/v1.0/me").Result;
                response.EnsureSuccessStatusCode();
                string responseJSON = response.Content.ReadAsStringAsync().Result;
                var responseJObject = Core.JSON.ParseJsonAsJObject(responseJSON);

                // Repack as user info object and return
                var userInfo = new UserInformation();
                userInfo.FirstName = responseJObject.Value<string>("givenName");
                userInfo.LastName = responseJObject.Value<string>("surname");
                userInfo.Email = responseJObject.Value<string>("mail");
                userInfo.ProfileImage = null; // not part of me, see https://learn.microsoft.com/en-us/graph/api/profilephoto-get?view=graph-rest-1.0&tabs=http
                userInfo.ProviderId = responseJObject.Value<string>("id");
                return userInfo;
            }



        }
    }
}
