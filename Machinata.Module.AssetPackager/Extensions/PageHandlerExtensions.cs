using Machinata.Core.Exceptions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.AssetPackager.Extensions {
    public static class PageHandlerExtensions {

        public static void RequireLoginExceptAssetPackager(this Core.Handler.PageTemplateHandler handler) {
            // Safety
            if(string.IsNullOrEmpty(Machinata.Module.AssetPackager.Config.AuthSecretForHeadlessAccess)) {
                throw new BackendException("invalid-headless-auth-secret", "The configuration AuthSecretForHeadlessAccess must be set.");
            }
            // Validate through-access via special auth-secret query param...
            if (handler.Params.String("auth-secret") == Machinata.Module.AssetPackager.Config.AuthSecretForHeadlessAccess) {
                // Allow access
            } else {
                // Require login
                handler.RequireLogin();
            }
        }
    }
}
