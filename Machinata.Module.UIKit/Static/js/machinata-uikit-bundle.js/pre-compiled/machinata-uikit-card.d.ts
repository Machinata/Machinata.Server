/// <reference types="jquery" />
declare namespace Machinata.UI {
    class UICard extends UIElement {
        contentsElem: JQuery;
        constructor();
        addToContents(elem: JQuery): UICard;
        title(title: string): UICard;
    }
}
