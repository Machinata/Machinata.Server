/// <reference types="jquery" />
import * as jQuery from '../../../../Machinata.Core/Static/js/machinata-core-bundle.js/typings/jquery';
declare global {
    interface Window {
        jQuery: typeof jQuery;
        $: typeof jQuery;
    }
}
