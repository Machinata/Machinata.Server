var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Machinata;
(function (Machinata) {
    var UI;
    (function (UI) {
        var UICard = /** @class */ (function (_super) {
            __extends(UICard, _super);
            function UICard() {
                var _this = _super.call(this, "<div class='ui-card'/>") || this;
                _this.contentsElem = null;
                return _this;
            }
            UICard.prototype.addToContents = function (elem) {
                if (this.contentsElem == null)
                    this.contentsElem = $("<div class='ui-card-content'/>").appendTo(this.elem);
                this.contentsElem.append(elem);
                return this;
            };
            UICard.prototype.title = function (title) {
                return this;
            };
            return UICard;
        }(UI.UIElement));
        UI.UICard = UICard;
    })(UI = Machinata.UI || (Machinata.UI = {}));
})(Machinata || (Machinata = {}));
