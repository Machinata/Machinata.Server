var Machinata;
(function (Machinata) {
    var UI;
    (function (UI) {
        function createFormRow(opts, formElem) {
            return MachinataJS.UI.createFormRow(opts, formElem);
        }
        UI.createFormRow = createFormRow;
        ;
        function createFormInputRow(opts, formElem) {
            return MachinataJS.UI.createFormInputRow(opts, formElem);
        }
        UI.createFormInputRow = createFormInputRow;
        ;
        function createFormDualInputRow(opts1, opts2, formElem) {
            return MachinataJS.UI.createFormDualInputRow(opts1, opts2, formElem);
        }
        UI.createFormDualInputRow = createFormDualInputRow;
        ;
        var UIElement = /** @class */ (function () {
            function UIElement(baseHTML) {
                this.elem = $(baseHTML);
                this.elem.data("uielement", this);
            }
            UIElement.prototype.addClass = function (className) {
                this.elem.addClass(className);
                return this;
            };
            UIElement.prototype.data = function (key, val) {
                this.elem.data(key, val);
                return this;
            };
            UIElement.prototype.hide = function () {
                this.elem.hide();
                return this;
            };
            UIElement.prototype.show = function () {
                this.elem.show();
                return this;
            };
            UIElement.prototype.css = function (key, val) {
                this.elem.css(key, val);
                return this;
            };
            UIElement.prototype.text = function (text) {
                this.elem.text(text);
                return this;
            };
            UIElement.prototype.id = function (id) {
                this.elem.attr("id", id);
                return this;
            };
            UIElement.prototype.autoid = function (name) {
                if (this.elem.attr("id") == null)
                    this.elem.attr("id", MachinataJS.UI.getElemUID(name));
                return this;
            };
            UIElement.prototype.tooltip = function (tooltip) {
                if (tooltip == null || tooltip == "" || tooltip.trim() == "")
                    this.elem.removeAttr("title");
                else
                    this.elem.attr("title", tooltip);
                return this;
            };
            UIElement.prototype.find = function (selector) {
                return this.elem.find(selector);
            };
            UIElement.prototype.appendTo = function (container) {
                this.elem.appendTo(container);
                return this;
            };
            UIElement.prototype.append = function (element) {
                this.elem.append(element);
                return this;
            };
            return UIElement;
        }());
        UI.UIElement = UIElement;
    })(UI = Machinata.UI || (Machinata.UI = {}));
})(Machinata || (Machinata = {}));
