var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Machinata;
(function (Machinata) {
    var UI;
    (function (UI) {
        var UIDialogType;
        (function (UIDialogType) {
            UIDialogType[UIDialogType["Modal"] = 0] = "Modal";
        })(UIDialogType = UI.UIDialogType || (UI.UIDialogType = {}));
        var UIDialog = /** @class */ (function (_super) {
            __extends(UIDialog, _super);
            function UIDialog() {
                var _this = _super.call(this, "<div class='ui-dialog-overlay'/>") || this;
                _this.dialogType = UIDialogType.Modal;
                _this.buttons = [];
                _this.isModal = true;
                _this._onCancels = [];
                _this._onCloses = [];
                var self = _this;
                // General layout
                self.dialogElem = new UI.UIElement("<div/>").addClass("ui-dialog").addClass("option-uikit").appendTo(self.elem);
                self.dialogTitleElem = new UI.UIElement("<div/>").addClass("dialog-title").appendTo(self.dialogElem.elem).hide();
                self.dialogElem.append($("<div class='dialog-scroll-cover-top'/>"));
                self.dialogScrollElem = new UI.UIElement("<div/>").addClass("dialog-scroll").appendTo(self.dialogElem.elem);
                self.dialogElem.append($("<div class='dialog-scroll-cover-bottom'/>"));
                self.dialogMessageElem = new UI.UIElement("<div/>").addClass("dialog-message").appendTo(self.dialogScrollElem.elem).hide();
                self.dialogContentsElem = new UI.UIElement("<div/>").addClass("dialog-contents").appendTo(self.dialogScrollElem.elem).hide();
                self.dialogFormElem = new UI.UIElement("<div/>").addClass("dialog-form").addClass("ui-form").appendTo(self.dialogScrollElem.elem).hide();
                self.dialogActionsElem = new UI.UIElement("<div/>").addClass("dialog-actions").appendTo(self.dialogElem.elem).hide();
                // Bind events
                self.dialogScrollElem.elem.on("scroll", function (event) {
                    self._updateScrollView();
                });
                return _this;
            }
            UIDialog.prototype._updateScrollView = function () {
                var self = this;
                var elem = self.dialogScrollElem.elem[0];
                var hasHiddenContentAbove = false;
                var hasHiddenContentBelow = false;
                if (elem.clientHeight < elem.scrollHeight) {
                    var tolerance = 4;
                    hasHiddenContentAbove = elem.scrollTop >= tolerance;
                    hasHiddenContentBelow = elem.scrollTop <= (elem.scrollHeight - elem.clientHeight) - tolerance;
                    //console.log("scrollTop", elem.scrollTop, "scrollHeight", elem.scrollHeight, "clientHeight", elem.clientHeight);
                    //console.log("above", hasHiddenContentAbove, "below", hasHiddenContentBelow);
                }
                if (hasHiddenContentAbove == true)
                    self.dialogElem.elem.addClass("option-has-hidden-content-above");
                else
                    self.dialogElem.elem.removeClass("option-has-hidden-content-above");
                if (hasHiddenContentBelow == true)
                    self.dialogElem.elem.addClass("option-has-hidden-content-below");
                else
                    self.dialogElem.elem.removeClass("option-has-hidden-content-below");
            };
            UIDialog.prototype.button = function (opts) {
                var self = this;
                if (opts == null)
                    opts = {};
                if (opts.title == null)
                    opts.title = "{text.okay}";
                if (opts.buttonType == null && this.buttons.length == 0)
                    opts.buttonType = UI.UIButtonType.Primary;
                else if (opts.buttonType == null && this.buttons.length > 0)
                    opts.buttonType = UI.UIButtonType.Secondary;
                var button = new UI.UIButton(opts.buttonType);
                button.title(opts.title);
                if (opts.onClick != null) {
                    button.onClick(function () { opts.onClick(); });
                }
                if (opts.closeOnClick == true) {
                    button.onClick(function () { self.close(); });
                }
                if (opts.cancelOnClick == true) {
                    button.onClick(function () { self.cancel(); });
                }
                return this.addButton(button);
            };
            UIDialog.prototype.onClose = function (callback) {
                this._onCloses.push(callback);
                return this;
            };
            UIDialog.prototype.onCancel = function (callback) {
                this._onCancels.push(callback);
                return this;
            };
            UIDialog.prototype.confirmButtons = function (onConfirm) {
                this.okayButton(onConfirm);
                this.cancelButton();
                return this;
            };
            UIDialog.prototype.okayButton = function (onClick) {
                return this.button({
                    title: "{text.okay}",
                    closeOnClick: true,
                    onClick: onClick
                });
            };
            UIDialog.prototype.cancelButton = function (onClick) {
                return this.button({
                    title: "{text.cancel}",
                    cancelOnClick: true,
                    onClick: onClick
                });
            };
            UIDialog.prototype.addButton = function (button) {
                this.buttons.push(button);
                if (Machinata.UI.UIDialog.DEFAULT_BUTTON_SIZE == "small")
                    button.small();
                if (Machinata.UI.UIDialog.DEFAULT_BUTTON_SIZE == "large")
                    button.large();
                this.dialogActionsElem.append(button.elem).show();
                return this;
            };
            UIDialog.prototype.addInput = function (inputOpts) {
                var inputElem = Machinata.UI.createFormInputRow(inputOpts);
                this.dialogFormElem.append(inputElem).show();
                return this;
            };
            UIDialog.prototype.getInputValue = function (name) {
                var _a;
                return (_a = this.getInput(name)) === null || _a === void 0 ? void 0 : _a.val();
            };
            UIDialog.prototype.getInput = function (name) {
                if (name == null)
                    name = this.dialogFormElem.find("input").first().attr("name");
                if (name == null)
                    return null;
                var elem = this.dialogFormElem.find("input[name='" + name + "']");
                if (elem.length == 0)
                    return null;
                return elem;
            };
            UIDialog.prototype.addClass = function (name) {
                this.dialogElem.addClass(name);
                return this;
            };
            UIDialog.prototype.noPadding = function () {
                return this.addClass("option-no-padding");
            };
            UIDialog.prototype.noMaxWidth = function () {
                return this.addClass("option-no-maxwidth");
            };
            UIDialog.prototype.addContent = function (content) {
                this.dialogContentsElem.append(content).show();
                return this;
            };
            UIDialog.prototype.setContent = function (content) {
                this.dialogContentsElem.elem.empty();
                this.addContent(content);
                return this;
            };
            UIDialog.prototype.closeIcon = function () {
                var self = this;
                var closeButton = new UI.UIButton(UI.UIButtonType.Tertiary);
                closeButton.addClass("option-round");
                closeButton.addClass("close-icon");
                closeButton.icon("{icon.close}");
                closeButton.onClick(function () {
                    self.cancel();
                });
                this.dialogTitleElem.addClass("has-close-icon");
                this.dialogTitleElem.append(closeButton.elem);
                this.dialogTitleElem.show();
                return this;
            };
            UIDialog.prototype.title = function (title) {
                if (this.dialogTitleElem.find(".text").length == 0) {
                    this.dialogTitleElem.append($("<div class='text'/>"));
                }
                this.dialogTitleElem.find(".text").text(title);
                this.dialogTitleElem.show();
                return this;
            };
            UIDialog.prototype.message = function (message) {
                this.dialogMessageElem.text(message).show();
                return this;
            };
            UIDialog.prototype.large = function () {
                return this;
            };
            UIDialog.prototype.show = function () {
                this._updateScrollView();
                this.isModal = true;
                this.dialogElem.addClass("option-modal");
                this.elem.appendTo($("body"));
                this._updateScrollView();
                return this;
            };
            UIDialog.prototype.appendTo = function (parent) {
                this.isModal = false;
                this.dialogElem.addClass("option-inline");
                this.dialogElem.appendTo(parent);
                return this;
            };
            UIDialog.prototype.hide = function () {
                this.elem.hide();
                return this;
            };
            UIDialog.prototype.cancel = function () {
                var self = this;
                for (var _i = 0, _a = self._onCancels; _i < _a.length; _i++) {
                    var callback = _a[_i];
                    callback(self);
                }
                self.close();
                return this;
            };
            UIDialog.prototype.close = function () {
                var self = this;
                if (this.isModal == true) {
                    this.elem.hide();
                    this.elem.remove();
                }
                else {
                    this.dialogElem.hide();
                    this.dialogElem.elem.remove();
                }
                for (var _i = 0, _a = self._onCloses; _i < _a.length; _i++) {
                    var callback = _a[_i];
                    callback(self);
                }
                return this;
            };
            UIDialog.DEFAULT_BUTTON_SIZE = "default";
            return UIDialog;
        }(UI.UIElement));
        UI.UIDialog = UIDialog;
    })(UI = Machinata.UI || (Machinata.UI = {}));
})(Machinata || (Machinata = {}));
