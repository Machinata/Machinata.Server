var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Machinata;
(function (Machinata) {
    var UI;
    (function (UI) {
        var UIButtonType;
        (function (UIButtonType) {
            UIButtonType[UIButtonType["Primary"] = 0] = "Primary";
            UIButtonType[UIButtonType["Secondary"] = 1] = "Secondary";
            UIButtonType[UIButtonType["Toggle"] = 2] = "Toggle";
            UIButtonType[UIButtonType["Tertiary"] = 3] = "Tertiary";
        })(UIButtonType = UI.UIButtonType || (UI.UIButtonType = {}));
        var UIButton = /** @class */ (function (_super) {
            __extends(UIButton, _super);
            function UIButton(type, tagType) {
                if (tagType === void 0) { tagType = "button"; }
                var _this = _super.call(this, "<" + tagType + "/>") || this;
                _this.type = type;
                _this.elem.addClass("ui-button");
                if (type == UIButtonType.Primary)
                    _this.elem.addClass("option-primary");
                if (type == UIButtonType.Secondary)
                    _this.elem.addClass("option-secondary");
                if (type == UIButtonType.Tertiary)
                    _this.elem.addClass("option-tertiary");
                if (type == UIButtonType.Toggle)
                    _this.elem.addClass("option-toggle");
                return _this;
            }
            UIButton.prototype.clone = function () {
                var clone = new UIButton(this.type);
                clone.elem = this.elem.clone();
                return clone;
            };
            UIButton.prototype.title = function (title) {
                if (this.type == UIButtonType.Toggle) {
                    this.elem.append($("<span/>").text(title));
                }
                else {
                    this.elem.text(title);
                }
                return this;
            };
            UIButton.prototype.tooltip = function (tooltip) {
                _super.prototype.tooltip.call(this, tooltip);
                return this;
            };
            UIButton.prototype.large = function () {
                this.elem.addClass("option-large");
                return this;
            };
            UIButton.prototype.small = function () {
                this.elem.addClass("option-small");
                return this;
            };
            UIButton.prototype.gray = function () {
                this.elem.addClass("option-gray");
                return this;
            };
            UIButton.prototype.icon = function (icon) {
                return this.iconEnd(icon);
            };
            UIButton.prototype.iconEnd = function (icon) {
                this.elem.append($(icon));
                return this;
            };
            UIButton.prototype.iconStart = function (icon) {
                this.elem.prepend($(icon));
                this.elem.addClass("option-icon-before-text");
                return this;
            };
            UIButton.prototype.attr = function (key, val) {
                this.elem.attr(key, val);
                return this;
            };
            UIButton.prototype.onClick = function (handler) {
                this.elem.click(handler);
                return this;
            };
            return UIButton;
        }(UI.UIElement));
        UI.UIButton = UIButton;
        var UIButtonGroup = /** @class */ (function (_super) {
            __extends(UIButtonGroup, _super);
            function UIButtonGroup() {
                var _this = _super.call(this, "<div class='ui-button-group'/>") || this;
                _this.buttons = [];
                return _this;
            }
            UIButtonGroup.prototype.title = function (title) {
                return this;
            };
            UIButtonGroup.prototype.button = function (button) {
                this.elem.append(button.elem);
                this.buttons.push(button);
                return this;
            };
            return UIButtonGroup;
        }(UI.UIElement));
        UI.UIButtonGroup = UIButtonGroup;
    })(UI = Machinata.UI || (Machinata.UI = {}));
})(Machinata || (Machinata = {}));
