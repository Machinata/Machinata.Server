/// <reference types="jquery" />
declare namespace Machinata.UI {
    interface IFormInputOptions {
        name?: string;
        label: string;
        note?: string;
        type?: string;
        integratedLabel?: boolean;
        id?: string;
        required?: boolean;
        readonly?: boolean;
        value?: string;
        placeholder?: string;
        autocomplete?: string;
        min?: number;
        max?: number;
        step?: number;
        size?: string;
        html?: string;
        fileSource?: string;
        fileCategory?: string;
        fileSearchAPI?: string;
        fileUploadAPI?: string;
        useDecimalDateInput?: boolean;
        showAddMinusButtons?: boolean;
        actions?: any[];
        options?: any[] | string;
        onChange?: Function;
        onEnter?: Function;
    }
    function createFormRow(opts: IFormInputOptions, formElem?: JQuery): JQuery;
    function createFormInputRow(opts: IFormInputOptions, formElem?: JQuery): JQuery;
    function createFormDualInputRow(opts1: IFormInputOptions, opts2: IFormInputOptions, formElem?: JQuery): JQuery;
    class UIElement {
        elem: JQuery;
        constructor(baseHTML: string);
        addClass(className: string): UIElement;
        data(key: string, val: any): UIElement;
        hide(): UIElement;
        show(): UIElement;
        css(key: string, val: any): UIElement;
        text(text: string): UIElement;
        id(id: string): UIElement;
        autoid(name?: string): UIElement;
        tooltip(tooltip: string): UIElement;
        find(selector: string): JQuery;
        appendTo(container: JQuery): UIElement;
        append(element: JQuery): UIElement;
    }
}
