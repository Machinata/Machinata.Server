/// <reference types="jquery" />
declare namespace Machinata.UI {
    enum UIDialogType {
        Modal = 0
    }
    class UIDialog extends UIElement {
        static DEFAULT_BUTTON_SIZE: string;
        dialogType: UIDialogType;
        dialogElem: UIElement;
        dialogTitleElem: UIElement;
        dialogScrollElem: UIElement;
        dialogMessageElem: UIElement;
        dialogContentsElem: UIElement;
        dialogFormElem: UIElement;
        dialogActionsElem: UIElement;
        buttons: UIButton[];
        isModal: boolean;
        _onCancels: Function[];
        _onCloses: Function[];
        constructor();
        _updateScrollView(): void;
        button(opts: {
            title?: string;
            onClick?: Function;
            closeOnClick?: boolean;
            cancelOnClick?: boolean;
            buttonType?: UIButtonType;
        }): UIDialog;
        onClose(callback: Function): UIDialog;
        onCancel(callback: Function): UIDialog;
        confirmButtons(onConfirm?: Function): UIDialog;
        okayButton(onClick?: Function): UIDialog;
        cancelButton(onClick?: Function): UIDialog;
        addButton(button: UIButton): UIDialog;
        addInput(inputOpts: IFormInputOptions): UIDialog;
        getInputValue(name?: string): any;
        getInput(name?: string): JQuery;
        addClass(name: string): UIDialog;
        noPadding(): UIDialog;
        noMaxWidth(): UIDialog;
        addContent(content: JQuery): UIDialog;
        setContent(content: JQuery): UIDialog;
        closeIcon(): UIDialog;
        title(title: string): UIDialog;
        message(message: string): UIDialog;
        large(): UIDialog;
        show(): UIDialog;
        appendTo(parent: JQuery): UIDialog;
        hide(): UIDialog;
        cancel(): UIDialog;
        close(): UIDialog;
    }
}
