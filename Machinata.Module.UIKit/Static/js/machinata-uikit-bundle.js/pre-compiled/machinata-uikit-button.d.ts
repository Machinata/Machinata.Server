/// <reference types="jquery" />
declare namespace Machinata.UI {
    enum UIButtonType {
        Primary = 0,
        Secondary = 1,
        Toggle = 2,
        Tertiary = 3
    }
    class UIButton extends UIElement {
        type: UIButtonType;
        constructor(type: UIButtonType, tagType?: string);
        clone(): UIButton;
        title(title: string): UIButton;
        tooltip(tooltip: string): UIButton;
        large(): UIButton;
        small(): UIButton;
        gray(): UIButton;
        icon(icon: string): UIButton;
        iconEnd(icon: string): UIButton;
        iconStart(icon: string): UIButton;
        attr(key: string, val: string): UIButton;
        onClick(handler: JQuery.TypeEventHandler<HTMLElement, null, HTMLElement, HTMLElement, 'click'>): UIButton;
    }
    class UIButtonGroup extends UIElement {
        buttons: UIButton[];
        constructor();
        title(title: string): UIButtonGroup;
        button(button: UIButton): UIButtonGroup;
    }
}
