
namespace Machinata.UI {

    export interface IFormInputOptions {
        name?: string,
        label: string,
        note?: string,
        type?: string,

        integratedLabel?: boolean

        id?: string,
        required?: boolean,
        readonly?: boolean,
        value?: string,
        placeholder?: string,
        autocomplete?: string,
        min?: number,
        max?: number,
        step?: number,
        size?: string,
        html?: string,

        fileSource?: string,
        fileCategory?: string,
        fileSearchAPI?: string,
        fileUploadAPI?: string,

        useDecimalDateInput?: boolean,
        showAddMinusButtons?: boolean,

        actions?: any[],
        options?: any[] | string,

        onChange?: Function,
        onEnter?: Function,
    }

    export function createFormRow(opts: IFormInputOptions, formElem?: JQuery): JQuery {
        return MachinataJS.UI.createFormRow(opts, formElem);
    };

    export function createFormInputRow(opts: IFormInputOptions, formElem?: JQuery): JQuery {
        return MachinataJS.UI.createFormInputRow(opts, formElem);
    };

    export function createFormDualInputRow(opts1: IFormInputOptions, opts2: IFormInputOptions, formElem?: JQuery): JQuery {
        return MachinataJS.UI.createFormDualInputRow(opts1, opts2, formElem);
    };

    export class UIElement {
        elem: JQuery;

        constructor(baseHTML: string) {
            this.elem = $(baseHTML);
            this.elem.data("uielement", this);
        }

        addClass(className: string): UIElement {
            this.elem.addClass(className);
            return this;
        }

        data(key: string, val: any): UIElement {
            this.elem.data(key,val);
            return this;
        }

        hide(): UIElement {
            this.elem.hide();
            return this;
        }

        show(): UIElement {
            this.elem.show();
            return this;
        }

        css(key: string, val: any): UIElement {
            this.elem.css(key, val);
            return this;
        }

        text(text: string): UIElement {
            this.elem.text(text);
            return this;
        }

        id(id: string): UIElement {
            this.elem.attr("id", id);
            return this;
        }

        autoid(name?: string): UIElement {
            if (this.elem.attr("id") == null) this.elem.attr("id", MachinataJS.UI.getElemUID(name));
            return this;
        }

        tooltip(tooltip: string): UIElement {
            if (tooltip == null || tooltip == "" || tooltip.trim() == "") this.elem.removeAttr("title");
            else this.elem.attr("title",tooltip);
            return this;
        }

        find(selector: string): JQuery {
            return this.elem.find(selector);
        }

        appendTo(container: JQuery): UIElement {
            this.elem.appendTo(container);
            return this;
        }

        append(element: JQuery): UIElement {
            this.elem.append(element);
            return this;
        }
    }


}
