//

Machinata.UI.createForm = function (opts, containerElem) {
    // Init
    if (opts == null) opts = {};
    var elem = $("<div class='ui-form'></div>");
    // Add to container
    if (containerElem != null) containerElem.append(elem);
    // Return root elem
    return elem;
};
Machinata.UI.createFormRow = function (opts, formElem) {
    // Init
    if (opts == null) opts = {};
    var elem = $("<div class='ui-form-row'></div>");
    elem.attr("data-name", opts.name);
    // Label
    if (opts.label != null) {
        var labelElem = $('<label class="ui-label"></label>').text(opts.label + (opts.required == true ? "*" : "")).appendTo(elem);
        labelElem.attr("data-name", opts.name);
    } else {
        var labelElem = $('<label class="ui-label empty"></label>').appendTo(elem);
        labelElem.attr("data-name", opts.name);
    }
    if (opts.integratedLabel == true) {
        elem.addClass("ui-input-with-integrated-label");
    }
    // Add to container
    if (formElem != null) formElem.append(elem);
    // Return root elem
    return elem;
};
Machinata.UI.createFormInputRow = function (opts, formElem) {
    // Init
    if (opts == null) opts = {};
    var elem = Machinata.UI.createFormRow(opts,formElem);
    // Input
    var inputWrapperElem = Machinata.UI.createFormInput(opts).appendTo(elem);
    // Note?
    if (opts.note != null) {
        var noteElem = $("<div class='ui-label ui-input-note'/>").text(opts.note);
        elem.append(noteElem);
    }
    // Return root elem
    return elem;
};
Machinata.UI.createFormDualInputRow = function (opts1, opts2, formElem) {
    // Init
    if (opts1 == null) opts1 = {};
    if (opts2 == null) opts2 = {};
    var elem = $("<div class='ui-form-row option-dual-input'><div class='left-input'></div><div class='right-input'></div></div>");
    {
        // Label
        var labelElem = $('<label class="ui-label"></label>').text(opts1.label + (opts1.required == true ? "*" : "")).appendTo(elem.find('.left-input'));
        // Input
        var inputWrapperElem = Machinata.UI.createFormInput(opts1).appendTo(elem.find('.left-input'));
    }
    {
        // Label
        var labelElem = $('<label class="ui-label"></label>').text(opts2.label + (opts2.required == true ? "*" : "")).appendTo(elem.find('.right-input'));
        // Input
        var inputWrapperElem = Machinata.UI.createFormInput(opts2).appendTo(elem.find('.right-input'));
    }
    // Small size?
    if (opts1.size == "small") {
        elem.addClass("option-left-input-small");
    }
    if (opts2.size == "small") {
        elem.addClass("option-right-input-small");
    }
    // Integrated labels
    if (opts1.integratedLabel == true) elem.find(".left-input").addClass("ui-input-with-integrated-label");
    if (opts2.integratedLabel == true) elem.find(".right-input").addClass("ui-input-with-integrated-label");
    
    // Add to container
    if (formElem != null) formElem.append(elem);
    // Return root elem
    return elem;
};
Machinata.UI.createFormTrippleInputRow = function (opts1, opts2, opts3, formElem) {
    // Init
    if (opts1 == null) opts1 = {};
    if (opts2 == null) opts2 = {};
    if (opts3 == null) opts3 = {};
    var elem = $("<div class='ui-form-row option-tripple-input'><div class='left-input'></div><div class='middle-input'></div><div class='right-input'></div></div>");
    {
        // Label
        var labelElem = $('<label class="ui-label"></label>').text(opts1.label + (opts1.required == true ? "*" : "")).appendTo(elem.find('.left-input'));
        // Input
        var inputWrapperElem = Machinata.UI.createFormInput(opts1).appendTo(elem.find('.left-input'));
    }
    {
        // Label
        var labelElem = $('<label class="ui-label"></label>').text(opts2.label + (opts2.required == true ? "*" : "")).appendTo(elem.find('.middle-input'));
        // Input
        var inputWrapperElem = Machinata.UI.createFormInput(opts2).appendTo(elem.find('.middle-input'));
    }
    {
        // Label
        var labelElem = $('<label class="ui-label"></label>').text(opts3.label + (opts3.required == true ? "*" : "")).appendTo(elem.find('.right-input'));
        // Input
        var inputWrapperElem = Machinata.UI.createFormInput(opts3).appendTo(elem.find('.right-input'));
    }
    // Small size?
    if (opts1.size == "small") {
        elem.addClass("option-left-input-small");
    }
    if (opts2.size == "small") {
        elem.addClass("option-middle-input-small");
    }
    if (opts3.size == "small") {
        elem.addClass("option-right-input-small");
    }
    // Integrated labels
    if (opts1.integratedLabel == true) elem.find(".left-input").addClass("ui-input-with-integrated-label");
    if (opts2.integratedLabel == true) elem.find(".middle-input").addClass("ui-input-with-integrated-label");
    if (opts3.integratedLabel == true) elem.find(".right-input").addClass("ui-input-with-integrated-label");

    // Add to container
    if (formElem != null) formElem.append(elem);
    // Return root elem
    return elem;
};
Machinata.UI.createFormInput = function (opts, formRowElem) {
    // Init
    if (opts == null) opts = {};
    if (opts.type == null) opts.type = "text";
    if (opts.size == null) opts.size = "normal";
    // Input
    var elem = $('<div class="ui-input"></div>');
    var wrapperElem = elem;
    if (opts.type == "checkbox") {
        wrapperElem = $('<label class="ui-checkbox"></div>').appendTo(elem);
        if (opts.text != null) wrapperElem.text(opts.text);
        else if (opts.html != null) wrapperElem.html(opts.html);
    } else if (opts.actions != null) {
        wrapperElem = $('<div class="ui-input-and-action"><div class="input-group"></div><div class="action-group"></div></div>').appendTo(elem).find(".input-group");
        for (var i = 0; i < opts.actions.length; i++) {
            var action = opts.actions[i];
            var actionButton = $("<button class='ui-button'/>");
            actionButton.text(action.label);
            actionButton.click(action.action);
            actionButton.appendTo(elem.find(".action-group"));
        }
        wrapperElem.parent().addClass("num-actions-" + elem.find(".action-group button").length);
    }
    var inputElemTag = "input";
    if (opts.type == "textarea") inputElemTag = "textarea";
    if (opts.type == "file") inputElemTag = "input";
    if (opts.type == "select") inputElemTag = "select";
    var inputElem = $('<' + inputElemTag +'/>').appendTo(wrapperElem);
    inputElem.attr("type", opts.type);
    elem.attr("data-type", opts.type);
    inputElem.attr("name", opts.name);
    elem.attr("data-name", opts.name);
    inputElem.attr("id", opts.id);
    if (opts.type == "file") inputElem.addClass("ui-button");
    if (opts.type == "submit") inputElem.addClass("ui-button");
    if (opts.required == true) inputElem.attr("required", "required");
    if (opts.classes != null) inputElem.addClass(opts.classes);
    if (opts.placeholder != null) inputElem.attr("placeholder", opts.placeholder);
    if (opts.autocomplete != null) inputElem.attr("autocomplete", opts.autocomplete);
    if (opts.readonly == true) {
        inputElem.attr("readonly", "readonly");
        elem.attr("data-readonly", "true");
    }
    if (opts.min != null) inputElem.attr("min", opts.min);
    if (opts.max != null) inputElem.attr("max", opts.max);
    if (opts.step != null) inputElem.attr("step", opts.step);
    if (opts.disabled == true) inputElem.attr("disabled", "disabled");
    if (opts.onChange != null) inputElem.on("change", opts.onChange);
    if (opts.onEnter != null) inputElem.on("keypress", function (event) {
        if (event.which == 13 && !event.shiftKey) {
            event.preventDefault();
            opts.onEnter(event);
        }
    });
    inputElem.val(opts.value);
    // Countries
    if (opts.options == "countries" || opts.options == "western-countries") {
        let optionsSource = opts.options;
        opts.options = [];
        for (var i = 0; i < Machinata.COUNTRIES.length; i++) {
            var c = Machinata.COUNTRIES[i];
            if (optionsSource == "western-countries") {
                if (Machinata.COUNTRY_CODES_WESTERN.includes(c.code.toLowerCase()) == false) {
                    continue;
                };
            }
            var option = {
                title: c.name,
                value: c.code.toLowerCase()
            };
            if (c.code.toLowerCase() == opts.value) {
                option.selected = true;
            }
            opts.options.push(option);
        }
        opts.options.sort(function (a, b) {
            const aStr = a.title.toUpperCase(); // ignore upper and lowercase
            const bStr = b.title.toUpperCase(); // ignore upper and lowercase
            if (aStr < bStr) {
                return -1;
            } else if (aStr > bStr) {
                return 1;
            }
            return 0;
        });
    }
    // Select additions
    if (inputElemTag == "select" && opts.options != null) {
        if (opts.required != true) {
            var optionElem = $("<option/>");
            optionElem.text("");
            optionElem.attr("value", "");
            inputElem.append(optionElem);
        }
        for (var i = 0; i < opts.options.length; i++) {
            var option = opts.options[i];
            var optionElem = $("<option></option>");
            optionElem.text(option.title);
            optionElem.attr("value", option.value);
            if (option.selected == true) optionElem.attr("selected", "selected");
            inputElem.append(optionElem);
        }
    }
    // Checkbox additions
    if (opts.type == "checkbox") {
        wrapperElem.append($("<span class='checkmark'/>"));
    }
    // Buttons additions
    if (opts.type == "buttons" || opts.type == "radios") {
        // Init
        inputElem.hide();
        var groupClass = "ui-controlgroup";
        var optionClass = "ui-button";
        var optionTag = "button";
        if (opts.type == "radios") {
            groupClass = "ui-controlgroup";
            optionClass = "ui-radio";
            optionTag = "label";
        }
        // Create UI
        var controlGroupElem = $("<div class='" + groupClass+"'/>");
        wrapperElem.append(controlGroupElem);
        for (var i = 0; i < opts.options.length; i++) {
            var option = opts.options[i];
            var optElem = $("<" + optionTag+" class='" + optionClass +"'/>");
            optElem.text(option.title || option.value);
            optElem.attr("data-value", option.value);
            if (option.value == opts.value) optElem.addClass("selected");
            optElem.click(function () {
                optElem.parent().children().not($(this)).removeClass("selected");
                $(this).addClass("selected");
                inputElem.val($(this).attr("data-value"));
                inputElem.trigger("change");
            });
            if (opts.type == "radios") {
                optElem.prepend("<span class='checkmark'></span>");
            }
            controlGroupElem.append(optElem);
        }
    }
    // File-upload
    if (opts.type == "file-upload") {
        inputElem.hide();
        var previewElem = Machinata.Content.createFilePreview(opts.value || "", opts.value || "", true);
        previewElem.click(function () {
            var fileSource = opts.fileSource;
            var fileCategory = opts.fileCategory;
            var fileSearchAPI = opts.fileSearchAPI;
            var fileUploadAPI = opts.fileUploadAPI;
           
            var diag = Machinata.fileDialog("{text.upload-file}", fileSource, fileCategory, null, fileSearchAPI, fileUploadAPI); // title, source, category, buttons, api (search), api (success/upload)
            diag.okay(function (val) {
                inputElem.val(val);
                Machinata.Content.updateFilePreview(previewElem, val, val, true);
                inputElem.trigger("change");
            });
            diag.show();
        });
        wrapperElem.append(previewElem);
    }
    // Decimal Date Input
    if (opts.type == "date" && opts.useDecimalDateInput == true) {
        Machinata.UI.createNumericDateInput(inputElem, {value: opts.value});
    }
    // Plus minus buttons
    if (opts.type == "number" && opts.showAddMinusButtons == true) {
        var minusButton = $("{icon.minus-circled}").addClass("action-minus").appendTo(elem);
        var addButton = $("{icon.plus-circled}").addClass("action-plus").appendTo(elem);
        minusButton.on("click dblclick",function () {
            let input = $(this).closest(".ui-input").find("input");
            let currentVal = input.val();
            if (currentVal == null || currentVal == "") currentVal = 0;
            currentVal--;
            if (opts.min != null && currentVal < opts.min) currentVal = opts.min;
            input.val(currentVal);
            //input[0].setSelectionRange(0, 0); // input type number doesnt support this, apparently
        });
        addButton.on("click dblclick", function () {
            let input = $(this).closest(".ui-input").find("input");
            let currentVal = input.val();
            if (currentVal == null || currentVal == "") currentVal = 0;
            currentVal++;
            if (opts.max != null && currentVal > opts.max) currentVal = opts.max;
            input.val(currentVal);
            //input[0].setSelectionRange(0, 0); // input type number doesnt support this, apparently
        });
    }
    // Add to container
    if (formRowElem != null) formRowElem.append(elem);
    // Return root elem
    return elem;
};



Machinata.UI.onUIBind(function (elements) {
    elements.find(".ui-input-with-integrated-label").each(function () {
        var elem = $(this);
        var inputElem = elem.find("input,textarea,select");
        function updateEmptyClass() {
            if (inputElem.val() == "" || inputElem.val() == null) {
                elem.addClass("option-input-empty");
            } else {
                elem.removeClass("option-input-empty");
            }
        }
        function updateScrollClass() {
            if (inputElem[0].scrollTop > 0) {
                elem.addClass("option-input-scrolled");
            } else {
                elem.removeClass("option-input-scrolled");
            }
        }
        if (inputElem.prop("tagName").toLowerCase() == "textarea") {
            inputElem.on("change", function () {
                updateScrollClass();
            });
            inputElem.on("input", function () {
                updateScrollClass();
            });
            inputElem.on("scroll", function () {
                updateScrollClass();
            });
        }
        updateEmptyClass();
        inputElem.on("change", function () {
            updateEmptyClass()
        });
        inputElem.on("input", function () {
            updateEmptyClass()
        });
        inputElem.on("focus", function () {
            elem.addClass("option-input-focused");
        });
        inputElem.on("blur", function () {
            elem.removeClass("option-input-focused");
        });
    });
});
