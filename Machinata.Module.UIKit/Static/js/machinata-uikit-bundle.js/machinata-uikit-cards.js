//

Machinata.UI.createCard = function (opts, containerElem) {
    // Init
    if (opts == null) opts = {};
    var elem = $("<div class='ui-card'></div>");
    // Content
    if (opts.addContent != false) elem.append($("<div class='ui-card-content'></div>"));
    if (opts.content != null) opts.content(elem.find(".ui-card-content"));
    // Actions
    if (opts.addActions != false) elem.append($("<div class='ui-card-actions'></div>"));
    // Add to container
    if (containerElem != null) containerElem.append(elem);
    // Return root elem
    return elem;
};