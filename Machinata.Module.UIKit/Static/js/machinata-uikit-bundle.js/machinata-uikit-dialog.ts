
namespace Machinata.UI {

    export enum UIDialogType {
        Modal
    }

    export class UIDialog extends UIElement {

        static DEFAULT_BUTTON_SIZE: string = "default";

        dialogType: UIDialogType = UIDialogType.Modal;

        dialogElem: UIElement;
        dialogTitleElem: UIElement;
        dialogScrollElem: UIElement;
        dialogMessageElem: UIElement;
        dialogContentsElem: UIElement;
        dialogFormElem: UIElement;
        dialogActionsElem: UIElement;

        buttons: UIButton[] = [];

        isModal: boolean = true;

        _onCancels: Function[] = [];
        _onCloses: Function[] = [];

        constructor() {
            super("<div class='ui-dialog-overlay'/>");
            let self = this;
    
            // General layout
            self.dialogElem = new UIElement("<div/>").addClass("ui-dialog").addClass("option-uikit").appendTo(self.elem);
            self.dialogTitleElem = new UIElement("<div/>").addClass("dialog-title").appendTo(self.dialogElem.elem).hide();

            self.dialogElem.append($("<div class='dialog-scroll-cover-top'/>"));
            self.dialogScrollElem = new UIElement("<div/>").addClass("dialog-scroll").appendTo(self.dialogElem.elem);

            self.dialogElem.append($("<div class='dialog-scroll-cover-bottom'/>"));
            self.dialogMessageElem = new UIElement("<div/>").addClass("dialog-message").appendTo(self.dialogScrollElem.elem).hide();
            self.dialogContentsElem = new UIElement("<div/>").addClass("dialog-contents").appendTo(self.dialogScrollElem.elem).hide();
            self.dialogFormElem = new UIElement("<div/>").addClass("dialog-form").addClass("ui-form").appendTo(self.dialogScrollElem.elem).hide();
            self.dialogActionsElem = new UIElement("<div/>").addClass("dialog-actions").appendTo(self.dialogElem.elem).hide();


            // Bind events
            self.dialogScrollElem.elem.on("scroll", function (event) {
                self._updateScrollView();
            });

        }

        _updateScrollView() {
            let self = this;
            let elem = self.dialogScrollElem.elem[0];
            let hasHiddenContentAbove = false;
            let hasHiddenContentBelow = false;
            if (elem.clientHeight < elem.scrollHeight) {
                let tolerance = 4;
                hasHiddenContentAbove = elem.scrollTop >= tolerance;
                hasHiddenContentBelow = elem.scrollTop <= (elem.scrollHeight - elem.clientHeight) - tolerance;
                //console.log("scrollTop", elem.scrollTop, "scrollHeight", elem.scrollHeight, "clientHeight", elem.clientHeight);
                //console.log("above", hasHiddenContentAbove, "below", hasHiddenContentBelow);
            }
            if (hasHiddenContentAbove == true) self.dialogElem.elem.addClass("option-has-hidden-content-above");
            else self.dialogElem.elem.removeClass("option-has-hidden-content-above");
            if (hasHiddenContentBelow == true) self.dialogElem.elem.addClass("option-has-hidden-content-below");
            else self.dialogElem.elem.removeClass("option-has-hidden-content-below");
        }

        button(opts: { title?: string, onClick?: Function, closeOnClick?: boolean, cancelOnClick?: boolean, buttonType?: UIButtonType }): UIDialog {
            let self = this;

            if (opts == null) opts = {};
            if (opts.title == null) opts.title = "{text.okay}";
            if (opts.buttonType == null && this.buttons.length == 0) opts.buttonType = UIButtonType.Primary;
            else if (opts.buttonType == null && this.buttons.length > 0) opts.buttonType = UIButtonType.Secondary;

            let button = new UIButton(opts.buttonType);
            button.title(opts.title);

            if (opts.onClick != null) {
                button.onClick(function () { opts.onClick(); });
            }
            if (opts.closeOnClick == true) {
                button.onClick(function () { self.close(); });
            }
            if (opts.cancelOnClick == true) {
                button.onClick(function () { self.cancel(); });
            }

            return this.addButton(button);
        }

        onClose(callback: Function): UIDialog {
            this._onCloses.push(callback);
            return this;
        }

        onCancel(callback: Function): UIDialog {
            this._onCancels.push(callback);
            return this;
        }

        confirmButtons(onConfirm?: Function): UIDialog {
            this.okayButton(onConfirm);
            this.cancelButton();
            return this;
        }

        okayButton(onClick?: Function): UIDialog {
            return this.button({
                title: "{text.okay}",
                closeOnClick: true,
                onClick: onClick
            });
        }

        cancelButton(onClick?: Function): UIDialog {
            return this.button({
                title: "{text.cancel}",
                cancelOnClick: true,
                onClick: onClick
            });
        }

        addButton(button: UIButton): UIDialog {
            this.buttons.push(button);

            if (Machinata.UI.UIDialog.DEFAULT_BUTTON_SIZE == "small") button.small();
            if (Machinata.UI.UIDialog.DEFAULT_BUTTON_SIZE == "large") button.large();

            this.dialogActionsElem.append(button.elem).show();

            return this;
        }

        addInput(inputOpts: IFormInputOptions): UIDialog {

            let inputElem = Machinata.UI.createFormInputRow(inputOpts);
            this.dialogFormElem.append(inputElem).show();

            return this;
        }

        getInputValue(name?: string): any {

            return this.getInput(name)?.val();

        }

        getInput(name?: string): JQuery {

            if (name == null) name = this.dialogFormElem.find("input").first().attr("name");
            if (name == null) return null;

            let elem = this.dialogFormElem.find("input[name='" + name + "']");
            if (elem.length == 0) return null;

            return elem;

        }

        addClass(name: string): UIDialog {
            this.dialogElem.addClass(name);
            return this;
        }

        noPadding(): UIDialog {
            return this.addClass("option-no-padding");
        }

        noMaxWidth(): UIDialog {
            return this.addClass("option-no-maxwidth");
        }

        addContent(content: JQuery): UIDialog {

            this.dialogContentsElem.append(content).show();

            return this;
        }

        setContent(content: JQuery): UIDialog {

            this.dialogContentsElem.elem.empty();
            this.addContent(content);

            return this;
        }

        closeIcon(): UIDialog {
            let self = this;
            let closeButton = new UIButton(UIButtonType.Tertiary);
            closeButton.addClass("option-round");
            closeButton.addClass("close-icon");
            closeButton.icon("{icon.close}");
            closeButton.onClick(function () {
                self.cancel();
            });

            this.dialogTitleElem.addClass("has-close-icon");
            this.dialogTitleElem.append(closeButton.elem);
            this.dialogTitleElem.show();
            return this;
        }

        title(title: string): UIDialog {
            if (this.dialogTitleElem.find(".text").length == 0) {
                this.dialogTitleElem.append($("<div class='text'/>"));
            }
            this.dialogTitleElem.find(".text").text(title);
            this.dialogTitleElem.show();
            return this;
        }

        message(message: string): UIDialog {
            this.dialogMessageElem.text(message).show();
            return this;
        }

        large(): UIDialog {
            return this;
        }



        show(): UIDialog {
            this._updateScrollView();
            this.isModal = true;
            this.dialogElem.addClass("option-modal");
            this.elem.appendTo($("body"));
            this._updateScrollView();
            return this;
        }

        appendTo(parent: JQuery): UIDialog {
            this.isModal = false;
            this.dialogElem.addClass("option-inline");
            this.dialogElem.appendTo(parent);
            return this;
        }

        hide(): UIDialog {
            this.elem.hide();
            return this;
        }

        cancel(): UIDialog {
            let self = this;
            for (let callback of self._onCancels) {
                callback(self);
            }
            self.close();
            return this;
        }

        close(): UIDialog {
            let self = this;
            if (this.isModal == true) {
                this.elem.hide();
                this.elem.remove();
            } else {
                this.dialogElem.hide();
                this.dialogElem.elem.remove();
            }
            for (let callback of self._onCloses) {
                callback(self);
            }
            return this;
        }

    }

}
