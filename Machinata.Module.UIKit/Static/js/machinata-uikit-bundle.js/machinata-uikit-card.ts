
namespace Machinata.UI {



    export class UICard extends UIElement {

        contentsElem: JQuery = null;

        constructor() {
            super("<div class='ui-card'/>");

        }

        addToContents(elem: JQuery): UICard {
            if (this.contentsElem == null) this.contentsElem = $("<div class='ui-card-content'/>").appendTo(this.elem);
            this.contentsElem.append(elem);
            return this;
        }

        title(title: string): UICard {

            return this;
        }
    }

}
