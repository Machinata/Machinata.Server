
namespace Machinata.UI {

    export enum UIButtonType {
        Primary,
        Secondary,
        Toggle,
        Tertiary
    }

    export class UIButton extends UIElement {

        type: UIButtonType;

        constructor(type: UIButtonType, tagType: string = "button") {
            super("<"+tagType+"/>");
            this.type = type;

            this.elem.addClass("ui-button");
            if (type == UIButtonType.Primary) this.elem.addClass("option-primary");
            if (type == UIButtonType.Secondary) this.elem.addClass("option-secondary");
            if (type == UIButtonType.Tertiary) this.elem.addClass("option-tertiary");
            if (type == UIButtonType.Toggle) this.elem.addClass("option-toggle");
        }

        clone(): UIButton {
            let clone = new UIButton(this.type);
            clone.elem = this.elem.clone();
            return clone;
        }

        title(title: string): UIButton {
            if (this.type == UIButtonType.Toggle) {
                this.elem.append($("<span/>").text(title));
            } else {
                this.elem.text(title);
            }
            return this;
        }

        tooltip(tooltip: string): UIButton {
            super.tooltip(tooltip);
            return this;
        }

        large(): UIButton {
            this.elem.addClass("option-large");
            return this;
        }

        small(): UIButton {
            this.elem.addClass("option-small");
            return this;
        }

        gray(): UIButton {
            this.elem.addClass("option-gray");
            return this;
        }

        icon(icon: string): UIButton {
            return this.iconEnd(icon);
        }

        iconEnd(icon: string): UIButton {
            this.elem.append($(icon));
            return this;
        }

        iconStart(icon: string): UIButton {
            this.elem.prepend($(icon));
            this.elem.addClass("option-icon-before-text");
            return this;
        }

        attr(key: string, val: string): UIButton {
            this.elem.attr(key, val);
            return this;
        }

        onClick(handler: JQuery.TypeEventHandler<HTMLElement, null, HTMLElement, HTMLElement, 'click'>): UIButton {
            this.elem.click(handler);
            return this;
        }
    }

    export class UIButtonGroup extends UIElement {

        buttons: UIButton[] = [];

        constructor() {
            super("<div class='ui-button-group'/>");
        }

        title(title: string): UIButtonGroup {
            
            return this;
        }

        button(button: UIButton): UIButtonGroup {
            this.elem.append(button.elem);
            this.buttons.push(button);
            return this;
        }
    }
}
