
Machinata.ready(function () {
    // Tooltip
    Machinata.UI.tooltips({
        //disableForTouchDevices: true,
        disableForMobile: true,
        disableForTablet: true,
    });

    // Action: page down actions
    Machinata.UI.bindPageDownElements($(".ui-action-page-down"), {
        duration: 1200,
        nextTargetSelector: null // auto via data-page-down-target attrib
    });


    // Action: menu toggle
    $(".ui-action-toggle-menu").click(function () {
        if ($("body").hasClass("menu-is-open")) {
            $("body").removeClass("menu-is-open")
        } else {
            $("body").addClass("menu-is-open")
        }
    });
});
