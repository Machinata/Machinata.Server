

/// <summary>
/// Creates a symbol collection element.
/// Options:
///  -currently none
/// </summary>
/// <param name="opts">A options object</param>
/// <param name="containerElem">A containerElem to which automatically add the returned elemement to. Optional.</param>
/// <returns>The created symbol collection element</returns>
Machinata.UI.createSymbolCollection = function (opts, containerElem) {
    // Init
    if (opts == null) opts = {};
    var elem = $("<div class='ui-symbol-collection'><div class='clear'></div></div>");
    // Add to container
    if (containerElem != null) containerElem.append(elem);
    // Return root elem (symbol collection)
    return elem;
}

/// <summary>
/// Creates a symbol element.
/// Options:
///  -```isTag```: if true, the symbol is setup to act as a tag (boolean, optional)
///  -```text```: if defined, sets the text/label of the symbol (string, optional)
///  -```link```: if defined, sets the link (URL) of the symbol (URL string, optional)
/// </summary>
/// <param name="opts">A options object</param>
/// <param name="containerElem">A containerElem to which automatically add the returned elem to. Should be a symbol collection elemement. Optional.</param>
/// <returns>The created symbol element</returns>
Machinata.UI.createSymbol = function (opts, containerElem) {
    // Init
    if (opts == null) opts = {};
    var elem = $("<div class='ui-symbol'></div>");
    // Tag
    if (opts.isTag == true) {
        elem.addClass("option-tag");
    }
    // Text
    if (opts.text != null) {
        var panelTextElem = $("<div class='panel text'></div>");
        if (opts.link != null) {
            var linkElem = $("<a class='ui-link'/>");
            linkElem.attr("href",opts.link);
            linkElem.text(opts.text);
            linkElem.appendTo(panelTextElem);
        } else {
            panelTextElem.text(opts.text);
        }
        panelTextElem.appendTo(elem);
    }
    // Menu
    //TODO
    // Add to container
    if (containerElem != null) containerElem.append(elem);
    // Return root elem (symbol)
    return elem;
}