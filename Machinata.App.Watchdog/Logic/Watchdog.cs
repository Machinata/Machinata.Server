using Machinata.Watchdog.Config;
using Machinata.Watchdog.Util;
using System;
using System.Collections.Generic;
using System.IO;

namespace Machinata.Watchdog.Logic {
    public class Watchdog {

        public int MaxWarningsPerHour = 6; //TODO
        public bool Verbose = false;
        public Config.Config GlobalConfig;

        private const string LAST_ALIVE_MESSAGE_SENT_FILENAME = "lastAliveMessageSent.txt";
        private const int LAST_ALIVE_MESSAGE_DAYS = 5;


        private List<Endpoint> _endpoints = new List<Endpoint>();
        private List<Contact> _globalContacts = new List<Contact>();
        private static object _logger = new object();

        public IEnumerable<Endpoint> Endpoints {
            get {
                return _endpoints;
            }
        }

        public Watchdog(Config.Config config) {
            this.Verbose = config.Verbose;
            this.GlobalConfig = config;
        }

        /// <summary>
        /// Global Contacts
        /// </summary>
        /// <param name="emailOrPhone"></param>
        public void RegisterContact(Contact contact) {
            _globalContacts.Add(contact);
        }

        public void RegisterEndpoint(Endpoint endpoint) {
            Console.WriteLine("Registering endpoint "+endpoint.Name);
            Console.WriteLine("   "+endpoint.URL);
            endpoint.Watchdog = this;
            _endpoints.Add(endpoint);
        }

        public void Monitor() {
            // Init
            string machineName = Environment.MachineName;
            Console.WriteLine("Starting monitor with " + _endpoints.Count + " endpoints on " + machineName);
            Console.WriteLine("Sending startup notification...");

            // Send startup notifs
            foreach (var contact in _globalContacts) {
                string message = DateTime.UtcNow.ToLongDateString();
                message += "\n";
                message += "\n" + "Registered "+_globalContacts.Count + " contacts";
                message += "\n";
                foreach (var contact2 in _globalContacts) {
                    message += "\n" + contact2;
                }
                message += "\n";
                message += "\n" + "Starting monitor with "+_endpoints.Count + " endpoints";
                message += "\n";
                foreach (var endpoint in _endpoints) {
                    message += "\n" + ("Endpoint "+endpoint.Name);
                    message += "\n" + endpoint.CompileLog(this.GlobalConfig.Verbose);
                }
                //if (contact.Contains('@')) {
                //    // Email
                //    try {
                //        if(Verbose) Console.WriteLine("   "+"Sending Email to "+contact);
                //        Email.SendEmail(contact, null, null, "NervesWatchdog: " + "Started on " + machineName, message, false);
                //    } catch (Exception e) {
                //        Console.WriteLine("***WARNING: Could not send Email to "+contact+": "+e.Message);
                //    }
                //}

             
                // Message
                try {
                    if (Verbose) Console.WriteLine("   " + "Sending message to " + contact);
                    contact.SendMessage("NervesWatchdog: " + "Started on " + machineName, message, this.GlobalConfig, null);
                } catch (Exception e) {
                    Console.WriteLine("***WARNING: Could not send message to " + contact + ": " + e.Message);
                }

            }
            Console.WriteLine("Done.");

            // Loop forever
            while (true) {
                // Loop endpoints
                List<Endpoint> transitionedEndpoints = new List<Endpoint>();
                foreach (var endpoint in _endpoints) {
                    if (endpoint.ShouldCheckEndpoint()) {
                        endpoint.CheckEndpoint();
                        bool didTransition = endpoint.ValidateEndpoint();
                        if (didTransition) transitionedEndpoints.Add(endpoint);
                        
                    }
                }

                // Compile report of all transitions
                foreach(var endpoint in transitionedEndpoints) {
                    string subject = "";
                    if (endpoint.IsHealthy()) {
                        Console.WriteLine("TRANSITION: " + endpoint.Name + " is HEALTHY");
                        subject = "INFO: " + endpoint.Name + " is " + "HEALTHY";
                    } else {
                        Console.WriteLine("TRANSITION: " + endpoint.Name + " is UNHEALTHY");
                        subject = "WARNING: " + endpoint.Name + " is " + "UNHEALTHY";
                    }
                    string emailData = endpoint.CompileLog(true);
                   
                    foreach (var contact in endpoint.Contacts) {

                        // Message
                        try {
                            if (Verbose) Console.WriteLine("   " + "Sending message to " + contact);
                            contact.SendMessage("NervesWatchdog: " + subject, emailData, endpoint.Config, endpoint);
                        } catch (Exception e) {
                            Console.WriteLine("***WARNING: Could not send message to " + contact + ": " + e.Message);
                        }

                    }
                }

                // Show state
                if (Verbose) {
                    Console.WriteLine("Endpoints:");
                    foreach (var endpoint in _endpoints) {
                        Console.WriteLine("   "+endpoint.Name + ": " + endpoint.IsHealthy().ToString());
                    }
                }


                // Send alive signal
               {

                    try {
                        var lastAliveSent = GetLastAliveTimestamp();

                        if (_aliveFileLastOpened < DateTime.UtcNow.AddMinutes(-1)) {
                            Console.WriteLine("Last alive message sent: " + lastAliveSent);
                            if (lastAliveSent < DateTime.UtcNow.AddDays(-LAST_ALIVE_MESSAGE_DAYS)) {
                               
                                foreach (var contact in this._globalContacts) {
                                    try {
                                        Console.WriteLine("Sending alive message to " + contact);
                                        contact.SendMessage("NervesWatchdog: Is alive and running", "Last alive message sent: " + lastAliveSent, this.GlobalConfig, null);
                                        SaveLastAliveTimestamp(DateTime.UtcNow);
                                    } catch (Exception e) {
                                        Console.WriteLine("***WARNING: Could not send message to " + contact + ": " + e.Message);
                                    }
                                }
                            }
                            _aliveFileLastOpened = DateTime.UtcNow;
                        }

                    } catch (Exception e) {
                        Console.WriteLine("***WARNING: Could not check or send alive message" + e.Message);
                    }

                }

                // Sleep
                if (Verbose) Console.WriteLine("Sleeping...");
                System.Threading.Thread.Sleep(1000);
            }
        }

        private DateTime _aliveFileLastOpened = DateTime.MinValue;

        private DateTime GetLastAliveTimestamp() {
            try {
                var content = File.ReadAllText(GetLastAliveTimestampFilePath());
                var ticks = long.Parse(content);
                return new DateTime(ticks);
            }
            catch (Exception) {
                return DateTime.MinValue;
            }
        }

        private void SaveLastAliveTimestamp(DateTime datetime) {
            string path = GetLastAliveTimestampFilePath();
            var content = datetime.Ticks.ToString();

            File.WriteAllText(path, content);
        }

        private string GetLastAliveTimestampFilePath() {
            return this.GlobalConfig.LoggingDestination + LAST_ALIVE_MESSAGE_SENT_FILENAME;
        }
    }
}
