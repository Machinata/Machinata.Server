using Machinata.Watchdog.Util;
using System;
using System.Collections.Generic;
using Machinata.Watchdog.Config;
using CommandLine;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Globalization;

namespace Machinata.App.Watchdog {
    class Program {

        static void Main(string[] args) {
            

            System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;

            Console.WriteLine("Starting Watchdog on " + Environment.MachineName + "...");
            // Loop forever
            while (true) {
                try {
                    // Configure
                    CommandLine.Parser.Default.ParseArguments<Arguments>(args)
                        .WithParsed<Arguments>(opts => StartWatchdogWithConfiguration(opts))
                        .WithNotParsed<Arguments>((errs) => HandleParseError(errs));
                } catch (Exception e) {
                    // Send warning
                    try {

                        // Cannot send email without config
                        // Email.SendEmail("support@nerves.ch", null, null, "NervesWatchdog: WARNING: Watchdog Crashed on " + Environment.MachineName, e.Message, false);
                        var argsstring = string.Join(", ", args);
                        
                        Console.WriteLine("ERROR - NOT ABLE TO LOAD CONFIGURATION WITH: " + argsstring);

                    } catch (Exception) { }
                    // Wait a while
                    System.Threading.Thread.Sleep(5000);
                }
            }
        }

        private static void HandleParseError(IEnumerable<Error> errs) {
            Console.WriteLine("Error parsing configuration file");
        }



        public static void StartWatchdogWithConfiguration(Arguments options) {
            Configuration globalConfig = null;

            // Config file
            var content = File.ReadAllText(options.Config);
            globalConfig = JsonConvert.DeserializeObject<Configuration>(content);

            // Create watchdog
            Machinata.Watchdog.Logic.Watchdog watchdog = new Machinata.Watchdog.Logic.Watchdog(globalConfig.Config);

            // Contacts
            foreach (var contact in globalConfig.Contacts.Where(c => c.HasAddress() == true)) {
                watchdog.RegisterContact(contact);
            }

            // Register endpoints
            foreach (var website in globalConfig.Websites) {

                try {

                    // Init empty
                    if (website.Config == null) {
                        website.Config = new Config();
                    }
                    if (website.Contacts == null) {
                        website.Contacts = new List<Contact>();
                    }


                    // Endpoint configuration
                    {
                        var fallbackConfiguration = BaseEndpointConfig.Default();


                        // Endpoint: Missing values from from config file
                        if (globalConfig.Config.Endpoint != null) {
                            Configuration.CopyProperties(globalConfig.Config.Endpoint, website);
                        }

                        // Endpoint: Missing values from from default c# values
                        Configuration.CopyProperties(fallbackConfiguration, website);
                    }
                    

                    // Configuration override
                    Configuration.CopyProperties(globalConfig.Config, website.Config);

                    
                    // Contacts (additional not override)
                    var contacts = globalConfig.Contacts.Concat(website.Contacts).Where(c => c.HasAddress());


                    // Endpoint: create and keep
                    Endpoint endpoint = new Endpoint(website.Name, website, website.Config, contacts.ToArray());
                    watchdog.RegisterEndpoint(endpoint);

                } catch (Exception e) {
                    Email.SendEmail("support@nerves.ch", null, null, "NervesWatchdog: WARNING: Could not load configuration for " + website.Name, e.Message, false, globalConfig.Config);
                }

            }

            // Initialize Loggers
            FileLogger.Initialize(watchdog.Endpoints.Select(e => e.Id), globalConfig.Config);


            // Start monitor
            watchdog.Monitor();
        }

    }
}



