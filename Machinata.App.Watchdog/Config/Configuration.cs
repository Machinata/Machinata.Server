using Machinata.App.Watchdog.Util;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Machinata.Watchdog.Config {

    public class Configuration {

        public Config Config { get; set; }

        public Contact[] Contacts { get; set; }
        public EndpointConfig[] Websites { get; set; }

       
            /// <summary>
            /// Used to create a custom config based on the global config
            /// </summary>
            /// <param name="source"></param>
            /// <param name="destination"></param>
            public static void CopyProperties( object source, object destination) {
            // Iterate the Properties of the destination instance and  
            // populate them from their source counterparts  
            PropertyInfo[] destinationProperties = destination.GetType().GetProperties();
            foreach (PropertyInfo destinationPi in destinationProperties) {

                if (destinationPi.CanWrite) { 
                PropertyInfo sourcePi = source.GetType().GetProperty(destinationPi.Name);
                var destinationValue = destinationPi.GetValue(destination, null);
                if (sourcePi != null) {
                    var sourceValue = sourcePi.GetValue(source, null);
                    if (destinationValue == null) {
                        destinationPi.SetValue(destination, sourceValue, null);
                    }
                }
                }
            }
        }

    }

   
    public class EmailConfig {
        public string Server;
        public string Port;
        public string Username;
        public string SenderAddress;
        public string SenderName;
        public string Password;
        public string OverrideAddress;
    }

    public class SMSConfig {
        public string Endpoint;
        public string Username;
        public string Password;
    }


    public class TelegramConfig {
        public string Endpoint;
        public string Token;
        public string ChatId;
    }

    public class Config {
        public bool Verbose;
        public string LoggingDestination;
        public EmailConfig Email { get; set; }
        public SMSConfig SMS { get; set; }
        public TelegramConfig Telegram { get; set; }
        public BaseEndpointConfig Endpoint { get; set; } // Default endpoint settings

        public override string ToString() {
            return JSON.ToJSON(this);
        }

    }

    public class EndpointConfig  : BaseEndpointConfig{
        public string Name;
        public string URL;
        public Config Config { get; set; } = new Config(); // Custom Endpoint configuration
        public List<Contact> Contacts { get; set; } = new List<Contact>(); // custom endpoint additi

      
        //public int CheckIntervalSeconds { get; set; } = 20;
        //public int PingsToKeep { get; set; } = 50;
        //public int UnhealthyThreshold { get; set; } = 4;
        //public int HealthyThreshold { get; set; } = 2;
        //public int HealthyMinContentLength { get; set; } = 20;
        //public int HealthyMaxResponseTime { get; set; } = 4000;
        //public int Timeout { get; set; } = 15000;
        //public bool SendSMSWarnings { get; set; } = true;
        //public bool SendEmailWarnings { get; set; } = true;
        //public bool HealthReport { get; set; } = false;

    }

    public class BaseEndpointConfig {
       
        public int? CheckIntervalSeconds { get; set; }
        public int? PingsToKeep { get; set; } 
        public int? UnhealthyThreshold { get; set; } 
        public int? HealthyThreshold { get; set; } 
        public int? HealthyMinContentLength { get; set; } 
        public int? HealthyMaxResponseTime { get; set; }
        public int? Timeout { get; set; } 
        public bool? SendTelegramWarnings { get; set; } 
        public bool? SendEmailWarnings { get; set; } 
        public bool? HealthReport { get; set; } 

        public TimeSpan Interval {
            get {
                return new TimeSpan(0, 0, this.CheckIntervalSeconds.Value);
            }
        }

        public static BaseEndpointConfig Default() {
            return new BaseEndpointConfig() {
                CheckIntervalSeconds = 20,
                PingsToKeep = 50,
                UnhealthyThreshold = 4,
                HealthyThreshold = 2,
                HealthyMinContentLength = 20,
                HealthyMaxResponseTime = 4000,
                Timeout = 15000,
                SendTelegramWarnings = true,
                SendEmailWarnings = true,
                HealthReport = false

            };
               

        }

    }


}
