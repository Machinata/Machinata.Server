﻿using Machinata.App.Watchdog.Util;
using Machinata.Watchdog.Util;
using System.Collections.Generic;

namespace Machinata.Watchdog.Config {

    public class Contact {
     
        public string Name;
        public string Email;
        public string TelegramChat;
        public string Mobile;

        public void SendMessage(string subject, string message, Config config, Endpoint endPoint) {

            var sendEmailWarning = config.Endpoint.SendEmailWarnings; // this is a global setting
            var sendTelegramWarning = config.Endpoint.SendTelegramWarnings;

            // If we have an endpoint specific setting, use that instead
            if (endPoint != null) {
                sendEmailWarning = endPoint.SendEmailWarnings;
                sendTelegramWarning = endPoint.SendTelegramWarnings;
            }

            if (string.IsNullOrWhiteSpace(this.Email) == false && sendEmailWarning == true) {
                Util.Email.SendEmail(this.Email, null, null, subject, message, false, config);
            } else if (string.IsNullOrWhiteSpace(this.Mobile) == false) {
                Util.SMS.SendSMS(this.Mobile, message, config);
            } else if (string.IsNullOrWhiteSpace(this.TelegramChat) == false && sendTelegramWarning == true) {
                Util.Telegram.SendMessage(this.TelegramChat, subject, message, config);
            }
        }

        public bool HasAddress() {
            return string.IsNullOrWhiteSpace(this.Email) == false
                || string.IsNullOrWhiteSpace(this.TelegramChat) == false
                || string.IsNullOrWhiteSpace(this.Mobile) == false;
        }

        public override string ToString() {
            return $"Name: {this.Name}, Email: {this.Email}, TelegramChat: {this.TelegramChat}, Phone: {this.Mobile}";
        }

        public static string ContactsAsJSON (IEnumerable<Contact> contacts) {
            return JSON.ToJSON(contacts);
        }
    
    }
}
