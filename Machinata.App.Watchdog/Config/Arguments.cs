using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using CommandLine;

namespace Machinata.Watchdog.Config {

    public class Arguments {

        [Option("config", Required = true, HelpText = "Project config file to be processed. Has to be one path to a valid json configuration.")]
        public string Config { get; set; }

    }

   
   
}
