﻿using System.IO;
using System.Net;
using System.Web;

namespace Machinata.Watchdog.Util {
    public class Telegram {

        //
        //    curl -X POST \
        //   -H 'Content-Type: application/json' \
        //   -d '{"chat_id": "-653819540", "text": "This is a test from curl 4", "disable_notification": false}' \
        //   https://api.telegram.org/bot5359107020:AAEA6PTSPQg9LcAZvBoSaoFlm6eJPRF55WE/sendMessage

        //https://stackoverflow.com/questions/9145667/how-to-post-json-to-a-server-using-c

        internal static void SendMessage(string telegramChat, string subject, string message, Config.Config config) {


            var endpoint = config.Telegram.Endpoint;
            var token = config.Telegram.Token;
            var path = "/sendMessage";
            var request = endpoint + token + path;
            int maxLength = 4096;
            string summaryTrail = "...";

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(request);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            // Subject to the message
            message = subject + "\n\n" + message;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream())) {

                var formatedMessage = HttpUtility.JavaScriptStringEncode(message);

                // Summarize message
                if (formatedMessage != null && formatedMessage.Length > maxLength) {
                    formatedMessage = formatedMessage.Substring(0, maxLength - summaryTrail.Length) + summaryTrail;
                }

                string json = "{\"chat_id\":\"" + telegramChat + "\"," +
                              "\"text\":\""+ formatedMessage + "\"}";

                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream())) {
                var result = streamReader.ReadToEnd();
            }
        }
    }
}
