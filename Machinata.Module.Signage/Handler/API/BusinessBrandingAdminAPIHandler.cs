using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Machinata.Module.Signage.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using System.Text;
using Machinata.Core.Model;
using Machinata.Module.Signage.Logic;

namespace Machinata.Product.NervesEvents.Handler.API {
    public class BusinessBrandingAdminAPIHandler : Module.Admin.Handler.AdminAPIHandler {
        

        [RequestHandler("/api/admin/signage/businesses/business/{publicId}/edit")]
        public void Edit(string publicId) {

            // Rights
            this.RequireGenericAdminRights();

            var entity = DB.Businesses().GetByPublicId(publicId);

            // Populate and sync back
            var branding = new BusinessBranding(entity);
            branding.Populate(this,new FormBuilder(Forms.Admin.EDIT));
            branding.Validate();
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("edit-success");
            
        }
        


    }
}