﻿signage.en=Signage

sign.en=Sign
signs.en=Signs

qrcodes.en=QR-Codes
qrcodes.de=QR-Codes

wifi-name.en=WiFi Name
wifi-name.de=WiFi Name

wifi-password.en=Password
wifi-password.de=Passwort

station-name.en=Station
station-name.de=Haltestelle

in-direction.en=In direction
in-direction.de=In Richtung

view-at-zvvch.en=View at ZVV.ch
view-at-zvvch.de=Auf ZVV.ch anschauen

zvvch.en=ZVV.ch
zvvch.de=ZVV.ch

opdch.en=OPDCH
opdch.de=OPDCH

refresh-timetable.en=Refresh
refresh-timetable.de=Aktualisieren

station-distance-walk.en=walk
station-distance-walk.de=zu Fuss

calendar-prev-day.en=Previous Day
calendar-prev-day.de=Vorheriger Tag

calendar-next-day.en=Next Day
calendar-next-day.de=Nächster Tag

calendar-free.en=Free
calendar-free.de=Verfügbar

calendar-free-until.en=Free until
calendar-free-until.de=Verfügbar bis

calendar-busy.en=Occupied
calendar-busy.de=Besetzt

calendar-busy-until.en=Occupied until
calendar-busy-until.de=Besetzt bis

uber-pickup.en=Uber Pickup
uber-pickup.de=Uber Pickup

uber-connecting.en=Connecting...
uber-connecting.de=Verbinden...

weblink-connecting.en=Connecting...
weblink-connecting.de=Verbinden...