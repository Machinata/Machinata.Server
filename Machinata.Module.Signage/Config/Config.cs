using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Signage {
    public class Config {

        public static bool SignageSendErrorMessagesViaEmail = Core.Config.GetBoolSetting("SignageSendErrorMessagesViaEmail");

    }
}
