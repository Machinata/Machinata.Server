using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Ticketing {
    public class Config {

        public static string SupportTicketPublicPath = Core.Config.GetStringSetting("SupportTicketPublicPath", "tickets/ticket");

        /// <summary>
        /// Add Admin email to BCC on Ticket emails to customers
        /// </summary>
        public static bool SupportTicketCustomerEmailNotficationBCC = Core.Config.GetBoolSetting("SupportTicketCustomerEmailNotficationBCC");

        /// <summary>
        /// The support ticket customer support response enabled.
        /// Deactivate this if customer doesnt use this or there are no proper template for a response
        /// </summary>
        public static bool SupportTicketCustomerSupportResponseEnabled = Core.Config.GetBoolSetting("SupportTicketCustomerSupportResponseEnabled");


        /// <summary>
        /// Max File Upload Size in MB per file
        /// </summary>
        public static int SupportTicketFileUploadMaxSizeMB = 10;


        public static string SupportTicketBoilerPlates = Core.Config.GetStringSetting("SupportTicketBoilerPlates", "/Misc/Support/BoilerPlates");



     
        /// <summary>
        /// If true and SupportTicketStaffEmailNotficationEnabled is enabled, staff is notified only if the user as explicitly sent a message along with the support ticket.
        /// </summary>
        public static bool SupportTicketStaffEmailNotficationRequiresUserMessage = Core.Config.GetBoolSetting("SupportTicketStaffEmailNotficationRequiresUserMessage");



    }
}
