using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using System.ComponentModel;
using Machinata.Core.Cards;
using Machinata.Core.Util;

namespace Machinata.Module.Ticketing.Model {


    /// <summary>
    /// 
    /// </summary>
    [NotMapped]
    public class SupportTicketBoilerPlate  {
        public string Title { get; set; }
        public string Content { get; set; }
        public string Preview { get; set; }
        public string Category { get; set; }
        public string Language { get; set; }

        /// <summary>
        /// Gets the boiler plate 
        /// </summary>
        /// <param name="db"></param>
        /// <param name="ticket"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public static List<SupportTicketBoilerPlate> GetBoilerPlates(ModelContext db, SupportTicket ticket, string language) {
            var result = new List<SupportTicketBoilerPlate>();

            var boilerPlateNode = db.ContentNodes().GetNodeByPath(Config.SupportTicketBoilerPlates, false);
            if (boilerPlateNode == null) {
                return result;
            }

            var categoryPages = db.ContentNodes()
                .Include(nameof(ContentNode.Children) + "." + nameof(ContentNode.Children) + "." + nameof(ContentNode.Children))
                .GetPagesAtPath(Config.SupportTicketBoilerPlates);

            foreach (var categoryPage in categoryPages) {

                var boilerPlatePages = categoryPage.
                    ChildrenForType(ContentNode.NODE_TYPE_NODE)
                    .Where(p => p.IsSystemNode == false).OrderBy(p => p.Sort);

                foreach (var boilerPlatePage in boilerPlatePages) {

                    boilerPlatePage.IncludeContent();
                    var trans = boilerPlatePage.TranslationForLanguage(language);
                    var title = trans.Title;
                    var content = trans.ChildrenForType(ContentNode.NODE_TYPE_HTML).FirstOrDefault()?.Value;
                    //content = Core.Util.HTML.GetHTMLNodeInnerTextByNodeType(content, "p");
                    
                    // Inject
                    content = content.Replace("{user.name}", ticket?.User?.Name);
                    content = content.Replace("{user.email}", ticket?.Email);

                    var boilerPlate = new SupportTicketBoilerPlate();
                    boilerPlate.Content = content;
                    boilerPlate.Category = categoryPage.Name;
                    boilerPlate.Title = title;
                    boilerPlate.Preview = Core.Util.String.CreateSummarizedText(Core.Util.HTML.GetHTMLNodeInnerTextByNodeType(content, "p"), 300, true, true, true);
                    boilerPlate.Language = trans.Value;
                    result.Add(boilerPlate);
                }
            }

            return result;
        }

    }


}
