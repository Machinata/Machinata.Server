using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using System.ComponentModel;
using Machinata.Core.Cards;
using Machinata.Core.Util;

namespace Machinata.Module.Ticketing.Model {
    
    public class DefaultSupportTicketType : SupportTicketType {

        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public DefaultSupportTicketType() {

        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////


        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////


        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////


        public override Dictionary<string, ModelObject> GetEntities() {
            return new Dictionary<string, ModelObject>() { { "", new SupportTicket() } };
        }

        public override string GetId() {
            return "default";
        }


        #endregion

    }


}
