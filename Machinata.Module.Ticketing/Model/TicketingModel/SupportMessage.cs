using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Data;
using static Machinata.Module.Ticketing.Model.SupportTicket;
using Machinata.Core.Util;

namespace Machinata.Module.Ticketing.Model {

    [Serializable()]
    [ModelClass]
    public partial class SupportMessage : ModelObject {
        private const string CONTENT_FILE_SOURCE = "SupportTicket";

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public SupportMessage() {
            // Parameterless constructure is required for reflection...

        }

        public override void Validate() {
            if (!string.IsNullOrEmpty(this.Message) && Core.Util.String.CheckForSpamLinks(this.Message, false)) {
                throw new BackendException("links-not-allowed", "Sorry, to avoid spam messages we don't allow links.");
            }
        }



        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [Column]
        [ForeignKey("TicketId")]
        [InverseProperty("Messages")]
        [FormBuilder]
        public SupportTicket Ticket { get; set; }

        [Column]
        [FormBuilder]
        public int? TicketId { get; set; }

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public User User { get; set; }
        
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Frontend.CREATE)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool CustomerMessage { get; set; }


        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [DataType(DataType.Upload)]
        public string File { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public TicketStatus? PreviousTicketStatus  { get; set; }


        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public TicketStatus? NewTicketStatus { get; set; }

        

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [NotMapped]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Author {
            get
            {
                if (CustomerMessage) {
                    Include(nameof(Ticket));
                    return Ticket.Author;
                }
                return User?.Name;
            }
        }

        [NotMapped]
        [FormBuilder()]
        public string Summary {
            get {
                if (this.Message != null) {
                    return Core.Util.String.CreateSummarizedText(this.Message, 30, true, true);
                }
                return null;
            }
        }

        [NotMapped]
        [FormBuilder()]
        public string FileName {
            get {
                if (this.File != null) {
                    return Core.Util.Files.GetFileNameWithExtension(this.File);
                }
                return null;
            }
        }

        [NotMapped]
        [FormBuilder()]
        public ContentFile.ContentCategory? FileCategory {
            get {
                if (this.File != null) {
                    return DataCenter.GetCategoryFromFilename(this.FileName);
                }
                return null;
            }
        }

        [NotMapped]
        [FormBuilder()]
        public bool HasImage {
            get {
                if (this.FileCategory != null) {
                    return this.FileCategory == ContentFile.ContentCategory.Image;
                }
                return false;
            }
        }

        [NotMapped]
        [FormBuilder()]
        public bool HasFile {
            get {
                return (this.File != null);
            }
        }


        [NotMapped]
        [FormBuilder()]
        public bool StatusDidChange {
            get {
                return (this.PreviousTicketStatus != this.NewTicketStatus);
            }
        }

        [NotMapped]
        [FormBuilder()]
        public string StatusDidChangeMessage {
            get {
                if (this.StatusDidChange == true && this.NewTicketStatus != null) {
                    return "{text.support-ticket.message.ticket-status-changed." + this.NewTicketStatus.ToString().ToDashedLower()  +"}";
                }
                return null;
            }
        }




        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// If a file from the request is available this adds a ContentFile and saves the URL in contentFile.ContentUR
        /// </summary>
        /// <param name="handler"></param>
        /// <returns>the uploaded ContentFile or null if there was no file</returns>
        public ContentFile AddFile(Core.Handler.Handler handler) {

            if (handler.Request.HasFiles() == true) {

                // Validate
                ValidateFile(handler);

                // Upload
                var contentFile = Core.Data.DataCenter.UploadFile(handler.Request.File(), handler.User, CONTENT_FILE_SOURCE);
                handler.DB.ContentFiles().Add(contentFile);
                handler.DB.SaveChanges(); // to get id


                // Register
                this.File = contentFile.ContentURL;

                return contentFile;
            }
            return null;

        }

        public static void ValidateFile(Core.Handler.Handler handler, string customExceptionDataKey = null, string customExceptionDataValue= null) {
            if (handler.Request.HasFiles() == true) {
                // Check image size
                int maxRequestLength = Config.SupportTicketFileUploadMaxSizeMB * 1024 * 1024;
                if (handler.Context.Request.ContentLength > maxRequestLength) {
                    var exception =  new LocalizedException(handler, "file-too-large");
                    if (customExceptionDataKey != null) {
                        exception.CustomData(customExceptionDataKey, customExceptionDataValue);
                    }
                    throw exception;
                }
            }
        }

        public void SetTicketStatus(SupportTicket.TicketStatus oldStatus, SupportTicket.TicketStatus newStatus) {
            this.PreviousTicketStatus = oldStatus;
            this.NewTicketStatus = newStatus;

        }

        #endregion

        #region Override Methods //////////////////////////////////////////////////////////////////
        public override string ToString() {
            return this.Message;
        }


        public override void OnDelete(ModelContext db) {
            if (string.IsNullOrEmpty(this.File) == false) {
                Core.Data.DataCenter.DeleteFileByContentURL(db, this.File, CONTENT_FILE_SOURCE);
            }
            base.OnDelete(db);
        }

       

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextSupportMessageExtenions {
        public static DbSet<SupportMessage> SupportMessages(this Core.Model.ModelContext context) {
            return context.Set<SupportMessage>();
        }
        public static IQueryable<SupportMessage> SupportMessages(this Core.Model.User user) {
            return user.Context.SupportMessages().Where(o => o.User.Id == user.Id);
        }
    }

  

    #endregion
}
