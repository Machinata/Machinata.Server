using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Ticketing.Model {

    [Serializable()]
    [ModelClass]
    public partial class SupportStaffUser : ModelObject , IPublishedModelObject {

        public const string USER_STAFF_ALL_TYPES_KEY = "user-staff-all-types";
        public const string USER_STAFF_ALL_CATEGORIES_KEY = "user-staff-all-categories";
        public const string SUPPORT_STAFF_FILTER_WILDCARD = "*";


        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public SupportStaffUser() {
            // Parameterless constructure is required for reflection...

        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////


        /// <summary>
        /// Admin User
        /// </summary>
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public User User { get; set; }

        /// <summary>
        /// Category to opt in
        /// Can be *
        /// </summary>
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string Category { get; set; }

        /// <summary>
        /// Type to opt in
        /// Can be *, or e.g. SupportTicketTypeFeedback
        /// </summary>
        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public string Type { get; set; }


        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder(Forms.System.LISTING)]
        public string UserName {
            get {
                return this.User?.Name;
            }
        }

        [FormBuilder(Forms.System.LISTING)]
        public string Title {
            get {
                return this.ToString();
            }
        }


        [FormBuilder(Forms.System.LISTING)]
        public string UserEmail {
            get {
                return this.User?.Email;
            }
        }

        /// <summary>
        /// E.g Feedback
        /// </summary>
        [FormBuilder(Forms.Admin.LISTING, Name ="Type")]
        [FormBuilder(Forms.Admin.VIEW, Name = "Type")]
        public string TypeId {
            get {
                if (this.Type == "*") {
                    return "*";
                }
                var typeId = SupportTicketType.GetTypeByTypeName(this.Type)?.GetId();
                return typeId;
            }
        }

        public bool Published {
            get {
                return this.User?.Enabled == true;
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        #endregion

        #region Override Methods //////////////////////////////////////////////////////////////////
        public override string ToString() {
            return $"{this.User?.Username}: {this.Category} - {this.TypeId}";
        }
        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextSupportStaffExtenions {
        public static DbSet<SupportStaffUser> SupportStaffUsers(this Core.Model.ModelContext context) {
            return context.Set<SupportStaffUser>();
        }
        
    }

    #endregion
}
