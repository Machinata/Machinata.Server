﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;


namespace Machinata.Module.Ticketing.Handler {


    public class SupportStaffAdminAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Model.SupportStaffUser> {


        protected override void EditPopulate(Model.SupportStaffUser entity) {
            SetStarValues(entity);

            base.EditPopulate(entity);


        }


        /// <summary>
        /// Needed because * as valu does some kind of custom input in the form builder
        /// </summary>
        /// <param name="entity"></param>
        private void SetStarValues(Model.SupportStaffUser entity) {
            var type = this.Params.String("type");
            var category = this.Params.String("category");

            if (type == Model.SupportStaffUser.USER_STAFF_ALL_TYPES_KEY) {
                entity.Type = "*";
            }

            if (category == Model.SupportStaffUser.USER_STAFF_ALL_CATEGORIES_KEY) {
                entity.Category = "*";
            }
        }

        protected override void CreatePopulate(Model.SupportStaffUser entity) {
            SetStarValues(entity);

            base.CreatePopulate(entity);
        }

        [RequestHandler("/api/admin/tickets/staff-users/staff-user/{publicId}/edit")]
        public void Edit(string publicId) {
            this.CRUDEdit(publicId);
        }



        [RequestHandler("/api/admin/tickets/staff-users/create")]
        public void Create() {

            
            var entity = this.CRUDCreate("entity");

        }

        [RequestHandler("/api/admin/tickets/staff-users/staff-user/{publicId}/delete")]
        public void Delete(string publicId) {
            this.CRUDDelete(publicId);
        }

    }
}
