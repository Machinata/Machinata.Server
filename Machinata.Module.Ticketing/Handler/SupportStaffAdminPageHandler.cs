﻿using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Ticketing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Ticketing.Handler {
    public class SupportStaffAdminPageHandler : Admin.Handler.AdminPageTemplateHandler {
       

        [RequestHandler("/admin/tickets/staff-users")]
        public void Default() {

            var entities = this.DB.SupportStaffUsers()
                .Include(nameof(Model.SupportStaffUser.User))
                .AsQueryable();
            entities = this.Template.Filter(entities, this,
              new string[] { nameof(Model.SupportStaffUser.Category),
                    nameof(Model.SupportStaffUser.Type),
                    nameof(Model.SupportStaffUser.Category),
                    nameof(Model.SupportStaffUser.UserName),
                    nameof(Model.SupportStaffUser.UserEmail) });

            entities = this.Template.Paginate(entities, this);

            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: entities,
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/tickets/staff-users/staff-user/{entity.public-id}"
                );

            // Navigation
            this.Navigation.Add("tickets", "{text.support}");
            this.Navigation.Add("staff-users", "{text.support.staff-users}");
        }

        [RequestHandler("/admin/tickets/staff-users/create")]
        public void Create() {


            var entity = new SupportStaffUser();
            FormBuilder form = GetCreateEditForm(entity);

            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: form,
                apiCall: "/api/admin/tickets/staff-users/create",
                onSuccess: "/admin/tickets/staff-users/staff-user/{entity.public-id}"
                );

            // Navigation
            this.Navigation.Add("tickets", "{text.support}");
            this.Navigation.Add("staff-users", "{text.support.staff-users}");
            this.Navigation.Add("create");
        }

        private FormBuilder GetCreateEditForm(SupportStaffUser staffUser) {
            var form = new FormBuilder(Forms.Admin.CREATE);

            // Users
            {
                var users = this.DB.Users().HideFrontendCreationUsers().ToList().Where(u => u.IsAdminUser == true);
                form.DropdownListEntities("user", users, staffUser.User, true);
            }

            // Categories
            {

                //var categories = this.DB.SupportTickets().Select(e => e.Category).Distinct().ToDictionary(k => k, v => v);
                //categories["*"] = SupportStaffUser.USER_STAFF_ALL_CATEGORIES_KEY; // * cant be used as id

                var categories = this.DB.SupportTickets().Select(e => e.Category).Distinct().ToList().Select(t=> new KeyValuePair<string,string>(t,t)).ToList();
                categories.Add( new KeyValuePair<string, string>("*", SupportStaffUser.USER_STAFF_ALL_CATEGORIES_KEY)); // * cant be used as id
                var value = staffUser.Category;
                if (value == SupportStaffUser.SUPPORT_STAFF_FILTER_WILDCARD) {
                    value = SupportStaffUser.USER_STAFF_ALL_CATEGORIES_KEY;
                }
                form.DropdownList("category", "category", categories, value, true);
            }

            // Types
            {
                //var types = SupportTicketType.GetAllTypes()
                //    .Where(t => t.GetType() != typeof(DefaultSupportTicketType))
                //    .Where(t => t.GetType() != typeof(TestSupportTicketType))
                //    .ToDictionary(k => k.GetId(), v => v.TypeName);
                //types[SupportStaffUser.SUPPORT_STAFF_FILTER_WILDCARD] = SupportStaffUser.USER_STAFF_ALL_TYPES_KEY; // * cant be used as id . and keys are mixed up with values general formbuilder problem

                var types = SupportTicketType.GetAllTypes()
                    .Where(t => t.GetType() != typeof(DefaultSupportTicketType))
                    .Where(t => t.GetType() != typeof(TestSupportTicketType))
                    .ToList().Select(t => new KeyValuePair<string,string>(t.GetId(), t.TypeName)).ToList();


                //   types[SupportStaffUser.SUPPORT_STAFF_FILTER_WILDCARD]
                types.Add(new KeyValuePair<string, string>( "*", SupportStaffUser.USER_STAFF_ALL_TYPES_KEY)); // * cant be used as id

                var value = staffUser.Type;
                if (value == SupportStaffUser.SUPPORT_STAFF_FILTER_WILDCARD) {
                    value = SupportStaffUser.USER_STAFF_ALL_TYPES_KEY;
                }
                form.DropdownList("type", "type", types, value, true);
            }

          

            return form;
        }

        [RequestHandler("/admin/tickets/staff-users/staff-user/{publicId}")]
        public void StaffUser(string publicId) {

            var entity = this.DB.SupportStaffUsers()
                .Include(nameof(SupportStaffUser.User))
                .GetByPublicId(publicId);

            this.Template.InsertPropertyList(
                variableName: "entity",
                entity: entity,
                form: new FormBuilder(Forms.Admin.VIEW)
                    );

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("tickets", "{text.support}");
            this.Navigation.Add("staff-users", "{text.support.staff-users}");
            this.Navigation.Add("staff-user/" + entity.PublicId, entity.ToString());

        }

        [RequestHandler("/admin/tickets/staff-users/staff-user/{publicId}/edit")]
        public void Edit(string publicId) {

            var entity = this.DB.SupportStaffUsers()
                .Include(nameof(SupportStaffUser.User))
                .GetByPublicId(publicId);
            var form = GetCreateEditForm(entity);
            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: form,
                apiCall: $"/api/admin/tickets/staff-users/staff-user/{publicId}/edit",
                onSuccess: $"/admin/tickets/staff-users/staff-user/{publicId}"
            );

            // Navigation
            this.Navigation.Add("tickets", "{text.support}");
            this.Navigation.Add("staff-users", "{text.support.staff-users}");
            this.Navigation.Add("staff-user/" + entity.PublicId, entity.ToString());
            this.Navigation.Add("edit");

        }


    }
}
