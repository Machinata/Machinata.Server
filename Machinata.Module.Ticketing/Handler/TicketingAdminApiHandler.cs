
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Module.Ticketing.Model;
using Machinata.Core.Builder;
using Machinata.Module.Admin.Handler;
using Machinata.Module.Ticketing.Messaging;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Ticketing.Handler {


    public class TicketingAdminApiHandler : Module.Admin.Handler.AdminAPIHandler {

        public override string PackageName
        {
            get
            {
                return "Machinata.Module.Ticketing";
            }
        }

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("tickets");
        }
        /// <summary>
        /// Creates and Sends the support message.
        /// Optionally override new status= processed
        /// </summary>
        /// <param name="publicTicketId">The public ticket identifier.</param>
        [RequestHandler("/api/admin/tickets/{publicTicketId}/create-message")]
        public void CreateSupportMessage(string publicTicketId) {
            this.RequireWriteARN();

            // Response enabled
            if (Config.SupportTicketCustomerSupportResponseEnabled == false) {
                throw new BackendException("not-supported", "Support tickets reply is not supported");
            }
        
            var message = Params.String("message", null);
       
            var ticket = this.DB.SupportTickets().GetByPublicId(publicTicketId);
            var oldStatus = ticket.Status;
            ticket.Populate(this, new FormBuilder("admin-response"));
            ticket.LoadAllImplementationData();

            // Create message
            var supportMessage = ticket.AddMessageByAdmin(User, message, oldStatus, ticket.Status);

            // Save changes 
            DB.SaveChanges();

            SendAPIMessage("create-message-success", new {
                Id = supportMessage.PublicId
            });

            // Email to customer
            var email = ticket.CreateEmail(DB, SupportTicket.Receivers.Customer, ticket.Language);

            // Send
            email.SendTicketEmail(ticket, ticket.Category);
        }

        [RequestHandler("/api/admin/tickets/{publicTicketId}/status")]
        public void ChangeSupportTicketStatus(string publicTicketId) {
            this.RequireWriteARN();

            var message = Params.String("message", null);
            var status = this.Params.Enum<SupportTicket.TicketStatus?>("status", null);
            var ticket = this.DB.SupportTickets().GetByPublicId(publicTicketId);

            // Change status
            ticket.ChangeStatus(status);

            // Save changes
            DB.SaveChanges();

            // Email to customer
            var email = ticket.CreateEmail(DB, SupportTicket.Receivers.Customer, ticket.Language);
            email.SendTicketEmail(ticket,ticket.Category);


            SendAPIMessage("change-status-success");
        }

        [RequestHandler("/api/admin/tickets/ticket/{publicId}/delete")]
        public void TicketDelete(string publicId) {
            // Make changes and save
            var entity = this.DB.SupportTickets().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            foreach (var message in entity.Messages.ToList()) {
                DB.DeleteEntity(message);
            }
            this.DB.DeleteEntity(entity);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success");
        }

        [RequestHandler("/api/admin/tickets/message/{publicId}/delete")]
        public void MessageDelete(string publicId) {
            // Make changes and save
            var entity = this.DB.SupportMessages().GetByPublicId(publicId);

            this.DB.DeleteEntity(entity);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success");
        }

        [RequestHandler("/api/admin/tickets/message/{publicId}/edit")]
        public void MessageEdit(string publicId) {
            // Make changes and save
            var entity = this.DB.SupportMessages().GetByPublicId(publicId);
            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                Message = new {
                    PublicId = entity.PublicId
                }
            });
        }


        [RequestHandler("/api/admin/tickets/ticket/{publicId}/edit")]
        public void TicketEdit(string publicId) {
            // Make changes and save
            var entity = this.DB.SupportTickets().GetByPublicId(publicId);
            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                Ticket = new {
                    PublicId = entity.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/tickets/ticket/{publicId}/edit-extension/{extensionId}")]
        public void TicketExtensionEdit(string publicId, string extensionId) {
            // Make changes and save
            var entity = this.DB.SupportTickets().GetByPublicId(publicId);
            var extensionData = entity.LoadExtensionData();
            var extensionEntity = extensionData.Item2[extensionId];
            extensionEntity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();
            entity.ExtensionData[extensionId] = extensionEntity;
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                Ticket = new {
                    PublicId = entity.PublicId,
                    ExtensionId = extensionId
                }
            });
        }

        [RequestHandler("/api/admin/tickets/ticket/{publicId}/close")]
        public void CloseTicket(string publicId) {
            // Make changes and save
            var entity = this.DB.SupportTickets().GetByPublicId(publicId);
            entity.ChangeStatus(SupportTicket.TicketStatus.Closed);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("close-success", new {
                Ticket = new {
                    PublicId = entity.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/tickets/{publicTicketId}/boiler-plates")]
        public void TicketBoilerPlates(string publicTicketId) {

            var ticket = this.DB.SupportTickets()
                .Include(nameof(SupportTicket.User))
                .GetByPublicId(publicTicketId);

            var language = this.Params.String("language", ticket.Language);

            if (string.IsNullOrWhiteSpace(language) == true) {
                language = ticket?.User.Language;
            }

            List<SupportTicketBoilerPlate> result = SupportTicketBoilerPlate.GetBoilerPlates(this.DB, ticket, language);

            // Return
            this.SendAPIMessage("success", new { BoilerPlates = result });
        }

        
    }
}
