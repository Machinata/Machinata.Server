using Machinata.Core.Templates;
using Machinata.Core.Model;

using System.Linq;
using Machinata.Core.Builder;
using Machinata.Module.Ticketing.Model;
using static Machinata.Module.Ticketing.Model.SupportTicket;
using Machinata.Core.Exceptions;
using Machinata.Module.Ticketing;
using Machinata.Core.Messaging;
using System.Collections.Generic;

namespace Machinata.Module.Ticketing.Messaging {
    public class SupportTicketEmailTemplate : EmailTemplate {

        public SupportTicket Ticket { get; private set; }
        public Receivers Receiver { get; private set; }

        public SupportTicketEmailTemplate(ModelContext db, SupportTicket ticket, Receivers receiver, string templateName, string extension = ".htm", string language = null) : base(db, templateName, extension, language) {
            this.Ticket = ticket;
            this.Receiver = receiver;
        }

        public override void Compile() {
            if (Ticket.Context != null) {
                this.Ticket.Include(nameof(Ticket.Messages));
            }

            // Init
            bool newTicket = Ticket.Messages.Count == 1;

            // Subject & Introduction
            string subject = null;
            string introduction = null;
            string salutation = null;
            if (Receiver == Receivers.Admin) {
                subject = newTicket ?
                    "{text.support-email-subject-admin-new}"
                  : "{text.support-email-subject-admin}";

                introduction = newTicket ?
                "{text.support-email-introduction-admin-new}"
              : "{text.support-email-introduction-admin}";

                salutation = "{text.support-email-salutation-admin}";

            } else if (Receiver == Receivers.Customer) {
                subject = newTicket ?
                "{text.support-email-subject-customer-new}"
              : "{text.support-email-subject-customer}";

                introduction = newTicket ?
               "{text.support-email-introduction-customer-new}"
             : "{text.support-email-introduction-customer}";

                salutation = "{text.support-email-salutation-customer}";

            } else {
                throw new BackendException("receiver-error", $"{Receiver} is not supported");
            }

            // CMS Content
            this.InsertContent(
                variableName: "support-ticket.cms-content",
                cmsPath: "/General/SupportTicket/EmailText",
                throw404IfNotExists: false,
                db: _db
            );

            // Conclusion
            var conclusion = this.Ticket.Status >= TicketStatus.Closed ? "{text.support-email-conclusion}" : string.Empty;

            // Template
            this.InsertVariables(introduction, conclusion, salutation);

            // Subject translations
            var subjectTextTemplate = new PageTemplate(subject);
            subjectTextTemplate.Language = this.Language;
            subjectTextTemplate.InsertTextVariables();
            subjectTextTemplate.InsertVariables("support-ticket", this.Ticket);
            subject = subjectTextTemplate.Content;

            if (string.IsNullOrEmpty(this.Subject)) {
                this.Subject = subject;
            }

            base.Compile();

        }

        private void InsertVariables(string introduction, string conclusion, string salution) {
            this.InsertTemplateVariables();
            this.InsertTextVariables();

            // Path
            if (this.Receiver == Receivers.Customer) {
                this.InsertVariable("ticket-public-path", Config.SupportTicketPublicPath);
            } else {
                this.InsertVariable("ticket-public-path", "admin/tickets/ticket");
            }

            // Ticket infos
            this.InsertPropertyList(
                variableName: "support-ticket",
                entity: Ticket,
                form: new FormBuilder(Forms.Admin.EMAIL),
                showCard: false
            );

            this.InsertVariable("support-ticket.infos", Ticket.Infos.ToString());
            this.InsertVariable("support-ticket.introduction", introduction);
            this.InsertVariable("support-ticket.conclusion", conclusion);
            this.InsertVariable("support-ticket.salution", salution);
            this.DiscoverVariables();
            this.InsertTextVariables();

            this.InsertVariables("support-ticket", this.Ticket);
        }

        /// <summary>
        /// Sends the email to the matching StaffUsers 
        /// If no active StaffUsers are defined the email will be sent to the Fallback/BackwardsComp SendNotificationEmail
        /// </summary>
        /// <param name="handler"></param>
        public void SendEmailToStaff(Core.Handler.Handler handler) {
            var staffUsers =
                handler.DB.SupportStaffUsers()
            .Include(nameof(SupportStaffUser.User))
            .Where(su => su.User != null && su.User.Enabled == true).ToList();

            if (Config.SupportTicketStaffEmailNotficationRequiresUserMessage == true) {
                if (string.IsNullOrWhiteSpace(this.Ticket?.LatestSupportMessage?.Message) == true) {
                    // ignore this message
                    return;
                }
            }

            if (staffUsers.Any() == true) {
                // do matching logic

                var staffUsersToSendTo = staffUsers
                    .Where(su => su.Category == SupportStaffUser.SUPPORT_STAFF_FILTER_WILDCARD || su.Category == this.Ticket.Category)
                    .Where(su => su.TypeId == SupportStaffUser.SUPPORT_STAFF_FILTER_WILDCARD || su.TypeId == this.Ticket.Type).ToList();

                var emailsSent = new List<string>();
                foreach (var staffUser in staffUsersToSendTo) {

                    handler.Log("Found match for staff user: " + staffUser.ToString());
                    handler.Log("\t: category" + this.Ticket.Category);
                    handler.Log("\t: type" + this.Ticket.Type);

                    if (emailsSent.Contains(staffUser.UserEmail) == true) {
                        handler.Log("Email for staff user already sent");
                    } else {
                        var contact = new EmailContact();
                        contact.Address = staffUser.UserEmail;
                        contact.Name = staffUser.UserName;
                        var contacts = new List<EmailContact>() { contact };
                        this.Send(to: contacts, cc: null, bcc: null, attachements: null);
                        emailsSent.Add(staffUser.UserEmail);
                    }
                    handler.Log("---------------------------");
                }
            } else {
                this.SendNotificationEmail();

            }
        }
    }
}