using Machinata.Core.Messaging;
using Machinata.Core.Templates;
using Machinata.Module.Ticketing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Machinata.Core.Model.MailingUnsubscription;

namespace Machinata.Module.Ticketing.Messaging {
    public static class EmailTemplateExtension {
        public static void SendTicketEmail(
            this SupportTicketEmailTemplate template,
            SupportTicket ticket,
            string unsubscribeSubtype, 
            IEnumerable<EmailAttachment> attachements = null
            ) {
            ticket.Include(nameof(ticket.User));

            string bcc = null;

            // Send duplicates to notification emails?
            if (template.Receiver == SupportTicket.Receivers.Customer &&  Config.SupportTicketCustomerEmailNotficationBCC) {
                bcc = string.Join(",", Core.Config.NotificationEmails);
            }

            // Send the email
            if (ticket.User != null) {
                template.SendUserEmail(
                    user: ticket.User,
                    email: ticket.Email,
                    unsubsribeType: Categories.Ticket,
                    unsubscribeSubtype: unsubscribeSubtype,
                    attachements: attachements,
                    bcc: bcc);
            } else {
                template.SendEmail(
                    email: ticket.Email,
                    unsubsribeType: Categories.Ticket,
                    unsubscribeSubtype: unsubscribeSubtype,
                    attachements: attachements,
                    bcc: bcc);
            }


        }
    }
}
