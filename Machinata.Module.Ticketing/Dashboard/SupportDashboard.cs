﻿using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Module.Admin.Dashboard;
using Machinata.Module.Reporting.Model;
using Machinata.Module.Ticketing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Ticketing.Dashboard {
    class SupportDashboard {

       [DashboardLoader]
        public static void LoadDashboard(DashboardLoader loader) {

            var section = new SectionNode();
            section.SetTitle("Support");
            section.Style = "W_H2";

            var layout = new LayoutNode();
            layout.TwoColumns();


            // Open tickets
            {
                var donut = new DonutNode();
                donut.Theme = "default";
                donut.SetTitle("Open Tickets");
                donut.SetSubTitle(loader.GetNodeSubtitle("by Categories"));
                donut.LegendLabelsShowValue = true;


                var tickets = loader.DB.SupportTickets().Where(t => t.Status == SupportTicket.TicketStatus.AwaitingReply).AsQueryable();
                tickets = loader.FilterItemsForTimerangeCreated(tickets);
                if (tickets.Any()) {
                    var cats = tickets.GroupBy(t => t.Category).Select(g => new { Category = g.Key, Count = g.Count() });
                    foreach (var cat in cats) {
                        //data.Items.Add(new DonutChartDataItem() { Name = cat.Category, Title = cat.Category, Value = cat.Count });
                        donut.AddSliceFormatWithThousandsNoDecimal(cat.Count, cat.Category);
                    }
                }

                layout.AddLeft(donut);

            }

            // Closed tickets
            {
                var donut = new DonutNode();
                donut.Theme = "default";
                donut.SetTitle("Closed Tickets");
                donut.SetSubTitle(loader.GetNodeSubtitle("by Categories"));
                donut.LegendLabelsShowValue = true;

                var tickets = loader.DB.SupportTickets().Where(t => t.Status > SupportTicket.TicketStatus.AwaitingReply).AsQueryable(); ;
                tickets = loader.FilterItemsForTimerangeCreated(tickets);
                if (tickets.Any()) {
                    var cats = tickets.GroupBy(t => t.Category).Select(g => new { Category = g.Key, Count = g.Count() });
                    foreach (var cat in cats) {
                        donut.AddSliceFormatWithThousandsNoDecimal(cat.Count, cat.Category);
                    }
                }

                layout.AddRight(donut);

            }

            section.AddChild(layout);

            loader.AddItem(section, "/admin/tickets");
        }

    }
}
