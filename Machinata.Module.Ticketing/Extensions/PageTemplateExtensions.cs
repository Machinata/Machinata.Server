using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Ticketing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Ticketing.Extensions {
    public static class PageTemplateExtensions {
        public static void InsertSupportTicketConversation(this PageTemplate template, string variableName, SupportTicket ticket, IOrderedQueryable<SupportMessage> messages, bool isCustomerView , string apiCallMessageCreation) {

            // Main Template
            var messagesOrdered = messages.OrderBy(m => m.Id);
            var conversationTemplate = template.LoadTemplate("ticket.conversation");
            conversationTemplate.InsertTemplates(
                variableName: "messages",
                entities: messagesOrdered,
                templateName: "ticket.message",
                forEachEntity: delegate (SupportMessage m, PageTemplate t) {
                    var ownMessage = m.CustomerMessage && isCustomerView || isCustomerView == false && m.CustomerMessage == false;
                    var classes = new List<string>();
                    if (ownMessage) classes.Add("is-own-message");
                    else classes.Add("is-response-message");
                    if (m.HasFile) classes.Add("has-file");
                    if (m.HasImage) classes.Add("has-image");
                    if (m.StatusDidChange) classes.Add("status-did-change");
                    if (m == messagesOrdered.LastOrDefault()) classes.Add("is-latest-message");
                    if (m.Message == null || m.Message == "") classes.Add("message-empty");
                    t.InsertVariable("entity.classes", string.Join(" ",classes));
                    t.InsertVariableXMLSafeWithLineBreaks("entity.message-with-breaks", m.Message);
                });

          
            var sendTemplate = template.LoadTemplate("ticket.send");
            sendTemplate.InsertVariable("api-call",apiCallMessageCreation);


            // Statuses
            var statusTemplates = new List<PageTemplate>();
            foreach (var status in Core.Util.EnumHelper.GetEnumValues<SupportTicket.TicketStatus>(typeof(SupportTicket.TicketStatus))) {
                var optionTemplate = template.LoadTemplate("ticket.send.status-option");
                optionTemplate.InsertVariable("option.key", status);
                optionTemplate.InsertVariable("option.selected", status == SupportTicket.TicketStatus.Processed ? "selected='selected'" : string.Empty);
                optionTemplate.InsertVariable("option.value", status.GetEnumTranslationTextVariable());
                statusTemplates.Add(optionTemplate);
            }
            sendTemplate.InsertTemplates("status.options", statusTemplates);

            // Send Teplate
            conversationTemplate.InsertTemplate("reply-send", sendTemplate);

            // Response enabled?
            if (ticket.Status < SupportTicket.TicketStatus.Closed) {
                conversationTemplate.InsertVariableBool("response-enabled", true);
            } else {
                conversationTemplate.InsertVariableBool("response-enabled", false);
            }


            template.InsertTemplate(variableName, conversationTemplate);
        }
    }
}
