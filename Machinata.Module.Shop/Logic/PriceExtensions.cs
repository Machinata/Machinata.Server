using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Shop.Logic {
    public static class PriceExtensions {
        public static Price GetRoundForeignCurrencyPrice(this Price price, string currency) {
            if (Shop.Config.ShopRoundForeignCurrencyPrices) {
                return price.ConvertToRoundedCurrency(currency);
            } else {
                return price.Clone();
            }
        }
    }
}
