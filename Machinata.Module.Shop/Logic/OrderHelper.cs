using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Module.Shop.Logic;
using System;
using System.Linq;
using System.Linq.Expressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace Machinata.Module.Shop.Model {
    public partial class Order  {
        /// <summary>
        /// Gets the last shipment which has at least the status partially shipped
        /// </summary>
        /// <returns></returns>
        public OrderShipment GetShipmentForOrder() {
            return this.Shipments.Where(s => s.Status >= OrderShipment.ShipmentStatus.PartiallyShipped).OrderByDescending(o => o.Id).FirstOrDefault();
        }

        public JObject GetJsonForCart() {
            var jsonForm = new FormBuilder(Forms.Frontend.JSON).Include(nameof(Order.PublicId));
            var orderJson = this.GetJSON(jsonForm);
            var orderItems = this.OrderItems.Select(oi => oi.GetJSON(jsonForm));
            orderJson["items"] = JToken.FromObject(orderItems);
            orderJson["quantity"] = this.OrderItems.Sum(oi => oi.Quantity);
            return orderJson;
        }

       

        public byte[] GetDeliverySlipPdfData(Model.OrderShipment shipment, User user) {
            var data = Core.Handler.PDFPageTemplateHandler.GeneratePDFDateForRoute($"/pdf/shop/order/{this.PublicId}/shipment/{shipment.PublicId}/delivery-slip", user);
            return data;
        }

        /// <summary>
        /// Converts the customer prices of the Order and its items to the given currency.
        /// </summary>
        /// <param name="currency">The currency.</param>
        public bool ConvertCustomerPricesToCurrency(ModelContext db, string currency) {
            // Check status
            //if (!this.IsStatusBefore(StatusType.Placed)) {
            //    throw new BackendException("currency-error", $"Cannot change the currency  to '{currency}'. The order is already placed.");
            //}

            // Nothing to do
            if (this.Currency == currency) {
                return false;
            }

            // Redraft if possible
            var createNewInvoice = false;
            if (this.Status != Order.StatusType.Draft && this.ProcessLogic.AllowRedraft(true)) {
                this.ChangeStatus(Order.StatusType.Draft, this.User, true);
                this.Include(nameof(Order.ShippingAddress));
            }

            // OrderItems, Get the price like when AddOrderItem
            foreach(var orderItem in this.OrderItems) {
                var mergedConfiguration = orderItem.Product.GetConfiguration(orderItem.Configuration);
                orderItem.CustomerPrice = mergedConfiguration.CustomerPrice.GetRoundForeignCurrencyPrice(currency);
            }
  
            // Order
            this.Currency = currency;
            this.UpdateOrder();

            // Invoice
            if (createNewInvoice) {
                this.PlaceOrderForPayment(db, this.User);
            }

            return true;
        }

        public void Redraft() {
            if (this.Status != Order.StatusType.Draft && this.ProcessLogic.AllowRedraft(true)) {
                this.ChangeStatus(Order.StatusType.Draft, this.User, true);
            }
        }

        public void RequireStatusDraft() {
            if (this.Status != Order.StatusType.Draft) {
                throw new BackendException("not-draft", "This action needs the order to be in status 'draft'.");
            }
        }


        public bool HasLowStockItem
        {
            get
            {
                this.Include(nameof(Order.OrderItems));
                this.OrderItems.ToList().ForEach(oi => oi.Include(nameof(OrderItem.Configuration)));
                //return this.OrderItems.Select(oi => oi.Configuration).Any(c => c.IsStockLow);
                return !this.OrderItems.All(oi => oi.Configuration.HasEnoughStockItems(oi.Quantity));
            }
        }

        public static Expression<Func<Order, bool>> HasOrderDate
        {
            get
            {
                if (Shop.Config.ShopOrderRequiresDeliveryDate) {
                    return order => order.DeliveryDate.HasValue;
                } else {
                    return order => order.ConfirmedDate.HasValue;
                }

               
            }
        }

        public static Expression<Func<Order, DateTime?>> OrderDateExpression
        {
            get
            {
                if (Shop.Config.ShopOrderRequiresDeliveryDate) {
                    return order => order.DeliveryDate;
                } else {
                    return order => order.ConfirmedDate;
                }
            }
        }

        public static Expression<Func<Order, bool>> Between(DateTime from, DateTime to)
        {
         
                if (Shop.Config.ShopOrderRequiresDeliveryDate) {
                    return order => order.DeliveryDate >= from && order.DeliveryDate < to;
                } else {
                return order => order.ConfirmedDate >= from && order.ConfirmedDate < to;
            }
          
        }



        /// <summary>
        /// Tracks the sale on the configured analytics service
        /// </summary>
        public void TrackSale() {

            if (!Config.ShopEnableSalesTracking) { return; }

            this.Include(nameof(Order.OrderItems));
            this.Include(nameof(Order.OrderCoupons));
         

            // Tracker
            var tracker = Core.Analytics.AnalyticsTracker.DefaultTrackerForHandler();
            tracker.IP = this.Infos[Order.INFOS_REMOTE_IP_KEY]?.ToString();
            tracker.UserAgent = this.Infos[Order.INFOS_USER_AGENT_KEY]?.ToString();
            tracker.ClientId = this.Infos[Order.INFOS_REMOTE_IP_KEY]?.ToString();
            tracker.UserId = this.User?.PublicId;

            // Order
            tracker.TrackTransaction(
                                      transactionId: this.SerialId,
                                      affiliation: null, //todo
                                      revenue: this.TotalCustomer.Value?.ToString(),
                                      shipping: this.ShippingCustomer.Value?.ToString(),
                                      tax: this.VATCustomer.Value?.ToString(),
                                      coupon: this.OrderCoupon?.Code,
                                      currency: this.Currency
                                      );
            // Items
            foreach (var orderItem in this.OrderItems) {
                orderItem.Include(nameof(OrderItem.Configuration));
                tracker.TrackTransactionItem(
                                    transactionId: this.SerialId,
                                    sku: orderItem.SKU,
                                    name: orderItem.Title,
                                    price: orderItem.CustomerPrice.Value?.ToString(),
                                    quantity: orderItem.Quantity,
                                    variation: orderItem.Configuration.PublicId,
                                    currency: this.Currency);
            }


        }


    }
}
