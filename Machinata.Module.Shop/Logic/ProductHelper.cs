using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Machinata.Module.Shop.Model {
    public partial class Product  {

        public static IList<ProductConfiguration> GetMergedBusinessConfigurationsForProducts(Business business, IEnumerable<Product> products, bool doLoad = true) {

            IList<ProductConfiguration> configurations = new List<ProductConfiguration>();

            foreach (var product in products.ToList()) {
                ProductConfiguration configurationForBusiness = product.GetConfigurationForBusiness(business, doLoad);
                configurations.Add(configurationForBusiness);
            }

            return configurations;
        }
              

        public static Dictionary<string, string> GetProductTemplates() {

            try {
                if (Config.ShopProductPackagingTemplates != null && Config.ShopProductPackagingTemplates.Any() == true) {
                    var templates = Config.ShopProductPackagingTemplates.ToDictionary(k => k.ToLowerInvariant(), v => v);
                    return templates;
                }
                return new Dictionary<string, string>();
            } catch (Exception) {
                throw new Exception("Could not load custom labels from Config");
            }

        }

        public static Tuple<string, string> GetProductTemplateDefaultOrFirst() {

            var templates = GetProductTemplates();
           if (templates.Any(t => t.Key == "default")) {
                return new Tuple<string, string>("default", templates["default"]);
            } else {
                var first = templates.FirstOrDefault();
                if (first.Key != null) {
                    return new Tuple<string, string>(first.Key, first.Value);
                }
            }
            return null;    


        }


    }
}
