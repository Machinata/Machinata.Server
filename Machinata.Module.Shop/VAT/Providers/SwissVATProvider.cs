using Machinata.Core.Model;
using Machinata.Module.Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Shop.VAT.Providers {

    /*
        See http://www.buchhaltungs-forum.ch/t/versandkosten-mwst-bei-versand-ins-ausland/270/2
      
        Im Mehrwertsteuer Gesetzt gibt es den Artikel 19 (Mehrheit von Leistungen)127. Dieser bestimmt im Absatz 4 folgendes:

        Nebenleistungen, namentlich Umschliessungen und Verpackungen, werden steuerlich gleich behandelt wie die Hauptleistung.

        Weil in Deinem Fall die Hauptleistung mit 8% besteuert wird, musst Du auch den Versand und die Verpackung mit 8% verrechnen. Deshalb ist Deine Variante a) korrekt.

        Wenn Du Artikel mit verschiedenen MWST Sätzen verkaufst und alles auf einer Rechnung zusammenfasst, dann wird es etwas komplizierter. Ein Beispiel dazu findest Du in der MWST Info209.

        Und wie verhält es sich wenn wir die Produkte ins Ausland versenden? Müssen wir die MwSt. trotzdem auf der Rechnung ausweisen oder nicht?

        Wenn Du Produkte ins Ausland exportierst, sind diese nicht MWST pflichtig. Das selbe gilt für die Versandkosten. Du solltest auf der Rechnung ausweisen, dass die Artikel exportiert wurden. Das Zielland wird die ausländische MWST bei der Verzollung erheben und Deinem Kunden verrechnen.

        Wichtig ist, dass Du den Nachweis des Exports zusammen mit der Rechnungskopie aufbewahrst.

        For differing VAT rates:
        See https://www.gate.estv.admin.ch/mwst-webpublikationen/public/pages/taxInfos/cipherDisplay.xhtml?publicationId=1002536&componentId=1002651&&winid=994247

        
        2.4.3 Rechnungen mit verschiedenen Steuersätzen und Nebenkosten

        Die dem Kunden in Rechnung gestellten Nebenkosten (z.B. Fracht, Porto, Verpackung oder Kleinmengenzuschlag) gehören zum steuerbaren Entgelt.

 

        Bei Rechnungen mit verschiedenen Steuersätzen lassen sich die Nebenkosten in der Regel nicht eindeutig den einzelnen Gegenständen zuordnen. Zur Vereinfachung stehen daher folgende Aufteilungsvarianten zur Auswahl:

        Proportionale Aufteilung der Nebenkosten anhand der fakturierten Werte;

        Versteuerung der Nebenkosten zum Steuersatz, der in der Rechnung wertmässig überwiegt;

        die Nebenkosten werden durchwegs zum Normalsatz versteuert.

 
     */
    public class SwissVATProvider : VATProvider {

        public const bool AllowZeroVATItems = true;

        public override Price CalculateVATForOrder(Order order) {
            
            // Calculate all the VAT on the items
            var totalVat = new Price(0,order.Currency);
            foreach(var item in order.OrderItems) {
                var itemVatRate = CalculateVATRateForOrderItem(order, item);

                if (Config.ShopVATIncluded) {
                    // Only if we have a rate
                    if (itemVatRate != 0) {
                        totalVat += new Price(item.CustomerPriceSum.Value.Value / ((1.0m / itemVatRate) + 1), order.Currency);
                    }
                } else {
                    totalVat += (item.CustomerPriceSum * itemVatRate);
                }

            }

            // Calculate VAT on the shipping
            // Shipping uses the VAT rate of the order items.
            // If the order items have varying VAT rates, then the shipping must use
            // the weighted value of the subtotal of all the item-vat-rate-groups (unsupported, see https://www.gate.estv.admin.ch/mwst-webpublikationen/public/pages/taxInfos/cipherDisplay.xhtml?publicationId=1002536&componentId=1002651&&winid=994247)
            var itemVATRates = GetOrderItemVATRates(order);
            if (itemVATRates.Count == 0) throw new NotImplementedException("SwissVATProvider could not determine the VAT rate for shipping because no items have a VAT rate.");
            if (itemVATRates.Count > 1 && AllowZeroVATItems == false) throw new NotImplementedException("Orders with varying item VAT rates are not supported by SwissVATProvider.");
            if (itemVATRates.Count > 2 && AllowZeroVATItems == true &&  itemVATRates.Contains(0) == false) throw new NotImplementedException("Orders with varying item VAT rates (except 0) are not supported by SwissVATProvider");


            totalVat += (order.ShippingCustomer * itemVATRates.First());

            return totalVat;
        }

        public override decimal CalculateVATRateForOrderItem(Order order, OrderItem item) {
            // Validate
            if (order.OrderItems == null || order.OrderItems.Count == 0) throw new Exception($"CalculateVATRateForOrderItem cannot get VAT rate because order '{order.SerialId}' has no items.");
            if (order.ShippingAddress == null) throw new Exception($"CalculateVATRateForOrderItem cannot get VAT rate because shipping address is null. order '{order.SerialId}'");
            if (order.ShippingAddress.CountryCode == null) throw new Exception($"CalculateVATRateForOrderItem cannot get VAT rate because shipping address country code is null. '{order.SerialId}'");

            // Destination Switzerland?
            // We only apply VAT to swiss destinations, all other destinations we don't do VAT
            if (order.ShippingAddress.CountryCode.ToLower() != "ch") return 0m;

            // Return the rate
            if (item.CustomerVATRate == null) return 0;
            else return (decimal)item.CustomerVATRate;
        }
    }
}
