using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Module.Shop.Model;
using Machinata.Core.Messaging.Interfaces;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Module.Finance.Model;
using Machinata.Core.Messaging;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Shop.Messaging {
    public class ShopMessageCenter : IMessageSender {

       
        public enum MessageSubCategories {
            StatusChanged
        }

        internal static void OrderStatusChanged(Order order, Order.StatusType newStatus, User user) {
            // Check notification settings of business
            try {
                if (newStatus == Order.StatusType.Confirmed) {
                    SendConfirmedEmail(order);
                } else if (newStatus == Order.StatusType.Fulfilled || newStatus == Order.StatusType.PartialFulfilled) {
                    SendFulfillmentEmail(order);
                } else if (newStatus == Order.StatusType.Refunded ) {
                    SendRefundEmail(order);
                }
            }
            catch (Exception e) {
                Core.EmailLogger.SendMessageToAdminEmail("ShopMessageCenter Error", "Machinata.Module.Shop", null, null, e);
            }
        }

        private static void SendRefundEmail(Order order) {
            var refundEmail = new OrderEmailTemplate(order.Context, order, "order/refund", order.User?.Language);
            refundEmail.Compile();

            // Send Admin
            refundEmail.SendNotificationEmail();

            // Customer email
            SendEmail(order, refundEmail, null); 
        }

        public static void SendFulfillmentEmail(Order order) {
            // Delivery email activated?
            if (!Shop.Config.ShopSendFulfillmentEmail) { return; }

            // Create email or fax or notfication
            order.Include(nameof(Order.User));
            var deliverEmail = new OrderEmailTemplate(order.Context, order, "order/shipment", order.User?.Language);
            deliverEmail.Compile();

            // Send Admin
            deliverEmail.SendNotificationEmail();

            // Send email to customer
            order.Include(nameof(order.BillingAddress));
            order.Include(nameof(order.Business));
            order.Include(nameof(order.Shipments));

            List<EmailAttachment> attachements = new List<EmailAttachment>();

            OrderShipment shipment = order.GetShipmentForOrder();

            if (shipment != null) {
                // Attach to email
                // Get the PDF data
                var attachement = new EmailAttachment();
                attachement.Content = new System.IO.MemoryStream(order.GetDeliverySlipPdfData(shipment, order.Context.Users().SystemUser()));
                attachement.Name = shipment.GetPDFFileName();
                attachements.Add(attachement);
            } else {
                throw new Exception("No shipment found to send as email");
            }
            SendEmail(order, deliverEmail, attachements);
        }

        private static void SendEmail(Order order, OrderEmailTemplate deliverEmail, List<EmailAttachment> attachements) {

            if (string.IsNullOrEmpty(order?.BillingAddress?.Email) == true) {
                throw new BackendException("email-error", $"Cannot send email for order {order?.SerialId}, because there is no Email defined in the Billing Address");
            }

            // Send to Business
            if (order.Business != null) {
                deliverEmail.SendBusinessEmail(
                    business: order.Business,
                    email: order.BillingAddress.Email,
                    unsubsribeType: Core.Model.MailingUnsubscription.Categories.Shop,
                    unsubscribeSubtype: Core.Util.String.CreateShortURLForName(MessageSubCategories.StatusChanged.ToString()),
                    attachements: attachements);
            } else  if (order.User != null){
                deliverEmail.SendUserEmail(
                       user: order.User,
                       email: order.BillingAddress.Email,
                       unsubsribeType: Core.Model.MailingUnsubscription.Categories.Shop,
                       unsubscribeSubtype: Core.Util.String.CreateShortURLForName(MessageSubCategories.StatusChanged.ToString()),
                       attachements: attachements);
            }
            else {
                deliverEmail.SendEmail(
                       email: order.BillingAddress.Email,
                       unsubsribeType: Core.Model.MailingUnsubscription.Categories.Shop,
                       unsubscribeSubtype: Core.Util.String.CreateShortURLForName(MessageSubCategories.StatusChanged.ToString()),
                       attachements: attachements);
            }
        }

        public static void SendConfirmedEmail(Order order) {
           
            // Create email or fax or notfication

            order.Include(nameof(Order.User));
            order.Include(nameof(Order.BillingAddress));
            order.Include(nameof(Order.Business));
            order.Include(nameof(Order.Invoice));

            var orderEmail = new OrderEmailTemplate(order.Context, order, "order/order", order.User?.Language);
            orderEmail.Compile();

            // Send Admin
            orderEmail.SendNotificationEmail();
           
            // Attachemnt
            List<EmailAttachment> attachements = new List<EmailAttachment>();
            if (order.Invoice != null ) {
                // Attach to email
                // Get the PDF data
                var attachement = new EmailAttachment();
                attachement.Content = new System.IO.MemoryStream(Invoice.GetPdfData(order.Invoice, order.Context.Users().SystemUser()));
                attachement.Name = order.Invoice.GetPDFFileName();
                attachements.Add(attachement);
            }

            // Send 
            SendEmail(order, orderEmail, attachements);
        }


        public IEnumerable<Tuple<string, string>> GetSubCategories(MailingUnsubscription.Categories category) {
            var categories = new List<Tuple<string, string>>();
            if (category == MailingUnsubscription.Categories.Shop) {
                foreach (string subcategory in Core.Util.EnumHelper.GetEnumValues<MessageSubCategories>(typeof(MessageSubCategories)).Select(c => c.ToString())) {
                    categories.Add(new Tuple<string, string>(Core.Util.String.CreateShortURLForName(subcategory), "{text.shop.unsubscription-type." + subcategory + "}"));
                }
            }
            return categories;
        }

        public EmailTemplate GetTestTemplate(ModelContext db) {
            var order = db.Orders().OrderByDescending(o=>o.Id).FirstOrDefault(o => o.Status == Order.StatusType.Confirmed);
            if (order != null) {
                return new OrderEmailTemplate(order.Context, order, "order/order");
            }
            return null;
        }
    }
}
