﻿using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Admin.Dashboard;
using Machinata.Module.Finance.Model;
using Machinata.Module.Reporting.Logic;
using Machinata.Module.Reporting.Model;
using Machinata.Module.Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Shop.Dashboard {
    class ShopDashboard {

        [DashboardLoader]
        public static void LoadDashboard(DashboardLoader loader) {

            var section = new SectionNode();
            section.SetTitle("Shop");

            var orders = loader.DB.Orders()
                .Include("ShippingAddress")
                .Include("OrderItems.Product.Categories")
                .Where(o=>o.ConfirmedDate.HasValue);

            // Any orders?
            if (orders.Any() == false) {
                var note = new InfoNode();
                note.SetInfo("No data", "No orders yet", null);
                note.Message = "No orders yet";
                section.AddChild(note);
                loader.AddItem(section, "/admin/shop");
                return;
            }

            IQueryable<OrderItem> orderItems = orders.SelectMany(o => o.OrderItems);

            
            var oldestOrder = orders.Min(i => i.ConfirmedDate).Value;
            var minDate = DateTime.UtcNow.AddMonths(-1);
            if (oldestOrder > minDate) {
                minDate = oldestOrder;
            }

            // Revenue by currency per day
            {
                var currencies = orders.Select(o => o.Currency).Distinct().ToList();
                var node = new LineColumnNode();
                node.RandomID();
                node.SetTitle("Revenue");
                node.SetSubTitle("by Day");
                node.Theme = "default";
                var group = node.AddSerieGroup();
                section.AddChild(node);
                group.XAxisDays();
                group.YAxisNumberThousands();

                var ordersList = orders.Where(o => o.ConfirmedDate >= minDate);

                foreach (var currency in currencies) {
                    var entities = ordersList.Where(o => o.Currency == currency);
                    var serie = Reporting.Logic.CostsOverTimeReports.CreateDailyCostsSerie(entities.ToList().AsQueryable(), currency, c => c.ConfirmedDate, c => c.TotalCustomer?.Value, false);
                    group.AddColumnSerie(serie);

                }

            }

            // Deliveries
            {

                var node = new OnlineMapNode();
                node.RandomID();
                node.SetTitle("Deliveries");

                node.Theme = "default";

                section.AddChild(node);

                var mapOrders = orders
                    .Where(o => o.Status >= Order.StatusType.Confirmed)
                    .Where(o => o.ShippingAddress != null)
                    ;
                AddOrdersToMap(loader.Language, node, mapOrders);

                node.Facts.Add(new GeoFact() { Address = "Räffelstrasse, Zürich", Link = new Link("https://nerves.ch", "Nerves GmnbH") });

            }


            // Orders per day
            {

                var node = new LineColumnNode();
                node.RandomID();
                node.SetTitle("Orders");
                node.SetSubTitle("per Day");
                node.Theme = "default";
                var group = node.AddSerieGroup();
                section.AddChild(node);
                group.XAxisDays();
                group.YAxisNumberThousands();

                var entities = orders.Where(o => o.ConfirmedDate >= minDate);

                var serie = Reporting.Logic.CostsOverTimeReports.CreateDailyCostsSerie(entities.ToList().AsQueryable(), "Orders", c => c.ConfirmedDate, c =>1m, false);
                group.AddColumnSerie(serie);


            }



            // top seller products
            {
                DonutNode node = TopSellersDonutChart(orderItems);
                section.AddChild(node);


            }

            // Categories
            {
                DonutNode node = CategoriesDonutChart(loader.DB, orderItems);
                section.AddChild(node);


            }







            loader.AddItem(section, "/admin/shop");
        }

        public static DonutNode CategoriesDonutChart(ModelContext db, IQueryable<OrderItem> orderItems, Category.CategoryTypes categoryType = Category.CategoryTypes.Main) {
            var node = new DonutNode();
            node.RandomID();
            node.SetTitle("Categories");
            node.SetSubTitle(categoryType + " Categories");
            node.Theme = "default";


            foreach (var cat in db.Categories().Where(c => c.CategoryType == categoryType).ToList()) {
                var count = orderItems.Count(oi => oi.Product.Categories.Select(c => c.Id).Contains(cat.Id));
                node.AddSliceFormatWithThousandsNoDecimal(count, cat.Name);
            }

            return node;
        }

        public static DonutNode TopSellersDonutChart(IQueryable<OrderItem> orderItems) {
            var node = new DonutNode();
            node.RandomID();
            node.SetTitle("Products");
            node.SetSubTitle("Top 10 Sellers");
            node.Theme = "default";


            var products = orderItems.GroupBy(oi => oi.Product).Select(g => new { Name = g.Key.Name, Count = g.Count() });
            products = products.OrderByDescending(g => g.Count).Take(10);
            foreach (var p in products) {
                node.AddSliceFormatWithThousandsNoDecimal(p.Count, p.Name);

            }

            return node;
        }

        public static void AddOrdersToMap(string language, OnlineMapNode node, IQueryable<Order> mapOrders) {
            foreach (var order in mapOrders) {

                var address = order.ShippingAddress.FormatAsString(",", "Company,Name,Address2");

                var template = new PageTemplate(address);
                template.Language = language;
                template.InsertTextVariables();
                address = template.Content.ToString();

                node.AddGeoFact(address, order.SerialId, "/admin/shop/orders/order/" + order.PublicId);
            }
        }

        public static LineColumnNode RevenueLastMonths(IQueryable<Invoice> invoices, int? months = 12, decimal? burnRate = null) {


            if (months != null) {
                var minDate = DateTime.UtcNow.AddMonths(-months.Value);
                invoices = invoices.Where(i => i.InvoiceSent > minDate);
            }

           
            var node = CostsOverTimeReports.GetMonthlyCosts(invoices, "Revenue", i => i.InvoiceSent, c => c.TotalCustomer?.Value, accumulate: false, unit: Core.Config.CurrencyDefault);

            // External Costs
            {
                var externalCostsSerie = CostsOverTimeReports.CreateMonthlyCostsSerie(invoices.Where(i => i.ExternalCosts.HasValue), "External Costs", i => i.InvoiceSent, c => c.ExternalCosts?.Value, accumulate: false);
                //node.Series.Add(externalCostsSerie);

                node.SeriesGroups.First().AddColumnSerie(externalCostsSerie);
            }

            // Burn rate
            if (burnRate != null && invoices.Any(i=>i.InvoiceSent.HasValue)) {
                var minDate = invoices.Min(i => i.InvoiceSent).Value.StartOfMonth();
                var maxDate = invoices.Max(i => i.InvoiceSent).Value.EndOfMonth();
                var burnRateLine = CostsOverTimeReports.CreateBurnRateLine("Burn Rate", minDate, maxDate, new Price(burnRate));
                node.SeriesGroups.First().Series.Add(burnRateLine);
            }




            return node;
        }
    }
}
