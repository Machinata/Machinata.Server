using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Module.Finance.Model;
using Machinata.Core.Util;
using Machinata.Module.Shop.Logic;
using static Machinata.Module.Finance.Model.Invoice;
using Machinata.Module.Finance.Payment;
using Machinata.Module.Shop.VAT;
using System.Data.Entity.Infrastructure;
using System.Web;

namespace Machinata.Module.Shop.Model {

    [Serializable()]
    [ModelClass]
    public partial class Order : ModelObject, Module.Finance.Invoices.IInvoiceable {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constants /////////////////////////////////////////////////////////////////////////

        public const string INFOS_REMOTE_IP_KEY = "RemoteIP";
        public const string INFOS_USER_AGENT_KEY = "UserAgent";
        public const string INFOS_USER_LANGUAGES_KEY = "UserLanguages";


        public enum StatusType : short {
            Created = 0,
            Draft = 10,                 // Cart
            Placed = 20,                // Placed with print providers
            Paid = 30,                  // Payed by payment provider
            Confirmed = 40,             // Confirmed with print providers
            PartialFulfilled = 50,      // Partially shipped
            Fulfilled = 60,             // Shipped


            Error = 400,
            OnHold = 410,
            Failed = 420,
            Canceled = 430,
            Refunded = 440
        }


        #endregion


        #region Events

        // Order Updated
        public delegate void PreOrderUpdateDelegate(Order order);
        public static event PreOrderUpdateDelegate PreOrderEvent;

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Order() {
            this.OrderItems = new List<OrderItem>();
            this.OrderHistories = new List<OrderHistory>();
            this.OrderCoupons = new List<OrderCoupon>();
            this.Shipments = new List<OrderShipment>();
            this.Infos = new Properties();

            // default currency for internal prices
            this.ShippingInternal = new Price(0);
            this.SubtotalInternal = new Price(0);
            this.TotalInternal = new Price(0);
        }

        // Subscribe to Invoice Status changes
        static Order() {
            Invoice.StatusChanged += new Invoice.StatusChangedDelegate(HandleInvoiceStatusChange);
            Invoice.InvoiceDeleted += new Invoice.InvoiceDeletedDelegate(HandleInvoiceDeleted);
        }
        

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public string SerialId { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public StatusType Status { get; set; }

        [FormBuilder]
        [Column]
        [FormBuilder(Forms.Frontend.JSON)]
        [Index(IsUnique = true)]
        [MaxLength(64)]
        public string Hash { get; set; }


        /// <summary>
        /// When the order is to be delivered
        /// </summary>
        /// <value>
        /// The delivery date.
        /// </value>
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.CREATE)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public DateTime? DeliveryDate { get; set; }

        /// <summary>
        /// When the order was confirmed.
        /// </summary>
        /// <value>
        /// The confirmed date.
        /// </value>
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        public DateTime? ConfirmedDate { get; set; }

        [NotMapped]
        public DateTime? OrderDate {
            get {
                if (Shop.Config.ShopOrderRequiresDeliveryDate) {
                    return this.DeliveryDate;
                } else {
                    return this.ConfirmedDate;
                }
            }
            set
            {
                if (Shop.Config.ShopOrderRequiresDeliveryDate) {
                    this.DeliveryDate = value;
                } else {
                    this.ConfirmedDate = value;
                }
            }
        }

        /// <summary>
        /// When the order was shipped.
        /// </summary>
        /// <value>
        /// The shipped date.
        /// </value>
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        public DateTime? ShippedDate { get; set; }
        
        /// <summary>
        /// Total customer has to pay (including vat, shipment, paymentfee, discount)
        /// </summary>
        /// <value>
        /// The total customer.
        /// </value>
        [FormBuilder]
        [Column]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.API.VIEW)]
        public Price TotalCustomer { get; set; }

        /// <summary>
        /// customer has to pay for shipment
        /// </summary>
        /// <value>
        /// The total customer.
        /// </value>
        [FormBuilder]
        [Column]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.API.VIEW)]
        public Price ShippingCustomer { get; set; }

        [FormBuilder]
        [Column]
        public Price TotalInternal { get; set; }

        [FormBuilder]
        [Column]
        public Price ShippingInternal { get; set; }

        [FormBuilder]
        [Column]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public Price SubtotalCustomer { get; set; }

        [FormBuilder]
        [Column]
        public Price SubtotalInternal { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public Price Discount { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public Price VATCustomer { get; set; }

        [FormBuilder]
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        public Finance.Payment.PaymentTime PaymentPointOfTime { get; set; }

        [FormBuilder]
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        public Finance.Payment.PaymentMethod PaymentMethod { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.API.VIEW)]
        [MaxLength(3)]
        public string Currency { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
      //  [FormBuilder(Forms.Admin.EDIT)]
        public Properties Infos { get; set; }

        #endregion

        #region Public Navigation Properties /////////////////////////////////////////////////////

        [Column]
        public User User { get; set; }

        [Column]
        public Address OriginAddress { get; set; }

        [Column]
        public Address ShippingAddress { get; set; }

        [Column]
        public Address BillingAddress { get; set; }

        [Column]
        public ICollection<OrderItem> OrderItems { get; set; }

        [Column]
        public ICollection<OrderShipment> Shipments { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public Business Business { get; set; }

        [Column]
        public ICollection<Model.OrderHistory> OrderHistories { get; set; }

        [Column]
        public virtual Invoice Invoice { get; set; }

        [Column]
        public ICollection<Model.OrderCoupon> OrderCoupons { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [NotMapped]
        public OrderProcessLogic ProcessLogic
        {
            get {
                return OrderProcessLogic.CreateOrderProcessLogic(this, this.PaymentPointOfTime);
            }
        }

        [NotMapped]
        public OrderCoupon OrderCoupon
        {
            get
            {
                return this.OrderCoupons.SingleOrDefault();
            }
        }

        [NotMapped]
        [FormBuilder(Forms.Frontend.JSON)]
        public Price TotalCustomerExclVat
        {
            get
            {
                var total = this.TotalCustomer.Value - this.VATCustomer.Value ;
                if (total > 0) {
                    return new Price(total, this.Currency);
                }
                return new Price(0, this.Currency);
            }
        }

        [NotMapped]
        [FormBuilder(Forms.Frontend.JSON)]
        public string CustomerVATKnown
        {
            get
            {
                //string: otherwise it will be transleted to "Nein" etc // TODO WHY?
                return this.CanCalculateVAT().ToString().ToLower();
            }
        }
        [NotMapped]
        [FormBuilder(Forms.Frontend.JSON)]
        public string HasCustomerVAT
        {
            get
            {
                //otherwise it will be transleted to "Nein" etc // TODO WHY?
                return (this.VATCustomer != null && this.VATCustomer.HasValue && this.VATCustomer.Value > 0).ToString().ToLower();
            }
        }



        //[FormBuilder(Forms.API.VIEW)]
        //public string StatusName
        //{
        //    get {
        //        return this.Status.ToString().ToDashedLower(false);
        //    }

        //}

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        [OnModelCreating]
        private static void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder) {
            // One to one relation to Payment
            //  https://stackoverflow.com/questions/39965350/a-dependent-property-in-a-referentialconstraint-is-mapped-to-a-store-generated-c
            //  modelBuilder.Entity<Order>()
            //.HasOptional(t => t.Payment)
            //.WithOptionalPrincipal(t => t.Order);

        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public string GetPDFFileName() {
            return $"{Core.Config.ProjectID}_Order_{this.SerialId}.pdf";
        }

        public override void OnLoad(ModelContext db) {
            base.OnLoad(db);
        }

        public void InitializeBusinessOrder(ModelContext db, HttpContext context, string currency, User user) {

            var paymentPointOfTime = Finance.Payment.PaymentTime.After;
            var paymentMethod = Finance.Payment.PaymentMethod.Invoice;

            // Apply Business settings to Order
            if (this.Business.Settings.Keys.Contains(Business.SETTING_KEY_PAYMENT) && Business.Settings[Business.SETTING_KEY_PAYMENT].ToString() == Business.SETTING_VALUE_PAYMENT_BEFORE) {
                paymentPointOfTime = Finance.Payment.PaymentTime.Before;
                paymentMethod = Finance.Payment.PaymentMethod.CreditCard;
            } 
            
            // Set default addresses
            this.ShippingAddress = Business.GetShipmentAddressForBusiness();
            this.BillingAddress = Business.GetBillingAddressForBusiness();

            // Initialize common code
            this.InitializeOrder(db, context, currency, user, paymentPointOfTime, paymentMethod);
        }

        public void InitializeOrder(ModelContext db, HttpContext context, string currency, User user, PaymentTime paymentTime = PaymentTime.Before, PaymentMethod paymentMehod = PaymentMethod.CreditCard) {
            var originAddress = Order.GetDefaultOriginAddressForOrders(db);

            var ip = Core.Util.HTTP.GetRemoteIP(context);
            var userAgent = Core.Util.HTTP.GetUserAgent(context);
            var languages = Core.Util.HTTP.GetUserLanguages(context);
            var languagesConcatenated = languages != null ? string.Join(",",languages ) : null;

            this.InitializeOrder(user, currency, originAddress, ip, userAgent, languagesConcatenated, paymentTime, paymentMehod);


        }

        public void InitializeOrder(User user, string currency, Address originAddress, string ipAddress, string userAgent, string languages, PaymentTime paymentTime , PaymentMethod paymentMehod) {
            // Type
            this.PaymentPointOfTime = paymentTime;
            this.PaymentMethod = paymentMehod;

            // Set a new hash
            this.Hash = GetHash();

            // Sender Address
            this.OriginAddress = originAddress;

            // Register creator user
            this.User = user;

            // Currency
            this.Currency = currency;
            this.TotalCustomer = new Price(0, this.Currency);
            this.SubtotalCustomer = new Price(0, this.Currency);
            this.Discount = new Price(0, this.Currency);
            this.VATCustomer = new Price(0, this.Currency);
            this.ShippingCustomer = new Price(0, this.Currency);

            // HTTP
            this.Infos[Order.INFOS_REMOTE_IP_KEY] = ipAddress;
            this.Infos[Order.INFOS_USER_AGENT_KEY] = userAgent;
            this.Infos[Order.INFOS_USER_LANGUAGES_KEY] = languages;

            // Set first status
            this.ChangeStatus(StatusType.Draft, user);
        }

        private string GetHash() {
           return Core.Encryption.DefaultHasher.HashString(Created.Ticks + "_" + Guid.NewGuid().ToString());
        }

      


        /// <summary>
        /// Gets the default origin address for orders.
        /// Currently this just retuns the owner business shipping address, but probably needs
        /// to be extended for allowing a business owner to set the origin.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <returns></returns>
        public static Address GetDefaultOriginAddressForOrders(ModelContext db) {
            // Get the owner
            var owner = db.OwnerBusiness(false);
            if (owner == null) return null;
            // Get the origin address
            if (owner.Context == null) {
                string x = "";
            }
            owner.Include("DefaultShipmentAddress");
            return owner.DefaultShipmentAddress;
        }

        /// <summary>
        /// Archives/Copies the Shipping and Billing Address of the order
        /// Uses archived version with the same hash if one exists to reduce number of copies
        /// </summary>
        public void ArchiveAddressesForBusiness() {
            if (Context != null) {
                this.Include(nameof(this.Business));
            }
            
            this.Business.Include(nameof(this.Business.Addresses));

            var shippingArchived = GetArchivedAddressByHash(ShippingAddress.Hash);
            if (shippingArchived == null) {
                // Archive address and relink to order
                this.ShippingAddress = Address.CreateArchived(ShippingAddress);
                this.Business.Addresses.Add(ShippingAddress);
            } else {
                this.ShippingAddress = shippingArchived;
            }

            var billingArchived = GetArchivedAddressByHash(BillingAddress.Hash);
            if (billingArchived == null) {
                // Archive address and relink to order
                this.BillingAddress = Address.CreateArchived(BillingAddress);
                this.Business.Addresses.Add(BillingAddress);
            } else {
                this.BillingAddress = billingArchived;
            }
        }

        private Address GetArchivedAddressByHash(string hash) {
            // Check if same address has already been archived
            return Business.Addresses.FirstOrDefault(a => a.Archived && hash == a.Hash);
        }


        public void AddHistory(string description, User user) {
            var history = new OrderHistory();
            history.Description = description;
            history.Created = DateTime.UtcNow;
            history.User = user?.Username;
            history.Status = this.Status;
            this.OrderHistories.Add(history);
        }

        /// <summary>
        /// Adds the order item.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="product">The product.</param>
        /// <param name="quantity">The quantity. (delta)</param>
        public OrderItem AddOrderItem(ModelContext db, Product product, ProductConfiguration configuration, string currency, int quantity,string title, bool ignoreStatus = false) {

            // Init           
            OrderItem orderItem = null;
            if (this.Context != null) {
                this.Include(nameof(Order.OrderItems));
                this.OrderItems.Where(oi =>oi.Context != null ).ToList().ForEach(oi => oi.Include(nameof(OrderItem.Configuration)));
            }

            // Find existing item           
            OrderItem existingOrderItem = this.OrderItems.FirstOrDefault(oi => oi.Configuration == configuration);
            if (existingOrderItem != null) {
                existingOrderItem.Quantity += quantity;
                orderItem = existingOrderItem;
            } else {
                orderItem = OrderItem.CreateOrderItem(db, product, configuration, currency, quantity, title);
                this.OrderItems.Add(orderItem);
            }

            this.CheckQuantity(orderItem,orderItem.Quantity);
            this.UpdateOrder(ignoreStatus);

            return orderItem;
        }

        public void UpdateOrderItemQuantity(OrderItem item, int newQuantity) {
            // Only if quantity different
            if (item.Quantity != newQuantity) {

                // Check
                this.CheckQuantity(item,newQuantity);

                // Set Quantity
                item.Quantity = newQuantity;
                this.UpdateOrder();

            }
        }

        private void CheckQuantity(OrderItem orderItem, int newQuantity) {
            // Check Quantity
            if (newQuantity <= 0) {
                throw new BackendException("quantity-error", $"Order Item {orderItem} cannot have a quantity below 1");
            }
        }

        public void RemoveOrderItem(string orderItemId) {
            // Find, remove item
            var orderItem = this.OrderItems.AsQueryable().GetByPublicId(orderItemId);
            this.OrderItems.Remove(orderItem);
            this.UpdateOrder();
        }


        public OrderItem GetOrderItem(ProductConfiguration configuration) {
            return this.OrderItems.SingleOrDefault(oi => oi.Configuration == configuration && oi.Rejection == false);
        }


        /// <summary>
        /// Updates the quantity or add.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="configuration">The configuration. must be a entity from db. not a merged!</param>
        /// <param name="currency">The currency.</param>
        /// <param name="quantity">The quantity.</param>
        /// <returns></returns>
        public OrderItem UpdateQuantityOrAdd(ModelContext db, ProductConfiguration configuration,string currency,  int quantity, bool ignoreStatus = false) {
            OrderItem orderItem = null;
            var existingOrderItem = this.GetOrderItem(configuration);
            if (existingOrderItem != null) {
                // Change
                if (quantity > 0) {
                    existingOrderItem.Quantity = quantity;
                    orderItem = existingOrderItem;
                } else {
                    // Remove
                    db.OrderItems().Remove(existingOrderItem);
                    orderItem = null;
                }
            } else {
                if (quantity > 0) {
                    string title = GetTitle(configuration);
                    orderItem = this.AddOrderItem(db, configuration.Product, configuration, currency, quantity, title, ignoreStatus);
                } else {
                    orderItem = null;
                }
            }

            // Check
            if (orderItem != null) {
                this.CheckQuantity(orderItem, orderItem.Quantity);
            }

            // Order
            this.UpdateOrder(ignoreStatus);

            return orderItem;
        }
        /// <summary>
        /// Gets the title from either the default or the overload  configuration
        /// </summary>
        /// <param name="configuration">The 'not merged' configuration from the db</param>
        /// <returns></returns>
        private static string GetTitle(ProductConfiguration configuration) {
            configuration.Product.Include(nameof(Product.Configurations));
            var merged = configuration.Product.GetConfiguration(configuration);

            // Use Product Name or merged Configuration as Order Item title?
            if (Config.ShopUseMergedConfigurationTitleForOrderItem == false) {
                var title = $"{merged.SKU} {configuration.Product.Name}";
                return title;
            } else {
                var title = $"{merged.SKU} {configuration.Title}";
                return title;
            }
        }

        public OrderItem AddRejectedItem(ModelContext db, User user, OrderItem orderItem, int quantity) {
            orderItem.LoadFirstLevelNavigationReferences();
            orderItem.LoadFirstLevelObjectReferences();

            // sum of already rejected items 
            orderItem.Order.Include(nameof(orderItem.Order.OrderItems));
            var orderItems = orderItem.Order.OrderItems.ToList();
            orderItems.ForEach(oi => oi.Include(nameof(oi.Product)));
            var otherRejectionsSameProduct = orderItems.Where(oi => oi.Product.Id == orderItem.Product.Id && oi.Rejection == true);
            var sumRejections = otherRejectionsSameProduct.Sum(oi => oi.Quantity);

            // Check status
            if (Invoice != null) {
                throw new BackendException("invoice-exists", "The order already has an invoice and can't be updated.");
            }

            // Check quantity
            if (quantity == 0) {
                throw new BackendException("no-number-provided", "There was no valid number provided");
            }

            if (orderItem.Quantity < quantity + sumRejections) {
                throw new BackendException("too-many-rejections", $"The maximum of new rejections is {orderItem.Quantity - sumRejections}");
            }

            // Create new order item 
            var rejectedOrderItem = new OrderItem();
            rejectedOrderItem.Quantity = quantity;
            rejectedOrderItem.Title = "{text.rejected=Rejected}: " + orderItem.Title;
            rejectedOrderItem.InternalPrice = new Price(0, orderItem.InternalPrice.Currency);
            rejectedOrderItem.CustomerPrice = new Price(-orderItem.CustomerPrice.Value, orderItem.CustomerPrice.Currency);
            rejectedOrderItem.RetailPrice = new Price(0, orderItem.RetailPrice.Currency);
            rejectedOrderItem.Rejection = true;
            rejectedOrderItem.CustomerVATRate = orderItem.CustomerVATRate;
            rejectedOrderItem.Product = orderItem.Product;
            rejectedOrderItem.Infos = orderItem.Infos;

            this.OrderItems.Add(rejectedOrderItem);

            //Update order totals
            this.UpdateOrder();

            // History
            AddHistory("{text.order-history-add-rejected}: " + rejectedOrderItem.ToString(), user);

            return rejectedOrderItem;
        }

        public void ValidateDeliveryTime(DateTime? previousDeliveryTime) {
            if (previousDeliveryTime.HasValue) {

                ValidateDeliveryTime(this.DeliveryDate.Value, previousDeliveryTime, this.OrderItems);
            }
          
        }

        public static bool ValidateDeliveryTime(DateTime deliveryDate, DateTime? previousDeliveryTime, IEnumerable<OrderItem> orderItems, bool throwException = true, bool forceFuture = true) {
            // Make sure we don't change our delivery day if we already have items
            if (orderItems != null && orderItems.Count() > 0 && previousDeliveryTime.HasValue) {
                if (Core.Util.Time.ConvertToDefaultTimezone(previousDeliveryTime.Value).Year != Core.Util.Time.ConvertToDefaultTimezone(deliveryDate).Year
                    || Core.Util.Time.ConvertToDefaultTimezone(previousDeliveryTime.Value).DayOfYear != Core.Util.Time.ConvertToDefaultTimezone(deliveryDate).DayOfYear) {
                    if (throwException) {
                        throw new BackendException("invalid-delivery-time", "Sorry, the delivery time of this order cannot be changed to a different day.");
                    }
                    return false;
                }
            }

            // Make sure it is not in the past...
            if (forceFuture == true && deliveryDate < DateTime.UtcNow) throw new BackendException("invalid-delivery-time", "Sorry, the delivery time cannot be in the past.");


            // Make sure the delivery time is complaint with the proper window
            if (!string.IsNullOrEmpty(Module.Shop.Config.ShopOrderDeliveryTimeWindow)) {
                if (!Core.Util.Time.IsBetweenTimeRange(Core.Util.Time.ConvertToDefaultTimezone(deliveryDate), Module.Shop.Config.ShopOrderDeliveryTimeWindow)) {
                    if (throwException) {
                        throw new BackendException("invalid-delivery-time", $"Sorry, the delivery time of this order must be between {Module.Shop.Config.ShopOrderDeliveryTimeWindow}.");
                    }
                    return false;
                }
            }

            // Make sure the delivery time is complaint with the proper weekday
            if (!string.IsNullOrEmpty(Module.Shop.Config.ShopOrderDeliveryTimeValidDays)) {
                if (!Core.Util.Time.IsOnDayOfWeek(Core.Util.Time.ConvertToDefaultTimezone(deliveryDate), Module.Shop.Config.ShopOrderDeliveryTimeValidDays)) {
                    if (throwException) {
                        throw new BackendException("invalid-delivery-time", $"Sorry, the delivery time of this order must be on one of the following days: {Module.Shop.Config.ShopOrderDeliveryTimeValidDays}.");
                    }
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Validates the order.
        /// </summary>
        public bool ValidateOrder(bool throwException = false, IList<OrderError> errors = null) {

            if (errors == null) { errors = new List<OrderError>(); }   

            var valid =

                    // Items
                    this.ValidateOrderItems(throwException, errors) &&
                    // Coupons
                    this.ValidateCoupons(throwException, errors) &&
                    // Restrictions
                    this.ValidateRestrictions(this.Context, throwException, errors);

            return valid;

        }

        private bool ValidateOrderItems(bool throwException = false, IList<OrderError> errors = null) {
            if (errors == null) { errors = new List<OrderError>(); }
            //Check product minimums
            foreach (var orderItem in OrderItems) {
                if (orderItem.Context != null) {
                    orderItem.Include(nameof(orderItem.Configuration));
                }
                var orderMinimum = orderItem.Configuration?.MinimumOrderAmount;
                if (orderMinimum.HasValue && orderItem.Quantity < orderMinimum.Value) {

                    var code = "product-order-minimum";
                    var message = $"The order minimum for product '{orderItem.Product.Name}' ist at least {orderMinimum.Value}";
                    errors.Add(new OrderError() { Code = code, Message = message });

                    if (throwException) {
                        throw new BackendException(code, message);
                    }
                    return false;
                }
            }

            // Quantity  
            // workaround for linq overload sum
            int count = 0;
            foreach (var item in this.OrderItems) {
                count += item.Quantity;
            }
            if (count <= 0) {
                var code = "too-few-items";
                var message = "Please select at least one time";

                errors.Add(new OrderError () { Code = code, Message = message });

                if (throwException) {
                    throw new BackendException(code, message);
                }
                return false;
            }

            return true;

        }


        private bool ValidateCoupons(bool throwException = false, IList<OrderError> errors = null) {

            if (errors == null) { errors = new List<OrderError>(); }
            if (this.Context != null) {
                this.Include(nameof(Order.OrderCoupons));
            }
           
            foreach (var coupon in this.OrderCoupons) {
                var valid = coupon.ValidateForOrder(this.Context, this, throwException, errors);
                if (!valid) {
                    return false;
                }
            }

            return true;
        }

        public bool ValidateRestrictions(ModelContext db, bool throwException = true, IList<OrderError> errors = null) {
            if (errors == null) { errors = new List<OrderError>(); }

            var retrictions  = OrderRestriction.ValidateRestrictions(db, this.DeliveryDate, this.Business, this);
            if (retrictions.Any()) {

                foreach (var restriction in retrictions) {
                    errors.Add(new OrderError() { Code = "restriction-" + restriction.RestrictionType.ToString().ToDashedLower(), Message = Core.Localization.Text.InsertVariables( restriction.ToString() )});
                }

                if (throwException) {
                    throw new BackendException("order-restricted", $"Order is being restriced: {string.Join(",", retrictions.Select(e => e.Name))}");
                }
                else {
                    return false;
                }
            }
            return true;
        }

        public StatusType? GetStatusByName(string status) {
            return StatusFlow.FirstOrDefault(sf => sf.ToString().ToLower() == status.ToLower());
        }

      
        public void UpdateOrder(bool ignoreStatus = false) {

            if (this.Context != null) {
                this.LoadFirstLevelNavigationReferences();
                this.LoadFirstLevelObjectReferences();
            }

            // Inform subscribers
            PreOrderEvent?.Invoke(this);

            // Require Draft
            if (ignoreStatus == false) {
                this.RequireStatusDraft();
            }

            // Items totals
            this.CalculateSubtotals();

            // Shipping
            this.ShippingInternal = ShipmentCenter.CalculateInternalShippingCosts(this);
            this.ShippingCustomer = ShipmentCenter.CalculateCustomerShippingCosts(this);

            // Payment fees --> todo move to payment helper method
            //long paymentCents = 0;
            //foreach(var payment in Invoice.Payments) {
            //    paymentCents+= payment.ProviderTransactionFee.Cents.Value;
            //}

            // VAT
            if (CanCalculateVAT()) {
                this.VATCustomer = VATProvider.GetVATProvier().CalculateVATForOrder(this);
            } else {
                this.VATCustomer = new Price(0, this.SubtotalCustomer.Currency);
            }

            // Calc and register the discount
            this.Discount = CalculateDiscount();

            // Totals
            this.TotalInternal = this.SubtotalInternal + this.ShippingInternal;

            this.TotalCustomer = this.SubtotalCustomer + this.ShippingCustomer + this.Discount;
            if (Config.ShopVATIncluded == false) {
                this.TotalCustomer = this.TotalCustomer + this.VATCustomer;
            }


        }

        public void CreateShipment(ModelContext db, string trackingLink, User user) {
            this.LoadFirstLevelNavigationReferences();
            if (this.Shipments.Any()) {
                throw new BackendException("too-many-shipments", "There is currently only one shipment per order supported");
            }

            var sender = Order.GetDefaultOriginAddressForOrders(db);

            var shipment = new OrderShipment() {
                Status = OrderShipment.ShipmentStatus.Shipped,
                Shipped = DateTime.UtcNow
            };
            shipment.TrackingLink = trackingLink;
            this.Shipments.Add(shipment);
            foreach (var orderItem in this.OrderItems) {
                shipment.OrderItems.Add(orderItem);
            }
            db.OrderShipments().Add(shipment);

            // History
            this.AddHistory("{text.order-history-shipment-added}: Tracking Link: " + shipment.TrackingLink, user);

            // Save (otherwise the order wont have a shipment for the email)
            db.SaveChanges();

            // Set Order status to fulfilled if in correct status
            if (this.GetNextStatus().HasValue && this.GetNextStatus() == Order.StatusType.Fulfilled) {
                this.ChangeStatus(Order.StatusType.Fulfilled, user);
            }
        }

        /// <summary>
        /// Applies a coupon by its code
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="couponCode">The coupon code.</param>
        /// <exception cref="BackendException">coupon-not-found</exception>
        public bool ApplyCoupon(ModelContext db, string couponCode, bool throwException = true, bool ignoreStatus = false) {
            OrderCoupon coupon = OrderCoupon.GetCoupon(db, couponCode);
            if (coupon == null) {
                if (!throwException) { return false; }
                throw new BackendException("coupon-not-found", $"The coupon code '{couponCode}' does not exist");
            }
            var valid = this.ApplyCoupon(db, coupon, throwException, ignoreStatus);
            this.AddHistory("{text.order-history-coupon-applied}: " + coupon.ToString(), User);
            return valid;
        }


        public void RemoveCoupon() {
            var coupon = this.OrderCoupons.SingleOrDefault();
            if (coupon == null) {
                throw new BackendException("coupon-not-found", $"The order hasn't a coupon applied");
            }
            this.RemoveCoupon(coupon);
            this.AddHistory("{text.order-history-coupon-removed}: " + coupon.ToString(), User);
        }

        /// <summary>
        /// Deletes the order, its items and its statuses
        /// </summary>
        public void Delete(ModelContext db) {
            this.Include(nameof(this.OrderItems));
            this.Include(nameof(this.OrderHistories));
            // Delete Order Items
            foreach (var orderItem in this.OrderItems.ToList()) {
                db.OrderItems().Remove(orderItem);
            }

            // Delete Order Items
            foreach (var orderStatus in this.OrderHistories.ToList()) {
                db.OrderHistories().Remove(orderStatus);
            }

            db.Orders().Remove(this);
        }

       

        private Price CalculateDiscount() {
            Price discount = new Price(0, this.Currency); // This should add up to negative if we want to give a discount
            if (this.OrderCoupons != null && this.OrderCoupons.Count() > 0) {
                // Currently we only support a single coupon, this might change though
                foreach (OrderCoupon coupon in this.OrderCoupons) {
                    discount += coupon.CalculateNegativeDiscountForOrder(this);
                }
            }

            // Clip discount
            if (discount.HasValue) {
                // Make sure we don't go larger than the subtotal
                Price minCosts = this.ShippingCustomer + this.SubtotalCustomer + this.VATCustomer;
                discount = new Price( -System.Math.Min(-discount.Value.Value, minCosts.Value.Value) , discount.Currency);
            }

            return discount;
        }

        private void CalculateSubtotals() {
            Price internalSubtotal = new Price(0);
            Price customerSubtotal = new Price(0, this.Currency);

            foreach (var orderItem in this.OrderItems) {
                internalSubtotal += orderItem.Quantity * orderItem.InternalPrice;
                customerSubtotal += orderItem.Quantity * orderItem.CustomerPrice;
            }

            this.SubtotalInternal = internalSubtotal.Clone();
            this.SubtotalCustomer = customerSubtotal.Clone();
        }

        public OrderHistory ChangeStatus(Order.StatusType newStatus, User user, bool silent = false, bool validate = false) {
            var orderHistory = new OrderHistory();
            var oldStatus = this.Status;

            ProcessLogic.Process(newStatus, validate, user);

            // Set new status
            orderHistory.Status = newStatus;
            orderHistory.User = user?.Username;
            orderHistory.Description = "{text.order-history-status-changed}";

            // Order history
            this.Status = newStatus;
            this.OrderHistories.Add(orderHistory);

            // Tell subscribers
            if (!silent) {
                Shop.Messaging.ShopMessageCenter.OrderStatusChanged(this, newStatus, user);
            }
                    
            return orderHistory;
        }

        public StatusType? GetNextStatus( bool reverse = false) {
            var current = StatusFlow.Find(this.Status);
            if (reverse == false) {
                return current?.Next?.Value;
            }
            return current?.Previous?.Value;
        }
             
        public LinkedList<StatusType> StatusFlow
        {
            get
            {
                return ProcessLogic.StatusFlow;
            }
        }

        public string GetDescription() {
            var segs = new List<string>();
            segs.Add("{text.created} " + this.Created.ToString(Core.Config.DateFormat));

            if (Shop.Config.ShopInvoiceUseShipmentDateInDesc == true && this.Shipments.Any()) {
                segs.Add("{text.shipped} " + this.Shipments.FirstOrDefault()?.Created.ToString(Core.Config.DateFormat));
            }
            if (Shop.Config.ShopInvoiceUseOrderDateInDesc == true && this.DeliveryDate.HasValue == true) {
                segs.Add("{text.order.delivery-date} " + this.DeliveryDate.Value.ToString(Core.Config.DateFormat));
            }

            if (Shop.Config.ShopInvoiceUseItemCountInDesc == true) {
                segs.Add(this.OrderItems.Count() + " {text.order-items}");
            }
           
            return string.Join(", ", segs);
        }

        public List<Finance.Model.LineItemGroup> GetLineItemGroups() {
            this.LoadFirstLevelNavigationReferences();
            var groups = new List<Finance.Model.LineItemGroup>();
            {
                // Order group
                var group = new Finance.Model.LineItemGroup();
                group.Title = "{text.order} "+this.SerialId;
                group.Description = GetDescription();
                group.Category = "order"; //TODO: @dan
                              
                // Items
                foreach (var orderItem in this.OrderItems.OrderBy(oi => oi.UsedProductConfigurationTitle)) {

                    var customerVATRate = VAT.VATProvider.GetVATProvier().CalculateVATRateForOrderItem(this, orderItem);
                    var lineItem = new Finance.Model.LineItem();
                    lineItem.InternalItemPrice = orderItem.InternalPrice.Clone();
                    lineItem.Category = "product"; //TODO: @dan
                    lineItem.Quantity = orderItem.Quantity;
                    lineItem.Description = orderItem.Title;
                    lineItem.CustomerVATRate = customerVATRate;

                  
                    if (Config.ShopVATIncluded == false) {
                        lineItem.CustomerItemPrice = orderItem.CustomerPrice.Clone();
                        lineItem.CustomerVAT = lineItem.CustomerItemPrice * lineItem.CustomerVATRate;
                        lineItem.CustomerTotal =  new Price (lineItem.CustomerItemPrice.Value.Value * (1.0m + lineItem.CustomerVATRate) * lineItem.Quantity);
                    } else {
                        // Derive ItemPrice and VAT from orderItem
                        lineItem.CustomerItemPrice = new Price (orderItem.CustomerPrice.Value.Value / ( 1.0m + customerVATRate), this.Currency);
                        lineItem.CustomerVAT = new Price(orderItem.CustomerPrice.Value.Value / (1.0m + customerVATRate) * customerVATRate, this.Currency);
                        lineItem.CustomerTotal = orderItem.CustomerPrice * orderItem.Quantity;
                    }
                                                         
                    group.LineItems.Add(lineItem);
                }

            

                

                // Discount
                if(this.Discount.HasValue && this.Discount.Value != 0) {
                    group.AddDiscount(this.Discount);
                }

                // Shipping
                if(this.ShippingCustomer.HasValue && this.ShippingCustomer.Value > 0) {
                    group.AddShipping(this.ShippingCustomer);
                }
                groups.Add(group);
            }
            return groups;
        }

        public bool ApplyCoupon(ModelContext db, OrderCoupon coupon, bool throwException = true, bool ignoreStatus = false) {
        

            // Order status?
            if (ignoreStatus == false && this.Status != StatusType.Draft) {
                if (!throwException) { return false; }
                throw new BackendException("order-already-placed", "Sorry, your order has already been placed.");
            }

            // Already got a coupon?
            if (this.OrderCoupons.Any()) {
                if (!throwException) { return false; }
                throw new BackendException("coupon-already-applied", "Sorry, you can only apply one coupon at a time per order.");
            }

            // Valid for the order?
            bool valid = coupon.ValidateForOrder(db, this, throwException);

            // Register this coupon to the order
            if (valid) {
                this.OrderCoupons.Add(coupon);
            }

            this.UpdateOrder(ignoreStatus: ignoreStatus);
            return valid;
        }

        public void RemoveCoupon(OrderCoupon coupon) {
            this.OrderCoupons.Remove(coupon);
            this.UpdateOrder();
        }

        /// <summary>
        /// Gets the pending orders
        /// </summary>
        /// <param name="db">The database.</param>
        /// <returns></returns>
        public static IQueryable<Order> GetPendingOrders(ModelContext db, bool loadBusiness = false) {
            // Pending orders
            DbQuery<Order> orders = null;
            if (loadBusiness == true) {
                orders = db.Orders().Include(nameof(Order.Business));
            } else {
                orders = db.Orders();
            }

            return orders.Where(o => 
                (o.PaymentPointOfTime == PaymentTime.Before && (o.Status == Order.StatusType.Paid   || o.Status == Order.StatusType.Confirmed))
             || (o.PaymentPointOfTime == PaymentTime.After  && (o.Status == Order.StatusType.Placed || o.Status == Order.StatusType.Confirmed))
            );
        }

        /// <summary>
        /// Places the order for payment. Creates a new invoice.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="actingUser">The acting user.</param>
        /// <exception cref="BackendException">wrong-type;This order is not supposed to be paid before</exception>
        public void PlaceOrderForPayment(ModelContext db, User actingUser, bool validate = false) {
            if (this.PaymentPointOfTime != PaymentTime.Before) {
                throw new BackendException("wrong-type", "This order is not supposed to be paid before");
            }

            if (this.Status == StatusType.Draft) {
                this.ChangeStatus(StatusType.Placed, actingUser, false, validate);
            }
         
        }

        #endregion

        #region Public Override Methods ///////////////////////////////////////////////////////////

        public override int TypeNumber { get { return 20; } }
        
        public override Core.Cards.CardBuilder VisualCard() {
            // Init card
            this.Include(nameof(Business));

            var card = new Core.Cards.CardBuilder(this)
                .Title(this.SerialId)
                .Subtitle(this.Business?.Name)
                .Sub(this.Status)
                .Icon("tags")
                .Wide();
            // Delivery
            if (IsStatusBefore(StatusType.Fulfilled)) {
                if (this.DeliveryDate.HasValue) {
                    card.Sub("{text.order.delivery-date}" + " " + this.DeliveryDate.Value.ToDefaultTimezoneDateTimeString());
                    if (this.DeliveryDate != DateTime.MinValue && this.DeliveryDate.Value.Subtract(new TimeSpan(24, 0, 0)) < DateTime.UtcNow) card.BlinkSub();
                }
               
            }
            // Shipped?
            if(this.Shipments?.Count > 0) {
                card.Sub("{text.order-shipped}" + " " + this.Shipments.First().Created.ToDefaultTimezoneDateTimeString());
            }
            // Price
            card.Tag(this.TotalCustomer.ToString());
            // Breadcrumb
            card.Breadcrumb(this.StatusFlow, this.Status);
            return card;
        }



        /// <summary>
        /// Determines whether a given status  is before th
        /// if the compareStatus is null the current status is applied
        /// </summary>
        public bool IsStatusBefore(StatusType status, StatusType? compareStatus = null) {

            if (compareStatus == null) {
                compareStatus = this.Status;
            }

            var previous = StatusFlow.Find(status).Previous;
            if (previous == null) {
                return false;
            }
            if (previous.Value == compareStatus) {
                return true;
            }
            return IsStatusBefore(previous.Value, compareStatus);

        }


        public List<Core.Cards.CardBuilder> VisualCardsForCosts() {
            var ret = new List<Core.Cards.CardBuilder>();
            ret.Add(new Core.Cards.CardBuilder()
                .Type("{text.order-costs}")
                .Title("{text.order-customer}")
                .Sub("{text.order-subtotal}: "+this.SubtotalCustomer)
                .Sub("{text.order-shipping}: "+this.ShippingCustomer)
                .Sub("{text.order-discount}: "+this.Discount)
                .Sub("{text.order-vat}: "+this.VATCustomer)
                .Sub("{text.order-total}: "+this.TotalCustomer)
                .Icon("text-dollar"));
            ret.Add(new Core.Cards.CardBuilder()
                .Type("{text.order-costs}")
                .Title("{text.order-internal}")
                .Sub("{text.order-subtotal}: "+this.SubtotalInternal)
                .Sub("{text.order-shipping}: "+this.ShippingInternal)
                .Sub("{text.order-total}: "+this.TotalInternal)
                .Icon("text-dollar"));
            return ret;
        }

        public List<Core.Cards.CardBuilder> VisualCardsForShippingBilling() {
            var ret = new List<Core.Cards.CardBuilder>();
            // where to initialize these addresses
            if (this.ShippingAddress == null || this.BillingAddress == null) {
                return ret;
            }
            if (this.ShippingAddress == this.BillingAddress) {
                ret.Add(this.ShippingAddress.VisualCard().ClearType().Type("{text.order-shipping-and-billing}"));
            } else {
                ret.Add(this.ShippingAddress.VisualCard().ClearType().Type("{text.order-shipping}"));
                ret.Add(this.BillingAddress.VisualCard().ClearType().Type("{text.order-billing}"));
            }
            return ret;
        }



        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////
        private static void HandleInvoiceDeleted(Invoice invoice) {
            // Unlink 
            var orders = invoice.Context.Orders().Include("Invoice").Where(o => o.Invoice.Id == invoice.Id);
            foreach (var order in orders) {
                order.Invoice = null;
            }
        }

        private static void HandleInvoiceStatusChange(Invoice.InvoiceStatus status, Invoice invoice) {
            var orders = invoice.Context.Orders().Where(o => o.Invoice.Id == invoice.Id).ToList();
            foreach (var order in orders) {
                if (order.PaymentPointOfTime == PaymentTime.After) {
                    // Set order to paid if invoice is paid
                    if (status == InvoiceStatus.Paid && order.GetNextStatus().HasValue && order.GetNextStatus() == StatusType.Paid) {
                        order.ChangeStatus(Order.StatusType.Paid, null);
                        // Set order to refunded if invoice refunded
                    } else if (status == InvoiceStatus.Refunded) {
                        order.ChangeStatus(Order.StatusType.Refunded, null);
                    } else {
                        _logger.Warn($"Cannot update order {order.SerialId} status for invoice {invoice.SerialId}, Status is incorrect -> {order.Status}");
                    }

                } else if (order.PaymentPointOfTime == PaymentTime.Before) {
                    if (order.GetNextStatus().HasValue && order.GetNextStatus() == StatusType.Paid) {
                        order.ChangeStatus(Order.StatusType.Paid, null);
                        order.ChangeStatus(Order.StatusType.Confirmed, null);
                    } else if (status == InvoiceStatus.Refunded) {
                        order.ChangeStatus(Order.StatusType.Refunded, null);
                    } else {
                        _logger.Warn($"Cannot update order {order.SerialId} status for invoice {invoice.SerialId}, Status is incorrect -> {order.Status}");
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether we have Shipping address and items
        /// </summary>
        /// <returns></returns>
        private bool CanCalculateVAT() {
            return this.ShippingAddress != null && this.OrderItems.Count > 0;
        }



        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextOrderExtenions {
        [ModelSet]
        public static DbSet<Order> Orders(this Core.Model.ModelContext context) {
            return context.Set<Order>();
        }

        public static Order GetByHash(this IQueryable<Order> query, string orderHash, bool throwExceptions = true)  {
            var ret = query.Where(e => e.Hash == orderHash).SingleOrDefault();
            if (ret == null && throwExceptions == true) throw new Backend404Exception("order-not-found", $"The entity {typeof(Order).Name} with ID {orderHash} could not be found.");
            return ret;
        }

        public static IQueryable<Order> ConfirmedWithDates(this DbQuery<Order> orders) {
            return orders.Where(o => o.Status >= Order.StatusType.Confirmed && o.Status <= Order.StatusType.Fulfilled).Where(Order.HasOrderDate);
        }
    }

    public static class ModelObjectOrderExtenions {
        public static IQueryable<Order> Orders(this Core.Model.User user) {
            return user.Context.Orders().Where(o => o.User.Id == user.Id);
        }

        public static IQueryable<Order> Orders(this Core.Model.ModelContext context, Invoice invoice) {
            return context.Orders().Include(nameof(Order.Invoice)).Where(o => o.Invoice.Id == invoice.Id);
        }
    }

    #endregion

}
