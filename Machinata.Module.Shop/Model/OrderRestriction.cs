using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using static Machinata.Module.Shop.View.PageTemplateExtension;
using Machinata.Core.Util;

namespace Machinata.Module.Shop.Model {

    [Serializable()]
    [ModelClass]
    public class OrderRestriction : ModelObject {

        public enum RestrictionTypes : short {
            NoOrdersDuringPeriod = 10,
            ShopOrderDeliveryMinimumDaysBefore = 20,
            ShopOrderMinimumTotal = 30,
            NoOrdersOnWeekday = 40
        }

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public OrderRestriction() {
            this.RestrictionPeriode = new DateRange();
            this.BusinessOrderRestrictions = new List<BusinessOrderRestriction>();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public string Name { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public string Value { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public bool AllBusinesses { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        [Required]
        public RestrictionTypes RestrictionType { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public DateRange RestrictionPeriode { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder(Forms.Frontend.LISTING)]
        public string Description {
            get {
                return ToString();
            }
        }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        [CascadeDelete]
        public ICollection<BusinessOrderRestriction> BusinessOrderRestrictions { get; set; }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public bool IsValid(DateTime? date, Order order) {
            if (date.HasValue && this.RestrictionType == RestrictionTypes.NoOrdersDuringPeriod) {
                var fromDate = Core.Util.Time.GetUTCTimezoneDateForDefaultTimezone(this.RestrictionPeriode.Start.Value);
                var toDate = Core.Util.Time.GetUTCTimezoneDateForDefaultTimezone(this.RestrictionPeriode.End.Value).AddDays(1).AddSeconds(-1);
                if (date >= fromDate && date <= toDate) {
                    return false;
                }
            } else if (date.HasValue && this.RestrictionType == RestrictionTypes.ShopOrderDeliveryMinimumDaysBefore) {
                int value = 0;
                if (!int.TryParse(this.Value, out value)) {
                    throw new BackendException("restriction-error", $"Restriction value is not supported: {this.Value}");
                }
                if ((date.Value - DateTime.UtcNow).TotalDays < value && date > Core.Util.Time.GetUTCTimezoneTodayForDefaultTimezone()) {
                    return false;
                }
            } else if (this.RestrictionType == RestrictionTypes.ShopOrderMinimumTotal) {
                if (order != null) {
                    int value = 0;
                    if (!int.TryParse(this.Value, out value)) {
                        throw new BackendException("restriction-error", $"Restriction value is not supported: {this.Value}");
                    }
                    if (order.TotalCustomer < new Price(value, order.Currency)) {
                        return false;
                    }
                }
            } else if (this.RestrictionType == RestrictionTypes.NoOrdersOnWeekday) {
                if (Core.Util.Time.IsWeekday(this.Value) == false) {
                    throw new BackendException("restriction-error", $"Restriction value is not supported: {this.Value}");
                }

                var dayValid = true;
                
                if (date.HasValue == true) {
                    var dateToLocalTime = Core.Util.Time.ToDefaultTimezone(date.Value);
                    var checkDayOfWeek = dateToLocalTime.DayOfWeek;
                    if (this.Value.ToLower() == checkDayOfWeek.ToString().ToLower()) {
                        dayValid = false;
                    }
                }

                if (dayValid == false && date.HasValue && this.RestrictionPeriode != null && this.RestrictionPeriode.Start != null && this.RestrictionPeriode.End != null) {
                    var fromDate = Core.Util.Time.GetUTCTimezoneDateForDefaultTimezone(this.RestrictionPeriode.Start.Value);
                    var toDate = Core.Util.Time.GetUTCTimezoneDateForDefaultTimezone(this.RestrictionPeriode.End.Value).AddDays(1).AddSeconds(-1);
                    if (date >= fromDate && date <= toDate) {
                        return false;
                    } else {
                        return true;
                    }
                }

                return dayValid;
               
            } else {
                throw new BackendException("restriction-error", $"Restriction type '{this.RestrictionType}' is not supported");
            }
            return true;
        }

        /// <summary>
        /// Validates an order for the given Business, 
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="order">The order.</param>
        /// <param name="business">The business.</param>
        /// <returns>OrderRestrictions which didnt validate</returns>
        public static IEnumerable<OrderRestriction> ValidateRestrictions(ModelContext db, DateTime? date, Business business, Order order) {
            var restrictionsToCheck = GetRestrictions(db, business);
            var validationErrors = new List<OrderRestriction>();
            foreach (var restriction in restrictionsToCheck) {
                if (!restriction.IsValid(date,order)) {
                    validationErrors.Add(restriction);
                }
            }
            return validationErrors;
        }

        public static IEnumerable<OrderRestriction> GetRestrictions(ModelContext db, Business business) {
            var restrictionsForAll = db.OrderRestrictions().Where(r => r.AllBusinesses == true);
            if (business != null) {
                var businessRestrictions = GetBusinessOrderRestrictions(db).Where(bor => bor.Business.Id == business.Id).Select(bor => bor.Restriction).Where(r => r.AllBusinesses == false).ToList();
                return businessRestrictions.Union(restrictionsForAll);
            }
            return restrictionsForAll; 
        }

        public IQueryable<Business> GetBusinesses() {
            return this.BusinessOrderRestrictions.Select(bor => bor.Business).AsQueryable();
        }

        public void Add(Business business) {
            this.BusinessOrderRestrictions.Add(new BusinessOrderRestriction() { Business = business, Restriction = this });
        }

        public void Remove(ModelContext db, Business business) {
            var businessRestriction = this.BusinessOrderRestrictions.SingleOrDefault(bor => bor.Business == business);
            if (businessRestriction != null) {
                this.BusinessOrderRestrictions.Remove(businessRestriction);
                db.Set<BusinessOrderRestriction>().Remove(businessRestriction);
            }
          
        }

        public static IQueryable<BusinessOrderRestriction> GetBusinessOrderRestrictions(ModelContext db) {
            var businessOrderRestrictions = db.Set<BusinessOrderRestriction>().Include(nameof(BusinessOrderRestriction.Business)).Include(nameof(BusinessOrderRestriction.Restriction));
            return businessOrderRestrictions;      
        }

        //public static bool IsOrderDateRestricted(ModelContext db, DateTime date, Business business) {
        //    var restrictions = GetRestrictions(db, business);
        //    foreach (var restriction in restrictions) {
        //        if (!restriction.IsValid(date)) {
        //            return true;
        //        }
        //    }
        //    return false;
        //}



        #endregion

        #region Override Methods ///////////////////////////////////////////////////////////////////
        public override void OnDelete(ModelContext db) {
            this.Include(nameof(BusinessOrderRestrictions));

            // Cascade delete
            foreach (var businessRestriction in this.BusinessOrderRestrictions.Where(r => r.Restriction.Id == this.Id).ToList()) {
                db.Set<BusinessOrderRestriction>().Remove(businessRestriction);
            }
            base.OnDelete(db);

        }

        public override void Validate() {
            if (this.RestrictionType == RestrictionTypes.NoOrdersDuringPeriod) {
                if (this.RestrictionPeriode == null
                 || this.RestrictionPeriode.Start == null
                 || this.RestrictionPeriode.Start.HasValue == false
                 || this.RestrictionPeriode.End == null
                 || this.RestrictionPeriode.End.HasValue == false) {
                    throw new BackendException("restriction-validation", $"{this.RestrictionType} Restriction needs a valid Restriction Periode");
                }
            } else if (this.RestrictionType == RestrictionTypes.ShopOrderDeliveryMinimumDaysBefore || this.RestrictionType == RestrictionTypes.ShopOrderMinimumTotal) {
                int val;
                if (string.IsNullOrEmpty(this.Value)) {
                    throw new BackendException("restriction-validation", $"{this.RestrictionType} Restriction needs a value");
                }

                if (!int.TryParse(this.Value, out val)) {
                    throw new BackendException("restriction-validation", $"{this.RestrictionType} Restriction needs a number");
                }
                base.Validate();
            }
            else if (this.RestrictionType == RestrictionTypes.NoOrdersOnWeekday) {

                if (string.IsNullOrEmpty(this.Value)) {
                    throw new BackendException("restriction-validation", $"{this.RestrictionType} Restriction needs a value");
                }

                if (Core.Util.Time.IsWeekday(this.Value) == false) {
                    throw new BackendException("restriction-validation", $"{this.RestrictionType} Restriction needs a weekday for a value");
                }

                base.Validate();
            }

        }

       

        public override string ToString() {
            if (this.RestrictionType == RestrictionTypes.NoOrdersDuringPeriod) {
                return this.RestrictionType.GetEnumTranslationTextVariable() + ": " + this.RestrictionPeriode.ToDefaultTimezone() + " - " + this.Name;
            } else if (this.RestrictionType == RestrictionTypes.ShopOrderDeliveryMinimumDaysBefore) {
                return this.RestrictionType.GetEnumTranslationTextVariable() + ": " + this.Value + " {text.days}";
            } else if (this.RestrictionType == RestrictionTypes.ShopOrderMinimumTotal) {
                return this.RestrictionType.GetEnumTranslationTextVariable() + ": " + this.Value + $" {Core.Config.CurrencyDefault}";
            } else if (this.RestrictionType == RestrictionTypes.NoOrdersOnWeekday) {
                return this.RestrictionType.GetEnumTranslationTextVariable() + ": " + this.Value;
            }
            return base.ToString();
        }
        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextOrderRestrictionExtenions {

        [ModelSet]
        public static DbSet<OrderRestriction> OrderRestrictions(this Core.Model.ModelContext context) {
            return context.Set<OrderRestriction>();
        }
    }
    #endregion

    public class BusinessOrderRestriction : ModelObject {

        [Column]
        public Business Business { get; set; }

        [Column]
        public OrderRestriction Restriction { get; set; }
 
    }


}

