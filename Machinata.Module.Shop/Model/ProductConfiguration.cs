using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using System.Data.Entity.Infrastructure;

namespace Machinata.Module.Shop.Model {

    /// <summary>
    /// Receives Order, creates deliverables, sends out goods
    /// (PrintProvider)
    /// </summary>
    /// <seealso cref="Machinata.Core.Model.ModelObject" />
    [Serializable()]
    [ModelClass]
    public partial class ProductConfiguration : ModelObject {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public ProductConfiguration() {
            this.InternalPrice = new Price();
            this.CustomerPrice = new Price();
            this.RetailPrice = new Price();
            this.Infos = new Properties();
        }

        #endregion

        #region Constants //////////////////////////////////////////////////////////////////////

     
        #endregion


        #region Public Data Store Properties //////////////////////////////////////////////////////

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.SELECTION)]
        [FormBuilder(Forms.API.VIEW)]
        [MinLength(3)]
        [MaxLength(30)]
        public string SKU { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.WILDCARD)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.EDIT)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public string Title { get; set; }
 

        /// <summary>
        /// Can be overridden in the ProductPrice per Business
        /// </summary>
        /// <value>
        /// The customer price.
        /// </value>
        [FormBuilder]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.EDIT)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public Price RetailPrice { get; set; }

        /// <summary>
        /// Can be overridden in the ProductPrice per Business
        /// </summary>
        /// <value>
        /// The customer price.
        /// </value>
        [FormBuilder]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.WILDCARD)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public Price CustomerPrice { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.WILDCARD)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        [Percent]
        public decimal? CustomerVATRate { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        [Percent]
        public decimal? RetailVATRate { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        public int? MinimumOrderAmount { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public Price InternalPrice { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public Properties Infos { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public string Value { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public bool IsDefault { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public int? StockQuantity { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public string BarCode { get; set; }

        #endregion

        #region Navigation Properties  /////////////////////////////////////////////////////////////
        [FormBuilder]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public Business Business { get; set; }

        [Column]
        public Product Product { get; set; }
        #endregion


        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder]
        [NotMapped]
        public string Name
        {
            get
            {
                if (this.IsDefault) {
                    return $"Default: {this.Title} - {this.SKU}";
                }
                return $"{this.Business?.Name}: {this.Title} - {this.SKU}";
            }
        }

        [NotMapped]
        public bool IsStockLow
        {
            get
            {
                if (this.StockQuantity == null) {
                    return false; // We don't now
                } else {
                    return this.StockQuantity <= 0; // It's definately low
                }
            }
        }

        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)] // Used for link in low-stock view
        [FormBuilder(Forms.System.LISTING)]
        [NotMapped]
        public string ProductId
        {
            get
            {
                return this.Product?.PublicId;
            }
        }

        [FormBuilder]
     //   [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [NotMapped]
        public string ProductName
        {
            get
            {

                return this.Product?.Name;
            }
        }

        /// <summary>
        /// If this is a merged configuration this is set to the selected/overloaded Configuration.PublicId of 
        /// </summary>
        /// <value>
        /// The origin public identifier.
        /// </value>
        [NotMapped]
        [FormBuilder]
        public string OriginPublicId { get; internal set; }


        [FormBuilder]
        [NotMapped]
        public Properties ProductData {
            get {
              return this.Product?.Details;
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////


        /// <summary>
        /// Determines whether the stock too low,
        /// </summary>
        /// <param name="items">The number of items to take from stock.</param>
        /// <returns></returns>
        //public bool CanDecreaseStockItems(int items = 1) {
        //    return this.StockQuantity.HasValue && this.StockQuantity - items < Config.ShopMinQuantityStock;
        //}

        public bool HasEnoughStockItems(int quantity) {
            if (!this.StockQuantity.HasValue) {
                return true;
            }
            return this.StockQuantity - quantity >= 0;
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override void Validate() {
            base.Validate();
            if (this.IsDefault) {
                if (!this.CustomerPrice.HasValue) {
                    throw new BackendException("customer-price-error", "The default configuration requires a Customer Price");
                }
            }
        }

        [FormBuilder]
        [NotMapped]
        [FormBuilder(Forms.System.LISTING)]
        public override string PublicId
        {
            get
            {
                return this.OriginPublicId == null ? base.PublicId : this.OriginPublicId;
            }
        }


     
        #endregion

    }

        #region Extensions ////////////////////////////////////////////////////////////////////////////

        public static class ModelContextProductConfigurationExtenions {

        [ModelSet]
        public static DbSet<ProductConfiguration> ProductConfigurations(this Core.Model.ModelContext context) {
            return context.Set<ProductConfiguration>();
        }

        public static IQueryable<ProductConfiguration> GetLowStockConfigurations(this DbQuery<ProductConfiguration> configurations, int threshold = 0) {
            return configurations.Where(p => p.StockQuantity <= threshold);
        }
    }
        

    #endregion

}
