//using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;
//using System.Data.Entity;
//using System.Data.Entity.Core.Objects.DataClasses;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//using Machinata.Core.Builder;
//using Machinata.Core.Model;
//using Machinata.Core.Exceptions;

//namespace Machinata.Module.Shop.Model {
    
//    [Serializable()]
//    [ModelClass] 
//    public partial class ProductPrice : ModelObject {
        
//        #region Class Logger
//        /// <summary>
//        /// The logger helper for this class. Use this logger instance anytime you want to
//        /// log something from within this class.
//        /// </summary>
//        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
//        #endregion
        
//        #region Constructors //////////////////////////////////////////////////////////////////////

//        public ProductPrice() {
//            this.CustomerPrice = new Price();
//            this.RetailPrice = new Price();
//        }

//        #endregion

//        #region Public Data Store Properties //////////////////////////////////////////////////////
        
//        [FormBuilder]
//        [FormBuilder(Forms.Admin.CREATE)]
//        [FormBuilder(Forms.Admin.EDIT)]
//        [FormBuilder(Forms.Admin.VIEW)]
//        [FormBuilder(Forms.Admin.SELECTION)]
//        [FormBuilder(Forms.Admin.LISTING)]
//        [Column]
//        public Price CustomerPrice { get; set; }
              
        
//        [FormBuilder]
//        [FormBuilder(Forms.Admin.CREATE)]
//        [FormBuilder(Forms.Admin.EDIT)]
//        [FormBuilder(Forms.Admin.VIEW)]
//        [FormBuilder(Forms.Frontend.VIEW)]
//        [FormBuilder(Forms.Frontend.EDIT)]
//        [FormBuilder(Forms.Admin.SELECTION)]
//        [FormBuilder(Forms.Admin.LISTING)]
//        [Column]
//        public Price RetailPrice { get; set; }
        
//        [FormBuilder]
//        [FormBuilder(Forms.Admin.CREATE)]
//        [FormBuilder(Forms.Admin.EDIT)]
//        [FormBuilder(Forms.Admin.VIEW)]
//        [FormBuilder(Forms.Admin.LISTING)]
//        [Column]
//        [Percent]
//        public decimal? CustomerVATRate { get; set; }
        
//        [FormBuilder]
//        [FormBuilder(Forms.Admin.CREATE)]
//        [FormBuilder(Forms.Admin.EDIT)]
//        [FormBuilder(Forms.Admin.VIEW)]
//        [FormBuilder(Forms.Admin.LISTING)]
//        [FormBuilder(Forms.Frontend.VIEW)]
//        [FormBuilder(Forms.Frontend.EDIT)]
//        [Column]
//        [Percent]
//        public decimal? RetailVATRate { get; set; }
      

//        #endregion

//        #region Public Navigation Properties /////////////////////////////////////////////////////

//        [Column]
//        [ForeignKey("ProductId")]
//        public Product Product { get; set; }

//        [Index("IX_BusinessPrice", 1, IsUnique = true)]
//        public int ProductId { get; set; }

//        [Column]
//        [FormBuilder(Forms.Admin.VIEW)]
//        [FormBuilder(Forms.Admin.EDIT)]
//        [FormBuilder(Forms.Admin.SELECTION)]
//        [FormBuilder(Forms.Admin.LISTING)]
//        [ForeignKey("BusinessId")]
//        public Business Business { get; set; }

//        [Index("IX_BusinessPrice", 2, IsUnique = true)]
//        public int? BusinessId { get; set; }


//        #endregion

//        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////



//        #endregion

//        #region Model Creation ////////////////////////////////////////////////////////////////////

//        #endregion

//        #region Public Methods ////////////////////////////////////////////////////////////////////

//        ///// <summary>
//        ///// check if there is a specific price for the given business, if none return ProductPrice without a Business
//        ///// </summary>
//        ///// <param name="db"></param>
//        ///// <param name="product"></param>
//        ///// <param name="business"></param>
//        ///// <returns></returns>
//        //public static ProductPrice GetPrice(Product product, Business business) {
//        //    if (product.Context != null) {
//        //        product.Include(nameof(product.Prices));
//        //        product.Prices.ToList().ForEach(p => p.Include(nameof(p.Business)));
//        //    }
//        //    ProductPrice businessPrice = GetBusinessPrice(product, business);
//        //    if (businessPrice == null) {
             
//        //        return product.Prices.SingleOrDefault(p => p.Business == null && p.Product == product);
//        //    }
//        //    return businessPrice;

//        //}

//        /// <summary>
//        /// Gets the price for a business.
//        /// </summary>
//        /// <param name="product">The product.</param>
//        /// <param name="business">The business.</param>
//        /// <returns></returns>
//        public static ProductPrice GetBusinessPrice(Product product, Business business) {
//            if (product.Context != null) {
//                product.Include("Prices");
//            }
//            product.Prices.ToList().ForEach(p => p.Include(nameof(p.Business)));
//            return product.Prices.SingleOrDefault(p => p.Business == business && p.Product == product);
//        }

//        public void SetBusiness(Business business) {
//            this.Include(nameof(Product));
//            Product.Include(nameof(Product.Prices));

//            foreach (var price in Product.Prices) {
//                price.Include(nameof(price.Business));
//            }

//            // There is a Index 'IX_BusinessPrice' to assure Business and Product combination is unique
//            // Extra check only one <null> is allowed for Business --> TODO: HOW IN GUI/db???
//            if (Product.Prices.Select(p => p.Business).Contains(business)) {
//                throw new BackendException("product-already-price-business", "This product already contains a Price for this Business");
//            }
//            this.Business = business;
//        }


//        #endregion

//        #region Private Methods ///////////////////////////////////////////////////////////////////

//        #endregion

//    }

//    #region Extensions ////////////////////////////////////////////////////////////////////////////

//    public static class ModelContextPriceExtenions {
//        [ModelSet]
//        public static DbSet<ProductPrice> Prices(this Core.Model.ModelContext context) {
//            return context.Set<ProductPrice>();
//        }
//    }

//    //public static class ModelObjectOrderExtenions {
//    //    public static IQueryable<Business> Businesses(this Price price) {
//    //        return price.Context.Businesses().Where(p => p.Id == );
//    //    }
//    //}

//    #endregion

//}
