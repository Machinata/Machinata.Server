using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Module.Shop.Model;
using Machinata.Module.Finance.Model;
using System.ComponentModel;
using Machinata.Core.Exceptions;
using Machinata.Module.Shop.Logic;

namespace Machinata.Module.Shop.Model {
    
    [Serializable()]
    [ModelClass] 
    public partial class OrderItem : ModelObject, IQuanityModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public OrderItem() {
            this.Infos = new Properties();
        }

        #endregion

        #region Constants //////////////////////////////////////////////////////////////////////

        public static string INFOS_KEY_PRODUCT_CONFIGURATION = "ProductConfiguration";

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [Column]
        public string Title { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [Column]
        public string SKU { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [Column]
        public Properties Infos { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [Column]
        public int Quantity { get; set; }

      
        
        [FormBuilder(Forms.Admin.EDIT)]
        [NotMapped]
        public int SetQuantity
        {
            get { return Quantity; }
            set
            {
                Quantity = value;
                Include(nameof(Order));
                Order.UpdateOrder();
            }
        }

        [FormBuilder]
        [Column]
        public string ExternalId { get; set; }


        [Column]
        [DefaultValue(true)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.EXPORT)]
        public bool Rejection { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.EXPORT)]
        public Price RetailPrice { get; set; }

        [Column]
        public Price InternalPrice { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.EXPORT)]
        public Price CustomerPrice { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [Column]
        [Percent]
        public decimal? CustomerVATRate { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [Column]
        [Percent]
        public decimal? RetailVATRate { get; set; }

        #endregion

        #region Public Navigation Properties ////////////////////////////////////////////////////// 
               
        [Column]
        public ProductConfiguration Configuration { get; set; }

        [Column]
        public Order Order { get; set; }
        
        [Column]
        public OrderShipment Shipment { get; set; }

        [Column]
        public Product Product { get; set; }
       
      
        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.TOTAL)]
        [NotMapped]
        public Price CustomerPriceSum
        {
            get
            {
                return Quantity * CustomerPrice;
            }
        }


        [NotMapped]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Frontend.TOTAL)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public int QuantityForTotal
        { 
            get
            {
                return this.Rejection ? -this.Quantity : this.Quantity;
            }
        }

        [NotMapped]
        public Price UnitPriceWithVAT
        {
            get
            {
                return this.CustomerPrice + this.CustomerPrice * this.CustomerVATRate;
            }
        }

        [NotMapped]
        [FormBuilder(Forms.Admin.EXPORT)]
        public Price CustomerVAT
        {
            get
            {
                return this.CustomerPriceSum *  this.CustomerVATRate;
            }
        }

        [NotMapped]
        [FormBuilder(Forms.Admin.EXPORT)]
        public Price CustomerTotal
        {
            get
            {
                return this.CustomerVAT + this.CustomerPriceSum;
            }
        }

        public string PublicIdForQuantityEntity
        {
            get
            {
                if (this.Configuration == null) return null;
                else return this.Configuration.PublicId;
            }
        }

        [NotMapped]
        public string UsedProductConfigurationTitle {
            get {
                return this.UsedProductConfiguration?.Title;
            }
        }

      
        [NotMapped]
        [FormBuilder(Forms.Admin.EXPORT)]
        public string BusinessName {
            get {
                return this.Order?.Business?.Name;
            }
        }

        [NotMapped]
        [FormBuilder(Forms.Admin.EXPORT)]
        public DateTime? OrderDate {
            get {
                return this.Order?.OrderDate;
            }
        }

        [NotMapped]
        [FormBuilder(Forms.Admin.EXPORT)]
        public string OrderNumber {
            get {
                return this.Order?.SerialId;
            }
        }


        /// <summary>
        /// Gets the merged ProductConfiguration (which was used when creating the OrderItem) from the serialized OrderItem.Infos
        /// HINT: not from DB
        /// </summary>
        /// <param name="orderItem"></param>
        /// <returns></returns>
        [NotMapped]
        public ProductConfiguration UsedProductConfiguration {
            get {
                // Configuration
                ProductConfiguration configuration = null;
                if (this.Infos != null && this.Infos.Keys.Contains(OrderItem.INFOS_KEY_PRODUCT_CONFIGURATION)) {
                    var jobject = this.Infos[OrderItem.INFOS_KEY_PRODUCT_CONFIGURATION] as Newtonsoft.Json.Linq.JObject;
                    configuration = jobject.ToObject<ProductConfiguration>();
                }

                return configuration;
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////


        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Creates the order item with a given configuration from the database
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="product">The product.</param>
        /// <param name="quantity">The quantity.</param>
        /// <param name="configuration">The configuration.</param>
        /// <returns></returns>
        public static OrderItem CreateOrderItem(ModelContext db, Product product, ProductConfiguration configuration, string currency, int quantity, string title) {
            var merged = product.GetConfiguration(configuration);
            var orderItem = CreateOrderItemWithMergedConfiguration(db, product, merged, quantity, title, currency);
            orderItem.Configuration = configuration;
            return orderItem;
        }

        private static OrderItem CreateOrderItemWithMergedConfiguration(ModelContext db, Product product, ProductConfiguration configuration, int quantity, string title, string currency) {
            var orderItem = new OrderItem();
            orderItem.Product = product;
            orderItem.Quantity = quantity;
            orderItem.SetConfigurationData(configuration,currency,title);
            db.SaveChanges();
            return orderItem;
        }

        private void SetConfigurationData(ProductConfiguration configuration, string currency, string title) {
            this.Title = title;
            this.SKU = configuration.SKU;
            this.CustomerPrice = configuration.CustomerPrice.GetRoundForeignCurrencyPrice(currency);
            this.RetailPrice = configuration.RetailPrice.Clone();
            this.CustomerVATRate = configuration.CustomerVATRate;
            this.RetailVATRate = configuration.RetailVATRate;

            var shallow = new ProductConfiguration();
            configuration.CopyValuesTo(shallow, new FormBuilder(Forms.Admin.VIEW));
            this.Infos[OrderItem.INFOS_KEY_PRODUCT_CONFIGURATION] = shallow;
            this.InternalPrice = configuration.InternalPrice.Clone();
        }

               
        public override string ToString() {
            return "Order Item: " + Title;
        }


        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }
    
    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextOrderItemExtenions {
        [ModelSet]
        public static DbSet<OrderItem> OrderItems(this Core.Model.ModelContext context) {
            return context.Set<OrderItem>();
        }
    }

    #endregion

}
