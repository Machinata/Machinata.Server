
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Module.Shop.Model;
using System.Collections;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Shop.Logic;

namespace Machinata.Module.Admin.Handler {


    public class CustomersAdminHandler : AdminPageTemplateHandler {
        
        [RequestHandler("/admin/shop/customers")]
        public void Default() {
            // Navigation
            var confirmedOrderUsers = Customers.GetUsersWithConfirmedOrders(this.DB);
            confirmedOrderUsers = this.Template.Paginate(confirmedOrderUsers, this);
            this.Template.InsertEntityList("entity-list", confirmedOrderUsers, new FormBuilder(Forms.Admin.LISTING), "/admin/shop/customers/customer/{entity.public-id}");
            this.Navigation.Add("shop");
            this.Navigation.Add("customers");
        }


        [RequestHandler("/admin/shop/customers/customer/{userId}")]
        public void Customer(string userId) {
            var customer = this.DB.Users().GetByPublicId(userId);
            var confirmedOrdersUser = this.DB.Orders()
                .Include(nameof(Order.Business))
                .Include(nameof(Order.User))
                .ConfirmedWithDates()
                .Where(o => o.User.Id == customer.Id);
            confirmedOrdersUser = this.Template.Paginate(confirmedOrdersUser, this);
            this.Template.InsertEntityList("entity-list", confirmedOrdersUser, new FormBuilder(Forms.Admin.LISTING), "/admin/shop/orders/order/{entity.public-id}");

            this.Navigation.Add("shop");
            this.Navigation.Add("customers");
            this.Navigation.Add("customer/" + userId, customer.Name);
        }


    }
}
