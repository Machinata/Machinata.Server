using System.Linq;
using Machinata.Core.Handler;
using Machinata.Module.Shop.Model;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using System.Collections.Generic;
using Machinata.Core.Builder;

namespace Machinata.Module.Admin.Handler {


    public class ShopAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("shop");
        }

        #endregion

        #region Menu Item

        //[Core.Builder.MenuBuilder]
        //public static MenuItem GetMenuItem() {
        //    var item = new MenuItem();
        //    item.Icon = "shop";
        //    item.Path = "/admin/shop";
        //    item.Title = "{text.shop}";
        //    return item;
        //}

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "shop",
                Path = "/admin/shop",
                Title = "{text.shop}",
                Sort = "500"
            });
        }

        #endregion

        [RequestHandler("/admin/shop")]
        public void Default() {
            // Menu items
            var menuItems = PageTemplate.Cache.FindAll(this.Template.Package, "admin/shop/menu/menu.item.", this.TemplateExtension);

            // Show packaging
            if (!Shop.Config.ShopPackagingEnabled) {
                menuItems = menuItems.Where(m => !m.Name.Contains(".packaging")).ToList();
            }

            this.Template.InsertTemplates("shop.menu-items", menuItems);
            // Pending orders
            var pendingOrders = Order.GetPendingOrders(DB, loadBusiness: true).OrderByDescending(o => o.Id).ToList();
            this.Template.InsertEntityList("pending-orders", pendingOrders.AsQueryable());
            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
        }

       

        [RequestHandler("/admin/shop/sandbox")]
        public void Sandbox() {
            // Invoke a handler method without using a context
            var handler = Core.Handler.Handler.CreateForRoute<Shop.Handler.ShopPDFHandler>("/pdf/shop/invoices/invoice/hTKaaQ");
            handler.Invoke();
            // Save...
            handler.SaveToFile("out.pdf");
            // Or get data
            var data = handler.GeneratePDFData();

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("sandbox", "{text.sandbox}");
        }

    }
}
