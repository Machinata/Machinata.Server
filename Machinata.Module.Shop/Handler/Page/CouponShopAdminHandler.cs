
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Module.Shop.Model;
using System.Collections;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Admin.Handler {


    public class CouponShopAdminHandler : AdminPageTemplateHandler {
        
        [RequestHandler("/admin/shop/coupons")]
        public void Coupons() {
            var entities = this.DB.OrderCoupons().OrderByDescending(e => e.Created);
            entities = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList("entity-list", entities, new FormBuilder(Forms.Admin.LISTING), "/admin/shop/coupons/coupon/{entity.public-id}");

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("coupons", "{text.coupons}");
        }



        [RequestHandler("/admin/shop/coupons/coupon/{publicId}")]
        public void CouponView(string publicId) {
            var entity = DB.OrderCoupons()
                .Include(nameof(OrderCoupon.Orders) + "." + nameof(Order.Business))
                .GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            entity.LoadFirstLevelObjectReferences();

            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW), true, true);

            //Products in catalog
            this.Template.InsertEntityList("entity.orders", entity.Orders.AsQueryable(), new FormBuilder(Forms.Admin.LISTING), "/admin/shop/orders/order/{entity.public-id}");
            this.Template.InsertVariable("entity.orders.count", entity.Orders.Count);

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("coupons", "{text.coupons}");
            this.Navigation.Add($"coupon/{publicId}", entity.Title);
        }

        

        [RequestHandler("/admin/shop/coupons/create")]
        public void CouponCreate() {
            this.RequireWriteARN();

            this.Template.InsertForm(
               variableName: "form",
               entity: new OrderCoupon() { Expires = DateTime.UtcNow.AddYears(1), Enabled = true },
               form: new FormBuilder(Forms.Admin.CREATE),
               apiCall: "/api/admin/shop/coupons/create",
               onSuccess: "/admin/shop/coupons/coupon/{coupon.public-id}"
            );

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("coupons", "{text.coupons}");
            this.Navigation.Add("create", "{text.create}");
        }

        [RequestHandler("/admin/shop/coupons/create/advanced")]
        public void CouponCreateAdvanced() {
            this.RequireWriteARN();

            this.Template.InsertForm(
               variableName: "form",
               entity: new OrderCoupon() { Expires = DateTime.UtcNow.AddYears(1), Enabled = true },
               form: new FormBuilder(Forms.Admin.CREATE),
               apiCall: "/api/admin/shop/coupons/create",
               onSuccess: "/admin/shop/coupons/coupon/{coupon.public-id}"
            );

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("coupons", "{text.coupons}");
            this.Navigation.Add("create", "{text.create}");
            this.Navigation.Add("advanced", "{text.advanced}");
        }


        [RequestHandler("/admin/shop/coupons/coupon/{publicId}/edit")]
        public void CouponEdit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.OrderCoupons().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: "/api/admin/shop/coupon/" + entity.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("coupons", "{text.coupons}");
            this.Navigation.Add($"coupon/{publicId}", entity.Title);
            this.Navigation.Add("edit", "{text.edit}");
        }



    }
}
