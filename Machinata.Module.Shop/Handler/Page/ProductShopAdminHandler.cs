
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Module.Shop.Model;
using System.Collections;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Newtonsoft.Json.Linq;
using Machinata.Module.Shop.Services;
using NLog.Filters;

namespace Machinata.Module.Admin.Handler {


    public class ProductShopAdminHandler : AdminPageTemplateHandler {
        
        [RequestHandler("/admin/shop/products")]
        public void Products() {
            var filter = this.Params.String("filter-query")?.Trim();
            //    var filterProperties = new string[] { nameof(Product.Name), nameof(Product.Notes), nameof(Product.DetailsSearchString) };
            var entities = this.DB.Products()
                .Include(nameof(Product.Related))
                .Include(nameof(Product.Categories))
              //  .Include(nameof(Product.Description)+"." + nameof(ContentNode.Children))
                .OrderByDescending(e => e.Created).AsQueryable(); // TODO refactor to not use .ToList if perf problems
            entities = this.Filter(this.Template, entities, this, entitiesToFilter => {
                if (filter == null || filter == "") return entitiesToFilter;
                return entitiesToFilter.Where(u =>
                    (u.Name != null && u.Name.Contains(filter))
                    ||
                    (u.Details.Data != null && u.Details.Data.Contains(filter))
                    ||
                    (u.Notes != null && u.Notes.Contains(filter) == true)
                    // ||
                    //(u.Description != null && u.Description.Summary.Contains(filter) == true) // NOT working?

                );
            });

           

            entities = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList(
                variableName:"entity-list", 
                entities: entities.ToList(), 
                form: new FormBuilder(Forms.Admin.LISTING), 
                link: "/admin/shop/products/product/{entity.public-id}"
            );
            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("products", "{text.products}");
        }


        // HINT: this might me removed when upstream is pulled  Machinata.Core.Templates.PageTempalte
        public IQueryable<T> Filter<T>(PageTemplate template, IQueryable<T> entities, Core.Handler.Handler handler, Func<IQueryable<T>, IQueryable<T>> customFilterFunction) where T : ModelObject {
            // Init
            var filterQuery = handler.Params.String("filter-query");
            // UI
            var filterTemplate = template.LoadTemplate("entity-list.filter");
            template.InsertTemplate("entity-list.filter", filterTemplate);
            // Variables
            template.InsertVariable("filter.property", "");
            template.InsertVariable("filter.query", filterQuery);
            // Do filter
            return customFilterFunction(entities);
        }

        [RequestHandler("/admin/shop/products/export/html")]
        public void ExportHTML() {
            // Init
            var entities = this.DB.Products()
                .OrderBy(e => e.Name)
                .ToList();

            // Recursively load all nodes from entities as HTML
            this.Template.ChangeTemplate("content-html");
            var subtemplates = new List<PageTemplate>();
            foreach (var entity in entities) {
                var subtemplate = this.Template.RecursivelyInsertHTMLContent(
                    template: null,
                    path: ContentNode.GetNodePathForEntity(entity),
                    onlyPublished: false,
                    includeChildren: true,
                    customName: entity.Name,
                    throwExceptionIfNotExists: false
                );
                subtemplates.Add(subtemplate);
                subtemplates.Add(this.Template.LoadTemplate("content-html.break"));
            }

            // Insert result
            this.Template.InsertTemplates("pages", subtemplates);
        }

        
        [RequestHandler("/admin/shop/products/export/translation", null, null, Verbs.Get, ContentType.Binary)]
        public void ExportTranslation() {
            // Init
            var source = this.Params.String("source");
            var target = this.Params.String("target");
            var entities = this.DB.Products()
                .OrderBy(e => e.Name)
                .ToList();
            var jsonRoot = new JArray();

            // Validate
            if (string.IsNullOrEmpty(source)) throw new BackendException("invalid-source", "Source must be specified.");
            if (string.IsNullOrEmpty(target)) throw new BackendException("invalid-target", "Target must be specified.");

            foreach (var entity in entities) {
                var json = ContentNode.ExportContentAsTranslationJSON(
                    db: this.DB, 
                    path: ContentNode.GetNodePathForEntity(entity),
                    sourceLanguage: source, 
                    targetLanguage: target,
                    customName: entity.Name+": ",
                    onlyPublished: false,
                    throwExceptionIfNotExists: false);
                if (json != null) {
                    foreach (var child in json) {
                        jsonRoot.Add(child);
                    }
                }
            }
            var pageName = "Products";
            var filename = Core.Config.ProjectID + "_" + pageName + "_Translation_"+source.ToUpper()+"_to_"+target.ToUpper()+".json";
            SendJSONAsFile(jsonRoot,filename);
        }

        [RequestHandler("/admin/shop/products/low-stock")]
        public void LowStockProductConfigurations() {
            var entities = this.DB.ProductConfigurations()
                .Include(nameof(ProductConfiguration.Product)).GetLowStockConfigurations()
                .OrderByDescending(e => e.Created);
            entities = this.Template.Filter(entities, this, nameof(Product.Name));
            entities = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: entities,
                form: new FormBuilder(Forms.EMPTY)
                    .Include(nameof(ProductConfiguration.SKU))
                    .Include(nameof(ProductConfiguration.StockQuantity))
                    .Include(nameof(ProductConfiguration.ProductId))
                    ,
                link: "/admin/shop/products/product/{entity.productid}"
            );

            // Navigation
            this.Navigation.Add("shop");
            this.Navigation.Add("products");
            this.Navigation.Add("low-stock");
        }


        [RequestHandler("/admin/shop/products/create")]
        public void ProductCreate() {
            this.RequireWriteARN();

            this.Template.InsertForm(
               variableName: "form",
               entity: new Product(),
               form: new FormBuilder(Forms.Admin.CREATE),
               apiCall: "/api/admin/shop/products/create",
               onSuccess: "/admin/shop/products/product/{product.public-id}"
            );
            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("products", "{text.products}");
            this.Navigation.Add("create", "{text.create}");
        }

        [RequestHandler("/admin/shop/products/product/{publicId}")]
        public void ProductView(string publicId) {
            var entity = DB.Products()
                .Include(nameof(Product.Configurations)  +"." + nameof(ProductConfiguration.Business))
                .GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            entity.LoadFirstLevelObjectReferences();

            var defaultConfiguration = entity.GetDefaultConfiguration();

            this.Template.InsertVariables("entity.default-configuration", defaultConfiguration);
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW), true, true);

            // Categories
            this.Template.InsertEntityList(
                variableName: "product.categories",
                entities:  entity.Categories.AsQueryable(),
                form:  new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/shop/categories/category/{entity.public-id}");

            // Variations
            this.Template.InsertEntityList("product.variations", entity.Variations.AsQueryable(), new FormBuilder(Forms.Admin.LISTING),"/admin/shop/products/product/{entity.public-id}");

            // Related
            this.Template.InsertEntityList("product.related", entity.Related.AsQueryable(), new FormBuilder(Forms.Admin.LISTING), "/admin/shop/products/product/{entity.public-id}");

            // Configuration
            this.Template.InsertEntityList(
                variableName: "entity.configurations",
                entities: entity.Configurations.AsQueryable(), 
                form: new FormBuilder(Forms.Admin.LISTING).Exclude(nameof(ProductConfiguration.ProductId)), 
                link: "/admin/shop/products/product/" + entity.PublicId + "/configuration/{entity.public-id}");

            // Information
            //TODO: @dan: what if it is not a nutrition table?
            this.Template.InsertProductInformationTable(entity.Information,"entity.information-table","information-table","preview");
            this.Template.InsertVariable("entity.information-type", entity.Information?.Type.GetEnumDescription());

            // Product Updater
            this.Template.InsertVariable("product-updater.name", ProductUpdater.Updater?.Name);
            this.Template.InsertVariable("product-updater.available", ProductUpdater.Updater != null);

            // Label selection (entity.Details does have defined 'Label' in ShopProductDetailsCustomKeys)
            if (Shop.Config.ShopProductPackagingTemplates.Any() && Core.Config.GetStringListSetting("ShopProductDetailsCustomKeys").Contains(Product.PRODUCT_DETAILS_LABEL_KEY) == true) {
                this.Template.InsertVariableBool("label-template-selection.available", true);
                // Additional label templates?
                this.Template.InsertJSON("packagingTemplates", Product.GetProductTemplates());


            } else {
                this.Template.InsertVariableBool("label-template-selection.available", false);

                this.Template.InsertJSON("PackagingTemplates", null);
            }

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("products", "{text.products}");
            this.Navigation.Add($"product/{publicId}", entity.Name);
        }

        [RequestHandler("/admin/shop/products/product/{publicId}/edit")]
        public void ProductEdit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Products().GetByPublicId(publicId);
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/shop/product/{entity.PublicId}/edit",
                onSuccess: $"/admin/shop/products/product/{entity.PublicId}"
            );

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("products", "{text.products}");
            this.Navigation.Add($"product/{publicId}", entity.Name);
            this.Navigation.Add("edit", "{text.edit}", "{text.edit} " + entity.Name);
        }
        
        [RequestHandler("/admin/shop/products/product/{publicId}/categories")]
        public void ProductCategories(string publicId) {
            var entity = DB.Products().GetByPublicId(publicId);
            entity.Include("Categories");
            this.Template.InsertVariables("entity", entity);
            
            // Categories
            this.Template.InsertSelectionList("categories", DB.Categories(), entity.Categories, new FormBuilder(Forms.Admin.SELECTION), $"/api/admin/shop/product/{entity.PublicId}/categories/" + "{entity.public-id}/toggle");

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("products", "{text.products}");
            this.Navigation.Add($"product/{publicId}", entity.Name);
            this.Navigation.Add("categories", "{text.categories}");
        }

        [RequestHandler("/admin/shop/products/product/{publicId}/variations")]
        public void ProductVariations(string publicId) {
            var entity = DB.Products().GetByPublicId(publicId);
            entity.Include(nameof(Product.Variations));
            this.Template.InsertVariables("entity", entity);

            // Product Variations
            this.Template.InsertSelectionList("variations", DB.Products().Where(p=> p.Id != entity.Id), entity.Variations, new FormBuilder(Forms.Admin.SELECTION), $"/api/admin/shop/product/{entity.PublicId}/variations/" + "{entity.public-id}/toggle");

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("products", "{text.products}");
            this.Navigation.Add($"product/{publicId}", entity.Name);
            this.Navigation.Add("variations");
        }

        [RequestHandler("/admin/shop/products/product/{publicId}/related")]
        public void ProductRelated(string publicId) {
            var entity = DB.Products().GetByPublicId(publicId);
            entity.Include(nameof(Product.Related));
            this.Template.InsertVariables("entity", entity);

            // Related Products
            this.Template.InsertSelectionList("related", DB.Products().Where(p => p.Id != entity.Id), entity.Related, new FormBuilder(Forms.Admin.SELECTION), $"/api/admin/shop/product/{entity.PublicId}/related/" + "{entity.public-id}/toggle");

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("products", "{text.products}");
            this.Navigation.Add($"product/{publicId}", entity.Name);
            this.Navigation.Add("related");
        }

        [RequestHandler("/admin/shop/products/product/{publicId}/information/preview")]
        public void ProductInformationPreview(string publicId) {
            var entity = DB.Products().GetByPublicId(publicId);
          
            // Variables
            this.Template.InsertProductInformationTable(entity.Information,"entity.information-table","information-table","preview margined");
            this.Template.InsertVariable("entity.information-type", entity.Information?.Type.GetEnumDescription());
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("products", "{text.products}");
            this.Navigation.Add($"product/{publicId}", entity.Name);
            this.Navigation.Add("information/preview","{text.preview} "+entity.Information?.Type.GetEnumDescription());
        }

        [RequestHandler("/admin/shop/products/product/{publicId}/information/edit")]
        public void ProductInformationEdit(string publicId) {
            var entity = DB.Products().GetByPublicId(publicId);

            // Insert an empty dummy table
            if (entity.Information == null || entity.Information.Data == null) {
                // Dummy 
                var productInfo = new ProductInformation();
                productInfo.Type = ProductInformation.ProductInfoType.NutritionTable;
                var empty = new ProductInfoGroup();
                empty.Name = "New Per 100gm Group";
                empty.AddItem(",Gram100:");
                productInfo.AddData(empty);
                this.Template.InsertProductInformationTable(productInfo, "entity.information-table", "information-table", "table editable");
            } else {
                this.Template.InsertProductInformationTable(entity.Information, "entity.information-table", "information-table", "table editable");
            }



            // Variables
            this.Template.InsertVariable("entity.information-type", entity.Information?.Type.GetEnumDescription());
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("products", "{text.products}");
            this.Navigation.Add($"product/{publicId}", entity.Name);
            this.Navigation.Add("information/edit","{text.edit} "+entity.Information?.Type.GetEnumDescription());
        }

     

        //[RequestHandler("/admin/shop/products/product/{publicId}/configurations")]
        //public void ProductConfigurations(string publicId) {
        //    var entity = DB.Products().Include("Configurations.Business").GetByPublicId(publicId);
        //    this.Template.InsertVariables("entity", entity);

        //    // Configurations
        //    this.Template.InsertEntityList("configurations", entity.Configurations.AsQueryable(), new FormBuilder(Forms.Admin.LISTING) , $"/admin/shop/products/product/{entity.PublicId}" + "configurations/configuration/{entity.public-id}");
            
        //    // Navigation
        //    this.Navigation.Add("shop", "{text.shop}");
        //    this.Navigation.Add("products", "{text.products}");
        //    this.Navigation.Add($"product/{publicId}", entity.Name);
        //    this.Navigation.Add("configurations");
        //}


        [RequestHandler("/admin/shop/products/product/{productId}/configuration/{publicId}/edit")]
        public void ConfigurationEdit(string productId, string publicId) {
            this.RequireWriteARN();


            var product = DB.Products().Include(nameof(Product.Configurations)).GetByPublicId(productId);

            var configuration = product.Configurations.AsQueryable().GetByPublicId(publicId);


            configuration.LoadFirstLevelNavigationReferences();
            configuration.LoadFirstLevelObjectReferences();

            FormBuilder form = GetAdminEditForm(configuration);

            this.Template.InsertForm(
               variableName: "form",
               entity: configuration,
               form: form,
               apiCall: "/api/admin/shop/product-configuration/" + configuration.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

            // Business
            //  this.Template.InsertSelectionList("price.business", DB.Businesses().Where(b=>!b.IsOwner), price.Business, new FormBuilder(Forms.Admin.SELECTION), "/api/admin/shop/price/" + price.PublicId + "/business/{entity.public-id}/toggle");

            // Price
            this.Template.InsertVariables("entity", configuration);

            // Product
            this.Template.InsertVariables("product", product);

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("products", "{text.products}");
            this.Navigation.Add($"product/{product.PublicId}", product.Name);
            var businessTitle = configuration.Business != null ? configuration.Business.Name + ": " : string.Empty;
            this.Navigation.Add($"configuration/{configuration.PublicId}", "{text.configuration}: " + configuration.Name);
            this.Navigation.Add($"edit", "{text.edit}");
        }

        public static FormBuilder GetAdminEditForm(ProductConfiguration configuration) {
            var form = new FormBuilder(Forms.Admin.EDIT);
            if (configuration.IsDefault) {
                form.Exclude(nameof(configuration.Business));
            }

            return form;
        }

        [RequestHandler("/admin/shop/products/product/{productId}/configuration/{publicId}")]
        public void ConfigurationView(string productId, string publicId) {
            var configuration = DB.ProductConfigurations().GetByPublicId(publicId);
            var product = DB.Products().GetByPublicId(productId);

            this.Template.InsertVariables("entity", configuration);
            configuration.LoadFirstLevelNavigationReferences();
            configuration.LoadFirstLevelObjectReferences();

            if (!product.Configurations.Contains(configuration)) {
                throw new BackendException("wrong-configuration", "This product does not contain the given configuration ");
            }

            var form = new FormBuilder(Forms.Admin.VIEW);
            if (configuration.IsDefault) {
                form.Exclude(nameof(ProductConfiguration.Business));
            }

            this.Template.InsertPropertyList("entity", configuration,form);

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("products");
            this.Navigation.Add($"product/{productId}", product.Name);
            var businessTitle = configuration.Business != null ? configuration.Business.Name + ": " : string.Empty;
            this.Navigation.Add($"configuration/{configuration.PublicId}", "{text.configuration}: " + configuration.Name);
        }



        [RequestHandler("/admin/shop/products/product/{productId}/add-configuration")]
        public void AddConfiguration(string productId) {

            var product = DB.Products().GetByPublicId(productId);
            this.Template.InsertForm(
                variableName: "form",
                entity: new ProductConfiguration(),
                form: new FormBuilder(Forms.Admin.CREATE),
                apiCall: $"/api/admin/shop/product/{product.PublicId}/add-configuration",
                onSuccess: $"/admin/shop/products/product/{product.PublicId}/configuration/" + "{entity.public-id}/edit"
                );




            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("products");
            this.Navigation.Add($"product/{productId}", product.Name);
            this.Navigation.Add($"add-configuration");
        }


        [RequestHandler("/admin/shop/products/pricing-table")]
        public void PricingTable() {

            // Business
            var businesses = DB.Businesses().Where(b => b.IsOwner == false).ToList();

            var businessHeaders = new List<PageTemplate>();
            // Heading Business
            foreach(var business in businesses) {
                var template = this.Template.LoadTemplate("pricing-table.th-business");
                template.InsertVariable("name", business.Name);
                businessHeaders.Add(template);
            }
        
            var productTemplates = new List<PageTemplate>();

            // OLD
            //var products =  DB.Products()
            //    .Include(nameof(Product.Configurations)) .ToList();


            var products = this.DB.Products()
                  .Include(nameof(Shop.Model.Product.Configurations) + "." + nameof(ProductConfiguration.Business))
                  .Include(nameof(Shop.Model.Product.Configurations) + "." + nameof(ProductConfiguration.Product)).ToList();

            foreach (var product in products) {

                var defaultConfiguration = product.GetDefaultConfiguration();
                var template = this.Template.LoadTemplate("pricing-table.tr-product");
                template.InsertVariables("product", product);
                template.InsertVariables("configuration", defaultConfiguration);

                //template.InsertVariable("confiu.retail-price", defaultConfiguration.RetailPrice);
                //template.InsertVariable("product.customer-price", defaultConfiguration.CustomerPrice);
                //template.InsertVariable("product.retail-vat-rate", defaultConfiguration.RetailVATRate);
                //template.InsertVariable("product.customer-vat-rate", defaultConfiguration.CustomerVATRate);
                //template.InsertVariable("product.internal-price", defaultConfiguration.InternalPrice);

                // Business prices
                var retailTemplates = new List<PageTemplate>();
                var customerTemplates = new List<PageTemplate>();
                var internalPricesTemplates = new List<PageTemplate>();
                var retailVatTemplates = new List<PageTemplate>();
                var customerVatTemplates = new List<PageTemplate>();
                var skuTemplates = new List<PageTemplate>();
                foreach (var business in businesses) {
                    var configuration = product.GetBusinessConfiguration(business);
                    InsertPriceTemplate(retailTemplates, configuration, configuration?.RetailPrice);
                    InsertPriceTemplate(customerTemplates, configuration, configuration?.CustomerPrice);
                    InsertPriceTemplate(internalPricesTemplates, configuration, configuration?.InternalPrice);
                    InsertPercentageTemplate(retailVatTemplates, configuration, configuration?.RetailVATRate);
                    InsertPercentageTemplate(customerVatTemplates, configuration, configuration?.CustomerVATRate);
                    InsertTextTemplate(skuTemplates, configuration, configuration?.SKU);
                }

                template.InsertTemplates("business-prices.internal-price", retailTemplates);
                template.InsertTemplates("business-prices.retail", retailTemplates);
                template.InsertTemplates("business-prices.customer", customerTemplates);
                template.InsertTemplates("business-prices.retail-vat", retailVatTemplates);
                template.InsertTemplates("business-prices.customer-vat", customerVatTemplates);
                template.InsertTemplates("business-prices.sku", customerVatTemplates);

                productTemplates.Add(template);
            }

            this.Template.InsertTemplates("businesses", businessHeaders);
            this.Template.InsertTemplates("products", productTemplates);
            this.Template.InsertVariable("internal-price.colspan", businesses.Count + 1);

            // Navigation
            this.Navigation.Add("shop", "{text.shop}");
            this.Navigation.Add("products", "{text.products}");
            this.Navigation.Add($"pricing-table");
          
        }

        private void InsertPriceTemplate(List<PageTemplate> retailTemplates, ProductConfiguration configuration, Price price ) {
            var retailTemplate = this.Template.LoadTemplate("pricing-table.td-business-price");
            retailTemplate.InsertVariable("price", price);
            retailTemplate.InsertVariable("price.enabled", configuration != null && price!= null && price.HasValue ? "enabled" : "unpublished");
            retailTemplates.Add(retailTemplate);
        }

        private void InsertPercentageTemplate(List<PageTemplate> retailTemplates, ProductConfiguration configuration, decimal? percentage) {
            var retailTemplate = this.Template.LoadTemplate("pricing-table.td-business-price");
            retailTemplate.InsertVariable("price", percentage.HasValue ?string.Format("{0:P2}",percentage): null);
            retailTemplate.InsertVariable("price.enabled", configuration != null && percentage != null && percentage.HasValue ? "enabled" : "unpublished");
            retailTemplates.Add(retailTemplate);
        }

        private void InsertTextTemplate(List<PageTemplate> retailTemplates, ProductConfiguration configuration, string text) {
            var retailTemplate = this.Template.LoadTemplate("pricing-table.td-business-price");
            retailTemplate.InsertVariable("price", text);
            retailTemplate.InsertVariable("price.enabled", configuration != null && ! string.IsNullOrEmpty(text)? "enabled" : "unpublished");
            retailTemplates.Add(retailTemplate);
        }
    }
}
