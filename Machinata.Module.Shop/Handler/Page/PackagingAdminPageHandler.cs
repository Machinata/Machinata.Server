using System.Linq;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Module.Admin.Handler;
using System.Collections.Generic;

using Machinata.Core.Templates;
using Machinata.Module.Shop.Model;
using System.Collections;
using System;
using Machinata.Core.TaskManager;

namespace Machinata.Module.Shop.Handler {


    public class PackagingAdminPageHandler: AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("shop");
        }

        #endregion

             
        [RequestHandler("/admin/shop/packaging")]
        public void PackagingDesigns() {

            // Designs
            var entities = DB.PackagingDesigns().AsQueryable();
            entities = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: entities,
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/shop/packaging/design/{entity.public-id}"
            );

            // Configurations
            var configurations = this.DB.PackagingConfigurations()
                         .Include(nameof(PackagingConfiguration.Business));

            this.Template.InsertEntityList(
                   variableName: "configurations",
                   entities: configurations,
                   link: "/admin/shop/packaging/configuration/{entity.public-id}");

            this.Navigation.Add("shop");
            this.Navigation.Add("packaging");
        }

        [RequestHandler("/admin/shop/packaging/create-design")]
        public void PackagingDesignsCreate() {

            var entity = new PackagingDesign();
            this.Template.InsertForm(
                variableName: "entity",
                entity: entity,
                form: new FormBuilder(Forms.Admin.CREATE),
                apiCall: "/api/admin/shop/packaging/designs/create",
                onSuccess: "/admin/shop/packaging/design/{packaging-design.public-id}"
            );

            this.Navigation.Add("shop");
            this.Navigation.Add("packaging");
            this.Navigation.Add("create", "{text.create} " + "{text.design}");
        }

        [RequestHandler("/admin/shop/packaging/design/{publicId}")]
        public void PackagingDesignView(string publicId) {

            var entity = DB.PackagingDesigns().GetByPublicId(publicId);
            this.Template.InsertPropertyList(
                variableName: "entity",
                entity: entity,
                form: new FormBuilder(Forms.Admin.VIEW),
                loadFirstLevelReferences: true,
                includeNavigationProperties: true
            );

            this.Template.InsertVariables("entity",entity);
            this.Template.InsertVariables("entity", entity);

            this.Navigation.Add("shop");
            this.Navigation.Add("packaging");
            this.Navigation.Add("design/" + publicId,"{text.design}: " +  entity.ShortURL);
        }

        [RequestHandler("/admin/shop/packaging/design/{publicId}/edit")]
        public void PackagingDesignEdit(string publicId) {

            var entity = DB.PackagingDesigns().GetByPublicId(publicId);
            this.Template.InsertForm(
                variableName: "entity",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/shop/packaging/design/{publicId}/edit",
                onSuccess: $"/admin/shop/packaging/design/{publicId}"
            );

            this.Navigation.Add("shop");
            this.Navigation.Add("packaging");
            this.Navigation.Add("design/" + publicId, "{text.design}: " + entity.ShortURL);
            this.Navigation.Add("edit");
        }

        [RequestHandler("/admin/shop/packaging/configuration/{publicId}")]
        public void PackagingDesignConfigurationView(string publicId) {

            var entity = DB.PackagingConfigurations().Include(nameof(PackagingConfiguration.Business)).GetByPublicId(publicId);
            this.Template.InsertPropertyList(
                variableName: "entity",
                entity: entity,
                form: new FormBuilder(Forms.Admin.VIEW),
                loadFirstLevelReferences: true,
                includeNavigationProperties: true
            );

            this.Template.InsertVariables("entity", entity);
            this.Template.InsertVariables("entity.business", entity.Business);

            this.Navigation.Add("shop");
            this.Navigation.Add("packaging");
            this.Navigation.Add("configuration/" + publicId, "{text.configuration}: " + entity.Business.Name);
        }

        [RequestHandler("/admin/shop/packaging/configuration/{publicId}/edit")]
        public void PackagingDesignConfigurationEdit(string publicId) {

            var entity = DB.PackagingConfigurations().Include(nameof(PackagingConfiguration.Business)).GetByPublicId(publicId);
            this.Template.InsertForm(
                variableName: "entity",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/shop/packaging/configuration/{publicId}/edit",
                onSuccess: $"/admin/shop/packaging/configuration/{publicId}"
            );

            this.Navigation.Add("shop");
            this.Navigation.Add("packaging");
            this.Navigation.Add("configuration/" + publicId, "{text.configuration}: " + entity.Business.Name);
            this.Navigation.Add("edit");
        }

        [RequestHandler("/admin/shop/packaging/create-configuration")]
        public void PackagingDesignsConfigurationCreate() {

            var businessesWithConfigs = this.DB.PackagingConfigurations().Include(nameof(PackagingConfiguration.Business)).Select(pc => pc.Business).Distinct().ToList();
            var unassignedBusinesses = this.DB.CustomerBusinesses().ToList().Except(businessesWithConfigs);
            this.Template.InsertSelectionList(
                variableName: "businesses",
                entities: unassignedBusinesses.AsQueryable(),
                selectedEntity: null,
                form: new FormBuilder(Forms.EMPTY).Include(nameof(Business.Name))
            );

            this.Navigation.Add("shop");
            this.Navigation.Add("packaging");
            this.Navigation.Add("create", "{text.create} " + "{text.configuration}");
        }


        [RequestHandler("/admin/shop/packaging/labels")]
        public void PackagingLabels() {

            // Orders
            var entities = Order.GetPendingOrders(this.DB, loadBusiness: true).OrderByDescending(o=>o.DeliveryDate);
            entities = this.Template.Paginate(entities, this);
                this.Template.InsertSelectionList(
                variableName: "entity-list",
                entities: entities,
                form: new FormBuilder(Forms.Admin.SELECTION),
                selectedEntities:  new List<ModelObject>(),
                selectionAPICall: null,
                //loadFirstLevelReferences: true,
                enableAutoSortAndFilters: true
            );

            // Date
            this.Template.InsertVariable("date", Core.Util.Time.ConvertToDefaultTimezone(DateTime.UtcNow).ToString(Core.Config.DateFormat));
                    
            this.Navigation.Add("shop");
            this.Navigation.Add("packaging");
            this.Navigation.Add("labels");
           
        }


        [RequestHandler("/admin/shop/packaging/labels/print-remote")]
        public void PackagingLabelsPrintRemote() {

            var date = this.Params.DateTime("date", DateTime.UtcNow);

            // Selected Orders
            var selectedItems = this.Params.StringArray("selected", new string[] { });
            var orders = this.DB.Orders().Include(nameof(Order.OrderItems)).GetByPublicIds(selectedItems);

            // Selected Items
            var orderItems = orders.SelectMany(o => o.OrderItems);
            this.Template.InsertTemplates("labels", orderItems, "print-remote.label", InsertLabelsVariables);

            // Targets
            var targets = Core.TaskManager.RemoteTaskManagerAPI.GetRemoteTaskTargetsConfig(true).ToList();

            this.Template.InsertSelectionList(
                variableName: "targets", 
                entities: targets.AsQueryable(),
                form: new FormBuilder(Forms.EMPTY).Include(nameof(RemoteTaskManagerAPI.RemoteTaskTarget.Title)),
                selectedEntity: targets.SingleOrDefault(t=>t.Name == "*")
             );

            // Date
            this.Template.InsertVariable("date", date.ToString(Core.Config.DateFormat));
            this.Template.InsertVariable("params", "?" + this.Context.Request.QueryString);

            this.Navigation.Add("shop");
            this.Navigation.Add("packaging");
            this.Navigation.Add("labels");
            this.Navigation.Add("print");

        }

        [RequestHandler("/admin/shop/packaging/labels/print")]
        public void PackagingLabelsPrint() {

            var date = this.Params.DateTimeGlobalConfigFormat("date", DateTime.UtcNow);

            // Selected Orders
            var selectedItems = this.Params.StringArray("selected", new string[] { });
            var orders = this.DB.Orders().Include(nameof(Order.OrderItems)).GetByPublicIds(selectedItems);

            // Selected Items
            var orderItems = orders.SelectMany(o => o.OrderItems).ToList();
            this.Template.InsertTemplates("labels", orderItems, "print.label", InsertLabelsVariables);

            // Additional label templates?
            this.Template.InsertJSON("packagingTemplates", Product.GetProductTemplates());


            // Date
            this.Template.InsertVariable("date", date.ToString(Core.Config.DateFormat));
            this.Template.InsertVariable("params", "?" + this.Context.Request.QueryString);

            this.Navigation.Add("shop");
            this.Navigation.Add("packaging");
            this.Navigation.Add("labels");
            this.Navigation.Add("print");

        }




        private void InsertLabelsVariables(OrderItem orderItem, PageTemplate template) {
            orderItem.Include(nameof(OrderItem.Order));
            orderItem.Include(nameof(OrderItem.Product));
            orderItem.Order.Include(nameof(orderItem.Order.Business));
            template.InsertVariables("business", orderItem.Order.Business);
            template.InsertVariables("order", orderItem.Order);
            template.InsertVariables("order-item", orderItem);
            template.InsertVariables("product", orderItem.Product);
        }

        private void InsertTargetVariables(RemoteTaskManagerAPI.RemoteTaskTarget target, PageTemplate template) {
            template.InsertVariable("test", target.Name);
        }
    }
}
