
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Module.Shop.Model;
using Machinata.Core.Builder;
using System.Collections.Generic;
using Machinata.Core.Exceptions;
using System.Linq;

namespace Machinata.Module.Shop.Handler {


    public class CouponShopApiHandler : Module.Admin.Handler.AdminAPIHandler {
        
        [RequestHandler("/api/admin/shop/coupon/{publicId}/delete")]
        public void CouponDelete(string publicId) {
            this.RequireWriteARN();

            // Make changes and save          
            var entity = this.DB.OrderCoupons().GetByPublicId(publicId);

            //what happens to orders which applied this coupon? -> not editable
            CheckDeleteUpdate(entity);

            this.DB.DeleteEntity(entity);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success");
        }

        private static void CheckDeleteUpdate(OrderCoupon entity) {
            entity.Include(nameof(entity.Orders));
            if (entity.Orders.Count > 0) {
                throw new BackendException("coupon-used", "This coupon can't be changed, there are orders which applied this order.");
            }
        }

        [RequestHandler("/api/admin/shop/coupons/create")]
        public void CouponCreate() {
            this.RequireWriteARN();
            
            // Validations
            var coupon = new OrderCoupon();
            coupon.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            coupon.Validate();

            // If we provide a code, make sure it is unique
            if (!string.IsNullOrEmpty(coupon.Code)) {
                var matchingCoupons = this.DB.OrderCoupons().Where(c => c.Enabled == true && c.Code.ToLower() == coupon.Code.ToLower()).Count();
                if (matchingCoupons > 0) throw new BackendException("invalid-code", $"The code {coupon.Code} has already been used.");
            }

            // Register and save
            this.DB.OrderCoupons().Add(coupon);
            this.DB.SaveChanges();

            // Do we need a code? We can only do this after registering and saving, as we need the db id
            if (string.IsNullOrEmpty(coupon.Code)) {
                coupon.Code = OrderCoupon.GenerateCouponCodeForId(coupon.Id);
                DB.SaveChanges();
            }

            SendAPIMessage("create-coupon-success", new {
                Coupon = new {
                    PublicId = coupon.PublicId
                }
            });
        }
        
        [RequestHandler("/api/admin/shop/coupon/{publicId}/edit")]
        public void CouponEdit(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var entity = this.DB.OrderCoupons().GetByPublicId(publicId);

            //Don't allow change if is already applied
            CheckDeleteUpdate(entity);

            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                Coupon = new {
                    PublicId = entity.PublicId
                }
            });
        }

    }
}
