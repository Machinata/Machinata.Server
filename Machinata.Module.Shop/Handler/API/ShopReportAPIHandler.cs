﻿using System.Collections.Generic;
using System.Linq;



using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Module.Admin.Handler;
using Newtonsoft.Json.Linq;
using Machinata.Module.Reporting.Model;
using Machinata.Module.Shop.Model;

namespace Machinata.Module.Shop.Handler {


    public class ShopReportAPIHandler : AdminAPIHandler {


        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("shop");
        }

        #endregion

        [RequestHandler("/api/admin/shop/reporting/map/deliveries")]
        public void MapShopDeliveriesToday() {

            var start = Core.Util.Time.GetDefaultTimezoneToday().ToUniversalTime();
            var end = start.AddDays(1);

            var orders = this.DB.Orders().Include("ShippingAddress")
                .Where(Order.HasOrderDate).Where(o => o.Status == Order.StatusType.Confirmed || o.Status == Order.StatusType.Fulfilled)
                .Where(Order.Between(start,end))
                .OrderByDescending(o => o.Id);
            var node = new OnlineMapNode();
            Dashboard.ShopDashboard.AddOrdersToMap(this.Language, node, orders);

            this.SendAPIMessage("node-data", JObject.FromObject(node));

        }

        [RequestHandler("/api/admin/shop/reporting/timeseries/orders/count")]
        public void TimeSeriesShopOrders() {

            //var data = new TimeSeriesChartData();
             var node = new LineColumnNode();
            node.RandomID();
            node.SetTitle("Orders");
            node.SetSubTitle("per Day");
            node.Theme = "default";
            var group = node.AddSerieGroup();
            group.XAxisDays();
            //            group.YAxisNumberThousandsInteger();
            //groupReporting.Config.NumberThousandsFormatD3;
            group.YAxisNumberThousands();


            var end = Core.Util.Time.GetDefaultTimezoneToday().EndOfDay().ToUniversalTime();
            var start = end.AddDays(-14);
           

            var entities = this.DB.Orders().ConfirmedWithDates()
                 .Where(Order.Between(start, end));

            var serie = Reporting.Logic.CostsOverTimeReports.CreateDailyCostsSerie(entities.ToList().AsQueryable(), "Orders", c => c.ConfirmedDate, c => 1m, false);
            group.AddColumnSerie(serie);

            this.SendAPIMessage("node-data", JObject.FromObject(node));
        }

        [RequestHandler("/api/admin/shop/repporting/donut/products/sales")]
        public void DonutShopProductsSales() {
            var report = new Report();


            var filterQuery = this.Params.String("filter-query");
            IQueryable<OrderItem> orderItems = this.DB.OrderItems();

            if (!string.IsNullOrEmpty(filterQuery)) {
                foreach (var filter in filterQuery.Split(' ')) {
                    orderItems = orderItems.Where(oi => oi.Product.Name.Contains(filter)); // AND
                }
            }
            var node = Dashboard.ShopDashboard.TopSellersDonutChart(orderItems);
            report.AddToBody(node);
            this.SendAPIMessage("node-data", JObject.FromObject(report));

        }

        [RequestHandler("/api/admin/shop/reporting/donut/categories/sales/{type}")]
        public void DonutShopCategories(string type) {
            var categoryType = EnumHelper.ParseEnum<Category.CategoryTypes>(type, true, Category.CategoryTypes.Main);
            var report = new Report();
            var orderItems = this.DB.OrderItems().Include("Product.Categories");
            var node = Dashboard.ShopDashboard.CategoriesDonutChart(this.DB, orderItems, categoryType);
            report.AddToBody(node);
            this.SendAPIMessage("node-data", JObject.FromObject(report));
        }



    }

}
