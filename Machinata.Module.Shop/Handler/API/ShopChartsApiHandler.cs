using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Module.Shop.Model;
using Machinata.Core.Charts;
using Machinata.Core.Builder;
using System.Linq;
using System;

namespace Machinata.Module.Shop.Handler {
    
    public class ShopChartsApiHandler : Module.Admin.Handler.AdminAPIHandler {
        

        [RequestHandler("/api/admin/shop/chart/donut/categories/sales/{type}")]
        public void DonutShopCategories(string type) {

            var categoryType = EnumHelper.ParseEnum<Category.CategoryTypes>(type, true, Category.CategoryTypes.Main);
            
            var data = new DonutChartData();
            data.Title = "Categories";
            foreach(var cat in this.DB.Categories().Where(c=>c.CategoryType == categoryType)) {
                var count = this.DB.OrderItems().Include("Product.Categories").Count(oi => oi.Product.Categories.Select(c=>c.Id).Contains(cat.Id));
                data.Items.Add(new DonutChartDataItem() { Name = cat.Name, Title = cat.Name, Value = count });
            }
            data.UpdatePercentages();
            SendAPIMessage("chart-data", data);
        }

        [RequestHandler("/api/admin/shop/chart/donut/products/sales")]
        public void DonutShopProductsSales() {
            // Note: this method implements a filter query to mimick that of the entity list
            // on the Admin/Shop/Products page
            var filterQuery = this.Params.String("filter-query");
            var data = new DonutChartData();
            data.Title = "Top Sellers";
            IQueryable<OrderItem> orderItems = this.DB.OrderItems();
            if(!string.IsNullOrEmpty(filterQuery)) {
                foreach (var filter in filterQuery.Split(' ')) {
                    orderItems = orderItems.Where(oi => oi.Product.Name.Contains(filter)); // AND
                }
            }
            var products = orderItems.GroupBy(oi => oi.Product).Select(g => new { Name = g.Key.Name, Count = g.Count() });
            products = products.OrderByDescending(g => g.Count).Take(10);
            foreach(var p in products) {
                data.Items.Add(new DonutChartDataItem() { Name = p.Name, Title = p.Name, Value = p.Count });
            }
            data.UpdatePercentages();
            SendAPIMessage("chart-data", data);
        }

        [RequestHandler("/api/admin/shop/chart/donut/orders/business-revenues")]
        public void DonutShopOrdersBusinessRevenues() {
            
            var data = new DonutChartData();
            data.Title = "Top Businesses";
            //var businesses = this.DB.Businesses().Select(b => new { Name = b.Name, Revenue = this.DB.Orders().Where(o => o.Business == b).ToList().Sum(o => o.TotalCustomer.Cents.Value)/100.0 });
            //businesses = businesses.OrderByDescending(g => g.Revenue).Take(10);
            var businesses = this.DB.Businesses().Where(b => b.IsOwner == false).ToList();
            foreach(var b in businesses) {
                var rev = this.DB.Orders().Where(o => o.Business.Id == b.Id).ToList().Sum(o => o.TotalCustomer.Value.Value);
                data.Items.Add(new DonutChartDataItem() { Name = b.Name, Title = b.Name, Value = (int)rev });
            }
            data.UpdatePercentages();
            SendAPIMessage("chart-data", data);
        }

        [RequestHandler("/api/admin/shop/chart/timeseries/orders/count")]
        public void TimeSeriesShopOrders() {

            var data = new TimeSeriesChartData();
            data.XAxisIsTime = false;
            AddOrderCountData(data);
            SendAPIMessage("chart-data", data);
        }

        private void AddOrderCountData(TimeSeriesChartData data) {
            data.Series.Add(TimeSeries.GetTimeSeriesForEntitiesWithDelegates<Order>(
                 handler: this,
                 entities: this.DB.Orders().ConfirmedWithDates(),
                 entitiesForRange: delegate (IQueryable<Order> entities, DateTime start, DateTime end) { return entities.ToList().Where(o => o.OrderDate >= start && o.OrderDate < end).AsQueryable(); },
                 valueForEntities: TimeSeries.ValueForEntitiesOnCount,
                 title: "Orders"
             ));
        }

        [RequestHandler("/api/admin/shop/chart/timeseries/orders-and-items/count")]
        public void TimeSeriesShopOrderAndItems() {
            
            var data = new TimeSeriesChartData();
       
            data.XAxisIsTime = false;
            AddOrderCountData(data);
            data.Series.Add(TimeSeries.GetTimeSeriesForEntitiesWithDelegates<OrderItem>(
                handler: this,
                entities: this.DB.Orders().Include(nameof(Order.OrderItems)).ConfirmedWithDates().AsQueryable().SelectMany(o=>o.OrderItems).AsQueryable(),
                entitiesForRange: TimeSeries.EntitiesForRangeOnCreated,
                valueForEntities: delegate(IQueryable<OrderItem> entities) {
                    return entities.ToList().Sum(e => e.Quantity); //TODO: why is ToList required?
                },
                title: "Items"
            ));
            SendAPIMessage("chart-data", data);
        }

        [RequestHandler("/api/admin/shop/chart/timeseries/order-deliveries/count")]
        public void TimeSeriesShopOrderDeliveries() {
            
            var data = new TimeSeriesChartData();
            data.XAxisIsTime = false;
            data.Series.Add(TimeSeries.GetTimeSeriesForEntitiesWithDelegates(
                handler: this,
                entities: this.DB.Orders(),
                entitiesForRange: delegate(IQueryable<Order> entities, DateTime start, DateTime end) {
                    return entities.Where(e => e.DeliveryDate > start && e.DeliveryDate < end);
                },
                valueForEntities: TimeSeries.ValueForEntitiesOnCount
            ));
            SendAPIMessage("chart-data", data);
        }

        [RequestHandler("/api/admin/shop/chart/timeseries/orders/revenue-over-time")]
        public void TimeSeriesRevenueOverTime() {
            
            var data = new TimeSeriesChartData();
            data.XAxisIsTime = false;
            data.Format = Core.Config.CurrencyFormat;
            data.Metric = Core.Config.CurrencyDefault;
            var currencies = this.DB.Orders().Select(o => o.Currency).Distinct();
            //TODO
            foreach(var currency in currencies) {
                data.Series.Add(TimeSeries.GetTimeSeriesForEntitiesWithDelegates(
                    handler: this,
                    entities: this.DB.Orders().Where(o => o.Currency == currency),
                    entitiesForRange: TimeSeries.EntitiesForRangeOnCreated,
                    valueForEntities: delegate (IQueryable<ModelObject> entities) {
                        // todo why toList necessary? - theres a problem with the Price class i guess
                        return (double)entities.Cast<Order>().Where(o => o.Status == Order.StatusType.Confirmed).ToList().Sum(e => e.TotalCustomer.Value.Value);
                    },
                    title: "Order Revenue " + currency
                ));
            }
            
           
            SendAPIMessage("chart-data", data);
        }

        

        [RequestHandler("/api/admin/shop/chart/timeseries/orders/business-revenues")]
        public void TimeSeriesBusinessRevenues() {
            
            var data = new TimeSeriesChartData();
            data.XAxisIsTime = false;
            data.Format = Core.Config.CurrencyFormat;
            foreach (var business in DB.Businesses().Where(b => b.IsOwner == false)) {
                data.Series.Add(TimeSeries.GetTimeSeriesForEntitiesWithDelegates(
                    handler: this,
                    entities: this.DB.Orders(),
                    entitiesForRange: delegate (IQueryable<ModelObject> entities, DateTime start, DateTime end) {
                        return ((IQueryable<Order>)entities).Where(e => e.Created > start && e.Created < end).ToList().Where(e=>e.Business == business).AsQueryable();
                    },
                    valueForEntities: delegate (IQueryable<ModelObject> entities) {
                         return (double)entities.Select(e => e as Order).Where(o => o.Status == Order.StatusType.Confirmed).ToList().Sum(e => e.TotalCustomer.Value.Value);
                    },
                    title: business.Name
                ));
            }

            SendAPIMessage("chart-data", data);
        }

        [RequestHandler("/api/admin/shop/chart/map/deliveries")]
        public void MapShopDeliveries() {
            var orders = this.DB.Orders().Include("ShippingAddress").Where(Order.HasOrderDate).Where( o=> o.Status == Order.StatusType.Confirmed || o.Status == Order.StatusType.Fulfilled).OrderByDescending(o=>o.Id);
            var data = GetOrderMapChart(this.DB, orders,false, this.Params.String("timespan", null) );
            SendAPIMessage("chart-data", data);

        }
        

        [RequestHandler("/api/admin/shop/chart/map/fulfilled")]
        public void MapShopFullfilled() {
            //TODO
            var orders = this.DB.Orders().Include("ShippingAddress").Where( o => o.Status >= Order.StatusType.Fulfilled);
            var data = GetOrderMapChart(this.DB, orders,true, this.Params.String("timespan", null));
            SendAPIMessage("chart-data", data);
        }

        [RequestHandler("/api/admin/shop/chart/map/confirmed")]
        public void MapShopConfirmed() {
            //TODO
            var orders = this.DB.Orders().Include("ShippingAddress").Where( o => o.Status >= Order.StatusType.Confirmed);
            var data = GetOrderMapChart(this.DB, orders,true, this.Params.String("timespan", null));
            SendAPIMessage("chart-data", data);
        }

        #region Helper Methods

        public static MapChartData GetOrderMapChart(ModelContext db, IQueryable<Order> orders, bool heatmap, string timeSpanFromContext , string orderURLPrefix = "/admin/shop/orders/order/") {
            var data = new MapChartData();
            // Default is today
            var start = Core.Util.Time.GetDefaultTimezoneToday().ToUniversalTime();
            var end = start.AddDays(1);
            // Timespan from context?
        
            if (timeSpanFromContext != null) {
                var timespan = Core.Util.Time.GetTimespanFromString(timeSpanFromContext);
                end = DateTime.UtcNow;
                start = end - timespan;
            }
         
        //    orders = orders.Where(o => o.Created >= start && o.Created < end);
            orders = orders.Where( Order.Between(start,end));

            foreach (var order in orders) {
                var address = order.ShippingAddress.FormatAsString(",", "Name,Company,Address2");
                address = Core.Localization.TextParser.ReplaceTextVariablesForData("Machinata.Core", address, "en");//TODO
                var dp = new MapChartDataMarker() {
                    Name = order.SerialId,
                    Address = address,
                    Link = orderURLPrefix + order.PublicId
                };
                if (heatmap) data.Heat.Add(dp);
                else data.Markers.Add(dp);
            }

            return data;
           
        }

        #endregion
    }
}
