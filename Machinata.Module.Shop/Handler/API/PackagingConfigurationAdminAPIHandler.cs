using System.Linq;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Builder;
using Machinata.Core.Model;

using Machinata.Module.Admin.Handler;
using System.Collections.Generic;
using Machinata.Module.Shop.Model;
using System;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Shop.Handler {


    public class PackagingConfigurationAdminAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<PackagingConfiguration> {

        #region Handler Policies
        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("shop");
        }
        #endregion
        

        [RequestHandler("/api/admin/shop/packaging/configurations/create")]
        public void Create() {
            var businesses = this.DB.Businesses().ToList();
            var businessId = this.Params.GetSelectedItem(businesses.Select(b => b.PublicId));
            var configuration = new PackagingConfiguration();
            configuration.Business = businesses.AsQueryable().GetByPublicId(businessId);
            this.DB.PackagingConfigurations().Add(configuration);
            this.DB.SaveChanges();
            SendAPIMessage("create-success", new { PackagingConfiguration = new { PublicId = configuration.PublicId } });

        }

        [RequestHandler("/api/admin/shop/packaging/configuration/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }
        [RequestHandler("/api/admin/shop/packaging/configuration/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }


        [RequestHandler("/api/admin/shop/packaging/upload-logo/{businessId}")]
        public void UploadLogo(string businessId) {
            var business = this.DB.Businesses().GetByPublicId(businessId);

            var configuration = this.DB.PackagingConfigurations()
                .Include(nameof(PackagingConfiguration.Business))
                .FirstOrDefault(pc => pc.Business.Id == business.Id);

            if (configuration == null) {
                throw new BackendException("packaging-config-error", "No Packaging Configuration for business found");
            }


            // Uploading logo?
            if (this.Context.Request.Files.Count > 0) {
                var contentFile = Core.Data.DataCenter.UploadFile(this.Request.File(), this.User, "Admin");
                this.DB.ContentFiles().Add(contentFile);
                this.DB.SaveChanges();
                configuration.Configuration[PackagingDesign.PACKAGING_LOGO_KEY] = contentFile.ContentURL;
            }

            // Save
            this.DB.SaveChanges();

            // Return
            this.SendAPIMessage("edit-success", new {
            
                PackagingLogo = configuration.Configuration[PackagingDesign.PACKAGING_LOGO_KEY]?.ToString()
            });



        }

        

    }
}
