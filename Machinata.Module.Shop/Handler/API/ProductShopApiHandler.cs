using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Module.Shop.Model;

using Machinata.Core.Builder;
using System.Collections.Generic;
using System.Linq;
using Machinata.Core.Exceptions;
using Machinata.Core.Reporting;
using System.IO;
using Machinata.Module.Admin.Handler;
using System;
using Machinata.Module.Shop.Services;
using System.Text;

namespace Machinata.Module.Shop.Handler {
    
    public class ProductShopApiHandler : Module.Admin.Handler.AdminAPIHandler {
        
        [RequestHandler("/api/admin/shop/product-configuration/{publicId}/delete")]
        public void ConfigurationDelete(string publicId) {
            this.RequireWriteARN();

            var entity = DB.ProductConfigurations()
                .GetByPublicId(publicId);

           
            if (entity.IsDefault) {
                throw new BackendException("delete-error", "Deleting the Default Configuration is not allowed");
            }

            var orderItems = this.DB.OrderItems()
                .Include(nameof(OrderItem.Configuration))
                .Include(nameof(OrderItem.Order))
                .Where(oi => oi.Configuration.Id == entity.Id);
            if (orderItems.Any()) {
                var orders = orderItems.Where(o => o.Order != null).Select(oi => oi.Order.SerialId);
                var serialIds = string.Join(", ", orders);
                throw new BackendException("delete-error", $"Please delete first the Orders {serialIds} using this configuration"); ;
            }


            this.DB.ProductConfigurations().Remove(entity);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-price-success");
        }


        [RequestHandler("/api/admin/shop/product/{publicId}/add-configuration")]
        public void ProductAddConfiguration(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Products().GetByPublicId(publicId);
            var configuration = new ProductConfiguration();
            configuration.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            entity.Configurations.Add(configuration);

            entity.ValidateConfigurations();
           
            //entity.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("add-success",new {
                Entity = new {
                    PublicId = configuration.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/shop/product/{productId}/delete")]
        public void ProductDelete(string productId) {
            this.RequireWriteARN();

            try {
                // Make changes and save
                var entity = this.DB.Products()
                    .GetByPublicId(productId);
                this.DB.DeleteEntity(entity);
                this.DB.SaveChanges();
                // Return
                this.SendAPIMessage("delete-success");
            }
            catch (Exception e) {
                throw new BackendException("delete-error", "Cannot delete this product since it's referenced in other entities (e.g. OrderItems)",e);
            }
            
        }

        [RequestHandler("/api/admin/shop/products/create")]
        public void ProductCreate() {
            this.RequireWriteARN();

            var product = new Shop.Model.Product();
            product.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            product.Validate();

            product.AddConfiguration(true);

            this.DB.Products().Add(product);
            this.DB.SaveChanges();

            SendAPIMessage("create-product-success", new {
                Product = new {
                    PublicId = product.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/shop/product/{productId}/edit")]
        public void ProductEdit(string productId) {
            this.RequireWriteARN();

            // Make changes and save
            var product = this.DB.Products().GetByPublicId(productId);
            product.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            product.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                Product = new {
                    PublicId = product.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/shop/product-configuration/{publicId}/edit")]
        public void ConfigurationEdit(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var entity = this.DB.ProductConfigurations().Include(nameof(ProductConfiguration.Product)).GetByPublicId(publicId);
            var form = ProductShopAdminHandler.GetAdminEditForm(entity);

            if (entity.IsDefault) {
                form.Exclude(nameof(entity.Business));
            }
            entity.Populate(this, form);
            entity.Validate();

            // Validate Configrations
            entity.Product.ValidateConfigurations();

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                Price = new {
                    PublicId = entity.PublicId
                }
            });
        }



        [RequestHandler("/api/admin/shop/product/{publicId}/categories/{categoryId}/toggle")]
        public void ProductToggleCategory(string publicId, string categoryId) {
            this.RequireWriteARN();

            // Init
            var entity = this.DB.Products().GetByPublicId(publicId);
            var category = this.DB.Categories().GetByPublicId(categoryId);
            entity.Include(nameof(entity.Categories));
            var enable = this.Params.Bool("value", false);
            if (enable) {
                entity.AddCategory(category);
            } else {
                entity.RemoveCategory(category);
            }
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("toggle-success");
        }

        [RequestHandler("/api/admin/shop/product/{publicId}/variations/{variationId}/toggle")]
        public void ProductToggleVariation(string publicId, string variationId) {
            this.RequireWriteARN();

            // Init
            var entity = this.DB.Products().Include(nameof(Shop.Model.Product.Variations)).GetByPublicId(publicId);
            var variation = this.DB.Products().Include(nameof(Shop.Model.Product.Variations)).GetByPublicId(variationId);
            var enable = this.Params.Bool("value", false);
            if (enable) {
                entity.AddVariation(variation);
                variation.AddVariation(entity);
            } else {
                entity.RemoveVariation(variation);
                variation.RemoveVariation(entity);
            }
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("toggle-success");
        }

        [RequestHandler("/api/admin/shop/product/{publicId}/related/{relatedId}/toggle")]
        public void ProductToggleRelated(string publicId, string relatedId) {
            this.RequireWriteARN();

            // Init
            var entity = this.DB.Products().GetByPublicId(publicId);
            var related = this.DB.Products().GetByPublicId(relatedId);
            entity.Include(nameof(entity.Related));
            var enable = this.Params.Bool("value", false);
            if (enable) {
                entity.AddRelated(related);
            } else {
                entity.RemoveRelated(related);
            }
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("toggle-success");
        }

        [RequestHandler("/api/admin/shop/product/{publicId}/label-update")]
        public void ProductLabelUpdate(string publicId) {
            this.RequireWriteARN();

            var label = this.Params.String("label");

            //if (string.IsNullOrWhiteSpace(label) == true) {
            //    throw new Exception("ProductLabelUpdate: no label passed");
            //}

            // Init
            var entity = this.DB.Products().GetByPublicId(publicId);
            entity.Details[Product.PRODUCT_DETAILS_LABEL_KEY] = label;
       
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("success");
        }

        [RequestHandler("/api/admin/shop/product/{productId}/informtion/edit")]
        public void ProductInformationEdit(string productId) {
            this.RequireWriteARN();

            // Product
            var product = this.DB.Products().GetByPublicId(productId);

            var allKeys = Context.Request.Form.AllKeys.Where(k=> k.StartsWith("group-", StringComparison.InvariantCulture)).ToList();

            var newData = new List<ProductInfoGroup>();
            string newDescription = null;

            // Parse request data and fill into data structure:
            // group-0.item-0.name: TestName
            // group-1.item-2.key1: Value1
            foreach (var requestKey in allKeys) {
                if (requestKey == "description") {
                    newDescription = this.Params.String(requestKey);
                    continue;
                }
                var paths = requestKey.Split('.');
                var group = newData.FirstOrDefault(nd => nd.Name.ToLower() == paths.First());
                if (group == null) {
                    var newGroup = new ProductInfoGroup();
                    newGroup.Name = paths.First();
                    newData.Add(newGroup);
                    group = newGroup;
                }
                var itemId = paths.Skip(1).First();
                var item = group.Items.FirstOrDefault(i =>  i.Id == itemId);
                if (item == null) {
                    var newItem = new ProductInfoItem();
                    newItem.Id = itemId;
                    group.Items.Add(newItem);
                    item = newItem;
                }
                if (paths.Length == 3) {
                    if (paths.Skip(2).First().ToLower() == "name") {
                        item.Name = this.Params.String(requestKey);
                    } else {
                        var key = paths.Skip(2).First();
                        if (key != null) {
                            item.Values[key] = this.Params.String(requestKey);

                        }
                    }
                }
            }

            product.Information.SetData(newData);
            product.Information.Description = newDescription;

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                Product = new {
                    PublicId = product.PublicId
                }
            });
        }

        /// <summary>
        /// Imports products from xlsx. Creates new catalogs of required.
        /// Adds the new catalog to params catalog-group or to default catalog group if defined
        /// </summary>
        [RequestHandler("/api/admin/shop/products/import")]
        public void ProductsImport() {
            this.RequireWriteARN();

            var existingCatalogs = this.DB.Catalogs().ToList();
            var catalogGroupUrl = this.Params.String("catalog-group", Config.DefaultCatalogGroupUrl);

            var stream = this.Request.File().InputStream;
            var duplicateStream = new MemoryStream();
            stream.CopyTo(duplicateStream);
            
            var products = ExportService.ImportXLSX<Shop.Model.Product>(this.DB, stream, new FormBuilder(Forms.Admin.EXPORT), ImportProduct);
             
            this.DB.Products().AddRange(products);

            // Add to catalog group
            if (!string.IsNullOrEmpty(catalogGroupUrl)) {
                var group = this.DB.CatalogGroupByUrl(catalogGroupUrl);
                var catalogs = products.SelectMany(p => p.Catalogs).Distinct().Where(c=>!existingCatalogs.Contains(c));
                foreach(var catalog in catalogs) {
                    group.AddCatalog(catalog);
                }
            }

            this.DB.SaveChanges();

            ExportService.ImportXLSXContentNodes(products, duplicateStream, new FormBuilder(Forms.Admin.EXPORT), this.DB);

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("import-success", new {
               
            });
        }

        private void ImportProduct(Shop.Model.Product product, ModelContext db, Dictionary<string, string> row, int rowNumber) {
            var config = product.AddConfiguration(true);
            config.CustomerPrice = new Price( decimal.Parse(row[nameof(ProductConfiguration.CustomerPrice)]) );
            config.SKU = row[nameof(ProductConfiguration.SKU)];
          

            // Categories
            var categories = row[nameof(Shop.Model.Product.Categories)].Split(';');
            foreach(var categoryName in categories) {
                if (categoryName == null || categoryName.Length < 4) { continue; }
                var category = Category.FindOrCreateMainCategory(db, categoryName);
                product.AddCategory(category);
            }

            // Cataloges
            var catalogs = row[nameof(Shop.Model.Product.Catalogs)].Split(';');
            foreach (var catalogName in catalogs) {
                var catalog = Catalog.FindOrCreateCatalog(db, catalogName);
                product.Catalogs.Add(catalog);
            }

            // Name
            if (string.IsNullOrEmpty(product.Name)) {
                product.Name = config.SKU;
            }
        }

        [RequestHandler("/api/admin/shop/product/{productId}/update")]
        public void Update(string productId) {
            this.RequireWriteARN();

            Product product = null;
            StringBuilder log = new StringBuilder();
            try {
                var mode = this.Params.String("mode", "test");

                // Make changes and save
                product = this.DB.Products()
                                .Include(nameof(Product.Ingredients))
                                .Include(nameof(Product.Description))
                                .Include(nameof(Product.Preparation))
                                .Include(nameof(Product.Categories))
                                .GetByPublicId(productId);
                var configuration = product.GetDefaultConfiguration();
                log = ProductUpdater.Updater.Update(product, configuration);

                // Save?
                if (mode != "test") {
                    this.DB.SaveChanges();
                } else {
                    log.Insert(0, "TEST MODE: Changes will not be saved.\n\n");
                }

                this.Log(log.ToString());

            }catch (Exception e) {
                throw new BackendException("update-error", "Could not update the product", e);
            }

            // Return
            this.SendAPIMessage("update-success", new {
                Product = new {
                    PublicId = productId
                },
                Result = log.ToString()
            });
        }



    }
}
