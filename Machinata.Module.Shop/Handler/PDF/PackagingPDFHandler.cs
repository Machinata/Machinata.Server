using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Templates;
using Machinata.Core.Model;

using Machinata.Module.Shop.Model;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Shop.Handler.PDF {
    public class PackagingPDFHandler : PDFPageTemplateHandler {
        
        [RequestHandler("/pdf/shop/packaging/product/{publicId}", AccessPolicy.PUBLIC_ARN)]
        public void PackagingProduct(string publicId) {
            
            // Get business and product
            var businessId = this.Params.String("business", null);
            var productionDate = this.Params.DateTimeGlobalConfigFormat("date", DateTime.UtcNow);
            var templateKey = this.Params.String("template"); // template to be used
            var templateTitle = "";

            // Todo security via api keys?
            Business business = this.DB.Businesses().GetByPublicId(businessId);

            // Security
            this.RequireAdminARNOrBusinessOrUserAssociation("/admin/shop", business, null);

            // Product
            var product = this.DB.Products()
                .Include(nameof(Module.Shop.Model.Product.Title) + "." + nameof(ContentNode.Children) + "." + nameof(ContentNode.Children))
                .Include(nameof(Module.Shop.Model.Product.Ingredients))
                .Include(nameof(Module.Shop.Model.Product.Description))
                .GetByPublicId(publicId);

            // Configured Label templates
            var availableTemplates = Product.GetProductTemplates();

            if (string.IsNullOrWhiteSpace(templateKey) == true) { throw new BackendException("product-no-template", "No label template passed"); }

            // Template from Product (default)?
            if (templateKey == "default") {
                if (product.Details.Keys.Contains(Product.PRODUCT_DETAILS_LABEL_KEY)) {
                    templateTitle = product.Details.StringNullable(Product.PRODUCT_DETAILS_LABEL_KEY); // e.g. "Double-Round-Sticker-84x161mm"
                    if (availableTemplates.ContainsValue(templateTitle)){
                        templateKey = availableTemplates.FirstOrDefault(t => t.Value == templateTitle).Key;
                    }
                    
                }
                if (string.IsNullOrWhiteSpace(templateKey) == true || templateKey=="default") { throw new BackendException("product-no-default-label", "This product has no default label. Please set first on the product"); } 
            }
                        
            string packagingLabelConfigSuffix = ""; // some of the config values have to be configured additionally if we use another template
            if (availableTemplates.ContainsKey(templateKey)) {
                templateTitle = availableTemplates[templateKey];
                packagingLabelConfigSuffix = "-" + templateTitle;
            } else {
                throw new BackendException("invalid-template", $"The template '{templateKey}' is not available");
            }

            // Change the template
            var templateName = "product." + templateKey;
            this.Template.ChangeTemplate(templateName);
            if (string.IsNullOrWhiteSpace(this.Template.Content)) {
                throw new BackendException("invalid-template", $"The template '{templateName}' is not available");
            }
              
            // Get configuration
            var configuration = product.GetMergedBusinessConfiguration(business);
            var retailPrice = configuration.RetailPrice.Clone();


            // Get design
            var selectedDesign = PackagingDesign.GetPackagingDesignByBusiness(this.DB, business);
            var designConfiguration = PackagingDesign.GetConfiguration(this.DB, business);
            var selectedLogo = PackagingDesign.GetLogoByBusiness(this.DB,business);

            if (selectedDesign == null) {
                throw new BackendException("design-error", $"No packaging design for business {business?.Name} defined.");
            }

            // Init page
            this.RemoveAllHeaders();
            this.RemoveAllFooters();
            this.PageWidth = Core.Config.GetDoubleSetting("ShopProductPackagingLabelPageWidth" + packagingLabelConfigSuffix); //mm
            this.PageHeight = Core.Config.GetDoubleSetting("ShopProductPackagingLabelPageHeight" + packagingLabelConfigSuffix); //mm
            this.MarginBottom = Core.Config.GetDoubleSetting("ShopProductPackagingLabelMarginBottom" + packagingLabelConfigSuffix); //mm
            this.MarginTop = Core.Config.GetDoubleSetting("ShopProductPackagingLabelMarginTop" + packagingLabelConfigSuffix); //mm
            this.MarginLeft = Core.Config.GetDoubleSetting("ShopProductPackagingLabelMarginLeft" + packagingLabelConfigSuffix); //mm
            this.MarginRight = Core.Config.GetDoubleSetting("ShopProductPackagingLabelMarginRight" + packagingLabelConfigSuffix); //mm

            if (this.Params.Bool("guides", false)) {
                //this.TemplateGuideImage = Core.Config.GetStringSetting("ShopProductPackagingLabelTemplateGuideImage" + packagingLabelConfigSuffix, null); //mm
                this.Template.InsertVariable("show-guides-class", "show-guides");
            } else {
                this.Template.InsertVariable("show-guides-class", "");
            }
            
            // Get product settings
            int sellByDays = GetInt(product.Details[PackagingDesign.DETAILS_KEY_SELL_BY_DAYS]);
            var sellByDate = productionDate.AddDays(sellByDays);
            var consumeByDays = GetInt(product.Details[PackagingDesign.DETAILS_KEY_CONSUME_BY_DAYS]);
            var consumeByDate = productionDate.AddDays(consumeByDays);

            // Variables
            this.Template.InsertVariables("packaging.design", selectedDesign);
            this.Template.InsertVariables("packaging.configuration", designConfiguration);
            this.Template.InsertVariable("business.logo", selectedLogo);

            // Title
            {
                var title = product.Title?.GetExactTranslationForLanguageOrDefault(this.Language)?.Title;
                this.Template.InsertVariable("product.title", title );

                if (string.IsNullOrWhiteSpace(title) == true) {
                    this.Template.InsertVariable("product-or-configuration.title", configuration.Title);
                } else {
                    this.Template.InsertVariable("product-or-configuration.title", title);
                }

            }
            this.Template.InsertVariables("product", product);
            this.Template.InsertVariables("product.configuration", configuration);
            this.Template.InsertContent("product.ingredients.content", product.Ingredients);
            this.Template.InsertVariable("product.custom-retail-price", retailPrice);
            this.Template.InsertVariable("product.consume-by-date", Core.Util.Time.ToDefaultTimezoneDateString(consumeByDate));
            this.Template.InsertVariable("product.sell-by-date", Core.Util.Time.ToDefaultTimezoneDateString(sellByDate));
            this.Template.InsertVariable("product.production-date", Core.Util.Time.ToDefaultTimezoneDateString(productionDate));
            this.Template.InsertProductInformationTable(product.Information, "product.information-table", "information-table");
          
            // Cover page?
            if(this.Params.Bool("cover",false)) {
                // Load as template and set contents
                var coverTemplate = this.Template.LoadTemplate("product.cover-page");
                coverTemplate.InsertVariables("product", product);
                coverTemplate.InsertVariables("business", business);
                coverTemplate.InsertVariables("packaging.design", selectedDesign);
                coverTemplate.InsertVariables("product.configuration", configuration);
                coverTemplate.InsertVariable("pdf.copies", this.Params.String("copies"));
                coverTemplate.InsertVariable("product.sell-by-date", Core.Util.Time.ToDefaultTimezoneDateString(sellByDate));
                coverTemplate.InsertVariable("product.production-date", Core.Util.Time.ToDefaultTimezoneDateString(productionDate));
                this.CoverPage = coverTemplate.Content;
            }

            // Trailer page?
            if(this.Params.Bool("trailer",false)) {
                // Load as template and set contents
                var trailerTemplate = this.Template.LoadTemplate("product.trailer-page");
                trailerTemplate.InsertVariables("product", product);
                this.TrailerPage = trailerTemplate.Content;
            }

            this.FileName = $"ProductPackaging_{configuration.SKU}_{business.Name}_{templateTitle}." + this.ContentFormat;
        }

        private static int GetInt(object val) {
            int result = 0;
            if (val != null) {
                int.TryParse(val.ToString(), out result);
            }
            return result;
        }
    }
}