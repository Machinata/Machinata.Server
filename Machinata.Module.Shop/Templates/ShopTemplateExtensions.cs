using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Util;
using Machinata.Core.Builder;
using Machinata.Module.Shop.Model;
using Machinata.Core.Model;
using Machinata.Module.Finance.Model;
using System.Runtime.CompilerServices;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public static class ShopTemplateExtentions {

        public static void InsertProductInformationTable(this PageTemplate template, ProductInformation productInformation, string variableName, string templateName, string classes = null) {
            var informationTableTemplate = template.LoadTemplate(templateName);
            var groups = productInformation.GetData();
            var groupTemplates = new List<PageTemplate>();
            var columns = groups.SelectMany(g => g.Items).SelectMany(i => i.Values.Keys).Distinct().ToList();
            columns.Insert(0, "Name");

            var columnHederTemplates = new List<PageTemplate>();
            foreach (var column in columns) {
                var columnHeaderTemplate = template.LoadTemplate(templateName+".header");
                columnHeaderTemplate.InsertVariable("value", column);
                columnHederTemplates.Add(columnHeaderTemplate);
            }

            informationTableTemplate.InsertTemplates("header", columnHederTemplates);
            int groupIndex = 0;
            foreach (var group in groups) {
                var groupTemplate = template.LoadTemplate(templateName+".group");
                var groupName = $"group-{groupIndex}";
                group.Name = groupName;

                var itemTemplates = new List<PageTemplate>();
                int itemIndex = 0;
                var maxColumns = 0;
                foreach (var item in group.Items) {
                    var itemTemplate = template.LoadTemplate(templateName+".group.item");
                    var itemValueTemplates = new List<PageTemplate>();
                    foreach (var column in columns) {

                        PageTemplate columnTemplate = null;
                        if (column == columns.First()) {
                            columnTemplate = template.LoadTemplate(templateName + ".group.item.name");
                            columnTemplate.InsertVariable("item.value", item.Name);
                        } else {
                            columnTemplate = template.LoadTemplate(templateName + ".group.item.value");
                            var value = item.Values.GetStringValue(column, true);
                            columnTemplate.InsertVariable("item.value", value);
                        }
                        // form name
                        var name = $"{group.Name}.item-{itemIndex}.{column}".Trim().ToLower(); // todo cleanup

                        columnTemplate.InsertVariable("item.name", name);
                        columnTemplate.InsertVariable("item.index", itemIndex);
                        columnTemplate.InsertVariable("item.column", column);
                        itemValueTemplates.Add(columnTemplate);
                    }
                    maxColumns = System.Math.Max(maxColumns, itemValueTemplates.Count);
                    itemIndex++;
                    itemTemplate.InsertTemplates("values", itemValueTemplates);
                    itemTemplates.Add(itemTemplate);
                }
                groupTemplate.InsertTemplates("items", itemTemplates);
                groupTemplate.InsertVariable("group.name", groupName);
                groupTemplate.InsertVariable("group.columns", maxColumns);
                groupTemplates.Add(groupTemplate);
                groupIndex++;

            }

           

            informationTableTemplate.InsertTemplates("groups", groupTemplates);
            informationTableTemplate.InsertVariable("description", productInformation.Description);
            informationTableTemplate.InsertVariable("classes", classes);
            template.InsertTemplate(variableName, informationTableTemplate);
        }



    }
}
