using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Core.Model;
using Machinata.Module.Mailing.Model;
using Machinata.Core.Builder;
using Machinata.Module.Shop.Model;
using Machinata.Core.Util;

namespace Machinata.Module.Shop.Mailing {
    public class CustomerMailingContactProvider : Module.Mailing.ContactProvider.MailingContactProvider {

        public const string SettingIncludeGuestsKey = "IncludeGuests";
        public const string SettingIncludeTestContacts = "IncludeTestContacts";

        protected override IQueryable<MailingContact> GetContactsImpl(ModelContext db) {
            var contacts = new List<MailingContact>();

            // Test Contacts
            if (this.IncludeTestContacts) {
                var firstAdmin = Core.Config.AdminEmails.FirstOrDefault();
                if (string.IsNullOrEmpty(firstAdmin) == false) {
                    var contact = new MailingContact() { Name = "Test Contact", Email = firstAdmin };
                    contacts.Add(contact);
                }
            }

            // Orders From Registered Users
            var customers = Shop.Logic.Customers.GetUsersWithConfirmedOrders(db).ToList();
          
            foreach(var customer in customers) {
                var lastOrder = Logic.Customers.GetConfirmedOrdersWithDates(db, nameof(Order.BillingAddress))
                    .Where(o => o.User.Id == customer.Id).OrderByDescending(o => o.Id).First();
                var contact = new MailingContact() { Name = lastOrder.BillingAddress.Name, Email = lastOrder.BillingAddress.Email, Id = lastOrder.Id };
                contact.Properties["LastOrder"] = lastOrder.PublicId;
                contacts.Add(contact);
            }


            // Guest Contacts
            if (this.IncludeGuestOrders) {
                // Orders from Guest "Users"
                var ordersNoUsers = Shop.Logic.Customers.GetConfirmedOrdersWithDates(db, new string[] { nameof(Order.BillingAddress) }).Where(o => o.User == null);
                // distinct by email address
                ordersNoUsers = ordersNoUsers.OrderByDescending(o => o.Created).DistinctBy(o => o.BillingAddress.Email).AsQueryable();
                foreach (var order in ordersNoUsers) {
                    var contact = new MailingContact() { Name = order.BillingAddress.Name, Email = order.BillingAddress.Email, Id = order.Id};
                    contact.Properties["LastOrder"] = order.PublicId;
                    contacts.Add(contact);
                }
            }
           



            return contacts.DistinctBy(c=>c.EmailHash).AsQueryable();
        }
        
        public override string GetContactAdminLink() {
            return "/admin/shop/orders/order/{entity.public-id}";
        }

        public override IDictionary<string, string> GetDefaultSettings() {
            var settings =  new Dictionary<string, string>();
            settings[SettingIncludeGuestsKey] = "false";
            settings[SettingIncludeTestContacts] = "false";
            return settings;
        }

        public bool IncludeGuestOrders
        {
            get
            {
                bool result = false;
                if (this.List.ContactProviderSettings.Keys.Contains(SettingIncludeGuestsKey)) {
                    var rawVal = this.List.ContactProviderSettings[SettingIncludeGuestsKey]?.ToString();
                    bool.TryParse(rawVal,out result);
                }

                return result;
            }
        }

        public bool IncludeTestContacts
        {
            get
            {
                bool result = false;
                if (this.List.ContactProviderSettings.Keys.Contains(SettingIncludeTestContacts)) {
                    var rawVal = this.List.ContactProviderSettings[SettingIncludeTestContacts]?.ToString();
                    bool.TryParse(rawVal, out result);
                }

                return result;
            }
        }
    }


}
