using System;
using System.Collections.Generic;
using Machinata.App.StressTester.Config;
using CommandLine;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Globalization;

namespace Machinata.App.StressTester {
    class Program {

        static void Main(string[] args) {
            

            System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;

            Console.WriteLine("Starting StressTester on " + Environment.MachineName + "...");
            
                    // Configure
                    CommandLine.Parser.Default.ParseArguments<Arguments>(args)
                        .WithParsed<Arguments>(opts => StartStressTesterWithConfiguration(opts))
                        .WithNotParsed<Arguments>((errs) => HandleParseError(errs));
              
        }

        private static void HandleParseError(IEnumerable<Error> errs) {
            Console.WriteLine("Error parsing configuration file");
        }



        public static void StartStressTesterWithConfiguration(Arguments options) {
            Configuration globalConfig = null;

            // Config file
            var content = File.ReadAllText(options.Config);
            globalConfig = JsonConvert.DeserializeObject<Configuration>(content);

            // Create watchdog
            var watchdog = new Logic.StressTester(globalConfig.Config);


            // Register endpoints
            foreach (var website in globalConfig.Endpoints) {

                try {

                    


                    // Endpoint configuration
                    {
                        var fallbackConfiguration = BaseEndpointConfig.Default();


                        // Endpoint: Missing values from from config file
                        if (globalConfig.Config.Endpoint != null) {
                            Configuration.CopyProperties(globalConfig.Config.Endpoint, website);
                        }

                        // Endpoint: Missing values from from default c# values
                        Configuration.CopyProperties(fallbackConfiguration, website);
                    }




                    // Endpoint: create and keep
                    if (website.Enabled.Value) {
                        var endpoint = new Logic.Endpoint(website);
                        watchdog.RegisterEndpoint(endpoint);
                    }

                } catch (Exception e) {
                    
                }

            }

            // Initialize Loggers
            //FileLogger.Initialize(watchdog.Endpoints.Select(e => e.Id), globalConfig.Config);


            // Start 
            watchdog.Start();
            Console.WriteLine("Done!");
            Console.ReadLine();
        }

    }
}



