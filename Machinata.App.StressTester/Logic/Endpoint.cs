using Machinata.App.StressTester.Config;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Machinata.App.StressTester.Logic {

    public class Endpoint {
        
        public Logic.StressTester StressTester;
        public Config.EndpointConfig Config;
        public Thread MainThread;
        public List<User> Users = new List<User>();

        public DateTime ProcessStartTime;
        public DateTime ProcessEndTime;

        public Endpoint(Config.EndpointConfig endpointConfig) {
            this.Config = endpointConfig;
        }

        public void Start() {
            this.MainThread = new Thread(new ThreadStart(this.Process));
            this.MainThread.Start();
        }


        public void Process() {
            Console.WriteLine("Processing Endpoint...");
            this.ProcessStartTime = DateTime.Now;
            this.ProcessEndTime = ProcessStartTime.AddMinutes(this.Config.DurationMinutes.Value);
            // Create users
            for (var i = 0; i < this.Config.NumberOfUsers; i++) {
                var user = new User(this);
                user.Id = i.ToString();
                this.Users.Add(user);
            }
            // Start users
            foreach(var user in this.Users) {
                user.Start();
            }
            // Wait for users
            foreach (var user in this.Users) {
                user.UserThread.Join();
            }
            Console.WriteLine("Finished Processing Endpoint");
        }

        public void Log(string msg) {
            Console.WriteLine($"{this.Config.Name}: {msg}");
        }


        public string CompileLog(bool verbose) {
            // Init
            StringBuilder log = new StringBuilder();
            
            // Settings
            if (verbose) {
                log.AppendLine("Name: " + this.Config.Name);
                log.AppendLine("URL: " + this.Config.URL);
                log.AppendLine("");
            } else {
                log.AppendLine(this.Config.URL);
            }

            
            return log.ToString();
        }


        
    }
}
