
using System;
using System.Collections.Generic;
using System.IO;

namespace Machinata.App.StressTester.Logic {
    public class StressTester {

        public int MaxWarningsPerHour = 6; //TODO
        public bool Verbose = false;
        public Config.Config GlobalConfig;


        private List<Endpoint> _endpoints = new List<Endpoint>();

        public IEnumerable<Endpoint> Endpoints {
            get {
                return _endpoints;
            }
        }

        public StressTester(Config.Config config) {
            this.Verbose = config.Verbose;
            this.GlobalConfig = config;
        }

        public void RegisterEndpoint(Endpoint endpoint) {
            Console.WriteLine("Registering endpoint " + endpoint.Config.Name);
            Console.WriteLine("   " + endpoint.Config.URL);
            endpoint.StressTester = this;
            _endpoints.Add(endpoint);
        }

        public void Start() {
            Console.WriteLine("Starting...");
            foreach(var endpoint in _endpoints) {
                endpoint.Start();
            }
            Console.WriteLine("All endpoints started!");
            foreach (var endpoint in _endpoints) {
                endpoint.MainThread.Join();
            }
            Console.WriteLine("All endpoints finished!");
        }
    }
}
