using Machinata.App.StressTester.Config;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;


namespace Machinata.App.StressTester.Logic {

    public class User {
        
        public string Id;
        public Endpoint Endpoint;
        public Thread UserThread;

        public DateTime? LastHit = null;
        public List<Hit> Hits = new List<Hit>();

        public User(Endpoint endpoint) {
            this.Endpoint = endpoint;


        }


        public void Start() {
            this.UserThread = new Thread(new ThreadStart(this.Process));
            this.UserThread.Start();
        }


        public void Process() {

            double secondsBetweenHit = 60.0 / this.Endpoint.Config.RequestsPerMinutePerUser.Value;
            double startDelayInSeconds = Util.Jitter.Apply(secondsBetweenHit, this.Endpoint.Config.StartJitterInPercent.Value);
            Console.WriteLine($"Processing User... (startup delay = {startDelayInSeconds}s)");
            System.Threading.Thread.Sleep((int)(startDelayInSeconds * 1000));


            while (this.ShouldContinue() == true) {
                if(ShouldDoHit()) {
                    DoHit();
                }
                System.Threading.Thread.Sleep(100);
            }
            Console.WriteLine("Finished Processing User");
        }

        public void Log(string msg) {
            this.Endpoint.Log($"User{this.Id}: {msg}");
        }

        public void DoHit() {
            this.LastHit = DateTime.Now;
            var urlToHit = this.Endpoint.Config.URL;
            var hit = new Hit(urlToHit);
            this.Hits.Add(hit);
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try {
                this.ProcessHit(urlToHit);
                hit.Result = Hit.HitResult.Success;
            } catch(Exception e) {
                hit.Result = Hit.HitResult.Error;
                Console.WriteLine("***" + e.Message);
            }
            stopWatch.Stop();
            Log($"Hit #{this.Hits.Count}\t{hit.Result}\t\t{stopWatch.ElapsedMilliseconds}ms\t\t{urlToHit} ");
        }

        public void ProcessHit(string urlToHit) {
            bool useWGET = true;
            string results = null;
            if(useWGET) {
                var proc = new Process();
                proc.StartInfo.FileName = "curl.exe";
                proc.StartInfo.Arguments = urlToHit;
                //proc.StartInfo.CreateNoWindow = true;
                //proc.StartInfo.UseShellExecute = false;
                //proc.StartInfo.RedirectStandardOutput = true;
                //proc.StartInfo.RedirectStandardError = true;
                //proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proc.Start();
                proc.WaitForExit();
               //System.Console.Write(proc.StandardOutput.ReadToEnd());
                //System.Threading.Thread.Sleep(100000);
            } else {
                var wc = new WebClient();
                results = wc.DownloadString(urlToHit);
            }
            
            if(this.Endpoint.Config.DownloadBundles.Value == true) {
                var regex = new Regex(@"(\/static\/bundle\/[\w\d\/\-\.?=&;]*)");
                var matches = regex.Matches(results);
                var uniqueMatches = new Dictionary<string, string>();
                foreach(var match in matches) {
                    if (!uniqueMatches.ContainsKey(match.ToString())) uniqueMatches.Add(match.ToString(), match.ToString());
                }
                foreach(var match in uniqueMatches.Keys) {
                    var matchCleaned = match;
                    matchCleaned = matchCleaned.Replace("&amp;", "&");
                    var originalURI = new Uri(this.Endpoint.Config.URL);
                    if (matchCleaned.StartsWith("/")) matchCleaned = originalURI.Scheme + "://" + originalURI.Host + matchCleaned;
                    var wc2 = new WebClient();
                    var results2 = wc2.DownloadString(matchCleaned);
                }
            }
        }

        public bool ShouldDoHit() {
            double secondsBetweenHit = 60.0 / this.Endpoint.Config.RequestsPerMinutePerUser.Value;
            secondsBetweenHit = Util.Jitter.Apply(secondsBetweenHit, this.Endpoint.Config.RequestsPerMinutePerUserJitterInPercent.Value);

            if (this.LastHit == null) return true;
            if (DateTime.Now > this.LastHit.Value.AddSeconds(secondsBetweenHit)) return true;

            return false;
        }

        public bool ShouldContinue() {
            if (DateTime.Now > this.Endpoint.ProcessEndTime) return false;

            return true;
        }



    }
}
