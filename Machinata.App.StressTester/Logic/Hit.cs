using Machinata.App.StressTester.Config;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Machinata.App.StressTester.Logic {

    public class Hit {

        public enum HitResult {
            Waiting,
            Success,
            Error
        }


        public DateTime Timestamp;
        public string URL = null;
        public HitResult Result;

        public Hit(string url) {
            this.Timestamp = DateTime.Now;
            this.URL = url;
            this.Result = HitResult.Waiting;
        }




    }
}
