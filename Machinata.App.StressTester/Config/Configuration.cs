using Machinata.App.StressTester.Util;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Machinata.App.StressTester.Config {

    public class Configuration {

        public Config Config { get; set; }

        public EndpointConfig[] Endpoints { get; set; }

       
            /// <summary>
            /// Used to create a custom config based on the global config
            /// </summary>
            public static void CopyProperties( object source, object destination) {
            // Iterate the Properties of the destination instance and  
            // populate them from their source counterparts  
            PropertyInfo[] destinationProperties = destination.GetType().GetProperties();
            foreach (PropertyInfo destinationPi in destinationProperties) {

                if (destinationPi.CanWrite) { 
                PropertyInfo sourcePi = source.GetType().GetProperty(destinationPi.Name);
                var destinationValue = destinationPi.GetValue(destination, null);
                if (sourcePi != null) {
                    var sourceValue = sourcePi.GetValue(source, null);
                    if (destinationValue == null) {
                        destinationPi.SetValue(destination, sourceValue, null);
                    }
                }
                }
            }
        }

    }

   

    public class Config {
        public bool Verbose;
        public BaseEndpointConfig Endpoint { get; set; }

        public override string ToString() {
            return JSON.ToJSON(this);
        }

    }

    public class EndpointConfig  : BaseEndpointConfig{
        public string Name;
        public string URL;
        

    }

    public class BaseEndpointConfig {

        public bool? Enabled { get; set; }
        public int? NumberOfUsers { get; set; }
        public int? DurationMinutes { get; set; }
        public int? StartJitterInPercent { get; set; }
        public int? RequestsPerMinutePerUser { get; set; }
        public int? RequestsPerMinutePerUserJitterInPercent { get; set; }
        public bool? DownloadBundles { get; set; }


        public static BaseEndpointConfig Default() {
            return new BaseEndpointConfig() {
                Enabled = true,
                NumberOfUsers = 50,
                DurationMinutes = 5,
                StartJitterInPercent = 100,
                RequestsPerMinutePerUser = 30,
                RequestsPerMinutePerUserJitterInPercent = 20,
                DownloadBundles = false,

            };
               

        }

    }


}
