﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Machinata.App.StressTester.Util {
    class Jitter {

        private static Random rnd = new Random((int)DateTime.Now.Ticks);

        public static double Apply(double input, int amount) {
            double jitter = rnd.Next(-(int)(amount), +(int)(amount)) / 100.0;
            return input + jitter;
        }
    }
}
