﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Machinata.App.StressTester.Util {
    class JSON {


        public static string ToJSON(object data) {
            
            var enumToString = true;
            var indendFormatting = true;

            var settings = new JsonSerializerSettings {

            };

            if (enumToString) {
                settings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            }

            var format = Formatting.None;
            if (indendFormatting == true) {
                format = Formatting.Indented;
            }

            // Serialize
            return JsonConvert.SerializeObject(data, format, settings);
        }
    }
}
