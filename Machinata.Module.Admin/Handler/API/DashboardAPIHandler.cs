using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Admin.Dashboard;
using Machinata.Module.Reporting.Extensions;
using Machinata.Module.Reporting.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class DashboardAPIHandler : AdminAPIHandler {


        //#region Class Logger
        ///// <summary>
        ///// The logger helper for this class. Use this logger instance anytime you want to
        ///// log something from within this class.
        ///// </summary>
        //private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        //#endregion


        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("dashboard");
        }

        #endregion

        [RequestHandler("/api/admin/dashboard/report" /*, "/admin"*/)]
        public void Report() {

            this.RequireAdmin();

            var debug = this.Params.Bool("debug", false);
            var start = this.Params.DateTimeGlobalConfigFormatNullable("start", null);
            var end = this.Params.DateTimeGlobalConfigFormatNullable("end", null);

            var dateRange = new DateRange(start, end);

            // prefix namespace filter for the full qualified name of a method
            // E.g filters=Machinata.Module.Finance loads "Machinata.Module.Finance.Dashboard.FinanceDashboard.LoadDashboard"
            var namespacesFilter = this.Params.StringArray("filters", new string[] { }).ToList();
            
            var report = new Reporting.Model.Report();
            report.SetTitle("Dashboard");
            report.ReportId = "dashboard-report";

            var rootNode = new NoOpNode();
            report.AddToBody(rootNode);

            var items = DashboardLoader.LoadDashboardItems(this.Language, namespacesFilter, dateRange, debug);

            // Remove titles for main sections if only one item
            if(items.Count() == 1) {
                foreach(var item in items) {
                    if(item.Node.NodeType == "SectionNode") item.Node.MainTitle = "";
                }
            }


            // Menu Sections, Try to make same sort as menu
            {
                var menuBuilder = MenuBuilder.LoadMenuFromHandlers(this);
                var sections = menuBuilder.GetSections().OrderBy(s => s.Sort != null).ThenBy(s => s.Sort).ThenBy(s => s.Title).Select(s => s.Title).ToList();
                sections = sections.Select(s => Core.Localization.Text.GetTranslatedTextByTextVariable(s)).ToList();
                items = items.OrderBy(n => (sections.IndexOf(n.Node.MainTitle?.Resolved) >= 0) ? sections.IndexOf(n.Node.MainTitle?.Resolved) : int.MaxValue).ThenBy(n => n.Node.MainTitle?.Resolved);
            }
            

            // Add Node only if has matching ARN
            foreach (var item in items) {
                var matchesArn = this.User.HasMatchingARN(item.ARN);

              
                //// Section filter
                //if (namespacesFilter.Any()) {
                //    if (namespacesFilter.Contains(item.Node.MainTitle.Resolved) == false) {
                //        continue;
                //    }
                //}

                if (matchesArn == true) {
                    // Screen
                    item.Node.Screen = true;

                    // Debugging loading times

                    if(debug == true) {
                        var infoNode = new InfoNode();
                        infoNode.Message = "Loading time: " + item.LoadingTime.TotalMilliseconds.ToString();
                        item.Node.AddChild(infoNode);
                    }
                    //item.Node.SetTitle(item.Node.MainTitle.Resolved +  " " + item.LoadingTime.TotalMilliseconds.ToString());
                    
                    // Add
                    rootNode.AddChild(item.Node);

                   
                }
            }

            this.SendReportingAPIMessage(report);
        }

    }
}
