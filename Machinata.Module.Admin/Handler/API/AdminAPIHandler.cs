
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;

using Machinata.Core.Reporting;

using Machinata.Core.Data;
using System.Web;

namespace Machinata.Module.Admin.Handler {
    
    public abstract class AdminAPIHandler : Core.Handler.APIHandler {

        public override bool ValidateARN(string requiredARN, bool throwExceptions = true) {

            // Are we enforcing an IP for this request
            if (!string.IsNullOrEmpty(Core.Config.SecurityAllAdminAccessIPRestrictions) && Core.Config.SecurityAllAdminAccessIPRestrictions != "*") {
                // Make sure IP matches
                var ipsOrSubnets = Core.Config.SecurityAllAdminAccessIPRestrictions.Split(',').ToList();
                var valid = Core.Util.IP.DoesIPMatcheAnyAddressOrSubnetInList(this.Request.IP, ipsOrSubnets);
                if (!valid) throw new BackendException("access-denied", $"This request has not been authorized for this IP address ({this.Request.IP}) (this admin zone requires a specific IP source).");
            }

            return base.ValidateARN(requiredARN, throwExceptions);

        }

    }
    
    public abstract class CRUDAdminAPIHandler<T> : Core.Handler.CRUDAPIHandler<T> where T : ModelObject, new() {

        public override bool ValidateARN(string requiredARN, bool throwExceptions = true) {
            
            // Are we enforcing an IP for this request
            if (!string.IsNullOrEmpty(Core.Config.SecurityAllAdminAccessIPRestrictions) && Core.Config.SecurityAllAdminAccessIPRestrictions != "*") {
                // Make sure IP matches
                var ipsOrSubnets = Core.Config.SecurityAllAdminAccessIPRestrictions.Split(',').ToList();
                var valid = Core.Util.IP.DoesIPMatcheAnyAddressOrSubnetInList(this.Request.IP, ipsOrSubnets);
                if (!valid) throw new BackendException("access-denied", $"This request has not been authorized for this IP address ({this.Request.IP}) (this admin zone requires a specific IP source).");
            }

            return base.ValidateARN(requiredARN, throwExceptions);

        }

    }
}
