
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;

namespace Machinata.Module.Admin.Handler {


    public class AccessPolicyAPIHandler : AdminAPIHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("security");
        }

        #endregion

        [RequestHandler("/api/admin/security/access-policy/{publicId}/delete")]
        public void Delete(string publicId) {
            this.RequireSuperuser();

            this.DB.AccessPolicies().RemoveByPublicId(publicId);
            this.DB.SaveChanges();
            SendAPIMessage("delete-success");
        }

        [RequestHandler("/api/admin/security/access-policy/{publicId}/user/{userId}/toggle")]
        public void ToggleUser(string publicId, string userId) {
            this.RequireSuperuser();

            // Init
            var entity = this.DB.AccessPolicies().Include("Users").GetByPublicId(publicId);
            var user = this.DB.Users().GetByPublicId(userId);
            var enable = this.Params.Bool("value",false);
            if(enable) {
                user.AddPolicy(entity);
            } else {
                user.RemovePolicy(entity);
            }
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("toggle-success");
        }

        [RequestHandler("/api/admin/security/access-policy/{publicId}/group/{groupId}/toggle")]
        public void ToggleGroup(string publicId, string groupId) {
            this.RequireSuperuser();

            // Init
            var entity = this.DB.AccessPolicies().GetByPublicId(publicId);
            var group = this.DB.AccessGroups().Include("AccessPolicies").GetByPublicId(groupId);
            var enable = this.Params.Bool("value",false);
            if(enable) {
                group.AddPolicy(entity);
            } else {
                group.RemovePolicy(entity);
            }
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("toggle-success");
        }

        [RequestHandler("/api/admin/security/access-policy/create")]
        public void Create() {
            this.RequireSuperuser();

            // Create the new user
            var entity = new AccessPolicy();
            entity.Populate(this,new FormBuilder(Forms.Admin.CREATE));
            entity.Validate();
            this.DB.AccessPolicies().Add(entity);
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("create-success", new {
                Policy = new {
                    Name = entity.Name,
                    ARN = entity.ARN,
                    PublicId = entity.PublicId
                }
            });
        }
        
        [RequestHandler("/api/admin/security/access-policy/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireSuperuser();

            // Make changes and save
            var entity = this.DB.AccessPolicies().GetByPublicId(publicId);
            entity.Populate(this,new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success");
        }
        
    }
}
