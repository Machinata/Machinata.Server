
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Charts;

namespace Machinata.Module.Admin.Handler {


    public class SecurityAPIHandler : AdminAPIHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("security");
        }

        #endregion

        [RequestHandler("/api/admin/security/standard-policies/rebuild")]
        public void RebuildStandardPolicies() {
            RequireSuperuser();

            AccessPolicy.RebuildStandardPoliciesUsingPolicyProviders(this.DB);
            this.DB.SaveChanges();
            AccessGroup.AutomaticallyAddStandardPoliciesToGroups(this.DB);
            this.DB.SaveChanges();

            // Inform others
            AccessPolicy.FirePoliciesAndGroupsRebuilt(this.DB);

            this.SendAPIMessage("success");
        }

        [RequestHandler("/api/admin/security/chart/sankey/network")]
        public void SankeySecurityNetwork() {
            
            var sankey = new SankeyChartData();

            var policies = this.DB.AccessPolicies().Include("Users").ToList();
            var groups = this.DB.AccessGroups().Include("AccessPolicies").Include("Users").ToList();
            var users = this.DB.Users().HideFrontendCreationUsers().ToList();
            
            // Nodes
            foreach(var entity in policies) {
                sankey.Nodes.Add(new SankeyChartDataNode(){ Name = entity.Name, UID = entity.UniversalId, Comment = entity.TypeName });
            }
            foreach(var entity in groups) {
                sankey.Nodes.Add(new SankeyChartDataNode(){ Name = entity.Name, UID = entity.UniversalId, Comment = entity.TypeName });
            }
            foreach(var entity in users) {
                sankey.Nodes.Add(new SankeyChartDataNode(){ Name = entity.Name, UID = entity.UniversalId, Comment = entity.TypeName });
            }
            // Links (Policy > Group)
            foreach(var group in groups) {
                foreach(var policy in group.AccessPolicies) {
                    var sourceNode = sankey.Nodes.Where(n => n.UID == policy.UniversalId).SingleOrDefault();
                    var targetNode = sankey.Nodes.Where(n => n.UID == group.UniversalId).SingleOrDefault();
                    double val = 1;
                    sankey.Links.Add(new SankeyChartDataLink() { Source = sankey.Nodes.IndexOf(sourceNode), Target = sankey.Nodes.IndexOf(targetNode), Value = val });
                }
            }
            // Links (Group > User)
            foreach(var group in groups) {
                foreach(var user in group.Users) {
                    var sourceNode = sankey.Nodes.Where(n => n.UID == group.UniversalId).SingleOrDefault();
                    var targetNode = sankey.Nodes.Where(n => n.UID == user.UniversalId).SingleOrDefault();
                    double val = 1;
                    sankey.Links.Add(new SankeyChartDataLink() { Source = sankey.Nodes.IndexOf(sourceNode), Target = sankey.Nodes.IndexOf(targetNode), Value = val });
                }
            }
            // Links (Policy > User)
            foreach(var policy in policies) {
                foreach(var user in policy.Users) {
                    var sourceNode = sankey.Nodes.Where(n => n.UID == policy.UniversalId).SingleOrDefault();
                    var targetNode = sankey.Nodes.Where(n => n.UID == user.UniversalId).SingleOrDefault();
                    double val = 1;
                    sankey.Links.Add(new SankeyChartDataLink() { Source = sankey.Nodes.IndexOf(sourceNode), Target = sankey.Nodes.IndexOf(targetNode), Value = val });
                }
            }
            

            SendAPIMessage("sankey-data", sankey);
        }
        
        
    }
}
