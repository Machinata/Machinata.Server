using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class SandboxAPIHandler : AdminAPIHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("sandbox");
        }

        #endregion

        [RequestHandler("/api/admin/sandbox/echo")]
        public void Echo() {
            Dictionary<string, object> req = new Dictionary<string, object>();
            int id = 0;
            foreach(string key in this.Context.Request.Form.Keys) {
                req.Add($"Form_{id++}[{key}]", this.Context.Request.Form[key]);
            }
            foreach(string key in this.Context.Request.QueryString.Keys) {
                req.Add($"QueryString_{id++}[{key}]", this.Context.Request.QueryString[key]);
            }
            foreach(string key in this.Context.Request.Files.Keys) {
                req.Add($"Files_{id++}[{key}]", this.Context.Request.Files[key].FileName + $" ({this.Context.Request.Files[key].ContentLength} bytes)");
            }
            foreach (string key in this.Context.Request.Cookies.Keys) {
                req.Add($"Cookies_{id++}[{key}]", this.Context.Request.Cookies[key].Value + $" ({this.Context.Request.Cookies[key].Path})");
            }
            foreach (string key in this.Context.Request.Headers.Keys) {
                req.Add($"Headers_{id++}[{key}]", this.Context.Request.Headers[key] + $" ({this.Context.Request.Headers[key]})");
            }
            SendAPIMessage("echo", req);
        }
        
        [RequestHandler("/api/admin/sandbox/delay")]
        public void Delay() {
            System.Threading.Thread.Sleep(this.Params.Int("seconds",2)*1000);
            Echo();
        }


        [RequestHandler("/api/admin/sandbox/chart", AccessPolicy.PUBLIC_ARN)]
        public void Chart() {

            // Access
            this.RequireLoginExceptAuthSecret();


            var node = new Reporting.Model.DonutNode();
            node.Theme = "default";
            var random = new Random(123);
            node.LoadSampleData(random);
            SendAPIMessage("node-data", JObject.FromObject(node));
        }

        [RequestHandler("/api/admin/sandbox/report", AccessPolicy.PUBLIC_ARN)]
        public void Report() {

            // Access
            this.RequireLoginExceptAuthSecret();

            var report = new Reporting.Model.Report();
            var random = new Random(123);

            {
                var layout = new Reporting.Model.LayoutNode();
                layout.Style = "CS_W_Two_Columns";
                report.AddToBody(layout);
                {
                    var node = new Reporting.Model.DonutNode();
                    node.MainTitle = "Donut 1";
                    node.Theme = "default";
                    node.LoadSampleData(random);
                    layout.AddChildToSlot(node,"{left}");
                }
                {
                    var node = new Reporting.Model.DonutNode();
                    node.MainTitle = "Donut 2";
                    node.Theme = "default";
                    node.LoadSampleData(random);
                    layout.AddChildToSlot(node, "{right}");
                }
            }

            SendAPIMessage("node-data", JObject.FromObject(report));
        }

        [RequestHandler("/api/admin/sandbox/errors/api")]
        public void APIError() {
            throw new Exception("This is an unknown test exception.");
            SendAPIMessage("test", "success");
        }

    }
}
