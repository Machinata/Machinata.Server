
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;

namespace Machinata.Module.Admin.Handler {


    public class AccountAPIHandler : AdminAPIHandler {

        [RequestHandler("/api/admin/account/login", Core.Model.AccessPolicy.PUBLIC_ARN)]
        public void Login() {
            string username = this.Params.String("username");
            string password = this.Params.String("password");
            string duration = this.Params.String("duration");

            var token = Core.Model.User.LoginUserWithUsernameAndPassword(this.DB, this.Context, username, password, requireAdminAccount: true ,duration);


            SendAPIMessage("login-success", new {
                AuthToken = new {
                    Hash = token.Hash,
                    Created = token.Created,
                    Expires = token.Expires
                }
            });
        }

        [RequestHandler("/api/admin/account/logout", Core.Model.AccessPolicy.PUBLIC_ARN)]
        public void Logout() {
            // Make sure we have a authtoken
            if (this.AuthToken == null) throw new BackendException("already-logged-out", "You are already logged out.");

            // Disable this auth token
            this.AuthToken.Valid = false;
            this.DB.SaveChanges();

            SendAPIMessage("logout-success", new {
                AuthToken = new {
                    Hash = this.AuthToken.Hash,
                }
            });
        }

        [RequestHandler("/api/admin/account/generate-two-factor-setup-code")]
        public void GenerateTwoFactorSetupCode() {
            this.RequireAdmin();

            this.User.PrepareAccountForTwoFactorAuthentication();
            var code = this.User.GenerateAuthenticationSetupCode();
            var link = this.User.GenerateAuthenticationQRCodeLink();
            this.DB.SaveChanges();


            SendAPIMessage("setup-code", new {
                SetupCode = code,
                QRCodeLink = link
            });
        }

        [RequestHandler("/api/admin/account/enable-two-factor-authentication")]
        public void EnableTwoFactorAuthentication() {
            this.RequireAdmin();

            var pin = this.Params.String("pin");

            var valid = this.User.ValidateAuthenticationPIN(pin);
            if (valid == false) throw new BackendException("two-factor-authentication-pin-invalid","The PIN is not valid. Please re-scan the QR-Code and try again.");

            this.User.TwoFactorStatus = User.TwoFactorAuthenticationStatuses.EnabledAndSetup;
            this.DB.SaveChanges();

            SendAPIMessage("success");
        }


        [RequestHandler("/api/admin/account/edit")]
        public void Edit() {
            this.RequireWriteARN();

            // Make changes and save
            // Note: form must that in /api/account/edit and /admin/account/edit
            var entity = this.User;
            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT).Exclude(nameof(User.Name)).Exclude(nameof(User.Username)).Exclude(nameof(User.Enabled)));
            entity.Validate();
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("edit-success", new {
                User = new {
                    Username = entity.Username,
                    PublicId = entity.PublicId
                }
            });
        }


        [RequestHandler("/api/admin/account/change-password")]
        public void ChangePassword() {
            this.RequireWriteARN();

            // Update and save
            this.User.ChangePassword(this.DB, this.Params.String("password"), this.AuthToken);
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("change-password-success", new {
                User = new {
                    Username = this.User.Username,
                    PublicId = this.User.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/account/activate", AccessPolicy.PUBLIC_ARN)]
        public void ActivateAccount() {

            User user = this.DB.Users().GetByUsername(this.Params.String("username"));
            var code = this.Params.String("code");
            var resetCode = this.Params.String("reset-code");
            var name = this.Params.String("name");
            var password = this.Params.String("password");
            var passwordVerify = this.Params.String("password_verify");
            var terms = this.Params.BoolNullable("terms", null);

            // Password
            if (password != passwordVerify) throw new BackendException("invalid-password", "Sorry, your passwords do not match.");

            // Check terms if available
            if (terms != null && terms == false) {
                throw new LocalizedException(this, "account.activation.terms-unchecked");
            }

            // Reset Code Check
            user.CheckResetCode(resetCode, code, this);

            // Name 
            if (!string.IsNullOrEmpty(name)) {
                user.Name = name;
            }

            // Set password 
            user.ChangePassword(this.DB, password);

            // Activate user
            user.Enabled = true;

            // Set IP
            user.Settings[User.SETTING_KEY_IP] = Core.Util.HTTP.GetRemoteIP(this.Context);

            // Update reset codes
            user.Settings[User.PASSWORD_RESET_CODE_EXPIRATION_KEY] = null;
            user.Settings[User.PASSWORD_RESET_CODE_KEY] = null;
            user.Settings[User.USER_ACTIVATION_DATE_KEY] = DateTime.UtcNow;

            // Save
            this.DB.SaveChanges();

            // Send Admin Email
            Core.Messaging.MessageCenter.SendMessageToAdminEmail("User Activated", $"User '{user.Username}' has been activated", this.PackageName);

            // Create a new authtoken and send as api message
            var token = user.CreateAuthToken(this.Context);
            this.DB.SaveChanges();

            var message = _createMessageForAuthToken(token, Core.Config.AccountNewUserActivationSuccess);

            SendAPIMessage("login-success", message);
        }


        [RequestHandler("/api/admin/account/reset-password", AccessPolicy.PUBLIC_ARN)]
        public void ResetPassword() {

            // Validate request
            User user = this.DB.Users().GetByUsername(this.Params.String("username"));
            var code = this.Params.String("code");
            var resetCode = this.Params.String("reset-code");
            var password = this.Params.String("password");
            var passwordVerify = this.Params.String("password_verify");
            user.CheckResetCode(resetCode, code, this);
            if (password != passwordVerify) throw new BackendException("invalid-password", "Sorry, your passwords do not match.");

            // Set password 
            user.ChangePassword(this.DB, password);

            //Activate user
            user.Enabled = true;

            // Create a new authtoken and send as api message
            var token = user.CreateAuthToken(this.Context);
            this.DB.SaveChanges();
            SendAPIMessage("login-success", _createMessageForAuthToken(token));
        }


        [RequestHandler("/api/admin/account/request-password-reset", AccessPolicy.PUBLIC_ARN)]
        public void RequestPasswordReset() {

            if (Core.Config.AccountUsernameUsesEmail) {
                // Init
                var email = this.Params.String("email");
                var username = this.Params.String("username");
                if (string.IsNullOrEmpty(username)) username = email;

                // Universal login user?
                if (User.IsUniversalLoginUser(username)) {
                    var endpoint = User.GetUniversalLoginEndpointForUser(username);
                    throw new LocalizedException(this, "cannot-reset-universal-login-user", username, endpoint);
                }

                // Get user send reset email
                var user = this.DB.Users().GetByUsername(this, username);
                user.SendPasswordResetEmail(this.DB, this.Language);
                this.SendAPIMessage("request-success");
            } else {
                //TODO: @micha
                throw new NotImplementedException();
            }
        }

        [RequestHandler("/api/admin/account/request-sync", AccessPolicy.PUBLIC_ARN)]
        public void AccountSync() {
            if (!Core.Config.UniversalLoginEnabled) throw new BackendException("universal-login-disabled", "Universal login is not enabled on this system.");

            // Init
            var username = this.Params.String("username");

            // Prevalidations
            if (User.IsUniversalLoginUser(username) == false) {
                throw new BackendException("user-not-universal-user", "The user is not a universal user.");
            }

            // Load user
            var user = this.DB.Users().Include(nameof(User.AuthTokens)).GetByUsername(username, false);
            if (user == null) {
                // User doesnt exist locally, just ignore
                this.SendAPIMessage("sync-ignored");
                return;
            }

            // Validate the sync key
            var providerSynckey = this.Params.String("synckey");
            var userSynckey = user.Settings[User.USER_UNIVERSAL_LOGIN_SYNCKEY_KEY] as string;
            if (providerSynckey != userSynckey || userSynckey == null || providerSynckey == null) {
                throw new BackendException("synckey-invalid", "The synckey is not valid.");
            }

            // Sync user
            var syncData = Core.JSON.ParseJsonAsJObject(this.Params.String("sync-data"));
            var tokenData = Core.JSON.ParseJsonArrayAsJObjects(this.Params.String("token-data"));
            // Sync properties
            user.SyncUniversalLoginPropertiesWithJSON(this.DB, syncData);
            // Sync tokens
            //TODO:
            // Save!
            this.DB.SaveChanges();
            this.SendAPIMessage("sync-success");

        }



        [RequestHandler("/api/account/create", AccessPolicy.PUBLIC_ARN)]
        public void AccountCreate() {

            // Make sure this feature is enabled
            if (!Core.Config.AccountAllowGuestCreations) throw new BackendException("feature-disabled", "Guest creations on this website are not enabled.");

            // Validate matching passwords
            var password = this.Params.String("password");
            var passwordConfirm = this.Params.String("password_confirm");
            if (password != passwordConfirm) throw new BackendException("invalid-password", "The passwords do not match. Please make sure to enter the same password for each input.");

            // Init new user
            var newUser = new User();
            newUser.Email = this.Params.String("email");
            newUser.Username = this.Params.String("username");
            if (Core.Config.AccountUsernameUsesEmail) {
                if (newUser.Email != null && newUser.Username == null) {
                    // Set username from email
                    newUser.Username = newUser.Email;
                } else if (newUser.Username != null && newUser.Email == null) {
                    // Set email from username
                    newUser.Email = newUser.Username;
                } else {
                    newUser.Username = newUser.Email;
                }
            }

            // IP
            newUser.Settings[User.SETTING_KEY_IP] = Core.Util.HTTP.GetRemoteIP(this.Context);

            // Language
            newUser.Settings[User.USER_LANGUAGE_KEY] = this.Language;

            newUser.Enabled = true;

            // Register Groups
            foreach (var groupName in Core.Config.AccountGuestCreationGroups) {
                var group = this.DB.AccessGroups().Where(g => g.Name == groupName).SingleOrDefault();
                if (group == null) throw new BackendException("invalid-group", $"The access group '{groupName}' does not exist.");
                newUser.AddToAccessGroup(group);
            }

            // Validate
            newUser.Validate();

            // Save so we can get the secret codes that rely on the db id
            this.DB.Users().Add(newUser);
            this.DB.SaveChanges();

            // Regiser password (we can only do this after creating it in the db)
            newUser.Password = password;
            this.DB.SaveChanges();

            // User Activation Email
            //newUser.SendUserActivationEmail(this.DB);

            // Send Admin Email
            //Core.Messaging.MessageCenter.SendMessageToAdminEmail("User Created", $"User '{newUser.Username}' has been created by '{this.User.Username}'", this.PackageName);

            // All good!
            // Create the authtoken and send as api message
            var token = newUser.CreateAuthToken(this.Context);
            this.DB.SaveChanges();
            SendAPIMessage("create-success", _createMessageForAuthToken(token));
        }


        #region Core Login APIs

       

        

        #endregion


        #region Helper Methods

        private static object _createMessageForAuthToken(AuthToken token, string redirect = null) {
            return new {
                AuthToken = new {
                    Hash = token.Hash,
                    Created = token.Created,
                    Expires = token.Expires
                },
                Redirect = redirect
            };
        }

        #endregion

    }
}
