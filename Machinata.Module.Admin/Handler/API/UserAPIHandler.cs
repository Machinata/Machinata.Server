
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;

namespace Machinata.Module.Admin.Handler {


    public class UserAPIHandler : AdminAPIHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("users");
        }

        #endregion

        [RequestHandler("/api/admin/users/user/{publicId}")]
        public void GetUser(string publicId) {
            throw new NotImplementedException();
        }

        [RequestHandler("/api/admin/users/user/{publicId}/delete")]
        public void DeleteUser(string publicId) {
            this.RequireWriteARN();
            
            // Validate
            var entity = this.DB.Users().Include("AuthTokens").GetByPublicId(publicId);
            if(this.User == entity) {
                throw new BackendException("self-delete","Sorry, you can't delete yourself. Please login with a different account.");
            }
            
            // Disable all other auth tokens
            var tokensToDisable = this.DB.AuthTokens().Where(at => at.UserId == entity.Id);
            tokensToDisable.ToList().ForEach(at => at.Valid = false);

            // Inform others
            Core.Model.User.FireUserDeletedEvent(entity);

            // Delete and save
            this.DB.DeleteEntity(entity);
            this.DB.SaveChanges();

            // Admin Email
            Core.Messaging.MessageCenter.SendMessageToAdminEmail("User Deleted", $"User '{entity.Username}' has been deleted by '{this.User.Username}'",this.PackageName);
            SendAPIMessage("delete-success");
        }

        [RequestHandler("/api/admin/users/user/{publicId}/group/{groupId}/toggle")]
        public void ToggleGroup(string publicId, string groupId) {
            this.RequireWriteARN();

            // Init
            var entity = this.DB.Users().Include("AccessGroups").GetByPublicId(publicId);
            var group = this.DB.AccessGroups().GetByPublicId(groupId);
            var enable = this.Params.Bool("value",false);
            if(enable) {
                entity.AddToAccessGroup(group);
            } else {
                entity.RemoveFromAccessGroup(group);
            }
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("toggle-success");
        }

        [RequestHandler("/api/admin/users/user/{publicId}/policy/{policyId}/toggle")]
        public void TogglePolicy(string publicId, string policyId) {
            this.RequireSuperuser();

            // Init
            var entity = this.DB.Users().Include("AccessPolicies").GetByPublicId(publicId);
            var policy = this.DB.AccessPolicies().GetByPublicId(policyId);
            var enable = this.Params.Bool("value",false);
            if(enable) {
                entity.AddPolicy(policy);
            } else {
                entity.RemovePolicy(policy);
            }
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("toggle-success");
        }

        [RequestHandler("/api/admin/users/user/{publicId}/business/{policyId}/toggle")]
        public void ToggleBusiness(string publicId, string policyId) {
            this.RequireWriteARN();

            // Init
            var entity = this.DB.Users().Include("Businesses").GetByPublicId(publicId);
            var policy = this.DB.Businesses().GetByPublicId(policyId);
            var enable = this.Params.Bool("value",false);
            if(enable) {
                entity.Businesses.Add(policy);
            } else {
                entity.Businesses.Remove(policy);
            }
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("toggle-success");
        }

        [RequestHandler("/api/admin/users/create")]
        public void CreateUser() {
            this.RequireWriteARN();
            var newUser = new User();

            // Username == Email
            newUser.Username = this.Params.String("email");
            newUser.Email = this.Params.String("email");
            newUser.Name = this.Params.String("name");

            // Groups
            var groups = this.DB.AccessGroups().GetByPublicIds(this.Params.StringArray("selected", new string[] { }));
            foreach (var group in groups) {
                newUser.AddToAccessGroup(group);
            }

            // Set IP
            newUser.Settings[User.SETTING_KEY_IP] = Core.Util.HTTP.GetRemoteIP(this.Context);

            // Validate
            newUser.Validate();

            // Save so we can get the secret codes that rely on the db id
            this.DB.Users().Add(newUser);
            this.DB.SaveChanges();


            // User Activation Email
            newUser.SendUserActivationEmail(this.DB);

            // Send Admin Email
            newUser.SendAdminNotificationEmail(this.User);


            // Return
            SendAPIMessage("create-success", new {
                User = new {
                    Username = newUser.Username,
                    PublicId = newUser.PublicId
                }
            });
        }

       

        [RequestHandler("/api/admin/users/user/{publicId}/edit")]
        public void EditUser(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var entity = this.DB.Users().GetByPublicId(publicId);
            var form = new FormBuilder(Forms.Admin.EDIT);
            if(Core.Config.AccountUsernameUsesEmail) {
                form = form.Exclude(nameof(User.Username));
            }
          
            entity.Populate(this,form);
            entity.Validate();
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("edit-success", new {
                User = new {
                    Username = entity.Username,
                    PublicId = entity.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/users/user/{publicId}/edit-parameters")]
        public void EditUserParameters(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var entity = this.DB.Users().GetByPublicId(publicId);
            var form = new FormBuilder(Forms.EMPTY);
            form.Include(nameof(User.Settings));
            

            entity.Populate(this, form);
            entity.Validate();
            this.DB.SaveChanges();

            // Return
            this.SendAPIMessage("edit-success", new {
                User = new {
                    Username = entity.Username,
                    PublicId = entity.PublicId
                }
            });
        }



        [RequestHandler("/api/admin/users/user/{publicId}/change-username")]
        public void ChangeUsername(string publicId)
        {
            this.RequireWriteARN();

            // Validate
            var entity = this.DB.Users().GetByPublicId(publicId);
            if (this.User.IsSuperUser)
            {
                // Super user... good
            }
            else
            {
                // Must be self
                if (this.User != entity)
                {
                    throw new Backend403Exception("insufficient-rights", "Sorry, only a superuser can change other account's usernames.", AccessPolicy.SUPERUSER_ARN);
                }
            }

            // Update and save
            entity.ChangeUsername(this.DB, this.Params.String("username"), this.AuthToken);
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("change-username-success", new
            {
                User = new
                {
                    Username = entity.Username,
                    PublicId = entity.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/users/user/{publicId}/change-password")]
        public void ChangePassword(string publicId) {
            this.RequireWriteARN();

            // Validate
            var entity = this.DB.Users().GetByPublicId(publicId);
            if(this.User.IsSuperUser) {
                // Super user... good
            } else {
                // Must be self
                if(this.User != entity) {
                    throw new Backend403Exception("insufficient-rights", "Sorry, only a superuser can change other account's passwords.", AccessPolicy.SUPERUSER_ARN);
                }
            }
            
            // Update and save
            entity.ChangePassword(this.DB, this.Params.String("password"), this.AuthToken);
            this.DB.SaveChanges();
            
            // Return
            SendAPIMessage("change-password-success", new {
                User = new {
                    Username = entity.Username,
                    PublicId = entity.PublicId
                }
            });
        }

        /// <summary>
        /// Sends a reset password email
        /// </summary>
        /// <param name="publicId">The public identifier.</param>
        [RequestHandler("/api/admin/users/user/{publicId}/reset-password")]
        public void ResetPassword(string publicId) {
            this.RequireWriteARN();

            // Validate
            var entity = this.DB.Users().GetByPublicId(publicId);

            // PW reset Email
            entity.SendPasswordResetEmail(this.DB);
           
            // Return
            SendAPIMessage("reset-password-success");
        }


        /// <summary>
        /// sends an activation email
        /// </summary>
        /// <param name="publicId">The public identifier.</param>
        [RequestHandler("/api/admin/users/user/{publicId}/send-activation")]
        public void ResendActivaiton(string publicId) {
            this.RequireWriteARN();

            // Validate
            var entity = this.DB.Users().GetByPublicId(publicId);

            // activation Email
            entity.SendUserActivationEmail(this.DB);

            // Return
            SendAPIMessage("resend-activation-success");
        }



        /// <summary>
        /// sends an an email with a message to the user
        /// </summary>
        /// <param name="publicId">The public identifier.</param>
        [RequestHandler("/api/admin/users/user/{publicId}/send-message")]
        public void SendMessage(string publicId) {
            this.RequireWriteARN();

            // Validate
            var entity = this.DB.Users().GetByPublicId(publicId);

            // message  email
            entity.SendMessage(this.DB, this.Params.String("message"));

            // Return
            this.SendAPIMessage("success");
        }

        /// <summary>
        /// sends an an rich text email with a message to the user
        /// </summary>
        /// <param name="publicId">The public identifier.</param>
        [RequestHandler("/api/admin/users/user/{publicId}/send-email")]
        public void SendEmail(string publicId) {
            this.RequireWriteARN();

            // Validate
            var entity = this.DB.Users().GetByPublicId(publicId);

            var language = entity.Language;

            var messageObject = new RichtTextEmailMessage();
          
            var form = new FormBuilder(Forms.Admin.EDIT);

            messageObject.Populate(this, form);

            var message = messageObject.Message;


            if (string.IsNullOrWhiteSpace(message)) {
                throw new BackendException("message-missing", $"No content for language '{language}' defined");
              
            }

            if (string.IsNullOrWhiteSpace(messageObject.Subject)) {
                throw new BackendException("subject-missing", $"No subject");

            }

            // Send  email
            entity.SendMessage(this.DB, message, messageObject.Subject, html: true, "account/email");

            // Return
            this.SendAPIMessage("success");
        }



    }
}
