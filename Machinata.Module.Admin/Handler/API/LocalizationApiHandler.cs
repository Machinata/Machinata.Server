
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;


using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Localization;

namespace Machinata.Module.Admin.Handler {


    public class LocalizationApiHandler : AdminAPIHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("config");
        }

        #endregion

        [RequestHandler("/api/admin/config/localization/translate")]
        public void Translate() {

            var text = this.Params.String("source-text");
            var sourceLanguage = this.Params.String("source-language");
            var destLanguage = this.Params.String("dest-language");
            List<string> translations = new List<string>();
            var debugInfo = "";

            {
                var provider = Services.NLP.NLPService.Default;
                var model = this.Params.String("model", provider.GetDefaultModel());
                var prompt = $"Translate the following from {sourceLanguage} to {destLanguage} and provide multiple variations, one per line: {text}";
                var temperature = this.Params.Double("temperature", provider.GetDefaultTemperature());
                var maxTokens = this.Params.Int("max-tokens", provider.GetDefaultMaxTokens());


                var ret = provider.GetTextCompletion(prompt, model, temperature, maxTokens);
                debugInfo += "Prompt=" + prompt;
                foreach (var translation in ret.Choices.First().Text.Trim().Split("\n")) {
                    var translationCleaned = translation.Trim();
                    for (var n = 0; n < 10; n++) {
                        translationCleaned = translationCleaned.ReplacePrefix($"{n}. ","");
                        translationCleaned = translationCleaned.ReplacePrefix($"{n}.","");
                    }
                    foreach (var c in "abcdefghijklmnopqrstuvwxyz") {
                        translationCleaned = translationCleaned.ReplacePrefix($"{c}. ", "");
                        translationCleaned = translationCleaned.ReplacePrefix($"{c}.", "");
                        translationCleaned = translationCleaned.ReplacePrefix($"{c}) ", "");
                        translationCleaned = translationCleaned.ReplacePrefix($"{c})", "");
                    }
                    translations.Add(translationCleaned);
                }
            }

            SendAPIMessage("success", new { Text = text, Translations = translations, DebugInfo=debugInfo});
        }

        [RequestHandler("/api/admin/config/localization/update")]
        public void Update() {
            this.RequireWriteARN();

            var textIds = this.Params.StringValuesAllowEmtpy("text-id", new string[] { }).ToList();
            var languages = this.Params.StringValuesAllowEmtpy("language", new string[] { }).ToList();
            var packages = this.Params.StringValuesAllowEmtpy("package", new string[] { }).ToList();
            var customValues = this.Params.StringValuesAllowEmtpy("custom-value", new string[] { }).ToList();

            for (int i = 0; i < textIds.Count(); i++) {

                var language = languages[i];
                var textId = textIds[i];
                var package = packages[i];
                var customValue = customValues[i];

                // existing entry
                var entity = LocalizationText.FindLocalizationText(this.DB, textId, language);

                if (string.IsNullOrEmpty(customValue) && entity != null) {
                    this.DB.LocalizationTexts().Remove(entity);

                    // Save
                    this.DB.SaveChanges();

                    // Unregister
                    Text.UnRegisterText(textId, language, Text.SourceTypes.Database);
                } else if (!string.IsNullOrEmpty(customValue) && entity == null) { 
                
                    // new
                    entity = new LocalizationText();
                    entity.Language = languages[i];
                    entity.TextId = textId;
                    entity.Package = package;
                    entity.Value = customValue;
                    entity.Validate();
                    this.DB.LocalizationTexts().Add(entity);
                    // Register
                    Text.RegisterText(entity.Source, package, textId, customValue, language, Text.SourceTypes.Database);
                } else if (entity != null && entity.Value != customValue) {

                    // Update
                    entity.Value = customValue;

                    // Update/Register
                    var currentDbValue = Text.GetTexts(textId, language).Last(t => t.SourceType == Text.SourceTypes.Database);
                    currentDbValue.Value = customValue;

                }

                // Save
                this.DB.SaveChanges();

                

            }

            // Clear bundle
            Core.Bundles.Bundle.CleanAllCacheBundles();

            // Success
            this.SendAPIMessage("update-success");
        }



    }
}
