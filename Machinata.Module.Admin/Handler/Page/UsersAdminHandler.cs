using Machinata.Core.Handler;
using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Util;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Machinata.Module.Admin.Handler {


    public class UsersAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("users");
        }

        #endregion

        #region Menu Item

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menuBuilder) {
            var section = new MenuSection {
                Icon = "account",
                Path = "/admin/users",
                Title = "{text.users}",
                Sort = "500"
            };
            menuBuilder.AddSection(section);

            // Menu: add users as submenu
            if (menuBuilder.CurrentPathStartsWith(section.Path)) {
                var maxItems = 5;
                //var users = menuBuilder.Handler.DB.Users().Active().ToList().Where(u => u.IsSuperUser == false);

                //// Number of nodes
                //var count = users.Count();

                //// Add as items
                //foreach (var user in users.OrderByDescending(o => o.Created).Take(maxItems).OrderBy(u =>u.Name)) {
                //    menuBuilder.Item(user.Name, user.PublicId, "/admin/users/user/" + user.PublicId);
                //}

                // Add Show all item
                //if (count > maxItems) {
                //    menuBuilder.Item("{text.all}", "all", "/admin/users/", null, classes: "not-selectable");
                //}
                if (true) {
                    menuBuilder.Item("{text.all}", "all", "/admin/users/all", null/*, classes: "not-selectable"*/);
                }

                // Add Show Create user
                if (true) {
                    menuBuilder.Item("{text.create}", "all", "/admin/users/create", null/*, classes: "not-selectable"*/);
                }
            }
        }

        #endregion

        [RequestHandler("/admin/users")]
        public void Default() {
            // Paginate and insert list
            var entities = this.DB.Users()
                .Active()
                .HideFrontendCreationUsers() // hide the 
                .ToList()
                .AsQueryable().OrderBy(e => e.Name);
            entities = this.Template.Filter(entities, this, new string[] { nameof(User.Name), nameof(User.Username), nameof(User.Email), nameof(User.SettingsData) });
            entities = this.Template.Paginate(entities, this, nameof(User.Name), "asc");
            this.Template.InsertEntityList(
                variableName: "entity-list", 
                entities: entities, 
                form: new FormBuilder(Forms.Admin.LISTING).Include(nameof(User.Created)), 
                link: "/admin/users/user/{entity.public-id}"
            );
            // Navigation
            this.Navigation.Add("users","{text.users}");
        }

        [RequestHandler("/admin/users/all")]
        public void All() {
            // Paginate and insert list
            var entities = this.DB.Users().ToList().AsQueryable().OrderBy(e => e.Name);
            entities = this.Template.Filter(entities, this, new string[] { nameof(User.Name), nameof(User.Username), nameof(User.Email), nameof(User.SettingsData) });
            entities = this.Template.Paginate(entities, this, nameof(User.Name), "asc");
            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: entities,
                form: new FormBuilder(Forms.Admin.LISTING).Include(nameof(User.Created)),
                link: "/admin/users/user/{entity.public-id}"
            );
            // Navigation
            this.Navigation.Add("users", "{text.users}");
            this.Navigation.Add("all", "{text.all-users}");
        }

        [RequestHandler("/admin/users/create")]
        public void Create() {
            this.RequireWriteARN();
            var form = new FormBuilder("empty").Include(nameof(User.Name)).Include(nameof(User.Email));
            var user = new User();
            this.Template.InsertForm(
               variableName: "form",
               entity:user,
               form: form,
               //  apiCall: "/api/portal/account/business/users/create",
               onSuccess: "{page.navigation.prev-path}"
            );

            // Groups
            this.Template.InsertSelectionList(
                variableName: "groups",
                entities: this.DB.AccessGroups(),
                form: new FormBuilder(Forms.Admin.LISTING),
                selectedEntities: user.AccessGroups );
            
            // Navigation
            this.Navigation.Add("users","{text.users}");
            this.Navigation.Add("create","{text.create}");
        }

        [RequestHandler("/admin/users/user/{publicId}")]
        public void View(string publicId) {
            // Get entity
            var entity = this.DB.Users().Include(nameof(this.Businesses)).GetByPublicId(publicId);
            // Create form
            var form = new FormBuilder(Forms.Admin.VIEW)
                    .IncludeStandard()
                    .Exclude(nameof(User.Settings) + "." + User.PASSWORD_RESET_CODE_KEY)
                    .Exclude(nameof(User.Settings) + "." + User.PASSWORD_RESET_CODE_EXPIRATION_KEY);
            // If the user has been activated, show
            if(entity.Settings.Keys.Contains(User.USER_ACTIVATION_DATE_KEY)) {
                form.Custom("ActivationDate", "activationdate", "datetime", entity.Settings[User.USER_ACTIVATION_DATE_KEY]);
            }
            // Insert variables
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList(
                variableName: "entity.property-list", 
                entity: entity, 
                form: form
            );
            // Show user activation hint plus activation URL if not yet activated
            if(entity.IsActivationPending == true) {
                this.Template.InsertVariable("activation-pending", "true");
                this.Template.InsertVariable("activation-path", Core.Config.AccountNewUserActivationPage);
                this.Template.InsertVariable("activation-code", entity.Settings[User.PASSWORD_RESET_CODE_KEY]?.ToString());
                this.Template.InsertVariable("secret-code", entity.GetSecretCode());
                this.Template.InsertVariable("username", entity.Username);
            } else {
                this.Template.InsertVariable("activation-pending", "false");
            }
            // Navigation
            this.Navigation.Add("users","{text.users}");
            this.Navigation.Add($"user/{publicId}",entity.Name,$"User {entity.Name}");
        }

        [RequestHandler("/admin/users/user/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();

            var entity = this.DB.Users().Include("Businesses").GetByPublicId(publicId);

            var form = new FormBuilder(Forms.Admin.EDIT);

            if (Core.Config.AccountUsernameUsesEmail) {
                form = form.Exclude(nameof(User.Username));
            } 

            this.Template.InsertVariables("entity", entity);
            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: form,
                apiCall: $"/api/admin/users/user/{entity.PublicId}/edit",
                onSuccess: $"/admin/users/user/{entity.PublicId}"
            );

            // Navigation
            this.Navigation.Add("users","{text.users}");
            this.Navigation.Add($"user/{publicId}",entity.Name,$"User {entity.Name}");
            this.Navigation.Add("edit","{text.edit}","{text.edit} "+entity.Name);
        }

        [RequestHandler("/admin/users/user/{publicId}/edit-parameters")]
        public void EditParameters(string publicId) {
            this.RequireWriteARN();

            var entity = this.DB.Users().Include("Businesses").GetByPublicId(publicId);
            this.Template.InsertVariables("entity", entity);

            var form = new FormBuilder(Forms.EMPTY);
            form.Include(nameof(User.Settings));


            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: form,
                apiCall: $"/api/admin/users/user/{entity.PublicId}/edit-parameters",
                onSuccess: $"/admin/users/user/{entity.PublicId}"
            );

            // Navigation
            this.Navigation.Add("users", "{text.users}");
            this.Navigation.Add($"user/{publicId}", entity.Name, $"User {entity.Name}");
            this.Navigation.Add("edit-parameters", "{text.edit-parameters}", "{text.edit-parameters} " + entity.Name);
        }


        /// <summary>
        /// Send rich text email
        /// </summary>
        /// <param name="publicId"></param>
        [RequestHandler("/admin/users/user/{publicId}/send-email")]
        public void SendEmail(string publicId) {
            this.RequireWriteARN();

            var entity = this.DB.Users().Include("Businesses").GetByPublicId(publicId);
            this.Template.InsertVariables("entity", entity);

            var form = new FormBuilder(Forms.Admin.EDIT);

            var customProperty = new CustomFormBuilderProperty(form);
            customProperty.Name = "message";
            customProperty.FormName = "message";
            customProperty.FormType = "text";
            customProperty.FormFormat = "html";
            customProperty.FormValue = "<p>tes<b>T</b></p>";
            form.Button("send", "send");
                               
            var messageEntity = new RichtTextEmailMessage();
         
            this.Template.InsertForm(
                variableName: "form",
                entity: messageEntity,
                form: form,
                apiCall: $"/api/admin/users/user/{entity.PublicId}/send-email",
                onSuccess: $"/admin/users/user/{entity.PublicId}"
            );

            // Navigation
            this.Navigation.Add("users", "{text.users}");
            this.Navigation.Add($"user/{publicId}", entity.Name, $"User {entity.Name}");
            this.Navigation.Add("send-email", "{text.send-email}", "{text.send-email} " + entity.Name + ": " + entity.Email);
        }

      
        [RequestHandler("/admin/users/user/{publicId}/groups")]
        public void Groups(string publicId) {
            // Init entity
            var entity = this.DB.Users().Include("AccessGroups").GetByPublicId(publicId);
            this.Template.InsertVariables("entity", entity);
            // Insert selection list
            var entities = this.Template.AutoSort(this.DB.AccessGroups(), this);
            this.Template.InsertSelectionList(
                variableName: "entity-list", 
                entities: entities, 
                selectedEntities: entity.AccessGroups.Cast<ModelObject>(), 
                selectionAPICall: "/api/admin/users/user/"+entity.PublicId+"/group/{entity.public-id}/toggle",
                form: new FormBuilder(Forms.Admin.SELECTION)
            );
            // Navigation
            this.Navigation.Add("users","{text.users}");
            this.Navigation.Add($"user/{publicId}",entity.Name,$"User {entity.Name}");
            this.Navigation.Add("groups","{text.groups}",entity.Name+" {text.groups}");
        }
        
        [RequestHandler("/admin/users/user/{publicId}/policies")]
        public void Policies(string publicId) {
            // Init entity
            var entity = this.DB.Users().Include("AccessPolicies").GetByPublicId(publicId);
            this.Template.InsertVariables("entity", entity);
            // Insert selection list
            var entities = this.Template.AutoSort(this.DB.AccessPolicies(), this);
            this.Template.InsertSelectionList(
                variableName: "entity-list", 
                entities: entities, 
                selectedEntities: entity.AccessPolicies.Cast<ModelObject>(), 
                selectionAPICall: "/api/admin/users/user/"+entity.PublicId+"/policy/{entity.public-id}/toggle",
                form: new FormBuilder(Forms.Admin.SELECTION)
            );
            // Navigation
            this.Navigation.Add("users","{text.users}");
            this.Navigation.Add($"user/{publicId}",entity.Name,$"User {entity.Name}");
            this.Navigation.Add("groups","{text.policies}",entity.Name+" {text.policies}");
        }

        [RequestHandler("/admin/users/user/{publicId}/businesses")]
        public void Businesses(string publicId) {
            // Init entity
            var entity = this.DB.Users().Include("Businesses").GetByPublicId(publicId);
            this.Template.InsertVariables("entity", entity);
            // Insert selection list
            var entities = this.Template.AutoSort(this.DB.ActiveBusinesses(), this, nameof(Business.Name), "asc");
            this.Template.InsertSelectionList(
                variableName: "entity-list", 
                entities: entities, 
                selectedEntities: entity.Businesses.Cast<ModelObject>(), 
                selectionAPICall: "/api/admin/users/user/"+entity.PublicId+"/business/{entity.public-id}/toggle",
                form: new FormBuilder(Forms.Admin.SELECTION)
            );
            // Navigation
            this.Navigation.Add("users", "{text.users}");
            this.Navigation.Add($"user/{publicId}", entity.Name, $"User {entity.Name}");
            this.Navigation.Add("businesses", "{text.businesses}", entity.Name + " {text.businesses}");
        }

    }


    public class RichtTextEmailMessage : ModelObject {

        [FormBuilder(Forms.Admin.EDIT)]
        [Required]
        public string Subject { get; set; }


        [FormBuilder(Forms.Admin.EDIT)]
        [CustomFormat("html")]
        public string Message { get; set; }
                
        
    }
}
