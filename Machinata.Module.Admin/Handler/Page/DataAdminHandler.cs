using Machinata.Core.Data;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class DataAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("data");
        }

        #endregion

        #region Menu 
        
        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "data",
                Path = "/admin/data",
                Title = "{text.data}",
                Sort = "500"
            });
        }

        #endregion

        [RequestHandler("/admin/data")]
        public void Default() {
            this.RequireSuperuser();
            // Menu items
            var menuItems = PageTemplate.Cache.FindAll(this.Template.Package, "admin/data/menu/menu.item.", this.TemplateExtension);
            this.Template.InsertTemplates("data.menu-items", menuItems);
            // Navigation
            this.Navigation.Add("data", "{text.data}");
        }
        
    }
}
