using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Admin.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class DashboardAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("dashboard");
        }

        #endregion

        #region Menu

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "dashboard",
                Path = "/admin/dashboard",
                Title = "{text.dashboard}",
                Sort = "100"
            });
        }

        #endregion

        [RequestHandler("/admin/dashboard")]
        public void Default() {
            // Dashboard items
            // DEPRECATED
            //var items = PageTemplate.Cache.FindAll(this.Template.Package, "default/dashboard.item.", this.TemplateExtension);
            //this.Template.InsertTemplates("dashboard-items", items);
            //this.Template.InsertVariable("chart-timespan", this.Params.String("timespan", "30d"));


            // Variables
            DashboardLoader.InsertVariables(this, this.Template);

            // Navigation
            this.Navigation.Add("dashboard","{text.dashboard}");
        }
        
    }
}
