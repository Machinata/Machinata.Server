using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class SandboxAdminHandler : AdminPageTemplateHandler {
        
        [RequestHandler("/admin/system/tools/sandbox")]
        public void Default() {
            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("tools","{text.tools}");
            this.Navigation.Add("sandbox","{text.tools-sandbox}");
        }

        [RequestHandler("/admin/system/tools/sandbox/icons")]
        public void Icons() {
            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("tools","{text.tools}");
            this.Navigation.Add("sandbox","{text.tools-sandbox}");
            this.Navigation.Add("icons","{text.sandbox-icons}");
        }

        [RequestHandler("/admin/system/tools/sandbox/ui-test")]
        public void UITest() {
            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("tools","{text.tools}");
            this.Navigation.Add("sandbox","{text.tools-sandbox}");
            this.Navigation.Add("ui-test","{text.sandbox-ui-test}");
        }

        [RequestHandler("/admin/system/tools/sandbox/form-test")]
        public void FormTest() {
            this.Template.InsertForm(
                variableName: "simple-form",
                form: new FormBuilder()
                    .Custom("Text", "text", "text", null)
                    .Custom("Date", "date", "date", null)
                    .Custom("DateTime", "datetime", "datetime", null)
                    .Custom("DateRange", "daterange", "daterange", null)
                    .Custom("Is Default", "isdefault", "bool", null)
                    .Custom("Password", "password", "password", null),
                apiCall: "/api/admin/sandbox/echo",
                onSuccess: "Machinata.echo"
            );
            this.Template.InsertForm(
                variableName: "file1-form",
                form: new FormBuilder()
                    .Custom("File", "file", "file", null)
                    .Custom("Text", "text", "text", null),
                apiCall: "/api/admin/sandbox/echo",
                onSuccess: "Machinata.echo"
            );
            this.Template.InsertForm(
                variableName: "file2-form",
                form: new FormBuilder()
                    .Custom("File1", "filea", "file", null)
                    .Custom("File2", "fileb", "file", null)
                    .Custom("Text", "text", "text", null),
                apiCall: "/api/admin/sandbox/echo",
                onSuccess: "Machinata.echo"
            );
            var radiosAndCheckboxes = new List<KeyValuePair<string, string>>();
            radiosAndCheckboxes.Add(new KeyValuePair<string, string>( "Entity 1", "e1"));
            radiosAndCheckboxes.Add(new KeyValuePair<string, string>("Entity 2", "e2"));
            radiosAndCheckboxes.Add(new KeyValuePair<string, string>("Entity 3", "e3"));
            radiosAndCheckboxes.Add(new KeyValuePair<string, string>("Entity 4", "e4"));
            this.Template.InsertForm(
                variableName: "radio1-form",
                form: new FormBuilder()
                    .RadioList("Radio", "radios", radiosAndCheckboxes,"e2")
                    .CheckboxList("Checkbox", "checkboxes", radiosAndCheckboxes,"e2,e4")
                    .Custom("Text", "text", "text", null)
                    .Custom("Published", "published", "bool", null),
                apiCall: "/api/admin/sandbox/echo",
                onSuccess: "Machinata.echo"
            );
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("tools","{text.tools}");
            this.Navigation.Add("sandbox","{text.tools-sandbox}");
            this.Navigation.Add("form-test","{text.sandbox-form-test}");
        }

        [RequestHandler("/admin/system/tools/sandbox/dialogs")]
        public void Dialogs() {
            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("tools","{text.tools}");
            this.Navigation.Add("sandbox","{text.tools-sandbox}");
            this.Navigation.Add("dialogs","{text.sandbox-dialogs}");
        }

        [RequestHandler("/admin/system/tools/sandbox/cards")]
        public void Cards() {
            // Generate cards
            var cards = new List<Core.Cards.CardBuilder>();
            cards.Add(this.DB.Users().FirstOrDefault().VisualCard());
            cards.Add(this.DB.AuthTokens().FirstOrDefault().VisualCard());
            this.Template.InsertCards("cards",cards);
            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("tools","{text.tools}");
            this.Navigation.Add("sandbox","{text.tools-sandbox}");
            this.Navigation.Add("cards","{text.sandbox-cards}");
        }

    }
}
