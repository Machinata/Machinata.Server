using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class ThemesAdminHandler : AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("config");
        }

        #endregion

        public override void InsertAdditionalVariables() {
            base.InsertAdditionalVariables();
        }

        [RequestHandler("/admin/config/themes")]
        public void Default() {

            // List all
            this.Template.InsertEntityList(
                variableName: "entities",
                entities: Theme.GetCachedThemes().AsQueryable(),
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/config/themes/theme/{entity.name}"
            );


            // Navigation
            this.Navigation.Add("config", "{text.config}");
            this.Navigation.Add("themes", "{text.themes}");
        }

        [RequestHandler("/admin/config/themes/theme/{publicId}")]
        public void ViewTheme(string publicId) {
            Theme entity = Theme.GetCachedThemeByName(publicId);

            if (entity == null) {
                throw new Backend404Exception("theme-not-found", "The theme could not be found or loaded");
            }

            this.Template.InsertPropertyList("entity.properties", entity, new FormBuilder(Forms.Admin.VIEW));
            this.Template.InsertVariables("entity", entity);

            // Variables
            this.Template.InsertPropertyList("entity.variables", entity, new FormBuilder(Forms.EMPTY).Include(nameof(Theme.Variables)));
            var hasVariables = entity.Variables?.Keys.Any();
            this.Template.InsertVariable("has-variables", hasVariables);


            // Navigation
            this.Navigation.Add("config", "{text.config}");
            this.Navigation.Add("themes", "{text.themes}");
            this.Navigation.Add($"theme/{publicId}", entity.Title);
        }

    }
}
