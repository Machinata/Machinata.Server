using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class SeedAdminHandler : AdminPageTemplateHandler {


        /// <summary>
        /// Special public ARN caller to poke the database which will cause a seed/migration if need be.
        /// TODO: @dan: should this be in core?
        /// </summary>
        [RequestHandler(Core.Model.ModelSeed.SEED_DB_STATUS_ROUTE, Core.Model.AccessPolicy.PUBLIC_ARN)]
        public void DBStatus() {
            // Query the db to poke if it is here...
            this.DB.Database.Initialize(true);
        }

        [RequestHandler("/admin/data/seed")]
        public void Default() {
            this.RequireSuperuser();
            // All tables
            var templates = new List<PageTemplate>();
            foreach (var dataset in Core.Config.DatabaseSeedDatasets) {
                // Init
                var template = this.Template.LoadTemplate("dataset-list.item");
                var datasetId = dataset;
                datasetId = datasetId.Replace("DATASET_ALL",Core.Model.SeedDatasets.DATASET_ALL);
                datasetId = datasetId.Replace("DATASET_DUMMY",Core.Model.SeedDatasets.DATASET_DUMMY);
                datasetId = datasetId.Replace("DATASET_INITIAL",Core.Model.SeedDatasets.DATASET_INITIAL);
                datasetId = datasetId.Replace("DATASET_RANDOM",Core.Model.SeedDatasets.DATASET_RANDOM);
                datasetId = datasetId.Replace("DATASET_REQUIRED",Core.Model.SeedDatasets.DATASET_REQUIRED);
                datasetId = datasetId.Replace("DATASET_THEMES",Core.Model.SeedDatasets.DATASET_THEMES);
                // Already seeded?
                bool alreadySeeded = this.DB.SeedLogs().Count(sl => sl.SeedType.StartsWith("Project") && sl.Dataset == datasetId) > 0;
                // Variables
                template.InsertVariable("dataset.id", datasetId);
                template.InsertVariable("dataset.icon", alreadySeeded?"tick":"none");
                template.InsertTextForLanguage("dataset.name", "dataset-"+datasetId);
                templates.Add(template);
            }
            this.Template.InsertTemplates("datasets", templates);
            // Logs
            this.Template.InsertEntityList("seed-logs", this.DB.SeedLogs().OrderByDescending(sl => sl.Id), new FormBuilder(Forms.Admin.LISTING).Include("Created"));
            // Navigation
            this.Navigation.Add("data","{text.data}");
            this.Navigation.Add("seed","{text.data-seed}");
        }
        
        

    }
}
