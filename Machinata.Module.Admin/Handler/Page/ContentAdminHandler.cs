using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Admin.Handler {


    public class ContentAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("content");
        }

        #endregion

        #region Menu

        //[MenuBuilder]
        //public static void GetMenu(MenuBuilder menu) {
        //    menu.Section("{text.content}", "content", "/admin/content", null, "document");
        //}

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menuBuilder) {
            var section = new MenuSection {
                Icon = "document",
                Path = "/admin/content",
                Title = "{text.content}",
                Sort = "500",
                AutoSelectionRequiresFullMatch = false
            };

            menuBuilder.AddSection(section);



            // Menu: add sections as submenu
            if (menuBuilder.CurrentPathStartsWith(section.Path)){
                //var maxItems = 5;
                //var root = menuBuilder.Handler.DB.ContentNodes()
                //    .Include(nameof(ContentNode.Children))
                //    .GetRoot();

                //// All first level nodes
                //IEnumerable<ContentNode> nodes = root.ChildrenForType(ContentNode.NODE_TYPE_NODE).Published().Where(n => n.Path != ContentNode.ENTITIES_PATH_NAME && n.Path != ContentNode.TRASH_PATH_NAME);

                //// Number of nodes
                //var count = nodes.Count();

                //// Add as items
                //foreach (var node in nodes.OrderBy(o => o.Sort).Take(maxItems)) {
                //    menuBuilder.Item(node.Name, node.PublicId, "/admin/content/page" + node.Path);
                //}

                //// Add Show all item
                //if (count > maxItems) {
                //    menuBuilder.Item("{text.all}", "all", "/admin/content/", null, classes: "not-selectable");
                //}

                // Add Show all item
              
                // Templates
                menuBuilder.Item("{text.templates}", "templates", "/admin/content/page/Templates", null);
                menuBuilder.Item("{text.layouts}", "layoutes-page", "/admin/content/layouts", null);

            }
        }

        #endregion

        #region Helper Methods

        public void AddNavigationForPath(string path) {
            this.Template.InsertVariable("node.path", path);
            this.Navigation.Add("content","{text.content}");
            if(path != null && path != ContentNode.NODE_PATH_SEP) {
                path = path.TrimStart(ContentNode.NODE_PATH_SEP[0]);
                var segs = path.Split(ContentNode.NODE_PATH_SEP[0]);
                for(int i = 0; i < segs.Length; i++) {
                    var seg = segs[i];
                    this.Navigation.Add((i==0?"page/":"")+seg,seg);
                }
            }
        }

        #endregion

        [RequestHandler("/admin/content")]
        public void Default() {
            // Makes sure a trash node exists (when starting to use cms)
            {
                var created = ContentNode.CreateTrashNodeIfMissing(this.DB);
                if (created == true) {
                    this.DB.SaveChanges();
                }
            }
            


            // Get node
            ContentNode node = this.DB.ContentNodes().Include("Children").GetRoot();
            // All root pages
            {
                var templates = new List<PageTemplate>();
                foreach (var page in node.Children.Where(n => n.IsSystemNode == false).OrderBy(n => n.Sort)) {
                    var template = this.Template.LoadTemplate("page-list.item");
                    template.InsertVariable("page.name", page.Name);
                    template.InsertVariable("page.short-url", page.ShortURL);
                    template.InsertVariable("page.path", page.Path);
                    template.InsertVariable("page.public-id", page.PublicId);
                    template.InsertVariable("page.summary-short", page.SummaryShort);
                    template.InsertVariable("page.icon", "folder-open");
                    template.InsertVariable("page.published", page.Published);
                    template.InsertVariable("page.subpages-count", this.DB.ContentNodes().Count(n => n.ParentId == page.Id && n.NodeType == ContentNode.NODE_TYPE_NODE));
                    templates.Add(template);
                }
                this.Template.InsertTemplates("pages", templates);
            }

            // Variables
            this.Template.InsertVariables("node", node);

            // Navigation
            this.AddNavigationForPath("/");
        }
        
        [RequestHandler("/admin/content/page/{path}")]
        public void Page(string path) {
            
            // Get node
            ContentNode node = this.DB.ContentNodes()
                .Include("Children")
                .Include("Children.Children")
                .GetNodeByPath(path);
            
            // Sub-pages
            {
                var templates = new List<PageTemplate>();
                foreach (var page in node.ChildrenForType(ContentNode.NODE_TYPE_NODE).OrderBy(n => n.Sort)) {
                    var template = this.Template.LoadTemplate("page-list.item");
                    template.InsertVariable("page.name", page.Name);
                    template.InsertVariable("page.short-url", page.ShortURL);
                    template.InsertVariable("page.path", page.Path);
                    template.InsertVariable("page.public-id", page.PublicId);
                    template.InsertVariable("page.summary-short", page.SummaryShort);
                    template.InsertVariable("page.icon", "folder-open");
                    
                    template.InsertVariable("page.published", page.Published);
                    template.InsertVariable("page.subpages-count", this.DB.ContentNodes().Count(n => n.ParentId == page.Id && n.NodeType == ContentNode.NODE_TYPE_NODE));
                    templates.Add(template);
                }
                this.Template.InsertTemplates("pages", templates);
                this.Template.InsertVariable("pages.count", templates.Count);
            }

            // Editor
            var translationCount = this.Template.InsertContentNodeEditor("page.content-editor", node);
            
            // Variables
            this.Template.InsertVariable("node.name",node.Name);
            this.Template.InsertVariable("node.short-url",node.ShortURL);
            this.Template.InsertVariable("node.public-id",node.PublicId);

            // Restore path
            {
                var restorePath = node.Options.StringNullable(ContentNode.TRASH_OPTION_ORIGIN_PATH_KEY);
                this.Template.InsertVariable("node.restore-path", restorePath);
                this.Template.InsertVariable("node.show-restore", string.IsNullOrWhiteSpace(restorePath) == false && node.Path != ContentNode.TRASH_PATH_NAME ? "show-restore": "");
                this.Template.InsertVariable("node.is-in-trash", node.IsInTrash());
            }

            if (translationCount > 0) {
                this.Template.InsertVariable("page-content-class","");
                this.Template.InsertVariable("add-content-class","hidden-content");
            } else {
                this.Template.InsertVariable("page-content-class","hidden-content");
                this.Template.InsertVariable("add-content-class","");
            }

            // Previews
            var previewButtons = new List<PageTemplate>();
            foreach(var preview in node.GetNodeCMSPreviews().DistinctBy(n => n.RoutePath)) {
                var template = this.Template.LoadTemplate("page.preview");
                template.InsertVariable("page.preview.route-path", preview.RoutePath);
                template.InsertVariable("page.preview.node-path", preview.NodePath);
                template.InsertVariable("page.preview.language", preview.Language?.ToUpper());
                previewButtons.Add(template);
            }
            this.Template.InsertTemplates("page.preview-buttons", previewButtons);


           


            // Navigation
            this.AddNavigationForPath(node.Path);
        }
        
        
        [RequestHandler("/admin/content/html/{path}")]
        public void Html(string path) {
            this.Template.InsertHTMLContent(
                path: path, 
                onlyPublished: true, 
                includeChildren: true,
                customName: "Export"
            );
        }

        [RequestHandler("/admin/content/export", null, null, Verbs.Get, ContentType.Binary)]
        public void Export() {
            var path = this.Params.String("path");
            if (string.IsNullOrEmpty(path)) throw new BackendException("invalid-path", "Path must be specified.");
            var json = ContentNode.ExportContentAsJSON(this.DB, path, true, ContentNode.ENTITIES_PATH_NAME, ContentNode.TRASH_PATH_NAME);
            var pageName = path.Replace(ContentNode.NODE_PATH_SEP, "_").Trim('_');
            if (path == ContentNode.NODE_PATH_SEP || path == "") pageName = ContentNode.ROOT_NODE_NAME;
            var filename = Core.Config.ProjectID + "_" + pageName + ".json";
            SendJSONAsFile(json,filename);
        }

        [RequestHandler("/admin/content/export-translation", null, null, Verbs.Get, ContentType.Binary)]
        public void ExportTranslation() {
            var path = this.Params.String("path");
            var source = this.Params.String("source");
            var target = this.Params.String("target");
            if (string.IsNullOrEmpty(path)) throw new BackendException("invalid-path", "Path must be specified.");
            if (string.IsNullOrEmpty(source)) throw new BackendException("invalid-source", "Source must be specified.");
            if (string.IsNullOrEmpty(target)) throw new BackendException("invalid-target", "Target must be specified.");
            var json = ContentNode.ExportContentAsTranslationJSON(this.DB, path, source, target);
            var pageName = path.Replace(ContentNode.NODE_PATH_SEP, "_").Trim('_');
            if (path == ContentNode.NODE_PATH_SEP || path == "") pageName = ContentNode.ROOT_NODE_NAME;
            var filename = Core.Config.ProjectID + "_" + pageName + "_Translation_"+source.ToUpper()+"_to_"+target.ToUpper()+".json";
            SendJSONAsFile(json,filename);
        }

        //DEPRECATED:
        /*
        [RequestHandler("/admin/content/add-section")]
        public void AddSection() {
            this.RequireWriteARN();

            // Init
            var path = "/";
            // Create form
            this.Template.InsertForm(
                variableName: "form",
                entity: null,
                form: new FormBuilder().Custom("Name","name","text",null).Hidden("parent",path),
                apiCall: "/api/admin/content/node/create",
                onSuccess: "/admin/content"
            );
            // Navigation
            this.AddNavigationForPath(path);
            this.Navigation.Add("add-section","{text.add-section}");
        }

        [RequestHandler("/admin/content/add-page")]
        public void AddPage() {
            this.RequireWriteARN();

            // Init
            var path = this.Params.String("path");
            // Create form
            this.Template.InsertForm(
                variableName: "form",
                entity: null,
                form: new FormBuilder().Custom("Name","name","text", null).Hidden("parent",path),
                apiCall: "/api/admin/content/node/create",
                onSuccess: "/admin/content/page{node.parent-path}"
            );
            // Navigation
            this.AddNavigationForPath(path);
            this.Navigation.Add("add-page","{text.add-page}");
        }

        [RequestHandler("/admin/content/add-translation")]
        public void AddTranslation() {
            this.RequireWriteARN();

            // Init
            var path = this.Params.String("path");
            // Create form
            this.Template.InsertForm(
                variableName: "form",
                entity: null,
                form: new FormBuilder().Custom("Language","language","text", null).Hidden("parent",path),
                apiCall: "/api/admin/content/node/translation/create",
                onSuccess: "/admin/content/page{node.parent-path}?langauge={node.value}"
            );
            // Navigation
            this.AddNavigationForPath(path);
            this.Navigation.Add("add-translation","{text.add-translation}");
        }*/

        [RequestHandler("/admin/content/explore")]
        public void Explore() {
            // Init
            var path = this.Params.String("path");
            // Navigation
            this.AddNavigationForPath(path);
            this.Navigation.Add("explore","{text.explore}");
        }


        [RequestHandler("/admin/content/layouts")]
        public void Layouts() {
            // Init

            this.Template.InsertEntityList(
                variableName: "entity-list",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/content/layouts/layout/{entity.public-id}",
                entities: DB.ContentLayouts()
                );

            // Navigation
            this.Navigation.Add("content");
            this.Navigation.Add("layouts");
        }

        [RequestHandler("/admin/content/layouts/layout/{publicId}")]
        public void Layout(string publicId) {
            // Init
            var entity = DB.ContentLayouts().GetByPublicId(publicId);

            // Form
            this.Template.InsertPropertyList(
                variableName: "entity",
                form: new FormBuilder(Forms.Admin.VIEW),
                entity:entity
                );

            // Options
            this.Template.InsertEntityList(
            variableName: "entity.options-listing",
            entities: entity.Options.Keys.Select(k => new LayoutOption() { Key = k, Value = entity.Options[k]?.ToString() }).ToList(),
            link: "{page.navigation.current-path}/option/{entity.public-id}");

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("content");
            this.Navigation.Add("layouts");
            this.Navigation.Add("layout/"+publicId, entity.GetTitle());
        }

        [RequestHandler("/admin/content/layouts/layout/{publicId}/option/{key}")]
        public void LayoutOption(string publicId, string key) {
            // Init
            var layout = DB.ContentLayouts().GetByPublicId(publicId);

            var entity = new LayoutOption() { Key = key, Value = layout.Options[key]?.ToString() };

            // Options
            this.Template.InsertPropertyList(
            variableName: "form",
            form: new FormBuilder(Forms.Admin.VIEW),
            entity: entity);

            // Variables
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertVariables("layout", layout);

            // Navigation
            this.Navigation.Add("content");
            this.Navigation.Add("layouts");
            this.Navigation.Add("layout/" + publicId, layout.GetTitle());
            this.Navigation.Add("option/" + key, key);
        }


        [RequestHandler("/admin/content/layouts/layout/{publicId}/option/{key}/edit")]
        public void LayoutOptionEdit(string publicId, string key) {
            // Init
            var layout = DB.ContentLayouts().GetByPublicId(publicId);

            var entity = new LayoutOption() { Key = key, Value = layout.Options[key]?.ToString() };

            // Options
            this.Template.InsertForm(
            variableName: "form",
            form: new FormBuilder(Forms.Admin.EDIT),
            apiCall: "/api/admin/content/layout/" + publicId+"/option/"+key+"/edit",
            onSuccess: "{page.navigation.prev-path}",
            entity: entity);

            // Variables
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertVariables("layout", layout);

            // Navigation
            this.Navigation.Add("content");
            this.Navigation.Add("layouts");
            this.Navigation.Add("layout/" + publicId, layout.GetTitle());
            this.Navigation.Add("option/" + key, key);
            this.Navigation.Add("edit");
        }
        [RequestHandler("/admin/content/layouts/create")]
        public void CreateLayout() {
            // Init

            this.RequireWriteARN();

            this.Template.InsertForm(
                variableName: "form",
                form: new FormBuilder(Forms.Admin.CREATE),
                entity: new ContentLayout(),
                apiCall: "/api/admin/content/layouts/create",
                onSuccess: "{page.navigation.prev-path}"

                );

            // Navigation
            this.Navigation.Add("content");
            this.Navigation.Add("layouts");
            this.Navigation.Add("create");
        }

        [RequestHandler("/admin/content/layouts/layout/{publicId}/edit")]
        public void EditLayout(string publicId) {
            // Init
            this.RequireWriteARN();

            var entity = DB.ContentLayouts().GetByPublicId(publicId);

            this.Template.InsertForm(
                variableName: "form",
                form: new FormBuilder(Forms.Admin.EDIT),
                entity: entity,
                apiCall: "/api/admin/content/layout/" + publicId + "/edit",
                onSuccess: "{page.navigation.prev-path}"
                );

            // Navigation
            this.Navigation.Add("content");
            this.Navigation.Add("layouts");
            this.Navigation.Add("layout/" + publicId, entity.GetTitle());
            this.Navigation.Add("edit");
        }


        [RequestHandler("/admin/content/templates/create-page/{publicId}")]
        public void CreatePageFromTemplate(string publicId) {
            // Init
            this.RequireWriteARN();

            var templateNode = ContentNode.GetByPath(DB, ContentNode.TEMPLATES_PATH_NAME);
            if (templateNode == null) {
                throw new BackendException("add-template-error", $"Content nodes for templates not found");
            }
            templateNode.Include(nameof(templateNode.Children));

            var destinationNode = this.DB.ContentNodes().GetByPublicId(publicId);

            var templateNodes = templateNode.ChildrenForType(ContentNode.NODE_TYPE_NODE);

            if (templateNodes.Any()) {
                this.Template.InsertTemplates(
                 variableName: "links",
                 entities: templateNodes.AsQueryable(),
                 templateName: "create-page.link");
            }
            else {
                this.Template.InsertVariable("links", "{text.no-templates=No template found}");
            }

            this.Template.InsertVariable("public-id", publicId);


            // Navigation
            this.AddNavigationForPath(destinationNode.Path);
            this.Navigation.Add("add-template");

        }
    }

    public class LayoutOption : ModelObject {

        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Key { get; set; }
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public string Value { get; set; }

        public override string PublicId
        {
            get
            {
                return Key;
            }
        }
    }
}
