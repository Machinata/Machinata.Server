using Machinata.Core.Data;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Handler {


    public class MigrationAdminHandler : AdminPageTemplateHandler {
        
        [RequestHandler("/admin/data/migration")]
        public void Default() {
            this.RequireSuperuser();

            this.Template.InsertVariable("migration.database", Core.Config.DatabaseDatabase+"@"+Core.Config.DatabaseHost);

            // Navigation
            this.Navigation.Add("data");
            this.Navigation.Add("migration");
        }


        [RequestHandler("/admin/data/migration/analyze")]
        public void Analyze() {
            this.RequireSuperuser();
            this.RequireAdminIP();

            // Analyze
            ModelMigration migration = new ModelMigration(this.DB);
            migration.ResetAnalysis();
            var migrations = migration.Analyze();

            // Show results
            this.Template.InsertEntityList(
                variableName:"migration.migrations-needing-action", 
                entities:migrations.Where(m => m.RecommendedAction != null).AsQueryable(),
                link: "/admin/data/migration/analyze/migration/{entity.public-id}"
            );
            this.Template.InsertEntityList(
                variableName:"migration.all-migrations", 
                entities:migrations,
                link: "/admin/data/migration/analyze/migration/{entity.public-id}"
            );
            this.Template.InsertPropertyList("migration.db-info", migration.DBInfo);

            // Navigation
            this.Navigation.Add("data");
            this.Navigation.Add("migration");
            this.Navigation.Add("analyze");
        }


        [RequestHandler("/admin/data/migration/analyze/migration/{publicId}")]
        public void Migration(string publicId) {
            this.RequireSuperuser();
            this.RequireAdminIP();

            // Analyze
            ModelMigration migration = new ModelMigration(this.DB);
            var migrations = migration.Analyze();
            var entity = migrations.SingleOrDefault(m => m.PublicId == publicId);

            // Show results
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity);
            this.Template.InsertEntityList(
                variableName:"entity.columns", 
                entities: entity.Columns
            );
            //this.Template.InsertVariableUnsafe("entity.raw-diff", entity.RawDiff);
            //this.Template.InsertVariableUnsafe("entity.reduced-diff", entity.ReducedDiff);

            // Navigation
            this.Navigation.Add("data");
            this.Navigation.Add("migration");
            this.Navigation.Add("analyze");
            this.Navigation.Add("migration/"+publicId,"{text.migration} "+publicId);
        }

        [RequestHandler("/admin/data/migration/recreate")]
        public void Recreate() {
            this.RequireSuperuser();
            this.RequireAdminIP();

            //TODO: @dan
            throw new NotImplementedException();
            // Validate this is enabled
            //if (Core.Config.DatabaseMigrationEnableAutoDrop == false) throw new BackendException("autodrop-disabled", "Autodrop is not enabled via Core.Config.DatabaseMigrationEnableAutoDrop.");
            // Variables
            this.Template.InsertVariable("database.database", Core.Config.DatabaseDatabase);
            this.Template.InsertVariable("database.host", Core.Config.DatabaseHost);
            // Navigation
            this.Navigation.Add("data");
            this.Navigation.Add("migration");
            this.Navigation.Add("recreate");
        }

        [RequestHandler("/admin/data/migration/recreate/confirm")]
        public void RecreateConfirm() {
            this.RequireSuperuser();
            this.RequireWriteARN();
            //TODO: @dan
            throw new NotImplementedException();

            // Validate this is enabled
            //if (Core.Config.DatabaseMigrationEnableAutoDrop == false) throw new BackendException("autodrop-disabled", "Autodrop is not enabled via Core.Config.DatabaseMigrationEnableAutoDrop.");

            // Drop database
            //this.DB.Database.Delete();

            // Navigation
            this.Navigation.Add("data");
            this.Navigation.Add("migration");
            this.Navigation.Add("recreate");
            this.Navigation.Add("confirm");
        }
        
    }
}
