
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using static Machinata.Core.Localization.Text;

namespace Machinata.Module.Admin.Handler {


    public class ToolsAdminHandler : AdminPageTemplateHandler {
        
        [RequestHandler("/admin/system/tools")]
        public void Default() {
            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("tools","{text.tools}");
        }

        [RequestHandler("/admin/system/tools/id-converter")]
        public void IDConverter() {
            this.RequireSuperuser();
            string publicID = "";
            string serialID = "";
            string dbID = "";
            
            if(this.Params.String("db-id") != null) {
                var business = new Business();
                business.Id = this.Params.Int("db-id",0);
                dbID = this.Params.String("db-id");
                publicID = Machinata.Core.Ids.Obfuscator.Default.ObfuscateId(dbID.ToInt());
                serialID = Machinata.Core.Ids.SerialIdGenerator.Default.GetSerialIdForEntity(business);
            }
            if(this.Params.String("public-id") != null) {
                publicID = this.Params.String("public-id");
                dbID = Machinata.Core.Ids.Obfuscator.Default.UnobfuscateId(publicID).ToString();
            }
            
            this.Template.InsertVariable("db-id",dbID);
            this.Template.InsertVariable("public-id",publicID);
            this.Template.InsertVariable("serial-id",serialID);

            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("tools","{text.tools}");
            this.Navigation.Add("id-converter","{text.tools-id-converter}");
        }


        internal class TextIdAndValueWithSection {
            public string Id;
            public string Value;
            public string Language;
            public string Section;
            public bool IsComment = false;
        }

        [RequestHandler("/admin/system/tools/localization-converter")]
        public void LocalizationConverter() {



            this.RequireSuperuser();

            // Localization Ids > Excel
            {
                var convertIn = "";
                var convertOut = "";
                if (this.Params.String("localization-ids-in") != null) {
                    convertIn = this.Params.String("localization-ids-in");
                    var lines = convertIn.Split("\n");
                    var ids = new List<string>();
                    var translations = new List<TextIdAndValueWithSection>();
                    string currentSection = null;
                    foreach (var line in lines) {
                        var lineTrimmed = line.Trim();
                        if (lineTrimmed.StartsWith("# ")) {
                            ids.Add(lineTrimmed);
                            currentSection = lineTrimmed.ReplacePrefix("# ", "");
                        } else if (lineTrimmed.StartsWith("#")) {
                            ids.Add(lineTrimmed);
                        } else {
                            var trans = Core.Localization.Text.ParseLocalizationFileLineForTextIdAndValue(lineTrimmed);
                            if (trans == null) continue;
                            if(ids.Contains(trans.Id) == false) ids.Add(trans.Id);
                            translations.Add(new TextIdAndValueWithSection() { 
                                Id = trans.Id,
                                Value = trans.Value,
                                Section = currentSection,
                                Language = trans.Language
                            });
                        }
                    }

                    var langs = translations.Select(t => t.Language).Distinct();

                    // Header
                    convertOut = "";
                    convertOut += $"Section\tText ID";
                    foreach (var lang in langs) convertOut += $"\t{lang.ToUpper()}";
                    convertOut += $"\n";

                    bool showComments = true;
                    var previousRowIsComment = false;
                    string prevSection = null;
                    foreach (var id in ids) {
                        if (id.StartsWith("#")) {
                            if(previousRowIsComment == false) {
                                if (showComments) convertOut += $"\n\n";
                            }
                            if(showComments) convertOut += $"{id}\n";
                            previousRowIsComment = true;
                        } else {
                            var section = translations.FirstOrDefault(t => t.Id == id)?.Section;
                            if(previousRowIsComment == true) {
                                if (showComments) convertOut += $"\n";
                            }
                            var sectionToShow = "";
                            if (section != prevSection) {
                                sectionToShow = section; 
                                convertOut += $"\n";
                            }
                            prevSection = section;
                            previousRowIsComment = false;
                            convertOut += $"{sectionToShow}\t{id}";
                            foreach (var lang in langs) {
                                var trans = translations.SingleOrDefault(t => t.Id == id && t.Language == lang);
                                var val = "";
                                if (trans != null) val = trans.Value;
                                convertOut += $"\t{val}";
                            }
                            convertOut += $"\n";
                        }
                    }
                }
                convertOut = convertOut.Replace("\n\n", "\n");
                this.Template.InsertVariable("localization-ids-in", convertIn);
                this.Template.InsertVariable("excel-out", convertOut);
            }

            // Excel > Localization Ids
            {
                var convertIn = "";
                var convertOut = "";
                if (this.Params.String("excel-in") != null) {
                    convertIn = this.Params.String("excel-in");
                    var lines = convertIn.Split("\n");
                    var ids = new List<string>();
                    var translations = new List<TextIdAndValueWithSection>();
                    string currentSection = null;
                    List<string> columns = null;
                    foreach (var line in lines) {
                        var lineTrimmed = line.Trim('\n','\r',' ');
                        if (lineTrimmed.Trim() == "") {
                            continue;
                        //} else if (lineTrimmed.StartsWith("# ")) {
                        //    ids.Add(lineTrimmed);
                        //    currentSection = lineTrimmed.ReplacePrefix("# ", "");
                        } else if (lineTrimmed.StartsWith("#")) {
                            translations.Add(new TextIdAndValueWithSection() {
                                Id = null,
                                Value = lineTrimmed,
                                Language = null,
                                Section = null,
                                IsComment = true
                            });
                        } else {

                            var segments = lineTrimmed.Split('\t');

                            // Header or data?
                            if (columns == null) {
                                // Header
                                columns = new List<string>();
                                foreach (var segment in segments) columns.Add(segment);
                            } else {
                                // Data
                                string section = null;
                                string id = null;
                                for (var i = 0; i < columns.Count; i++) {
                                    var column = columns[i];
                                    string data = null;
                                    if (i < segments.Count()) data = segments[i];
                                    if (column == "Text ID" && data != null) id = data;
                                    else if (column == "Section" && data != null) section = data;
                                    else {
                                        var trans = new TextIdAndValueWithSection() {
                                            Id = id,
                                            Value = data,
                                            Language = column,
                                            Section = section
                                        };
                                        translations.Add(trans);
                                    }

                                }
                            }


                        }
                    }

                    
                    // Header
                    convertOut = "";
                    bool showComments = true;
                    var previousRowIsComment = false;
                    string prevSection = null;
                    string prevId = null;
                    foreach (var trans in translations) {
                        if(trans.IsComment) {
                            if(previousRowIsComment == false) {
                                if (showComments) convertOut += "\n";
                            }
                            if (showComments) convertOut += trans.Value+"\n";
                            previousRowIsComment = true;
                            
                        } else {
                            if (previousRowIsComment == true) {
                                if (showComments) convertOut += "\n";
                            }
                            previousRowIsComment = false;

                            if (trans.Section != prevSection && prevSection != null) convertOut += $"\n";
                            if (trans.Id != prevId && prevId != null) convertOut += $"\n";
                            if(trans.Section != null) prevSection = trans.Section;
                            if(trans.Id != null) prevId = trans.Id;

                            convertOut += $"{trans.Id}.{trans.Language.ToLower()}={trans.Value}\n";


                        }
                    }
                }
                convertOut = convertOut.Replace("\n\n\n", "\n\n");
                convertOut = convertOut.Replace("\n\n\n", "\n\n");
                this.Template.InsertVariable("excel-in", convertIn);
                this.Template.InsertVariable("localization-ids-out", convertOut);
            }


            // Navigation
            this.Navigation.Add("system", "{text.system}");
            this.Navigation.Add("tools", "{text.tools}");
            this.Navigation.Add("localization-converter", "{text.tools-localization-converter}");
        }

        [RequestHandler("/admin/system/tools/encryption")]
        public void Encryption() {
            this.RequireSuperuser();
            string plainText = "";
            string encryptedText = "";
            
            if(this.Params.String("encrypted-text") != null) {
                encryptedText = this.Params.String("encrypted-text");
                plainText = Machinata.Core.Encryption.DefaultEncryption.DecryptString(encryptedText);
            }
            if(this.Params.String("plain-text") != null) {
                plainText = this.Params.String("plain-text");
                encryptedText = Machinata.Core.Encryption.DefaultEncryption.EncryptString(plainText);
            }
            
            this.Template.InsertVariable("encrypted-text",encryptedText);
            this.Template.InsertVariable("hash-text",Core.Encryption.DefaultHasher.HashString(plainText));
            this.Template.InsertVariable("hash-username",Core.Encryption.DefaultHasher.ComputeEmailOrUsernameHash(plainText));
            this.Template.InsertVariable("plain-text",plainText);

            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("tools","{text.tools}");
            this.Navigation.Add("encryption","{text.tools-encryption}");
        }

        
        [RequestHandler("/admin/system/tools/key-generator")]
        public void KeyGenerator() {
            this.RequireSuperuser();
            this.Template.InsertVariable("EncryptionCryptKeyString32",Core.Encryption.DefaultRandomNumberGenerator.GeneratePasswordXMLSafe(32));
            this.Template.InsertVariable("EncryptionAuthKeyString32",Core.Encryption.DefaultRandomNumberGenerator.GeneratePasswordXMLSafe(32));
            this.Template.InsertVariable("EncryptionHashSalt1",Core.Encryption.DefaultRandomNumberGenerator.GeneratePasswordXMLSafe(32));
            this.Template.InsertVariable("EncryptionHashSalt2",Core.Encryption.DefaultRandomNumberGenerator.GeneratePasswordXMLSafe(32));

            this.Template.InsertVariable("ValidateFrontendToBackendRequestsHashSalt1", Core.Encryption.DefaultRandomNumberGenerator.GeneratePasswordXMLSafe(32));
            this.Template.InsertVariable("ValidateFrontendToBackendRequestsHashSalt2", Core.Encryption.DefaultRandomNumberGenerator.GeneratePasswordXMLSafe(32));

            // Get Id Obfuscator primes...
            var secretPrime = Core.Util.Math.GenerateLargePrimeNumber();
            var secretPrimeInverse = Core.Ids.Providers.StandardIdObfuscator.CalculatePrimeInverse(secretPrime);
            var randomXOR = Core.Util.Math.GenerateRandomBigInteger();

            // Test uniqueness of public ids (note: currently only tests 10 million)
            Core.Ids.Providers.StandardIdObfuscator.ProveIdObfuscatorProducesUniqueAndCorrectResults(secretPrime, secretPrimeInverse, randomXOR);
            
            this.Template.InsertVariable("IdObfuscatorStandardSecretPrime", secretPrime);
            this.Template.InsertVariable("IdObfuscatorStandardSecretPrimeInverse", secretPrimeInverse);
            this.Template.InsertVariable("IdObfuscatorStandardSecretRandomXOR", randomXOR);

            // VAPID KEYS
            {
                var vapidKeys = Core.Messaging.Providers.PushService.VAPIDKeys.GenerateKeys();
                this.Template.InsertVariable("VapidPublicKey", vapidKeys.PublicKey);
                this.Template.InsertVariable("VapidPrivateKey", vapidKeys.PrivateKey);
            }

            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("tools","{text.tools}");
            this.Navigation.Add("encryption","{text.tools-encryption}");
        }


        [RequestHandler("/admin/system/tools/link-generator")]
        public void LinkGenerator() {

            var entity = new Campaign();
            entity.URL = Core.Config.PublicURL;

            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.CREATE).Button("generate","generate"),
                action: null);


            // Navigation
            this.Navigation.Add("system", "{text.system}");
            this.Navigation.Add("tools", "{text.tools}");
            this.Navigation.Add("link-generator");
        }

        public class Campaign : ModelObject {

            [FormBuilder(Forms.Admin.CREATE)]
            public string URL { get; set; }

            [Placeholder("eg. google, newsletter")]
            [FormBuilder(Forms.Admin.CREATE)]
            [Required]
            public string Source { get; set; }

            [Placeholder("eg. spring_sale, summer20")]
            [FormBuilder(Forms.Admin.CREATE)]
            public string Name { get; set; }

            [Placeholder("eg. cpc, banner, email, sticker")]
            [FormBuilder(Forms.Admin.CREATE)]
            public string Medium { get; set; }

            [FormBuilder(Forms.Admin.CREATE)]
            public string Term { get; set; }

            [FormBuilder(Forms.Admin.CREATE)]
            [Placeholder("eg. spring_sale, summer20")]
            public string Content { get; set; }

            [FormBuilder(Forms.Admin.CREATE)]
            public bool IncludeCoupon { get; set; } = false;

            [FormBuilder(Forms.Admin.CREATE)]
            public string CouponCode { get; set; }
        }

        public class AssemblyTypeInfo: ModelObject {

            [FormBuilder(Forms.Admin.LISTING)]
            public string Assembly { get; set; }

            [FormBuilder(Forms.Admin.LISTING)]
            public string Type { get; set; }
            
            [FormBuilder(Forms.Admin.LISTING)]
            public string AQN { get; set; }
        }

        [RequestHandler("/admin/system/tools/assemblies")]
        public void Assemblies() {
            var types = new List<AssemblyTypeInfo>();
            foreach(var assembly in Core.Reflection.Assemblies.GetMachinataAssemblies()) {
                foreach(var type in assembly.GetTypes()) {
                    if (type.Name.StartsWith("<>")) continue;
                    types.Add(new AssemblyTypeInfo() {
                        Assembly = assembly.GetName().Name,
                        Type = type.Name,
                        AQN = type.AssemblyQualifiedName
                    });
                }
            }
            this.Template.InsertEntityList("types", types, new FormBuilder(Forms.Admin.LISTING));
            // Navigation
            this.Navigation.Add("system","{text.system}");
            this.Navigation.Add("tools","{text.tools}");
            this.Navigation.Add("assemblies","{text.tools-assemblies}");
        }



      
    }
}
