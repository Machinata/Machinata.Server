using Machinata.Core.Data;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Machinata.Module.Admin.Handler {

    /// <summary>
    /// This is the new Sandbox handler for UIKit etc
    /// </summary>

    public class SandboxUIKitAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("sandbox");
        }

        #endregion

        public override void DefaultNavigation() {
            base.DefaultNavigation();
            this.Navigation.Add("sandbox");
        }

        public override string DefaultTheme() {
            if (string.IsNullOrEmpty(this.Params.String("theme")) == false) {
                return this.Params.String("theme");
            } else {
                return base.DefaultTheme();
            }
        }

        #region Menu

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "sandbox",
                Path = "/admin/sandbox",
                Title = "{text.sandbox}",
                Sort = "800"
            });
        }

        #endregion

        #region Test Form Entity 

        internal class FormEntity : ModelObject {


            public static DateTime TestDate = new DateTime(2020, 11, 20);

            public FormEntity() {
                this.TranslatableText = new TranslatableText();
                this.TranslatableText.SetDefault("Default: Translateable Text");

                var propertiesValues = new Dictionary<string, object>();
                propertiesValues["Integer"] = 1000;
                propertiesValues["Double"] = 123.123d;
                propertiesValues["Decimal"] = 1123.123m;
                propertiesValues["String"] = "String";
                this.Properties = new Properties(propertiesValues);
            }

            [FormBuilder(Forms.Frontend.EDIT)]
            public string RegularText { get; set; } = "Regular Text";

            [FormBuilder(Forms.Frontend.EDIT)]
            [Range(0, 100)]
            [DataType("range")]
            public int IntSlider { get; set; } = 33;

            [FormBuilder(Forms.Frontend.EDIT)]
            [Minimum(10)]
            [Maximum(90)]
            public IntRange IntRangeSlider { get; set; } =  new IntRange(45, 80);

            [FormBuilder(Forms.Frontend.EDIT)]
            public TranslatableText TranslatableText { get; set; }

            [FormBuilder(Forms.Frontend.EDIT)]
            public DateRange DateRange { get; set; } = new DateRange(TestDate, TestDate.AddDays(10));


            [FormBuilder(Forms.Frontend.EDIT)]
            public Price Price { get; set; } = new Price(13333.33m);


            [FormBuilder(Forms.Frontend.EDIT, Group = "Properties")]
            public Properties Properties { get; set; } = new Properties();

        }

        #endregion

        [RequestHandler("/admin/sandbox")]
        public void Default() {
            // Menu items
            var menuItems = PageTemplate.Cache.FindAll(this.Template.Package, "admin/sandbox/menu/menu.item.", this.TemplateExtension);
            this.Template.InsertTemplates("sandbox.menu-items", menuItems);
        }

        [RequestHandler("/admin/sandbox/errors/page")]
        public void ErrorsPage() {
            throw new Exception("This is an unknown test exception.");
        }


        [RequestHandler("/admin/sandbox/errors/2fa-download", null, null, Verbs.Any, ContentType.Binary)]
        public void TwoFactorDownload() {
            this.RequireTwoFactorAuthentication();

            byte[] data = new byte[] {
                255, 255, 000, 000,  255, 255, 255, 255,  255, 255, 255, 255,  255, 255, 255, 255,  255, 255, 000, 000,
                255, 255, 255, 255,  255, 255, 000, 000,  255, 255, 255, 255,  255, 255, 000, 000,  255, 255, 255, 255,
                255, 255, 255, 255,  255, 255, 255, 255,  255, 255, 000, 000,  255, 255, 255, 255,  255, 255, 255, 255,
                255, 255, 255, 255,  255, 255, 000, 000,  255, 255, 255, 255,  255, 255, 000, 000,  255, 255, 255, 255,
                255, 255, 000, 000,  255, 255, 255, 255,  255, 255, 255, 255,  255, 255, 255, 255,  255, 255, 000, 000
            };
            SendBinary(data, "test.bmp");
        }

        [RequestHandler("/admin/sandbox/uikit")]
        public void UIKit() {

            // Change theme
            

            // Tools
            this.Template.InsertTemplate("uikit.tools", "uikit.tools");

            // UI-Kit components
            List<PageTemplate> listItems = GetUIKitComponents(this.Template.Package, this.TemplateExtension);
            this.Template.InsertTemplates("uikit.components", listItems);

            // Themes
            this.Template.InsertTemplates("uikit.themes", Core.Model.Theme.GetCachedThemes(), "uikit.theme");

            // Navigation
            this.Navigation.Add("uikit", "{text.uikit}");
        }

        public static List<PageTemplate> GetUIKitComponents(string packageName, string templateExtension) {
            return PageTemplate.Cache.FindAll(packageName, "admin/sandbox/uikit/components/uikit.component.item.", templateExtension) ;
        }

        [RequestHandler("/admin/sandbox/uikit/{page}", AccessPolicy.PUBLIC_ARN)]
        public void UIKitPage(string page) {
            // Access for admins and 
            //this.RequireLoginExceptHeadless()
            this.RequireLoginExceptAuthSecret( requireAdminLogin: true);

            // Note UIKit sandbox subpages are dynamic, no handler required...

            // Change to page name template
            this.Template.ChangeTemplate("components/"+page);

            // Tools
            this.Template.InsertTemplate("uikit.tools", "uikit.tools");

            // UI-Kit components
            var listItems = PageTemplate.Cache.FindAll(this.Template.Package, "admin/sandbox/uikit/components/uikit.component.item.", this.TemplateExtension);
            this.Template.InsertTemplates("uikit.components", listItems);

            // Themes
            this.Template.InsertTemplates("uikit.themes", Core.Model.Theme.GetCachedThemes().ToList(), "uikit.theme");

            // Form?
            if(this.Template.HasVariable("form")) {
                this.Template.InsertForm("form", new FormEntity(), new FormBuilder(Forms.Frontend.EDIT), "", "/api/admin/sandbox/echo", "Machinata.echo");
            }

           

            // Authtoken?
            {
                if (this.Template.HasVariable("auth-secret") == true) {

                    var authSecret = this.Params.String("auth-secret");

                    if (string.IsNullOrWhiteSpace(authSecret) == false) {
                        this.Template.InsertVariable("auth-secret", "?auth-secret=" + authSecret);
                    } else {
                        this.Template.InsertVariableEmpty("auth-secret");
                    }
                   
                }
            }

            // Navigation
            this.Navigation.Add("uikit");
            this.Navigation.Add(page,page.Capitalize());
        }

       
    }
}
