
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Exceptions;
using Machinata.Core.Localization;
using Machinata.Core.Model;
using Machinata.Core.Builder;

namespace Machinata.Module.Admin.Handler {


    public class LocalizationAdminHandler : AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("config");
        }

        #endregion

        [RequestHandler("/admin/config/localization")]
        public void Default() {
            // All packages with localization
            var templates = new List<PageTemplate>();
            foreach (var package in Core.Localization.Text.RegisteredPackages()) {
                var template = this.Template.LoadTemplate("packages.item");
                template.InsertVariable("package.name", package);
                templates.Add(template);
            }
            // All
            var allTemplate = this.Template.LoadTemplate("packages.item");
            allTemplate.InsertVariable("package.name", "All");
            templates.Add(allTemplate);
            this.Template.InsertTemplates("packages", templates);

            // Navigation
            this.Navigation.Add("config", "{text.config}");
            this.Navigation.Add("localization", "{text.localization}");
        }
      
        [RequestHandler("/admin/config/localization/package/{package}")]
        public void Package(string package) {

            var langs = this.Params.String("langs")=="all" ? Core.Localization.Text.RegisteredLanguages() : Core.Localization.Text.SupportedLanguages();
            IEnumerable<Text> texts = Text.GetAllTexts().OrderBy(t => t.Sort);
            var mainLang = langs.FirstOrDefault();

            // Filter?
            var filter = this.Params.String("filter", string.Empty);
            var search = this.Params.String("search", string.Empty);

            // Package
            if (package.ToLower() != "all") {
                texts = texts.Where(t=>t.Package.ToLower() == package.ToLower());
            }

            if (filter == "missing") {
                texts = texts.Where(t => Text.GetMissing(t.Id, langs).Count > 0);
            }

            if (filter == "duplicates") {
                texts = texts.Where(t => Text.GetDuplicates(t.Id).Count > 0);
            }
                    
            // Search
            if (!string.IsNullOrWhiteSpace(search)) {
                texts = texts.Where(t=> t.Id.ToLower().Contains(search.ToLower()) || t.Value.ToLower().Contains(search.ToLower()));
            }

            // Get text ids
            var textIds = texts.Select(t => t.Id).Distinct();

            // Insert table
            InsertLocalizationTable(langs, textIds);


            // Total
            this.Template.InsertVariable("texts.count", texts.Count());

            // Filter ui
            this.Template.InsertVariable("filter.all", filter =="all" ? "selected": string.Empty);
            this.Template.InsertVariable("filter.missing", filter =="missing" ? "selected" : string.Empty);
            this.Template.InsertVariable("filter.duplicates", filter =="duplicates" ? "selected" : string.Empty);
            this.Template.InsertVariable("filter.search", search != string.Empty ? "selected" : string.Empty);
            this.Template.InsertVariable("search-filter", search);
            this.Template.InsertVariable("filter", filter);


            // Navigation
            this.Navigation.Add("config", "{text.config}");
            this.Navigation.Add("localization", "{text.localization}");
            this.Navigation.Add("package/" + package, package);
        }

        private void InsertLocalizationTable(List<string> langs, IEnumerable<string> textIds) {
            // Build header
            var headerColumns = new List<PageTemplate>();
            {
                var headerIdTemplate = this.Template.LoadTemplate("package.row.header-column");
                headerIdTemplate.InsertVariable("column", "Initial Source");
                headerColumns.Add(headerIdTemplate);
            }
            {
                var headerIdTemplate = this.Template.LoadTemplate("package.row.header-column");
                headerIdTemplate.InsertVariable("column", "ID");
                headerColumns.Add(headerIdTemplate);
            }
            foreach (var lang in langs) {
                var template =  this.Template.LoadTemplate("package.row.header-column");
                template.InsertVariable("column", lang);
                template.InsertVariable("sort.id", lang);
                headerColumns.Add(template);
            }

            this.Template.InsertTemplates("texts.header", headerColumns);
            // Iterate each text, sorted alphabetically
            var rows = new List<PageTemplate>();
            foreach (var textId in textIds) {
                // Build row
                var rowTemplate = this.Template.LoadTemplate("package.row");
                var columns = new List<PageTemplate>();
                {
                    var template = rowTemplate.LoadTemplate("package.row.column");
                    template.InsertVariable("column", Text.GetTexts(textId).First().Source);
                    template.InsertVariable("text-id", textId);
                    template.InsertVariable("column.classes", string.Empty);
                    columns.Add(template);
                }
                {
                    var template = rowTemplate.LoadTemplate("package.row.column");
                    template.InsertVariable("column", textId);
                    template.InsertVariable("text-id", textId);
                    template.InsertVariable("column.classes", string.Empty);
                    columns.Add(template);
                }
                foreach (var lang in langs) {
                    var langTexts = Text.GetTexts(textId, lang);
                    var prioText = langTexts?.LastOrDefault();
                    var classes = "";
                    if (prioText == null || string.IsNullOrEmpty(prioText.Value)) classes += "missing red ";
                    var column = rowTemplate.LoadTemplate("package.row.column");

                    if (langTexts != null) {
                        var overloads = langTexts.Select(t => t.Value);
                        if (langTexts.Count > 1) {
                            classes += "duplicates yellow";
                        }
                        var content = langTexts != null ? string.Join("/", overloads) : null;
                        column.InsertVariable("column", content);
                    } else {
                        column.InsertVariable("column", string.Empty);
                    }

                    column.InsertVariable("column.classes", classes);
                    column.InsertVariable("text-id", textId);
                    columns.Add(column);
                }
                rowTemplate.InsertTemplates("columns", columns);
                rows.Add(rowTemplate);
            }
            this.Template.InsertTemplates("texts.rows", rows);
        }
             
        private static string GetPackageByName(string package) {
            var packages = Core.Localization.Text.RegisteredPackages();
            if (!packages.Contains(package)) throw new BackendException("invalid-package", "The package is invalid.");
            return package;
        }

        [RequestHandler("/admin/config/localization/package/{package}/text/{textId}")]
        public void PackageText(string package, string textId) {

            var langs = this.Params.String("langs") == "all" ? Core.Localization.Text.RegisteredLanguages() : Core.Localization.Text.SupportedLanguages();

            // All texts with the id
            var texts = Core.Localization.Text.GetTexts(textId);

            // Fill dummy texts for missing languages
            foreach(var language in langs) {
                if (!texts.Any(t=>t.Language == language )) {
                    texts.Add(new Text() { Id = textId, Language = language, Package = package, Source="" });
                }
            }

            var textsByLanguage = texts.GroupBy(t => t.Language).OrderBy(t=>t.Key); //TODO: @micha: should be ordred by order of langs

            var rowTemplates = new List<PageTemplate>();
            foreach(var textLanguate in textsByLanguage) {
                var rowTemplate = this.Template.LoadTemplate("text.row");
                var currentTranslation = textLanguate.Last();
                rowTemplate.InsertVariable("entity.source", currentTranslation.Source);
                rowTemplate.InsertVariable("entity.package", currentTranslation.Package);
                rowTemplate.InsertVariable("entity.language", currentTranslation.Language);
                rowTemplate.InsertVariable("entity.text-id", currentTranslation.Id);
                rowTemplate.InsertVariable("config-value", textLanguate.LastOrDefault(t=>t.SourceType != Text.SourceTypes.Database)?.Value);
                rowTemplate.InsertVariable("custom-value", textLanguate.SingleOrDefault(t=>t.SourceType == Text.SourceTypes.Database)?.Value);
                rowTemplates.Add(rowTemplate);

            }
            this.Template.InsertTemplates("text.rows", rowTemplates);

            // Duplicates
            var duplicates = Text.GetDuplicates(textId);

            this.Template.InsertEntityList(
                variableName: "duplicates",
                entities: duplicates,
                form: new FormBuilder(Forms.Admin.LISTING)
                );

            this.Navigation.Add("config", "{text.config}");
            this.Navigation.Add("localization", "{text.localization}");
            this.Navigation.Add("package/" + package, package);
            this.Navigation.Add("text/" + textId, textId);

        }

      
        [RequestHandler("/admin/config/localization/package/{package}/text/{textId}/edit")]
        public void PackageTextLanguage(string package, string textId) {
            GetPackageByName(package);

            var langs = this.Params.String("langs") == "all" ? Core.Localization.Text.RegisteredLanguages() : Core.Localization.Text.SupportedLanguages();

            var texts = Core.Localization.Text.RegisteredTexts(package).Where(t => t.Id == textId);
          
            var language = this.Params.String("id").Split('-').Last();
            if (!langs.Contains(language)){
                throw new BackendException("no-language", $"Language {language} not found");
            }
            var text = texts.FirstOrDefault(t => t.Language == language);


            this.Template.InsertForm(
                variableName: "form",
                entity: text == null ? new Text() : text,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: "{page.navigation.current-path}/edit?id={entity.public-id}");

            this.Template.InsertVariable("language", language);
            this.Navigation.Add("config", "{text.config}");
            this.Navigation.Add("localization", "{text.localization}");
            this.Navigation.Add("package/" + package, package);
            this.Navigation.Add("text/" + textId, textId);
            this.Navigation.Add("edit");

        }
    }

  
}
