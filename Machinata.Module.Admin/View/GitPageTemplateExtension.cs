using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Machinata.Module.Admin.View {
    public static class GitPageTemplateExtension {

        public static void InsertGitLogs(this PageTemplate template, string keyword, string variableName = "git-logs") {
            // Change Log
            if (template.HasVariable(variableName)) {
                IEnumerable<Git.GitLog> gitLogs = Git.GetGitLog(keyword);
                template.InsertGitLogs( gitLogs, variableName);
            }
        }

        public static void InsertGitLogs(this PageTemplate template, IEnumerable<Git.GitLog> gitLogs, string variableName) {
            if (template.HasVariable(variableName)) {
                if (gitLogs != null) {
                    template.InsertTemplates(variableName, gitLogs, "default.git-log", insertGitLogVariables);
                } else {
                    template.InsertVariable(variableName, "Gitlogs are not installed");
                }
            }
        }

        private static void insertGitLogVariables(Git.GitLog gitLog, PageTemplate template) {
            var classes = new List<string>();
            if (gitLog.IsUpstream == true) {
                classes.Add("is-upstream");
            }
            if (gitLog.IsBreakingChange == true) {
                classes.Add("is-breaking-change");
            }
            template.InsertVariable("classes", string.Join(" ", classes));
        }

        public static void InsertNewGitLogs(this PageTemplate template, string keyword, string variableName = "git-logs") {
           
            // Profile vs Version
            if (Git.HasDefaultLogFile() && template.Handler.User.Settings?[User.USER_LAST_LOGIN_BUILDVERSION]?.ToString() != Core.Config.BuildVersion) {

                var usersLastBuildDate = DateTime.MinValue;
                if (string.IsNullOrEmpty(template.Handler.User.Settings[User.USER_LAST_LOGIN_BUILDVERSION]?.ToString()) == false) {
                    usersLastBuildDate = Core.Util.Time.ParseDateTime(template.Handler.User.Settings[User.USER_LAST_LOGIN_BUILDTIME]?.ToString(), usersLastBuildDate, false);
                }

                // Load git logs since last view
                var gitLogs = Git.GetGitLog(keyword).Where(g => g.CommitDate > usersLastBuildDate);

                template.InsertTemplates(variableName, gitLogs, "default.git-log", insertGitLogVariables);
                template.InsertVariable(variableName + ".has-new-logs", gitLogs.Any() && usersLastBuildDate != DateTime.MinValue ? "true" : "false");
                template.InsertVariable(variableName + ".is-new-version", gitLogs.Any() && usersLastBuildDate != DateTime.MinValue ? "true" : "false");

                // Save the new version
                template.Handler.User.Settings[User.USER_LAST_LOGIN_BUILDVERSION] = Core.Config.BuildVersion;
                template.Handler.User.Settings[User.USER_LAST_LOGIN_BUILDTIME] = Core.Config.BuildTimestamp.ToString(Core.Config.DateTimeFormat);
                template.Handler.DB.SaveChanges();
            } else {
                template.InsertVariable(variableName + ".is-new-version", "false");
                template.InsertVariable(variableName, "");
                template.InsertVariable(variableName + ".has-new-logs", "false");
            }

        }




    }
    
}