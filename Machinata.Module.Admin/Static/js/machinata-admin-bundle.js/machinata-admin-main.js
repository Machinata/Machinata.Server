﻿


Machinata.Admin = {};

Machinata.Admin.printPage = function () {
    Machinata.openPage(Machinata.updateQueryString("print","true"));
};


Machinata.ready(function () {

    if (Machinata.queryParameter("print") == "true") {
        setTimeout(function () {
            window.print();
        }, 300);
    }

    // Dashboards with timeranges
    $(".ui-card.admin-dashboard ").each(function () {
        let toolbarElem = $(`
            <div class="ui-toolbar option-sticky">
                <h2></h2>
                <div class="tools content-tool">
                    <a class="ui-button option-icon-only">{icon.calendar}</a>
                </div>
            </div>`);


        // not working bc of timing
        if (false) { 
        let uiReportElem = $(this).find("#dashboard-report");

        console.info("uiReportElem", uiReportElem);

        // get url from div with attribute data-api-call
        let url = uiReportElem.attr("data-api-call");

        // add start and end date to the url
        let start = Machinata.queryParameter("start");
        let end = Machinata.queryParameter("end");
        if (start != null && end != null) {
            url = Machinata.updateQueryString("start", start, url);
            url = Machinata.updateQueryString("end", end, url);
        }
        // udpate the url in the div
        uiReportElem.attr("data-api-call", url);
    }
       

        toolbarElem.find(".tools .ui-button").click(function () {

            let start = Machinata.queryParameter("start");
            let end = Machinata.queryParameter("end");
            let currentDateString = null;
            if (start != null && end != null) {
                currentDateString = start + " - " + end;
            }

            var diag = Machinata.dateDialog("{text.admins-dashboard-date-chooser.en=Choose date range}", null, null, currentDateString, null, null, true);
            diag.input(function (val, id) {
                if (id == "clear") {
                    let url = Machinata.updateQueryString("start", null);
                    url = Machinata.updateQueryString("end", null, url);
                    Machinata.goToPage(url);
                } else {
                    // set current date to the query string
                               
                    let from = Machinata.formattedDateFromDate(val.start);
                    let to = Machinata.formattedDateFromDate(val.end);

                    let url = Machinata.updateQueryString("start", from);
                    url = Machinata.updateQueryString("end", to, url);
                                     

                    Machinata.goToPage(url);
                }
            });
            diag.show();

        });
        
        function setTimeRange(startDate, endDate) {
            
            if (startDate == "") startDate = null;
            if (endDate == "") endDate = null;
            let title = "";
            if (startDate == null && endDate == null) title = "Timerange: all";
            else title = "Timerange: " + startDate + " - " + endDate;
            toolbarElem.find("h2").text(title);

        };
        setTimeRange(Machinata.queryParameter("start"), Machinata.queryParameter("end"));
        
        $(".ui-card.admin-dashboard ").addClass("option-has-sticky-toolbar");
        $(".ui-card.admin-dashboard ").prepend(toolbarElem);
    });

    // Form tweaks
    $(".ui-card > .ui-toolbar.option-sticky").each(function () {
        $(this).closest(".ui-card").addClass("option-has-sticky-toolbar");
    });

    // Actions
    $(".action-save-card-form").click(function () {
        var elem = $(this);
        var cardElem = elem.closest(".ui-card");
        var formElem = cardElem.find(".ui-form");
        var formSaveButtonElem = formElem.find("input[type='submit']");
        formSaveButtonElem.trigger("click");
    });

});