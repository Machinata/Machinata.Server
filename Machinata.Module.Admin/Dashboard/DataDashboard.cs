﻿using Machinata.Core.Model;
using Machinata.Module.Reporting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Dashboard {
    class DataDashboard {

        [DashboardLoader]
        public static void LoadDashboard(DashboardLoader loader) {

            var section = new SectionNode();
            section.SetTitle("Data");


            var contentFiles = loader.DB.ContentFiles().AsQueryable();
            contentFiles = loader.FilterItemsForTimerangeCreated(contentFiles);

            {
                var layoutNode = new LayoutNode();
                //layoutNode.SetTitle("Files");
                layoutNode.Style = "CS_W_Two_Columns";

                // By Category
                {
                    var chart = new DonutNode();
                    chart.Theme = "default";
                    chart.SetTitle("Files");
                    chart.SetSubTitle(loader.GetNodeSubtitle("by Category"));
                    chart.LegendLabelsShowValue = true;
                 

                    if (contentFiles.Any()) {
                        var fileGroups = contentFiles.GroupBy(cf => cf.FileCategory);
                        foreach (var fileGroup in fileGroups) {
                            chart.AddSliceNumber(fileGroup.Count(), fileGroup.Key.ToString());
                        }
                    }
                   
                    layoutNode.AddChildToSlot(chart, "{left}");
                }

                // By Source
                {
                    var chart = new DonutNode();
                    chart.Theme = "default";
                    chart.SetTitle("Files");
                    chart.SetSubTitle(loader.GetNodeSubtitle("by Source"));
                    chart.LegendLabelsShowValue = true;

                    if (contentFiles.Any()) {
                        var fileGroups = contentFiles.GroupBy(cf => cf.Source);
                        foreach (var fileGroup in fileGroups) {
                            chart.AddSliceNumber(fileGroup.Count(), fileGroup.Key.ToString());
                        }
                    }
                    layoutNode.AddChildToSlot(chart, "{right}");
                }

                //
                //layoutNode.AddChildToSlot(GetBudgetsBars(project), "{right}");
                section.AddChild(layoutNode);
            }
            

            loader.AddItem(section, "/admin/data");
        }

    }
}
