﻿using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Module.Reporting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Admin.Dashboard {
    class ContentDashboard {

        [DashboardLoader]
        public static void LoadDashboard(DashboardLoader loader) {

            var section = new SectionNode();
            section.Style = "W_H2";
            section.SetTitle("Content");


            // Donut with Top Sections 
            {
                var donut = new DonutNode();
                donut.Theme = "default";
                donut.SetTitle("Sections");
                donut.SetSubTitle(loader.GetNodeSubtitle("by Children"));
                var nodes = loader.DB.ContentNodes()
                    .Include(nameof(ContentNode.Children) + "." + nameof(ContentNode.Children)+ "." + nameof(ContentNode.Children)).ToList().AsQueryable();
               

                var bucketSize = 6;
                var sections = nodes.GetRoot().ChildrenForType(ContentNode.NODE_TYPE_NODE).Published().Where(s => s.Path != "/Entities" && s.Path != "/Journal");

                sections = loader.FilterItemsForTimerangeCreated(sections.AsQueryable()).AsQueryable();

                foreach (var node in sections.OrderByDescending(s => s.ChildrenForType(ContentNode.NODE_TYPE_NODE).Count()).ThenBy(s => s.Sort).Take(bucketSize-1)) {

                    var count = node.ChildrenForType(ContentNode.NODE_TYPE_NODE).Count();
                    donut.AddSliceFormatWithThousandsNoDecimal(count, node.Name);

                 
                }

                // Others
                {
                    var others = sections.Skip(bucketSize - 1);
                    var count = others.Sum(o => o.ChildrenForType(ContentNode.NODE_TYPE_NODE).Count());
                    donut.AddSliceFormatWithThousandsNoDecimal(count,"Others");
                }

                // Legend
                donut.LegendLabelsShowValue = true;


                section.AddChild(donut);

            }

            loader.AddItem(section, "/admin/content");
        }

    }
}
