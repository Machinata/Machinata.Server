using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace Machinata.Module.Admin.Dashboard {


    public class DashboardLoader {


        private List<DashboardItem> _dashboardItems = new List<DashboardItem>();
        private ModelContext _db;
        private DateRange dateRange;
        public bool Debug;

        public virtual Core.Model.ModelContext DB {
            get {
                if (_db == null) {
                    _db = Core.Model.ModelContext.GetModelContext(null);
                }
                return _db;
            }
        }

        public string Language { get; set; }

        public string GetNodeSubtitle(string title) {
            if (this.dateRange != null && this.dateRange.HasValue()) {
                return title + " (" + this.dateRange.ToHumanReadableDateString() + ")"; //TODO: @micha
            }
            return title; 
        }

              
        /// <summary>
        /// items has to be a loaded list,
        /// slower but works on all date properties
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public IQueryable<T> FilterItemsForTimerange<T>(IQueryable<T> items, Func<T, DateTime?> dateTime){
            if (this.dateRange != null) {
                if (this.dateRange.Start.HasValue) {
                    items = items.Where(i => dateTime(i) >= this.dateRange.Start);
                }
                if (this.dateRange.End.HasValue) {
                    items = items.Where(i => dateTime(i) <= this.dateRange.End);
                }
            }
            return items;
        }

        /// <summary>
        /// This is faster than the other method, but only works for ModelObjects and on Created property
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        public IQueryable<T> FilterItemsForTimerangeCreated<T>(IQueryable<T> items) where T: ModelObject {
            if (this.dateRange != null) {
                if (this.dateRange.Start.HasValue) {
                    items = items.Where(i => i.Created >= this.dateRange.Start);
                }
                if (this.dateRange.End.HasValue) {
                    items = items.Where(i => i.Created <= this.dateRange.End);
                }
            }
            return items;
        }

        public DashboardLoader(string language, DateRange dateRange, bool debug) {
            this.Language = language;
            this.dateRange = dateRange;
            this.Debug = debug;
        }     


        /// <summary>
        /// Add Node to dashboard
        /// IMPORTANT: SET CORRECT ARN
        /// </summary>
        /// <param name="node"></param>
        /// <param name="arn"></param>
        public void AddItem(Reporting.Model.Node node, string arn) {
            if (string.IsNullOrWhiteSpace(arn) == true) {
                throw new Exception("No ARN defined for node");
            }

            var item = new DashboardItem();
            item.ARN = arn;
            item.Node = node;

            _dashboardItems.Add(item);
        }

        public static IEnumerable<DashboardItem> LoadDashboardItems(string language, List<string> namespacesFilter, DateRange dateRange, bool debug) {

            var methods = Core.Reflection.Methods.GetMachinataMethodsWithAttribute(typeof(DashboardLoaderAttribute));

            var dashBoardLoader = new DashboardLoader(language, dateRange, debug);
            foreach (var method in methods) {


                // Section filter
                if (namespacesFilter.Any()) {

                    string fullname = method.DeclaringType.FullName + "." + method.Name;
                    if (namespacesFilter.Any(nf => fullname.StartsWith(nf)) == false) {
                        continue;
                    }
                }

                var start = Stopwatch.StartNew();
                method.Invoke(null, new object[] { dashBoardLoader }) ;
                start.Stop();
                if (dashBoardLoader._dashboardItems.Any()) {
                    dashBoardLoader._dashboardItems.Last().LoadingTime = start.Elapsed;
                }
            }

            return dashBoardLoader._dashboardItems;
        }


        /// <summary>
        /// Inserts the filter/query variables into the template
        /// </summary>
        /// <param name="handler"></param>
        /// <param name="template"></param>
        public static void  InsertVariables(Core.Handler.Handler handler, PageTemplate template) {
            template.InsertVariable("debug", handler.Params.Bool("debug", false));

            var start = handler.Params.String("start", null);
            var end = handler.Params.String("end", null);

            template.InsertVariable("start", start);
            template.InsertVariable("end", end);

        }


    }
}
