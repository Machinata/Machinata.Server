using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace Machinata.Module.Admin.Dashboard {


    public class DashboardItem {

        public string ARN { get; set; }
        public Reporting.Model.Node Node { get; set; }
        public TimeSpan LoadingTime { get; set; }

    }
}
