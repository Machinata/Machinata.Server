using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Documentation.Model;
using Markdig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Core.Documentation.Parser;

namespace Machinata.Module.Documentation.Extensions {
    public static class DocumentationPageTemplateExtensions {

        public static void InsertDocumentationItemsTemplates(this PageTemplate template, string variableName , string name, DocumentationPackage document, bool insertCode = false) {

            // Main Items
            var rootItem = document.Items.Where(i => i.FullNameURL == name && i.Hidden == false).FirstOrDefault();
            if (rootItem == null) {
                template.InsertVariable(variableName, string.Empty);
            } else { 
                var rootTemplate = template.LoadTemplate("documentation.root");
                var rootNamespace = rootItem.FullName;
                rootTemplate.InsertVariableUnsafe("root-item.summary-html", rootItem.Summary.MarkdownToHtml());
                rootTemplate.InsertVariables("root-item", rootItem);
                InsertItems(rootTemplate, "documentation.item", "documentation.exploded", document, rootNamespace, 1, 0, insertCode);
                InsertItems(rootTemplate, "documentation.index", "documentation.index", document, rootNamespace, 1, 0, insertCode);
                template.InsertTemplate(variableName, rootTemplate);
            }
        }

        /// <summary>
        /// Inserts the items.
        /// HINT: filters all items with Hidden = true out
        /// </summary>
        /// <param name="template">The template.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="variableName">Name of the variable.</param>
        /// <param name="document">The document.</param>
        /// <param name="fullNamespace">The full namespace.</param>
        /// <param name="h1Offset">The h1 offset.</param>
        /// <param name="level">The level.</param>
        public static void InsertItems(this PageTemplate template, string templateName, string variableName, DocumentationPackage document, string fullNamespace, int h1Offset = 0, int level = 0, bool insertCode = false) {
            var errorHelper = "";
            try {
                var templates = new List<PageTemplate>();
                var children = document.GetItemsForNamespace(fullNamespace)
                    .Where(c => c.Hidden == false)
                    .OrderBy(e => e.Type != Parser.TYPE_NAMESPACE)
                    .ThenBy(e =>  Parser.IsStructure(e.Type) == false)
                    .ThenBy(e => e.Type)
                    .ThenBy(e => e.FullName);

                foreach (var child in children) {
                    errorHelper = "See "+child.FullName;
                    PageTemplate itemTemplate = null;;
                    try {
                        itemTemplate = template.LoadTemplate(templateName + "." + child.Type.ToLower());
                    } catch {
                        itemTemplate = template.LoadTemplate(templateName);
                    }
                    itemTemplate.InsertVariable("item.h-level", level + 1 + h1Offset);
                    itemTemplate.InsertVariable("item.level", level);
                    if (level == 0) itemTemplate.InsertVariable("item.title", child.FullName);
                    else itemTemplate.InsertVariable("item.title", child.Name);
                    if (child.Type == "namespace") itemTemplate.InsertVariable("item.nice-title", child.FullName);
                    else itemTemplate.InsertVariable("item.nice-title", child.Name);

                    itemTemplate.InsertVariable("item.example-html", child.Example?.MarkdownToHtml());
                    itemTemplate.InsertVariableUnsafe("item.summary-html", child.Summary?.Trim().MarkdownToHtml());

                    // Code
                    if (insertCode == true) {
                        itemTemplate.InsertVariable("item.code", child.Code);
                    } else {
                        itemTemplate.InsertVariable("item.code", "no code");
                    }

                    // IMPORTANT: THIS LINE MUST BE AFTER THE CODE (item.code insert) LINE
                    itemTemplate.InsertVariables("item", child);

                    // CSS 
                    var cssClasses = new List<string>();
                    AddClass(child.Summary, nameof(child.Summary), cssClasses);
                    AddClass(insertCode ? child.Code : string.Empty, nameof(child.Code), cssClasses);
                    AddClass(child.Deprecated, nameof(child.Deprecated), cssClasses);
                    AddClass(child.Example, nameof(child.Example), cssClasses);
                    AddClass(child.Hidden, nameof(child.Hidden), cssClasses);
                    AddClass(child.Inherits, nameof(child.Inherits), cssClasses);
                    AddClass(child.Signature, nameof(child.Signature), cssClasses);
                    AddClass(child.Type, nameof(child.Type), cssClasses);
                    AddClass(child.Value, nameof(child.Value), cssClasses);
                    AddClass(child.Returns, nameof(child.Returns), cssClasses);
                    AddClass(child.Parameters?.Keys.FirstOrDefault(), nameof(child.Parameters), cssClasses);

                    // Parameters
                    var parameterTemplates = new List<PageTemplate>();
                    foreach (var paramKey in child.Parameters.Keys) {
                        var paramTemplate = itemTemplate.LoadTemplate(templateName + ".parameter");

                        var param = child.Parameters[paramKey];

                        paramTemplate.InsertVariable("parameter.name", paramKey);
                        paramTemplate.InsertVariable("parameter.type", param.Type);
                        paramTemplate.InsertVariable("parameter.value", param.Value);
                        paramTemplate.InsertVariableUnsafe("parameter.description", param.Description?.ToString().MarkdownToHtml());
                        //paramTemplate.InsertVariableUnsafe("parameter.value-html", param.Value?.ToString().MarkdownToHtml());
                        parameterTemplates.Add(paramTemplate);
                    }
                    itemTemplate.InsertTemplates("item.parameter-list", parameterTemplates);


                    itemTemplate.InsertVariable("css-classes", string.Join(" ", cssClasses));


                    // Recursively add all children
                    InsertItems(itemTemplate, templateName, "item.children", document, child.FullName, h1Offset, level + 1, insertCode);

                    templates.Add(itemTemplate);
                }
                template.InsertTemplates(variableName, templates);
                template.InsertVariable("package.name", document.Name);
            }catch(Exception e) {
                throw new BackendException("documentation-error", $"The documentation item with namespace {fullNamespace} could not be processed! "+errorHelper, e);
            }
        }

        private static void AddClass(string value, string name, List<string> cssClasses) {
            name = name.ToLowerInvariant();
            if (string.IsNullOrWhiteSpace(value)) {
                cssClasses.Add("no-" + name);
            } else {
                cssClasses.Add("has-" + name);
            }
        }

        private static void AddClass(bool value, string name, List<string> cssClasses) {
            name = name.ToLowerInvariant();
            if (value == true) {
                cssClasses.Add("is-" + name);
            } else {
                cssClasses.Add("not-" + name);
            }
        }

        public static void AddNamespaceNavigation(this NavigationBuilder navigation, string packageUrl , string packageName, string name, string templateName = null) {
            var currentNav = new StringBuilder();
            foreach (var part in name.Split("-")) {
                currentNav.Append(part);
                if(templateName != null)    navigation.Add($"{packageUrl}/{packageName}/namespace/{templateName}/" + currentNav, part);
                else                        navigation.Add($"{packageUrl}/{packageName}/namespace/" + currentNav, part);
                currentNav.Append("-");
            }
        }

        public static string MarkdownToHtml(this string val) {
            if (val == null) return null;
            // Dont lose breaks, hint: breaks can be done with two spaces ' ' at end of line
            var pipeline = new Markdig.MarkdownPipelineBuilder()
                //.UseSoftlineBreakAsHardlineBreak()
                .UsePipeTables()
                
                .Build();
            return Markdig.Markdown.ToHtml(val, pipeline);
        }
    }
}
