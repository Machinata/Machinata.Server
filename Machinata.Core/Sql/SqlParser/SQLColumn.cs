using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.SQL.SQLParser {

    /// <summary>
    /// WORK IN PROGRESS
    /// </summary>
    public class SqlColumn {
        
        public enum ColumnTypes {
            Unknown,
            Member,
            Key,
            PrimaryKey,
            UniqueKey,
            Constraint
        }

        public ColumnTypes ColumnType;
        public List<SqlToken> SQLTokens;

        public SqlColumn(List<SqlToken> tokens) {
            this.SQLTokens = tokens;
            if(SqlToken.MatchTokens(this.SQLTokens, 
                new SqlToken(SqlToken.TokenTypes.Keyword,"PRIMARY"),
                new SqlToken(SqlToken.TokenTypes.Keyword,"KEY"),
                new SqlToken(SqlToken.TokenTypes.OpenParenthesis),
                new SqlToken(SqlToken.TokenTypes.Name)
                )) {
                this.ColumnType = ColumnTypes.PrimaryKey;
            } else if(SqlToken.MatchTokens(this.SQLTokens, 
                new SqlToken(SqlToken.TokenTypes.Keyword,"UNIQUE"),
                new SqlToken(SqlToken.TokenTypes.Keyword,"KEY"),
                new SqlToken(SqlToken.TokenTypes.Name)
                )) {
                this.ColumnType = ColumnTypes.UniqueKey;
            } else if(SqlToken.MatchTokens(this.SQLTokens, 
                new SqlToken(SqlToken.TokenTypes.Keyword,"KEY"),
                new SqlToken(SqlToken.TokenTypes.Name)
                )) {
                this.ColumnType = ColumnTypes.Key;
            } else if(SqlToken.MatchTokens(this.SQLTokens, 
                new SqlToken(SqlToken.TokenTypes.Keyword,"CONSTRAINT"),
                new SqlToken(SqlToken.TokenTypes.Name)
                )) {
                this.ColumnType = ColumnTypes.Constraint;
            } else if(SqlToken.MatchTokens(this.SQLTokens, 
                new SqlToken(SqlToken.TokenTypes.Name)
                )) {
                this.ColumnType = ColumnTypes.Member;
            } else {
                this.ColumnType = ColumnTypes.Unknown;
            }
        }
        
        public string GeneratedSQL {
            get {
                return string.Join(" ", this.SQLTokens.Select(tt => tt.Text));
            }
        }
        
        public string FirstName {
            get {
                return this.SQLTokens.Where(t => t.TokenType == SqlToken.TokenTypes.Name).FirstOrDefault()?.Text;
            }
        }
        public string ColumnNameCleaned {
            get {
                return ColumnName.Replace("`","");
            }
        }

        public string ColumnName {
            get {
                if(this.ColumnType == ColumnTypes.PrimaryKey) {
                    var names = this.SQLTokens.Where(t => t.TokenType == SqlToken.TokenTypes.Name).Take(2).Select(t => t.Text);
                    return string.Join("_",names);
                } else if(this.ColumnType == ColumnTypes.UniqueKey) {
                    var names = this.SQLTokens.Where(t => t.TokenType == SqlToken.TokenTypes.Name).Take(1).Select(t => t.Text);
                    return string.Join("_",names);
                } else if (this.ColumnType == ColumnTypes.Constraint) {
                    var names = this.SQLTokens.Where(t => t.TokenType == SqlToken.TokenTypes.Name).Select(t => t.Text).Where(c => c.StartsWith("`FK_") == false);
                    return string.Join("_",names);
                } else {
                    return this.SQLTokens.First(t => t.TokenType == SqlToken.TokenTypes.Name).Text;
                }
            }
        }

        public string ColumnUID {
            get {
                return $"{this.ColumnType}_{this.ColumnName}".Replace("`","").ToUpper();
            }
        } 


        public string MemberType {
            get {
                var ret = new List<SqlToken>();
                bool inType = false;
                for(int i = 0; i < this.SQLTokens.Count; i++) {
                    var t = this.SQLTokens[i];
                    if (t.TokenType == SqlToken.TokenTypes.Type) {
                        ret.Add(t);
                        inType = true;
                    } else if (t.TokenType == SqlToken.TokenTypes.OpenParenthesis && inType == true) {
                        ret.Add(t);
                    } else if (t.TokenType == SqlToken.TokenTypes.CloseParenthesis && inType == true) {
                        ret.Add(t);
                        inType = false;
                    } else if(inType == true) {
                        ret.Add(t);
                    }
                }
                return string.Join(" ", ret.Select(t => t.Text));
            }
        }


        public void Reduce() {
            foreach(var t in SQLTokens) {
                //if(t.TokenType == SqlToken.TokenTypes.Name && t.Text.StartsWith("`FK_")) {
                //    Regex regex = new Regex("`FK_[0-9a-z]{32}`");
                //    t.Text = regex.Replace(t.Text, "`FK_AUTOGENERATED`");
                //}
            }
        }
    }
}
