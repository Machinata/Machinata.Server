using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Machinata.Core.Handler {


    /// <summary>
    /// A Machinata Handler that handles the serving of static files and
    /// pre-compiled static bundles mounted on /static.
    /// 
    /// Serves content files saved in the ContentFile storage system. Each file can be stored using different providers (for example on the local filesystem, via a network share, or via external providers such as AWS S3).
    /// Typically, serving content is in conjunction with the CMS system(ContentNodes).
    /// 
    /// Example URL:

    /// ```
    /// /content/{category}/{publicId}/{fullFilename}
    /// ```
    ///  Images(category == image) may use any of the imaging functionality provied by Core.Imaging (Core.Imaging.ImageFactory.ProcessImageForRequest).
    /// 
    /// </summary>
    /// <seealso cref="Machinata.Core.Handler.Handler" />
    public class ContentFileHandler : Core.Handler.Handler {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

      
        
        public static string CachePath = Core.Config.CachePath + Core.Config.PathSep + "content";
        public static string CachePathPDFPNG = CachePath + "-pdftopng";



        /// <summary>
        /// Gets the specific content file.
        /// If the content file cateogry is 'image', then the image can automatically be formatted (see Core.Imaging.ImageFactory.ProcessImage for documentation).
        /// </summary>
        /// <param name="category">The category.</param>
        /// <param name="publicId">The public identifier.</param>
        /// <param name="fullFilename">The full filename.</param>
        /// <exception cref="Backend404Exception">
        /// file-not-found;File not found
        /// or
        /// file-not-found;File not found
        /// or
        /// file-not-found;File not found
        /// </exception>
        [RequestHandler("/content/{category}/{publicId}/{fullFilename}", Core.Model.AccessPolicy.PUBLIC_ARN, null, Verbs.Get, ContentType.StaticFile)]
        public void File(string category, string publicId, string fullFilename) {

            _logger.Trace($"Downloading file {category}/{publicId}/{fullFilename}");

            var contentFile = DB.ContentFiles().GetByPublicId(publicId);
            if (contentFile == null) throw new Backend404Exception("file-not-found", "File not found");

            // Check category
            if (contentFile.FileCategory.ToString().ToLower() != category) throw new Backend404Exception("file-not-found", "File not found");

            // Check filename vs public Id
            if (contentFile.FullFileName.ToLower() != fullFilename.ToLower()) throw new Backend404Exception("file-not-found", "File not found");

            // Download file
            var filePath = Core.Data.DataCenter.DownloadFile(contentFile);

            // Do image transformations
            if(category == "image") {
                try {
                    filePath = Core.Imaging.ImageFactory.ProcessImageForRequest(filePath, this);
                } catch(Exception e) {
                    throw new BackendException("image-process-error", "Could not process the image transformation request: " + e.Message, e);
                }
            }

            // Do PDF transformations
            if (contentFile.FileExtension == "pdf") {
                // Re-format?
                if (!string.IsNullOrEmpty(this.Params.String("format"))) {
                    if (this.Params.String("format") == "png") {
                        
                        //create a PNG version of the first page and overwrite the filePath var with that
                        var fileName = Path.GetFileNameWithoutExtension(filePath);
                        var path = CachePathPDFPNG + Core.Config.PathSep + fileName + ".png";

                        Core.Caching.FileCacheGenerator.CreateIfNeeded(path, () => {
                          Core.PDF.PDFConverter.SavePDFAsImage(filePath, path);
                        });

                        filePath = path;
                          

                    } else {
                        throw new BackendException("invalid-format", "Format is not supported");
                    }
                }
            }
            
            // Transmit the file
            _logger.Trace($"Transmitting file {filePath}");
            string filename = null;
            if (this.Params.Bool("download",false) == true) filename = fullFilename;
            SetCachePolicy(System.Web.HttpCacheability.Public); // static resources can be cached by gateways/cdns
            SendFile(filePath, new TimeSpan(Core.Config.StaticCacheMaxDays, 0, 0, 0), filename);
        }
        
        

    }
}
