using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Machinata.Core.Handler {


    /// <summary>
    /// A Machinata Handler that handles the serving of static files and
    /// pre-compiled static bundles mounted on /static.
    /// </summary>
    /// <seealso cref="Machinata.Core.Handler.Handler" />
    public class StaticFileHandler : Core.Handler.Handler {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        

        /// <summary>
        /// Gets a static bundle resource. A bundle is a collection of js or css (or any text format) that is compiled
        /// into a single resource. Bundles can be automatically minimized, compressed or muzzled.
        /// </summary>
        /// <param name="path">The path.</param>
        [RequestHandler("/static/bundle/{path}", Core.Model.AccessPolicy.PUBLIC_ARN, null, Verbs.Get, ContentType.StaticFile)]
        public void StaticBundle(string path) {
            // Init
            string language = this.Params.String("lang");
            string themeName = this.Params.String("theme","default");
            string build = this.Params.String("build",Core.Config.BuildID);
            string charset = this.Params.String("charset","utf-8");
            var profile = this.Params.String("profile", null);

            // Build ID
            if (string.IsNullOrWhiteSpace(build) == false && build.Length > 36) {
                throw new BackendException("build-id-error", "Build ID is too long");
            }

            // Theme
            if (string.IsNullOrWhiteSpace(themeName) == false && themeName != "default") {
                Machinata.Core.Model.Theme.GetThemeByName(themeName); // try get existing theme
            }

            // Profile
            if (string.IsNullOrWhiteSpace(profile) == false) {
                var cleanedProfile = Core.Util.String.CreateShortURLForName(profile, makeLowercase: false, removeSpaces: true);
                if (cleanedProfile != profile) {
                    throw new BackendException("profile-name-error", "The profile is not valid");
                }
            }



            // Transmit the file
            string cachePath = Core.Bundles.Bundle.GetBundleCachePath(
                db: this.DB, 
                packageName: this.PackageName, 
                bundlePath: path, 
                language: language,
                themeName: themeName,
                build: build,
                debug: this.Params.Bool("debug",false),
                profile: profile,
                outputFormat: this.Params.String("format", null)
            );
            SetCachePolicy(System.Web.HttpCacheability.Public); // static resources can be cached by gateways/cdns
            SendFile(cachePath, new TimeSpan(Core.Config.StaticCacheMaxDays, 0, 0, 0), null, charset);
        }
        
        //NOTE: this was never used, neither is it needed since static/file handles images directly
        //[RequestHandler("/static/image/{path}", Core.Model.AccessPolicy.PUBLIC_ARN, null, Verbs.Get, ContentType.StaticFile)]
        //public void StaticImage(string path) {
        //    throw new NotImplementedException();
        //    _logger.Trace(path);
        //}

        /// <summary>
        /// Get's a static file.
        /// If the static file is in 'images', then the image can automatically be formatted (see Core.Imaging.ImageFactory.ProcessImage for documentation).
        /// </summary>
        /// <param name="path">The path.</param>
        [RequestHandler("/static/file/{path}", Core.Model.AccessPolicy.PUBLIC_ARN, null, Verbs.Get, ContentType.StaticFile)]
        public void StaticFile(string path) {
            _logger.Trace(path);
            string filePath = Core.Config.StaticPath + Core.Config.PathSep + path.Replace("/",Core.Config.PathSep);
            _logger.Trace(filePath);

            // Hotswap?
            filePath = Core.Util.Files.GetHotSwappableFile(filePath, null);
            
            // Do image transformations
            if(path.StartsWith("images/")) {
                try {
                    filePath = Core.Imaging.ImageFactory.ProcessImageForRequest(filePath, this);
                } catch(Exception e) {
                    throw new BackendException("image-process-error", "Could not process the image transformation request: " + e.Message, e);
                }
            }

            // Filename
            string filename = null;
            if(this.Params.Bool("download", false) == true) {
                filename = Core.Util.Files.GetFileNameWithExtension(filePath);
            }
             
            // Transmit the file
            if(!File.Exists(filePath)) {
                throw new Localized404Exception(this, "file-not-found", Core.Util.Files.GetFileNameWithExtension(filePath));
            }
            SetCachePolicy(System.Web.HttpCacheability.Public); // static resources can be cached by gateways/cdns
            SendFile(filePath, new TimeSpan(Core.Config.StaticCacheMaxDays, 0, 0, 0), filename);
        }
        
        

    }
}
