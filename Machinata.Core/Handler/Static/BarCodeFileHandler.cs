using BarcodeLib;
using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Util;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Machinata.Core.Handler {


    /// <summary>
    /// Code 128	Code11	Code 39 (Extended / Full ASCII)
    /// Code 93	EAN-8	EAN-13
    /// UPC-A	UPC-E	JAN-13
    /// MSI	ISBN	Standard 2 of 5
    /// Interleaved 2 of 5	PostNet	UPC Supplemental 2
    /// UPC Supplemental 5	Codabar	ITF-14
    /// Telepen	Pharmacode	FIM (Facing Identification Mark)
    /// </summary>
    /// <seealso cref="Machinata.Core.Handler.Handler" />
    public class BarCodeFileHandler : Core.Handler.Handler {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        public static string CachePath = Core.Config.CachePath + Core.Config.PathSep + "static";
        public static string CachePathBarCodes = CachePath + "-barcode";

        
        [RequestHandler("/static/barcode", Core.Model.AccessPolicy.PUBLIC_ARN, null, Verbs.Get, ContentType.StaticFile)]
        public void BarCode() {

            // Init cachepath
            string data = this.Params.String("data");
            int size = this.Params.Int("size", 600);
            int width = size;
            if(this.Params.String("width",null) != null) {
                width = this.Params.Int("width",width);
            }
            int height = size/3;
            if(this.Params.String("height",null) != null) {
                height = this.Params.Int("height",height);
            }
            string codeHash = Core.Encryption.DefaultHasher.HashString(data);
            string type = Params.String("type", Config.BarcodeTypeDefault);
            string format = Params.String("format", "png");
            string color = Params.String("color", "black");
            string bg = Params.String("background", "transparent");
            bool includeLabel = Params.Bool("label", false);
            int labelSize = Params.Int("label-size", 12);
            string cachePath = CachePathBarCodes + Core.Config.PathSep + codeHash + "_w" + width+ "_h" + height+ "_"+ type+ "_"+ (includeLabel?"label":"nolabel")+ "_ls"+ labelSize.ToString()+ "_" + color.Replace("#","")+ "_" + bg.Replace("#","") + "." + format;
            

            // Barcode Type
            var barcodeTypeDefault = Util.EnumHelper.ParseEnum(Config.BarcodeTypeDefault, true, TYPE.EAN13 );
            var barcodType = Util.EnumHelper.ParseEnum(type,true, barcodeTypeDefault);

            // Colors
            Color backColor = Color.White;
            Color frontColor = Color.Black;
            if (color == "white") {
                backColor = Color.Black;
                frontColor = Color.White;
            }
            
            // Create the bundle as cached if needed
            bool forceCreate = !Core.Config.StaticQRCodeCacheEnabled;
            Core.Caching.FileCacheGenerator.CreateIfNeeded(cachePath, ()=> {
                // Init
                _logger.Info("Creating bar code "+data);

                BarcodeLib.Barcode barcode = new BarcodeLib.Barcode() {
                    IncludeLabel = includeLabel,
                    Alignment = AlignmentPositions.LEFT,
                    Width = width,
                    Height = height,
                    RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                    BackColor = backColor,
                    ForeColor = frontColor,
                    LabelFont = new Font("Lucida Console", labelSize, FontStyle.Bold),
                    LabelPosition = LabelPositions.BOTTOMCENTER
                };

                var bitmap = barcode.Encode(barcodType, data);
                var imgFormat = System.Drawing.Imaging.ImageFormat.Png;
                if (format == "png") imgFormat = System.Drawing.Imaging.ImageFormat.Png;
                else if (format == "jpg") imgFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
                else if (format == "gif") imgFormat = System.Drawing.Imaging.ImageFormat.Gif;
                else throw new Exception("Format is not supported.");
                bitmap.Save(cachePath, imgFormat);

            }, forceCreate);

            SetCachePolicy(System.Web.HttpCacheability.Public); // static resources can be cached by gateways/cdns
            SendFile(cachePath, new TimeSpan(Core.Config.StaticCacheMaxDays, 0, 0, 0));
        }
        
        

    }
}
