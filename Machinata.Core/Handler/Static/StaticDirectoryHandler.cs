using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Machinata.Core.Handler {


    /// <summary>
    /// A simple handler for integrating a entire directory as a static file server.
    /// 
    /// The handler supports index.htm pages, and also automatically redirects not trailing / pages to a correct
    /// URL with a trailing /.
    /// 
    /// All the mime types supported by Core.Handler.SendFile() are supported.
    /// 
    /// Example integration:
    /// 
    /// public class GlueckStaticDirectoryHandler : StaticDirectoryHandler {
    ///     [RequestHandler("/glueck/{path?}", Core.Model.AccessPolicy.PUBLIC_ARN, null, Verbs.Get, ContentType.StaticFile)]
    ///     public void StaticFile(string path) {
    ///         SendFileAtPath(path);
    ///     }
    /// }
    /// 
    /// The files are placed in the static dir (in this case, ProjectDir/static/glueck).
    /// </summary>
    /// <seealso cref="Machinata.Core.Handler.Handler" />
    public abstract class StaticDirectoryHandler : Core.Handler.Handler {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        public static TimeSpan DefaultCacheTime = new TimeSpan(1, 0, 0, 0); // 1 days

        protected void SendFileAtPath(string path) {
            if (path.Contains("..")) throw new Exception("Invalid path symbol .. in StaticDirectoryHandler path!");
            if (path.Contains("./")) throw new Exception("Invalid path symbol ./ in StaticDirectoryHandler path!");
            // Get the path
            var dir = this.Route.Path;
            var reformattedPath = dir.Replace("{path}",path).Replace("{path?}",path).Replace("/", Core.Config.PathSep).Trim(Core.Config.PathSep[0]);
            string filePath = Core.Config.StaticPath + Core.Config.PathSep + reformattedPath;
            // Index page?
            string indexFile1 = (filePath + Core.Config.PathSep + "index.htm").Replace(Core.Config.PathSep + Core.Config.PathSep, Core.Config.PathSep);
            if (File.Exists(indexFile1)) {
                // Make sure we redirect to a full dir slash path, 
                // ie https://nerves.icasa.office.nerves.ch/glueck becomes https://nerves.icasa.office.nerves.ch/glueck/
                //if(!filePath.EndsWith("index.htm") && !filePath.EndsWith(Core.Config.PathSep)) {
                //    this.Context.Response.Redirect(this.Uri+"/",false);
                //    return;
                //}
                var incommingURL = this.Context.Request.Url.LocalPath;
                if(!incommingURL.EndsWith("/")) {
                    this.Context.Response.Redirect(this.Uri+"/",false);
                }
                // Overwrite
                filePath = indexFile1;
            }
            // Hotswap?
            filePath = Core.Util.Files.GetHotSwappableFile(filePath, null);
            // Transmit the file
            SendFile(filePath, DefaultCacheTime);
        }

    }
}
