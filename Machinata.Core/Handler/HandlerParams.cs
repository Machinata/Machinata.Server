using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Exceptions;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Util;

namespace Machinata.Core.Handler {

    public partial class HandlerParams {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Private Static Fields

        private HttpContext _context = null;

        #endregion

        #region Constructor

        public HandlerParams(HttpContext context) {
            _context = context;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the string parameter from context form or query string. If neither is specified, the default value is returned. 
        /// This method can return an empty string.
        /// Note: When a handler has been invoked without a context (like from a task manager), the default value is returned...
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public string String(string name, string defaultValue = null) {
            // When a handler has been invoked without a context (like from a task manager), the default
            // value is returned...
            if (_context == null) return defaultValue;
            // Try form first
            string ret = _context.Request.Form[name];
            if (ret != null) return ret;
            // Then try query
            ret = _context.Request.QueryString[name];
            if (ret != null) return ret;
            // If all failed return default
            return defaultValue;
        }

        /// <summary>
        /// Gets the string from the request body via the InputStream.
        /// After the stream has been read in, the contents are cached so that
        /// this method can be called twice.
        /// Note: this method is not thread safe
        /// </summary>
        public string Data(string defaultValue = null) {
            if (_context == null) return defaultValue;

            if (_cachedData == null) {
                _context.Request.InputStream.Position = 0;
                using (var inputStream = new StreamReader(_context.Request.InputStream)) {
                    _cachedData = inputStream.ReadToEnd();
                }
            }
            if (string.IsNullOrEmpty(_cachedData)) return defaultValue;
            else return _cachedData;
        }
        private string _cachedData = null;

        /// <summary>
        /// Gets a JSON object (JObject) from the request body. If there is not request body data
        /// null is returned.
        /// </summary>
        public Newtonsoft.Json.Linq.JObject JSON() {
            if (_context == null) return null;

            return Core.JSON.ParseJsonAsJObject(this.Data(), true);
        }

        public int Int(string name, int defaultValue) {
            // Get value
            string ret = String(name, defaultValue.ToString());
            // Try to convert
            try {
                return int.Parse(ret);
            } catch { }
            // Return default
            return defaultValue;
        }

       

        public Double Double(string name, double defaultValue) {
            // Get value
            string ret = String(name, defaultValue.ToString());
            // Try to convert
            try {
                return double.Parse(ret);
            } catch { }
            // Return default
            return defaultValue;
        }
        
        public double? Double(string name, double? defaultValue) {
            // Get value
            string ret = String(name, defaultValue?.ToString());
            // Try to convert
            try {
                return double.Parse(ret);
            } catch { }
            // Return default
            return defaultValue;
        }

        public long Long(string name, long defaultValue) {
            // Get value
            string ret = String(name, defaultValue.ToString());
            // Try to convert
            try {
                return long.Parse(ret);
            } catch { }
            // Return default
            return defaultValue;
        }

        public decimal Decimal(string name, decimal defaultValue, bool sanitize = true) {
            // Get value
            string ret = String(name, defaultValue.ToString());
            if(sanitize && ret != null) {
                ret = ret.Replace("'", "");
            }
            // Try to convert
            try {
                return decimal.Parse(ret);
            } catch { }
            // Return default
            return defaultValue;
        }

        /// <summary>
        /// Gets the selected item from a SINGLE selectable entity list
        /// </summary>
        /// <param name="possibleItems">The possible items.</param>
        /// <returns></returns>
        public string GetSelectedItem(IEnumerable<string> possibleItems) {
            foreach(var possibleItem in possibleItems) {
                var on = this.String(possibleItem, null) == "on";
                if (on) {
                    return possibleItem;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the key/value params from a a list of possible keys
        /// </summary>
        /// <param name="possibleItems">The possible items.</param>
        /// <returns></returns>
        public Dictionary<string,string> GetKeyValuePairs(IEnumerable<string> possibleItems) {
            var selectedKeys = _context.Request.Form.AllKeys.Where(k => possibleItems.Contains(k));

            // Query String ? 
            if (selectedKeys.Count() == 0) {
                selectedKeys = _context.Request.QueryString.AllKeys.Where(k => possibleItems.Contains(k));
                return selectedKeys.ToDictionary(k => k, v => this._context.Request.QueryString[v]);
            }
            return selectedKeys.ToDictionary(k => k, v => this._context.Request.Form[v]);
        }


        /// <summary>
        /// Gets the key/value params from both the form and the query string.
        /// </summary>
        /// <param name="possibleItems">The possible items.</param>
        /// <returns></returns>
        public Dictionary<string, string> AllValues() {
            var form = _context.Request.Form.AllKeys.ToDictionary(k => k, v => this._context.Request.Form[v]);
            var query = _context.Request.QueryString.AllKeys.ToDictionary(k => k, v => this._context.Request.QueryString[v]);
            form.ToList().ForEach(x => query.Add(x.Key, x.Value));
            return form;
        }

        public Dictionary<string, string> AllParamsAndValues() {
            var @params = _context.Request.Params.AllKeys.ToDictionary(k => k, v => this._context.Request.Params[v]);
            return @params;
        }

        /// <summary>
        /// Gets the all the form values for the given input name.
        /// If all the values are empty string the default value is returned.
        /// </summary>
        public IEnumerable<string> StringValues(string name, string[] defaultValue) {
            var ret = _context.Request.Params.GetValues(name);
            if (ret == null || ret.Count() == 0 || (ret.Count(e => e != "")==0) ) return defaultValue;
            return ret;
        }

        /// <summary>
        /// It usnafe if the values contain comas ','
        /// Better use StringValuesEmtpy or StringValues
        /// </summary>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <param name="separator"></param>
        /// <param name="splitOptions"></param>
        /// <returns></returns>
        /// <summary>
        /// Converts a form/query value into a string array with the given separator.
        /// Note that this method does not support forms with multiple inputs of the same name, if the input values contain the seperator. For such cases it is better to use StringValues
        /// </summary>
        [Obsolete]
        public IEnumerable<string> StringArray(string name, string[] defaultValue, char separator = ',', StringSplitOptions splitOptions =  StringSplitOptions.None) {
            // Get value
            string ret = String(name, null);
            if (ret == null || ret == "") return defaultValue;
            // Try to convert
            try {
                return ret.Split(new char[] { separator }, splitOptions);
            } catch { }
            // Return default
            return defaultValue;
        }

        /// <summary>
        /// Reads a string array: only from the query string
        /// Note: Does not support form params
        /// </summary>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <param name="separator"></param>
        /// <param name="splitOptions"></param>
        /// <returns></returns>
        public IEnumerable<string> StringArrayFromQueryString(string name, string[] defaultValue, char separator = ',', StringSplitOptions splitOptions = StringSplitOptions.None) {
            // Get value
            string ret = _context.Request.QueryString[name];
            if (ret == null || ret == "") return defaultValue;
            // Try to convert
            try {
                return ret.Split(new char[] { separator }, splitOptions);
            } catch { }
            // Return default
            return defaultValue;
        }


        /// <summary>
        /// Gets safely string arrays from the params
        /// The values in the result are allowed to be string.empty
        /// </summary>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public IEnumerable<string> StringValuesAllowEmtpy(string name, string[] defaultValue) {
            // Get value
            if (_context == null) return defaultValue;
            // Try params
            var ret = _context.Request.Params.GetValues(name);
            if (ret != null) return ret;
            return defaultValue;

        }

        public T Enum<T>(string name, T defaultValue, bool ignoreCase = true) {
            string ret = String(name);
            if (string.IsNullOrWhiteSpace(ret)) {
                return defaultValue;
            }
            try {
                Type t = typeof(T);
                if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>)) {
                    t = t.GetGenericArguments().First();
                    return (T)(System.Enum.Parse(t, ret, ignoreCase));
                }
                return (T)(System.Enum.Parse(typeof(T), ret, ignoreCase));
            } catch (Exception e) {
                return defaultValue;
            }
        }

        /// <summary>
        /// Parses DateTime from the Context, 
        /// 1. exact 'yyyy-MM-dd'
        /// 2. loosly
        /// 3. milliseconds UTC (FormBuilder)
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public DateTime DateTime(string name, DateTime defaultValue) {
            // Get value
            string ret = String(name, defaultValue.ToString("yyyy-MM-dd"));
            // Try to convert
            try {
                return System.DateTime.ParseExact(ret, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            } catch { 
                try {
                    return System.DateTime.Parse(ret);
                } catch {
                    try {
                        return Time.ConvertStringToDateTime(ret);
                    } catch {
                        return defaultValue;
                    }
                }
            }
        }


        /// <summary>
        /// Parses the DateTime with the exact global Config.DateFormat
        /// if fails returns default value
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public DateTime DateTimeGlobalConfigFormat(string name, DateTime defaultValue) {
            // Get value
            string ret = String(name, defaultValue.ToString(Core.Config.DateFormat));
            // Try to convert
            try {
                return System.DateTime.ParseExact(ret, Config.DateFormat, System.Globalization.CultureInfo.InvariantCulture);
            } catch {
                return defaultValue;
            }
        }

        /// <summary>
        /// Parses the DateTime with the exact format
        /// if fails returns default value
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="format"></param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public DateTime DateTimeFormat(string name, string format, DateTime defaultValue) {
            // Get value
            string ret = String(name);
            // Try to convert
            try {
                return System.DateTime.ParseExact(ret, format, System.Globalization.CultureInfo.InvariantCulture);
            } catch {
                return defaultValue;
            }
        }


        public DateTime? DateTimeGlobalConfigFormatNullable(string name, DateTime? defaultValue) {
            // Get value
            string ret = String(name, defaultValue?.ToString(Core.Config.DateFormat));
            // Try to convert
            try {
                return System.DateTime.ParseExact(ret, Config.DateFormat, System.Globalization.CultureInfo.InvariantCulture);
            } catch {
                return defaultValue;
            }
        }

        public short Short(string name, short defaultValue) {
            // Get value
            string ret = String(name, defaultValue.ToString());
            // Try to convert
            try {
                return short.Parse(ret);
            } catch { }
            // Return default
            return defaultValue;
        }


        public bool Bool(string name, bool defaultValue) {
            // Get value
            string ret = String(name, defaultValue.ToString());
            // Try to convert
            try {
                return bool.Parse(ret);
            } catch { }
            // Return default
            return defaultValue;
        }

        public bool? BoolNullable(string name, bool? defaultValue) {
            // Get value
            string ret = String(name, defaultValue.ToString());
            // Try to convert
            try {
                return bool.Parse(ret);
            } catch { }
            // Return default
            return defaultValue;
        }

        public int? Int(string name, int? defaultValue) {
            // Get value
            string ret = String(name, defaultValue.ToString());
            // Try to convert
            try {
                return int.Parse(ret);
            } catch { }
            // Return default
            return defaultValue;
        }

        public string Language() {
            return Core.Util.HTTP.DetectLanguageFromContext(_context);
        }

        public string Currency() {
            // Param or cookie?
            if (_context == null) { return null; }
            string requestCurrency = _context.Request.QueryString["currency"];
            if (requestCurrency == null) requestCurrency = _context.Request.Form["currency"];
            //if (requestCurrency == null) requestCurrency = _context.Request.Params["currency"]; // this includes cookies, which is not ideal
            if (requestCurrency == null && _context.Request.Cookies["PageCurrency"] != null) requestCurrency = _context.Request.Cookies["PageCurrency"].Value.ToUpper();
            if (requestCurrency != null) return requestCurrency.ToUpper();
            // Nothing was set
            return null;
        }

        public IEnumerable<T> Enums<T>(string name, IEnumerable<T> defaultValue, bool ignoreCase = true) {
            var ret = StringArray(name, new string[] { });

            IList<T> enums = new List<T>();
            if (!ret.Any()) {
                return defaultValue;
            }

            foreach (var value in ret) {
                try {
                    Type t = typeof(T);
                    if (t.IsGenericTypeDefinition && t.GetGenericTypeDefinition() == typeof(Nullable<>)) {
                        t = t.GetGenericArguments().First();
                        enums.Add( (T)(System.Enum.Parse(t, value, ignoreCase)));
                    }
                    enums.Add((T)(System.Enum.Parse(typeof(T), value, ignoreCase)));
                } catch (Exception e) {
                    return defaultValue;
                }
            }

            return enums;
        }

        public bool ContainsKey(string key) {
            if (_context == null) return false;
            // Try form first
            string ret = _context.Request.Form[key];
            if (ret != null) return true;
            // Then try query
            ret = _context.Request.QueryString[key];
            if (ret != null) return true;
            // If all failed return false
            return false;
        }

        #endregion


    }
}
