using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Templates;

namespace Machinata.Core.Handler {


    /// <summary>
    /// A very simple, lightweight, and self-contained debugging handler to help
    /// understand and debug problems.
    /// This handler is designed to use zero resources (for example templates, bundles
    /// et cetera), thus is rather simple but robust.
    /// </summary>
    /// <seealso cref="Machinata.Core.Handler.Handler" />
    public class DebuggingHandler : Core.Handler.Handler {
        
        #region Handler Policies

        // No policies required - special handling via IP restrictions

        #endregion

        #region Private Helper Methods

        private void _insertLog(StringBuilder log, List<string> logs, string filters, bool insertTimeDeltas) {
            _insertNewSection(log);
            DateTime lastSectionTimestamp = DateTime.MinValue;
            for(var i = logs.Count-1; i >= 0; i--) {
                var line = logs[i];
                var prevLine = ""; 
                if(i > 0) prevLine = logs[i-1];
                bool addLine = true;
                if (line.EndsWith(Core.Logging.LOG_SEPARATOR)) {
                    addLine = false;
                    _insertNewSection(log);
                }
                if(!string.IsNullOrEmpty(filters)) {
                    foreach (var filter in filters.Split(' ')) {
                        // Not filter?
                        if (filter.StartsWith("!")) {
                            var notFilter = filter.TrimStart('!');
                            if (line.ToLower().Contains(notFilter.ToLower())) addLine = false;
                        } else {
                            if (!line.ToLower().Contains(filter.ToLower())) addLine = false;
                        }
                    }
                }
                if(addLine) {
                    if (insertTimeDeltas) {
                        // Parse out timecode
                        string deltaMS = "-----";
                        string deltaColor = null;
                        try {
                            var prevTimestamp = DateTime.Parse(prevLine.Substring(0, prevLine.IndexOf('|')));
                            var thisTimestamp = DateTime.Parse(line.Substring(0, line.IndexOf('|')));
                            var diff = thisTimestamp.Subtract(prevTimestamp);
                            var delta = (int)(diff.TotalMilliseconds);
                            if (delta > 200) deltaColor = "red";
                            else if (delta > 100) deltaColor = "red";
                            else if (delta > 50) deltaColor = "orange";
                            deltaMS = delta.ToString();
                        } catch { }
                        deltaMS = deltaMS.PadLeft(5);
                        if (deltaColor != null) deltaMS = "<span style=\"color:" + deltaColor + "\">" + deltaMS + "</span>";
                        // Append log
                        log.Append(deltaMS + "ms" + "|" + line + "\n");
                    } else {
                        log.Append(line + "\n");
                    }
                }
            }
            _closeSection(log);
        }
        private void _insertCommonHeader(string title) {
            var filter = this.Params.String("filter","");
            this.StringOutput.Append("<html>");
            this.StringOutput.Append("<head>");
            this.StringOutput.Append("<title>"+title+" - Machinata Debugging</title>");
            this.StringOutput.Append("<style>");
            this.StringOutput.Append("* {font-family: monospace}");
            this.StringOutput.Append("input {border:1px solid black; margin-top: 4px; margin-right: 4px;}");
            this.StringOutput.Append("</style>");
            this.StringOutput.Append("</head>");
            this.StringOutput.Append("<body>");
            this.StringOutput.Append("<h2>" + title + "</h2>");
            this.StringOutput.Append("<a href='trace'>" + "Trace" + "</a>");
            this.StringOutput.Append("&#160;|&#160;");
            this.StringOutput.Append("<a href='error'>" + "Error" + "</a>");
            this.StringOutput.Append("&#160;|&#160;");
            this.StringOutput.Append("<a href='startup'>" + "Startup" + "</a>");
            this.StringOutput.Append("&#160;|&#160;");
            this.StringOutput.Append("<a href='stats'>" + "Stats" + "</a>");
            this.StringOutput.Append("&#160;|&#160;");
            this.StringOutput.Append("<a href='caches'>" + "Caches" + "</a>");
            this.StringOutput.Append("&#160;|&#160;");
            this.StringOutput.Append("<a href='config'>" + "Config" + "</a>");
            this.StringOutput.Append("&#160;|&#160;");
            this.StringOutput.Append("<a href='build'>" + "Build" + "</a>");
            this.StringOutput.Append("&#160;|&#160;");
            this.StringOutput.Append("<a href='modules'>" + "Modules" + "</a>");
            this.StringOutput.Append("&#160;|&#160;");
            this.StringOutput.Append("<a href='echo'>" + "Echo" + "</a>");
            this.StringOutput.Append("<form><input title='Set multiple filters using spaces, using ! to negate a filter.' name='filter' placeholder='filter' value='"+filter+"'/><input type='submit' value='Refresh'/></form>");
        }
        private void _insertCommonFooter() {
            this.StringOutput.Append("</body></html>");
        }
        private int _lastSectionNumber = 0;
        private void _insertNewSection(StringBuilder log) {
            _closeSection(log);
            _lastSectionNumber++;
            string color = _lastSectionNumber % 2 == 0 ? "#CEECDE" : "#D4D4D4";
            log.Append($"<div style='padding:6px;background-color:{color};'>");
        }
        private void _closeSection(StringBuilder log) {
            log.Append("</div>");
        }
        private void _insertSubtitle(StringBuilder log, string text) {
            log.Append($"<h2>{text}</h2>");
        }

        private void _validateRequestAndAccess() {
            if (!Core.Config.DebuggingHandlerEnabled) throw new BackendException("debugging-disabled","The debugging handler has been disabled.");
            this.RequireAdminIP();
        }

        #endregion
        
        [RequestHandler("/debugging/trace/", Core.Model.AccessPolicy.PUBLIC_ARN)]
        public void DebuggingTrace() {
            _validateRequestAndAccess();

            // Compile log
            var log = new StringBuilder();
            if(Core.Logging.MemoryTarget != null) _insertLog(log, Core.Logging.MemoryTarget.Logs, this.Params.String("filter"), true);
            // Compile page
            _insertCommonHeader("Trace");
            this.StringOutput.Append("<pre>"+log.ToString()+"</pre>");
            _insertCommonFooter();
        }

        
        [RequestHandler("/debugging/error/", Core.Model.AccessPolicy.PUBLIC_ARN)]
        public void ErrorTrace() {
            _validateRequestAndAccess();

            // Compile log
            var log = new StringBuilder();
            if(Core.Logging.MemoryTarget != null) _insertLog(log, Core.Logging.MemoryTarget.Logs, "|ERROR|", false);
            // Compile page
            _insertCommonHeader("Error");
            this.StringOutput.Append("<pre>"+log.ToString()+"</pre>");
            _insertCommonFooter();
        }

        [RequestHandler("/debugging/startup/", Core.Model.AccessPolicy.PUBLIC_ARN)]
        public void DebuggingStartup() {
            _validateRequestAndAccess();

            // Compile log
            var log = new StringBuilder();
            _insertLog(log, Core.Logging.GetStartupLogs(),this.Params.String("filter"), true);
            // Compile page
            _insertCommonHeader("Startup");
            this.StringOutput.Append("<pre>"+log.ToString()+"</pre>");
            _insertCommonFooter();
        }

        [RequestHandler("/debugging/config/", Core.Model.AccessPolicy.PUBLIC_ARN)]
        public void DebuggingConfig() {
            _validateRequestAndAccess();

            // Compile log
            var configs = new List<string>();
            foreach(var config in Core.Config.AllLoadedConfigs().OrderBy(e => e.Name)) {
                var val = config.Value;
                if(config.Name.Contains("Key") || config.Name.Contains("Password") || config.Name.Contains("Secret")) {
                    val = "**********";
                }
                configs.Add($"{config.Name}: '{val}' ({config.Key} from {config.Source})");
            }
            var log = new StringBuilder();
            _insertLog(log, configs, this.Params.String("filter"), false);
            // Compile page
            _insertCommonHeader("Config");
            this.StringOutput.Append("<pre>"+log.ToString()+"</pre>");
            _insertCommonFooter();
        }

        [RequestHandler("/debugging/caches/", Core.Model.AccessPolicy.PUBLIC_ARN)]
        public void DebuggingCaches() {
            _validateRequestAndAccess();
            var log = new StringBuilder();

            log.AppendLine(" -- mutex store");
            var mutexStore = new List<string>();
            foreach (var entry in Core.Util.Mutex.GetStore().GetMutexes()) {
                mutexStore.Add(entry.Awaiting.ToString().PadLeft(3) + "  " + entry.Key + ": " + entry.Awaiting + " waiting");
            }
            _insertLog(log, mutexStore, this.Params.String("filter"), false);

            log.AppendLine(" -- dynamic cache");
            var dynamicCache = new List<string>();
            foreach (var entry in Config.Dynamic.GetCache()) {
                dynamicCache.Add(entry.Key + ": " + entry.Value);
            }
            _insertLog(log, dynamicCache, this.Params.String("filter"), false);

            log.AppendLine(" -- text cache");
            var textCache = new List<string>();
            //foreach (var entry in Localization.Text.RegisteredTexts()) {
            //    textCache.Add(entry.Value);
            //}
            textCache.Add(Localization.Text.RegisteredTexts().Count().ToString());
            _insertLog(log, textCache, this.Params.String("filter"), false);

            log.AppendLine(" -- theme cache");
            var themeCache = new List<string>();
            foreach (var entry in Theme.GetCache()) {
             themeCache.Add(entry.Key + ": " + entry.Value);
            }
            _insertLog(log, themeCache, this.Params.String("filter"), false);

            var templateCacheEntries = PageTemplate.Cache.GetCache();
            log.AppendLine(" -- template cache: entries :" + templateCacheEntries.Count);
            var templateCache = new List<string>();
            foreach (var entry in templateCacheEntries) {
                templateCache.Add(entry.Key + ": variable count: " + entry.Value.VariablesCopy.Count());
            }
            _insertLog(log, templateCache, this.Params.String("filter"), false);



            // Compile page
            _insertCommonHeader("cache");
            this.StringOutput.Append("<pre>" + log.ToString() + "</pre>");
            _insertCommonFooter();
        }


        [RequestHandler("/debugging/build/", Core.Model.AccessPolicy.PUBLIC_ARN)]
        public void DebuggingBuild() {
            _validateRequestAndAccess();
            var log = new StringBuilder();
            var info = Core.Util.Git.GetInfo();

            _insertLog(log, new List<string>() {
                "Build ID: " +Core.Config.BuildID,
                "Build Timestamp: " +Core.Config.BuildTimestamp.ToString(Core.Config.DateTimeFormat) + " ("+Core.Config.BuildTimestamp.Ticks+")",
                "Build User: "+Core.Config.BuildUser,
                "Build Machine: "+Core.Config.BuildMachine,
                "Project Package: "+Core.Config.ProjectPackage
            }, this.Params.String("filter"), false);
            _insertLog(log, info.ToString().Split('\n').Reverse().Where(s=>s.Trim() != string.Empty).ToList(), this.Params.String("filter"), false);

            // Compile page
            _insertCommonHeader("Build");
            this.StringOutput.Append("<pre>" + log.ToString() + "</pre>");
            _insertCommonFooter();
        }


        [RequestHandler("/debugging/stats/", Core.Model.AccessPolicy.PUBLIC_ARN)]
        public void DebuggingStats() {
            _validateRequestAndAccess();
            var log = new StringBuilder();

            _insertLog(log, new List<string>() {
                "Dynamic Config Cache: " +Config.Dynamic.GetCache().Count,
                "Localization Text Cache: " +Localization.Text.RegisteredTexts().Count,
                "Theme Cache: "+Theme.GetCache().Count,
                "Page Template Cache: "+PageTemplate.Cache.GetCache().Count,
                "Mutex Store: "+Core.Util.Mutex.GetStore().GetKeys().Count,
            }, this.Params.String("filter"), false);
            
            // Compile page
            _insertCommonHeader("Stats");
            this.StringOutput.Append("<pre>" + log.ToString() + "</pre>");
            _insertCommonFooter();
        }


        [RequestHandler("/debugging/modules/", Core.Model.AccessPolicy.PUBLIC_ARN)]
        public void DebuggingModules() {
            _validateRequestAndAccess();
            var log = new StringBuilder();

            foreach(var assembly in Core.Reflection.Assemblies.GetMachinataAssemblies()) {
                var module = assembly.GetName().Name;
                Build.BuildInfo buildInfo = new Build.BuildInfo();
                try {
                    buildInfo = Core.Util.Build.GetBuildInfoForModule(module);
                } catch { }
                _insertLog(log, new List<string>() {
                    "Build Timestamp: " +buildInfo.BuildTimestamp.ToString(Core.Config.DateTimeFormat) + " ("+buildInfo.BuildTimestamp.Ticks+")",
                    "Build User: "+buildInfo.BuildUser,
                    "Build Machine: "+buildInfo.BuildMachine,
                    "Module: " +module
                }, this.Params.String("filter"), false);
            }
            

            // Compile page
            _insertCommonHeader("Modules");
            this.StringOutput.Append("<pre>" + log.ToString() + "</pre>");
            _insertCommonFooter();
        }


        [RequestHandler("/debugging/echo/", Core.Model.AccessPolicy.PUBLIC_ARN)]
        public void DebuggingEcho() {
            _validateRequestAndAccess();
            var log = new StringBuilder();

            {
                var keyVals = new List<string>();
                keyVals.Add($"HttpMethod = " + this.Context.Request.HttpMethod);
                keyVals.Add($"RequestType = " + this.Context.Request.RequestType);
                keyVals.Add($"Path = " + this.Context.Request.Path);
                keyVals.Add($"QueryString = " + this.Context.Request.QueryString);
                keyVals.Add($"Url = " + this.Context.Request.Url);
                keyVals.Add($"RawUrl = " + this.Context.Request.RawUrl);
                keyVals.Add($"UrlReferrer = " + this.Context.Request.UrlReferrer);
                keyVals.Add($"UserAgent = " + this.Context.Request.UserAgent);
                keyVals.Add($"UserHostAddress = " + this.Context.Request.UserHostAddress);
                keyVals.Add($"UserHostName = " + this.Context.Request.UserHostName);
                keyVals.Add($"UserLanguages = " + string.Join(",",this.Context.Request.UserLanguages));
                keyVals.Add("Request:");
                _insertLog(log, keyVals, this.Params.String("filter"), false);
            }
            {
                var keyVals = new List<string>();
                foreach (string key in this.Context.Request.Form.Keys) {
                    keyVals.Add($"{key} = " + this.Context.Request.Form[key]);
                }
                keyVals.Add("Form:");
                _insertLog(log, keyVals, this.Params.String("filter"), false);
            }
            {
                var keyVals = new List<string>();
                foreach (string key in this.Context.Request.QueryString.Keys) {
                    keyVals.Add($"{key} = " + this.Context.Request.QueryString[key]);
                }
                keyVals.Add("QueryString:");
                _insertLog(log, keyVals, this.Params.String("filter"), false);
            }
            {
                var keyVals = new List<string>();
                foreach (string key in this.Context.Request.Files.Keys) {
                    keyVals.Add($"{key} = " + this.Context.Request.Files[key].FileName + $" ({this.Context.Request.Files[key].ContentLength} bytes)");
                }
                keyVals.Add("Files:");
                _insertLog(log, keyVals, this.Params.String("filter"), false);
            }
            {
                var keyVals = new List<string>();
                foreach (string key in this.Context.Request.Cookies.Keys) {
                    keyVals.Add($"{key} = " + this.Context.Request.Cookies[key].Value + $" ({this.Context.Request.Cookies[key].Path})");
                }
                keyVals.Add("Cookies:");
                _insertLog(log, keyVals, this.Params.String("filter"), false);
            }
            {
                var keyVals = new List<string>();
                foreach (string key in this.Context.Request.Headers.Keys) {
                    keyVals.Add($"{key} = " + this.Context.Request.Headers[key]);
                }
                keyVals.Add("Headers:");
                _insertLog(log, keyVals, this.Params.String("filter"), false);
            }

            // Compile page
            _insertCommonHeader("Echo");
            this.StringOutput.Append("<pre>" + log.ToString() + "</pre>");
            _insertCommonFooter();
        }


    }
}
