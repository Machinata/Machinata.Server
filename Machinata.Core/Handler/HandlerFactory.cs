using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Core.Routes;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Handler {


    /// <summary>
    /// The factory for mapping call paths (routes) to handler methods.
    /// </summary>
    public abstract partial class Handler : IPopulateProvider{

        #region Immutable Declarations

        /// <summary>
        /// The handler types that were discovered.
        /// </summary>
        private static readonly List<Type> _handlerTypes = new List<Type>();
        
        /// <summary>
        /// The master delegate map that maps routes to a request handler function (tupled with the http verb)
        /// This list is sorted using the longest (more specific) routes first, which allows for longer routes to take precedence over
        /// shorter ones (less specific)
        /// </summary>
        //private static SortedDictionary<string, Dictionary<Verbs, Tuple<Func<object>, MethodInfo>>> _delegateMap = new  SortedDictionary<string, Dictionary<Verbs, Tuple<Func<object>, MethodInfo>>>(new DescendingRoutePriorityComparer());

        //public static readonly Regex RouteParamRegex = new Regex(@"\{[^\/]*\}", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        //public static readonly Regex RouteOptionalParamRegex = new Regex(@"\{[^\/]*\?\}", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        //public const string RegexRouteReplace = "(.*)";

        //public const string RouteLanguageKeyName = "route_lang";

        #endregion

        #region Handler Initialization and Route Matching
        
        private static Handler _createHandlerForURI(Uri uri, Verbs verb, HttpContext context) {
            // Get normalized path
            var regExRouteParams = new Dictionary<string, object>();
            var path = _normalizeRegexPath(verb, uri, regExRouteParams);

            // Make sure we got a route
            //if (path == null) throw new Backend404Exception("invalid-path", "No page was found for '"+uri.PathAndQuery+"'.");
            if (path == null) throw new Localized404Exception(null, "invalid-path", uri.PathAndQuery);
            _logger.Trace("Found path "+path);

            // Validate that the verb matches
            Verbs matchingVerb = Verbs.None;
            if(Core.Routes.Route.GetRouteMapping()[path].ContainsKey(verb)) {
                matchingVerb = verb;
            } else if(Core.Routes.Route.GetRouteMapping()[path].ContainsKey(Verbs.Any)) {
                matchingVerb = Verbs.Any;
            } else {
                //throw new Backend404Exception("invalid-path", "No page was found for '"+uri.PathAndQuery+"' with verb '"+verb+"'.");
                throw new Localized404Exception(null, "invalid-verb", uri.PathAndQuery, verb.ToString());
            }

            // Extract handler and request
            var route = Core.Routes.Route.GetRouteMapping()[path][matchingVerb];
            var handler = route.HandlerFactory() as Handler;
            handler._method = route.MethodInfo;
            var requestAttribute = handler._method.GetCustomAttributes(typeof(RequestHandlerAttribute), true).FirstOrDefault() as RequestHandlerAttribute;

            // Do our custom request validation (for SQL injection et cetera)
            // Note: this only happens on form and query string
            if(route.SkipRequestValidation == false && context != null) {
                var validator = new System.Web.Util.RequestValidator(); // default .net validator
                int validationFailureIndex;
                // Test query string
                foreach(var key in context.Request.QueryString.AllKeys) {
                    var val = context.Request.QueryString[key];
                    var ret = validator.InvokeIsValidRequestString(context, val, System.Web.Util.RequestValidationSource.QueryString, key, out validationFailureIndex);
                    if (ret == false) throw new BackendException("invalid-request", $"An invalid request was detected ({key} = {val}).");
                }
                // Test form
                foreach(var key in context.Request.Form.AllKeys) {
                    var val = context.Request.Form[key];
                    var ret = validator.InvokeIsValidRequestString(context, val, System.Web.Util.RequestValidationSource.QueryString, key, out validationFailureIndex);
                    if (ret == false) throw new BackendException("invalid-request", $"An invalid request was detected ({key} = {val}).");
                }
            }

            // Setup the handler
            if(requestAttribute.Content != ContentType.Default) {
                handler.ContentType = requestAttribute.Content;
            } else {
                handler.ContentType = handler.GetDefaultContentType();
            }
            handler.RequiredRequestARN = requestAttribute.ARN;
            if(handler.RequiredRequestARN == null) {
                handler.RequiredRequestARN = handler.GetDefaultRequiredARN();
            }
            handler.Uri = uri;
            handler.Route = route;
            handler.Verb = verb.ToString().ToLower(); // We set the original intended verb, not the matching verb
            handler.RequestPath = uri.LocalPath;
            handler.Language = route.Language;

            // Set the language from the route path if it was not explicitly set by the route
            if (route.Language == null) {
                if (Core.Config.RoutingEnableLanguageRootRoutes && regExRouteParams.ContainsKey(Core.Routes.Route.RouteLanguageKeyName)) {
                    // Set lang from route param
                    handler.Language = regExRouteParams[Core.Routes.Route.RouteLanguageKeyName].ToString();
                } else {
                    // Set default
                    //TODO: @dan - is this really correct? doesnt this break the default behavior when language is set by the cookie or other...?
                    handler.Language = Core.Config.LocalizationDefaultLanguage;
                }
            }
            
            // Parse the arguments to their intended type
            handler._args = new List<object>();
            foreach (var arg in handler._method.GetParameters()) {
                // Is it in our route path
                if (regExRouteParams.ContainsKey(arg.Name) == false) {
                    // It's missing, so we skip - but first we check if there is a default val (ie foo(int = 2))
                    if(arg.HasDefaultValue) {
                        handler._args.Add(arg.DefaultValue);
                    }
                    continue;
                }
                // get a reference to the parse method
                var parameterTypeNullable = Nullable.GetUnderlyingType(arg.ParameterType);
                var parameterHasDefault = arg.HasDefaultValue;
                var parseMethod = parameterTypeNullable != null
                    ? parameterTypeNullable.GetMethod(nameof(int.Parse), new[] { typeof(string) })
                    : arg.ParameterType.GetMethod(nameof(int.Parse), new[] { typeof(string) });

                // add the parsed argument to the argument list if available
                if (parseMethod != null)
                {
                    // parameter is nullable and value is empty, so force null
                    if (parameterTypeNullable != null && string.IsNullOrWhiteSpace((string)regExRouteParams[arg.Name])) {
                        handler._args.Add(null);
                    } else {
                        handler._args.Add(parseMethod.Invoke(null, new[] { regExRouteParams[arg.Name] }));
                    }
                } else {
                    handler._args.Add(regExRouteParams[arg.Name]);
                }
            }

            return handler;
        }

        #endregion

        #region Handler Processing for Web Requests

        /// <summary>
        /// Processes a exception by automatically detecting the 'nearest' handler and package
        /// and loading the corresponding error template.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="be">The be.</param>
        public static void ProcessException(HttpContext context, BackendException be) {
            
            // Get a cleaned up url
            Uri uri = _cleanURL(context, context.Request.Url);
            
            // Find a matching handler for the uri and it's package template
            // First we search for a template called "error-{code}", then for just "error"
            var handler = FindNearestHandlerForUri(uri);

            // Setup the handler
            handler._currentException = be;
            handler.PrepareForError(uri, be);

            // Preprocess, handle error, finish
            handler.SetupForContext(context);
            handler.SetupForContentType();
            handler.OnError(be);
            handler.Finish();
        }

        /// <summary>
        /// Finds the nearest handler for given URI.
        /// If no handler could be found, then a default URI "/" is attemted to find the root route handler.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Could not find a default handler using the root routing path '+bestMatchingHandlerKey+'.</exception>
        public static Handler FindNearestHandlerForUri(Uri uri) {
            // Get the normalized path
            var path = uri.LocalPath;
            _logger.Info("Searching for a suitable handler for '" + path + "'...");
            Handler bestMatchingHandler = null;
            string bestMatchingHandlerKey = null;
            foreach(var key in Core.Routes.Route.GetRouteMapping().Keys) {
                // Does the requested path contain the key path?
                if(path.StartsWith(key)) {
                    _logger.Trace("  Found " + key);
                    // Is it a better (longer) match?
                    if (bestMatchingHandlerKey == null || key.Length > bestMatchingHandlerKey.Length) {
                        _logger.Trace("    Better match than '" + bestMatchingHandlerKey+"'!");
                        bestMatchingHandlerKey = key;
                    } else {
                        //_logger.Trace("  '" + bestMatchingHandlerKey + "' is better");
                    }
                }
            }
            // Fallback
            if(bestMatchingHandlerKey == null) {
                bestMatchingHandlerKey = "/";
                if (!Core.Routes.Route.GetRouteMapping().ContainsKey(bestMatchingHandlerKey)) throw new Exception("Could not find a default handler using the root routing path '"+bestMatchingHandlerKey+"'.");
            }
            // Get the final route
            var route = Core.Routes.Route.GetRouteMapping()[bestMatchingHandlerKey].FirstOrDefault().Value;
            // Create handler
            bestMatchingHandler = route.HandlerFactory() as Handler;
            _logger.Info("Found handler " + bestMatchingHandler.Name);
            return bestMatchingHandler;
        }

        public static void ProcessRequest(HttpContext context) {
            var uri = _cleanURL(context, context.Request.Url);
            // Wrap "Dangerous cookie" or similiar exceptions in a BackendException
            try {
                ProcessRequest(context, uri, context.RequestVerb());
            } catch (System.Web.HttpRequestValidationException hrve) {
                // MSDN: The exception that is thrown when
                // a potentially malicious input string is received
                // from the client as part of the request data.
                // This class cannot be inherited.
                throw new BackendException("malicious-input-error", hrve.Message, hrve);
            }
        }

        public static void ProcessRequest(HttpContext context, Uri uri, Verbs verb) {

            // Force redirect to main dir?
            if(Core.Config.RedirectHostsToPublicHost != null && Core.Config.RedirectHostsToPublicHost.Count > 0) {
                var currentHost = uri.Host;
                if (Core.Config.RedirectHostsToPublicHost.Contains(currentHost)) {
                    var publicUri = new Uri(Core.Config.PublicURL);
                    var newUri = new UriBuilder(uri);
                    newUri.Host = publicUri.Host;
                    // Redirect with 301 Moved Permanently
                    context.Response.RedirectPermanent(newUri.Uri.ToString(),false);
                    return;
                }
            }

            // Force HTTPS?
            if(Core.Config.SecurityForceHTTPS) {
                // Set header
                // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security
                context.Response.Headers["Strict-Transport-Security"] = "max-age=2630000"; // one month
                // Force redirect and end response
                if (uri.Scheme != "https") {
                    var httpsUrl = uri.ToString().Replace("http://","https://");
                    // Redirect with 301 Moved Permanently
                    context.Response.RedirectPermanent(httpsUrl,false);
                    return;
                }
            }
                

            // Stopwatch
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            
            // Get a cleaned up url
            _logger.Trace(Core.Logging.LOG_SEPARATOR);
            _logger.Trace("Processing Virtual URL "+uri);

            // Create the handler
            var handler = _createHandlerForURI(uri, verb, context);
            
            // Preprocess
            handler.SetupForContext(context);
            handler.SetupForContentType();

            // Time tracking...
            handler.ProcessTimeHandlerMapping = stopWatch.Elapsed;
            
            // Validate ARN
            handler.ValidateARN(handler.RequiredRequestARN);

            // Invoke!
            handler._invoke();

            // At this point we have invoked the method, now we can post-process

            // Finish the response
            handler.ProcessTimeRequestProcess = stopWatch.Elapsed;
            handler.Finish();

            _logger.Trace("Processed in "+handler.ProcessTimeRequestProcess.TotalMilliseconds+" ms");
        }

        #endregion

        #region Handler Processing for Tasks and Manual Invokation

        /// <summary>
        /// Creates a handler of the specified type for the given route.
        /// This can be used to dynamically invoke a handler and a method by only specifying the route.
        /// For example:
        /// 
        /// var handler = Core.Handler.Handler.CreateForRoute<Ticketing.Handler.ShopPdfHandler>("/pdf/shop/invoices/invoice/hTKaaQ");
        /// handler.Invoke();
        /// handler.SaveToFile("out.pdf");
        /// 
        /// </summary>
        public static T CreateForRoute<T>(string routePath, string language = null, Verbs verb = Verbs.Any, Model.User user = null) where T : Handler {
            // Create a suitable uri
            var uri = new Uri("invoke://"+routePath);

            // Create the handler
            var handler = _createHandlerForURI(uri, verb, null);
            
            // Preprocess
            handler.SetupForInvocation(language);
            handler.SetupForContentType();
            if (user != null) {
                handler.SetupForUser(user);
            }

            return (T)handler;
        }

       

        #endregion

        #region Handler Application Startup

        [Core.Lifecycle.OnApplicationStartup]
        public static void OnApplicationStartup() {

            // Load all the handlers for each dll
            var assemblies = Core.Reflection.Assemblies.GetMachinataAssemblies();
            foreach(var assembly in assemblies) {
                _logger.Trace("Found Assembly " + assembly.GetName());
                _loadHandlersFromDLL(assembly);
            }

            // Rebuild the route tree
            Route.RebuildRouteTree();

            _logger.Trace("Loaded all handlers and routes ("+Core.Routes.Route.GetRouteMapping().Keys.Count.ToString()+")!");
            foreach(var key in Core.Routes.Route.GetRouteMapping().Keys) {
                _logger.Trace("  Route '" + key + "'");
            }
        }

        #endregion

        #region Private Helper Methods

        private static Uri _cleanURL(HttpContext context, Uri uri) {
            // Trim out the extra /AppName which AWS puts in each deployed EBS instance
            string url = uri.ToString();
            int appPathPos = url.IndexOf(context.Request.ApplicationPath);
            if(context.Request.ApplicationPath != "/") {
                if(appPathPos > 0) {
                    url = url.Substring(0, appPathPos) + url.Substring(appPathPos + context.Request.ApplicationPath.Length);
                }
            }
            var newUri = new Uri(url);
            // Fix trailing slash in path
            if(newUri.AbsolutePath.EndsWith("/")) {
                url = newUri.Scheme + "://" + newUri.Host + newUri.AbsolutePath.TrimEnd('/') + uri.Query;
                newUri = new Uri(url);
            }
            return newUri;
        }

        private static void _loadHandlersFromDLL(Assembly dll) {
            foreach (Type type in dll.GetTypes()) {
                // Is this a ContentControl class? We find out by propogating up to the super class looking for a type match...
                System.Type t = type;
                bool isRequestHandler = false;
                while (t.BaseType != null) {
                    if (t.ToString() == "Machinata.Core.Handler.Handler") {
                        isRequestHandler = true;
                        break;
                    }
                    t = t.BaseType;
                }
                if (!type.IsAbstract && isRequestHandler) {

                    // Load an instance and register the name
                    //Handler handler = dll.CreateInstance(type.FullName) as Handler;
                    //Handler.RequestTypeToHandlerMapping[handler.GetHandlerRequestType()] = type;
                    _loadRequestHandlersForHandler(type);
                }
            }
        }

        private static void _loadRequestHandlersForHandler(Type handlerType) {
            _logger.Trace("  Found Handler "+handlerType.Name);

            // Init the constructor method
            Func<object> handlerFactory = () => Activator.CreateInstance(handlerType);

            // Loop all methods of handler
            var methods = handlerType.GetMethods(BindingFlags.Instance | BindingFlags.Public);
            foreach (var method in methods) {

                // Get the RequestHandlerAttribute attribute
                {
                    var attributes = method.GetCustomAttributes<RequestHandlerAttribute>(true);
                    if (attributes.Count() > 0) _logger.Trace("    Found Request Handler " + method.Name);
                    string firstRoutePath = null;
                    foreach (var attribute in attributes) {
                        if (attribute.GetType() == typeof(MultiRouteRequestHandlerAttribute)) continue;
                        _logger.Trace("      Found Route with path '" + attribute.Path + "'");
                        // Create the route
                        var route = Routes.Route.CreateRouteForAttribute(attribute, handlerFactory, method);
                        if (firstRoutePath == null) firstRoutePath = route.Path;
                        // Set the alias to self if it is not set
                        if (route.AliasFor == Route.ALIAS_FOR_FIRST_ROUTE) {
                            route.AliasFor = firstRoutePath;
                        }
                        // Register it
                        _logger.Trace("        Registering route with path '" + route.Path + "' and lang '"+route.Language+"'" + (route.AliasFor != null? " (alias for "+route.AliasFor+")":""));
                        Route.RegisterRoute(route);
                    }
                }
                
                // Get the MultiRouteRequestHandlerAttribute attribute
                {
                    var attributes = method.GetCustomAttributes<MultiRouteRequestHandlerAttribute>(true);
                    if (attributes.Count() > 0) _logger.Trace("    Found Multi-Route Request Handler " + method.Name);
                    foreach (var attribute in attributes) {
                        _logger.Trace("      Found Multi-Route with id '" + attribute.Path + "'");
                        // Create the routes
                        var routes = Routes.Route.CreateRoutesForMultiRouteAttribute(attribute, handlerFactory, method);
                        // Register it
                        foreach (var route in routes) {
                        _logger.Trace("        Registering Multi-Route route with path '" + route.Path + "' and lang '"+route.Language+"'" + (route.AliasFor != null? " (alias for "+route.AliasFor+")":""));
                            Route.RegisterRoute(route);
                        }
                    }
                }
            }
            
            // Register the handle type
            _handlerTypes.Add(handlerType);
        }

        /// <summary>
        /// Normalizes a path meant for Regex matching, extracts the route parameters, and returns the registered
        /// path in the internal delegate map.
        /// </summary>
        /// <param name="verb">The verb.</param>
        /// <param name="context">The context.</param>
        /// <param name="routeParams">The route parameters.</param>
        /// <returns></returns>
        private static string _normalizeRegexPath(Verbs verb, Uri uri, Dictionary<string, object> routeParams)
        {
            // Init
            var path = uri.LocalPath;
            _logger.Trace($"Normalizing path '{path}'...");

            // Validate if a language is specified as the root route
            if (Core.Config.RoutingEnableLanguageRootRoutes) {
                var pathParts = path.TrimStart('/').Split('/');
                if (pathParts.Count() > 0) {
                    var firstPart = pathParts.First();
                    if (Core.Config.LocalizationSupportedLanguages.Contains(firstPart)) {
                        // First part is language!
                        _logger.Trace($"Found language '{firstPart}' in path");
                        routeParams.Add(Core.Routes.Route.RouteLanguageKeyName, firstPart);
                        path = '/'+string.Join("/", pathParts.Skip(1));
                        _logger.Trace($"New path is '{path}'");
                    } else {
                        // Just a regular path
                        // Nothing to do...
                    }
                }
            }

            // Find match
            foreach (var route in Core.Routes.Route.GetRouteMapping().Keys) {
                // Init
                var routeOrig = route;
                var formattedRegex = Core.Routes.Route.RouteParamRegex.Replace(route, Core.Routes.Route.RegexRouteReplace);
                var regex = new Regex(formattedRegex);
                var match = regex.Match(path);
                var routeParts = route.Split('/');

                // Do an exception for the root path
                if(formattedRegex == "/" && path != "/") {
                    continue;
                }
                // Find out if the path matches and verb matches
                bool pathMatches = match.Success;
                bool verbMatches = Core.Routes.Route.GetRouteMapping()[route].Keys.Contains(verb) || Core.Routes.Route.GetRouteMapping()[route].Keys.Contains(Verbs.Any);
                //if (!match.Success || !(_delegateMap[route].Keys.Contains(verb))) {
                if (pathMatches)    _logger.Trace($"  Found matching route '{route}'");
                if (pathMatches && verbMatches)    _logger.Trace($"    Route has matching verb '{verb}'");
                if (pathMatches && !verbMatches)   _logger.Trace($"    Route does not have any verb '{verb}'");
                if (!pathMatches || !verbMatches) {
                    // Init
                    var optionalPath = Core.Routes.Route.RouteOptionalParamRegex.Replace(route, string.Empty);
                    var tempPath = path;
                    // Reformat trailing path
                    if (optionalPath.Last() == '/' && path.Last() != '/') {
                        tempPath += "/";
                    }
                    // Validate if the route is actually a optional parameter
                    if (optionalPath == tempPath) {
                        foreach (var pathPart in routeParts.Where(x => x.StartsWith("{"))) {
                            routeParams.Add(
                                pathPart.Replace("{", string.Empty)
                                    .Replace("}", string.Empty)
                                    .Replace("?", string.Empty), null);
                        }
                        return route;
                    }

                    continue;
                } else {
                    // Match!

                    // Make sure it is a full match if the route has a variable in it but the path does not....
                    if (!route.Contains("{") && route != path) continue;

                }

                var i = 1; // match group index
                foreach (var pathPart in routeParts.Where(x => x.StartsWith("{"))) {
                    routeParams.Add(
                        pathPart.Replace("{", string.Empty).Replace("}", string.Empty).Replace("?", string.Empty),
                        match.Groups[i++].Value);
                }

                return route;
            }

            return null;
        }
        
        #endregion
        
        #region Public Helper Methods
        
        public static Uri CleanURL(HttpContext context, Uri url) {
            return _cleanURL(context, url);
        }

        public static List<T> GetBlankHandlersForType<T>() where T : Handler {
            List<T> handlers = new List<T>();
            foreach(var type in _handlerTypes) {
                if(typeof(T).IsAssignableFrom(type)) {
                    handlers.Add( (T)Activator.CreateInstance(type));
                }
            }
            return handlers;
        }

        #endregion  

        #region IPopulateProvider 

        public string GetValue(string key) {
            return this.Params.String(key);
        }

        #endregion

    }

    #region Extensions for Context

    public static partial class HttpContextExtensions {
        
        /// <summary>
        /// Retrieves the Request Verb of this context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static Verbs RequestVerb(this HttpContext context)
        {
            Verbs verb;
            Enum.TryParse(context.Request.HttpMethod.ToLowerInvariant().Trim(), true, out verb);
            return verb;
        }
    }

    #endregion
}
