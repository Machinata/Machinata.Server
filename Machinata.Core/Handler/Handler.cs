using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Exceptions;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Util;
using System.Runtime.ExceptionServices;
using Machinata.Core.Encryption.Providers;
using System.ComponentModel;

/// <summary>
/// 
/// </summary>
namespace Machinata.Core.Handler {

    #region Handler Enums
    /// <summary>
    /// Enumerates the different HTTP Verbs
    /// </summary>
    public enum Verbs
    {
        // <summary>
        // Wildcard Method
        // </summary>
        Any,
        // <summary>
        // DELETE Method
        // </summary>
        Delete,
        // <summary>
        // GET Method
        // </summary>
        Get,
        // <summary>
        // HEAD method
        // </summary>
        Head,
        // <summary>
        // OPTIONS method
        // </summary>
        Options,
        // <summary>
        // PATCH method
        // </summary>
        Patch,
        // <summary>
        // POST method
        // </summary>
        Post,
        // <summary>
        // PUT method
        // </summary>
        Put,
        // <summary>
        // NO Method
        // </summary>
        None
    }

    /// <summary>
    /// Enumerates the different types of Content Types which a handler method can return.
    /// </summary>
    public enum ContentType
    {
        Default,
        TemplatePage,
        StaticFile,
        Binary,
        JSON,
        Text
    }

    /// <summary>
    /// Enumerates the different types of Content Types which a handler method can return.
    /// </summary>
    public enum TwoFactorAuthenticationScope {
        Request,
        Session
    }

    #endregion

    /// <summary>
    /// A Handler class provides methods with the RequestHandler attribute. 
    /// In addtion, a handler can do additional pre and post processing.
    /// </summary>
    public abstract partial class Handler {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constants

        public const string ROUTE_PARAMETER_REGEX = "{([a-zA-Z0-9-.]+)}";

        #endregion

        #region Private Static Fields

        private static int _handlerRequestCount = 0;
        
        #endregion

        #region Private Fields
        
        private StringBuilder _stringOutput = null;
        private string _stringMimeType = null;
        private string _fileOutput = null;
        private string _fileCharset = null; // Sets the Response.ContentType = "XYZ; charset=utf-8";
        private string _fileName = null; // Download name for _fileOutput
        private byte[] _binaryOutput = null;
        private string _binaryFilename = null; // Download name for _binaryOutput
        private TimeSpan _cacheTime = TimeSpan.Zero; // Default is no cache time setting
        private HttpCacheability _cachePolicy = HttpCacheability.Private; // Default is no cache time setting
        private object _jsonOutput = null;
        private Core.Model.ModelContext _db = null;

        private MethodInfo _method;
        private List<object> _args;
        
        private HttpContext _context;
        private HandlerRequest _request;
        private HandlerParams _params;

        protected BackendException _currentException = null;
        
        #endregion

        #region Public Model Properties
        
        public virtual Core.Model.ModelContext DB {
            get {
                if(_db == null) {
                    _db = Model.ModelContext.GetModelContext(this);
                }
                return _db;
            }
        }

        private Model.AuthToken _authToken = null;
        public Model.AuthToken AuthToken {
            get {
                if (_authToken != null) return _authToken;

                _authToken = GetAuthTokenFromContext();
                if(_authToken != null) _user = _authToken.User;

                return _authToken;
            }
        }

        private Model.APIKey _apiKey = null;
        public Model.APIKey APIKey {
            get {
                if (_apiKey != null) return _apiKey;

                _apiKey = GetAPIKeyFromContext();
                if(_apiKey != null) _user = _apiKey.User;

                return _apiKey;
            }
        }

        private Model.User _user = null;
        public Model.User User {
            get {
                if (_user != null) return _user;
                // When we access AuthToken, make sure to catch the non exist method, and just silently return null
                try {
                    // Return the user from the auth token, if we have one
                    if (this.AuthToken != null) _user = this.AuthToken.User;
                } catch (BackendException e) {
                    if (e.Code != BackendException.CODE_AUTHTOKEN_NONEXIST) throw e;
                }
                return _user;
            }
        }

        #endregion

        #region Public Fields

        public HandlerEvents Events = new HandlerEvents();

        public HandlerRequest Request {
            get {
                if (_request == null) throw new Exception("The HandlerRequest was not set!");
                return _request;
            }
        }
        
        public HandlerParams Params {
            get {
                if (_params == null) throw new Exception("The HandlerParams was not set!");
                return _params;
            }
        }

        public StringBuilder StringOutput {
            get {
                return _stringOutput;
            }
        }

        public string FileOutput {
            get {
                return _fileOutput;
            }
        }

        public string Name {
            get {
                return this.GetType().Name;
            }
        }

        public string AssemblyName
        {
            get
            {
                return System.Reflection.Assembly.GetAssembly(this.GetType()).GetName().Name;
            }
        }

        public virtual string PackageName {
            get {
                return this.AssemblyName;
            }
        }

        /// <summary>
        /// Name of the package to fallback to by default if the template cannot be found in the package.
        /// </summary>
        public virtual string FallbackPackageName {
            get {
                return null;
            }
        }

        public virtual string DefaultMimeType
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// The route path for the request that was mapped.
        /// For example, /admin/database/{table}/{page}
        /// </summary>
        public Core.Routes.Route Route;

        /// <summary>
        /// The original route of the request. This is typically null, however
        /// some pages (such as error pages, maybe have this set).
        /// </summary>
        public Core.Routes.Route OriginalRoute;

        
        public string Verb;

        /// <summary>
        /// The request path from the request.
        /// For example, /admin/database/Users/3
        /// </summary>
        public string RequestPath;

        /// <summary>
        /// The required ARN (Access Resource Name) required to process this request.
        /// </summary>
        public string RequiredRequestARN;

        public HttpContext Context {
            get {
                return _context;
            }
            set {
                _context = value;
                _request = new HandlerRequest(value);
                _params = new HandlerParams(value);
            }
        }

        
        public string Language;
        public string Currency;
        public int RequestCount;
        public Uri Uri;
        public ContentType ContentType;
        public TimeSpan ProcessTimeHandlerMapping;
        public TimeSpan ProcessTimeRequestProcess;

        #endregion

        #region Context Helpers

        /// <summary>
        /// Sets the language from context. 
        /// If none is specified (or detected), then the default is set.
        /// If one is specified or detected, and it is supported, then that is taken. 
        /// If one is specified but not supported, then the default is set.
        /// </summary>
        public void SetLanguageFromContext() {
            var langContext = this.Params.Language();
            if (langContext != null && langContext != "") {
                // Not null - supported?
                if (Core.Config.LocalizationSupportedLanguages.Contains(langContext)) {
                    //if (this.Language != langContext) this.Events.Trigger(HandlerEvents.HANDLER_EVENT_LANGUAGE_CHANGED);
                    this.Language = langContext;
                    return;
                } else {
                    //if (this.Language != Core.Config.LocalizationDefaultLanguage) this.Events.Trigger(HandlerEvents.HANDLER_EVENT_LANGUAGE_CHANGED);
                    this.Language = Core.Config.LocalizationDefaultLanguage;
                    return;
                }
            }
                
            // Fallback
            this.Language = Core.Config.LocalizationDefaultLanguage;
        }

        public void SetCurrencyFromContext() {
            var currencyContext = this.Params.Currency();
            if(currencyContext != null && currencyContext != "") {
                // Not null - supported?
                if (Core.Config.CurrencySupported.Contains(currencyContext)) {
                    //if (this.Currency != currencyContext) this.Events.Trigger(HandlerEvents.HANDLER_EVENT_CURRENCY_CHANGED);
                    this.Currency = currencyContext;
                } else {
                    //if (this.Currency != Core.Config.CurrencyDefault) this.Events.Trigger(HandlerEvents.HANDLER_EVENT_CURRENCY_CHANGED);
                    this.Currency = Core.Config.CurrencyDefault;
                }

                // Figure out if the user is changing the currency
                // We know if the currency is changing manually by the user if the query param is set
                // and the cookie is set to something else (or blank)
                if(_context.Request.Params["currency"] != null) {
                    string cookieValue = null;
                    if (_context.Request.Cookies["PageCurrency"] != null) cookieValue = _context.Request.Cookies["PageCurrency"].Value.ToUpper();
                    if (cookieValue != _context.Request.Params["currency"].ToUpper()) this.Events.Trigger(HandlerEvents.HANDLER_EVENT_CURRENCY_CHANGED);
                }

                // Set new cookie for client
                RegisterCurrencyForUser(this.Currency);
            } else {
                // Null
                this.Currency = Core.Config.CurrencyDefault;
            }
        }

        public void RegisterCurrencyForUser(string currency) {
            // Set
            this.Currency = currency;
            // Send cookie
            var cookie = new HttpCookie("PageCurrency");
            cookie.Value = this.Currency;
            cookie.Expires = DateTime.UtcNow.AddDays(30);
            this.Context.Response.Cookies.Add(cookie);
        }

        public Model.APIKey GetAPIKeyFromContext() {
            // When a handler has been invoked without a context (like from a task manager), 
            // there is no auth token
            if (this.Context == null) return null;

            // Get the authentication header
            string authHeader = this.Context.Request.Headers[API.APIAuth.AUTHENTICATION_HEADER];
            if (string.IsNullOrEmpty(authHeader)) return null;

            // Validate that we it is a machinata auth header
            if (!authHeader.StartsWith(API.APIAuth.AUTHENTICATION_HEADER_TYPE)) return null;

            // Extract the API key
            return API.APIAuth.ValidateRequest(this.Context, this.DB);
        }
        
        public virtual Model.AuthToken  GetAuthTokenFromContext() {
            // When a handler has been invoked without a context (like from a task manager), 
            // there is no auth token
            if (this.Context == null) return null;

            // Get the auth token hash from the request uri
            string hash = this.Context.Request.Params["auth-token"];
            if (hash == null && this.Context.Request.Cookies["AuthTokenHash"] != null) hash = this.Context.Request.Cookies["AuthTokenHash"].Value;

            // Do we have an auth token hash?
            if (hash != null) {

                // Grab the token from the database
                Model.AuthToken token = this.DB.AuthTokens()
                    .Include(nameof(AuthToken.User))
                    .FirstOrDefault(t => t.Hash == hash);

                // Verify from database
                if (token == null) throw new Backend401Exception("authtoken-nonexist", "Invalid Auth Token hash!", this.RequiredRequestARN);
                if (token.Valid == false) throw new Backend401Exception("authtoken-nonexist", "Your login is no longer valid.", this.RequiredRequestARN);
                if (token.User.Enabled == false) throw new Backend401Exception("authtoken-nonexist", "Your account is no longer enabled.", this.RequiredRequestARN);

                // Expired?
                if (DateTime.Now.ToUniversalTime() >= token.Expires) {
                    throw new Backend401Exception("authtoken-nonexist", "Your login has expired.", this.RequiredRequestARN);
                }

                // Register the auth token and user
                return token;

            } else {
                return null;
            }
        }

        /// <summary>
        /// Checks that the API call being made is coming from Machinata frontend via a double salted hash check and timeframe window.
        /// </summary>
        public virtual void ValidateAPICallRequest() {
            if (Core.Config.ValidateFrontendToBackendRequestsEnabled == true) {
                // Init
                var givenValue = this.Params.String("machinatachecka"); // path
                var givenTimestamp = this.Params.String("machinatacheckb"); // timestamp
                var givenHash = this.Params.String("machinatacheckc"); // hash
                var expectedValue = givenValue; //TODO: @dankrusi make sure the path (checka) matches the api call endpoint
                // Validate using common helper
                Core.Encryption.FrontendToBackendRequestValidator.ValidateCheckValues(expectedValue, givenValue, givenTimestamp, givenHash);
            }
        }

        #endregion

        #region Handler Lifecycle

        public Handler() {
            RequestCount = _handlerRequestCount++;
            //_logger.Trace("Creating handler for request #"+RequestCount +this.Uri);
        }

        public void Invoke() {
            _invoke();
        }

        private void _invoke() {
            // Now, check if the call is handled asynchronously.
            try {
                
                // If the handler is not asynchronous, simply call the method.
                try {
                    this._method.Invoke(this, this._args.ToArray());
                } catch (System.Reflection.TargetInvocationException e) {
                    // Throw the inner, we're not interetsed in reflections problems...
                    //   throw e.InnerException; old -> this loses the stacktrack

                    e.InnerException.PreserveStackTrace();
                    throw e.InnerException;

                } catch (Exception e) {
                    throw e;
                }
            } catch(Exception e) {
                if(this != null && this.ShouldHandleExceptionsInline()) {
                    var be = Core.Exceptions.BackendException.WrapExceptionWithBackendException(e);
                    this._currentException = be;
                    this.OnError(be);
                } else {
                    throw e;
                }
            }
        }
        
        
        /// <summary>
        /// Gets the default required ARN for RequestHandler methods that do not explicitly specify a access ARN.
        /// The default ARN is AccessPolicy.DEFAULT_ARN, which is the route.
        /// For many handlers, it makes sense to define the ARN based on the entity the handler is handling. 
        /// For example:
        ///     return Business.GetDefaultAccessARN();
        /// If the handler is an admin handler, then one might use:
        ///     return Business.GetDefaultAdminAccessARN();
        /// </summary>
        /// <returns></returns>
        public virtual string GetDefaultRequiredARN() {
            return AccessPolicy.DEFAULT_ARN;
        }

        /// <summary>
        /// Gets the default type of content. This is used if a request handler attribute does
        /// not specify the content type.
        /// </summary>
        /// <returns></returns>
        public virtual ContentType GetDefaultContentType() {
            return ContentType.Text;
        }

        public virtual bool ShouldHandleExceptionsInline() {
            return false;
        }

        /// <summary>
        /// Call this method to register a custom user. In normal cases, this should not be called and the user is automatically
        /// extracted from the context (ie a cookie or API key).
        /// Note: This will not set a auth token.
        /// </summary>
        /// <param name="user">The user.</param>
        public virtual void SetupForUser(User user) {
            this._user = user;
        }

        public virtual void SetupForContext(HttpContext context) {
            // Register context for use within the request
            this.Context = context;
            // Set language and currency
            if(this.Language == null) this.SetLanguageFromContext();
            if(this.Currency == null) this.SetCurrencyFromContext();
            // Set api key
            var apiKey = this.GetAPIKeyFromContext();
            if(apiKey != null) {
                this._apiKey = apiKey;
                this._user = apiKey.User;
            }
        }

        public virtual void SetupForInvocation(string language = null) {
            this.Context = null;
            if (language != null) {
                this.Language = language;
            } else {
                if(this.Language == null) this.SetLanguageFromContext();
            }
            if(this.Currency == null) this.SetCurrencyFromContext();
        }

        public virtual void SetupForContentType() {

            if(this.ContentType == ContentType.Text) {

                // String
                _stringOutput = new StringBuilder();

            } else if(this.ContentType == ContentType.StaticFile) {

                // StaticFile
                // Nothing to setup...

            } else if(this.ContentType == ContentType.JSON) {

                // JSON
                // Nothing to setup...

            } else if (this.ContentType == ContentType.Binary) {

                // Binary
                // Nothing to setup...

            //} else if (this.ContentType == ContentType.Default) {
            //
            //    // Default
            //    // Nothing to setup...
            //
            } else {

                throw new BackendException("invalid-content-type","Sorry, the content type '"+this.ContentType.ToString()+"' is not supported.");

            }
        }

        public virtual void PrepareForError(Uri uri, BackendException be) {

        }

        /// <summary>
        /// Called when an error occurs during processing.
        /// In addition, this method can be called when no route can be found.
        /// </summary>
        /// <param name="be">The be.</param>
        public virtual void OnError(BackendException be) {
            // Set the status code
            if (this.Context != null && this.Context.Response != null) {
                this.Context.Response.StatusCode = be.Status;
                
                this.Context.Response.Headers[BackendException.ERROR_HTTP_HEADER_STATUS] = be.Status.ToString();
                this.Context.Response.Headers[BackendException.ERROR_HTTP_HEADER_CODE] = be.Code;
                this.Context.Response.Headers[BackendException.ERROR_HTTP_HEADER_MESSAGE] = be.Message;
            }
            // Is it unknown?
            if (be.Code == BackendException.CODE_UNKNOWN_ERROR) {
                try {
                    Core.EmailLogger.SendMessageToAdminEmail("Unknown exception", this.PackageName, this.Context, this, be);
                } catch (Exception sendEmailException) {
                    _logger.Error(sendEmailException, "There was an error sending a exception email for the error: " + be.Code + ": " + be.Message);
                }
            }
        }

        public void RequireARN(string requiredARN) {
            ValidateARN(requiredARN);
        }

        public void RequireWriteARN() {
            ValidateARN(AccessPolicy.WRITE_ARN);
        }

        public void RequireSuperuser() { 
            this.RequireLogin();
            if (this.User.IsSuperUser == false) {
                throw new Backend403Exception("insufficient-rights", "Sorry, only a superuser is allowed for this actions", AccessPolicy.SUPERUSER_ARN);
            }
        }

        public void RequireLogin() {
            if (this.User == null) {
                throw new Localized401Exception(this, "login-required", this.RequiredRequestARN);
            }
        }

        public void RequireAdmin() {
            if (this.User == null) {
                throw new Localized401Exception(this, "authorization-required", this.RequiredRequestARN);
            } else if (this.User.IsAdminOrSuperUser == false) {
                throw new Localized401Exception(this, "authorization-required", this.RequiredRequestARN);
            }
        }

        public void RequireTwoFactorAuthentication(TwoFactorAuthenticationScope scope = TwoFactorAuthenticationScope.Request) {

            if (this.User == null) {
                throw new Localized401Exception(this, "authorization-required", this.RequiredRequestARN);
            }

            // Does this user not have 2fa enabled?
            if (this.User.TwoFactorStatus == User.TwoFactorAuthenticationStatuses.Disabled) {
                throw new Localized401Exception(this, "two-factor-authentication-not-enabled", this.RequiredRequestARN);
            }

            if (scope == TwoFactorAuthenticationScope.Request) {
                
                var pin = this.Params.String("two-factor-authentication-pin");
                if(string.IsNullOrEmpty(pin)) {
                    // Send exception that pin is required
                    throw new Localized401Exception(this, "two-factor-authentication-required", this.RequiredRequestARN);
                } else {
                    // Ensure that the pin is correct
                    if (this.User.ValidateAuthenticationPIN(pin) == false) {
                        throw new Localized401Exception(this, "two-factor-authentication-invalid", this.RequiredRequestARN);
                    }
                }
                
            } else if(scope == TwoFactorAuthenticationScope.Session) {
                throw new NotImplementedException();
            } else {
                throw new InvalidEnumArgumentException();
            }
        }

        /// <summary>
        /// Makes sure that the request IP matches one stored in the setting
        /// SecurityRequireAdminIPRestrictions (comma seperated list of allowed ips).
        /// </summary>
        /// <exception cref="BackendException">access-denied</exception>
        public void RequireAdminIP() {
            if (Core.Config.SecurityRequireAdminIPRestrictions != "*") {
                // Make sure IP matches
                var ipsOrSubnets = Core.Config.SecurityRequireAdminIPRestrictions.Split(',').ToList();
                var valid = Core.Util.IP.DoesIPMatcheAnyAddressOrSubnetInList(this.Request.IP, ipsOrSubnets);
                if (!valid) throw new BackendException("access-denied",$"This request has not been authorized for this IP address ({this.Request.IP}) (a admin IP source is required for this action).");
            }
        }

        /// <summary>
        /// Requires the given admin ARN (adminARN) for the currently logged in user exist (ie the user has the admin ARN),
        /// or either a business or user association exists (ie the logged-in user belongs to the given business or
        /// the logged-in user is the given user).
        /// </summary>
        /// <param name="adminARN">The admin ARN.</param>
        /// <param name="businessToMatch">The business.</param>
        /// <param name="userToMatch">The user.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// adminARN cannot be null for RequireAdminARNOrBusinessOrUserAssociation.
        /// or
        /// businessToMatch and userToMatch cannot both be null for RequireAdminARNOrBusinessOrUserAssociation.
        /// </exception>
        /// <exception cref="Backend401Exception">authorization-required
        /// or
        /// authorization-required</exception>
        public bool RequireAdminARNOrBusinessOrUserAssociation(string adminARN, Business businessToMatch, User userToMatch) {
            
            // Validate
            if(adminARN == null) {
                throw new Exception("adminARN cannot be null for RequireAdminARNOrBusinessOrUserAssociation.");
            }
            if(businessToMatch == null && userToMatch == null) {
                throw new Exception("businessToMatch and userToMatch cannot both be null for RequireAdminARNOrBusinessOrUserAssociation.");
            }

            // Make sure we have a logged int user
            this.RequireLogin();
            var exception = new Backend401Exception("authorization-required", $"You do not have sufficient priveleges to access this page (ARN={adminARN}). Please contact your administrator or login for the correct account.", adminARN);

            // A: Check for admin
            var isAdmin = ValidateARN(adminARN,false);
            if (isAdmin) {
                // User is admin - return true already (dont check for business association)
                return true;
            }
            
            // B: Check if we (this.User) belongs to business...
            if (businessToMatch != null) {
                // Make sure businesses are loaded
                if(this.User.Context != null) {
                    this.User.Include(nameof(this.User.Businesses));
                }
                if (this.User.Businesses.Contains(businessToMatch)) {
                    return true;
                } else {
                    throw exception;
                }
            }
            
            // C: Check if we (this.User) are the given user (user)  
            if(userToMatch != null) {
                if (this.User == userToMatch) {
                    return true;
                } else { 
                    throw exception;
                }
            }
            
            // No match found (neither A or B or C)
            throw exception;
        }

        public void RequireLoginExceptAuthSecret(bool requireAdminLogin = false) {
            // Safety only test this if we have auth-secret-> otherwise each product has to define a secret
            if (this.Params.ContainsKey("auth-secret") && string.IsNullOrWhiteSpace(Machinata.Core.Config.AuthSecretForHeadlessAccess)) {
                throw new BackendException("invalid-auth-secret", "The configuration AuthSecretForHeadlessAccess must be set.");
            }


            // Validate through-access via special auth-secret query param...
            if (this.Params.String("auth-secret") == Machinata.Core.Config.AuthSecretForHeadlessAccess) {

                // Allow access if this is reached
            } else {
                if (requireAdminLogin == true) {
                    // Require admin
                    this.RequireAdmin();
                } else {
                    // Require login
                    this.RequireLogin();
                }
            }
        }

        public virtual bool ValidateARN(string requiredARN, bool throwExceptions = true) {
            _logger.Trace($"Validating ARN for '{this.Route.Path}'...");

            // If no arn is set throw an exception
            if (requiredARN == null) {
                if (throwExceptions) throw new Exception($"No ARN was set for the request '{this.Route.Path}'. A ARN is required. To allow public access set the ARN to Core.Model.AccessPolicy.PUBLIC_ARN.");
                else return false;
            }
            _logger.Trace($"  Required Route ARN is '{requiredARN}'...");

            // If the ARN is Core.Model.AccessPolicy.PUBLIC_ARN, then just allow
            if (requiredARN == Core.Model.AccessPolicy.PUBLIC_ARN) {
                _logger.Trace($"  Route ARN is public ({Core.Model.AccessPolicy.PUBLIC_ARN}), allowing access");
                return true;
            }

            // Do some processing on the route ARN
            // Note: if the required ARN has {route}, then the ARN will become the route 1:1
            // Example:
            //      Request: /admin/shop/catalogs >> ARN: /admin/shop/catalogs
            // Note: if the required ARN has {reduced-route}, then the ARN will become the route without a /api/ prefix
            // Example:
            //      Request: /api/admin/shop/catalogs/create-group >> ARN: /admin/shop/catalogs/create-group
            string processedRouteARN = requiredARN.Replace(AccessPolicy.ROUTE_ARN, this.Route.Path);
            processedRouteARN = requiredARN.Replace(AccessPolicy.REDUCED_ROUTE_ARN, this.Route.Path.ReplacePrefix("/api/","/"));
            _logger.Trace($"  Processed Route ARN is '{processedRouteARN}'...");

            // If no user is set, we can't validate the ARN...
            if (this.User == null) {
                _logger.Trace("  No user logged in to validate ARN");
                if (throwExceptions) throw new Localized401Exception(this, "login-required", processedRouteARN);
                else return false;
            }

            // Validate that the user has matching arn
            return this.User.HasMatchingARN(processedRouteARN, throwExceptions);
        }

        /// <summary>
        /// Called with the route processing has completed.
        /// </summary>
        public virtual void Finish() {
            if (_stringOutput != null) {

                // Send String

                // Set proper mime type
                var mimeType = this.DefaultMimeType;
                if(this._stringMimeType != null) mimeType = this._stringMimeType;
                if (mimeType != null) this.Context.Response.ContentType = mimeType;

                // New method: Use util method that also supports compression
                Core.Util.HTTP.WriteStringWithCompressionSupport(_stringOutput, this.Context, this._fileName);

                // Old method: (deprecated)
                //this.Context.Response.Write(_stringOutput);
                //this.Context.Response.Flush();

            } else if (_jsonOutput != null) {

                // Send JSON object

                this.Context.Response.ContentType = "application/json";
                if (this.Params.Bool("debug", false) == true) {
                    this.Context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(_jsonOutput, Newtonsoft.Json.Formatting.Indented));
                } else {
                    this.Context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(_jsonOutput));
                }
                this.Context.Response.Flush();

            } else if (_fileOutput != null) {

                // Send Static File
                // We use this custom method to support streaming of files
                if (File.Exists(_fileOutput) == false) throw new Localized404Exception(this, "file-not-found", Core.Util.Files.GetFilepathWithoutSensativePathInformation(_fileOutput).Replace("\\","/"));
                Core.Util.HTTP.WriteFileWithStreamingSupport(_fileOutput, this.Context, _fileName, _fileCharset);
                
            } else if (_binaryOutput != null) {

                // Send binary output

                HTTP.SendBinaryData(this.Context, _binaryOutput, _binaryFilename);

            } else {

                // Unknown, must be implemented by handler

            }

            // Set caching
            this.WriteCacheHeaders();

            // Cleanup db handle if we created one
            if (_db != null) {
                _db.Dispose();
            }
        }
        #endregion


        #region Helper Methods

        public void WriteCacheHeaders() {
            if (this._cacheTime != TimeSpan.Zero) {
                // Set expires
                this.Context.Response.Cache.SetExpires(DateTime.UtcNow.Add(this._cacheTime)); 
                this.Context.Response.Cache.SetMaxAge(this._cacheTime);
                // Set caching policy
                //Note: For streaming responses (accept-ranges) this used to have to be private.
                this.Context.Response.Cache.SetCacheability(this._cachePolicy);
            }
        }

        /// <summary>
        /// Applies the current route paremeters to given route.
        /// The given route must match the same amount of paremters.
        /// This is usefull for example for autoamtically getting a translated
        /// URL using route aliases.
        /// </summary>
        /// <param name="route">The route.</param>
        /// <returns></returns>
        public string ApplyCurrentRouteParemetersToRoute(string route) {
            if (route == null) return null;

            Regex regex = new Regex(ROUTE_PARAMETER_REGEX);
            var matches = regex.Matches(route);
            for(var i = 0; i < matches.Count; i++) {
                var match = matches[i];
                if (this._args.Count < i) throw new Exception($"Unable to find matching route arguments for alias route '{route}'! Are you sure the route alias has the same arguments/parameters?");
                route = route.Replace(match.Value,this._args[i].ToString());
            }
            return route;
        }

        public void Log(string message, NLog.LogLevel level) {
            _logger.Log(level, message);
        }


        public void Log(string message) {
            _logger.Log(NLog.LogLevel.Info, message);
        }

        public void LogError(string message, Exception exception) {
            _logger.Error(exception, message);
        }

        public void SetCacheTime(TimeSpan time) {
            _cacheTime = time;
        }

        public void SetCacheTime(int minutes) {
            SetCacheTime(new TimeSpan(0, minutes, 0));
        }

        public void SetCachePolicy(HttpCacheability policy) {
            _cachePolicy = policy;
        }

        /// <summary>
        /// Sends the file on the filepath with no caching.
        /// </summary>
        /// <param name="filepath">The filepath.</param>
        public void SendFile(string filepath) {
            SendFile(filepath, TimeSpan.Zero, null);
        }

        /// <summary>
        /// Sends the file on the filepath with no caching.
        /// </summary>
        /// <param name="filepath">The filepath.</param>
        /// <param name="fileName">The name.</param>
        public void SendFile(string filepath,string fileName) {
            SendFile(filepath, TimeSpan.Zero, fileName);
        }

        /// <summary>
        /// Sends the file on the filepath with the given cachetime headers.
        /// </summary>
        /// <param name="filepath">The filepath.</param>
        /// <param name="cacheTime">The cache time.</param>
        public void SendFile(string filepath, TimeSpan cacheTime, string filename = null, string charset = null) {
            _fileName = filename;
            _fileOutput = filepath;
            _fileCharset = charset;
            _cacheTime = cacheTime;
        }

        /// <summary>
        /// Sends the object as JSON.
        /// </summary>
        /// <param name="json">The json.</param>
        public void SendJSON(object json) {
            _jsonOutput = json;
        }

        /// <summary>
        /// Sends the text as plain text...
        /// </summary>
        public void SendText(string text, string fileName = null, string mimeType = null){
            if(mimeType != null) _stringMimeType = mimeType;
            if(fileName != null) _fileName = fileName;
            _stringOutput = new StringBuilder(text);
        }

        /// <summary>
        /// Sends the object as JSON as a file download.
        /// </summary>
        /// <param name="json">The json.</param>
        /// <param name="filename">The filename.</param>
        public void SendJSONAsFile(object json, string filename) {
            var jsonstr = Newtonsoft.Json.JsonConvert.SerializeObject(json);
            SendBinary(Encoding.UTF8.GetBytes(jsonstr),filename);
        }

        /// <summary>
        /// Sends the given bytes as a file download.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <param name="filename">The filename.</param>
        public void SendBinary(byte[] bytes, string filename) {
            _binaryOutput = bytes;
            _binaryFilename = filename;
        }


        /// <summary>
        /// Marks the current unser as active (if any). Will automatically save to DB...
        /// Note: this only updates every hour (or minTimeNeededForUpdate in User.MarkAsActive).
        /// </summary>
        public virtual void MarkUserAsActive() {
            if(this.User != null) {
                var lastActive = this.User.LastActive;
                this.User.MarkAsActive();
                if (lastActive != this.User.LastActive) {
                    this.DB.SaveChanges();
                }
            }
        }

        #endregion

    }
}
