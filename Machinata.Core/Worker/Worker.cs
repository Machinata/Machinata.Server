﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// The Worker namespace provides easy and lightweight async worker tasks that run automatically in a worker pool.
/// 
/// By default, a local worker pool is automatically created and started at startup:
/// ```
/// Core.Worker.LocalPool
/// ```
/// To create a new async task(work packet), just implement the class ```Core.Worker.WorkerTask```. For example:
/// ```
/// private class MyWorkerTask : Core.Worker.WorkerTask {
/// private Dictionary<string, string> _data;
///   public MyWorkerTask(Dictionary<string, string> data) {
///       _data = data;
///   }
///   public override void Process() {
///       // Do something with data...
///   }
/// }
/// // Execute the task on the pool
/// Core.Worker.LocalPool.QueueTask(new MyWorkerTask(data));
/// ```
///Note: You should make sure to avoid including DB referenced members/parameters in a task.Rather than attaching entity objects directly, pass the id's instead and then let the task re-populat the entity object.
/// </summary>
namespace Machinata.Core.Worker {
   
}
