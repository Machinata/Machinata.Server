using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Worker {

    /// <summary>
    /// A single work task which can be processed by the Worker Pool Thread.
    /// This abstract class should be implmented to accomplish the work task.
    /// A worker pool can process different types of tasks.
    /// </summary>
    public abstract class WorkerTask {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        public WorkerThread AssignedWorkerThread = null;

        private int _attempts = 0;
        
        public int Attempts {
            get {
                return _attempts;
            }
        }

        public virtual int MaxAttempts {
            get {
                return 1;
            }
        }

        public virtual int ExceptionCooldownTimeMS {
            get {
                return 100;
            }
        }

        /// <summary>
        /// Processes the work task. Implmented this method to accomplish the work task.
        /// </summary>
        public abstract void Process();

        public virtual void Failed(Exception e) { }

        public void RegisterAttempt() {
            _attempts++;
        }
    }
}
