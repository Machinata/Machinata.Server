using Machinata.Core.Documentation.Model;

using System.Collections.Generic;
using System.Linq;

/// <summary>
/// /
/// </summary>
namespace Machinata.Core.Documentation.Parser {


    public abstract class Parser {

        public const string TYPE_FUNCTION = "function";
        public const string TYPE_NAMESPACE = "namespace";
        public const string TYPE_CLASS = "class";
        public const string TYPE_INTERFACE = "interface";
        public const string TYPE_VARIABLE = "variable";
        public const string TYPE_PROPERTY = "property";
        public const string TYPE_ENUM = "enum";
        public const string TYPE_CONSTRUCTOR = "constructor";
        public const string TYPE_RULE = "rule";
        public const string TYPE_REGION = "region";
        public const string TYPE_UNKNOWN = "unknown";

        /// <summary>
        /// Parses the given content
        /// </summary>
        /// <param name="language">The language such as js, cs, css which defines which parser is gonna be used</param>
        /// <param name="content">The content that should be parsed.</param>
        /// <param name="file">The file where the content is  originally from</param>
        /// <returns></returns>
        public static IEnumerable<DocumentationItem> Parse(string language, string content,  string file, bool includeSourceCode, bool privateMembersAreHidden) {
            
            // Find Parser
            var parser = CreateParser(language);

            var results = parser.ParseImpl(content, includeSourceCode, privateMembersAreHidden);
            
            foreach (var item in results) {
                if (string.IsNullOrEmpty(item.File) == true) {
                    item.File = file;
                }
            }

            return results;
        }

        private static Parser CreateParser(string language) {
            var parsers = Core.Reflection.Types.GetMachinataTypes(typeof(Parser)).Where(p=>p.IsAbstract == false).Select(p => Core.Reflection.Types.CreateInstance<Parser>(p));

            // TODO Multiple
            var parser = parsers.FirstOrDefault(p => p.Language == language);
                       

            return parser;
        }

        /// <summary>
        /// Programming language of the parser like cs, js, css
        /// </summary>
        public abstract string Language { get; }

        


        /// <summary>
        /// Generic signature for concrete implementations of a parser
        /// </summary>
        internal abstract IEnumerable<DocumentationItem> ParseImpl(string content, bool includeSourceCode, bool privateMembersAreHidden);


        /// <summary>
        /// Determines whether the specified type is a class like structure e.g. class, interface, struct etc which can contain methods, vars, props etc.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static bool IsStructure(string type) {
            if (type == Parser.TYPE_CLASS) {
                return true;
            }
            if (type == Parser.TYPE_INTERFACE) {
                return true;
            }
            if (type == Parser.TYPE_ENUM) {
                return true;
            }

            return false;
        }
    }
}
