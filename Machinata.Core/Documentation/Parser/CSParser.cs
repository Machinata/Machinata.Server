using Machinata.Core.Documentation.Model;
using Machinata.Core.Model;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Machinata.Core.Documentation.Parser {
    public class CSParser : GhostDocParser {
        public override string Language {
            get {
                return "cs";
            }
        }

        public readonly string[] AccessibilityLevels = new string[] { "private" , "internal", "public", "protected" }; // leave order like this!!!  // acually there is also "protected internal" and "private protected"

        public string CurrentStructure { get; private set; }
        public string CurrentStructureType { get; private set; }
        public object CurrentStructureAccessibility { get; private set; }
        public string CurrentNameSpace { get; private set; }
        public List<DocumentationItemAttribute> Attributes { get; private set; } = new List<DocumentationItemAttribute>();

        public override string GetType(string line) {
            if (string.IsNullOrWhiteSpace(line)) {
                return null;
            }


            // Constructor
            if (string.IsNullOrEmpty(this.CurrentStructure) == false && Regex.IsMatch(line, @"\s" + this.CurrentStructure + @"(\s)*\(")) {
                if (IsConstructorDefinition(line) == true) {
                    return Parser.TYPE_CONSTRUCTOR;
                }
            }

            // Function: signature in one line
            if (line.Contains("(") && line.Contains(")") && line.Contains(";") == false) {
                if (IsFunctionDefinition(line)) {
                    return Parser.TYPE_FUNCTION;
                }

            }
            // Function (abstract)
            else if (line.Contains("(") && line.Contains(")") && line.Contains(";") == true && (line.Contains(" abstract ") == true || this.CurrentStructureType == Parser.TYPE_INTERFACE)) {
                return Parser.TYPE_FUNCTION;
            }
            // Full function definnition in one line
            if (line.Contains("(") && line.Contains(")") && line.Contains("{") == true && line.Contains("}") == true) {
                if (IsFunctionDefinition(line)) {
                    return Parser.TYPE_FUNCTION;
                }
            }
            // Function: signature multiple lines
            if (ExtractMultilineFunctionSignature(line, this.LineNumber, this.Lines) != null) {
                return Parser.TYPE_FUNCTION;
            }

            // Class
            var className = ExtractClassFromLine(line);
            if (string.IsNullOrEmpty(className) == false) {
                return Parser.TYPE_CLASS;
            }

            // Interface
            var @interface = ExtractStructureFromLine(line, "interface");
            if (string.IsNullOrEmpty(@interface) == false) {
                return Parser.TYPE_INTERFACE;
            }

            // Namespace
            var @region = ExtractStructureFromLine(line, "#region");
            if (string.IsNullOrEmpty(@region) == false) {
                return Parser.TYPE_REGION;
            }


            // Variable and not a initialized auto property
            if (line.Contains("get;") == false && line.Contains("set;") == false &&

                (line.Contains('=')
                || (line.Contains("(") == false && line.Trim().EndsWith(";", StringComparison.InvariantCulture))

                )) {
                return Parser.TYPE_VARIABLE;
            }

            // enum multiple lines
            if (ParserHelper.IndexOfKeyword(line, "enum") >= 0) {
                return Parser.TYPE_ENUM;
            }

            // Namespace
            if (line != null && line.Trim().StartsWith("namespace ")) {
                return Parser.TYPE_NAMESPACE;
            }

            // Propertiy one liner
            if ((line.Contains("{") && line.Contains("}")) && (line.Contains("get ") || line.Contains("set") || line.Contains("get;"))) {
                return Parser.TYPE_PROPERTY;
            }

            // Property multiple lines
            if (line.Contains(";") == false) {
                return Parser.TYPE_PROPERTY;
            }

            // Property multiple lines
            // "public string LinkDisplay {"
            if (Regex.IsMatch(line, @"public (\w+) (\w+) \{")) {
                return Parser.TYPE_PROPERTY;
            }




            return Parser.TYPE_UNKNOWN;
        }

        private bool IsFunctionDefinition(string line) {
            //  make sure this is not a function:   if (Core.Config.AnalyticsUseWorkerPool) {
            // it's a function definition if it has at leas two words before the "("
            var preBracket = line.GetUntilOrEmpty("(");
            var preBracketSplits = preBracket.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var hasMinKeywords = preBracketSplits.Length >= 2;
            var notFunctionKeywords = new List<string>() { "catch", "if", "else", "=", "throw", "new" }; // keywords, which indicate that this is not a function:  before "("

            if (ContainsComment(preBracket)) {
                return false;
            }

            if (hasMinKeywords) {
                if (notFunctionKeywords.Any(k => preBracketSplits.Contains(k)) == false) { // not: return, new, void
                    return true;
                }
            }

            return false;
        }
        private bool IsConstructorDefinition(string line) {
            //  make sure this is not a function:   if (Core.Config.AnalyticsUseWorkerPool) {
            // it's a function definition if it has at leas two words before the "("
            var preBracket = line.GetUntilOrEmpty("(");
            var preBracketSplits = preBracket.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var hasMinKeywords = preBracketSplits.Length >= 1;
            var notConstructorKeywords = new List<string>() { "return", "new", "void" };

            if (ContainsComment(preBracket)) {
                return false;
            }

            if (hasMinKeywords && this.CurrentStructure == preBracketSplits.LastOrDefault()) {
                if (notConstructorKeywords.Any(k => preBracketSplits.Contains(k)) == false) { // not: return, new, void
                    return true;
                }
            }



            return false;

        }


        private bool ContainsComment(string content) {
            // https://stackoverflow.com/questions/11468839/extract-comments-from-cs-file
            var commentRegex = @"(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|(//.*)";
            var regex = new Regex(commentRegex);
            return regex.IsMatch(content);
        }

        private bool IsOnlyComment(string line) {
            // https://stackoverflow.com/questions/11468839/extract-comments-from-cs-file
            var commentRegex = @"(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|(//.*)";
            var regex = new Regex(commentRegex);
            var trimmedLine = line.Trim();

            var match = regex.Match(trimmedLine);
            if (match.ToString() == trimmedLine) {
                return true;
            }
            return true;
        }


        /// <summary>
        /// Looks forward in the next lines whether this is a function signature in multiple lines
        /// </summary>
        private string ExtractMultilineFunctionSignature(string startLine, int lineNumber, string[] lines) {
            // Not a multiline function signature
            if (startLine.Contains("(") == false || startLine.Contains(")") == true) {
                return null;
            }
            if (lineNumber >= lines.Length) {
                return null;
            }
            var signature = new StringBuilder(startLine);
            var currentLine = string.Empty;
            do {
                currentLine = lines.ElementAt(lineNumber);
                currentLine = currentLine.Trim();
                signature.AppendLine(currentLine);

                lineNumber++;

            } while (currentLine.EndsWith(","));

            // We have a multiline signature
            if (currentLine.EndsWith(")") || currentLine.EndsWith("{")) {
                return signature.ToString().Trim().TrimEnd('{').TrimEnd('{');
            } else {
                return null;
            }


        }

        protected override string GetFullName(DocumentationItem item, IEnumerable<DocumentationItem> previousItems, string line, string type) {
            var name = GetName(line, type);

            var ns = this.CurrentNameSpace;
            if (type == Parser.TYPE_NAMESPACE) {
                if (ns != null && ns.Contains(".")) {
                    return ns; // e.g. Machinata.Core
                }
                return ns; // e.g. Machinata
            }
            if (IsStructure(type) == false || CurrentStructure.Contains(".") == true) {
                var className = this.CurrentStructure;
                if (className != null && IsStructure(type) == true) {
                    // NESTED CLASS
                    // TODO: MORE LEVELS OF NESTING
                    className = className.Split('.').First();
                }
                return $"{ns}.{className}.{name}";
            } else {
                return $"{ns}.{name}";
            }

        }

        private string GetName(string line, string type) {
            if (type == Parser.TYPE_CLASS) {
                return ExtractClassFromLine(line);
            } else if (type == Parser.TYPE_INTERFACE) { // TODO ONE FOR STRUCT,CLASS,INTERFACE
                return ExtractStructureFromLine(line, "interface");
            } else if (type == Parser.TYPE_FUNCTION || type == Parser.TYPE_CONSTRUCTOR) {
                var indexOfBracket = line.IndexOf("(");
                var name = line.Substring(0, indexOfBracket);
                var lastSpaceBefore = name.LastIndexOf(' ');
                return name.Substring(lastSpaceBefore + 1, name.Length - lastSpaceBefore - 1);
            } else if (type == Parser.TYPE_VARIABLE) {
                var indexOfE = line.IndexOf("=");
                // with an equal
                if (indexOfE >= 0) {
                    var name = line.Substring(0, indexOfE);
                    name = name.Trim();
                    var lastSpaceBefore = name.LastIndexOf(' ');
                    name = name.Substring(lastSpaceBefore + 1, name.Length - lastSpaceBefore - 1);
                    return name;
                }
                // without equal: e.g. public string FormNamespace;
                {
                    var lastSpaceBefore = line.LastIndexOf(' ');
                    var name = line.Substring(lastSpaceBefore + 1, line.Length - lastSpaceBefore - 1);
                    return name.Trim(';');
                }

            } else if (type == Parser.TYPE_PROPERTY) {
                var indexOfE = line.IndexOf("{");
                string name = line;

                // Inline Property
                if (indexOfE > 0) {
                    name = line.Substring(0, indexOfE);
                } else {
                    // Property multiple lines
                    name = line;
                }
                name = name.Trim();

                // Indexer (currently handled as property)
                //https://stackoverflow.com/questions/20313308/c-sharp-beginner-delete-all-between-two-characters-in-a-string-regex
                name = Regex.Replace(name, @"\[.*?\]", string.Empty);

                var lastSpaceBefore = name.LastIndexOf(' ');
                return name.Substring(lastSpaceBefore + 1, name.Length - lastSpaceBefore - 1);
            } else if (type == Parser.TYPE_ENUM) {
                const string searchString = "enum";
                var index = ParserHelper.IndexOfKeyword(line, searchString);
                string name = line;
                if (index > 0) {
                    name = line.Substring(index + searchString.Length + 1, name.Length - index - searchString.Length - 1);
                }
                var indexDouble = name.IndexOf(":");
                if (indexDouble > 0) {
                    name = name.Substring(0, indexDouble);
                }
                name = name.Trim().TrimEnd('{').Trim();
                return name;
            } else if (type == Parser.TYPE_NAMESPACE) {
                const string searchString = "namespace";
                var index = ParserHelper.IndexOfKeyword(line, searchString);
                string name = line;
                if (index >= 0) {
                    name = line.Substring(index + searchString.Length + 1, name.Length - index - searchString.Length - 1);
                }
                name = name.Trim().TrimEnd('{').Trim();
                return name;
            } 
            return "unknown";
        }
        
        public override string GetSignature(string line, string type) {
            if (type == Parser.TYPE_PROPERTY) {
                return line.Trim();
            }
            if (type == Parser.TYPE_FUNCTION || type == Parser.TYPE_CONSTRUCTOR) {

                // mulitline signature
                var multiLineSignature = ExtractMultilineFunctionSignature(line, this.LineNumber, this.Lines);
                if (multiLineSignature != null) {
                    return multiLineSignature;
                }

                // one line signature
                var indexBraket = line.IndexOf("{");
                if (indexBraket > 0) {
                    return line.Substring(0, indexBraket).Trim();
                }
            }
            if (line != null) {
                line = line.Replace("{", string.Empty);
                line = line.Trim();
            }
            return line;
        }


        private string ExtractNamespaceFromLine(string line) {
            //var regex = new Regex(@"\s*namespace\s");
            var regex = new Regex(@"^namespace\s"); // only beginning of string

            var regex2 = new Regex("\"\\snamespace\\s\""); // "namepace"

            if (string.IsNullOrWhiteSpace(line) == false) {
                if (regex.IsMatch(line.Trim()) && regex2.IsMatch(line) == false) {
                    var ns = line;
                    ns = ns.Replace("namespace", string.Empty);
                    ns = ns.Replace("{", string.Empty);
                    ns = ns.Trim();
                    return ns;
                }
            }
            return null;
        }

        private string ExtractClassFromLine(string line) {
            var regex = new Regex(@"\sclass\s");
            if (string.IsNullOrWhiteSpace(line) == false) {
                line = RemoveEnclosedString(line);
                var match = regex.Match(line);
                if (match.Index > 0) {
                    var indexAfterClass = match.Index + "class ".Length;
                    var c = line.Substring(indexAfterClass, line.Length - indexAfterClass);
                    c = c.Replace("class", string.Empty);
                    c = c.Replace("{", string.Empty);
                    var indexOfColon = c.IndexOf(':');
                    if (indexOfColon > 0) {
                        c = c.Substring(0, indexOfColon - 1);
                    }
                    c = c.Trim();
                    return c;
                }
            }
            return null;
        }

        /// <summary>
        /// Removes all occurences of an opening and closing substring
        /// e.g. string.Concat("we want this string removed" + "and this")
        /// results in string.Concat( +)
        /// </summary>
        /// <param name="line">The line.</param>
        /// <returns></returns>
        private string RemoveEnclosedString(string line) {
            if (string.IsNullOrWhiteSpace(line) == true) {
                return line;
            }
            var start = line.IndexOf("\"");

            if (start < 0) {
                return line;
            }

            var end = line.IndexOf("\"", start + 1);

            if (start > 0 && end > 0) {
                var stringToDelete = line.Substring(start, end - start + 1);
                line = line.Replace(stringToDelete, string.Empty);
                return RemoveEnclosedString(line);
            } else {
                return line;
            }

        }


        /// <summary>
        /// Extracts interface, class, struct from a given line of code
        /// </summary>
        /// <param name="line">The line.</param>
        /// <param name="keyword">The keyword (class, interface, struct).</param>
        /// <returns></returns>
        private string ExtractStructureFromLine(string line, string keyword) {
            var regex = new Regex(@"\s" + keyword + @"\s");
            if (string.IsNullOrWhiteSpace(line) == false) {
                line = RemoveEnclosedString(line);
                var match = regex.Match(line);
                if (match.Index > 0) {
                    var indexAfterClass = match.Index + (keyword + " ").Length;
                    var c = line.Substring(indexAfterClass, line.Length - indexAfterClass);
                    c = c.Replace(keyword, string.Empty);
                    c = c.Replace("{", string.Empty);
                    var indexOfColon = c.IndexOf(':');
                    if (indexOfColon > 0) {
                        c = c.Substring(0, indexOfColon - 1);
                    }
                    c = c.Trim();
                    return c;
                }
            }
            return null;
        }


        private DocumentationItemAttribute ExtractAttributeFromLine(string line, string keyword) {
            // Remove all whitespaces
            var lineCleaned = Regex.Replace(line, @"\s+", "");
            if (lineCleaned.StartsWith("[") && line.Contains("]")) {
              //  var lineCleaned = Regex.Replace(line, @"\s+", "");

                lineCleaned = lineCleaned.Replace("[", "");

                var parameters = new List<KeyValuePair<string, string>>();
                if (lineCleaned.Contains("(")) {
                    var paramsString = lineCleaned.ToString();
                    lineCleaned = lineCleaned.GetUntilOrEmpty("(");


                    // (......)]
                    // parameters -> only named supported
                    {
                        paramsString = paramsString.ReplacePrefix(lineCleaned, string.Empty).Replace("]",string.Empty);
                        paramsString = paramsString.ReplacePrefix("(", string.Empty);
                        paramsString = paramsString.ReplaceSuffix(")", string.Empty);
                        var splits = paramsString.Split(",");
                        foreach (var split in splits) {
                            var keyValue = split.Split("=").ToList();
                            parameters.Add( new KeyValuePair<string, string>( keyValue.First(), keyValue.SecondOrDefault()?.Trim('"')));
                        }

                    }
                }
                lineCleaned = lineCleaned.Replace("]", "");
                var parameterItems = parameters.Select(p => new DocumentationItemAttributeParameter() { Name = p.Key?.Trim(), Value = p.Value?.Trim() });
                return new DocumentationItemAttribute(lineCleaned) { Parameters = parameterItems.ToList()};
            }
            return null;
            
        }

        protected override bool IgnoreLine(string line) {

            // Is it an Attribute?
            if (string.IsNullOrEmpty(line) == false && line.Trim().StartsWith("[", StringComparison.InvariantCulture)) {
                return true;
            }
            if (line?.TrimStart().StartsWith("//")== true) {
                // ignore if its commented out line
                return true;
            }
            return false;
        }

        public override string CleanupCode(string code, DocumentationItem item) {

            var lines = code.Replace("\r", "").Split('\n');

            int lineNumber = 0;
            var cleanedUp = new StringBuilder();
            foreach (var line in lines) {

                if (lineNumber > 1) { // start checking after self
                    var type = GetType(line);
                    var trimmedLine = line.Trim();
                    if (item.Type == TYPE_CLASS) {
                        if (type == TYPE_CLASS || type == TYPE_REGION) {
                            break; // if the current item is a class only stop if we detect a new class or region
                        }
                    }
                    if (item.Type == TYPE_FUNCTION || item.Type == TYPE_CONSTRUCTOR) {
                        if (type == TYPE_FUNCTION || type == TYPE_CONSTRUCTOR) {
                            break; // if the current item is a function or cstr only stop if we detect a new  function/ctr
                        }
                    }
                    if (item.Type == TYPE_VARIABLE) {
                        break; // if the current item is a variable we expect it to be a one liner
                    }
                }

                cleanedUp.AppendLine(line);

                lineNumber++;
            }



            return base.CleanupCode(cleanedUp.ToString(), item);

        }

        public override void PreProcess(DocumentationItem lastItem, string line) {
            base.PreProcess(lastItem, line);

            // Ignore comment lines
            if (line.Trim().StartsWith(@"//", StringComparison.InvariantCulture) == true) {
                return;
            }

            var className = ExtractClassFromLine(line);
            if (string.IsNullOrEmpty(className) == false) {
                this.CurrentStructure = className;
                this.CurrentStructureType = Parser.TYPE_CLASS;
                this.CurrentStructureAccessibility = null;
            }

            var @interface = ExtractStructureFromLine(line, "interface");
            if (string.IsNullOrEmpty(@interface) == false) {
                this.CurrentStructure = @interface;
                this.CurrentStructureType = Parser.TYPE_INTERFACE;
            }

            var @enum = ExtractStructureFromLine(line, "enum");
            if (string.IsNullOrEmpty(@enum) == false) {

                // enum nested in class
                if (string.IsNullOrEmpty(this.CurrentStructure) == false) {
                    this.CurrentStructure += "." + @enum;
                } else {
                    // enum outside class
                    this.CurrentStructure = @enum;
                }
                this.CurrentStructureType = Parser.TYPE_ENUM;
            }

            // enum: end of
            if (line.Trim() == "}" && this.CurrentStructureType == Parser.TYPE_ENUM) {

                var nsParts = this.CurrentStructure?.Split('.');

                if (nsParts == null || nsParts.Length == 1) {
                    this.CurrentStructure = null;
                } else {
                    // we find ourselves in the parent class
                    this.CurrentStructure = nsParts.First();
                    this.CurrentStructureType = Parser.TYPE_CLASS;
                }
            }

            var namespaceName = ExtractNamespaceFromLine(line);
            if (string.IsNullOrEmpty(namespaceName) == false) {
                this.CurrentNameSpace = namespaceName;
            }

            // Attributes?
            var attribute = ExtractAttributeFromLine(line, null);
            if (attribute != null) {
                this.Attributes.Add(attribute);
            }

            // Enum members?
            if (this.CurrentStructureType == Parser.TYPE_ENUM && lastItem != null && lastItem.Type == Parser.TYPE_ENUM) {
                if (line.Contains("{") == false) {
                    var enumMember = line;
                    enumMember = enumMember.Replace(",", string.Empty);
                    enumMember = enumMember.Replace("}", string.Empty);
                    enumMember = enumMember.Trim();
                    if (string.IsNullOrWhiteSpace(enumMember) == false) {
                        lastItem.EnumMembers.Add(new DocumentationItemEnumMembers(enumMember));
                    }
                }
            }



        }


        public override string GetAccessibility(string line, DocumentationItem item) {

            var preName = line.GetUntilOrEmpty(item.Name);
            var preBracketSplits = preName.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            foreach(var accessiblity in AccessibilityLevels) {
                if (preBracketSplits.Contains(accessiblity)) {
                    return accessiblity;
                }
            }
            return null;  // todo CurrentStructureAccessibility as fallback
        }


        public override void PostProcess(DocumentationItem item) {
            if (item.Type == Parser.TYPE_FUNCTION && item.Signature != null) {
                item.SymanticName = item.Name + GetParametersString(item.Signature, false);
            } else if (item.Type == Parser.TYPE_CONSTRUCTOR && item.Signature != null) {
                item.SymanticName = item.Name + GetParametersString(item.Signature, false);
            }
            else {
                item.SymanticName = item.Name;
            }


            // Clean attributes
            this.Attributes.Clear();





            // Data type e.g. string, int, etc
            if(item.Signature.StartsWith("//")) {
                // comment - exclude
            } else if (item.Type == Parser.TYPE_PROPERTY && item.Signature != null  ) {
                try {
                var searchPattern = " " + item.Name; // public int Test : " Test"
                var accessorAndType = item.Signature.GetUntilLastOccurencyOrEmpty(searchPattern);
                var dataType = accessorAndType.Split(" ").LastOrDefault(); // this was Last() but changed because enum members enter here
                item.DataType = dataType;
                } catch(Exception e) {
                    throw new Exception($"Could not parse property: {item.Signature}, {this.CurrentStructure}:{item.LineNumber}", e);
                }
            } else {
                item.DataType = item.Type;
            }

            if (item.Type == Parser.TYPE_FUNCTION && item.Signature != null) {
                item.SymanticName = item.Signature.Replace(item.Namespace + ".", "").Replace(" = function ", "").Replace(";", "").Replace("{}", "");
            } else {
                item.SymanticName = item.Name;
            }

            if (item.Name.StartsWith("_")) item.Hidden = true;


        }
       
        /// <summary>
        /// Extracts the type, name and default value from a function, e.g. function( int param1, string param2, double param3 = 3.3)
        /// </summary>
        /// <param name="line"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public override Dictionary<string, DocumentationItemParameter> GetParameters(string line, DocumentationItem item) {

            var parameters = new Dictionary<string, DocumentationItemParameter>();
            if (item.Type != TYPE_FUNCTION && item.Type != TYPE_CONSTRUCTOR) { return parameters; }

            string parametersString = GetParametersString(item.Signature);
           
            var parameterStringSplits = parametersString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var parameterStringSplit in parameterStringSplits) {

                // Splits into before the '=' and after
                var defaultValueSplits = parameterStringSplit.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                string defaultValue = null;

                // we dont have a default value
                if (defaultValueSplits.Count() > 1) {
                    defaultValue = defaultValueSplits.Last().Trim();
                }

                var typeAndName = defaultValueSplits.FirstOrDefault();
                var type = typeAndName.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)?.FirstOrDefault()?.Trim();
                var name = typeAndName.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)?.LastOrDefault()?.Trim();
                parameters[name] = new DocumentationItemParameter(null, type, defaultValue);

            }

            return parameters;

        }

        public override List<DocumentationItemAttribute> GetAttributes(string line, DocumentationItem item) {
            //  var parameters = new Dictionary<string, DocumentationItemAttribute>();

            return this.Attributes.ToList();

        }
        /// <summary>
        /// Retrieves the string between the brackets
        /// </summary>
        /// <param name="signature"></param>
        /// <returns></returns>
        private static string GetParametersString(string signature, bool removeBrackets = true) {
            var start = signature.IndexOf("(");
            var end = signature.IndexOf(")");

            var parametersString = signature.Substring(start, end - start + 1);
            if (removeBrackets) {
                parametersString = parametersString.Trim(new char[] { '(', ')' });
            }
            return parametersString;
        }
    }
}
