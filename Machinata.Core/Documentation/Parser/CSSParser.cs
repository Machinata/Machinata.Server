using Machinata.Core.Model;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Machinata.Core.Util;
using Machinata.Core.Documentation.Model;

namespace Machinata.Core.Documentation.Parser {
    public class CSSParser : GhostDocParser {
        public override string Language
        {
            get
            {
                return "css";
            }
        }

        public string CurrentNamespace { get; private set; }

        public override string GetType(string line) {
          
            return Parser.TYPE_RULE;
        }

        protected override string GetFullName(DocumentationItem item, IEnumerable<DocumentationItem> previousItems, string line, string type) {

            var name = GetName(line, type);
    
            if (this.CurrentNamespace != null) {
                return $"{this.CurrentNamespace}.{name}";
            }

            return name;
        }

        private string GetName(string line, string type) {
            return GetSignature(line, type);
        }

        public override string GetSignature(string line, string type) {
            line = line.Trim().TrimEnd('{').Trim();
            return line;
        }

        public override string GetNamespace(DocumentationItem item) {
            return this.CurrentNamespace;
        }

        protected override string GetNameFromFullName(string fullName) {
            if (this.CurrentNamespace != null) {
                var name = fullName.Replace(this.CurrentNamespace + ".", string.Empty);
                return name;
            }
            return fullName;
        }

        protected override bool IgnoreLine(string line) {
            var trimmed = line.Trim();
            if (trimmed.StartsWith("/*", StringComparison.InvariantCulture) || trimmed.StartsWith("*/", StringComparison.InvariantCulture)) {
                return true;
            }
            return false;
        }

        public override void PreProcess(DocumentationItem lastItem, string line) {
            base.PreProcess(lastItem, line);


            // Find out if this is a second line of a css selector rule

            // No last item to add to
            if (lastItem == null) {
                return;
            }

            // CSS comment line
            if (this.IgnoreLine(line) == true) {
                return;
            }

            var trimmed = line.Trim();

            
            if (trimmed.StartsWith(COMMENT_PREFIX, StringComparison.InvariantCulture)) {
                return;
            }

            if ((trimmed.EndsWith(",", StringComparison.InvariantCulture) || trimmed.EndsWith("{", StringComparison.InvariantCulture)) && lastItem.Name.EndsWith(",", StringComparison.InvariantCulture)) {
                lastItem.Name += "\n" + trimmed.Trim().TrimEnd('{').Trim();
                lastItem.FullName += "\n" + trimmed.Trim().TrimEnd('{').Trim();
                lastItem.Signature = lastItem.Name;
            }

        }

        public override void PostProcess(DocumentationItem item) {
            
            if (item.Type == Parser.TYPE_NAMESPACE) {
                this.CurrentNamespace = item.FullName;
            }
        }

        public override string CleanupCode(string code, DocumentationItem item) {
           
            if (string.IsNullOrWhiteSpace (code)== true) {
                return code;
            }

            code = code.Trim().TrimStart("*/").TrimEnd("/*").Trim();

            return code;
        }
    }
}
