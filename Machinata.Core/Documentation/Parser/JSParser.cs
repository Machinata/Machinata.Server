using Machinata.Core.Model;
using Machinata.Core.Documentation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Machinata.Core.Documentation.Parser {
    public class JSParser : GhostDocParser {
        public override string Language
        {
            get
            {
                return "js";
            }
        }

        // TODO: make more intelligent
        public override string GetType(string line) {
            if (line.Contains("function")) {
                return Parser.TYPE_FUNCTION;
            } else if (line.Contains("=")) {
                return Parser.TYPE_VARIABLE;
            }
            return "unknown";
        }


        public override Dictionary<string, DocumentationItemParameter> GetParameters(string line, DocumentationItem item) {
            var parameters = new Dictionary<string, DocumentationItemParameter>();

            var type = item.Type;

            // Functions as params  not supported yet
            if (line.Count(f => (f == '(')) > 1) {
                return parameters;
            }

            if (type == Parser.TYPE_FUNCTION) {
                var start = line.IndexOf("(");
                var end = line.IndexOf(")");

                // we have ( and )
                if (start > 0 && end > start) {
                    var paramsString = line.Substring(start + 1, end - start - 1);
                    var splits = paramsString.Split(',');
                    foreach (var split in splits) {
                        var param = split.Trim();
                        // Add only if a string
                        if (string.IsNullOrEmpty(param) == false) {
                            parameters[param] = new DocumentationItemParameter(null); // the information is just the name/key
                        }
                    }
                }
            }
            return parameters;
        }


        public override string GetSignature(string line, string type) {
            var signature = line?.Trim();
            if (signature.EndsWith("{")) signature = signature + "}";
            return signature;
        }

        protected override string GetFullName(DocumentationItem item, IEnumerable<DocumentationItem> previousItems, string line, string type) {
            var signature = GetSignature(line, item.Type);
            var symbolIndex = signature.IndexOf("=", StringComparison.InvariantCulture);
            if (symbolIndex > 0) {
                return signature.Substring(0, symbolIndex).Trim();
            }
            return signature;
        }

        protected override bool IgnoreLine(string line) {
            return false;
            // No reason to ignore some lines yet
        }


        public override void PostProcess(DocumentationItem item) {
            

            if (item.Type == Parser.TYPE_VARIABLE && item.Signature != null) {
                if (Regex.IsMatch(item.Signature, " ?= ?(true|false);?")) item.DataType = "boolean " + item.Type;
                if (Regex.IsMatch(item.Signature, " ?= ?\"(.*)\";?")) item.DataType = "string " + item.Type;
                if (Regex.IsMatch(item.Signature, " ?= ?(\\d*);")) item.DataType = "int " + item.Type;
                if (Regex.IsMatch(item.Signature, " ?= ?(\\d*.\\d*);")) item.DataType = "float " + item.Type;
                if (Regex.IsMatch(item.Signature, " ?= ?function ?\\((.*)")) item.DataType = "function handler " + item.Type;
            } else {
                item.DataType = item.Type;
            }

            if (item.Type == Parser.TYPE_FUNCTION && item.Signature != null) {
                item.SymanticName = item.Signature.Replace(item.Namespace + ".", "").Replace(" = function ", "").Replace(";", "").Replace("{}", "");
            } else {
                item.SymanticName = item.Name;
            }

            if (item.Name.StartsWith("_")) item.Hidden = true;
        }
    }
}
