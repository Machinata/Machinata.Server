using Machinata.Core.Builder;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// 
/// </summary>
namespace Machinata.Core.Documentation.Model {


    public class Comment : ModelObject {
        [FormBuilder(Forms.Frontend.JSON)]
        public string Summary { get; set; }
        [FormBuilder(Forms.Frontend.JSON)]
        public string RawContent { get; set; }
      
        public override string ToString() {
            return this.RawContent?.ToString();
        }

    }
    

    public class FunctionComment : Comment {
      
        public string Return;
        public string Signature;
        public CommentParameter[] Paramters;
    }

    public class CommentParameter {
        public string Name;
        public string Description;
    }
}
