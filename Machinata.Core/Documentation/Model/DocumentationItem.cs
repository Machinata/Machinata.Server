using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Machinata.Core.Documentation.Model {

    /// <summary>
    /// Part of Code that can be documented
    /// </summary>
    public class DocumentationItem : ModelObject {


        public DocumentationItem() {
        }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Code { get; set; }



        /// <summary>
        /// Gets or sets the namespace.
        /// C# Example for Machinata.Core.Model.Price.ToString: Machinata.Core.Model.Price
        /// JS Example for Machinata.Reporting.Data.loadData: Machinata.Reporting.Data
        /// </summary>
        /// <value>
        /// The namespace.
        /// </value>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Namespace { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// C# Example for Machinata.Core.Model.Price.ToString: ToString
        /// JS Example for Machinata.Reporting.Data.loadData: loadData
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Name { get; set; }

        /// <summary>
        /// Represents the item name in a more programmer-understable fashion.
        /// For example, a function with the signature xyz = function(a,b,c) would have a name of
        /// 'xyz(a,b,c)' instead of just 'xyz'.
        /// This makes the documentation much more easier to read.
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string SymanticName { get; set; }

        /// <summary>
        /// Gets or sets the full name.
        /// C# Example for Machinata.Core.Model.Price.ToString: Machinata.Core.Model.Price.ToString
        /// JS Example for Machinata.Reporting.Data.loadData: Machinata.Reporting.Data.loadData
        /// </summary>
        /// <value>
        /// The full name.
        /// </value>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string FullName { get; set; }


        /// <summary>
        /// Gets or sets the signature.
        /// C# Example for Machinata.Core.Model.Price.ToString: Machinata.Core.Model.Price.ToString(string format = null, bool changeThousandsSeperator = true, bool changeZeroCents = true)
        /// JS Example for Machinata.Reporting.Data.loadData: Machinata.Reporting.Data.loadData(dataSource, onSuccess)
        /// </summary>
        /// <value>
        /// The signature.
        /// </value>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Signature { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// Either 
        ///     function
        ///     class
        ///     variable
        ///     constant
        ///     ...
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Type { get; set; }

        /// <summary>
        /// Represents the datatype for variable types.
        /// e.g. string, int, float,. ....
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string DataType { get; set; }

        /// <summary>
        /// Represents the datatype for variable types.
        /// e.g. string, int, float,. ....
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool InheritsFromModelObject { get; set; }

        /// <summary>
        /// The comment summary, for example in C# the [summary] XML tag 
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        //[FormBuilder(Forms.Admin.LISTING)]
        //[FormBuilder(Forms.Admin.VIEW)]
        public string Summary { get; set; }

        /// <summary>
        /// The comment value, for example in C# the [value] XML tag
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Value { get; set; }

        /// <summary>
        /// The comment example, for example in C# the [example] XML tag
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Example { get; set; }

        /// <summary>
        /// To mark an item deprecated
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Deprecated { get; set; }

        /// <summary>
        /// To show an items inheritance
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Inherits { get; set; }

        /// <summary>
        /// To show an items inheritance
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Returns { get; set; }

        /// <summary>
        /// To mark an item hidden
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool Hidden { get; set; }

        /// <summary>
        /// The actual file the item comes from
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string File { get; set; }

        /// <summary>
        /// Paramaters which have been defined in the comment 
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        public Dictionary<string, DocumentationItemParameter> Parameters { get; set; } = new Dictionary<string, DocumentationItemParameter>();


        /// <summary>
        /// Attributes
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        public List<DocumentationItemAttribute> Attributes { get; set; } = new List<DocumentationItemAttribute>();

        [FormBuilder(Forms.Admin.VIEW)]
        public List<DocumentationItemEnumMembers> EnumMembers { get; set; } = new List<DocumentationItemEnumMembers>();

        /// <summary>
        /// This an additional item generated from the parser (e.g. missing class or namespace)
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        public bool Generated { get; set; }


        /// <summary>
        /// Accessibility/Visibilty of the the item
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Accessibility { get; set; }


        /// <summary>
        /// Linenumber from the source file
        /// </summary>
        [FormBuilder(Forms.Frontend.JSON)]
        public int LineNumber { get; set; }

        public int NamespaceLevel {
            get {
                return this.NamespaceURL.Count(ns => ns == '-');
            }
        }

        [FormBuilder(Forms.System.LISTING)]
        public string NamespaceURL {
            get { return _createShortUrl(this.Namespace); }
        }

        private string _createShortUrl(string path) {
            return path.Replace(".", "-");
        }


        [FormBuilder(Forms.System.LISTING)]
        public string FullNameURL {
            get { return _createShortUrl(this.FullName); }
        }


        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        public string SummaryShort {
            get {
                if (this.Summary == null) return null;
                return Core.Util.String.CreateSummarizedText(this.Summary, 30, true, true, true);
            }
        }



        public override JObject GetJSON(FormBuilder form = null, SerializationType serializationType = SerializationType.Default) {
            var current = base.GetJSON(form);
            current["parameters"] = JObject.FromObject(this.Parameters);
            current["attributes"] = JToken.FromObject(this.Attributes);
            current["enum-members"] = JToken.FromObject(this.EnumMembers);
            return current;
        }

        public override string ToString() {
            return this.FullName;
        }

        /// <summary>
        /// Gets the hierarchy  in list of strings
        /// </summary>
        public IList<string> GetNSParts() {
            return this.FullName.Split('.');
        }

        /// <summary>
        /// Checks whether the item has an attribute whith the name
        /// HINT: it automatically removes the Postfix Attribute
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public bool HasAttribute(string attributeName) {
            //if (string.IsNullOrWhiteSpace(attributeName)) {
            //    return false;
            //}
            //var cleanedAttributeName = attributeName.ReplaceSuffix("Attribute",string.Empty);
            //var cleanedAttributes = this.Attributes.Select(a=>a.Type.ReplaceSuffix("Attribute", string.Empty));
            //if (cleanedAttributes.Contains(cleanedAttributeName)) {
            //    return true;
            //}
            //// Not found
            //return false;
            return this.GetAttributes(attributeName).Any();

        }


        public IEnumerable<DocumentationItemAttribute> GetAttributes(string attributeName) {
            if (string.IsNullOrWhiteSpace(attributeName)) {
                return new List<DocumentationItemAttribute>();
            }
            var cleanedAttributeName = attributeName.ReplaceSuffix("Attribute", string.Empty);
            var foundAttributes = this.Attributes.Where(a => a.Type.ReplaceSuffix("Attribute", string.Empty) == cleanedAttributeName);
            return foundAttributes;

        }
    }

    [Serializable]
    [JsonObject]
    public class DocumentationItemParameter {

        public DocumentationItemParameter(string description, string type = null, object value = null) {
            this.Description = description;
            this.Type = type;
            this.Value = value;
        }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("value")]
        public object Value { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
    }

    [Serializable]
    [JsonObject]
    public class DocumentationItemAttribute {

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("parameters")]
        public IList<DocumentationItemAttributeParameter> Parameters { get; set; }


        public DocumentationItemAttribute() {

        }

        public DocumentationItemAttribute(string type) {
            this.Type = type;
        }

        public DocumentationItemAttributeParameter GetParameterByName(string name) {
            return this.Parameters.FirstOrDefault(p => p.Name == name);
        }
        //public DocumentationItemAttribute(string description, string type = null, object value = null) {
        //    this.Description = description;
        //    this.Type = type;
        //    this.Value = value;
        //}

        //[JsonProperty("type")]
        //public string Type { get; set; }
        //[JsonProperty("value")]
        //public object Value { get; set; }
        //[JsonProperty("description")]
        //public string Description { get; set; }
    }

    [Serializable]
    [JsonObject]
    public class DocumentationItemAttributeParameter {

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }


        //public DocumentationItemAttribute(string description, string type = null, object value = null) {
        //    this.Description = description;
        //    this.Type = type;
        //    this.Value = value;
        //}

        //[JsonProperty("type")]
        //public string Type { get; set; }
        //[JsonProperty("value")]
        //public object Value { get; set; }
        //[JsonProperty("description")]
        //public string Description { get; set; }
    }

    [Serializable]
    [JsonObject]
    public class DocumentationItemEnumMembers {
        public DocumentationItemEnumMembers(string name, object value = null) {
            this.Name = name;
            this.Value = value;
        }

        [JsonProperty("name")]
        public string Name { get; private set; }
        [JsonProperty("value")]
        public object Value { get; private set; }
    }

}
