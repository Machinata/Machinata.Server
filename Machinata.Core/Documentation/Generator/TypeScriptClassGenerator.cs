﻿using Machinata.Core.Documentation.Model;
using Machinata.Core.Documentation.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static Machinata.Core.Builder.Forms;

namespace Machinata.Core.Documentation.Generator {
    class TypeScriptClassGenerator : ClassGenerator {

            
        public const string AttributeName = "TypeScriptConverterAttribute";
        public const string AttributeFrontendNSParameterName = "FrontendNamespace";
        public const string AttributeTypeNameParameterName = "TypeName";
        private const string CoreModelPriceFullName = "Machinata.Core.Model.Price";
        private const string CoreModelTranslatableTextFullName = "Machinata.Core.Model.TranslatableText";
        private const string CoreModelContentFileAssetFullName = "Machinata.Core.Model.ContentFileAsset";
        private const string CoreModelPropertiesFullName = "Machinata.Core.Model.Properties";
        static string ENUM_TEMPLATE =
@"/// Warning: autogenerated base class - do not edit    
namespace {enum.frontend-namespace} {
    export enum {enum.name} {
       {enum.entries}
    }
}
";

        static string CLASS_TEMPLATE =
@"/// Warning: autogenerated base class - do not edit    

namespace {class.frontend-namespace} { 

    {class.summary}
    export class {class.name} extends Machinata.Core.ModelObject {

        {class.properties}

        /**
        * Constructor for Base-Class of {class.name}.
        * ModelObject classes must support a empty constructor for automatic data loading/getting.
        */
        constructor() {
            super();
            this.Type = ""{class.name}"";
        }

        /**
        * Internal loader routine for JSON format...
        * Call ```loadFromJSON``` instead.
        */
        fromJSON(json: any): {class.name} {
            super.fromJSON(json);
            // Register all properties
            {class.properties.json-loaders}
            // Return self for chaining
            return this;
        }

        /**
        * Internal getter routine for JSON format...
        * Call ```getJSON``` instead.
        */
        toJSON(): any {
            let json = super.toJSON();
            {class.properties.json-getters}
            return json;
        }

    }
}
";

        static string CLASS_PROPERTY_TEMPLATE =
@"        {property.comment}
        {property.name}: {property.data-type.ts};";

        static string CLASS_PROPERTY_JSON_LOADER_TEMPLATE =
@"            if(json[""{property.name}""] != null) { this.{property.name} = {loader}; }
            else { this.{property.name} = null; }";

        static string CLASS_PROPERTY_JSON_GETTER_TEMPLATE =
@"            if(this.{property.name} != null) { {getter}; }
            else { json[""{property.name}""] = null; }";

        private static string _convertCSTypeToTSType(DocumentationItem property, List<string> enumList) {

            if (property.DataType == "decimal") return "number";
            else if (property.DataType == "int") return "number";
            else if (property.DataType == "int?") return "number | null";
            else if (property.DataType == "float") return "number";
            else if (property.DataType == "long") return "number";
            else if (property.DataType == "bool") return "boolean";
            else if (property.DataType == "String") return "string";
            else if (property.DataType == "List<string>") return "string[]";
            else if (property.DataType == "DateTime") return "Date";
            else if (property.DataType == "DateTime?") return "Date";
            else if (property.DataType == "Price") return CoreModelPriceFullName; 
            else if (property.DataType == "Properties") return CoreModelPropertiesFullName; 
            else if (property.DataType == "TranslatableText") return CoreModelTranslatableTextFullName;
            else if (property.DataType == "ContentFileAsset") return CoreModelContentFileAssetFullName;
            else if (enumList.Contains(property.DataType)) {
                // we have an enum which was most likely defined before in class
                return property.DataType;
                //throw new Exception("this can be removed!?");
            } else if (property.DataType.StartsWith("ICollection<") == true) {
                // IEnumerable<RLListItem>
                var dataType = new Regex(@"ICollection<(\w+)>").Match(property.DataType).Groups[1].Value;

                // Explicit TypeName
                var explicitTypeName = property.GetAttributes(AttributeName).FirstOrDefault()?.GetParameterByName(AttributeTypeNameParameterName)?.Value;
                if (string.IsNullOrEmpty(explicitTypeName) == false) {
                    dataType = explicitTypeName;
                }

                return dataType + "[]";
            } else if (property.DataType.StartsWith("List<") == true) {
                // IEnumerable<RLListItem>
                var dataType = new Regex(@"List<(\w+)>").Match(property.DataType).Groups[1].Value;

                // Explicit TypeName
                var explicitTypeName = property.GetAttributes(AttributeName).FirstOrDefault()?.GetParameterByName(AttributeTypeNameParameterName)?.Value;
                if (string.IsNullOrEmpty(explicitTypeName) == false) {
                    dataType = explicitTypeName;
                }

                return dataType + "[]";
            } else {
                var feNsName = property.GetAttributes(AttributeName).FirstOrDefault()?.GetParameterByName(AttributeFrontendNSParameterName);
                var ns = "";
                if (string.IsNullOrEmpty(feNsName?.Value) == false) {
                    ns = feNsName.Value + ".";
                }
                return ns + property.DataType; // fallback //TODO: @micha try to get attribute frontend namespace property
            }
        }


        private static string _getPropertyLoaderForTSType(string type, List<string> enumList) {

            var direct = @"json[""{property.name}""]";
            var obj = @"new {property.data-type.ts}(); this.{property.name}.fromJSON(json[""{property.name}""])";
            if (type == "Date") {
                return @"new Date(json[""{property.name}""])";
                //} else if (type == "Price") {
                //    return direct;
            } else if (type == "number") {
                return direct;
            } else if (type == "number | null") {
                return direct;
            } else if (type == "string") {
                return direct;
            } else if (type == "string[]") {
                return direct;
            } else if (type == CoreModelPriceFullName) {
                 return @"new "+CoreModelPriceFullName+ @"(json[""{property.name}""][""Value""],json[""{property.name}""][""Currency""])";
            } else if (type == CoreModelTranslatableTextFullName) {
                return @"new " + CoreModelTranslatableTextFullName + @"(json[""{property.name}""])";
            } else if (type == CoreModelContentFileAssetFullName) {
                return @"new " + CoreModelContentFileAssetFullName + @"(json[""{property.name}""][""SourceURL""],json[""{property.name}""][""AssetURL""],json[""{property.name}""][""Hash""])";
            } else if (type == CoreModelPropertiesFullName) {
                return @"new " + CoreModelPropertiesFullName + @"(json[""{property.name}""])";
            } else if (type == "boolean") {
                return direct;
            } else if (type.EndsWith("[]")) {
                // Array of class
                return @"[]; for(let j of json[""{property.name}""]){this.{property.name}.push(new "+type.Replace("[]","")+"().fromJSON(j));}";
            } else if (enumList.Contains(type)) {
                // Enum
                return type + @"[json[""{property.name}""] as keyof typeof "+type+"]";
            } else {
                return obj;
            }
        }


        private static string _getPropertyGetterForTSType(string type, List<string> enumList) {

            var direct = @"json[""{property.name}""] = this.{property.name}";
            var obj = @"json[""{property.name}""] = this.{property.name}?.toJSON()";
            if (type == "Date") {
                return @"json[""{property.name}""] = this.{property.name}?.toISOString()";
            } else if (type == "number") {
                return direct;
            } else if (type == "number | null") {
                return direct;
            } else if (type == "string") {
                return direct;
            } else if (type == "string[]") {
                return direct;
            } else if (type == "boolean") {
                return direct;
            } else if (type.EndsWith("[]")) {
                // Array of class
                return @"json[""{property.name}""] = []; for(let o of this.{property.name}) { json[""{property.name}""].push(o.toJSON()); }";
            } else if (enumList.Contains(type)) {
                // Enum
                return @"json[""{property.name}""] = "+type+"[this.{property.name}]";
            } else {
                return obj;
            }
        }

        public override List<KeyValuePair<string, string>> GenerateClasses(DocumentationPackage package) {

            var propertiesFromModelObject = new List<DocumentationItem>();
            propertiesFromModelObject.Add(new DocumentationItem() {
                Name = "Created",
                Type = "property",
                DataType = "DateTime",
            });

            var result = new List<KeyValuePair<string, string>>();

            var enumList = new List<string>();

            // Enums outside classes
            foreach (var enumToTest in package.Items.Where(i => i.Type == "enum" && i.HasAttribute("TypeScriptConverterAttribute"))) {
                var classesInWhichTheEnumis = package.Items.Where(i => i.FullName == enumToTest.Namespace && i.Type == "class");
                if (classesInWhichTheEnumis.Any()) {
                    continue; // this will be handled below in the class
                }
                var frontendNS = GetExplicitFrontendNameSpace(enumToTest);
                var template = ENUM_TEMPLATE
                    .Replace("{enum.comment}", WrapInTSComment(enumToTest.Summary, "        "))
                    .Replace("{enum.name}", enumToTest.Name)
                    .Replace("{enum.frontend-namespace}", frontendNS) // TODO: get this from attribute directly
                    .Replace("{enum.entries}", string.Join(",", enumToTest.EnumMembers.Select(em => em.Name)));
                enumList.Add(enumToTest.Name);

                result.Add(new KeyValuePair<string, string>(enumToTest.FullName, template));

            }

            foreach (var classItem in package.Items.Where(i => i.Type == "class" && i.HasAttribute("TypeScriptConverterAttribute"))) {

                var classTemplate = new StringBuilder(CLASS_TEMPLATE);

                var enumsFromClass = package.Items.Where(i => i.Namespace == classItem.FullName && i.Type == GhostDocParser.TYPE_ENUM && i.HasAttribute(AttributeName));

                string frontendNS = GetExplicitFrontendNameSpace(classItem);

                foreach (var enumFromClass in enumsFromClass) {
                    var template = ENUM_TEMPLATE
                        .Replace("{enum.comment}", WrapInTSComment(enumFromClass.Summary, "        "))
                        .Replace("{enum.name}", enumFromClass.Name)
                        .Replace("{enum.frontend-namespace}", frontendNS) // TODO: get this from attribute directly
                        .Replace("{enum.entries}", string.Join(",", enumFromClass.EnumMembers.Select(em => em.Name)));

                    enumList.Add(enumFromClass.Name);
                    result.Add(new KeyValuePair<string, string>(enumFromClass.FullName, template));
                }

                var propertiesFromClass = package.Items.Where(i => i.Namespace == classItem.FullName && i.Type == "property" && i.HasAttribute(AttributeName));
                //var allProperties = new List<DocumentationItem>();
                //allProperties.AddRange(propertiesFromModelObject);
                //allProperties.AddRange(propertiesFromClass);

                var propertiesTemplates = new List<string>();
                foreach (var property in propertiesFromClass) {
                    var template = CLASS_PROPERTY_TEMPLATE
                        .Replace("{property.comment}", WrapInTSComment(property.Summary, "        "))
                        .Replace("{property.name}", property.Name)
                        .Replace("{property.data-type.ts}", _convertCSTypeToTSType(property, enumList));
                    propertiesTemplates.Add(template);
                }

                var loadersTemplates = new List<string>();
                foreach (var property in propertiesFromClass) {
                    var template = CLASS_PROPERTY_JSON_LOADER_TEMPLATE;
                    var loader = _getPropertyLoaderForTSType(_convertCSTypeToTSType(property, enumList), enumList);
                    template = template
                        .Replace("{loader}", loader)
                        .Replace("{property.name}", property.Name)
                        .Replace("{property.data-type.ts}", _convertCSTypeToTSType(property, enumList));
                    loadersTemplates.Add(template);
                }

                var gettersTemplates = new List<string>();
                foreach (var property in propertiesFromClass) {
                    var template = CLASS_PROPERTY_JSON_GETTER_TEMPLATE;
                    var getter = _getPropertyGetterForTSType(_convertCSTypeToTSType(property, enumList), enumList);
                    template = template
                        .Replace("{getter}", getter)
                        .Replace("{property.name}", property.Name)
                        .Replace("{property.data-type.ts}", _convertCSTypeToTSType(property, enumList));
                    gettersTemplates.Add(template);
                }

                // Frontend NS

                classTemplate.Replace("{class.summary}", WrapInTSComment(classItem.Summary, "    "));
                classTemplate.Replace("{class.frontend-namespace}", frontendNS);
                classTemplate.Replace("{class.name}", classItem.Name);
                classTemplate.Replace("        {class.properties}", String.Join("\r\n\r\n", propertiesTemplates));
                classTemplate.Replace("            {class.properties.json-loaders}", String.Join("\r\n", loadersTemplates));
                classTemplate.Replace("            {class.properties.json-getters}", String.Join("\r\n", gettersTemplates));

                result.Add(new KeyValuePair<string, string>(classItem.FullName, classTemplate.ToString()));

            }

            return result;
        }

        private static string GetExplicitFrontendNameSpace(DocumentationItem classItem) {
            var frontendNS = "Unknown";
            var frontendNSParam = classItem.GetAttributes(AttributeName).FirstOrDefault()?.GetParameterByName(AttributeFrontendNSParameterName);
            if (frontendNSParam != null) {
                frontendNS = frontendNSParam.Value;
            }

            return frontendNS;
        }

        private string WrapInTSComment(string summary, string indent) {
            if (string.IsNullOrWhiteSpace(summary)) {
                return summary;
            }
            var lines = summary.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            var sb = new StringBuilder();
            sb.AppendLine("/**");
            foreach (var line in lines) {
                if (string.IsNullOrWhiteSpace(line) == false) {
                    sb.AppendLine(indent + "* " + line.Trim());
                }
            }
            sb.Append(indent + "*/");
            return sb.ToString();
        }
    }
}

