﻿using Machinata.Core.Documentation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Machinata.Core.Documentation.Model.DocumentationPackage;

namespace Machinata.Core.Documentation.Generator {
    public class ClassGenerator {

        public virtual List<KeyValuePair<string,string>> GenerateClasses(DocumentationPackage package) {
            throw new NotImplementedException("GenerateClasses in base class is not supported");
        }

        public static ClassGenerator GetClassGenerator(ExportFormat exportFormat) {
            if (exportFormat == ExportFormat.TypeScript) {
                return new TypeScriptClassGenerator();
            }
            throw new Exception($"  {exportFormat} not supported");
        }
    }
}
