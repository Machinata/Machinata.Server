using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Model {

    
    
    [AttributeUsage(validOn: AttributeTargets.All)]
    public class TypeScriptConverterAttribute : System.Attribute {

        public string FrontendNamespace;
        public string TypeName;

    }
   
}
