using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Machinata.Core.TaskManager {

    public abstract class BatchTask<T> : Task where T : BatchTaskItem {

        protected virtual int BatchSize { get; set; } = 100;

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override sealed void Process() {

            var taskToRun = GetTaskIdToRun();
            if (taskToRun == null) {
                Log($"Nothing to do for {this.Name}");
                return;
            }

            LogFormat($"Running Task: " + GetTitle(taskToRun.TaskId));

            // All Items
            var items = GetAllItems(taskToRun.TaskId);

            // Already Processed
            var processedItems = GetProcessedItems(taskToRun.TaskId).ToList();

            // Already Failed
            var failedItems = GetFailedItems(taskToRun.TaskId).ToList();


            // Items to process in this batch -> to list because count() hits db as well
            var itemsToProcess = GetItemsToProcess(items, processedItems, failedItems).ToList();
            var itemsToProcessCount =  itemsToProcess.Count();
            int itemsProcess = 0;

            LogFormat("Total Items: " + items.Count());
            LogFormat("Processed Items: " + processedItems.Count());
            LogFormat("To Process: " + itemsToProcessCount);

            if (taskToRun.Status == TaskLog.TaskStatuses.Waiting) {
                taskToRun = WriteTaskStatus(this.DB, taskToRun.TaskId, TaskLog.TaskStatuses.Started);
                this.DB.SaveChanges();
            }

            var itemExceptions = new Dictionary<string,Exception>();

            foreach(var item in itemsToProcess) {
                try {

                    // TaskManager still running?
                    if (TaskManager.State != TaskManager.TaskManagerState.Running){
                        LogFormat("TaskManager is not running anymore. Quit Processing.");
                        LogFormat("State: " + TaskManager.State);
                        break;
                    }

                    // Is the task paused?
                    if (IsPaused(taskToRun.TaskId) == true) {
                        LogFormat("Task is paused. Quit Processing.");
                        LogFormat("State: " + GetTaskStatus(taskToRun.TaskId));
                        break;
                    }

                    // Process the next item
                    LogFormat("Processing item {0}: ", item.BatchTaskItemTitle);
                    var result = Process(taskToRun.TaskId, item);

                    // Log 
                    WriteItemStatus(taskToRun, item, TaskLog.TaskStatuses.ItemProcessed);

                    // Save
                    this.DB.SaveChanges();

                    // Progress
                    this.RegisterProgress(itemsProcess++, itemsToProcessCount);

                    // Cool down
                    Thread.Sleep(100);
                }
                catch(Exception e) {
                    // Log Fail --> item wont be processed again.
                    try {
                        WriteItemStatus(taskToRun, item, TaskLog.TaskStatuses.ItemFailed);
                        // Save
                        this.DB.SaveChanges();
                    } catch (Exception e1){
                        LogWithWarning($"Error logging item fail status {item.BatchTaskItemTitle}", e1);
                    }

                    LogWithWarning($"Error processing item: {item.BatchTaskItemTitle}",e);
                    itemExceptions[item.BatchTaskItemId] = e;
                    OnItemException(taskToRun.TaskId, item, e);
                }
            }

       
            // Finished
            if (!itemsToProcess.Any()) {

                // Error?
                if (GetFailedItems(taskToRun.TaskId).Any()) {
                    WriteTaskStatus(this.DB, taskToRun.TaskId, TaskLog.TaskStatuses.Error);
                    this.DB.SaveChanges();
                } else {
                    WriteTaskStatus(this.DB, taskToRun.TaskId, TaskLog.TaskStatuses.Finished);
                    this.DB.SaveChanges();
                }
            }

            // Errors
            if (itemExceptions.Any()) {
                throw new BackendException("batch-task-error", CompileErrors(itemExceptions));
            }

        }

        /// <summary>
        /// BatchTasks can implement additional logic when a exception on processing an item occurs
        /// IMPORTANT: Wrap code in try/catch to not break the execution
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="e">The exeption</param>
        /// <exception cref="System.NotImplementedException"></exception>
        protected virtual void OnItemException(string taskId, T item, Exception e) {
           // nothing in default
        }

        public abstract T GetProcessedTypedItem(string taskId, string id, IEnumerable<T> allItems);


        /// <summary>
        /// Has to provide all items which have to bee processed
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        /// <returns></returns>
        public abstract IEnumerable<T> GetAllItems(string taskId);

        /// <summary>
        /// Logic for each item to be processed
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public abstract object Process(string taskId, T item);


        public abstract string GetTitle(string taskId);

        #endregion

        #region Public Methods ///////////////////////////////////////////////////////////////

        /// <summary>
        /// count( Failed + Process)
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        /// <returns></returns>
        public int GetProgress(string taskId) {
            var processedItems = GetProcessedItems(taskId);
            var failedItems = GetFailedItems(taskId);
            return processedItems.Count() + failedItems.Count();
        }

        /// <summary>
        /// Sets the BatchTask to Ready for Processing
        /// </summary>
        /// <param name="taskId">The task identifier. Wich is only valid for one run of the batch task. E.g  Mailing.PublicId</param>
        /// <exception cref="BackendException">start-error;Task already started</exception>
        public void Start(string taskId) {

            var currentStatus = GetTaskStatus(taskId);
          
            if (currentStatus == null) {
                WriteTaskStatus(this.DB, taskId, TaskLog.TaskStatuses.Waiting);
            } else {
                throw new BackendException("start-error", $"Task already started. Current status: {currentStatus.Value}");
            }
        }

        public bool IsPauseable(string taskId) {
            var currentStatus = GetTaskStatus(taskId);
            if (currentStatus == TaskLog.TaskStatuses.Waiting 
                || currentStatus == TaskLog.TaskStatuses.Started
                || currentStatus == TaskLog.TaskStatuses.ItemProcessed
                || currentStatus == TaskLog.TaskStatuses.ItemFailed) {
                return true;
            } else {
                return false;
            }
        }

        public bool IsResumeable(string taskId) {
            var currentStatus = GetTaskStatus(taskId);
            if (currentStatus != null && currentStatus == TaskLog.TaskStatuses.Paused) {
                return true;
            } else {
                return false;
            }
        }

        public void Pause(string taskId) {
            var currentStatus = GetTaskStatus(taskId);
            if (currentStatus == TaskLog.TaskStatuses.Waiting || IsRunning(taskId)) {
                WriteTaskStatus(this.DB, taskId, TaskLog.TaskStatuses.Paused);
            } else {
                throw new BackendException("pause-error", $"Cannot start task in current status: {currentStatus.Value}");
            }
        }

        public void Resume(string taskId) {
            var currentStatus = GetLatestTaskLog(taskId);
            if (currentStatus != null && currentStatus.Status == TaskLog.TaskStatuses.Paused) {
                this.DB.TaskLogs().Remove(currentStatus);
            } else {
                throw new BackendException("pause-error", $"Cannot start task in current status: {currentStatus?.Status}");
            }
        }

        public void RetryFailed(string taskId) {
            var currentStatus = GetLatestTaskLog(taskId);
            if (currentStatus != null && currentStatus.Status == TaskLog.TaskStatuses.Error) {
                var failed = GetFailedTaskLogs(taskId);

                // Failed Items
                this.DB.TaskLogs().RemoveRange(failed);

                // Error Status
                this.DB.DeleteEntity(currentStatus);
            } else {
                throw new BackendException("error-error", $"Cannot retry failed items with task in current status: {currentStatus?.Status}");
            }
        }

        /// <summary>
        /// Delete the error log and write a finished log
        /// </summary>
        /// <param name="taskId"></param>
        public void IgnoreFailed(string taskId) {
            var currentStatus = GetLatestTaskLog(taskId);
            if (currentStatus != null && currentStatus.Status == TaskLog.TaskStatuses.Error) {
               
                
                // DeleError Status
                this.DB.DeleteEntity(currentStatus);

                // Write finished
                this.WriteTaskStatus(this.DB, taskId, TaskLog.TaskStatuses.Finished);
            } else {
                throw new BackendException("error-error", $"Cannot retry failed items with task in current status: {currentStatus?.Status}");
            }
        }

        /// <summary>
        /// Gets the task status based on the latest log for the taskId (ie, the latest task status is returned, if any).
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        /// <returns></returns>
        public TaskLog.TaskStatuses? GetTaskStatus(string taskId) {
            var latestLog = GetLatestTaskLog(taskId);
            return latestLog?.Status;
        }

        /// <summary>
        /// Gets the latest task log. if a pause log is there it is first
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        /// <returns></returns>
        public TaskLog GetLatestTaskLog(string taskId) {
            var taskLogs = GetTasksLogs(taskId);
            return taskLogs.OrderByDescending(t => t.Status == TaskLog.TaskStatuses.Paused).ThenByDescending(t => t.Id).FirstOrDefault();
        }

        public bool IsRunning(string taskId) {
            var status = GetTaskStatus(taskId);
            if (status == TaskLog.TaskStatuses.Started
                || status == TaskLog.TaskStatuses.ItemProcessed
                || status == TaskLog.TaskStatuses.ItemFailed) {
                return true;
            }
            return false;
        }

        public bool IsFailed(string taskId) {
            var status = GetTaskStatus(taskId);
            return (status == TaskLog.TaskStatuses.Error);
        }

        /// <summary>
        /// Determines whether the specified task is paused. -> Gets a new DB context because a pause is might have happened dureing the current Process scope and there is an dirty tasklog
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        /// <returns></returns>
        public bool IsPaused(string taskId) {
            using (var db = Model.ModelContext.GetModelContext(null)) {
                var pausedLog = db.TaskLogs().FirstOrDefault(tl => tl.TaskId == taskId && tl.Status == TaskLog.TaskStatuses.Paused && tl.TaskType == this.TaskType);
                if (pausedLog != null) {
                    return true;
                }
                return false;
            }
        }

        public IQueryable<string> GetProcessedItems(string taskId) {
            var taskLogs = GetTasksLogs(taskId);

            taskLogs = taskLogs.Where(tl => tl.Status == TaskLog.TaskStatuses.ItemProcessed);

            return taskLogs.Select(tl => tl.ItemId);
        }

        public IEnumerable<string> GetFailedItems(string taskId) {
            var taskLogs = GetTasksLogs(taskId);
            taskLogs = taskLogs.Where(tl => tl.Status == TaskLog.TaskStatuses.ItemFailed);
            return taskLogs.Select(tl => tl.ItemId);
        }

        public IEnumerable<TaskLog> GetFailedTaskLogs(string taskId) {
            var taskLogs = GetTasksLogs(taskId);
            taskLogs = taskLogs.Where(tl => tl.Status == TaskLog.TaskStatuses.ItemFailed);
            return taskLogs;
        }

        /// <summary>
        /// Gets the processed converted back to original type
        /// It's not guaranteed all items are in the list (original might have changed)
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        /// <returns></returns>
        public IEnumerable<T> GetProcessedTypedItems(string taskId, IEnumerable<T> allItems) {
            var processItems = GetProcessedItems(taskId).ToList();
            return processItems.Select(i => GetProcessedTypedItem(taskId, i, allItems)).Where(i => i != null);
        }

        /// <summary>
        /// Gets the failed converted back to original type
        /// It's not guaranteed all items are in the list (original might have changed)
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        /// <returns></returns>
        public IEnumerable<T> GetFailedTypedItems(string taskId, IEnumerable<T> allItems) {
            var failedItems = GetFailedItems(taskId).ToList();
            return failedItems.Select(i => GetProcessedTypedItem(taskId, i, allItems)).Where(i => i != null);
        }

        /// <summary>
        /// Deletes all logs for this task.
        /// Returns whether any logs where found/deleted
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        public bool DeleteLogs(string taskId) {
            // Logs
            var logs = GetTasksLogs(taskId);

            // Remove
            this.DB.TaskLogs().RemoveRange(logs);

            // Return something was deleted
            return logs.Any();
        }

        #endregion

        #region Private Methods //////////////////////////////////////////////////////////////

        private IQueryable<TaskLog> GetTasksLogs(string taskId) {
            var tasksOfType = this.DB.TaskLogs().Where(tl => tl.TaskType == this.TaskType);
            var tasksOfCurrentRun = tasksOfType.Where(t => t.TaskId == taskId);
            return tasksOfCurrentRun;
        }

        private void WriteItemStatus(TaskLog taskToRun, T item, TaskLog.TaskStatuses status) {
            var log = new TaskLog();
            log.Status = status;
            log.ItemId = item.BatchTaskItemId;
            log.TaskId = taskToRun.TaskId;
            log.TaskType = this.TaskType;
            this.DB.TaskLogs().Add(log);
        }

        private string CompileErrors(Dictionary<string, Exception> itemExceptions) {
            var errors = new StringBuilder();

            foreach(var exception in itemExceptions) {
                errors.AppendLine($"Error Item: {exception.Key}");
                errors.AppendLine($"Error: {exception.Value}");
                errors.AppendLine($"Error Stack: {exception.Value.StackTrace}");
                if (exception.Value.InnerException != null) {
                    errors.AppendLine($"Inner Error: {exception.Value.InnerException}");
                    errors.AppendLine($"Inner Error Stack: {exception.Value.InnerException.StackTrace}");
                }
                errors.AppendLine("------------------------------------------------------------------------------------");
            }
            return errors.ToString();
        }

        private string TaskType {
           get { return this.GetType().FullName; }
        }

        private TaskLog WriteTaskStatus(ModelContext db, string taskId, TaskLog.TaskStatuses status) {
            var log = new TaskLog();
            log.Status = status;
            log.TaskId = taskId;
            log.TaskType = this.TaskType;
            db.TaskLogs().Add(log);
            return log;
        }

        private TaskLog GetTaskIdToRun() {
            // TODO: multiple tasks -> switch between types and instances to not serially process them

            // Filter out ProcessedItem and FailedItem Logs
            var tasksOfType = this.DB.TaskLogs().Where(
                tl =>
                tl.TaskType == this.TaskType
                && (tl.Status == TaskLog.TaskStatuses.Started
                    || tl.Status == TaskLog.TaskStatuses.Finished 
                    || tl.Status == TaskLog.TaskStatuses.Waiting
                    || tl.Status == TaskLog.TaskStatuses.Paused
                    || tl.Status == TaskLog.TaskStatuses.Error
                    ));

            // Group by TaskId
            var groupByTaskId = tasksOfType.GroupBy(t => t.TaskId);

            var startedNotFinished = groupByTaskId.FirstOrDefault
                 (g =>
                       g.Select(s => s.Status).Contains(TaskLog.TaskStatuses.Started)
                   && !g.Select(s => s.Status).Contains(TaskLog.TaskStatuses.Finished)
                   && !g.Select(s => s.Status).Contains(TaskLog.TaskStatuses.Error)
                   && !g.Select(s => s.Status).Contains(TaskLog.TaskStatuses.Paused)
                 );

                
            if (startedNotFinished != null) {
                return startedNotFinished.OrderByDescending(b => b.Status).FirstOrDefault();
            }

            var waitingNotStarted = groupByTaskId.FirstOrDefault(
                g => 
                    g.Select(s => s.Status).Contains(TaskLog.TaskStatuses.Waiting)
                && !g.Select(s => s.Status).Contains(TaskLog.TaskStatuses.Started)
                && !g.Select(s => s.Status).Contains(TaskLog.TaskStatuses.Paused)
                );
            if (waitingNotStarted != null) {
                return waitingNotStarted.OrderByDescending(b => b.Status).FirstOrDefault();
            }
            return null;
        }

        /// <summary>
        /// Gets the items to process: (All - Processed).Take(BatchSize)
        /// </summary>
        /// <param name="items">The items.</param>
        /// <param name="processedItems">The processed items.</param>
        /// <returns></returns>
        private IEnumerable<T> GetItemsToProcess(IEnumerable<T> items, IEnumerable<string> processedItems, IEnumerable<string> failedItems) {
            // All withoud the processed, without the filed 
            var itemsToProcess = items.Where(i => !processedItems.Contains(i.BatchTaskItemId) && !failedItems.Contains(i.BatchTaskItemId));

            // Paging
            itemsToProcess = itemsToProcess.Take(BatchSize);

            // Additional distinct. In the batch might be duplicates
            return itemsToProcess.DistinctBy(i => i.BatchTaskItemId);
        }


        #endregion


    }

    public interface BatchTaskItem {
        /// <summary>
        /// Gets the batch task item identifier.
        /// Should be encrypted since its logged to the db
        /// </summary>
        /// <value>
        /// The batch task item identifier.
        /// </value>
        string BatchTaskItemId { get; }

        /// <summary>
        /// Gets the batch task item title.
        /// May be plain text since its only used for logging and in UI
        /// </summary>
        /// <value>
        /// The batch task item title.
        /// </value>
        string BatchTaskItemTitle { get; }
    }

}
