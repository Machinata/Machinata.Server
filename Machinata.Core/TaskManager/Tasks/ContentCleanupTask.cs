using Machinata.Core.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Model;

namespace Machinata.Core.TaskManager.Tasks {

    public class ContentCleanupTask : Task {

        public override void Process() {

            // Content files from CMS
            CleanupContentFiles();
                     
            // Old pages in trash
            CleanupCMSTrash();

        }

        private void CleanupContentFiles() {

            Log($"Starting CMS content files cleanup");

            int deleted = 0;
            int total = this.DB.ContentFiles().Count();
            var oldFilesDate = DateTime.UtcNow.AddDays(-7); // we want files older than a specific date
            var contentFiles = this.DB.ContentFiles().Where(cf => cf.Created < oldFilesDate).OrderBy(cf => cf.Id).ToList(); // we must cache this unfortunately to iterate it safely...
            for (int i = 0; i < total; i++) {

                // Get the content file
                try {
                    var cf = contentFiles.Skip(i).FirstOrDefault();
                    if (cf == null) continue;
                    if (cf.ContentURL == null) continue;

                    // Is it content node?
                    if (cf.Source == nameof(ContentNode)) {
                        // Is it used somewhere?
                        string url = cf.ContentURL;
                        var nodeCount = this.DB.ContentNodes().Where(cn =>
                            (cn.Value == url)
                            ||
                            (cn.Value != null && cn.Value.Contains(url))
                            ||
                            (cn.Options.Data != null && cn.Options.Data.Contains(url))
                        ).Count();
                        if (nodeCount == 0) {
                            this.Log($"Content File {cf.FileName} ({cf.ContentURL}, {cf.Source}, created {cf.Created}) unused, will delete...");
                            try {
                                Data.DataCenter.DeleteFile(this.DB, cf);
                                this.DB.SaveChanges();
                            } catch (Exception deleteException) {
                                throw deleteException;
                            } finally {
                                deleted++;
                            }
                        }
                    }
                } catch (Exception e) {
                    LogWithWarning("Error", e);
                }

                RegisterProgress(i, total);
            }

            Log($"Deleted {deleted} files.");
        }

        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = true;
            config.Install = true;
            config.Interval = "1d";
            return config;
        }

        private void CleanupCMSTrash() {

            Log($"Starting CMS trash cleanup");

            var maxAge = DateTime.UtcNow.AddDays(14);
            var trashNode = ContentNode.GetByPath(this.DB, ContentNode.TRASH_PATH_NAME);

            if (trashNode == null) {
                // no trash nothing todo
                return;
            }

            // Load subpages and content
            trashNode.IncludeContent();

            // Grab pages under /Trash
            var trashedPages = trashNode
                .ChildrenForType(ContentNode.NODE_TYPE_NODE)
                .Where(n => n.Created < maxAge).ToList();

            int deletedItems = 0;

            foreach (var page in trashedPages) {

                this.Log($"ContentNode {page.Name} ({page.Path}, {page.SummaryShort} created {page.Created}) older than {maxAge}, will delete...");

                // Delete
                this.DB.DeleteEntity(page); // hint: this will also delete children via OnDelete
               
                // Progress
                deletedItems++;

                // Save
                this.DB.SaveChanges();
            }

            Log($"Deleted {deletedItems} content pages.");
        }
    }
}
