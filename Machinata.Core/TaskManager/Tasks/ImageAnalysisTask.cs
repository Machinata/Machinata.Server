using Machinata.Core.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Model;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using static Machinata.Core.Util.Image;

namespace Machinata.Core.TaskManager.Tasks {

    public class ImageAnalysisTask : Task {

        public override void Process() {

            

            var imageFiles = this.DB.ContentFiles().Where(cf => cf.FileCategory == ContentFile.ContentCategory.Image);
            foreach(var imageFile in imageFiles) {

                try {
                    Thread.Sleep(200);
                    var path = Data.DataCenter.DownloadFile(imageFile);
                    var extension = System.IO.Path.GetExtension(path).ToLower();
                    if (extension == ".svg") continue;
                    if (extension == ".gif") continue;
                    using (var image = new Bitmap(path)) {
                        var format = Core.Util.Image.GetColorFormat(image);
                        if (format!= ImageColorFormat.Rgb) {
                            Log("--------------------------------------------------- " );
                            Log("Image does not have RGB format: " + imageFile.ContentURL);
                            Log("format: " + format);
                            Log("date: " + imageFile.Created);
                            Log("link: /admin/data/files/content-file/" + imageFile.PublicId);

                            var nodes = this.DB.ContentNodes().Where(n => n.Value != null && n.Value.Contains(imageFile.ContentURL));
                            foreach(var node in nodes) {
                                Log("CMS reference: " + node.Path);
                            }

                        }
                    }
                }
                catch(Exception e) {
                    Log("--------------------------------------------------- ");
                    var report = Core.EmailLogger.CompileFullErrorReport(null, null, e);
                    Log($"Error analysing file {imageFile.ContentURL}, error= {report}");
                }

            }

            Log("--------------------------------------------------- ");

        }

        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = false;
            config.Install = false;
            config.Interval = "1d";
            return config;
        }

      
    }
}
