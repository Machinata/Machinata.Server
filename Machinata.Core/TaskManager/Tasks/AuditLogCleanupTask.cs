using Machinata.Core.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Model;
using Machinata.Core.Util;

namespace Machinata.Core.TaskManager.Tasks {

    public class AuditLogCleanupTask : Task {

        public override void Process() {

            // Max AuditLogs per run
            const int MaxLogs = 10000;

            // Check 
            if (Core.Config.AuditLogsAutoRemoveAfterDays < 0) {
                throw new Exception($"Please define AuditLogsAutoRemoveAfterDays");
            }

            // Audit Logs, Max 1000 per time
            var auditLogs = this.DB.AuditLogs().AsQueryable();

            // Older Logs
            var timeSpan = new TimeSpan( 
                days: Core.Config.AuditLogsAutoRemoveAfterDays,
                hours: 0, 
                minutes: 0, 
                seconds: 0,
                milliseconds: 0
                );

            var minDate = DateTime.UtcNow - timeSpan;
            DateTime? minDateSkipTargets = null;
            if (Config.AuditLogsAutoRemoveSkipTargetsMaxDays.HasValue) {
                minDateSkipTargets = DateTime.UtcNow - new TimeSpan(Config.AuditLogsAutoRemoveSkipTargetsMaxDays.Value, 0, 0, 0);
            }
            
            
            
           
            Log("Filter out AuditLogs newer than: " + minDate.ToString(Core.Config.DateFormat));
            auditLogs = auditLogs.Where(al => al.Created < minDate);
          

            // Check Skip Targets
            if (Core.Config.AuditLogsAutoRemoveSkipTargets.Count == 0) {
                throw new Exception($"Please define AuditLogsAutoRemoveSkipTargets");
            }

            Log("Filter out AuditLogs with Targets: " + string.Join(", ", Core.Config.AuditLogsAutoRemoveSkipTargets) + " if not older than: " + minDateSkipTargets?.ToDefaultTimezoneDateTimeString());

            auditLogs = auditLogs.Where(al => Core.Config.AuditLogsAutoRemoveSkipTargets.Contains(al.TargetType) == false || (minDateSkipTargets.HasValue && al.Created < minDateSkipTargets));

            var auditLogsGroups = auditLogs.Take(MaxLogs).GroupBy(al => al.TargetType);
            int count = 0;
            foreach( var auditlogGroup in auditLogsGroups.ToList()) {
                Log($"Found '{auditlogGroup.Key}'-Target AuditLogs: {auditlogGroup.Count()}.");
                foreach(var auditLog in auditlogGroup.OrderBy(a=>a.Created)) {

                    // DOUBLE CHECK
                    if(Core.Config.AuditLogsAutoRemoveSkipTargets.Contains(auditLog.TargetType) == true && minDateSkipTargets.HasValue && minDateSkipTargets.Value < auditLog.Created){
                        throw new Exception("Something is wrong with Config.AuditLogsAutoRemoveSkipTargetsMaxDays");
                    }

                    Log($"Deleting AuditLog '{auditLog.Target}' Created: {auditLog.Created.ToString(Core.Config.DateFormat)}.");
                    this.DB.DeleteEntity(auditLog);
                    count++;
                }

              

                if (this.TestMode == false) {
                    // Save per Target
                    this.DB.SaveChanges();
                }
            }

            Log($"Deleted {count} AuditLogs.");
        }

        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = true;
            config.Install = true;
            config.Interval = "1d";
            return config;
        }
    }
}
