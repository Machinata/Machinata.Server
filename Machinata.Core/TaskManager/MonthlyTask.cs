
using Machinata.Core.Model;

using System;

using System.Linq;


namespace Machinata.Core.TaskManager {


    /// <summary>
    /// Helper class if you want to do a task once a month
    /// this uses a task log the save whether the task was once executed
    /// </summary>
    public abstract class MonthlyTask : Core.TaskManager.Task {

        public TaskLog GetTaskLog(DateTime dateTime) {
            var id = GetTaskId(dateTime);
            var taskLog = this.DB.TaskLogs().FirstOrDefault(t => t.TaskType == this.Type && t.TaskId == id);
            return taskLog;
        }

        public string GetTaskId(DateTime dateTime) {
            return dateTime.ToString("yyyy-MM");
        }

        public void SetMonthlyTaskDone(DateTime dateTime) {
            var id = GetTaskId(dateTime);
            var taskLog = new TaskLog();
            taskLog.TaskId = id;
            taskLog.TaskType = this.Type;
            this.DB.TaskLogs().Add(taskLog);
        }

        public bool IsMonthlyTaskDone(DateTime dateTime) {
            var taskLog = GetTaskLog(dateTime);
            return taskLog != null;
        }


    }





}
