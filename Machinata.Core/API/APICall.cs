using Machinata.Core.Exceptions;
using System;
using System.Linq;
using System.Text;


namespace Machinata.Core.API {

    #region APICall Plug-n-Play Class
    /// <summary>
    /// A light-weight utility class for making chained api requests to Machinata api endpoints.
    /// 
    /// To simplify access, one can use the APICall class to make requests to a Machinata API endpoint:
    /// ```
    /// var data = new API.APICall("/api/admin/sandbox/echo","https://api.yourserver.com")
    ///.WithAPIKey(API_KEY_ID, API_KEY_SECRET)
    ///.WithParameter("param1", "val1")
    ///.Send()
    ///.Data();
    /// ```
    /// An endpoint can be configured by default to make things easier (in any config):
    /// ```
    /// <add key="{ENDPOINT_NAME}API-Server" value="https://api.yourserver.com" />
    ///<add key="{ENDPOINT_NAME}API-KeyId" value="YOUR API KEY ID" />
    ///<add key="{ENDPOINT_NAME}API-KeySecret" value="YOUR API KEY SECRET" />
    ///<add key="{ENDPOINT_NAME}API-IgnoreSSLCertificate" value="false" /> 
    /// ```
    /// ```
    /// var json = new API.APICall("/api/admin/sandbox/echo")
    ///.WithAPIKey()
    ///.Send()
    ///.JSON(); 
    /// ```
    /// By default, the APICall class uses the endpoint configuration Default. You can change your endpoint by calling WithEndPoint():
    /// ```
    /// var json = new API.APICall("/api/admin/sandbox/echo")
    ///.WithEndpoint("MySpecialEndpoint")
    ///.WithAPIKey()
    /// .Send()
    ///.JSON();
    /// ```
    /// ```
    /// <add key="MySpecialEndpointAPI-Server" value="https://api.myspecialendpoint.com" />
    /// <add key = "MySpecialEndpointAPI-KeyId" value="YOUR API KEY ID" />
    /// <add key = "MySpecialEndpointAPI-KeySecret" value="YOUR API KEY SECRET" />
    /// ```
    /// You can also access non-Machinata APIs easily:
    /// ```
    /// var json = new API.APICall("/users","https://jsonplaceholder.typicode.com")
    /// .Send()
    /// .JSON();
    /// ```










    /// </summary>
    public class APICall {
        /// <summary>
        /// 
        /// </summary>
        /// <hidden/>
        private object _uploadData = null;

        /// <summary>
        /// 
        /// </summary>
        /// <hidden/>
        private object _responseData = null;

        /// <summary>
        /// 
        /// </summary>
        /// <hidden/>
        private System.Collections.Specialized.NameValueCollection _params = null;


        /// <summary>
        /// 
        /// </summary>
        /// <hidden/>
        public string EndPoint = "Default";

        /// <summary>
        /// 
        /// </summary>
        /// <hidden/>
        public string Server;

        /// <summary>
        /// 
        /// </summary>
        /// <hidden/>
        public string Resource;

        /// <summary>
        /// 
        /// </summary>
        /// <hidden/>
        public string KeyId;

        /// <summary>
        /// 
        /// </summary>
        /// <hidden/>
        public string KeySecret;

        /// <summary>
        /// 
        /// </summary>
        /// <hidden/>
        private string _getConfig(string key, string defaultVal = null) {
            return Core.Config.GetStringSetting($"{EndPoint}API-{key}",defaultVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <hidden/>
        private bool _getBoolConfig(string key, bool defaultVal = false) {
            return bool.Parse(_getConfig(key, defaultVal.ToString()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <hidden/>
        private string _getURLConfig(string key) {
            return Core.Config.GetURLSetting($"{EndPoint}API-{key}");
        }

        /// <summary>
        /// 
        /// </summary>
        public APICall(string resource, string server = null) {
            this.Server = server;
            this.Resource = resource;
        }

        /// <summary>
        /// 
        /// </summary>
        public APICall WithEndPoint(string endpoint) {
            this.EndPoint = endpoint;
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        public APICall WithAPIKey() {
            return WithAPIKey(
                _getConfig("KeyId"),
                _getConfig("KeySecret")
            );
        }

        /// <summary>
        /// 
        /// </summary>
        public APICall WithAPIKey(string keyId, string keySecret) {
            this.KeyId = keyId;
            this.KeySecret = keySecret;
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        public APICall WithParameter(string key, string val) {
            if (_params == null) _params = new System.Collections.Specialized.NameValueCollection();
            _params[key] = val;
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        public APICall Send() {
            // Init
            string server = this.Server;
            if (server == null) server = _getURLConfig("Server");
            string method = "GET";
            var wc = new System.Net.WebClient();
            wc.BaseAddress = server;
            // SSL ignore?
            if(_getBoolConfig("IgnoreSSLCertificate")) {
                Core.Util.HTTP.IgnoreSSLCertificates();
            }
            if(_params != null) {
                method = "POST";
            }
            // Apply API key?
            if(this.KeyId != null && this.KeySecret != null) {
                var authHeader = Core.API.APIAuth.GenerateAuthenticationHeader(this.KeyId, this.KeySecret, method, this.Resource);
                wc.Headers.Add("Authentication", authHeader);
            }
            // Do request
            // We catch all exceptions to see if we can unwrap the X-Error headers if the api call exception is coming
            // from another Machinata server...
            try {
                if (_uploadData != null) {
                    throw new NotImplementedException();
                    var bytes = wc.UploadData(this.Resource, null);
                    _responseData = Encoding.UTF8.GetString(bytes);
                } else if (_params != null) {
                    var bytes = wc.UploadValues(this.Resource, _params);
                    _responseData = Encoding.UTF8.GetString(bytes);
                } else {
                    _responseData = wc.DownloadString(this.Resource);
                }
            }catch(System.Net.WebException we) {
                var response = we.Response;
                if(response != null) {
                    // Upack the X-Error headers from the original server BackendException code/message/status
                    var errorStatus = response.Headers[BackendException.ERROR_HTTP_HEADER_STATUS];
                    var errorCode = response.Headers[BackendException.ERROR_HTTP_HEADER_CODE];
                    var errorMsg = response.Headers[BackendException.ERROR_HTTP_HEADER_MESSAGE];
                    if(errorCode != null && errorMsg != null && errorStatus != null) {
                        var errorStatusInt = int.Parse(errorStatus);
                        throw new BackendException(errorCode, errorMsg, we, errorStatusInt);
                    }
                }
                throw we;
            } catch(Exception e) {
                throw e;
            }
            // Return self
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        public object Data() {
            return _responseData;
        }

        /// <summary>
        /// 
        /// </summary>
        public Newtonsoft.Json.Linq.JObject JSON() {
            return Core.JSON.ParseJsonAsJObject((string)_responseData);
        }

        /// <summary>
        /// 
        /// </summary>
        public System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JObject> JSONArray() {
            return Core.JSON.ParseJsonArrayAsJObjects((string)_responseData);
        }

        

    }
    #endregion

}
