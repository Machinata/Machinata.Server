﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// The API namespace provides common API routines for publicly exposing API's. Any regular API handler
/// can be integrated into a public API through the use of API Keys. API Keys are backwards compatable
/// with AuthTokens (or rather, a replacement), with the difference that each request is further validated
/// for basic replay attacks and author authenticity through the use of timestamps and key secrets.
/// </summary>
namespace Machinata.Core.API {

}