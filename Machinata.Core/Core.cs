﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// ## Building

/// Machinata can be built on any .NET system with version 4.6.1 or greater. It is compatible on Linux with MonoDevelop/XamarinStudio and Mono 5.0+, and compatible on Mac with MonoDevelop/XamarinStudio/VisualStudio with Mono 5.0+.


/// ## Adding Products
///  - Create a new ASP.net web project called ```Machinata.Product.ProductXYZ``` where ```ProductXYZ``` would be something like ```CocaColaWebsite```. Use the empty template  and make sure to deselect "Use Application Insights"
///  - Under Project Settings, make sure the .net version is 4.6.1.
///  - Under Project Settings > Web, deselect "Apply server settings to all users"
///  - Copy the boiler plate files at Machinata.Product.Template to the project root, replacing any default files from Visual Studio. Make sure in the product project to 'include' the files and set any non-code files to 'Copy if newer'.
///  - Change AssemblyInfo.cs version to the following (ensures that bundle caches are properly versioned across all libraries):

/// ```c#
/// [assembly: AssemblyVersion("1.0.*")]
///  //    // [assembly: AssemblyFileVersion("1.0.0.0")] 
/// ```
///  - In VS 2019: edit the.csproj and change the following entry to false
///  
/// ```xml
/// <Deterministic>false</Deterministic>
/// ```
///  - Set the product config file (at Config/Machinata.Product.ProductXYZ.config) to 'Always Copy'.This make sure that Visual Studio will always build the new modules into the project bin.
///  - Remove reference ```Microsoft.CodeDom.Providers.DotNetCompilerPlatform```
///  - Remove reference ```Microsoft.CSharp```
///  - If the project created has the section ```EnsureNuGetPackageBuildImports``` or project imports in .csproj, remove it from the Project file
///  ```
///  <Target Name="EnsureNuGetPackageBuildImports" BeforeTargets="PrepareForBuild">
///    ...
///  </Target>
///  <Import Project="..\packages\Microsoft.CodeDom.Providers.DotNetCompilerPlatform.1.0.0\build\Microsoft.CodeDom.Providers.DotNetCompilerPlatform.props" Condition="Exists('..\packages\Microsoft.CodeDom.Providers.DotNetCompilerPlatform.1.0.0\build\Microsoft.CodeDom.Providers.DotNetCompilerPlatform.props')" />
///  <Import Project="..\packages\Microsoft.Net.Compilers.1.0.0\build\Microsoft.Net.Compilers.props" Condition="Exists('..\packages\Microsoft.Net.Compilers.1.0.0\build\Microsoft.Net.Compilers.props')" />-
///  ```
///  - Run the command ```Update-Package -reinstall -ProjectName Machinata.Product.XYZ``` in the package manager console (you might need to restart Visual Studio).
///  - Run a Project Find-and-Replace for ```ProductXYZ```, replacing with your own product name.
///  - Modify Web.config to include the proper product configurations. Make sure the database tables are setup with a own product table and make sure to create new encryption keys and salts.
///  - It is paramount to create a custom encryption keys and salts for your product: [https:///nerves.ch/admin/system/tools/key-generator]
///    - EncryptionCryptKeyString32
///    - EncryptionAuthKeyString32
///    - EncryptionHashSalt1
///    - EncryptionHashSalt2
///  - Under Project Settings > Build Events, add the following pre-build event: ```echo "BuildTime=%DATE% %TIME%;BuildUser=%USERNAME%;BuildMachine=%COMPUTERNAME%" > $(ProjectDir)\Config\$(ProjectName).timestamp```. This is used to identify the exact build.
///  - Build the Project, then locate the newly created ```Machinata.Product.ProductXYZ.timestamp``` file in the ```Config``` directory and Right-Click > Include in Project. Then under it's properties, set 'Copy to Output Directory' to 'Copy if newer'.

///## Adding New Modules
///  - Create a new class library called ```Machinata.Module.XYZ```. 
///  - Make sure the.net version is 4.6.1.
///  - Remove all ApplicationInsights packages (start with the most specific)
///  - Remove reference ```Microsoft.CodeDom.Providers.DotNetCompilerPlatform```
///  - Remove reference ```Microsoft.CSharp```
///  - Add a packages.config file with following: 

/// ```xml
/// <?xml version="1.0" encoding="utf-8"?>
/// <packages>
///  <package id="EntityFramework" version= "6.1.3" targetFramework= "net461" />
///  <package id="MySql.Data" version= "6.9.9" targetFramework= "net461" />
///  <package id="MySql.Data.Entity" version= "6.9.9" targetFramework= "net461" />
///  <package id="NLog" version= "4.4.7" targetFramework= "net461" />
///  <package id="Newtonsoft.Json" version= "9.0.1" targetFramework= "net461" />
/// </packages >
/// ```

///  - Run the command ```Update-Package -reinstall -ProjectName Machinata.Module.XYZ``` in the package manager console (you might need to restart Visual Studio).
///  - Add a project refence to ```Machinata.Core``` and any other modules you need.
///  - Change AssemblyInfo.cs version to the following (ensures that bundle caches are properly versioned across all libraries):

///```c#
///[assembly: AssemblyVersion("1.0.*")]
//////[assembly: AssemblyFileVersion("1.0.0.0")]
///```
///  - In VS 2019: edit the.csproj and change the following entry to false
///```xml
///<Deterministic>false</Deterministic>
///```


///  - Add a root folder called ```Config``` with the following files:
///  - Machinata.Module.XYZ.config:

///```xml
///<?xml version="1.0" encoding="utf-8"?>
///<configuration>
///  <appSettings>
///    <!-- SSL Settings -->
///    <add key = "YourModuleConfig" value= "xyz" />

/// </ appSettings >
///</ configuration >
///```

///  - Config.cs:

///```c#
///using System;
///using System.Collections.Generic;
///using System.Linq;
///using System.Text;
///using System.Threading.Tasks;

///namespace Machinata.Module.XYZ {
///    public class Config {
///        public static int YourModuleConfig = Core.Config.GetIntSetting("YourModuleConfig", 0);
///    }
///}
///```
///  - Under Project Settings > Build Events, add the following pre-build event: ```echo "BuildTime=%DATE% %TIME%;BuildUser=%USERNAME%;BuildMachine=%COMPUTERNAME%" > $(ProjectDir)\Config\$(ProjectName).timestamp```. This is used to identify the exact build.
///  - Build the Project, then locate the newly created ```Machinata.Module.ProductXYZ.timestamp``` file in the ```Config``` directory and Right-Click > Include in Project. Then under it's properties, set 'Copy to Output Directory' to 'Copy if newer'.



///## Running Products in IIS

///  - Running a product is best when using a own domain. You can register a new domain at https:///10.0.0.1:10000/ (iron) under Server > BIND DNS Server > office.nerves.ch...
///  - Furthermore, there are some required config settings that are not shipped by default:
///    - Environment
///    - AdminEmail
///  - Some additional recommended local configs could be:
///    - TestEmail
///    - SecurityForceHTTPS
///  - To use a common network file share like 'FileStorageProviderPath' 
///    - Map the share to a network drive: e.g. '\\\\10.0.0.1\DevData\'
///    - In 'Application Pools' under 'Advanced Settings' 'Identity' set your local user as 'custom account'
///    - In 'Application Settings' set the 'FileStorageProviderPath-NervesTest' to the desired path
///  - In IIS, make sure under 'Compression' settings 'Enable static content compression' is disabled, as this interferes with Machinata's own compression and streaming support.
///  - If the website has tasks that need to run regularly, set the website's Application Pool ```Idle Time-out (minutes)``` to zero in the application pool advanced settings.
///  - To access a common FileStorage define the config setting: FileStorageProviderPath 
///    - for incstance a network path ```FileStorageProviderPath-NervesTest=\\10.0.0.1\DevData\MachinataStorage\{domain}``` 
///  - To give the same access rights to the IIS like to local user has. (e.g. needed to access a network drive), go to the 'Application Pools' select the Application Pool open 'Advanced Settings' and set the desired user under 'Idenity'


///## Running Products with XSP Mono Server

///  - To run a product with XSP, just use the default settings and update a local config (Web.Local.config) as follows:

///```xml
///<?xml version="1.0" encoding="utf-8"?>
///<configuration>
///  <appSettings>
///        <add key="Environment" value="NervesTest" />
///        <add key="SecurityForceHTTPS" value="false" />
///        <add key="AdminEmail" value="youremail@domain.com" />
///        <add key="TestEmail" value="youremail@domain.com" />
///  </appSettings>
///</configuration>
///```


///## Deploying Products to remote IIS

///  - On the server, install Web/Management Service from the Windows Server Rols & Services installer
///  - On the server, install Web Deploy 3.6 (without dependencies or SQL Server addons)
///  - In ISS Management on remote machine, make sure the server under 'Management' has 'Management Service' and is configured to allow remote updates
///  - In Visual Studio, right-click product, select 'Publish', then 'Custom' with Publish Method 'Web Deploy'. Destination URL can be left blank. Make sure the name of the publish profile reflects both the target server and website, for example ```Hydrogen events.nerves.ch```.
///  - Press Deploy

///Note: Make sure to always deploy either the master branch or the specific product branch (ie do not deploy a module or feature branch). For example,
///when deploying yourwebsite.com, use the product/yourwebsite branch.

///Note: If encountering strange errors after a successful deploy regarding connection strings, make sure the update of connections strings is disabled in the deply config file:

///```
///<ItemGroup>
///    <MSDeployParameterValue Include="$(DeployParameterPrefix)Machinata.Core.Model.ModelContext-Web.config Connection String" >
///      <UpdateDestWebConfig>False</UpdateDestWebConfig>
///    </MSDeployParameterValue>
///    <MSDeployParameterValue Include="$(DeployParameterPrefix)Machinata.Core.Model.ModelHistory-Web.config Connection String" >
///      <UpdateDestWebConfig>False</UpdateDestWebConfig>
///    </MSDeployParameterValue>
///    <MSDeployParameterValue Include="$(DeployParameterPrefix)SQLConnection-NervesLive-Web.config Connection String" >
///      <UpdateDestWebConfig>False</UpdateDestWebConfig>
///    </MSDeployParameterValue>
///    <MSDeployParameterValue Include="$(DeployParameterPrefix)SQLConnection-NervesTest-Web.config Connection String" >
///      <UpdateDestWebConfig>False</UpdateDestWebConfig>
///    </MSDeployParameterValue>
///  </ItemGroup>
///```


///### Let's Encrypt / HTTPS - Certificates Generation

///#### Add new Certificate
///  1. Run the letsencrypt script
///  2. Menu: select "N: Create new certificate"
///  3. Which kind of certificate would you like to create?: "1: Single binding of an IIS site"
///  4. Choose site: "xx: Website to add Certificate"
///  5. "Do you want to replace the existing task? (y/n): -> "n"


///### No Database Connection Strings in Publish Profiles
///  - Add Machinata.Product.XXX.wpp.targets to root project dir with the following content:

///```xml
///<?xml version="1.0" encoding="utf-8"?>
///<Project xmlns="http:///schemas.microsoft.com/developer/msbuild/2003"
///         ToolsVersion="4.0">
///  <PropertyGroup>
///    <AutoParameterizationWebConfigConnectionStrings>false</AutoParameterizationWebConfigConnectionStrings>
///  </PropertyGroup>
///</Project>
///```

///## Project Structure

///Each project (usually called a product) has the following structure:

///- Machinata.Product.XYZ
///  - Config: Product specific configuration
///  - Handler
///    - Page: Page handlers for handling routes for web pages
///    - API: API handlers for handling routes for APIs
///  - Localization: localization files for using {text.xyz} variables
///  - Model: all model objects (entities) for the product
///  - Static:
///    - css: all CSS bundles
///    - images: static images
///    - js: all JS (or typescript) bundles
///    - themes: all theme configuration files
///  - Templates:
///    - Machinata.Module.XYZ: module specific templates
///    - Machinata.Product.XYZ: product specific templates

///## Entities

///Machinata Core ships with a entity system and datastore. By default, entities are stored on a MySQL database via the .Net EntityFramework.

///All Machinata entities inherit from ModelObject. Every entity has an database ```Id`` and ```Created``` timestamp at bare minimum.

///## Routes

///Routes provide access to different views, files or APIs for your projects. In the admin backend one
///can view a interactive tree of all the active routes.

///The following practice is standardized:

///### /api/*

///This is where all the API calls should be organized under.

///### /admin/*

///All of the admin backend tools should be organized under this.

///### /static/*

///Serving static files (files provided with the binary) hould be organized under this.

///### /content/*

///For all content related files that are stored in the Data center

///### Views, API's and Admin Routes

///The following best practice is defined, and provides a universal method of ensuring that namespaces are very clean,
///both for the frontend and backend, as well as for the API, taking into account the programmers needs as well as SEO needs.

///#### Entity
///  - Artist

///#### API's 
///  - ```/api/artists/create```
///  - ```/api/artists/list```
///  - ```/api/artists/artist/{publicId}/edit```
///  - ```/api/artists/artist/{publicId}/delete```
///  - ```/api/artists/artist/{publicId}/custom-method```

///#### Admin Views 
///  - ```/admin/artists``` (list)
///  - ```/admin/artists/artist/{publicId}``` (view)
///  - ```/admin/artists/artist/{publicId}/edit``` (edit)

///#### Frontend Views 
///  - ```/artists``` (list)
///  - ```/artists/by/style``` (list grouped by style)
///  - ```/artists/by/country``` (list grouped by country)
///  - ```/artists/artist/{artistName}``` (view)

///### Binding Routes

///All routes are served by a request handler (Handler.cs). Every request handler can be customized
///in any way. 

///Machinata.Core provides the most common, including:
///  - APIHandler
///  - PageTemplateHandler
///  - TextHandler
///  - StaticFileHandler
///  - StaticDirectoryHandler

///In addition, Machinata.Core also provides some useful handlers:
///  - AccountAPIHandler
///  - CRUDAPIHandler
///  - HeartbeatHandler
///  - DebuggingHandler
///  - ContentFileHandler
///  - BarCodeFileHandler
///  - CaptchaFileHandler
///  - QRCodeFileHandler

///To bind a route to a handler, create a class that inherits from Handler.cs and add a method with
///the attribute ```[RequestHandler]``` with its path defined:

///```
///[RequestHandler("/jobs")]
///public void Jobs() {
///    ...
///}
///```

///Parameters are automatically parsed and registered:

///```
///[RequestHandler("/jobs/{jobID}")]
///public void Jobs(string jobID) {
///    ...
///}
///```
///By default, the access policy ARN is the same as the route path, which means if the route
///is ```/jobs```, the required ARN for access will also be ```/jobs```. This can be customized
///using additional parameters:

///```
///[RequestHandler("/jobs", AccessPolicy.PUBLIC_ARN)]
///public void Default() {
///    ...
///}
///```

///Additionally, through the use of ```[AliasRequestHandler]```, a route alias can be added.
///A route alias is useful if you want to bind completely different routes to the same handler,
///but want to make sure that, for example, templating systems can still find the original route.
///This, for example, is very useful, because it allows the PageTemplateHandler to know that
///it should try to find the master route templates, instead of the translated templates.

///```
///[RequestHandler("/jobs", AccessPolicy.PUBLIC_ARN, "en")]
///[AliasRequestHandler("/de/jobs", AccessPolicy.PUBLIC_ARN, "de")]
///public void Default() {
///    ...
///}
///```

///If your project uses translated routes (through the use of ```routes.txt```), you can use the
///```[MultiRouteRequestHandler]```, which will automatically explode the routes into all the
///registered language versions:


///routes.txt:
///```
///routes.jobs.en=/jobs
///routes.jobs.de=/de/jobs
///```

///Handler:
///```
///[MultiRouteRequestHandler("routes.jobs",AccessPolicy.PUBLIC_ARN)]
///public void Default() {
///    ...
///}
///```

///A route handler can also be appended with multiple features, which any part of the system
///can use to cleverly compile data, cross-reference, and more:

///```
///[MultiRouteRequestHandler("routes.jobs",AccessPolicy.PUBLIC_ARN)]
///[SitemapRoute]
///[CMSPath("Jobs")]
///public void Default() {
///    ...
///}
///```

/// </summary>
namespace Machinata {
   
}


/// <summary>
/// TODO migrate from README.md
/// </summary>
namespace Machinata.Core {
   

}
