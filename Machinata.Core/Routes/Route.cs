using Machinata.Core.Builder;
using Machinata.Core.Handler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

/// <summary>
/// 
/// </summary>
namespace Machinata.Core.Routes {

    /// <summary>
    /// A class that represents a single route to a request handler.
    /// Routes are automatically loaded on application startup for all request handler methods with the
    /// RequestHandler attribute.
    /// </summary>
    public class Route : Model.ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Immutable Declarations

        /// <summary>
        /// The master list that maps routes to a request handler function (tupled with the http verb)
        /// This list is sorted using the longest (more specific) routes first, which allows for longer routes to take precedence over
        /// shorter ones (less specific) (using DescendingRoutePriorityComparer)
        /// </summary>
        private static SortedDictionary<string, Dictionary<Verbs,Route>> _loadedRoutes = new  SortedDictionary<string, Dictionary<Verbs,Route>>(new DescendingRoutePriorityComparer());

        /// <summary>
        /// The root route (/), automatically assigned when calling RegisterRoute
        /// </summary>
        private static Route _root;

        /// <summary>
        /// A flat list of all the routes loaded.
        /// </summary>
        private static List<Route> _allRoutes = new List<Route>();

        private static Dictionary<string,Route> _allSkipValidationRoutes = new Dictionary<string,Route>();

        public static readonly Regex RouteParamRegex = new Regex(@"\{[^\/]*\}", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        public static readonly Regex RouteOptionalParamRegex = new Regex(@"\{[^\/]*\?\}", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        public const string RegexRouteReplace = "(.*)";

        public const string RouteLanguageKeyName = "route_lang";

        public const string ALIAS_FOR_FIRST_ROUTE = "{alias-for-first-route}";

        #endregion

        #region Public Properties

        /// <summary>
        /// The sorting key for the route which is generted using GetKeyForRoutePath()
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        public string Key { get; set; }

        /// <summary>
        /// The declared route path.
        /// </summary>
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        public string Path { get; set; }

        /// <summary>
        /// The route HTTP verb.
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        public Verbs Verb { get; set; }

        /// <summary>
        /// The method information for the route. You can use this call the method for the route.
        /// </summary>
        public MethodInfo MethodInfo { get; set; }

        /// <summary>
        /// The handler factory method for the route. You can use this to create a handler for the route.
        /// </summary>
        public Func<object> HandlerFactory { get; set; }

        /// <summary>
        /// The assigned language for the route (if any).
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        public string Language { get; set; }

        /// <summary>
        /// If set, indicates that the route is a alias for antoher route.
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        public string AliasFor { get; set; }

        /// <summary>
        /// The method name for the route.
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        public string MethodName { get; set; }

        /// <summary>
        /// The handler name for the route.
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        public string HandlerName { get; set; }

        /// <summary>
        /// The last path segment for the route.
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        public string LastPathSegment { get; set; }

        /// <summary>
        /// The depth of the route path (ie how many segments).
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        public int Depth { get; set; }

        /// <summary>
        /// The physical children routes of the route.
        /// Physical means that the represented tree may skip nodes if those routes do not exist.
        /// For example, if the route "/api/whatever" exists but the route "/api" does not exist,
        /// then the root route "/" will have the child "/api/whatever" but not "/api". 
        /// </summary>
        public List<Route> Children;

        /// <summary>
        /// The physical parent route of the route.
        /// Physical means that the represented tree may skip nodes if those routes do not exist.
        /// For example, if the route "/api/whatever" exists but the route "/api" does not exist,
        /// then the root route "/" will have the child "/api/whatever" but not "/api". 
        /// </summary>
        public Route Parent { get; set; }

        /// <summary>
        /// The trimmed path of the route where the route path does not start or end with a slash.
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        public string TrimmedPath { get; set; }

        /// <summary>
        /// The path segments of the route. Note this uses the trimmed path.
        /// </summary>
        public List<string> PathSegs { get; set; }
        
        /// <summary>
        /// A universal identifier for the route that combines the path, language and verb.
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        public string UID { get; set; }
        
        /// <summary>
        /// If true, then this route is not checked for standard request validation from .net (ie no SQL injection et cetera).
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        public bool SkipRequestValidation { get; set; }

        #endregion

        #region Constructors

        public Route(string path, Verbs verb, string aliasFor, string language) {
            // Set properties
            this.Key = Route.GetKeyForRoutePath(path);
            this.Path = path;
            this.Verb = verb;
            this.Language = language;
            this.AliasFor = aliasFor;
            this.Children = new List<Route>();
            // Derived properties
            this.TrimmedPath = this.Path.Trim('/');
            this.PathSegs = this.TrimmedPath.Split('/').ToList();
            this.Depth = this.PathSegs.Count;
            if (this.Path == "/") this.Depth = 0;
            this.LastPathSegment = this.PathSegs.Last();
            this.UID = this.Verb + ":" + this.Path;
            if (this.Language != null) this.UID += ":" + this.Language;
        }
        
        public static Route CreateRouteForAttribute(RequestHandlerAttribute attribute, Func<object> handlerFactory, MethodInfo method) {
            // Init
            var route = new Route(attribute.Path, attribute.Verb, attribute.AliasFor, attribute.Language);
            // Set method
            route.MethodInfo = method;
            route.MethodName = method.Name;
            // Set handler
            route.HandlerFactory = handlerFactory;
            route.HandlerName = method.DeclaringType.Name;
            // Misc
            route.SkipRequestValidation = attribute.SkipRequestValidation;
            // Return
            return route;
        }

        /// <summary>
        /// Creates the routes for multi-route attribute. This bascially is the same as a regular request attribute, except
        /// that it explodes using all the translations...
        /// </summary>
        /// <param name="attribute">The attribute.</param>
        /// <param name="handlerFactory">The handler factory.</param>
        /// <param name="method">The method.</param>
        /// <returns></returns>
        public static List<Route> CreateRoutesForMultiRouteAttribute(MultiRouteRequestHandlerAttribute attribute, Func<object> handlerFactory, MethodInfo method) {
            // Init
            var ret = new List<Route>();
            // Find all the translations...
            var texts = Core.Localization.Text.RegisteredTexts().Where(t => t.Id == attribute.Path);
            string firstRoutePath = null;
            if (texts.Count() == 0) _logger.Error($"No route text id's found for {attribute.Path}!");
            foreach (var text in texts) {
                var route = new Route(text.Value, attribute.Verb, attribute.AliasFor, text.Language);
                // Set method
                route.MethodInfo = method;
                route.MethodName = method.Name;
                // Set handler
                route.HandlerFactory = handlerFactory;
                route.HandlerName = method.DeclaringType.Name;
                // Set the alias to self if it is not set
                if (firstRoutePath != null) {
                    route.AliasFor = firstRoutePath;
                } else {
                    firstRoutePath = route.Path;
                }
                // Register
                ret.Add(route);
            }
            // Return
            return ret;
        }

        #endregion

        #region Private Helper Methods
        
        private static void _addRouteToRouteDictionary(Route route, SortedDictionary<string, Dictionary<Verbs, Route>> dictionary) {
            var key = route.Path;
            if (dictionary.ContainsKey(key) == false) {
                dictionary.Add(key, new Dictionary<Verbs, Route>()); // create
            }
            if (dictionary[key].ContainsKey(route.Verb)) {
                //TODO: show warning? exceptions?
                dictionary[key][route.Verb] = route; // update
            } else {
                dictionary[key].Add(route.Verb, route); // add
            }
        }

        private static Route _getClosestParentRoute(Route route) {
            if (route.Path == "/") return null;
            for(int i = route.PathSegs.Count-1; i > 0; i--) {
                var currentPath = string.Join("/",route.PathSegs.Take(i));
                foreach (var r in _allRoutes) {
                    if (r.TrimmedPath == currentPath) {
                        return r;
                    }
                }
            }
            return _root;
        }

        #endregion

        #region Public Helper Methods

        /// <summary>
        /// Gets the route path or alias for, if set.
        /// </summary>
        /// <returns></returns>
        public string GetRoutePathOrAliasIfSet() {
            if (this.AliasFor != null) return this.AliasFor;
            else return this.Path;
        }

        public List<Route> GetAliases() {
            // Get the master route
            var master = this;
            if (this.AliasFor != null) master = Route.GetFirstRouteAtPath(this.AliasFor); // Sometimes the alias doesnt match a route for some specially crafted routes, such as error pages
            if (master == null) master = this;
            var routes = GetAllRoutes().Where(r => r.AliasFor == master.Path).ToList();
            if(!routes.Contains(master)) routes.Add(master);
            return routes;
        }

        /// <summary>
        /// Gets all routes loaded in the system.
        /// Note: the data is returned directly and modifications are permanant.
        /// </summary>
        /// <returns></returns>
        public static SortedDictionary<string, Dictionary<Verbs, Route>> GetRouteMapping() {
            return _loadedRoutes;
        }

        /// <summary>
        /// Gets the routes for given filter.
        /// Note: the data is returned directly and modifications are permanant.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        public static SortedDictionary<string, Dictionary<Verbs, Route>> GetRouteMappingForFilter(string filter) {
            var ret = new SortedDictionary<string, Dictionary<Verbs, Route>>();
            foreach(var key in _loadedRoutes.Keys) {
                foreach(var keyVal in _loadedRoutes[key]) {
                    var route = keyVal.Value;
                    if(route.Path.Contains(filter) || route.HandlerName.Contains(filter) || (route.AliasFor!=null && route.AliasFor.Contains(filter)) || route.MethodName.Contains(filter)) {
                        _addRouteToRouteDictionary(route, ret);
                    }
                }
            }
            return ret;
        }


        /// <summary>
        /// Gets all routes flattend into a single list.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        public static List<Route> GetAllRoutes(string filter = null) {
            var ret = new List<Route>();
            foreach(var route in _allRoutes) {
                if (filter != null) {
                    if (route.Path.Contains(filter) || route.HandlerName.Contains(filter) || (route.AliasFor != null && route.AliasFor.Contains(filter)) || route.MethodName.Contains(filter)) {
                        ret.Add(route);
                    }
                } else {
                    ret.Add(route);
                }
            }
            return ret;
        }

        /// <summary>
        /// Registers the route in the system.
        /// </summary>
        /// <param name="route">The route.</param>
        public static void RegisterRoute(Route route) {
            // Add to mapping
            _addRouteToRouteDictionary(route, _loadedRoutes);
            _allRoutes.Add(route);
            // Skip validation route?
            if(route.SkipRequestValidation) {
                _allSkipValidationRoutes[route.Path] = route;
            }
            // Register root
            if(route.Path == "/") {
                if (_root != null) _logger.Warn("Duplicate root route detected!");
                _root = route;
            }
        }
        
        /// <summary>
        /// Returns true if the given URL matches any route marked as SkipRequestValidation,
        /// otherwise returns false.
        /// </summary>
        public static bool IsURLSkipRequestValidationRoute(Uri url) {
            return _allSkipValidationRoutes.ContainsKey(url.AbsolutePath);
        }

        /// <summary>
        /// Rebuilds the physical route tree.
        /// Physical means that the represented tree may skip nodes if those routes do not exist.
        /// For example, if the route "/api/whatever" exists but the route "/api" does not exist,
        /// then the root route "/" will have the child "/api/whatever" but not "/api". 
        /// </summary>
        public static void RebuildRouteTree() {
            _logger.Debug("Rebuilding Route Tree...");
            foreach(var route in _allRoutes) {
                route.Children.Clear();
                route.Parent = null;
            }
            foreach(var route in _allRoutes) {
                Route parent = _getClosestParentRoute(route);
                if(parent != null) {
                    parent.Children.Add(route);
                    route.Parent = parent;
                }
            }
            _logger.Debug("Route Tree Rebuilt");
        }

        public static Route GetRootRoute() {
            return _root;
        }

        public static Route GetFirstRouteAtPath(string path) {
            return _allRoutes.Where(r => r.Path == path).FirstOrDefault(); 
        }

        public static Route GetFirstRouteAtTrimmedPath(string trimmedPath) {
            return _allRoutes.Where(r => r.TrimmedPath == trimmedPath).FirstOrDefault(); 
        }

        public static Route GetRouteForUID(string uid) {
            return _allRoutes.Where(r => r.UID == uid).SingleOrDefault(); 
        }

        public static IEnumerable<Route> GetRoutesForMethod(MethodInfo method) {
            return _allRoutes.Where(r => r.MethodInfo == method); 
        }
        
        /// <summary>
        /// Gets the sorting key (ie priority) for route.
        /// </summary>
        /// <param name="route">The route.</param>
        /// <returns></returns>
        public static string GetKeyForRoutePath(string route) {
            // Init
            var key = "";
            var routeTrimmed = route.Trim('/');
            var segs = routeTrimmed.Split('/');

            // Split into blocks of all hard/variable in a row
            var blocks = new List<List<string>>();
            var currentBlock = new List<string>();
            string lastSeg = null;
            for(var i = 0; i < segs.Length; i++) {
                // Init
                var seg = segs[i];
                // Are we changing?
                if(lastSeg != null) {
                    if(seg.Contains('{') != lastSeg.Contains('{')) {
                        blocks.Add(currentBlock);
                        currentBlock = new List<string>();
                    }
                }
                // Register
                currentBlock.Add(seg);
                lastSeg = seg;
            }
            blocks.Add(currentBlock);
            // Compile the key for each block
            foreach(var block in blocks) {
                var nSegsWithVars = block.Where(s => s.Contains('{')).Count();
                var nSegsHard = block.Count() - nSegsWithVars;
                key += "H" + nSegsHard.ToString("#00");
                key += "V" + nSegsWithVars.ToString("#00");
                //key += "L" + block.Sum(s => s.Length).ToString("#00");
                key += ".";
            }
            key += "_"+route;
            return key;
        }

        #endregion

    }

    #region Descending Route Priority Comparer

    /// <summary>
    /// Comapares (in descending order) the route priority through the use
    /// of multiple signals.
    /// This should be used when getting a route priority.
    /// </summary>
    /// <seealso cref="System.Collections.Generic.IComparer{System.String}" />
    public class DescendingRoutePriorityComparer : IComparer<string>{
        public int Compare(string x, string y) {
            return string.Compare(Route.GetKeyForRoutePath(y), Route.GetKeyForRoutePath(x), false); 
        }
    }

    /// <summary>
    /// Comapares (in asccending order) the route priority through the use
    /// of multiple signals.
    /// </summary>
    /// <seealso cref="System.Collections.Generic.IComparer{System.String}" />
    public class AscendingRoutePriorityComparer : IComparer<string>{
        public int Compare(string x, string y) {
            return string.Compare(Route.GetKeyForRoutePath(x), Route.GetKeyForRoutePath(y), false); 
        }
    }

    #endregion
}
