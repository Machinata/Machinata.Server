
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.PDF {
    public class PDFConverter {
        
        /// <summary>
        /// Saves the PDF as a image.
        /// </summary>
        /// <param name="pdfFile">The PDF file.</param>
        /// <param name="imageFile">The image file.</param>
        /// <param name="dpi">The dpi.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Only PNG is supported.</exception>
        public static string SavePDFAsImage(string pdfFile, string imageFile, int dpi = 300, int pageNumber = 1) {
            // Get a mutex on the imagefile
            string ret = null;
            var mut = Core.Util.Mutex.CreateKeyedMutexForFilepath(imageFile);
            try {   
                // Wait until it is safe to enter.
                mut.Wait();

                // Do work
                ret = _savePDFAsImageUsingGhostscriptEXE(pdfFile, imageFile, dpi, pageNumber);

            } finally {
                // Release the Mutex.
                mut.Release();
            }
            return ret;
        }

        public static string CreatePDFFromImage(string imageFile, string outputFile) {
            var img = iTextSharp.text.ImgWMF.GetInstance(imageFile);

            var document = new iTextSharp.text.Document(new iTextSharp.text.Rectangle(img.Width,img.Height));
            var stream = new FileStream(outputFile, FileMode.Create);
            var writer = iTextSharp.text.pdf.PdfWriter.GetInstance(document, stream);
            document.Open();

            img.SetAbsolutePosition(0, 0);
            document.Add(img);
            
            // Close and return
            document.Close();
            stream.Close();
            

            return outputFile;
        }

        #region Ghostscript.Net implemenation (deprecated)
        /*
        private static string _savePDFAsImageUsingGhostscriptNet(string pdfFile, string imageFile, int dpi = 300, int pageNumber = 1) {
            
            // Validate
            if(!imageFile.EndsWith(".png")) throw new Exception("Only PNG is supported.");
            
            // Get ghostscript dll
            var dllName = Environment.Is64BitProcess ? "gsdll64.dll" : "gsdll32.dll";
            var dllPath = Core.Config.BinPath + Core.Config.PathSep + "libs" + Core.Config.PathSep + dllName;
            var dllVer = new GhostscriptVersionInfo(new Version(0, 0, 0), dllPath, string.Empty, GhostscriptLicense.GPL);

            bool USE_RASTERIZER = false;

            if (USE_RASTERIZER) {

                // Use the built in rasterizier helper calss,
                using (var rasterizer = new GhostscriptRasterizer()) {
                    var loadFromMemory = true; // When true, we can use multi-threading, however looking at source this just loads all the bytes of dll on every Open call (see https://github.com/jhabjan/Ghostscript.NET/blob/master/Ghostscript.NET/GhostscriptLibrary.cs)
                    rasterizer.Open(pdfFile, dllVer, loadFromMemory);
                    using (var img = rasterizer.GetPage(dpi, dpi, pageNumber)) {
                        img.Save(imageFile, System.Drawing.Imaging.ImageFormat.Png);
                    }
                }
                
            } else {

                // Call ghostscript manually

                // Allows only a single PDF at a time to be converted - is there any alternative
                lock (_PDF_CONVERTER_LOCK) {

                    using (GhostscriptProcessor ghostscript = new GhostscriptProcessor(dllVer)) {
                        //ghostscript.Processing += new GhostscriptProcessorProcessingEventHandler(ghostscript_Processing);

                        List<string> switches = new List<string>();
                        switches.Add("-empty");
                        switches.Add("-dSAFER");
                        switches.Add("-dBATCH");
                        switches.Add("-dNOPAUSE");
                        switches.Add("-dNOPROMPT");
                        //switches.Add("-dFirstPage=" + pageFrom.ToString());
                        //switches.Add("-dLastPage=" + pageTo.ToString());
                        switches.Add("-sDEVICE=png16m");
                        switches.Add($"-r{dpi}");
                        switches.Add("-dTextAlphaBits=4");
                        switches.Add("-dGraphicsAlphaBits=4");
                        switches.Add(@"-sOutputFile=" + imageFile);
                        switches.Add(@"-f");
                        switches.Add(pdfFile);

                        ghostscript.Process(switches.ToArray());
                    }

                }
            }

            return imageFile;
        }
        */
        #endregion
        
        #region Ghostscript .exe implemenation
        private static string _savePDFAsImageUsingGhostscriptEXE(string pdfFile, string imageFile, int dpi = 300, int pageNumber = 1) {
            
            // Validate
            if(!imageFile.EndsWith(".png")) throw new Exception("Only PNG is supported.");
            
            // Get ghostscript dll
            var exeName = Environment.Is64BitProcess ? "gswin64c.exe" : "gswin32c.exe";
            var exePath = Core.Config.BinPath + Core.Config.PathSep + "libs" + Core.Config.PathSep + exeName;
            
            // Call ghostscript manually...
            
            // Setup the process and arguments
            var process = new Process();
            process.StartInfo.FileName = exePath;
            List<string> switches = new List<string>();
            switches.Add("-empty");
            switches.Add("-dSAFER");
            switches.Add("-dBATCH");
            switches.Add("-dNOPAUSE");
            switches.Add("-dNOPROMPT");
            //switches.Add("-dFirstPage=" + pageFrom.ToString());
            //switches.Add("-dLastPage=" + pageTo.ToString());
            switches.Add("-sDEVICE=png16m");
            switches.Add($"-r{dpi}");
            switches.Add("-dTextAlphaBits=4");
            switches.Add("-dGraphicsAlphaBits=4");
            switches.Add("-sOutputFile=" + imageFile);
            switches.Add("-f");
            switches.Add(pdfFile);
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.Arguments = string.Join(" ", switches);

            // Execute
            process.Start();
            process.WaitForExit();
            if(process.ExitCode != 0) {
                var msg = process.StandardOutput.ReadToEnd() + process.StandardError.ReadToEnd();
                throw new Exception($"Ghostscript process did not exit successfully: Exit Code={process.ExitCode}: {msg}");
            }
            
            return imageFile;
        }
        #endregion

    }
}
