﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Machinata.Core.Threading {
    public class KeyedMutex {

        private const int MAX_THREAD_PER_MUTEX = 1;

        private KeyedMutexStore _store;
        private string _key;
        private SemaphoreSlim _mutex = new SemaphoreSlim(MAX_THREAD_PER_MUTEX, MAX_THREAD_PER_MUTEX);

        public KeyedMutex(KeyedMutexStore store, string key) {
            _store = store;
            _key = key;
            _mutex = new SemaphoreSlim(1, 1);
        }

        public string Key {
            get {
                return _key;
            }
        }

        public int Awaiting {
            get {
                return MAX_THREAD_PER_MUTEX-_mutex.CurrentCount; // current count indicates how many slots available are there...
            }
        }

        public void Wait() {
            _mutex.Wait();
        }

        public void Release() {
            // Are we the last one?
            if (this.Awaiting == 1) {
                // We only release after we remove from the store table,
                // thus it might be that a thread comes in next line and creates a new 
                // lock entry just after removing from store but before releasing, however
                // this is absolutely okay, because we have already called to release the resource...
                _store.RemoveMutexByKey(this.Key);
            }
            _mutex.Release();
        }
    }
}
