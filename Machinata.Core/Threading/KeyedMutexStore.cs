﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Threading {
    public class KeyedMutexStore {

        private ConcurrentDictionary<string, KeyedMutex> _store;

        public KeyedMutexStore() {
            _store = new ConcurrentDictionary<string, KeyedMutex>();
        }

        public KeyedMutex GetMutexForKey(string key) {
            var mutex = _store.GetOrAdd(key, (k) => {
                return new KeyedMutex(this,k);
            });
            return mutex;
        }

        public List<string> GetKeys() {
            return _store.Keys.ToList();
        }

        public List<KeyedMutex> GetMutexes() {
            return _store.Values.ToList();
        }

        public void RemoveMutexByKey(string key) {
            _store.TryRemove(key, out _);
        }

    }
}
