using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using System.ComponentModel;
using Machinata.Core.Util;
using Machinata.Core.Exceptions;
using System.Data.Entity.Infrastructure;
using Newtonsoft.Json.Linq;
using System.Globalization;
using Amazon.S3.Model;
using DocumentFormat.OpenXml.Office2010.PowerPoint;
using Newtonsoft.Json;
using System.Collections;
using System.CodeDom;
using Machinata.Core.Reflection;

namespace Machinata.Core.Model {

    /// <summary>
    /// 
    /// See
    /// https://msdn.microsoft.com/en-us/library/jj591583(v=vs.113).aspx
    /// </summary>
    public abstract class ModelObject : IComparable<ModelObject> {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constants //////////////////////////////////////////////////////////////////////

        public const int MODELOBJECT_NO_ID = 0;
        public const int MODELOBJECT_NO_TYPENUMBER = 0;

        #endregion

        #region Enums //////////////////////////////////////////////////////////////////////

        public enum SerializationType {
            Default,
            Consistent,
            LowerCamelCase
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public ModelObject() {
            // Register created date
            this.Created = DateTime.UtcNow;
        }

        #endregion

        #region Private  Properties ///////////////////////////////////////////////////////////////
        
        [NotMapped]
        private bool? _isStoredInDB = null;

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order=1)]
        [FormBuilder]
        [FormBuilder(Forms.System.LISTING)]
        public int Id { get; set; }
        
        [Column]
        [Required]
        [FormBuilder]
        public DateTime Created { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////


        [NotMapped]
        //[NonSerialized]
        public ModelContext Context { get; set; }
        
        /// <summary>
        /// Returns a bool indicated if this object is or can be stored in the DB.
        /// This is defined by the implementing class type having the class attribute
        /// 'ModelClass'. This method can be used to ensure that entity framework
        /// routines aren't run on classes for which entity framework does not know about.
        /// </summary>
        [NotMapped]
        //[NonSerialized]
        public bool IsStoredInDB {
            get {
                if(_isStoredInDB == null) {
                    // Is it a DB object type?
                    var modelClassAttribute = this.GetType().GetCustomAttribute<ModelClassAttribute>();
                    _isStoredInDB = (modelClassAttribute != null);
                }
                return _isStoredInDB.Value;
            }
        }

        /// <summary>
        /// To use a ModelClass without db context
        /// </summary>
        /// <param name="inDB">if set to <c>true</c> [in database].</param>
        public void SetStoredInDB(bool inDB)
        {
            _isStoredInDB = inDB;
        }

        /// <summary>
        /// Gets the public identifier for the enitity in the entity set.
        /// This property is directly derived from the database Id using the 
        /// default obfuscator.
        /// </summary>
        /// <value>
        /// The public identifier.
        /// </value>
        [FormBuilder]
        [NotMapped]
        [FormBuilder(Forms.System.LISTING)]
        [FormBuilder(Forms.API.LISTING)]
        public virtual string PublicId {
            get {
                if (this.Id == 0) return null; // Not yet set by DB (db ids start at 1)
                return Core.Ids.Obfuscator.Default.ObfuscateId(this.Id);
            }
        }
        
        [FormBuilder]
        [NotMapped]
        public string UniversalId {
            get {
                return Core.Config.ProjectID+"-"+Core.Config.Environment+"-"+ this.ActualType+"-"+PublicId;
            }
        }

        /// <summary>
        /// Gets a rolling id that is suitable for use such as a invoice number or order number.
        /// Note that usually rolling Ids are quite easy to guess (since they are only minutely spaced in the Integer number space).
        /// Uses the default Id Roller Core.Ids.Roller.Default
        /// </summary>
        [FormBuilder]
        [NotMapped]
        public int RollingId {
            get {
                // Validate id and compute rolling id number
                if (this.Id == MODELOBJECT_NO_ID) return MODELOBJECT_NO_ID;// throw new Exception("The entity does not yet have a id assigned for a rolling id.");
                int rollingId = Core.Ids.Roller.Default.RollId(this.Id);
                return rollingId;
            }
        }

        /// <summary>
        /// Gets a 'spreaded' id that is suitable for use in exchange for a public id that is easier to read.
        /// Typically this is a DB id (ModelObject.Id) that is mapped to the Int32 number space randomly, with a very even distribution.
        /// Typically uses the same system as PublicId (Core.Ids.Obfuscator.Default)
        /// Uses the default Id Spreader Core.Ids.Spreader.Default
        /// </summary>
        [FormBuilder]
        [NotMapped]
        public int SpreadId {
            get {
                // Validate id and compute obfuscted id number
                if (this.Id == MODELOBJECT_NO_ID) return MODELOBJECT_NO_ID; // Not yet set by DB (db ids start at 1)
                return Core.Ids.Spreader.Default.SpreadId(this.Id);
            }
        }

        /// <summary>
        /// Updates the serial id for this model object using the default SerialIdProvider Core.Ids.SerialIdGenerator.Default.
        /// Note: This method requires the model object to have a string property called SerialId or else it will fail.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.Exception">This entity does not have a property SerialId</exception>
        public string UpdateSerialId() {
            // Compute serial id
            var serialId = Core.Ids.SerialIdGenerator.Default.GetSerialIdForEntity(this);
            // Get property and set value
            var prop = this.GetType().GetProperty("SerialId");
            if (prop == null) throw new Exception("This entity does not have a property SerialId");
            prop.SetValue(this, serialId);
            return serialId;
        }

        /// <summary>
        /// Called when a model object is added to the context.
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        [OnModelObjectAddedToDB]
        public static bool OnModelObjectAddedToDB(ModelContext db, ModelObject entity) {
            var serialIdProp = entity.GetType().GetProperty("SerialId");
            if(serialIdProp != null) {
                // Update the serial id
                var sid = entity.UpdateSerialId();
                _logger.Debug($"Automatically set serial id for entity {entity.TypeName} {entity.Id} to '{sid}'");
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets the type number for the object type. This is used by RollingIDs, which indicate
        /// the type of object one is dealing with purely in a numerical fashion.
        /// If this virtual method is not implemented by the subclass, the rolling id cannot be used
        /// and will return null.
        /// </summary>
        [NotMapped]
        public virtual int TypeNumber { get { return MODELOBJECT_NO_TYPENUMBER; } }
        
        /// <summary>
        /// Gets the actual type. This will return the proxy type if the entity is proxied.
        /// </summary>
        /// <value>
        /// The actual type.
        /// </value>
        [NotMapped]
        public string ActualType {
            get {
                return this.GetType().Name;
            }
        }

        /// <summary>
        /// Gets the name of the type automatically unwrapping any proxied entities.
        /// </summary>
        /// <value>
        /// The name of the type.
        /// </value>
        [FormBuilder]
        [NotMapped]
        [FormBuilder(Forms.System.LISTING)]
        public string TypeName {
            get {
                // http://stackoverflow.com/questions/16004707/unexpected-gettype-result-for-entity-entry
                var actualType = System.Data.Entity.Core.Objects.ObjectContext.GetObjectType(this.GetType());
                return actualType.Name; 
            }
        }

       
        #endregion

        #region Conversion Methods ////////////////////////////////////////////////////////////////

        public override string ToString() {
            return this.ActualType + "_" + this.Id;
        }

        #endregion

        #region Public Helper Methods /////////////////////////////////////////////////////////////
        
        /// <summary>
        /// Includes the specified property by making sure it is loaded from the DB.
        /// This is a shortcut to using the generic method, if the return type is not needed.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <returns></returns>
        public ModelObject Include(string property) {
            return Include<ModelObject>(property);
        }


        private HashSet<string> _IncludeExecuted = new HashSet<string>();

        /// <summary>
        /// Includes the specified property by making sure it is loaded from the DB,
        /// in a similar fashion to how entity framework supports Include on DbSets.
        /// The entity must be attached to a context for this to work.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property">The property.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Entity is not attached to context.</exception>
        public T Include<T>(string property) where T:ModelObject {
            // Don't do this if the class is not stored in db
            if (!this.IsStoredInDB) return (T)this;
            if (_IncludeExecuted.Contains(property)) return (T)this;

            var entityContext = this.Context;
            if (entityContext == null) throw new Exception($"The object {this} does not have a DB context attached to it.");
            var propEntry = entityContext.Entry(this);
            if (propEntry != null) {
                // Collection or property?
                if(this.GetProperties().Where(p => p.Name == property && p.PropertyType.ToString().Contains("ICollection")).Count() == 1) {
                    var propCollection = propEntry.Collection(property);
                    if (propCollection != null) propCollection.Load();
                } else {
                    var propReferernce = propEntry.Reference(property);
                    if (propReferernce != null) propReferernce.Load();
                }
                
            } else {
                throw new Exception("Entity is not attached to context.");
            }
            _IncludeExecuted.Add(property);
            return (T)this;
        }

        private bool _LoadFirstLevelObjectReferencesExecuted = false;

        /// <summary>
        /// Loads the first level references to all ModelObject properties.
        /// </summary>
        public void LoadFirstLevelObjectReferences() {
            // Don't do this if the class is not stored in db
            if (!this.IsStoredInDB) return;
            if (_LoadFirstLevelObjectReferencesExecuted == true) return;

            var propertiesOfModelObject = this.GetModelObjectProperties().Where(p => p.PropertyType.IsSubclassOf(typeof(ModelObject))).Where(p => p.CustomAttributes.Where(a => a.AttributeType == typeof(NotMappedAttribute)).Any() == false);
            foreach (var prop in propertiesOfModelObject) {
                var entityContext = this.Context;
                if (entityContext == null) throw new Exception($"The object {this} does not have a DB context attached to it.");
                var propEntry = entityContext.Entry(this);
                if (propEntry != null) {
                    var propReferernce = propEntry.Reference(prop.Name);
                    if (propReferernce != null) propReferernce.Load();
                } else {
                    throw new Exception("Entity is not attached to context.");
                }
            }
            _LoadFirstLevelObjectReferencesExecuted = true;
        }

        private bool _LoadFirstLevelNavigationReferencesExecuted = false;

        /// <summary>
        /// Loads the first level references to all ICollection<ModelObject> properties.
        /// </summary>
        public ModelObject LoadFirstLevelNavigationReferences() {
            // Don't do this if the class is not stored in db
            if (!this.IsStoredInDB) return this;
            if (_LoadFirstLevelNavigationReferencesExecuted == true) return this;

            //TODO: how can this be done better? I.e. the hard string...
            var propertiesOfModelObject = this.GetNavigationProperties();
            foreach (var prop in propertiesOfModelObject) {
                var entityContext = this.Context;
                if (entityContext == null) throw new Exception($"The object {this} does not have a DB context attached to it. Please make sure the entity first has a context before calling LoadFirstLevelNavigationReferences().");
                var propEntry = entityContext.Entry(this);
                if (propEntry != null) {
                    var propCollection = propEntry.Collection(prop.Name);
                    if (propCollection != null) propCollection.Load();
                } else {
                    throw new Exception("Entity is not attached to context.");
                }
            }
            _LoadFirstLevelNavigationReferencesExecuted = true;
            return this;
        }
        
        public List<PropertyInfo> GetProperties() {
            return this.GetType().GetProperties().ToList();
        }

        public List<PropertyInfo> GetNavigationProperties() {
            return this.GetProperties().Where(p => p.PropertyType.ToString().Contains("ICollection")).ToList();
        }

        public List<PropertyInfo> GetDBProperties() {
            // https://stackoverflow.com/questions/5851274/how-to-get-all-names-of-properties-in-an-entity
            return this.GetType().GetProperties(BindingFlags.DeclaredOnly | 
                                           BindingFlags.Public | 
                                           BindingFlags.Instance).ToList();
        }

        public List<PropertyInfo> GetModelObjectProperties() {
            return this.GetProperties().Where(p => p.PropertyType.IsSubclassOf(typeof(ModelObject))).ToList();;
        }

        public List<PropertyInfo> GetPropertiesWithAttribute(Type attributeType) {
            return this.GetProperties().Where(t => t.GetCustomAttributes(attributeType, inherit: true).Any()).ToList();
        }

        public static int GetIdForPublicId(string publicId) {
            return Core.Ids.Obfuscator.Default.UnobfuscateId(publicId);
        }

        public string GetObjectValueSummary(FormBuilder form, bool includePropertyNames = true) {
            var editedProperties = this.GetPropertiesForForm(form);
            if (includePropertyNames) {
                var keyValues = string.Join(", ", editedProperties.Select(p => p.Name + ": " + p.GetValue(this)));
                return keyValues;
            }
            else {
                var values = string.Join(", ", editedProperties.Select(p =>p.GetValue(this)));
                return values;
            }
        }


        public static string  GetObjectValueDiffSummary(IDictionary<string, string> mo1, IDictionary<string, string> mo2) {
            var diffs = DiffValues(mo1, mo2);
            var editedKeyValues = string.Join(", ", diffs.Select(p => p.Key + ": '" + p.Value.Item1 + "' -> '" + p.Value.Item2 + "'"));
            return editedKeyValues;
        }

        /// <summary>
        /// compiles the differences in the values.<PropertyName,<oldValue,newValue>>
        /// </summary>
        /// <param name="mo1">The mo1.</param>
        /// <param name="mo2">The mo2.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">The dictionaries don't have the same keys</exception>
        public static IDictionary<string, Tuple<string,string>> DiffValues(IDictionary<string, string> mo1, IDictionary<string, string>  mo2) {
            var diffs = new Dictionary<string, Tuple<string, string>>();

            if (!mo2.Keys.All(k=>mo1.ContainsKey(k)) || !mo1.Keys.All(t => mo2.ContainsKey(t))) {
                throw new Exception("The dictionaries don't have the same keys");
            }

            foreach (var k1 in mo1.Keys) {
                if ( string.IsNullOrEmpty(mo1[k1]?.ToString()) 
                     && string.IsNullOrEmpty(mo2[k1]?.ToString())) {
                    continue;
                }
                if (mo1[k1]?.ToString() != mo2[k1]?.ToString()) {
                    diffs[k1] = new Tuple<string, string>(mo1[k1], mo2[k1]);
                }
            }
            return diffs;
        }

        /// <summary>
        /// Gets the values as strings
        /// </summary>
        /// <param name="form">The form.</param>
        /// <returns></returns>
        public IDictionary<string, string> GetValues(FormBuilder form) {
            var properties = this.GetPropertiesForForm(form);
            var values = new Dictionary<string, string>();
            foreach (var property in properties) {
                var p1 = new EntityFormBuilderProperty(this, property, form);
                values[property.Name] = property.GetValue(this)?.ToString();
            }
            return values;
        }


        public static ModelObject GetByUniversalId(Model.ModelContext db, string universalId)  {

            // Format: Core.Config.ProjectID+"-"+Core.Config.Environment+"-"+ this.ActualType+"-"+PublicId;
            var segments = universalId.Split('-');
            if (segments.Length != 4) {
                throw new Exceptions.BackendException("invalid-universalid", "An invalid universalid was provided");
            }
            if (segments.ElementAt(0) != Core.Config.ProjectID) {
                throw new Exceptions.BackendException("invalid-universalid", "An invalid universalid was provided");
            }
            if (segments.ElementAt(1) != Core.Config.Environment) {
                throw new Exceptions.BackendException("invalid-universalid", "An invalid universalid was provided");
            }
            var elements = db.GetSetQueryableByName(segments.ElementAt(2));

            return elements.GetByPublicId<ModelObject>(segments.ElementAt(3));
        }

        /// <summary>
        ///  Language from the contexts handler if available
        /// </summary>
        public string ContextLanguage(string defaultLanguage = null) {
            var contextLanguage = this.Context?.Handler?.Language;
            if (contextLanguage == null && defaultLanguage != null) {
                return defaultLanguage;
            }
            return contextLanguage;
        }


        #endregion

        #region Comparer //////////////////////////////////////////////////////////////////


        public int CompareTo(ModelObject other) {
            var thisval = this.GetValueForDefaultOrdering();
            var otherval = other.GetValueForDefaultOrdering();
            if (thisval == null) thisval = "";
            if (otherval == null) otherval = "";
            if(thisval.GetType() == typeof(string)) {
                // String type
                return ((string)thisval).CompareTo( ((string)otherval) );
            } else if(thisval.GetType() == typeof(int)) { 
                // Int type
                return ((int)otherval) - ((int)thisval);
            } else {
                throw new NotImplementedException("ModelObject.GetValueForDefaultOrdering() must return either a string or int.");
            }
        }

        #endregion

        #region Public Virtual Methods ////////////////////////////////////////////////////

        public virtual SerializationType GetSerializationType() {
            return SerializationType.LowerCamelCase; // Default behavior, newer classes should use Consistent
        }

        public virtual object GetValueForDefaultOrdering() {
            return this.Id; // Default behavior
        }

        /// <summary>
        /// Determines whether this entity is published.
        /// Per default, the entity is always published, however a subclass
        /// can override this behaviour to match the actual state. This is used
        /// for all generic listings et cetera...
        /// NOTE: This has been deprecated in favor of IPublishedModelObject
        /// </summary>
        /// <returns></returns>
        /// public virtual bool IsPublished() {
        ///    return true;
        ///}

        /// <summary>
        /// Gets the audit log for this entity.
        /// Per default, a full audit log including all properties is tracked.
        /// If you don't want to do auditing at all for a entity, just override the method and return null.
        /// </summary>
        /// <param name="handler">The handler.</param>
        /// <param name="entry">The entry.</param>
        /// <returns></returns>
        public virtual AuditLog GetAuditLog(Handler.Handler handler, DbEntityEntry entry) {
            var audit = new AuditLog();
            audit.InitWithHandler(handler);
            audit.InitWithEntry(entry);
            audit.InitWithEntity(this);
            return audit;
        }

        /// <summary>
        /// Returns the visual Card Builder for this entity. Should be implemented by
        /// inheriting class to fully represent the entity.
        /// </summary>
        /// <returns></returns>
        public virtual Cards.CardBuilder VisualCard() {
            return new Cards.CardBuilder()
                .Type(this.TypeName)
                .Title(this.ToString())
                .Sub(this.PublicId)
                .DefaultCard(true);
        }

        /// <summary>
        /// Validates this model object entity to ensure that the current state and properties
        /// are valid. If the model object is not valid, then an exception is raised indicating so,
        /// otherwise the method should just return.
        /// Can be overridden, however make sure to call base.Validate() before doing your own validation routines.
        /// </summary>
        public virtual void Validate() {
            // Nothing to do here
        }

        public virtual JObject GetJSON(FormBuilder form = null, SerializationType serializationType = SerializationType.Default) {
            // Get the correct serialization type if not explicitly set
            if(serializationType == SerializationType.Default) {
                serializationType = this.GetSerializationType();
            }
            
            List<PropertyInfo> props = null;
            if (form != null) {
                if (serializationType == SerializationType.Consistent) {
                    form.Include("PublicId");
                }
                props = this.GetPropertiesForForm(form);
                
            } else { props = this.GetProperties(); }
            var jobj = new JObject();
            foreach (var prop in props) {
                if (prop.CanRead) {
                    var val = prop.GetValue(this);

                    if (serializationType == SerializationType.LowerCamelCase) {
                        
                        // V1 LowerCamelCase
                        if (val is Properties) {
                            val = ((Properties)val).ToString();
                        } else if (val is Price) {
                            val = (val as Price).ToString();
                        } else if (val is TranslatableText) {
                            var transText = val as TranslatableText;
                            val = transText.GetForLanguage(this.ContextLanguage());
                        }
                        jobj.Add(new JProperty(prop.Name.ToDashedLower(), val));

                    } else if (serializationType == SerializationType.Consistent) {
                        
                        // V2 Consistent
                        if (val is Properties) {
                            val = ((Properties)val).ToString();
                        } else if (val is Price) {
                            val = (val as Price).ToJSON();
                        } else if (val is TranslatableText) {
                            var transText = val as TranslatableText;
                            val = transText.ToJObject();
                        } else if (val is ContentFileAsset) {
                            var asset = val as ContentFileAsset;
                            val = asset.ToJObject();
                        } else if (val is ModelObject) { // verschachtelt
                            var modelObject = val as ModelObject;
                            val = modelObject.GetJSON(form);
                        } else if (val != null && val.GetType().AreListMembersOfSubtype(typeof(ModelObject))) {
                            var modelObjects = val as IEnumerable<ModelObject>;
                            var list = modelObjects.Select(mo => mo.GetJSON(form));
                            val = JToken.FromObject(list);
                        } else if (val != null && val.GetType().IsEnum) {
                            val = val.ToString();
                        }
                        jobj.Add(new JProperty(prop.Name, val));


                    } else {
                        throw new NotImplementedException($"{serializationType} is not supported");
                    }
                }
            }
            return jobj;
        }

        public virtual ModelObject FromJSON(JObject json, FormBuilder form = null, SerializationType serializationType = SerializationType.Default) {
            // Get the correct serialization type if not explicitly set
            if (serializationType == SerializationType.Default) {
                serializationType = this.GetSerializationType();
            }

            List<PropertyInfo> props = null;
            if (form != null) props = this.GetPropertiesForForm(form);
            else props = this.GetProperties();
            foreach (var prop in props) {
                if (prop.CanWrite) {

                    string key = null;
                    if (serializationType == SerializationType.LowerCamelCase) {
                        key = prop.Name.ToDashedLower();
                    } else if (serializationType == SerializationType.Consistent) {
                        key = prop.Name;
                    } else {
                        throw new NotImplementedException($"{serializationType} is not supported");
                    }

                    var val = json[key];
                    if ((val == null || val.Type == JTokenType.Null)) {
                        if (prop.PropertyType == typeof(Properties)) {
                            prop.SetValue(this, new Properties());
                        } else {
                            prop.SetValue(this, null);
                        }
                    }
                    else if (prop.PropertyType == typeof(Int64)) prop.SetValue(this, val.ToObject<Int64>());
                    else if (prop.PropertyType == typeof(Int16)) prop.SetValue(this, val.ToObject<Int16>());
                    else if (prop.PropertyType == typeof(Int32)) prop.SetValue(this, val.ToObject<Int32>());
                    else if   (prop.PropertyType == typeof(DateTime)) {
                        try {
                            prop.SetValue(this, val.ToObject<DateTime>());
                        } catch (Exception ex) {
                            //  try long instead
                            prop.SetValue(this, Core.Util.Time.GetDateTimeFromUTCMilliseconds(val.ToObject<long>()));
                        }
                    } else if (prop.PropertyType == typeof(DateTime?)) {
                        try {
                            prop.SetValue(this, val.ToObject<DateTime?>());
                        } catch (Exception ex) {
                            //  try long instead
                            prop.SetValue(this, Core.Util.Time.GetDateTimeFromUTCMilliseconds(val.ToObject<long>()));
                        }
                    } else if (prop.PropertyType == typeof(string)) prop.SetValue(this, val.ToString());
                    else if (prop.PropertyType == typeof(bool)) prop.SetValue(this, val.ToObject<bool>());
                    else if (prop.PropertyType == typeof(float)) prop.SetValue(this, val.ToObject<float>());
                    else if (prop.PropertyType == typeof(double)) prop.SetValue(this, val.ToObject<double>());
                    else if (prop.PropertyType == typeof(decimal)) prop.SetValue(this, val.ToObject<decimal>());
                    else if (prop.PropertyType == typeof(Properties)) prop.SetValue(this, val.ToObject<Properties>());
                    else if (prop.PropertyType == typeof(Price)) prop.SetValue(this, Price.FromJSON(val));
                    else if (prop.PropertyType == typeof(string[])) prop.SetValue(this, val.ToObject<string[]>());
                    else if (prop.PropertyType == typeof(List<string>)) prop.SetValue(this, val.ToObject<List<string>>());
                    else if (prop.PropertyType == typeof(Price)) prop.SetValue(this, Price.FromJSON(val));
                    else if (prop.PropertyType == typeof(TranslatableText)) prop.SetValue(this, TranslatableText.FromJSON(val));
                    else if (prop.PropertyType.IsEnum) prop.SetValue(this, Enum.Parse(prop.PropertyType,val.ToString()));
                    else {
                        throw new Exception($"Unknown property type {prop.PropertyType} for property {prop.Name} in ModelObject.FromJSON(). Please implement the conversion type.");
                    }
                }
            }
            return this;
        }

        /// <summary>
        /// Populates the model object entity using the specified FormBuilder form and context via the handler.
        /// Can be overridden, however make sure to call base.Populate() before doing your own population routines.
        /// </summary>
        /// <param name="handler">The handler.</param>
        /// <param name="form">The form.</param>
        /// <param providerKeyPrefix="string">e.g. multi edit list uses {public-id}-{key} param names</param>
        /// <exception cref="System.Exception"></exception>
        /// <exception cref="BackendException">
        /// invalid-value
        /// or
        /// invalid-value
        /// </exception>
        public virtual void Populate(IPopulateProvider populateProvider, FormBuilder form) {
            
            // First task: get the form id
            form.RequireSpecific();
            var formBuilderId = form.Id;
            
            // If the entity does not have a assigned context (for example if it was just created), we set it from the handler.
            //if (this.Context == null) this.Context = handler.DB;
            //TODO

            // Special handling for properties attribute
            var propertiesProp = this.GetPropertiesWithAttribute(typeof(PropertiesKeyAttribute));
            foreach(var prop in propertiesProp) {
                var propertiesProperty = prop.GetValue(this) as Properties;
                propertiesProperty.LoadDefaultsForProperty(prop);
            }

            // Load all form properties and get the values/defaults
            var props = this.GetPropertiesForForm(form);
            if (props.Count == 0) throw new Exception($"The populate method for the form {form.Id} does not have any properties!");
            foreach(var prop in props) {
                // Set the value
                var formName = prop.GetFormName(form);
                var formValue = populateProvider.GetValue(formName);
                if (string.IsNullOrEmpty(formValue)) {
                    formValue = null;
                    // Default?
                    var defaultAttribute = prop.GetCustomAttribute<DefaultValueAttribute>();
                    if (defaultAttribute != null) {
                        formValue = defaultAttribute.Value.ToString();
                    }
                }
                // Set the value using our helper method
                prop.SetValue(this, _convertStringToObject(formValue, prop.PropertyType, prop, populateProvider.DB));
                //TODO: @dan @micha: shouldnt all this validation just be handled by the validation method instead?
                // Validate it...
                // Required?
                var reqAttribute = prop.GetCustomAttribute<RequiredAttribute>();
                if (reqAttribute != null && formValue == null) throw new BackendException("invalid-value", $"Please enter a value for {prop.Name}.");
                // Min/max?
                //TODO: @dan
                // Validation attr
                if (formValue != null) {
                    foreach (var attr in prop.GetCustomAttributes<ValidationAttribute>()) {
                        if (!attr.IsValid(prop.GetValue(this))) {
                            throw new BackendException("invalid-value", $"Please enter a valid value for {prop.Name}.");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Copies the values to destination object using the form.
        /// skipEmpty wont set the destination properties to null or ""  
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="destination">The destination.</param>
        /// <param name="form">The form.</param>
        /// <param name="skipEmpty">if set to <c>true</c> [skip empty].</param>
        /// <exception cref="System.Exception">
        /// Copying to a different is not supported
       
        /// or
        /// </exception>
        public virtual void CopyValuesTo<T>(T destination, FormBuilder form, bool skipEmpty = false, bool ignoreType = false) where T : ModelObject {
            if (this.GetType() != destination.GetType() && ignoreType == false) {
                throw new Exception("Copying to a different is not supported");
            }
            var props = this.GetPropertiesForForm(form);
            if (props.Count == 0) throw new Exception($"The populate method for the form {form.Id} does not have any properties!");
            foreach (var prop in props) {

                object val = prop.GetValue(this);


                // skip empty?
                if (skipEmpty && (val == null || val.ToString() == string.Empty)) {
                    continue;
                }
                if (prop.CanWrite) {

                    if (val != null && val is Price) {
                        var valPrice = val as Price;
                        prop.SetValue(destination, valPrice.Clone());
                    }
                    else if(val != null && val is TranslatableText) {
                        var valTyped = val as TranslatableText;
                        prop.SetValue(destination, valTyped.Clone());
                    } else {
                        prop.SetValue(destination, val);
                    }

                }
            }
        }


        /// <summary>
        /// Called when a entity is loaded from the database.
        /// </summary>
        /// <param name="db">The database.</param>
        public virtual void OnLoad(ModelContext db) {

        }

        /// <summary>
        /// Called when an entity is deleted using the ModelContext.DeleteEntity() method.
        /// </summary>
        /// <param name="db">The database.</param>
        public virtual void OnDelete(ModelContext db) {
            _logger.Trace($"Deleting entity {this.UniversalId}...");
            // Make sure first level refs are loaded
            this.LoadFirstLevelObjectReferences();
            this.LoadFirstLevelNavigationReferences();
            // Get all mapped properties (doesn't have NotMapped)
            var cascadeDeleteProperties = this.GetModelObjectProperties().Where(p => p.CustomAttributes.Where(a => a.AttributeType == typeof(CascadeDeleteAttribute)).Any() == true);
            // Automatically remove ContentNode properties
            foreach(var prop in cascadeDeleteProperties) {
                var val = prop.GetValue(this);
                if(val != null) {
                    var obj = (ModelObject)val;
                    db.DeleteEntity(obj);
                }
            }
        }

        #endregion

        #region Type Implementer Methods ////////////////////////////////////////////////////

        protected internal class PropertyToJSONStringMapping<T> {
            
            private string _propertyName;
            private string _jsonStoreName;
            private ModelObject _entity;

            public PropertyToJSONStringMapping(string propertyName, string jsonStoreName , ModelObject entity) {
                this._propertyName = propertyName;
                this._jsonStoreName = jsonStoreName;
                this._entity = entity;
            }
            private JObject _loadJSONStore() {
                // Load the store value
                var storeValue = _entity.GetType().GetProperty(_jsonStoreName).GetValue(_entity);
                // Convert to json
                if (storeValue == null || (string)storeValue == "") {
                    return new JObject();
                } else {
                    var json = Core.JSON.ParseJsonAsJObject(storeValue.ToString()); // here we assume the json store is string
                    return json;
                }
            }
            public T Get() {
                return Get(default(T));
            }
            public T Get(T defaultValue) {
                var json = _loadJSONStore();
                // Extract the property value and convert it to T
                var jtoken = json.GetValue(_propertyName.ToDashedLower());
                if (jtoken != null) {
                    return jtoken.ToObject<T>();
                } else {
                    return defaultValue;
                }
            }
            public void Set(T val) {
                // Set the property on the virtual store value
                var json = _loadJSONStore();

                // This is not working for complex types
                //json[_propertyName.ToDashedLower()] = new JValue(val);


                if (val != null) {
                    // This alllows installing JsonConverters for complex types
                    json[_propertyName.ToDashedLower()] = JToken.FromObject(val);
                } else {
                    json[_propertyName.ToDashedLower()] = new JValue(val);
                }




                // Sync back to store
                var storeValue = Core.JSON.Serialize(json, true);
                _entity.GetType().GetProperty(_jsonStoreName).SetValue(_entity, storeValue);
            }
        }

        /// Note: this is deprecated because of performance issues caused in Reflection
        /// <summary>
        /// Automatically map a ModelObject property to a JSON store.
        /// The JSON store is always a string object that can either be on the same
        /// inheritance type level or can be up the inheritance chain for a type.
        /// 
        /// This method will automatically search for the JSON store property using
        /// the ```DefaultPropertyMappingJSONStore``` attribute.
        /// </summary>
        /// <example>
        /// Class for which you want to map a property:
        /// ```
        /// [FormBuilder(BurriForms.Portal.EDIT)]
        /// [FormBuilder(BurriForms.Portal.VIEW)]
        /// [FormBuilder(Forms.System.JSON)]
        /// [DataType("didoks")]
        /// public string DiDoks {
        ///     get {
        ///         return MapPropertyToJSONStore<string>(nameof(DiDoks)).Get();
        ///     }
        ///     set {
        ///         MapPropertyToJSONStore<string>(nameof(DiDoks)).Set(value);
        ///     }
        /// }
        /// ```
        /// Class up the heirarchy (or on same level) which acts as the data store:
        /// ```
        /// [Column]
        /// [DefaultPropertyMappingJSONStore]
        /// public string Content { get; set; }
        /// ```
        /// In this example, the property ```DiDoks``` is automatically mapped to a JSON object stored in ```Content```.
        /// </example>
        /// HINT: this is slow if its used in list -> better use MapPropertyToJSONStore<T>(string propertyName, string jsonStoreName) directly
        //protected PropertyToJSONStringMapping<T> MapPropertyToJSONStore<T>(string propertyName) {
        //    // No store property name was given, lets find the default
        //    List<PropertyInfo> props = this.GetType().GetProperties().Where(p => p.CustomAttributes.Any(a => a.AttributeType == typeof(DefaultPropertyMappingJSONStoreAttribute))).ToList();
        //    if (props.Count == 1) {
        //        string jsonStoreName = props.First().Name;
        //        return MapPropertyToJSONStore<T>(propertyName, jsonStoreName);
        //    } else {
        //        throw new Exception("Could not get the default property mapping JSON store. Your class must have exactly one string property wiht the attribute DefaultPropertyMappingJSONStore. ");
        //    }
        //}

       

        /// <summary>
        /// Automatically map a ModelObject property to the specified JSON store.
        /// The JSON store is always a string object that can either be on the same
        /// inheritance type level or can be up the inheritance chain for a type.
        /// </summary>
        /// <example>
        /// Class for which you want to map a property:
        /// ```
        /// [FormBuilder(BurriForms.Portal.EDIT)]
        /// [FormBuilder(BurriForms.Portal.VIEW)]
        /// [FormBuilder(Forms.System.JSON)]
        /// [DataType("didoks")]
        /// public string DiDoks {
        ///     get {
        ///         return MapPropertyToJSONStore<string>(nameof(DiDoks),nameof(Content)).Get();
        ///     }
        ///     set {
        ///         MapPropertyToJSONStore<string>(nameof(DiDoks),nameof(Content)).Set(value);
        ///     }
        /// }
        /// ```
        /// Class up the heirarchy (or on same level) which acts as the data store:
        /// ```
        /// [Column]
        /// [DefaultPropertyMappingJSONStore]
        /// public string Content { get; set; }
        /// ```
        /// In this example, the property ```DiDoks``` is automatically mapped to a JSON object stored in ```Content```.
        /// </example>
        protected PropertyToJSONStringMapping<T> MapPropertyToJSONStore<T>(string propertyName, string jsonStoreName) {
            return new PropertyToJSONStringMapping<T>(propertyName, jsonStoreName, this);
        }

        


        /// <summary>
        ///  Is the current class, not the base class potentially a DB object
        /// </summary>
        [NotMapped]
        public bool IsClassStoredInDB {
            get {
                var respectBaseClasses = false;
                    var modelClassAttribute = this.GetType().GetCustomAttribute<ModelClassAttribute>(respectBaseClasses);
                    return (modelClassAttribute != null);
               }
                         
        }

        protected internal class TypeImplementerMapping<T> where T:ModelObject  {

            private string _typeStoreName;
            private ModelObject _entity;
            private Type _baseType;
            private Type _implType;

            public TypeImplementerMapping(string typeStoreName, ModelObject entity) {
                this._typeStoreName = typeStoreName;
                this._entity = entity;

                // Get the implementation type name from the type store property
                // ie this is "{T}{StoreTypeValue}"
                string baseTypeName = _entity.GetType().GetProperty(_typeStoreName).GetValue(_entity)?.ToString();
                
                // Load the impl type
                _baseType = typeof(T);
                string baseName = _baseType.FullName;
                string implName = baseName + baseTypeName;
               
                _implType = _baseType.Assembly.GetType(implName); // type must be in same assembly

                // try all machinata types if not in base type assembly
                if (_implType == null) {
                    var implTypes = Core.Reflection.Types.GetMachinataTypes(_baseType).Where(t => t.Name == baseTypeName); // try all machinata modules
                    if (implTypes.Count() > 1) {
                        throw new Exception($"there is only one implementation with name {baseTypeName} inside machinata allowed");
                    } else if (implTypes.Count() == 1) {
                        _implType = implTypes.First();
                    }
                }
               
                if (_implType == null) throw new Exception(implName + " is not implemented!");
            }
            public T Load() {
                // Load a new instance of this type
                T implInstance = Activator.CreateInstance(_implType) as T;
                // Sync all base property values to the implementation
                var baseProps = _baseType.GetProperties();
                foreach(var prop in baseProps) {
                    if (prop.CanWrite) {
                        var val = _baseType.GetProperty(prop.Name).GetValue(_entity);
                        _implType.GetProperty(prop.Name).SetValue(implInstance, val);
                    }
                }
                return implInstance;
            }
            public void Save(T implInstance) {
                // Sync all implementation values back to the base entity
                var baseProps = _baseType.GetProperties();
                foreach (var prop in baseProps) {
                    if (prop.CanWrite && prop.Name != "Id") { // never update id
                        var val = _implType.GetProperty(prop.Name).GetValue(implInstance);
                        _baseType.GetProperty(prop.Name).SetValue(_entity, val);
                    }
                }
            }
        }

        protected TypeImplementerMapping<T> ModelObjectTypeImplementer<T>(string typeStoreName) where T : ModelObject {
            return new TypeImplementerMapping<T>(typeStoreName, this);

        }

        #endregion

        #region Private Helper Methods ////////////////////////////////////////////////////////////

        /// <summary>
        /// Generic method that converts a string to the specified type.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        private static object _convertStringToObject(string input, Type type, PropertyInfo prop, ModelContext db) {

            // Unwrap nullable
            var rawType = type;
            var nullableUnderlyingType = Nullable.GetUnderlyingType(type);
            if (nullableUnderlyingType != null) {
                type = nullableUnderlyingType;
            }

            // Enum handling
            if (type.IsEnum) {
                if (Enum.IsDefined(type, input)) {
                    return Enum.Parse(type, input);
                }
                throw new BackendException("convert-error", $"Could not convert {input} to enum {type.Name}");
            }

            // Special DateTime handling
            else if (rawType == typeof(DateTime?)) {
                // Datetime? (nullable)
                if (string.IsNullOrEmpty(input)) return null;
                return Core.Util.Time.ConvertStringToUTCDateTime(input);
            }
            else if (type == typeof(DateTime)) {
                // Datetime (not-nullable)
                if (string.IsNullOrEmpty(input)) throw new BackendException("invalid-date", $"The date {prop.Name} cannot be empty.");
                return Core.Util.Time.ConvertStringToUTCDateTime(input);
            }

            // Special DateRange handling
            // Note on complex types: Entity Framework doesn't support null complex types, thus here we ensure to return 
            else if (type == typeof(DateRange)) {
                return Core.Util.Time.ConvertStringToUTCDateRange(input);
            }

            // Special IntRange handling
            // Note on complex types: Entity Framework doesn't support null complex types, thus here we ensure to return 
            else if (type == typeof(IntRange)) {
                return new IntRange(input);
            }

            // Special Price handling
            // Note on complex types: Entity Framework doesn't support null complex types, thus here we ensure to return 
            else if (type == typeof(Price)) {
                return new Price(input);
            }

            // Special ContentNode handling
            else if (type == typeof(Core.Model.ContentNode)) {
                if (input == null || (string)input == "") {
                    // Setting to null, we must remove it!
                    throw new NotImplementedException();
                } else {
                    // Use the ContentNode builder to generate everything and set the value to the returned type
                    var node = Core.Model.ContentNode.UpdateContentUsingJSON(db, input);
                    return node;
                }
            }


            // Special TranslatableText handling
            else if (type == typeof(Core.Model.TranslatableText)) {
                if (input == null || (string)input == "") {
                    return new TranslatableText();
                } else {
                    // Encoded?
                    var json = JsonConvert.DeserializeObject<SortedDictionary<string, object>>(input);
                    if(json.ContainsKey("uri-encoded") && json["uri-encoded"].ToString() == true.ToString()) {
                        foreach(var key in json.Keys.Where(key => key != "uri-encoded" && key != "rich-text").ToList()) {
                            json[key] = Uri.UnescapeDataString(json[key].ToString());
                        }
                        json.Remove("uri-encoded");
                        input = JsonConvert.SerializeObject(json);
                    }
                    return new TranslatableText(input);
                }
            }

            // Special Model Object Hanlding
            // Note: make sure this is last!
            else if (typeof(ModelObject).IsAssignableFrom(type)) {
                // Null?
                if(string.IsNullOrEmpty(input)) return null;
                // Sanity
                if (db == null) throw new Exception("Could not get a ModelObject reference for property because the context is null.");
                var set = db.GetSetQueryableByName(type.Name);
                if (set == null) throw new Exception($"Could not get a DbSet for property because the type {type.Name} is not known.");
                // Extract the id
                int id = Core.Model.ModelObject.GetIdForPublicId(input);
                // Get the object
                var entity = set.SingleOrDefault(e => e.Id == id);
                return entity;
            }

            // Return null if the input was null, don't bother converting
            if (input == null) { return null; }

            // Percent type?
            var percentAttr = prop.GetCustomAttribute<PercentAttribute>(true);
            if(percentAttr != null) {
                // Trim percent
                var inputTrimmed = input;
                if(input.EndsWith("%")) inputTrimmed = input.Substring(0, input.Length - 1);
                // Convert to decimal
                double dec = 0;
                if(double.TryParse(inputTrimmed,out dec)) {
                    input = (dec / 100.0).ToString(CultureInfo.InvariantCulture);
                } else {
                    throw new BackendException("convert-error", $"Could not convert {input} to percent value of {type.Name}");
                }
            }

            // Percent type?
            var customFormatAttr = prop.GetCustomAttribute<CustomFormatAttribute>(true);
            if (customFormatAttr != null) {
                if(customFormatAttr.DataFormatString == "html") {
                    input = Uri.UnescapeDataString(input);
                }
            }

            // Other types
            var converter = System.ComponentModel.TypeDescriptor.GetConverter(type);
            if (converter != null && converter.IsValid(input)) {
                return converter.ConvertFromString(input);
            }
            throw new BackendException("convert-error", $"Could not convert {input} to {type.Name}");
        }

     

        #endregion
    }
}
