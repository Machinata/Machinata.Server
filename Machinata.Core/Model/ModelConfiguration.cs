using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.EntityFramework;
using MySql.Data.MySqlClient;

namespace Machinata.Core.Model {


    /// <summary>
    /// https://msdn.microsoft.com/en-us/library/jj680699(v=vs.113).aspx
    /// https://dev.mysql.com/doc/connector-net/en/connector-net-entityframework60.html
    /// </summary>
    /// <seealso cref="System.Data.Entity.DbConfiguration" />
    public class ModelConfiguration : MySqlEFConfiguration 
    { 
        public ModelConfiguration() 
        {
            var providerName = "MySql.Data.MySqlClient";
            SetExecutionStrategy(providerName, () => new MySqlExecutionStrategy());
            //SetHistoryContext(client,(connection, defaultSchema) => new ModelHistory(connection, defaultSchema));
            
            //SetDatabaseInitializer(new DropCreateDatabaseAlways<ModelContext>());
            //SetDefaultConnectionFactory(new LocalDbConnectionFactory("v11.0")); 

            SetMigrationSqlGenerator(providerName, () => new ModelGenerator());
            
        } 
    } 
}
