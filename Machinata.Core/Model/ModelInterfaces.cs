using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Model {
    
    public interface IQuanityModelObject {

        /// <summary>
        /// Gets the quantity.
        /// </summary>
        /// <value>
        /// The quantity.
        /// </value>
        int Quantity { get; }

        /// <summary>
        /// Gets the public identifier for quantity entity.
        /// This means that for example, if the entity implementing this interface is a OrderItem,
        /// then this should return Product.PublicId, since the quanity in OrderItem is describing how 
        /// many of a product.
        /// </summary>
        /// <value>
        /// The public identifier for quantity entity.
        /// </value>
        string PublicIdForQuantityEntity { get; }

    }
    

    public interface IPublishedModelObject {
        
        /// <summary>
        /// Indicates whether or not this entity is published.
        /// </summary>
        bool Published { get; }

    }

    public interface IEnabledModelObject {
        
        /// <summary>
        /// Indicates whether or not this entity is enabled.
        /// </summary>
        bool Enabled { get; }

    }

    public interface IShortURLModelObject {
        
        /// <summary>
        /// Provides the short-url component for the entity.
        /// </summary>
        string ShortURL { get; }

    }

    public interface IInfoStatusModelObject {
        /// <summary>
        /// If not null, should provide a theme color reflecting the status:
        ///
        ///  - ```info```
        ///  - ```warning```
        ///  - ```error```
        /// 
        ///  - ```green```
        ///  - ```red```
        /// </summary>
        string InfoStatusColor { get; }

        /// <summary>
        /// </summary>
        string InfoStatusText { get; }


        /// <summary>
        /// </summary>
        string InfoStatusTooltip { get; }

    }


}
