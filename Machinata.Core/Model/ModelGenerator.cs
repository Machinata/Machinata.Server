using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.Migrations.Sql;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Model {


    /// <summary>
    /// Custom model generator that adds some prefixes to allow for identification of specific foriegn keys or indexes created by EF.
    /// See https://stackoverflow.com/questions/31534945/change-foreign-key-constraint-naming-convention/31553476#31553476
    /// </summary>
    public class ModelGenerator : MySql.Data.EntityFramework.MySqlMigrationSqlGenerator {
        
        
        protected override MigrationStatement Generate(AddForeignKeyOperation op) {
            op.Name = "EF"+op.Name;
            return base.Generate(op);
        }

        protected override MigrationStatement Generate(DropForeignKeyOperation op) {
            op.Name = "EF"+op.Name;
            return base.Generate(op);
        }

        private string TrimSchemaPrefix(string table) {
            if (table.StartsWith("dbo."))
                return table.Replace("dbo.", "");
            return table;
        }

        protected override MigrationStatement Generate(CreateIndexOperation op) {
            op.Name = "EF"+op.Name;
            return  base.Generate(op);
        }

        /// <summary>
        /// Use this tempeorarly to generate missing statements on mysql 8
        /// use with caution
        /// </summary>
        /// <param name="op"></param>
        /// <returns></returns>
        protected MigrationStatement Generate2(CreateIndexOperation op) {
            

            StringBuilder sb = new StringBuilder();

            sb = sb.Append("CREATE ");

            if (op.IsUnique) {
                sb.Append("UNIQUE ");
            }

            sb.AppendFormat("index  `{0}` on `{1}` (", op.Name, TrimSchemaPrefix(op.Table));
            sb.Append(string.Join(",", op.Columns.Select(c => "`" + c + "`")) + ") ");

            var statement = new MigrationStatement() { Sql = sb.ToString() };

            //statement.Name = "EF" + op.Name;
            return statement;

        }

        protected override MigrationStatement Generate(DropIndexOperation op) {
            op.Name = "EF"+op.Name;
            return base.Generate(op);
        }

    }
}
