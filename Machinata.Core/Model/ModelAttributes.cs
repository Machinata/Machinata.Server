using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Model {

    public class SeedDatasets {
        public const string DATASET_REQUIRED = "required";
        public const string DATASET_ALL = "all";
        public const string DATASET_DUMMY = "dummy";
        public const string DATASET_INITIAL = "initial";
        public const string DATASET_RANDOM = "random";
        public const string DATASET_THEMES = "themes";
    }

    
    public class CustomDataTypes {
        /// <summary>
        /// Specify this on a CountryCode property to automatically insert a list of countries mapped to their two-digit codes.
        /// Example: 
        /// [DataType(CustomDataTypes.Country)]
        /// </summary>
        public const string Country = "country";
    }

    [AttributeUsage(validOn: AttributeTargets.All)]
    public class ModelClassAttribute : System.Attribute {}
    
    [AttributeUsage(AttributeTargets.All)]
    public class ModelSetAttribute : System.Attribute {}

    [AttributeUsage(AttributeTargets.All)]
    public class ModelContextAttribute : System.Attribute {}

    [AttributeUsage(AttributeTargets.All)]
    public class OnModelCreatingAttribute : System.Attribute {}

    public abstract class OnModelObjectEventAttribute : System.Attribute {
        private Type _targetType = null;

        public OnModelObjectEventAttribute(Type targetType = null) {
            _targetType = targetType;
        }

        public Type TargetType {
            get { return _targetType; }
        }
    }

    /// <summary>
    /// Use this attribute to hook into BEFORE a ModelObject is added to
    /// a model context. The method must accept two paraemters of ModelContext and ModelObject
    /// and must return a bool. 
    /// The attribute accepts a constructor parameter for it's target type, so that you can
    /// hook into only a specifc type of model object.
    /// If the returned bool is true, then the changes are saved (ie db.SaveChanges() is automatically
    /// called).
    /// Example method to hook into user:
    /// [OnModelObjectAddToDB(typeof(Core.Model.User))]
    /// public static bool OnModelObjectAddToDB(ModelContext db, ModelObject entity)
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.Method)]
    public class OnModelObjectAddToDBAttribute : OnModelObjectEventAttribute {
        public OnModelObjectAddToDBAttribute(Type targetType = null) : base(targetType) { }
    }

    /// <summary>
    /// Use this attribute to hook into BEFORE a ModelObject is DELETED from
    /// a model context. The method must accept two paraemters of ModelContext and ModelObject
    /// and must return a bool. 
    /// The attribute accepts a constructor parameter for it's target type, so that you can
    /// hook into only a specifc type of model object.
    /// If the returned bool is true, then the changes are saved (ie db.SaveChanges() is automatically
    /// called).
    /// Example method to hook into user:
    /// [OnModelObjectDeleteFromDB(typeof(Core.Model.User))]
    /// public static bool OnModelObjectDeleteFromDB(ModelContext db, ModelObject entity)
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.Method)]
    public class OnModelObjectDeleteFromDBAttribute : OnModelObjectEventAttribute {
        public OnModelObjectDeleteFromDBAttribute(Type targetType = null) : base(targetType) { }
    }

    /// <summary>
    /// Use this attribute to hook into AFTER a ModelObject is added to
    /// a model context. The method must accept two paraemters of ModelContext and ModelObject
    /// and must return a bool. If the returned bool is true, then the changes are saved.
    /// Example method:
    /// [OnModelObjectAddedToDB]
    /// public static bool OnModelObjectAddedToDB(ModelContext db, ModelObject entity)
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.Method)]
    public class OnModelObjectAddedToDBAttribute : System.Attribute {}

    /// <summary>
    /// Add this attribute to a static method with the parameter ModelContext in
    /// a ModelObject class to seed database data for that class.
    /// This method is called when the database is first created, and should be used
    /// for mission-critical data only.
    /// For example:
    /// [OnModelSeed]
    /// private static void OnModelSeed(ModelContext context, string dataset) { ... }
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.All)]
    public class OnModelSeedAttribute : System.Attribute {}

    /// <summary>
    /// Add this attribute to a static method with the parameter ModelContext in
    /// a ModelObject class to seed database connections between entities for that class.
    /// The difference to OnModelSeed is that OnModelSeedConnections is called once all
    /// the OnModelSeed routines have excecuted.
    /// This method is called when the database is first created, and should be used
    /// for mission-critical data only.
    /// For example:
    /// [OnModelSeedConnections]
    /// private static void OnModelSeedConnection(ModelContext context, string dataset) { ... }
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.All)]
    public class OnModelSeedConnectionsAttribute : System.Attribute {}
    
    /// <summary>
    /// Add this attribute to a static method with the parameter ModelContext in
    /// a ModelObject class to seed project data for that class.
    /// Use this attribute for seeding either project related example or dummy data, or initial data.
    /// The string parameter dataset can indicate a specific dataset to seed for, and may
    /// be set to one of the following constants:
    /// ProjectSeedDatasets.DATASET_ALL
    /// ProjectSeedDatasets.DATASET_DUMMY
    /// ProjectSeedDatasets.DATASET_INITIAL
    /// For example:
    /// [OnProjectSeed]
    /// private static void OnProjectSeed(ModelContext context, string dataset) { ... }
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.All)]
    public class OnProjectSeedAttribute : System.Attribute {}

    /// <summary>
    /// Add this attribute to a static method with the parameter ModelContext in
    /// a ModelObject class to seed project data for that class.
    /// Use this attribute for seeding project related connetions.
    /// The difference to OnProjectSeed is that OnProjectSeedConnections is called once all
    /// the OnProjectSeed routines have excecuted.
    /// The string parameter dataset can indicate a specific dataset to seed for, and may
    /// be set to one of the following constants:
    /// ProjectSeedDatasets.DATASET_ALL
    /// ProjectSeedDatasets.DATASET_DUMMY
    /// ProjectSeedDatasets.DATASET_INITIAL
    /// For example:
    /// [OnProjectSeedConnections]
    /// private static void OnProjectSeedConnections(ModelContext context, string dataset) { ... }
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.All)]
    public class OnProjectSeedConnectionsAttribute : System.Attribute {}

    [AttributeUsage(AttributeTargets.All)]
    public class CascadeDeleteAttribute : System.Attribute {}

    
        
    /// <summary>
    /// Provides a local mean for automatically generating policies on the model seed.
    /// All methods with this attribute will be called and the policied added to the database
    /// on the seed routines.
    /// 
    /// For example:
    /// [PolicyProvider]
    /// public static List<AccessPolicy> PolicyProvider() {
    ///     return new List<AccessPolicy>() {
    ///         new AccessPolicy() { Name = "Business", ARN = "/business/*" },
    ///         new AccessPolicy() { Name = "Business Admin", ARN = "/admin/business/*" }
    ///     };
    /// }
    /// 
    /// Often, the shortcut helper method can be used:
    /// 
    /// [PolicyProvider]
    /// public static List<AccessPolicy> PolicyProvider() {
    ///     return AccessPolicy.GetDefaultPolicies<Business>();
    /// }
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class PolicyProviderAttribute : System.Attribute {
        public PolicyProviderAttribute() { }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class DefaultPropertyMappingJSONStoreAttribute : System.Attribute {
        public DefaultPropertyMappingJSONStoreAttribute() { }
    }

    /// <summary>
    /// For theme loading system outside the usual .json themes
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class ThemeProviderAttribute : System.Attribute {
        public ThemeProviderAttribute() { }
    }
}
