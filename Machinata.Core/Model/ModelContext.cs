using MySql.Data.EntityFramework;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Model {

    /// <summary>
    /// https://msdn.microsoft.com/en-us/library/jj556606(v=vs.113).aspx
    /// </summary>
    /// <seealso cref="System.Data.Entity.DbContext" />
    [DbConfigurationType(typeof(ModelConfiguration))]
    [ModelContext]
    public partial class ModelContext : DbContext {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private static NLog.Logger _loggerSQL = NLog.LogManager.GetLogger("SQL");
        #endregion

        #region Constructor

        // For UnitTests
        public ModelContext():base () {

        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the handler that created the model context.
        /// </summary>
        /// <value>
        /// The handler.
        /// </value>
        public Handler.Handler Handler { get; set; }

        public bool SeedIfNotExists = true;
        public bool EnableModelValidationOnStartup = true;

        private string _connectionString;

        #endregion

        #region Model Initializers

        /// <summary>
        /// Gets a new model context object for the current environment and machine.
        /// Note: A handler should always be passed if there is one, so that the context can
        /// perform the necessary audit logs. If not handler is passed, then the context is 
        /// assumed to be a 'system' context, for example a task manager.
        /// </summary>
        /// <param name="handler">The handler.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">No connection string for '+connectionName+'!</exception>
        public static ModelContext GetModelContext(Handler.Handler handler) {
            // Create and return
            var ret = new ModelContext(GetConnectionString());
            ret.Handler = handler;
            return ret;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelContext"/> class with
        /// the given connection string.
        /// NOTE: Use ModelContext.GetModelContext() to get a default connection
        /// for the current environment and machine.
        /// </summary>
        /// <param name="connectionName">Name of the connection.</param>
        public ModelContext(string connectionString, bool seedIfNotExists = true, bool enableModelValidationOnStartup = true) : base(connectionString) 
        {
            // Init some settings
            this._connectionString = connectionString;
            this.SeedIfNotExists = seedIfNotExists;
            this.EnableModelValidationOnStartup = enableModelValidationOnStartup;

            // Enable/disable lazy loading
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
            
            // Set a SQL dump logger
            if(Core.Config.LogSQLEnabled) {
                this.Database.Log = new Action<string>(delegate(string sql) {
                    if(!string.IsNullOrEmpty(sql) && !string.IsNullOrEmpty(sql.Trim())) _loggerSQL.Trace(sql.Trim());
                });
            }

            // Register object context events
            System.Data.Entity.Infrastructure.IObjectContextAdapter contextAdapter = this;
            System.Data.Entity.Core.Objects.ObjectContext objectContext = contextAdapter.ObjectContext;
            objectContext.ObjectMaterialized += OnObjectMaterialized;
            objectContext.SavingChanges += OnSavingChanges;
        }

        public static string GetConnectionString(string database = null) {
            if (database == null) database = Core.Config.DatabaseDatabase;
            // Get connection string
            string connectionName = "SQLConnection-" + Core.Config.Environment;
            // Try regular config env first
            string connectionString = Core.Config.GetStringSetting("DatabaseConnectionString", null);
            if(string.IsNullOrEmpty(connectionString)) {
                // Try the <connectionStrings> settings from web.config or app config
                if (System.Configuration.ConfigurationManager.ConnectionStrings[connectionName] != null) {
                    connectionString = System.Configuration.ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
                }
            }
            // Validate
            if(string.IsNullOrEmpty(connectionString)) throw new Exception("No connection string for '"+connectionName+"', and no configuration setting for 'SQLConnection'!");
            // Do replacements
            string password = Core.Config.DatabasePassword;
            connectionString = connectionString.Replace("{username}", Core.Config.DatabaseUsername);
            connectionString = connectionString.Replace("{password}", password);
            connectionString = connectionString.Replace("{host}", Core.Config.DatabaseHost);
            connectionString = connectionString.Replace("{database}", database);
            return connectionString;
        }

        public List<string> GetTables() {
            var tables = this.Database.SqlQuery<string>("SHOW TABLES").OrderBy(t => t);
            return tables.Where(t => t.StartsWith("__") == false).ToList();
        }

        public ModelDescription GetDescription() {

            if (!string.IsNullOrEmpty(Core.Config.DatabaseMigrationForceSQLMode)) {
                this.ExecuteSQL($"SET sql_mode='{Core.Config.DatabaseMigrationForceSQLMode}';");
            }

            var ret = new ModelDescription();
            foreach (var tableName in GetTables()) {
                var tableMeta = new ModelDescription.Table();
                tableMeta.Name = tableName;
                tableMeta.CreateSQL = GetCreateSQLForTable(tableName);
                ret.Tables.Add(tableMeta);
            }
            return ret;
        }

        
        public string GetCreateSQLForAllTables() {
            StringBuilder sql = new StringBuilder();
            foreach(var table in GetTables()) {
                sql.AppendLine(GetCreateSQLForTable(table));
                sql.AppendLine("");
            }
            return sql.ToString().Trim();
        }

        public DateTime GetDBCreationTime() {
            DateTime ret = DateTime.MinValue;
            // Use the direct MySQL library...
            using (var con = new MySqlConnection(this._connectionString)) {
                con.Open();
                using (var cmd = new MySqlCommand("SELECT CREATE_TIME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = \"BASE TABLE\" AND TABLE_SCHEMA = \""+this.Database.Connection.Database+"\" LIMIT 1", con)) {
                    using (var reader = cmd.ExecuteReader()) {
                        while (reader.Read()) {
                            ret = reader.GetDateTime(0);
                        }
                    }
                }
            }
            return ret.ToUniversalTime();
        }

        public string GetCreateSQLForTable(string table) {
            string ret = null;
            
            // Validate
            if (table.Contains("'") || table.Contains("\"") || table.Contains(";")) throw new Exception("Table names cannot contain punctuations.");

            // Use the direct MySQL library...
            using (var con = new MySqlConnection(this._connectionString)) {
                con.Open();
                using (var cmd = new MySqlCommand("SHOW CREATE TABLE `" + table + "`;", con)) {
                    using (var reader = cmd.ExecuteReader()) {
                        while (reader.Read()) {
                            ret = reader.GetString(1);
                        }
                    }
                }
            }

            // Note: the following does not work. Somehow EF can't handle the column names for this statement, even if re-mapped in the class...
            // var ret = this.Database.SqlQuery<TableCreateSQL>("SHOW CREATE TABLE @TABLE", new MySql.Data.MySqlClient.MySqlParameter("TABLE",table)).FirstOrDefault();
            // sql.AppendLine(ret.CreateTable);

            return ret;
        }

        public static string CreateTemporaryDatabaseName() {
            return Core.Config.DatabaseDatabase+"_temp";
        }

        public static ModelContext CreateTemporaryContext() {
            // Get context for temp db
            var tempContext = new ModelContext(
                connectionString: GetConnectionString(CreateTemporaryDatabaseName()), 
                seedIfNotExists: false,
                enableModelValidationOnStartup: false);
            // Delete if exists
            if(tempContext.Database.Exists()) {
                tempContext.Database.Delete();
            }
            // Create
            tempContext.Database.Create();
            // Return
            return tempContext;
        }

        public static ModelContext GetTemporaryContext() {
            // Get context for temp db
            var tempContext = new ModelContext(
                connectionString: GetConnectionString(CreateTemporaryDatabaseName()), 
                seedIfNotExists: false,
                enableModelValidationOnStartup: false);
            // Create if not exist
            if(tempContext.Database.Exists() == false) {
                tempContext.Database.Create();
            }
            // Return
            return tempContext;
        }

        public int ExecuteSQL(string sql) {
            // Use the direct MySQL library...
            var ret = 0;
            using (var con = new MySqlConnection(this._connectionString)) {
                con.Open();
                using (var cmd = new MySqlCommand(sql, con)) {
                    ret = cmd.ExecuteNonQuery();
                }
            }
            return ret;
        }
        /// <summary>
        /// Called when an entity object is materialized.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Data.Entity.Core.Objects.ObjectMaterializedEventArgs"/> instance containing the event data.</param>
        private void OnObjectMaterialized(object sender, System.Data.Entity.Core.Objects.ObjectMaterializedEventArgs e) {
            // Set the context property
            dynamic entity = e.Entity;
            entity.Context = this;
            entity.OnLoad(this);
        }

        private void OnSavingChanges(object sender, EventArgs e) {
            /*ObjectContext context = sender as ObjectContext;
            if (context != null) {
                foreach (ObjectStateEntry entry in context.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Modified)) {
                    if (!entry.IsRelationship && (entry.Entity.GetType() == typeof(SalesOrderHeader) { 
                        //DO STUFF
                    }
                }
            }*/
        }
        

        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        /// before the model has been locked down and used to initialize the context.  The default
        /// implementation of this method does nothing, but it can be overridden in a derived class
        /// such that the model can be further configured before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        /// <remarks>
        /// Typically, this method is called only once when the first instance of a derived context
        /// is created.  The model for that context is then cached and is for all further instances of
        /// the context in the app domain.  This caching can be disabled by setting the ModelCaching
        /// property on the given ModelBuidler, but note that this can seriously degrade performance.
        /// More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        /// classes directly.
        /// </remarks>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            _logger.Trace("Running OnModelCreating...");
            // Set the decimal precision
            // On MySql, this property maps to the default type 'DECIMAL(18,2)',
            // where
            //     DECIMAL(M,D)
            //     M is the maximum number of digits (the precision). It has a range of 1 to 65. 
            //     D is the number of digits to the right of the decimal point (the scale). It has a range of 0 to 30 and must be no larger than M. 
            // See https://dev.mysql.com/doc/refman/5.7/en/precision-math-decimal-characteristics.html
            modelBuilder.Conventions.Remove<DecimalPropertyConvention>();
            modelBuilder.Conventions.Add(new DecimalPropertyConvention((byte)Core.Config.DefaultDecimalPrecision, (byte)Core.Config.DefaultDecimalScale)); // 18 >> quintillion 1,000,000,000,000,000,000

            // Scan each assembly and invoke its table creation for any sub types
            var entityMethod = typeof(DbModelBuilder).GetMethod("Entity");
            foreach (var type in GetModelClassTypes()) {
                _logger.Trace("Creating empty class for "+type.Name);
                {
                    // We do this at startup to ensure that things such as static constuctures are called and the type is registered and marshalled
                    object instance = Activator.CreateInstance(type);
                }
                // Invoke make method to create tables
                _logger.Trace("Running entity method for "+type.Name);
                entityMethod.MakeGenericMethod(type).Invoke(modelBuilder, new object[] { });
                // Invoke the helper method to define relations et cetera
                var onModelCreatingMethods = type
                    .GetMethods(BindingFlags.Static | BindingFlags.NonPublic)
                    .Where(t => t.GetCustomAttributes(typeof(OnModelCreatingAttribute), inherit: false).Any());
                foreach(var mi in onModelCreatingMethods) {
                    _logger.Trace("Running model builder relations for "+type.Name);
                    mi.Invoke(null, new object[] { modelBuilder });
                }
            }

            if (true) {
                _logger.Trace("Apply custom precision to model builder...");
                modelBuilder.Properties()
                    .Having(x => x.GetCustomAttributes(false).OfType<CoreTypes.DecimalPrecisionAttribute>().FirstOrDefault())
                    .Configure((config, att) => config.HasPrecision(att.Precision,att.Scale));
            }

            // Make sure EF doesnt create NVARCHAR for strings with MaxLength
            // NVARCHAR == VARCHAR WITH UTF8
            if (true) {
                _logger.Trace("Apply custom type to model builder...");
                modelBuilder.Properties<string>().Where(p => p.GetCustomAttributes<MaxLengthAttribute>().FirstOrDefault() != null)
                    .Configure((config) => config.HasColumnType("varchar"));

            }


            // (TESTING) Apply custom navigation property table names
            //modelBuilder.Properties()
            //        .Having(x => x.GetCustomAttributes(false).OfType<CoreTypes.TableNameAttribute>().FirstOrDefault())
            //        .Configure((config, att) => config.HasColumnName(att.Name));

            _logger.Trace("Running base.OnModelCreating...");
            base.OnModelCreating(modelBuilder);
            _logger.Trace("Done");

        }
        #endregion

        #region Helper Methods

        public System.Data.Entity.Core.Objects.ObjectContext GetObjectContext() {
            return ((System.Data.Entity.Infrastructure.IObjectContextAdapter)this).ObjectContext;
        }

        public void DeleteEntity(ModelObject entity) {
            // Sanity
            if (entity == null) throw new Exception("Entity to delete cannot be null.");
            // TODO: what about referenced Objects,FK, etc?
            entity.OnDelete(this);
            // Extract the object context
            GetObjectContext().DeleteObject(entity);
        }

        #endregion

        #region DbContext Overrides

        private bool _callHookFunctionForDBEntries<T>(List<DbEntityEntry> entries) where T : OnModelObjectEventAttribute {
            // Call all on-add hook methods
            var requireReSave = false;
            foreach (var entry in entries) {
                var entity = entry.Entity as ModelObject;
                if (entity != null) {
                    // Make sure the context is attached. In some cases we want to use the ModelObject.Context right after saving,
                    // but the event hasn't been fired because OnMaterialized is only when loading from DB...
                    if (entity.Context == null) entity.Context = this;
                    // Call all OnModelObjectAddToDB hooks
                    foreach(var method in Core.Reflection.Methods.GetMachinataMethodsWithAttribute(typeof(T))) {
                        // Get the original attribute
                        var attr = method.GetCustomAttribute<T>(false);
                        // Make sure that the attribute target matches
                        if (attr.TargetType == null || entity.GetType() == attr.TargetType) {
                            bool ret = (bool)method.Invoke(null, new object[] { this, entity });
                            if (ret == true) requireReSave = true;
                        }
                    }
                }
            }
            return requireReSave;
        }


        public override int SaveChanges() {
            return SaveChanges(true);
        }

        public int SaveChanges(bool leaveAuditTrail) {
            // Init
            bool requireReSave = false;
            // Created audit trail?
            var newAuditLogs = new Dictionary<AuditLog,ModelObject>();
            if (leaveAuditTrail && Core.Config.AuditTrailsEnabled) {
                var auditEntries = this.ChangeTracker.Entries().Where(e => e.State != EntityState.Unchanged && e.State != EntityState.Detached).ToList();
                foreach (var entry in auditEntries) {
                    // Get the entity
                    var entity = entry.Entity as ModelObject;
                    if(entity != null) {
                        try {
                            AuditLog audit = entity.GetAuditLog(this.Handler, entry);
                            if (audit != null) newAuditLogs.Add(audit,entity); 
                        }catch(Exception e) {
                            _logger.Error(e, $"Could not create a audit log for {entity.PublicId}: {e.Message}");
                        }
                    }
                }
            }
            // Save a list of all added entries (new in the db)
            var addedEntries = this.ChangeTracker.Entries().Where(e => e.State == EntityState.Added).ToList();
            var deletedEntries = this.ChangeTracker.Entries().Where(e => e.State == EntityState.Deleted).ToList();
            // Call all on-add hook methods
            /*foreach (var entry in addedEntries) {
                var entity = entry.Entity as ModelObject;
                if (entity != null) {
                    // Make sure the context is attached. In some cases we want to use the ModelObject.Context right after saving,
                    // but the event hasn't been fired because OnMaterialized is only when loading from DB...
                    if (entity.Context == null) entity.Context = this;
                    // Call all OnModelObjectAddToDB hooks
                    foreach(var method in Core.Reflection.Methods.GetMachinataMethodsWithAttribute(typeof(OnModelObjectAddToDBAttribute))) {
                        // Get the original attribute
                        var attr = method.GetCustomAttribute<OnModelObjectAddToDBAttribute>(false);
                        // Make sure that the attribute target matches
                        if (attr.TargetType == null || entity.GetType() == attr.TargetType) {
                            bool ret = (bool)method.Invoke(null, new object[] { this, entity });
                            if (ret == true) requireReSave = true;
                        }
                    }
                }
            }*/
            _callHookFunctionForDBEntries<OnModelObjectAddToDBAttribute>(addedEntries);
            _callHookFunctionForDBEntries<OnModelObjectDeleteFromDBAttribute>(deletedEntries);
            // Call base to save changes

            int changes;
            try {
                changes = base.SaveChanges();
            }
            catch(System.Data.Entity.Validation.DbEntityValidationException deve) {
               
                var errors = deve.EntityValidationErrors.Select(eve => new { Property = eve.Entry.Entity.GetType(), Errors = string.Join(",", eve.ValidationErrors.Select(ve => ve.ErrorMessage)) });
                var newException = new Exception($"DbEntityValidationException: { string.Join(",",errors.Select(e => e.Property + ": " + e.Errors))}");
                throw newException;
            }
            foreach (var entry in addedEntries) {
                var entity = entry.Entity as ModelObject;
                if (entity != null) {
                    _logger.Debug($"Added new DB entity {entity.TypeName} {entity.Id}");
                    // Special post-save side effects, for example for serial id?
                    foreach(var method in Core.Reflection.Methods.GetMachinataMethodsWithAttribute(typeof(OnModelObjectAddedToDBAttribute))) {
                        bool ret = (bool) method.Invoke(null, new object[] { this, entity });
                        if (ret == true) {
                            requireReSave = true;
                            _logger.Debug($"Added DB entity {entity.TypeName} {entity.Id} requires resave");
                        }
                    }
                }
            }
            // Attach new audit logs?
            if(newAuditLogs.Count > 0) {
                foreach(var kv in newAuditLogs) {
                    kv.Key.InitWithEntity(kv.Value); // Update entity state, since before we might not have gotten for example the db id
                    this.AuditLogs().Add(kv.Key);
                    requireReSave = true;
                }
            }
            // Save again?
            if(requireReSave == true) {
                _logger.Debug($"Added new entities require resave...");
                changes += base.SaveChanges();
            }
            return changes;
        }

        #endregion
        
        #region Handler Application Startup

        [Core.Lifecycle.OnApplicationStartup]
        public static void OnApplicationStartup() {
            // Set initializer
            Database.SetInitializer(new ModelSeed());
            if (Core.Config.DatabaseQuickStartup != true) {
                // Force the creation and registration of the model context...
                var context = ModelContext.GetModelContext(null);
                //Note: this still doesn't actually make a initial connection... The first user to hit a table will still have an additional 1000ms for the context to warmup...
            }
        }

        #endregion

        #region Sets/Contexts through reflection 

        /// <summary>
        /// Gets all the model context sets as member infos (shorthand).
        /// </summary>
        /// <value>
        /// The sets.
        /// </value>
        public List<Type> Sets {
            get {
                return ModelContext.ModelSets;
            }
        }

        /// <summary>
        ///Gets all the model context sets as member infos.
        /// </summary>
        /// <value>
        /// The model sets.
        /// </value>
        public static List<Type> ModelSets {
            get {
                return GetModelClassTypes();
            }
        }

        /// <summary>
        /// Gets a model context set by it's name through use of reflection.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>A IQueryable ModeObject list</returns>
        public object GetSetObjectByName(string name) {
            var type = GetModelCobjectClassByName(name);
            var set = this.Set(type);
            return set;
        }

        public Type GetModelCobjectClassByName(string name) {
            var type = GetModelClassTypes().Where(t => t.Name == name).SingleOrDefault();
            return type;
        }

        /*
        public object Attach(string name, object obj) {
            var set = GetSetObjectByName(name);
            var ret = set.GetType().InvokeMember("Attach", BindingFlags.InvokeMethod|BindingFlags.Public | BindingFlags.Instance, null, set, new object[] { obj });
            return ret;
        }*/

        public IQueryable<Core.Model.ModelObject> GetSetQueryableByName(string name) {
            return (IQueryable<Core.Model.ModelObject>)GetSetObjectByName(name);
        }

        public static List<MethodInfo> GetModelSetMembers() {
            var types = new List<MethodInfo>();
            foreach (var mem in Core.Reflection.Methods.GetMachinataMethods()) {
                if(mem.GetCustomAttributes((typeof(ModelSetAttribute))).Count() > 0) {
                    types.Add(mem);
                }
            }
            return types;
        }

        /// <summary>
        /// Gets all the model object types as member infos. This includes any extended model contexts and their classes.
        /// </summary>
        /// <returns></returns>
        public static List<Type> GetModelClassTypes() {
            var types = new List<Type>();
            foreach (var t in Core.Reflection.Types.GetMachinataTypesWithAttribute(typeof(ModelClassAttribute))) { 
                types.Add(t);
            }
            return types;
        }

        /// <summary>
        /// Gets all the model context types.
        /// </summary>
        /// <returns></returns>
        public static List<Type> GetModelContextTypes() {
            List<Type> types = new List<Type>();
            foreach (var t in Core.Reflection.Types.GetMachinataTypesWithAttribute(typeof(ModelContextAttribute))) {
                types.Add(t);
            }
            return types;
        }

       


        #endregion
    }


}
