using DocumentFormat.OpenXml.Math;
using Machinata.Core.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Model {


    /// <summary>
    /// A Complex Type which represents a media asset.
    /// </summary>
    [ComplexType]
    [TypeConverter(typeof(ContentFileAssetConverter))]
    public class ContentFileAsset {

        public string SourceURL { get; set; }
        public string AssetURL { get; set; }
        public int? ContentFileId { get; set; }
        public string Hash { get; set; }

        public bool HasValue {
            get {
                if (this.ContentFileId != null && SourceURL != null && this.AssetURL != null) {
                    return true;
                }
                return false;
            }
        }

        public void Reset() {
            this.SourceURL = null;
            this.AssetURL = null;
            this.ContentFileId = null;
            this.Hash = null;
        }


        public ContentFileAsset() {

        }

        public override string ToString() {
            return this.AssetURL;
        }

        /// <summary>
        /// </summary>
        public JObject ToJSON() {
            var result = new JObject();
            result["SourceURL"] = SourceURL;
            result["AssetURL"] = AssetURL;
            result["Hash"] = Hash;
            return result;
        }

        /// <summary>
        /// </summary>
        public JObject ToJObject() {
            var result = new JObject();
            result["SourceURL"] = SourceURL;
            result["AssetURL"] = AssetURL;
            result["Hash"] = Hash;
            return result;
        }



    }

    /// <summary>
    /// Allows for automatic conversion from a JSON string to a ContentFileAsset object.
    /// </summary>
    /// <seealso cref="System.ComponentModel.TypeConverter" />
    public class ContentFileAssetConverter : TypeConverter {

        public override bool IsValid(ITypeDescriptorContext context, object value) {
            if (value is string) {
                try {
                    JsonConvert.DeserializeObject<Dictionary<string, object>>((string)value);
                    return true;
                } catch (Exception e) {
                    return false;
                }
            }
            return base.IsValid(context, value);
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType) {
            //TODO
            return base.CanConvertFrom(context, sourceType);
            /*if (sourceType == typeof(string)) return true;
            return base.CanConvertFrom(context, sourceType);*/
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value) {
            return base.ConvertFrom(context, culture, value);
            //TODO
            /*if (value is string) {
                return new ContentFileAsset((string)value);
            } else {
                return base.ConvertFrom(context, culture, value);
            }*/
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType) {
            if (destinationType == typeof(JValue)) return true;
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType) {
            if (destinationType == typeof(JValue)) {
                return new JValue("");
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

}