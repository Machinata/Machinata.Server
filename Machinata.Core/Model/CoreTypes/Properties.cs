using Machinata.Core.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Model {

    /// <summary>
    /// Complex type that represents a series of key values.
    /// This class can be used as a property type of a model object to enable
    /// custom and flexible key-values. The internal data structure is
    /// automatically serialized and deserialized as JSON, which is stored in the DB.
    /// 
    /// By using the PropertiesKey attribute, one can set default key values.
    /// 
    /// The order of all the Properties keys is alphabetical.
    /// </summary>
    [ComplexType]
    [TypeConverter(typeof(PropertiesConverter))]
    public class Properties {

        /// <summary>
        /// The internal JSON Data. 
        /// DO NOT SET THIS.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public string Data { get; set; } // TODO: migrate to a private backing field? See https://docs.microsoft.com/en-us/ef/core/modeling/backing-field

        /// <summary>
        /// The internal serialed dictionary.
        /// </summary>
        private SortedDictionary<string, object> _serializedData;

        /// <summary>
        /// Initializes a new instance of the <see cref="Properties"/> class.
        /// </summary>
        public Properties() : base() {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Properties"/> class
        /// using a JSON key-value string.
        /// </summary>
        /// <param name="json">The json.</param>
        public Properties(string json) : base() {
            this.Data = json;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Properties"/> class.
        /// The keyvalue dictionary is automatically serialized for JSON.
        /// </summary>
        /// <param name="keysAndValues">The keys and values.</param>
        public Properties(Dictionary<string,object> keysAndValues) : base() {
            this.Data = Core.JSON.Serialize(keysAndValues, false);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Properties"/> class.
        /// The dynamic object is automatically serialized for JSON.
        /// </summary>
        /// <param name="keysAndValues">The keys and values.</param>
        public Properties(dynamic keysAndValues) : base() {
            this.Data = Core.JSON.Serialize(keysAndValues, false);
        }
      
        private void _syncBackingData() {
            this.Data = JsonConvert.SerializeObject(_getSerializedData());
        }

        /// <summary>
        /// Gets the serialized data dictionary.
        /// On first call the JSON data is automatically loaded. If there is no JSON data available, then
        /// a empty dictonary is created.
        /// </summary>
        /// <returns></returns>
        private SortedDictionary<string, object> _getSerializedData() {
            if (_serializedData == null) {
                if (this.Data != null) {
                    _serializedData = JsonConvert.DeserializeObject<SortedDictionary<string, object>>(this.Data);
                } else {
                    _serializedData = new SortedDictionary<string, object>();
                }
            }
            return _serializedData;
        }


        /// <summary>
        /// Shortcut method to get all keys for the object.
        /// </summary>
        /// <value>
        /// The keys.
        /// </value>
        public SortedDictionary<string,object>.KeyCollection Keys {
            get {
                return _getSerializedData().Keys;
            }
        }

        public void Clear() {
            this.Data = null;
        }

        public void LoadFromJSONString(string jsonString) {
            this.Data = jsonString;
        }

        /// <summary>
        /// Gets or sets the <see cref="System.Object"/> with the specified key using the 
        /// array index operator.
        /// </summary>
        /// <value>
        /// The <see cref="System.Object"/>.
        /// </value>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public object this[string key] {
            get {
                if(_getSerializedData().Keys.Contains(key)) return _getSerializedData()[key];
                else return null;
            }
            set {
                _getSerializedData()[key] = value;
                _syncBackingData();
            }
        }

     

        /// <summary>
        /// Loads the default keys (if not already set) if the object is used
        /// as a property for the given property info.
        /// If the property has the attribute PropertiesKey, then that is 
        /// registered as a default key.
        /// </summary>
        /// <param name="prop">The property.</param>
        public void LoadDefaultsForProperty(PropertyInfo prop) {
            var attribs = prop.GetCustomAttributes<PropertiesKeyAttribute>();
            foreach(var attr in attribs) {
                if (string.IsNullOrEmpty(attr.Keys)) continue;
                var attrKeys = attr.Keys.Split(',');
                foreach(var keyAndDefault in attrKeys) {
                    // Parse out key and default value (can be in format key=default...)
                    var segs = keyAndDefault.Split('=');
                    object defaultValue = null;
                    string key = null;
                    if(segs.Length == 1) {
                        key = segs.First();
                        defaultValue = null;
                    } else if (segs.Length == 2) {
                        key = segs.First();
                        defaultValue = segs.Last();
                    }
                    // Change default to specific types
                    if (defaultValue != null) {
                        if (defaultValue.ToString() == "true") defaultValue = true;
                        else if (defaultValue.ToString() == "false") defaultValue = false;
                        int parsedInt;
                        if (int.TryParse(defaultValue.ToString(), out parsedInt)) {
                            defaultValue = parsedInt;
                        }
                    }
                    //TODO int
                    // Set the default value
                    if(!this.Keys.Contains(key)) {
                        _getSerializedData()[key] = defaultValue;
                    } else if(defaultValue != null && (_getSerializedData()[key] == null)) {
                        _getSerializedData()[key] = defaultValue;
                    }
                }
            }
            _syncBackingData();
        }

        public void SetDefaultValueForEmptyPropertyKey(string key, object defaultValue) {
            if (this[key] == null || (string)this[key] == "") {
                this[key] = defaultValue;
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance as a JSON string.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() {
            var data = _getSerializedData();
            // return null if no keys or no data
            if (data != null && data.Any()) {
                return JsonConvert.SerializeObject(_getSerializedData());
            }
            return null;
        }

        public SortedDictionary<string, object> ToDictionary() {
            return _getSerializedData();
        }

        public JObject ToJObject() {
            var ret = new JObject();
            var data = _getSerializedData();
            foreach(var key in data.Keys) {
                var val = data[key];
                if (val == null) {
                    ret[key] = null;
                } else if (val.GetType() == typeof(JArray)) {
                    ret[key] = val as JToken;
                } else if (val.GetType() == typeof(JObject)) {
                    ret[key] = val as JObject;
                } else {
                    ret[key] = new JValue(val);
                }
            }
            return ret;
        }

        public Properties Clone() {
            var dictionary = this.ToDictionary();
            var result = new Properties(dictionary);
            return result;
        }

        public string ToHumanReadableString(string seperator = "\n") {
            var data = _getSerializedData();
            // Return null if no keys or no data
            if (data == null || !data.Any()) return null;
            // Compile
            List<string> keyVals = new List<string>();
            foreach(var key in data.Keys) {
                var val = data[key];
                if (val != null && !string.IsNullOrEmpty(val.ToString())) keyVals.Add(key + ": " + val);
            }
            // Validate
            if (keyVals.Count == 0) return null;
            // CSV
            return string.Join(seperator, keyVals);
        }

        public void Delete(string key) {
            _getSerializedData().Remove(key);
            _syncBackingData();
        }

        /// <summary>
        /// Initializes the datastructure for a specific project
        /// </summary>
        /// <param name="keys">The keys.</param>
        public void Initialize(List<string> keys) {
            foreach(var key in keys) {
                if (!this.Keys.Contains(key)) {
                    this[key] = string.Empty;
                }
            }
        }

        /// <summary>
        /// The value cannot be null
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string String(string key) {
            if (this.Keys.Contains(key)) {
                return this[key].ToString();
            } else {
                return null;
            }
        }

        /// <summary>
        /// The value can be null
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string StringNullable(string key) {
            if (this.Keys.Contains(key)) {
                return this[key]?.ToString();
            } else {
                return null;
            }
        }

        public bool Bool(string key, bool defaultValue) {
            if (this.Keys.Contains(key)) {
                return bool.Parse(this[key].ToString());
            } else {
                return defaultValue;
            }
        }


        public int Int(string key, int defaultValue, bool allowEmptyString = false) {
            if (this.Keys.Contains(key)) {

                if (allowEmptyString == true && this[key].ToString() == string.Empty) {
                    return defaultValue;
                }

                return int.Parse(this[key].ToString());
            } else {
                return defaultValue;
            }
        }

       


        public Price Price(string key) {
            if (this.Keys.Contains(key)) {
                return new Price(this[key].ToString());
            } else {
                return null;
            }
        }
    }

    /// <summary>
    /// PropertiesKey attribute for defining defaults when using
    /// the Properties class as a Property.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class PropertiesKeyAttribute : System.Attribute {
        public string Keys = null;
        public string DefaultValue = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertiesKeyAttribute"/> class.
        /// This defines the default keys used by the Properties underlying data structure, or
        /// when used by the form builder.
        /// </summary>
        /// <param name="keys">The keys as a comma-seperated string.</param>
        public PropertiesKeyAttribute(string keys, string formType = "text", string defaultValue = null) {
            if (formType != "text") throw new BackendException("deprecated", "PropertiesKeyAttribute param formType is no longer supported");
            if (defaultValue != null) throw new BackendException("deprecated", "PropertiesKeyAttribute param defaultValue is no longer supported");
            this.Keys = keys;
            this.DefaultValue = defaultValue; // deprecated
        }
    }
    
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class PropertiesConfigKeyAttribute : PropertiesKeyAttribute
    {
        public PropertiesConfigKeyAttribute(string configKeyToSearchFor) : base(null) {
            // The configKeyToSearchFor will match any config key starting with it
            // This way, multiple users (ie modules) can specify a config for it without
            //TODO: BUG: Environment settings (ie XYZ-NervesTest) won't work properly with this
            var allConfigValues = new List<string>();
            var matchingConfigKeys = Core.Config.Find(configKeyToSearchFor).ToList();
            foreach(var matchingConfigKey in matchingConfigKeys) {
                var value = Core.Config.GetStringSetting(matchingConfigKey);
                allConfigValues.Add(value);
            }
            var allKeys = string.Join(",", allConfigValues);
            var uniqueKeys = allKeys.Split(',').ToList().Distinct();
            this.Keys = string.Join(",",uniqueKeys);
        }
    }

    /// <summary>
    /// Allows for automatic conversion from a JSON string to a Properties object.
    /// </summary>
    /// <seealso cref="System.ComponentModel.TypeConverter" />
    public class PropertiesConverter : TypeConverter {
        
        public override bool IsValid(ITypeDescriptorContext context, object value) {
            if(value is string) {
                try {
                    JsonConvert.DeserializeObject<Dictionary<string, object>>((string)value);
                    return true;
                } catch(Exception e) {
                    return false;
                }
            }
            return base.IsValid(context, value);
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType) {
            if (sourceType == typeof(string)) return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value) {
            if (value is string) {
                return new Properties((string)value);
            } else {
                return base.ConvertFrom(context, culture, value);
            }
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType) {
            if (destinationType == typeof(JValue)) return true;
            if (destinationType == typeof(string)) return true;
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType) {
            if (destinationType == typeof(JValue)) {
                return new JValue("");
            } else if (destinationType == typeof(string)) {
                return (value as Properties)?.ToHumanReadableString();
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }




    }

}