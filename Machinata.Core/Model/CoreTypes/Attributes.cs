using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Model.CoreTypes {

    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class DecimalPrecisionAttribute : System.Attribute 
    {
        public enum DecimalPrecisionType {
            Default,
            Price,
            Custom
        }

        public byte Precision;
        public byte Scale;
        
        public DecimalPrecisionAttribute(DecimalPrecisionType type, int precision = 0, int scale = 0) {
            if(type == DecimalPrecisionType.Default) {
                Precision = (byte)Core.Config.DefaultDecimalPrecision;
                Scale = (byte)Core.Config.DefaultDecimalScale;
            } else if(type == DecimalPrecisionType.Price) {
                Precision = (byte)Core.Config.PriceDecimalPrecision;
                Scale = (byte)Core.Config.PriceDecimalScale;
            } else if(type == DecimalPrecisionType.Custom) {
                if(precision <= 0) throw new Exception("Invalid DecimalPrecision precision!");
                if(scale <= 0) throw new Exception("Invalid DecimalPrecision scale!");
                Precision = (byte)precision;
                Scale = (byte)scale;
            } else {
                throw new Exception("Invalid DecimalPrecisionType!");
            }
        }
    }

    /* NOTE: attempt to provide an attribute for setting navigation prop table names (didnt work yet)
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class TableNameAttribute : System.Attribute 
    {
        public string Name;
        
        public TableNameAttribute(string name) {
            this.Name = name;
        }
    }*/
}
