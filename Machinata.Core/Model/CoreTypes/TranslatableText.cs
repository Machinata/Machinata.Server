using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Model {

    /// <summary>
    /// Complex type that represents a series of key values.
    /// This class can be used as a property type of a model object to enable
    /// custom and flexible key-values. The internal data structure is
    /// automatically serialized and deserialized as JSON, which is stored in the DB.
    /// 
    /// By using the PropertiesKey attribute, one can set default key values.
    /// 
    /// The order of all the Properties keys is alphabetical.
    /// </summary>
    [ComplexType]
    [TypeConverter(typeof(TranslatableTextConverter))]
    public class TranslatableText {

        /// <summary>
        /// The internal JSON Data. 
        /// DO NOT SET THIS.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public string Data { get; private set; }

        /// <summary>
        /// The internal serialed dictionary.
        /// </summary>
        private SortedDictionary<string, object> _serializedData;

        /// <summary>
        /// Initializes a new instance of the <see cref="TranslatableText"/> class.
        /// </summary>
        public TranslatableText() : base() {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TranslatableText"/> class
        /// using a JSON key-value string.
        /// </summary>
        /// <param name="json">The json.</param>
        public TranslatableText(string json) : base() {
            this.Data = json;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TranslatableText"/> class.
        /// The keyvalue dictionary is automatically serialized for JSON.
        /// </summary>
        /// <param name="keysAndValues">The keys and values.</param>
        public TranslatableText(Dictionary<string,object> keysAndValues) : base() {
            this.Data = Core.JSON.Serialize(keysAndValues, false);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TranslatableText"/> class.
        /// The dynamic object is automatically serialized for JSON.
        /// </summary>
        /// <param name="keysAndValues">The keys and values.</param>
        public TranslatableText(dynamic keysAndValues) : base() {
            this.Data = Core.JSON.Serialize(keysAndValues, false);
        }
      
        private void _syncBackingData() {
            this.Data = JsonConvert.SerializeObject(_getSerializedData());
        }

        /// <summary>
        /// Gets the serialized data dictionary.
        /// On first call the JSON data is automatically loaded. If there is no JSON data available, then
        /// a empty dictonary is created.
        /// </summary>
        /// <returns></returns>
        private SortedDictionary<string, object> _getSerializedData() {
            if (_serializedData == null) {
                if (this.Data != null) {
                    _serializedData = JsonConvert.DeserializeObject<SortedDictionary<string, object>>(this.Data);
                } else {
                    _serializedData = new SortedDictionary<string, object>();
                }
            }
            return _serializedData;
        }


        /// <summary>
        /// Shortcut method to get all keys for the object.
        /// </summary>
        /// <value>
        /// The keys.
        /// </value>
        public SortedDictionary<string,object>.KeyCollection Keys {
            get {
                return _getSerializedData().Keys;
            }
        }

        public void Clear() {
            this.Data = null;
        }

        public void LoadFromJSONString(string jsonString) {
            this.Data = jsonString;
        }

      

        /// <summary>
        /// Gets or sets the <see cref="System.Object"/> with the specified key using the 
        /// array index operator.
        /// </summary>
        /// <value>
        /// The <see cref="System.Object"/>.
        /// </value>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public object this[string key] {
            get {
                if(_getSerializedData().Keys.Contains(key)) return _getSerializedData()[key];
                else return null;
            }
            set {
                _getSerializedData()[key] = value;
                _syncBackingData();
            }
        }


        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance as a JSON string.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() {
            var data = _getSerializedData();
            // return null if no keys or no data
            if (data != null && data.Any()) {
                return JsonConvert.SerializeObject(_getSerializedData());
            }
            return null;
        }

        public SortedDictionary<string, object> ToDictionary() {
            return _getSerializedData();
        }

        public JObject ToJObject() {
            var ret = new JObject();
            var data = _getSerializedData();
            foreach(var key in data.Keys) {
                var val = data[key];
                if (val == null) {
                    ret[key] = null;
                } else if (val.GetType() == typeof(JArray)) {
                    ret[key] = val as JToken;
                } else if (val.GetType() == typeof(JObject)) {
                    ret[key] = val as JObject;
                } else {
                    ret[key] = new JValue(val);
                }
            }
            return ret;
        }

        public static TranslatableText FromJSON(JToken val) {
            var result = new TranslatableText();
            if (val != null) {
                result.LoadFromJSONString(val.ToString());
            }
            return result;
        }

        public TranslatableText Clone() {
            var dictionary = this.ToDictionary();
            var result = new TranslatableText(dictionary);
            return result;
        }

        public string ToHumanReadableString(string seperator = "\n") {
            var data = _getSerializedData();
            // Return null if no keys or no data
            if (data == null || !data.Any()) return null;
            // Compile
            List<string> keyVals = new List<string>();
            foreach (var key in data.Keys) {
                var val = data[key];
                if (val != null && !string.IsNullOrEmpty(val?.ToString())) keyVals.Add(key + ": " + val);
            }
            // Validate
            if (keyVals.Count == 0) return null;
            // CSV
            return string.Join(seperator, keyVals);
        }

        public void Delete(string language) {
            _getSerializedData().Remove(language);
            _syncBackingData();
        }


        /// <summary>
        /// Get for the defined language or default if not available
        /// </summary>
        public string GetForLanguage(string language) {
            if (this.Keys.Contains(language) && this[language] != null) {
                return this[language]?.ToString();
            } else {
                if (this.Keys.Contains("default")) {
                    return this["default"]?.ToString();
                } else {
                    return null;
                }
            }
        }

        /// <summary>
        /// Tries to get by language, than default than any other
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public string GetForLanguageOrAny(string language) {
            var val = GetForLanguage(language);
            if (val != null) return val;

            // Try others
            foreach (var lang in Core.Config.LocalizationSupportedLanguages.Where(l => l != "default" && l != language)) {
                val = this[lang]?.ToString();
                if (val != null) {
                    return val;
                }
            }
            return null;
        }





        /// <summary>
        /// </summary>
        public string GetDefault() {
            // First try explicit default value
            if (this.Keys.Contains("default") && this["default"] != null) {
                return this["default"]?.ToString();
            } else {
                // Return any value
                if (this.Keys.Count > 0) {
                    return this[this.Keys.First()]?.ToString();
                } else {
                    // No value, return null
                    return null;
                }
            }
        }

      

        /// <summary>
        /// </summary>
        public void SetForLanguage(string language, string val) {
            this[language] = val;
        }

        /// <summary>
        /// </summary>
        public void SetDefault(string val) {
            this["default"] = val;
        }


        /// <summary>
        /// </summary>
        public void SetForAllSupportedLanguages( string val) {
            foreach (var lang in Core.Config.LocalizationSupportedLanguages) {
                if (lang == Core.Config.LocalizationDefaultLanguage) {
                    SetDefault(val);
                } else {
                    this[lang] = val;
                }
            }
        }

        public static TranslatableText GetTranslateableText(Func<string, string> getValueForLang) {
            var translatableText = new TranslatableText();
            foreach (var lang in Core.Config.LocalizationSupportedLanguages) {
                var valueForLang = getValueForLang(lang);
                if (lang == Core.Config.LocalizationDefaultLanguage) {
                    translatableText.SetDefault(valueForLang);
                } else {
                    translatableText.SetForLanguage(lang, valueForLang);
                }
            }
            return translatableText;
        }



    }



    /// <summary>
    /// Allows for automatic conversion from a JSON string to a TranslatableText object.
    /// </summary>
    /// <seealso cref="System.ComponentModel.TypeConverter" />
    public class TranslatableTextConverter : TypeConverter {
        
        public override bool IsValid(ITypeDescriptorContext context, object value) {
            if(value is string) {
                try {
                    JsonConvert.DeserializeObject<Dictionary<string, object>>((string)value);
                    return true;
                } catch(Exception e) {
                    return false;
                }
            }
            return base.IsValid(context, value);
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType) {
            if (sourceType == typeof(string)) return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value) {
            if (value is string) {
                return new TranslatableText((string)value);
            } else {
                return base.ConvertFrom(context, culture, value);
            }
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType) {
            if (destinationType == typeof(JValue)) return true;
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType) {
            if (destinationType == typeof(JValue)) {
                return new JValue("");
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

}