using Machinata.Core.Exceptions;
using Machinata.Core.Model.CoreTypes;
using Machinata.Core.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Model {


    /// <summary>
    /// A complex type that represents a monetary token made up of Cents and Currency.
    /// This allows for perfect precision keeping to two decimal places while mainting
    /// a internal concept of the currency value as well, with possible conversion between
    /// currencies theoretically possible.
    /// The class provides operator overloads for addition, subtraction and multiplication.
    /// Note: For Entity Framework objects, you must always initialize ComplexTypes in the
    /// constructor.
    /// </summary>
    [ComplexType]
    [TypeConverter(typeof(PriceConverter))]
    public class Price : IComparable {

        #region Constants

        /// <summary>
        /// If true, we always perform rounding on the price value.
        /// Any value hitting the DB is already rounded to the DBs precision, so
        /// the DB will never do a rounding.
        /// 
        /// Note: This should always be true!
        /// </summary>
        public const bool PRICE_ROUNDED_INLINE = true; // do not change

        #endregion

        #region Private Internal Properties

        private decimal? _value;
        private string _currency;

        #endregion
        
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Price"/> class with no value or currency.
        /// </summary>
        public Price() {
            _value = null;
            _currency = null;
        }

        public Price(decimal? val = null, string currency = null) {
            // First set the actual fields to null
            _value = null;
            _currency = null;
            // Now set via properties to make sure they are validated fully
            this.Value = val;
            this.Currency = currency;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Price"/> class using a string.
        /// A value must be given, however the currency is optional.
        /// The method works by splitting on each word and parsing each word. If the word
        /// is a decimal or int, then this is assumed to be the coin value (not cents). 
        /// Otherwise the word is assumed to be currency.
        /// If either the coin value or currency is found more than once, this constructor
        /// will fail.
        /// </summary>
        /// <param name="currencyAndCoins">The currency and coins.</param>
        /// <exception cref="System.Exception">
        /// price has too many parts to it.
        /// or
        /// two values where found.
        /// or
        /// two values where found.
        /// or
        /// two currencies where found.
        /// or
        /// no valid value was given.
        /// or
        /// </exception>
        public Price(string currencyAndValue) {
            // Special handling if null string
            if (string.IsNullOrEmpty(currencyAndValue)) {
                _value = null;
                _currency = null;
            } else {
                // We have something, trim it
                currencyAndValue = currencyAndValue.Trim();
                
                try {
                    var segs = currencyAndValue.Split(' ');
                    if (segs.Length > 2) throw new Exception("price has too many parts to it.");
                    bool didSetValue = false;
                    bool didSetCurrency = false;
                    foreach(var segRaw in segs) {
                        // Create formatted seg...
                        var seg = segRaw;
                        // Change thousands seperator
                        if (!string.IsNullOrEmpty(Core.Config.CurrencyThousandsSeparatorChar)) seg = seg.Replace(Core.Config.CurrencyThousandsSeparatorChar, ",");
                        // Change .- to .00
                        if (seg.EndsWith(".-")) seg = seg.ReplaceSuffix(".-", ".00");
                        if (!string.IsNullOrEmpty(Core.Config.CurrencyZeroCentsChar) && seg.EndsWith("." + Core.Config.CurrencyZeroCentsChar)) seg = seg.ReplaceSuffix("." + Core.Config.CurrencyZeroCentsChar, ".00");
                        // Decimal?
                        if(seg.IsDecimal()) {
                            if(didSetValue) throw new Exception("two values where found.");
                            this.Value = decimal.Parse(seg);
                            didSetValue = true;
                        } else if(seg.IsInt64()) {
                            if(didSetValue) throw new Exception("two values where found.");
                            this.Value = int.Parse(seg);
                            didSetValue = true;
                        } else {
                            // Must be currency
                            if(didSetCurrency) throw new Exception("two currencies where found.");
                            this.Currency = seg;
                            didSetCurrency = true;
                        }
                    }
                    if(!didSetValue) throw new Exception("no valid value was given.");
                    if (!didSetCurrency) this.Currency = null;
                }catch (Exception e) {
                    throw new Exception($"Could not parse the price '{currencyAndValue}': "+e.Message);
                }
            }
        }

        #endregion

        #region Public Datastore Properties

        /// <summary>
        /// Gets or sets the value in decimal.
        /// I.E to set 10.50 USD, you would set 10.50m.
        /// On MySql, this property maps to the default type 'DECIMAL(18,2)',
        /// where
        ///     DECIMAL(M,D)
        ///     M is the maximum number of digits (the precision). It has a range of 1 to 65. 
        ///     D is the number of digits to the right of the decimal point (the scale). It has a range of 0 to 30 and must be no larger than M. 
        /// See https://dev.mysql.com/doc/refman/5.7/en/precision-math-decimal-characteristics.html
        /// </summary>
        /// <value>
        /// The cents.
        /// </value>
        [Column]
        [DecimalPrecision(DecimalPrecisionAttribute.DecimalPrecisionType.Price)]
        public decimal? Value {
            get {
                return _value;
            } set {
                if(PRICE_ROUNDED_INLINE) {
                    // Debugging
                    //if(value.HasValue && value != Decimal.Round(value.Value, Core.Config.PriceDecimalScale)) {
                    //    var dbg = $"{value} vs {Decimal.Round(value.Value, Core.Config.PriceDecimalScale)}";
                    //}
                    // Round to same precision as on DB
                    if (value.HasValue) _value = Decimal.Round(value.Value, Core.Config.PriceDecimalScale);
                    else _value = value;
                } else {
                    // No rounding, decimal precision is different from what is stored on DB
                    #pragma warning disable CS0162
                    _value = value;
                    #pragma warning restore CS0162
                }
            }
        }

        /// <summary>
        /// Gets or sets the currency.
        /// The currency is always stored in uppercase, and must be supported by the underlying project.
        /// </summary>
        /// <value>
        /// The currency.
        /// </value>
        [Column]
        [MaxLength(3)]
        public string Currency {
            get {
                return _currency;
            } set {
                // Set default if null
                if (string.IsNullOrEmpty(value)) value = Core.Config.CurrencyDefault;
                // Make upper
                value = value.ToUpper();
                // Validate
                if (!Core.Config.CurrencySupported.Contains(value)) throw new BackendException("invalid-currency",$"The currency {value} is not supported.");
                // Set underlying value
                _currency = value.ToUpper();
            }
        }

        #endregion

        #region Public Not-Mapped or Derived Properties
        

        #endregion
        
        #region Public Helper Methods
        

        public bool HasValue {
            get {
                return this.Value.HasValue && this.Currency != null;
            }
        }

        /// <summary>
        /// Converts the price into its stwo main components Value and Currency in JSON format:
        /// ```{ Value: 0.33, Currency: "CHF"}```
        /// </summary>
        public JObject ToJSON() {
            var result = new JObject(); // { new { Value = _value, Currency = _currency } };
            result["Value"] = _value;
            result["Currency"] = _currency;
            return result;
        }

        /// <summary>
        /// Loads a new Price object from a JSON in format.
        /// ```{ Value: 0.33, Currency: "CHF"}```
        /// </summary>
        /// <param name="jobject"></param>
        /// <returns></returns>
        public static Price FromJSON(JToken jobject) {
            var result = new Price();
            if (jobject["Currency"] == null) {
                throw new Exception("Price JSON has no currency");
            }
            if (jobject["Value"] == null) {
                throw new Exception("Value JSON has no currency");
            }

            result.Currency = jobject["Currency"]?.ToString();
            var value = jobject["Value"]?.ToObject<decimal>();
            if (value != null) {
                result.Value = value;
            }
            return result;
        }

        /// <summary>
        /// Converts the price in a nicely formatted string using Core.Config.CurrencyFormat.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() {
            return this.ToString(Core.Config.CurrencyFormat, true, true); // default frontend format
        }

        /// <summary>
        /// Convert the price in a nicely formatted string using the format.
        /// If no format is provided, then the default frontend fromat Core.Config.CurrencyFormat is used.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="changeThousandsSeperator">if set to <c>true</c> [change thousands seperator].</param>
        /// <param name="changeZeroCents">if set to <c>true</c> [change zero cents].</param>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public string ToString(string format = null, bool changeThousandsSeperator = true, bool changeZeroCents = true) {
            // Set the format
            if (format == null) format = Core.Config.CurrencyFormat; // default frontend format
            // Make sure we have a set value
            if (!this.Value.HasValue) return null;
            // Convert using config format (includes currency id, ie USD)
            var ret = (System.String.Format(CultureInfo.InvariantCulture, format, this.Value, Currency));
            // Change thousands seperator
            if (changeThousandsSeperator && !string.IsNullOrEmpty(Core.Config.CurrencyThousandsSeparatorChar)) ret = ret.Replace(",", Core.Config.CurrencyThousandsSeparatorChar);
            // Change .00 to .-, for example
            if (changeZeroCents && !string.IsNullOrEmpty(Core.Config.CurrencyZeroCentsChar) && ret.EndsWith(".00")) ret = ret.ReplaceSuffix(".00", "." + Core.Config.CurrencyZeroCentsChar);
            return ret;
        }

        /// <summary>
        /// Convert a price for an api by providing the format.
        /// The changeThousandsSeperator is always false in this method.
        /// The changeZeroCents is always false in this method.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        public string ToStringForAPI(string format) {
            return this.ToString(format, false, false);
        }
        

        public int ToCents() {
            _validateValue(this, "converted to cents");
            decimal cents = 100 * this.Value.Value;
            return (int) cents;
        }

        /// <summary>
        /// Converts the specified currency.
        /// 
        /// This uses a set of defined dynamic configs to lookup the currency conversion.
        /// 
        /// For example, we have a price:
        /// 
        /// var price = new Price(10,"CHF")
        ///                          ^source
        /// 
        /// which we convert to EUR:
        /// 
        /// price.ConvertToCurrency("EUR")
        ///                           ^target
        /// 
        /// This will looking for the following key:
        /// 
        /// CurrencyConversionRateCHFtoEUR=0.87
        ///                  source^    ^target
        ///                  
        /// Returned value is: source * CurrencyConversionRateCHFtoEUR
        ///                  
        /// Alternatively, if this key does not exist, the reverse can be used:
        /// 
        /// CurrencyConversionRateEURtoCHF
        ///                  target^    ^source
        ///                  
        /// Note that in the second situation, the source and target are reversed and are thus
        /// calculated differently
        /// 
        /// Returned value is: source / CurrencyConversionRateEURtoCHF
        /// 
        /// </summary>
        /// <param name="currency">The currency.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException">
        /// </exception>
        /// <exception cref="System.Exception">
        /// </exception>
        public Price ConvertToCurrency(string currency) {
            // Validate
            if (this.Currency == currency) return this.Clone();
            if (currency == null || currency == "") throw new Exception("Cannot convert currency because target currency is null!");
            if (this.Currency == null || this.Currency == "") throw new Exception("Cannot convert currency because price has no currency!");
            if (this.HasValue == false) throw new NotImplementedException();
            
            // Init
            var currencyFrom = this.Currency;
            var currencyTo = currency;

            // CurrencyConversionRateCHFtoEUR
            var configKeyA = $"CurrencyConversionRate{currencyFrom}to{currencyTo}";
            var configRateA = Core.Config.Dynamic.Decimal(configKeyA, false);
            if(configRateA != null) {
                if (configRateA <= 0m) throw new Exception($"The currency conversion rate {configKeyA} ({configRateA}) is not valid!");
                return new Price(this.Value.Value * configRateA, currencyTo);
            }
            
            // CurrencyConversionRateEURtoCHF
            var configKeyB = $"CurrencyConversionRate{currencyTo}to{currencyFrom}";
            var configRateB = Core.Config.Dynamic.Decimal(configKeyB, false);
            if(configRateB != null) {
                if (configRateB <= 0m) throw new Exception($"The currency conversion rate {configKeyB} ({configRateB}) is not valid!");
                return new Price(this.Value.Value / configRateB, currencyTo);
            } else {
                throw new Exception($"No currency conversion rate exists ({configKeyA} or {configKeyB})!");
            }
            
        }

        /// <summary>
        /// Converts to a different currency using the specified rounding or the project default rounding
        /// specified by Core.Config.CurrencyConversionDefaultRounding
        /// 
        /// Note: if the target currency is the same as the source currency, then no rounding is done.
        /// </summary>
        /// <param name="currency">The currency.</param>
        /// <param name="roundTo">The round to.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Cannot convert to rounded currency because rountTo has no value.</exception>
        public Price ConvertToRoundedCurrency(string currency, decimal? roundTo = null) {
            // Validate
            if (this.Currency == currency) return this.Clone(); // we dont round if the currency is the same
            // Convert
            var converted = ConvertToCurrency(currency);
            // Round
            return converted.RoundTo(roundTo);
        }

        /// <summary>
        /// Rounds to the nearest rounding bandwidth specified by roundTo.
        /// If no rountTo is specified, then Core.Config.CurrencyConversionDefaultRounding
        /// is used.
        /// </summary>
        /// <param name="roundTo">The round to.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Cannot convert to rounded currency because rountTo has no value.</exception>
        public Price RoundTo(decimal? roundTo = null) {
            // Init
            if (roundTo == null) roundTo = Core.Config.CurrencyConversionDefaultRounding.Value;
            if (!roundTo.HasValue) throw new Exception("Cannot convert to rounded currency because rountTo has no value.");
            decimal factor = 1m / roundTo.Value;
            // Rounds down
            // See for alternative: http://www.xbeat.net/vbspeed/i_BankersRounding.htm
            decimal newVal = System.Math.Round(this.Value.Value * factor) / factor;
            return new Model.Price(newVal,this.Currency);
        }

        /// <summary>
        /// Clones this instance.
        /// Important: Since Price is not a struct, you must be very careful to clone this class
        /// when copying references.
        /// </summary>
        /// <returns></returns>
        public Price Clone() {
            return new Price(this.Value, this.Currency);
        }

        public void Set(Price price) {
            this.Value = price.Value;
            this.Currency = price.Currency;
        }


        #endregion

        #region Operators


        /*
            Note: deprecated for safety
        public static implicit operator Price(Int64 val) {
            return new Price(val);
        }

            Note: deprecated for safety
        public static implicit operator Price(decimal val) {
            return new Price(val);
        }*/
        
        public static Price operator +(Price p1, Price p2) {
            // Operator: Price + Price
            //TODO: validate cents overflow?
            _validateValue(p1, "added (left)");
            _validateValue(p2, "added (right)");
            _validateCurrencyCompatibility(p1, p2);
            return new Price(p1.Value.Value + p2.Value.Value, p1.Currency);
        }
        
        public static Price operator -(Price p1, Price p2) {
            // Operator: Price - Price
            //TODO: validate cents overflow?
            _validateValue(p1, "subtracted (left)");
            _validateValue(p2, "subtracted (right)");
            _validateCurrencyCompatibility(p1, p2);
            return new Price(p1.Value.Value - p2.Value.Value, p1.Currency);
        }
        
        public static Price operator -(Price p1) {
            // Operator: -Price
            //TODO: validate cents overflow?
            _validateValue(p1, "subtracted (left)");
            return new Price(-p1.Value.Value , p1.Currency);
        }
        
        public static Price operator *(Price p1, Price p2) {
            // Operator: Price * Price
            //TODO: validate cents overflow?
            _validateValue(p1, "multiplied (left)");
            _validateValue(p2, "multiplied (right)");
            _validateCurrencyCompatibility(p1, p2);
            return new Price(p1.Value.Value * p2.Value.Value, p1.Currency);
        }
        
        public static Price operator *(Price p1, int factor) {
            // Operator: Price + int
            //TODO: validate cents overflow?
            _validateValue(p1, "multiplied (left)");
           
            return new Price(p1.Value.Value * factor, p1.Currency);
        }
        
        public static Price operator *(int factor, Price p1) {
            // Operator: int * Price
            //TODO: validate cents overflow?
            _validateValue(p1, "multiplied (right)");
           
            return new Price(p1.Value.Value * factor, p1.Currency);
        }

        public static Price operator *(Price p1, decimal? factor) {
            // Operator: Price * decimal?
            //TODO: validate cents overflow?
            _validateValue(p1, "multiplied (left)");
           
            return new Price(p1.Value.Value * factor, p1.Currency);
        }
        
        public static Price operator / (Price p1, int divisor) {
            // Operator: Price / int
            //TODO: validate cents overflow?
            _validateValue(p1, "divided (left)");
            return new Price(p1.Value.Value / divisor, p1.Currency);
        }
        
        public static bool operator > (Price p1, Price p2) {
            // Operator: Price > Price
            //TODO: validate cents overflow?
            _validateValue(p1, "greater than (left)");
            _validateValue(p2, "smaller than (right)");
            _validateCurrencyCompatibility(p1, p2);
            return p1.Value.Value > p2.Value.Value;
        }

        public static bool operator >= (Price p1, Price p2) {
            // Operator: Price >= Price
            //TODO: validate cents overflow?
            _validateValue(p1, "greater than or equal (left)");
            _validateValue(p2, "smaller than or equal (right)");
            _validateCurrencyCompatibility(p1, p2);
            return p1.Value.Value >= p2.Value.Value;
        }
        
        public static bool operator < (Price p1, Price p2) {
            // Operator: Price < Price
            //TODO: validate cents overflow?
            _validateValue(p1, "smaller than (left)");
            _validateValue(p2, "greater than (right)");
            _validateCurrencyCompatibility(p1, p2);
            return p1.Value.Value<  p2.Value.Value;
        }

        public static bool operator <= (Price p1, Price p2) {
            // Operator: Price <= Price
            //TODO: validate cents overflow?
            _validateValue(p1, "smaller than or equal (left)");
            _validateValue(p2, "greater than or equal (right)");
            _validateCurrencyCompatibility(p1, p2);
            return p1.Value.Value <=  p2.Value.Value;
        }


        #endregion

        #region Comparer

        public int CompareTo(object obj) {
            Price other = (Price)obj;
            _validateCurrencyCompatibility(this, other);
            _validateValue(this, "compare to (self)");
            _validateValue(other, "compare to (other)");
            return decimal.Compare(this.Value.Value, other.Value.Value);
        }

        #endregion

        #region Private Helper Methods

        private static void _validateValue(Price p, string msg) {
            if(!p.Value.HasValue) {
                throw new Exception($"The price could not be {msg} because it does not have a value.");
            }
        }

        private static void _validateCurrencyCompatibility(Price p1, Price p2) {
            if(p1.Currency != p2.Currency) {
                throw new Exception($"The currenices '{p1.Currency}' and '{p2.Currency}' are not compatible.");
            }
        }

     
        #endregion

        
        
    }

    #region LINQ Extensions

    public static class PriceLINQExtensions {
        
        public static Price Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, Price> selector) {
            Price total = null;
            foreach (var item in source) {
                var val = selector(item);
                if (total == null) total = new Price(0, val.Currency);
                total += val;
            }
            return total;
        }

        public static Price SumIgnoringEmptyValues<TSource>(this IEnumerable<TSource> source, Func<TSource, Price> selector) {
            Price total = null;
            foreach (var item in source) {
                var val = selector(item);
                if (val == null || val.HasValue == false) continue;
                if (total == null) total = new Price(0, val.Currency);
                total += val;
            }
            return total;
        }

    }

    #endregion

    #region PriceConverter

    /// <summary>
    /// Allows for automatic conversion from a string or int to a Price object.
    /// If a string is passed, then the currency and the decimal value (coins) is accepted. The currency
    /// is optional.
    /// If a int passed, then cents is assumed and the default currency is set.
    /// </summary>
    /// <seealso cref="System.ComponentModel.TypeConverter" />
    public class PriceConverter : TypeConverter {
        
        public override bool IsValid(ITypeDescriptorContext context, object value) {
            if(value is string) {
                try {
                    new Price((string)value);
                    return true;
                } catch(Exception e) {
                    return false;
                }
            }
            return base.IsValid(context, value);
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType) {
            //if (sourceType == typeof(int)) return true;
            if (sourceType == typeof(string)) return true;
            //if (sourceType == typeof(decimal)) return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value) {
            if (value is int) {
                throw new NotImplementedException();
                return new Price((int)value);
            } else if (value is decimal) {
                throw new NotImplementedException();
                return new Price((decimal)value);
            } else if (value is string) {
                return new Price((string)value);
            } else {
                return base.ConvertFrom(context, culture, value);
            }
        }

    }

    #endregion

}