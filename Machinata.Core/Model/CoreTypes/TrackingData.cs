using Machinata.Core.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUglify.Html;
using NUglify.JavaScript.Syntax;
using Org.BouncyCastle.Asn1.Mozilla;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Model {

    /// <summary>
    /// 
    /// </summary>
    
    public class TrackingData : Properties {

        public const string RemoteIPKey = "RemoteIP";
        public const string UserAgentKey = "UserAgent";
        public const string UserLanguagesKey = "UserLanguages";
        public const string ReferrerKey = "Referrer";
        public const string CampaignKey = "Campaign";
        public const string SourceKey = "Source";
        public const string CouponKey = "Coupon";
        public const string ContentKey = "Content";
        public const string MediumKey = "Medium";
        public const string TermKey = "Term";




        [NotMapped]
        public string RemoteIP {
            set {
                this[RemoteIPKey] = value;
            }
            get {
                return this[RemoteIPKey]?.ToString();
            }
        }

        [NotMapped]
        public string Source {
            set {
                this[SourceKey] = value;
            }
            get {
                return this[SourceKey]?.ToString();
            }
        }

        [NotMapped]
        public string Referrer {
            set {
                this[ReferrerKey] = value;
            }
            get {
                return this[ReferrerKey]?.ToString();
            }
        }


        [NotMapped]
        public string UserAgent {
            set {
                this[UserAgentKey] = value;
            }
            get {
                return this[UserAgentKey]?.ToString();
            }
        }

        [NotMapped]
        public string Campaign {
            set {
                this[CampaignKey] = value;
            }
            get {
                return this[CampaignKey]?.ToString();
            }
        }

        [NotMapped]
        public string Medium {
            set {
                this[MediumKey] = value;
            }
            get {
                return this[MediumKey]?.ToString();
            }
        }


        [NotMapped]
        public string Term {
            set {
                this[TermKey] = value;
            }
            get {
                return this[TermKey]?.ToString();
            }
        }


        [NotMapped]
        public string Content {
            set {
                this[ContentKey] = value;
            }
            get {
                return this[ContentKey]?.ToString();
            }
        }

        [NotMapped]
        public string Coupon {
            set {
                this[CouponKey] = value;
            }
            get {
                return this[CouponKey]?.ToString();
            }
        }




        public void Populate(HttpContext context) {
            this.RemoteIP = Core.Util.HTTP.GetRemoteIP(context);
            this.UserAgent = Core.Util.HTTP.GetUserAgent(context);
            this.Referrer = context.Request.Params["referrer"]?.ToString();
            this.Source = context.Request.Params["utm_source"]?.ToString();
            this.Campaign = context.Request.Params["utm_campaign"]?.ToString();
            this.Medium = context.Request.Params["utm_medium"]?.ToString();
            this.Term = context.Request.Params["utm_term"]?.ToString();
            this.Content = context.Request.Params["utm_content"]?.ToString();
            this.Coupon = context.Request.Params["coupon"]?.ToString();
            //TODO: tracking ids, such as Google Client Id? Is this even possible?
        }

    }

}