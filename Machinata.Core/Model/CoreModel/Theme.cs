using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using System.Reflection;
using System.Diagnostics.Contracts;
using System.Runtime.InteropServices.WindowsRuntime;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Machinata.Core.Lifecycle;
using Machinata.Core.Util;

namespace Machinata.Core.Model {
    
    //[Serializable()]
    //[ModelClass] 
    public partial class Theme : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

       
        #endregion


        #region Constants ////////////////////////////////////////////////////////////////////////

    
        #endregion


        #region Constructors //////////////////////////////////////////////////////////////////////

        public Theme() {
            this.Properties = new Properties();
            this.Variables = new Properties();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Index(IsUnique = true)]
        [MaxLength(128)]
        public string Name { get; set; } 

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string Title { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string Inherits { get; set; }

        [Column]
        //[FormBuilder(Forms.Admin.LISTING)]
        public bool SetAsDefault { get; set; } 
        
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public Properties Properties { get; set; }


        [Column]
        public Properties Variables { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        /// <summary>
        /// Flag to mark this theme is from an external theme loading system
        /// </summary>
        public bool External { get; set; }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        [OnProjectSeed]
        private static void OnProjectSeed(ModelContext context, string dataset) {
          
        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////



        public static Theme GetThemeByName(string name) {
            // Cached?
            var cacheName = GetCachename(name);
            Theme cachedTheme = null;
            if(Core.Config.ThemeCacheEnabled) cachedTheme = GetCachedThemeByName(cacheName);
            if (cachedTheme != null) {
                return cachedTheme;
            }

            // External Theme
            if (Core.Config.ThemeCacheEnabled == false ) {
                cachedTheme = GetExternalThemeByName(cacheName);
            }
            if (cachedTheme != null) {
                return cachedTheme;
            }

            // Not cached -> load the theme from json
            Theme ret = null;

            // Load via json file
            string filePath = Core.Util.Files.GetHotSwappableFile(GetFilePath(name),null);
            try {
                ret = LoadThemeFromJSON(filePath);
            }catch(Exception e) {
                throw new Exception($"Could not load the theme with the name {name}",e);
            }

            // Final exception
            if (ret == null) {
                _logger.Warn($"Theme {name} not found using default fallbacktheme");
                throw new BackendException("theme-not-found", $"The theme '{name}' could not be found");
            } else {
                AddCachedThemeByName(cacheName, ret);
            }

            return ret;

        }

      
        /// <summary>
        /// Get the theme from the cache, null if not found
        /// thread safe
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Theme GetCachedThemeByName(string name) {
            lock (_themesCacheLock) {
                if (name == null) {
                    throw new Exception("GetCachedThemeByName called with <null>");
                }

                if (_themesCache.ContainsKey(name)) {
                    return _themesCache[name];
                }
            }
            return null;
        }

        

        /// <summary>
        /// Retrieves all cached themes
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Theme> GetCachedThemes() {
            lock (_themesCacheLock) {
                return _themesCache.Values;
            }
        }
        /// <summary>
        /// Loads a theme from the given filePath from a json file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="theme">if the theme is null a new instance will be created with name and title,
        /// if the theme is not null all new properties from the current json will be loaded into the theme leaving the already existing properties as is </param>
        /// <returns></returns>
        public static Theme LoadThemeFromJSON(string filePath) {
            // Does the file exist?
            if (File.Exists(filePath) == false) {
                return null;
            }

            // Init
            Theme theme;
            JObject jObject;
            var jsonCache = new Dictionary<string, JObject>();

            // Load JSON
            try {
                jObject = _LoadThemeFromJSON_loadOrGetJSONFromCache(jsonCache, filePath);
            } catch (Exception e) {
                throw new Exception("Could not load the theme JSON (invalid JSON or invalid file).",e);
            }

            // Create theme object
            theme = new Theme();
            try {
                theme.Name = jObject["name"]?.ToString();
                theme.Title = jObject["title"]?.ToString();
                theme.Inherits = jObject["inherits"]?.ToString();
                theme.Variables = new Properties();
            } catch (Exception e) {
                throw new Exception("Could not initialize the theme (invalid JSON object?).", e);
            }

            try { 
                // Variables
                _LoadThemeFromJSON_loadThemeVariablesFromJSONRecursivly(jsonCache, filePath, theme);
            } catch (Exception e) {
                throw new Exception("Could not load the theme variables.", e);
            }
            try {
                // Properties
                _LoadThemeFromJSON_loadThemePropertiesFromJSONRecursivly(jsonCache, filePath, theme);
            } catch (Exception e) {
                throw new Exception("Could not load the theme properties.", e);
            }
            try {
                // Properties as variables
                bool hasMissingVariablePlaceholders;
                string missingVariablePlaceholders;
                int counter = 0;
                const int maxLoops = 6;
                do {
                    // Loop maxLoops to resolve multi layered variables 
                    _LoadThemeFromJSON_loadThemePropertiesAsVariables(theme, out hasMissingVariablePlaceholders, out missingVariablePlaceholders);

                    if (++counter >= maxLoops) {
                        throw  new Exception($"Only {maxLoops} levels of nested referencing are allowed! Either too many nested references have occured, or some variables ({missingVariablePlaceholders}) could not be resolved.");
                    }
                } while (hasMissingVariablePlaceholders == true);
            } catch (Exception e) {
                throw new Exception("Could not load the theme properties variables.", e);
            }

            return theme;
        }

        private static JObject _LoadThemeFromJSON_loadOrGetJSONFromCache(Dictionary<string,JObject> cache, string filepath) {
            if(cache.ContainsKey(filepath)) {
                return cache[filepath];
            } else {
                var jObject = JObject.Parse(File.ReadAllText(filepath));
                cache[filepath] = jObject;
                return jObject;
            }
        }

        private static object _LoadThemeFromJSON_injectThemeVariablesIntoPropValue(Theme theme, KeyValuePair<string, object> prop) {
            if (prop.Value != null && prop.Value.GetType() == typeof(string)) {
                var stringValue = prop.Value.ToString();
                var variableName = stringValue.ReplacePrefix("$", string.Empty);
                if (stringValue.StartsWith("$") == true && theme.Variables.Keys.Contains(variableName)) {
                    //theme.Properties[prop.Key] = variables[variableName];
                    return theme.Variables[variableName];
                } else {
                    return prop.Value;
                }
            } else {
                return prop.Value;
            }
        }

        private static Theme _LoadThemeFromJSON_loadThemeVariablesFromJSONRecursivly(Dictionary<string, JObject> jsonCache, string filePath, Theme theme = null) {
            // NOTE: Recursive method

            // Does the file exist?
            if (File.Exists(filePath) == false) {
                return theme;
            }

            // Load JSON
            var jObject = _LoadThemeFromJSON_loadOrGetJSONFromCache(jsonCache, filePath);

            // Variables
            var variables = _LoadThemeFromJSON_loadKeyValues(jObject, "variables");
           
            // Load variables into theme
            if (variables != null && variables.Any()) {
                foreach(var variable in variables) {
                    if (theme.Variables.Keys.Contains(variable.Key) == false) {
                        theme.Variables[variable.Key] = variable.Value;
                    }
                }
            }
              
            // Load all inherited themes
            if (jObject["inherits"] != null) {
                var inherits = jObject["inherits"].Select(i => i.ToString());
                foreach (var inherit in inherits) {
                    var inheritFilePath = Core.Util.Files.GetHotSwappableFile(GetFilePath(inherit), null);
                    theme = _LoadThemeFromJSON_loadThemeVariablesFromJSONRecursivly(jsonCache, inheritFilePath, theme);
                }
            }

            return theme;
        }

        private static Theme _LoadThemeFromJSON_loadThemePropertiesFromJSONRecursivly(Dictionary<string, JObject> jsonCache, string filePath, Theme theme = null) {
            // NOTE: Recursive method

            // Does the file exist?
            if (File.Exists(filePath) == false) {
                return theme;
            }

            // Load JSON
            var jObject = _LoadThemeFromJSON_loadOrGetJSONFromCache(jsonCache, filePath);

            // Properties
            var properties = _LoadThemeFromJSON_loadKeyValues(jObject, "properties");


            // We are at an ancestor of a theme we're actually loading  
            foreach (var prop in properties) {
                if (theme.Properties.Keys.Contains(prop.Key) == false) {
                    theme.Properties[prop.Key] = _LoadThemeFromJSON_injectThemeVariablesIntoPropValue(theme, prop);
                }
            }

            // Load all inherited themes
            if (jObject["inherits"] != null) {
                var inherits = jObject["inherits"].Select(i => i.ToString());
                foreach (var inherit in inherits) {
                    var inheritFilePath = Core.Util.Files.GetHotSwappableFile(GetFilePath(inherit), null);
                    theme = _LoadThemeFromJSON_loadThemePropertiesFromJSONRecursivly(jsonCache, inheritFilePath, theme);
                }
            }


            return theme;
        }


        /// <summary>
        /// Replaces refernece property values in the theme.Properties  eg. "highlight-color": "$highlight-color2", "highlight-color2": "black"
        /// will become "highlight-color": "black", "highlight-color2": "black"
        /// </summary>
        /// <param name="theme"></param>
        /// <returns></returns>
        private static Theme _LoadThemeFromJSON_loadThemePropertiesAsVariables(Theme theme, out bool hasMissingVariablePlaceholders, out string missingVariablePlaceholders) {
            var propertiesWithRefs = new Dictionary<string, object>();
            var propertiesAsVars = new Dictionary<string, object>();
            missingVariablePlaceholders = "";
            hasMissingVariablePlaceholders = false;

            foreach (var propKey in theme.Properties.Keys) {
                var value = theme.Properties[propKey]?.ToString();
                if (value != null && value.StartsWith("$") == false) {
                    propertiesAsVars[propKey] = value; // this is a property with a concrete value
                } else {
                    propertiesWithRefs[propKey] = value; // this is a property with a reference to another property
                }
            }

            _logger.Debug("all propertiesWithRefs: " + theme.Name);
            foreach (var propKey in propertiesWithRefs.Keys) {
                _logger.Debug(propKey + ": " + propertiesWithRefs[propKey]);
            }


            // Go through each property with a reference
            foreach (var propKey in propertiesWithRefs.Keys) {
                var value = theme.Properties[propKey]?.ToString();
                if (value != null && value.StartsWith("$") == true) { // should always be true
                    var refKey = value.TrimStart("$"); // trim to extract the referenced property key
                    if (propertiesAsVars.ContainsKey(refKey) == false) {
                        // mark for next round
                        hasMissingVariablePlaceholders = true;
                        missingVariablePlaceholders += refKey+",";
                    } else {
                        theme.Properties[propKey] = propertiesAsVars[refKey]; // read the actual value from the referenced property
                    }
                }
            }

            return theme;
        }

        private static SortedDictionary<string, object> _LoadThemeFromJSON_loadKeyValues(JObject jObject, string key) {
            var obj = jObject[key];
            if (obj != null) {
                var dictionary = JsonConvert.DeserializeObject<SortedDictionary<string, object>>(obj.ToString());
                return dictionary;
            }
            return new SortedDictionary<string, object>();
        }

        /// <summary>   
        /// Gets a Themes from the cache
        /// </summary>
        /// <returns></returns>
        internal static IDictionary<string,Theme> GetCache() {
            lock (_themesCacheLock){
                return _themesCache.ToDictionary(d => d.Key, v => v.Value);
            }
        }

        /// <summary>
        /// Load all available Themes in /static/themes and adds them to the cache
        /// 
        /// </summary>
        /// <returns></returns>
        public static void LoadAllThemes() {

            var basePath = GetThemesFilesPath();

            // No themes
            if (Directory.Exists(basePath)== false) {
                return;
            }

            _logger.Info("Loading themes from: " + basePath);

            var paths = Directory.GetFiles(basePath, "*.json");

            foreach (var path in paths) {
                try {
                    _logger.Info("Loading theme from: " + path);
                    var content = File.ReadAllText(path);
                    var jObject = JObject.Parse(content);
                    var name = jObject["name"]?.ToString();
                    GetThemeByName(name);
                } catch (Exception e) {
                    throw new BackendException("theme-loading-error", "Could not load theme from: " + path, e);
                }

            }
        }

        /// <summary>
        /// TODO: do we need this?
        /// </summary>
        [Obsolete]
        public static void ClearThemeCache() {
            lock (_themesCacheLock) {
                _themesCache.Clear();
            }
        }

        public static void ClearExternalThemesCache() {
            lock (_themesCacheLock) {
                foreach (var external in _themesCache.Where(c =>c.Value.External == true).ToList()){
                    _themesCache.Remove(external.Key);
                }
            }
        }

        [OnApplicationStartup]
        public static void OnApplicationStartup() {
            Theme.LoadAllThemes();

            Theme.LoadExternalThemes();
        }

        /// <summary>
        /// Loads themes outside the usual .json system with a ThemeProviderAttribute
        /// </summary>
        public static void LoadExternalThemes() {

            ClearExternalThemesCache();

            var themes = GetExternalThemes();
            foreach(var theme in themes) {
                AddCachedThemeByName(theme.Name, theme);
            }
        }

        /// <summary>
        /// Loads all external Themes
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<Theme> GetExternalThemes() {

            try {
                var methods = Core.Reflection.Methods.GetMachinataMethodsWithAttribute(typeof(Core.Model.ThemeProviderAttribute));
                var newThemes = new List<Theme>();
                foreach (var mi in methods) {
                    var ret = mi.Invoke(null, null) as List<Theme>;
                    foreach (var theme in ret) {
                        theme.External = true;
                        newThemes.Add(theme);
                    }
                }
                return newThemes;
            }
            catch {
                return new List<Theme>();
            }
        }

        //public static Theme GetExternalThemeByName(string name) {
        //    lock (_themesCacheLock) {
        //        if (_themesCache.Any(c => c.Key == name && c.Value.External == true)) {
        //            var external = _themesCache.FirstOrDefault(c => c.Value.External == true && c.Key == name);
        //            return external.Value;
        //        }
        //        return null;

        //    }
        //}

        public static Theme GetExternalThemeByName(string name) {
            var themes = GetExternalThemes();
            var theme = themes.FirstOrDefault(t => t.Name == name);
            return theme;
        }


        #endregion


        #region Private Methods ///////////////////////////////////////////////////////////////////

        /// <summary>
        /// Get the file path for a theme name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetFilePath(string name) {
            var fileName = name + ".json";
            var filePath = GetThemesFilesPath() + Core.Config.PathSep + fileName;
            return filePath;
        }

        private static string GetThemesFilesPath() {
            return Core.Config.StaticPath + Core.Config.PathSep + "themes";
        }

        /// <summary>
        /// Adds to the cache
        /// thread save
        /// </summary>
        /// <param name="name"></param>
        /// <param name="theme"></param>
        private static void AddCachedThemeByName(string name, Theme theme) {
            lock (_themesCacheLock) {
                _themesCache[name] = theme;
            }
        }

        private static string GetCachename(string name) {
            return name;
        }


        #endregion


        #region Private Variables /////////////////////////////////////////////////////////////////

        private static object _themesCacheLock = new object();
        private static IDictionary<string, Theme> _themesCache = new Dictionary<string, Theme>();

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


  
}
