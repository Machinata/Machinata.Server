using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using System.Web;
using System.Data.Entity.Infrastructure;
using System.Net;
using Machinata.Core.API;

namespace Machinata.Core.Model {

    /// <summary>
    /// API Keys
    /// Access to API's are given through API keys which can be generated for any user. The API key inherits
    /// the same access groups and policies for which user it is assigned to.
    /// Generation and management of API keys is easily integrated by linking to the module Machinata.Module.APIKeys.
    /// API Keys are integrated directly in the core, however they are disabled by default. To
    /// enable the use of authentication using API Keys one must add the following config, or include the module ```Machinata.Module.APIKeys```
    /// in your project:
    /// </summary>
    /// <namespace>Machinata.Core.API</namespace>
    /// <example>
    /// <!-- API Keys -->
    /// <add key = "APIKeysEnabled" value="true" />
    /// </example>
    [Serializable()]
    [ModelClass]
    public partial class APIKey : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public APIKey() {
            // Parameterless constructure is required for reflection...
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [MaxLength(512)]
        [Required]
        [FormBuilder]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string Title { get; set; } 

        [Column]
        [ForeignKey("UserId")]
        [InverseProperty("APIKeys")]
        [Required]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public User User { get; set; } 

        [Column]
        public int? UserId { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public bool Enabled { get; set; } 

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public DateTime? Expires { get; set; } 

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public bool LogAllRequests { get; set; } 

        [Column]
        [MaxLength(128)]
        [Index]
        [Required]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public string KeyId { get; set; } 
        
        [Column]
        [MaxLength(512)]
        public string KeySecretEncrypted { get; set; } 


        
        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////
        
        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [MaxLength(512)]
        [NotMapped]
        public string KeySecret {
            get {
                return Core.Encryption.DefaultEncryption.DecryptString(this.KeySecretEncrypted);
            }
            set {
                this.KeySecretEncrypted = value == null ? null : Core.Encryption.DefaultEncryption.EncryptString(value);
            }
        }
        
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [NotMapped]
        public bool IsValid {
            get {
                if (this.Enabled == false) return false; // Deactivated
                if (this.Expires != null) {
                    if (DateTime.UtcNow >= this.Expires) return false; // Expired
                }
                return true; // Valid
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public override void Populate(IPopulateProvider populateProvider, FormBuilder form) {
            base.Populate(populateProvider, form);
            
            if (this.KeyId == null) this.KeyId = GenerateNewKeyId();
            if (this.KeySecret == null) this.KeySecret = GenerateNewKeySecret();
        }

        public static string GenerateNewKeyId() {
            return Guid.NewGuid().ToString().Replace("-", "") + Guid.NewGuid().ToString().Replace("-", "") + Guid.NewGuid().ToString().Replace("-", "") + Guid.NewGuid().ToString().Replace("-", "");
        }

        public static string GenerateNewKeySecret() {
            return Guid.NewGuid().ToString().Replace("-", "") + Guid.NewGuid().ToString().Replace("-", "") + Guid.NewGuid().ToString().Replace("-", "") + Guid.NewGuid().ToString().Replace("-", "");
        }

        /// <summary>
        /// Helper method to get a webclient with the api secret/id prepard in the header
        /// </summary>
        /// <param name="resource"></param>
        /// <param name="ignoreSSL"></param>
        /// <returns></returns>
        public WebClient GetWebclient(string resource, bool ignoreSSL = false) {
            var webclient = new WebClient();

            var authenticationHeader = APIAuth.GenerateAuthenticationHeader(
               keyId: this.KeyId,
               keySecret: this.KeySecret,
               method: "POST",
               resource: resource,
              timestamp: Core.Util.Time.GetUTCMillisecondsFromDateTime(DateTime.UtcNow));
            webclient.Headers[HttpRequestHeader.ContentType] = "application/json";
            webclient.Headers.Add("Authentication", authenticationHeader);

            if (ignoreSSL == true) {
                Core.Util.HTTP.IgnoreSSLCertificates();
            }
            return webclient;
        }
        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextAPIKeyExtenions {
        
        public static DbSet<APIKey> APIKeys(this Core.Model.ModelContext context) {
            return context.Set<APIKey>();
        }

        public static IQueryable<APIKey> Active(this IQueryable<APIKey> query) {
            var now = DateTime.UtcNow;
            return query.Where(e => 
                e.Enabled == true && 
                (e.Expires == null || now < e.Expires)
            );
        }

    }

    #endregion
}
