using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using Machinata.Core.Handler;

namespace Machinata.Core.Model {

    public partial class ContentNode {


        private static Dictionary<string, ContentNode> _GetCachedContentNodeAndChildrenForPathCache = new Dictionary<string, ContentNode>();



        public static ContentNode GetCachedContentNodeAndChildrenForPath(string cmsPath, bool throw404IfNotExists = true, ModelContext db = null, bool ignoreHidden = false) {

            var cacheKey = $"{cmsPath}";
            if (_GetCachedContentNodeAndChildrenForPathCache.ContainsKey(cacheKey)) {
                return _GetCachedContentNodeAndChildrenForPathCache[cacheKey];
            }

            var node = db.ContentNodes()
                .Include(nameof(ContentNode.Children) + "." + nameof(ContentNode.Children))
                .SingleOrDefault(n => n.Path.ToLower() == cmsPath.ToLower() && n.NodeType == ContentNode.NODE_TYPE_NODE && (n.Published == true || ignoreHidden));
            if (node == null && throw404IfNotExists) throw new Backend404Exception("not-found", $"The CMS page {cmsPath} could not be found or is not published.");
            
            _GetCachedContentNodeAndChildrenForPathCache[cacheKey] = node;
            
            return node;
        }
    }
}
