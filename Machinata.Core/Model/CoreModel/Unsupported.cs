

namespace Machinata.Core.Model {
    /// <summary>
    /// Mark a aaramter as unsupported
    /// </summary>
    public enum Unsupported {
        Parameter = 100
    }
}