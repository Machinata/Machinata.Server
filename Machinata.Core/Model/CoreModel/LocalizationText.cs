using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;

namespace Machinata.Core.Model {
    
    [Serializable()]
    [ModelClass] 
    public partial class LocalizationText : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        #region Constructors //////////////////////////////////////////////////////////////////////

        public LocalizationText() {
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        public string Value { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        public string TextId { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        public string Language { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        public string Package { get; set; }



        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////


        [NotMapped]
        public string Source
        {
            get { return "Custom Config"; }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion


        #region Application Lifecycle///////////////////////////////////////////////////////////////

        [Core.Lifecycle.OnApplicationStartup]
        public static void OnApplicationStartup() {
            //TODO:@micha @dan this breaks the seed on a new database
            if (Core.Config.LocalizationEnableDatabase) {
                using (var db = Core.Model.ModelContext.GetModelContext(null)) {
                    Core.Localization.Text.LoadAllLocalizationTextsFromDB(db);
                }
            }
        }
        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
        public static LocalizationText FindLocalizationText(ModelContext db, string textId, string language) {
            var dbText = db.LocalizationTexts().SingleOrDefault(lt => lt.TextId == textId && lt.Language == language);
            return dbText;
        }
        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }


    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextLocalizationTextExtenions {
        public static DbSet<LocalizationText> LocalizationTexts(this Core.Model.ModelContext context) {
            return context.Set<LocalizationText>();
        }
    }

    #endregion
}
