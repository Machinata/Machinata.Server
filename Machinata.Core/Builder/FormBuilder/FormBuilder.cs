using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {

    public class Forms {

        public const string EMPTY = "empty";
        
        public class Order {
            public const int BEFORE = -100;
            public const int AFTER = +100;
        }

        public class Admin {
            public const string WILDCARD = "*";
            public const string EMPTY = "";
            public const string VIEW = "/view";
            public const string LISTING = "/listing";
            public const string EDIT = "/edit";
            public const string LISTEDIT = "/list-edit";
            public const string CREATE = "/create";
            public const string EXPORT = "/export";
            public const string IMPORT = "/import";
            public const string SELECTION = "/selection";
            public const string TOTAL = "/total";
            public const string PDF = "/pdf";
            public const string PDFv2 = "/pdfv2";
            public const string EMAIL = "/email";
            public const string INVOICE = "/invoice";

        }

        public class Groups {
            public const string GENERAL = "";
            public const string SETTINGS = "Settings";

            public class Order {
                public const string GENERAL = "0010";
                public const string SETTINGS = "0020";
                public const string ENTITY = "0100";
                public const string ENTITY_SUBGROUP = "0200";

            }
        }

        public class Frontend {
            public const string CREATE = "/frontend/create";
            public const string VIEW = "/frontend/view";
            public const string EDIT = "/frontend/edit";
            public const string LISTING = "/frontend/listing";
            public const string SELECTION = "/frontend/selection";
            public const string TOTAL = "/frontend/total";
            public const string JSON = "/frontend/json";
        }

        public class API {
            public const string LISTING = "/api/listing";
            public const string CREATE = "/api/create";
            public const string VIEW = "/api/view";
            public const string EDIT = "/api/edit";
        }

        public class System {
            public const string LISTING = "/system/listing";
            public const string HASH_KEY = "/system/hash-key";
            public const string JSON = "/system/json";
            public const string VIEW = "/system/view";
            public const string DUPLICATE = "/system/duplicate";
        }

        public class Button {
            public const string SAVE = "{text.form-save}";
            public const string SEND = "{text.form-send}";
        }
        // TODO in core a project? needs some kind of configuration logic in the 
        public class KETS {
            public const string FORM = "/kets/form";
        }
    }

    public class FormBuilder {

        

        public class FormButton {
            public string Value;
            public string Name;
        }

        /// <summary>
        /// If defined, form names will have this appended to it.
        /// </summary>
        public string FormNamespace;
        
        public string Id;
        public string Method = "post";
    
        public List<string> PropertiesToExclude;
        public List<string> PropertiesToInclude;
        public List<CustomFormBuilderProperty> CustomProperties;
        public string ContentSource = null;

        private List<FormButton> _buttons;

        public FormBuilder(string id = Forms.Admin.WILDCARD) {
            this._buttons = new List<FormButton>();
            this.Id = id;
            if (this.Id == null) this.Id = Forms.Admin.EMPTY;
        }

        public FormBuilder Custom(CustomFormBuilderProperty customProperty) {
            if (this.CustomProperties == null) this.CustomProperties = new List<CustomFormBuilderProperty>();
            this.CustomProperties.Add(customProperty);
            return this;
        }

        public FormBuilder Custom(string name, string formName, string formType, object value = null, bool required = false, bool readOnly = false, string autoComplete = null, string tooltip = null, string group = null, string groupOrder = null) {
            return this.Custom(new CustomFormBuilderProperty(this) {
                Name = name,
                FormName = formName,
                FormType = formType,
                FormRequired = required,
                FormValue = value,
                FormReadOnly = readOnly,
                FormAutoComplete = autoComplete,
                FormTooltip = tooltip,
                PropertyGroup = group,
                PropertyGroupOrder = groupOrder
            });
        }

        public FormBuilder DropdownList(string name, string formName, List<KeyValuePair<string, string>> options, object value = null, bool required = false, string tooltip = null) {
            return this.Custom(new CustomFormBuilderProperty(this) {
                Name = name,
                FormName = formName,
                FormType = "list",
                FormRequired = required,
                FormValue = value,
                FormOptions = options,
                FormTooltip = tooltip
            });
        }

        /// <summary>
        /// Please use DropdownListEntities to avoid confusion with DropdownList<T>
        /// </summary>
        /// <param name="name"></param>
        /// <param name="entities"></param>
        /// <param name="selected"></param>
        /// <param name="required"></param>
        /// <param name="tooltip"></param>
        /// <returns></returns>
        [Obsolete]
        public FormBuilder DropdownList(string name, IEnumerable<ModelObject> entities, ModelObject selected = null, bool required = false, string tooltip = null) {
            var options = new List<KeyValuePair<string, string>>();
            foreach (var entity in entities) {
                options.Add(new KeyValuePair<string, string>(entity.ToString(),entity.PublicId));
            }
            return this.DropdownList(name, name, options, selected?.PublicId, required, tooltip);
        }

        public FormBuilder DropdownListEntities(string name, IEnumerable<ModelObject> entities, ModelObject selected = null, bool required = false, string tooltip = null) {
            var options = new List<KeyValuePair<string, string>>();
            foreach (var entity in entities) {
                options.Add(new KeyValuePair<string, string>(entity.ToString(), entity.PublicId));
            }
            return this.DropdownList(name, name, options, selected?.PublicId, required, tooltip);
        }

        public FormBuilder DropdownList<T>(string name, IEnumerable<T> values, Enum selected = null, bool required = false, string tooltip = null) {
            var options = new List<KeyValuePair<string, string>>();
            foreach (var entity in values) {
                options.Add(new KeyValuePair<string, string>(entity.ToString(), entity.ToString()));
            }
            return this.DropdownList(name, name, options, selected?.ToString(), required, tooltip);
        }

        public FormBuilder DropdownList<T>(string name, string formName, IEnumerable<T> values, Enum selected = null, bool required = false, string tooltip = null) {
            var options = new List<KeyValuePair<string, string>>();
            foreach (var entity in values) {
                options.Add(new KeyValuePair<string, string>(entity.ToString(), entity.ToString()));
            }
            return this.DropdownList(name, formName, options, selected?.ToString(), required, tooltip);
        }

        public FormBuilder CheckboxList(string name, string formName, List<KeyValuePair<string, string>> options, object value = null, bool required = false, string tooltip = null) {
            return this.Custom(new CustomFormBuilderProperty(this) {
                Name = name,
                FormName = formName,
                FormType = "checkboxlist",
                FormRequired = required,
                FormValue = value,
                FormOptions = options,
                FormTooltip = tooltip
            });
        }

        public FormBuilder RadioList(string name, string formName, List<KeyValuePair<string, string>> options, object value = null, bool required = false, string tooltip = null) {
            return this.Custom(new CustomFormBuilderProperty(this) {
                Name = name,
                FormName = formName,
                FormType = "radiolist",
                FormRequired = required,
                FormValue = value,
                FormOptions = options,
                FormTooltip = tooltip
            });
        }



        /// <summary>
        /// Creates a selection list for a list of types
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="types">The types.</param>
        /// <returns></returns>
        public FormBuilder SelectionListForTypes(string name, string formName,string value, IEnumerable<Type> types, Func<Type,string> getTitle = null) {
            var select = new CustomFormBuilderProperty(this);
            select.Name = name;
            select.FormName = formName;
            select.FormType = "select";
            select.FormValue = value;
            select.FormOptions = new List<KeyValuePair<string, string>>();
            foreach (var type in types) {
                var title = type.Name;
                var val = type.Name;
                if (getTitle != null) {
                    title = getTitle(type);
                }
                select.FormOptions.Add(new KeyValuePair<string, string>(title, val)); 
            }
            this.Custom(select);
            return this;
        }

       

        public FormBuilder Button(string name, string value) {
            //in rendering form if _button is empty, use default button, else use the first specified here
            var button = new FormButton(){ Name = name, Value = value };
            _buttons.Add(button);
            return this;
        }

        public FormBuilder Namespace(string formNamespace) {
            this.FormNamespace = formNamespace;
            return this;
        }

        public FormBuilder Hidden(string formName, string formValue) {
            return this.Custom(new CustomFormBuilderProperty(this) {
                Name = formName,
                FormName = formName,
                FormType = "hidden",
                FormRequired = true,
                FormValue = formValue
            });
        }

        public FormBuilder IncludeStandard() {
            this.Include(nameof(ModelObject.PublicId));
            this.Include(nameof(ModelObject.Created));
            return this;
        }

        public FormBuilder IncludeCreated() {
            this.Include(nameof(ModelObject.Created));
            return this;
        }

        public FormBuilder Include(string name) {
            if (this.PropertiesToInclude == null) this.PropertiesToInclude = new List<string>();
            this.PropertiesToInclude.Add(name);
            return this;
        }

        public FormBuilder Exclude(string name) {
            if (this.PropertiesToExclude == null) this.PropertiesToExclude = new List<string>();
            this.PropertiesToExclude.Add(name);
            return this;
        }

      
        public void RequireSpecific() {
            if(this.Id == null) {
                throw new Exception("You must provide a form builder id when calling Populate()");
            }
            if (this.Id == "*") {
                throw new Exception("You must provide a specific (not *) form builder id when calling Populate()");
            }
        }



        public IEnumerable<FormButton> Buttons
        {
            get
            {
                return _buttons;
            }
        }


        // EXPERIMENTAL
        /*
        public IEnumerable<FormBuilderProperty> GetFormProperties(ModelObject object) {
            return new List<FormBuilderProperty>(); //TODO
        }

        public IEnumerable<FormBuilderProperty> GetMethodInfoProperties(ModelObject object) {
            return GetFormProperties().Select(p => p.MethodInfo); //TODO
        }
        */
    }
}
