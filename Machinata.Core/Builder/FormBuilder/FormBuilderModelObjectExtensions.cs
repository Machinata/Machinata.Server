using Machinata.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Builder {

    public static class FormBuilderModelObjectExtensions {
        
        /// <summary>
        /// Gets the properties for a specific form through the use of FormBuilder attributes.
        /// For example, specifying the form '/admin/database/entity' on a property will return 
        /// all properties that have such a FormBuilder attributes.
        /// Specifying '/admin/database/*' will match both '/admin/database/entity' and '/admin/database/table'.
        /// Additionally, one can specify * as the form path, which will return any form builder form.
        /// </summary>
        /// <param name="form">The form name or path.</param>
        /// <returns></returns>
        public static List<PropertyInfo> GetPropertiesForForm(this Model.ModelObject modelObject, FormBuilder form) {
            var formBuilderId = form.Id;
            var allProps = modelObject.GetProperties();
            var ret = allProps
                .Where(p =>
                    // Get all form builder custom properties 
                    p.GetCustomAttributes<FormBuilderAttribute>(true)
                    // Select all that match exactly, or if given form path ends with *, all that match up to the *
                    .Any(fba => formBuilderId == "*" || fba.Form.Id == formBuilderId || (formBuilderId.EndsWith("*") && fba.Form.Id.StartsWith(formBuilderId.Substring(0, formBuilderId.Length - 1))))
                );
            // Do include
            if(form.PropertiesToInclude != null) {
                var toInclude = allProps.Where(p => form.PropertiesToInclude.Contains(p.Name));
                ret = ret.Union(toInclude);
            }
            // Do exclude
            if (form.PropertiesToExclude != null) {
                ret = ret.Where(p => !form.PropertiesToExclude.Contains(p.Name));
            }
            return ret.OrderBy(p => p.GetSortKey(formBuilderId)).ToList();
        }

        public static bool ContainsProperty(this Model.ModelObject modelObject, FormBuilder form, string propertyName) {
            var properties = GetPropertiesForForm(modelObject, form);
            // does the property name from param exist in properties?
            var property = properties.FirstOrDefault(p => p.Name == propertyName);
            return property != null;
        }

    }
}
