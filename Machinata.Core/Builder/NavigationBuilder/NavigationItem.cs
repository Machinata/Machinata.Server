using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {
    
    public class NavigationItem {

        private List<NavigationItem> _siblings = new List<NavigationItem>();

        private List<NavigationItem> _aliases = new List<NavigationItem>();

        public string Language;

        public string Link;

        public string Path;

        public string Title;

        public string Classes;
        
        public string CustomPageTitle;


        public List<NavigationItem> Siblings {
            get {
                return _siblings;
            }
        }

        public List<NavigationItem> Aliases {
            get {
                return _aliases;
            }
        }

        public string PageTitle {
            get {
                if (this.CustomPageTitle != null) return this.CustomPageTitle;
                else return this.Title;
            }
        }

        public NavigationItem Sibling(NavigationItem item) {
            _siblings.Add(item);
            return this;
        }

        public NavigationItem Alias(NavigationItem item) {
            _aliases.Add(item);
            return this;
        }
    }

}
