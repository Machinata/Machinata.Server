using Machinata.Core.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Builder {

    /// <summary>
    /// A simple navigation builder for handlers that support it.
    /// The builder consists of navigation items which then can be used
    /// to build a full navigation stack.
    /// For example, a PageTemplateHanlder can use the builder to allow for
    /// syntax as the following:
    /// this.Navigation.Add("/admin/database","Database");
    /// this.Navigation.Add("/admin/database/list-entities","List Entities");
    /// </summary>
    public class NavigationBuilder {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        
        #region Constructors 

        public NavigationBuilder() {
            _items = new List<NavigationItem>();
        }

        #endregion

        #region Private Static Fields
        #endregion

        #region Private Fields

        private List<NavigationItem> _items = null;
        
        #endregion

        #region Public Fields
        
        public List<NavigationItem> Items {
            get {
                return _items;
            }
        }


        public int? MaxDisplayDepth = null;

        public string TitlePrefix = "";
        public string TitleSeparator = " - ";

        #endregion

        /// <summary>
        /// Adds the specified item to the navigation stack.
        /// This method returns the navigation guilder to allow for chaining.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public NavigationBuilder Add(NavigationItem item) {
            _items.Add(item);
            return this;
        }

        /// <summary>
        /// Shorthand method for adding a new item to the navigation stack.
        /// This method returns the navigation guilder to allow for chaining.
        /// </summary>
        /// <param name="link">The link.</param>
        /// <param name="title">The title.</param>
        /// <returns></returns>
        public NavigationBuilder Add(string link, string title, string customPageTitle = null, string classes = null) {
            return Add(new NavigationItem { Link = link, Title = title, CustomPageTitle = customPageTitle, Classes = classes } );
        }

        /// <summary>
        /// Adds the specified link with the title set to {text.[link]}.
        /// </summary>
        /// <param name="link">The link.</param>
        /// <returns></returns>
        public NavigationBuilder Add(string link) {
            return Add(link, "{text."+link+"}");
        }

        public NavigationBuilder LanguageAlias(string language, string link, string title, string customPageTitle = null, string classes = null) {
            _items.Last().Alias(new NavigationItem { Language = language, Link = link, Title = title, CustomPageTitle = customPageTitle, Classes = classes });
            return this;
        }

        public NavigationBuilder Alias(NavigationItem item) {
            _items.Last().Alias(item);
            return this;
        }

        public NavigationBuilder Sibling(NavigationItem item) {
            _items.Last().Sibling(item);
            return this;
        }

        public NavigationBuilder Sibling(string link, string title, string customPageTitle = null, string classes = null) {
            return Sibling(new NavigationItem { Link = link, Title = title, CustomPageTitle = customPageTitle, Classes = classes } );
        }

        public NavigationBuilder CompilePaths(PageTemplate template) {
            if (_items == null) return this;
            for (var i = 0; i < _items.Count; i++) {
                // Translate path 
                var parsedLink = Core.Localization.TextParser.ReplaceTextVariablesForData(template.Package, _items[i].Link, template.Language);
                // Root? If so use as absolute, otherwise append prev path
                if (parsedLink.StartsWith("/") || parsedLink.StartsWith("javascript:") || parsedLink.StartsWith("mailto:") || parsedLink.StartsWith("tel:") || i == 0) {
                    _items[i].Path = parsedLink;
                } else {
                    _items[i].Path = _items[i-1].Path + "/"  +parsedLink;
                }
                // Siblings
                foreach (var sibling in _items[i].Siblings) {
                    var parsedLink2 = Core.Localization.TextParser.ReplaceTextVariablesForData(template.Package, sibling.Link, template.Language);
                    if (parsedLink2.StartsWith("/") || parsedLink2.StartsWith("javascript:") || parsedLink2.StartsWith("mailto:") || parsedLink2.StartsWith("tel:") || i == 0) {
                        sibling.Path = parsedLink2;
                    } else {
                        sibling.Path = _items[i - 1].Path + "/" + parsedLink2;
                    }
                }
                // Aliases
                foreach (var alias in _items[i].Aliases) {
                    var parsedLinkAlias = Core.Localization.TextParser.ReplaceTextVariablesForData(template.Package, alias.Link, template.Language);
                    if (parsedLinkAlias.StartsWith("/") || parsedLinkAlias.StartsWith("javascript:") || parsedLinkAlias.StartsWith("mailto:") || parsedLinkAlias.StartsWith("tel:") || i == 0) {
                        // Alias starts with a new path (ie the full path or so was set)
                        alias.Path = parsedLinkAlias;
                    } else {
                        // Alias is relative, get the matching alias from the prev item
                        try {
                            var prevItem = _items[i - 1];
                            var prevItemMatchingAlias = prevItem.Aliases.SingleOrDefault(a => a.Language == alias.Language);
                            if (prevItemMatchingAlias == null) throw new Exception("language is missing in previous nav item");
                            alias.Path = prevItemMatchingAlias.Path + "/" + parsedLinkAlias;
                        }catch(Exception e) {
                            throw new Exception($"Could not compile NavigationBuilder path for alias {alias.Link} with language ${alias.Language}:"+e.Message,e);
                        }
                    }
                }
            } 
            return this;
        }

        public string PageTitle {
            get {
                if (this.Items == null || this.Items.Count == 0) return null;
                return this.Items.Last().PageTitle;
            }
        }

        public string Path {
            get {
                return this.Items.Last().Path;
            }
        }

        public string TitlePath {
            get {
                var navTitlePath = this.TitlePrefix;
                foreach (var item in this.Items) {
                    navTitlePath = item.PageTitle + (navTitlePath==""?"":this.TitleSeparator) + navTitlePath;
                }
                return navTitlePath;
            }
        }

        public List<NavigationItem> PreviousItems {
            get {
                if (_items.Count <= 1) return new List<NavigationItem>();
                return _items.Take(_items.Count - 1).ToList();
            }
        }

        public string PreviousPath {
            get {
                return this.PreviousItems.LastOrDefault()?.Path;
            }
        }
        public string PreviousTitle {
            get {
                return this.PreviousItems.LastOrDefault()?.Title;
            }
        }

        public NavigationItem LastItem {
            get {
                return this.Items.LastOrDefault();
            }
        }

        public List<NavigationItem> PreviousPreviousItems {
            get {
                if (_items.Count <= 2) return new List<NavigationItem>();
                return _items.Take(_items.Count - 2).ToList();
            }
        }

        public string PreviousPreviousPath {
            get {
                return this.PreviousPreviousItems.LastOrDefault()?.Path;
            }
        }
        
        public NavigationBuilder AutoSummarize(int maxLength) {
            foreach(var item in _items) {
                item.Title = Core.Util.String.CreateSummarizedText(item.Title, maxLength, true, true);
                foreach(var sibling in item.Siblings) {
                    sibling.Title = Core.Util.String.CreateSummarizedText(sibling.Title, maxLength, true, true);
                }
            }
            return this;
        }

        public NavigationItem SetTitleLastItem(string title) {
            var item = this.Items.LastOrDefault();
            if (item != null) {
                item.Title = title;
            }
            return item;
        }
    }
}
