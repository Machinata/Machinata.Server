using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
 
namespace Machinata.Core.Caching {


    /// <summary>
    /// A utility library for easily managing and cachable files and creating them on the fly if needed.
    /// </summary>
    public class FileCacheGenerator {

        /// <summary>
        /// Creates the file at cachePath using the fileFactory if it does not already exist. This method is thread-safe.
        /// </summary>
        /// <param name="cachePath">The cache path.</param>
        /// <param name="fileFactory">The file factory function.</param>
        /// <param name="forceCreate">if set to <c>true</c> [force create].</param>
        /// <returns></returns>
        public static string CreateIfNeeded(string cachePath, Action fileFactory, bool forceCreate = false, string mutextId = null) {
            // Get a mutex on the cachepath derived mutexkey to make sure two threads dont try to
            // create the cache at the same time
            Core.Threading.KeyedMutex mutex = null;
            if (mutextId == null) mutex = Core.Util.Mutex.CreateKeyedMutexForFilepath(cachePath);
            else mutex = Core.Util.Mutex.CreateKeyedMutexForId(mutextId);
            
            // Wait until it is safe to enter.
            mutex.Wait();

            try {
                // Exist?
                if (forceCreate || System.IO.File.Exists(cachePath) == false) {
                    // Make sure the dir exists
                    Directory.CreateDirectory(System.IO.Path.GetDirectoryName(cachePath));

                    // Call the factory method
                    fileFactory();
                }

            } finally {
                // Release the Mutex.
                mutex.Release();
            }

            return cachePath;
        }

    }
}
