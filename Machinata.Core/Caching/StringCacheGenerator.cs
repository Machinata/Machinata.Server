using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
 
namespace Machinata.Core.Caching {


    /// <summary>
    /// A utility library for easily managing and cachable string data and creating them on the fly if needed.
    /// </summary>
    public class StringCacheGenerator {

        /// <summary>
        /// Creates the file at cacheKey using the fileFactory if it does not already exist. This method is thread-safe.
        /// </summary>
        /// <param name="cacheKey">The cache key. Must be unique for the task at hand.</param>
        /// <param name="cacheFactory">The cache factory.</param>
        /// <param name="maxAge">The maximum age.</param>
        /// <param name="forceCreate">if set to <c>true</c> [force create].</param>
        /// <returns></returns>
        public static string CreateIfNeeded(string cacheKey, Func<string> cacheFactory, TimeSpan maxAge, bool forceCreate = false) {
            // Clean cache key
            cacheKey = Core.Util.String.CreateShortURLForName(cacheKey, false, true);
            string cachePath = Core.Config.CachePath + Core.Config.PathSep + "string" + Core.Config.PathSep + cacheKey + ".txt";

            // Get a mutex on the cacheKey derived mutexkey to make sure two threads dont try to
            // create the cache at the same time
            string ret = null;
            var mut = Core.Util.Mutex.CreateKeyedMutexForFilepath(cachePath);
            try {   
                // Wait until it is safe to enter.
                mut.Wait();

                // Re-create?
                bool recreate = false;
                if(!System.IO.File.Exists(cachePath)) {
                    recreate = true; // doesn't exist yet
                } else {
                    var fileDateTime = new System.IO.FileInfo(cachePath).LastWriteTimeUtc;
                    var maxDateTime = fileDateTime.Add(maxAge);
                    if (DateTime.UtcNow > maxDateTime) {
                        recreate = true; // its too old
                    }
                }

                if(forceCreate || recreate) {
                    
                    // Make sure the dir exists
                    Directory.CreateDirectory(System.IO.Path.GetDirectoryName(cachePath));
                    // Call the factory method
                    ret = cacheFactory();
                    // Write back
                    System.IO.File.WriteAllText(cachePath, ret);

                } else {

                    // Read back
                    ret = System.IO.File.ReadAllText(cachePath);

                }

            } finally {
                // Release the Mutex.
                mut.Release();
            }

            return ret;
        }

        public static void DestroyCaches(string searchPattern = null) {
            string cachePath = Core.Config.CachePath + Core.Config.PathSep + "string";
            foreach(var file in System.IO.Directory.EnumerateFiles(cachePath, searchPattern)) {
                System.IO.File.Delete(file);
            }
        }

    }
}
