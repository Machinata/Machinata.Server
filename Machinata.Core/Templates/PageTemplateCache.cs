using Machinata.Core.Exceptions;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Helper class for managing a cache of templates allowing for very fast lookups and loading.
    /// </summary>
    public class PageTemplateCache {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        /// <summary>
        /// The cached objects lookup table which allows direct access to a cached object using a key.
        /// </summary>
        public Dictionary<string, PageTemplate> _cachedObjects = new Dictionary<string, PageTemplate>();

        private bool _cacheContainsKey(string key) {
            return _cachedObjects.Keys.Contains(key);
        }

        public Dictionary<string, PageTemplate> GetCache() {
            return _cachedObjects.ToDictionary(c=>c.Key,c=>c.Value);
        }

        /// <summary>
        /// Gets a universal template key.
        /// </summary>
        /// <param name="packageName">Name of the package.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="extension">The extension.</param>
        /// <returns></returns>
        public static string GetTemplateKey(string packageName, string templateName, string extension) {
            return packageName + "_" + templateName + "_" + extension;
        }

        /// <summary>
        /// Gets the template key without the extension. Use this method only for searching for templates.
        /// </summary>
        /// <param name="packageName">Name of the package.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <returns></returns>
        public static string GetTemplateKey(string packageName, string templateName) {
            return packageName + "_" + templateName;
        }

        /// <summary>
        /// Checks if the template exists in the cache.
        /// </summary>
        /// <param name="packageName">Name of the package.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="extension">The extension.</param>
        /// <returns></returns>
        public bool Exists(string packageName, string templateName, string extension) {
            var cacheKey = GetTemplateKey(packageName, templateName, extension);
            return _cacheContainsKey(cacheKey);
        }

        /// <summary>
        /// Finds the specified template by cascading down the template path tree, finally lastly trying the
        /// defaults folder. First we see if it exists directly on the path, if it doesnt, we move down the path,
        /// if we still didnt find the template, we will try one last default path by using the 'defaults' folder
        /// in the package.
        /// </summary>
        /// <param name="packageName">Name of the package for which to search and get the template from.</param>
        /// <param name="fallbackPackageName">Name of the package to fallback to by default if the template cannot be found in the package.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="extension">The extension.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Could not find the template  + templateName</exception>
        public PageTemplate Find(string packageName, string fallbackPackageName, string templateName, string extension, bool throwException = true) {
            if(String.IsNullOrEmpty(fallbackPackageName)) {
                return _find(packageName, templateName, extension, throwException);
            } else {
                var ret = _find(packageName, templateName, extension, false);
                if (ret != null) return ret;

                ret = _find(fallbackPackageName, templateName, extension, throwException);
                return ret;
            }
        }

        public PageTemplate _find(string packageName, string templateName, string extension, bool throwException = true) {

            _logger.Trace("Searching for template " + packageName + "/" + templateName + extension);

            // Template names to search for in order
            // We build this list first, then check it for a first matching template
            // (ie first added names have priority)
            List<string> templateNamesToSearchInOrder = new List<string>();

            // Helper method to always add the overwrite versions
            void registerTemplateNameToSearchFor(string templateNameToRegister) {
                // Add first the overwrite versions...
                // Transforms
                //      "default/default/layout.content.content-rendered.node.image"
                // into
                //      "default/default/[overwrite]layout.content.content-rendered.node.image"
                //      "default/[overwrite]default/layout.content.content-rendered.node.image"
                //      "[overwrite]default/default/layout.content.content-rendered.node.image"
                {
                    var segments = templateNameToRegister.Split('/').ToList();
                    for (var s = segments.Count - 1; s >= 0; s--) {
                        var templateNameToRegisterWithOverwrite = "";
                        for (var i = 0; i < segments.Count; i++) {
                            if (i != 0) templateNameToRegisterWithOverwrite += "/";
                            if (i == s) templateNameToRegisterWithOverwrite += "[overwrite]";
                            templateNameToRegisterWithOverwrite += segments[i];
                        }
                        templateNamesToSearchInOrder.Add(templateNameToRegisterWithOverwrite);
                    }
                }
                // Add the regular template name
                templateNamesToSearchInOrder.Add(templateNameToRegister);
            }

            // Search if it exists as a subdir default to the current
            registerTemplateNameToSearchFor(templateName + "/default");

            // Now we see if it exists directly
            registerTemplateNameToSearchFor(templateName);

            // It doesnt, so we move down the path
            var currentNamePath = templateName.Split('/').ToList();
            while(currentNamePath.Count >= 2) {
                currentNamePath.RemoveAt(currentNamePath.Count - 2);
                string currentNamePathConcat = string.Join("/", currentNamePath.ToArray());
                // Check if it exists as default in subdir
                registerTemplateNameToSearchFor(currentNamePathConcat + "/default");
                // Check if it exists directly in subdir
                registerTemplateNameToSearchFor(currentNamePathConcat);
            }

            // We still didnt find the template, now we will try one last default path
            // At this point currentNamePath should be exactly one item
            string defaultNamePath = "default" + "/" + string.Join("/", currentNamePath.ToArray());
            registerTemplateNameToSearchFor(defaultNamePath);

            // Since some templates refer to others, we add fallbacks to non-overwrite methods
            {
                List<string> additionalTemplateNamesWithOverwriteStripped = new List<string>();
                foreach (var templateNameAttempt in templateNamesToSearchInOrder) {
                    if (templateNameAttempt.Contains("[overwrite]")) {
                        var templateNameWithoutOverwrite = templateNameAttempt.Replace("[overwrite]", "");
                        if(templateNamesToSearchInOrder.Contains(templateNameWithoutOverwrite) == false) additionalTemplateNamesWithOverwriteStripped.Add(templateNameWithoutOverwrite);
                    }
                }
                templateNamesToSearchInOrder.AddRange(additionalTemplateNamesWithOverwriteStripped);
            }

            //TODO: remove duplicates?
            //templateNamesToSearchInOrder = templateNamesToSearchInOrder.Distinct().ToList();

            // Now we have all our guesses in order, now
            // try to find a match...
            foreach (var templateNameAttempt in templateNamesToSearchInOrder) {
                if (Exists(packageName, templateNameAttempt, extension)) {
                    _logger.Trace("  Exists as "+ templateNameAttempt);
                    return Get(packageName, templateNameAttempt, extension);
                }
            }

            // Invalid template
            if (throwException) throw new Exception("Could not find the template " + templateName);
            else return null;
        }

        public List<PageTemplate> FindAll(string packageName, string templateName, string extension) {
            List<PageTemplate> ret = new List<PageTemplate>();
            string searchkey = GetTemplateKey(packageName, templateName);
            foreach (var k in _cachedObjects.Keys.Where(k => k.StartsWith(searchkey) && k.EndsWith(extension))) {
                _cachedObjects[k].LoadContentsIfNeeded();
                ret.Add(_cachedObjects[k]);
            }
            return ret;
        }

        /// <summary>
        /// Gets the specified template for the package. If the template does not exist
        /// in the cache it is loaded automatically.
        /// </summary>
        /// <param name="packageName">Name of the package.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="extension">The extension.</param>
        /// <returns></returns>
        public PageTemplate Get(string packageName, string templateName, string extension) {
            // Get the template from cache, if we have...
            PageTemplate cachedObj = _getFromCache(packageName, templateName, extension);
            cachedObj.LoadContentsIfNeeded();
            // Get unique copy
            var copy = cachedObj.Copy();
            // Force reload if no cahcing?
            if(!Core.Config.PageTemplateCacheEnabled || Core.Config.HotSwappingEnabled) {
                copy.Reload();
            }
            // Return
            return copy;
        }

        /// <summary>
        /// Gets the specified template for the package. If the template does not exist
        /// in the cache it is loaded automatically.
        /// </summary>
        /// <param name="packageName">Name of the package.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="extension">The extension.</param>
        /// <returns></returns>
        public PageTemplate _getFromCache(string packageName, string templateName, string extension) {
            // Get the template from cache, if we have...
            var cacheKey = GetTemplateKey(packageName, templateName, extension);
            PageTemplate cachedObj = null;
            if (!_cacheContainsKey(cacheKey)) {
                bool loadTemplateContents = true;
                //Note: following doesnt work because of inline transations which need to be loaded via templates...
                //if (!Core.Config.PageTemplateCacheEnabled || Core.Config.HotSwappingEnabled) {
                //    loadTemplateContents = false; // optimization: if we always force a reload anyways, then we don't need to do it here
                //}
                cachedObj = new PageTemplate(packageName, templateName, extension, null, loadTemplateContents);
                _cachedObjects[cacheKey] = cachedObj;
            } else {
                cachedObj = _cachedObjects[cacheKey];
            }
            return cachedObj;
        }

    }

    public partial class PageTemplate {

        /// <summary>
        /// The internal static template cache object.
        /// </summary>
        private static PageTemplateCache _cache = new PageTemplateCache();

        /// <summary>
        /// Provides access to a cache of templates that are loaded by the system. 
        /// Use PageTemplate.Cache.Find() to load a new template using cascading (standard).
        /// Use PageTemplate.Cache.Get() to load a specifc template using a fixed path.
        /// </summary>
        /// <value>
        /// The template cache helper class.
        /// </value>
        public static PageTemplateCache Cache { get { return _cache; } }

    }
}
