using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public partial class PageTemplate {

        /// <summary>
        /// Automatically inserts all the icon variables (ie {icon.xyz}) that are in this template...
        /// </summary>
        public void InsertIconVariables() {
            _logger.Trace("Inserting icon variables...");
            foreach(string var in _variables) {
                if(var.StartsWith("icon.")) {
                    _logger.Trace("  Found "+var);
                    this.InsertIconVariable(var);
                }
            }
        }

        public void InsertIconVariable(string variableName) {
            // Sanity
            if (!variableName.StartsWith("icon.")) throw new Exception("A icon variable must start with 'icon.'.");
            // Extract id
            var iconId = variableName.Substring(5);
            // Get bundle from theme?
            string iconBundleName = null;
            {
                var templateHandler = this.Handler as PageTemplateHandler;
                if (templateHandler != null) {
                    var theme = Theme.GetThemeByName(templateHandler.ThemeName);
                    if (theme.Properties["icon-bundle"] != null) iconBundleName = theme.Properties["icon-bundle"] as string;
                }
            }
            // Do we want the code or URL?
            if (iconId.EndsWith(".url")) {
                // URL
                iconId = iconId.ReplaceSuffix(".url", "");
                var iconURL = Core.Imaging.IconFactory.GetIconURL(iconId, iconBundleName);
                this.InsertVariableUnsafe(variableName, iconURL);
            } else {
                // Code
                var iconCode = Core.Imaging.IconFactory.GetIconHTML(iconId, iconBundleName);
                this.InsertVariableUnsafe(variableName, iconCode);
            }
        }

    }
}
