using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Util;
using Machinata.Core.Builder;
using Machinata.Core.Model;
using System.Reflection;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Filters Inserts Helper Methods
    /// </summary>
    public partial class PageTemplate {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="variableName"></param>
        /// <param name="currentValue"></param>
        /// <param name="allValues"></param>
        /// <param name="availableValues"></param>
        /// <param name="key"></param>
        /// <param name="postfixIndexEachParamter">creates for each value a separate key e.g. status-1, status-2</param>
        public void InsertFilters(string variableName, IEnumerable<string> currentValues,  IEnumerable<string> allValues, IEnumerable<string> availableValues, string key, bool postfixIndexEachParamter = false, Func<string,string> getTitleForValue = null) {
            var templates = new List<PageTemplate>();
            // All
            {
                var allTemplate = this.LoadTemplate("filters.all");
                allTemplate.InsertVariable("all-selected", currentValues.Any() == false ? "selected" : string.Empty);

                var allKey = key;

                // postfixIndexEachParamter all keys need to be set to ''
                if (postfixIndexEachParamter == true) {
                    int kIndex = 0;
                    var allKeys = new List<string>();
                    foreach (var v in allValues) {
                        kIndex++;
                        allKeys.Add(key + "-" + kIndex);
                    }
                    allKey = string.Join(",", allKeys);
                }

                allTemplate.InsertVariable("key", allKey);
                templates.Add(allTemplate);
            }

            int index = 0;
            // Filters
            foreach (var v in allValues) {
                index++;
                var title = v;
                if (getTitleForValue != null) {
                    title = getTitleForValue(v);
                }
                var template = this.LoadTemplate("filters.filter");
                template.InsertVariable("selected", currentValues.Contains(v) ? "selected" : string.Empty);
                template.InsertVariable("disabled", availableValues.Contains(v) == false ? "disabled" : string.Empty);
                template.InsertVariable("title", title);
                template.InsertVariable("value", v);
                if (postfixIndexEachParamter == false) {
                    template.InsertVariable("key", key);
                } else {
                    template.InsertVariable("key", key +"-"+index);
                }
                templates.Add(template);
            }

            this.InsertTemplates(variableName, templates);
        }


    }
}
