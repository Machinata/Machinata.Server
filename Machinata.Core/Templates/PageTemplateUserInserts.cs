using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Util;
using System.Reflection;
using Machinata.Core.Model;
using Machinata.Core.Builder;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public partial class PageTemplate {

        public User InsertAccountActivationForm(string variableName = "form", FormBuilder form = null, string onSuccess = null, string onError = null) {
            
            // Validate the request
            User user = this._handler.DB.Users().GetByUsername(this._handler.Params.String("username"));
            var code = this._handler.Params.String("code");
            var resetCode = this._handler.Params.String("reset-code");
            user.CheckResetCode(resetCode ,code, this._handler);
            this.InsertVariables("entity", user);

            // Generate a default form
            if (form == null) {
                form = new FormBuilder("Empty");
                // Note name should not be closest to password, otherwise browsers will take this as the username/password for automatic password saving
                form.Custom(
                    name: "name",
                    formName: "name",
                    formType: "text",
                    value: user.Name,
                    required: true,
                    readOnly: false,
                    tooltip: "{text.account.activate-tooltip.name}"
                    //autoComplete: "off"
                );
                if (Core.Config.AccountUsernameUsesEmail) {
                    form.Custom(
                        name: "email", 
                        formName: "username", 
                        formType: "text", 
                        value: user.Username,
                        required: false, 
                        readOnly: true,
                        autoComplete: "username",
                        tooltip: "{text.account.activate-tooltip.email-as-username}"
                    );
                } else {
                    form.Custom(
                        name: "username", 
                        formName: "username", 
                        formType: "text", 
                        value: user.Username, 
                        required: false, 
                        readOnly: true,
                        autoComplete: "username",
                        tooltip: "{text.account.activate-tooltip.username}"
                    );
                }
                form.Custom(
                    name: "password", 
                    formName: "password",
                    formType: "password",
                    value: null,
                    required: true,
                    autoComplete: "new-password"
                    );
                form.Custom(
                    name: "password-verify", 
                    formName: "password_verify", 
                    formType: "password",
                    value: null,
                    required: true,
                    autoComplete: "new-password"
                );


                form.Button("button", "{text.activate}");
            }

            // Insert form directly into template
            if (onSuccess == null) onSuccess = Core.Config.AccountLoginPage;
            this.InsertForm(
                entity: user,
                variableName: variableName,
                form: form,
                apiCall: $"/api/admin/account/activate?user={user.Username}&code={code}&reset-code={resetCode}",
                onSuccess: onSuccess,
                onError: onError
            );

            // Return the user if the caller needs it...
            return user;

        }

        public User InsertResetPasswordForm(string variableName = "form", FormBuilder form = null, string onSuccess = null, string onError = null) {
            
            // Validate the request
            User user = this._handler.DB.Users().GetByUsername(this._handler.Params.String("username"));
            var code = this._handler.Params.String("code");
            var resetCode = this._handler.Params.String("reset-code");
            user.CheckResetCode(resetCode, code, this._handler);

            // Generate a default form
            if (form == null) {
                form = new FormBuilder("Empty");
                if (Core.Config.AccountUsernameUsesEmail) {
                    form.Custom("email", "username", "text", user.Username, false, true);
                } else {
                    form.Custom("username", "username", "text", user.Username, false, true);
                }
                form
                    
                    .Custom("password", "password", "password", null, true)
                    .Custom("password-verify", "password_verify", "password", null, true)
                    .Button("button", "{text.reset}");
            }
            
            // Insert form directly into template
            if (onSuccess == null) onSuccess = Core.Config.AccountLoginPage;

            this.InsertForm(
                entity: new User(),
                variableName: variableName,
                form: form,
                apiCall: $"/api/admin/account/reset-password?user={user.Username}&code={code}&reset-code={resetCode}",
                onSuccess: onSuccess,
                onError: onError
            );
            
            // Return the user if the caller needs it...
            return user;
        }
        
    }
}
