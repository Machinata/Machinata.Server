using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Util;
using System.Linq.Expressions;
using Machinata.Core.Model;

namespace Machinata.Core.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public partial class PageTemplate {

        public const int PAGINATION_DEFAULT_PAGE_SIZE = 50;

        /// <summary>
        /// Paginates the specified entities using the context of the handler as parameters for the pagination.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities">The entities.</param>
        /// <param name="handler">The handler.</param>
        /// <returns></returns>
        public IOrderedQueryable<T> Paginate<T>(IQueryable<T> entities, Handler.Handler handler, string sortBy = "Created", string sortDir = "desc", int pageNum = 1, int pageSize = PAGINATION_DEFAULT_PAGE_SIZE) where T : Core.Model.ModelObject {
            // Extract page info from context
            int page = handler.Params.Int("page", pageNum);
            int size = handler.Params.Int("num", pageSize);
            // Sorting
            var sort = handler.Params.String("sort", sortBy);
            var dir = handler.Params.String("dir", sortDir);
            // Compile list of filters
            string filters = "";
            foreach(var key in handler.Context.Request.QueryString.AllKeys.Where(k => k != "page")) {
                filters += "&";
                filters += key + "=" + handler.Context.Request.QueryString[key];
            }
            // Do pagination
            return this.Paginate(entities, handler.Route.GetRoutePathOrAliasIfSet(), filters, page, size, sort, dir);
        }

        /// <summary>
        /// Automatically sorts the entities based on context parameters from the query string (or default passed parameters).
        /// This is a shorthand for paginate, where page size = int.max.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities">The entities.</param>
        /// <param name="handler">The handler.</param>
        /// <param name="sortBy">The sort by.</param>
        /// <param name="sortDir">The sort dir.</param>
        /// <returns></returns>
        public IOrderedQueryable<T> AutoSort<T>(IQueryable<T> entities, Handler.Handler handler, string sortBy = "Created", string sortDir = "desc") where T : Core.Model.ModelObject {
            return Paginate(entities, handler, sortBy, sortDir, 1, int.MaxValue);
        }


        /// <summary>
        /// Paginates the specified entities using a specific page number and page size.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities">The entities.</param>
        /// <param name="route">The route.</param>
        /// <param name="filters">The filters to pass along in the pagination links.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns></returns>
        public IOrderedQueryable<T> Paginate<T>(IQueryable<T> entities, string route, string filters, int pageNumber, int pageSize, string sortBy = null, string sortDirection = null) where T : Core.Model.ModelObject {
            // Init
            if (sortBy != null) {
                var desc = false;
                if (sortDirection == "asc") desc = false;
                else if (sortDirection == "desc") desc = true;
                else desc = false; // default asc
                entities = entities.OrderBy(sortBy, desc);
            }
            string oppositeSortDirection = sortDirection == "desc" ? "asc" : "desc";
            // Get count
            int count = entities.Count();
            int pages = (int)System.Math.Ceiling((float)count / (float)pageSize);
            // Do the pagination
            if (pages > 1) {
                entities = entities.Paginate(pageNumber, pageSize);
            }
            // Master template
            var template = this.LoadTemplate("pagination");
            var pageTemplates = new List<PageTemplate>();
            // Compile list of pages
            for(int p = 1; p <= pages; p++) {
                var pageTemplate = this.LoadTemplate("pagination.page");
                var pageClasses = "";
                if (p == pageNumber) pageClasses += "selected ";
                if (p == 1) pageClasses += "first ";
                if (p == pages) pageClasses += "last ";
                pageTemplate.InsertVariable("page.number", p.ToString());
                pageTemplate.InsertVariable("page.class", pageClasses);
                pageTemplates.Add(pageTemplate);
            }
            // Insert helper variables
            template.InsertVariable("pagination.first-class", pageNumber <= 1 ? "xselected" : "");
            template.InsertVariable("pagination.prev-class", pageNumber <= 1 ? "disabled" : "");
            template.InsertVariable("pagination.next-class", pageNumber >= pages ? "disabled" : "");
            template.InsertVariable("pagination.last-class", pageNumber >= pages ? "xselected" : "");
            template.InsertTemplates("pagination.pages", pageTemplates);
            template.InsertVariable("pagination.next", (pageNumber+1));
            template.InsertVariable("pagination.prev", (pageNumber-1));
            template.InsertVariable("pagination.last", (pages));
            template.InsertVariable("pagination.route", route);
            template.InsertVariable("pagination.filters", filters);
            template.InsertVariable("pagination.sort-by", sortBy);
            template.InsertVariable("pagination.num-pages", pages);
            template.InsertVariable("pagination.sort-direction", sortDirection);
            template.InsertVariable("pagination.opposite-sort-direction", oppositeSortDirection);
            this.InsertTemplate("pagination", template);
            return entities as IOrderedQueryable<T>;
        }

        /// <summary>
        /// Filters the specified entities and insert the corresponding UI.
        /// If a UI is wished to be displayed, one must add {entity-list.filter} to the template.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities">The entities.</param>
        /// <param name="handler">The handler.</param>
        /// <param name="defaultFilterProperty">The default filter property.</param>
        /// <returns></returns>
        public IOrderedQueryable<T> Filter<T>(IOrderedQueryable<T> entities, Handler.Handler handler, string defaultFilterProperty = null) where T : Core.Model.ModelObject {
            return (IOrderedQueryable<T>)this.Filter<T>((IQueryable<T>)entities, handler, defaultFilterProperty);
        }

        public IOrderedQueryable<T> Filter<T>(IOrderedQueryable<T> entities, Handler.Handler handler, IEnumerable<string> defaultFilterProperties) where T : Core.Model.ModelObject {
            return (IOrderedQueryable<T>)this.Filter<T>((IQueryable<T>)entities, handler, defaultFilterProperties);
        }

        /// <summary>
        /// Filters the specified entities and insert the corresponding UI.
        /// If a UI is wished to be displayed, one must add {entity-list.filter} to the template.
        /// Hint: entities have to be the original set from the DB, since 'to lower' only works on mysql not on objects in memory
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities">The entities.</param>
        /// <param name="handler">The handler.</param>
        /// <param name="defaultFilterProperty">The default filter property.</param>
        /// <returns></returns>
        public IQueryable<T> Filter<T>(IQueryable<T> entities, Handler.Handler handler, string defaultFilterProperty = null) where T : Core.Model.ModelObject {
            string[] defaultProperties = new string[] { defaultFilterProperty };
            return Filter(entities, handler, defaultProperties);
        }

        public IQueryable<T> Filter<T>(IQueryable<T> entities, Handler.Handler handler, IEnumerable<string> defaultFilterProperties) where T : ModelObject {
            // Init

            if (defaultFilterProperties == null) {
                defaultFilterProperties = new List<string>();
            }

            var filterProp = handler.Params.StringArray("filter-prop", defaultFilterProperties.ToArray());
            var filterQuery = handler.Params.String("filter-query");
            // UI
            var filterTemplate = this.LoadTemplate("entity-list.filter");
            this.InsertTemplate("entity-list.filter", filterTemplate);
            // Variables
            this.InsertVariable("filter.property", filterProp);
            this.InsertVariable("filter.query", filterQuery);
            // Do filter
            if (filterProp.Any(f => string.IsNullOrEmpty(f) == false) && !string.IsNullOrEmpty(filterQuery)) {
                return entities.Filter(filterProp.ToArray(), filterQuery);
            } else {
                return entities;
            }
        }



        public IQueryable<T> Filter<T>(IQueryable<T> entities, Handler.Handler handler, Func<IQueryable<T>, IQueryable<T>> customFilterFunction) where T : ModelObject {
            // Init
            var filterQuery = handler.Params.String("filter-query");
            // UI
            var filterTemplate = this.LoadTemplate("entity-list.filter");
            this.InsertTemplate("entity-list.filter", filterTemplate);
            // Variables
            this.InsertVariable("filter.property", "");
            this.InsertVariable("filter.query", filterQuery);
            // Do filter
            return customFilterFunction(entities);
        }
    }
}
