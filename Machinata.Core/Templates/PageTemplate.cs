using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Machinata.Core.Builder;
using Machinata.Core.Model;

namespace Machinata.Core.Templates {

    public partial class PageTemplate {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constants
        
        public const string VARIABLE_REGEX = "{([a-zA-Z0-9-.]+)}";

        #endregion

        #region Private Fields

        protected StringBuilder _data;
        protected string _filepath;
        protected string _package;
        protected string _name;
        protected string _extension;
        protected string _language;
        protected string _currency;
        protected string _themeName;
        protected Core.Handler.Handler _handler;
        protected List<string> _variables;
     


        #endregion

        #region Public Properties

        public StringBuilder Data {
            get { return _data; }
        }

        public string Package {
            get { return _package; }
        }

        public string Name {
            get { return _name; }
        }

        public string Extension {
            get { return _extension; }
        }

        public string Language {
            get { return _language; }
            set { _language = value; }
        }

        public string Currency {
            get { return _currency; }
            set { _currency = value; }
        }

        public string ThemeName {
            get { return _themeName; }
            set { _themeName = value; }
        }

        public Core.Handler.Handler Handler {
            get { return _handler; }
            set { _handler = value; }
        }

        public string Content {
            get
            {
                return Data.ToString();
            }
        }

        public StructuredData.StructuredDataInfo StructuredData = new Core.StructuredData.StructuredDataInfo();


        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PageTemplate"/> class by copying a existing template.
        /// </summary>
        /// <param name="templateToCopy">The template to copy.</param>
        public PageTemplate(PageTemplate templateToCopy, string newName = null, StringBuilder data = null) {
            if (data == null) {
                _data = new StringBuilder(templateToCopy._data.ToString()); // Force copy
            } else {
                _data = data;
            }
            _filepath = templateToCopy._filepath;
            _package = templateToCopy._package;
            if (newName != null) _name = newName;
            else _name = templateToCopy._name;
            _extension = templateToCopy._extension;
            _variables = templateToCopy._variables.ToList();
            _language = templateToCopy._language;
            _currency = templateToCopy._currency;
            _handler = templateToCopy._handler;
        }

        public PageTemplate(string packageName, string templateName, string extension = ".htm", string fallbackPackage = null, bool loadTemplateContents = true) {
            
            // Init
            _package = packageName;
            _name = templateName;
            _extension = extension;
            _handler = null;

            //NOTE: Templates are cached by the PageTemplateCache extension class

            // Compile the path
            string fileName = templateName + extension;
            fileName = fileName.Replace("/",Core.Config.PathSep);
            
            // Test main package
            _filepath = Core.Config.TemplatePath + Core.Config.PathSep + packageName + Core.Config.PathSep + fileName;
            if(!System.IO.File.Exists(_filepath)) {
                // Do we have a fallback package to test?
                if(!string.IsNullOrEmpty(fallbackPackage)) {
                    _logger.Trace("Original package file does not exist, falling back to " + _filepath);
                    var fallbackFilepath = Core.Config.TemplatePath + Core.Config.PathSep + fallbackPackage + Core.Config.PathSep + fileName;
                    if(System.IO.File.Exists(fallbackFilepath)) {
                        _filepath = fallbackFilepath;
                        //TODO: should we change the package to fallback? I dont think so, because then LoadTemplate/ChangeTemplate will never work on the original upper package namespace again...
                    }
                }
            }

            // Load template contents
            if(loadTemplateContents) _loadFromDisk();
        }

        public void LoadContentsIfNeeded() {
            if (_data == null) {
                _loadFromDisk();
            }
        }

        private void _loadFromDisk() {
            // Load from disk
            _logger.Trace("Loading file "+_filepath);
            string stringData = System.IO.File.ReadAllText(Core.Util.Files.GetHotSwappableFile(_filepath, _package));

            // Load string builder
            _data = new StringBuilder(stringData);

            // Find all variables and templates
            _variables = new List<string>();
            DiscoverVariables();
        }




        /// <summary>
        /// Use this without an actual file. To use Templating functionality on a string
        /// </summary>
        /// <param name="content"></param>
        public PageTemplate(string content) {
            // Init
            _handler = null;
            // Load string builder
            _data = new StringBuilder(content);
                        
            // Find all variables and templates
            _variables = new List<string>();
            DiscoverVariables();
        }

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public PageTemplate GetBlankCopy(string name) {
            var ret = new PageTemplate(this, name, new StringBuilder());
            return ret;
        }

        public void Reload() {
            _loadFromDisk();
        }

        /// <summary>
        /// Automatically discovers all the variables in the template.
        /// This method is automatically called when the template is loaded, however
        /// if some variables have been inserted dynamically through text inserts,
        /// this method can be called again to ensure that all text variables are 
        /// registered and replaced.
        /// </summary>
        public IEnumerable<string> DiscoverVariables() {
            // Parse out all inline text variables
            Core.Localization.TextParser.ParseDataForTextVariables(this.Name, this.Package, _data);
            // Find all variable matches
            Regex variableMatchesRegex = new Regex(VARIABLE_REGEX);
            var variableMatches = variableMatchesRegex.Matches(_data.ToString());

            var newVariables = new List<string>();
            foreach(var match in variableMatches) {
                string varName = match.ToString().Trim('{').Trim('}');
                RegisterVariable(varName);

                // Collect the new vars
                if (!string.IsNullOrEmpty(varName)) {
                    newVariables.Add(varName);
                }
            }
            return newVariables;
        }

        public void RegisterVariable(string varName) {
            //_logger.Trace("  Found variable "+varName);
            if (varName == null) {
                _logger.Warn("  Found null variable " + varName);
                return;
            }
            if (!_variables.Contains(varName)) _variables.Add(varName);
        }

        public bool HasVariable(string varName) {
            return _variables.Contains(varName);
        }

        public bool HasVariableStartingWith(string varNamePrefix) {
            return _variables.Where(v => v.StartsWith(varNamePrefix)).Any();
        }

     
        public IEnumerable<string> VariablesCopy {
             get{ return _variables.ToList();  }
        }

      

        #endregion


    }
}
