using Machinata.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace Machinata.Core.Server.Module
{
    /// <summary>
    /// The IHttpModule for Machinata that manages some additional headers.
    /// </summary>
    /// <seealso cref="System.Web.IHttpModule" />
    public class HttpModule : IHttpModule 
    {
        public void Init(HttpApplication context) {
            context.PreSendRequestHeaders += OnPreSendRequestHeaders;
            if(Core.Config.SecurityBasicHTTPAuthenticationEnabled) {
                context.AuthenticateRequest += OnAuthenticateRequest;
            }
        }

        public void Dispose() { }

        void OnPreSendRequestHeaders(object sender, EventArgs e) {
            var response = HttpContext.Current.Response;
            // Remove some standard headers
            if (response.Headers["Server"] != null) response.Headers.Remove("Server");
            if (response.Headers["X-Powered-By"] != null) response.Headers.Remove("X-Powered-By");
            if (response.Headers["X-AspNet-Version"] != null) response.Headers.Remove("X-AspNet-Version");
            // Set some custom headers
            //response.Headers.Set("Server", "Machinata");
            //response.Headers.Set("X-Developer", "Nerves");
            
        }

        #region HTTP Basic Authentication for Dev/Beta Testing

        void OnAuthenticateRequest(object sender, EventArgs e) {
            var request = HttpContext.Current.Request;
            var authHeader = request.Headers["Authorization"];
            if (authHeader != null) {
                var authHeaderVal = AuthenticationHeaderValue.Parse(authHeader);
                // RFC 2617 sec 1.2, "scheme" name is case-insensitive
                if (authHeaderVal.Scheme.Equals("basic", StringComparison.OrdinalIgnoreCase) && authHeaderVal.Parameter != null) {
                    _httpBasicAuthenticateUser(authHeaderVal.Parameter);
                }
            } else {
                HttpContext.Current.Response.StatusCode = 401;
                HttpContext.Current.Response.Headers.Add("WWW-Authenticate",string.Format("Basic realm=\"{0}\"", Core.Config.ProjectID));
            }
        }

        private static void _httpBasicAuthenticateUser(string credentials) {
            try {
                // Decode
                var encoding = Encoding.GetEncoding("iso-8859-1");
                credentials = encoding.GetString(Convert.FromBase64String(credentials));
                int separator = credentials.IndexOf(':');
                string username = credentials.Substring(0, separator);
                string password = credentials.Substring(separator + 1);
                string requiredUsername = Core.Config.GetStringSetting("SecurityBasicHTTPAuthenticationUsername");
                string requiredPassword = Core.Config.GetStringSetting("SecurityBasicHTTPAuthenticationPassword");
                if (string.IsNullOrEmpty(requiredUsername)) throw new Exception("SecurityBasicHTTPAuthenticationUsername is not set.");
                if (string.IsNullOrEmpty(requiredPassword)) throw new Exception("SecurityBasicHTTPAuthenticationPassword is not set.");
                if (username == requiredUsername && password == requiredPassword) { 
                    // All good
                } else {
                    // Invalid username or password.
                    HttpContext.Current.Response.StatusCode = 401;
                    HttpContext.Current.Response.Headers.Add("WWW-Authenticate",string.Format("Basic realm=\"{0}\"", Core.Config.ProjectID));
                }
            } catch (FormatException) {
                // Credentials were not formatted correctly.
                HttpContext.Current.Response.StatusCode = 401;
                HttpContext.Current.Response.Headers.Add("WWW-Authenticate",string.Format("Basic realm=\"{0}\"", Core.Config.ProjectID));
            }
        }

        #endregion

    }
}