using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Text;
using Machinata.Core.Exceptions;

namespace Machinata.Core.Server.Requests
{
    /// <summary>
    /// The IHttpHandler that handles all requests for Machinata.  This IHttpHandler forwards
    /// the request to the Machinata Handler factory and provides a fallback for worst-case
    /// error messages.
    /// </summary>
    /// <seealso cref="System.Web.IHttpHandler" />
    /// <seealso cref="System.Web.SessionState.IReadOnlySessionState" />
    public class RequestFactory : IHttpHandler, System.Web.SessionState.IReadOnlySessionState
    {
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        public void ProcessRequest(HttpContext context) {

            // Maintenance mode?
            if(Core.Config.MaintenanceMode) {
                // Show super simple worst-case error message
                context.Response.StatusCode = 503;
                context.Response.StatusDescription = "Maintenance";
                var html = Core.Util.HTML.CreateStandaloneHTMLPageWithBasicCSS(
                    title: "Maintenance", 
                    message: "Sorry, this server is down for maintenance at the moment.", 
                    details: Core.Config.MaintenanceDetails, 
                    autoReload: true
                );
                context.Response.Write(html);
                context.Response.Flush();
                return;
            }

            // SecurityAllRequestsRequirePIN mode?
            if (!string.IsNullOrEmpty(Core.Config.SecurityAllRequestsRequirePIN)) {
                // Init
                bool requirePINInput = true;
                bool pinEnteredIsWrong = false;
                // Validate if we have a POST parameter with the correct pin
                if (!string.IsNullOrEmpty(context.Request.Params["pin"])) {
                    if (context.Request.Params["pin"] == Core.Config.SecurityAllRequestsRequirePIN) {
                        // Allow access
                        requirePINInput = false;
                        // Save PIN as cookie
                        HttpCookie cookie = new HttpCookie("SecurityAccessPIN", Core.Config.SecurityAllRequestsRequirePIN);
                        cookie.Expires = DateTime.Now.AddDays(365);
                        cookie.Domain = context.Request.Url.Host;
                        cookie.Path = "/";
                        context.Response.SetCookie(cookie);
                    } else {
                        pinEnteredIsWrong = true;
                    }
                } else if (context.Request.Cookies["SecurityAccessPIN"] != null) {
                    if (context.Request.Cookies["SecurityAccessPIN"].Value == Core.Config.SecurityAllRequestsRequirePIN) {
                        // Allow access
                        requirePINInput = false;
                    } else {
                        pinEnteredIsWrong = true;
                    }
                } else if (context.Request.Headers["SecurityAccessPIN"] != null) {
                    if (context.Request.Headers["SecurityAccessPIN"] == Core.Config.SecurityAllRequestsRequirePIN) {
                        // Allow access
                        requirePINInput = false;
                    } else {
                        pinEnteredIsWrong = true;
                    }
                }
                if (requirePINInput) {
                    // Show super simple worst-case error message
                    context.Response.StatusCode = 403;
                    context.Response.StatusDescription = "Forbidden";
                    // API?
                    if (context.Request.RawUrl.StartsWith("/api/")) {
                        var message = "Sorry, this website requires a PIN code to access. Please provide the correct access PIN via the header or cookie SecurityAccessPIN.";
                        var json = "{\"type\":\"error\",\"data\":{\"code\":\"invalid-access-pin\",\"message\":\""+ message + "\"}}";
                        context.Response.Write(json);
                        context.Response.Flush();
                    } else {
                        var message = "Sorry, this website requires a PIN code to access.";
                        if (pinEnteredIsWrong) message = "The PIN entered is not correct.";
                        var html = Core.Util.HTML.CreateStandaloneHTMLPageWithBasicCSS(
                            title: "Access Denied",
                            message: message,
                            details: "Please enter the PIN code to continue:",
                            richContent: @"
                            <form method='post'>
                            <input name='pin' id='pin' type='text'/><input type='submit' value='Enter'>
                            </form>",
                            autoReload: false
                        );
                        context.Response.Write(html);
                        context.Response.Flush();
                    }
                    return;
                }
            }

            // Catch all exceptions that occur at the very deep handler level...
            try {
                
                // Process the request by our handler factory
                Core.Handler.Handler.ProcessRequest(context);
                
            } catch (Exception e1) {

                _logger.Error(e1, e1.GetFullDescriptionIncludingTrace());

                if(e1 is RichException) {
                    Core.Exceptions.FailSafeMessage.Show(context, Core.Exceptions.BackendException.WrapExceptionWithBackendException(e1));
                    return;
                }

                // An error occured!
                try {
                    
                    // Try to create a request to handle this error...
                    _logger.Trace("Will try to create error page...");
                    Core.Handler.Handler.ProcessException(context, Core.Exceptions.BackendException.WrapExceptionWithBackendException(e1));

                } catch(Exception e2) {
                    // Last resort
                    _logger.Error(e2);
                    _logger.Trace("Could not create error page!");
                    Core.Exceptions.FailSafeMessage.Show(context, Core.Exceptions.BackendException.WrapExceptionWithBackendException(e2));
                }
            }
            
        }

        

        public bool IsReusable
        {
            get { return true; }
        }
    }
}