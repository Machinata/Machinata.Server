using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace Machinata.Core.Reflection
{
    public class Modules
    {
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        
        private static Object _modulesByNameLock = new Object();
        private static List<string> _modulesByName = null;

        /// <summary>
        /// Gets all the Machinata modules by name.
        /// This call is cached
        /// </summary>
        /// <returns></returns>
        public static List<string> GetMachinataModuleNames() {
            if(_modulesByName == null) {
                lock (_modulesByNameLock) {
                    var assemblies = Core.Reflection.Assemblies.GetMachinataAssemblies();
                    _modulesByName = new List<string>();
                    foreach (var assembly in assemblies) {
                        var name = assembly.GetName().Name;
                        if (name.StartsWith("Machinata.Module.")) {
                            _modulesByName.Add(name);
                            _logger.Trace("Found Module " + name);
                        }
                    }
                }
            }
            return _modulesByName;
        }

        
    }

    
}

