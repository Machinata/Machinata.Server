using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Reflection {
    public class Properties
    {
        public static List<PropertyInfo> _machinataProperties = null;
        public static ConcurrentDictionary<Type,List<PropertyInfo>> _machinataPropertyForAttribute = new ConcurrentDictionary<Type, List<PropertyInfo>>();

        /// <summary>
        /// Gets the BlackBox methods, ie all the Machinata Types' methods.
        /// This list is cached.
        /// </summary>
        /// <returns></returns>
        public static List<PropertyInfo> GetMachinataProperties() {
            if(_machinataProperties == null) {
                _machinataProperties = Types.GetMachinataTypes().SelectMany(a => a.GetProperties()).ToList();
            }
            return _machinataProperties;
        }

        /// <summary>
        /// Gets the Machinata properties with a specifc attribute.
        /// The returned list is cached.
        /// </summary>
        /// <param name="attribute">The attribute.</param>
        /// <returns></returns>
        public static List<PropertyInfo> GetMachinataPropertiesWithAttribute(Type attribute) {
            if (!_machinataPropertyForAttribute.ContainsKey(attribute)) {
                _machinataPropertyForAttribute[attribute] = GetMachinataProperties().Where(m => m.GetCustomAttributes(attribute, false).Length > 0).ToList();
            }
            return _machinataPropertyForAttribute[attribute];
        }
    }
}
