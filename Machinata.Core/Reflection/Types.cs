using DocumentFormat.OpenXml.Vml;
using Machinata.Core.Model;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Reflection {
    public class Types
    {
        public static List<Type> _machinataTypes = null;
        public static ConcurrentDictionary<Type,List<Type>> _machinataTypesForAttribute = new ConcurrentDictionary<Type, List<Type>>();

        /// <summary>
        /// Gets the BlackBox types.
        /// This list is cached.
        /// </summary>
        /// <returns></returns>
        public static List<Type> GetMachinataTypes() {
            if(_machinataTypes == null) {
                try {
                    _machinataTypes = Assemblies.GetMachinataAssemblies().SelectMany(a => a.GetTypes()).ToList();
                } catch (System.Reflection.ReflectionTypeLoadException e) {
                    throw new Exception("Could not load Machinata type: "+e.LoaderExceptions.FirstOrDefault()?.Message,e);
                } catch (Exception e) {
                    throw e;
                }
            }
            return _machinataTypes;
        }

        /// <summary>
        /// Gets the BlackBox types which are the given class or sub classes
        /// This list is cached.
        /// </summary>
        /// <returns></returns>
        public static List<Type> GetMachinataTypes(Type baseClass) {
           return GetMachinataTypes().Where(t=>IsSameOrSubclass(t,baseClass)).ToList();
        }

        /// <summary>
        /// Determines whether given type is same or subclass of potentialBase
        /// </summary>
        /// <param name="potentialDescendant">The potential descendant.</param>
        /// <param name="potentialBase">The potential base.</param>
        /// <returns></returns>
        public static bool IsSameOrSubclass(Type potentialDescendant, Type potentialBase) {
            return potentialDescendant.IsSubclassOf(potentialBase)
                   || potentialDescendant == potentialBase 
                   || potentialDescendant.IsAssignableFrom(potentialBase);
        }

        /// <summary>
        /// Gets the BlackBox types which implement the given interface
        /// </summary>
        /// <returns></returns>
        public static List<Type> GetMachinataTypesImplementingInterface(Type @interface) {
            return GetMachinataTypes().Where(t => t.GetInterfaces().Contains(@interface)).ToList();
        }

        /// <summary>
        /// Gets the Machinata types with a specific attribute.
        /// </summary>
        /// <param name="attribute">The attribute.</param>
        /// <returns></returns>
        public static List<Type> GetMachinataTypesWithAttribute(Type attribute) {
            if (!_machinataTypesForAttribute.ContainsKey(attribute)) {
                _machinataTypesForAttribute[attribute] = Core.Reflection.Types.GetMachinataTypes().Where(t => t.GetCustomAttributes(attribute, inherit: false).Any()).ToList();
            }
            return _machinataTypesForAttribute[attribute];
        }

        public static T CreateInstance<T>(Type type) where T : class {
            var instance = Activator.CreateInstance(type);
            return instance as T;
        }

        public static T CreateInstance<T>(Type type, params object[] args) where T : class {
            var instance = Activator.CreateInstance(type, args);
            return instance as T;
        }

    }

    public static class TypeExtensions {
        public static bool IsNullableType(this Type theType) {
            return (theType.IsGenericType &&
            theType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)));
        }

        /// <summary>
        /// If the given object is a generic list with the items sublcass
        /// </summary>
        /// <param name="theType"></param>
        /// <returns></returns>
        public static bool AreListMembersOfSubtype(this Type listType, Type itemTypes) {
            // Is it a collection/enumerable? maybe this can be more generic
            if ( typeof(IEnumerable).IsAssignableFrom(listType) == false) {

                return false;
            }
            return listType.GetGenericArguments().FirstOrDefault()?.IsSubclassOf(itemTypes)== true;
        }

       
    }
}
