# https://pegjs.org/documentation#generating-a-parser

# Run pegjs via node
echo "Buidling JS from grammar..."
#pegjs --export-var "Machinata.Parser.textFilter" --format globals --optimize speed --cache -o machinata-core-parser-text-filter.js machinata-core-parser-text-filter.grammar
pegjs --format "commonjs" --optimize speed --cache -o machinata-core-parser-text-filter.peg machinata-core-parser-text-filter.grammar

# Strip commonjs loader
echo "Stripping commonjs loader..."
sed -i ':a;N;$!ba;s/module.exports = {\n  SyntaxError: peg$SyntaxError,\n  parse:       peg$parse\n};//g' machinata-core-parser-text-filter.peg

# Delete existing if any
rm -f machinata-core-parser-text-filter.js

# Add header
echo "Writing header and inplace function loader..."
echo "/// <summary>" >> machinata-core-parser-text-filter.js
echo "/// Powerful text filter parser generated with peg.js." >> machinata-core-parser-text-filter.js
echo "/// Note: this file was generated with Peg using machinata-core-parser-text-filter.grammar and Machinata.Server\Machinata.Core\Libraries\pegjs-parsers" >> machinata-core-parser-text-filter.js
echo "/// </summary>" >> machinata-core-parser-text-filter.js
echo "/// <type>function</type>" >> machinata-core-parser-text-filter.js
echo "/// <hidden/>" >> machinata-core-parser-text-filter.js
echo "Machinata.Parser._textFilterParser = " >> machinata-core-parser-text-filter.js
echo "(function() {" >> machinata-core-parser-text-filter.js

# Add peg JS
cat machinata-core-parser-text-filter.peg >> machinata-core-parser-text-filter.js

# Add our own loader
# Should look like:
#  return {
#    SyntaxError: peg$SyntaxError,
#    parse:       peg$parse
#  };
echo "  return {" >> machinata-core-parser-text-filter.js
echo "    SyntaxError: peg\$SyntaxError," >> machinata-core-parser-text-filter.js
echo "    parse:       peg\$parse" >> machinata-core-parser-text-filter.js
echo "  };" >> machinata-core-parser-text-filter.js

# Close self function call
echo "})();" >> machinata-core-parser-text-filter.js
echo "Done!"

# Copy to Core JS
echo "Do you want to copy to machinata-core-bundle.js?"
read -p "Press enter to continue"

cp machinata-core-parser-text-filter.js ../../Static/js/machinata-core-bundle.js/machinata-core-parser-text-filter.js

