using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core {

    public partial class JSON {

        public const int PARSING_MAX_DEPTH = 1024;

        public static void LoadDefaultSettings() {
            // Security Advisory GHSA-5crp-9r3c-p9vr:
            // MaxDepth: https://github.com/advisories/GHSA-5crp-9r3c-p9vr
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings { MaxDepth = PARSING_MAX_DEPTH };
        }


        public static Newtonsoft.Json.Linq.JObject ParseJsonAsJObject(byte[] data) {
            return ParseJsonAsJObject(Encoding.UTF8.GetString(data));
        }

        public static IEnumerable<JObject> ParseJsonArrayAsJObjects(byte[] data) {
            return ParseJsonArrayAsJObjects(Encoding.UTF8.GetString(data));
        }

        public static IEnumerable<JObject> ParseJsonArrayAsJObjects(string data) {
            var array = Newtonsoft.Json.Linq.JArray.Parse(data);
            return array.Children<JObject>().ToList();
        }

        public static Newtonsoft.Json.Linq.JObject ParseJsonAsJObject(string data, bool returnNullIfInvalid = false) {
            try {
                return Newtonsoft.Json.Linq.JObject.Parse(data);
            } catch(Exception e) {
                if (returnNullIfInvalid) {
                    return null;
                }
                throw e;
            }
        }


        /// <summary>
        /// Gets the depth of JSON structure
        /// rewrite from https://stackoverflow.com/questions/38046665/how-to-find-depth-of-nodes-in-a-nested-json-file
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        public static int GetDepth(string data) {
            if (string.IsNullOrWhiteSpace(data)) {
                return 0;
            }
            var parsed = ParseJsonAsJObject(data);
            return GetDepthToken(parsed);
            
        }

        private static int GetDepthToken(JToken obj) {
            var depth = 0;
            if (obj.Children().Count() > 0) {
                foreach (var child in obj.Children()) {
                    var tmpDepth = GetDepthToken(child);
                    if (tmpDepth > depth) {
                        depth = tmpDepth;
                    }

                }

            }
            return 1 + depth;
        }

        /// <summary>
        /// Hint:
        /// 
        /// As it turns out, it is by-design that JObject serialization ignores contract resolvers.
        /// So any JObject will serialize its properties as-is.
        /// That means that if you end up with a JObject that already has PascalCased properties,
        /// there is no built-in way to handle this case.
        /// 
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="lowerDashed"></param>
        /// <param name="enumToString"></param>
        /// <param name="indendFormatting"></param>
        /// <returns></returns>

        public static string Serialize(object data, bool lowerDashed = true, bool enumToString = false, bool indendFormatting = false) {
            // Create settings
            var settings = new JsonSerializerSettings {
                MaxDepth = PARSING_MAX_DEPTH
            };

            if (lowerDashed) {
                settings.ContractResolver = new DefaultContractResolver {
                    NamingStrategy = new DashedLowerNamingStrategy(true, true)
                };
            }

            if (enumToString) {
                settings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            }

            var format = Formatting.None;
            if (indendFormatting == true) {
                format = Formatting.Indented;
            }

            // this adds the Z at the end of a date when serializing, but does not change it otherwise since we already have utc
            settings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;


            // Serialize
            return JsonConvert.SerializeObject(data, format, settings);
        }

        /// <summary>
        /// New way to serialize Consistant Naming and Dates are in JS format
        /// </summary>
        /// <param name="data"></param>
        /// <param name="indendFormatting"></param>
        /// <returns></returns>
        public static string SerializeForSerializationTypeConsistent(object data,  bool indendFormatting = false) {
            // Create settings
            var settings = new JsonSerializerSettings {
                MaxDepth = PARSING_MAX_DEPTH
            };

            if (true) {
                settings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            }

            var format = Formatting.None;
            if (indendFormatting == true) {
                format = Formatting.Indented;
            }

            // this adds the Z at the end of a date when serializing, but does not change it otherwise since we already have utc
            settings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;


            // Serialize
            return JsonConvert.SerializeObject(data, format, settings);
        }

        public static string SerializeForStructuredData(object data,  bool indendFormatting = false) {
            // Create settings
            var settings = new JsonSerializerSettings {
                MaxDepth = PARSING_MAX_DEPTH
            };
            
            // CamelCase
            settings.ContractResolver = new DefaultContractResolver {
                NamingStrategy = new CamelCaseNamingStrategy(true, true)
            };
          
            // Enum To String
            if (true) {
                settings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            }

            var format = Formatting.None;
            if (indendFormatting == true) {
                format = Formatting.Indented;
            }

            // Serialize
            return JsonConvert.SerializeObject(data, format, settings);
        }


        public static JObject FromObject(object data) {
            // Create settings
            var settings = new JsonSerializerSettings {
                MaxDepth = PARSING_MAX_DEPTH,
                ContractResolver = new DefaultContractResolver {
                    NamingStrategy = new DashedLowerNamingStrategy(true, true)
                }
            };
            var serializer = JsonSerializer.Create(settings);
         
            // Serialize
            return JObject.FromObject(data,serializer);
        }

        public static T ToObject<T>(JObject jobject) {
            // Create settings
            var settings = new JsonSerializerSettings {
                MaxDepth = PARSING_MAX_DEPTH,
                ContractResolver = new DefaultContractResolver {
                    NamingStrategy = new DashedLowerNamingStrategy(true, true)
                }
            };
            var serializer = JsonSerializer.Create(settings);

            // Serialize
            return jobject.ToObject<T>(serializer);
        }


        /// <summary>
        /// To Serialize e.g. arrays
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static JToken FromObjects(object data, bool enumToString = false) {
            // Create settings
            var settings = new JsonSerializerSettings {
                MaxDepth = PARSING_MAX_DEPTH,
                ContractResolver = new DefaultContractResolver {
                    NamingStrategy = new DashedLowerNamingStrategy(true, true)
                }
            };

            // Enum To String
            if (enumToString) {
                settings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            }

            var serializer = JsonSerializer.Create(settings);

            // Serialize
            return JToken.FromObject(data, serializer);
        }


        public static T Deserialize<T>(string data) {
            // Deserialize
            return JsonConvert.DeserializeObject<T>(data);
        }


        public static T Deserialize<T>(string data, bool lowerDashed) {
            var settings = new JsonSerializerSettings {
                MaxDepth = PARSING_MAX_DEPTH
            };
            if (lowerDashed) {
                settings.ContractResolver = new DefaultContractResolver {
                    NamingStrategy = new DashedLowerNamingStrategy(true, true)
                };
            }

            // Deserialize
            return JsonConvert.DeserializeObject<T>(data,settings);
        }


        public static object Deserialize(string data, Type type) {

            // Deserialize
            return JsonConvert.DeserializeObject(data, type);
        }

        public static object Deserialize(string data, Type type, bool lowerDashed) {
            var settings = new JsonSerializerSettings {
                MaxDepth = PARSING_MAX_DEPTH 
            };
         
            if (lowerDashed) {
                settings.ContractResolver = new DefaultContractResolver {
                    NamingStrategy = new DashedLowerNamingStrategy(true, true)
                };
            }

            // Deserialize
            return JsonConvert.DeserializeObject(data, type,settings);
        }

        public class IgnoreSpecifiedContractResolver : DefaultContractResolver
        {
            // As of 7.0.1, Json.NET suggests using a static instance for "stateless" contract resolvers, for performance reasons.
            // http://www.newtonsoft.com/json/help/html/ContractResolver.htm
            // http://www.newtonsoft.com/json/help/html/M_Newtonsoft_Json_Serialization_DefaultContractResolver__ctor_1.htm
            // "Use the parameterless constructor and cache instances of the contract resolver within your application for optimal performance."
            static IgnoreSpecifiedContractResolver instance;

            static IgnoreSpecifiedContractResolver() {
                instance = new IgnoreSpecifiedContractResolver() {
                    NamingStrategy = new DashedLowerNamingStrategy(true, true)
                };
            }

            public static IgnoreSpecifiedContractResolver Instance { get { return instance; } }

            protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
            {
                var property = base.CreateProperty(member, memberSerialization);
                property.GetIsSpecified = null;
                property.SetIsSpecified = null;
                //property.Converter = null;
                //property.ItemConverter = null;
                return property;
            }
        }

       
    }
}
