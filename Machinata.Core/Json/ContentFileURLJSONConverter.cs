﻿using Machinata.Core.Data;
using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Machinata.Core {

    public partial class JSON {
        public class ContentFileURLJSONConverter : JsonConverter {

            public override bool CanConvert(Type objectType) {
                // These are stored on the DB as a string in the format of 
                // "/content/image/22wWNw/AdieuWinterblues-Image-Placeholder.png"
                if (objectType == typeof(string)) {
                    return true;
                } else {
                    return false;
                }
            }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {

                var data = JObject.Load(reader);
                if (data != null) {

                    var base64String = data["Data"]?.ToString();
                    var fileName = data["Name"]?.ToString(); ;
                    using (var db = Core.Model.ModelContext.GetModelContext(null)) {

                        var bytes = Convert.FromBase64String(base64String);
                        var contentFile = DataCenter.UploadFile(fileName, bytes, "Import", providerName: null, user: null);
                        db.ContentFiles().Add(contentFile);
                        db.SaveChanges();
                        return contentFile.ContentURL;
                    }


                }

                return null;

            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
                // These are stored on the DB as a string in the format of 
                // "/content/image/22wWNw/AdieuWinterblues-Image-Placeholder.png"
                // First validate...
                var stringValue = value as string;

                if (stringValue != null) {
                    if (stringValue.StartsWith("/content/") == false) {
                        throw new BackendException("invalid-content-file", $"The content file URL ({stringValue}) could not be imported. The URL must start with /content/!");
                    }
                    var filename = Files.GetFileNameWithExtension(stringValue);
                    var base64String = ContentNode.DownloadBinaryContent(stringValue, null);
                    var toSerialize = new JObject();
                    toSerialize["Data"] = base64String;
                    toSerialize["Name"] = filename;
                    serializer.Serialize(writer, toSerialize);

                } else {
                    throw new BackendException("invalid-content-type", $"Only strings are supported for content export");
                }
            }

        }
    }
}
