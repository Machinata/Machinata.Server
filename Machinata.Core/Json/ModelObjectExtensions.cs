using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Util;
using Machinata.Core.Model;

namespace Machinata.Core {

    public static class JSONModelObjectExtensions {

        public static JObject ConvertToJObject<T>(this T entity, FormBuilder form) where T : Core.Model.ModelObject {
            // Sanity
            if (entity == null) return null;
            // Init
            var obj = new JObject();
            // Do entity properties
            List<PropertyInfo> properties = entity.GetPropertiesForForm(form);
            foreach (var prop in properties) {
                string propName = prop.Name.ToDashedLower();
                object val = null;
                if (prop.CanRead) {
                    val = prop.GetValue(entity);
                }
                if (val != null) obj.Add(propName, JToken.FromObject(val));
                else obj.Add(propName, null);
            }
            // Do custom
            if (form.CustomProperties != null) {
                foreach (var prop in form.CustomProperties) {
                    //TODO:
                }
            }
            // Return
            return obj;
        }


        public static List<JObject> ConvertToJObjects<T>(this IQueryable<T> set, FormBuilder form) where T : Core.Model.ModelObject {
            List<JObject> ret = new List<JObject>();
            foreach (var entity in set) {
                ret.Add(entity.ConvertToJObject(form));
            }
            return ret;
        }

     }

    public static class JSONHelper {
        /// <summary>
        /// Converts an ModelObject into a JObject like ModelObject.GetJSON expect you can decide how the names of the properties look like
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="form"></param>
        /// <param name="toLowerDashedNames"></param>
        /// <returns></returns>
        public static JObject ToJSON(ModelObject entity, FormBuilder form, bool toLowerDashedNames) {

            List<PropertyInfo> props = null;
            if (form != null) props = entity.GetPropertiesForForm(form);
            else props = entity.GetProperties();
            return ToJSON(entity, toLowerDashedNames, props);

        }

        /// <summary>
        /// Creatoes an Jobject based on the List of PropertyInfos
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="toLowerDashedNames"></param>
        /// <param name="props"></param>
        /// <returns></returns>
        public static JObject ToJSON(ModelObject entity, bool toLowerDashedNames, List<PropertyInfo> props) {
            var jobj = new JObject();
            foreach (var prop in props) {
                if (prop.CanRead) {
                    // Get property value
                    var val = prop.GetValue(entity);
                    // Special handling for some special types
                    if (val is Properties) {
                        val = ((Properties)val).ToString();
                    } else if (val is Price) {
                        val = (val as Price).ToString();
                    } else if(val != null) {
                        // Try with type
                        var type = val.GetType();
                        if (type != null && type.FullName.StartsWith("System.Collections.Generic.Dictionary")) {
                            // Dictionary: we support any dictionary with a key type of string
                            // We need to validate it, then we need can just use JObject.FromObject
                            // Note: why this doesnt work automatically is unclear
                            var keyType = "".GetType(); // must be string
                            if (type.GenericTypeArguments.Length != 2 || type.GenericTypeArguments.First() != keyType) throw new Exception("ModelObject.ToJSON: invalid dictionary only dictionaries with exactly two generic type arguments are supported. The first argument (key) must be of type string.");
                            var jdictionary = JObject.FromObject(val);
                            val = jdictionary;
                        }
                    }
                    // Add value
                    if (toLowerDashedNames) {
                        jobj.Add(new JProperty(prop.Name.ToDashedLower(), val));
                    } else {
                        jobj.Add(new JProperty(prop.Name, val));
                    }
                }
            }
            return jobj;
        }
    }
}
