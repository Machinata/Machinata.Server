<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <appSettings>
    <!-- Environment -->
    <add key="Environment" value="Test" />
    <add key="Role" value="Web" />
    <add key="IsTestEnvironment" value="false" />
    <!-- Project Settings -->
    <add key="ProjectName" value="UNKNOWN" />
    <add key="ProjectID" value="Unknown" />
    <!-- Providers -->
    <add key="ProviderSerialId" value="Machinata.Core.Ids.Providers.StandardSerialIdGenerator, Machinata.Core" />
    <!-- Paths -->
    <add key="TemplatePath" value="{bin-path}Templates" />
    <add key="StaticPath" value="{bin-path}Static" />
    <add key="LocalizationPath" value="{bin-path}Localization" />
    <add key="CachePath" value="" />
    <add key="LogPath" value="" />
    <!-- Maintenance -->
    <!-- Note: Maintenance mode settings (due to their low-level nature) must set in either Web.config or ISS -->
    <add key="MaintenanceMode" value="false" />
    <add key="MaintenanceDetails" value="" />
    <!-- Routing -->
    <add key="RoutingEnableLanguageRootRoutes" value="false" />
    <!-- Currency -->
    <add key="CurrencySupported" value="CHF,USD" />
    <add key="CurrencyDefault" value="CHF" />
    <add key="CurrencyFormat" value="{1} {0:#,0.00}" />
    <add key="CurrencyThousandsSeparatorChar" value="'" />
    <add key="CurrencyZeroCentsChar" value="" />
    <add key="CurrencyConversionDefaultRounding" value="1.0" />
    <add key="CurrencyAutoSetFromContext" value="true" />
    <!-- Currency Conversions -->
    <!-- <add key="CurrencyConversionRateCHFtoEUR" value="0.87" /> -->
    <!-- <add key="CurrencyConversionRateCHFtoUSD" value="1.10" /> -->
    <!-- Decimal/Number Format Settings -->
    <add key="PercentFormat" value="0.##%" />
    <add key="DecimalFormat" value="0.######" />
    <add key="DefaultDecimalPrecision" value="18" />
    <add key="DefaultDecimalScale" value="6" />
    <add key="PriceDecimalPrecision" value="18" />
    <add key="PriceDecimalScale" value="2" />
    <!-- PDF (all units in mm) -->
    <add key="PDFPageMarginLeft" value="10" />
    <add key="PDFPageMarginRight" value="10" />
    <add key="PDFPageMarginTop" value="20" />
    <add key="PDFPageMarginBottom" value="20" />
    <add key="PDFPageWidth" value="210" />
    <add key="PDFPageHeight" value="297" />
    <add key="PDFBaseFont" value="Courier New" />
    <add key="PDFSupportUppercaseTag" value="true" />
    <add key="PDFSupportConvertNewLinesTag" value="true" />
    <add key="PDFGetLanguageFromContext" value="false" />
    <!-- Debugging -->
    <add key="DebuggingHandlerEnabled" value="true" />
    <!-- Server URLs -->
    <add key="ServerURL" value="https://{local-domain}" />
    <!-- Public Facing Server URLs -->
    <add key="PublicURL" value="https://{local-domain}" />
    <!-- Hostname Redirects (list) -->
    <add key="RedirectHostsToPublicHost" value="" />
    <!-- CDNs -->
    <add key="CDNURL" value="" />
    <!-- Localization -->
    <add key="LocalizationEnableFallback" value="true" />
    <add key="LocalizationSupportedLanguages" value="en" />
    <add key="LocalizationDefaultLanguage" value="en" />
    <add key="LocalizationAdminLanguage" value="en" />
    <add key="LocalizationEnableDatabase" value="true" />
    <!-- Dates and Tmes -->
    <add key="DefaultTimezone" value="Central European Standard Time" />
    <add key="DefaultHolidayCountryCode" value="CH" />
    <add key="DefaultHolidayCountyCode" value="CH-ZH" />
    <add key="DateFormat" value="yyyy.MM.dd" />
    <add key="DateFormatHumanReadable" value="dd.MM.yyyy" />
    <add key="MonthFormat" value="yyyy.MM" />
    <add key="TimeFormat" value="HH:mm" />
    <add key="DateTimeFormat" value="yyyy.MM.dd HH:mm" />
    <add key="DateRangeSeperator" value=" - " />
    <!-- Compression -->
    <add key="CompressionBundleEnabled" value="true" />
    <add key="CompressionPageTemplatesEnabled" value="true" />
    <add key="CompressionHTTPGZIPEnabled" value="true" />
    <add key="CompressionHTTPGZIPMimeTypes" value="text/html,text/css,text/plain,text/xml,text/x-component,text/javascript,application/x-javascript,application/javascript,application/json,application/manifest+json,application/vnd.api+json,application/xml,application/xhtml+xml,application/rss+xml,application/atom+xml,application/vnd.ms-fontobject,application/x-font-ttf,application/x-font-opentype,application/x-font-truetype,image/svg+xml,image/x-icon,image/vnd.microsoft.icon,font/ttf,font/eot,font/otf,font/opentype" />
    <!-- Caching -->
    <add key="PageTemplateCacheEnabled" value="true" />
    <add key="StaticBundleCacheEnabled" value="true" />
    <!--<add key="StaticBundleCacheEnabled-SVG" value="true" />-->
    <add key="StaticImageCacheEnabled" value="true" />
    <add key="StaticQRCodeCacheEnabled" value="true" />
    <add key="StaticCaptchaCodeCacheEnabled" value="true" />
    <add key="ContentImageCacheEnabled" value="true" />
    <add key="ThemeCacheEnabled" value="true" />
    <!-- Hot Swapping -->
    <add key="HotSwappingEnabled" value="false" />
    <add key="HotSwappingAdditionalPackageHints" value="" />
    <add key="HotSwappingModulesSubPath" value="Machinata.Server" />
    <!-- File Cache -->
    <add key="CacheFileMaxDays" value="90" />
    <!-- Static Cache -->
    <add key="StaticCacheMaxDays" value="30" />
    <!-- Errors -->
    <add key="ErrorStackTraceEnabled" value="false" />
    <add key="ErrorLogTraceEnabled" value="false" />
    <add key="ErrorUnmaskingEnabled" value="false" />
    <add key="ErrorHandlerAdminEmailsEnabled" value="true" />
    <!-- Audit Trails -->
    <add key="AuditTrailsEnabled-AWSLive" value="true" />
    <add key="AuditTrailsEnabled-NervesLive" value="true" />
    <add key="AuditTrailsEnabled-NervesTest" value="true" />
    <add key="AuditTrailsEnabled-LocalTest" value="true" />
    <add key="AuditTrailsEnabled-UnitTests" value="true" />
    <!-- Logging -->
    <add key="LogSQLEnabled" value="false" />
    <!-- Each logger is automatically loaded -->
    <!-- Encryption -->
    <!-- Note: These can never be changed once used in a live environment -->
    <!-- Note: These should always be set by the project/product to custom values using /admin/system/tools/key-generator -->
    <!--
    <add key="EncryptionCryptKeyString32"     value="" />
    <add key="EncryptionAuthKeyString32"      value="" />
    <add key="EncryptionHashSalt1" value="" />
    <add key="EncryptionHashSalt2" value="" />
    -->
    <!-- Ids -->
    <!-- Note: Default secret numbers are provided so that the Id Obfuscator works out-of-the-box for those that aren't interested in security but more in reformatting -->
    <!-- Note: These should always be set by the project/product to custom values using /admin/system/tools/key-generator -->
    <add key="IdObfuscatorStandardSecretPrime" value="1680030173" />
    <add key="IdObfuscatorStandardSecretPrimeInverse" value="1072883317" />
    <add key="IdObfuscatorStandardSecretRandomXOR" value="230230872" />
    <add key="CodeGeneratorStandardSecretPrime" value="1500450271" />
    <add key="CodeGeneratorStandardSecretRandomXOR" value="212271893" />
    
    <!-- Email Settings -->
    <add key="SendProductiveEmails" value="false" />
    <add key="EmailSubjectPrefix" value="{project-name} / " />
    <add key="EmailAdminSubjectPrefix" value="{project-name} Admin / " />
    <add key="EmailAdminSubjectPostfix" value=" ({server-url}) ({environment}) ({machine-id})" />
    <add key="EmailNotificationSubjectPrefix" value="{project-name} Admin / " />
    <add key="EmailNotificationSubjectPostfix" value=" ({server-url}) ({environment}) ({machine-id})" />
    <add key="NotificationEmails" value="" />

    <add key="EmailServer" value="smtp.mailserver.com" />
    <add key="EmailPort" value="587" />
    <add key="EmailReplyToName" value="" />
    <add key="EmailReplyToEmail" value="" />
    <add key="EmailEnabled" value="true" />
    <add key="EmailNotificationsQueueUrl" value="" />
    <add key="DefaultUnsubscribeAction" value="confirm" />
    <add key="EmailLogoPath" value="" />

    <add key="EmailPackage" value="Machinata.Email" />
    <add key="EmailFallbackPackage" value="" />
    <add key="EmailCSSBundle" value="machinata-email-bundle.css" />


    <!-- Webhooks -->
    <add key="EnabledWebhooks" value="" />
    <add key="EnabledWebhooks-NervesTest" value="mailgun" />
    
    <!-- Mailgun Settings -->
    <add key="MailgunApiKey" value="" />

    <!-- Amazon Settings -->
    <add key="AWSAccessKey" value="" />
    <!-- Amazon S3 Settings -->
    <add key="S3DefaultBucket" value="" />
    <add key="S3DefaultBucketPublicURL" value="" />
    <add key="S3ArchiveBucket" value="" />
    <!-- Analytics Settings -->
    <add key="AnalyticsEnabled" value="false" />
    <add key="AnalyticsUseWorkerPool" value="true" />
    <add key="AnalyticsId" value="" />
    <add key="AnalyticsDomain" value="" />
    <add key="AnalyticsGTMId" value="" />
    <add key="AnalyticsGA4Id" value="" />

    <!-- Analytics Settings -->
    <add key="BarcodeTypeDefault" value="EAN13" />
    <!--
    New with 'profiles':
    
    <! Analytics Settings >
    <add key="AnalyticsUseWorkerPool" value="true" />
    
    <! ANALYTICS_PROFILE_FRONTEND >
    <add key="AnalyticsEnabled-FrontEnd" value="false" />
    <add key="AnalyticsId-FrontEnd" value="" />
    <add key="AnalyticsDomain-FrontEnd" value="" />
    
    <! ANALYTICS_PROFILE_BACKEND >
    <add key="AnalyticsEnabled-PageHandler" value="false" />
    <add key="AnalyticsId-PageHandler" value="" />
    <add key="AnalyticsDomain-PageHandler" value="" />
    
    -->
   
    <add key="AnalyticsEnablePageViewTracking" value="false" />
    
    <!-- AWS Environment -->
    <add key="EnvironmentRunsOnAWS" value="false" />
    <!-- Security -->
    <add key="SecurityAllRequestsRequirePIN" value="" />
    <add key="SecurityAllPagesRequirePIN" value="" />
    <add key="SecurityForceHTTPS" value="true" />
    <add key="SecurityRequireAdminIPRestrictions" value="*" />
    <add key="SecurityAllAdminAccessIPRestrictions" value="*" />
    <add key="SecurityBasicHTTPAuthenticationEnabled" value="false" />
    <add key="SecurityBasicHTTPAuthenticationUsername" value="" />
    <add key="SecurityBasicHTTPAuthenticationPassword" value="" />
    <add key="SecuritySuperuserInitialPassword" value="{guid}" />
    <add key="SecurityAllAdminAccessRequiresTwoFactorAuthentication" value="true" />

    <add key="ValidateFrontendToBackendRequestsEnabled" value="true" />
    <add key="ValidateFrontendToBackendRequestsHashSalt1" value="6Trpa3?}F]jNH=7(m!7#iA2!x91[*:!$" />
    <add key="ValidateFrontendToBackendRequestsHashSalt2" value="Ij!wg)Cf4(:b*lfPZ!0%MRsd$z/BCm)b" />

    
    <!-- Universal Login -->
    <add key="UniversalLoginEnabled" value="false" />
    <!-- MinimumDiskSpaceGB -->
    <add key="MinimumDiskSpaceGB-Mechanic" value="5" />
    <add key="MinimumDiskSpaceGB-App" value="10" />
    <add key="MinimumDiskSpaceGB-Web" value="10" />
    <add key="MinimumDiskSpaceGB-Test" value="1" />
    <!-- Task Manager -->
    <add key="TaskManagerEnabled" value="true" />
    <!-- Database -->
    <add key="DatabaseSeedDatasets" value="DATASET_ALL,DATASET_REQUIRED,DATASET_DUMMY,DATASET_INITIAL,DATASET_RANDOM,DATASET_THEMES" />
    <add key="DatabaseMigrationForceSQLMode" value="STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION"/>
    <add key="DatabaseQuickStartup" value="false"/>
    <add key="DatabaseConnectionString" value="server={host};uid={username};password={password};database={database};Convert Zero Datetime=True"/>
    <!-- Storage -->
    <add key="FileStorageProviderName" value="file" />
    <add key="FileStorageProviderPath" value="C:\MachinataStorage\{domain}" />
    
    <!-- Maps -->
    <add key="MapsAccessToken" value="" />
    
    <!-- Robots (default is Disallow all!) -->
    <add key="RobotsTXT" value="User-Agent: *
Disallow: *
" />
    <!-- Account-->
    <add key="AccountNewUserActivationPage" value="/admin/account/activate" />
    <add key="AccountNewUserActivationSuccess" value="/admin/" />
    <add key="AccountPasswordResetPage" value="/admin/account/reset-password" />
    <add key="AccountLoginPage" value="/admin/" />
    <add key="AccountActivationExpirationHours" value="72" />
    <add key="AccountPasswordResetExpirationHours" value="1" />
    <add key="AccountUsernameUsesEmail" value="true" />
    <add key="AccountAllowGuestCreations" value="false" />
    <add key="AccountGuestCreationGroups" value="" />
    <add key="AccountUsernameToLower" value="false" />
    <!-- Worker Pool -->
    <add key="WorkerPoolMinWorkerThreads" value="1" />
    <add key="WorkerPoolMaxWorkerThreads" value="20" />
    <add key="WorkerPoolAutoScalingQueueScaleUpThreshold" value="50" />
    <add key="WorkerPoolAutoScalingQueueScaleDownThreshold" value="20" />
    <add key="WorkerPoolAutoScalingScaleUpIncrement" value="1" />
    <add key="WorkerPoolAutoScalingScaleDownIncrement" value="1" />
    <add key="WorkerPoolAutoScalingScaleUpCooldownSeconds" value="30" />
    <add key="WorkerPoolAutoScalingScaleDownCooldownSeconds" value="30" />
    <add key="WorkerPoolWorkerThreadTaskProcessTimeoutSeconds" value="60" />
    <!-- API Keys -->
    <add key="APIKeysEnabled" value="false" />
    <add key="APIKeysRequireHTTPS" value="true" />

    <!-- AuditLogs -->
    <add key="AuditLogsAutoRemoveAfterDays" value="30" />
    <add key="AuditLogsAutoRemoveSkipTargets" value="AuthToken,AccessPolicy,AccessGroup,User,SeedLog,APIKey" />
    <add key="AuditLogsAutoRemoveSkipTargetsMaxDays" value="365" />
    
    <!-- Icons-->
    <add key="IconsDefaultBundleName" value="uikit-icons8win10-bundle.svg" />


    <!-- Themes-->
    <add key="ThemeDefaultName" value="default" />

    <!-- Documentation-->
    <add key="DocumentationCMSPath" value="/Misc/Documentation" />

    <!-- Git-->
    <add key="GitLogKeyword" value="" />


    <!-- Sitemap -->
    <add key="SitemapEnabled" value="true" />


    <!-- Core Model-->
    <add key="CoreBusinessSettingsCustomKeys" value="" />

    <!--UniversalLogin-->
    <add key="UniversalLoginEnabled" value="false" />
    <add key="UniversalLoginDomains" value="" />

    <!--Headless Access-->
    <add key="AuthSecretForHeadlessAccess" value="" />

  </appSettings>
</configuration>