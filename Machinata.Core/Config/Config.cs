using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Web.Configuration;
using Machinata.Core.Exceptions;
using System.Net;
using System.Reflection;
using System.Web.Hosting;
using System.Web;

/// <summary>
/// 
/// </summary>
namespace Machinata.Core {

    /// <summary>
    /// The main configuration class for Machinata.Core where all the configuration is loaded and stored on startup.
    /// </summary>
    public partial class Config {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Internal Configuration Tracker Class

        public class ConfigurationInfo {
            public string Name;
            public string Key;
            public string Value;
            public string Source;
        }

        #endregion

        #region Private Static Variables for Config

        private const bool ENABLE_ENVIRONMENT_VARIABLE_SETTINGS = false;
        private const bool ENABLE_MACHINE_CONFIG_SETTINGS = true;
        private const bool ENABLE_LOCAL_CONFIG_SETTINGS = true;
        private const bool ENABLE_MODULE_CONFIG_SETTINGS = true;

        // Note: each key also will be first tested for the encrypted option
        private static string[] CONFIG_KEY_PRIORITY_LIST = new string[] {
            "{domain}-{key}-{environment}-{role}",
            "{domain}-{key}-{environment}",
            "{domain}-{key}",
            "{key}-{environment}-{role}",
            "{key}-{environment}",
            "{key}"
        };

        private static bool _localConfigLoaded = false;
        private static Configuration _localConfig = null;

        private static bool _machineConfigLoaded = false;
        private static Configuration _machineConfig = null;

        private static Object _moduleConfigsLock = new Object();
        private static List<Configuration> _moduleConfigs = null;

        private static Dictionary<string, ConfigurationInfo> _loadedConfigurationValues = new Dictionary<string, ConfigurationInfo>();

        #endregion

        #region Build Configs

        /// <summary>
        /// The build identifier for the entire solution. On application startup, this ID will be gerenated
        /// using the revision version number of each Machinata.* assembly. This ensures that the build ID
        /// is different if any dll has changed in any way.
        /// </summary>
        public static string BuildID = "0";
        
        public static string BuildDate = null;
        public static DateTime BuildTimestamp = DateTime.MinValue;
        public static string BuildUser = null;
        public static string BuildMachine = null;
        public static string BuildOS = null;
        public static string BuildArch = null;
        public static string BuildVersion = null;
        public static string BuildGUID = null;

        public static DateTime StartupTime = DateTime.UtcNow;
        #endregion

        #region Environment Configs

        /// <summary>
        /// The domain for the configuration. This allows machines with multiple projects to load
        /// specific domain settings on a machine level. The ConfigurationDomain should be set
        /// in the projects web.config.
        /// </summary>
        public static string Domain = GetStringSetting("ConfigurationDomain");
        
        /// <summary>
        /// The environment used for all configuration options. Any number of environments can be
        /// created as long as they have all the required settings.
        /// </summary>
        public static string Environment = GetStringSetting("Environment");

        // Which role to load from config
        // Allowed values: App,Web,Mechanic
        /// <summary>
        /// The role which defines various settings. These settings help differentiate different
        /// instances. For example, a role could be 'Web', while another could be 'App'.
        /// </summary>
        public static string Role = GetStringSetting("Role");
        

        /// <summary>
        /// The machine identifier. This setting is automatically populated.
        /// </summary>
        public static string MachineID = "unknown";

        
        public static bool IsTestEnvironment = GetBoolSetting("IsTestEnvironment");

        #endregion

        #region Maintenance Configs

        /// <summary>
        /// If set to true, all requests will return a down-for-maintance message as HTML with error code 503.
        /// Additional details (such as duration, reason, etc) can be provided by the ```MaintenanceDetails````configuration.
        /// Note: Maintenance mode settings (due to their low-level nature) must set in either Web.config or ISS as configuration key without any environment of product prefixes/suffixes.
        /// </summary>
        public static bool MaintenanceMode = false;

        /// <summary>
        /// Additional details (such as duration, reason, etc) when in maintenance mode.
        /// Note: Maintenance mode settings (due to their low-level nature) must set in either Web.config or ISS as configuration key without any environment of product prefixes/suffixes.
        /// </summary>
        public static string MaintenanceDetails = null;

        #endregion

        #region Encryption Configs

        //public static string EncryptionCryptKeyString32 = GetStringSetting("EncryptionCryptKeyString32");       
        //public static string EncryptionAuthKeyString32 = GetStringSetting("EncryptionAuthKeyString32");        
        public static byte[] EncryptionCryptKeyBytes = GetBytesSetting("EncryptionCryptKeyString32"); // WARNING: this cannot be changed after going live
        public static byte[] EncryptionAuthKeyBytes = GetBytesSetting("EncryptionAuthKeyString32"); // WARNING: this cannot be changed after going live

        public static string EncryptionHashSalt1 = GetStringSetting("EncryptionHashSalt1");        // WARNING: this cannot be changed after going live
        public static string EncryptionHashSalt2 = GetStringSetting("EncryptionHashSalt2");        // WARNING: this cannot be changed after going live


        #endregion

        #region Vapid Push Configs

      
        public static string WebPushVAPIDPublicKey = GetStringSetting("WebPushVAPIDPublicKey");    
        public static string WebPushVAPIDPrivateKey = GetStringSetting("WebPushVAPIDPrivateKey");
        public static string WebPushVAPIDSubject = GetStringSetting("WebPushVAPIDSubject");
        


        #endregion

        #region ID Configs

        public static int IdObfuscatorStandardSecretPrime = GetIntSetting("IdObfuscatorStandardSecretPrime"); // WARNING: this cannot be changed after going live, as it alters the PublicId behavior. See StandardIdObfuscator documentation...
        public static int IdObfuscatorStandardSecretPrimeInverse = GetIntSetting("IdObfuscatorStandardSecretPrimeInverse"); // WARNING: this cannot be changed after going live, as it alters the PublicId behavior. See StandardIdObfuscator documentation...
        public static int IdObfuscatorStandardSecretRandomXOR = GetIntSetting("IdObfuscatorStandardSecretRandomXOR"); // WARNING: this cannot be changed after going live, as it alters the PublicId behavior. See StandardIdObfuscator documentation...
        public static int CodeGeneratorStandardSecretPrime = GetIntSetting("CodeGeneratorStandardSecretPrime"); // WARNING: this cannot be changed after going live, as it alters the PublicId behavior. See StandardIdObfuscator documentation...
        public static int CodeGeneratorStandardSecretRandomXOR = GetIntSetting("CodeGeneratorStandardSecretRandomXOR"); // WARNING: this cannot be changed after going live, as it alters the PublicId behavior. See StandardIdObfuscator documentation...

        #endregion

        #region Database Settings

        public static string DatabaseHost = GetStringSetting("DatabaseHost");
        public static string DatabaseUsername = GetStringSetting("DatabaseUsername");
        public static string DatabasePassword = GetStringSetting("DatabasePassword");
        public static string DatabaseDatabase = GetStringSetting("DatabaseDatabase");

        /* These are deprecated in favor a more simple solution: if the database does not exist, it can
         * be re-created.
        public static bool DatabaseMigrationValidateModelOnStartup = GetBoolSetting("DatabaseMigrationValidateModelOnStartup");
        public static bool DatabaseMigrationEnableAutoDrop = GetBoolSetting("DatabaseMigrationEnableAutoDrop");
        public static bool DatabaseMigrationEnableContinueWorking = GetBoolSetting("DatabaseMigrationEnableContinueWorking");
        */
        public static bool DatabaseQuickStartup = GetBoolSetting("DatabaseQuickStartup");

        /// <summary>
        /// If not null or empty, then when analyzing the database this mode is set on that connection to ensure that the sql mode
        /// is consistent. (due to a bug when random connections have strange SQL-ANSI mode set).
        /// See https://dev.mysql.com/doc/refman/8.0/en/sql-mode.html
        /// </summary>
        public static string DatabaseMigrationForceSQLMode = GetStringSetting("DatabaseMigrationForceSQLMode", null);

        public static List<string> DatabaseSeedDatasets = GetStringListSetting("DatabaseSeedDatasets");
        
        #endregion
        
        #region Task Manager
        
        public static bool TaskManagerEnabled = GetBoolSetting("TaskManagerEnabled");
        public static readonly int TaskManagerSchedularIntervalMS = 1000;

        #endregion
        
        #region Model Settings
        

        #endregion
        
        #region Analytics Settings
        
        public static bool AnalyticsEnabled = GetBoolSetting("AnalyticsEnabled");
        public static bool AnalyticsUseWorkerPool = GetBoolSetting("AnalyticsUseWorkerPool");


        /// <summary>
        /// The Google Analytics Universal Id (newer website should use GA4 - since UA is being deprecated).
        /// </summary>
        public static string AnalyticsId = GetStringSetting("AnalyticsId");

        public static string AnalyticsDomain = GetStringSetting("AnalyticsDomain");

        /// <summary>
        /// If true, backend analytics tracking is enabled using AnalyticsId.
        /// </summary>
        public static bool AnalyticsEnablePageViewTracking = GetBoolSetting("AnalyticsEnablePageViewTracking");

        /// <summary>
        /// The Google Tag Manager id to use.
        /// </summary>
        public static string AnalyticsGTMId = GetStringSetting("AnalyticsGTMId");

        /// <summary>
        /// The Google Analytics GA4 Id
        /// </summary>
        public static string AnalyticsGA4Id = GetStringSetting("AnalyticsGA4Id");

        #endregion

        #region Project Settings

        /// <summary>
        /// The project package. This must match the product assembly name (typically Machinata.Product.XYZWebsite).
        /// </summary>
        public static string ProjectPackage = GetStringSetting("ProjectPackage","unknown");
        public static string ProjectName = GetStringSetting("ProjectName");
        public static string ProjectID = GetStringSetting("ProjectID");

        #endregion

        #region Provider Settings

        public static string ProviderSerialId = GetStringSetting("ProviderSerialId");

        #endregion

        #region Currency Settings

        /// <summary>
        /// Comma seperated list of officially supported currencies by the system.
        /// For example 'CHF,USD'
        /// </summary>
        public static List<string> CurrencySupported = GetStringListSetting("CurrencySupported");
        
        /// <summary>
        /// The default currency.
        /// For example 'CHF'
        /// </summary>
        public static string CurrencyDefault = GetStringSetting("CurrencyDefault");

        /// <summary>
        /// The currency format that is used for displaying prices (using String.ToFormat).
        /// For example, '{1} {0:#,0.00}'
        /// </summary>
        public static string CurrencyFormat = GetStringSetting("CurrencyFormat");

        /// <summary>
        /// If specified, the currency format thousands separator to use.
        /// </summary>
        public static string CurrencyThousandsSeparatorChar = GetStringSetting("CurrencyThousandsSeparatorChar");

        /// <summary>
        /// If specified, currencies with zero cents (ie 33.00) are displayed as '33.-' using the specified char.
        /// Note: This works on the formatting, not on the underlying value. That means your format must return .00 values
        /// for this feature to work.
        /// </summary>
        public static string CurrencyZeroCentsChar = GetStringSetting("CurrencyZeroCentsChar");
        
        /// <summary>
        /// The currency conversion default rounding precision.
        /// For example, if you set this to '5.0', then a automatic currency conversion will round to neareast 5 full coins.
        /// This means that, for example, CHF 100.00 converted to EUR will result in EUR 90.00 instead of EUR 87.77.
        /// </summary>
        public static decimal? CurrencyConversionDefaultRounding = GetNullableDecimalSetting("CurrencyConversionDefaultRounding");
        
        /// <summary>
        /// If enabled, then any currency=USD query param will automatically set and save the currency via a cookie.
        /// </summary>
        public static bool CurrencyAutoSetFromContext = GetBoolSetting("CurrencyAutoSetFromContext");

        #endregion

        #region Decimal/Number Format Settings
        
        public static string PercentFormat = GetStringSetting("PercentFormat");
        public static string DecimalFormat = GetStringSetting("DecimalFormat");

        /// <summary>
        /// The default decimal precision to use on the datastore for decimal types.
        /// The precision is the maximum number of digits (the precision, left of the decimal). It has a range of 1 to 65. 
        /// On MySql, decimal precision and scale maps to 'DECIMAL(18,2)',
        /// where
        ///     DECIMAL(M,D)
        ///     M is the maximum number of digits (the precision). It has a range of 1 to 65. 
        ///     D is the number of digits to the right of the decimal point (the scale). It has a range of 0 to 30 and must be no larger than M. 
        /// See https://dev.mysql.com/doc/refman/5.7/en/precision-math-decimal-characteristics.html
        /// Note: this can be modified for any column using the [DecimalPrecision(DecimalPrecisionAttribute.DecimalPrecisionType.Custom,18,8)] attribute.
        /// </summary>
        public static int DefaultDecimalPrecision = GetIntSetting("DefaultDecimalPrecision");

        /// <summary>
        /// The default decimal precision to use on the datastore for decimal types.
        /// The scale is the number of digits to the right of the decimal point (the scale). It has a range of 0 to 30 and must be no larger than the precision. 
        /// On MySql, decimal precision and scale maps to 'DECIMAL(18,2)',
        /// where
        ///     DECIMAL(M,D)
        ///     M is the maximum number of digits (the precision). It has a range of 1 to 65. 
        ///     D is the number of digits to the right of the decimal point (the scale). It has a range of 0 to 30 and must be no larger than M. 
        /// See https://dev.mysql.com/doc/refman/5.7/en/precision-math-decimal-characteristics.html
        /// Note: this can be modified for any column using the [DecimalPrecision(DecimalPrecisionAttribute.DecimalPrecisionType.Custom,18,8)] attribute.
        /// </summary>
        public static int DefaultDecimalScale = GetIntSetting("DefaultDecimalScale");

        /// <summary>
        /// The default decimal precision to use on the datastore for Price types.
        /// The precision is the maximum number of digits (the precision, left of the decimal). It has a range of 1 to 65. 
        /// </summary>
        public static int PriceDecimalPrecision = GetIntSetting("PriceDecimalPrecision");

        /// <summary>
        /// The default decimal precision to use on the datastore for Price types.
        /// The scale is the number of digits to the right of the decimal point (the scale). It has a range of 0 to 30 and must be no larger than the precision. 
        /// </summary>
        public static int PriceDecimalScale = GetIntSetting("PriceDecimalScale");

        #endregion

        #region URL Configs

        /// <summary>
        /// The server URL used when one needs to know the exact and full server address.
        /// For example: https://app.nerves.ch
        /// This differs from the PublicURL in that it can be used for generating full 
        /// AJAX request URLS, as this will match where the page is hosted.
        /// </summary>
        public static string ServerURL = GetURLSetting("ServerURL");

        /// <summary>
        /// The public URL used when one needs to know the exact and full public url address.
        /// For example: https://nerves.ch
        /// This differs from ServerURL in that it can always be used for creating full links
        /// for sharing et cetera...
        /// </summary>
        public static string PublicURL = GetURLSetting("PublicURL");
        
        /// <summary>
        /// If set, the hostname will redirect to the main hostname provided in the PublicURL.
        /// For example, one could set: 
        /// "www.nerves.ch,website.nerves.ch"
        /// which will redirect to nerves.ch (PublicURL is https://nerves.ch).
        /// The redirect is fully translated, ie, entering
        /// "https://www.nerves.ch/page?var=val" 
        /// will redirect to
        /// "https://nerves.ch/page?var=val" 
        /// Note: the redirect is 303 PERMENANT (SEO)
        /// </summary>s
        public static List<string> RedirectHostsToPublicHost = GetStringListSetting("RedirectHostsToPublicHost");

        #endregion
        
        #region Audit Trails Configs
        
        public static bool AuditTrailsEnabled = GetBoolSetting("AuditTrailsEnabled");

        #endregion

        #region Routing Configs
        
        public static bool RoutingEnableLanguageRootRoutes = GetBoolSetting("RoutingEnableLanguageRootRoutes");

        #endregion

        #region Email Configs
        
        // The email address used to send messages to admin (change this to support@nerves.ch for live systems, and your own personal for testing)
        public static List<string> AdminEmails = GetStringListSetting("AdminEmail");
        public static string TestEmail = GetStringSetting("TestEmail");

        // Email addresses for notifications (non technical) 
        public static List<string> NotificationEmails = GetStringListSetting("NotificationEmails", new List<string>());

        public static string DefaultUnsubscribeAction = GetStringSetting("DefaultUnsubscribeAction"); // USA must use "unsubscribe"
        public static string EmailSubjectPrefix = Core.Config.GetStringSetting("EmailSubjectPrefix");
        public static string EmailAdminSubjectPrefix = Core.Config.GetStringSetting("EmailAdminSubjectPrefix");
        public static string EmailAdminSubjectPostfix = Core.Config.GetStringSetting("EmailAdminSubjectPostfix");
        public static string EmailNotificationSubjectPrefix = Core.Config.GetStringSetting("EmailNotificationSubjectPrefix");
        public static string EmailNotificationSubjectPostfix = Core.Config.GetStringSetting("EmailNotificationSubjectPostfix");

        public static string EmailLogoPath = Core.Config.GetStringSetting("EmailLogoPath");

        public static string EmailPackage = GetStringSetting("EmailPackage", "Machinata.Email");
        public static string EmailFallbackPackage = GetStringSetting("EmailFallbackPackage", null);
        public static string EmailCSSBundle = GetStringSetting("EmailCSSBundle", "machinata-email-bundle.css");

        /// <summary>
        /// If set to false all emails are sent to TestEmail instead of receipient
        /// </summary>
        public static bool SendProductiveEmails = GetBoolSetting("SendProductiveEmails");

        public static bool AccountUsernameUsesEmail {get
            {
                var value = GetBoolSetting("AccountUsernameUsesEmail");
                if (value == false) {
                    throw new NotImplementedException("Usernames different to Email is not implemented.");
                }
                return value;
            }
        }
        /// <summary>
        /// Username email always tolower before register signup and during login routines
        /// </summary>
        public static bool AccountUsernameToLower = GetBoolSetting("AccountUsernameToLower");
        public static string AccountNewUserActivationPage = GetStringSetting("AccountNewUserActivationPage");
        public static string AccountNewUserActivationSuccess = GetStringSetting("AccountNewUserActivationSuccess");
        public static string AccountPasswordResetPage = GetStringSetting("AccountPasswordResetPage");
        public static string AccountLoginPage = GetStringSetting("AccountLoginPage");
        public static int AccountActivationExpirationHours = GetIntSetting("AccountActivationExpirationHours");
        public static int AccountPasswordResetExpirationHours = GetIntSetting("AccountPasswordResetExpirationHours");
        
        /// <summary>
        /// If enabled, the api call /api/account/create will allow new accounts to be created.
        /// </summary>
        public static bool AccountAllowGuestCreations = GetBoolSetting("AccountAllowGuestCreations");

        /// <summary>
        /// A list of groups (group names) that guest account creations are automatically added to.
        /// </summary>
        public static List<string> AccountGuestCreationGroups = GetStringListSetting("AccountGuestCreationGroups");

        #endregion

        #region CDN Configs

        /// <summary>
        /// The CDN URL used for resources hosted by a CDN. Leave empty if there
        /// is no CDN.
        /// </summary>
        public static string CDNURL = GetStringSetting("CDNURL");

        #endregion
        
        #region Localization Configs

        /// <summary>
        /// If enabled, translations can use a fallback language if they do not exist for the
        /// requested language.
        /// </summary>
        public static bool LocalizationEnableFallback = GetBoolSetting("LocalizationEnableFallback");

        /// <summary>
        /// The localization supported languages used by the various frontend UIs such as the CMS.
        /// Comma seperated.
        /// </summary>
        public static List<string> LocalizationSupportedLanguages = GetStringListSetting("LocalizationSupportedLanguages");
        
        /// <summary>
        /// The localization default language if no language is explicitly set.
        /// </summary>
        public static string LocalizationDefaultLanguage = GetStringSetting("LocalizationDefaultLanguage");

        /// <summary>
        /// The localization admin language to force if set.
        /// If empty, the default language detection works.
        /// </summary>
        public static string LocalizationAdminLanguage = GetStringSetting("LocalizationAdminLanguage");
        
        /// <summary>
        /// If enabled, translations can be overwritten using the database.
        /// </summary>
        public static bool LocalizationEnableDatabase = GetBoolSetting("LocalizationEnableDatabase");

        #endregion

        #region Date and Time Configs
        
        /// <summary>
        /// Defines the default time zone to use when converting dates to strings.
        /// The following values are typically available on most machines:
        /// Afghanistan Standard Time
        /// Alaskan Standard Time
        /// Alaskan Standard Time\Dynamic DST
        /// Arab Standard Time
        /// Arabian Standard Time
        /// Arabic Standard Time
        /// Arabic Standard Time\Dynamic DST
        /// Argentina Standard Time
        /// Argentina Standard Time\Dynamic DST
        /// Atlantic Standard Time
        /// Atlantic Standard Time\Dynamic DST
        /// AUS Central Standard Time
        /// AUS Eastern Standard Time
        /// AUS Eastern Standard Time\Dynamic DST
        /// Azerbaijan Standard Time
        /// Azores Standard Time
        /// Azores Standard Time\Dynamic DST
        /// Bahia Standard Time
        /// Bahia Standard Time\Dynamic DST
        /// Bangladesh Standard Time
        /// Bangladesh Standard Time\Dynamic DST
        /// Canada Central Standard Time
        /// Cape Verde Standard Time
        /// Caucasus Standard Time
        /// Caucasus Standard Time\Dynamic DST
        /// Cen. Australia Standard Time
        /// Cen. Australia Standard Time\Dynamic DST
        /// Central America Standard Time
        /// Central Asia Standard Time
        /// Central Brazilian Standard Time
        /// Central Brazilian Standard Time\Dynamic DST
        /// Central Europe Standard Time
        /// Central European Standard Time
        /// Central Pacific Standard Time
        /// Central Standard Time
        /// Central Standard Time\Dynamic DST
        /// Central Standard Time (Mexico)
        /// China Standard Time
        /// Dateline Standard Time
        /// E. Africa Standard Time
        /// E. Australia Standard Time
        /// E. Europe Standard Time
        /// E. South America Standard Time
        /// E. South America Standard Time\Dynamic DST
        /// Eastern Standard Time
        /// Eastern Standard Time\Dynamic DST
        /// Egypt Standard Time
        /// Egypt Standard Time\Dynamic DST
        /// Ekaterinburg Standard Time
        /// Ekaterinburg Standard Time\Dynamic DST
        /// Fiji Standard Time
        /// Fiji Standard Time\Dynamic DST
        /// FLE Standard Time
        /// Georgian Standard Time
        /// GMT Standard Time
        /// Greenland Standard Time
        /// Greenland Standard Time\Dynamic DST
        /// Greenwich Standard Time
        /// GTB Standard Time
        /// Hawaiian Standard Time
        /// India Standard Time
        /// Iran Standard Time
        /// Iran Standard Time\Dynamic DST
        /// Israel Standard Time
        /// Israel Standard Time\Dynamic DST
        /// Jordan Standard Time
        /// Jordan Standard Time\Dynamic DST
        /// Kaliningrad Standard Time
        /// Kaliningrad Standard Time\Dynamic DST
        /// Kamchatka Standard Time
        /// Korea Standard Time
        /// Libya Standard Time
        /// Libya Standard Time\Dynamic DST
        /// Magadan Standard Time
        /// Magadan Standard Time\Dynamic DST
        /// Mauritius Standard Time
        /// Mauritius Standard Time\Dynamic DST
        /// Mid-Atlantic Standard Time
        /// Middle East Standard Time
        /// Middle East Standard Time\Dynamic DST
        /// Montevideo Standard Time
        /// Montevideo Standard Time\Dynamic DST
        /// Morocco Standard Time
        /// Morocco Standard Time\Dynamic DST
        /// Mountain Standard Time
        /// Mountain Standard Time\Dynamic DST
        /// Mountain Standard Time (Mexico)
        /// Myanmar Standard Time
        /// N. Central Asia Standard Time
        /// N. Central Asia Standard Time\Dynamic DST
        /// Namibia Standard Time
        /// Namibia Standard Time\Dynamic DST
        /// Nepal Standard Time
        /// New Zealand Standard Time
        /// New Zealand Standard Time\Dynamic DST
        /// Newfoundland Standard Time
        /// Newfoundland Standard Time\Dynamic DST
        /// North Asia East Standard Time
        /// North Asia East Standard Time\Dynamic DST
        /// North Asia Standard Time
        /// North Asia Standard Time\Dynamic DST
        /// Pacific SA Standard Time
        /// Pacific SA Standard Time\Dynamic DST
        /// Pacific Standard Time
        /// Pacific Standard Time\Dynamic DST
        /// Pacific Standard Time (Mexico)
        /// Pakistan Standard Time
        /// Pakistan Standard Time\Dynamic DST
        /// Paraguay Standard Time
        /// Paraguay Standard Time\Dynamic DST
        /// Romance Standard Time
        /// Russian Standard Time
        /// Russian Standard Time\Dynamic DST
        /// SA Eastern Standard Time
        /// SA Pacific Standard Time
        /// SA Western Standard Time
        /// Samoa Standard Time
        /// Samoa Standard Time\Dynamic DST
        /// SE Asia Standard Time
        /// Singapore Standard Time
        /// South Africa Standard Time
        /// Sri Lanka Standard Time
        /// Syria Standard Time
        /// Syria Standard Time\Dynamic DST
        /// Taipei Standard Time
        /// Tasmania Standard Time
        /// Tasmania Standard Time\Dynamic DST
        /// Tokyo Standard Time
        /// Tonga Standard Time
        /// Turkey Standard Time
        /// Turkey Standard Time\Dynamic DST
        /// Ulaanbaatar Standard Time
        /// US Eastern Standard Time
        /// US Eastern Standard Time\Dynamic DST
        /// US Mountain Standard Time
        /// UTC
        /// UTC+12
        /// UTC-02
        /// UTC-11
        /// Venezuela Standard Time
        /// Venezuela Standard Time\Dynamic DST
        /// Vladivostok Standard Time
        /// Vladivostok Standard Time\Dynamic DST
        /// W. Australia Standard Time
        /// W. Australia Standard Time\Dynamic DST
        /// W. Central Africa Standard Time
        /// W. Europe Standard Time
        /// West Asia Standard Time
        /// West Pacific Standard Time
        /// Yakutsk Standard Time
        /// Yakutsk Standard Time\Dynamic DST
        /// </summary>
        public static string DefaultTimezone = GetStringSetting("DefaultTimezone");
        
        /// <summary>
        /// The global .net formatter for dates.
        /// </summary>
        public static string DateFormat = GetStringSetting("DateFormat");

        /// <summary>
        /// A more readable date format (usually DD.MM.YYYY)
        /// </summary>
        public static string DateFormatHumanReadable = GetStringSetting("DateFormatHumanReadable");


        /// <summary>
        /// The global .net formatter for dates. without the ady
        /// </summary>
        public static string MonthFormat = GetStringSetting("MonthFormat");

        /// <summary>
        /// The global .net formatter for times.
        /// Officially supported is 24h format: 'HH:mm'
        /// If the project needs timezones, then 'HH:mm zzz' can be used.
        /// </summary>
        public static string TimeFormat = GetStringSetting("TimeFormat");

        /// <summary>
        /// The global .net formatter for date and times.
        /// </summary>
        public static string DateTimeFormat = GetStringSetting("DateTimeFormat");
        
        public static string DateRangeSeperator = GetStringSetting("DateRangeSeperator");
        
        #endregion
        
        #region Error Configs

        public static bool ErrorStackTraceEnabled = GetBoolSetting("ErrorStackTraceEnabled");
        public static bool ErrorLogTraceEnabled = GetBoolSetting("ErrorLogTraceEnabled");
        public static bool ErrorUnmaskingEnabled = GetBoolSetting("ErrorUnmaskingEnabled");
        public static bool ErrorHandlerAdminEmailsEnabled = GetBoolSetting("ErrorHandlerAdminEmailsEnabled");

        #endregion

        #region Log Configs

        /// <summary>
        /// The CDN URL used for resources hosted by a CDN. Leave empty if there
        /// is no CDN.
        /// </summary>
        public static bool LogSQLEnabled = GetBoolSetting("LogSQLEnabled");

        #endregion
        
        #region Compression Configs
        
        public static bool CompressionBundleEnabled = GetBoolSetting("CompressionBundleEnabled");
        public static bool CompressionHTTPGZIPEnabled = GetBoolSetting("CompressionHTTPGZIPEnabled");
        public static bool CompressionPageTemplatesEnabled = GetBoolSetting("CompressionPageTemplatesEnabled");
        public static List<string> CompressionHTTPGZIPMimeTypes = GetStringListSetting("CompressionHTTPGZIPMimeTypes", new List<string>());


        #endregion

        #region Cache Configs

        public static bool PageTemplateCacheEnabled = GetBoolSetting("PageTemplateCacheEnabled");
        public static bool StaticBundleCacheEnabled = GetBoolSetting("StaticBundleCacheEnabled");
        public static bool StaticQRCodeCacheEnabled = GetBoolSetting("StaticQRCodeCacheEnabled");
        public static bool StaticCaptchaCodeCacheEnabled = GetBoolSetting("StaticCaptchaCodeCacheEnabled");
        public static bool StaticImageCacheEnabled = GetBoolSetting("StaticImageCacheEnabled");
        public static bool ContentImageCacheEnabled = GetBoolSetting("ContentImageCacheEnabled");
        public static bool ThemeCacheEnabled = GetBoolSetting("ThemeCacheEnabled");

        public static int StaticCacheMaxDays = GetIntSetting("StaticCacheMaxDays");

        #endregion

        #region Hot Swapping

        /// <summary>
        /// If enabled, the system will try to load 'hot' files directly from the project directories rather than the 
        /// compiled bin directories. This increases development times extremely since a rebuild is not needed.
        /// 
        /// Note: This should never be enabled on a non test environment (will raise exception).
        /// It is best to enable this setting on your local testing machine.
        /// 
        /// Note: Some packages are not well supported due the unreliable nature of reverse engineering where they
        /// came from.
        /// 
        /// Note: Enabling HotSwappingEnabled will automatically disable caching of the suppurted files.
        /// 
        /// Supported Hot Swapping:
        ///     Page Templates
        ///     Bundles Files
        ///     
        /// Known Issues:
        ///     Package 'Machinata.PDF'
        ///     Package 'Machinata.Emails'
        ///     Cross-Package Files (for example Machinata.Admin templates in Machinata.Product.XYZ)
        /// </summary>
        public static bool HotSwappingEnabled = GetBoolSetting("HotSwappingEnabled");

        /// <summary>
        /// A list of package names (for example Machinata.Product.ProductXYZ) as a hint for the hot swapping engine
        /// to check while finding hot swappable files.
        /// </summary>
        public static List<string> HotSwappingAdditionalPackageHints = GetStringListSetting("HotSwappingAdditionalPackageHints");

        /// <summary>
        /// Often, Machinata Core and other other modules are included as a subpath (or Git submodule). This can define
        /// this additional path when trying to find packages to hotswap to...
        /// </summary>
        public static string HotSwappingModulesSubPath = GetStringSetting("HotSwappingModulesSubPath");

        #endregion

        #region Debugging Configs

        public static bool DebuggingHandlerEnabled = GetBoolSetting("DebuggingHandlerEnabled");
        
        #endregion

        #region PDF Configs
        
        public static double PDFPageMarginLeft = GetDoubleSetting("PDFPageMarginLeft");
        public static double PDFPageMarginRight = GetDoubleSetting("PDFPageMarginRight");
        public static double PDFPageMarginTop = GetDoubleSetting("PDFPageMarginTop");
        public static double PDFPageMarginBottom = GetDoubleSetting("PDFPageMarginBottom");
        public static double PDFPageWidth = GetDoubleSetting("PDFPageWidth");
        public static double PDFPageHeight = GetDoubleSetting("PDFPageHeight");
        public static string PDFBaseFont = GetStringSetting("PDFBaseFont");
        public static string PDFHeader = GetStringSetting("PDFHeader",null);
        public static string PDFHeaderLeft = GetStringSetting("PDFHeaderLeft",null);
        public static string PDFHeaderCenter = GetStringSetting("PDFHeaderCenter",null);
        public static string PDFHeaderRight = GetStringSetting("PDFHeaderRight",null);
        public static string PDFHeaderLeftFirstPage = GetStringSetting("PDFHeaderLeftFirstPage",null);
        public static string PDFHeaderCenterFirstPage = GetStringSetting("PDFHeaderCenterFirstPage",null);
        public static string PDFHeaderRightFirstPage = GetStringSetting("PDFHeaderRightFirstPage",null);
        public static string PDFFooter = GetStringSetting("PDFFooter",null);
        public static string PDFFooterLeft = GetStringSetting("PDFFooterLeft",null);
        public static string PDFFooterCenter = GetStringSetting("PDFFooterCenter",null);
        public static string PDFFooterRight = GetStringSetting("PDFFooterRight",null);
        public static string PDFFooterLeftFirstPage = GetStringSetting("PDFFooterLeftFirstPage",null);
        public static string PDFFooterCenterFirstPage = GetStringSetting("PDFFooterCenterFirstPage",null);
        public static string PDFFooterRightFirstPage = GetStringSetting("PDFFooterRightFirstPage",null);
        public static int PDFFirstPageHeaderPusher = GetIntSetting("PDFFirstPageHeaderPusher",0);
        public static bool PDFGetLanguageFromContext = GetBoolSetting("PDFGetLanguageFromContext");

        #endregion
        
        #region Paths Configs

        public static string PathSep = System.IO.Path.DirectorySeparatorChar.ToString();
        public static string BasePath = _getBaseDirectory();
        public static string BinPath = _getBinDirectory();
        public static string TemplatePath = GetPathSetting("TemplatePath");
        public static string StaticPath = GetPathSetting("StaticPath");
        public static string CachePath = GetPathSetting("CachePath");
        public static string LogPath = GetPathSetting("LogPath");
        public static string LocalizationPath = GetPathSetting("LocalizationPath");

        #endregion
        
        #region File Storage Configs
       
        public static string FileStorageProviderName = GetStringSetting("FileStorageProviderName");
        public static string FileStorageProviderPath = GetPathSetting("FileStorageProviderPath");
        public static int CacheFileMaxDays = GetIntSetting("CacheFileMaxDays");
        

        #endregion

        #region Maps Configs

        public static string MapsAccessToken = GetStringSetting("MapsAccessToken");

        #endregion

        #region AWS Configs

        public static string S3DefaultBucket = GetStringSetting("S3DefaultBucket");
        public static string S3DefaultFolder = GetStringSetting("S3DefaultFolder", "");
        public static string AWSAccessKey = GetStringSetting("AWSAccessKey");
        public static string AWSSecretKey = GetStringSetting("AWSSecretKey", "");





        #endregion

        #region Security Configs

        /// <summary>
        /// If set, all requests to the server must have a cookie ```SecurityAllRequestsRequirePIN``` set with the value specified by
        /// this config, otherwise the request will fail with a enter-pin page.
        /// This is useful for locking down a website that is in testing, where it is still wished to test
        /// anonymous access and login functionality.
        /// </summary>
        public static string SecurityAllRequestsRequirePIN = GetStringSetting("SecurityAllRequestsRequirePIN");

        /// <summary>
        /// If set, all PageTemplateHandler requests (pages) to the server must have a cookie ```SecurityAllPagesRequirePIN``` set with the value specified by
        /// this config, otherwise the request will fail with a enter-pin page.
        /// This is useful for locking down a website that is in testing, where it is still wished to test
        /// anonymous access and login functionality.
        /// </summary>
        public static string SecurityAllPagesRequirePIN = GetStringSetting("SecurityAllPagesRequirePIN");

        /// <summary>
        /// If enabled, all http traffic is converted to https.
        /// </summary>
        public static bool SecurityForceHTTPS = GetBoolSetting("SecurityForceHTTPS");

        /// <summary>
        /// If enabled, basic HTTP authentication is required for every request.
        /// The username and password must be set via SecurityBasicHTTPAuthenticationUsername/SecurityBasicHTTPAuthenticationPassword.
        /// </summary>
        public static bool SecurityBasicHTTPAuthenticationEnabled = GetBoolSetting("SecurityBasicHTTPAuthenticationEnabled");

        /// <summary>
        /// A comma seperated list of IPs (or subnets) that are required for specific 
        /// requests that require a fixed ip as the source of the request.
        /// This can be used by any handler through the use of Handler.RequireAdminIP.
        /// If the setting is set to '*', then any IP is allowed for such ips.
        /// Note: subnets should be given in the CIDR format (ie 136.226.202.0/23).
        /// </summary>
        public static string SecurityRequireAdminIPRestrictions = GetStringSetting("SecurityRequireAdminIPRestrictions");

        /// <summary>
        /// If set, any access to a admin page or admin API must match this
        /// comma seperated list of IPs (or subnets).
        /// If the setting is set to '*', then any IP is allowed for admin access.
        /// Note: subnets should be given in the CIDR format (ie 136.226.202.0/23).
        /// </summary>
        public static string SecurityAllAdminAccessIPRestrictions = GetStringSetting("SecurityAllAdminAccessIPRestrictions");

        /// <summary>
        /// </summary>
        public static bool SecurityAllAdminAccessRequiresTwoFactorAuthentication = GetBoolSetting("SecurityAllAdminAccessRequiresTwoFactorAuthentication");


        public static bool ValidateFrontendToBackendRequestsEnabled = GetBoolSetting("ValidateFrontendToBackendRequestsEnabled");
        public static string ValidateFrontendToBackendRequestsHashSalt1 = GetStringSetting("ValidateFrontendToBackendRequestsHashSalt1");
        public static string ValidateFrontendToBackendRequestsHashSalt2 = GetStringSetting("ValidateFrontendToBackendRequestsHashSalt2");

        #endregion

        #region Barcode Configs

        /// <summary>
        /// Default Barcode type the BarCodeFileHandler uses if no type has been provided
        /// </summary>
        public static string BarcodeTypeDefault = GetStringSetting("BarcodeTypeDefault");

        #endregion

        #region Universal Login Configs
        
        public static bool UniversalLoginEnabled = GetBoolSetting("UniversalLoginEnabled");
        public static List<string> UniversalLoginDomains = GetStringListSetting("UniversalLoginDomains");
        
        #endregion
        
        #region API Keys

        public static bool APIKeysEnabled = GetBoolSetting("APIKeysEnabled");
        public static bool APIKeysRequireHTTPS = GetBoolSetting("APIKeysRequireHTTPS");
        
        #endregion

        #region Webhooks 

        public static List<string> EnabledWebhooks = GetStringListSetting("EnabledWebhooks");
        public static string MailgunApiKey = GetStringSetting("MailgunApiKey");


        #endregion

        #region AuditLogs 

        public static int AuditLogsAutoRemoveAfterDays = GetIntSetting("AuditLogsAutoRemoveAfterDays",-1);
        public static List<string> AuditLogsAutoRemoveSkipTargets = GetStringListSetting("AuditLogsAutoRemoveSkipTargets");
        public static int? AuditLogsAutoRemoveSkipTargetsMaxDays = GetNullableIntSetting("AuditLogsAutoRemoveSkipTargetsMaxDays");

        #endregion

        #region Icons 

        public static string IconsDefaultBundleName = GetStringSetting("IconsDefaultBundleName");

        #endregion

        #region Themes 

        public static string ThemeDefaultName = GetStringSetting("ThemeDefaultName");

        #endregion

        #region Sitemap 

        public static bool SitemapEnabled = GetBoolSetting("SitemapEnabled",true);

        #endregion

        #region Documentation 

        public static string DocumentationCMSPath = GetStringSetting("DocumentationCMSPath");

        #endregion

        #region Git 

        public static string GitLogKeyword = GetStringSetting("GitLogKeyword");

        #endregion

        #region AuthSecret 

        public static string AuthSecretForHeadlessAccess = Core.Config.GetStringSetting("AuthSecretForHeadlessAccess");

        #endregion 

        #region Config Helper Methods

        [Core.Lifecycle.OnApplicationStartup]
        public static void OnApplicationStartup() {


            var buildInfo = Core.Util.Build.GetProductBuildInfo();
            BuildID = buildInfo.BuildGUID;
            BuildUser = buildInfo.BuildUser;
            BuildMachine = buildInfo.BuildMachine;
            BuildDate = buildInfo.BuildDate;
            BuildTimestamp = buildInfo.BuildTimestamp;
            BuildVersion = buildInfo.BuildVersion;
            BuildGUID = buildInfo.BuildGUID;

            /*
            // Get build timestamp from the accompanying product build timestamp file (created via build events)
            try {
                var buildInfo = Core.Util.Build.GetBuildInfoForModule(Core.Config.ProjectPackage);
                BuildUser = buildInfo.BuildUser;
                BuildMachine = buildInfo.BuildMachine;
                BuildTimestamp = buildInfo.BuildTimestamp;
            } catch (Exception e) {
                _logger.Warn(e, "Could not read timestamp file:"+e.Message);
            }
            
            // Compile a build id
            string buildID = "";
            foreach(var assembly in Core.Reflection.Assemblies.GetMachinataAssemblies()) {
                _logger.Trace("Found Assembly " + assembly.GetName());
                long buildTimestampNumber = 0;
                var module = assembly.GetName().Name;
                Core.Util.Build.BuildInfo buildInfo = new Core.Util.Build.BuildInfo();
                try {
                    buildInfo = Core.Util.Build.GetBuildInfoForModule(assembly.GetName().Name);
                    buildTimestampNumber = buildInfo.BuildTimestamp.Ticks;
                    if(buildTimestampNumber > 0) {
                        buildTimestampNumber = buildTimestampNumber - (new DateTime(2000, 01, 01).Ticks);
                    } else {

                    }
                } catch(Exception e) {
                    buildTimestampNumber = assembly.GetName().Version.Build;
                    _logger.Warn("  Could not get timestamp, falling back to assembly build number: "+e.Message);
                }
                _logger.Trace("  Build Timestamp Number "+buildTimestampNumber);
                buildID += buildTimestampNumber.ToString();
            }
            buildID = Core.Encryption.DefaultHasher.HashString(buildID);
            BuildID = buildID;
            */

            _loadedConfigurationValues.Add("BuildID", new ConfigurationInfo() { Name = "BuildID", Key = "BuildID", Value = BuildID, Source = "Auto Generated" });
            _loadedConfigurationValues.Add("BuildVersion", new ConfigurationInfo() { Name = "BuildVersion", Key = "BuildVersion", Value = BuildVersion, Source = "Auto Generated" });
            _loadedConfigurationValues.Add("BuildDate", new ConfigurationInfo() { Name = "BuildDate", Key = "BuildDate", Value = BuildDate, Source = "Auto Generated" });
            _loadedConfigurationValues.Add("BuildMachine", new ConfigurationInfo() { Name = "BuildMachine", Key = "BuildMachine", Value = BuildMachine, Source = "Auto Generated" });
            _loadedConfigurationValues.Add("BuildUser", new ConfigurationInfo() { Name = "BuildUser", Key = "BuildUser", Value = BuildUser, Source = "Auto Generated" });
           
            _loadedConfigurationValues.Add("BasePath", new ConfigurationInfo() { Name = "BasePath", Key = "BasePath", Value = BasePath, Source = "Auto Generated" });
            _loadedConfigurationValues.Add("BinPath", new ConfigurationInfo() { Name = "BinPath", Key = "BinPath", Value = BinPath, Source = "Auto Generated" });

            // Try to figure out the instance id
            MachineID = System.Net.Dns.GetHostName(); // Fallback to hostname
            if (GetStringSetting("EnvironmentRunsOnAWS") == "true") {
                _logger.Trace("Resolving MachineID using AWS EC2...");
                try {
                    // See http://docs.amazonwebservices.com/AWSEC2/latest/DeveloperGuide/index.html?AESDG-chapter-instancedata.html
                    MachineID = new System.Net.WebClient().DownloadString("http://169.254.169.254/latest/meta-data/instance-id");
                } catch(Exception e) {
                    _logger.Error(e);
                }
            }
            _logger.Info("BuildID: "+BuildID);
            _logger.Info("BuildVersion: "+ BuildVersion);
            _logger.Info("BuildDate: "+ BuildDate);
            _logger.Info("BuildMachine: " + BuildMachine);
            _logger.Info("BuildUser: " + BuildUser);
            _logger.Info("Domain: "+Domain);
            _logger.Info("Environment: "+Environment);
            _logger.Info("Role: "+Role);
            _logger.Info("MachineID: "+MachineID);
            _logger.Info("BasePath: "+BasePath);
            _logger.Info("BinPath: "+BinPath);
            _logger.Info("CachePath: "+CachePath);
            _logger.Info("LogPath: "+LogPath);
            _logger.Info("ProjectPackage: "+Core.Config.ProjectPackage);
            _logger.Info("ProjectName: "+Core.Config.ProjectName);
            _logger.Info("ProjectID: "+Core.Config.ProjectID);
            _logger.Info("FileStorageProviderPath: "+FileStorageProviderPath);
            _logger.Info("TemplatePath: "+TemplatePath);
            _logger.Info("DatabaseDatabase: "+DatabaseDatabase);
            _logger.Info("DatabaseHost: "+DatabaseHost);

        }

        private static string _replacesPathVariables(string path) {
            path = path.Replace("{base-path}", BasePath);
            path = path.Replace("{bin-path}", BinPath);
            path = path.Replace("{domain}", Domain);
            return path;
        }
        
        /// <summary>
        /// Injects the URL variables for testing environments where IPs can change depending on test machine.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns></returns>
        private static string _injectURLVariables(string url) {
            if (Environment != "AWSLive") {
                // Inject ip from local machine - but don't do this for live config
                string localIP = "localhost";// fallback
                string machineHostName = System.Net.Dns.GetHostName();
                System.Net.IPHostEntry hostEntry = System.Net.Dns.GetHostEntry(machineHostName);
                foreach (IPAddress ip in hostEntry.AddressList) {
                    if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {
                        localIP = ip.ToString();
                    }
                }
                url = url.Replace("{local-ip}", localIP);
                // Get local domain
                string localDomain = localIP; // fallback
                if (GetStringSetting("LocalDomain",null) != null) localDomain = GetStringSetting("LocalDomain",null);
                url = url.Replace("{local-domain}", localDomain);
                url = url.Replace("{machine-domain}", GetStringSetting("MachineDomain", machineHostName));
                url = url.Replace("{project-id}", GetStringSetting("ProjectId", "").ToLower());
                
            }
            return url;
        }
        
        private static string _getBinDirectory() {
            try {
                // As a web app, we get from the http runtime
                // Note: couldnt figure out how to test for this nicely
                return System.Web.HttpRuntime.BinDirectory;
            } catch(Exception e) {
                // Other runtimes, like a unit test, we need to get from the assembly
                return System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + System.IO.Path.DirectorySeparatorChar.ToString();
            }
        }

        private static string _getBaseDirectory() {
            return AppDomain.CurrentDomain.BaseDirectory;
        }

        private static void _loadModuleConfigs() {
            lock(_moduleConfigsLock) {
                if(_moduleConfigs == null) {
                    // Load all module configs
                    _logger.Info("Loading all module configs...");
                    _moduleConfigs = new List<Configuration>();
                    var configsDir = _getBinDirectory() + "Config";
                    var dirInfo = new System.IO.DirectoryInfo(configsDir);
                    Configuration coreConfig = null;
                    foreach(var file in dirInfo.GetFiles()) {
                        if(file.Name.StartsWith("Machinata.") && file.Name.EndsWith(".config")) {
                            // Load it!
                            _logger.Info("  " + "Loading config " + file.Name);
                            ExeConfigurationFileMap configMap = new ExeConfigurationFileMap();
                            configMap.ExeConfigFilename = file.FullName;
                            var config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
                            if (file.Name == "Machinata.Core.config") coreConfig = config;
                            else _moduleConfigs.Add(config);
                        }
                    }
                    _moduleConfigs.Add(coreConfig);
                    _logger.Info("Done.");
                }
            }
        }

        private static void _loadLocalConfig() {
            if(!_localConfigLoaded) {
                string configFilePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web.Local.config");
                if (System.IO.File.Exists(configFilePath)) {
                    _logger.Info("Loading local config " + configFilePath);
                    ExeConfigurationFileMap configMap = new ExeConfigurationFileMap();
                    configMap.ExeConfigFilename = configFilePath;
                    _localConfig = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
                } else {
                    _logger.Info("No local config");
                }
                _localConfigLoaded = true;
            }
        }

        private static void _loadMachineConfig() {
            if (!_machineConfigLoaded) {
                string configFilePath = ConfigurationManager.AppSettings["MachineConfig"];
                if (configFilePath != null && configFilePath != "" && System.IO.File.Exists(configFilePath)) {
                    configFilePath = configFilePath.Replace("{base-path}", _getBaseDirectory());
                    configFilePath = configFilePath.Replace("{bin-path}", _getBinDirectory());
                    _logger.Info("Loading machine config " + configFilePath);
                    ExeConfigurationFileMap configMap = new ExeConfigurationFileMap();
                    configMap.ExeConfigFilename = configFilePath;
                    _machineConfig = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
                } else {
                    _logger.Info("No machine config");
                }
                _machineConfigLoaded = true;
            }
        }

        private static void _registerAppSetting(string name, string key, string value, string source) {
            if(!_loadedConfigurationValues.ContainsKey(name)) {
                _loadedConfigurationValues[name] = new ConfigurationInfo() { Name = name, Key = key, Value = value, Source = source };
            }
        }

        private static string _getAppSetting(string key, string name) {
            // Check if a env variable is set (running apache or so...)
            if (ENABLE_ENVIRONMENT_VARIABLE_SETTINGS) {
                // First check for environement variable
                var ret = System.Environment.GetEnvironmentVariable("Machinata" + key);
                if (ret != null) {
                    _registerAppSetting(name, key, ret, "Environment Variable");
                    return ret;
                }
            }
            // Check if a local Web.Local.Config file exists
            if(ENABLE_LOCAL_CONFIG_SETTINGS) {
                _loadLocalConfig();
                if(_localConfig != null) {
                    var ret = _localConfig.AppSettings.Settings[key];
                    if (ret != null) {
                        _registerAppSetting(name, key, ret.Value, "Web.Local.Config");
                        return ret.Value;
                    }
                }
            }
            // Check if a local Web.Local.Config file exists
            if (ENABLE_MACHINE_CONFIG_SETTINGS) {
                _loadMachineConfig();
                if (_machineConfig != null) {
                    var ret = _machineConfig.AppSettings.Settings[key];
                    if (ret != null) {
                        _registerAppSetting(name, key, ret.Value, "MachineConfig");
                        return ret.Value;
                    }
                }
            }
            // Return the regular .net app setting (on iis these settings can come from the app pool or web settings, in addition to the web.config)
            if (ConfigurationManager.AppSettings[key] != null) {
                var ret = ConfigurationManager.AppSettings[key];
                _registerAppSetting(name, key, ret, "Web.Config");
                return ret;
            }
            // Check all modules
            if(ENABLE_MODULE_CONFIG_SETTINGS) {
                _loadModuleConfigs();
                // Check each config
                foreach(var moduleConfig in _moduleConfigs) {
                    var ret = moduleConfig.AppSettings.Settings[key];
                    if (ret != null) {
                        _registerAppSetting(name, key, ret.Value, Core.Util.Files.GetFileNameWithExtension(moduleConfig.FilePath));
                        return ret.Value;
                    }
                }
            }
            // Nothing found
            return null;
        }

        /// <summary>
        /// Returns all the loaded configuration infos. 
        /// When a configuration is loaded for the first time, it is registered and tracked here.
        /// </summary>
        /// <returns></returns>
        public static List<ConfigurationInfo> AllLoadedConfigs() {
            return _loadedConfigurationValues.Values.ToList();
        }

        /// <summary>
        /// Compiles and returns all the keys found in the app config (web.config and IIS), local config (Web.Local.Config) and module configs.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<string> AllKeys() {
            var settings = ConfigurationManager.AppSettings.AllKeys.ToList();
            if(ENABLE_LOCAL_CONFIG_SETTINGS) {
                _loadLocalConfig();
                if(_localConfig != null) settings.AddRange(_localConfig.AppSettings.Settings.AllKeys.ToList());
            }
            if (ENABLE_MODULE_CONFIG_SETTINGS) {
                _loadModuleConfigs();
                foreach(var moduleConfig in _moduleConfigs) {
                    settings.AddRange(moduleConfig.AppSettings.Settings.AllKeys.ToList());
                }
            }
            return settings.Distinct();
        }

        public static IEnumerable<string> Find(string name) {
            return AllKeys().Where(k => k.StartsWith(name));
        }

        public static IEnumerable<string> FindForEnvironement(string name) {
            //TODO:@dan what about role?
            return Find(name).Where(k => k.EndsWith("-"+Core.Config.Environment));
        }

        public static string GetPathSetting(string name) {
            return _replacesPathVariables(GetStringSetting(name));
        }
        
        public static byte[] GetBytesSetting(string name) {
            var ret = GetStringSetting(name);
            return Encoding.UTF8.GetBytes(ret);
        }
        
        public static string GetStringSetting(string name) {
            var ret = GetStringSetting(name, null);
            if (ret == null) throw new BackendException("configuration-key-null", "The configuration key " + name + " cannot be null.");
            else return ret;
        }

        public static string GetStringSetting(string name, string defaultValue) {
            // Test each key if it is set
            foreach(var key in CONFIG_KEY_PRIORITY_LIST) {
                // Do replacements
                var formattedKey = key
                    .Replace("{domain}", Domain)
                    .Replace("{key}", name)
                    .Replace("{environment}", Environment)
                    .Replace("{role}", Role);
                // Test for encryption
                if (_getAppSetting(formattedKey + "-Encrypted", name) != null) {
                    return Core.Encryption.DefaultEncryption.DecryptString(_getAppSetting(formattedKey + "-Encrypted",name));
                } else if (_getAppSetting(formattedKey,name) != null) {
                    return _getAppSetting(formattedKey,name);
                }
            }
            // Fallback to default value
            _registerAppSetting(name, name, defaultValue, "Default Value");
            return defaultValue;
        }

        public static bool GetBoolSetting(string name) {
            return GetStringSetting(name) == "true";
        }

        public static bool GetBoolSetting(string name, bool defaultValue) {
            var ret = GetStringSetting(name, null);
            if (ret == null) return defaultValue;
            return ret == "true";
        }

        public static int GetIntSetting(string name) {
            return int.Parse(GetStringSetting(name));
        }

        public static double GetDoubleSetting(string name) {
            return double.Parse(GetStringSetting(name));
        }

        public static DateTime? GetDateTimeSetting(string name, DateTime? defaultValue = null, string format = "yyyy.MM.dd HH:mm") {
            var raw = GetStringSetting(name, null);
            try {
                var dateTime = DateTime.ParseExact(raw, format, System.Globalization.CultureInfo.InvariantCulture);
                return dateTime;
            } catch {
                return defaultValue;
            }
        }


        public static int GetIntSetting(string name, int defaultValue) {
            var ret = GetStringSetting(name, null);
            if (ret == null) return defaultValue;
            return int.Parse(ret);
        }

        public static decimal? GetNullableDecimalSetting(string name, decimal? defaultValue = null) {
            var val = GetStringSetting(name, null);
            if (string.IsNullOrEmpty(val)) return defaultValue;
            else return decimal.Parse(val);
        }

        public static int? GetNullableIntSetting(string name, int? defaultValue = null) {
            var val = GetStringSetting(name,null);
            if (string.IsNullOrEmpty(val)) return defaultValue;
            else return int.Parse(val);
        }

        public static double? GetNullableDoubleSetting(string name) {
            var val = GetStringSetting(name);
            if (string.IsNullOrEmpty(val)) return null;
            else return double.Parse(val);
        }

        /// <summary>
        /// Gets a list of string values from a setting, where each value is seperated by a comma.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static List<string> GetStringListSetting(string name, IEnumerable<string> defaultValue = null) {
            if (defaultValue != null) {
                var setting = GetStringSetting(name, null);
                if (string.IsNullOrEmpty(setting)) {
                    return defaultValue.ToList();
                } else {
                    return setting.Split(',').ToList();
                }
            }
            return GetStringSetting(name).Split(',').ToList();
        }

        /// <summary>
        /// Gets a URL for a environment configuration. Use this method to support variables in the URL.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static string GetURLSetting(string name) {
            string url = GetStringSetting(name);
            url = _injectURLVariables(url);
            return url;
        }
        
        /// <summary>
        /// Gets a key value setting where the setting string is made up of various keys and values.
        /// Such a string looks like:
        /// key1=val1;key2=val2;key3=val3
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static Dictionary<string,string> GetKeyValueSetting(string name) {
            return Core.Util.String.GetKeyValuesFromString(GetStringSetting(name));
        }


        #endregion

    }
}
