


/* ======== Machinata Fingerprint ======== */
/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (!Machinata) Machinata = {};

/// <summary>
/// Machinata Wizard JS Library
/// </summary>
/// <type>namespace</type>
Machinata.Fingerprint = {};

/// <summary>
/// If true, the fingerprint is automatically loaded (on page init) so that it is ready to use when needed.
/// </summary>
Machinata.Fingerprint.autoGet = true;

/// <summary>
/// Defines which fingerprint provider implementation to use.
/// Can be any of the following:
///  - FingerprintJS
/// </summary>
Machinata.Fingerprint.provider = "FingerprintJS";

/// <summary>
/// Defines the token that the provider may need in order to function.
/// </summary>
Machinata.Fingerprint.providerToken = null;

/// <summary>
/// If for some reason the provider has changed or a new provider environment has been switched,
/// The cache version can be adjusted to ensure that re-curring users are forced to get a new (and valid) fingerprint.
/// </summary>
Machinata.Fingerprint.cacheVersion = "1";

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Fingerprint._cacheKey = "_ga_fp";

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Fingerprint._errorId = "FINGERPRINT_ERROR";

Machinata.Fingerprint.getFingerprint = function (callback) {
    var cached = Machinata.getSessionCache(Machinata.Fingerprint._cacheKey + Machinata.Fingerprint.cacheVersion);
    if (cached != null && cached != Machinata.Fingerprint._errorId) {
        callback(cached);
        return;
    }

    var provider = Machinata.Fingerprint.Providers[Machinata.Fingerprint.provider];
    provider.getFingerprint(function (id) {
        if (id != Machinata.Fingerprint._errorId) Machinata.setSessionCache(Machinata.Fingerprint._cacheKey + Machinata.Fingerprint.cacheVersion, id);
        callback(id);
    });
};

Machinata.onInit(function () {
    // Get fingerprint
    if (Machinata.Fingerprint.autoGet == true) {
        Machinata.Fingerprint.getFingerprint(function (id) {
            //console.log("Machinata.Fingerprint.autoGet",id);
        });
    }

});


Machinata.Fingerprint.Providers = {};
Machinata.Fingerprint.Providers["FingerprintJS"] = {
    getFingerprint: function (callback) {
        // Initialize the agent at application startup.
        // https://dev.fingerprint.com/docs/subdomain-integration
        const fpPromise = FingerprintJS.load({ token: Machinata.Fingerprint.providerToken, endpoint: Machinata.Fingerprint.endpoint });
        // Get the visitor identifier when you need it.
        fpPromise
            .then(fp => fp.get())
            .then(result => callback(result.visitorId))
            .catch(error => callback(Machinata.Fingerprint._errorId));
    }
};