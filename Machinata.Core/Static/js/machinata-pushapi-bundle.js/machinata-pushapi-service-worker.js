// This code executes in its own worker or thread
self.addEventListener("install", event => {
    console.log("Service worker installed");
});
self.addEventListener("activate", event => {
    console.log("Service worker activated");
});


self.addEventListener('notificationclick', function (event) {
    var messageId = event.notification.data;
    console.log("notificationclick: ", event, event.notification.data);
    event.notification.close();

    // With notif data
    if (event.notification != null && event.notification.data != null && event.notification.data.URL != null) {
        console.log("openWindow", event.notification.data.URL);
        event.waitUntil(clients.openWindow(event.notification.data.URL));
    }

    // With actions
    // Note: Safari does not support actions
    /*
    console.log("event.notification.actions: ", event.notification.actions);
    console.log("event.action: ", event.action);
    var action = null;
    if (action == null && event.action != null) {
        action = event.action;
    }
    if (action == null && event.notification.actions.length > 0) {
        action = event.notification.actions[0]; // fallback
    }
    console.log("event.notification.actions: ", action);
    if (action.action == 'action') {
        console.log("openWindow", event.notification.actions);
        clients.openWindow(event.notification.data.url);

    }*/
});


self.addEventListener('push', function (event) {
    if (event.data) {
        console.log('This push event has data: ', event.data.text());
        console.log('This push event: ', event);
        console.log('This push event has data json: ', event.data.json());
    } else {
        console.log('This push event has no data.');
    }

    // Returns string
    //event.data.text()

    // Parses data as JSON string and returns an Object
    //event.data.json()

    // Returns blob of data
    //event.data.blob()

    // Returns an arrayBuffer
    //event.data.arrayBuffer()

    // https://web.dev/articles/push-notifications-handling-messages

    // this is the data
    //{
    //    "Title": "10 ultimative Autohacks ",
    //        "Message": "<h2>Bist du bereit, dein Auto in einen strahlenden Traum zu verwandeln? \n</h2><h2>M�chtest du den Titel �Autoreinigungs-Champion� erobern und gleichzeitig die Bewunderung aller auf dich ziehen? \n</h2><p>Egal, ob du hartn�ckige Flecken von Polstern entfernen, die Scheiben zum Gl�nzen bringen oder den Glanz der Lackoberfl�che wiederherstellen m�chtest � wir haben die ultimativen Autohacks, um dein Auto auf Hochglanz zu polieren. Ausserdem erh�ltst du coole Anti-Hitze-Ideen f�rs Auto. Lade eigene Hacks hoch und bring uns zum Staunen.&nbsp;<br></p><p><a href=\"#disclaimer\">Bevor du startest, lies bitte kurz unsere Erkl�rung durch</a><br></p>",
    //            "URL": "https://test.ch",
    //                "ThumbnailURL": "https://test.ch/image.jpg"
    //}

    // show notification with the data above



    // HINT: ACTIONS ONLY SUPPORTED IN CHROME,EDGE,OPERA
    // https://developer.mozilla.org/en-US/docs/Web/API/Notification/actions

    var data = event.data.json();

    // https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration/showNotification
    const promiseChain = self.registration.showNotification(data.Title, {
        body: data.Message,
        icon: data.ThumbnailURL,
        image: data.ThumbnailURL,
        data: data,
        actions: [
            {
                action: 'action',
                title: data.ActionTitle
            }
        ]
    })

    event.waitUntil(promiseChain);
});