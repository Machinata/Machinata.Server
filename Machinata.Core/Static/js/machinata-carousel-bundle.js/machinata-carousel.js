
/* ======== Machinata Carousel ======== */
/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (typeof Machinata === "undefined") var Machinata = ((typeof global !== 'undefined') ? global : window).MACHINATA;


/// <summary>
/// Machinata Carousel JS Library
/// </summary>
/// <type>namespace</type>
Machinata.Carousel = {};

/// <summary>
/// Creates a carousel using the slick API (see https://kenwheeler.github.io/slick/)
/// ```elements```: jQuery selector of element(s) to which a carousel is created. Typically this is the container of the carousal slides.
/// ```opts```: options for creating the carousel (see Settings at https://kenwheeler.github.io/slick/)
/// </summary>
Machinata.Carousel.create = function (elements, opts) {
    if (opts == null) opts = {};
    if (opts.addPrevNextIcons == null) opts.addPrevNextIcons = true;
    if (opts.dots == null) opts.dots = null;
    if (opts.slideChangedCallback == null) opts.slideChangedCallback = null;
    if (opts.slideWillChangeCallback == null) opts.slideWillChangeCallback = null;
    var carouselElem = elements.slick(opts);
    if (opts.slideChangedCallback != null) {
        elements.on('afterChange', function (event, slick, currentSlide) {
            opts.slideChangedCallback(slick.$slider, currentSlide);
        });
    }
    if (opts.slideWillChangeCallback != null) {
        elements.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            opts.slideWillChangeCallback(slick.$slider, currentSlide, nextSlide);
        });
    }
    // Manipulate default buttons
    if (opts.addPrevNextIcons == true) {
        carouselElem.find(".slick-prev.slick-arrow").text("").append("{icon.back}");
        carouselElem.find(".slick-next.slick-arrow").text("").append("{icon.forward}");
        carouselElem.find(".slick-dots button").text("");
    }
    return carouselElem;
};


/// <summary>
/// Create a flipbook version of a carousel which displays multiple slides at once, but with a strong 3d focus on a single
/// front facing slide at a time.
/// </summary>
Machinata.Carousel.createFlipbook = function (elements, opts) {
    // Inspired by https://benkimo6i.github.io/vanilla-js-carousel/carousel.js

    if (opts == null) opts = {};
    if (opts.slidesToShow == null) opts.slidesToShow = 5;
    if (opts.slidesToScroll == null) opts.slidesToScroll = 1;
    if (opts.startingSlideNumber == null) opts.startingSlideNumber = 0;
    if (opts.slideScaleDownFactor == null) opts.slideScaleDownFactor = 0.6;
    if (opts.addNextAndPrevZone == null) opts.addNextAndPrevZone = true;
    if (opts.equalizeSlideHeight == null) opts.equalizeSlideHeight = false;
    if (opts.slideHeightCalculator == null) opts.slideHeightCalculator = null; // opts.slideHeightCalculator = function(elem,slideElem) { return 100...}
    if (opts.allowSwiping == null) opts.allowSwiping = false;
    if (opts.swipeThreshold == null) opts.swipeThreshold = 70;
    if (opts.swipeUseOnlyTouch == null) opts.swipeUseOnlyTouch = false;

    elements.each(function () {
        // Init
        var slidesLeftAndRight = (opts.slidesToShow - 1) / 2;
        var currentSlideNumber = opts.startingSlideNumber;
        var carouselElem = $(this);
        var carouselSlides = carouselElem.children();
        var slideWidth = carouselSlides.width();


        // Add classes
        carouselElem.addClass("ui-carousel");
        carouselElem.addClass("option-flipbook");
        carouselSlides.addClass("ui-carousel-slide");

        // Create prev/next zones
        var prevZoneElem = null;
        var nextZoneElem = null;
        if (opts.addNextAndPrevZone) {
            prevZoneElem = $("<div class='prev-zone'></div>").appendTo(carouselElem);
            nextZoneElem = $("<div class='next-zone'></div>").appendTo(carouselElem);
            prevZoneElem.click(function () { prevSlide(); });
            nextZoneElem.click(function () { nextSlide(); });
        }

        function resize() {
            function getSlideHeight(elem, slideElem) {
                // Built in slideHeightCalculator - uses all three varations of height calculation...
                var maxHeight = 0;
                if (slideElem.height() > maxHeight) maxHeight = slideElem.height();
                if (slideElem[0].getBoundingClientRect().height > maxHeight) maxHeight = slideElem[0].getBoundingClientRect().height;
                if (slideElem[0].scrollHeight > maxHeight) maxHeight = slideElem[0].scrollHeight;
                return maxHeight;
            }
            function getMaxSlideHeight() {
                var maxHeight = 0;
                carouselSlides.each(function () {
                    var slideHeight = 0;
                    if (opts.slideHeightCalculator != null) {
                        slideHeight = opts.slideHeightCalculator(carouselElem, $(this));
                    } else {
                        slideHeight = getSlideHeight(carouselElem, $(this));
                    }
                    if (slideHeight > maxHeight) maxHeight = slideHeight;
                });
                return maxHeight;
            }

            if (opts.equalizeSlideHeight == true) {
                // Equalize height == true

                // Reset height
                // Note: this actually doesnt work, one would have to wait for the layout to update after setting this
                //carouselElem.height(null);
                //carouselSlides.height(null);
                // Note: unset seems to work better, not on every frame, but mostly...
                carouselElem.height("unset");
                carouselSlides.height("unset");

                // Calculate new max height
                var maxSlideHeight = getMaxSlideHeight();
                //console.log("maxSlideHeight", maxSlideHeight); //////////////////////////////////////////////////////////////
                carouselElem.height(maxSlideHeight);
                carouselSlides.height(maxSlideHeight);
                updateForSlideNumber();
            } else {
                // Equalize height == false
                var maxSlideHeight = getMaxSlideHeight();
                carouselElem.height(maxSlideHeight);
                updateForSlideNumber();
            }
        }

        function positionSlide(elem, position, zIndex) {
            // Init some dimenstions
            var width = elem.width();
            var height = elem.height();
            var carouselWidth = carouselElem.width();
            var carouselHeight = carouselElem.height();
            var perspectiveInPX = 1000;
            // position is -2..+2, 0 is middle (for slidesLeftAndRight==2)
            // Calculate scale
            var scale = Machinata.Math.map(Math.abs(position), 0, +slidesLeftAndRight, 1.0, opts.slideScaleDownFactor);
            var scaleInverse = 1.0 - scale;
            var widthAdjustedForScale = width * scale;
            var heightAdjustedForScale = height * scale;
            // Get the log position
            var posLog = Machinata.Math.map(Math.abs(position), 0, +slidesLeftAndRight, 0.0, 1.0);
            posLog = Math.sqrt(posLog);
            var posLogMax = 1;
            if (position < 0) posLog = -posLog;

            // Calculate left positiion in percent
            // This is a bit complicated, as the CSS 3d transform does percent positioning on self and not parent, and we have to take the scale into account...
            var percentFromLeftMin = scaleInverse * -0.5; // we need to shift the left most coord because we are scaling down but want the items to stick on the bounds...
            var percentFromLeftMaxRelativeToParent = (carouselWidth - widthAdjustedForScale) / carouselWidth;
            var percentFromLeftMax = (carouselWidth / width) * percentFromLeftMaxRelativeToParent + percentFromLeftMin; // translate3d in % is not relative to parent, but relative to self
            var percentFromLeft = Machinata.Math.map(posLog, -posLogMax, +posLogMax, percentFromLeftMin, percentFromLeftMax);

            var percentFromTopMin = scaleInverse * -0.5; // we need to shift the left most coord because we are scaling down but want the items to stick on the bounds...
            var percentFromTopMaxRelativeToParent = (carouselHeight - heightAdjustedForScale) / carouselHeight;
            var percentFromTopMax = (carouselHeight / height) * percentFromTopMaxRelativeToParent + percentFromTopMin; // translate3d in % is not relative to parent, but relative to self
            var percentFromTop = Machinata.Math.map(0.5, 0, 1, percentFromTopMin, percentFromTopMax);


            // Convert percent from left to abs position in percent
            var translateXInPercent = (percentFromLeft) * 100;
            var translateYInPercent = (percentFromTop) * 100;
            var translateZInPX = zIndex * 2; // we use z in px for zindex, we use slighltly larger values so that animation has better rounding values

            // Debugging
            if (false) {
                console.log(
                    "position=" + position,
                    "z=" + zIndex,
                    "width=" + width,
                    "widthAdjustedForScale=" + widthAdjustedForScale,
                    "carouselWidth=" + carouselWidth,
                    "scale=" + scale,
                    "percentFromLeft=" + percentFromLeft,
                    "percentFromLeftMin=" + percentFromLeftMin,
                    "percentFromLeftMax=" + percentFromLeftMax,
                    "translateXInPercent=" + translateXInPercent);
            }

            // Apply CSS transofrmation
            elem.css("transform", "perspective(" + perspectiveInPX + "px) translate3D(" + translateXInPercent + "%," + translateYInPercent + "%," + translateZInPX + "px) scale3d(" + scale + "," + scale + "," + scale + ") rotateY("+position*-0.4+"deg)");

            // Apply attributes
            elem.attr("data-carousel-position", position);
            elem.attr("data-carousel-zlayer", Math.abs(position));
            
        }

        function getSlideAtIndex(index) {
            if (index < 0) index = index + carouselSlides.length;
            index = index % carouselSlides.length;
            return carouselSlides.eq(index);
        }

        function prevSlide() {
            carouselElem.trigger("willChangeSlide");
            currentSlideNumber--;
            if (currentSlideNumber < 0) currentSlideNumber = carouselSlides.length-1;
            updateForSlideNumber();
            carouselElem.trigger("didChangeSlide");
        }

        function nextSlide() {
            carouselElem.trigger("willChangeSlide");
            currentSlideNumber++;
            if (currentSlideNumber > carouselSlides.length-1) currentSlideNumber = 0;
            updateForSlideNumber();
            carouselElem.trigger("didChangeSlide");
        }

        function updateForSlideNumber() {
            //console.log("currentSlideNumber", currentSlideNumber);
            var i = 0;
            var n = 0;
            var z = 100;
            if (prevZoneElem != null) prevZoneElem.css("z-index", z);
            if (nextZoneElem != null) nextZoneElem.css("z-index", z);
            while (n < carouselSlides.length) {
                if (n == 0) {
                    var pm = 0;
                    var zm = 0;
                    var im = currentSlideNumber;
                    var slideElem = getSlideAtIndex(im);
                    positionSlide(slideElem, pm, zm);
                    n++;
                    //console.log("n", n, "pm", pm, "im", im, "zm", zm, slideElem.find("h2").text());
                } else {
                    p++;
                    i++;
                    var p = i;
                    if (p > slidesLeftAndRight) p = slidesLeftAndRight;

                    var pl = -p;
                    var zl = -i;
                    var il = -i + currentSlideNumber;
                    var slideElemLeft = getSlideAtIndex(il);
                    positionSlide(slideElemLeft, pl, zl);
                    n++;

                    var pr = +p;
                    var zr = -i;
                    var ir = +i + currentSlideNumber;
                    var slideElemRight = getSlideAtIndex(ir);
                    positionSlide(slideElemRight, pr, zr);
                    n++;
                    //console.log("n", n, "pl", pl, "il", il, "zl", zl, slideElemLeft.find("h2").text());
                    //console.log("n", n, "pr", pr, "ir", ir, "zr", zr, slideElemRight.find("h2").text());
                }
            }
        }

        // Bind events
        $(window).resize(function () {
            resize();
        });

        // Bind custom events
        carouselElem.on("prev-slide swipe-right", function () {
            prevSlide();
        });
        carouselElem.on("next-slide swipe-left", function () {
            nextSlide();
        });
        carouselElem.on("resize", function () {
            resize();
        });

        // Swiping?
        if (opts.allowSwiping == true) {
            Machinata.UI.detectSwipe(carouselElem, {
                swipeThreshold: opts.swipeThreshold,
                useOnlyTouch: opts.swipeUseOnlyTouch,
            });
        }

        // Initial update
        resize();
        setTimeout(function () {
            resize();
        }, 15);
        setTimeout(function () {
            carouselElem.addClass("option-animate");
        }, 1);
    });

    return elements;
};