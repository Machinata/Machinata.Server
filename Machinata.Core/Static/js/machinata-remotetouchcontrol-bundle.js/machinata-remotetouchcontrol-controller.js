
/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Controller = {};

Machinata.RemoteTouchControl.Controller.STATUS_LOADING = "loading";
Machinata.RemoteTouchControl.Controller.STATUS_CONNECTING = "connecting";
Machinata.RemoteTouchControl.Controller.STATUS_OPENED = "opened";
Machinata.RemoteTouchControl.Controller.STATUS_CONNECTED = "connected";
Machinata.RemoteTouchControl.Controller.STATUS_ALIVE = "alive";
Machinata.RemoteTouchControl.Controller.STATUS_DEAD = "dead";
Machinata.RemoteTouchControl.Controller.STATUS_ERROR = "error";

/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Controller.init = function (opts) {
    if (Machinata.DebugEnabled == true) Machinata.Debugging.logAllErrorsToVisualConsole();

    // Init
    if (opts == null) opts = {};
    if (opts.inputControllerMode == null) opts.inputControllerMode = Machinata.RemoteTouchControl.INPUT_MODE_JOYSTICK;
    if (opts.automaticJoystickPositioning == null) opts.automaticJoystickPositioning = true;

    // Create instance
    var instance = {};
    instance.opts = opts;
    instance.status = Machinata.RemoteTouchControl.Controller.STATUS_LOADING;

    instance.cursor = {
        x: 0,
        y: 0,
        pressed: false,
        originX: 0,
        originY: 0,
    };
    instance.joystick = {};
    instance.joystick.x = 0;
    instance.joystick.y = 0;
    instance.joystick.rawX = 0;
    instance.joystick.rawY = 0;
    instance.joystick.scrollX = 0;
    instance.joystick.scrollY = 0;



    // Init controller UI
    instance = Machinata.RemoteTouchControl.Controller.initUI(instance);
    instance = Machinata.RemoteTouchControl.Controller.initRenderer(instance);

    // Connect to socket
    instance = Machinata.RemoteTouchControl.Controller.reconnect(instance);

    // Return
    return instance;
};

/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Controller.reconnect = function (instance) {
    Machinata.RemoteTouchControl.Controller.updateStatusUI(instance, Machinata.RemoteTouchControl.Controller.STATUS_CONNECTING);

    // OLD API:
    /*
    function newDataCallback(data) {
        console.log("Machinata.RemoteTouchControl.Controller.newDataCallback",data);
    }
    function newWebsocketCallback(newWebsocket) {
        // Register socket
        instance.websocket = newWebsocket;
        // Send connect request
        Machinata.RemoteTouchControl.Controller.sendConnectStatus(instance);
    }
    var websocket = Machinata.WebSocket.init(
        instance.opts.websocketEndpoint + instance.opts.accessCode,
        newDataCallback,
        null,
        null,
        null,
        newWebsocketCallback);
    instance.websocket = websocket;*/

    // NEW API:
    var socketOpts = {
        unpackDataAsJSON: true,
        onSocketCreate: function (websocket) {
            Machinata.RemoteTouchControl.Controller.updateStatusUI(instance, Machinata.RemoteTouchControl.Controller.STATUS_CONNECTING);
            // Register socket
            instance.websocket = websocket;
        },
        onSocketOpen: function (websocket, event) {
            Machinata.RemoteTouchControl.Controller.updateStatusUI(instance, Machinata.RemoteTouchControl.Controller.STATUS_OPENED);
            // Send connect request
            Machinata.RemoteTouchControl.Controller.sendConnectRequest(instance);

        },
        onSocketError: function (websocket, event, code) {
            Machinata.RemoteTouchControl.Controller.updateStatusUI(instance, Machinata.RemoteTouchControl.Controller.STATUS_ERROR);

        },
        onSocketMessage: function (websocket, event, data, message) {
            // CONTROLLER logic:
            Machinata.debug("Machinata.RemoteTouchControl.Controller: got data ", data, message);
            if (message != null) {
                if (message.type == "connect-ack") {
                    instance.websocketConnectAck = true;
                    Machinata.RemoteTouchControl.Controller.updateStatusUI(instance, Machinata.RemoteTouchControl.Controller.STATUS_CONNECTED);
                    // Set alive timer
                    if (instance.websocketAliveTimer != null) clearInterval(instance.websocketAliveTimer);
                    instance.websocketAliveTimer = setInterval(function () {
                        var now = new Date();
                        var last = instance.websocketAliveLastTimestamp || 0;
                        if (now - last > 2000) {
                            Machinata.RemoteTouchControl.Controller.updateStatusUI(instance, Machinata.RemoteTouchControl.Controller.STATUS_DEAD);

                        }
                    }, 2000);
                } else if (message.type == "connection-alive") {
                    if (instance.websocketConnectAck == true) {
                        var now = new Date();
                        instance.websocketAliveLastTimestamp = now;
                        Machinata.RemoteTouchControl.Controller.updateStatusUI(instance, Machinata.RemoteTouchControl.Controller.STATUS_ALIVE);
                    }
                }
            }
        },
        onSocketClose: function (websocket, event, code) {

        }
    };
    instance.websocketConnectAck = false;
    instance.websocketAliveLastTimestamp = null;
    instance.websocket = Machinata.WebSocket.init(instance.opts.websocketEndpoint + instance.opts.accessCode, socketOpts);

    return instance;

};

/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Controller.sendMessage = function (instance, message) {
    if (instance.websocket != null) {
        instance.websocket.send(JSON.stringify(message));
    }
};

/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Controller.sendConnectRequest = function (instance) {
    var message = {
        type: "connect-request"
    };
    Machinata.RemoteTouchControl.Controller.sendMessage(instance, message);
};

/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Controller.sendJoystickStatus = function (instance) {
    var message = {
        type: "joystick",
        data: instance.joystick,
    };
    Machinata.RemoteTouchControl.Controller.sendMessage(instance, message);
};

/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Controller.sendClickEvent = function (instance) {
    var message = {
        type: "click"
    };
    Machinata.RemoteTouchControl.Controller.sendMessage(instance, message);
};

/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Controller.initUI = function (instance) {
    // Init
    var lastUpdatePressedState = false;
    instance.elem = $(".ma-rtc-input-controller");
    
    // Create canvas elem
    instance.canvasElem = $("<canvas/>").appendTo(instance.elem);
    instance.canvasElem.css("position", "absolute");
    instance.canvasElem.css("top", "0px");
    instance.canvasElem.css("left", "0px");
    instance.canvasElem.attr("width", instance.elem.width());
    instance.canvasElem.attr("height", instance.elem.height());

    // Create status elem
    instance.statusElem = $("<div class='status'><div class='msg'></div></div>").appendTo(instance.elem);
    instance.statusMSGElem = instance.statusElem.find(".msg");
    instance.statusMSGElem.css("color", Machinata.RemoteTouchControl.CONTROLLER_TEXT_COLOR)
    Machinata.RemoteTouchControl.Controller.updateStatusUI(instance, Machinata.RemoteTouchControl.Controller.STATUS_LOADING);

    // Initial cursor and joystick state
    instance.cursor.originX = instance.canvasElem.width() / 2;
    instance.cursor.originY = instance.canvasElem.height() / 2;
    
    function updateCursor(event, sendToScreen, trackOrigin) {
        var parentOffset = instance.canvasElem.parent().offset();
        //or canvasElem.offset(); if you really just want the current element's offset

        var pageX = event.pageX;
        var pageY = event.pageY;

        // https://stackoverflow.com/questions/36312150/mousedown-event-not-firing-on-tablet-mobile-html5-canvas
        // for mobile
        if (Machinata.Device.isTouchEnabled() == true) {
            pageX = (event.targetTouches[0] ? event.targetTouches[0].pageX : event.changedTouches[event.changedTouches.length - 1].pageX);
            pageY = (event.targetTouches[0] ? event.targetTouches[0].pageY : event.changedTouches[event.changedTouches.length - 1].pageY);
        }

        var x = pageX - parentOffset.left;
        var y = pageY - parentOffset.top;

        instance.cursor.x = x;
        instance.cursor.y = y;
        if (trackOrigin == true) {
            if (instance.opts.automaticJoystickPositioning == true) {
                instance.cursor.originX = x;
                instance.cursor.originY = y;
            } else {
                instance.cursor.originX = instance.canvasW / 2;
                instance.cursor.originY = instance.canvasH / 2;
            }
        } 
        instance.joystick.rawX = 0;
        instance.joystick.rawY = 0;
        if (instance.cursor.pressed == true) {
            instance.joystick.rawX = x - instance.cursor.originX;
            instance.joystick.rawY = y - instance.cursor.originY;
        }
        // Determin the strength and normalize to -1.0 to 1.0
        var dist = Math.sqrt(instance.joystick.rawX * instance.joystick.rawX + instance.joystick.rawY * instance.joystick.rawY);
        var angle = Math.atan2(instance.joystick.rawY, instance.joystick.rawX);
        var distLimited = dist;
        if (distLimited > instance.joystickRadius) distLimited = instance.joystickRadius;
        instance.joystick.strengthRaw = dist / instance.joystickRadius;
        instance.joystick.strength = (distLimited / instance.joystickRadius);
        // If overstrength, then tune back the strength using the strengthRaw overflow
        if (instance.joystick.strengthRaw > 1.0) {
            instance.joystick.strength = instance.joystick.strength - (instance.joystick.strengthRaw - 1.0);
            instance.joystick.strength = Math.max(0, instance.joystick.strength);
        }
        // Normalize x and y using final strength
        instance.joystick.x = instance.joystick.strength * Math.cos(angle);
        instance.joystick.y = instance.joystick.strength * Math.sin(angle);

        // Scrolling?
        instance.joystick.scrollX = 0;
        instance.joystick.scrollY = 0;
        var angleP = angle / Math.PI;
        var scrollStartRadius = 1.1;
        if (instance.joystick.strengthRaw > scrollStartRadius) {
            var scrollStrength = instance.joystick.strengthRaw - scrollStartRadius;
            var scrollStrength = Math.min(1.0, scrollStrength);
            if (angleP >= -0.7 && angleP <= -0.3) {
                // Scroll up
                instance.joystick.scrollY = -scrollStrength;
            } else if (angleP >= 0.3 && angleP <= 0.7) {
                // Scroll down
                instance.joystick.scrollY = +scrollStrength;
            } else if (angleP <= -0.9 || angleP >= 0.9) {
                // Scroll left
                instance.joystick.scrollX = -scrollStrength;
            } else if (angleP >= -0.1 && angleP <= 0.1) {
                // Scroll right
                instance.joystick.scrollX = +scrollStrength;
            }
        }

        
        if (instance.cursor.pressed == true || lastUpdatePressedState != instance.cursor.pressed) {
            if (sendToScreen == true) Machinata.RemoteTouchControl.Controller.sendJoystickStatus(instance);
        }
        lastUpdatePressedState = instance.cursor.pressed;

        console.log(instance.joystick);
    }

    // Resize events
    function updateSize() {
        instance.canvasElem.attr("width", instance.elem.width());
        instance.canvasElem.attr("height", instance.elem.height());
        instance.canvasW = instance.canvasElem.width();
        instance.canvasH = instance.canvasElem.height();
        instance.canvasS = Math.min(instance.canvasW, instance.canvasH);
        instance.joystickRadius = Math.min(140,instance.canvasS * 0.65 / 2);
        instance.knobRadius = instance.canvasS * 0.2 / 2;
    }
    $(window).resize(updateSize);
    updateSize();

    // Canvas events
    var eventBindingsMove = "mousemove";
    var eventBindingsStart = "mousedown";
    var eventBindingsEnd = "mouseup";
    if (Machinata.Device.isTouchEnabled() == true) {
        eventBindingsMove = "touchmove";
        eventBindingsStart = "touchstart";
        eventBindingsEnd = "touchend";
    }
    instance.canvasElem.on(eventBindingsMove, function (event) {
        // Timekeeping
        var elapsed = (new Date()) - instance.cursor.pressedTimestamp;
        // Make sure we really want to move (might just ba tap/click)
        if (elapsed > Machinata.RemoteTouchControl.JOYSTICK_MOVE_DELAY_MS) {
            updateCursor(event,true);
        }
    });
    instance.canvasElem.on(eventBindingsStart, function (event) {
        // Timekeeping
        instance.cursor.pressed = true;
        instance.cursor.pressedTimestamp = new Date();

        updateCursor(event, false, true);
    });
    instance.canvasElem.on(eventBindingsEnd, function (event) {
        // Timekeeping
        var elapsed = (new Date()) - instance.cursor.pressedTimestamp;
        instance.cursor.pressed = false;
        updateCursor(event, true);
        // If we only moved for a very short period of time, we assume a click/tap
        if (elapsed <= Machinata.RemoteTouchControl.JOYSTICK_CLICK_MAX_TIME_MS) {
            instance.cursor.lastClickTimestamp = new Date();
            Machinata.RemoteTouchControl.Controller.sendClickEvent(instance);
        }
    });

    return instance;
};

/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Controller.updateStatusUI = function (instance,status,text) {
    console.log("Machinata.RemoteTouchControl.Controller: new status: ",status);
    // if(status == "") msg = "{text.remote-touch-control.connecting}";
    instance.statusMSGElem.text(status); //todo
}

/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Controller.initRenderer = function (instance) {

    // Renderer for joystick
    var renderer = Machinata.Drawing.createRenderer(instance.canvasElem, function (ctx, time, renderer) {

       

        if (instance.cursor == null) return;
        if (instance.joystick == null) return;

        var timeSinceLastClick = (new Date) - (instance.cursor.lastClickTimestamp || 0);

        var centerX = instance.canvasW / 2;
        var centerY = instance.canvasH / 2;
        centerX = instance.cursor.originX;
        centerY = instance.cursor.originY;

        var knobX = centerX;
        var knobY = centerY;
        if (instance.cursor.pressed == true) {
            knobX = instance.cursor.x;
            knobY = instance.cursor.y;
        }

        var fontSize = Math.round(instance.joystickRadius * 0.10);
        var font = fontSize + "px" + " {theme.text-font}";
        ctx.font = font;
    
        // Clear
        ctx.clearRect(0, 0, instance.canvasElem.width(), instance.canvasElem.height());

        // Draw joystick area
        ctx.beginPath();
        ctx.arc(
            centerX, //x,
            centerY, //y,
            instance.joystickRadius, //radius,
            0,//startAngle,
            Math.PI * 2,//endAngle
        );
        ctx.closePath();
        ctx.fillStyle = "rgb(30,30,30)";
        ctx.fill();

        // Draw origin
        if (true) {
            ctx.beginPath();
            ctx.arc(
                centerX, //x,
                centerY, //y,
                instance.knobRadius*0.2, //radius,
                0,//startAngle,
                Math.PI * 2,//endAngle
            );
            ctx.closePath();
            ctx.fillStyle = Machinata.RemoteTouchControl.JOYSTICK_KNOB_SOLID_COLOR;
            ctx.fill();
        }


        // Draw stick
        ctx.beginPath();
        ctx.moveTo(centerX,centerY);
        ctx.lineTo(knobX, knobY);
        ctx.lineWidth = 10;
        ctx.strokeStyle = Machinata.RemoteTouchControl.JOYSTICK_KNOB_SOLID_COLOR;
        ctx.stroke();

        // Draw joystick axis state (as a knob) (debug only)
        if (false) {
            ctx.beginPath();
            ctx.arc(
                centerX + instance.joystick.x * instance.joystickRadius, //x,
                centerY + instance.joystick.y * instance.joystickRadius, //y,
                instance.knobRadius / 2, //radius,
                0,//startAngle,
                Math.PI * 2,//endAngle
            );
            ctx.closePath();
            ctx.fillStyle = "red";
            ctx.fill();
        }



        // Draw scroll arrows
        function drawScrollArrow(x, y, angle, strength, label) {
            if (strength == 0) return;
            ctx.save(); {
                var alpha = Machinata.Math.map(strength, 0.0, 1.0, 0.0, 1.0);
                var lineSegmentSizeW = instance.joystickRadius * 0.4;
                var lineSegmentSizeH = lineSegmentSizeW * 0.4;
                ctx.translate(x + centerX, y + centerY);
                ctx.rotate(angle);
                ctx.translate(0, -lineSegmentSizeH*2.0);
                ctx.beginPath();
                ctx.moveTo(-lineSegmentSizeW, lineSegmentSizeH);
                ctx.lineTo(0, 0);
                ctx.lineTo(+lineSegmentSizeW, lineSegmentSizeH);
                ctx.closePath();

                // Draw solid background, then the faded overlay
                ctx.globalAlpha = 1;
                ctx.fillStyle = "black";
                ctx.fill();
                ctx.lineWidth = 10 / 2;
                ctx.strokeStyle = "black";
                ctx.stroke();
                ctx.globalAlpha = alpha;
                ctx.fillStyle = "white";
                ctx.fill();

                if (angle == Math.PI) {
                    ctx.rotate(-angle);
                    ctx.translate(0, fontSize);
                } else {
                    ctx.translate(0, -fontSize);
                }
                ctx.textAlign = "center";
                ctx.textBaseline = "middle";
                // Draw solid text, then the faded overlay
                ctx.globalAlpha = 1;
                ctx.fillStyle = "black";
                ctx.fillText(label, 0, 0);
                ctx.lineWidth = 10/2;
                ctx.strokeStyle = "black";
                ctx.strokeText(label, 0, 0);
                ctx.globalAlpha = alpha;
                ctx.fillStyle = "white";
                ctx.fillText(label, 0, 0);
            } ctx.restore();
        }
        drawScrollArrow(0, -instance.joystickRadius, 0, Math.max(instance.joystick.scrollY * -1, 0), "Scroll Up"); // up
        drawScrollArrow(0, +instance.joystickRadius, Math.PI, Math.max(instance.joystick.scrollY, 0), "Scroll Down"); // down
        drawScrollArrow(-instance.joystickRadius, 0, -Math.PI / 2, Math.max(instance.joystick.scrollX * -1, 0), "Scroll Left"); // left
        drawScrollArrow(+instance.joystickRadius, 0, Math.PI / 2, Math.max(instance.joystick.scrollX, 0), "Scroll Right"); // right


        // Draw knob
        ctx.beginPath();
        ctx.arc(
            knobX, //x,
            knobY, //y,
            instance.knobRadius, //radius,
            0,//startAngle,
            Math.PI * 2,//endAngle
        );
        ctx.closePath();
        if (timeSinceLastClick < Machinata.RemoteTouchControl.JOYSTICK_CLICK_EFFECT_DURATION_MS) {
            ctx.fillStyle = Machinata.RemoteTouchControl.JOYSTICK_KNOB_BORDER_COLOR;
            ctx.strokeStyle = Machinata.RemoteTouchControl.JOYSTICK_KNOB_SOLID_COLOR;
        } else {
            ctx.fillStyle = Machinata.RemoteTouchControl.JOYSTICK_KNOB_SOLID_COLOR;
            ctx.strokeStyle = Machinata.RemoteTouchControl.JOYSTICK_KNOB_BORDER_COLOR;
        }
        ctx.lineWidth = 3;
        ctx.fill();
        ctx.stroke();
    });
    //renderer.showStats = true;
    renderer.start();

    // Save reference and return
    instance.renderer = renderer;
    return instance;
};