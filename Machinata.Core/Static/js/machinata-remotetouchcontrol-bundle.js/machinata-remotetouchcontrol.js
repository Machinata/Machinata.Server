


/* ======== Machinata Remote Touch Control ======== */
/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (!Machinata) Machinata = {};

/// <summary>
/// Machinata Wizard JS Library
/// </summary>
/// <type>namespace</type>
Machinata.RemoteTouchControl = {};

Machinata.RemoteTouchControl.INPUT_MODE_ABSOLUTE = "INPUT_MODE_ABSOLUTE";
Machinata.RemoteTouchControl.INPUT_MODE_JOYSTICK = "INPUT_MODE_JOYSTICK";

Machinata.RemoteTouchControl.CURSOR_AUTOHIDE_TIME_MS = 5000;
Machinata.RemoteTouchControl.CURSOR_CLICK_EFFECT_DURATION_MS = 200;
Machinata.RemoteTouchControl.CURSOR_SOLID_COLOR = "white";
Machinata.RemoteTouchControl.CURSOR_BORDER_COLOR = "black";

Machinata.RemoteTouchControl.CONTROLLER_TEXT_COLOR = "white";

Machinata.RemoteTouchControl.JOYSTICK_MOVE_DELAY_MS = 100;
Machinata.RemoteTouchControl.JOYSTICK_CLICK_MAX_TIME_MS = 150;
Machinata.RemoteTouchControl.JOYSTICK_CLICK_EFFECT_DURATION_MS = 200;
Machinata.RemoteTouchControl.JOYSTICK_KNOB_SOLID_COLOR = Machinata.RemoteTouchControl.CURSOR_SOLID_COLOR;
Machinata.RemoteTouchControl.JOYSTICK_KNOB_BORDER_COLOR = Machinata.RemoteTouchControl.CURSOR_BORDER_COLOR;

