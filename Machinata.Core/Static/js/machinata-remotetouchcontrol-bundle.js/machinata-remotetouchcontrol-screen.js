

/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Screen = {};

/// <summary>
/// Options:
///  - accessTokenGeneratorEndpoint
///  - autoHideCursor
///  - websocketEndpoint
///  - remoteControlURL
///  - generateAccessTokenCallback
///  - userMovementCallback
///  - userScrollCallback
/// </summary>
Machinata.RemoteTouchControl.Screen.init = function (opts) {
    if (Machinata.DebugEnabled == true) Machinata.Debugging.logAllErrorsToVisualConsole();

    // Init
    if (opts.accessTokenGeneratorEndpoint == null) throw "opts.accessTokenGeneratorEndpoint must be set to a API Call URL";

    // Create instance
    var instance = {};
    instance.opts = opts;

    Machinata.RemoteTouchControl.Screen.initCursor(instance);

    // Return
    return instance;
};

/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Screen.initCursor = function (instance) {
    Machinata.debug("Machinata.RemoteTouchControl.Screen.initCursor");

    instance.cursorElem = $(".ma-rtc-input-cursor");
    if (instance.cursorElem.length == 0) {
        instance.cursorElem = $("<div class='ma-rtc-input-cursor'/>");
        instance.cursorElem.css("position","fixed");
        instance.cursorElem.css("width",30);
        instance.cursorElem.css("height",30);
        instance.cursorElem.css("background-color", Machinata.RemoteTouchControl.CURSOR_SOLID_COLOR);
        instance.cursorElem.css("border", "2px solid " + Machinata.RemoteTouchControl.CURSOR_BORDER_COLOR);
        instance.cursorElem.css("border-radius", "50%");
        instance.cursorElem.css("z-index", "99999");
        instance.cursorElem.css("pointer-events", "none");
        instance.cursorElem.appendTo($("body"));
    }

    instance.cursor = {x: 0, y: 0, speed: 1.5};
    instance.joystick = { x: 0, y: 0 };

    Machinata.RemoteTouchControl.Screen.enableRemoteCursor(instance);
};

/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Screen.registerUserMovement = function (instance) {
    instance.lastUserAction = new Date();
    if (instance.opts.userMovementCallback) instance.opts.userMovementCallback(instance);
};

/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Screen.enableRemoteCursor = function (instance) {

    instance.cursor.x = $(window).width() / 2;
    instance.cursor.y = $(window).height() / 2;

    var cursorWidth = instance.cursorElem.width();
    var cursorHeight = instance.cursorElem.height();

    var lastTimestamp;
    function update(timestamp) {
        // Timekeepting
        var delta = 0;
        if (lastTimestamp != null) delta = timestamp - lastTimestamp;
        lastTimestamp = timestamp;
        var timeSinceLastUserAction = (new Date) - (instance.lastUserAction || 0);
        var timeSinceLastClick = (new Date) - (instance.lastClick || 0);

        //TODO: set joystick to zero if a update hasn't been recieved for a while...

        // Update cursor position
        instance.cursor.x += instance.joystick.x * delta * instance.cursor.speed;
        instance.cursor.y += instance.joystick.y * delta * instance.cursor.speed;

        // Find out scroll value
        if (instance.joystick.scrollX != 0 || instance.joystick.scrollY != 0) {
            if (instance.opts.userScrollCallback) instance.opts.userScrollCallback(instance);
        }

        // Clip to bounds
        if (instance.cursor.x < 0) instance.cursor.x = 0;
        if (instance.cursor.y < 0) instance.cursor.y = 0;
        if (instance.cursor.x > $(window).width()) instance.cursor.x = $(window).width();
        if (instance.cursor.y > $(window).height()) instance.cursor.y = $(window).height();

        // Update UI and request next frame
        instance.cursorElem.css("top", instance.cursor.y - cursorHeight / 2 + "px").css("left", instance.cursor.x - cursorWidth / 2 + "px");
        if (instance.opts.autoHideCursor == true) {
            if (timeSinceLastUserAction > Machinata.RemoteTouchControl.CURSOR_AUTOHIDE_TIME_MS) instance.cursorElem.hide();
            else instance.cursorElem.show();
        }
        if (timeSinceLastClick < Machinata.RemoteTouchControl.CURSOR_CLICK_EFFECT_DURATION_MS) {
            instance.cursorElem.css("background-color", Machinata.RemoteTouchControl.CURSOR_BORDER_COLOR);
            instance.cursorElem.css("border", "2px solid " + Machinata.RemoteTouchControl.CURSOR_SOLID_COLOR);
        } else {
            instance.cursorElem.css("background-color", Machinata.RemoteTouchControl.CURSOR_SOLID_COLOR);
            instance.cursorElem.css("border", "2px solid " + Machinata.RemoteTouchControl.CURSOR_BORDER_COLOR);
        }
        window.requestAnimationFrame(update);
    }
    window.requestAnimationFrame(update);
};


/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Screen.simulatCursorClick = function (instance) {
    // Simulates a click by seeking for objects under the cursor
    function simulateClickOnDocument(document, x, y, isIframe) {
        var element = document.elementFromPoint(x, y);
        if (element != null) {
            
            // Iframe?
            if (element.nodeName == "IFRAME") {
                var iframe = element;
                var iframeWindow = (iframe.contentWindow || iframe.contentDocument);
                var iframeRect = iframe.getBoundingClientRect();
                simulateClickOnDocument(
                    iframeWindow.document,
                    x - iframeRect.left,
                    y - iframeRect.top,
                    true
                );
            } else if (element.nodeName == "HTML" || element.nodeName == "BODY") {
                // Ignore
            } else {
                try {
                    console.log("marker",element.nodeName,element.onclick);
                    element.click();
                } catch (e) { }
            }
        }
    }

    simulateClickOnDocument(document, instance.cursor.x, instance.cursor.y, false);

    
};

/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Screen.generateAccessToken = function (instance ) {
    // Close any old ones on server?

    // Request a new access token and channel

   

    var params = {};
    params["forced-access-token"] = Machinata.queryParameter("ac");
    if (instance.session != null && instance.session.accessCode != null) {
        params["old-access-token"] = instance.session.accessCode;
    }

    var call = Machinata.apiCall(instance.opts.accessTokenGeneratorEndpoint, params);
    call.success(function (message) {

        instance.session = {};

        instance.session.accessCode = message.data["access-code"];
        instance.session.websocketEndpoint = message.data["websocket-endpoint"] || instance.opts.websocketEndpoint;
        instance.session.remoteControlURL = message.data["remote-control-url"] || instance.opts.remoteControlURL; 

        // Setup frontend URL
        if (window.location.protocol == "http:") {
            instance.session.remoteControlURL = instance.session.remoteControlURL.replace("https:", "http:");
        }
        instance.session.remoteControlURL = instance.session.remoteControlURL.replace("{access-code}", instance.session.accessCode);

        instance.session.qrCodeURL = Machinata.RemoteTouchControl.Screen.createQRCodeForAccessToken(instance);
        instance.session.qrCodeImageURL = "/static/qrcode?data=" + encodeURIComponent(Machinata.RemoteTouchControl.Screen.createQRCodeForAccessToken(instance));

        // Connect!
        Machinata.RemoteTouchControl.Screen.connectToWebsocket(instance);

        // Callback
        if (instance.opts.generateAccessTokenCallback) instance.opts.generateAccessTokenCallback(instance);
           
       

    });
    call.genericError();
    call.send();

    // Return
    //return {
    //    accessCode: instance.session.accessCode,
    //    websocketEndpoint: instance.session.websocketEndpoint,
    //};
    return instance;
   
};

/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Screen.connectToWebsocket = function (instance) {

    // OLD API
    /*
    function newDataCallback(data) {
        try {
            var message = JSON.parse(data);
            
        } catch (error){
            // not a json
            console.warn("newDataCallback: data is not JSON:",data);
            return;
        }
        console.log("Machinata.RemoteTouchControl.Screen", message);
        if (message.type == "joystick") {
            Machinata.RemoteTouchControl.Screen.registerUserMovement(instance);
            instance.joystick = message.data;
        } else if (message.type == "click") {
            instance.lastClick = new Date();
            Machinata.RemoteTouchControl.Screen.registerUserMovement(instance);
            Machinata.RemoteTouchControl.Screen.simulatCursorClick(instance);
        }
    };

    var websocket = Machinata.WebSocket.init(
        instance.session.websocketEndpoint + instance.session.accessCode, // url
        newDataCallback, // newDataCallback
        null, // username
        null, // password
        null // onErrorCallback
    );
    instance.websocket = websocket;*/

    // NEW API
    var socketOpts = {
        unpackDataAsJSON: true,
        onSocketCreate: function (websocket) {
            // Register socket
            instance.websocket = websocket;
        },
        onSocketOpen: function (websocket, event) {
            // Set alive timer
            if (instance.websocketAliveTimer != null) clearInterval(instance.websocketAliveTimer);
            instance.websocketAliveTimer = setInterval(function () {
                // Sanity
                if (instance.websocket == null) return;
                if (instance.websocket.readyState != 1) return;
                var response = {
                    type: "connection-alive"
                };
                instance.websocket.send(JSON.stringify(response));
            }, 1000);
        },
        onSocketError: function (websocket, event, code) {
            Machinata.RemoteTouchControl.Controller.updateStatusUI(instance, Machinata.RemoteTouchControl.Controller.STATUS_ERROR);

        },
        onSocketMessage: function (websocket, event, data, message) {
            // SCREEN logic:
            if (message != null) {
                Machinata.debug("Machinata.RemoteTouchControl.Screen", message);
                if (message.type == "connect-request") {
                    // Signals a someone wants to connect - all we need to do is responde
                    var response = {
                        type: "connect-ack"
                    };
                    instance.websocket.send(JSON.stringify(response));
                } else if (message.type == "joystick") {
                    // Signals a joystick movement update
                    Machinata.RemoteTouchControl.Screen.registerUserMovement(instance);
                    instance.joystick = message.data;
                } else if (message.type == "click") {
                    // Signals a click event
                    instance.lastClick = new Date();
                    Machinata.RemoteTouchControl.Screen.registerUserMovement(instance);
                    Machinata.RemoteTouchControl.Screen.simulatCursorClick(instance);
                }
            }
        },
        onSocketClose: function (websocket, event, code) {

        }
    };
    instance.websocket = Machinata.WebSocket.init(instance.session.websocketEndpoint + instance.session.accessCode, socketOpts);

    

    return instance;
};

/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Screen.createQRCodeForAccessToken = function (instance) {
    return instance.session.remoteControlURL;
};

/// <summary>
///
/// </summary>
Machinata.RemoteTouchControl.Screen.createQRCodeImageForAccessToken = function (instance) {
    var qrcodeImageURL = "/static/qrcode?data=" + encodeURIComponent(Machinata.RemoteTouchControl.Screen.createQRCodeForAccessToken(instance));
    var qrcodeElem = $("<img/>")
        .attr("src", qrcodeImageURL)
        .attr("class", "qrcode");
    return qrcodeElem;
};






