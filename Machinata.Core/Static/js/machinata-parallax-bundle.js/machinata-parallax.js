


/* ======== Machinata Parallax ======== */
/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (!Machinata) Machinata = {};

/// <summary>
/// Machinata Wizard JS Library
/// </summary>
/// <type>namespace</type>
Machinata.Parallax = {};



/// <summary>
/// Creates a generic parallax at the given selector with the given layers and options.
/// </summary>
Machinata.Parallax.createLayeredParallax = function (elem, layers, options) {
    // Init
    if (options == null) options = {};
    if (options.triggerOnElement == null) options.triggerOnElement = true;
    if (options.triggerPaddingTop == null) options.triggerPaddingTop = 0;
    if (options.triggerPaddingBottom == null) options.triggerPaddingBottom = 0;
    if (options.mirrorProgress == null) options.mirrorProgress = false;
    if (options.transitionModel == null) options.transitionModel = "constant";
    if (options.selector == null) options.selector = "#" + Machinata.UI.autoUIDElem(elem);

    if (options.debug == true) console.log("Machinata.Parallax.initLayeredParallax()", options);

    var isFirstUpdate = true;
    var currentP = 0;

    // Classes
    elem.addClass("ma-parallax");
    if (options.classes != null) elem.addClass(options.classes);

    // Init layers
    for (var i = 0; i < layers.length; i++) {
        var layer = layers[i];
        layer.elem = $("<div class='ma-parallax-layer'><div class='ma-parallax-layer-bg'></div></div>");
        layer.elem.addClass("layer-" + i);
        layer.elemBG = layer.elem.find(".ma-parallax-layer-bg");
        elem.append(layer.elem);
    }
    
    // Init controller
    var controller = new ScrollMagic.Controller({ addIndicators: options.debug });

    // Create a scene
    var scene = null;
    var triggerPaddingTop = options.triggerPaddingTop * elem.height();
    var triggerPaddingBottom = options.triggerPaddingBottom * elem.height();
    if (options.triggerOnElement == true) {
        scene = new ScrollMagic.Scene({
            triggerElement: options.selector,
            duration: elem.height() + triggerPaddingTop + triggerPaddingBottom,
            offset: -triggerPaddingTop,
        }).addTo(controller);
    } else {
        scene = new ScrollMagic.Scene({
            duration: "100%",
            offset: 0,
        }).addTo(controller);
    }

    // Dimensions
    var windowWidth = null;
    var windowHeight = null;
    var elemWidth = null;
    var elemHeight = null;
    function cacheDimensions(allowAnimations) {
        windowWidth = $(window).width();
        if (windowHeight == null) windowHeight = $(window).height(); // only set once, since on mobile this changes based on tools/navbar
        elemWidth = elem.width();
        elemHeight = elem.height();
        // Setup the layer settings for the current window size
        for (var i = 0; i < layers.length; i++) {
            var layer = layers[i];
            // Merge layouts into layer props (overwrite)
            for (var ii = 0; ii < layer.layouts.length; ii++) {
                var layout = layer.layouts[ii];
                if (layout.maxWidth == null || elemWidth <= layout.maxWidth) {
                    var keys = Object.keys(layout);
                    for (var iii = 0; iii < keys.length; iii++) {
                        layer[keys[iii]] = layout[keys[iii]];
                    }
                }
            }
            layer.bgImageAR = layer.bgImageWidth / layer.bgImageHeight;
            if (layer.y1PositionOffset == null) layer.y1PositionOffset = 0;
            if (layer.y2PositionOffset == null) layer.y2PositionOffset = 0;
            if (layer.y1PositionVCenter == null) layer.y1PositionVCenter = 0;
            layer.elemBG.css("background-image", "url(" + layer.bgImage + ")");
            if (isFirstUpdate == false) layer.elemBG.css("transition-duration", layer.transitionSpeed + "s");
            if (options.debug == true) console.log(layer);
        }

        if (isFirstUpdate == true) {
            isFirstUpdate = false;

            // Enable transitions, defered by 10ms to ensure the first draw is without transitions
            setTimeout(function () {
                for (var i = 0; i < layers.length; i++) {
                    var layer = layers[i];
                    layer.elemBG.css("transition-duration", layer.transitionSpeed + "s");
                }
            }, 10);
        }
    }

    // Update
    function update(p) {
        if (options.mirrorProgress == true) {
            //p = Machinata.Parallax.map(p, 0, 1, 0, 2);
            //if (p > 1) p = 2.0 - p;
            var bufferZone = 0.2;
            var p1 = 0.0;
            var p2 = 0.4;//buffer
            var p3 = 0.6;//buffer
            var p4 = 1.0;
            if (p >= p1 && p < p2) {
                p = Machinata.Math.map(p, p1, p2, 0, 1);
            } else if (p >= p2 && p < p3) {
                p = 1.0;
            } else if (p >= p3 && p <= p4) {
                p = Machinata.Math.map(p, p3, p4, 1, 0);
            }
        }
        if (options.debug == true) console.log(p);
        // Update each layer
        for (var i = 0; i < layers.length; i++) {
            var layer = layers[i];

            var s1Width = layer.s1Width;
            var s2Width = layer.s2Width;
            var sWidth = Machinata.Math.map(p, 0, 1, s1Width, s2Width);

            var bgImageWidthCurrent = (elemWidth * sWidth);
            var bgImageHeightCurrent = bgImageWidthCurrent / layer.bgImageAR;

            var bgPosX = null;
            var bgPosY = null;
            if (layer.fixedBGPosition == null) {
                if (options.transitionModel == "constant") {
                    var y1 = ((elemHeight - bgImageHeightCurrent) * layer.y1Position);
                    var y2 = ((elemHeight - bgImageHeightCurrent) * layer.y2Position);
                    var y = Machinata.Math.map(p, 0, 1, y1, y2);
                    bgPosX = "center";
                    bgPosY = y;
                } else {
                    var y1 = windowHeight * layer.y1Position + (bgImageHeightCurrent * layer.y1PositionOffset) + ((windowHeight - bgImageHeightCurrent) * layer.y1PositionVCenter);
                    var y2 = (elemHeight * layer.y2Position) - bgImageHeightCurrent + (bgImageHeightCurrent * layer.y2PositionOffset);
                    var y = Machinata.Math.map(p, 0, 1, y1, y2);
                    bgPosX = "center";
                    bgPosY = y;
                }
            } else {
                bgPosX = layer.fixedBGPosition.split(' ')[0];
                bgPosY = layer.fixedBGPosition.split(' ')[1];
            }

            // Old method: use background positioning
            //if (bgPosX == "center") {
            //    bgPosX = Math.round(elemWidth/2 - (bgImageWidthCurrent/2)) + "px"; // fix for jittery bg animation in css stack (doesnt work)
            //}
            //layer.elem.css("background-position", bgPosX + " " + bgPosY);
            //layer.elem.css("background-size", (sWidth * 100) + "% auto");

            // New method: use transform
            if (bgPosX == "center") bgPosX = 0; // fix for center
            var posRelX = (elemWidth / 2) - (bgImageWidthCurrent / 2) + (bgPosX);
            var posRelY = (bgPosY / 1);
            var widthCSS = bgImageWidthCurrent + "px";
            var heightCSS = bgImageHeightCurrent + "px";
            var transformTranslateCSS = "translate(" + posRelX + "px" + ", " + posRelY + "px" + ")";
            var transformTranslateZCSS = "translateZ(0)";
            var transformScaleCSS = "";
            var transformCSS = transformTranslateCSS + " " + transformTranslateZCSS + " " + transformScaleCSS;
            layer.elemBG.css("width", widthCSS);
            layer.elemBG.css("height", heightCSS);
            layer.elemBG.css("transform", transformCSS);
            //console.log("widthCSS:" + widthCSS, "heightCSS:" + heightCSS, "transformCSS:" + transformCSS);

            if (layer.debug == true) console.log(layer.bgImage, "p:" + p, "y1:" + y1, "y2:" + y2, "y:" + y, "size:" + bgImageWidthCurrent + "x" + bgImageHeightCurrent, "windowHeight:" + windowHeight);

        }

    }

    // Initial update
    cacheDimensions(false);
    isFirstUpdate = false;
    update(currentP);
    isFirstUpdate = true;

    // Bind events
    scene.on("progress", function (event) {
        //console.log("progress", event.progress);
        var p = event.progress;
        currentP = p;
        //update(currentP);
    });
    $(window).resize(function () {
        cacheDimensions(true);
        update(currentP);
    });

    // Start animation loop at 60hz target
    function animateCallback() {
        update(currentP);
        requestAnimationFrame(animateCallback);
    }
    animateCallback();

};
