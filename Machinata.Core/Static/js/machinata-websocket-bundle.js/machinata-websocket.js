/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (typeof Machinata === "undefined") var Machinata = ((typeof global !== 'undefined') ? global : window).MACHINATA;


/// <summary>
/// Machinata WebSocket JS Library
/// </summary>
/// <type>namespace</type>
Machinata.WebSocket = {};



/// <summary>
/// Creates and connects to a websocket and offers some additional functionality,
/// such as automatic reconnecting.
/// Options:
///  - automaticReconnectEnabled: bool
///  - automaticReconnectTimeMS: bool
///  - unpackDataAsJSON: bool, it true will automatically to unpack all data as JSON and provide it on the onSocketMessage callback
///  - username: string (optional)
///  - password: string (optional)
///  - onSocketCreate: function callback function(websocket) (optional)
///  - onSocketOpen: function callback function(websocket, event) (optional)
///  - onSocketMessage: function callback function(websocket, event, data, json) (optional)
///  - onSocketClose: function callback function(websocket, event, code) (optional)
///  - onSocketError: function callback function(websocket, event, code) (optional)
/// </summary>
Machinata.WebSocket.init = function (url, opts) {
    Machinata.debug("Machinata.WebSocket.init");
    // Init
    if (opts == null) opts = {};
    if (opts.automaticReconnectEnabled == null) opts.automaticReconnectEnabled = true;
    if (opts.automaticReconnectTimeMS == null) opts.automaticReconnectTimeMS = 4000;
    if (opts.unpackDataAsJSON == null) opts.unpackDataAsJSON = false;

    // Create websocket
    // See https://developer.mozilla.org/en-US/docs/Web/API/WebSocket
    var websocket = null;
    try {
        websocket = Machinata.WebSocket.create(url, opts.username, opts.password);
        // Callback
        if (opts.onSocketCreate) opts.onSocketCreate(websocket);
    } catch (exception) {
        console.warn("Machinata.WebSocket: could not create websocket: ", exception);
        throw exception;
    }

    // Bind open
    websocket.onopen = function (event) {
        // Callback
        if (opts.onSocketOpen) opts.onSocketOpen(websocket, event);
    };

    websocket.onerror = function (event) {
        console.warn("Machinata.WebSocket error:", event);

        // Callback
        if (opts.onSocketError) opts.onSocketError(websocket, event, event.code);
        else throw "Machinata.WebSocket error:" + event;
    };

    websocket.onmessage = function (event) {

        Machinata.debug("Machinata.WebSocket: received data:", event.data);

        var json = null;
        if (opts.unpackDataAsJSON == true && event != null && event.data != null) {
            try {
                json = JSON.parse(event.data);
            } catch (e) {
                // Ignore?
                //TODO: should we call the error method?
                console.warn("Machinata.WebSocket: could not unpack data as JSON!",e);
            }
        }

        // Callback
        if (opts.onSocketMessage) opts.onSocketMessage(websocket, event, event.data, json);
    }

    websocket.onclose = function (event) {
        console.warn("Machinata.WebSocket: websocket connection lost", event);

        // Callback
        if (opts.onSocketClose) opts.onSocketClose(websocket, event, event.code);

        // Retry connection
        if (opts.automaticReconnectEnabled == true) {
            setTimeout(function () {
                websocket.close();
                console.warn("Machinata.WebSocket: retrying to connect to websocket");
                var newWebsocket = Machinata.WebSocket.init(url, opts);
                // Callback
                if (opts.onSocketCreate) opts.onSocketCreate(newWebsocket);
            }, opts.automaticReconnectTimeMS);
        }

    };

    // Return
    return websocket;
    
};


/// <summary>
/// Creates a basic JS WebSocket
/// </summary>
Machinata.WebSocket.create = function (url, username, password) {
    // Create the connections
    var connectionString = "";
    if (username != null && username != "" && password != null && password != "") {
        connectionString += username + ":" + password + "@" + url;
    } else {
        connectionString += url;
    }
    // Create and return
    var websocket = new WebSocket(connectionString);
    return websocket;

};
