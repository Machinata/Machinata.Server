
/// <summary>
/// </summary>
/// <namespace></namespace>
/// <name>Machinata.ModelObject</name>
/// <type>namespace</type>

namespace Machinata.Core { 

    export class ModelObject {

        public Type: string;

        public PublicId: string;
        public Created: Date;

        constructor() {
            this.Type = "ModelObject";
        }

        fromJSON(json: any): void {
            if (json["PublicId"] != null) { this.PublicId = json["PublicId"]; }
            else { this.PublicId = null; }
            if (json["Created"] != null) { this.Created = new Date(json["Created"]); }
            else { this.Created = null; }
        }

        toJSON(): any {
            let json = {};
            json["Type"] = this.Type;
            if (this.PublicId != null) { json["PublicId"] = this.PublicId; }
            else json["PublicId"] = null;
            if (this.Created != null) { json["Created"] = this.Created.getTime(); }
            else json["Created"] = null;
            return json;
        }
        
    }

}


namespace Machinata.Core.Model {


    export class ContentFileAsset {

        public SourceURL: string;
        public AssetURL: string;
        public Hash: string;

        constructor(sourceURL: string, assetURL: string, hash: string) {
            this.SourceURL = sourceURL;
            this.AssetURL = assetURL;
            this.Hash = hash;
        }

        fromJSON(json: any): void {
            if (json["SourceURL"] != null) this.SourceURL = json["SourceURL"];
            if (json["AssetURL"] != null) this.AssetURL = json["AssetURL"];
            if (json["Hash"] != null) this.Hash = json["Hash"];
        }

        toJSON(): any {
            var json = {};
            json["SourceURL"] = this.SourceURL;
            json["AssetURL"] = this.AssetURL;
            json["Hash"] = this.Hash;
            return json;
        }
    }

    export class Price {

        public Currency: string;
        public Value: number;

        constructor(value: number, currency: string) {
            this.Value = value;
            this.Currency = currency;
        }

        toString(): string {
            let currencyString = this.Currency;
            let valueString = this.Value != null ? this.Value.toFixed(2) : null;
            let ret = "";
            if ((valueString == null || valueString == "") && (currencyString == null || currencyString == "")) {
                ret = "";
            } else if (currencyString == null || currencyString == "") {
                ret = `${valueString}`;
            } else if (valueString == null || valueString == "") {
                ret = "";
            } else {
                ret = `${currencyString} ${valueString}`;
            }
            if (this.Currency == "CHF") {
                ret = ret.replace("0.00", "0.�");
                ret = ret.replace(".00", ".�");
            }
            return ret;
        }

        fromString(currencyAndValue: string): Price {
            let self = this;

            // Helper methods
            function segIsDecimal(seg: string): boolean {
                return !isNaN(parseFloat(seg));
            }
            function segIsInt64(seg: string): boolean {
                return !isNaN(parseFloat(seg));
            }

            // Special handling if null string
            if (currencyAndValue == null || currencyAndValue == "") {
                this.Value = null;
                this.Currency = null;
            } else {
                // We have something, trim it
                currencyAndValue = currencyAndValue.trim();

                try {
                    var segs = currencyAndValue.split(' ');
                    if (segs.length > 2) throw ("price has too many parts to it.");
                    let didSetValue = false;
                    let didSetCurrency = false;
                    for (let segRaw of segs) {
                        // Create formatted seg...
                        let seg = segRaw;
                        // Change thousands seperator
                        //TODO: @dan finish porting C# code...
                        //ORIG C#: if (!string.IsNullOrEmpty(Core.Config.CurrencyThousandsSeparatorChar)) seg = seg.Replace(Core.Config.CurrencyThousandsSeparatorChar, ",");
                        seg = MachinataJS.String.replaceAll(seg, "'", ",");
                        // Change .- to .00
                        //ORIG C#: if (seg.EndsWith(".-")) seg = seg.ReplaceSuffix(".-", ".00");
                        seg = MachinataJS.String.replaceAll(seg, ".-", ".00");
                        seg = MachinataJS.String.replaceAll(seg, ".�", ".00");
                        //ORIG C#: if (!string.IsNullOrEmpty(Core.Config.CurrencyZeroCentsChar) && seg.EndsWith("." + Core.Config.CurrencyZeroCentsChar)) seg = seg.ReplaceSuffix("." + Core.Config.CurrencyZeroCentsChar, ".00");
                        // Decimal?

                        if (segIsDecimal(seg)) {
                            if (didSetValue) throw "two values where found.";
                            this.Value = parseFloat(seg);
                            didSetValue = true;
                        } else if (segIsInt64(seg)) {
                            if (didSetValue) throw "two values where found.";
                            this.Value = parseFloat(seg);
                            didSetValue = true;
                        } else {
                            // Must be currency
                            if (didSetCurrency) throw "two currencies where found.";
                            this.Currency = seg;
                            didSetCurrency = true;
                        }
                    }
                    if (!didSetValue) throw "no valid value was given.";
                    if (!didSetCurrency) this.Currency = null;
                } catch (e) {
                    throw "Could not parse the price '" + currencyAndValue + "': " + e;
                }
            }

            return this;
        }

        fromJSON(json: any): void {
            if (json["Currency"] != null) this.Currency = json["Currency"];
            if (json["Value"] != null) this.Currency = json["Value"];
        }

        toJSON(): any {
            var json = {};
            json["Value"] = this.Value;
            json["Currency"] = this.Currency;
            return json;
        }
    }

    export class TranslatableText {
        public Data: any;
        constructor(data?: any) {
            if (data == null) data = {};
            this.Data = data;
        }

        fromJSON(json: any): void {
            this.Data = json;
        }

        toJSON(): any {
            return this.Data;
        }

        toString(): string {
            return this.get();
        }

        getForLanguage(lang: string): string {
            //TODO: @dan test
            let ret = this.Data[lang];
            if (ret != null) return ret;
            return this.getDefault();
        }

        getDefault(): string {
            //TODO: @dan test
            // First try explicit default value
            if (this.Data["default"] != null) {
                return this.Data["default"];
            } else {
                // Return any value
                if (Object.keys(this.Data).length > 0) {
                    return this.Data[Object.keys(this.Data)[0]];
                } else {
                    // No value, return null
                    return null;
                }
            }
        }

        setDefault(text: string): TranslatableText {
            //TODO: @dan test
            this.Data["default"] = text;
            return this;
        }

        get(): string {
            //TODO: @dan test
            return this.getForLanguage("{language}"); 
        }

        set(text: string): TranslatableText {
            //TODO: @dan test
            this.setForLanguage(text, "{language}"); 
            return this;
        }

        setForLanguage(text: string, language: string): TranslatableText {
            //TODO: @dan test
            if (language == null) this.setDefault(text);
            else this.Data[language] = text;
            return this;
        }
    }

    export class Properties {

        public Data: string;

        constructor(data: string) {
            this.Data = data;
        }

        set(key: string, val: any) {
            let json = this._deserialize();
            json[key] = val;
            this._serialize(json);
        }

        get(key: string) {
            return this._deserialize()[key];
        }

        _serialize(json: any): any {
            if (json == null) this.Data = null;
            this.Data = JSON.stringify(json);
        }

        _deserialize(): any {
            if (this.Data == null) return {};
            return JSON.parse(this.Data);
        }

        fromJSON(json: any): void {
            this.Data = json;
        }

        toJSON(): any {
            return this.Data;
        }
    }
}
