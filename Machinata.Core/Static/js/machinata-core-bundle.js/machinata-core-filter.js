

/// <summary>
/// Library for a generic filter on a set of elements.
/// </summary>
/// <type>namespace</type>
Machinata.Filter = {};


/// <summary>
/// 
/// </summary>
Machinata.Filter.elements = null;


/// <summary>
/// 
/// </summary>
/// <hidden/>
Machinata.onInit(function () {
    
    $(".bb-data-filter").each(function () {
        // Init
        var elem = $(this);
        // Set settings
        elem.data("filter-url", elem.attr("data-filter-url"));
        elem.data("filter-results-selector", elem.attr("data-filter-results-selector"));
        elem.data("filter-item-selector", elem.attr("data-filter-item-selector"));
        if (elem.data("filter-url") == null) {
            var url = window.location.href;
            Machinata.debug("Machinata.Filter: autodetect URL: " + url);
            elem.data("filter-url", url);
        }
        if (elem.data("filter-item-selector") == null) {
            elem.data("filter-item-selector", ".bb-data-filter-results-item");
        }
        if (elem.data("filter-results-selector") == null) {
            elem.data("filter-results-selector", ".bb-data-filter-results");
        }
        // Bind interactions
        elem.find(".bb-data-filter-toggle").click(function () {
            if ($(this).hasClass("selected")) {
                $(this).removeClass("selected")
            } else {
                $(this).addClass("selected")
            }
            Machinata.Filter.update(elem);
        });
        var inputTimer = null;
        elem.find(".bb-data-filter-input").on("keyup", function () {
            if (inputTimer != null) clearTimeout(inputTimer);
            inputTimer = setTimeout(function () {
                Machinata.Filter.update(elem);
            }, 600);
            
        });
        // Initial state
        elem.find(".bb-data-filter-show-if-loading").hide();
        elem.find(".bb-data-filter-show-if-success").hide();
        elem.find(".bb-data-filter-show-if-empty").hide();
        elem.find(".bb-data-filter-show-if-error").hide();
        // Register element
        if (Machinata.Filter.elements == null) Machinata.Filter.elements = [];
        Machinata.Filter.elements.push(elem);
    });
});


/// <summary>
/// 
/// </summary>
Machinata.Filter.countElements = function (elem) {
    if (elem.data("filter-item-selector") != null) {
        return elem.find(elem.data("filter-item-selector")).length;
    } else {
        return elem.children().length;
    }
};

/// <summary>
/// 
/// </summary>
Machinata.Filter.update = function (elem) {
    // Validate
    if (elem.data("filter-loading") == true) return;
    // Show loading
    elem.addClass("loading");
    elem.data("filter-loading", true);
    elem.find(".bb-data-filter-show-if-loading").show();
    elem.find(".bb-data-filter-show-if-success").hide();
    elem.find(".bb-data-filter-show-if-empty").hide();
    elem.find(".bb-data-filter-show-if-error").hide();
    Machinata.showLoading(true);
    // Empty
    var resultsContainer = $(elem.data("filter-results-selector"));
    resultsContainer.empty();
    // Build URL
    var filterData = {};
    elem.find(".bb-data-filter-toggle.selected").each(function () {
        var key = $(this).attr("data-key");
        var val = $(this).attr("data-value");
        if (filterData[key] == null) {
            filterData[key] = val;
        } else {
            filterData[key] = filterData[key] + "," + val;
        }
    });
    elem.find(".bb-data-filter-input").each(function () {
        var key = $(this).attr("data-key");
        var val = $(this).val();
        if (val == null || val == "") return;
        if (filterData[key] == null) {
            filterData[key] = val;
        } else {
            filterData[key] = filterData[key] + "," + val;
        }
    });
    var loadString = elem.data("filter-url");
    var keys = Object.keys(filterData);
    for (var i = 0; i <keys.length; i++) {
        var key = keys[i];
        var val = filterData[key];
        loadString = Machinata.updateQueryString(key, val, loadString);
    }
    if (elem.data("filter-results-selector") != null) loadString += " " + elem.data("filter-results-selector");
    // Bookeeping
    var currentElems = Machinata.Filter.countElements(elem);
    Machinata.debug("Machinata.Filter: currentElems: " + currentElems);
    // Make request
    Machinata.debug("Machinata.Filter: load URL: " + loadString);
    var newElemsContainer = $("<div></div>");
    newElemsContainer.load(loadString, null, function (responseText, textStatus, jqXHR) {
        Machinata.showLoading(false);
        elem.removeClass("loading");
        // Error?
        if (textStatus == "error") {
            Machinata.debug("Machinata.Filter: error with URL: " + loadString);
            elem.data("filter-loading", false);
            elem.find(".bb-data-filter-show-if-loading").hide();
            elem.find(".bb-data-filter-show-if-error").show();
        } else {
            elem.data("filter-loading", false);
            elem.find(".bb-data-filter-show-if-loading").hide();
            elem.find(".bb-data-filter-show-if-success").show();
            newElemsContainer.children().appendTo(resultsContainer).addClass("new-item");
        }
        // Did we add elements?
        var newElems = Machinata.Filter.countElements(elem);
        Machinata.debug("Machinata.Filter: newElems: " + newElems);
        if (newElems <= 0) {
            Machinata.debug("Machinata.Filter: no elements added");
            elem.addClass("empty");
            elem.find(".bb-data-filter-show-if-empty").show();
            elem.find(".bb-data-filter-show-if-success").hide();
        } else {
            Machinata.UI.bind(resultsContainer);
        }
    });
};





/// <summary>
/// </summary>
/// <type>namespace</type>
Machinata.Filter.Tables = {};

/// <summary>
/// </summary>
Machinata.Filter.Tables.makeTableFilterable = function (tableElem, opts) {
    // Init
    if (opts == null) opts = {};
    if (opts.keyDelay == null) opts.keyDelay = 300;
    if (opts.noResultsText == null) opts.noResultsText = "No results.";
    if (opts.filters == null) opts.filters = [];
    if (opts.didFilterWithResults == null) opts.didFilterWithResults = null;
    if (opts.didFilterWithNoResults == null) opts.didFilterWithNoResults = null;
    if (opts.buildNoResultsUI == null) opts.buildNoResultsUI = null;
    if (opts.buildFilterInputUI == null) opts.buildFilterInputUI = null;
    if (opts.buildNamedFilterUI == null) opts.buildNamedFilterUI = null;

    // Set initial filters
    Machinata.Filter.Tables.convertNamelessFiltersToExpressions(opts.filters);
    tableElem.data("filters", opts.filters);

    // Helper functions
    function getFilters() {
        var ret = tableElem.data("filters");
        if (ret == null) {
            ret = {};
            tableElem.data("filters", ret);
        }
        return ret;
    }
    function getNamedFiltersForColumn(columnId) {
        var filters = getFilters();
        var matches = [];
        for (var i = 0; i < filters.length; i++) {
            if (filters[i].columnId == columnId && filters[i].name != null) {
                matches.push(filters[i])
            }
        }
        return matches;
    }
    function getUITextFilterForColumn(columnId) {
        var filters = getFilters();
        var matches = [];
        for (var i = 0; i < filters.length; i++) {
            if (filters[i].columnId == columnId && filters[i].isUIText == true) {
                return filters[i];
            }
            if (filters[i].columnId == columnId) {
                matches.push(filters[i]);
            }
        }
        // Could any of the column matches be a ui text filter?
        for (var i = 0; i < matches.length; i++) {
            if (matches[i].expression != null && matches[i].name == null) {
                matches[i].isUIText = true; // convert into this
                matches[i].didConvertFromOriginalFilters = true;
                return matches[i];
            }
        }
        // No match, create one...
        var newFilter = {
            columnId: columnId,
            expression: "",
            isUIText: true,
            didAutoCreate: true
        };
        filters.push(newFilter);
        return newFilter;
    }
    function setFilters(filters) {
        Machinata.Filter.Tables.convertNamelessFiltersToExpressions(filters);
        tableElem.data("filters", filters);
        updateFiltersUIForCurrentFilters();
    }
    function updateFiltersUIForCurrentFilters() {
        tableElem.find("thead .filter-input").each(function () {
            // Init
            var textInputElem = $(this);
            // Update ui
            updateFilterInputUI(textInputElem);
        });
    }
    function log(msg, arg1, arg2) {
        return; // disabled for production
        if (arg2 != null) {
            console.log("Machinata.Filter.Tables.makeTableFilterable: " + msg, arg1, arg2);
        } else if (arg1 != null) {
            console.log("Machinata.Filter.Tables.makeTableFilterable: " + msg, arg1);
        } else {
            console.log("Machinata.Filter.Tables.makeTableFilterable: " + msg);
        }
    }
    function updateAllFilterInputUIs(updateText) {
        tableElem.find("thead .filter-input.option-text").each(function () {
            var textInputElem = $(this);
            updateFilterInputUI(textInputElem, updateText);
        });
    }
    function updateFilterInputUI(inputElem, updateText, updateNamedFilters) {
        // Update the text
        var columnId = getFilterInputColumnId(inputElem);
        var currentInputText = getFilterInputFilterText(inputElem);
        if (currentInputText != null && currentInputText != "") inputElem.addClass("option-has-text");
        else inputElem.removeClass("option-has-text");
        if (updateText != false) {
            var filter = getUITextFilterForColumn(columnId);
            if (currentInputText != filter.expression) setFilterInputFilterText(inputElem,filter.expression);
        }
        if (updateNamedFilters != false) {
            // Clear all named filters
            inputElem.find(".named-filters").empty();
            // Now re-add them
            var namedFilters = getNamedFiltersForColumn(columnId);
            for (var i = 0; i < namedFilters.length; i++) {
                var filter = namedFilters[i];
                // Build UI
                var namedFilterElem = $("<span></span>");
                namedFilterElem.text(filter.name);
                namedFilterElem.addClass("named-filter");
                namedFilterElem.attr("title",filter.expression);
                namedFilterElem.attr("data-id", filter.id);
                //console.log(filter);
                if (opts.buildNamedFilterUI) opts.buildNamedFilterUI(tableElem, opts, namedFilterElem, filter);
                // Actions
                namedFilterElem.find(".action-remove-filter").click(function () {
                    $(this).closest(".named-filter").remove();
                    onFilterInputChange(inputElem);
                });
                // Does the container exist? If not create it...
                if (inputElem.find(".named-filters").length == 0) inputElem.prepend($("<span class='named-filters' contenteditable='false'></span>"));
                // Add the named filter ui...
                inputElem.find(".named-filters").prepend(namedFilterElem);
            }
            if (namedFilters.length > 0) inputElem.addClass("option-has-named-filter");
            else inputElem.removeClass("option-has-named-filter");
        }
        // Make parent grow by own height
        inputElem.parent().height(inputElem.outerHeight());
    }
    function onFilterInputChange(inputElem) {
        // UI Update
        updateFilterInputUI(inputElem, false, false);
        // Get filter text from this input
        var columnId = getFilterInputColumnId(inputElem);
        var filterText = getFilterInputFilterText(inputElem);
        //log("filterText for " + columnId + ": " + filterText);
        // Update the text filter
        var filter = getUITextFilterForColumn(columnId);
        filter.expression = filterText;
        // Update named filters
        // We basically need to validate if it needs to be removed
        var namedFiltersToRemove = [];
        var namedFilters = getNamedFiltersForColumn(columnId);
        for (var i = 0; i < namedFilters.length; i++) {
            // Does it exist?
            var namedFilterUIElem = inputElem.find("[data-id='" + namedFilters[i].id + "']");
            if (namedFilterUIElem.length == 0) namedFiltersToRemove.push(namedFilters[i]);
        }
        if (namedFiltersToRemove.length > 0) {
            for (var i = 0; i < namedFiltersToRemove.length; i++) {
                var filter = namedFiltersToRemove[i];
                var filterIndex = getFilters().indexOf(filter);
                if (filterIndex != -1) getFilters().splice(filterIndex, 1);
            }
        }
        // Do delayed trigger
        triggerDelayedFilterUpdate(inputElem);
    }
    function triggerDelayedFilterUpdate() {
        // Reset timer
        var changeTimer = tableElem.data("changeTimer");
        if (changeTimer != null) clearTimeout(changeTimer);
        // Create new timer
        changeTimer = setTimeout(function () {
            triggerFilterUpdate();
        }, opts.keyDelay);
        tableElem.data("changeTimer", changeTimer);
    }
    function triggerFilterUpdate() {
        updateTableForFilters();
    }
    function getFilterInputFilterText(inputElem) {
        // We need to loop all children and get all the text nodes, building our returned string
        var ret = null;
        for (var i = 0; i < inputElem[0].childNodes.length; i++) {
            var node = inputElem[0].childNodes[i];
            if (node.nodeType == 3) {
                if (ret == null) ret = node.nodeValue;
                else ret += " " + node.nodeValue; //TODO: should this be a expression?
            }
        }
        if (ret == "") ret = null;
        return ret;
    }
    function setFilterInputFilterText(inputElem, text) {
        // We need to loop all children and get all the text nodes
        // assigning the text to the first and removing the rest (that the user may have created)
        var didSetText = false;
        for (var i = 0; i < inputElem[0].childNodes.length; i++) {
            var node = inputElem[0].childNodes[i];
            if (node.nodeType == 3) {
                if (didSetText == false) {
                    // Set the first
                    node.nodeValue = text;
                    didSetText = true;
                } else {
                    // Remove the rest
                    node.remove();
                }
            }
        }
        // Did we never set the text because there weren't any?
        if (didSetText == false) {
            var newNode = document.createTextNode(text); 
            inputElem[0].appendChild(newNode);
        }
    }
    function getFilterInputColumnId(inputElem, columnNumber) {
        // Column number is zero-based
        return inputElem.attr("data-col-id") || inputElem.attr("data-column") || columnNumber+"";
    }
    function getColumnCellColumnId(cellElem, columnNumber) {
        // Column number is zero-based
        return cellElem.attr("data-col-id") || cellElem.attr("data-column") || columnNumber + "";
    }
    function getColumnCellFilterValue(cellElem) {
        // Values can be any of the following strings which should be returned as a number:
        // 20
        // 20.0
        // 21%
        // 21.9%
        var text = cellElem.attr("data-text") || cellElem.text();
        return Machinata.Parser.normalizeStringInputToBasicType(text);
    }

    function resetFilters() {
        setFilters([]);
    }
    function resetFiltersAndUpdate() {
        resetFilters();
        updateTableForFilters();
    }
    function updateTableForFilters() {
        // First thing - sync the ui
        updateAllFilterInputUIs();

        // Init
        var rows = tableElem.find("tbody tr");
        var totalRows = 0;
        var rowsShown = 0;
        var rowsHidden = 0;
        var maxColumns = tableElem.find("thead").first().find("tr").first().find("th").length;
        var filters = getFilters();
        if (filters == null) filters = []; // sanity
        var noResultsRow = null;

        // Pre-build the full filter query
        var filterExpressionForColumn = {};
        function convertFilterToExpression(filter) {
            // Get the expression by converting all others
            var ret = null;
            if (filter.expression != null) ret = filter.expression;
            else if (filter.value != null) ret = Machinata.Parser.escapeString(filter.value);
            else if (filter.isAny != null) ret = Machinata.Parser.convertIsAnyArrayToTextFilter(filter.isAny);
            else if (filter.notAny != null) ret = Machinata.Parser.convertNotAnyArrayToTextFilter(filter.notAny);
            else console.warn("Machinata.Filter.Tables: could not convert filter to expression: ", filter,ret);
            // Cleanup
            if (ret != null) ret = ret.trim();
            if (ret == "") ret = null;
            return ret;
        }
        for (var i = 0; i < filters.length; i++) {
            var filter = filters[i];
            var filterExpression = convertFilterToExpression(filter);
            if (filterExpression != null) {
                if (filterExpressionForColumn[filter.columnId] == null) {
                    // New, just set the expression
                    filterExpressionForColumn[filter.columnId] = filterExpression;
                } else {
                    // Already exists, we need to append
                    //TODO
                    filterExpressionForColumn[filter.columnId] += " AND " + filterExpression;
                }
            }
        }
        log("updateTableForFilters: ", filters, filterExpressionForColumn);

        // Loop each row, figuring out if we need to show or hide rows based on the filters
        rows.each(function (rowIndex) {
            // Init
            var rowElem = $(this);
            var showRow = true; // by default true

            // Special handling no results row
            if (rowElem.hasClass("filter-no-results")) {
                noResultsRow = rowElem;
                return true;
            }

            // Go through each column of the row
            rowElem.find("td").each(function (columnIndex, item) {
                var columnElem = $(this);
                var columnId = getColumnCellColumnId(columnElem, columnIndex);
                // Do we have a filter for this?
                var columnFilter = filterExpressionForColumn[columnId];
                if (columnFilter != null) {
                    // Get the normalized value
                    var normalizedColumnValue = getColumnCellFilterValue(columnElem);
                    // Test
                    var filterResult = Machinata.Parser.parseTextFilter(columnFilter, normalizedColumnValue);
                    if (filterResult == false) {
                        log("cell filter: " + columnId + ": ", normalizedColumnValue + " vs " + columnFilter, filterResult);
                        showRow = false;
                        return false; // abort each
                    }
                }
            });

            // Update the row as needed
            if (showRow) {
                rowElem.show();
                rowsShown++;
            } else {
                rowElem.hide();
                rowsHidden++;
            }
            totalRows++;
        });


        // Update no results row
        if (rowsShown == 0) {
            // No results - show noResultsRow
            if (noResultsRow == null) {
                // Not yet created, create one
                noResultsRow = $("<tr class='filter-no-results'><td colspan='" + maxColumns + "'></td></tr>");
                if (opts.noResultsText != null) noResultsRow.find("td").text(opts.noResultsText);
                if (opts.buildNoResultsUI) opts.buildNoResultsUI(tableElem, opts, noResultsRow);
                // Bind events
                noResultsRow.find("a.reset-action").click(function () {
                    Machinata.Filter.Tables.resetFilters(tableElem);
                });
                // Add to table
                tableElem.find("tbody").last().append(noResultsRow);
            }
            noResultsRow.show();
            // Callback
            if (opts.didFilterWithNoResults) opts.didFilterWithNoResults(tableElem, opts, noResultsRow);
        } else {
            // We have results - hide noResultsRow
            if (noResultsRow != null) {
                noResultsRow.hide();
            }
            // Callback
            if (opts.didFilterWithResults) opts.didFilterWithResults(tableElem, opts, noResultsRow);
        }

        // Do a deffered update on the input UIs - we need the table to reflow the column widths first...
        setTimeout(function () {
            updateAllFilterInputUIs(false);
        }, 1);
    }

    function rebuildFiltersRow() {
        log("rebuildFiltersRow");
        // Create a row just for filtering...
        var headerColumnsRow = tableElem.find("thead .header.columns");
        var headerFiltersRow = tableElem.find("thead .header.filters");
        headerFiltersRow.remove();
        if (headerFiltersRow.length == 0) {
            // Re-create
            headerFiltersRow = headerColumnsRow.clone();
            headerFiltersRow.removeClass("columns");
            headerFiltersRow.removeClass("tablesorter-headerRow");
            headerFiltersRow.addClass("filters");
            headerFiltersRow.attr("role", "search");
            headerFiltersRow.find("th").text("");

            headerFiltersRow.find("th").each(function (index,item) {
                var filterHeaderColumnElem = $(this);
                var headerColumnElem = headerColumnsRow.find("[data-col-id='" + filterHeaderColumnElem.attr("data-col-id")+"']");
                filterHeaderColumnElem.removeClass("column");
                filterHeaderColumnElem.addClass("filter");
                // Add text input (as contenteditable)
                var inputWrapperElem = $("<div class='filter-input-wrapper'></div>").appendTo(filterHeaderColumnElem);
                var textInputElem = $("<div contenteditable='true'></div>");
                textInputElem.addClass("filter-input");
                textInputElem.addClass("option-text");
                textInputElem.attr("data-col-id", filterHeaderColumnElem.attr("data-col-id"));
                textInputElem.attr("data-column", filterHeaderColumnElem.attr("data-column") || index);
                textInputElem.attr("data-column-title", headerColumnElem.text());
                textInputElem.attr("data-placeholder", filterHeaderColumnElem.attr("data-filter-placeholder"));
                textInputElem.text("");
                textInputElem.on("input", function () {
                    onFilterInputChange($(this));
                });
                textInputElem.on("update-ui", function () {
                    updateFilterInputUI($(this));
                });
                // Build handler
                if (opts.buildFilterInputUI) opts.buildFilterInputUI(tableElem, opts, textInputElem, filterHeaderColumnElem);
                // Add to th
                inputWrapperElem.append(textInputElem);
            });

            // Append to table head (after the header columns)
            headerFiltersRow.insertAfter(headerColumnsRow);

            // Initial update
            headerFiltersRow.find(".filter-input").each(function () {
                updateFilterInputUI($(this));
            });
        }

    }

    // Bind events
    tableElem.on("reset-filters", function () {
        resetFiltersAndUpdate();
    });
    tableElem.on("update-for-filters", function () {
        //updateFiltersUIForCurrentFilters();
        updateTableForFilters();
    });

    // Initial setup
    log("-------------");
    log("opts filters", JSON.stringify(getFilters()));
    rebuildFiltersRow();
    updateTableForFilters();
    
};

/// <summary>
/// </summary>
Machinata.Filter.Tables.convertNamelessFiltersToExpressions = function (filters) {
    for (var i = 0; i < filters.length; i++) {
        var filter = filters[i];
        if (filter.name == null && filter.expression == null) {
            if (filter.value != null) filter.expression = Machinata.Parser.escapeString(filter.value); 
            else if (filter.isAny != null) filter.expression = Machinata.Parser.convertIsAnyArrayToTextFilter(filter.isAny);
            else if (filter.notAny != null) filter.expression = Machinata.Parser.convertNotAnyArrayToTextFilter(filter.notAny);
        }
        if (filter.id == null) filter.id = Machinata.guid();
    }
};

/// <summary>
/// </summary>
Machinata.Filter.Tables.resetFilters = function (tableElem) {
    tableElem.trigger("reset-filters");
};

/// <summary>
/// </summary>
Machinata.Filter.Tables.setFilters = function (tableElem, filters) {
    // Get a copy of filters
    var filters = Machinata.Util.cloneJSON(filters);
    Machinata.Filter.Tables.convertNamelessFiltersToExpressions(filters);
    tableElem.data("filters", filters);
    tableElem.trigger("update-for-filters");
};