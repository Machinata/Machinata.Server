
/// <summary>
/// Provides some cross-platform (browser vs NodeJS) functionality for altering the Machinata
/// core behaviour and depencies before the actual loading of the library.
/// </summary>
/// <namespace></namespace>
/// <type>namespace</type>
/// <name>global</name>

/// <summary>
/// Shortcut multi-platform handle for the namespace for globals.
/// </summary>
/// <hidden/>
var MACHINATA_GLOBAL = (typeof global !== 'undefined') ? global : window;


/// <summary>
/// If ```true```, allows debugging output and modes to be enabled.
/// </summary>
/// <namespace>global</namespace>
/// <type>variable</type>
if (MACHINATA_GLOBAL.MACHINATA_DEBUG_ENABLED == null) MACHINATA_GLOBAL.MACHINATA_DEBUG_ENABLED = false; 

/// <summary>
/// If ```true```, Machinata will auomatically noConflict() it's jQuery. This means that
/// if an version of jQuery already exists (on window.jQuery or window.$), the Machinata
/// included jQuery is "no conflicted".
/// </summary>
/// <namespace>global</namespace>
/// <type>variable</type>
MACHINATA_GLOBAL.MACHINATA_JQUERY_NO_CONFLICT_MODE = true;

/// <summary>
/// Defines the recommended minimum version of jQuery for Machinata.
/// </summary>
/// <namespace>global</namespace>
/// <type>variable</type>
MACHINATA_GLOBAL.MACHINATA_JQUERY_REQUIRED_VERSION = "3.7.1";

/// <summary>
/// If ```true```, Machinata has detected a already existing version of jQuery.
/// </summary>
/// <namespace>global</namespace>
/// <type>variable</type>
MACHINATA_GLOBAL.MACHINATA_JQUERY_ALREADY_EXISTS = false;

/// <summary>
/// If ```MACHINATA_JQUERY_ALREADY_EXISTS``` is ```true```, this stores
/// the version string of that detected version.
/// </summary>
/// <namespace>global</namespace>
/// <type>variable</type>
MACHINATA_GLOBAL.MACHINATA_JQUERY_EXISTING_VERSION = null;

/// <summary>
/// Stores a reference to the jQuery provided by Machianta.
/// </summary>
/// <namespace>global</namespace>
/// <type>variable</type>
/// <name>MACHINATA_JQUERY</name>
//MACHINATA_GLOBAL.MACHINATA_JQUERY = null;
