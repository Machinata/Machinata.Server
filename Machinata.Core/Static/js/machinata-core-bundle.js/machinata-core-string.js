

/// <summary>
/// Useful string-related functions.
/// Many of the functions align with the Machinata Server Core String functions.
/// </summary>
/// <type>namespace</type>
Machinata.String = {};

/// <summary>
/// 
/// </summary>
Machinata.String.createShortURL = function (text) {
    //TODO special characters

    // Lower case
    text = text.toLowerCase();
    // Remove double spaces
    text = text.replace(/\s\s+/g, ' ');
    // Remove spaces
    text = text.replace(/\s/g, '-');
    // Remove slashes
    text = text.replace(/\//g, '-');
    text = text.replace(/\\/g, '-');
    // Remove all special characters
    text = text.replace(/[^a-zA-Z0-9]/g, '-');

    return text.trim();
};

/// <summary>
/// 
/// </summary>
Machinata.String.createSummarizedText = function (text, length, includeTrail, breakAtWordIfPossible) {

    //Note: ported from Core.Util.String (CS)

    if (length == null) length = 60;
    if (includeTrail == null) includeTrail = true;
    if (breakAtWordIfPossible == null) breakAtWordIfPossible = true;

    // Remove special characters
    //TODO: PORT: 
    //if (!allowSpecialCharacters) {
    //    text = ReplaceUmlauts(text);
    //    text = RemoveDiacritics(text);
    //    text = System.Text.RegularExpressions.Regex.Replace(text, @"[^A-Za-z0-9 -]+", " ");
    //}
    // Remove double spaces
    text = text.replace(/\s\s+/g, ' ');
    // Trim to length, if longer
    if (text.length > length) {
        var trimLength = length;
        if(breakAtWordIfPossible == true) {
            // Try to find a length to cutoff at a word
            var tolerance = 12;
            for(var i = 0; i <= tolerance; i++) {
                var c1 = length - i;
                var c2 = length + i;
                if(c1 > 0 && c1 < text.length && text[c1] == ' ') {
                    trimLength = c1;
                    break;
                }
                if(c2 > 0 && c2 < text.length && text[c2] == ' ') {
                    trimLength = c2;
                    break;
                }
            }
        }
        // Cut strimg
        text = text.substring(0, trimLength);
        // Add trail
        if (includeTrail == true) {
            //TODO: PORT: text = text.trim('.');
            text += "...";
        }
    }
    return text.trim();
};



/// <summary>
/// 
/// </summary>
Machinata.String.isHTML = function (str) {
    var a = document.createElement('div');
    a.innerHTML = str;
    for (var c = a.childNodes, i = c.length; i--;) {
        if (c[i].nodeType == 1) return true;
    }
    return false;
};


/// <summary>
/// 
/// </summary>
Machinata.String.replaceAll = function (str, replace, withThis) {
    return str.split(replace).join(withThis);
};

/// <summary>
/// Returns the latlon coordinate from a location string.
/// Thus when ```8003 Z�rich (47.3744489,8.5410422)``` is input,
/// this method will return ```47.3744489,8.5410422```
///
/// A location string looks like:
/// -```47.3744489,8.5410422```
/// -```8003 Z�rich (47.3744489,8.5410422)```
/// -```8003 Z�rich```
/// 
/// See also Machinata.Core.String.ReadLatLonFromLocationString (Core Server)
/// </summary>
Machinata.String.readLatLonFromLocationString = function (location) {
    // Sanity
    if (location == null || location == "") return null;
    // Parse out coord pair
    var regex = /(\d*\.\d*,\d*\.\d*)/; // match any coord pair 47.3744489,8.5410422
    var latlons = location.match(regex);
    
    if (latlons != null && latlons.length > 0) {
        return latlons[latlons.length-1];
    } else {
        return null;
    }
};

/// <summary>
/// Reads the address portion from the a coordinate string.
/// Thus when ```8003 Z�rich (47.3744489,8.5410422)``` is input,
/// this method will return ```8003 Z�rich```
////
/// A location string looks like:
/// -```47.3744489,8.5410422```
/// -```8003 Z�rich (47.3744489,8.5410422)```
/// -```8003 Z�rich```
/// 
/// See also Machinata.Core.String.ReadAddressFromLocationString (Core Server)
/// </summary>
Machinata.String.readAddressFromLocationString = function (location) {
    // Sanity
    if (location == null || location == "") return null;
    // Get latlon
    var latlon = Machinata.String.readLatLonFromLocationString(location);
    if (latlon == null) {
        return location;
    } else {
        // Replace it
        var ret = location.replace("(" + latlon + ")", ""); // try with () first
        ret = ret.replace(latlon, ""); // try without
        ret = ret.trim();
        // Nothing left?
        if (ret == "") return location.trim();
        else return ret;
    }
};

/// <summary>
/// Updates the latlon portion of a location string. If the coordinate does not exist,
/// it is automatically added.
/// A location string looks like:
/// -```47.3744489,8.5410422```
/// -```8003 Z�rich (47.3744489,8.5410422)```
/// -```8003 Z�rich```
/// </summary>
Machinata.String.updateLatLonInLocationString = function (location,newLatLon) {
    var oldLatLon = Machinata.String.readLatLonFromLocationString(location);
    if (oldLatLon == null) {
        // Append it?
        return location + " (" + newLatLon + ")";
    } else {
        // Replace it
        var ret = location.replace("(" + oldLatLon + ")", "(" + newLatLon + ")"); // try with () first
        ret = ret.replace(oldLatLon, newLatLon); // try without
        return ret.trim();
    }
};



/// <summary>
/// Strips all leading indents from a multiline-string.
/// Useful for displaying source code that has built in indents.
/// </summary>
Machinata.String.stripExtraIndents = function (str) {
    // Init
    var ret = "";
    var lines = str.split("\n");
    // Count min indents per line
    var minIndents = 9999999;
    function countIndent(line) {
        var indents = 0;
        if (line == null || line.trim() == "") return 9999999;
        for (var c = 0; c < line.length - 1; c++) {
            if (line[c] == " ") {
                indents++;
            } else {
                break;
            }
        }
        return indents;
    }
    for (var i = 0; i < lines.length - 1; i++) {
        var indents = countIndent(lines[i]);
        if (indents < minIndents) minIndents = indents;
    }
    // Strip out each line
    for (var i = 0; i < lines.length - 1; i++) {
        ret += lines[i].substring(minIndents) + "\n";
        
    }
    return ret.trim();
};

/// <summary>
/// </summary>
Machinata.String.startsWith = function (str, prefix) {
    return str.slice(0, prefix.length) == prefix;
};

/// <summary>
/// </summary>
Machinata.String.endsWith = function (str, ending) {
    return str.slice(-ending.length) == ending;
};

/// <summary>
/// </summary>
Machinata.String.getOrdinalSuffixForNumber = function (number) {
    var j = number % 10,
        k = number % 100;
    if (j == 1 && k != 11) {
        return number + "st";
    }
    if (j == 2 && k != 12) {
        return number + "nd";
    }
    if (j == 3 && k != 13) {
        return number + "rd";
    }
    return number + "th";
};