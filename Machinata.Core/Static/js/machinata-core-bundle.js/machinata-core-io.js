

/// <summary>
/// IO-related utilities.
/// The implementation of this namespace varies from profile:
///  -On NodeJS this provides disk reading utilities from the fs package, and http/https utilities from the http/https package.
///  -On JS this provides http/https via browser APIs
/// </summary>
/// <type>namespace</type>
Machinata.IO = {};
