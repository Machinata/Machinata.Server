

/// <summary>
/// Framework for providing common privacy related settings and challenges.
/// </summary>
/// <type>namespace</type>
Machinata.Privacy = {};

/// <summary>
/// 
/// </summary>
Machinata.Privacy.autopadBody = false;

/// <summary>
/// 
/// </summary>
Machinata.Privacy.fadeInSpeedMS = 400;

/// <summary>
/// 
/// </summary>
Machinata.Privacy.autoShowDelayMS = 500;

/// <summary>
/// 
/// </summary>
Machinata.Privacy.agreements = [];

/// <summary>
/// 
/// </summary>
Machinata.Privacy.options = [];

/// <summary>
///
/// </summary>
/// <hidden/>
Machinata.Privacy._cachedSettings = null;

/// <summary>
///
/// </summary>
Machinata.Privacy.dialogUseBlockingOverlay = false;

/// <summary>
///
/// </summary>
Machinata.Privacy.dialogPosition = "top";

/// <summary>
/// Can either be ```standard``` or ```eu```.
/// </summary>
Machinata.Privacy.mode = "standard";

/// <summary>
/// 
/// </summary>
Machinata.Privacy.settings = function (val) {
    if (val == null) {
        if (Machinata.Privacy._cachedSettings == null) {
            var ret = $.cookie("Privacy");
            if (ret == null) Machinata.Privacy._cachedSettings = {};
            else Machinata.Privacy._cachedSettings = JSON.parse(ret);
        }
        return Machinata.Privacy._cachedSettings;
    } else {
        Machinata.debug("Machinata.Privacy.settings: saving");
        var expiresInDays = 360 * 2;
        $.cookie("Privacy", JSON.stringify(val), { expires: expiresInDays, path: '/' });
        return val;
    }
};

/// <summary>
/// 
/// </summary>
Machinata.Privacy.registerAgreement = function (type, version, date, link, title) {
    Machinata.debug("Machinata.Privacy.registerAgreement: "+type);
    //Machinata.debug("  type: " + type);
    //Machinata.debug("  version: " + version);
    //Machinata.debug("  date: " + date);
    //Machinata.debug("  link: " + link);
    //Machinata.debug("  title: " + title);
    var agreement = {};
    agreement["type"] = type;
    agreement["version"] = version;
    agreement["date"] = date;
    agreement["link"] = link;
    agreement["title"] = title;
    agreement["id"] = type + "_" + version;
    Machinata.Privacy.agreements.push(agreement);
};

/// <summary>
/// 
/// </summary>
Machinata.Privacy.registerOption = function (key, source, type, defaultValue, required, link, title, description, callback) {
    Machinata.debug("Machinata.Privacy.registerOption: "+key);
    //Machinata.debug("  key: " + key);
    //Machinata.debug("  type: " + type);
    //Machinata.debug("  defaultValue: " + defaultValue);
    //Machinata.debug("  required: " + required);
    //Machinata.debug("  link: " + link);
    //Machinata.debug("  title: " + title);
    //Machinata.debug("  description: " + description);
    // Validate
    if (source != "cookie") { alert("invalid option source"); return; };
    // Create option obj
    var option = {};
    option["key"] = key;
    option["type"] = type;
    option["defaultValue"] = defaultValue;
    option["source"] = source;
    option["required"] = required;
    option["link"] = link;
    option["title"] = title;
    option["description"] = description;
    option["id"] = key;
    option["callback"] = callback;
    Machinata.Privacy.options.push(option);
};

/// <summary>
/// 
/// </summary>
Machinata.Privacy.automaticValidationAndInfo = function (callback) {
    Machinata.debug("Machinata.Privacy.automaticValidationAndInfo");
    if ($("body").hasClass("bb-no-privacy-validation")) return;
    if (Machinata.queryParameter("privacy-validation") == "false") return;

    var pending = Machinata.Privacy.pendingAgreements();
    var settings = Machinata.Privacy.settings();
    if (pending.length > 0) {
        var reason = "new-visitor";
        if (settings["agreed"] == true) reason = "new-agreement";
        var elem = Machinata.Privacy.showSettings(reason, Machinata.Privacy.autoShowDelayMS);
        if (callback) callback(elem);
    } else {
        // Settings callback
        var options = Machinata.Privacy.currentOptions();
        for (var i = 0; i < options.length; i++) {
            var o = options[i];
            if (o["callback"] != null) o["callback"](Machinata.Privacy.getOption(o["key"], o));
        }
    }
};

/// <summary>
/// 
/// </summary>
Machinata.Privacy.closeSettings = function () {
    $("#privacy_settings").remove();
    $("#privacy_settings_overlay").remove();
    if (Machinata.Privacy.autopadBody == true) {
        $("body").css("padding-top", "unset");
    }
};

/// <summary>
/// 
/// </summary>
Machinata.Privacy.showSettings = function (reason, delay) {

    // Init
    var agreements = Machinata.Privacy.currentAgreements();

    // Reason
    if (reason == null) reason = "preferences";
    mode = Machinata.Privacy.mode;

    // Main elem
    var elem = $("<div class='ui-privacy-settings bb-privacy-settings' id='privacy_settings'></div>");
    if (Machinata.Privacy.dialogPosition == "top") elem.addClass("option-top");
    if (Machinata.Privacy.dialogPosition == "bottom") elem.addClass("option-bottom");
    if (Machinata.Privacy.dialogUseBlockingOverlay == true) elem.addClass("option-has-overlay");
    elem.hide();
    elem.addClass("settings-collapsed");

    // Title and description (standard mode)
    var agreeText = "{text.privacy.agree-button}";
    var essentialText = "{text.privacy.only-essential-button}";
    var title = null;
    if (reason == "preferences" || reason == "new-visitor") title = "{text.privacy.info-title.standard.preferences}";
    if (reason == "new-agreement") title = "{text.privacy.info-title.standard.new-agreement}";
    var desc = null;
    if (reason == "preferences" || reason == "new-visitor") desc = "{text.privacy.info-description.standard.preferences}";
    if (reason == "new-agreement") desc = "{text.privacy.info-description.standard.new-agreement}";
    desc += " {text.privacy.info-description.standard.general}";

    // Title and description (EU mode)
    if (mode == "eu") {
        agreeText = "{text.privacy.accept-all-button}";
        title = null;
        if (reason == "preferences" || reason == "new-visitor") title = "{text.privacy.info-title.eu.preferences}";
        if (reason == "new-agreement") title = "{text.privacy.info-title.eu.new-agreement}";
        desc = null;
        if (reason == "preferences" || reason == "new-visitor") desc = "{text.privacy.info-description.eu.preferences}";
        if (reason == "new-agreement") desc = "{text.privacy.info-description.eu.new-agreement}";
        desc += " {text.privacy.info-description.eu.general}";
    }

    if (reason == "preferences") agreeText += ", {text.privacy.save-button}";

    // Links
    var privacyLink = "";
    var termsLink = "";
    for (var i = 0; i < agreements.length; i++) {
        var a = agreements[i];
        if (a["type"] == "privacy") privacyLink = a["link"];
        if (a["type"] == "terms") termsLink = a["link"];
    }

    // Description elems
    desc = desc.replace("{terms}", "<a class='terms link'></a>");
    desc = desc.replace("{privacy}", "<a class='privacy link'></a>");
    var elemDesc = $("<div class='description'></div>");
    elemDesc.append($("<h1></h1>").text(title));
    elemDesc.append($("<p></p>").html(desc));
    elemDesc.find("a.link.terms").text("{text.privacy.terms-of-use}").attr("href", termsLink).attr("target", "_blank");
    elemDesc.find("a.link.privacy").text("{text.privacy.privacy-policy}").attr("href", privacyLink).attr("target", "_blank");
    elem.append(elemDesc);

    // Settings
    var options = Machinata.Privacy.currentOptions();
    var elemSettings = $("<div class='settings'></div>");
    var elemSettingsTitle = $("<div class='title-and-button'></div>");
    elemSettingsTitle.append($("<h2></h2>").text("{text.privacy.settings-title}"));
    elemSettingsTitle.append($("<a class='edit-button toggle-settings-button'></a>").text("{text.privacy.settings-edit}"));
    elemSettingsTitle.append($("<a class='hide-button toggle-settings-button'></a>").text("{text.privacy.settings-hide}"));
    elemSettings.append(elemSettingsTitle);
    elemSettings.append($("<div class='clear'></div>"));
    var elemSettingsOptions = $("<div class='options'></div>");
    for (var i = 0; i < options.length; i++) {
        var o = options[i];
        var elemOption = $("<div class='option'></div>");
        var elemId = "privacy_option_" + o["id"]
        var elemInput = $("<input/>").attr("type", o["type"]).attr("id", elemId);
        var val = o["value"];
        if (val == null) val = o["defaultValue"];
        if (val == true && o["type"] == "checkbox") {
            elemInput.attr("checked", "checked");
        }
        if (o["required"] == true) {
            elemInput.attr("disabled", "disabled");
        }
        elemOption.append(elemInput);
        elemOption.append($("<label/>").attr("for", elemId).text(o["title"]));
        elemOption.append($("<div/>").text(o["description"]));
        if (o["required"] == true) {
            elemInput.attr("title", "{text.privacy.option-required}");
            elemOption.find("label").attr("title", "{text.privacy.option-required}");
        }
        elemSettingsOptions.append(elemOption);
    }
    elemSettings.append(elemSettingsOptions);
    elem.append(elemSettings);

    // Agree button
    var elemButtons = $("<div class='buttons'></div>");
    if (mode == "eu") elemButtons.append($("<input class='only-essential-button ui-button option-secondary'/>").attr("type", "button").val(essentialText));
    elemButtons.append($("<input class='agree-button ui-button option-primary'/>").attr("type", "button").val(agreeText));
    elem.append(elemButtons);
    
    // Bind actions
    elemSettings.find(".toggle-settings-button").click(function () {
        if (elem.hasClass("settings-collapsed")) {
            elem.removeClass("settings-collapsed");
        } else {
            elem.addClass("settings-collapsed");
        }
    });
    elemButtons.find(".agree-button").click(function () {
        // Register options
        for (var i = 0; i < options.length; i++) {
            var o = options[i];
            var optionElem = $("#privacy_option_" + o.key);
            var val = optionElem.val();
            if (o["type"] == "checkbox") val = optionElem.is(":checked");
            Machinata.Privacy.setOption(o.key, val);
            if (o["callback"] != null) o["callback"](val, o);
        }
        // Accept agreements
        Machinata.Privacy.acceptCurrentAgrrements();
        // Mimick some loading activity
        Machinata.showLoading(true);
        setTimeout(function () {
            Machinata.showLoading(false);
            // Close
            if (!elem.hasClass("settings-inline")) Machinata.Privacy.closeSettings();
        }, 300);
    });
    elemButtons.find(".only-essential-button").click(function () {
        // Register options
        for (var i = 0; i < options.length; i++) {
            var o = options[i];
            var val = o.defaultValue;
            Machinata.Privacy.setOption(o.key, val);
            if (o["callback"] != null) o["callback"](val, o);
        }
        // Accept agreements
        Machinata.Privacy.acceptCurrentAgrrements();
        // Mimick some loading activity
        Machinata.showLoading(true);
        setTimeout(function () {
            Machinata.showLoading(false);
            // Close
            if (!elem.hasClass("settings-inline")) Machinata.Privacy.closeSettings();
        }, 300);
    });

    // Add to DOM
    let overlayElem = null;
    if(Machinata.Privacy.dialogUseBlockingOverlay == true) {
        overlayElem = $("<div class='ui-privacy-settings-overlay' id='privacy_settings_overlay'></div>");
        //overlayElem.click(function () {
        //    Machinata.Privacy.closeSettings();
        //});
        $("body").append(overlayElem);
    }
    $("body").append(elem);
    if (delay == null) delay = 0;
    setTimeout(function () {
        if (Machinata.Privacy.fadeInSpeedMS == 0 || Machinata.Privacy.fadeInSpeedMS == null) {
            elem.show();
        } else {
            elem.fadeTo(0, 0.0);
            elem.show();
            elem.fadeTo(Machinata.Privacy.fadeInSpeedMS, 1.0);
        }
        if (Machinata.Privacy.autopadBody == true) {
            elem.css("position", "absolute");
            $("body").css("padding-top", elem.height() + "px");
        }
    }, delay);
    return elem;
};

/// <summary>
/// 
/// </summary>
Machinata.Privacy.setOption = function (key, val) {
    Machinata.debug("Machinata.Privacy.setOption: "+key+"="+val);
    //Machinata.debug("  key: " + key);
    //Machinata.debug("  val: " + val);
    var settings = Machinata.Privacy.settings();
    settings["option_" + key] = val;
    Machinata.Privacy.settings(settings);
};

/// <summary>
/// 
/// </summary>
Machinata.Privacy.getOption = function (key) {
    Machinata.debug("Machinata.Privacy.getOption: "+key);
    //Machinata.debug("  key: " + key);
    var settings = Machinata.Privacy.settings();
    var val = settings["option_" + key];
    Machinata.debug("  val: " + val);
    return val;
};

/// <summary>
/// 
/// </summary>
Machinata.Privacy.cookie = function (option,key,val) {
    var optionValue = Machinata.Privacy.getOption(option);
    if (optionValue == true) {
        if (val != null) {
            var expiresInDays = 360 * 1;
            $.cookie(key, val, { expires: expiresInDays, path: '/' });
            return val;
        } else {
            return $.cookie(key);
        }
    } else {
        return null;
    }
};

/// <summary>
/// 
/// </summary>
Machinata.Privacy.acceptCurrentAgrrements = function () {
    var settings = Machinata.Privacy.settings();
    var current = Machinata.Privacy.currentAgreements();
    Machinata.debug("Machinata.Privacy.acceptCurrentAgrrements");
    for (var i = 0; i < current.length; i++) {
        var a = current[i];
        Machinata.debug("  " + a.id);
        settings["agreement_" + a.id + "_agreed"] = true;
        settings["agreement_" + a.id + "_agreedate"] = new Date();
    }
    settings["agreed"] = true;
    settings["agreedate"] = new Date();
    Machinata.Privacy.settings(settings);
};

/// <summary>
/// 
/// </summary>
Machinata.Privacy.currentOptions = function () {
    var ret = [];
    for (var i = 0; i < Machinata.Privacy.options.length; i++) {
        var o = Machinata.Privacy.options[i];
        o["value"] = Machinata.Privacy.getOption(o["key"]);
        ret.push(o);
    }
    return ret;
};

/// <summary>
/// 
/// </summary>
Machinata.Privacy.currentAgreements = function () {
    // Get the current most agreements
    var settings = Machinata.Privacy.settings();
    Machinata.debug("Machinata.Privacy.currentAgreements");
    var types = {};
    for (var i = 0; i < Machinata.Privacy.agreements.length; i++) {
        var a = Machinata.Privacy.agreements[i];
        if (types[a.type] == null || a.version > types[a.type].version) {
            types[a.type] = a;
        }
    }
    var ret = [];
    for (var i = 0; i < Object.keys(types).length; i++) {
        var k = Object.keys(types)[i];
        var a = types[k];
        Machinata.debug("  " + a.id);
        a["agreed"] = settings["agreement_" + a.id + "_agreed"];
        a["agreedate"] = settings["agreement_" + a.id + "_agreedate"];
        ret.push(a);
    }
    return ret;
};

/// <summary>
/// 
/// </summary>
Machinata.Privacy.pendingAgreements = function () {
    var current = Machinata.Privacy.currentAgreements();
    Machinata.debug("Machinata.Privacy.pendingAgreements");
    var ret = [];
    for (var i = 0; i < current.length; i++) {
        var a = current[i];
        if (a.agreed != true) {
            ret.push(a);
            Machinata.debug("  "+a.id);
        }
    }
    return ret;
};

