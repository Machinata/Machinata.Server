

/// <summary>
/// Utilities for parsing data...
/// </summary>
/// <type>namespace</type>
Machinata.Parser = {};

/// <summary>
/// A lightweight yet powerful text expression parser for filtering.
/// To normalize the input for the filter you can use ```Machinata.Parser.normalizeStringInputToBasicType```.
///
/// ## Filter Expressions
/// Expressions in filters allow for some simple yet powerful syntax within filters. The expression language
/// has been purposefully crafted with the end-user in mind, which can use expressions from the table filters UI.
/// The expression parser behind the filters is provided by ```Machinata.Parser.parseTextFilter```.
/// 
/// The following filter expressions are supported:
///  - exact match: ```"keyword"```,```'keyword'```
///  - not: ```!"keyword"```, ```!100```
///  - less than: ```< 100```, ```<100```, ```<= 100```
///  - greater than: ```> 100```, ```>100```, ```>= 100```
///  - and: ```> 100 and < 102```
///  - or: ```'EUR' or 'USD'```
///  - range: ```10 - 30```
///
/// String literals are provided by the ```'``` or ```"``` character, and can be escaped through ```\'``` or ```\"``` characters.
/// The util method Machinata.Parser.escapeString can be used to escape a string.
///
/// Numbers are represented through the regex ```/^-?\d+\.?\d*$/``` and table cell content is normalized to basic types
/// using ```Machinata.Parser.normalizeStringInputToBasicType```, where some characters (```'``` and ```,```) are stripped before testing for number.
/// </summary>
Machinata.Parser.parseTextFilter = function (filter, normalizedInput) {
    // Return from parser
    return Machinata.Parser._textFilterParser.parse(filter, { input: normalizedInput });
};

/// <summary>
/// </summary>
Machinata.Parser.escapeString = function (str) {
    // Sanity
    if (str == null) return "''";
    // Replace all ' chars with escape sequence
    str = Machinata.String.replaceAll(str, "'", "\\'");
    // Return escaped version
    return "'" + str + "'";
};

/// <summary>
/// </summary>
Machinata.Parser.convertIsAnyArrayToTextFilter = function (isAny) {
    // Sanity
    if (isAny == null) return "";
    if (isAny.length == 0) return "";
    // Generate
    var filters = [];
    for (var i = 0; i < isAny.length; i++) {
        // Escape strings, but not numbers
        if (isNaN(isAny[i])) {
            filters.push(Machinata.Parser.escapeString(isAny[i]));
        } else {
            filters.push(isAny[i]);
        }
    }
    return filters.join(" or ");
};

/// <summary>
/// </summary>
Machinata.Parser.convertNotAnyArrayToTextFilter = function (isAny) {
    // Sanity
    if (isAny == null) return "";
    if (isAny.length == 0) return "";
    // Generate
    var filters = [];
    for (var i = 0; i < isAny.length; i++) {
        // Escape strings, but not numbers
        if (isNaN(isAny[i])) {
            filters.push("!" +Machinata.Parser.escapeString(isAny[i]));
        } else {
            filters.push("!"+isAny[i]);
        }
    }
    return filters.join(" and ");
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Parser._IS_INT_OR_FLOAT_REGEX = /^-?\d+\.?\d*$/

/// <summary>
/// Normalizes string input into basic types (strings and floats) and returns the normalized type.
/// All numbers are returned as floats.
/// Numbers are represented through the regex ```/^-?\d+\.?\d*$/``` and table cell content is normalized to basic types
/// Some characters (```'``` and ```,```) are stripped before testing for a number, in addition numbers ending in % are treated as a number.
/// </summary>
Machinata.Parser.normalizeStringInputToBasicType = function (str) {
    // Sanity
    if (str == null || str == "") return str;
    // Get a simplified string version of the potential number
    var numberStrToTest = str.trim();
    // Does it start or end with a escape char?
    if (str[0] == "'" || str[0] == "\"") return str;
    if (str[str.length - 1] == "'" || str[str.length - 1] == "\"") return str;
    // Clean out digit makeup...
    numberStrToTest = Machinata.String.replaceAll(numberStrToTest,"'","");
    numberStrToTest = Machinata.String.replaceAll(numberStrToTest,",","");
    if (numberStrToTest[numberStrToTest.length - 1] == '%') numberStrToTest = numberStrToTest.substring(0, numberStrToTest.length - 1);
    //console.log("numberStrToTest", str, numberStrToTest);
    // Test if it is a number
    if (Machinata.Parser._IS_INT_OR_FLOAT_REGEX.test(numberStrToTest) == true) {
        var strAsFloat = parseFloat(numberStrToTest);
        if (isNaN(strAsFloat) == false) {
            //console.log("strAsFloat", strAsFloat);
            return strAsFloat; // float
        } else {
            return str; // fallback to string
        }
    } else {
        return str; // string
    }
};