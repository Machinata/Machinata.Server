
/// <summary>
/// NodeJS: Loads the file contents given at ```pathOrURL``` as text.
/// </summary>
Machinata.IO.loadFileContentsAsText = function (pathOrURL, opts) {
    if (opts == null) opts = {};
    if (opts.encoding == null) opts.encoding = "utf8";

    return new Promise((resolve, reject) => {
        try {
            if (pathOrURL.startsWith("https://") || pathOrURL.startsWith("http://")) {
                // URL
                const http = require("http");
                const https = require("https");
                const data = [];
                const client = pathOrURL.startsWith("https") ? https : http;
                client.request(pathOrURL, (res) => {
                    res.on("data", (chunk) => data.push(chunk));
                    res.on("end", () => {
                        const byteData = Buffer.concat(data);
                        const stringData = byteData.toString(opts.encoding);
                        resolve(stringData);
                    });
                    res.on("error", (err) => reject(err));
                }).end();
            } else {
                // Disk
                const fs = require("fs");
                fs.readFile(pathOrURL, opts.encoding, (err, data) => {
                    if (err) {
                        throw err;
                        return;
                    }
                    resolve(data);
                });
            }
        } catch (err) {
            reject(err);
        }
        
    });

    
};
