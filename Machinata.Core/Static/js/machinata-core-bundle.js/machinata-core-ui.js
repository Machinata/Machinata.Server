

/// <summary>
/// jQuery DOM Ready
/// </summary>
/// <hidden/>
$(document).ready(function () {
    Machinata.init();
});

/// <summary>
/// jQuery Window Load
/// </summary>
/// <hidden/>
$(window).on('load', function () {
    // Run all on loaded...
    for (var i = 0; i < Machinata._onLoaded.length; i++) Machinata._onLoaded[i]();
});

/// <summary>
/// Collection of useful UI related utilities.
/// </summary>
/// <type>namespace</type>
Machinata.UI = {};

/// <summary>
/// 
/// </summary>
Machinata.UI.elemUID = 0;

/// <summary>
/// 
/// </summary>
Machinata.DISABLE_CORE_UI = false;

/// <summary>
/// 
/// </summary>
Machinata.UI.getElemUID = function (name) {
    Machinata.UI.elemUID++;
    if (name != null) return name + "_" + Machinata.UI.elemUID;
    else return "machinata_autoid_" + Machinata.UI.elemUID;
};

/// <summary>
/// 
/// </summary>
Machinata.UI.autoUIDElem = function (elem) {
    if (elem.attr("id") == null) elem.attr("id", Machinata.UI.getElemUID());
    return elem.attr("id");
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.onInit(function () {
    if (Machinata.DISABLE_CORE_UI == true) return;
    // Register bind events
    Machinata.UI.onUIBind(Machinata.UI.bindEvents);
    Machinata.UI.onUIBind(Machinata.UI.bindButtons);
    Machinata.UI.onUIBind(Machinata.UI.bindAnimations);
    Machinata.UI.onUIBind(Machinata.UI.bindInputs);
    Machinata.UI.onUIBind(Machinata.UI.bindTables);
    Machinata.UI.onUIBind(Machinata.UI.bindText);
    Machinata.UI.onUIBind(Machinata.UI.bindAutosave);
    Machinata.UI.onUIBind(Machinata.UI.bindCurrencies);
    Machinata.UI.onUIBind(Machinata.UI.bindImages);
    Machinata.UI.onUIBind(Machinata.UI.bindEditableEntityLists);
});

/// <summary>
///
/// </summary>
/// <hidden/>

Machinata.ready(function () {
    if (Machinata.DISABLE_CORE_UI == true) return;
    // Initial bind
    Machinata.UI.bind();
});

/// <summary>
///
/// </summary>
/// <hidden/>
Machinata.UI._onUIBind = [];

/// <summary>
///
/// </summary>
/// <hidden/>
Machinata.UI.onUIBind = function (fn) {
    Machinata.UI._onUIBind.push(fn);
};


/// <summary>
/// 
/// </summary>
Machinata.UI.bind = function (elements) {
    if (Machinata.DISABLE_CORE_UI == true) return;

    var elementsName = "custom elements";
    if (elements == null) {
        elements = $("body");
        elementsName = "body";
    }
    var filteredElements = elements.not(".bb-ui-bound");
    if (Machinata.DebugEnabled == true) {
        Machinata.debug("Machinata.UI.bind(" + elementsName + ")");
        Machinata.debug("  Found " + filteredElements.length + " elements:");
        filteredElements.each(function () {
            Machinata.debug($(this));
        });
    }
    for (var i = 0; i < Machinata.UI._onUIBind.length; i++) {
        Machinata.UI._onUIBind[i](filteredElements);
    }
};


/// <summary>
/// 
/// </summary>
Machinata.UI.bindInputs = function (elements) {
    elements.find("textarea.bb-autogrow").autogrow({ vertical: true, horizontal: false, flickering: true });

    elements.find(".ui-input-and-action").each(function () {
        var inputAndActionElem = $(this);
        function incrementOrDecrement(amount) {
            var val = parseInt(inputAndActionElem.find("input").val());
            if (isNaN(val)) val = 0;
            val = val + amount;
            // Validate
            if (!isNaN(inputAndActionElem.find("input").attr("min"))) {
                var minVal = parseInt(inputAndActionElem.find("input").attr("min"));
                if (val < minVal) val = minVal;
            }
            if (!isNaN(inputAndActionElem.find("input").attr("max"))) {
                var maxVal = parseInt(inputAndActionElem.find("input").attr("max"));
                if (val > maxVal) val = maxVal;
            }
            // Set
            inputAndActionElem.find("input").val(val).trigger("change");
        }
        inputAndActionElem.find("button.option-minus").click(function (event) {
            event.preventDefault();
            incrementOrDecrement(-1);
        });
        inputAndActionElem.find("button.option-plus").click(function (event) {
            event.preventDefault();
            incrementOrDecrement(+1);
        });
    });

    
};


/// <summary>
/// 
/// </summary>
Machinata.UI.autogrowTextArea = function (elements, opts) {
    function update(domElem) {
        domElem.style.height = 'inherit';
        var computed = window.getComputedStyle(domElem);
        var height = parseInt(computed.getPropertyValue('border-top-width'), 10)
            + parseInt(computed.getPropertyValue('padding-top'), 10)
            + domElem.scrollHeight
            + parseInt(computed.getPropertyValue('padding-bottom'), 10)
            + parseInt(computed.getPropertyValue('border-bottom-width'), 10);
        domElem.style.height = height + 'px';
    }
    elements.on("input", function () {
        update(this);
    });
    setTimeout(function () {
        elements.each(function () {
            update(this);
        });
    }, 50);
    
};

/// <summary>
/// 
/// </summary>
Machinata.UI.bindTables = function (elements) {
    // See https://github.com/jmosbech/StickyTableHeaders
    elements.find("table.bb-sticky-headers,table.ui-table.option-sticky-header").each(function () {
        var elem = $(this);
        var rows = elem.find("tr").length;
        var minRowCount = 15; // default
        var attrMinRowCount = elem.attr("data-sticky-headers-min-row-count");
        if (attrMinRowCount != "" && attrMinRowCount != null) minRowCount = parseInt(attrMinRowCount);
        if (rows >= minRowCount) elem.stickyTableHeaders({ cacheHeaderHeight: true, fixedOffset: $("#header") });
    })
};

/// <summary>
/// 
/// </summary>
Machinata.UI.bindEvents = function (elements) {


    // Zoomable backgrounds
    elements.find(".bb-zoom-background").each(function () {
        var elem = $(this);
        elem.zoom({
            // TODO: options, see http://www.jacklmoore.com/zoom/
            // Property 	Default 	Description
            // url 	false 	The url of the large photo to be displayed. If no url is provided, zoom uses the src of the first child IMG element inside the element it is assigned to.
            // on 	'mouseover' 	The type of event that triggers zooming. Choose from mouseover, grab, click, or toggle.
            // duration 	120 	The fadeIn/fadeOut speed of the large image.
            // target 	false 	A selector or DOM element that should be used as the parent container for the zoomed image.
            // touch 	true 	Enables interaction via touch events.
            // magnify 	1 	This value is multiplied against the full size of the zoomed image. The default value is 1, meaning the zoomed image should be at 100% of its natural width and height.
            // callback 	false 	A function to be called when the image has loaded. Inside the function, `this` references the image element.
            // onZoomIn 	false 	A function to be called when the image has zoomed in. Inside the function, `this` references the image element.
            // onZoomOut 	false 	A function to be called when the image has zoomed out. Inside the function, `this` references the image element. 
        });
    }); 

    // Button on-press
    elements.find("[on-press]").addClass("bb-ui-bound").each(function () {
        var elem = $(this);
        var confirm = elem.attr("confirm-message");
        var input = elem.attr("input-message");
        var inputName = elem.attr("input-name");
        if (inputName == null) inputName = "input";
        var title = null;
        if (elem.attr("confirm-title") != null) title = elem.attr("confirm-title");
        if (elem.attr("input-title") != null) title = elem.attr("input-title");
        elem.click(function () {
            // Show confirm diag?
            if (!String.isEmpty(confirm) || !String.isEmpty(input)) {
                if (!String.isEmpty(input)) {
                    // Show input dialog
                    var dialogTitle = title;
                    var dialogMessage = input;
                    if (String.isEmpty(dialogTitle)) {
                        dialogTitle = input;
                        dialogMessage = null;
                    }
                    Machinata.inputDialog(dialogTitle, dialogMessage, null)
                        .input(function (val, id) {
                            var page = elem.attr("on-press");
                            page = page.replace("{" + inputName + "}",encodeURIComponent(val));
                            Machinata.goToPage(page);
                        })
                        .show();
                } else {
                    // Just show confirm dialog
                    var dialogTitle = title;
                    var dialogMessage = confirm;
                    if (String.isEmpty(dialogTitle)) {
                        dialogTitle = confirm;
                        dialogMessage = null;
                    }
                    Machinata.confirmDialog(dialogTitle, dialogMessage)
                        .okay(function () {
                            Machinata.goToPage(elem.attr("on-press"));
                        })
                        .show();
                }

            } else {
                Machinata.goToPage(elem.attr("on-press"));
            }
        });
    });

};


/// <summary>
/// 
/// </summary>
Machinata.UI.bindText = function (elements) {
    // Line breaks
    $(".bb-auto-linebreak").each(function () {
        $(this).html($(this).html().replaceAll("\n", "<br/>"));
        $(this).html($(this).html().replaceAll("\\\\", "<br/>"));
    });
};

/// <summary>
/// 
/// </summary>
Machinata.UI.convertNewLinesToLineBreaks = function (elements) {
    elements.each(function () {
        $(this).html($(this).html().replaceAll("\n", "<br/>"));
    });
};


/// <summary>
/// 
/// </summary>
Machinata.UI.bindImages = function (elements) {
    elements.find(".ui-image-comparer").addClass("bb-ui-bound").each(function () {
        Machinata.UI.createImageComparer($(this));
    });
};

/// <summary>
/// 
/// </summary>
Machinata.UI.bindButtons = function (elements) {
    // Pre-pass for jquery UI checkbox/radio
    elements.find(".bb-checkbox,.bb-radio").each(function () { //TODO:@dan why not add class uibound?
        var elem = $(this);
        // No ID?
        if (String.isEmpty(elem.attr("id"))) {
            elem.attr("id", Machinata.UI.getElemUID());
        }
        // Selected?
        if (elem.hasClass("selected")) {
            elem.attr("checked", "checked");
        }
        // Missing label elem?
        var labelElem = $("label[for='" + elem.attr("id") + "']");
        if (labelElem.length == 0) {
            var labelText = elem.attr("label");
            labelElem = $("<label for=''></label>");
            labelElem.attr("for", elem.attr("id"));
            labelElem.text(labelText);
            labelElem.insertAfter(elem);
        }
        // Custom state labels?
        //TODO
        
    });

    // Do button sets
    elements.find(".bb-button-set").addClass("bb-ui-bound").each(function () {
        var elem = $(this);
        if (elem.attr("selected-button") != null) {
            var button = elem.find(".button[name='" + elem.attr("selected-button") + "']");
            button.addClass("selected");
        }
        var childButtonSelector = "";
        childButtonSelector += "input";
        childButtonSelector += ",button";
        if (elem.children("a.enabled").length > 0) childButtonSelector += ",a.selected";
        else childButtonSelector += ",a";
        var numButtons = elem.children(childButtonSelector).length;
        elem.attr("data-button-count", numButtons);
        elem.buttonset();
    });

    // Do toggle sets
    elements.find(".bb-toggle-content").addClass("bb-ui-bound").each(function () {
        var toggleContent = $(this);
        var buttons = toggleContent.find("[data-toggle-element]");
        var elems = toggleContent.find("[data-element-id]");
        // Strip elements that are purposfully hidden, as we want to make sure we dont show these through the toggle...
        buttons.filter(":hidden").remove();
        elems.filter(":hidden").remove();
        buttons = toggleContent.find("[data-toggle-element]");
        elems = toggleContent.find("[data-element-id]");
        buttons.click(function () {
            var button = $(this);
            elems.hide();
            elems.filter("[data-element-id=\"" + button.attr("data-toggle-element") + "\"]").show();
            buttons.removeClass("selected");
            button.addClass("selected");
        });
        buttons.first().trigger("click");
    });

    // Post-pass for jquery UI checkbox/radio
    elements.find(".bb-checkbox,.bb-radio").checkboxradio().addClass("bb-ui-bound").each(function () {
        var elem = $(this);
        // Row selector?
        if (elem.hasClass("selects-row")) {
            if (!elem.is(":checked")) {
                elem.closest("tr").addClass("deselected");
            }
            elem.on('change selected deselected', function () {
                if (elem.is(":checked")) elem.closest("tr").removeClass("deselected");
                else elem.closest("tr").addClass("deselected")
            });
        }
    });
    elements.find(".ui-checkbox input").addClass("bb-ui-bound").each(function () { //Backwards compatibility to new uikit
        var elem = $(this);
        // Row selector?
        if (elem.hasClass("selects-row")) {
            if (!elem.prop("checked")) {
                elem.closest("tr").addClass("deselected");
            }
            elem.on('change selected deselected', function () {
                if (elem.prop("checked")) elem.closest("tr").removeClass("deselected");
                else elem.closest("tr").addClass("deselected")
            });
        }
    });

    // Select menu
    elements.find(".bb-select").selectmenu().addClass("bb-ui-bound");

    // Date range
    elements.find(".bb-daterange").addClass("bb-ui-bound").click(function () {
        // Init
        var input = $(this);
        var dateFormat = Machinata.DEFAULT_DATE_FORMAT_JAVASCRIPT_UI;
        var timeFormat = Machinata.DEFAULT_TIME_FORMAT_JAVASCRIPT_UI;
        if (!String.isEmpty(input.attr("date-format"))) dateFormat = input.attr("date-format");
        if (!String.isEmpty(input.attr("time-format"))) timeFormat = input.attr("time-format");
        var format = dateFormat + " " + timeFormat;
        // Convert for picker
        if (dateFormat != null) dateFormat = Machinata.convertDotNetTimeFormatToDatepickerFormat(dateFormat);
        if (timeFormat != null) timeFormat = Machinata.convertDotNetTimeFormatToDatepickerFormat(timeFormat);
        Machinata.dateDialog(null, null, null, input.val(), dateFormat, timeFormat, true)
                    .input(function (datetime, id) {
                        if (id == "clear") {
                            input.val("");
                            return;
                        }
                        input.val(Machinata.formattedDate(datetime.start, format) + Machinata.DEFAULT_DATERANGE_SEPARATOR + Machinata.formattedDate(datetime.end, format));
                    })
                    .show();
    });


};


/// <summary>
/// 
/// </summary>
Machinata.UI.bindAutosave = function (elements) {
    // Sanity
    try {
        if (!window.localStorage) return;
    } catch (error) {
        console.warn("Machinata.UI.bindAutosave: could not set localstorage: ", error);
    }
    elements.find("input.bb-autosave").each(function () {
        var elem = $(this);
        var key = elem.attr("data-bb-autosave");
        if (key == null || key == "") key = elem.attr("name");
        key = "autosave_" + key;
        elem.on("blur", function () {
            try {
                window.localStorage.setItem(key, elem.val());
            } catch (error) {
                console.warn("Machinata.UI.bindAutosave: could not set localstorage: ",error);
            }
        });
        if (elem.val() == null || elem.val() == "") {
            try {
                elem.val(window.localStorage.getItem(key));
            } catch (error) {
                console.warn("Machinata.UI.bindAutosave: could not set localstorage: ", error);
            }
        }
    });
    
};


/// <summary>
/// 
/// </summary>
Machinata.UI.bindCurrencies = function (elements) {
    Machinata.Formatting.formatCurrencyElements(elements.find(".bb-currency"));
};


/// <summary>
/// 
/// </summary>
Machinata.UI.isInViewport = function (elem) {
    var top_of_element = elem.offset().top;
    var bottom_of_element = elem.offset().top + elem.outerHeight();
    var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
    var top_of_screen = $(window).scrollTop();

    if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
        return true;
    } else {
        return false;
    }
};


/// <summary>
/// 
/// </summary>
Machinata.UI.getElementBoundsInViewport = function (elem) {
    var topOfElement = elem.offset().top;
    var bottomOfelement = topOfElement + elem.outerHeight();

    var leftOfElement = elem.offset().left;
    var rightOfElement = leftOfElement + elem.outerWidth();

    var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
    var top_of_screen = $(window).scrollTop();

    return {
        top: topOfElement,
        left: leftOfElement,
        right: rightOfElement,
        bottom: bottomOfelement,
    }
};


/// <summary>
/// 
/// </summary>
Machinata.UI.getElementBoundsInPage = function (elem) {
    var topOfElement = elem.offset().top;
    var bottomOfelement = topOfElement + elem.outerHeight();
    var leftOfElement = elem.offset().left;
    var rightOfElement = leftOfElement + elem.outerWidth();
    return {
        top: topOfElement,
        left: leftOfElement,
        right: rightOfElement,
        bottom: bottomOfelement,
    }
};

/// <summary>
/// 
/// </summary>
Machinata.UI.SCROLL_TO_HEADER_PUSHER_MULTIPLIER = 1.2;

/// <summary>
/// 
/// </summary>
Machinata.UI.scrollTo = function (target,duration,settings) {
    // Forward to jquery.scrollTo
    // Settings:
    //   axis: The axes to animate: xy (default), x, y, yx
    //   interrupt: If true will cancel the animation if the user scrolls. Default is false
    //   limit: If true the plugin will not scroll beyond the container's size. Default is true
    //   margin: If true, subtracts the margin and border of the target element. Default is false
    //   offset: Added to the final position, can be a number or an object with left and top
    //   over: Adds a % of the target dimensions: {left:0.5, top:0.5}
    //   queue: If true will scroll one axis and then the other. Default is false
    //   onAfter(target, settings): A callback triggered when the animation ends (jQuery's complete())
    //   onAfterFirst(target, settings): A callback triggered after the first axis scrolls when queueing
    if (settings == null) settings = {};
    // Automatically set the offset (if not set) to the header pusher height
    var headerPusherElem = $("#headerpusher,#header-pusher");
    if (settings.offset == null && headerPusherElem.length == 1) {
        settings.offset = -headerPusherElem.height() * Machinata.UI.SCROLL_TO_HEADER_PUSHER_MULTIPLIER;
    }
    // Validate that we even need to do a scroll, if the elem is close to the destination, then
    // we don't do the scroll, as it just annoys the user.
    var performScroll = true;
    if (settings.ignoreIfTargetIsCloseToDestination == true) {
        var targetTop = target.offset().top;
        var windowTop = $(window).scrollTop();
        var offsetTop = 0;
        if (settings.offset != null && settings.offset.top != null) offsetTop = settings.offset.top;
        else if (settings.offset != null) offsetTop = settings.offset;
        var windowTargetTop = targetTop + offsetTop;
        var dist = windowTargetTop - windowTop;
        // Only account for positive distances (if minus we need to reveal the element)
        var minDist = $(window).height() * 0.6;
        if (dist > 0 && dist < minDist) performScroll = false;
    }
    // Perform the scroll using jquery
    if (performScroll == true) {
        $(window).scrollTo(target, duration, settings);
    }
};


/// <summary>
/// 
/// </summary>
Machinata.UI.bindAnchorsToScroll = function (duration, settings) {
    $("[href^='#']").click(function (e) {
        e.preventDefault();
        var target = $($(this).attr("href")); // Try element id, ie #id
        if (target.length == 0) target = $("a[name='" + $(this).attr("href").substring(1) + "']");
        Machinata.UI.scrollTo(target, duration, settings);
    });
};
Machinata.UI.bindAncorsToScroll = Machinata.UI.bindAnchorsToScroll;

/// <summary>
/// Automatically converts all absolute anchors that point to the current page, to the local anchor on the page...
/// </summary>
Machinata.UI.convertAbsoluteAnchorsToLocal = function () {
    $("[href^='" + window.location.pathname + "#']").each(function () {
        var currentHREF = $(this).attr("href");
        var newHREF = currentHREF;
        try {
            newHREF = new URL(currentHREF, window.location.origin).hash;
        } catch (e) {
            console.warn("Machinata.UI.convertAbsoluteAnchorsToLocal: " + "Could not change href from " + currentHREF + " to " + newHREF + ": " + e);
        }
        $(this).attr("href", newHREF);
    });
};

/// <summary>
/// 
/// </summary>
Machinata.UI.bindAnimations = function (elements) {
    // Animate on appear
    elements.find(".ui-animate-on-appear,.bb-animate-on-appear").not(".ui-bound").addClass("ui-bound").each(function () {
        var elem = $(this);
        var update = function () {
            if (elem.visible(true)) {
                var delay = elem.attr("data-animate-on-appear-delay");
                if (delay == null) {
                    elem.addClass("on-appear");
                } else {
                    delay = parseInt(delay);
                    setTimeout(function () {
                        elem.addClass("on-appear");
                    }, delay);
                }
                
            } else if (elem.hasClass("animate-on-disappear")) {
                elem.removeClass("on-appear");
            }
        };
        $(window).on('scroll', update);
        update();
        elem.on("check-animate-on-appear", update);
    });
};



/// <summary>
/// <param name="input">input element selector</param>
/// <param name="countries">string array of country codes</param>
/// </summary>
Machinata.UI.convertInputToCountrySelection = function (input, countries, additional) {
    var selectElem = $("<select></select>");
    var didSetVal = false;
    var selectedVal = input.val();
    var selectedCountries = null;
    if (countries == null) {
        // All
        selectedCountries = Machinata.COUNTRIES;
    } else {
        // Create selection
        selectedCountries = [];
        for (var cci = 0; cci < countries.length; cci++) {
            var cc = countries[cci];
            for (var ci = 0; ci < Machinata.COUNTRIES.length; ci++) {
                var c = Machinata.COUNTRIES[ci];
                if (c.code == cc) {
                    selectedCountries.push(c);
                    break;
                }
            }
        }
    }
    // Additional?
    if (additional != null) {
        var additionals = additional.split(",");
        for (var i = 0; i < additionals.length; i++) {
            for (var ci = 0; ci < Machinata.COUNTRIES.length; ci++) {
                var c = Machinata.COUNTRIES[ci];
                if (c.code == additionals[i]) {
                    selectedCountries.push(c);
                    break;
                }
            }
        }
    }
    // Sort!
    selectedCountries.sort(function (a, b) {
        if (a.name < b.name) return -1;
        if (a.name > b.name) return 1;
        return 0;
    });
    if(selectedVal != null) selectedVal = selectedVal.toLowerCase();
    for (var i = 0; i < selectedCountries.length; i++) {
        var c = selectedCountries[i];
        var optElem = $("<option/>");
        optElem.text(c.name);
        optElem.attr("value", c.code.toLowerCase());
        if (c.code.toLowerCase() == selectedVal) {
            optElem.attr("selected", "selected");
            didSetVal = true;
        }
        selectElem.append(optElem);
    }
    selectElem.attr("name", input.attr("name"));
    selectElem.attr("class", input.attr("class"));
    if (input.attr("required") != null) selectElem.attr("required", input.attr("required"));
    input.replaceWith(selectElem);
};

/// <summary>
/// 
/// </summary>
Machinata.UI.convertExternalLinksToBlankTargets = function (elements) {
    if (elements == null) elements = $("body");
    var links = elements.find("a");
    links.each(function () {
        var link = $(this);
        var url = link.attr("href");
        if(url == null) return;
        if (url.startsWith("http://") || url.startsWith("https://")) {
            link.attr("target", "_blank");
        }
    });
};


/// <summary>
/// 
/// </summary>
Machinata.UI.userSelectElement = function (element) {
    // Create a user selection in the document for the given jquery element
    var body = document.body, range, sel;
    var el = element[0];
    if (document.createRange && window.getSelection) {
        range = document.createRange();
        sel = window.getSelection();
        sel.removeAllRanges();
        try {
            range.selectNodeContents(el);
            sel.addRange(range);
        } catch (e) {
            range.selectNode(el);
            sel.addRange(range);
        }
    } else if (body.createTextRange) {
        range = body.createTextRange();
        range.moveToElementText(el);
        range.select();
    }
};

/// <summary>
/// 
/// </summary>
Machinata.UI.copyElementToClipboardInTemporaryDOM = function (element, showSuccess) {
    var tempContainer = $("<div style='position: absolute; top: 0px; left:0px; pointer-events:none; opacity: 0;'></div>");
    tempContainer.append(element);
    $("body").append(tempContainer);
    Machinata.UI.copyElementToClipboard(element, showSuccess);
    setTimeout(function () {
        tempContainer.remove();
    }, 100);
};

/// <summary>
/// 
/// </summary>
Machinata.UI.copyElementToClipboard = function (element,showSuccess) {
    Machinata.UI.userSelectElement(element);
    document.execCommand("copy");

    if (showSuccess == true) {
        setTimeout(function () {
            var diag = Machinata.messageDialog("{text.copy-success-title=Copied}", "{text.copy-text-success-message}");
            diag.show();
        }, 100);
    }
};

/// <summary>
/// 
/// </summary>
Machinata.UI.copyTextToClipboard = function (text, showSuccess, successMessage) {
    // Legacy helper method
    function legacyCopyTextToClipboard(text) {
        // Init copy element
        var textArea = document.createElement("textarea");
        textArea.value = text;
        textArea.style.top = "0";
        textArea.style.left = "0";
        textArea.style.position = "fixed";
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
        // Perform copy
        try {
            var successful = document.execCommand('copy');
            if (successful) {
                if (showSuccess == true) {
                    setTimeout(function () {
                        var diag = Machinata.messageDialog("{text.copy-success-title=Copied}", successMessage || "{text.copy-text-success-message}");
                        diag.elem.find("p").css("word-break", "break-word");
                        diag.show();
                    }, 100);
                }
            }
        } catch (err) {
            console.warn("Machinata.UI.copyTextToClipboard: unable to copy to clipboard using legacy method", err);
        }
        // Cleanup
        document.body.removeChild(textArea);
    }
    // Fallback to legacy method?
    if (!navigator.clipboard) {
        legacyCopyTextToClipboard(text);
        return;
    }
    // Perform copy
    navigator.clipboard.writeText(text).then(function () {
        // Success
        if (showSuccess == true) {
            setTimeout(function () {
                var diag = Machinata.messageDialog("{text.copy-success-title=Copied}", successMessage || "{text.copy-text-success-message}");
                diag.elem.find("p").css("word-break", "break-word");
                diag.show();
            }, 100);
        }
    }, function (err) {
        // Error
        console.warn("Machinata.UI.copyTextToClipboard: unable to copy to clipboard using navigator.clipboard", err);
    });

};

/// <summary>
/// 
/// </summary>
Machinata.UI.enableAutoScroll = function () {
    if (Machinata.queryParameter("auto-scroll") != "") {
        Machinata.debug("Will auto-scroll window...");
        var y = 0;
        var scrollSpeed = 25;
        var scrollAmount = 1;
        var beginDelay = 0;
        var reloadURL = window.location.href;
        if (Machinata.queryParameter("auto-scroll-delay") != "") beginDelay = parseInt(Machinata.queryParameter("auto-scroll-delay"))
        setTimeout(function () {
            var scrollTimer = setInterval(function () {
                y += scrollAmount;
                if (y >= $(document).height() - $(window).height()) {
                    clearInterval(scrollTimer);
                    if (Machinata.queryParameter("auto-reload-after-scroll") == "true") {
                        Machinata.reload();
                        //window.location = reloadURL + "&uid=" + Math.random();
                    } else {
                        y = 0;
                    }
                }
                window.scrollTo(0, y);
            }, scrollSpeed);
        }, beginDelay);
    }
};

/// <summary>
/// 
/// </summary>
Machinata.UI.enableAutoReload = function () {
    if (Machinata.queryParameter("auto-reload") != "" && Machinata.queryParameter("auto-scroll") == "") {
        Machinata.debug("Will auto-reload window...");
        $("body").addClass("auto-reload-enabled");
        var autoReloadStr = Machinata.queryParameter("auto-reload");
        var autoReloadMS = parseInt(autoReloadStr);
        setTimeout(function () {
            window.location = window.location;
        }, autoReloadMS);
    }
};


/// <summary>
/// Automatically updates a body class when the user scrolls from the top of the page
/// A tolerance (in px) can be specified
/// </summary>
Machinata.UI.enableScrollChangeMonitor = function (tolerance) {
    if (tolerance == null) tolerance = 5;
    var bodyElem = $("body");
    function update() {
        if (window.scrollY > tolerance) {
            bodyElem.removeClass("scroll-top");
            bodyElem.addClass("scroll-middle");
        } else {
            bodyElem.addClass("scroll-top");
            bodyElem.removeClass("scroll-middle");
        }
    }
    $(window).scroll(function () {
        update();
    });
    update();
};


/// <summary>
///  Create a horizontal scrolling timeline based on timestamps in it's items attributes
/// </summary>
Machinata.UI.createTimeline = function (elements, pixelsPerMonth, endPaddingMonth, dateFormat) {
    elements.each(function () {

        // Cache some selectors
        var timeline = $(this);
        var view = timeline.find(".view");
        var items = timeline.find(".items .item");
        var contents = timeline.find(".contents");
        var axisMonths = timeline.find(".axis-months");

        // Init size
        if (pixelsPerMonth == null) pixelsPerMonth = 80;
        var pixelsPerDay = pixelsPerMonth / 30;
        var pixelsPerHour = pixelsPerDay / 24;
        var pixelsPerMinute = pixelsPerHour / 60;
        var pixelsPerSecond = pixelsPerMinute / 60;
        var pixelsPerTick = pixelsPerSecond / 1000;
        var minDate = null;
        var maxDate = null;
        if (endPaddingMonth == null) endPaddingMonth = 1;
        if (dateFormat == null) dateFormat = Machinata.convertDotNetTimeFormatToDatepickerFormat(Machinata.DEFAULT_DATE_FORMAT_JAVASCRIPT_UI);

        // Pre-process
        Machinata.debug("Machinata.UI.createTimeline");
        Machinata.debug("  Pre-processing items...");
        items.each(function () {
            // Init items start/end
            var item = $(this);
            var timeStart = Machinata.dateFromString(item.attr("data-start"), dateFormat);
            var timeEnd = Machinata.dateFromString(item.attr("data-end"), dateFormat);
            if (timeStart < minDate || minDate == null) minDate = timeStart;
            if (timeEnd > maxDate || maxDate == null) maxDate = timeEnd;
            // Convert to ticks
            item.data("ticks-start", timeStart.getTime());
            item.data("ticks-end", timeEnd.getTime());
            // Debug
            Machinata.debug("  " + "Item:");
            Machinata.debug("  " + "  " + item.attr("data-title"));
            Machinata.debug("  " + "  " + item.attr("data-start") + ": " + timeStart);
            Machinata.debug("  " + "  " + item.attr("data-end") + ": " + timeEnd);
        });

        // Validate
        if (minDate == null) minDate = new Date();
        if (maxDate == null) maxDate = new Date();


        // Trim timeline to start of the first month, and end of last month
        var timelineStart = new Date(minDate.getFullYear(), minDate.getMonth(), 1, 0, 0, 0, 0);
        var timelineEnd = new Date(maxDate.getFullYear(), maxDate.getMonth() + 1 + endPaddingMonth, 1, 0, 0, 0, 0);
        timelineEnd = new Date(timelineEnd.getTime() - 1000); // subtract a second to get previous month (since we added a hole)
        var timelineTicksStart = timelineStart.getTime();
        var timelineTicksEnd = timelineEnd.getTime();
        var timelineTicksSpan = timelineTicksEnd - timelineTicksStart;
        var timelinePixels = timelineTicksSpan * pixelsPerTick;
        var months = Machinata.Time.getMonths(timelineStart, timelineEnd);

        // Debug
        Machinata.debug("  " + "Configuration:");
        Machinata.debug("  " + "  " + "pixelsPerMonth: " + pixelsPerMonth);
        Machinata.debug("  " + "  " + "pixelsPerDay: " + pixelsPerDay);
        Machinata.debug("  " + "  " + "pixelsPerHour: " + pixelsPerHour);
        Machinata.debug("  " + "  " + "pixelsPerMinute: " + pixelsPerMinute);
        Machinata.debug("  " + "  " + "pixelsPerSecond: " + pixelsPerSecond);
        Machinata.debug("  " + "  " + "pixelsPerTick: " + pixelsPerTick);
        Machinata.debug("  " + "  " + "minDate: " + minDate);
        Machinata.debug("  " + "  " + "maxDate: " + maxDate);
        Machinata.debug("  " + "  " + "months: " + months);
        Machinata.debug("  " + "  " + "timelineStart: " + timelineStart);
        Machinata.debug("  " + "  " + "timelineEnd: " + timelineEnd);
        Machinata.debug("  " + "  " + "timelineTicksStart: " + timelineTicksStart);
        Machinata.debug("  " + "  " + "timelineTicksEnd: " + timelineTicksEnd);
        Machinata.debug("  " + "  " + "timelineTicksSpan: " + timelineTicksSpan);
        Machinata.debug("  " + "  " + "timelinePixels: " + timelinePixels);

        // Process each axis
        axisMonths.each(function () {
            var axisElem = $(this);
            // Loop all months
            for (var i = 0; i < months.length; i++) {
                // Get start end time
                var timeStart = months[i];
                var timeEnd = new Date(timeStart.getFullYear(), timeStart.getMonth() + 1, 1, 0, 0, 0, 0);
                // Create tick elem
                var tickElem = $("<div class='tick time-positioned'></div>");
                tickElem.data("ticks-start", timeStart.getTime());
                tickElem.data("ticks-end", timeEnd.getTime());
                // Addition tick data
                if (axisElem.hasClass("labels")) {
                    var label = Machinata.Time.getShortMonth(timeStart);
                    if (timeStart.getMonth() == 0) label += " " + timeStart.getFullYear();
                    tickElem.append($("<span></span>").text(label));
                }
                axisElem.append(tickElem);
            }
        });

        // Position each positioned element
        // Each such element is positioned in percent, meaning the entire timeline
        // is responsive out-of-the-box
        var positionedElems = timeline.find(".time-positioned");
        contents.css("width", timelinePixels + "px");
        positionedElems.each(function () {
            var elem = $(this);
            var percentStart = (elem.data("ticks-start") - timelineTicksStart) / timelineTicksSpan;
            var percentEnd = (elem.data("ticks-end") - timelineTicksStart) / timelineTicksSpan;
            elem.css("left", (percentStart) * 100.0 + "%");
            elem.css("width", (percentEnd - percentStart) * 100.0 + "%");
            elem.data("original-width", (percentEnd - percentStart) * 100.0 + "%");
        });

        // Extra interaction for items that got trimmed
        var expandableElems = timeline.find(".item.expandable");
        expandableElems.hover(function () {
            var title = $(this).find(".title").text();
            var fontSize = 4;
            var titleWidth = title.length * fontSize;
            if ($(this).width() < titleWidth) {
                $(this).css("width", "unset").data("expanded", true);
            }
        }, function () {
            if ($(this).data("expanded") == true) {
                $(this).css("width", $(this).data("original-width")).data("expanded", false);
            }
        });

        // Drag scroll
        if ($("body").hasClass("not-touch-enabled")) {
            var curDown = false;
            var curXPos = 0;
            timeline.addClass("grab-to-scroll");
            timeline.mousemove(function (event) {
                if (curDown == true) {
                    var parentOffset = timeline.parent().offset();
                    var newX = (curXPos - (event.pageX - parentOffset.left));
                    view.scrollLeft(newX);
                }
            });
            timeline.mousedown(function (event) {
                var parentOffset = timeline.parent().offset();
                curXPos = view.scrollLeft() + (event.pageX - parentOffset.left);
                curDown = true;
                timeline.addClass("grabbing");
            });
            timeline.mouseup(function () {
                curDown = false;
                timeline.removeClass("grabbing");
            });
            $(window).mouseup(function () {
                curDown = false;
                timeline.removeClass("grabbing");
            });
        }
    });
};


/// <summary>
/// 
/// </summary>
Machinata.UI.bindPageDownElements = function (elements, opts) {
    if (opts == null) opts = {};
    if (opts.nextTargetSelector == null) opts.nextTargetSelector = null;
    if (opts.closestTargetSelector == null) opts.closestTargetSelector = opts.nextTargetSelector;
    if (opts.duration == null) opts.duration = 1200;

    elements.each(function () {
        var elem = $(this);
        elem.click(function () {
            var resolveMethod = "direct via opts.target";
            var nextTargetSelector = opts.nextTargetSelector;
            if (nextTargetSelector == null) nextTargetSelector = elem.attr("data-page-down-next-target");
            var closestTargetSelector = opts.closestTargetSelector;
            if (closestTargetSelector == null) closestTargetSelector = elem.attr("data-page-down-closest-target");
            // Resolve the target
            var target = opts.target;
            if ((target == null || target.length == 0) && nextTargetSelector != null && closestTargetSelector != null) {
                target = elem.closest(closestTargetSelector).nextAll(nextTargetSelector).first();
                resolveMethod = "tree up via closestTargetSelector '" + closestTargetSelector + "' then next via nextTargetSelector '" + nextTargetSelector +"'";
            } else if ((target == null || target.length == 0) && nextTargetSelector) {
                target = elem.nextAll(nextTargetSelector).first();
                resolveMethod = "next via nextTargetSelector '" + nextTargetSelector + "'";
            }
            //else if (target == null || target.length == 0) Machinata.debug("Machinata.UI.bindPageDownElements: warning could not find target");
            // Warning
            if (target == null || target.length == 0) Machinata.debug("Machinata.UI.bindPageDownElements: warning could not find target for options:", { resolveMethod: resolveMethod, nextTargetSelector: nextTargetSelector, closestTargetSelector: closestTargetSelector, opts: opts });
            // Scroll to
            Machinata.UI.scrollTo(target, opts.duration);
        });
    });
};

/// <summary>
/// 
/// </summary>
Machinata.UI.createToggleSlideshow = function (elements, opts) {
    // Init
    if (opts == null) opts = {};
    if (opts.slideDuration == null) opts.slideDuration = 3000;
    if (opts.restartPause == null) opts.restartPause = 6000;
    if (opts.fadeSpeed == null) opts.fadeSpeed = 300;
    if (opts.toggleButtonHtml == null) opts.toggleButtonHtml = "<a class='toggle-button'></a>";

    elements.each(function () {
        // Init
        var elem = $(this);
        var items = elem.find(".toggle-item");
        var buttonContainer = elem.find(".toggle-buttons");
        var timer = null;
        // Helper functions
        function selectNext() {
            var selected = buttonContainer.find(".toggle-button.selected");
            var next = selected.next();
            if (next.length == 0) next = buttonContainer.find(".toggle-button").first();
            next.trigger("select");
        }
        function restartTimer() {
            if (timer != null) clearInterval(timer);
            timer = setInterval(function () {
                selectNext();
            }, opts.slideDuration);
        }
        function restartTimerAfterPause() {
            if (timer != null) clearInterval(timer);
            setTimeout(function () {
                restartTimer();
            }, opts.restartPause);
        }
        // Create all buttons
        items.each(function () {
            var item = $(this);
            var radio = $(opts.toggleButtonHtml);
            radio.on("click select", function () {
                if ($(this).hasClass("selected")) return;
                items.not(item).fadeTo(opts.fadeSpeed, 0.0);
                item.fadeTo(0, 0.0).delay(opts.fadeSpeed).show().fadeTo(opts.fadeSpeed, 1.0);
                buttonContainer.find(".toggle-button").not(radio).removeClass("selected");
                radio.addClass("selected");
            });
            radio.on("click", function () {
                restartTimerAfterPause();
            });
            buttonContainer.append(radio);
        });
        // Initial state
        items.first().show();
        buttonContainer.find(".toggle-button").first().addClass("selected");
        restartTimer();
    });
};

/// <summary>
/// 
/// </summary>
Machinata.UI.calculateIfElementsOverlap = function (elem1, elem2, elementsAreSVGGroups) {
    // Via https://gist.github.com/jtsternberg/c272d7de5b967cec2d3d

    // Elem 1 data
    var d1_offset = elem1.offset();
    var d1_height = elem1.outerHeight(true);
    var d1_width = elem1.outerWidth(true);
    if (elementsAreSVGGroups == true) {
        d1_height = elem1[0].getBoundingClientRect().height;
        d1_width = elem1[0].getBoundingClientRect().width;
    }
    var d1_distance_from_top = d1_offset.top + d1_height;
    var d1_distance_from_left = d1_offset.left + d1_width;

    // Elem 2 data
    var d2_offset = elem2.offset();
    var d2_height = elem2.outerHeight(true);
    var d2_width = elem2.outerWidth(true);
    if (elementsAreSVGGroups == true) {
        d2_height = elem2[0].getBoundingClientRect().height;
        d2_width = elem2[0].getBoundingClientRect().width;
    }
    var d2_distance_from_top = d2_offset.top + d2_height;
    var d2_distance_from_left = d2_offset.left + d2_width;

    var not_colliding = (d1_distance_from_top < d2_offset.top || d1_offset.top > d2_distance_from_top || d1_distance_from_left < d2_offset.left || d1_offset.left > d2_distance_from_left);

    // Return whether it IS colliding
    return !not_colliding;
};


/// <summary>
/// 
/// </summary>
Machinata.UI.bindPageScrolledEvent = function (tolerance, classes) {
    // Init
    if (tolerance == null) tolerance = 10;
    if (classes == null) classes = "page-scrolled";
    // Helper functions
    var bodyElem = $("body");
    function updateHeaderState() {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > tolerance) {
            bodyElem.addClass(classes);
        } else {
            bodyElem.removeClass(classes);
        }
    }
    $(window).scroll(function () {
        updateHeaderState();
    });
    updateHeaderState();
};

/// <summary>
///
/// </summary>
/// <hidden/>
Machinata.UI._doWithDelayTimers = {};

/// <summary>
/// Performs an action after a delay, but only ever once at a time. For each
/// time the method is called, the timer on the key will reset.
/// This is useful, for example, for updating a UI after a text input keystroke.
/// </summary>
/// <param  name="key">unique key for the timer to use</param>
/// <param  name="delay"> delay in milliseconds</param>
/// <param  name="action"> function to perform</param>
Machinata.UI.doWithDelay = function (key, delay, action) {
    var currentTimer = Machinata.UI._doWithDelayTimers[key];
    if (currentTimer != null) clearTimeout(currentTimer);
    currentTimer = setTimeout(function () {
        action();
    }, delay);
    Machinata.UI._doWithDelayTimers[key] = currentTimer;
};





/// <summary>
/// Creates a simple debug panel with series of key value elements.
/// keyvalue: JSON key value object
/// </summary>
Machinata.UI.createDebugPanel = function (title, keyvalue) {
    var debugElem = $("<div class='ui-debug-panel bb-debug-panel'></div>");
    debugElem.append($("<tr class='keyval head'></tr>")
        .append($("<td class='title' colspan='2'></td>").text(title))
    );
    function addKeyValue(key, value) {
        var isLink = false;
        if (value != null && typeof value == "string") {
            if (value.startsWith("http://") || value.startsWith("https://")) {
                isLink = true;
            }
            if (value.startsWith("/") && value.length > 5 && (value[value.length - 5] == '.')) {
                isLink = true;
            }
        }
        var debugItem = $("<div class='keyval'></div>").append($("<div class='key'></div>").text(key));
        if (Array.isArray(value)) {
            $.each(value, function (index, val) {
                debugItem.append($("<div class='val'></div>").text(val));
            });
        } else {
            debugItem.append($("<div class='val'></div>").text(value));
        }
        debugElem.append(debugItem);
        if (isLink == true) {
            debugItem.find(".val").text("").append($("<a></a>").text(value).attr("href", value));
        }
    }
    $.each(keyvalue, function (key, value) {
        addKeyValue(key, value);
    });
    return debugElem;
};



/// <summary>
/// </summary>
Machinata.UI.createSVGArc = function (elements, opts) {
    throw "not yet fully implemented";

    if (opts == null) opts = {};
    if (opts.value == null) opts.value = null;

    function getCoordFromDegrees(angle, radius) {
        const svgSize = 100;
        const x = Math.cos(angle * Math.PI / 180);
        const y = Math.sin(angle * Math.PI / 180);
        const coordX = x * radius + svgSize / 2;
        const coordY = y * -radius + svgSize / 2;
        return coordX + ", " + coordY;
    }


    elements.each(function () {
        let degrees = val * 360;
        let rotation = -90 + degrees;
        let rOuter = 50;
        let strokeWidth = 0.1;
        let rInner = rOuter - (100 * strokeWidth);
        let xAxisRotation = 0;
        let largeArcFlag = 0;
        if (degrees > 180) largeArcFlag = 1;
        let path = `
                M 100 50
                A ${rOuter} ${rOuter} ${xAxisRotation} ${largeArcFlag} 0 ${getCoordFromDegrees(degrees, rOuter)}
                L ${getCoordFromDegrees(degrees, rInner)}
                A ${rInner} ${rInner} ${xAxisRotation} ${largeArcFlag} 1 ${100 - (rOuter - rInner)} 50`;

        var svgElem = $(`<svg class='donut' viewBox="0 0 100 100"></svg>`);
        var pathElem = $(`<path style="transform-origin: center;" transform="rotate(${rotation})" d="${path}"/>`);
        
    });
    
};


/// <summary>
/// Creates a super high-performance SVG donut with a single segment.
/// You must provide the attribute data-value="[dot percent]" to specify
/// the donut segment size.
/// </summary>
Machinata.UI.microdonut = function (elements, opts) {
    // Inspired from from https://medium.com/@heyoka/scratch-made-svg-donut-pie-charts-in-html5-2c587e935d72

    if (opts == null) opts = {};
    if (opts.value == null) opts.value = null;
    if (opts.updateIfExists == null) opts.updateIfExists = false;
    if (opts.guide == null) opts.guide = false;
    if (opts.guideIsBehindValue == null) opts.guideIsBehindValue = false;
    if (opts.animationIncrement == null) opts.animationIncrement = 0.05;
    if (opts.animate == null) {
        opts.animate = false;
    }
    if (opts.animate == true) {
        opts.updateIfExists = true;
    }
    // Helper function to create or update the donut
    function createOrUpdateMicrodonut(elem,p) {
        var pp = Math.ceil(p * 100.0);
        var ppd = pp;
        //if (ppd == 100) ppd = 99;
        var da1 = ppd;
        var da2 = 100 - da1;
        var dao = 25; // default offset starting at top
        // Update existing?
        var insertByUpdating = false;
        var existingElem = null;
        if (opts.updateIfExists == true) {
            existingElem = elem.find(".ma-microdonut");
            if (existingElem.length != 0) insertByUpdating = true;
        }
        if (insertByUpdating == true && existingElem != null) {
            // Update existing
            var segmentElem = existingElem.find(".segment");
            segmentElem.attr("stroke-dasharray", da1 + " " + da2);
            segmentElem.attr("stroke-dashoffset", dao);
            existingElem.attr("data-value", p);
        } else {
            // Generate SVG code
            var svgcode =
                '<svg class="ma-microdonut" viewBox="0 0 42 42" data-value="'+p+'">' +
                '<circle class="hole" cx="21" cy="21" r="15.91549430918954"></circle>' +
                '<circle class="ring" cx="21" cy="21" r="15.91549430918954"></circle>' +
                ((opts.guide == true && opts.guideIsBehindValue == true) ? '<circle class="guide" cx="21" cy="21" r="15.91549430918954"></circle>' : '') +
                '<circle class="segment" cx="21" cy="21" r="15.91549430918954" stroke-dasharray="' + da1 + ' ' + da2 + '" stroke-dashoffset="' + dao + '"></circle>' +
                ((opts.guide == true && opts.guideIsBehindValue == false) ? '<circle class="guide" cx="21" cy="21" r="15.91549430918954"></circle>':'') +
                '</svg>';
            elem.append($(svgcode));
        }
    }

    // Process each
    elements.each(function () {
        // Get the value for the donut
        var elem = $(this);
        var val = opts.value;
        if(val == null) val = elem.attr("data-value");
        var p = parseFloat(val);
        // Branch on animation - animation is much more complex
        if (opts.animate == true) {
            var existingElem = elem.find(".ma-microdonut");
            var startingValue = 0.0;
            if (existingElem.length != 0) {
                startingValue = parseFloat(existingElem.attr("data-value"));
            }
            var endingValue = p;
            var currentValue = startingValue;
            var direction = +1;
            if (endingValue < startingValue) direction = -1;
            function animateMicrodonut() {
                currentValue += (opts.animationIncrement * direction);
                if (direction == +1 && currentValue > endingValue) currentValue = endingValue;
                if (direction == -1 && currentValue < endingValue) currentValue = endingValue;
                //console.log("s:", startingValue, "e:", endingValue, "d:", direction, "c:", currentValue);
                createOrUpdateMicrodonut(elem, currentValue);
                if (direction == +1 && currentValue < endingValue) {
                    requestAnimationFrame(animateMicrodonut);
                } else if (direction == -1 && currentValue > endingValue) {
                    requestAnimationFrame(animateMicrodonut);
                }
            };
            createOrUpdateMicrodonut(elem, startingValue);
            requestAnimationFrame(animateMicrodonut);
        } else {
            // No animation - just create or update
            createOrUpdateMicrodonut(elem, p);
        }
    });
};


/// <summary>
/// Creates a super high-performance bar chart using divs.
/// </summary>
Machinata.UI.microBar = function (val, title) {
    var p = val * 100;
    var barElem = $("<div class='microbar'><div class='bar'></div></div>");
    barElem.find(".bar").css("width", (p) + "%");
    barElem.attr("title", title);
    return barElem;
};




/// <summary>
/// Creates a custom tooltip UI for all tooltip events
/// </summary>
/// <example>
/// ## Example CSS
/// ```
/// .ui-tooltip {
///     padding: $padding-half;
///     position: absolute;
///     z-index: 9999;
///     max-width: 300px;
///     transition: none;
///     box-shadow: none;
///     border: none !important;
/// }
/// .ui-tooltip, 
/// .ui-tooltip .arrow:after {
///     background: $solid-color;
/// }
/// .ui-tooltip * {
///     color: $solid-text-color;
/// }
/// .ui-tooltip .arrow {
///     width: 70px;
///     height: 16px;
///     overflow: hidden;
///     position: absolute;
///     left: 50%;
///     margin-left: -35px;
///     bottom: -16px;
/// }
/// .ui-tooltip .arrow.top {
///     top: -16px;
///     bottom: auto;
/// }
/// .ui-tooltip .arrow.left {
///     left: 20%;
/// }
/// .ui-tooltip .arrow:after {
///     content: "";
///     position: absolute;
///     left: 20px;
///     top: -20px;
///     width: 25px;
///     height: 25px;
///     transform: rotate(45deg);
/// }
/// .ui-tooltip .arrow.top:after {
///     bottom: -20px;
///     top: auto;
/// }
/// .ui-helper-hidden-accessible {
///     display: none;
/// }
/// ```
/// </example>
Machinata.UI.tooltips = function (opts, elements) {
    // Init
    if (elements == null) elements = $(document);
    if (opts == null) opts = {};
    if (opts.my == null) opts.my = "center bottom-30";
    if (opts.at == null) opts.at = "center top";
    if (opts.disableForTouchDevices) opts.disableForTouchDevices = false;
    if (opts.disableForMobile) opts.disableForMobile = false;
    if (opts.disableForTablet) opts.disableForTablet = false;

    // Disable?
    if (opts.disableForTouchDevices == true && Machinata.Device.isTouchEnabled() == true) {
        return;
    }
    if (opts.disableForMobile == true && Machinata.Responsive.isMobileLayout() == true) {
        return;
    }
    if (opts.disableForTablet == true && Machinata.Responsive.isTabletLayout() == true) {
        return;
    }

    // Call jQuery UI tooltip plugin
    elements.tooltip({
        track: true,
        show: {
            delay: 0,
            effect: "none"
        },
        hide: {
            delay: 0,
            effect: "none"
        },
        position: {
            my: opts.my,
            at: opts.at,
            using: function (position, feedback) {
                $(this).css(position);
                $("<div>").addClass("arrow").addClass(feedback.vertical).appendTo(this);
            }
        }
    });
}




/// <summary>
/// Creates a interactive table-of-contents
/// </summary>
Machinata.UI.createTOC = function (tocElem, opts) {
    // Init
    var defaults = {
        tocEntryHTML: "<li><a></a></li>",
        tocElementsSelector: "h1,h2,h3,h4,.bb-toc-trigger,.bb-virtual-toc-trigger",
        tocElementsClassFilter: null,
        scrollDuration: 800,
        scrollOffset: -100
    };
    $.extend(defaults, opts);

    // Find triggers
    var tocTriggers = $(defaults.tocElementsSelector);
    if (defaults.tocElementsClassFilter) tocTriggers = tocTriggers.filter("." + defaults.tocElementsClassFilter + ",.bb-toc-trigger,.bb-virtual-toc-trigger")
    tocTriggers = tocTriggers.filter(":visible,.bb-virtual-toc-trigger");

    // Init
    var tocEntriesContainer = tocElem.find("ul");
    var scrollElem = $(window);

    // Build
    tocTriggers.each(function () {
        // Init
        var triggerElem = $(this);
        var actualTrigger = triggerElem;
        if (triggerElem.attr("data-toc-trigger-selector") != null) {
            actualTrigger = $(triggerElem.attr("data-toc-trigger-selector"));
            triggerElem.data("actual-trigger", actualTrigger);
        }
        Machinata.UI.autoUIDElem(actualTrigger);
        var tocEntryElem = $(defaults.tocEntryHTML)
            .addClass("autogen")
            .addClass("bb-toc-entry")
            .data("toc-trigger-elem", actualTrigger)
            .attr("data-toc-id", actualTrigger.attr("id"));
        triggerElem.data("toc-elem", tocEntryElem);
        if (triggerElem.attr("data-toc-level") != null) tocEntryElem.addClass("level-" + triggerElem.attr("data-toc-level"));
        if (triggerElem.attr("data-toc-url") != null) tocEntryElem.data("toc-url", triggerElem.attr("data-toc-url"));
        // Linked trigger?
       
        // Get title
        var title = triggerElem.attr("data-toc-title");
        if (title == null || title == "") title = triggerElem.text();
        // Interaction
        tocEntryElem.click(function () {
            var targetURL = $(this).data("toc-url");
            if (targetURL != null) {
                Machinata.goToPage(targetURL);
            } else {
                var targetElem = $(this).data("toc-trigger-elem");
                Machinata.UI.scrollTo(targetElem, defaults.scrollDuration, { offset: defaults.scrollOffset });
            }
        });
        // Register
        tocEntryElem.find("a").text(title);
        tocEntriesContainer.append(tocEntryElem);
    });
    var tocEntries = tocEntriesContainer.find(".bb-toc-entry");
    if (tocEntries.length > 0) tocElem.addClass("has-entries");

    // Helper function update toc selection
    function updateTOCUI() {
        // Init
        var minDist = Number.MAX_SAFE_INTEGER;
        var scrollTop = scrollElem.scrollTop();
        var scrollMiddle = scrollTop + (scrollElem.height() * 0.5);
        // Find match
        var matchingEntry = null;
        tocTriggers.each(function () {
            var triggerElem = $(this);
            if (triggerElem.hasClass("no-toc-trigger")) return;
            // Switch to actual trigger?
            var actualTrigger = triggerElem;
            if (triggerElem.data("actual-trigger") != null) actualTrigger = triggerElem.data("actual-trigger");
            // Calc dist to trigger...
            //TODO: probably not best algo for this
            var distToTrigger = Math.abs(scrollTop - actualTrigger.offset().top);
            if (distToTrigger < minDist) {
                minDist = distToTrigger;
                matchingEntry = triggerElem;
            }
        });
        if (matchingEntry != null) {
            var tocElem = matchingEntry.data("toc-elem");
            tocElem.addClass("selected");
            tocEntries.not(tocElem).removeClass("selected");
        } else {
            tocEntries.removeClass("selected");
        }
    }

    // Bind scroll for auto-selection
    scrollElem.scroll(function () {
        // Throttle updates
        //TODO
        updateTOCUI();
    });

    // Initial state
    updateTOCUI();
};



/// <summary>
/// 
/// </summary>
Machinata.UI.mirrorInputValueFromMaster = function (masterElem, mirrorElems) {
    mirrorElems.on("change", function () {
        $(this).data("has-custom-value", true);
    });
    masterElem.on("change", function () {
        mirrorElems.each(function () {
            if ($(this).data("has-custom-value") != true) {
                $(this).val(masterElem.val());
            }
        });
        
    });
};


/// <summary>
/// 
/// </summary>
Machinata.UI.createSwatchPicker = function (colorSystems, opts) {
    if (colorSystems == null) colorSystems = "*";
    if (opts == null) opts = {};
    if (opts.dropletSize == null) opts.dropletSize = 20;
    if (opts.swatchSize == null) opts.swatchSize = 300;
    var swatches = [];
    var includeRAL = false;
    var includeNCS = false;
    var includePantone = false;
    var colorSystemsArray = colorSystems.split(",");
    for (var i = 0; i < colorSystemsArray.length; i++) {
        if (colorSystemsArray[i] == "ral" || colorSystemsArray[i] == "RAL") includeRAL = true;
        if (colorSystemsArray[i] == "ncs" || colorSystemsArray[i] == "NCS") includeNCS = true;
        if (colorSystemsArray[i] == "pantone" || colorSystemsArray[i] == "Pantone") includePantone = true;
    }

    if (includeRAL || colorSystems == "*") {
        // From https://gist.github.com/hertsch/5876462
        // Converted to JSON via https://www.csvjson.com/csv2json
        var ralSwatches = [{ "code": "RAL 1000", "rgb": "190-189-127", "hex": "#BEBD7F", "de": "Grünbeige", "en": "Green beige", "fr": "Beige vert", "es": "Beige verdoso", "it": "Beige verdastro", "nl": "Groenbeige" }, { "code": "RAL 1001", "rgb": "194-176-120", "hex": "#C2B078", "de": "Beige", "en": "Beige", "fr": "Beige", "es": "Beige", "it": "Beige", "nl": "Beige" }, { "code": "RAL 1002", "rgb": "198-166-100", "hex": "#C6A664", "de": "Sandgelb", "en": "Sand yellow", "fr": "Jaune sable", "es": "Amarillo arena", "it": "Giallo sabbia", "nl": "Zandgeel" }, { "code": "RAL 1003", "rgb": "229-190-001", "hex": "#E5BE01", "de": "Signalgelb", "en": "Signal yellow", "fr": "Jaune de sécurité", "es": "Amarillo señales", "it": "Giallo segnale", "nl": "Signaalgeel" }, { "code": "RAL 1004", "rgb": "205-164-052", "hex": "#CDA434", "de": "Goldgelb", "en": "Golden yellow", "fr": "Jaune or", "es": "Amarillo oro", "it": "Giallo oro", "nl": "Goudgeel" }, { "code": "RAL 1005", "rgb": "169-131-007", "hex": "#A98307", "de": "Honiggelb", "en": "Honey yellow", "fr": "Jaune miel", "es": "Amarillo miel", "it": "Giallo miele", "nl": "Honinggeel" }, { "code": "RAL 1006", "rgb": "228-160-016", "hex": "#E4A010", "de": "Maisgelb", "en": "Maize yellow", "fr": "Jaune maïs", "es": "Amarillo maiz", "it": "Giallo polenta", "nl": "Maisgeel" }, { "code": "RAL 1007", "rgb": "220-156-000", "hex": "#DC9D00", "de": "Narzissengelb", "en": "Daffodil yellow", "fr": "Jaune narcisse", "es": "Amarillo narciso", "it": "Giallo narciso", "nl": "Narcissengeel" }, { "code": "RAL 1011", "rgb": "138-102-066", "hex": "#8A6642", "de": "Braunbeige", "en": "Brown beige", "fr": "Beige brun", "es": "Beige pardo", "it": "Beige marrone", "nl": "Bruinbeige" }, { "code": "RAL 1012", "rgb": "199-180-070", "hex": "#C7B446", "de": "Zitronengelb", "en": "Lemon yellow", "fr": "Jaune citron", "es": "Amarillo limón", "it": "Giallo limone", "nl": "Citroengeel" }, { "code": "RAL 1013", "rgb": "234-230-202", "hex": "#EAE6CA", "de": "Perlweiß", "en": "Oyster white", "fr": "Blanc perlé", "es": "Blanco perla", "it": "Bianco perla", "nl": "Parelwit" }, { "code": "RAL 1014", "rgb": "225-204-079", "hex": "#E1CC4F", "de": "Elfenbein", "en": "Ivory", "fr": "Ivoire", "es": "Marfil", "it": "Avorio", "nl": "Ivoorkleurig" }, { "code": "RAL 1015", "rgb": "230-214-144", "hex": "#E6D690", "de": "Hellelfenbein", "en": "Light ivory", "fr": "Ivoire clair", "es": "Marfil claro", "it": "Avorio chiaro", "nl": "Licht ivoorkleurig" }, { "code": "RAL 1016", "rgb": "237-255-033", "hex": "#EDFF21", "de": "Schwefelgelb", "en": "Sulfur yellow", "fr": "Jaune soufre", "es": "Amarillo azufre", "it": "Giallo zolfo", "nl": "Zwavelgeel" }, { "code": "RAL 1017", "rgb": "245-208-051", "hex": "#F5D033", "de": "Safrangelb", "en": "Saffron yellow", "fr": "Jaune safran", "es": "Amarillo azafrán", "it": "Giallo zafferano", "nl": "Saffraangeel" }, { "code": "RAL 1018", "rgb": "248-243-053", "hex": "#F8F32B", "de": "Zinkgelb", "en": "Zinc yellow", "fr": "Jaune zinc", "es": "Amarillo de zinc", "it": "Giallo zinco", "nl": "Zinkgeel" }, { "code": "RAL 1019", "rgb": "158-151-100", "hex": "#9E9764", "de": "Graubeige", "en": "Grey beige", "fr": "Beige gris", "es": "Beige agrisado", "it": "Beige grigiastro", "nl": "Grijsbeige" }, { "code": "RAL 1020", "rgb": "153-153-080", "hex": "#999950", "de": "Olivgelb", "en": "Olive yellow", "fr": "Jaune olive", "es": "Amarillo oliva", "it": "Giallo olivastro", "nl": "Olijfgeel" }, { "code": "RAL 1021", "rgb": "243-218-011", "hex": "#F3DA0B", "de": "Rapsgelb", "en": "Rape yellow", "fr": "Jaune colza", "es": "Amarillo colza", "it": "Giallo navone", "nl": "Koolzaadgeel" }, { "code": "RAL 1023", "rgb": "250-210-001", "hex": "#FAD201", "de": "Verkehrsgelb", "en": "Traffic yellow", "fr": "Jaune signalisation", "es": "Amarillo tráfico", "it": "Giallo traffico", "nl": "Verkeersgeel" }, { "code": "RAL 1024", "rgb": "174-160-075", "hex": "#AEA04B", "de": "Ockergelb", "en": "Ochre yellow", "fr": "Jaune ocre", "es": "Amarillo ocre", "it": "Giallo ocra", "nl": "Okergeel" }, { "code": "RAL 1026", "rgb": "255-255-000", "hex": "#FFFF00", "de": "Leuchtgelb", "en": "Luminous yellow", "fr": "Jaune brillant", "es": "Amarillo brillante", "it": "Giallo brillante", "nl": "Briljantgeel" }, { "code": "RAL 1027", "rgb": "157-145-001", "hex": "#9D9101", "de": "Currygelb", "en": "Curry", "fr": "Jaune curry", "es": "Amarillo curry", "it": "Giallo curry", "nl": "Kerriegeel" }, { "code": "RAL 1028", "rgb": "244-169-000", "hex": "#F4A900", "de": "Melonengelb", "en": "Melon yellow", "fr": "Jaune melon", "es": "Amarillo melón", "it": "Giallo melone", "nl": "Meloengeel" }, { "code": "RAL 1032", "rgb": "214-174-001", "hex": "#D6AE01", "de": "Ginstergelb", "en": "Broom yellow", "fr": "Jaune genêt", "es": "Amarillo retama", "it": "Giallo scopa", "nl": "Bremgeel" }, { "code": "RAL 1033", "rgb": "243-165-005", "hex": "#F3A505", "de": "Dahliengelb", "en": "Dahlia yellow", "fr": "Jaune dahlia", "es": "Amarillo dalia", "it": "Giallo dahlien", "nl": "Dahliageel" }, { "code": "RAL 1034", "rgb": "239-169-074", "hex": "#EFA94A", "de": "Pastellgelb", "en": "Pastel yellow", "fr": "Jaune pastel", "es": "Amarillo pastel", "it": "Giallo pastello", "nl": "Pastelgeel" }, { "code": "RAL 1035", "rgb": "106-093-077", "hex": "#6A5D4D", "de": "Perlbeige", "en": "Pearl beige", "fr": "Beige nacré", "es": "Beige perlado", "it": "Beige perlato", "nl": "Parelmoergrijs" }, { "code": "RAL 1036", "rgb": "112-083-053", "hex": "#705335", "de": "Perlgold", "en": "Pearl gold", "fr": "Or nacré", "es": "Oro perlado", "it": "Oro perlato", "nl": "Parelmoergoud" }, { "code": "RAL 1037", "rgb": "243-159-024", "hex": "#F39F18", "de": "Sonnengelb", "en": "Sun yellow", "fr": "Jaune soleil", "es": "Amarillo sol", "it": "Giallo sole", "nl": "Zonnegeel" }, { "code": "RAL 1039", "rgb": "206-193-158", "hex": "#CEC19E", "de": "Sandbeige", "en": "Sandbeige", "fr": "Sandbeige", "es": "Sandbeige", "it": "Sandbeige", "nl": "Sandbeige" }, { "code": "RAL 1040", "rgb": "187-172-129", "hex": "#BBAC81", "de": "Lehmbeige", "en": "Lehmbeige", "fr": "Lehmbeige", "es": "Lehmbeige", "it": "Lehmbeige", "nl": "Lehmbeige" }, { "code": "RAL 2000", "rgb": "237-118-014", "hex": "#ED760E", "de": "Gelborange", "en": "Yellow orange", "fr": "Orangé jaune", "es": "Amarillo naranja", "it": "Arancio giallastro", "nl": "Geeloranje" }, { "code": "RAL 2001", "rgb": "201-060-032", "hex": "#C93C20", "de": "Rotorange", "en": "Red orange", "fr": "Orangé rouge", "es": "Rojo anaranjado", "it": "Arancio rossastro", "nl": "Roodoranje" }, { "code": "RAL 2002", "rgb": "203-040-033", "hex": "#CB2821", "de": "Blutorange", "en": "Vermilion", "fr": "Orangé sang", "es": "Naranja sanguineo", "it": "Arancio sanguigno", "nl": "Vermiljoen" }, { "code": "RAL 2003", "rgb": "255-117-020", "hex": "#FF7514", "de": "Pastellorange", "en": "Pastel orange", "fr": "Orangé pastel", "es": "Naranja pálido", "it": "Arancio pastello", "nl": "Pasteloranje" }, { "code": "RAL 2004", "rgb": "244-070-017", "hex": "#F44611", "de": "Reinorange", "en": "Pure orange", "fr": "Orangé pur", "es": "Naranja puro", "it": "Arancio puro", "nl": "Zuiver oranje" }, { "code": "RAL 2005", "rgb": "255-035-001", "hex": "#FF2301", "de": "Leuchtorange", "en": "Luminous orange", "fr": "Orangé brillant", "es": "Naranja brillante", "it": "Arancio brillante", "nl": "Briljant oranje" }, { "code": "RAL 2007", "rgb": "255-164-032", "hex": "#FFA420", "de": "Leuchthellorange", "en": "Luminous bright orange", "fr": "Orangé clair rillant", "es": "Naranja claro brillante", "it": "Arancio chiaro brillante", "nl": "Briljant lichtoranje" }, { "code": "RAL 2008", "rgb": "247-094-037", "hex": "#F75E25", "de": "Hellrotorange", "en": "Bright red orange", "fr": "Orangé rouge clair", "es": "Rojo claro anaranjado", "it": "Rosso arancio chiaro", "nl": "Licht roodoranje" }, { "code": "RAL 2009", "rgb": "245-064-033", "hex": "#F54021", "de": "Verkehrsorange", "en": "Traffic orange", "fr": "Orangé signalisation", "es": "Naranja tráfico", "it": "Arancio traffico", "nl": "Verkeersoranje" }, { "code": "RAL 2010", "rgb": "216-075-032", "hex": "#D84B20", "de": "Signalorange", "en": "Signal orange", "fr": "Orangé de sécurité", "es": "Naranja señales", "it": "Arancio segnale", "nl": "Signaaloranje" }, { "code": "RAL 2011", "rgb": "236-124-038", "hex": "#EC7C26", "de": "Tieforange", "en": "Deep orange", "fr": "Orangé foncé", "es": "Naranja intenso", "it": "Arancio profondo", "nl": "Dieporanje" }, { "code": "RAL 2012", "rgb": "235-106-014", "hex": "#E55137", "de": "Lachsorange", "en": "Salmon range", "fr": "Orangé saumon", "es": "Naranja salmón", "it": "Arancio salmone", "nl": "Zalmoranje" }, { "code": "RAL 2013", "rgb": "195-088-049", "hex": "#C35831", "de": "Perlorange", "en": "Pearl orange", "fr": "Orangé nacré", "es": "Naranja perlado", "it": "Arancio perlato", "nl": "Parelmoeroranje" }, { "code": "RAL 3000", "rgb": "175-043-030", "hex": "#AF2B1E", "de": "Feuerrot", "en": "Flame red", "fr": "Rouge feu", "es": "Rojo vivo", "it": "Rosso fuoco", "nl": "Vuurrood" }, { "code": "RAL 3001", "rgb": "165-032-025", "hex": "#A52019", "de": "Signalrot", "en": "Signal red", "fr": "Rouge de sécurité", "es": "Rojo señales", "it": "Rosso  segnale", "nl": "Signaalrood" }, { "code": "RAL 3002", "rgb": "162-035-029", "hex": "#A2231D", "de": "Karminrot", "en": "Carmine red", "fr": "Rouge carmin", "es": "Rojo carmin", "it": "Rosso carminio", "nl": "Karmijnrood" }, { "code": "RAL 3003", "rgb": "155-017-030", "hex": "#9B111E", "de": "Rubinrot", "en": "Ruby red", "fr": "Rouge rubis", "es": "Rojo rubí", "it": "Rosso rubino", "nl": "Robijnrood" }, { "code": "RAL 3004", "rgb": "117-021-030", "hex": "#75151E", "de": "Purpurrot", "en": "Purple red", "fr": "Rouge pourpre", "es": "Rojo purpura", "it": "Rosso porpora", "nl": "Purperrood" }, { "code": "RAL 3005", "rgb": "094-033-041", "hex": "#5E2129", "de": "Weinrot", "en": "Wine red", "fr": "Rouge vin", "es": "Rojo vino", "it": "Rosso vino", "nl": "Wijnrood" }, { "code": "RAL 3007", "rgb": "065-034-039", "hex": "#412227", "de": "Schwarzrot", "en": "Black red", "fr": "Rouge noir", "es": "Rojo negruzco", "it": "Rosso nerastro", "nl": "Zwartrood" }, { "code": "RAL 3009", "rgb": "100-036-036", "hex": "#642424", "de": "Oxidrot", "en": "Oxide red", "fr": "Rouge oxyde", "es": "Rojo óxido", "it": "Rosso  ossido", "nl": "Oxyderood" }, { "code": "RAL 3011", "rgb": "120-031-025", "hex": "#781F19", "de": "Braunrot", "en": "Brown red", "fr": "Rouge brun", "es": "Rojo pardo", "it": "Rosso marrone", "nl": "Bruinrood" }, { "code": "RAL 3012", "rgb": "193-135-107", "hex": "#C1876B", "de": "Beigerot", "en": "Beige red", "fr": "Rouge beige", "es": "Rojo beige", "it": "Rosso beige", "nl": "Beigerood" }, { "code": "RAL 3013", "rgb": "161-035-018", "hex": "#A12312", "de": "Tomatenrot", "en": "Tomato red", "fr": "Rouge tomate", "es": "Rojo tomate", "it": "Rosso pomodoro", "nl": "Tomaatrood" }, { "code": "RAL 3014", "rgb": "211-110-112", "hex": "#D36E70", "de": "Altrosa", "en": "Antique pink", "fr": "Vieux rose", "es": "Rojo viejo", "it": "Rosa antico", "nl": "Oudroze" }, { "code": "RAL 3015", "rgb": "234-137-154", "hex": "#EA899A", "de": "Hellrosa", "en": "Light pink", "fr": "Rose clair", "es": "Rosa claro", "it": "Rosa chiaro", "nl": "Lichtroze" }, { "code": "RAL 3016", "rgb": "179-040-033", "hex": "#B32821", "de": "Korallenrot", "en": "Coral red", "fr": "Rouge corail", "es": "Rojo coral", "it": "Rosso corallo", "nl": "Koraalrood" }, { "code": "RAL 3017", "rgb": "230-050-068", "hex": "#E63244", "de": "Rosé", "en": "Rose", "fr": "Rosé", "es": "Rosa", "it": "Rosato", "nl": "Bleekrood" }, { "code": "RAL 3018", "rgb": "213-048-050", "hex": "#D53032", "de": "Erdbeerrot", "en": "Strawberry red", "fr": "Rouge fraise", "es": "Rojo fresa", "it": "Rosso fragola", "nl": "Aardbeirood" }, { "code": "RAL 3020", "rgb": "204-006-005", "hex": "#CC0605", "de": "Verkehrsrot", "en": "Traffic red", "fr": "Rouge signalisation", "es": "Rojo tráfico", "it": "Rosso traffico", "nl": "Verkeersrood" }, { "code": "RAL 3022", "rgb": "217-080-048", "hex": "#D95030", "de": "Lachsrot", "en": "Salmon pink", "fr": "Rouge saumon", "es": "Rojo salmón", "it": "Rosso salmone", "nl": "Zalmrood" }, { "code": "RAL 3024", "rgb": "248-000-000", "hex": "#F80000", "de": "Leuchtrot", "en": "Luminous red", "fr": "Rouge brillant", "es": "Rojo brillante", "it": "Rosso brillante", "nl": "Briljantrood" }, { "code": "RAL 3026", "rgb": "254-000-000", "hex": "#FE0000", "de": "Leuchthellrot", "en": "Luminous bright red", "fr": "Rouge clair brillant", "es": "Rojo claro brillante", "it": "Rosso chiaro brillante", "nl": "Briljant lichtrood" }, { "code": "RAL 3027", "rgb": "197-029-052", "hex": "#C51D34", "de": "Himbeerrot", "en": "Raspberry red", "fr": "Rouge framboise", "es": "Rojo frambuesa", "it": "Rosso lampone", "nl": "Framboosrood" }, { "code": "RAL 3028", "rgb": "203-050-052", "hex": "#CB3234", "de": "Reinrot", "en": "Pure  red", "fr": "Rouge puro", "es": "Rojo puro", "it": "Rosso puro", "nl": "Zuiverrood" }, { "code": "RAL 3031", "rgb": "179-036-040", "hex": "#B32428", "de": "Orientrot", "en": "Orient red", "fr": "Rouge oriental", "es": "Rojo oriente", "it": "Rosso oriente", "nl": "Oriëntrood" }, { "code": "RAL 3032", "rgb": "114-020-034", "hex": "#721422", "de": "Perlrubinrot", "en": "Pearl ruby red", "fr": "Rouge rubis nacré", "es": "Rojo rubí perlado", "it": "Rosso rubino perlato", "nl": "Parelmoer-donkerrood" }, { "code": "RAL 3033", "rgb": "180-076-067", "hex": "#B44C43", "de": "Perlrosa", "en": "Pearl pink", "fr": "Rose nacré", "es": "Rosa perlado", "it": "Rosa perlato", "nl": "Parelmoer-lichtrood" }, { "code": "RAL 4001", "rgb": "222-076-138", "hex": "#6D3F5B", "de": "Rotlila", "en": "Red lilac", "fr": "Lilas rouge", "es": "Rojo lila", "it": "Lilla rossastro", "nl": "Roodlila" }, { "code": "RAL 4002", "rgb": "146-043-062", "hex": "#922B3E", "de": "Rotviolett", "en": "Red violet", "fr": "Violet rouge", "es": "Rojo violeta", "it": "Viola rossastro", "nl": "Roodpaars" }, { "code": "RAL 4003", "rgb": "222-076-138", "hex": "#DE4C8A", "de": "Erikaviolett", "en": "Heather violet", "fr": "Violet bruyère", "es": "Violeta érica", "it": "Viola erica", "nl": "Heidepaars" }, { "code": "RAL 4004", "rgb": "110-028-052", "hex": "#641C34", "de": "Bordeauxviolett", "en": "Claret violet", "fr": "Violet bordeaux", "es": "Burdeos", "it": "Viola bordeaux", "nl": "Bordeauxpaars" }, { "code": "RAL 4005", "rgb": "108-070-117", "hex": "#6C4675", "de": "Blaulila", "en": "Blue lilac", "fr": "Lilas bleu", "es": "Lila azulado", "it": "Lilla bluastro", "nl": "Blauwlila" }, { "code": "RAL 4006", "rgb": "160-052-114", "hex": "#A03472", "de": "Verkehrspurpur", "en": "Traffic purple", "fr": "Pourpre signalisation", "es": "Púrpurá tráfico", "it": "Porpora traffico", "nl": "Verkeerspurper" }, { "code": "RAL 4007", "rgb": "074-025-044", "hex": "#4A192C", "de": "Purpurviolett", "en": "Purple violet", "fr": "Violet pourpre", "es": "Violeta púrpura", "it": "Porpora violetto", "nl": "Purperviolet" }, { "code": "RAL 4008", "rgb": "144-070-132", "hex": "#924E7D", "de": "Signalviolett", "en": "Signal violet", "fr": "Violet de sécurité", "es": "Violeta señales", "it": "Violetto segnale", "nl": "Signaalviolet" }, { "code": "RAL 4009", "rgb": "164-125-144", "hex": "#A18594", "de": "Pastellviolett", "en": "Pastel violet", "fr": "Violet pastel", "es": "Violeta pastel", "it": "Violetto pastello", "nl": "Pastelviolet" }, { "code": "RAL 4010", "rgb": "215-045-109", "hex": "#CF3476", "de": "Telemagenta", "en": "Telemagenta", "fr": "Telemagenta", "es": "Magenta tele", "it": "Tele Magenta", "nl": "Telemagenta" }, { "code": "RAL 4011", "rgb": "134-115-161", "hex": "#8673A1", "de": "Perlviolett", "en": "Pearl violet", "fr": "Violet nacré", "es": "Violeta perlado", "it": "Violetto perlato", "nl": "Parelmoer-donkerviolet" }, { "code": "RAL 4012", "rgb": "108-104-129", "hex": "#6C6874", "de": "Perlbrombeer", "en": "Pearl black berry", "fr": "Mûre nacré", "es": "Morado perlado", "it": "Mora perlato", "nl": "Parelmoer-lichtviolet" }, { "code": "RAL 5000", "rgb": "042-046-075", "hex": "#354D73", "de": "Violettblau", "en": "Violet blue", "fr": "Bleu violet", "es": "Azul violeta", "it": "Blu violaceo", "nl": "Paarsblauw" }, { "code": "RAL 5001", "rgb": "031-052-056", "hex": "#1F3438", "de": "Grünblau", "en": "Green blue", "fr": "Bleu vert", "es": "Azul verdoso", "it": "Blu verdastro", "nl": "Groenblauw" }, { "code": "RAL 5002", "rgb": "032-033-079", "hex": "#20214F", "de": "Ultramarinblau", "en": "Ultramarine blue", "fr": "Bleu outremer", "es": "Azul ultramar", "it": "Blu oltremare", "nl": "Ultramarijnblauw" }, { "code": "RAL 5003", "rgb": "029-030-051", "hex": "#1D1E33", "de": "Saphirblau", "en": "Saphire blue", "fr": "Bleu saphir", "es": "Azul zafiro", "it": "Blu zaffiro", "nl": "Saffierblauw" }, { "code": "RAL 5004", "rgb": "032-033-079", "hex": "#18171C", "de": "Schwarzblau", "en": "Black blue", "fr": "Bleu noir", "es": "Azul negruzco", "it": "Blu nerastro", "nl": "Zwartblauw" }, { "code": "RAL 5005", "rgb": "030-045-110", "hex": "#1E2460", "de": "Signalblau", "en": "Signal blue", "fr": "Bleu de sécurité", "es": "Azul señales", "it": "Blu segnale", "nl": "Signaalblauw" }, { "code": "RAL 5007", "rgb": "062-095-138", "hex": "#3E5F8A", "de": "Brillantblau", "en": "Brillant blue", "fr": "Bleu brillant", "es": "Azul brillante", "it": "Blu brillante", "nl": "Briljantblauw" }, { "code": "RAL 5008", "rgb": "038-037-045", "hex": "#26252D", "de": "Graublau", "en": "Grey blue", "fr": "Bleu gris", "es": "Azul grisáceo", "it": "Blu grigiastro", "nl": "Grijsblauw" }, { "code": "RAL 5009", "rgb": "002-086-105", "hex": "#025669", "de": "Azurblau", "en": "Azure blue", "fr": "Bleu azur", "es": "Azul azur", "it": "Blu  azzurro", "nl": "Azuurblauw" }, { "code": "RAL 5010", "rgb": "014-041-075", "hex": "#0E294B", "de": "Enzianblau", "en": "Gentian blue", "fr": "Bleu gentiane", "es": "Azul genciana", "it": "Blu  genziana", "nl": "Gentiaanblauw" }, { "code": "RAL 5011", "rgb": "035-026-036", "hex": "#231A24", "de": "Stahlblau", "en": "Steel blue", "fr": "Bleu acier", "es": "Azul acero", "it": "Blu acciaio", "nl": "Staalblauw" }, { "code": "RAL 5012", "rgb": "059-131-189", "hex": "#3B83BD", "de": "Lichtblau", "en": "Light blue", "fr": "Bleu clair", "es": "Azul luminoso", "it": "Blu luce", "nl": "Lichtblauw" }, { "code": "RAL 5013", "rgb": "037-041-074", "hex": "#1E213D", "de": "Kobaltblau", "en": "Cobalt blue", "fr": "Bleu cobalt", "es": "Azul cobalto", "it": "Blu cobalto", "nl": "Kobaltblauw" }, { "code": "RAL 5014", "rgb": "096-111-140", "hex": "#606E8C", "de": "Taubenblau", "en": "Pigeon blue", "fr": "Bleu pigeon", "es": "Azul olombino", "it": "Blu colomba", "nl": "Duifblauw" }, { "code": "RAL 5015", "rgb": "034-113-179", "hex": "#2271B3", "de": "Himmelblau", "en": "Sky blue", "fr": "Bleu ciel", "es": "Azul celeste", "it": "Blu cielo", "nl": "Hemelsblauw" }, { "code": "RAL 5017", "rgb": "006-057-113", "hex": "#063971", "de": "Verkehrsblau", "en": "Traffic blue", "fr": "Bleu signalisation", "es": "Azul tráfico", "it": "Blu traffico", "nl": "Verkeersblauw" }, { "code": "RAL 5018", "rgb": "063-136-143", "hex": "#3F888F", "de": "Türkisblau", "en": "Turquoise blue", "fr": "Bleu turquoise", "es": "Azul turquesa", "it": "Blu turchese", "nl": "Turkooisblauw" }, { "code": "RAL 5019", "rgb": "027-085-131", "hex": "#1B5583", "de": "Capriblau", "en": "Capri blue", "fr": "Bleu capri", "es": "Azul capri", "it": "Blu capri", "nl": "Capriblauw" }, { "code": "RAL 5020", "rgb": "029-051-074", "hex": "#1D334A", "de": "Ozeanblau", "en": "Ocean blue", "fr": "Bleu océan", "es": "Azul oceano", "it": "Blu oceano", "nl": "Oceaanblauw" }, { "code": "RAL 5021", "rgb": "037-109-123", "hex": "#256D7B", "de": "Wasserblau", "en": "Water blue", "fr": "Bleu d’eau", "es": "Azul agua", "it": "Blu acqua", "nl": "Waterblauw" }, { "code": "RAL 5022", "rgb": "037-040-080", "hex": "#252850", "de": "Nachtblau", "en": "Night blue", "fr": "Bleu nocturne", "es": "Azul noche", "it": "Blu notte", "nl": "Nachtblauw" }, { "code": "RAL 5023", "rgb": "073-103-141", "hex": "#49678D", "de": "Fernblau", "en": "Distant blue", "fr": "Bleu distant", "es": "Azul lejanía", "it": "Blu distante", "nl": "Verblauw" }, { "code": "RAL 5024", "rgb": "093-155-155", "hex": "#5D9B9B", "de": "Pastellblau", "en": "Pastel blue", "fr": "Bleu pastel", "es": "Azul pastel", "it": "Blu pastello", "nl": "Pastelblauw" }, { "code": "RAL 5025", "rgb": "042-100-120", "hex": "#2A6478", "de": "Perlenzian", "en": "Pearl gentian blue", "fr": "Gentiane nacré", "es": "Gencian perlado", "it": "Blu genziana perlato", "nl": "Parelmoerblauw" }, { "code": "RAL 5026", "rgb": "016-044-084", "hex": "#102C54", "de": "Perlnachtblau", "en": "Pearl night blue", "fr": "Bleu nuit nacré", "es": "Azul noche perlado", "it": "Blu notte perlato", "nl": "Parelmoer-nachtblauw" }, { "code": "RAL 6000", "rgb": "049-102-080", "hex": "#316650", "de": "Patinagrün", "en": "Patina green", "fr": "Vert patine", "es": "Verde patina", "it": "Verde patina", "nl": "Patinagroen" }, { "code": "RAL 6001", "rgb": "040-114-051", "hex": "#287233", "de": "Smaragdgrün", "en": "Emerald green", "fr": "Vert émeraude", "es": "Verde esmeralda", "it": "Verde smeraldo", "nl": "Smaragdgroen" }, { "code": "RAL 6002", "rgb": "045-087-044", "hex": "#2D572C", "de": "Laubgrün", "en": "Leaf green", "fr": "Vert feuillage", "es": "Verde hoja", "it": "Verde foglia", "nl": "Loofgroen" }, { "code": "RAL 6003", "rgb": "066-070-050", "hex": "#424632", "de": "Olivgrün", "en": "Olive green", "fr": "Vert olive", "es": "Verde oliva", "it": "Verde oliva", "nl": "Olijfgroen" }, { "code": "RAL 6004", "rgb": "031-058-061", "hex": "#1F3A3D", "de": "Blaugrün", "en": "Blue green", "fr": "Vert bleu", "es": "Verde azulado", "it": "Verde bluastro", "nl": "Blauwgroen" }, { "code": "RAL 6005", "rgb": "047-069-056", "hex": "#2F4538", "de": "Moosgrün", "en": "Moss green", "fr": "Vert mousse", "es": "Verde musgo", "it": "Verde muschio", "nl": "Mosgroen" }, { "code": "RAL 6006", "rgb": "062-059-050", "hex": "#3E3B32", "de": "Grauoliv", "en": "Grey olive", "fr": "Olive gris", "es": "Oliva grisáceo", "it": "Oliva grigiastro", "nl": "Grijs olijfgroen" }, { "code": "RAL 6007", "rgb": "052-059-041", "hex": "#343B29", "de": "Flaschengrün", "en": "Bottle green", "fr": "Vert bouteille", "es": "Verde botella", "it": "Verde bottiglia", "nl": "Flessegroen" }, { "code": "RAL 6008", "rgb": "057-053-042", "hex": "#39352A", "de": "Braungrün", "en": "Brown green", "fr": "Vert brun", "es": "Verde parduzco", "it": "Verde brunastro", "nl": "Bruingroen" }, { "code": "RAL 6009", "rgb": "049-055-043", "hex": "#31372B", "de": "Tannengrün", "en": "Fir green", "fr": "Vert sapin", "es": "Verde abeto", "it": "Verde abete", "nl": "Dennegroen" }, { "code": "RAL 6010", "rgb": "053-104-045", "hex": "#35682D", "de": "Grasgrün", "en": "Grass green", "fr": "Vert herbe", "es": "Verde hierba", "it": "Verde erba", "nl": "Grasgroen" }, { "code": "RAL 6011", "rgb": "088-114-070", "hex": "#587246", "de": "Resedagrün", "en": "Reseda green", "fr": "Vert réséda", "es": "Verde reseda", "it": "Verde reseda", "nl": "Resedagroen" }, { "code": "RAL 6012", "rgb": "052-062-064", "hex": "#343E40", "de": "Schwarzgrün", "en": "Black green", "fr": "Vert noir", "es": "Verde negruzco", "it": "Verde nerastro", "nl": "Zwartgroen" }, { "code": "RAL 6013", "rgb": "108-113-086", "hex": "#6C7156", "de": "Schilfgrün", "en": "Reed green", "fr": "Vert jonc", "es": "Verde caña", "it": "Verde canna", "nl": "Rietgroen" }, { "code": "RAL 6014", "rgb": "071-064-046", "hex": "#47402E", "de": "Gelboliv", "en": "Yellow olive", "fr": "Olive jaune", "es": "Amarillo oliva", "it": "Oliva giallastro", "nl": "Geel olijfgroen" }, { "code": "RAL 6015", "rgb": "059-060-054", "hex": "#3B3C36", "de": "Schwarzoliv", "en": "Black olive", "fr": "Olive noir", "es": "Oliva negruzco", "it": "Oliva nerastro", "nl": "Zwart olijfgroen" }, { "code": "RAL 6016", "rgb": "030-089-069", "hex": "#1E5945", "de": "Türkisgrün", "en": "Turquoise green", "fr": "Vert turquoise", "es": "Verde turquesa", "it": "Verde turchese", "nl": "Turkooisgroen" }, { "code": "RAL 6017", "rgb": "076-145-065", "hex": "#4C9141", "de": "Maigrün", "en": "May green", "fr": "Vert mai", "es": "Verde mayo", "it": "Verde maggio", "nl": "Meigroen" }, { "code": "RAL 6018", "rgb": "087-166-057", "hex": "#57A639", "de": "Gelbgrün", "en": "Yellow green", "fr": "Vert jaune", "es": "Verde amarillento", "it": "Verde giallastro", "nl": "Geelgroen" }, { "code": "RAL 6019", "rgb": "189-236-182", "hex": "#BDECB6", "de": "Weißgrün", "en": "Pastel green", "fr": "Vert blanc", "es": "Verde lanquecino", "it": "Verde biancastro", "nl": "Witgroen" }, { "code": "RAL 6020", "rgb": "046-058-035", "hex": "#2E3A23", "de": "Chromoxidgrün", "en": "Chrome green", "fr": "Vert oxyde chromique", "es": "Verde cromo", "it": "Verde cromo", "nl": "Chroom-oxydegroen" }, { "code": "RAL 6021", "rgb": "137-172-118", "hex": "#89AC76", "de": "Blassgrün", "en": "Pale green", "fr": "Vert pâle", "es": "Verde pálido", "it": "Verde pallido", "nl": "Bleekgroen" }, { "code": "RAL 6022", "rgb": "037-034-027", "hex": "#25221B", "de": "Braunoliv", "en": "Olive drab", "fr": "Olive brun", "es": "Oliva parduzco", "it": "Oliva brunastro", "nl": "Bruin olijfgroen" }, { "code": "RAL 6024", "rgb": "048-132-070", "hex": "#308446", "de": "Verkehrsgrün", "en": "Traffic green", "fr": "Vert signalisation", "es": "Verde tráfico", "it": "Verde traffico", "nl": "Verkeersgroen" }, { "code": "RAL 6025", "rgb": "061-100-045", "hex": "#3D642D", "de": "Farngrün", "en": "Fern green", "fr": "Vert fougère", "es": "Verde helecho", "it": "Verde felce", "nl": "Varengroen" }, { "code": "RAL 6026", "rgb": "001-093-082", "hex": "#015D52", "de": "Opalgrün", "en": "Opal green", "fr": "Vert opale", "es": "Verde opalo", "it": "Verde opale", "nl": "Opaalgroen" }, { "code": "RAL 6027", "rgb": "132-195-190", "hex": "#84C3BE", "de": "Lichtgrün", "en": "Light green", "fr": "Vert clair", "es": "Verde luminoso", "it": "Verde chiaro", "nl": "Lichtgroen" }, { "code": "RAL 6028", "rgb": "044-085-069", "hex": "#2C5545", "de": "Kieferngrün", "en": "Pine green", "fr": "Vert pin", "es": "Verde pino", "it": "Verde pino", "nl": "Pijnboomgroen" }, { "code": "RAL 6029", "rgb": "032-096-061", "hex": "#20603D", "de": "Minzgrün", "en": "Mint green", "fr": "Vert menthe", "es": "Verde menta", "it": "Verde menta", "nl": "Mintgroen" }, { "code": "RAL 6031", "rgb": "072-087-070", "hex": "#485746", "de": "Bronzegrün", "en": "Bronzegrün", "fr": "Bronzegrün", "es": "Bronzegrün", "it": "Bronzegrün", "nl": "Bronzegrün" }, { "code": "RAL 6032", "rgb": "049-127-067", "hex": "#317F43", "de": "Signalgrün", "en": "Signal green", "fr": "Vert de sécurité", "es": "Verde señales", "it": "Verde segnale", "nl": "Signaalgroen" }, { "code": "RAL 6033", "rgb": "073-126-118", "hex": "#497E76", "de": "Minttürkis", "en": "Mint turquoise", "fr": "Turquoise menthe", "es": "Turquesa menta", "it": "Turchese menta", "nl": "Mintturquoise" }, { "code": "RAL 6034", "rgb": "127-181-181", "hex": "#7FB5B5", "de": "Pastelltürkis", "en": "Pastel turquoise", "fr": "Turquoise pastel", "es": "Turquesa pastel", "it": "Turchese pastello", "nl": "Pastelturquoise" }, { "code": "RAL 6035", "rgb": "028-084-045", "hex": "#1C542D", "de": "Perlgrün", "en": "Pearl green", "fr": "Vert nacré", "es": "Verde perlado", "it": "Verde perlato", "nl": "Parelmoer-donkergroen" }, { "code": "RAL 6036", "rgb": "022-053-055", "hex": "#193737", "de": "Perlopalgrün", "en": "Pearl opal green", "fr": "Vert opal nacré", "es": "Verde ópalo perlado", "it": "Verde opalo perlato", "nl": "Parelmoer-lichtgroen" }, { "code": "RAL 6037", "rgb": "000-143-057", "hex": "#008F39", "de": "Reingrün", "en": "Pure green", "fr": "Vert pur", "es": "Verde puro", "it": "Verde puro", "nl": "Zuivergroen" }, { "code": "RAL 6038", "rgb": "000-187-045", "hex": "#00BB2D", "de": "Leuchtgrün", "en": "Luminous green", "fr": "Vert brillant", "es": "Verde brillante", "it": "Verde brillante", "nl": "Briljantgroen" }, { "code": "RAL 6040", "rgb": "130-126-88", "hex": "#827E58", "de": "Helloliv", "en": "Helloliv", "fr": "Helloliv", "es": "Helloliv", "it": "Helloliv", "nl": "Helloliv" }, { "code": "RAL 7000", "rgb": "120-133-139", "hex": "#78858B", "de": "Fehgrau", "en": "Squirrel grey", "fr": "Gris petit-gris", "es": "Gris ardilla", "it": "Grigio vaio", "nl": "Pelsgrijs" }, { "code": "RAL 7001", "rgb": "138-149-151", "hex": "#8A9597", "de": "Silbergrau", "en": "Silver grey", "fr": "Gris argent", "es": "Gris plata", "it": "Grigio argento", "nl": "Zilvergrijs" }, { "code": "RAL 7002", "rgb": "126-123-082", "hex": "#7E7B52", "de": "Olivgrau", "en": "Olive grey", "fr": "Gris olive", "es": "Gris oliva", "it": "Grigio olivastro", "nl": "Olijfgrijs" }, { "code": "RAL 7003", "rgb": "108-112-089", "hex": "#6C7059", "de": "Moosgrau", "en": "Moss grey", "fr": "Gris mousse", "es": "Gris musgo", "it": "Grigio muschio", "nl": "Mosgrijs" }, { "code": "RAL 7004", "rgb": "150-153-146", "hex": "#969992", "de": "Signalgrau", "en": "Signal grey", "fr": "Gris de sécurité", "es": "Gris señales", "it": "Grigio segnale", "nl": "Signaalgrijs" }, { "code": "RAL 7005", "rgb": "100-107-099", "hex": "#646B63", "de": "Mausgrau", "en": "Mouse grey", "fr": "Gris souris", "es": "Gris ratón", "it": "Grigio topo", "nl": "Muisgrijs" }, { "code": "RAL 7006", "rgb": "109-101-082", "hex": "#6D6552", "de": "Beigegrau", "en": "Beige grey", "fr": "Gris beige", "es": "Gris beige", "it": "Grigio beige", "nl": "Beigegrijs" }, { "code": "RAL 7008", "rgb": "106-095-049", "hex": "#6A5F31", "de": "Khakigrau", "en": "Khaki grey", "fr": "Gris kaki", "es": "Gris caqui", "it": "Grigio kaki", "nl": "Kakigrijs" }, { "code": "RAL 7009", "rgb": "077-086-069", "hex": "#4D5645", "de": "Grüngrau", "en": "Green grey", "fr": "Gris vert", "es": "Gris verdoso", "it": "Grigio verdastro", "nl": "Groengrijs" }, { "code": "RAL 7010", "rgb": "076-081-074", "hex": "#4C514A", "de": "Zeltgrau", "en": "Tarpaulin grey", "fr": "Gris tente", "es": "Gris lona", "it": "Grigio tenda", "nl": "Zeildoekgrijs" }, { "code": "RAL 7011", "rgb": "067-075-077", "hex": "#434B4D", "de": "Eisengrau", "en": "Iron grey", "fr": "Gris fer", "es": "Gris hierro", "it": "Grigio ferro", "nl": "IJzergrijs" }, { "code": "RAL 7012", "rgb": "078-087-084", "hex": "#4E5754", "de": "Basaltgrau", "en": "Basalt grey", "fr": "Gris basalte", "es": "Gris basalto", "it": "Grigio basalto", "nl": "Bazaltgrijs" }, { "code": "RAL 7013", "rgb": "070-069-049", "hex": "#464531", "de": "Braungrau", "en": "Brown grey", "fr": "Gris brun", "es": "Gris parduzco", "it": "Grigio brunastro", "nl": "Bruingrijs" }, { "code": "RAL 7015", "rgb": "067-071-080", "hex": "#434750", "de": "Schiefergrau", "en": "Slate grey", "fr": "Gris ardoise", "es": "Gris pizarra", "it": "Grigio ardesia", "nl": "Leigrijs" }, { "code": "RAL 7016", "rgb": "041-049-051", "hex": "#293133", "de": "Anthrazitgrau", "en": "Anthracite grey", "fr": "Gris anthracite", "es": "Gris antracita", "it": "Grigio antracite", "nl": "Antracietgrijs" }, { "code": "RAL 7021", "rgb": "035-040-043", "hex": "#23282B", "de": "Schwarzgrau", "en": "Black grey", "fr": "Gris noir", "es": "Gris negruzco", "it": "Grigio nerastro", "nl": "Zwartgrijs" }, { "code": "RAL 7022", "rgb": "051-047-044", "hex": "#332F2C", "de": "Umbragrau", "en": "Umbra grey", "fr": "Gris terre d’ombre", "es": "Gris sombra", "it": "Grigio ombra", "nl": "Ombergrijs" }, { "code": "RAL 7023", "rgb": "104-108-094", "hex": "#686C5E", "de": "Betongrau", "en": "Concrete grey", "fr": "Gris béton", "es": "Gris hormigón", "it": "Grigio calcestruzzo", "nl": "Betongrijs" }, { "code": "RAL 7024", "rgb": "071-074-081", "hex": "#474A51", "de": "Graphitgrau", "en": "Graphite grey", "fr": "Gris graphite", "es": "Gris grafita", "it": "Grigio grafite", "nl": "Grafietgrijs" }, { "code": "RAL 7026", "rgb": "047-053-059", "hex": "#2F353B", "de": "Granitgrau", "en": "Granite grey", "fr": "Gris granit", "es": "Gris granito", "it": "Grigio granito", "nl": "Granietgrijs" }, { "code": "RAL 7030", "rgb": "139-140-122", "hex": "#8B8C7A", "de": "Steingrau", "en": "Stone grey", "fr": "Gris pierre", "es": "Gris piedra", "it": "Grigio pietra", "nl": "Steengrijs" }, { "code": "RAL 7031", "rgb": "071-075-078", "hex": "#474B4E", "de": "Blaugrau", "en": "Blue grey", "fr": "Gris bleu", "es": "Gris azulado", "it": "Grigio bluastro", "nl": "Blauwgrijs" }, { "code": "RAL 7032", "rgb": "184-183-153", "hex": "#B8B799", "de": "Kieselgrau", "en": "Pebble grey", "fr": "Gris silex", "es": "Gris guijarro", "it": "Grigio ghiaia", "nl": "Kiezelgrijs" }, { "code": "RAL 7033", "rgb": "125-132-113", "hex": "#7D8471", "de": "Zementgrau", "en": "Cement grey", "fr": "Gris ciment", "es": "Gris cemento", "it": "Grigio cemento", "nl": "Cementgrijs" }, { "code": "RAL 7034", "rgb": "143-139-102", "hex": "#8F8B66", "de": "Gelbgrau", "en": "Yellow grey", "fr": "Gris jaune", "es": "Gris amarillento", "it": "Grigio giallastro", "nl": "Geelgrijs" }, { "code": "RAL 7035", "rgb": "215-215-215", "hex": "#D7D7D7", "de": "Lichtgrau", "en": "Light grey", "fr": "Gris clair", "es": "Gris luminoso", "it": "Grigio luce", "nl": "Lichtgrijs" }, { "code": "RAL 7036", "rgb": "127-118-121", "hex": "#7F7679", "de": "Platingrau", "en": "Platinum grey", "fr": "Gris platine", "es": "Gris platino", "it": "Grigio platino", "nl": "Platinagrijs" }, { "code": "RAL 7037", "rgb": "125-127-120", "hex": "#7D7F7D", "de": "Staubgrau", "en": "Dusty grey", "fr": "Gris poussière", "es": "Gris polvo", "it": "Grigio polvere", "nl": "Stofgrijs" }, { "code": "RAL 7038", "rgb": "195-195-195", "hex": "#B5B8B1", "de": "Achatgrau", "en": "Agate grey", "fr": "Gris agate", "es": "Gris ágata", "it": "Grigio agata", "nl": "Agaatgrijs" }, { "code": "RAL 7039", "rgb": "108-105-096", "hex": "#6C6960", "de": "Quarzgrau", "en": "Quartz grey", "fr": "Gris quartz", "es": "Gris cuarzo", "it": "Grigio quarzo", "nl": "Kwartsgrijs" }, { "code": "RAL 7040", "rgb": "157-161-170", "hex": "#9DA1AA", "de": "Fenstergrau", "en": "Window grey", "fr": "Gris fenêtre", "es": "Gris ventana", "it": "Grigio finestra", "nl": "Venstergrijs" }, { "code": "RAL 7042", "rgb": "141-148-141", "hex": "#8D948D", "de": "Verkehrsgrau A", "en": "Traffic grey A", "fr": "Gris signalisation A", "es": "Gris tráfico A", "it": "Grigio traffico A", "nl": "Verkeesgrijs A" }, { "code": "RAL 7043", "rgb": "078-084-082", "hex": "#4E5452", "de": "Verkehrsgrau B", "en": "Traffic grey B", "fr": "Gris signalisation B", "es": "Gris tráfico B", "it": "Grigio traffico B", "nl": "Verkeersgrijs B" }, { "code": "RAL 7044", "rgb": "202-196-176", "hex": "#CAC4B0", "de": "Seidengrau", "en": "Silk grey", "fr": "Gris soie", "es": "Gris seda", "it": "Grigio seta", "nl": "Zijdegrijs" }, { "code": "RAL 7045", "rgb": "144-144-144", "hex": "#909090", "de": "Telegrau 1", "en": "Telegrey 1", "fr": "Telegris 1", "es": "Gris tele 1", "it": "Tele grigio 1", "nl": "Telegrijs 1" }, { "code": "RAL 7046", "rgb": "130-137-143", "hex": "#82898F", "de": "Telegrau 2", "en": "Telegrey 2", "fr": "Telegris 2", "es": "Gris tele 2", "it": "Tele grigio 2", "nl": "Telegrijs 2" }, { "code": "RAL 7047", "rgb": "208-208-208", "hex": "#D0D0D0", "de": "Telegrau 4", "en": "Telegrey 4", "fr": "Telegris 4", "es": "Gris tele 4", "it": "Tele grigio 4", "nl": "Telegrijs 4" }, { "code": "RAL 7048", "rgb": "137-129-118", "hex": "#898176", "de": "Perlmausgrau", "en": "Pearl mouse grey", "fr": "Gris souris nacré", "es": "Gris musgo perlado", "it": "Grigio topo perlato", "nl": "Parelmoer-muisgrijs" }, { "code": "RAL 7050", "rgb": "130-136-122", "hex": "#82887A", "de": "Tarngrau", "en": "Tarngrau", "fr": "Tarngrau", "es": "Tarngrau", "it": "Tarngrau", "nl": "Tarngrau" }, { "code": "RAL 8000", "rgb": "130-108-052", "hex": "#826C34", "de": "Grünbraun", "en": "Green brown", "fr": "Brun vert", "es": "Pardo verdoso", "it": "Marrone verdastro", "nl": "Groenbruin" }, { "code": "RAL 8001", "rgb": "149-095-032", "hex": "#955F20", "de": "Ockerbraun", "en": "Ochre brown", "fr": "Brun terre de Sienne", "es": "Pardo ocre", "it": "Marrone ocra", "nl": "Okerbruin" }, { "code": "RAL 8002", "rgb": "108-059-042", "hex": "#6C3B2A", "de": "Signalbraun", "en": "Signal brown", "fr": "Brun de sécurité", "es": "Marrón señales", "it": "Marrone segnale", "nl": "Signaalbruin" }, { "code": "RAL 8003", "rgb": "115-066-034", "hex": "#734222", "de": "Lehmbraun", "en": "Clay brown", "fr": "Brun argile", "es": "Pardo arcilla", "it": "Marrone fango", "nl": "Leembruin" }, { "code": "RAL 8004", "rgb": "142-064-042", "hex": "#8E402A", "de": "Kupferbraun", "en": "Copper brown", "fr": "Brun cuivré", "es": "Pardo cobre", "it": "Marrone rame", "nl": "Koperbruin" }, { "code": "RAL 8007", "rgb": "089-053-031", "hex": "#59351F", "de": "Rehbraun", "en": "Fawn brown", "fr": "Brun fauve", "es": "Pardo corzo", "it": "Marrone capriolo", "nl": "Reebruin" }, { "code": "RAL 8008", "rgb": "111-079-040", "hex": "#6F4F28", "de": "Olivbraun", "en": "Olive brown", "fr": "Brun olive", "es": "Pardo oliva", "it": "Marrone oliva", "nl": "Olijfbruin" }, { "code": "RAL 8011", "rgb": "091-058-041", "hex": "#5B3A29", "de": "Nussbraun", "en": "Nut brown", "fr": "Brun noisette", "es": "Pardo nuez", "it": "Marrone noce", "nl": "Notebruin" }, { "code": "RAL 8012", "rgb": "089-035-033", "hex": "#592321", "de": "Rotbraun", "en": "Red brown", "fr": "Brun rouge", "es": "Pardo rojo", "it": "Marrone rossiccio", "nl": "Roodbruin" }, { "code": "RAL 8014", "rgb": "056-044-030", "hex": "#382C1E", "de": "Sepiabraun", "en": "Sepia brown", "fr": "Brun sépia", "es": "Sepia", "it": "Marrone seppia", "nl": "Sepiabruin" }, { "code": "RAL 8015", "rgb": "099-058-052", "hex": "#633A34", "de": "Kastanienbraun", "en": "Chestnut brown", "fr": "Marron", "es": "Castaño", "it": "Marrone castagna", "nl": "Kastanjebruin" }, { "code": "RAL 8016", "rgb": "076-047-039", "hex": "#4C2F27", "de": "Mahagonibraun", "en": "Mahogany brown", "fr": "Brun acajou", "es": "Caoba", "it": "Marrone mogano", "nl": "Mahoniebruin" }, { "code": "RAL 8017", "rgb": "069-050-046", "hex": "#45322E", "de": "Schokoladen-braun", "en": "Chocolate brown", "fr": "Brun chocolat", "es": "Chocolate", "it": "Marrone cioccolata", "nl": "Chocoladebruin" }, { "code": "RAL 8019", "rgb": "064-058-058", "hex": "#403A3A", "de": "Graubraun", "en": "Grey brown", "fr": "Brun gris", "es": "Pardo grisáceo", "it": "Marrone grigiastro", "nl": "Grijsbruin" }, { "code": "RAL 8022", "rgb": "033-033-033", "hex": "#212121", "de": "Schwarzbraun", "en": "Black brown", "fr": "Brun noir", "es": "Pardo negruzco", "it": "Marrone nerastro", "nl": "Zwartbruin" }, { "code": "RAL 8023", "rgb": "166-094-046", "hex": "#A65E2E", "de": "Orangebraun", "en": "Orange brown", "fr": "Brun orangé", "es": "Pardo anaranjado", "it": "Marrone arancio", "nl": "Oranjebruin" }, { "code": "RAL 8024", "rgb": "121-085-061", "hex": "#79553D", "de": "Beigebraun", "en": "Beige brown", "fr": "Brun beige", "es": "Pardo beige", "it": "Marrone beige", "nl": "Beigebruin" }, { "code": "RAL 8025", "rgb": "117-092-072", "hex": "#755C48", "de": "Blassbraun", "en": "Pale brown", "fr": "Brun pâle", "es": "Pardo pálido", "it": "Marrone pallido", "nl": "Bleekbruin" }, { "code": "RAL 8027", "rgb": "080-073-056", "hex": "#504938", "de": "Lederbraun", "en": "Lederbraun", "fr": "Lederbraun", "es": "Lederbraun", "it": "Lederbraun", "nl": "Lederbraun" }, { "code": "RAL 8028", "rgb": "078-059-049", "hex": "#4E3B31", "de": "Terrabraun", "en": "Terra brown", "fr": "Brun terre", "es": "Marrón tierra", "it": "Marrone terra", "nl": "Terrabruin" }, { "code": "RAL 8029", "rgb": "118-060-040", "hex": "#763C28", "de": "Perlkupfer", "en": "Pearl copper", "fr": "Cuivre nacré", "es": "Cobre perlado", "it": "Rame perlato", "nl": "Parelmoerkoper" }, { "code": "RAL 8031", "rgb": "180-157-123", "hex": "#B49D7B", "de": "Sandbraun", "en": "Sandbraun", "fr": "Sandbraun", "es": "Sandbraun", "it": "Sandbraun", "nl": "Sandbraun" }, { "code": "RAL 9001", "rgb": "250-244-227", "hex": "#FDF4E3", "de": "Cremeweiß", "en": "Cream", "fr": "Blanc crème", "es": "Blanco crema", "it": "Bianco crema", "nl": "Crèmewit" }, { "code": "RAL 9002", "rgb": "231-235-218", "hex": "#E7EBDA", "de": "Grauweiß", "en": "Grey white", "fr": "Blanc gris", "es": "Blanco grisáceo", "it": "Bianco grigiastro", "nl": "Grijswit" }, { "code": "RAL 9003", "rgb": "244-244-244", "hex": "#F4F4F4", "de": "Signalweiß", "en": "Signal white", "fr": "Blanc de sécurité", "es": "Blanco señales", "it": "Bianco segnale", "nl": "Signaalwit" }, { "code": "RAL 9004", "rgb": "040-040-040", "hex": "#282828", "de": "Signalschwarz", "en": "Signal black", "fr": "Noir de sécurité", "es": "Negro señales", "it": "Nero segnale", "nl": "Signaalzwart" }, { "code": "RAL 9005", "rgb": "010-010-013", "hex": "#0A0A0A", "de": "Tiefschwarz", "en": "Jet black", "fr": "Noir foncé", "es": "Negro intenso", "it": "Nero intenso", "nl": "Gitzwart" }, { "code": "RAL 9006", "rgb": "165-165-165", "hex": "#A5A5A5", "de": "Weißaluminium", "en": "White aluminium", "fr": "Aluminium blanc", "es": "Aluminio blanco", "it": "Aluminio brillante", "nl": "Blank aluminiumkleurig" }, { "code": "RAL 9007", "rgb": "143-143-143", "hex": "#8F8F8F", "de": "Graualuminium", "en": "Grey aluminium", "fr": "Aluminium gris", "es": "Aluminio gris", "it": "Aluminio grigiastro", "nl": "Grijs aluminiumkleurig" }, { "code": "RAL 9010", "rgb": "255-255-255", "hex": "#FFFFFF", "de": "Reinweiß", "en": "Pure white", "fr": "Blanc pur", "es": "Blanco puro", "it": "Bianco puro", "nl": "Zuiverwit" }, { "code": "RAL 9011", "rgb": "028-028-028", "hex": "#1C1C1C", "de": "Graphitschwarz", "en": "Graphite black", "fr": "Noir graphite", "es": "Negro grafito", "it": "Nero grafite", "nl": "Grafietzwart" }, { "code": "RAL 9016", "rgb": "246-246-246", "hex": "#F6F6F6", "de": "Verkehrsweiß", "en": "Traffic white", "fr": "Blanc signalisation", "es": "Blanco tráfico", "it": "Bianco traffico", "nl": "Verkeerswit" }, { "code": "RAL 9017", "rgb": "030-030-030", "hex": "#1E1E1E", "de": "Verkehrs-schwarz", "en": "Traffic black", "fr": "Noir signalisation", "es": "Negro tráfico", "it": "Nero traffico", "nl": "Verkeerszwart" }, { "code": "RAL 9018", "rgb": "215-215-215", "hex": "#D7D7D7", "de": "Papyrusweiß", "en": "Papyrus white", "fr": "Blanc papyrus", "es": "Blanco papiro", "it": "Bianco papiro", "nl": "Papyruswit" }, { "code": "RAL 9020", "rgb": "253-253-253", "hex": "#FDFDFD", "de": "Seidenmattweiß", "en": "Seidenmattweiß", "fr": "Seidenmattweiß", "es": "Seidenmattweiß", "it": "Seidenmattweiß", "nl": "Seidenmattweiß" }, { "code": "RAL 9021", "rgb": "001-005-014", "hex": "#01050E", "de": "Teerschwarz", "en": "Teerschwarz", "fr": "Teerschwarz", "es": "Teerschwarz", "it": "Teerschwarz", "nl": "Teerschwarz" }, { "code": "RAL 9022", "rgb": "156-156-156", "hex": "#9C9C9C", "de": "Perlhellgrau", "en": "Pearl light grey", "fr": "Gris clair nacré", "es": "Gris claro perlado", "it": "Grigio chiaro perlato", "nl": "Parelmoer-lichtgrijs" }, { "code": "RAL 9023", "rgb": "130-130-130", "hex": "#828282", "de": "Perldunkelgrau", "en": "Pearl dark grey", "fr": "Gris fonçé nacré", "es": "Gris oscuro perlado", "it": "Grigio scuro perlato", "nl": "Parelmoer-donkergrijs" }];
        for (var i = 0; i < ralSwatches.length; i++) swatches.push(ralSwatches[i]);
    }
    if (includeNCS || colorSystems == "*") {
        // From view-source: https://www.w3schools.com/colors/colors_ncs.asp
        // Converted manually
        var ncsSwatches = [{"code":"S 0300-N","hex":"#f7f7f7"},{"code":"S 0500-N","hex":"#f2f2f2"},{"code":"S 0502-B","hex":"#f5fbff"},{"code":"S 0502-B50G","hex":"#f7fffe"},{"code":"S 0502-G","hex":"#f4fff8"},{"code":"S 0502-G50Y","hex":"#fdfff6"},{"code":"S 0502-R","hex":"#fff1f2"},{"code":"S 0502-R50B","hex":"#fef6fd"},{"code":"S 0502-Y","hex":"#fffdf6"},{"code":"S 0502-Y50R","hex":"#fef8f4"},{"code":"S 0505-B","hex":"#e8f6fe"},{"code":"S 0505-B20G","hex":"#eafbff"},{"code":"S 0505-B50G","hex":"#ecfffe"},{"code":"S 0505-B80G","hex":"#eafffa"},{"code":"S 0505-G","hex":"#e4ffef"},{"code":"S 0505-G10Y","hex":"#ecffec"},{"code":"S 0505-G20Y","hex":"#f1ffeb"},{"code":"S 0505-G30Y","hex":"#f4ffea"},{"code":"S 0505-G40Y","hex":"#f7ffe9"},{"code":"S 0505-G50Y","hex":"#faffea"},{"code":"S 0505-G60Y","hex":"#fcffeb"},{"code":"S 0505-G70Y","hex":"#feffeb"},{"code":"S 0505-G80Y","hex":"#fffeec"},{"code":"S 0505-G90Y","hex":"#fefdeb"},{"code":"S 0505-R","hex":"#ffdee0"},{"code":"S 0505-R10B","hex":"#ffdfe3"},{"code":"S 0505-R20B","hex":"#ffe1e7"},{"code":"S 0505-R30B","hex":"#ffe3ec"},{"code":"S 0505-R40B","hex":"#fee6f2"},{"code":"S 0505-R50B","hex":"#ffeafb"},{"code":"S 0505-R60B","hex":"#f8e9fe"},{"code":"S 0505-R70B","hex":"#f0e8ff"},{"code":"S 0505-R80B","hex":"#e1e6ff"},{"code":"S 0505-R90B","hex":"#e3ecfe"},{"code":"S 0505-Y","hex":"#fffbea"},{"code":"S 0505-Y10R","hex":"#fffaea"},{"code":"S 0505-Y20R","hex":"#fff7e9"},{"code":"S 0505-Y30R","hex":"#fff5e8"},{"code":"S 0505-Y40R","hex":"#fff3e6"},{"code":"S 0505-Y50R","hex":"#fff0e5"},{"code":"S 0505-Y60R","hex":"#feede4"},{"code":"S 0505-Y70R","hex":"#ffe9e2"},{"code":"S 0505-Y80R","hex":"#fee5e0"},{"code":"S 0505-Y90R","hex":"#ffe2e0"},{"code":"S 0507-B","hex":"#dff3ff"},{"code":"S 0507-B20G","hex":"#e3faff"},{"code":"S 0507-B80G","hex":"#e2fff8"},{"code":"S 0507-G","hex":"#dbffe9"},{"code":"S 0507-G40Y","hex":"#f4ffe0"},{"code":"S 0507-G80Y","hex":"#fffee4"},{"code":"S 0507-R","hex":"#ffd2d5"},{"code":"S 0507-R20B","hex":"#ffd6de"},{"code":"S 0507-R40B","hex":"#ffddee"},{"code":"S 0507-R60B","hex":"#f5e0ff"},{"code":"S 0507-R80B","hex":"#d6ddff"},{"code":"S 0507-Y","hex":"#fffae3"},{"code":"S 0507-Y20R","hex":"#fff5e0"},{"code":"S 0507-Y40R","hex":"#ffeedd"},{"code":"S 0507-Y60R","hex":"#ffe6da"},{"code":"S 0507-Y80R","hex":"#fedcd5"},{"code":"S 0510-B","hex":"#d3eeff"},{"code":"S 0510-B10G","hex":"#d6f4ff"},{"code":"S 0510-B30G","hex":"#d9fbff"},{"code":"S 0510-B50G","hex":"#dafffd"},{"code":"S 0510-B70G","hex":"#d8fef9"},{"code":"S 0510-B90G","hex":"#d1ffea"},{"code":"S 0510-G","hex":"#cdffe0"},{"code":"S 0510-G10Y","hex":"#dbffdc"},{"code":"S 0510-G20Y","hex":"#e4ffd9"},{"code":"S 0510-G30Y","hex":"#ebffd6"},{"code":"S 0510-G40Y","hex":"#f0ffd4"},{"code":"S 0510-G50Y","hex":"#f5ffd6"},{"code":"S 0510-G60Y","hex":"#f9ffd8"},{"code":"S 0510-G70Y","hex":"#fdffd9"},{"code":"S 0510-G80Y","hex":"#fffdda"},{"code":"S 0510-G90Y","hex":"#fefbd8"},{"code":"S 0510-R","hex":"#ffc2c6"},{"code":"S 0510-R10B","hex":"#ffc4cb"},{"code":"S 0510-R20B","hex":"#ffc7d2"},{"code":"S 0510-R30B","hex":"#ffcbdb"},{"code":"S 0510-R40B","hex":"#ffd0e7"},{"code":"S 0510-R50B","hex":"#ffd7f7"},{"code":"S 0510-R60B","hex":"#f1d4ff"},{"code":"S 0510-R70B","hex":"#e3d4ff"},{"code":"S 0510-R80B","hex":"#c6d1ff"},{"code":"S 0510-R90B","hex":"#cbdcff"},{"code":"S 0510-Y","hex":"#fff9d8"},{"code":"S 0510-Y10R","hex":"#fff5d6"},{"code":"S 0510-Y20R","hex":"#fff1d4"},{"code":"S 0510-Y30R","hex":"#ffedd2"},{"code":"S 0510-Y40R","hex":"#ffe8d0"},{"code":"S 0510-Y50R","hex":"#ffe3ce"},{"code":"S 0510-Y60R","hex":"#ffddcc"},{"code":"S 0510-Y70R","hex":"#fed7c9"},{"code":"S 0510-Y80R","hex":"#ffd0c6"},{"code":"S 0510-Y90R","hex":"#ffc9c5"},{"code":"S 0515-B","hex":"#c0e7ff"},{"code":"S 0515-B20G","hex":"#c5f5fe"},{"code":"S 0515-B50G","hex":"#c8fffd"},{"code":"S 0515-B80G","hex":"#c4fff1"},{"code":"S 0515-G","hex":"#b8ffd4"},{"code":"S 0515-G20Y","hex":"#d8ffc8"},{"code":"S 0515-G40Y","hex":"#eaffc1"},{"code":"S 0515-G60Y","hex":"#f7fec6"},{"code":"S 0515-G80Y","hex":"#fffdc8"},{"code":"S 0515-G90Y","hex":"#fff9c7"},{"code":"S 0515-R","hex":"#feaaaf"},{"code":"S 0515-R10B","hex":"#feadb7"},{"code":"S 0515-R20B","hex":"#feb1c0"},{"code":"S 0515-R40B","hex":"#ffbcdd"},{"code":"S 0515-R60B","hex":"#ebc1ff"},{"code":"S 0515-R80B","hex":"#b0befe"},{"code":"S 0515-R90B","hex":"#b6ceff"},{"code":"S 0515-Y","hex":"#fff6c6"},{"code":"S 0515-Y10R","hex":"#fff1c4"},{"code":"S 0515-Y20R","hex":"#ffebc1"},{"code":"S 0515-Y30R","hex":"#ffe5bf"},{"code":"S 0515-Y40R","hex":"#ffdebc"},{"code":"S 0515-Y50R","hex":"#ffd7b9"},{"code":"S 0515-Y60R","hex":"#ffcfb6"},{"code":"S 0515-Y70R","hex":"#ffc6b3"},{"code":"S 0515-Y80R","hex":"#ffbdaf"},{"code":"S 0515-Y90R","hex":"#ffb4ae"},{"code":"S 0520-B","hex":"#aee0ff"},{"code":"S 0520-B10G","hex":"#b2ebff"},{"code":"S 0520-B30G","hex":"#b6f8ff"},{"code":"S 0520-B40G","hex":"#b8fcff"},{"code":"S 0520-B50G","hex":"#b8fffc"},{"code":"S 0520-B60G","hex":"#b7fff8"},{"code":"S 0520-B70G","hex":"#b5fff4"},{"code":"S 0520-B90G","hex":"#abffd9"},{"code":"S 0520-G","hex":"#a5ffc8"},{"code":"S 0520-G10Y","hex":"#bdffc0"},{"code":"S 0520-G20Y","hex":"#cdffb9"},{"code":"S 0520-G30Y","hex":"#daffb4"},{"code":"S 0520-G40Y","hex":"#e4ffb0"},{"code":"S 0520-G50Y","hex":"#edffb3"},{"code":"S 0520-G60Y","hex":"#f4feb5"},{"code":"S 0520-G70Y","hex":"#fbffb8"},{"code":"S 0520-G80Y","hex":"#fffcb8"},{"code":"S 0520-G90Y","hex":"#fff7b6"},{"code":"S 0520-R","hex":"#ff959c"},{"code":"S 0520-R10B","hex":"#ff98a5"},{"code":"S 0520-R20B","hex":"#ff9cb0"},{"code":"S 0520-R30B","hex":"#ffa2c0"},{"code":"S 0520-R40B","hex":"#ffa9d4"},{"code":"S 0520-R50B","hex":"#ffb4f0"},{"code":"S 0520-R60B","hex":"#e5b0ff"},{"code":"S 0520-R70B","hex":"#cbb0ff"},{"code":"S 0520-R80B","hex":"#9caeff"},{"code":"S 0520-R90B","hex":"#a3c1ff"},{"code":"S 0520-Y","hex":"#fff3b5"},{"code":"S 0520-Y10R","hex":"#feedb2"},{"code":"S 0520-Y20R","hex":"#ffe5b0"},{"code":"S 0520-Y30R","hex":"#ffddad"},{"code":"S 0520-Y40R","hex":"#ffd5aa"},{"code":"S 0520-Y50R","hex":"#ffcca7"},{"code":"S 0520-Y60R","hex":"#ffc2a3"},{"code":"S 0520-Y70R","hex":"#ffb89f"},{"code":"S 0520-Y80R","hex":"#ffac9b"},{"code":"S 0520-Y90R","hex":"#ffa19b"},{"code":"S 0525-R60B","hex":"#e09ffe"},{"code":"S 0525-R70B","hex":"#c1a0ff"},{"code":"S 0530-B","hex":"#8ed4ff"},{"code":"S 0530-B10G","hex":"#92e3ff"},{"code":"S 0530-B30G","hex":"#98f5ff"},{"code":"S 0530-B40G","hex":"#9afcff"},{"code":"S 0530-B50G","hex":"#9afffb"},{"code":"S 0530-B60G","hex":"#98fff5"},{"code":"S 0530-B70G","hex":"#96ffef"},{"code":"S 0530-B90G","hex":"#8bffcb"},{"code":"S 0530-G","hex":"#84ffb4"},{"code":"S 0530-G10Y","hex":"#a3ffa7"},{"code":"S 0530-G20Y","hex":"#baff9e"},{"code":"S 0530-G30Y","hex":"#cbff96"},{"code":"S 0530-G40Y","hex":"#dafe90"},{"code":"S 0530-G50Y","hex":"#e6ff93"},{"code":"S 0530-G60Y","hex":"#f0fe96"},{"code":"S 0530-G70Y","hex":"#f9fe99"},{"code":"S 0530-G80Y","hex":"#fefb9a"},{"code":"S 0530-G90Y","hex":"#fef598"},{"code":"S 0530-R","hex":"#ff737c"},{"code":"S 0530-R10B","hex":"#ff7687"},{"code":"S 0530-R20B","hex":"#fe7b95"},{"code":"S 0530-R30B","hex":"#ff81a9"},{"code":"S 0530-R40B","hex":"#fe89c4"},{"code":"S 0530-R50B","hex":"#ff95ea"},{"code":"S 0530-R80B","hex":"#7a92ff"},{"code":"S 0530-R90B","hex":"#81aaff"},{"code":"S 0530-Y","hex":"#ffef96"},{"code":"S 0530-Y10R","hex":"#fee593"},{"code":"S 0530-Y20R","hex":"#ffdb90"},{"code":"S 0530-Y30R","hex":"#ffd08d"},{"code":"S 0530-Y40R","hex":"#fec589"},{"code":"S 0530-Y50R","hex":"#feb986"},{"code":"S 0530-Y60R","hex":"#feac82"},{"code":"S 0530-Y70R","hex":"#ff9f7e"},{"code":"S 0530-Y80R","hex":"#ff9079"},{"code":"S 0530-Y90R","hex":"#ff8279"},{"code":"S 0540-B","hex":"#72caff"},{"code":"S 0540-B10G","hex":"#76dcff"},{"code":"S 0540-B30G","hex":"#7cf2ff"},{"code":"S 0540-G","hex":"#68ffa3"},{"code":"S 0540-G10Y","hex":"#8eff93"},{"code":"S 0540-G20Y","hex":"#a9ff86"},{"code":"S 0540-G30Y","hex":"#bfff7c"},{"code":"S 0540-G40Y","hex":"#d0ff74"},{"code":"S 0540-G50Y","hex":"#dfff77"},{"code":"S 0540-G60Y","hex":"#ecff7a"},{"code":"S 0540-G70Y","hex":"#f8fe7d"},{"code":"S 0540-G80Y","hex":"#fffa7e"},{"code":"S 0540-G90Y","hex":"#fff27c"},{"code":"S 0540-R","hex":"#ff5863"},{"code":"S 0540-R10B","hex":"#ff5b6f"},{"code":"S 0540-R20B","hex":"#ff5f7f"},{"code":"S 0540-R30B","hex":"#ff6596"},{"code":"S 0540-R90B","hex":"#6598ff"},{"code":"S 0540-Y","hex":"#ffeb7a"},{"code":"S 0540-Y10R","hex":"#ffdf77"},{"code":"S 0540-Y20R","hex":"#ffd274"},{"code":"S 0540-Y30R","hex":"#ffc571"},{"code":"S 0540-Y40R","hex":"#ffb76d"},{"code":"S 0540-Y50R","hex":"#ffa96a"},{"code":"S 0540-Y60R","hex":"#fe9a66"},{"code":"S 0540-Y70R","hex":"#ff8a62"},{"code":"S 0540-Y80R","hex":"#ff7a5e"},{"code":"S 0540-Y90R","hex":"#ff695f"},{"code":"S 0550-G10Y","hex":"#7bff81"},{"code":"S 0550-G20Y","hex":"#9aff71"},{"code":"S 0550-G30Y","hex":"#b3ff65"},{"code":"S 0550-G40Y","hex":"#c8ff5b"},{"code":"S 0550-G50Y","hex":"#daff5e"},{"code":"S 0550-G60Y","hex":"#e8ff61"},{"code":"S 0550-G70Y","hex":"#f7ff64"},{"code":"S 0550-G80Y","hex":"#fffa65"},{"code":"S 0550-G90Y","hex":"#ffef62"},{"code":"S 0550-R","hex":"#ff424e"},{"code":"S 0550-R10B","hex":"#ff455b"},{"code":"S 0550-Y","hex":"#ffe761"},{"code":"S 0550-Y10R","hex":"#ffd95e"},{"code":"S 0550-Y20R","hex":"#ffca5b"},{"code":"S 0550-Y30R","hex":"#ffbb58"},{"code":"S 0550-Y40R","hex":"#ffab55"},{"code":"S 0550-Y50R","hex":"#ff9b52"},{"code":"S 0550-Y60R","hex":"#ff8a4e"},{"code":"S 0550-Y70R","hex":"#fe794b"},{"code":"S 0550-Y80R","hex":"#ff6747"},{"code":"S 0550-Y90R","hex":"#ff5549"},{"code":"S 0560-G10Y","hex":"#6bff71"},{"code":"S 0560-G20Y","hex":"#8eff5f"},{"code":"S 0560-G30Y","hex":"#a9ff51"},{"code":"S 0560-G40Y","hex":"#c0ff45"},{"code":"S 0560-G50Y","hex":"#d4ff47"},{"code":"S 0560-G60Y","hex":"#e5ff4a"},{"code":"S 0560-G70Y","hex":"#f6ff4d"},{"code":"S 0560-G80Y","hex":"#fff94d"},{"code":"S 0560-G90Y","hex":"#ffed4b"},{"code":"S 0560-R","hex":"#ff303e"},{"code":"S 0560-Y","hex":"#fee34a"},{"code":"S 0560-Y10R","hex":"#ffd347"},{"code":"S 0560-Y20R","hex":"#ffc345"},{"code":"S 0560-Y30R","hex":"#ffb242"},{"code":"S 0560-Y40R","hex":"#ffa140"},{"code":"S 0560-Y50R","hex":"#fe8f3d"},{"code":"S 0560-Y60R","hex":"#ff7d3a"},{"code":"S 0560-Y70R","hex":"#ff6a37"},{"code":"S 0560-Y80R","hex":"#ff5734"},{"code":"S 0560-Y90R","hex":"#ff4437"},{"code":"S 0565-G10Y","hex":"#63ff6a"},{"code":"S 0565-G50Y","hex":"#d2ff3d"},{"code":"S 0565-R","hex":"#ff2836"},{"code":"S 0570-G20Y","hex":"#82ff4f"},{"code":"S 0570-G30Y","hex":"#a1ff3f"},{"code":"S 0570-G40Y","hex":"#baff31"},{"code":"S 0570-G60Y","hex":"#e2ff35"},{"code":"S 0570-G70Y","hex":"#f4ff37"},{"code":"S 0570-G80Y","hex":"#fff837"},{"code":"S 0570-G90Y","hex":"#ffeb36"},{"code":"S 0570-Y","hex":"#ffe035"},{"code":"S 0570-Y10R","hex":"#ffcf33"},{"code":"S 0570-Y20R","hex":"#ffbd31"},{"code":"S 0570-Y30R","hex":"#feaa2f"},{"code":"S 0570-Y40R","hex":"#fe982d"},{"code":"S 0570-Y50R","hex":"#ff852b"},{"code":"S 0570-Y60R","hex":"#ff7128"},{"code":"S 0570-Y70R","hex":"#ff5e26"},{"code":"S 0570-Y80R","hex":"#fe4a24"},{"code":"S 0570-Y90R","hex":"#fe3628"},{"code":"S 0575-G20Y","hex":"#7dff48"},{"code":"S 0575-G40Y","hex":"#b7ff28"},{"code":"S 0575-G60Y","hex":"#e1ff2b"},{"code":"S 0575-G70Y","hex":"#f4ff2d"},{"code":"S 0575-G90Y","hex":"#ffea2c"},{"code":"S 0580-G30Y","hex":"#99ff2e"},{"code":"S 0580-Y","hex":"#ffdd22"},{"code":"S 0580-Y10R","hex":"#ffca20"},{"code":"S 0580-Y10R","hex":"#ffca20"},{"code":"S 0580-Y20R","hex":"#feb71f"},{"code":"S 0580-Y20R","hex":"#feb71f"},{"code":"S 0580-Y30R","hex":"#ffa31d"},{"code":"S 0580-Y40R","hex":"#ff901c"},{"code":"S 0580-Y50R","hex":"#ff7b1b"},{"code":"S 0580-Y60R","hex":"#ff6719"},{"code":"S 0580-Y70R","hex":"#ff5318"},{"code":"S 0580-Y80R","hex":"#ff3e16"},{"code":"S 0580-Y90R","hex":"#ff2a1b"},{"code":"S 0585-Y20R","hex":"#ffb416"},{"code":"S 0585-Y30R","hex":"#ffa015"},{"code":"S 0585-Y40R","hex":"#fe8c14"},{"code":"S 0585-Y50R","hex":"#ff7713"},{"code":"S 0585-Y60R","hex":"#ff6212"},{"code":"S 0585-Y70R","hex":"#ff4e11"},{"code":"S 0585-Y80R","hex":"#ff3910"},{"code":"S 0603-G40Y","hex":"#f7fcef"},{"code":"S 0603-G80Y","hex":"#fcfbf0"},{"code":"S 0603-R20B","hex":"#fceaed"},{"code":"S 0603-R40B","hex":"#fcedf4"},{"code":"S 0603-R60B","hex":"#f8effc"},{"code":"S 0603-R80B","hex":"#e9edfc"},{"code":"S 0603-Y20R","hex":"#fcf8ef"},{"code":"S 0603-Y40R","hex":"#fcf5ed"},{"code":"S 0603-Y60R","hex":"#fcf1eb"},{"code":"S 0603-Y80R","hex":"#fcede9"},{"code":"S 0804-B50G","hex":"#e8f6f6"},{"code":"S 0804-G20Y","hex":"#ecf6e7"},{"code":"S 0804-G60Y","hex":"#f4f6e7"},{"code":"S 0804-G90Y","hex":"#f6f5e7"},{"code":"S 0804-R10B","hex":"#f6dee1"},{"code":"S 0804-R30B","hex":"#f6e1e8"},{"code":"S 0804-R50B","hex":"#f6e7f3"},{"code":"S 0804-R70B","hex":"#ebe5f6"},{"code":"S 0804-R90B","hex":"#e1e8f6"},{"code":"S 0804-Y10R","hex":"#f6f3e6"},{"code":"S 0804-Y30R","hex":"#f6efe5"},{"code":"S 0804-Y50R","hex":"#f6ebe3"},{"code":"S 0804-Y70R","hex":"#f6e6e0"},{"code":"S 0804-Y90R","hex":"#f6e0de"},{"code":"S 0907-B20G","hex":"#d9eff4"},{"code":"S 0907-B80G","hex":"#d8f4ee"},{"code":"S 0907-G20Y","hex":"#e1f4da"},{"code":"S 0907-G60Y","hex":"#f0f4d9"},{"code":"S 0907-G90Y","hex":"#f4f1da"},{"code":"S 0907-R10B","hex":"#f4cbd0"},{"code":"S 0907-R30B","hex":"#f4d0db"},{"code":"S 0907-R50B","hex":"#f4d9ef"},{"code":"S 0907-R70B","hex":"#e0d6f4"},{"code":"S 0907-R90B","hex":"#d0dcf4"},{"code":"S 0907-Y10R","hex":"#f4edd8"},{"code":"S 0907-Y30R","hex":"#f4e8d5"},{"code":"S 0907-Y50R","hex":"#f4e1d2"},{"code":"S 0907-Y70R","hex":"#f4d8cf"},{"code":"S 0907-Y90R","hex":"#f4cecc"},{"code":"S 1000-N","hex":"#e5e5e5"},{"code":"S 1002-B","hex":"#e8eef1"},{"code":"S 1002-B","hex":"#e8eef1"},{"code":"S 1002-B50G","hex":"#eaf1f1"},{"code":"S 1002-G","hex":"#e7f1eb"},{"code":"S 1002-G","hex":"#e7f1eb"},{"code":"S 1002-G50Y","hex":"#eff1e9"},{"code":"S 1002-R","hex":"#f1e4e5"},{"code":"S 1002-R","hex":"#f1e4e5"},{"code":"S 1002-R50B","hex":"#f1e9f0"},{"code":"S 1002-Y","hex":"#f1f0e9"},{"code":"S 1002-Y50R","hex":"#f1ebe7"},{"code":"S 1005-B","hex":"#dce9f1"},{"code":"S 1005-B20G","hex":"#deeef1"},{"code":"S 1005-B50G","hex":"#dff1f1"},{"code":"S 1005-B80G","hex":"#ddf1ed"},{"code":"S 1005-B80G","hex":"#ddf1ed"},{"code":"S 1005-G","hex":"#d8f1e2"},{"code":"S 1005-G10Y","hex":"#dff1e0"},{"code":"S 1005-G20Y","hex":"#e4f1de"},{"code":"S 1005-G20Y","hex":"#e4f1de"},{"code":"S 1005-G30Y","hex":"#e7f1dd"},{"code":"S 1005-G40Y","hex":"#eaf1dc"},{"code":"S 1005-G50Y","hex":"#edf1dd"},{"code":"S 1005-G60Y","hex":"#eef1de"},{"code":"S 1005-G70Y","hex":"#f0f1df"},{"code":"S 1005-G80Y","hex":"#f1f1df"},{"code":"S 1005-G80Y","hex":"#f1f1df"},{"code":"S 1005-G90Y","hex":"#f1efdf"},{"code":"S 1005-R","hex":"#f1d2d4"},{"code":"S 1005-R10B","hex":"#f1d3d7"},{"code":"S 1005-R20B","hex":"#f1d5db"},{"code":"S 1005-R20B","hex":"#f1d5db"},{"code":"S 1005-R30B","hex":"#f1d7e0"},{"code":"S 1005-R40B","hex":"#f1dae6"},{"code":"S 1005-R50B","hex":"#f1deed"},{"code":"S 1005-R60B","hex":"#eadcf1"},{"code":"S 1005-R70B","hex":"#e3dcf1"},{"code":"S 1005-R80B","hex":"#d5daf1"},{"code":"S 1005-R90B","hex":"#d7e0f1"},{"code":"S 1005-Y","hex":"#f1eede"},{"code":"S 1005-Y10R","hex":"#f1ecdd"},{"code":"S 1005-Y20R","hex":"#f1eadc"},{"code":"S 1005-Y20R","hex":"#f1eadc"},{"code":"S 1005-Y30R","hex":"#f1e8db"},{"code":"S 1005-Y40R","hex":"#f1e6da"},{"code":"S 1005-Y50R","hex":"#f1e3d9"},{"code":"S 1005-Y60R","hex":"#f1e0d8"},{"code":"S 1005-Y70R","hex":"#f1ddd6"},{"code":"S 1005-Y80R","hex":"#f1d9d4"},{"code":"S 1005-Y90R","hex":"#f1d6d4"},{"code":"S 1010-B","hex":"#c8e2f1"},{"code":"S 1010-B10G","hex":"#cae7f1"},{"code":"S 1010-B30G","hex":"#cdeef1"},{"code":"S 1010-B50G","hex":"#cef1f0"},{"code":"S 1010-B70G","hex":"#ccf1ec"},{"code":"S 1010-B90G","hex":"#c6f1de"},{"code":"S 1010-G","hex":"#c2f1d5"},{"code":"S 1010-G10Y","hex":"#cff1d1"},{"code":"S 1010-G20Y","hex":"#d8f1cd"},{"code":"S 1010-G30Y","hex":"#def1cb"},{"code":"S 1010-G40Y","hex":"#e4f1c9"},{"code":"S 1010-G50Y","hex":"#e8f1cb"},{"code":"S 1010-G60Y","hex":"#ecf1cc"},{"code":"S 1010-G70Y","hex":"#eff1ce"},{"code":"S 1010-G80Y","hex":"#f1f0ce"},{"code":"S 1010-G90Y","hex":"#f1eecd"},{"code":"S 1010-R","hex":"#f1b8bb"},{"code":"S 1010-R10B","hex":"#f1bac0"},{"code":"S 1010-R20B","hex":"#f1bdc7"},{"code":"S 1010-R30B","hex":"#f1c0d0"},{"code":"S 1010-R40B","hex":"#f1c5db"},{"code":"S 1010-R50B","hex":"#f1ccea"},{"code":"S 1010-R60B","hex":"#e4c9f1"},{"code":"S 1010-R70B","hex":"#d7c8f1"},{"code":"S 1010-R80B","hex":"#bcc6f1"},{"code":"S 1010-R90B","hex":"#c1d1f1"},{"code":"S 1010-Y","hex":"#f1eccc"},{"code":"S 1010-Y10R","hex":"#f1e8cb"},{"code":"S 1010-Y20R","hex":"#f1e4c9"},{"code":"S 1010-Y30R","hex":"#f1e0c7"},{"code":"S 1010-Y40R","hex":"#f1dcc5"},{"code":"S 1010-Y50R","hex":"#f1d7c3"},{"code":"S 1010-Y60R","hex":"#f1d1c1"},{"code":"S 1010-Y70R","hex":"#f1cbbe"},{"code":"S 1010-Y80R","hex":"#f1c5bc"},{"code":"S 1010-Y90R","hex":"#f1bebb"},{"code":"S 1015-B","hex":"#b6dbf1"},{"code":"S 1015-B20G","hex":"#bbe8f1"},{"code":"S 1015-B50G","hex":"#bef1ef"},{"code":"S 1015-B80G","hex":"#baf1e5"},{"code":"S 1015-G","hex":"#aef1c8"},{"code":"S 1015-G20Y","hex":"#cdf1be"},{"code":"S 1015-G40Y","hex":"#def1b7"},{"code":"S 1015-G60Y","hex":"#eaf1bc"},{"code":"S 1015-G80Y","hex":"#f1f0be"},{"code":"S 1015-G90Y","hex":"#f1ecbd"},{"code":"S 1015-R","hex":"#f1a1a6"},{"code":"S 1015-R10B","hex":"#f1a4ad"},{"code":"S 1015-R20B","hex":"#f1a7b6"},{"code":"S 1015-R40B","hex":"#f1b2d2"},{"code":"S 1015-R60B","hex":"#dfb7f1"},{"code":"S 1015-R80B","hex":"#a6b4f1"},{"code":"S 1015-R90B","hex":"#acc3f1"},{"code":"S 1015-Y","hex":"#f1e9bb"},{"code":"S 1015-Y10R","hex":"#f1e4b9"},{"code":"S 1015-Y20R","hex":"#f1dfb7"},{"code":"S 1015-Y30R","hex":"#f1d9b5"},{"code":"S 1015-Y40R","hex":"#f1d2b2"},{"code":"S 1015-Y50R","hex":"#f1cbb0"},{"code":"S 1015-Y60R","hex":"#f1c4ad"},{"code":"S 1015-Y70R","hex":"#f1bca9"},{"code":"S 1015-Y80R","hex":"#f1b3a6"},{"code":"S 1015-Y90R","hex":"#f1aaa5"},{"code":"S 1020-B","hex":"#a5d5f1"},{"code":"S 1020-B10G","hex":"#a8dff1"},{"code":"S 1020-B30G","hex":"#adebf1"},{"code":"S 1020-B40G","hex":"#afeff1"},{"code":"S 1020-B50G","hex":"#aef1ef"},{"code":"S 1020-B60G","hex":"#adf1eb"},{"code":"S 1020-B70G","hex":"#abf1e7"},{"code":"S 1020-B90G","hex":"#a2f1ce"},{"code":"S 1020-G","hex":"#9cf1bd"},{"code":"S 1020-G10Y","hex":"#b3f1b5"},{"code":"S 1020-G20Y","hex":"#c2f1af"},{"code":"S 1020-G30Y","hex":"#cef1aa"},{"code":"S 1020-G40Y","hex":"#d8f1a6"},{"code":"S 1020-G50Y","hex":"#e1f1a9"},{"code":"S 1020-G60Y","hex":"#e7f1ac"},{"code":"S 1020-G70Y","hex":"#eef1ae"},{"code":"S 1020-G80Y","hex":"#f1efae"},{"code":"S 1020-G90Y","hex":"#f1eaad"},{"code":"S 1020-R","hex":"#f18d94"},{"code":"S 1020-R10B","hex":"#f1909c"},{"code":"S 1020-R20B","hex":"#f194a7"},{"code":"S 1020-R30B","hex":"#f19ab5"},{"code":"S 1020-R40B","hex":"#f1a1c9"},{"code":"S 1020-R50B","hex":"#f1aae4"},{"code":"S 1020-R60B","hex":"#d9a7f1"},{"code":"S 1020-R70B","hex":"#c1a7f1"},{"code":"S 1020-R80B","hex":"#93a5f1"},{"code":"S 1020-R90B","hex":"#9ab7f1"},{"code":"S 1020-Y","hex":"#f1e7ab"},{"code":"S 1020-Y10R","hex":"#f1e0a9"},{"code":"S 1020-Y20R","hex":"#f1d9a7"},{"code":"S 1020-Y30R","hex":"#f1d2a4"},{"code":"S 1020-Y40R","hex":"#f1caa1"},{"code":"S 1020-Y50R","hex":"#f1c19e"},{"code":"S 1020-Y60R","hex":"#f1b89a"},{"code":"S 1020-Y70R","hex":"#f1ae97"},{"code":"S 1020-Y80R","hex":"#f1a393"},{"code":"S 1020-Y90R","hex":"#f19892"},{"code":"S 1030-B","hex":"#86c9f1"},{"code":"S 1030-B10G","hex":"#8bd7f1"},{"code":"S 1030-B30G","hex":"#90e8f1"},{"code":"S 1030-B40G","hex":"#92eef1"},{"code":"S 1030-B50G","hex":"#92f1ee"},{"code":"S 1030-B60G","hex":"#90f1e8"},{"code":"S 1030-B70G","hex":"#8ef1e2"},{"code":"S 1030-B90G","hex":"#84f1c0"},{"code":"S 1030-G","hex":"#7df1aa"},{"code":"S 1030-G10Y","hex":"#9bf19f"},{"code":"S 1030-G20Y","hex":"#b0f195"},{"code":"S 1030-G30Y","hex":"#c1f18e"},{"code":"S 1030-G40Y","hex":"#cef188"},{"code":"S 1030-G50Y","hex":"#daf18b"},{"code":"S 1030-G60Y","hex":"#e3f18e"},{"code":"S 1030-G70Y","hex":"#ecf191"},{"code":"S 1030-G80Y","hex":"#f1ee92"},{"code":"S 1030-G90Y","hex":"#f1e890"},{"code":"S 1030-R","hex":"#f16d76"},{"code":"S 1030-R10B","hex":"#f17080"},{"code":"S 1030-R20B","hex":"#f1748d"},{"code":"S 1030-R30B","hex":"#f17aa0"},{"code":"S 1030-R40B","hex":"#f182b9"},{"code":"S 1030-R50B","hex":"#f18dde"},{"code":"S 1030-R60B","hex":"#d088f1"},{"code":"S 1030-R70B","hex":"#ae8af1"},{"code":"S 1030-R80B","hex":"#738bf1"},{"code":"S 1030-R90B","hex":"#7aa1f1"},{"code":"S 1030-Y","hex":"#f1e28e"},{"code":"S 1030-Y10R","hex":"#f1d98b"},{"code":"S 1030-Y20R","hex":"#f1d088"},{"code":"S 1030-Y30R","hex":"#f1c585"},{"code":"S 1030-Y40R","hex":"#f1bb82"},{"code":"S 1030-Y50R","hex":"#f1af7f"},{"code":"S 1030-Y60R","hex":"#f1a37b"},{"code":"S 1030-Y70R","hex":"#f19677"},{"code":"S 1030-Y80R","hex":"#f18973"},{"code":"S 1030-Y90R","hex":"#f17b73"},{"code":"S 1040-B","hex":"#6cbff1"},{"code":"S 1040-B10G","hex":"#70d0f1"},{"code":"S 1040-B20G","hex":"#73dcf1"},{"code":"S 1040-B30G","hex":"#75e5f1"},{"code":"S 1040-B40G","hex":"#77eef1"},{"code":"S 1040-B50G","hex":"#77f1ed"},{"code":"S 1040-B60G","hex":"#75f1e6"},{"code":"S 1040-B70G","hex":"#74f1de"},{"code":"S 1040-B80G","hex":"#71f1d5"},{"code":"S 1040-B90G","hex":"#69f1b4"},{"code":"S 1040-G","hex":"#63f19a"},{"code":"S 1040-G10Y","hex":"#86f18b"},{"code":"S 1040-G20Y","hex":"#a0f17f"},{"code":"S 1040-G30Y","hex":"#b5f175"},{"code":"S 1040-G40Y","hex":"#c5f16e"},{"code":"S 1040-G50Y","hex":"#d4f171"},{"code":"S 1040-G60Y","hex":"#dff174"},{"code":"S 1040-G70Y","hex":"#ebf177"},{"code":"S 1040-G80Y","hex":"#f1ed77"},{"code":"S 1040-G90Y","hex":"#f1e575"},{"code":"S 1040-R","hex":"#f1535e"},{"code":"S 1040-R10B","hex":"#f15669"},{"code":"S 1040-R20B","hex":"#f15a79"},{"code":"S 1040-R30B","hex":"#f1608e"},{"code":"S 1040-R40B","hex":"#f167ac"},{"code":"S 1040-R50B","hex":"#f172d9"},{"code":"S 1040-R60B","hex":"#c76ef1"},{"code":"S 1040-R70B","hex":"#9e72f1"},{"code":"S 1040-R80B","hex":"#5975f1"},{"code":"S 1040-R90B","hex":"#6090f1"},{"code":"S 1040-Y","hex":"#f1de74"},{"code":"S 1040-Y10R","hex":"#f1d371"},{"code":"S 1040-Y20R","hex":"#f1c76e"},{"code":"S 1040-Y30R","hex":"#f1bb6b"},{"code":"S 1040-Y40R","hex":"#f1ae67"},{"code":"S 1040-Y50R","hex":"#f1a064"},{"code":"S 1040-Y60R","hex":"#f19260"},{"code":"S 1040-Y70R","hex":"#f1835d"},{"code":"S 1040-Y80R","hex":"#f17359"},{"code":"S 1040-Y90R","hex":"#f1645a"},{"code":"S 1050-B","hex":"#54b6f1"},{"code":"S 1050-B10G","hex":"#58caf1"},{"code":"S 1050-B20G","hex":"#5bd8f1"},{"code":"S 1050-B30G","hex":"#5de3f1"},{"code":"S 1050-B40G","hex":"#5fedf1"},{"code":"S 1050-B50G","hex":"#5ff1ec"},{"code":"S 1050-B60G","hex":"#5df1e3"},{"code":"S 1050-B70G","hex":"#5cf1db"},{"code":"S 1050-B80G","hex":"#59f1cf"},{"code":"S 1050-B90G","hex":"#52f1aa"},{"code":"S 1050-G","hex":"#4cf18c"},{"code":"S 1050-G10Y","hex":"#75f17a"},{"code":"S 1050-G10Y","hex":"#75f17a"},{"code":"S 1050-G20Y","hex":"#92f16b"},{"code":"S 1050-G20Y","hex":"#92f16b"},{"code":"S 1050-G30Y","hex":"#aaf160"},{"code":"S 1050-G40Y","hex":"#bdf156"},{"code":"S 1050-G50Y","hex":"#cef159"},{"code":"S 1050-G60Y","hex":"#dcf15c"},{"code":"S 1050-G70Y","hex":"#eaf15f"},{"code":"S 1050-G80Y","hex":"#f1ed5f"},{"code":"S 1050-G90Y","hex":"#f1e35d"},{"code":"S 1050-R","hex":"#f13f4a"},{"code":"S 1050-R10B","hex":"#f14157"},{"code":"S 1050-R20B","hex":"#f14567"},{"code":"S 1050-R30B","hex":"#f1497f"},{"code":"S 1050-R40B","hex":"#f150a1"},{"code":"S 1050-R90B","hex":"#4a81f1"},{"code":"S 1050-Y","hex":"#f1db5c"},{"code":"S 1050-Y10R","hex":"#f1cd59"},{"code":"S 1050-Y20R","hex":"#f1c056"},{"code":"S 1050-Y30R","hex":"#f1b153"},{"code":"S 1050-Y40R","hex":"#f1a250"},{"code":"S 1050-Y50R","hex":"#f1934d"},{"code":"S 1050-Y60R","hex":"#f1834a"},{"code":"S 1050-Y70R","hex":"#f17247"},{"code":"S 1050-Y80R","hex":"#f16143"},{"code":"S 1050-Y90R","hex":"#f15045"},{"code":"S 1055-B90G","hex":"#47f1a5"},{"code":"S 1060-B","hex":"#40aff1"},{"code":"S 1060-G","hex":"#39f180"},{"code":"S 1060-G10Y","hex":"#65f16b"},{"code":"S 1060-G20Y","hex":"#86f15a"},{"code":"S 1060-G30Y","hex":"#a0f14c"},{"code":"S 1060-G40Y","hex":"#b6f141"},{"code":"S 1060-G50Y","hex":"#c9f144"},{"code":"S 1060-G60Y","hex":"#d9f146"},{"code":"S 1060-G70Y","hex":"#e9f149"},{"code":"S 1060-G80Y","hex":"#f1ec49"},{"code":"S 1060-G90Y","hex":"#f1e147"},{"code":"S 1060-R","hex":"#f12e3a"},{"code":"S 1060-R10B","hex":"#f13047"},{"code":"S 1060-R20B","hex":"#f13259"},{"code":"S 1060-R30B","hex":"#f13672"},{"code":"S 1060-Y","hex":"#f1d746"},{"code":"S 1060-Y10R","hex":"#f1c844"},{"code":"S 1060-Y20R","hex":"#f1b941"},{"code":"S 1060-Y30R","hex":"#f1a93f"},{"code":"S 1060-Y40R","hex":"#f1983c"},{"code":"S 1060-Y50R","hex":"#f1883a"},{"code":"S 1060-Y60R","hex":"#f17637"},{"code":"S 1060-Y70R","hex":"#f16534"},{"code":"S 1060-Y80R","hex":"#f15331"},{"code":"S 1060-Y90R","hex":"#f14134"},{"code":"S 1070-G10Y","hex":"#57f15e"},{"code":"S 1070-G20Y","hex":"#7bf14b"},{"code":"S 1070-G30Y","hex":"#98f13b"},{"code":"S 1070-G40Y","hex":"#b0f12e"},{"code":"S 1070-G50Y","hex":"#c5f130"},{"code":"S 1070-G60Y","hex":"#d6f132"},{"code":"S 1070-G70Y","hex":"#e8f134"},{"code":"S 1070-G80Y","hex":"#f1eb35"},{"code":"S 1070-G90Y","hex":"#f1df33"},{"code":"S 1070-R","hex":"#f11f2d"},{"code":"S 1070-R10B","hex":"#f1213a"},{"code":"S 1070-R20B","hex":"#f1234c"},{"code":"S 1070-Y","hex":"#f1d432"},{"code":"S 1070-Y10R","hex":"#f1c430"},{"code":"S 1070-Y20R","hex":"#f1b32e"},{"code":"S 1070-Y30R","hex":"#f1a12c"},{"code":"S 1070-Y40R","hex":"#f1902a"},{"code":"S 1070-Y50R","hex":"#f17e28"},{"code":"S 1070-Y60R","hex":"#f16b26"},{"code":"S 1070-Y70R","hex":"#f15924"},{"code":"S 1070-Y80R","hex":"#f14622"},{"code":"S 1070-Y90R","hex":"#f13326"},{"code":"S 1075-G20Y","hex":"#76f144"},{"code":"S 1075-G40Y","hex":"#adf126"},{"code":"S 1075-G50Y","hex":"#c3f127"},{"code":"S 1075-G60Y","hex":"#d5f129"},{"code":"S 1075-G70Y","hex":"#e7f12b"},{"code":"S 1075-G80Y","hex":"#f1eb2b"},{"code":"S 1075-G90Y","hex":"#f1de2a"},{"code":"S 1080-G30Y","hex":"#91f12c"},{"code":"S 1080-R","hex":"#f11322"},{"code":"S 1080-Y","hex":"#f1d220"},{"code":"S 1080-Y10R","hex":"#f1c01e"},{"code":"S 1080-Y20R","hex":"#f1ad1d"},{"code":"S 1080-Y30R","hex":"#f19b1c"},{"code":"S 1080-Y40R","hex":"#f1881b"},{"code":"S 1080-Y50R","hex":"#f17519"},{"code":"S 1080-Y60R","hex":"#f16218"},{"code":"S 1080-Y70R","hex":"#f14e16"},{"code":"S 1080-Y80R","hex":"#f13b15"},{"code":"S 1080-Y90R","hex":"#f1281a"},{"code":"S 1085-Y80R","hex":"#f1360f"},{"code":"S 1085-Y90R","hex":"#f12214"},{"code":"S 1500-N","hex":"#d8d8d8"},{"code":"S 1502-B","hex":"#dbe1e4"},{"code":"S 1502-B50G","hex":"#dde4e3"},{"code":"S 1502-G","hex":"#dae4de"},{"code":"S 1502-G50Y","hex":"#e2e4dc"},{"code":"S 1502-R","hex":"#e4d7d8"},{"code":"S 1502-R50B","hex":"#e4dce2"},{"code":"S 1502-Y","hex":"#e4e3dc"},{"code":"S 1502-Y50R","hex":"#e4deda"},{"code":"S 1505-G80Y","hex":"#e4e3d3"},{"code":"S 1505-G90Y","hex":"#e4e2d2"},{"code":"S 1505-Y","hex":"#e4e1d2"},{"code":"S 1505-Y10R","hex":"#e4dfd1"},{"code":"S 1505-Y20R","hex":"#e4ddd0"},{"code":"S 1505-Y30R","hex":"#e4dbcf"},{"code":"S 1505-Y40R","hex":"#e4d9ce"},{"code":"S 1505-Y50R","hex":"#e4d7cd"},{"code":"S 1505-Y60R","hex":"#e4d4cc"},{"code":"S 1505-Y70R","hex":"#e4d1ca"},{"code":"S 1505-Y80R","hex":"#e4cdc9"},{"code":"S 1505-Y90R","hex":"#e4cac8"},{"code":"S 1510-B","hex":"#bdd5e4"},{"code":"S 1510-B20G","hex":"#c1dee4"},{"code":"S 1510-B50G","hex":"#c3e4e3"},{"code":"S 1510-B80G","hex":"#c0e4dc"},{"code":"S 1510-G","hex":"#b8e4c9"},{"code":"S 1510-G20Y","hex":"#cce4c2"},{"code":"S 1510-G40Y","hex":"#d7e4be"},{"code":"S 1510-G60Y","hex":"#dfe4c1"},{"code":"S 1510-G80Y","hex":"#e4e3c3"},{"code":"S 1510-G90Y","hex":"#e4e0c2"},{"code":"S 1510-R","hex":"#e4adb1"},{"code":"S 1510-R10B","hex":"#e4afb6"},{"code":"S 1510-R20B","hex":"#e4b2bc"},{"code":"S 1510-R40B","hex":"#e4bacf"},{"code":"S 1510-R60B","hex":"#d8bee4"},{"code":"S 1510-R80B","hex":"#b2bbe4"},{"code":"S 1510-R90B","hex":"#b6c5e4"},{"code":"S 1510-Y","hex":"#e4dec1"},{"code":"S 1510-Y10R","hex":"#e4dbbf"},{"code":"S 1510-Y20R","hex":"#e4d8be"},{"code":"S 1510-Y30R","hex":"#e4d4bc"},{"code":"S 1510-Y40R","hex":"#e4d0ba"},{"code":"S 1510-Y50R","hex":"#e4cbb8"},{"code":"S 1510-Y60R","hex":"#e4c6b6"},{"code":"S 1510-Y70R","hex":"#e4c0b4"},{"code":"S 1510-Y80R","hex":"#e4bab1"},{"code":"S 1510-Y90R","hex":"#e4b4b1"},{"code":"S 1515-B","hex":"#accfe4"},{"code":"S 1515-B20G","hex":"#b1dbe4"},{"code":"S 1515-B50G","hex":"#b3e4e2"},{"code":"S 1515-B80G","hex":"#afe4d8"},{"code":"S 1515-G","hex":"#a5e4bd"},{"code":"S 1515-G20Y","hex":"#c1e4b3"},{"code":"S 1515-G40Y","hex":"#d1e4ad"},{"code":"S 1515-G60Y","hex":"#dde4b1"},{"code":"S 1515-G80Y","hex":"#e4e2b3"},{"code":"S 1515-G90Y","hex":"#e4dfb2"},{"code":"S 1515-R","hex":"#e4989d"},{"code":"S 1515-R10B","hex":"#e49ba3"},{"code":"S 1515-R20B","hex":"#e49eac"},{"code":"S 1515-R40B","hex":"#e4a8c6"},{"code":"S 1515-R60B","hex":"#d2ade4"},{"code":"S 1515-R80B","hex":"#9daae4"},{"code":"S 1515-R90B","hex":"#a3b8e4"},{"code":"S 1515-Y","hex":"#e4dcb1"},{"code":"S 1515-Y10R","hex":"#e4d7af"},{"code":"S 1515-Y20R","hex":"#e4d2ad"},{"code":"S 1515-Y30R","hex":"#e4cdab"},{"code":"S 1515-Y40R","hex":"#e4c7a8"},{"code":"S 1515-Y50R","hex":"#e4c0a6"},{"code":"S 1515-Y60R","hex":"#e4b9a3"},{"code":"S 1515-Y70R","hex":"#e4b1a0"},{"code":"S 1515-Y80R","hex":"#e4a99d"},{"code":"S 1515-Y90R","hex":"#e4a19c"},{"code":"S 1550-R70B","hex":"#8857e4"},{"code":"S 1550-R80B","hex":"#405ee4"},{"code":"S 1555-B10G","hex":"#49bce4"},{"code":"S 1555-R70B","hex":"#824ee4"},{"code":"S 1555-R80B","hex":"#3757e4"},{"code":"S 1560-R90B","hex":"#346ee4"},{"code":"S 1565-B","hex":"#33a1e4"},{"code":"S 1565-G","hex":"#2de474"},{"code":"S 1575-R10B","hex":"#e41931"},{"code":"S 1580-R","hex":"#e41220"},{"code":"S 1580-Y80R","hex":"#e43814"},{"code":"S 1580-Y90R","hex":"#e42518"},{"code":"S 2000-N","hex":"#cccccc"},{"code":"S 2002-B","hex":"#cfd3d6"},{"code":"S 2002-B50G","hex":"#d0d6d6"},{"code":"S 2002-G","hex":"#cdd6d1"},{"code":"S 2002-G50Y","hex":"#d5d6cf"},{"code":"S 2002-R","hex":"#d6cbcb"},{"code":"S 2002-R50B","hex":"#d6cfd5"},{"code":"S 2002-Y","hex":"#d6d5cf"},{"code":"S 2002-Y50R","hex":"#d6d1cd"},{"code":"S 2005-B","hex":"#c3cfd6"},{"code":"S 2005-B20G","hex":"#c5d3d6"},{"code":"S 2005-B20G","hex":"#c5d3d6"},{"code":"S 2005-B50G","hex":"#c6d6d6"},{"code":"S 2005-B80G","hex":"#c5d6d2"},{"code":"S 2005-B80G","hex":"#c5d6d2"},{"code":"S 2005-G","hex":"#c0d6c9"},{"code":"S 2005-G10Y","hex":"#c6d6c7"},{"code":"S 2005-G20Y","hex":"#cbd6c6"},{"code":"S 2005-G20Y","hex":"#cbd6c6"},{"code":"S 2005-G30Y","hex":"#ced6c5"},{"code":"S 2005-G40Y","hex":"#d0d6c4"},{"code":"S 2005-G50Y","hex":"#d2d6c5"},{"code":"S 2005-G60Y","hex":"#d4d6c6"},{"code":"S 2005-G70Y","hex":"#d6d6c6"},{"code":"S 2005-G80Y","hex":"#d6d6c6"},{"code":"S 2005-G90Y","hex":"#d6d5c6"},{"code":"S 2005-R","hex":"#d6bbbc"},{"code":"S 2005-R10B","hex":"#d6bcbf"},{"code":"S 2005-R20B","hex":"#d6bdc2"},{"code":"S 2005-R20B","hex":"#d6bdc2"},{"code":"S 2005-R30B","hex":"#d6bfc7"},{"code":"S 2005-R40B","hex":"#d6c2cc"},{"code":"S 2005-R50B","hex":"#d6c5d3"},{"code":"S 2005-R50B","hex":"#d6c5d3"},{"code":"S 2005-R60B","hex":"#d0c4d6"},{"code":"S 2005-R70B","hex":"#cac3d6"},{"code":"S 2005-R80B","hex":"#bdc2d6"},{"code":"S 2005-R90B","hex":"#c0c7d6"},{"code":"S 2005-Y","hex":"#d6d4c5"},{"code":"S 2005-Y10R","hex":"#d6d2c5"},{"code":"S 2005-Y20R","hex":"#d6d0c4"},{"code":"S 2005-Y30R","hex":"#d6cfc3"},{"code":"S 2005-Y40R","hex":"#d6ccc2"},{"code":"S 2005-Y50R","hex":"#d6cac1"},{"code":"S 2005-Y50R","hex":"#d6cac1"},{"code":"S 2005-Y60R","hex":"#d6c7c0"},{"code":"S 2005-Y70R","hex":"#d6c4be"},{"code":"S 2005-Y80R","hex":"#d6c1bd"},{"code":"S 2005-Y90R","hex":"#d6bebc"},{"code":"S 2010-B","hex":"#b2c9d6"},{"code":"S 2010-B10G","hex":"#b4ced6"},{"code":"S 2010-B30G","hex":"#b6d3d6"},{"code":"S 2010-B50G","hex":"#b7d6d5"},{"code":"S 2010-B70G","hex":"#b6d6d1"},{"code":"S 2010-B90G","hex":"#b0d6c5"},{"code":"S 2010-G","hex":"#add6bd"},{"code":"S 2010-G10Y","hex":"#b8d6b9"},{"code":"S 2010-G20Y","hex":"#c0d6b7"},{"code":"S 2010-G30Y","hex":"#c6d6b4"},{"code":"S 2010-G40Y","hex":"#cad6b3"},{"code":"S 2010-G50Y","hex":"#ced6b4"},{"code":"S 2010-G60Y","hex":"#d2d6b6"},{"code":"S 2010-G70Y","hex":"#d5d6b7"},{"code":"S 2010-G80Y","hex":"#d6d5b7"},{"code":"S 2010-G90Y","hex":"#d6d3b6"},{"code":"S 2010-R","hex":"#d6a3a6"},{"code":"S 2010-R10B","hex":"#d6a5ab"},{"code":"S 2010-R20B","hex":"#d6a8b1"},{"code":"S 2010-R30B","hex":"#d6abb9"},{"code":"S 2010-R40B","hex":"#d6afc3"},{"code":"S 2010-R50B","hex":"#d6b5d0"},{"code":"S 2010-R60B","hex":"#cbb3d6"},{"code":"S 2010-R70B","hex":"#bfb2d6"},{"code":"S 2010-R80B","hex":"#a7b0d6"},{"code":"S 2010-R90B","hex":"#abb9d6"},{"code":"S 2010-Y","hex":"#d6d1b6"},{"code":"S 2010-Y10R","hex":"#d6ceb4"},{"code":"S 2010-Y20R","hex":"#d6cbb3"},{"code":"S 2010-Y30R","hex":"#d6c7b1"},{"code":"S 2010-Y40R","hex":"#d6c3af"},{"code":"S 2010-Y50R","hex":"#d6bfae"},{"code":"S 2010-Y60R","hex":"#d6baac"},{"code":"S 2010-Y70R","hex":"#d6b5a9"},{"code":"S 2010-Y80R","hex":"#d6afa7"},{"code":"S 2010-Y90R","hex":"#d6a9a6"},{"code":"S 2020-B","hex":"#92bdd6"},{"code":"S 2020-B10G","hex":"#96c6d6"},{"code":"S 2020-B30G","hex":"#9ad1d6"},{"code":"S 2020-B40G","hex":"#9bd5d6"},{"code":"S 2020-B50G","hex":"#9bd6d4"},{"code":"S 2020-B60G","hex":"#9ad6d1"},{"code":"S 2020-B70G","hex":"#98d6cd"},{"code":"S 2020-B90G","hex":"#90d6b7"},{"code":"S 2020-G","hex":"#8bd6a8"},{"code":"S 2020-G10Y","hex":"#9fd6a1"},{"code":"S 2020-G20Y","hex":"#add69c"},{"code":"S 2020-G30Y","hex":"#b7d697"},{"code":"S 2020-G40Y","hex":"#c0d694"},{"code":"S 2020-G50Y","hex":"#c8d696"},{"code":"S 2020-G60Y","hex":"#ced699"},{"code":"S 2020-G70Y","hex":"#d3d69b"},{"code":"S 2020-G80Y","hex":"#d6d49b"},{"code":"S 2020-G90Y","hex":"#d6d09a"},{"code":"S 2020-R","hex":"#d67e83"},{"code":"S 2020-R10B","hex":"#d6808b"},{"code":"S 2020-R20B","hex":"#d68494"},{"code":"S 2020-R30B","hex":"#d688a1"},{"code":"S 2020-R40B","hex":"#d68fb3"},{"code":"S 2020-R50B","hex":"#d697ca"},{"code":"S 2020-R60B","hex":"#c194d6"},{"code":"S 2020-R70B","hex":"#ab94d6"},{"code":"S 2020-R80B","hex":"#8392d6"},{"code":"S 2020-R90B","hex":"#89a2d6"},{"code":"S 2020-Y","hex":"#d6cd98"},{"code":"S 2020-Y10R","hex":"#d6c796"},{"code":"S 2020-Y20R","hex":"#d6c194"},{"code":"S 2020-Y30R","hex":"#d6bb92"},{"code":"S 2020-Y40R","hex":"#d6b38f"},{"code":"S 2020-Y50R","hex":"#d6ac8c"},{"code":"S 2020-Y60R","hex":"#d6a389"},{"code":"S 2020-Y70R","hex":"#d69b86"},{"code":"S 2020-Y80R","hex":"#d69183"},{"code":"S 2020-Y90R","hex":"#d68882"},{"code":"S 2030-B","hex":"#77b3d6"},{"code":"S 2030-B10G","hex":"#7bbfd6"},{"code":"S 2030-B30G","hex":"#80ced6"},{"code":"S 2030-B40G","hex":"#82d4d6"},{"code":"S 2030-B50G","hex":"#81d6d3"},{"code":"S 2030-B60G","hex":"#80d6ce"},{"code":"S 2030-B70G","hex":"#7ed6c9"},{"code":"S 2030-B90G","hex":"#75d6ab"},{"code":"S 2030-G","hex":"#6fd697"},{"code":"S 2030-G10Y","hex":"#8ad68d"},{"code":"S 2030-G20Y","hex":"#9cd685"},{"code":"S 2030-G30Y","hex":"#abd67e"},{"code":"S 2030-G40Y","hex":"#b7d679"},{"code":"S 2030-G50Y","hex":"#c2d67c"},{"code":"S 2030-G60Y","hex":"#cad67f"},{"code":"S 2030-G70Y","hex":"#d2d681"},{"code":"S 2030-G80Y","hex":"#d6d481"},{"code":"S 2030-G90Y","hex":"#d6ce80"},{"code":"S 2030-R","hex":"#d66168"},{"code":"S 2030-R10B","hex":"#d66471"},{"code":"S 2030-R20B","hex":"#d6677e"},{"code":"S 2030-R30B","hex":"#d66c8e"},{"code":"S 2030-R40B","hex":"#d673a5"},{"code":"S 2030-R50B","hex":"#d67dc5"},{"code":"S 2030-R60B","hex":"#b979d6"},{"code":"S 2030-R70B","hex":"#9b7bd6"},{"code":"S 2030-R80B","hex":"#677bd6"},{"code":"S 2030-R90B","hex":"#6d8fd6"},{"code":"S 2030-Y","hex":"#d6c97e"},{"code":"S 2030-Y10R","hex":"#d6c17c"},{"code":"S 2030-Y20R","hex":"#d6b979"},{"code":"S 2030-Y30R","hex":"#d6b076"},{"code":"S 2030-Y40R","hex":"#d6a674"},{"code":"S 2030-Y50R","hex":"#d69c70"},{"code":"S 2030-Y60R","hex":"#d6916d"},{"code":"S 2030-Y70R","hex":"#d6856a"},{"code":"S 2030-Y80R","hex":"#d67966"},{"code":"S 2030-Y90R","hex":"#d66d66"},{"code":"S 2040-B","hex":"#60aad6"},{"code":"S 2040-B10G","hex":"#64b9d6"},{"code":"S 2040-B20G","hex":"#66c4d6"},{"code":"S 2040-B30G","hex":"#68ccd6"},{"code":"S 2040-B40G","hex":"#6ad3d6"},{"code":"S 2040-B50G","hex":"#6ad6d3"},{"code":"S 2040-B60G","hex":"#68d6cc"},{"code":"S 2040-B70G","hex":"#67d6c6"},{"code":"S 2040-B80G","hex":"#65d6bd"},{"code":"S 2040-B90G","hex":"#5dd6a0"},{"code":"S 2040-G","hex":"#58d689"},{"code":"S 2040-G10Y","hex":"#77d67c"},{"code":"S 2040-G20Y","hex":"#8ed671"},{"code":"S 2040-G30Y","hex":"#a0d668"},{"code":"S 2040-G40Y","hex":"#afd661"},{"code":"S 2040-G50Y","hex":"#bcd664"},{"code":"S 2040-G60Y","hex":"#c7d667"},{"code":"S 2040-G70Y","hex":"#d1d66a"},{"code":"S 2040-G80Y","hex":"#d6d36a"},{"code":"S 2040-G90Y","hex":"#d6cc68"},{"code":"S 2040-R","hex":"#d64a53"},{"code":"S 2040-R10B","hex":"#d64d5d"},{"code":"S 2040-R20B","hex":"#d6506b"},{"code":"S 2040-R30B","hex":"#d6557e"},{"code":"S 2040-R40B","hex":"#d65c99"},{"code":"S 2040-R50B","hex":"#d666c1"},{"code":"S 2040-R60B","hex":"#b162d6"},{"code":"S 2040-R70B","hex":"#8d65d6"},{"code":"S 2040-R80B","hex":"#4f68d6"},{"code":"S 2040-R90B","hex":"#5580d6"},{"code":"S 2040-Y","hex":"#d6c667"},{"code":"S 2040-Y10R","hex":"#d6bc64"},{"code":"S 2040-Y20R","hex":"#d6b162"},{"code":"S 2040-Y30R","hex":"#d6a65f"},{"code":"S 2040-Y40R","hex":"#d69a5c"},{"code":"S 2040-Y50R","hex":"#d68e59"},{"code":"S 2040-Y60R","hex":"#d68156"},{"code":"S 2040-Y70R","hex":"#d67452"},{"code":"S 2040-Y80R","hex":"#d6664f"},{"code":"S 2040-Y90R","hex":"#d65950"},{"code":"S 2050-B","hex":"#4ba2d6"},{"code":"S 2050-B10G","hex":"#4fb4d6"},{"code":"S 2050-B20G","hex":"#51c0d6"},{"code":"S 2050-B30G","hex":"#53cad6"},{"code":"S 2050-B40G","hex":"#55d3d6"},{"code":"S 2050-B50G","hex":"#55d6d2"},{"code":"S 2050-B60G","hex":"#53d6ca"},{"code":"S 2050-B70G","hex":"#51d6c3"},{"code":"S 2050-B80G","hex":"#4fd6b8"},{"code":"S 2050-B90G","hex":"#49d697"},{"code":"S 2050-G","hex":"#44d67d"},{"code":"S 2050-G10Y","hex":"#68d66c"},{"code":"S 2050-G20Y","hex":"#82d660"},{"code":"S 2050-G30Y","hex":"#97d655"},{"code":"S 2050-G40Y","hex":"#a8d64d"},{"code":"S 2050-G50Y","hex":"#b7d64f"},{"code":"S 2050-G60Y","hex":"#c4d652"},{"code":"S 2050-G70Y","hex":"#d0d654"},{"code":"S 2050-G80Y","hex":"#d6d255"},{"code":"S 2050-G90Y","hex":"#d6ca53"},{"code":"S 2050-R","hex":"#d63842"},{"code":"S 2050-R10B","hex":"#d63a4d"},{"code":"S 2050-R20B","hex":"#d63d5c"},{"code":"S 2050-R30B","hex":"#d64171"},{"code":"S 2050-R40B","hex":"#d6478f"},{"code":"S 2050-R50B","hex":"#d650bd"},{"code":"S 2050-R60B","hex":"#aa4dd6"},{"code":"S 2050-R70B","hex":"#8052d6"},{"code":"S 2050-R80B","hex":"#3c59d6"},{"code":"S 2050-R90B","hex":"#4172d6"},{"code":"S 2050-Y","hex":"#d6c251"},{"code":"S 2050-Y10R","hex":"#d6b74f"},{"code":"S 2050-Y20R","hex":"#d6aa4d"},{"code":"S 2050-Y30R","hex":"#d69e4a"},{"code":"S 2050-Y40R","hex":"#d69047"},{"code":"S 2050-Y50R","hex":"#d68345"},{"code":"S 2050-Y60R","hex":"#d67442"},{"code":"S 2050-Y70R","hex":"#d6663f"},{"code":"S 2050-Y80R","hex":"#d6573c"},{"code":"S 2050-Y90R","hex":"#d6483e"},{"code":"S 2055-B10G","hex":"#45b1d6"},{"code":"S 2060-B","hex":"#399bd6"},{"code":"S 2060-B50G","hex":"#41d6d1"},{"code":"S 2060-B70G","hex":"#3ed6c0"},{"code":"S 2060-B90G","hex":"#37d68f"},{"code":"S 2060-G","hex":"#32d672"},{"code":"S 2060-G10Y","hex":"#5ad65f"},{"code":"S 2060-G20Y","hex":"#77d650"},{"code":"S 2060-G30Y","hex":"#8fd644"},{"code":"S 2060-G40Y","hex":"#a2d63a"},{"code":"S 2060-G50Y","hex":"#b3d63c"},{"code":"S 2060-G60Y","hex":"#c1d63e"},{"code":"S 2060-G70Y","hex":"#cfd640"},{"code":"S 2060-G80Y","hex":"#d6d241"},{"code":"S 2060-G90Y","hex":"#d6c83f"},{"code":"S 2060-R","hex":"#d62934"},{"code":"S 2060-R10B","hex":"#d62a3f"},{"code":"S 2060-R20B","hex":"#d62d4f"},{"code":"S 2060-R30B","hex":"#d63065"},{"code":"S 2060-R40B","hex":"#d63586"},{"code":"S 2060-R70B","hex":"#7642d6"},{"code":"S 2060-R80B","hex":"#2c4cd6"},{"code":"S 2060-R90B","hex":"#3067d6"},{"code":"S 2060-Y","hex":"#d6c03e"},{"code":"S 2060-Y10R","hex":"#d6b23c"},{"code":"S 2060-Y20R","hex":"#d6a43a"},{"code":"S 2060-Y30R","hex":"#d69638"},{"code":"S 2060-Y40R","hex":"#d68835"},{"code":"S 2060-Y50R","hex":"#d67933"},{"code":"S 2060-Y60R","hex":"#d66931"},{"code":"S 2060-Y70R","hex":"#d6592e"},{"code":"S 2060-Y80R","hex":"#d6492c"},{"code":"S 2060-Y90R","hex":"#d6392f"},{"code":"S 2065-B","hex":"#3098d6"},{"code":"S 2065-R20B","hex":"#d62649"},{"code":"S 2065-R90B","hex":"#2962d6"},{"code":"S 2070-G10Y","hex":"#4ed654"},{"code":"S 2070-G20Y","hex":"#6ed643"},{"code":"S 2070-G30Y","hex":"#87d635"},{"code":"S 2070-G40Y","hex":"#9dd629"},{"code":"S 2070-G50Y","hex":"#afd62b"},{"code":"S 2070-G60Y","hex":"#bed62d"},{"code":"S 2070-G70Y","hex":"#ced62e"},{"code":"S 2070-G80Y","hex":"#d6d12f"},{"code":"S 2070-G90Y","hex":"#d6c62d"},{"code":"S 2070-R","hex":"#d61c28"},{"code":"S 2070-R10B","hex":"#d61d34"},{"code":"S 2070-Y","hex":"#d6bd2c"},{"code":"S 2070-Y10R","hex":"#d6ae2b"},{"code":"S 2070-Y20R","hex":"#d69f29"},{"code":"S 2070-Y30R","hex":"#d68f27"},{"code":"S 2070-Y40R","hex":"#d68026"},{"code":"S 2070-Y50R","hex":"#d67024"},{"code":"S 2070-Y60R","hex":"#d65f22"},{"code":"S 2070-Y70R","hex":"#d64f20"},{"code":"S 2070-Y80R","hex":"#d63e1e"},{"code":"S 2070-Y90R","hex":"#d62d22"},{"code":"S 2075-G20Y","hex":"#69d63c"},{"code":"S 2075-G30Y","hex":"#84d62e"},{"code":"S 2075-Y60R","hex":"#d65b1b"},{"code":"S 2075-Y70R","hex":"#d64a1a"},{"code":"S 2500-N","hex":"#bfbfbf"},{"code":"S 2502-B","hex":"#c2c6c9"},{"code":"S 2502-G","hex":"#c0c9c4"},{"code":"S 2502-R","hex":"#c9bebf"},{"code":"S 2502-Y","hex":"#c9c8c2"},{"code":"S 2555-B20G","hex":"#43b3c9"},{"code":"S 2555-B30G","hex":"#44bcc9"},{"code":"S 2555-B40G","hex":"#46c5c9"},{"code":"S 2555-B60G","hex":"#44c9bd"},{"code":"S 2555-B80G","hex":"#41c9ab"},{"code":"S 2565-G","hex":"#28c967"},{"code":"S 2565-R80B","hex":"#2341c9"},{"code":"S 2570-G20Y","hex":"#67c93e"},{"code":"S 2570-G30Y","hex":"#7fc931"},{"code":"S 2570-R","hex":"#c91a25"},{"code":"S 2570-Y30R","hex":"#c98725"},{"code":"S 2570-Y40R","hex":"#c97823"},{"code":"S 2570-Y50R","hex":"#c96922"},{"code":"S 2570-Y60R","hex":"#c95920"},{"code":"S 2570-Y70R","hex":"#c94a1e"},{"code":"S 2570-Y80R","hex":"#c93a1c"},{"code":"S 2570-Y90R","hex":"#c92b20"},{"code":"S 3000-N","hex":"#b2b2b2"},{"code":"S 3005-B20G","hex":"#adb9bc"},{"code":"S 3005-B80G","hex":"#acbcb8"},{"code":"S 3005-G20Y","hex":"#b1bcad"},{"code":"S 3005-G50Y","hex":"#b8bcac"},{"code":"S 3005-G80Y","hex":"#bcbbae"},{"code":"S 3005-R20B","hex":"#bca6aa"},{"code":"S 3005-R50B","hex":"#bcacb9"},{"code":"S 3005-R80B","hex":"#a5aabc"},{"code":"S 3005-Y20R","hex":"#bcb6ab"},{"code":"S 3005-Y50R","hex":"#bcb1a9"},{"code":"S 3005-Y80R","hex":"#bca9a5"},{"code":"S 3010-B","hex":"#9cb0bc"},{"code":"S 3010-B10G","hex":"#9db4bc"},{"code":"S 3010-B30G","hex":"#a0b9bc"},{"code":"S 3010-B50G","hex":"#a0bcbb"},{"code":"S 3010-B70G","hex":"#9fbcb7"},{"code":"S 3010-B90G","hex":"#9abcad"},{"code":"S 3010-G","hex":"#97bca5"},{"code":"S 3010-G10Y","hex":"#a1bca2"},{"code":"S 3010-G20Y","hex":"#a8bca0"},{"code":"S 3010-G30Y","hex":"#adbc9e"},{"code":"S 3010-G40Y","hex":"#b1bc9c"},{"code":"S 3010-G50Y","hex":"#b5bc9e"},{"code":"S 3010-G60Y","hex":"#b8bc9f"},{"code":"S 3010-G70Y","hex":"#babca0"},{"code":"S 3010-G80Y","hex":"#bcbba0"},{"code":"S 3010-G90Y","hex":"#bcb9a0"},{"code":"S 3010-R","hex":"#bc8f92"},{"code":"S 3010-R10B","hex":"#bc9096"},{"code":"S 3010-R20B","hex":"#bc939b"},{"code":"S 3010-R30B","hex":"#bc96a2"},{"code":"S 3010-R40B","hex":"#bc99aa"},{"code":"S 3010-R50B","hex":"#bc9eb6"},{"code":"S 3010-R60B","hex":"#b29cbc"},{"code":"S 3010-R70B","hex":"#a79cbc"},{"code":"S 3010-R80B","hex":"#929abc"},{"code":"S 3010-R90B","hex":"#96a2bc"},{"code":"S 3010-Y","hex":"#bcb79f"},{"code":"S 3010-Y10R","hex":"#bcb59e"},{"code":"S 3010-Y20R","hex":"#bcb29c"},{"code":"S 3010-Y30R","hex":"#bcae9b"},{"code":"S 3010-Y40R","hex":"#bcab9a"},{"code":"S 3010-Y50R","hex":"#bca798"},{"code":"S 3010-Y60R","hex":"#bca396"},{"code":"S 3010-Y70R","hex":"#bc9e94"},{"code":"S 3010-Y80R","hex":"#bc9992"},{"code":"S 3010-Y90R","hex":"#bc9491"},{"code":"S 3020-B","hex":"#80a5bc"},{"code":"S 3020-B10G","hex":"#83adbc"},{"code":"S 3020-B30G","hex":"#86b7bc"},{"code":"S 3020-B40G","hex":"#88babc"},{"code":"S 3020-B50G","hex":"#88bcba"},{"code":"S 3020-B60G","hex":"#86bcb7"},{"code":"S 3020-B70G","hex":"#85bcb3"},{"code":"S 3020-B90G","hex":"#7ebca0"},{"code":"S 3020-G","hex":"#7abc93"},{"code":"S 3020-G10Y","hex":"#8bbc8d"},{"code":"S 3020-G20Y","hex":"#97bc88"},{"code":"S 3020-G30Y","hex":"#a1bc84"},{"code":"S 3020-G40Y","hex":"#a8bc81"},{"code":"S 3020-G50Y","hex":"#afbc84"},{"code":"S 3020-G60Y","hex":"#b4bc86"},{"code":"S 3020-G70Y","hex":"#b9bc87"},{"code":"S 3020-G80Y","hex":"#bcba88"},{"code":"S 3020-G90Y","hex":"#bcb686"},{"code":"S 3020-R","hex":"#bc6e73"},{"code":"S 3020-R10B","hex":"#bc7079"},{"code":"S 3020-R20B","hex":"#bc7382"},{"code":"S 3020-R30B","hex":"#bc778d"},{"code":"S 3020-R40B","hex":"#bc7d9c"},{"code":"S 3020-R50B","hex":"#bc84b1"},{"code":"S 3020-R60B","hex":"#a982bc"},{"code":"S 3020-R70B","hex":"#9682bc"},{"code":"S 3020-R80B","hex":"#7380bc"},{"code":"S 3020-R90B","hex":"#788ebc"},{"code":"S 3020-Y","hex":"#bcb385"},{"code":"S 3020-Y10R","hex":"#bcae83"},{"code":"S 3020-Y20R","hex":"#bca982"},{"code":"S 3020-Y30R","hex":"#bca37f"},{"code":"S 3020-Y40R","hex":"#bc9d7d"},{"code":"S 3020-Y50R","hex":"#bc967b"},{"code":"S 3020-Y60R","hex":"#bc8f78"},{"code":"S 3020-Y70R","hex":"#bc8775"},{"code":"S 3020-Y80R","hex":"#bc7f72"},{"code":"S 3020-Y90R","hex":"#bc7772"},{"code":"S 3030-B","hex":"#689cbc"},{"code":"S 3030-B10G","hex":"#6ca7bc"},{"code":"S 3030-B30G","hex":"#70b4bc"},{"code":"S 3030-B40G","hex":"#71b9bc"},{"code":"S 3030-B50G","hex":"#71bcb9"},{"code":"S 3030-B60G","hex":"#70bcb5"},{"code":"S 3030-B70G","hex":"#6ebcb0"},{"code":"S 3030-B90G","hex":"#66bc95"},{"code":"S 3030-G","hex":"#61bc84"},{"code":"S 3030-G10Y","hex":"#78bc7b"},{"code":"S 3030-G20Y","hex":"#89bc74"},{"code":"S 3030-G30Y","hex":"#96bc6e"},{"code":"S 3030-G40Y","hex":"#a0bc6a"},{"code":"S 3030-G50Y","hex":"#a9bc6c"},{"code":"S 3030-G60Y","hex":"#b1bc6f"},{"code":"S 3030-G70Y","hex":"#b8bc71"},{"code":"S 3030-G80Y","hex":"#bcb971"},{"code":"S 3030-G90Y","hex":"#bcb470"},{"code":"S 3030-R","hex":"#bc555b"},{"code":"S 3030-R10B","hex":"#bc5763"},{"code":"S 3030-R20B","hex":"#bc5a6e"},{"code":"S 3030-R30B","hex":"#bc5f7c"},{"code":"S 3030-R40B","hex":"#bc6590"},{"code":"S 3030-R50B","hex":"#bc6dad"},{"code":"S 3030-R60B","hex":"#a26abc"},{"code":"S 3030-R70B","hex":"#876bbc"},{"code":"S 3030-R80B","hex":"#5a6cbc"},{"code":"S 3030-R90B","hex":"#5f7ebc"},{"code":"S 3030-Y","hex":"#bcb06e"},{"code":"S 3030-Y10R","hex":"#bca96c"},{"code":"S 3030-Y20R","hex":"#bca16a"},{"code":"S 3030-Y30R","hex":"#bc9a68"},{"code":"S 3030-Y40R","hex":"#bc9165"},{"code":"S 3030-Y50R","hex":"#bc8862"},{"code":"S 3030-Y60R","hex":"#bc7f5f"},{"code":"S 3030-Y70R","hex":"#bc755c"},{"code":"S 3030-Y80R","hex":"#bc6a59"},{"code":"S 3030-Y90R","hex":"#bc6059"},{"code":"S 3040-B","hex":"#5495bc"},{"code":"S 3040-B10G","hex":"#57a2bc"},{"code":"S 3040-B20G","hex":"#59abbc"},{"code":"S 3040-B30G","hex":"#5bb3bc"},{"code":"S 3040-B40G","hex":"#5db9bc"},{"code":"S 3040-B50G","hex":"#5dbcb8"},{"code":"S 3040-B60G","hex":"#5bbcb3"},{"code":"S 3040-B70G","hex":"#5abcad"},{"code":"S 3040-B80G","hex":"#58bca5"},{"code":"S 3040-B90G","hex":"#52bc8c"},{"code":"S 3040-G","hex":"#4dbc78"},{"code":"S 3040-G10Y","hex":"#68bc6c"},{"code":"S 3040-G20Y","hex":"#7dbc63"},{"code":"S 3040-G30Y","hex":"#8cbc5b"},{"code":"S 3040-G40Y","hex":"#99bc55"},{"code":"S 3040-G50Y","hex":"#a5bc58"},{"code":"S 3040-G60Y","hex":"#aebc5a"},{"code":"S 3040-G70Y","hex":"#b7bc5c"},{"code":"S 3040-G80Y","hex":"#bcb95d"},{"code":"S 3040-G90Y","hex":"#bcb25b"},{"code":"S 3040-R","hex":"#bc4149"},{"code":"S 3040-R10B","hex":"#bc4352"},{"code":"S 3040-R20B","hex":"#bc465e"},{"code":"S 3040-R30B","hex":"#bc4a6e"},{"code":"S 3040-R40B","hex":"#bc5086"},{"code":"S 3040-R50B","hex":"#bc59a9"},{"code":"S 3040-R60B","hex":"#9b55bc"},{"code":"S 3040-R70B","hex":"#7b58bc"},{"code":"S 3040-R80B","hex":"#455bbc"},{"code":"S 3040-R90B","hex":"#4b70bc"},{"code":"S 3040-Y","hex":"#bcad5a"},{"code":"S 3040-Y10R","hex":"#bca458"},{"code":"S 3040-Y20R","hex":"#bc9b55"},{"code":"S 3040-Y30R","hex":"#bc9153"},{"code":"S 3040-Y40R","hex":"#bc8750"},{"code":"S 3040-Y50R","hex":"#bc7c4e"},{"code":"S 3040-Y60R","hex":"#bc714b"},{"code":"S 3040-Y70R","hex":"#bc6648"},{"code":"S 3040-Y80R","hex":"#bc5a45"},{"code":"S 3040-Y90R","hex":"#bc4d46"},{"code":"S 3050-B","hex":"#428ebc"},{"code":"S 3050-B10G","hex":"#459dbc"},{"code":"S 3050-B20G","hex":"#47a8bc"},{"code":"S 3050-B30G","hex":"#49b1bc"},{"code":"S 3050-B40G","hex":"#4ab8bc"},{"code":"S 3050-B50G","hex":"#4abcb8"},{"code":"S 3050-B60G","hex":"#49bcb1"},{"code":"S 3050-B70G","hex":"#47bcaa"},{"code":"S 3050-B80G","hex":"#45bca1"},{"code":"S 3050-B90G","hex":"#40bc84"},{"code":"S 3050-G","hex":"#3bbc6d"},{"code":"S 3050-G10Y","hex":"#5bbc5f"},{"code":"S 3050-G20Y","hex":"#72bc54"},{"code":"S 3050-G30Y","hex":"#84bc4a"},{"code":"S 3050-G40Y","hex":"#93bc43"},{"code":"S 3050-G50Y","hex":"#a0bc45"},{"code":"S 3050-G60Y","hex":"#abbc47"},{"code":"S 3050-G70Y","hex":"#b6bc4a"},{"code":"S 3050-G80Y","hex":"#bcb84a"},{"code":"S 3050-G90Y","hex":"#bcb048"},{"code":"S 3050-R","hex":"#bc313a"},{"code":"S 3050-R10B","hex":"#bc3343"},{"code":"S 3050-R20B","hex":"#bc3550"},{"code":"S 3050-R30B","hex":"#bc3963"},{"code":"S 3050-R40B","hex":"#bc3e7d"},{"code":"S 3050-R50B","hex":"#bc46a5"},{"code":"S 3050-R60B","hex":"#9543bc"},{"code":"S 3050-R70B","hex":"#7048bc"},{"code":"S 3050-R80B","hex":"#354ebc"},{"code":"S 3050-R90B","hex":"#3964bc"},{"code":"S 3050-Y","hex":"#bcaa47"},{"code":"S 3050-Y10R","hex":"#bca045"},{"code":"S 3050-Y20R","hex":"#bc9543"},{"code":"S 3050-Y30R","hex":"#bc8a41"},{"code":"S 3050-Y40R","hex":"#bc7e3e"},{"code":"S 3050-Y50R","hex":"#bc723c"},{"code":"S 3050-Y60R","hex":"#bc663a"},{"code":"S 3050-Y70R","hex":"#bc5937"},{"code":"S 3050-Y80R","hex":"#bc4c34"},{"code":"S 3050-Y90R","hex":"#bc3f36"},{"code":"S 3055-B50G","hex":"#41bcb7"},{"code":"S 3055-R30B","hex":"#bc315d"},{"code":"S 3055-R40B","hex":"#bc3679"},{"code":"S 3055-R50B","hex":"#bc3ea3"},{"code":"S 3060-B","hex":"#3188bc"},{"code":"S 3060-B10G","hex":"#3499bc"},{"code":"S 3060-B20G","hex":"#36a5bc"},{"code":"S 3060-B30G","hex":"#37afbc"},{"code":"S 3060-B40G","hex":"#39b8bc"},{"code":"S 3060-B70G","hex":"#36bca8"},{"code":"S 3060-B90G","hex":"#30bc7d"},{"code":"S 3060-G","hex":"#2cbc64"},{"code":"S 3060-G10Y","hex":"#4fbc53"},{"code":"S 3060-G20Y","hex":"#68bc46"},{"code":"S 3060-G30Y","hex":"#7dbc3b"},{"code":"S 3060-G40Y","hex":"#8ebc33"},{"code":"S 3060-G50Y","hex":"#9cbc35"},{"code":"S 3060-G60Y","hex":"#a9bc36"},{"code":"S 3060-G70Y","hex":"#b5bc38"},{"code":"S 3060-G80Y","hex":"#bcb739"},{"code":"S 3060-G90Y","hex":"#bcaf37"},{"code":"S 3060-R","hex":"#bc232d"},{"code":"S 3060-R10B","hex":"#bc2537"},{"code":"S 3060-R20B","hex":"#bc2745"},{"code":"S 3060-R70B","hex":"#673abc"},{"code":"S 3060-R80B","hex":"#2742bc"},{"code":"S 3060-R90B","hex":"#2a5abc"},{"code":"S 3060-Y","hex":"#bca836"},{"code":"S 3060-Y10R","hex":"#bc9c34"},{"code":"S 3060-Y20R","hex":"#bc9033"},{"code":"S 3060-Y30R","hex":"#bc8331"},{"code":"S 3060-Y40R","hex":"#bc772f"},{"code":"S 3060-Y50R","hex":"#bc692d"},{"code":"S 3060-Y60R","hex":"#bc5c2b"},{"code":"S 3060-Y70R","hex":"#bc4e29"},{"code":"S 3060-Y80R","hex":"#bc4026"},{"code":"S 3060-Y90R","hex":"#bc3229"},{"code":"S 3065-G10Y","hex":"#49bc4e"},{"code":"S 3065-G40Y","hex":"#8bbc2b"},{"code":"S 3065-G50Y","hex":"#9bbc2d"},{"code":"S 3065-G60Y","hex":"#a8bc2f"},{"code":"S 3065-R90B","hex":"#2456bc"},{"code":"S 3065-Y20R","hex":"#bc8d2b"},{"code":"S 3500-N","hex":"#a5a5a5"},{"code":"S 3502-B","hex":"#a8acae"},{"code":"S 3502-G","hex":"#a7aeaa"},{"code":"S 3502-R","hex":"#aea5a5"},{"code":"S 3502-Y","hex":"#aeada9"},{"code":"S 3555-B60G","hex":"#3baea4"},{"code":"S 3555-B80G","hex":"#39ae94"},{"code":"S 3555-R60B","hex":"#8836ae"},{"code":"S 3560-G","hex":"#29ae5d"},{"code":"S 3560-G10Y","hex":"#49ae4d"},{"code":"S 3560-G20Y","hex":"#61ae41"},{"code":"S 3560-G30Y","hex":"#74ae37"},{"code":"S 3560-G40Y","hex":"#84ae2f"},{"code":"S 3560-G50Y","hex":"#91ae31"},{"code":"S 3560-G60Y","hex":"#9dae33"},{"code":"S 3560-G70Y","hex":"#a8ae34"},{"code":"S 3560-R","hex":"#ae212a"},{"code":"S 3560-R80B","hex":"#243dae"},{"code":"S 3560-R90B","hex":"#2754ae"},{"code":"S 3560-Y","hex":"#ae9c32"},{"code":"S 3560-Y20R","hex":"#ae852f"},{"code":"S 3560-Y30R","hex":"#ae7a2d"},{"code":"S 3560-Y40R","hex":"#ae6e2b"},{"code":"S 3560-Y50R","hex":"#ae622a"},{"code":"S 3560-Y60R","hex":"#ae5528"},{"code":"S 3560-Y70R","hex":"#ae4926"},{"code":"S 3560-Y80R","hex":"#ae3c24"},{"code":"S 3560-Y90R","hex":"#ae2f26"},{"code":"S 4000-N","hex":"#999999"},{"code":"S 4005-B20G","hex":"#949fa1"},{"code":"S 4005-B80G","hex":"#94a19e"},{"code":"S 4005-G20Y","hex":"#98a194"},{"code":"S 4005-G50Y","hex":"#9ea194"},{"code":"S 4005-G80Y","hex":"#a1a095"},{"code":"S 4005-R20B","hex":"#a18e92"},{"code":"S 4005-R50B","hex":"#a1949e"},{"code":"S 4005-R80B","hex":"#8e91a1"},{"code":"S 4005-Y20R","hex":"#a19c93"},{"code":"S 4005-Y50R","hex":"#a19891"},{"code":"S 4005-Y80R","hex":"#a1918e"},{"code":"S 4010-B10G","hex":"#879aa1"},{"code":"S 4010-B30G","hex":"#899fa1"},{"code":"S 4010-B50G","hex":"#89a1a0"},{"code":"S 4010-B70G","hex":"#88a19d"},{"code":"S 4010-B90G","hex":"#84a194"},{"code":"S 4010-G10Y","hex":"#8aa18b"},{"code":"S 4010-G30Y","hex":"#94a187"},{"code":"S 4010-G50Y","hex":"#9ba187"},{"code":"S 4010-G70Y","hex":"#a0a189"},{"code":"S 4010-G90Y","hex":"#a19e89"},{"code":"S 4010-R10B","hex":"#a17c80"},{"code":"S 4010-R30B","hex":"#a1808b"},{"code":"S 4010-R50B","hex":"#a1889c"},{"code":"S 4010-R70B","hex":"#8f86a1"},{"code":"S 4010-R90B","hex":"#808ba1"},{"code":"S 4010-Y10R","hex":"#a19b87"},{"code":"S 4010-Y30R","hex":"#a19585"},{"code":"S 4010-Y50R","hex":"#a18f82"},{"code":"S 4010-Y70R","hex":"#a1887f"},{"code":"S 4010-Y90R","hex":"#a17f7d"},{"code":"S 4020-B","hex":"#6e8ea1"},{"code":"S 4020-B10G","hex":"#7094a1"},{"code":"S 4020-B30G","hex":"#739da1"},{"code":"S 4020-B50G","hex":"#74a19f"},{"code":"S 4020-B70G","hex":"#72a19a"},{"code":"S 4020-B90G","hex":"#6ca189"},{"code":"S 4020-G","hex":"#68a17e"},{"code":"S 4020-G10Y","hex":"#77a179"},{"code":"S 4020-G30Y","hex":"#8aa171"},{"code":"S 4020-G50Y","hex":"#96a171"},{"code":"S 4020-G70Y","hex":"#9fa174"},{"code":"S 4020-G90Y","hex":"#a19c73"},{"code":"S 4020-R","hex":"#a15e62"},{"code":"S 4020-R10B","hex":"#a16068"},{"code":"S 4020-R20B","hex":"#a1636f"},{"code":"S 4020-R30B","hex":"#a16679"},{"code":"S 4020-R40B","hex":"#a16b86"},{"code":"S 4020-R50B","hex":"#a17298"},{"code":"S 4020-R60B","hex":"#916fa1"},{"code":"S 4020-R70B","hex":"#806fa1"},{"code":"S 4020-R80B","hex":"#626ea1"},{"code":"S 4020-R90B","hex":"#677aa1"},{"code":"S 4020-Y","hex":"#a19a72"},{"code":"S 4020-Y10R","hex":"#a19571"},{"code":"S 4020-Y20R","hex":"#a1916f"},{"code":"S 4020-Y30R","hex":"#a18c6d"},{"code":"S 4020-Y40R","hex":"#a1876b"},{"code":"S 4020-Y50R","hex":"#a18169"},{"code":"S 4020-Y60R","hex":"#a17b67"},{"code":"S 4020-Y70R","hex":"#a17464"},{"code":"S 4020-Y80R","hex":"#a16d62"},{"code":"S 4020-Y90R","hex":"#a16662"},{"code":"S 4030-B","hex":"#5a86a1"},{"code":"S 4030-B10G","hex":"#5c8fa1"},{"code":"S 4030-B30G","hex":"#609ba1"},{"code":"S 4030-B50G","hex":"#61a19f"},{"code":"S 4030-B70G","hex":"#5fa197"},{"code":"S 4030-B90G","hex":"#58a180"},{"code":"S 4030-G","hex":"#53a171"},{"code":"S 4030-G10Y","hex":"#67a16a"},{"code":"S 4030-G30Y","hex":"#80a15f"},{"code":"S 4030-G50Y","hex":"#91a15d"},{"code":"S 4030-G70Y","hex":"#9ea161"},{"code":"S 4030-G90Y","hex":"#a19a60"},{"code":"S 4030-R","hex":"#a1494e"},{"code":"S 4030-R10B","hex":"#a14b55"},{"code":"S 4030-R20B","hex":"#a14d5e"},{"code":"S 4030-R30B","hex":"#a1516b"},{"code":"S 4030-R40B","hex":"#a1567c"},{"code":"S 4030-R50B","hex":"#a15e94"},{"code":"S 4030-R60B","hex":"#8b5ba1"},{"code":"S 4030-R70B","hex":"#745ca1"},{"code":"S 4030-R80B","hex":"#4d5ca1"},{"code":"S 4030-R90B","hex":"#516ca1"},{"code":"S 4030-Y","hex":"#a1975f"},{"code":"S 4030-Y10R","hex":"#a1915d"},{"code":"S 4030-Y20R","hex":"#a18a5b"},{"code":"S 4030-Y30R","hex":"#a18459"},{"code":"S 4030-Y40R","hex":"#a17c57"},{"code":"S 4030-Y50R","hex":"#a17554"},{"code":"S 4030-Y60R","hex":"#a16d52"},{"code":"S 4030-Y70R","hex":"#a1644f"},{"code":"S 4030-Y80R","hex":"#a15b4c"},{"code":"S 4030-Y90R","hex":"#a1524d"},{"code":"S 4040-B","hex":"#487fa1"},{"code":"S 4040-B10G","hex":"#4b8ba1"},{"code":"S 4040-B20G","hex":"#4d93a1"},{"code":"S 4040-B30G","hex":"#4e99a1"},{"code":"S 4040-B40G","hex":"#509ea1"},{"code":"S 4040-B50G","hex":"#4fa19e"},{"code":"S 4040-B60G","hex":"#4ea199"},{"code":"S 4040-B70G","hex":"#4da194"},{"code":"S 4040-B80G","hex":"#4ba18e"},{"code":"S 4040-B90G","hex":"#46a178"},{"code":"S 4040-G","hex":"#42a167"},{"code":"S 4040-G10Y","hex":"#59a15d"},{"code":"S 4040-G20Y","hex":"#6ba155"},{"code":"S 4040-G30Y","hex":"#78a14e"},{"code":"S 4040-G40Y","hex":"#83a149"},{"code":"S 4040-G50Y","hex":"#8da14b"},{"code":"S 4040-G60Y","hex":"#95a14d"},{"code":"S 4040-G70Y","hex":"#9da14f"},{"code":"S 4040-G80Y","hex":"#a19e50"},{"code":"S 4040-G90Y","hex":"#a1994e"},{"code":"S 4040-R","hex":"#a1383e"},{"code":"S 4040-R10B","hex":"#a13946"},{"code":"S 4040-R20B","hex":"#a13c50"},{"code":"S 4040-R30B","hex":"#a1405f"},{"code":"S 4040-R40B","hex":"#a14573"},{"code":"S 4040-R50B","hex":"#a14c91"},{"code":"S 4040-R60B","hex":"#8549a1"},{"code":"S 4040-R70B","hex":"#694ca1"},{"code":"S 4040-R80B","hex":"#3b4ea1"},{"code":"S 4040-R90B","hex":"#4060a1"},{"code":"S 4040-Y","hex":"#a1944d"},{"code":"S 4040-Y10R","hex":"#a18d4b"},{"code":"S 4040-Y20R","hex":"#a18549"},{"code":"S 4040-Y30R","hex":"#a17c47"},{"code":"S 4040-Y40R","hex":"#a17445"},{"code":"S 4040-Y50R","hex":"#a16b43"},{"code":"S 4040-Y60R","hex":"#a16140"},{"code":"S 4040-Y70R","hex":"#a1573e"},{"code":"S 4040-Y80R","hex":"#a14d3b"},{"code":"S 4040-Y90R","hex":"#a1423c"},{"code":"S 4050-B","hex":"#387aa1"},{"code":"S 4050-B10G","hex":"#3b87a1"},{"code":"S 4050-B20G","hex":"#3d90a1"},{"code":"S 4050-B30G","hex":"#3e98a1"},{"code":"S 4050-B40G","hex":"#3f9ea1"},{"code":"S 4050-B50G","hex":"#3fa19e"},{"code":"S 4050-B60G","hex":"#3ea198"},{"code":"S 4050-B70G","hex":"#3da192"},{"code":"S 4050-B80G","hex":"#3ca18a"},{"code":"S 4050-B90G","hex":"#36a171"},{"code":"S 4050-G","hex":"#33a15d"},{"code":"S 4050-G10Y","hex":"#4ea151"},{"code":"S 4050-G20Y","hex":"#62a148"},{"code":"S 4050-G30Y","hex":"#71a140"},{"code":"S 4050-G40Y","hex":"#7ea139"},{"code":"S 4050-G50Y","hex":"#89a13b"},{"code":"S 4050-G60Y","hex":"#93a13d"},{"code":"S 4050-G70Y","hex":"#9ca13f"},{"code":"S 4050-G80Y","hex":"#a19e3f"},{"code":"S 4050-G90Y","hex":"#a1973e"},{"code":"S 4050-R","hex":"#a12a31"},{"code":"S 4050-R10B","hex":"#a12b3a"},{"code":"S 4050-R20B","hex":"#a12e45"},{"code":"S 4050-R30B","hex":"#a13154"},{"code":"S 4050-R40B","hex":"#a1356b"},{"code":"S 4050-R50B","hex":"#a13c8e"},{"code":"S 4050-R60B","hex":"#8039a1"},{"code":"S 4050-R70B","hex":"#603ea1"},{"code":"S 4050-R80B","hex":"#2d42a1"},{"code":"S 4050-R90B","hex":"#3156a1"},{"code":"S 4050-Y","hex":"#a1923d"},{"code":"S 4050-Y10R","hex":"#a1893b"},{"code":"S 4050-Y20R","hex":"#a18039"},{"code":"S 4050-Y30R","hex":"#a17637"},{"code":"S 4050-Y40R","hex":"#a16c36"},{"code":"S 4050-Y50R","hex":"#a16233"},{"code":"S 4050-Y60R","hex":"#a15731"},{"code":"S 4050-Y70R","hex":"#a14c2f"},{"code":"S 4050-Y80R","hex":"#a1412d"},{"code":"S 4050-Y90R","hex":"#a1362e"},{"code":"S 4055-B","hex":"#3177a1"},{"code":"S 4055-B40G","hex":"#389ea1"},{"code":"S 4055-R70B","hex":"#5c37a1"},{"code":"S 4055-R95B","hex":"#2d5fa1"},{"code":"S 4055-Y10R","hex":"#a18734"},{"code":"S 4500-N","hex":"#8c8c8c"},{"code":"S 4502-B","hex":"#8e9193"},{"code":"S 4502-G","hex":"#8d9390"},{"code":"S 4502-R","hex":"#938b8c"},{"code":"S 4502-Y","hex":"#93938f"},{"code":"S 4550-B","hex":"#336f93"},{"code":"S 4550-B20G","hex":"#388493"},{"code":"S 4550-B30G","hex":"#398b93"},{"code":"S 4550-B40G","hex":"#3a9193"},{"code":"S 4550-B50G","hex":"#3a9390"},{"code":"S 4550-B80G","hex":"#37937f"},{"code":"S 4550-B90G","hex":"#329368"},{"code":"S 4550-G","hex":"#2e9356"},{"code":"S 4550-G10Y","hex":"#47934b"},{"code":"S 4550-G20Y","hex":"#599342"},{"code":"S 4550-G30Y","hex":"#68933a"},{"code":"S 4550-G40Y","hex":"#749335"},{"code":"S 4550-G50Y","hex":"#7e9336"},{"code":"S 4550-G60Y","hex":"#879338"},{"code":"S 4550-G70Y","hex":"#8f933a"},{"code":"S 4550-R70B","hex":"#583993"},{"code":"S 4550-R80B","hex":"#293d93"},{"code":"S 4550-R90B","hex":"#2d4f93"},{"code":"S 4550-Y","hex":"#938638"},{"code":"S 4550-Y30R","hex":"#936c33"},{"code":"S 4550-Y40R","hex":"#936331"},{"code":"S 4550-Y50R","hex":"#935a2f"},{"code":"S 4550-Y60R","hex":"#93502d"},{"code":"S 4550-Y70R","hex":"#93462b"},{"code":"S 4550-Y80R","hex":"#933b29"},{"code":"S 4550-Y90R","hex":"#93312a"},{"code":"S 5000-N","hex":"#7f7f7f"},{"code":"S 5005-B20G","hex":"#7b8486"},{"code":"S 5005-B80G","hex":"#7b8684"},{"code":"S 5005-G20Y","hex":"#7f867c"},{"code":"S 5005-G50Y","hex":"#83867b"},{"code":"S 5005-G80Y","hex":"#86867c"},{"code":"S 5005-R20B","hex":"#86767a"},{"code":"S 5005-R50B","hex":"#867b84"},{"code":"S 5005-R80B","hex":"#767986"},{"code":"S 5005-Y20R","hex":"#86827a"},{"code":"S 5005-Y50R","hex":"#867e79"},{"code":"S 5005-Y80R","hex":"#867976"},{"code":"S 5010-B10G","hex":"#708186"},{"code":"S 5010-B30G","hex":"#728486"},{"code":"S 5010-B50G","hex":"#738685"},{"code":"S 5010-B70G","hex":"#718683"},{"code":"S 5010-B90G","hex":"#6e867b"},{"code":"S 5010-G10Y","hex":"#738674"},{"code":"S 5010-G30Y","hex":"#7c8671"},{"code":"S 5010-G50Y","hex":"#818671"},{"code":"S 5010-G70Y","hex":"#858672"},{"code":"S 5010-G90Y","hex":"#868472"},{"code":"S 5010-R10B","hex":"#86676b"},{"code":"S 5010-R30B","hex":"#866b74"},{"code":"S 5010-R50B","hex":"#867182"},{"code":"S 5010-R70B","hex":"#776f86"},{"code":"S 5010-R90B","hex":"#6b7486"},{"code":"S 5010-Y10R","hex":"#868171"},{"code":"S 5010-Y30R","hex":"#867d6f"},{"code":"S 5010-Y50R","hex":"#86776d"},{"code":"S 5010-Y70R","hex":"#86716a"},{"code":"S 5010-Y90R","hex":"#866a68"},{"code":"S 5020-B","hex":"#5c7686"},{"code":"S 5020-B10G","hex":"#5e7c86"},{"code":"S 5020-B30G","hex":"#608286"},{"code":"S 5020-B50G","hex":"#618685"},{"code":"S 5020-B70G","hex":"#5f8680"},{"code":"S 5020-B90G","hex":"#5a8672"},{"code":"S 5020-G","hex":"#578669"},{"code":"S 5020-G10Y","hex":"#638665"},{"code":"S 5020-G30Y","hex":"#73865f"},{"code":"S 5020-G50Y","hex":"#7d865e"},{"code":"S 5020-G70Y","hex":"#848661"},{"code":"S 5020-G90Y","hex":"#868260"},{"code":"S 5020-R","hex":"#864e52"},{"code":"S 5020-R10B","hex":"#865057"},{"code":"S 5020-R20B","hex":"#86525d"},{"code":"S 5020-R30B","hex":"#865565"},{"code":"S 5020-R40B","hex":"#865970"},{"code":"S 5020-R50B","hex":"#865f7e"},{"code":"S 5020-R60B","hex":"#795d86"},{"code":"S 5020-R70B","hex":"#6b5d86"},{"code":"S 5020-R80B","hex":"#525b86"},{"code":"S 5020-R90B","hex":"#556586"},{"code":"S 5020-Y","hex":"#86805f"},{"code":"S 5020-Y10R","hex":"#867d5e"},{"code":"S 5020-Y20R","hex":"#86795c"},{"code":"S 5020-Y30R","hex":"#86755b"},{"code":"S 5020-Y40R","hex":"#867059"},{"code":"S 5020-Y50R","hex":"#866b58"},{"code":"S 5020-Y60R","hex":"#866656"},{"code":"S 5020-Y70R","hex":"#866154"},{"code":"S 5020-Y80R","hex":"#865b52"},{"code":"S 5020-Y90R","hex":"#865551"},{"code":"S 5030-B","hex":"#4b7086"},{"code":"S 5030-B10G","hex":"#4d7786"},{"code":"S 5030-B30G","hex":"#508186"},{"code":"S 5030-B50G","hex":"#518684"},{"code":"S 5030-B70G","hex":"#4f867e"},{"code":"S 5030-B90G","hex":"#49866b"},{"code":"S 5030-G","hex":"#45865f"},{"code":"S 5030-G10Y","hex":"#568658"},{"code":"S 5030-G30Y","hex":"#6b864f"},{"code":"S 5030-G50Y","hex":"#79864d"},{"code":"S 5030-G70Y","hex":"#838651"},{"code":"S 5030-G90Y","hex":"#868150"},{"code":"S 5030-R","hex":"#863c41"},{"code":"S 5030-R10B","hex":"#863e47"},{"code":"S 5030-R20B","hex":"#86404e"},{"code":"S 5030-R30B","hex":"#864459"},{"code":"S 5030-R40B","hex":"#864867"},{"code":"S 5030-R50B","hex":"#864e7b"},{"code":"S 5030-R60B","hex":"#734c86"},{"code":"S 5030-R70B","hex":"#614d86"},{"code":"S 5030-R80B","hex":"#404d86"},{"code":"S 5030-R90B","hex":"#445a86"},{"code":"S 5030-Y","hex":"#867e4f"},{"code":"S 5030-Y10R","hex":"#86794d"},{"code":"S 5030-Y20R","hex":"#86734c"},{"code":"S 5030-Y30R","hex":"#866e4a"},{"code":"S 5030-Y40R","hex":"#866848"},{"code":"S 5030-Y50R","hex":"#866146"},{"code":"S 5030-Y60R","hex":"#865b44"},{"code":"S 5030-Y70R","hex":"#865342"},{"code":"S 5030-Y80R","hex":"#864c40"},{"code":"S 5030-Y90R","hex":"#864440"},{"code":"S 5040-B","hex":"#3c6a86"},{"code":"S 5040-B10G","hex":"#3e7486"},{"code":"S 5040-B20G","hex":"#407a86"},{"code":"S 5040-B30G","hex":"#418086"},{"code":"S 5040-B40G","hex":"#428486"},{"code":"S 5040-B50G","hex":"#428684"},{"code":"S 5040-B60G","hex":"#418680"},{"code":"S 5040-B70G","hex":"#40867c"},{"code":"S 5040-B80G","hex":"#3f8676"},{"code":"S 5040-B90G","hex":"#3a8664"},{"code":"S 5040-G","hex":"#378656"},{"code":"S 5040-G10Y","hex":"#4b864d"},{"code":"S 5040-G20Y","hex":"#598646"},{"code":"S 5040-G30Y","hex":"#648641"},{"code":"S 5040-G40Y","hex":"#6e863d"},{"code":"S 5040-G50Y","hex":"#76863f"},{"code":"S 5040-G60Y","hex":"#7c8640"},{"code":"S 5040-G70Y","hex":"#838642"},{"code":"S 5040-G80Y","hex":"#868442"},{"code":"S 5040-G90Y","hex":"#867f41"},{"code":"S 5040-R","hex":"#862e34"},{"code":"S 5040-R10B","hex":"#86303a"},{"code":"S 5040-R20B","hex":"#863243"},{"code":"S 5040-R30B","hex":"#86354f"},{"code":"S 5040-R40B","hex":"#863960"},{"code":"S 5040-R50B","hex":"#863f78"},{"code":"S 5040-R60B","hex":"#6f3d86"},{"code":"S 5040-R70B","hex":"#583f86"},{"code":"S 5040-R80B","hex":"#324186"},{"code":"S 5040-R90B","hex":"#355086"},{"code":"S 5040-Y","hex":"#867c40"},{"code":"S 5040-Y10R","hex":"#86753f"},{"code":"S 5040-Y20R","hex":"#866f3d"},{"code":"S 5040-Y30R","hex":"#86683b"},{"code":"S 5040-Y40R","hex":"#866039"},{"code":"S 5040-Y50R","hex":"#865937"},{"code":"S 5040-Y60R","hex":"#865135"},{"code":"S 5040-Y70R","hex":"#864933"},{"code":"S 5040-Y80R","hex":"#864031"},{"code":"S 5040-Y90R","hex":"#863732"},{"code":"S 5045-B10G","hex":"#377286"},{"code":"S 5500-N","hex":"#727272"},{"code":"S 5502-B","hex":"#747779"},{"code":"S 5502-G","hex":"#747976"},{"code":"S 5502-R","hex":"#797272"},{"code":"S 5502-Y","hex":"#797875"},{"code":"S 5540-B","hex":"#366079"},{"code":"S 5540-B10G","hex":"#386879"},{"code":"S 5540-B20G","hex":"#396e79"},{"code":"S 5540-B30G","hex":"#3b7379"},{"code":"S 5540-B40G","hex":"#3c7779"},{"code":"S 5540-B50G","hex":"#3c7977"},{"code":"S 5540-B80G","hex":"#38796a"},{"code":"S 5540-B90G","hex":"#34795a"},{"code":"S 5540-G","hex":"#31794d"},{"code":"S 5540-G10Y","hex":"#437945"},{"code":"S 5540-G20Y","hex":"#50793f"},{"code":"S 5540-G30Y","hex":"#5a793b"},{"code":"S 5540-G40Y","hex":"#637937"},{"code":"S 5540-G50Y","hex":"#6a7938"},{"code":"S 5540-G60Y","hex":"#70793a"},{"code":"S 5540-G70Y","hex":"#76793b"},{"code":"S 5540-R70B","hex":"#4f3979"},{"code":"S 5540-R90B","hex":"#304879"},{"code":"S 5540-Y90R","hex":"#79322d"},{"code":"S 6000-N","hex":"#666666"},{"code":"S 6005-B20G","hex":"#636a6b"},{"code":"S 6005-B80G","hex":"#626b69"},{"code":"S 6005-G20Y","hex":"#656b63"},{"code":"S 6005-G50Y","hex":"#696b62"},{"code":"S 6005-G80Y","hex":"#6b6b63"},{"code":"S 6005-R20B","hex":"#6b5f61"},{"code":"S 6005-R50B","hex":"#6b636a"},{"code":"S 6005-R80B","hex":"#5f616b"},{"code":"S 6005-Y20R","hex":"#6b6862"},{"code":"S 6005-Y50R","hex":"#6b6560"},{"code":"S 6005-Y80R","hex":"#6b615e"},{"code":"S 6010-B10G","hex":"#5a676b"},{"code":"S 6010-B30G","hex":"#5b6a6b"},{"code":"S 6010-B50G","hex":"#5c6b6b"},{"code":"S 6010-B70G","hex":"#5b6b69"},{"code":"S 6010-B90G","hex":"#586b63"},{"code":"S 6010-G10Y","hex":"#5c6b5d"},{"code":"S 6010-G30Y","hex":"#636b5a"},{"code":"S 6010-G50Y","hex":"#676b5a"},{"code":"S 6010-G70Y","hex":"#6a6b5c"},{"code":"S 6010-G90Y","hex":"#6b6a5b"},{"code":"S 6010-R10B","hex":"#6b5356"},{"code":"S 6010-R30B","hex":"#6b565c"},{"code":"S 6010-R50B","hex":"#6b5a68"},{"code":"S 6010-R70B","hex":"#5f596b"},{"code":"S 6010-R90B","hex":"#565d6b"},{"code":"S 6010-Y10R","hex":"#6b675a"},{"code":"S 6010-Y30R","hex":"#6b6459"},{"code":"S 6010-Y50R","hex":"#6b5f57"},{"code":"S 6010-Y70R","hex":"#6b5a55"},{"code":"S 6010-Y90R","hex":"#6b5553"},{"code":"S 6020-B","hex":"#495e6b"},{"code":"S 6020-B10G","hex":"#4b636b"},{"code":"S 6020-B30G","hex":"#4d686b"},{"code":"S 6020-B50G","hex":"#4d6b6a"},{"code":"S 6020-B70G","hex":"#4c6b67"},{"code":"S 6020-B90G","hex":"#486b5b"},{"code":"S 6020-G","hex":"#456b54"},{"code":"S 6020-G10Y","hex":"#4f6b51"},{"code":"S 6020-G30Y","hex":"#5c6b4c"},{"code":"S 6020-G50Y","hex":"#646b4b"},{"code":"S 6020-G70Y","hex":"#6a6b4d"},{"code":"S 6020-G90Y","hex":"#6b684d"},{"code":"S 6020-R","hex":"#6b3f42"},{"code":"S 6020-R10B","hex":"#6b4045"},{"code":"S 6020-R20B","hex":"#6b424a"},{"code":"S 6020-R30B","hex":"#6b4451"},{"code":"S 6020-R40B","hex":"#6b4759"},{"code":"S 6020-R50B","hex":"#6b4c65"},{"code":"S 6020-R60B","hex":"#614a6b"},{"code":"S 6020-R70B","hex":"#564a6b"},{"code":"S 6020-R80B","hex":"#41496b"},{"code":"S 6020-R90B","hex":"#44516b"},{"code":"S 6020-Y","hex":"#6b674c"},{"code":"S 6020-Y10R","hex":"#6b644b"},{"code":"S 6020-Y20R","hex":"#6b614a"},{"code":"S 6020-Y30R","hex":"#6b5d49"},{"code":"S 6020-Y40R","hex":"#6b5a47"},{"code":"S 6020-Y50R","hex":"#6b5646"},{"code":"S 6020-Y60R","hex":"#6b5245"},{"code":"S 6020-Y70R","hex":"#6b4d43"},{"code":"S 6020-Y80R","hex":"#6b4841"},{"code":"S 6020-Y90R","hex":"#6b4441"},{"code":"S 6030-B","hex":"#3c596b"},{"code":"S 6030-B10G","hex":"#3e606b"},{"code":"S 6030-B30G","hex":"#40676b"},{"code":"S 6030-B50G","hex":"#416b6a"},{"code":"S 6030-B70G","hex":"#3f6b65"},{"code":"S 6030-B90G","hex":"#3a6b55"},{"code":"S 6030-G","hex":"#376b4c"},{"code":"S 6030-G10Y","hex":"#456b46"},{"code":"S 6030-G30Y","hex":"#566b3f"},{"code":"S 6030-G50Y","hex":"#616b3e"},{"code":"S 6030-G70Y","hex":"#696b40"},{"code":"S 6030-G90Y","hex":"#6b6740"},{"code":"S 6030-R","hex":"#6b3034"},{"code":"S 6030-R10B","hex":"#6b3239"},{"code":"S 6030-R20B","hex":"#6b343f"},{"code":"S 6030-R30B","hex":"#6b3647"},{"code":"S 6030-R40B","hex":"#6b3a52"},{"code":"S 6030-R50B","hex":"#6b3e63"},{"code":"S 6030-R60B","hex":"#5c3d6b"},{"code":"S 6030-R70B","hex":"#4d3d6b"},{"code":"S 6030-R80B","hex":"#333d6b"},{"code":"S 6030-R90B","hex":"#36486b"},{"code":"S 6030-Y","hex":"#6b653f"},{"code":"S 6030-Y10R","hex":"#6b613e"},{"code":"S 6030-Y20R","hex":"#6b5c3d"},{"code":"S 6030-Y30R","hex":"#6b583b"},{"code":"S 6030-Y40R","hex":"#6b533a"},{"code":"S 6030-Y50R","hex":"#6b4e38"},{"code":"S 6030-Y60R","hex":"#6b4836"},{"code":"S 6030-Y70R","hex":"#6b4335"},{"code":"S 6030-Y80R","hex":"#6b3d33"},{"code":"S 6030-Y90R","hex":"#6b3733"},{"code":"S 6035-B60G","hex":"#3a6b67"},{"code":"S 6500-N","hex":"#595959"},{"code":"S 6502-B","hex":"#5a5d5e"},{"code":"S 6502-G","hex":"#5a5e5b"},{"code":"S 6502-R","hex":"#5e5959"},{"code":"S 6502-Y","hex":"#5e5d5b"},{"code":"S 6530-B30G","hex":"#385a5e"},{"code":"S 6530-B50G","hex":"#395e5d"},{"code":"S 6530-G10Y","hex":"#3c5e3e"},{"code":"S 6530-G50Y","hex":"#555e36"},{"code":"S 7000-N","hex":"#4c4c4c"},{"code":"S 7005-B20G","hex":"#4a4f50"},{"code":"S 7005-B80G","hex":"#4a504f"},{"code":"S 7005-G20Y","hex":"#4c504a"},{"code":"S 7005-G50Y","hex":"#4f504a"},{"code":"S 7005-G80Y","hex":"#50504a"},{"code":"S 7005-R20B","hex":"#504749"},{"code":"S 7005-R50B","hex":"#504a4f"},{"code":"S 7005-R80B","hex":"#474950"},{"code":"S 7005-Y20R","hex":"#504e4a"},{"code":"S 7005-Y50R","hex":"#504c48"},{"code":"S 7005-Y80R","hex":"#504947"},{"code":"S 7010-B10G","hex":"#434d50"},{"code":"S 7010-B30G","hex":"#444f50"},{"code":"S 7010-B50G","hex":"#455050"},{"code":"S 7010-B70G","hex":"#44504f"},{"code":"S 7010-B90G","hex":"#42504a"},{"code":"S 7010-G10Y","hex":"#455046"},{"code":"S 7010-G30Y","hex":"#4a5044"},{"code":"S 7010-G50Y","hex":"#4e5044"},{"code":"S 7010-G70Y","hex":"#505045"},{"code":"S 7010-G90Y","hex":"#504f44"},{"code":"S 7010-R10B","hex":"#503e40"},{"code":"S 7010-R30B","hex":"#504045"},{"code":"S 7010-R50B","hex":"#50444e"},{"code":"S 7010-R70B","hex":"#484350"},{"code":"S 7010-R90B","hex":"#404650"},{"code":"S 7010-Y10R","hex":"#504d44"},{"code":"S 7010-Y30R","hex":"#504b42"},{"code":"S 7010-Y50R","hex":"#504841"},{"code":"S 7010-Y70R","hex":"#50443f"},{"code":"S 7010-Y90R","hex":"#503f3e"},{"code":"S 7020-B","hex":"#374750"},{"code":"S 7020-B10G","hex":"#384a50"},{"code":"S 7020-B30G","hex":"#3a4e50"},{"code":"S 7020-B50G","hex":"#3a5050"},{"code":"S 7020-B70G","hex":"#39504d"},{"code":"S 7020-B90G","hex":"#365045"},{"code":"S 7020-G","hex":"#34503f"},{"code":"S 7020-G10Y","hex":"#3c503c"},{"code":"S 7020-G30Y","hex":"#455039"},{"code":"S 7020-G50Y","hex":"#4b5038"},{"code":"S 7020-G70Y","hex":"#4f503a"},{"code":"S 7020-G90Y","hex":"#504e3a"},{"code":"S 7020-R","hex":"#502f31"},{"code":"S 7020-R10B","hex":"#503034"},{"code":"S 7020-R20B","hex":"#503138"},{"code":"S 7020-R30B","hex":"#50333c"},{"code":"S 7020-R40B","hex":"#503543"},{"code":"S 7020-R50B","hex":"#50394c"},{"code":"S 7020-R60B","hex":"#483750"},{"code":"S 7020-R70B","hex":"#403750"},{"code":"S 7020-R80B","hex":"#313750"},{"code":"S 7020-R90B","hex":"#333d50"},{"code":"S 7020-Y","hex":"#504d39"},{"code":"S 7020-Y10R","hex":"#504b38"},{"code":"S 7020-Y20R","hex":"#504837"},{"code":"S 7020-Y30R","hex":"#504637"},{"code":"S 7020-Y40R","hex":"#504336"},{"code":"S 7020-Y50R","hex":"#504035"},{"code":"S 7020-Y60R","hex":"#503d33"},{"code":"S 7020-Y70R","hex":"#503a32"},{"code":"S 7020-Y80R","hex":"#503631"},{"code":"S 7020-Y90R","hex":"#503331"},{"code":"S 7500-N","hex":"#3f3f3f"},{"code":"S 7502-B","hex":"#414243"},{"code":"S 7502-G","hex":"#404341"},{"code":"S 7502-R","hex":"#433f40"},{"code":"S 7502-Y","hex":"#434341"},{"code":"S 8000-N","hex":"#323232"},{"code":"S 8005-B20G","hex":"#313536"},{"code":"S 8005-B80G","hex":"#313635"},{"code":"S 8005-G20Y","hex":"#333632"},{"code":"S 8005-G50Y","hex":"#353631"},{"code":"S 8005-G80Y","hex":"#363632"},{"code":"S 8005-R20B","hex":"#362f31"},{"code":"S 8005-R50B","hex":"#363135"},{"code":"S 8005-R80B","hex":"#2f3036"},{"code":"S 8005-Y20R","hex":"#363431"},{"code":"S 8005-Y50R","hex":"#363330"},{"code":"S 8005-Y80R","hex":"#36302f"},{"code":"S 8010-B10G","hex":"#2d3336"},{"code":"S 8010-B30G","hex":"#2e3536"},{"code":"S 8010-B50G","hex":"#2e3635"},{"code":"S 8010-B70G","hex":"#2d3634"},{"code":"S 8010-B90G","hex":"#2c3631"},{"code":"S 8010-G10Y","hex":"#2e362e"},{"code":"S 8010-G30Y","hex":"#31362d"},{"code":"S 8010-G50Y","hex":"#34362d"},{"code":"S 8010-G70Y","hex":"#35362e"},{"code":"S 8010-G90Y","hex":"#36352e"},{"code":"S 8010-R10B","hex":"#36292b"},{"code":"S 8010-R30B","hex":"#362b2e"},{"code":"S 8010-R50B","hex":"#362d34"},{"code":"S 8010-R70B","hex":"#302d36"},{"code":"S 8010-R90B","hex":"#2b2e36"},{"code":"S 8010-Y10R","hex":"#36342d"},{"code":"S 8010-Y30R","hex":"#36322c"},{"code":"S 8010-Y50R","hex":"#36302b"},{"code":"S 8010-Y70R","hex":"#362d2a"},{"code":"S 8010-Y90R","hex":"#362a2a"},{"code":"S 8500-N","hex":"#262626"},{"code":"S 8500-N","hex":"#262626"},{"code":"S 8502-B","hex":"#272828"},{"code":"S 8502-G","hex":"#272827"},{"code":"S 8502-R","hex":"#282626"},{"code":"S 8502-Y","hex":"#282827"},{"code":"S 8505-B20G","hex":"#252828"},{"code":"S 8505-B80G","hex":"#252828"},{"code":"S 8505-G20Y","hex":"#262825"},{"code":"S 8505-G80Y","hex":"#282825"},{"code":"S 8505-R20B","hex":"#282425"},{"code":"S 8505-R80B","hex":"#242428"},{"code":"S 8505-Y20R","hex":"#282725"},{"code":"S 8505-Y80R","hex":"#282423"}];
        for (var i = 0; i < ncsSwatches.length; i++) {
            ncsSwatches[i].code = "NC"+ncsSwatches[i].code;
            swatches.push(ncsSwatches[i]);
        }
    }
    if (includePantone || colorSystems == "*") {
        // From view-source: https://github.com/vanderlee/colorpicker/wiki/Pantone-colors-(csv,-semicolon-separated)
        // Converted to JSON via https://www.csvjson.com/csv2json
        var pantoneSwatches = [{"code":100,"r":244,"g":237,"b":124,"hex":"#F4ED7C"},{"code":101,"r":244,"g":237,"b":71,"hex":"#F4ED47"},{"code":102,"r":249,"g":232,"b":20,"hex":"#F9E814"},{"code":103,"r":198,"g":173,"b":15,"hex":"#C6AD0F"},{"code":104,"r":173,"g":155,"b":12,"hex":"#AD9B0C"},{"code":105,"r":130,"g":117,"b":15,"hex":"#82750F"},{"code":106,"r":247,"g":232,"b":89,"hex":"#F7E859"},{"code":107,"r":249,"g":229,"b":38,"hex":"#F9E526"},{"code":108,"r":249,"g":221,"b":22,"hex":"#F9DD16"},{"code":109,"r":249,"g":214,"b":22,"hex":"#F9D616"},{"code":110,"r":216,"g":181,"b":17,"hex":"#D8B511"},{"code":111,"r":170,"g":147,"b":10,"hex":"#AA930A"},{"code":112,"r":153,"g":132,"b":10,"hex":"#99840A"},{"code":113,"r":249,"g":229,"b":91,"hex":"#F9E55B"},{"code":114,"r":249,"g":226,"b":76,"hex":"#F9E24C"},{"code":115,"r":249,"g":224,"b":76,"hex":"#F9E04C"},{"code":116,"r":252,"g":209,"b":22,"hex":"#FCD116"},{"code":"116 2X","r":247,"g":181,"b":12,"hex":"#F7B50C"},{"code":117,"r":198,"g":160,"b":12,"hex":"#C6A00C"},{"code":118,"r":170,"g":142,"b":10,"hex":"#AA8E0A"},{"code":119,"r":137,"g":119,"b":25,"hex":"#897719"},{"code":120,"r":249,"g":226,"b":127,"hex":"#F9E27F"},{"code":1205,"r":247,"g":232,"b":170,"hex":"#F7E8AA"},{"code":121,"r":249,"g":224,"b":112,"hex":"#F9E070"},{"code":1215,"r":249,"g":224,"b":140,"hex":"#F9E08C"},{"code":122,"r":252,"g":216,"b":86,"hex":"#FCD856"},{"code":1225,"r":255,"g":204,"b":73,"hex":"#FFCC49"},{"code":123,"r":255,"g":198,"b":30,"hex":"#FFC61E"},{"code":1235,"r":252,"g":181,"b":20,"hex":"#FCB514"},{"code":124,"r":224,"g":170,"b":15,"hex":"#E0AA0F"},{"code":1245,"r":191,"g":145,"b":12,"hex":"#BF910C"},{"code":125,"r":181,"g":140,"b":10,"hex":"#B58C0A"},{"code":1255,"r":163,"g":127,"b":20,"hex":"#A37F14"},{"code":126,"r":163,"g":130,"b":5,"hex":"#A38205"},{"code":1265,"r":124,"g":99,"b":22,"hex":"#7C6316"},{"code":127,"r":244,"g":226,"b":135,"hex":"#F4E287"},{"code":128,"r":244,"g":219,"b":96,"hex":"#F4DB60"},{"code":129,"r":242,"g":209,"b":61,"hex":"#F2D13D"},{"code":130,"r":234,"g":175,"b":15,"hex":"#EAAF0F"},{"code":"130 2X","r":226,"g":145,"b":0,"hex":"#E29100"},{"code":131,"r":198,"g":147,"b":10,"hex":"#C6930A"},{"code":132,"r":158,"g":124,"b":10,"hex":"#9E7C0A"},{"code":133,"r":112,"g":91,"b":10,"hex":"#705B0A"},{"code":134,"r":255,"g":216,"b":127,"hex":"#FFD87F"},{"code":1345,"r":255,"g":214,"b":145,"hex":"#FFD691"},{"code":135,"r":252,"g":201,"b":99,"hex":"#FCC963"},{"code":1355,"r":252,"g":206,"b":135,"hex":"#FCCE87"},{"code":136,"r":252,"g":191,"b":73,"hex":"#FCBF49"},{"code":1365,"r":252,"g":186,"b":94,"hex":"#FCBA5E"},{"code":137,"r":252,"g":163,"b":17,"hex":"#FCA311"},{"code":1375,"r":249,"g":155,"b":12,"hex":"#F99B0C"},{"code":138,"r":216,"g":140,"b":2,"hex":"#D88C02"},{"code":1385,"r":204,"g":122,"b":2,"hex":"#CC7A02"},{"code":139,"r":175,"g":117,"b":5,"hex":"#AF7505"},{"code":1395,"r":153,"g":96,"b":7,"hex":"#996007"},{"code":140,"r":122,"g":91,"b":17,"hex":"#7A5B11"},{"code":1405,"r":107,"g":71,"b":20,"hex":"#6B4714"},{"code":141,"r":242,"g":206,"b":104,"hex":"#F2CE68"},{"code":142,"r":242,"g":191,"b":73,"hex":"#F2BF49"},{"code":143,"r":239,"g":178,"b":45,"hex":"#EFB22D"},{"code":144,"r":226,"g":140,"b":5,"hex":"#E28C05"},{"code":145,"r":198,"g":127,"b":7,"hex":"#C67F07"},{"code":146,"r":158,"g":107,"b":5,"hex":"#9E6B05"},{"code":147,"r":114,"g":94,"b":38,"hex":"#725E26"},{"code":148,"r":255,"g":214,"b":155,"hex":"#FFD69B"},{"code":1485,"r":255,"g":183,"b":119,"hex":"#FFB777"},{"code":149,"r":252,"g":204,"b":147,"hex":"#FCCC93"},{"code":1495,"r":255,"g":153,"b":63,"hex":"#FF993F"},{"code":150,"r":252,"g":173,"b":86,"hex":"#FCAD56"},{"code":1505,"r":244,"g":124,"b":0,"hex":"#F47C00"},{"code":151,"r":247,"g":127,"b":0,"hex":"#F77F00"},{"code":152,"r":221,"g":117,"b":0,"hex":"#DD7500"},{"code":1525,"r":181,"g":84,"b":0,"hex":"#B55400"},{"code":153,"r":188,"g":109,"b":10,"hex":"#BC6D0A"},{"code":1535,"r":140,"g":68,"b":0,"hex":"#8C4400"},{"code":154,"r":153,"g":89,"b":5,"hex":"#995905"},{"code":1545,"r":71,"g":34,"b":0,"hex":"#472200"},{"code":155,"r":244,"g":219,"b":170,"hex":"#F4DBAA"},{"code":1555,"r":249,"g":191,"b":158,"hex":"#F9BF9E"},{"code":156,"r":242,"g":198,"b":140,"hex":"#F2C68C"},{"code":1565,"r":252,"g":165,"b":119,"hex":"#FCA577"},{"code":157,"r":237,"g":160,"b":79,"hex":"#EDA04F"},{"code":1575,"r":252,"g":135,"b":68,"hex":"#FC8744"},{"code":158,"r":232,"g":117,"b":17,"hex":"#E87511"},{"code":1585,"r":249,"g":107,"b":7,"hex":"#F96B07"},{"code":159,"r":198,"g":96,"b":5,"hex":"#C66005"},{"code":1595,"r":209,"g":91,"b":5,"hex":"#D15B05"},{"code":160,"r":158,"g":84,"b":10,"hex":"#9E540A"},{"code":1605,"r":160,"g":79,"b":17,"hex":"#A04F11"},{"code":161,"r":99,"g":58,"b":17,"hex":"#633A11"},{"code":1615,"r":132,"g":63,"b":15,"hex":"#843F0F"},{"code":162,"r":249,"g":198,"b":170,"hex":"#F9C6AA"},{"code":1625,"r":249,"g":165,"b":140,"hex":"#F9A58C"},{"code":163,"r":252,"g":158,"b":112,"hex":"#FC9E70"},{"code":1635,"r":249,"g":142,"b":109,"hex":"#F98E6D"},{"code":164,"r":252,"g":127,"b":63,"hex":"#FC7F3F"},{"code":1645,"r":249,"g":114,"b":66,"hex":"#F97242"},{"code":165,"r":249,"g":99,"b":2,"hex":"#F96302"},{"code":"165 2X","r":234,"g":79,"b":0,"hex":"#EA4F00"},{"code":1655,"r":249,"g":86,"b":2,"hex":"#F95602"},{"code":166,"r":221,"g":89,"b":0,"hex":"#DD5900"},{"code":1665,"r":221,"g":79,"b":5,"hex":"#DD4F05"},{"code":167,"r":188,"g":79,"b":7,"hex":"#BC4F07"},{"code":1675,"r":165,"g":63,"b":15,"hex":"#A53F0F"},{"code":168,"r":109,"g":48,"b":17,"hex":"#6D3011"},{"code":1685,"r":132,"g":53,"b":17,"hex":"#843511"},{"code":169,"r":249,"g":186,"b":170,"hex":"#F9BAAA"},{"code":170,"r":249,"g":137,"b":114,"hex":"#F98972"},{"code":171,"r":249,"g":96,"b":58,"hex":"#F9603A"},{"code":172,"r":247,"g":73,"b":2,"hex":"#F74902"},{"code":173,"r":209,"g":68,"b":20,"hex":"#D14414"},{"code":174,"r":147,"g":51,"b":17,"hex":"#933311"},{"code":175,"r":109,"g":51,"b":33,"hex":"#6D3321"},{"code":176,"r":249,"g":175,"b":173,"hex":"#F9AFAD"},{"code":1765,"r":249,"g":158,"b":163,"hex":"#F99EA3"},{"code":1767,"r":249,"g":178,"b":183,"hex":"#F9B2B7"},{"code":177,"r":249,"g":130,"b":127,"hex":"#F9827F"},{"code":1775,"r":249,"g":132,"b":142,"hex":"#F9848E"},{"code":1777,"r":252,"g":102,"b":117,"hex":"#FC6675"},{"code":178,"r":249,"g":94,"b":89,"hex":"#F95E59"},{"code":1785,"r":252,"g":79,"b":89,"hex":"#FC4F59"},{"code":1787,"r":244,"g":63,"b":79,"hex":"#F43F4F"},{"code":1788,"r":239,"g":43,"b":45,"hex":"#EF2B2D"},{"code":"1788 2X","r":214,"g":33,"b":0,"hex":"#D62100"},{"code":179,"r":226,"g":61,"b":40,"hex":"#E23D28"},{"code":1795,"r":214,"g":40,"b":40,"hex":"#D62828"},{"code":1797,"r":204,"g":45,"b":48,"hex":"#CC2D30"},{"code":180,"r":193,"g":56,"b":40,"hex":"#C13828"},{"code":1805,"r":175,"g":38,"b":38,"hex":"#AF2626"},{"code":1807,"r":160,"g":48,"b":51,"hex":"#A03033"},{"code":181,"r":124,"g":45,"b":35,"hex":"#7C2D23"},{"code":1810,"r":124,"g":33,"b":30,"hex":"#7C211E"},{"code":1817,"r":91,"g":45,"b":40,"hex":"#5B2D28"},{"code":182,"r":249,"g":191,"b":193,"hex":"#F9BFC1"},{"code":183,"r":252,"g":140,"b":153,"hex":"#FC8C99"},{"code":184,"r":252,"g":94,"b":114,"hex":"#FC5E72"},{"code":185,"r":232,"g":17,"b":45,"hex":"#E8112D"},{"code":"185 2X","r":209,"g":22,"b":0,"hex":"#D11600"},{"code":186,"r":206,"g":17,"b":38,"hex":"#CE1126"},{"code":187,"r":175,"g":30,"b":45,"hex":"#AF1E2D"},{"code":188,"r":124,"g":33,"b":40,"hex":"#7C2128"},{"code":189,"r":255,"g":163,"b":178,"hex":"#FFA3B2"},{"code":1895,"r":252,"g":191,"b":201,"hex":"#FCBFC9"},{"code":190,"r":252,"g":117,"b":142,"hex":"#FC758E"},{"code":1905,"r":252,"g":155,"b":178,"hex":"#FC9BB2"},{"code":191,"r":244,"g":71,"b":107,"hex":"#F4476B"},{"code":1915,"r":244,"g":84,"b":124,"hex":"#F4547C"},{"code":192,"r":229,"g":5,"b":58,"hex":"#E5053A"},{"code":1925,"r":224,"g":7,"b":71,"hex":"#E00747"},{"code":193,"r":196,"g":0,"b":67,"hex":"#C40043"},{"code":1935,"r":193,"g":5,"b":56,"hex":"#C10538"},{"code":194,"r":153,"g":33,"b":53,"hex":"#992135"},{"code":1945,"r":168,"g":12,"b":53,"hex":"#A80C35"},{"code":1955,"r":147,"g":22,"b":56,"hex":"#931638"},{"code":196,"r":250,"g":213,"b":225,"hex":"#FAD5E1"},{"code":197,"r":246,"g":165,"b":190,"hex":"#F6A5BE"},{"code":198,"r":239,"g":91,"b":132,"hex":"#EF5B84"},{"code":199,"r":160,"g":39,"b":75,"hex":"#A0274B"},{"code":200,"r":196,"g":30,"b":58,"hex":"#C41E3A"},{"code":201,"r":163,"g":38,"b":56,"hex":"#A32638"},{"code":202,"r":140,"g":38,"b":51,"hex":"#8C2633"},{"code":203,"r":242,"g":175,"b":193,"hex":"#F2AFC1"},{"code":204,"r":237,"g":122,"b":158,"hex":"#ED7A9E"},{"code":205,"r":229,"g":76,"b":124,"hex":"#E54C7C"},{"code":206,"r":211,"g":5,"b":71,"hex":"#D30547"},{"code":207,"r":192,"g":0,"b":78,"hex":"#C0004E"},{"code":208,"r":142,"g":35,"b":68,"hex":"#8E2344"},{"code":209,"r":117,"g":38,"b":61,"hex":"#75263D"},{"code":210,"r":255,"g":160,"b":191,"hex":"#FFA0BF"},{"code":211,"r":255,"g":119,"b":168,"hex":"#FF77A8"},{"code":212,"r":249,"g":79,"b":142,"hex":"#F94F8E"},{"code":213,"r":234,"g":15,"b":107,"hex":"#EA0F6B"},{"code":214,"r":204,"g":2,"b":86,"hex":"#CC0256"},{"code":215,"r":165,"g":5,"b":68,"hex":"#A50544"},{"code":216,"r":124,"g":30,"b":63,"hex":"#7C1E3F"},{"code":217,"r":244,"g":191,"b":209,"hex":"#F4BFD1"},{"code":218,"r":237,"g":114,"b":170,"hex":"#ED72AA"},{"code":219,"r":226,"g":40,"b":130,"hex":"#E22882"},{"code":220,"r":170,"g":0,"b":79,"hex":"#AA004F"},{"code":221,"r":147,"g":0,"b":66,"hex":"#930042"},{"code":222,"r":112,"g":25,"b":61,"hex":"#70193D"},{"code":223,"r":249,"g":147,"b":196,"hex":"#F993C4"},{"code":224,"r":244,"g":107,"b":175,"hex":"#F46BAF"},{"code":225,"r":237,"g":40,"b":147,"hex":"#ED2893"},{"code":226,"r":214,"g":2,"b":112,"hex":"#D60270"},{"code":227,"r":173,"g":0,"b":91,"hex":"#AD005B"},{"code":228,"r":140,"g":0,"b":76,"hex":"#8C004C"},{"code":229,"r":109,"g":33,"b":63,"hex":"#6D213F"},{"code":230,"r":255,"g":160,"b":204,"hex":"#FFA0CC"},{"code":231,"r":252,"g":112,"b":186,"hex":"#FC70BA"},{"code":232,"r":244,"g":63,"b":165,"hex":"#F43FA5"},{"code":233,"r":206,"g":0,"b":124,"hex":"#CE007C"},{"code":234,"r":170,"g":0,"b":102,"hex":"#AA0066"},{"code":235,"r":142,"g":5,"b":84,"hex":"#8E0554"},{"code":236,"r":249,"g":175,"b":211,"hex":"#F9AFD3"},{"code":2365,"r":247,"g":196,"b":216,"hex":"#F7C4D8"},{"code":237,"r":244,"g":132,"b":196,"hex":"#F484C4"},{"code":2375,"r":234,"g":107,"b":191,"hex":"#EA6BBF"},{"code":238,"r":237,"g":79,"b":175,"hex":"#ED4FAF"},{"code":2385,"r":219,"g":40,"b":165,"hex":"#DB28A5"},{"code":239,"r":224,"g":33,"b":158,"hex":"#E0219E"},{"code":2395,"r":196,"g":0,"b":140,"hex":"#C4008C"},{"code":240,"r":196,"g":15,"b":137,"hex":"#C40F89"},{"code":2405,"r":168,"g":0,"b":122,"hex":"#A8007A"},{"code":241,"r":173,"g":0,"b":117,"hex":"#AD0075"},{"code":2415,"r":155,"g":0,"b":112,"hex":"#9B0070"},{"code":242,"r":124,"g":28,"b":81,"hex":"#7C1C51"},{"code":2425,"r":135,"g":0,"b":91,"hex":"#87005B"},{"code":243,"r":242,"g":186,"b":216,"hex":"#F2BAD8"},{"code":244,"r":237,"g":160,"b":211,"hex":"#EDA0D3"},{"code":245,"r":232,"g":127,"b":201,"hex":"#E87FC9"},{"code":246,"r":204,"g":0,"b":160,"hex":"#CC00A0"},{"code":247,"r":183,"g":0,"b":142,"hex":"#B7008E"},{"code":248,"r":163,"g":5,"b":127,"hex":"#A3057F"},{"code":249,"r":127,"g":40,"b":96,"hex":"#7F2860"},{"code":250,"r":237,"g":196,"b":221,"hex":"#EDC4DD"},{"code":251,"r":226,"g":158,"b":214,"hex":"#E29ED6"},{"code":252,"r":211,"g":107,"b":198,"hex":"#D36BC6"},{"code":253,"r":175,"g":35,"b":165,"hex":"#AF23A5"},{"code":254,"r":160,"g":45,"b":150,"hex":"#A02D96"},{"code":255,"r":119,"g":45,"b":107,"hex":"#772D6B"},{"code":256,"r":229,"g":196,"b":214,"hex":"#E5C4D6"},{"code":2562,"r":216,"g":168,"b":216,"hex":"#D8A8D8"},{"code":2563,"r":209,"g":160,"b":204,"hex":"#D1A0CC"},{"code":2567,"r":191,"g":147,"b":204,"hex":"#BF93CC"},{"code":257,"r":211,"g":165,"b":201,"hex":"#D3A5C9"},{"code":2572,"r":198,"g":135,"b":209,"hex":"#C687D1"},{"code":2573,"r":186,"g":124,"b":188,"hex":"#BA7CBC"},{"code":2577,"r":170,"g":114,"b":191,"hex":"#AA72BF"},{"code":258,"r":155,"g":79,"b":150,"hex":"#9B4F96"},{"code":2582,"r":170,"g":71,"b":186,"hex":"#AA47BA"},{"code":2583,"r":158,"g":79,"b":165,"hex":"#9E4FA5"},{"code":2587,"r":142,"g":71,"b":173,"hex":"#8E47AD"},{"code":259,"r":114,"g":22,"b":107,"hex":"#72166B"},{"code":2592,"r":147,"g":15,"b":165,"hex":"#930FA5"},{"code":2593,"r":135,"g":43,"b":147,"hex":"#872B93"},{"code":2597,"r":102,"g":0,"b":140,"hex":"#66008C"},{"code":260,"r":104,"g":30,"b":91,"hex":"#681E5B"},{"code":2602,"r":130,"g":12,"b":142,"hex":"#820C8E"},{"code":2603,"r":112,"g":20,"b":122,"hex":"#70147A"},{"code":2607,"r":91,"g":2,"b":122,"hex":"#5B027A"},{"code":261,"r":94,"g":33,"b":84,"hex":"#5E2154"},{"code":2612,"r":112,"g":30,"b":114,"hex":"#701E72"},{"code":2613,"r":102,"g":17,"b":109,"hex":"#66116D"},{"code":2617,"r":86,"g":12,"b":112,"hex":"#560C70"},{"code":262,"r":84,"g":35,"b":68,"hex":"#542344"},{"code":2622,"r":96,"g":45,"b":89,"hex":"#602D59"},{"code":2623,"r":91,"g":25,"b":94,"hex":"#5B195E"},{"code":2627,"r":76,"g":20,"b":94,"hex":"#4C145E"},{"code":263,"r":224,"g":206,"b":224,"hex":"#E0CEE0"},{"code":2635,"r":201,"g":173,"b":216,"hex":"#C9ADD8"},{"code":264,"r":198,"g":170,"b":219,"hex":"#C6AADB"},{"code":2645,"r":181,"g":145,"b":209,"hex":"#B591D1"},{"code":265,"r":150,"g":99,"b":196,"hex":"#9663C4"},{"code":2655,"r":155,"g":109,"b":198,"hex":"#9B6DC6"},{"code":266,"r":109,"g":40,"b":170,"hex":"#6D28AA"},{"code":2665,"r":137,"g":79,"b":191,"hex":"#894FBF"},{"code":267,"r":89,"g":17,"b":142,"hex":"#59118E"},{"code":268,"r":79,"g":33,"b":112,"hex":"#4F2170"},{"code":2685,"r":86,"g":0,"b":140,"hex":"#56008C"},{"code":269,"r":68,"g":35,"b":89,"hex":"#442359"},{"code":2695,"r":68,"g":35,"b":94,"hex":"#44235E"},{"code":270,"r":186,"g":175,"b":211,"hex":"#BAAFD3"},{"code":2705,"r":173,"g":158,"b":211,"hex":"#AD9ED3"},{"code":2706,"r":209,"g":206,"b":221,"hex":"#D1CEDD"},{"code":2707,"r":191,"g":209,"b":229,"hex":"#BFD1E5"},{"code":2708,"r":175,"g":188,"b":219,"hex":"#AFBCDB"},{"code":271,"r":158,"g":145,"b":198,"hex":"#9E91C6"},{"code":2715,"r":147,"g":122,"b":204,"hex":"#937ACC"},{"code":2716,"r":165,"g":160,"b":214,"hex":"#A5A0D6"},{"code":2717,"r":165,"g":186,"b":224,"hex":"#A5BAE0"},{"code":2718,"r":91,"g":119,"b":204,"hex":"#5B77CC"},{"code":272,"r":137,"g":119,"b":186,"hex":"#8977BA"},{"code":2725,"r":114,"g":81,"b":188,"hex":"#7251BC"},{"code":2726,"r":102,"g":86,"b":188,"hex":"#6656BC"},{"code":2727,"r":94,"g":104,"b":196,"hex":"#5E68C4"},{"code":2728,"r":48,"g":68,"b":181,"hex":"#3044B5"},{"code":273,"r":56,"g":25,"b":122,"hex":"#38197A"},{"code":2735,"r":79,"g":0,"b":147,"hex":"#4F0093"},{"code":2736,"r":73,"g":48,"b":173,"hex":"#4930AD"},{"code":2738,"r":45,"g":0,"b":142,"hex":"#2D008E"},{"code":274,"r":43,"g":17,"b":102,"hex":"#2B1166"},{"code":2745,"r":63,"g":0,"b":119,"hex":"#3F0077"},{"code":2746,"r":63,"g":40,"b":147,"hex":"#3F2893"},{"code":2747,"r":28,"g":20,"b":107,"hex":"#1C146B"},{"code":2748,"r":30,"g":28,"b":119,"hex":"#1E1C77"},{"code":275,"r":38,"g":15,"b":84,"hex":"#260F54"},{"code":2755,"r":53,"g":0,"b":109,"hex":"#35006D"},{"code":2756,"r":51,"g":40,"b":117,"hex":"#332875"},{"code":2757,"r":20,"g":22,"b":84,"hex":"#141654"},{"code":2758,"r":25,"g":33,"b":104,"hex":"#192168"},{"code":276,"r":43,"g":33,"b":71,"hex":"#2B2147"},{"code":2765,"r":43,"g":12,"b":86,"hex":"#2B0C56"},{"code":2766,"r":43,"g":38,"b":91,"hex":"#2B265B"},{"code":2767,"r":20,"g":33,"b":61,"hex":"#14213D"},{"code":2768,"r":17,"g":33,"b":81,"hex":"#112151"},{"code":277,"r":181,"g":209,"b":232,"hex":"#B5D1E8"},{"code":278,"r":153,"g":186,"b":221,"hex":"#99BADD"},{"code":279,"r":102,"g":137,"b":204,"hex":"#6689CC"},{"code":280,"r":0,"g":43,"b":127,"hex":"#002B7F"},{"code":281,"r":0,"g":40,"b":104,"hex":"#002868"},{"code":282,"r":0,"g":38,"b":84,"hex":"#002654"},{"code":283,"r":155,"g":196,"b":226,"hex":"#9BC4E2"},{"code":284,"r":117,"g":170,"b":219,"hex":"#75AADB"},{"code":285,"r":58,"g":117,"b":196,"hex":"#3A75C4"},{"code":286,"r":0,"g":56,"b":168,"hex":"#0038A8"},{"code":287,"r":0,"g":56,"b":147,"hex":"#003893"},{"code":288,"r":0,"g":51,"b":127,"hex":"#00337F"},{"code":289,"r":0,"g":38,"b":73,"hex":"#002649"},{"code":290,"r":196,"g":216,"b":226,"hex":"#C4D8E2"},{"code":2905,"r":147,"g":198,"b":224,"hex":"#93C6E0"},{"code":291,"r":168,"g":206,"b":226,"hex":"#A8CEE2"},{"code":2915,"r":96,"g":175,"b":221,"hex":"#60AFDD"},{"code":292,"r":117,"g":178,"b":221,"hex":"#75B2DD"},{"code":2925,"r":0,"g":142,"b":214,"hex":"#008ED6"},{"code":293,"r":0,"g":81,"b":186,"hex":"#0051BA"},{"code":2935,"r":0,"g":91,"b":191,"hex":"#005BBF"},{"code":294,"r":0,"g":63,"b":135,"hex":"#003F87"},{"code":2945,"r":0,"g":84,"b":160,"hex":"#0054A0"},{"code":295,"r":0,"g":56,"b":107,"hex":"#00386B"},{"code":2955,"r":0,"g":61,"b":107,"hex":"#003D6B"},{"code":296,"r":0,"g":45,"b":71,"hex":"#002D47"},{"code":2965,"r":0,"g":51,"b":76,"hex":"#00334C"},{"code":297,"r":130,"g":198,"b":226,"hex":"#82C6E2"},{"code":2975,"r":186,"g":224,"b":226,"hex":"#BAE0E2"},{"code":298,"r":81,"g":181,"b":224,"hex":"#51B5E0"},{"code":2985,"r":81,"g":191,"b":226,"hex":"#51BFE2"},{"code":299,"r":0,"g":163,"b":221,"hex":"#00A3DD"},{"code":2995,"r":0,"g":165,"b":219,"hex":"#00A5DB"},{"code":300,"r":0,"g":114,"b":198,"hex":"#0072C6"},{"code":3005,"r":0,"g":132,"b":201,"hex":"#0084C9"},{"code":301,"r":0,"g":91,"b":153,"hex":"#005B99"},{"code":3015,"r":0,"g":112,"b":158,"hex":"#00709E"},{"code":302,"r":0,"g":79,"b":109,"hex":"#004F6D"},{"code":3025,"r":0,"g":84,"b":107,"hex":"#00546B"},{"code":303,"r":0,"g":63,"b":84,"hex":"#003F54"},{"code":3035,"r":0,"g":68,"b":84,"hex":"#004454"},{"code":304,"r":165,"g":221,"b":226,"hex":"#A5DDE2"},{"code":305,"r":112,"g":206,"b":226,"hex":"#70CEE2"},{"code":306,"r":0,"g":188,"b":226,"hex":"#00BCE2"},{"code":"306 2X","r":0,"g":163,"b":209,"hex":"#00A3D1"},{"code":307,"r":0,"g":122,"b":165,"hex":"#007AA5"},{"code":308,"r":0,"g":96,"b":124,"hex":"#00607C"},{"code":309,"r":0,"g":63,"b":73,"hex":"#003F49"},{"code":310,"r":114,"g":209,"b":221,"hex":"#72D1DD"},{"code":3105,"r":127,"g":214,"b":219,"hex":"#7FD6DB"},{"code":311,"r":40,"g":196,"b":216,"hex":"#28C4D8"},{"code":3115,"r":45,"g":198,"b":214,"hex":"#2DC6D6"},{"code":312,"r":0,"g":173,"b":198,"hex":"#00ADC6"},{"code":3125,"r":0,"g":183,"b":198,"hex":"#00B7C6"},{"code":313,"r":0,"g":153,"b":181,"hex":"#0099B5"},{"code":3135,"r":0,"g":155,"b":170,"hex":"#009BAA"},{"code":314,"r":0,"g":130,"b":155,"hex":"#00829B"},{"code":3145,"r":0,"g":132,"b":142,"hex":"#00848E"},{"code":315,"r":0,"g":107,"b":119,"hex":"#006B77"},{"code":3155,"r":0,"g":109,"b":117,"hex":"#006D75"},{"code":316,"r":0,"g":73,"b":79,"hex":"#00494F"},{"code":3165,"r":0,"g":86,"b":91,"hex":"#00565B"},{"code":317,"r":201,"g":232,"b":221,"hex":"#C9E8DD"},{"code":318,"r":147,"g":221,"b":219,"hex":"#93DDDB"},{"code":319,"r":76,"g":206,"b":209,"hex":"#4CCED1"},{"code":320,"r":0,"g":158,"b":160,"hex":"#009EA0"},{"code":"320 2X","r":0,"g":127,"b":130,"hex":"#007F82"},{"code":321,"r":0,"g":135,"b":137,"hex":"#008789"},{"code":322,"r":0,"g":114,"b":114,"hex":"#007272"},{"code":323,"r":0,"g":102,"b":99,"hex":"#006663"},{"code":324,"r":170,"g":221,"b":214,"hex":"#AADDD6"},{"code":3242,"r":135,"g":221,"b":209,"hex":"#87DDD1"},{"code":3245,"r":140,"g":224,"b":209,"hex":"#8CE0D1"},{"code":3248,"r":122,"g":211,"b":193,"hex":"#7AD3C1"},{"code":325,"r":86,"g":201,"b":193,"hex":"#56C9C1"},{"code":3252,"r":86,"g":214,"b":201,"hex":"#56D6C9"},{"code":3255,"r":71,"g":214,"b":193,"hex":"#47D6C1"},{"code":3258,"r":53,"g":196,"b":175,"hex":"#35C4AF"},{"code":326,"r":0,"g":178,"b":170,"hex":"#00B2AA"},{"code":3262,"r":0,"g":193,"b":181,"hex":"#00C1B5"},{"code":3265,"r":0,"g":198,"b":178,"hex":"#00C6B2"},{"code":3268,"r":0,"g":175,"b":153,"hex":"#00AF99"},{"code":327,"r":0,"g":140,"b":130,"hex":"#008C82"},{"code":"327 2X","r":0,"g":137,"b":119,"hex":"#008977"},{"code":3272,"r":0,"g":170,"b":158,"hex":"#00AA9E"},{"code":3275,"r":0,"g":178,"b":160,"hex":"#00B2A0"},{"code":3278,"r":0,"g":155,"b":132,"hex":"#009B84"},{"code":328,"r":0,"g":119,"b":112,"hex":"#007770"},{"code":3282,"r":0,"g":140,"b":130,"hex":"#008C82"},{"code":3285,"r":0,"g":153,"b":135,"hex":"#009987"},{"code":3288,"r":0,"g":130,"b":112,"hex":"#008270"},{"code":329,"r":0,"g":109,"b":102,"hex":"#006D66"},{"code":3292,"r":0,"g":96,"b":86,"hex":"#006056"},{"code":3295,"r":0,"g":130,"b":114,"hex":"#008272"},{"code":3298,"r":0,"g":107,"b":91,"hex":"#006B5B"},{"code":330,"r":0,"g":89,"b":81,"hex":"#005951"},{"code":3302,"r":0,"g":73,"b":63,"hex":"#00493F"},{"code":3305,"r":0,"g":79,"b":66,"hex":"#004F42"},{"code":3308,"r":0,"g":68,"b":56,"hex":"#004438"},{"code":331,"r":186,"g":234,"b":214,"hex":"#BAEAD6"},{"code":332,"r":160,"g":229,"b":206,"hex":"#A0E5CE"},{"code":333,"r":94,"g":221,"b":193,"hex":"#5EDDC1"},{"code":334,"r":0,"g":153,"b":124,"hex":"#00997C"},{"code":335,"r":0,"g":124,"b":102,"hex":"#007C66"},{"code":336,"r":0,"g":104,"b":84,"hex":"#006854"},{"code":337,"r":155,"g":219,"b":193,"hex":"#9BDBC1"},{"code":3375,"r":142,"g":226,"b":188,"hex":"#8EE2BC"},{"code":338,"r":122,"g":209,"b":181,"hex":"#7AD1B5"},{"code":3385,"r":84,"g":216,"b":168,"hex":"#54D8A8"},{"code":339,"r":0,"g":178,"b":140,"hex":"#00B28C"},{"code":3395,"r":0,"g":201,"b":147,"hex":"#00C993"},{"code":340,"r":0,"g":153,"b":119,"hex":"#009977"},{"code":3405,"r":0,"g":178,"b":122,"hex":"#00B27A"},{"code":341,"r":0,"g":122,"b":94,"hex":"#007A5E"},{"code":3415,"r":0,"g":124,"b":89,"hex":"#007C59"},{"code":342,"r":0,"g":107,"b":84,"hex":"#006B54"},{"code":3425,"r":0,"g":104,"b":71,"hex":"#006847"},{"code":343,"r":0,"g":86,"b":63,"hex":"#00563F"},{"code":3435,"r":2,"g":73,"b":48,"hex":"#024930"},{"code":344,"r":181,"g":226,"b":191,"hex":"#B5E2BF"},{"code":345,"r":150,"g":216,"b":175,"hex":"#96D8AF"},{"code":346,"r":112,"g":206,"b":155,"hex":"#70CE9B"},{"code":347,"r":0,"g":158,"b":96,"hex":"#009E60"},{"code":348,"r":0,"g":135,"b":81,"hex":"#008751"},{"code":349,"r":0,"g":107,"b":63,"hex":"#006B3F"},{"code":350,"r":35,"g":79,"b":51,"hex":"#234F33"},{"code":351,"r":181,"g":232,"b":191,"hex":"#B5E8BF"},{"code":352,"r":153,"g":229,"b":178,"hex":"#99E5B2"},{"code":353,"r":132,"g":226,"b":168,"hex":"#84E2A8"},{"code":354,"r":0,"g":183,"b":96,"hex":"#00B760"},{"code":355,"r":0,"g":158,"b":73,"hex":"#009E49"},{"code":356,"r":0,"g":122,"b":61,"hex":"#007A3D"},{"code":357,"r":33,"g":91,"b":51,"hex":"#215B33"},{"code":358,"r":170,"g":221,"b":150,"hex":"#AADD96"},{"code":359,"r":160,"g":219,"b":142,"hex":"#A0DB8E"},{"code":360,"r":96,"g":198,"b":89,"hex":"#60C659"},{"code":361,"r":30,"g":181,"b":58,"hex":"#1EB53A"},{"code":362,"r":51,"g":158,"b":53,"hex":"#339E35"},{"code":363,"r":61,"g":142,"b":51,"hex":"#3D8E33"},{"code":364,"r":58,"g":119,"b":40,"hex":"#3A7728"},{"code":365,"r":211,"g":232,"b":163,"hex":"#D3E8A3"},{"code":366,"r":196,"g":229,"b":142,"hex":"#C4E58E"},{"code":367,"r":170,"g":221,"b":109,"hex":"#AADD6D"},{"code":368,"r":91,"g":191,"b":33,"hex":"#5BBF21"},{"code":"368 2X","r":0,"g":158,"b":15,"hex":"#009E0F"},{"code":369,"r":86,"g":170,"b":28,"hex":"#56AA1C"},{"code":370,"r":86,"g":142,"b":20,"hex":"#568E14"},{"code":371,"r":86,"g":107,"b":33,"hex":"#566B21"},{"code":372,"r":216,"g":237,"b":150,"hex":"#D8ED96"},{"code":373,"r":206,"g":234,"b":130,"hex":"#CEEA82"},{"code":374,"r":186,"g":232,"b":96,"hex":"#BAE860"},{"code":375,"r":140,"g":214,"b":0,"hex":"#8CD600"},{"code":"375 2X","r":84,"g":188,"b":0,"hex":"#54BC00"},{"code":376,"r":127,"g":186,"b":0,"hex":"#7FBA00"},{"code":377,"r":112,"g":147,"b":2,"hex":"#709302"},{"code":378,"r":86,"g":99,"b":20,"hex":"#566314"},{"code":379,"r":224,"g":234,"b":104,"hex":"#E0EA68"},{"code":380,"r":214,"g":229,"b":66,"hex":"#D6E542"},{"code":381,"r":204,"g":226,"b":38,"hex":"#CCE226"},{"code":382,"r":186,"g":216,"b":10,"hex":"#BAD80A"},{"code":"382 2X","r":158,"g":196,"b":0,"hex":"#9EC400"},{"code":383,"r":163,"g":175,"b":7,"hex":"#A3AF07"},{"code":384,"r":147,"g":153,"b":5,"hex":"#939905"},{"code":385,"r":112,"g":112,"b":20,"hex":"#707014"},{"code":386,"r":232,"g":237,"b":96,"hex":"#E8ED60"},{"code":387,"r":224,"g":237,"b":68,"hex":"#E0ED44"},{"code":388,"r":214,"g":232,"b":15,"hex":"#D6E80F"},{"code":389,"r":206,"g":224,"b":7,"hex":"#CEE007"},{"code":390,"r":186,"g":196,"b":5,"hex":"#BAC405"},{"code":391,"r":158,"g":158,"b":7,"hex":"#9E9E07"},{"code":392,"r":132,"g":130,"b":5,"hex":"#848205"},{"code":393,"r":242,"g":239,"b":135,"hex":"#F2EF87"},{"code":3935,"r":242,"g":237,"b":109,"hex":"#F2ED6D"},{"code":394,"r":234,"g":237,"b":53,"hex":"#EAED35"},{"code":3945,"r":239,"g":234,"b":7,"hex":"#EFEA07"},{"code":395,"r":229,"g":232,"b":17,"hex":"#E5E811"},{"code":3955,"r":237,"g":226,"b":17,"hex":"#EDE211"},{"code":396,"r":224,"g":226,"b":12,"hex":"#E0E20C"},{"code":3965,"r":232,"g":221,"b":17,"hex":"#E8DD11"},{"code":397,"r":193,"g":191,"b":10,"hex":"#C1BF0A"},{"code":3975,"r":181,"g":168,"b":12,"hex":"#B5A80C"},{"code":398,"r":175,"g":168,"b":10,"hex":"#AFA80A"},{"code":3985,"r":153,"g":140,"b":10,"hex":"#998C0A"},{"code":399,"r":153,"g":142,"b":7,"hex":"#998E07"},{"code":3995,"r":109,"g":96,"b":2,"hex":"#6D6002"},{"code":400,"r":209,"g":198,"b":181,"hex":"#D1C6B5"},{"code":401,"r":193,"g":181,"b":165,"hex":"#C1B5A5"},{"code":402,"r":175,"g":165,"b":147,"hex":"#AFA593"},{"code":403,"r":153,"g":140,"b":124,"hex":"#998C7C"},{"code":404,"r":130,"g":117,"b":102,"hex":"#827566"},{"code":405,"r":107,"g":94,"b":79,"hex":"#6B5E4F"},{"code":406,"r":206,"g":193,"b":181,"hex":"#CEC1B5"},{"code":408,"r":168,"g":153,"b":140,"hex":"#A8998C"},{"code":409,"r":153,"g":137,"b":124,"hex":"#99897C"},{"code":410,"r":124,"g":109,"b":99,"hex":"#7C6D63"},{"code":411,"r":102,"g":89,"b":76,"hex":"#66594C"},{"code":412,"r":61,"g":48,"b":40,"hex":"#3D3028"},{"code":413,"r":198,"g":193,"b":178,"hex":"#C6C1B2"},{"code":414,"r":181,"g":175,"b":160,"hex":"#B5AFA0"},{"code":415,"r":163,"g":158,"b":140,"hex":"#A39E8C"},{"code":416,"r":142,"g":140,"b":122,"hex":"#8E8C7A"},{"code":417,"r":119,"g":114,"b":99,"hex":"#777263"},{"code":418,"r":96,"g":94,"b":79,"hex":"#605E4F"},{"code":419,"r":40,"g":40,"b":33,"hex":"#282821"},{"code":420,"r":209,"g":204,"b":191,"hex":"#D1CCBF"},{"code":421,"r":191,"g":186,"b":175,"hex":"#BFBAAF"},{"code":422,"r":175,"g":170,"b":163,"hex":"#AFAAA3"},{"code":423,"r":150,"g":147,"b":142,"hex":"#96938E"},{"code":424,"r":130,"g":127,"b":119,"hex":"#827F77"},{"code":425,"r":96,"g":96,"b":91,"hex":"#60605B"},{"code":426,"r":43,"g":43,"b":40,"hex":"#2B2B28"},{"code":427,"r":221,"g":219,"b":209,"hex":"#DDDBD1"},{"code":428,"r":209,"g":206,"b":198,"hex":"#D1CEC6"},{"code":429,"r":173,"g":175,"b":170,"hex":"#ADAFAA"},{"code":430,"r":145,"g":150,"b":147,"hex":"#919693"},{"code":431,"r":102,"g":109,"b":112,"hex":"#666D70"},{"code":432,"r":68,"g":79,"b":81,"hex":"#444F51"},{"code":433,"r":48,"g":56,"b":58,"hex":"#30383A"},{"code":"433 2X","r":10,"g":12,"b":17,"hex":"#0A0C11"},{"code":434,"r":224,"g":209,"b":198,"hex":"#E0D1C6"},{"code":435,"r":211,"g":191,"b":183,"hex":"#D3BFB7"},{"code":436,"r":188,"g":165,"b":158,"hex":"#BCA59E"},{"code":437,"r":140,"g":112,"b":107,"hex":"#8C706B"},{"code":438,"r":89,"g":63,"b":61,"hex":"#593F3D"},{"code":439,"r":73,"g":53,"b":51,"hex":"#493533"},{"code":440,"r":63,"g":48,"b":43,"hex":"#3F302B"},{"code":441,"r":209,"g":209,"b":198,"hex":"#D1D1C6"},{"code":442,"r":186,"g":191,"b":183,"hex":"#BABFB7"},{"code":443,"r":163,"g":168,"b":163,"hex":"#A3A8A3"},{"code":444,"r":137,"g":142,"b":140,"hex":"#898E8C"},{"code":445,"r":86,"g":89,"b":89,"hex":"#565959"},{"code":446,"r":73,"g":76,"b":73,"hex":"#494C49"},{"code":447,"r":63,"g":63,"b":56,"hex":"#3F3F38"},{"code":448,"r":84,"g":71,"b":45,"hex":"#54472D"},{"code":4485,"r":96,"g":76,"b":17,"hex":"#604C11"},{"code":449,"r":84,"g":71,"b":38,"hex":"#544726"},{"code":4495,"r":135,"g":117,"b":48,"hex":"#877530"},{"code":450,"r":96,"g":84,"b":43,"hex":"#60542B"},{"code":4505,"r":160,"g":145,"b":81,"hex":"#A09151"},{"code":451,"r":173,"g":160,"b":122,"hex":"#ADA07A"},{"code":4515,"r":188,"g":173,"b":117,"hex":"#BCAD75"},{"code":452,"r":196,"g":183,"b":150,"hex":"#C4B796"},{"code":4525,"r":204,"g":191,"b":142,"hex":"#CCBF8E"},{"code":453,"r":214,"g":204,"b":175,"hex":"#D6CCAF"},{"code":4535,"r":219,"g":206,"b":165,"hex":"#DBCEA5"},{"code":454,"r":226,"g":216,"b":191,"hex":"#E2D8BF"},{"code":4545,"r":229,"g":219,"b":186,"hex":"#E5DBBA"},{"code":455,"r":102,"g":86,"b":20,"hex":"#665614"},{"code":456,"r":153,"g":135,"b":20,"hex":"#998714"},{"code":457,"r":181,"g":155,"b":12,"hex":"#B59B0C"},{"code":458,"r":221,"g":204,"b":107,"hex":"#DDCC6B"},{"code":459,"r":226,"g":214,"b":124,"hex":"#E2D67C"},{"code":460,"r":234,"g":221,"b":150,"hex":"#EADD96"},{"code":461,"r":237,"g":229,"b":173,"hex":"#EDE5AD"},{"code":462,"r":91,"g":71,"b":35,"hex":"#5B4723"},{"code":4625,"r":71,"g":35,"b":17,"hex":"#472311"},{"code":463,"r":117,"g":84,"b":38,"hex":"#755426"},{"code":4635,"r":140,"g":89,"b":51,"hex":"#8C5933"},{"code":464,"r":135,"g":96,"b":40,"hex":"#876028"},{"code":"464 2X","r":112,"g":66,"b":20,"hex":"#704214"},{"code":4645,"r":178,"g":130,"b":96,"hex":"#B28260"},{"code":465,"r":193,"g":168,"b":117,"hex":"#C1A875"},{"code":4655,"r":196,"g":153,"b":119,"hex":"#C49977"},{"code":466,"r":209,"g":191,"b":145,"hex":"#D1BF91"},{"code":4665,"r":216,"g":181,"b":150,"hex":"#D8B596"},{"code":467,"r":221,"g":204,"b":165,"hex":"#DDCCA5"},{"code":4675,"r":229,"g":198,"b":170,"hex":"#E5C6AA"},{"code":468,"r":226,"g":214,"b":181,"hex":"#E2D6B5"},{"code":4685,"r":237,"g":211,"b":188,"hex":"#EDD3BC"},{"code":469,"r":96,"g":51,"b":17,"hex":"#603311"},{"code":4695,"r":81,"g":38,"b":28,"hex":"#51261C"},{"code":470,"r":155,"g":79,"b":25,"hex":"#9B4F19"},{"code":4705,"r":124,"g":81,"b":61,"hex":"#7C513D"},{"code":471,"r":188,"g":94,"b":30,"hex":"#BC5E1E"},{"code":"471 2X","r":163,"g":68,"b":2,"hex":"#A34402"},{"code":4715,"r":153,"g":112,"b":91,"hex":"#99705B"},{"code":472,"r":234,"g":170,"b":122,"hex":"#EAAA7A"},{"code":4725,"r":181,"g":145,"b":124,"hex":"#B5917C"},{"code":473,"r":244,"g":196,"b":160,"hex":"#F4C4A0"},{"code":4735,"r":204,"g":175,"b":155,"hex":"#CCAF9B"},{"code":474,"r":244,"g":204,"b":170,"hex":"#F4CCAA"},{"code":4745,"r":216,"g":191,"b":170,"hex":"#D8BFAA"},{"code":475,"r":247,"g":211,"b":181,"hex":"#F7D3B5"},{"code":4755,"r":226,"g":204,"b":186,"hex":"#E2CCBA"},{"code":476,"r":89,"g":61,"b":43,"hex":"#593D2B"},{"code":477,"r":99,"g":56,"b":38,"hex":"#633826"},{"code":478,"r":122,"g":63,"b":40,"hex":"#7A3F28"},{"code":479,"r":175,"g":137,"b":112,"hex":"#AF8970"},{"code":480,"r":211,"g":183,"b":163,"hex":"#D3B7A3"},{"code":481,"r":224,"g":204,"b":186,"hex":"#E0CCBA"},{"code":482,"r":229,"g":211,"b":193,"hex":"#E5D3C1"},{"code":483,"r":107,"g":48,"b":33,"hex":"#6B3021"},{"code":484,"r":155,"g":48,"b":28,"hex":"#9B301C"},{"code":485,"r":216,"g":30,"b":5,"hex":"#D81E05"},{"code":"485 2X","r":204,"g":12,"b":0,"hex":"#CC0C00"},{"code":486,"r":237,"g":158,"b":132,"hex":"#ED9E84"},{"code":487,"r":239,"g":181,"b":160,"hex":"#EFB5A0"},{"code":488,"r":242,"g":196,"b":175,"hex":"#F2C4AF"},{"code":489,"r":242,"g":209,"b":191,"hex":"#F2D1BF"},{"code":490,"r":91,"g":38,"b":38,"hex":"#5B2626"},{"code":491,"r":117,"g":40,"b":40,"hex":"#752828"},{"code":492,"r":145,"g":51,"b":56,"hex":"#913338"},{"code":494,"r":242,"g":173,"b":178,"hex":"#F2ADB2"},{"code":495,"r":244,"g":188,"b":191,"hex":"#F4BCBF"},{"code":496,"r":247,"g":201,"b":198,"hex":"#F7C9C6"},{"code":497,"r":81,"g":40,"b":38,"hex":"#512826"},{"code":4975,"r":68,"g":30,"b":28,"hex":"#441E1C"},{"code":498,"r":109,"g":51,"b":43,"hex":"#6D332B"},{"code":4985,"r":132,"g":73,"b":73,"hex":"#844949"},{"code":499,"r":122,"g":56,"b":45,"hex":"#7A382D"},{"code":4995,"r":165,"g":107,"b":109,"hex":"#A56B6D"},{"code":500,"r":206,"g":137,"b":140,"hex":"#CE898C"},{"code":5005,"r":188,"g":135,"b":135,"hex":"#BC8787"},{"code":501,"r":234,"g":178,"b":178,"hex":"#EAB2B2"},{"code":5015,"r":216,"g":173,"b":168,"hex":"#D8ADA8"},{"code":502,"r":242,"g":198,"b":196,"hex":"#F2C6C4"},{"code":5025,"r":226,"g":188,"b":183,"hex":"#E2BCB7"},{"code":503,"r":244,"g":209,"b":204,"hex":"#F4D1CC"},{"code":5035,"r":237,"g":206,"b":198,"hex":"#EDCEC6"},{"code":504,"r":81,"g":30,"b":38,"hex":"#511E26"},{"code":505,"r":102,"g":30,"b":43,"hex":"#661E2B"},{"code":506,"r":122,"g":38,"b":56,"hex":"#7A2638"},{"code":507,"r":216,"g":137,"b":155,"hex":"#D8899B"},{"code":508,"r":232,"g":165,"b":175,"hex":"#E8A5AF"},{"code":509,"r":242,"g":186,"b":191,"hex":"#F2BABF"},{"code":510,"r":244,"g":198,"b":201,"hex":"#F4C6C9"},{"code":511,"r":96,"g":33,"b":68,"hex":"#602144"},{"code":5115,"r":79,"g":33,"b":58,"hex":"#4F213A"},{"code":512,"r":132,"g":33,"b":107,"hex":"#84216B"},{"code":5125,"r":117,"g":71,"b":96,"hex":"#754760"},{"code":513,"r":158,"g":35,"b":135,"hex":"#9E2387"},{"code":5135,"r":147,"g":107,"b":127,"hex":"#936B7F"},{"code":514,"r":216,"g":132,"b":188,"hex":"#D884BC"},{"code":5145,"r":173,"g":135,"b":153,"hex":"#AD8799"},{"code":515,"r":232,"g":163,"b":201,"hex":"#E8A3C9"},{"code":5155,"r":204,"g":175,"b":183,"hex":"#CCAFB7"},{"code":516,"r":242,"g":186,"b":211,"hex":"#F2BAD3"},{"code":5165,"r":224,"g":201,"b":204,"hex":"#E0C9CC"},{"code":517,"r":244,"g":204,"b":216,"hex":"#F4CCD8"},{"code":5175,"r":232,"g":214,"b":209,"hex":"#E8D6D1"},{"code":518,"r":81,"g":45,"b":68,"hex":"#512D44"},{"code":5185,"r":71,"g":40,"b":53,"hex":"#472835"},{"code":519,"r":99,"g":48,"b":94,"hex":"#63305E"},{"code":5195,"r":89,"g":51,"b":68,"hex":"#593344"},{"code":520,"r":112,"g":53,"b":114,"hex":"#703572"},{"code":5205,"r":142,"g":104,"b":119,"hex":"#8E6877"},{"code":521,"r":181,"g":140,"b":178,"hex":"#B58CB2"},{"code":5215,"r":181,"g":147,"b":155,"hex":"#B5939B"},{"code":522,"r":198,"g":163,"b":193,"hex":"#C6A3C1"},{"code":5225,"r":204,"g":173,"b":175,"hex":"#CCADAF"},{"code":523,"r":211,"g":183,"b":204,"hex":"#D3B7CC"},{"code":5235,"r":221,"g":198,"b":196,"hex":"#DDC6C4"},{"code":524,"r":226,"g":204,"b":211,"hex":"#E2CCD3"},{"code":5245,"r":229,"g":211,"b":204,"hex":"#E5D3CC"},{"code":525,"r":81,"g":38,"b":84,"hex":"#512654"},{"code":5255,"r":53,"g":38,"b":79,"hex":"#35264F"},{"code":526,"r":104,"g":33,"b":122,"hex":"#68217A"},{"code":5265,"r":73,"g":61,"b":99,"hex":"#493D63"},{"code":527,"r":122,"g":30,"b":153,"hex":"#7A1E99"},{"code":5275,"r":96,"g":86,"b":119,"hex":"#605677"},{"code":528,"r":175,"g":114,"b":193,"hex":"#AF72C1"},{"code":5285,"r":140,"g":130,"b":153,"hex":"#8C8299"},{"code":529,"r":206,"g":163,"b":211,"hex":"#CEA3D3"},{"code":5295,"r":178,"g":168,"b":181,"hex":"#B2A8B5"},{"code":530,"r":214,"g":175,"b":214,"hex":"#D6AFD6"},{"code":5305,"r":204,"g":193,"b":198,"hex":"#CCC1C6"},{"code":531,"r":229,"g":198,"b":219,"hex":"#E5C6DB"},{"code":5315,"r":219,"g":211,"b":211,"hex":"#DBD3D3"},{"code":532,"r":53,"g":56,"b":66,"hex":"#353842"},{"code":533,"r":53,"g":63,"b":91,"hex":"#353F5B"},{"code":534,"r":58,"g":73,"b":114,"hex":"#3A4972"},{"code":535,"r":155,"g":163,"b":183,"hex":"#9BA3B7"},{"code":536,"r":173,"g":178,"b":193,"hex":"#ADB2C1"},{"code":537,"r":196,"g":198,"b":206,"hex":"#C4C6CE"},{"code":538,"r":214,"g":211,"b":214,"hex":"#D6D3D6"},{"code":539,"r":0,"g":48,"b":73,"hex":"#003049"},{"code":5395,"r":2,"g":40,"b":58,"hex":"#02283A"},{"code":540,"r":0,"g":51,"b":91,"hex":"#00335B"},{"code":5405,"r":63,"g":96,"b":117,"hex":"#3F6075"},{"code":541,"r":0,"g":63,"b":119,"hex":"#003F77"},{"code":5415,"r":96,"g":124,"b":140,"hex":"#607C8C"},{"code":542,"r":102,"g":147,"b":188,"hex":"#6693BC"},{"code":5425,"r":132,"g":153,"b":165,"hex":"#8499A5"},{"code":543,"r":147,"g":183,"b":209,"hex":"#93B7D1"},{"code":5435,"r":175,"g":188,"b":191,"hex":"#AFBCBF"},{"code":544,"r":183,"g":204,"b":219,"hex":"#B7CCDB"},{"code":5445,"r":196,"g":204,"b":204,"hex":"#C4CCCC"},{"code":545,"r":196,"g":211,"b":221,"hex":"#C4D3DD"},{"code":5455,"r":214,"g":216,"b":211,"hex":"#D6D8D3"},{"code":546,"r":12,"g":56,"b":68,"hex":"#0C3844"},{"code":5463,"r":0,"g":53,"b":58,"hex":"#00353A"},{"code":5467,"r":25,"g":56,"b":51,"hex":"#193833"},{"code":547,"r":0,"g":63,"b":84,"hex":"#003F54"},{"code":5473,"r":38,"g":104,"b":109,"hex":"#26686D"},{"code":5477,"r":58,"g":86,"b":79,"hex":"#3A564F"},{"code":548,"r":0,"g":68,"b":89,"hex":"#004459"},{"code":5483,"r":96,"g":145,"b":145,"hex":"#609191"},{"code":5487,"r":102,"g":124,"b":114,"hex":"#667C72"},{"code":549,"r":94,"g":153,"b":170,"hex":"#5E99AA"},{"code":5493,"r":140,"g":175,"b":173,"hex":"#8CAFAD"},{"code":5497,"r":145,"g":163,"b":153,"hex":"#91A399"},{"code":550,"r":135,"g":175,"b":191,"hex":"#87AFBF"},{"code":5503,"r":170,"g":196,"b":191,"hex":"#AAC4BF"},{"code":5507,"r":175,"g":186,"b":178,"hex":"#AFBAB2"},{"code":551,"r":163,"g":193,"b":201,"hex":"#A3C1C9"},{"code":5513,"r":206,"g":216,"b":209,"hex":"#CED8D1"},{"code":5517,"r":201,"g":206,"b":196,"hex":"#C9CEC4"},{"code":552,"r":196,"g":214,"b":214,"hex":"#C4D6D6"},{"code":5523,"r":214,"g":221,"b":214,"hex":"#D6DDD6"},{"code":5527,"r":206,"g":209,"b":198,"hex":"#CED1C6"},{"code":553,"r":35,"g":68,"b":53,"hex":"#234435"},{"code":5535,"r":33,"g":61,"b":48,"hex":"#213D30"},{"code":554,"r":25,"g":94,"b":71,"hex":"#195E47"},{"code":5545,"r":79,"g":109,"b":94,"hex":"#4F6D5E"},{"code":555,"r":7,"g":109,"b":84,"hex":"#076D54"},{"code":5555,"r":119,"g":145,"b":130,"hex":"#779182"},{"code":556,"r":122,"g":168,"b":145,"hex":"#7AA891"},{"code":5565,"r":150,"g":170,"b":153,"hex":"#96AA99"},{"code":557,"r":163,"g":193,"b":173,"hex":"#A3C1AD"},{"code":5575,"r":175,"g":191,"b":173,"hex":"#AFBFAD"},{"code":558,"r":183,"g":206,"b":188,"hex":"#B7CEBC"},{"code":5585,"r":196,"g":206,"b":191,"hex":"#C4CEBF"},{"code":559,"r":198,"g":214,"b":196,"hex":"#C6D6C4"},{"code":5595,"r":216,"g":219,"b":204,"hex":"#D8DBCC"},{"code":560,"r":43,"g":76,"b":63,"hex":"#2B4C3F"},{"code":5605,"r":35,"g":58,"b":45,"hex":"#233A2D"},{"code":561,"r":38,"g":102,"b":89,"hex":"#266659"},{"code":5615,"r":84,"g":104,"b":86,"hex":"#546856"},{"code":562,"r":30,"g":122,"b":109,"hex":"#1E7A6D"},{"code":5625,"r":114,"g":132,"b":112,"hex":"#728470"},{"code":563,"r":127,"g":188,"b":170,"hex":"#7FBCAA"},{"code":5635,"r":158,"g":170,"b":153,"hex":"#9EAA99"},{"code":564,"r":5,"g":112,"b":94,"hex":"#05705E"},{"code":5645,"r":188,"g":193,"b":178,"hex":"#BCC1B2"},{"code":565,"r":188,"g":219,"b":204,"hex":"#BCDBCC"},{"code":5655,"r":198,"g":204,"b":186,"hex":"#C6CCBA"},{"code":566,"r":209,"g":226,"b":211,"hex":"#D1E2D3"},{"code":5665,"r":214,"g":214,"b":198,"hex":"#D6D6C6"},{"code":567,"r":38,"g":81,"b":66,"hex":"#265142"},{"code":568,"r":0,"g":114,"b":99,"hex":"#007263"},{"code":569,"r":0,"g":135,"b":114,"hex":"#008772"},{"code":570,"r":127,"g":198,"b":178,"hex":"#7FC6B2"},{"code":571,"r":170,"g":219,"b":198,"hex":"#AADBC6"},{"code":572,"r":188,"g":226,"b":206,"hex":"#BCE2CE"},{"code":573,"r":204,"g":229,"b":214,"hex":"#CCE5D6"},{"code":574,"r":73,"g":89,"b":40,"hex":"#495928"},{"code":5743,"r":63,"g":73,"b":38,"hex":"#3F4926"},{"code":5747,"r":66,"g":71,"b":22,"hex":"#424716"},{"code":575,"r":84,"g":119,"b":48,"hex":"#547730"},{"code":5753,"r":94,"g":102,"b":58,"hex":"#5E663A"},{"code":5757,"r":107,"g":112,"b":43,"hex":"#6B702B"},{"code":576,"r":96,"g":142,"b":58,"hex":"#608E3A"},{"code":5763,"r":119,"g":124,"b":79,"hex":"#777C4F"},{"code":5767,"r":140,"g":145,"b":79,"hex":"#8C914F"},{"code":577,"r":181,"g":204,"b":142,"hex":"#B5CC8E"},{"code":5773,"r":155,"g":158,"b":114,"hex":"#9B9E72"},{"code":5777,"r":170,"g":173,"b":117,"hex":"#AAAD75"},{"code":578,"r":198,"g":214,"b":160,"hex":"#C6D6A0"},{"code":5783,"r":181,"g":181,"b":142,"hex":"#B5B58E"},{"code":5787,"r":198,"g":198,"b":153,"hex":"#C6C699"},{"code":579,"r":201,"g":214,"b":163,"hex":"#C9D6A3"},{"code":5793,"r":198,"g":198,"b":165,"hex":"#C6C6A5"},{"code":5797,"r":211,"g":209,"b":170,"hex":"#D3D1AA"},{"code":580,"r":216,"g":221,"b":181,"hex":"#D8DDB5"},{"code":5803,"r":216,"g":214,"b":183,"hex":"#D8D6B7"},{"code":5807,"r":224,"g":221,"b":188,"hex":"#E0DDBC"},{"code":581,"r":96,"g":94,"b":17,"hex":"#605E11"},{"code":5815,"r":73,"g":68,"b":17,"hex":"#494411"},{"code":582,"r":135,"g":137,"b":5,"hex":"#878905"},{"code":5825,"r":117,"g":112,"b":43,"hex":"#75702B"},{"code":583,"r":170,"g":186,"b":10,"hex":"#AABA0A"},{"code":5835,"r":158,"g":153,"b":89,"hex":"#9E9959"},{"code":584,"r":206,"g":214,"b":73,"hex":"#CED649"},{"code":5845,"r":178,"g":170,"b":112,"hex":"#B2AA70"},{"code":585,"r":219,"g":224,"b":107,"hex":"#DBE06B"},{"code":5855,"r":204,"g":198,"b":147,"hex":"#CCC693"},{"code":586,"r":226,"g":229,"b":132,"hex":"#E2E584"},{"code":5865,"r":214,"g":206,"b":163,"hex":"#D6CEA3"},{"code":587,"r":232,"g":232,"b":155,"hex":"#E8E89B"},{"code":5875,"r":224,"g":219,"b":181,"hex":"#E0DBB5"},{"code":600,"r":244,"g":237,"b":175,"hex":"#F4EDAF"},{"code":601,"r":242,"g":237,"b":158,"hex":"#F2ED9E"},{"code":602,"r":242,"g":234,"b":135,"hex":"#F2EA87"},{"code":603,"r":237,"g":232,"b":91,"hex":"#EDE85B"},{"code":604,"r":232,"g":221,"b":33,"hex":"#E8DD21"},{"code":605,"r":221,"g":206,"b":17,"hex":"#DDCE11"},{"code":606,"r":211,"g":191,"b":17,"hex":"#D3BF11"},{"code":607,"r":242,"g":234,"b":188,"hex":"#F2EABC"},{"code":608,"r":239,"g":232,"b":173,"hex":"#EFE8AD"},{"code":609,"r":234,"g":229,"b":150,"hex":"#EAE596"},{"code":610,"r":226,"g":219,"b":114,"hex":"#E2DB72"},{"code":611,"r":214,"g":206,"b":73,"hex":"#D6CE49"},{"code":612,"r":196,"g":186,"b":0,"hex":"#C4BA00"},{"code":613,"r":175,"g":160,"b":12,"hex":"#AFA00C"},{"code":614,"r":234,"g":226,"b":183,"hex":"#EAE2B7"},{"code":615,"r":226,"g":219,"b":170,"hex":"#E2DBAA"},{"code":616,"r":221,"g":214,"b":155,"hex":"#DDD69B"},{"code":617,"r":204,"g":196,"b":124,"hex":"#CCC47C"},{"code":618,"r":181,"g":170,"b":89,"hex":"#B5AA59"},{"code":619,"r":150,"g":140,"b":40,"hex":"#968C28"},{"code":620,"r":132,"g":119,"b":17,"hex":"#847711"},{"code":621,"r":216,"g":221,"b":206,"hex":"#D8DDCE"},{"code":622,"r":193,"g":209,"b":191,"hex":"#C1D1BF"},{"code":623,"r":165,"g":191,"b":170,"hex":"#A5BFAA"},{"code":624,"r":127,"g":160,"b":140,"hex":"#7FA08C"},{"code":625,"r":91,"g":135,"b":114,"hex":"#5B8772"},{"code":626,"r":33,"g":84,"b":63,"hex":"#21543F"},{"code":627,"r":12,"g":48,"b":38,"hex":"#0C3026"},{"code":628,"r":204,"g":226,"b":221,"hex":"#CCE2DD"},{"code":629,"r":178,"g":216,"b":216,"hex":"#B2D8D8"},{"code":630,"r":140,"g":204,"b":211,"hex":"#8CCCD3"},{"code":631,"r":84,"g":183,"b":198,"hex":"#54B7C6"},{"code":632,"r":0,"g":160,"b":186,"hex":"#00A0BA"},{"code":633,"r":0,"g":127,"b":153,"hex":"#007F99"},{"code":634,"r":0,"g":102,"b":127,"hex":"#00667F"},{"code":635,"r":186,"g":224,"b":224,"hex":"#BAE0E0"},{"code":636,"r":153,"g":214,"b":221,"hex":"#99D6DD"},{"code":637,"r":107,"g":201,"b":219,"hex":"#6BC9DB"},{"code":638,"r":0,"g":181,"b":214,"hex":"#00B5D6"},{"code":639,"r":0,"g":160,"b":196,"hex":"#00A0C4"},{"code":640,"r":0,"g":140,"b":178,"hex":"#008CB2"},{"code":641,"r":0,"g":122,"b":165,"hex":"#007AA5"},{"code":642,"r":209,"g":216,"b":216,"hex":"#D1D8D8"},{"code":643,"r":198,"g":209,"b":214,"hex":"#C6D1D6"},{"code":644,"r":155,"g":175,"b":196,"hex":"#9BAFC4"},{"code":645,"r":119,"g":150,"b":178,"hex":"#7796B2"},{"code":646,"r":94,"g":130,"b":163,"hex":"#5E82A3"},{"code":647,"r":38,"g":84,"b":124,"hex":"#26547C"},{"code":648,"r":0,"g":48,"b":94,"hex":"#00305E"},{"code":649,"r":214,"g":214,"b":216,"hex":"#D6D6D8"},{"code":650,"r":191,"g":198,"b":209,"hex":"#BFC6D1"},{"code":651,"r":155,"g":170,"b":191,"hex":"#9BAABF"},{"code":652,"r":109,"g":135,"b":168,"hex":"#6D87A8"},{"code":653,"r":51,"g":86,"b":135,"hex":"#335687"},{"code":654,"r":15,"g":43,"b":91,"hex":"#0F2B5B"},{"code":655,"r":12,"g":28,"b":71,"hex":"#0C1C47"},{"code":656,"r":214,"g":219,"b":224,"hex":"#D6DBE0"},{"code":657,"r":193,"g":201,"b":221,"hex":"#C1C9DD"},{"code":658,"r":165,"g":175,"b":214,"hex":"#A5AFD6"},{"code":659,"r":127,"g":140,"b":191,"hex":"#7F8CBF"},{"code":660,"r":89,"g":96,"b":168,"hex":"#5960A8"},{"code":661,"r":45,"g":51,"b":142,"hex":"#2D338E"},{"code":662,"r":12,"g":25,"b":117,"hex":"#0C1975"},{"code":663,"r":226,"g":211,"b":214,"hex":"#E2D3D6"},{"code":664,"r":216,"g":204,"b":209,"hex":"#D8CCD1"},{"code":665,"r":198,"g":181,"b":196,"hex":"#C6B5C4"},{"code":666,"r":168,"g":147,"b":173,"hex":"#A893AD"},{"code":667,"r":127,"g":102,"b":137,"hex":"#7F6689"},{"code":668,"r":102,"g":73,"b":117,"hex":"#664975"},{"code":669,"r":71,"g":43,"b":89,"hex":"#472B59"},{"code":670,"r":242,"g":214,"b":216,"hex":"#F2D6D8"},{"code":671,"r":239,"g":198,"b":211,"hex":"#EFC6D3"},{"code":672,"r":234,"g":170,"b":196,"hex":"#EAAAC4"},{"code":673,"r":224,"g":140,"b":178,"hex":"#E08CB2"},{"code":674,"r":211,"g":107,"b":158,"hex":"#D36B9E"},{"code":675,"r":188,"g":56,"b":119,"hex":"#BC3877"},{"code":676,"r":160,"g":0,"b":84,"hex":"#A00054"},{"code":677,"r":237,"g":214,"b":214,"hex":"#EDD6D6"},{"code":678,"r":234,"g":204,"b":206,"hex":"#EACCCE"},{"code":679,"r":229,"g":191,"b":198,"hex":"#E5BFC6"},{"code":680,"r":211,"g":158,"b":175,"hex":"#D39EAF"},{"code":681,"r":183,"g":114,"b":142,"hex":"#B7728E"},{"code":682,"r":160,"g":81,"b":117,"hex":"#A05175"},{"code":683,"r":127,"g":40,"b":79,"hex":"#7F284F"},{"code":684,"r":239,"g":204,"b":206,"hex":"#EFCCCE"},{"code":685,"r":234,"g":191,"b":196,"hex":"#EABFC4"},{"code":686,"r":224,"g":170,"b":186,"hex":"#E0AABA"},{"code":687,"r":201,"g":137,"b":158,"hex":"#C9899E"},{"code":688,"r":178,"g":102,"b":132,"hex":"#B26684"},{"code":689,"r":147,"g":66,"b":102,"hex":"#934266"},{"code":690,"r":112,"g":35,"b":66,"hex":"#702342"},{"code":691,"r":239,"g":209,"b":201,"hex":"#EFD1C9"},{"code":692,"r":232,"g":191,"b":186,"hex":"#E8BFBA"},{"code":693,"r":219,"g":168,"b":165,"hex":"#DBA8A5"},{"code":694,"r":201,"g":140,"b":140,"hex":"#C98C8C"},{"code":695,"r":178,"g":107,"b":112,"hex":"#B26B70"},{"code":696,"r":142,"g":71,"b":73,"hex":"#8E4749"},{"code":697,"r":127,"g":56,"b":58,"hex":"#7F383A"},{"code":698,"r":247,"g":209,"b":204,"hex":"#F7D1CC"},{"code":699,"r":247,"g":191,"b":191,"hex":"#F7BFBF"},{"code":700,"r":242,"g":165,"b":170,"hex":"#F2A5AA"},{"code":701,"r":232,"g":135,"b":142,"hex":"#E8878E"},{"code":702,"r":214,"g":96,"b":109,"hex":"#D6606D"},{"code":703,"r":183,"g":56,"b":68,"hex":"#B73844"},{"code":704,"r":158,"g":40,"b":40,"hex":"#9E2828"},{"code":705,"r":249,"g":221,"b":214,"hex":"#F9DDD6"},{"code":706,"r":252,"g":201,"b":198,"hex":"#FCC9C6"},{"code":707,"r":252,"g":173,"b":175,"hex":"#FCADAF"},{"code":708,"r":249,"g":142,"b":153,"hex":"#F98E99"},{"code":709,"r":242,"g":104,"b":119,"hex":"#F26877"},{"code":710,"r":224,"g":66,"b":81,"hex":"#E04251"},{"code":711,"r":209,"g":45,"b":51,"hex":"#D12D33"},{"code":712,"r":255,"g":211,"b":170,"hex":"#FFD3AA"},{"code":713,"r":249,"g":201,"b":163,"hex":"#F9C9A3"},{"code":714,"r":249,"g":186,"b":130,"hex":"#F9BA82"},{"code":715,"r":252,"g":158,"b":73,"hex":"#FC9E49"},{"code":716,"r":242,"g":132,"b":17,"hex":"#F28411"},{"code":717,"r":211,"g":109,"b":0,"hex":"#D36D00"},{"code":718,"r":191,"g":91,"b":0,"hex":"#BF5B00"},{"code":719,"r":244,"g":209,"b":175,"hex":"#F4D1AF"},{"code":720,"r":239,"g":196,"b":158,"hex":"#EFC49E"},{"code":721,"r":232,"g":178,"b":130,"hex":"#E8B282"},{"code":722,"r":209,"g":142,"b":84,"hex":"#D18E54"},{"code":723,"r":186,"g":117,"b":48,"hex":"#BA7530"},{"code":724,"r":142,"g":73,"b":5,"hex":"#8E4905"},{"code":725,"r":117,"g":56,"b":2,"hex":"#753802"},{"code":726,"r":237,"g":211,"b":181,"hex":"#EDD3B5"},{"code":727,"r":226,"g":191,"b":155,"hex":"#E2BF9B"},{"code":728,"r":211,"g":168,"b":124,"hex":"#D3A87C"},{"code":729,"r":193,"g":142,"b":96,"hex":"#C18E60"},{"code":730,"r":170,"g":117,"b":63,"hex":"#AA753F"},{"code":731,"r":114,"g":63,"b":10,"hex":"#723F0A"},{"code":732,"r":96,"g":51,"b":10,"hex":"#60330A"},{"code":801,"r":0,"g":170,"b":204,"hex":"#00AACC"},{"code":"801 2X","r":0,"g":137,"b":175,"hex":"#0089AF"},{"code":802,"r":96,"g":221,"b":73,"hex":"#60DD49"},{"code":"802 2X","r":28,"g":206,"b":40,"hex":"#1CCE28"},{"code":803,"r":255,"g":237,"b":56,"hex":"#FFED38"},{"code":"803 2X","r":255,"g":216,"b":22,"hex":"#FFD816"},{"code":804,"r":255,"g":147,"b":56,"hex":"#FF9338"},{"code":"804 2X","r":255,"g":127,"b":30,"hex":"#FF7F1E"},{"code":805,"r":249,"g":89,"b":81,"hex":"#F95951"},{"code":"805 2X","r":249,"g":58,"b":43,"hex":"#F93A2B"},{"code":806,"r":255,"g":0,"b":147,"hex":"#FF0093"},{"code":"806 2X","r":247,"g":2,"b":124,"hex":"#F7027C"},{"code":807,"r":214,"g":0,"b":158,"hex":"#D6009E"},{"code":"807 2X","r":191,"g":0,"b":140,"hex":"#BF008C"},{"code":808,"r":0,"g":181,"b":155,"hex":"#00B59B"},{"code":"808 2X","r":0,"g":160,"b":135,"hex":"#00A087"},{"code":809,"r":221,"g":224,"b":15,"hex":"#DDE00F"},{"code":"809 2X","r":214,"g":214,"b":12,"hex":"#D6D60C"},{"code":810,"r":255,"g":204,"b":30,"hex":"#FFCC1E"},{"code":"810 2X","r":255,"g":188,"b":33,"hex":"#FFBC21"},{"code":811,"r":255,"g":114,"b":71,"hex":"#FF7247"},{"code":"811 2X","r":255,"g":84,"b":22,"hex":"#FF5416"},{"code":812,"r":252,"g":35,"b":102,"hex":"#FC2366"},{"code":"812 2X","r":252,"g":7,"b":79,"hex":"#FC074F"},{"code":813,"r":229,"g":0,"b":153,"hex":"#E50099"},{"code":"813 2X","r":209,"g":0,"b":132,"hex":"#D10084"},{"code":814,"r":140,"g":96,"b":193,"hex":"#8C60C1"},{"code":"814 2X","r":112,"g":63,"b":175,"hex":"#703FAF"}];
        for (var i = 0; i < pantoneSwatches.length; i++) {
            pantoneSwatches[i].code = "Pantone " + pantoneSwatches[i].code;
            swatches.push(pantoneSwatches[i]);
        }
    }


    // Search
    var searchElem = $("<input class='ui-input' type='text'/>");
    searchElem.attr("placeholder", "{text.search}");

    var elemColors = $("<div class='colors'></div>");
    for (var i = 0; i < swatches.length; i++) {
        var color = swatches[i];
        var rgb = Machinata.Math.hexToRBG(color.hex);
        var brighness = Machinata.Math.rbgBrightness(rgb);
        var colorElem = $("<div class='color'></div>");
        colorElem.css("background-color", color.hex);
        colorElem.data("code", color.code);
        colorElem.data("code-searchable", color.code.toLowerCase().replaceAll(" ",""));
        colorElem.data("hex", color.hex);
        colorElem.data("color", color);
        var labelElem = $("<div class='label'></div>");
        if (brighness < 0.5) {
            labelElem.css("color", "white");
        } else {
            labelElem.css("color", "black");
        }
        labelElem.text(color.code);
        labelElem.appendTo(colorElem);
        colorElem.appendTo(elemColors);
    }

    var elem = $("<div class='bb-color-swatch ui-color-swatch'></div>");
    elem.append(searchElem);
    elem.append(elemColors);

    var allColorsElems = elemColors.find(".color");
    searchElem.on("change keyup", function () {
        var val = searchElem.val().toLowerCase().replaceAll(" ", "");
        var numSelected = 0;
        var firstSelected = null;
        allColorsElems.each(function () {
            var code = $(this).data("code-searchable");
            if (code.indexOf(val) != -1) {
                $(this).show();
                numSelected++;
                firstSelected = $(this);
            } else {
                $(this).hide();
            }
        });
        if (numSelected == 1) {
            firstSelected.trigger("click");
        }
    });
    allColorsElems.click(function () {
        allColorsElems.removeClass("selected");
        var colorElem = $(this);
        colorElem.addClass("selected");
        elem.attr("data-selected-code", colorElem.data("code"));
        elem.attr("data-selected-hex", colorElem.data("hex"));
        if (opts.onSelect) opts.onSelect({code:colorElem.data("code"), hex: colorElem.data("hex"), elem:colorElem});
    });

    return elem;
};


/// <summary>
/// 
/// </summary>
Machinata.UI.createColorPicker = function (pickerType, opts) {

    // Init
    if (opts == null) opts = {};
    if (opts.dropletSize == null) opts.dropletSize = 20;
    if (opts.swatchSize == null) opts.swatchSize = 300;
    if (opts.swatch == null) opts.swatch = "hsv";


    var currentColorRGB = [255, 255, 255];
    var dropletCoord = [opts.swatchSize/2, opts.swatchSize/2];

    // For HSL wheel, see https://medium.com/@bantic/hand-coding-a-color-wheel-with-canvas-78256c9d7d43
    var elem = $("<div class='bb-color-picker'></div>");
    elem.addClass(pickerType);

    // Containers
    var elemSwatch = $("<div class='swatch'></div>");
    elemSwatch.css("position", "relative");
    elem.append(elemSwatch);

    var elemSliders = $("<div class='sliders'></div>");
    elemSliders.css("position", "relative");
    elem.append(elemSliders);



    // Swatch droplet elem
    var elemSwatchDroplet = $("<div class='droplet'></div>");
    elemSwatchDroplet.css("position", "absolute");
    elemSwatchDroplet.css("width", opts.dropletSize+"px");
    elemSwatchDroplet.css("height", opts.dropletSize+"px");
    elemSwatchDroplet.css("border-radius", "50%");
    elemSwatchDroplet.css("border", "1px solid black");
    elemSwatchDroplet.css("pointer-events", "none");
    elemSwatch.append(elemSwatchDroplet);

    var colorWheelCanvasElem = $("<canvas class='select-on-click select-on-drag'></canvas>");
    colorWheelCanvasElem.attr("data-canvas-type", "hsv-color-wheel");
    colorWheelCanvasElem.attr("width", opts.swatchSize);
    colorWheelCanvasElem.attr("height", opts.swatchSize);
    colorWheelCanvasElem.css("border-radius", "50%");
    colorWheelCanvasElem.css("border", "1px solid black");
    elemSwatch.append(colorWheelCanvasElem);

    var sliderHSVValueCanvasElem = null;
    {
        sliderHSVValueCanvasElem = $("<canvas class='horizontal-slider'></canvas>");
        sliderHSVValueCanvasElem.attr("data-canvas-type", "slider");
        sliderHSVValueCanvasElem.attr("data-slider-type", "hsv-value");
        sliderHSVValueCanvasElem.data("value", 1.0);
        sliderHSVValueCanvasElem.attr("width", opts.swatchSize);
        sliderHSVValueCanvasElem.attr("height", 30);
        sliderHSVValueCanvasElem.css("border", "1px solid black");
        elemSliders.append(sliderHSVValueCanvasElem);
        elemSliders.append("<div class='clear'></div>");
    }

    var sliderRGBRCanvasElem = null;
    if(false){
        sliderRGBRCanvasElem = $("<canvas class='horizontal-slider'></canvas>");
        sliderRGBRCanvasElem.attr("data-canvas-type", "slider");
        sliderRGBRCanvasElem.attr("data-slider-type", "rgb-r");
        sliderRGBRCanvasElem.attr("width", opts.swatchSize);
        sliderRGBRCanvasElem.attr("height", 30 / 2);
        sliderRGBRCanvasElem.css("border", "1px solid black");
        elemSliders.append(sliderRGBRCanvasElem);
        elemSliders.append("<div class='clear'></div>");
    }
    var sliderRGBGCanvasElem = null;
    if (false) {
        sliderRGBGCanvasElem = $("<canvas class='horizontal-slider'></canvas>");
        sliderRGBGCanvasElem.attr("data-canvas-type", "slider");
        sliderRGBGCanvasElem.attr("data-slider-type", "rgb-g");
        sliderRGBGCanvasElem.attr("width", opts.swatchSize);
        sliderRGBGCanvasElem.attr("height", 30 / 2);
        sliderRGBGCanvasElem.css("border", "1px solid black");
        elemSliders.append(sliderRGBGCanvasElem);
        elemSliders.append("<div class='clear'></div>");
    }
    var sliderRGBBCanvasElem = null;
    if (false) {
        sliderRGBBCanvasElem = $("<canvas class='horizontal-slider'></canvas>");
        sliderRGBBCanvasElem.attr("data-canvas-type", "slider");
        sliderRGBBCanvasElem.attr("data-slider-type", "rgb-b");
        sliderRGBBCanvasElem.attr("width", opts.swatchSize);
        sliderRGBBCanvasElem.attr("height", 30 / 2);
        sliderRGBBCanvasElem.css("border", "1px solid black");
        elemSliders.append(sliderRGBBCanvasElem);
        elemSliders.append("<div class='clear'></div>");
    }


    var colorCanvasElem = $("<canvas></canvas>");
    colorCanvasElem.attr("data-canvas-type", "color");
    colorCanvasElem.attr("width", opts.swatchSize);
    colorCanvasElem.attr("height", 80);
    colorCanvasElem.css("border", "1px solid black");
    elem.append(colorCanvasElem);

  

    function updateCanvases() {
        

        elem.find("canvas").each(function () {
            var canvasElem = $(this);
            var canvas = canvasElem[0];
            var ctx = canvas.getContext('2d');
            var ww = canvas.width;
            var hh = canvas.height;
            if (canvasElem.attr("data-canvas-type") == "color") {

                // Selected Color
                ctx.fillStyle = "rgb(" + currentColorRGB[0] + "," + currentColorRGB[1] + "," + currentColorRGB[2] + ")";
                ctx.fillRect(0, 0, ww, hh);

            } else if (canvasElem.attr("data-canvas-type") == "slider") {

                // Draw bar
                var val = canvasElem.data("value");
                var closestRGB = null;
                var closestDist = null;
                for (var x = 0; x < ww; x++) {
                    var px = x / ww;
                    var dist = val - px;
                    var rgb = [0, 0, 0];
                    if (canvasElem.attr("data-slider-type") == "hsv-value") {
                        rgb = [Math.round(currentColorRGB[0] * px), Math.round(currentColorRGB[1] * px), Math.round(currentColorRGB[2] * px)];
                    } else if (canvasElem.attr("data-slider-type") == "rgb-r") {
                        rgb = [Math.round(currentColorRGB[0] * px), 0, 0];
                    } else if (canvasElem.attr("data-slider-type") == "rgb-g") {
                        rgb = [0,Math.round(currentColorRGB[1] * px), 0];
                    } else if (canvasElem.attr("data-slider-type") == "rgb-b") {
                        rgb = [0,0,Math.round(currentColorRGB[2] * px)];
                    }
                    if (closestDist == null || dist < closestDist) {
                        closestRGB = rgb;
                        closestDist = dist;
                    }
                    ctx.fillStyle = "rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")";
                    ctx.fillRect(x, 0, 1, hh);
                }
                // Draw slider
                var closestRGBInverted = Machinata.Math.rbgInvert(closestRGB);
                var closestRGBGrayscale = Machinata.Math.rbgGrayscale(closestRGBInverted);
                ctx.fillStyle = "rgb(" + (closestRGBGrayscale[0]) + "," + (closestRGBGrayscale[1]) + "," + (closestRGBGrayscale[2]) + ")";
                ctx.fillRect(Math.round(val*ww)-8/2, 0, 8, hh);

            } else if (canvasElem.attr("data-canvas-type") == "hsv-color-wheel") {

                // HSV Color Wheel

                // Draw a flat RBG color grid
                var radius = Math.min(ww/2,hh/2);
                var image = ctx.createImageData(2 * radius, 2 * radius);
                var data = image.data;
                var hsvValue = sliderHSVValueCanvasElem.data("value");
                if (hsvValue == null) hsvValue = 1.0;

                for (var x = -radius; x < radius; x++) {
                    for (var y = -radius; y < radius; y++) {
        
                        //let [r, phi] = xy2polar(x, y);
                        var p = Machinata.Math.xy2Polar(x, y);
                        var r = p[0];
                        var phi = p[1];
        
                        if (r > radius+1) {
                            // skip all (x,y) coordinates that are outside of the circle
                            continue;
                        }
        
                        var deg = Machinata.Math.rad2Deg(phi);
        
                        // Figure out the starting index of this pixel in the image data array.
                        var rowLength = 2*radius;
                        var adjustedX = x + radius; // convert x from [-50, 50] to [0, 100] (the coordinates of the image data array)
                        var adjustedY = y + radius; // convert y from [-50, 50] to [0, 100] (the coordinates of the image data array)
                        var pixelWidth = 4; // each pixel requires 4 slots in the data array
                        var index = (adjustedX + (adjustedY * rowLength)) * pixelWidth;
        
                        var hue = deg;
                        var saturation = r / radius;

                        var rgb = Machinata.Math.hsv2RGB(hue, saturation, hsvValue, true);
                        var rgbToDraw = Machinata.Math.hsv2RGB(hue, saturation, 1.0, true);
                        var alpha = 255;
        
                        data[index] = rgbToDraw[0];
                        data[index + 1] = rgbToDraw[1];
                        data[index + 2] = rgbToDraw[2];
                        data[index + 3] = alpha;

                        //if (rgb[0] == currentColorRGB[0] && rgb[1] == currentColorRGB[1] && rgb[2] == currentColorRGB[2]) {
                        //    selectedPixel = [adjustedX, adjustedY];
                        //}
                        if (adjustedX == dropletCoord[0] && adjustedY == dropletCoord[1]) {
                            currentColorRGB = rgb;
                            if(sliderRGBRCanvasElem != null) sliderRGBRCanvasElem.data("value", (currentColorRGB[0] / 255));
                            if (sliderRGBGCanvasElem != null) sliderRGBGCanvasElem.data("value", (currentColorRGB[1] / 255));
                            if (sliderRGBBCanvasElem != null) sliderRGBBCanvasElem.data("value", (currentColorRGB[2] / 255));
                        }
                    }
                }
                ctx.putImageData(image, 0, 0);
                

                
            }

            

            if (canvasElem.hasClass("ui-bound") == false) {
                canvasElem.addClass("ui-bound");
                if (canvasElem.hasClass("horizontal-slider")) {
                    var mouseDown = false;
                    canvasElem.mousemove(function (event) {
                        if (mouseDown == true) {
                            var x = event.pageX - $(this).offset().left;
                            canvasElem.data("value", x / canvasElem.width());
                            updateCanvases();
                        }
                    });
                    canvasElem.mousedown(function (event) {
                        mouseDown = true;
                        updateCanvases();
                    });
                    canvasElem.mouseup(function (event) {
                        mouseDown = false;
                        var x = event.pageX - $(this).offset().left;
                        canvasElem.data("value", x / canvasElem.width());
                        updateCanvases();
                    });
                    canvasElem.mouseout(function (event) {
                        mouseDown = false;
                        updateCanvases();
                    });
                }
                if (canvasElem.hasClass("select-on-drag")) {
                    var mouseDown = false;
                    canvasElem.mousemove(function (event) {
                        if (mouseDown == true) {
                            var x = event.pageX - $(this).offset().left;
                            var y = event.pageY - $(this).offset().top;
                            dropletCoord = [x, y];
                            updateCanvases();
                        }
                    });
                    canvasElem.mousedown(function (event) {
                        mouseDown = true;
                        updateCanvases();
                    });
                    canvasElem.mouseup(function (event) {
                        mouseDown = false;
                        updateCanvases();
                    });
                    canvasElem.mouseout(function (event) {
                        mouseDown = false;
                        updateCanvases();
                    });
                }
                if (canvasElem.hasClass("select-on-click")) {
                    canvasElem.on("click", function (event) {
                        var x = event.pageX - $(this).offset().left;
                        var y = event.pageY - $(this).offset().top;
                        dropletCoord = [x,y];
                        updateCanvases();
                    });
                }
            }
        });

        if (elemSwatchDroplet != null) {
            elemSwatchDroplet.css("background-color", "rgb(" + currentColorRGB[0] + "," + currentColorRGB[1] + "," + currentColorRGB[2] + ")");
            elemSwatchDroplet.css("left", (dropletCoord[0] - opts.dropletSize / 2) + "px");
            elemSwatchDroplet.css("top", (dropletCoord[1] - opts.dropletSize / 2) + "px");
        }
    }
    updateCanvases();

    return elem;
};



/// <summary>
/// 
/// </summary>
Machinata.UI.restartCSSAnimation = function (elem, className) {
    elem.removeClass(className);
    elem.each(function () {
        void this.offsetWidth;
    });
    elem.addClass(className);
};


/// <summary>
/// 
/// </summary>
Machinata.UI.delayedExpandable = function (elems, opts) {
    if (opts == null) opts = {};
    if (opts.delay == null) opts.delay = 800;

    elems.each(function () {
        var elem = $(this);
        var hoverElem = elem;
        if (elem.find(".expandable-trigger").length > 0) hoverElem = elem.find(".expandable-trigger");

        hoverElem.hover(function () {
            elem.addClass("waiting");
            elem.addClass("option-waiting");
            setTimeout(function () {
                if (!elem.hasClass("waiting")) return;
                elem.addClass("expanded");
                elem.addClass("option-expanded");
                elem.removeClass("waiting");
                elem.removeClass("option-waiting");
            }, opts.delay);
        }, function () {
            
        });
        elem.hover(function () {
            
        }, function () {
            elem.removeClass("waiting");
            elem.removeClass("option-waiting");
            elem.removeClass("expanded");
            elem.removeClass("option-expanded");
        });
    });

    
};



/// <summary>
/// Automatically applies the proper first/last classes to a control group buttons which contain
/// invisible elements.
/// Use this method for control groups that have magnetic styling (ie the buttons stick to each other)
/// </summary>
Machinata.UI.updateControlGroupClasses = function (elem) {
    var buttons = elem.find(".ui-button");
    buttons.removeClass("option-first").removeClass("option-last");
    buttons.filter(":visible").first().addClass("option-first");
    buttons.filter(":visible").last().addClass("option-last");
};

/// <summary>
/// 
/// </summary>
Machinata.UI.magnificationPopup = function (elems, opts) {
    if (opts == null) opts = {};
    if (opts.delay == null) opts.delay = 100;

    elems.each(function () {
        var elem = $(this);
        var hoverElem = elem;
        if (elem.find(".magnification-trigger").length > 0) hoverElem = elem.find(".magnification-trigger");

        function update(event) {
            if (event == null) return;
            var magElem = elem.data("mag-elem");
            if (magElem != null) {
                magElem.css("left", event.pageX + "px");
                magElem.css("top", event.pageY + "px");
            }
            elem.data("last-move-event", event);
        }

        hoverElem.on("mousemove", function (event) {
            update(event);
        });
        hoverElem.hover(function (event) {
            elem.addClass("waiting");
            elem.addClass("option-waiting");


            // Create mag elem
            var bgImage = "url(" + elem.attr("data-magnification-image") + ")";
            if (elem.find(".magnification-image").length > 0) {
                bgImage = elem.find(".magnification-image").css("background-image");
            }
            var magElem = $("<div class='ui-magnification'></div>");
            magElem.css("width", hoverElem.width() + "px");
            magElem.css("height", hoverElem.height() + "px");
            magElem.css("background-image", bgImage);
            magElem.addClass("hidden");
            $("body").append(magElem);
            elem.data("mag-elem", magElem);


            setTimeout(function () {
                if (!elem.hasClass("waiting")) return;


                setTimeout(function () {

                    update(elem.data("last-move-event"));
                    magElem.removeClass("hidden");
                    magElem.css("width", "").css("height", "");
                }, 10);

                elem.addClass("magnified");
                elem.addClass("option-magnified");
                elem.removeClass("waiting");
                elem.removeClass("option-waiting");
            }, opts.delay);
        }, function () {

        });
        hoverElem.hover(function () {

        }, function () {
            elem.removeClass("waiting");
            elem.removeClass("option-waiting");
            elem.removeClass("magnified");
            elem.removeClass("option-magnified");
            var magElem = elem.data("mag-elem");
            if (magElem != null) magElem.remove();
        });
    });


};

/// <summary>
/// 
/// </summary>
Machinata.UI.buildSVGIcon = function (iconName, iconURL) {
    if (iconURL == null) {
        var dummyIcon = $("{icon.test}");
        iconURL = dummyIcon.find("use").attr("xlink:href");
        iconURL = iconURL.replace("#test", "#" + iconName);
    }
    return $('<svg class="icon icon-' + iconName + '"><use xlink:href="' + iconURL + '"></use></svg>');
};

/// <summary>
/// 
/// </summary>
Machinata.UI.isTruncated = function (elem) {
    // See https://stackoverflow.com/questions/7738117/html-text-overflow-ellipsis-detection
    var tolerance = 2; // In px. Depends on the font you are using
    return (elem.outerWidth() + tolerance < elem[0].scrollWidth);

};

/// <summary>
/// 
/// </summary>
Machinata.UI.autoShowTextAsTooltipOnTruncated = function (elements) {
    elements.hover(function () {
        if (Machinata.UI.isTruncated($(this))) {
            $(this).attr("data-title-orig", $(this).attr("title"));
            $(this).attr("title", $(this).text());
        } else {
            $(this).attr("title", $(this).attr("data-title-orig"));
        }
    });
};



/// <summary>
/// 
/// </summary>
Machinata.UI.triggerOnElementInViewport = function (elements, enterViewportCallback, leaveViewportCallback) {
    elements.each(function () {
        var elem = $(this);
        var update = function () {
            if (elem.visible(true)) {
                //TODO: only call on change state
                if (enterViewportCallback) enterViewportCallback(elem);
            } else if (elem.hasClass("animate-on-disappear")) {
                //TODO: only call on change state
                if (leaveViewportCallback) leaveViewportCallback(elem);
            }
        };
        $(window).on('scroll', update);
        update();
    });
};




/// <summary>
/// 
/// </summary>
Machinata.UI.createFileDropZone = function (elements) {
    elements.each(function () {
        var elem = $(this);
        elem.append("<div class='tip'>{text.drop-files-here}</div>");
        elem.append("<div class='previews'></div>");
        elem.append("<div class='clear'></div>");
        var files = [];
        elem.on('dragover', function (e) {
            e.preventDefault();
            e.stopPropagation();
        });
        elem.on('dragenter', function (e) {
            e.preventDefault();
            e.stopPropagation();
        });
        elem.on('drop',function (e) {
            if (e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files.length) {
                e.preventDefault();
                e.stopPropagation();
                    
                console.log(e.originalEvent.dataTransfer.files);
                for (var i = 0; i < e.originalEvent.dataTransfer.files.length; i++) {
                    // Register in form
                    var file = e.originalEvent.dataTransfer.files[i];
                    files.push(file);
                    // Create preview
                    var previewElem = Machinata.Content.createFilePreview(file.name, file.name, false);
                    elem.find(".previews").append(previewElem);
                    elem.find(".tip").hide();
                }
            }
        });
        var uploadButtonElem = $(elem.attr("data-dropzone-button"));
        uploadButtonElem.click(function () {
            uploadButtonElem.addClass("disabled");
            elem.addClass("uploading");
            // Do API call to upload
            var call = Machinata.apiCall(elem.attr("data-dropzone-api-call"));
            for (var i = 0; i < files.length; i++) {
                // Register in form
                call.attachFileObject(files[i]);
            }
            // Prepare api call
            call.success(function (message) {
                uploadButtonElem.removeClass("disabled");
                elem.removeClass("uploading");
                Machinata.processCallback(elem.attr("data-dropzone-api-call-success"), message);
            })
            call.error(function (message) {
                uploadButtonElem.removeClass("disabled");
                elem.removeClass("uploading");
            });
            call.genericError();
            // Send
            call.send();
        });
    });
};




/// <summary>
/// 
/// </summary>
Machinata.UI.toggleTableRowsSortable = function (tableElem) {
    if (tableElem.hasClass("option-rows-sortable")) {
        Machinata.UI.disableTableRowsSortable(tableElem);
    } else {
        Machinata.UI.makeTableRowsSortable(tableElem);
    }
};


/// <summary>
/// 
/// </summary>
Machinata.UI.makeTableRowsSortable = function (tableElem) {
    tableElem.addClass("option-rows-sortable");
    tableElem.find("tbody").sortable({
        //placeholder: "content-node placeholder",
        //handle: ".handle"
        start: function (event, ui) {

        },
        stop: function (event, ui) {
            // Remove fixed withs...
            //BUG: what if a cell had a width set?
            tableElem.find("td[data-original-width]").each(function () {
                //$(this).css("width", $(this).attr("data-original-width") || "");
                $(this).css("width", null);
            });
            // Send new order data api call?
            if (tableElem.attr("data-sort-api-call") != null && tableElem.attr("data-sort-api-call") != "") {
                var call = Machinata.apiCall(tableElem.attr("data-sort-api-call"));
                var data = {};
                var newOrder = [];
                var sortNumber = 0;
                tableElem.find("tbody tr").each(function () {
                    newOrder.push($(this).attr("data-public-id"));
                    data[$(this).attr("data-public-id")] = sortNumber;
                    sortNumber++;
                });
                data["order"] = newOrder.join(",");
                call.data(data);
                call.success(function (message) {

                })
                call.error(function (message) {

                });
                call.genericError();
                call.send();
            }
        },
        create: function (event, ui) {
            // Update UI with needed additions
            tableElem.find("thead tr").each(function () {
                $(this).prepend($("<th class='sorter-handle'></th>"));
            });
            tableElem.find("tbody tr").each(function () {
                $(this).prepend($("<td class='sorter-handle'></td>").append("{icon.sort}"));
            });
            // Set all all TDs to fixed width (since the dragged thing has no total width)
            tableElem.find("td").each(function () {
                $(this).attr("data-original-width", $(this).css("width"));
                $(this).css("width", $(this).outerWidth());
            });
        },
    });
};

/// <summary>
/// 
/// </summary>
Machinata.UI.disableTableRowsSortable = function (tableElem) {
    tableElem.find("th.sorter-handle,td.sorter-handle").remove();
    tableElem.removeClass("option-rows-sortable");
    tableElem.find("tbody").sortable("destroy");
};

/// <summary>
/// 
/// </summary>
Machinata.UI.createImageComparer = function (elem, opts) {
    // Loosely based on https://codyhouse.co/demo/image-comparison-slider/index.html

    // Init
    if (opts == null) opts = {};
    if (elem.attr("data-starting-position") != null) opts.startingPosition = parseFloat(elem.attr("data-starting-position"));
    if (opts.minMaxPadding == null) opts.minMaxPadding = 0; // in percent of handle width
    if (opts.startingPosition == null) opts.startingPosition = 0.5; // in decimal percent

    // Create the handle
    var handleElem = $("<div class='handle'/>").appendTo(elem);

    // Convert image b to overlay
    var imageB = $("<div class='image-b'/>")
        .css("background-image","url("+elem.find("img.image-b").attr("src")+")")
        .appendTo(elem);

    // Init drag params
    var dragWidth = null;
    var xPosition = null;
    var containerOffset = null;
    var containerWidth = null;
    var minLeft = null;
    var maxLeft = null;
    var isDragging = false;

    // Helper functions
    function loadDimensions() {
        dragWidth = handleElem.outerWidth();
        containerOffset = elem.offset().left;
        containerWidth = elem.outerWidth();
        minLeft = containerOffset + (handleElem.width() * opts.minMaxPadding) - handleElem.width()/2;
        maxLeft = containerOffset + containerWidth - dragWidth - (handleElem.width() * opts.minMaxPadding) + handleElem.width() / 2;
    }
    function setHandlePosition(decimalPercent) {
        //var percentValue = (leftValue + dragWidth / 2 - containerOffset) * 100 / containerWidth + '%';
        var percentValue = decimalPercent * 100 + "%";
        // Update
        handleElem.css('left', percentValue);
        imageB.css('width', percentValue);
    }

    // Initial state
    loadDimensions();
    setHandlePosition(opts.startingPosition);

    // Bind handle events
    handleElem.on("mousedown touchstart", function (event) {
        // Get the proper event (since we support both touch and reg)
        var eventToUse = event;
        if (event.originalEvent != null && event.originalEvent.touches != null) eventToUse = event.originalEvent.touches[0];
        // Set params
        // We do this on every start since the sizes may have changed
        loadDimensions();
        xPosition = handleElem.offset().left + dragWidth - eventToUse.pageX;
        // Update
        isDragging = true;
        handleElem.addClass('dragging');
        // Prevent other events
        event.preventDefault();
    }).on("mouseup touchend", function (e) {
        isDragging = false;
        handleElem.removeClass('dragging');
    });
    handleElem.parent().on("mousemove touchmove", function (event) {
        if (!isDragging) return;
        // Get the proper event (since we support both touch and reg)
        var eventToUse = event;
        if (event.originalEvent != null && event.originalEvent.touches != null) eventToUse = event.originalEvent.touches[0];
        // Calculate new values
        var leftValue = eventToUse.pageX + xPosition - dragWidth;
        if (leftValue < minLeft) {
            leftValue = minLeft;
        } else if (leftValue > maxLeft) {
            leftValue = maxLeft;
        }
        var decimalPercent = (leftValue + dragWidth / 2 - containerOffset) / containerWidth;
        // Update
        setHandlePosition(decimalPercent);
    }).on("mouseup touchend", function (e) {
        isDragging = false;
        handleElem.removeClass('dragging');
    });
};

/// <summary>
/// 
/// </summary>
Machinata.UI.createTrackSlider = function (elem, opts) {
    // Init
    if (opts == null) opts = {};

    // Create the handle
    var handleElem = elem.find(".slider");

    // Init drag params
    var dragWidth = null;
    var xPosition = null;
    var containerOffset = null;
    var containerWidth = null;
    var minLeft = null;
    var maxLeft = null;
    var isDragging = false;
    var sliderValue = null;
    var lastTriggerSliderValue = null; // tracks the last value we triggered, to ensure we don't trigger twice...

    // Helper functions
    function calculateDimentions() {
        dragWidth = handleElem.outerWidth();
        containerOffset = elem.offset().left;
        containerWidth = elem.outerWidth();
        minLeft = containerOffset + (handleElem.width() * 0.0);
        maxLeft = containerOffset + containerWidth - dragWidth - (handleElem.width() * 0.0);
    } calculateDimentions();
    function updateSliderValue(val,callTrigger) {
        if (val < 0) {
            val = 0;
        } else if (val > 1.0) {
            val = 1.0;
        }
        var leftValuePadding = dragWidth / containerWidth / 2; // because the actual value is not entirely from 0 to 1
        var leftValueAdjusted = Machinata.Math.map(val, 0.0, 1.0, leftValuePadding, 1.0 - leftValuePadding);
        handleElem.css('left', (leftValueAdjusted*100) + "%");
        elem.attr('data-slider-value', val);
        sliderValue = val;
        // Trigger?
        if (sliderValue != lastTriggerSliderValue && callTrigger == true) {
            lastTriggerSliderValue = sliderValue;
            elem.trigger("sliderValueChanged", [sliderValue]);
        }
    };
    function snapToStartEnd() {
        if (sliderValue != null) {
            if (sliderValue < 0.33333) {
                updateSliderValue(0.0, true);
            } else if (sliderValue > 0.66666) {
                updateSliderValue(1.0, true);
            } else {
                updateSliderValue(0.5, true);
            }
        }
    }
    function cancelDrag() {
        if (isDragging == true) {
            isDragging = false;
            handleElem.removeClass('dragging');
            if (opts.snapToStartEnd == true) {
                snapToStartEnd();
            }
        }
    }

    // Initial state
    if (opts.initialValue != null) {
        handleElem.addClass('dragging');
        setTimeout(function () {
            calculateDimentions();
            updateSliderValue(opts.initialValue, false);
            setTimeout(function () {
                handleElem.removeClass('dragging');
            }, 20);
        }, 20);
    }

    // Bind handle events
    elem.on("updateSliderValue", function (event, val) {
        calculateDimentions();
        lastTriggerSliderValue = val; // ensures we don't trigger this
        updateSliderValue(val);
    });
    handleElem.on("mousedown touchstart", function (event) {
        // Get the proper event (since we support both touch and reg)
        var eventToUse = event;
        if (event.originalEvent != null && event.originalEvent.touches != null) eventToUse = event.originalEvent.touches[0];
        // Set params
        // We do this on every start since the sizes may have changed
        calculateDimentions();
        xPosition = handleElem.offset().left + dragWidth - eventToUse.pageX;
        // Update
        isDragging = true;
        handleElem.addClass('dragging');
        // Prevent other events
        event.preventDefault();
    }).on("mouseup touchend", function (e) {
        cancelDrag();
    });
    handleElem.parent().on("mousemove touchmove", function (event) {
        if (!isDragging) return;
        // Get the proper event (since we support both touch and reg)
        var eventToUse = event;
        if (event.originalEvent != null && event.originalEvent.touches != null) eventToUse = event.originalEvent.touches[0];
        // Calculate new values
        var bandwidth = maxLeft - minLeft;
        var leftValueRaw = eventToUse.pageX + xPosition - dragWidth - minLeft;
        var leftValueP = leftValueRaw / bandwidth;
        // Update
        updateSliderValue(leftValueP);
    }).on("mouseup touchend", function (e) {
        cancelDrag();
    });
    $(document).on("mouseup touchend", function () {
        cancelDrag();
    });
};


/// <summary>
///
/// </summary>
Machinata.UI.createEditableTable = function (tableElem, opts) {
    tableElem.addClass("ui-table");
    tableElem.addClass("ui-editable-table");
    tableElem.attr("contenteditable", "true");
    // Init
    var CELL_PLACEHOLDER = "Cell";
    var COLUMN_PLACEHOLDER = "Column";
    // Helper functions
    function getColumn(senderElem) {
        var cellElem = senderElem.closest("td,th");
        var columnIndex = cellElem.index()+1;
        return tableElem.find("tr td:nth-child(" + columnIndex + "), tr th:nth-child(" + columnIndex + ")");
    }
    function deleteColumn(senderElem) {
        var columnElems = getColumn(senderElem);
        columnElems.remove();
        // Rebuild
        rebuildUI();
    }
    function addColumn(senderElem, where) {
        var columnElems = getColumn(senderElem);
        columnElems.each(function () {
            var tag = "td";
            var placeholder = CELL_PLACEHOLDER;
            if ($(this).parent().parent().prop("tagName") == "THEAD") { tag = "th"; placeholder = COLUMN_PLACEHOLDER;};
            var cellElem = $("<" + tag + ">" + placeholder+"</" + tag + ">");
            if (where == "after") cellElem.insertAfter($(this));
            else if (where == "before") cellElem.insertBefore($(this));
        });
        // Rebuild
        rebuildUI();
    }
    function getRow(senderElem) {
        return senderElem.closest("tr");
    }
    function deleteRow(senderElem) {
        var rowElem = getRow(senderElem);
        if (rowElem.parent().prop("tagName") == "THEAD") {
            //TODO: warn
        } else {
            rowElem.remove();
        }
        // Rebuild
        rebuildUI();
    }
    function addRow(senderElem, where) {
        var rowElem = getRow(senderElem);
        if (rowElem.parent().prop("tagName") == "THEAD") {
            rowElem = tableElem.find("tbody tr").first();
            if (where == "after") where = "before";
        }
        var newRow = $("<tr></tr>");
        var numColumns = Math.max(tableElem.find("thead tr").first().find("th").length, tableElem.find("tbody tr").first().find("td").length);
        for (var i = 0; i < numColumns; i++) newRow.append($("<td>" + CELL_PLACEHOLDER+"</td>"));
        if (rowElem.length == 0) newRow.appendTo(tableElem.find("tbody"));
        else if (where == "after") newRow.insertAfter(rowElem);
        else if (where == "before") newRow.insertBefore(rowElem);
        // Rebuild
        rebuildUI();
    }
    function rebuildUI() {
        // Remove all
        tableElem.find(".table-tools").remove();
        // Prune empty rows
        tableElem.find("tr").each(function () {
            if ($(this).children().length == 0) $(this).remove();
        });
        // Empty table?
        if (tableElem.find("td").length == 0 && tableElem.find("th").length == 0) {
            // Default table
            tableElem.find("thead").append($("<tr><th>" + COLUMN_PLACEHOLDER + "</th></tr>"));
            tableElem.find("tbody").append($("<tr><td>" + CELL_PLACEHOLDER + "</td></tr>"));
        }
        // Add columns header UI
        tableElem.find("thead th").each(function () {
            var toolsElem = $("<div class='table-tools position-top-center' contenteditable='false'></div>");
            toolsElem.append("<a title='{text.delete-column}' class='ui-button option-small option-translucent option-icon-only action-delete-column'>{icon.trash}</a>");
            $(this).append(toolsElem);
        });
        tableElem.find("thead th").each(function () {
            var toolsElem = $("<div class='table-tools position-top-right' contenteditable='false'></div>");
            toolsElem.append("<a title='{text.add-column}' class='ui-button option-small option-translucent option-icon-only action-add-column-after'>{icon.plus}</a>");
            $(this).append(toolsElem);
        });
        tableElem.find("thead th").each(function () {
            var toolsElem = $("<div class='table-tools position-top-left' contenteditable='false'></div>");
            toolsElem.append("<a title='{text.add-column}' class='ui-button option-small option-translucent option-icon-only action-add-column-before'>{icon.plus}</a>");
            $(this).append(toolsElem);
        });
        // Add rows UI
        tableElem.find("tr td:first-child, tr th:first-child").each(function () {
            var toolsElem = $("<div class='table-tools position-left-center' contenteditable='false'></div>");
            toolsElem.append("<a title='{text.delete-row}' class='ui-button option-small option-translucent option-icon-only action-delete-row'>{icon.trash}</a>");
            $(this).append(toolsElem);
        });
        tableElem.find("tr td:last-child, tr th:last-child").each(function () {
            var toolsElem = $("<div class='table-tools position-right-center' contenteditable='false'></div>");
            toolsElem.append("<a title='{text.delete-row}' class='ui-button option-small option-translucent option-icon-only action-delete-row'>{icon.trash}</a>");
            $(this).append(toolsElem);
        });
        tableElem.find("tr td:last-child, tr th:last-child").each(function () {
            var toolsElem = $("<div class='table-tools position-right-bottom' contenteditable='false'></div>");
            toolsElem.append("<a title='{text.add-row}' class='ui-button option-small option-translucent option-icon-only action-add-row-after'>{icon.plus}</a>");
            $(this).append(toolsElem);
        });
        tableElem.find("tbody tr:last-child td").each(function () {
            var toolsElem = $("<div class='table-tools position-bottom-center' contenteditable='false'></div>");
            toolsElem.append("<a title='{text.add-row}' class='ui-button option-small option-translucent option-icon-only action-add-row-after'>{icon.plus}</a>");
            $(this).append(toolsElem);
        });
        // Bind actions
        tableElem.find(".action-delete-row").click(function () {
            deleteRow($(this));
        });
        tableElem.find(".action-add-row-after").click(function () {
            addRow($(this), "after");
        });
        tableElem.find(".action-delete-column").click(function () {
            deleteColumn($(this));
        });
        tableElem.find(".action-add-column-after").click(function () {
            addColumn($(this), "after");
        });
        tableElem.find(".action-add-column-before").click(function () {
            addColumn($(this), "before");
        });
    }
    // Initial UI build
    rebuildUI();
};

/// <summary>
///
/// </summary>
Machinata.UI.getEditableTableHTML = function (tableElem, opts) {
    var clonedElem = tableElem.clone();
    clonedElem.attr("class", null);
    clonedElem.attr("contenteditable", null);
    clonedElem.find(".table-tools").remove();
    return clonedElem.prop("outerHTML");
};


/// <summary>
/// 
/// </summary>
Machinata.UI.bindScrollFadeForElement = function (scrollableElem) {
    scrollableElem.on("scroll", function () {
        Machinata.UI.updateScrollFadeForElement(scrollableElem);
    });
    scrollableElem.addClass("ui-bound");
    Machinata.UI.updateScrollFadeForElement(scrollableElem);
}

/// <summary>
/// 
/// </summary>
Machinata.UI.updateScrollFadeForElement = function (scrollableElem) {
    // Via https://stackoverflow.com/questions/70970529/css-div-fade-scroll-styling
    const isScrollable = scrollableElem[0].scrollHeight > scrollableElem[0].clientHeight;
    if (!isScrollable) {
        scrollableElem[0].classList.remove('option-bottom-overflowing', 'option-top-overflowing');
        return;
    }
    // Otherwise, the element is overflowing!
    // Now we just need to find out which direction it is overflowing to (can be both)
    const isScrolledToBottom = scrollableElem[0].scrollHeight <= scrollableElem[0].clientHeight + scrollableElem[0].scrollTop;
    const isScroledlToTop = scrollableElem[0].scrollTop === 0;
    scrollableElem[0].classList.toggle('option-bottom-overflowing', !isScrolledToBottom);
    scrollableElem[0].classList.toggle('option-top-overflowing', !isScroledlToTop);
    return scrollableElem;
}

/// <summary>
/// 
/// </summary>
Machinata.UI.bindEditableEntityLists = function (elements) {

    elements.find(".ui-editable-entity-list").not(".bb-ui-bound").addClass("bb-ui-bound").each(function () {

        // Init
        var elem = $(this);
        var tableElem = elem.find("table");

        // Get template
        var templateElem = elem.find("tr.entity-list-item").first();
        templateElem.remove();

        // Create item
        function createNewItem(copyFromRow, insertAfter){
            var newRow = templateElem.clone();
            // Temporary id (guid) for new entity 
            var guid = Machinata.guid().replaceAll("-", "");
            // Update names of inputs
            newRow.find("input").each(function () {
                var input = $(this);
                var formname = input.closest("td").attr("form-name");
                input.attr("name", guid + ":" + formname);
            });
            // Copy values of last item?
            if (copyFromRow != null) {
                var columnsNewRow = newRow.find("td");
                var columnsCopyRow = copyFromRow.find("td");
                for (var i = 0; i < columnsCopyRow.length; i++) {
                    // Get val
                    var copyVal = columnsCopyRow.eq(i).find("input[data-property-original-value]").val();
                    columnsNewRow.eq(i).find("input[data-property-original-value]").val(copyVal);
                }
            }
            // Apend to table
            if (insertAfter != null) {
                newRow.insertAfter(insertAfter);
            } else {
                tableElem.append(newRow);
            }
            // Setup
            setupItem(newRow);
            // Bind UI
            Machinata.Forms.bindFormBuilderUIForForms(newRow);
        }

        // Helper function to bind item actions and UI
        function setupItem(itemElem) {
            var tr = itemElem; //TODO: support non table row type listings?

            var toolsColumn = $("<td></td>").append(deleteButton);
            tr.append(toolsColumn);
            // Delete
            {
                var deleteButton = $("<a title={text.delete-item} class='ui-button option-translucent option-icon-only option-small'>" + "{icon.trash}" + "</a>");
                deleteButton.css("display", "inline-block");
                deleteButton.click(function (event) {
                    event.preventDefault();
                    var button = $(this);
                    button.closest("tr").remove();
                    var form = button.closest("form");
                    var deletedItem = $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', 'deleted:' + button.closest("tr").attr("data-public-id"))
                        .appendTo('form');
                    form.append(deletedItem);
                });
                toolsColumn.append(deleteButton);
            }
            // Duplicate
            {
                var duplicateButton = $("<a title={text.duplicate-item} class='ui-button option-translucent option-icon-only option-small'>" + "{icon.duplicate}" + "</a>");
                duplicateButton.css("display", "inline-block");
                duplicateButton.click(function (event) {
                    event.preventDefault();
                    var button = $(this);
                    var rowToCopy = button.closest("tr");
                    createNewItem(rowToCopy, rowToCopy);
                });
                toolsColumn.append(duplicateButton);
            }
        }

        // New row action
        {
            elem.find(".action-new-row").click(function (event) {
                // Init
                event.preventDefault();
                // Copy values of last item?
                if (event.shiftKey) {
                    var lastRow = tableElem.find("tr").last();
                    createNewItem(lastRow);
                } else {
                    createNewItem();
                }
            });
        }

        // Setup all items
        elem.find("tr.entity-list-item").each(function () {
            setupItem($(this));
        });

        // Bind UI
        Machinata.Forms.bindFormBuilderUIForForms(elem);
          
    });
};



/// <summary>
/// 
/// </summary>
Machinata.UI.bindPageNavigationToShowLoading = function () {
    var didShowLoadingDueToUnload = false; // gate to ensure that the show loading is deactivated if a page is refocused... (usually happens when user uses back button in browser)
    $(window).on('beforeunload', function () {
        Machinata.showLoading(true);
        didShowLoadingDueToUnload = true;
    });
    $(window).on('focus', function () {
        if (didShowLoadingDueToUnload == true) {
            Machinata.showLoading(false);
            didShowLoadingDueToUnload = false;
        }
    });
}



/// <summary>
/// 
/// </summary>
Machinata.UI.detectSwipe = function (elements, opts) {
    // Via https://gist.github.com/Skerth/7d2bcefd33e8b440e7183c38b64fcc5f

    // States: 0 - no swipe, 1 - swipe started, 2 - swipe released
    var swipeState = 0;
    // Coordinates when swipe started
    var startX = 0;
    var startY = 0;
    // Distance of swipe
    var pixelOffsetX = 0;
    var pixelOffsetY = 0;
    // Target element which should detect swipes.
    var swipeTarget = elements;
    var defaultSettings = {
        // Amount of pixels, when swipe don't count.
        swipeThreshold: 70,
        // Flag that indicates that plugin should react only on touch events.
        // Not on mouse events too.
        useOnlyTouch: false
    };

    var options = $.extend(defaultSettings, opts);
    // Support touch and mouse as well.
    swipeTarget.on('mousedown touchstart', swipeStart);
    $('html').on('mouseup touchend', swipeEnd);
    $('html').on('mousemove touchmove', swiping);

    function swipeStart(event) {
        if (options.useOnlyTouch && !event.originalEvent.touches)
            return;

        if (event.originalEvent.touches)
            event = event.originalEvent.touches[0];

        if (swipeState === 0) {
            swipeState = 1;
            startX = event.clientX;
            startY = event.clientY;
        }
    }

    function swipeEnd(event) {
        if (swipeState === 2) {
            swipeState = 0;

            if (Math.abs(pixelOffsetX) > Math.abs(pixelOffsetY) &&
                Math.abs(pixelOffsetX) > options.swipeThreshold) { // Horizontal Swipe
                if (pixelOffsetX < 0) {
                    swipeTarget.trigger("swipe-left");
                } else {
                    swipeTarget.trigger("swipe-right");
                }
            } else if (Math.abs(pixelOffsetY) > options.swipeThreshold) { // Vertical swipe
                if (pixelOffsetY < 0) {
                    swipeTarget.trigger("swipe-up");
                } else {
                    swipeTarget.trigger("swipe-down");
                }
            }
        }
    }

    function swiping(event) {
        // If swipe don't occuring, do nothing.
        if (swipeState !== 1)
            return;


        if (event.originalEvent.touches) {
            event = event.originalEvent.touches[0];
        }

        var swipeOffsetX = event.clientX - startX;
        var swipeOffsetY = event.clientY - startY;

        if ((Math.abs(swipeOffsetX) > options.swipeThreshold) ||
            (Math.abs(swipeOffsetY) > options.swipeThreshold)) {
            swipeState = 2;
            pixelOffsetX = swipeOffsetX;
            pixelOffsetY = swipeOffsetY;
        }
    }

    return swipeTarget; // Return element available for chaining.
};




/// <summary>
/// 
/// </summary>
Machinata.UI.createNumericDateInput = function (elements, opts) {
    elements.each(function () {
        var inputElem = $(this);
        var DEBUG_ENABLED = false;
        if (opts == null) opts = {};

        // General setup for input
        inputElem.attr("type","text");
        inputElem.attr("inputmode","decimal");
        inputElem.attr("placeholder","{text.date-placeholder.eu}");
        inputElem.attr("maxlength","255");
        inputElem.attr("pattern","(?:0?[1-9]|1[0-9]|2[0-9]|3[01]).(?:0?[1-9]|1[012]).[0-9]{4}");
        if (opts.value != null) inputElem.val(opts.value);

        function autocomplete(event, text, char) {
            event.preventDefault();
            inputElem.val(text + char);
            if (DEBUG_ENABLED) console.log("autocomplete", text + " => " + text+char, event);
        }

        function validateAndAutocomplete(event) {
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            var text = inputElem.val();
            if (text == null) text = "";
            text += key;
            var segs = text.split(".");
            var dd = null;
            var mm = null;
            var yyyy = null;
            if (segs.length >= 1) dd = segs[0];
            if (segs.length >= 2) mm = segs[1];
            if (segs.length >= 3) yyyy = segs[2];
            var ddNum = null
            if (dd != null) ddNum = parseInt(dd);
            if (isNaN(ddNum)) ddNum = null;
            console.log(ddNum);

            // Autocompletes - here we automatically continue the number segment by adding a . when anything else no longer
            // makes sense...
            // dd.mm.yyyy
            //
            // dd >> dd.
            if (segs.length == 1 && dd != null && dd.length == 2) {
                autocomplete(event, text, ".");
            }
            // d(4-9) >> d(4-9).
            else if (segs.length == 1 && dd != null && dd.length == 1 && ddNum != null && (ddNum >= 4 && ddNum <= 9)) {
                autocomplete(event, text, ".");
            }
            // dd.mm >> dd.mm.
            else if (segs.length == 2 && mm != null && mm.length == 2) {
                autocomplete(event, text, ".");
            }
            // dd.2 >> dd.2.
            // dd.3 >> dd.3.
            // dd.4 >> dd.4.
            // ...
            else if (segs.length == 2 && mm != null && (mm == "2" || mm == "3" || mm == "4" || mm == "5" || mm == "6" || mm == "7" || mm == "8" || mm == "9")) {
                autocomplete(event, text, ".");
            }

            if (DEBUG_ENABLED) console.log("validateAndAutocomplete", "text=" + text, "dd=" + dd, "mm=" + mm, "yyyy=" + yyyy, event);
        }

        // Bind input events
        //inputElem.on("input", function (event) {
        //});
        inputElem.on('keypress', function (event) {
            // Filter out anything but numbers and dot
            var regex = new RegExp("^[.0-9]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                if (DEBUG_ENABLED) console.log("cancel keypress",key,event);
                return false;
            }
            // Is the latest character already a dot?
            // If so, ignore a double dot input...
            // This is useful if a user is inputting blindly, but we are autocompleting the dots, resulting double dots if we dont trim them out
            if (key == ".") {
                var text = inputElem.val();
                if (text == null) text = "";
                if (Machinata.String.endsWith(text,".") == true) {
                    event.preventDefault();
                    if (DEBUG_ENABLED) console.log("cancel keypress (duplicate .)", key, event);
                    return false;
                }
            }
            // Validate and auto complete
            validateAndAutocomplete(event);
        });
    });
};