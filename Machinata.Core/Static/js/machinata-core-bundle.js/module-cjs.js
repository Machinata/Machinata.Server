
/// <summary>
/// Provides CommonJS module loading support
/// </summary>
var Machinata = {};
Machinata.test2342 = 3223;

MACHINATA_GLOBAL.MACHINATA = Machinata;
if (typeof module === 'object' && typeof module.exports === 'object') {
	// CommonJS
	module.exports = Machinata;
}
