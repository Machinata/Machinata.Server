﻿/* ======== Javascript Extensions ======== */

if (typeof String.prototype.hashCode != 'function') {
    // https://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript-jquery
    String.prototype.hashCode = function () {
        var hash = 0, i, chr;
        if (this.length === 0) return hash;
        for (i = 0; i < this.length; i++) {
            chr = this.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return hash;
    };
}

if (typeof String.prototype.trim != 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, "");
    };
}

if (typeof String.isEmpty != 'function') {
    String.isEmpty = function (str) {
        return (str == null || str == "");
    };
}

if (typeof String.isNullOrEmpty != 'function') {
    String.isNullOrEmpty = function (str) {
        return (str == null || str == "");
    };
}

if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function (str) {
        return this.slice(0, str.length) == str;
    };
}

if (typeof String.prototype.endsWith != 'function') {
    String.prototype.endsWith = function (str) {
        return this.slice(-str.length) == str;
    };
}

if (typeof String.prototype.contains != 'function') {
    String.prototype.contains = function (str) {
        return this.indexOf(str) > -1;
    };
}

if (typeof String.prototype.toSentence != 'function') {
    String.prototype.toSentence = function () {
        if (this.toUpperCase() == this) return this.toLowerCase();
        return this.replace(new RegExp("([A-Z])([A-Z])([a-z])|([a-z])([A-Z])","g"), "$1$4 $2$3$5");
    };
}

if (typeof String.prototype.padLeft != 'function') {
    String.prototype.padLeft = function (length, character) {
        return new Array(length - this.length + 1).join(character || '0') + this;
    };
}

if (typeof String.prototype.replaceAll != 'function') {
    String.prototype.replaceAll = function (replace, withThis) {
        //return this.replace(new RegExp(replace, 'g'), withThis);
        return this.split(replace).join(withThis);
    };
}

if (typeof String.prototype.replaceAllRegex != 'function') {
    String.prototype.replaceAllRegex = function (replace, withThis) {
        return this.replace(new RegExp(replace, 'g'), withThis);
    };
}

if (typeof String.prototype.capitalize != 'function') {
    String.prototype.capitalize = function () {
        return this[0].toUpperCase() + this.substring(1);
    };
}

if (typeof String.prototype.replaceNonAscii != 'function') {
    String.prototype.replaceNonAscii = function (withThis) {
        var ret = this;
        ret = ret.replaceAll("\\.", withThis);
        ret = ret.replaceAll("-", withThis);
        ret = ret.replaceAll(" ", withThis);
        return ret;
    };
}

if (!String.prototype.encodeHTML) {
    String.prototype.encodeHTML = function () {
        return this.replace(/&/g, '&amp;')
                   .replace(/</g, '&lt;')
                   .replace(/>/g, '&gt;')
                   .replace(/"/g, '&quot;');
        //.replace(/'/g, '&apos;');
    };
}

if (!String.prototype.decodeHTML) {
    String.prototype.decodeHTML = function () {
        //return this.replace(/&apos;/g, "'")
        return this.replace(/&quot;/g, '"')
                   .replace(/&gt;/g, '>')
                   .replace(/&lt;/g, '<')
                   .replace(/&amp;/g, '&');
    };
}

// Math.log10 Polyfill (for IE11 support)
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/log10
if (typeof Math.log10 != 'function') {
    Math.log10 = Math.log10 || function (x) {
        return Math.log(x) * Math.LOG10E;
    };
}

// Math.log10 Polyfill (for IE11 support)
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isFinite
if (typeof Number.isFinite != 'function') {
    if (Number.isFinite === undefined) Number.isFinite = function (value) {
        return typeof value === 'number' && isFinite(value);
    }
}

// Math.log10 Polyfill (for IE11 support)
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isNaN
if (typeof Number.isNaN != 'function') {
    Number.isNaN = Number.isNaN || function isNaN(input) {
        return typeof input === 'number' && input !== input;
    }
}