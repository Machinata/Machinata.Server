

/// <summary>
/// </summary>
/// <type>namespace</type>
Machinata.Debugging = {};

Machinata.Debugging._start = null;
Machinata.Debugging._timers = null;

/// <summary>
///
/// </summary>
Machinata.Debugging.startTimer = function (id, category) {
    var ms = new Date();
    if (Machinata.Debugging._start == null) Machinata.Debugging._start = ms;
    if (Machinata.Debugging._timers == null) Machinata.Debugging._timers = {};
    var timer = {
        id: id,
        category: category,
        start: ms
    };
    Machinata.Debugging._timers[id] = timer;
    return timer;
};

/// <summary>
///
/// </summary>
Machinata.Debugging.stopTimer = function (id) {
    var timer = Machinata.Debugging._timers[id];
    timer.end = new Date();
    return timer;
};


/// <summary>
///
/// </summary>
Machinata.Debugging.compileTimerStats = function () {
    $.each(Machinata.Debugging._timers, function (key, timer) {
        timer.ellapsed = timer.end - timer.start;
        timer.rstart = timer.start - Machinata.Debugging._start;
        timer.rend = timer.end - Machinata.Debugging._start;
    });
};

/// <summary>
///
/// </summary>
Machinata.Debugging.createTimersDebugPanel = function (title) {
    var data = {};
    var totalEllapsed = 0;
    Machinata.Debugging.compileTimerStats();
    $.each(Machinata.Debugging._timers, function (key, timer) {
        data[key] = [timer.ellapsed + "ms", timer.rstart+"ms..."+ timer.rend+"ms"];
        totalEllapsed += timer.ellapsed;
    });
    data["Total"] = totalEllapsed + "ms";
    return Machinata.UI.createDebugPanel(title, data);
};



/// <summary>
///
/// </summary>
Machinata.Debugging.logAllErrorsToVisualConsole = function () {
    
    window.onerror = function (message, source, lineno, colno, error) {
        // https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/onerror
        // Prepare panel
        var errorPanelElem = $("#ma-debugging-console");
        if (errorPanelElem.length == 0) {
            errorPanelElem = $("<div id='ma-debugging-console'/>");
            errorPanelElem.css("background-color", "rgba(0,0,0,0.8)");
            errorPanelElem.css("position", "fixed");
            errorPanelElem.css("top", "0px");
            errorPanelElem.css("left", "0px");
            errorPanelElem.css("width", "100%");
            errorPanelElem.css("height", "auto");
            errorPanelElem.css("max-height", "120px");
            errorPanelElem.css("overflow-y", "scroll");
            errorPanelElem.css("z-index", "100000");
            errorPanelElem.click(function () { errorPanelElem.hide(); });
            $("body").append(errorPanelElem);
        }
        errorPanelElem.show();
        // Create message
        var errorMessageElem = $("<div/>");
        errorMessageElem.css("padding", "4px");
        errorMessageElem.append($("<div/>").css("font-size", "10px").css("color", "white").text(message));
        errorMessageElem.append($("<div/>").css("font-size", "10px").css("color", "white").text(source + ", line " + lineno + ":" + colno));
        errorPanelElem.prepend(errorMessageElem);
    };
};
