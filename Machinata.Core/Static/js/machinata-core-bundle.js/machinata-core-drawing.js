

/// <summary>
/// Collection of powerful drawing tools.
/// </summary>
/// <type>namespace</type>
Machinata.Drawing = {};


/// <summary>
/// Provides a mechanism for automatically rendering a draw loop 
/// using the browsers builtin callbacks.
/// The renderer provides functionality for bookkeeping (fps) as well
/// as start/stop/pause.
/// </summary>
Machinata.Drawing.createRenderer = function (canvasElem, renderCallback) {


    var renderer = {
        enabled: false,
        canvasElem: canvasElem,
        canvas: canvasElem[0],
        lastRenderTime: new Date().getTime(),
        lastDebugTime: new Date().getTime(),
        fps: 0,
        showStats: false,
        stats: "initializing...",

        render: function () {
            var nowTime = new Date().getTime();
            render(nowTime);
        },
        start: function () {
            this.enabled = true;
            window.requestAnimationFrame(animate);
        },
        stop: function () {
            this.enabled = false;
        },
        debugStepOnClick: function () {
            this.enabled = false;
            var step = 0;
            canvasElem.click(function () {
                step += 1000 / 60;
                console.log("render step", step);
                render(step); 
            });
            render(step); 
        },
    };

    function render(time) {
        // Get drawing context
        var ctx = renderer.canvas.getContext("2d");
        // Save/resrotre (we provide this to the callback)
        ctx.save(); {
            // Call callback
            renderCallback(ctx, time, renderer);
        } ctx.restore();
        // Stats?
        if (renderer.showStats == true) {
            Machinata.Drawing.debugText(ctx, renderer.stats, 10, 10+3, false);
        }
    }

    function animate(time) {
        // Bookkeeping
        var nowTime = new Date().getTime();
        renderer.deltaRenderTime = nowTime - renderer.lastRenderTime;
        renderer.lastRenderTime = nowTime;
        renderer.fps = Math.round(1000 / renderer.deltaRenderTime);
        if (nowTime - renderer.lastDebugTime > 1000) {
            renderer.lastDebugTime = nowTime;
            renderer.stats = renderer.fps + " fps @ " + renderer.canvas.width+"x"+renderer.canvas.height+" (" + time + ")";
            //if (Machinata.DebugEnabled == true) console.log(renderer.stats);
        }
        // Re-render
        if (renderer.enabled == true) {
            render(time);
            window.requestAnimationFrame(animate);
        }
    }
    

    return renderer;
};

/// <summary>
/// </summary>
Machinata.Drawing.createRendererCursor = function (renderer, opts) {
    // Opts
    if (opts == null) opts = {};
    if (opts.containerElem == null) opts.containerElem = renderer.canvasElem;
    if (opts.mode == null) opts.mode = "raw";
    if (opts.followerSlowDown == null) opts.followerSlowDown = 10;
    if (opts.followerMinDist == null) opts.followerMinDist = 0.5;
    if (opts.velocityDrag == null) opts.velocityDrag = null;
    if (opts.springGravity == null) opts.springGravity = 0;
    if (opts.springMass == null) opts.springMass = 30;
    if (opts.springDamping == null) opts.springDamping = 8;
    if (opts.springK == null) opts.springK = 2;
    if (opts.springTimeStep == null) opts.springTimeStep = 0.30;
    if (opts.jiggleOnNoMove == null) opts.jiggleOnNoMove = false;
    if (opts.scale == null) opts.scale = 1;
    // Init
    var cursor = {
        x: renderer.canvas.width / 2,
        y: renderer.canvas.height / 2,
        velX: 0,
        velY: 0,
        deltaVelX: 0,
        deltaVelY: 0,
        userMoved: false,
    };
    cursor.input = {
        x: cursor.x,
        y: cursor.y,
    };
    cursor.lastInput = {
        x: 0,
        y: 0,
    };
    
    renderer.cursor = cursor;
    // Update
    cursor.update = function (ctx, time, renderer) {
        var dt = renderer.deltaRenderTime;
        if (dt == null || dt == 0) dt = 1000 * (1 / 60);
        var ix = cursor.input.x;
        var iy = cursor.input.y;
        var cx = cursor.x;
        var cy = cursor.y;
        var cx2 = cx;
        var cy2 = cy;
        if (cursor.vx == null) cursor.vx = 0;
        if (cursor.vy == null) cursor.vy = 0;
        if (opts.mode == "raw") {
            cx2 = ix;
            cy2 = iy;
        } else if (opts.mode == "follower") {
            var dist = Machinata.Math.vec2Dist(ix, iy, cx, cy);
            var angle = Math.atan2(iy - cy, ix - cx);
            if (dist > opts.followerMinDist) {
                cx2 = cx + Math.cos(angle) * dist / opts.followerSlowDown;
                cy2 = cy + Math.sin(angle) * dist / opts.followerSlowDown;
            }
        } else if (opts.mode == "spring") {
            var gravity = opts.springGravity;
            var mass = opts.springMass;
            var k = opts.springK;
            var damping = opts.springDamping;
            var timeStep = opts.springTimeStep;// 0.30; //dt;
            var anchorX = ix;
            var anchorY = iy;
            var positionX = cx;
            var positionY = cy;
            var springForceY = -k * (positionY - anchorY);
            var springForceX = -k * (positionX - anchorX);
            var dampingForceY = damping * cursor.vy;
            var dampingForceX = damping * cursor.vx;
            var forceY = springForceY + mass * gravity - dampingForceY;
            var forceX = springForceX - dampingForceX;
            var accelerationY = forceY / mass;
            var accelerationX = forceX / mass;
            cursor.vy = cursor.vy + accelerationY * timeStep;
            cursor.vx = cursor.vx + accelerationX * timeStep;
            positionY = positionY + cursor.vy * timeStep;
            positionX = positionX + cursor.vx * timeStep;
            cx2 = positionX;
            cy2 = positionY;
        } else if (opts.mode == "orbit") {
            var cursorMass = 500.0;
            var dist = Machinata.Math.vec2Dist(ix, iy, cx, cy);
            var angle = Math.atan2(iy - cy, ix - cx);
            var calculationSpeed = 1.0;
            if (dist > 5) {
                cursor.deltaVelX += (
                    Math.cos(angle) *
                    (cursorMass / Math.pow(dist, 2))
                );
                cursor.deltaVelY += (
                    Math.sin(angle) *
                    (cursorMass / Math.pow(dist, 2))
                );
            }
            cursor.velX += cursor.deltaVelX * calculationSpeed;
            cursor.velY += cursor.deltaVelY * calculationSpeed;
            // Drag
            opts.velocityDrag = 0.05;
            // Limit
            var limit = 10;
            if (cursor.velX > 0) cursor.velX = Math.min(cursor.velX, limit);
            else if (cursor.velX < 0) cursor.velX = -Math.min(-cursor.velX, limit);
            if (cursor.velY > 0) cursor.velY = Math.min(cursor.velY, limit);
            else if (cursor.velY < 0) cursor.velY = -Math.min(-cursor.velY, limit);

            cx2 += cursor.velX * calculationSpeed;
            cy2 += cursor.velY * calculationSpeed;

            // Reset object delta velocity
            cursor.deltaVelX = 0;
            cursor.deltaVelY = 0;
        }
        cursor.x = cx2;
        cursor.y = cy2;

        // Drag
        if (opts.velocityDrag != null) {
            if (cursor.velX > 0) cursor.velX = Math.max(0, cursor.velX - opts.velocityDrag);
            else if (cursor.velX < 0) cursor.velX = Math.min(0, cursor.velX + opts.velocityDrag);
            if (cursor.velY > 0) cursor.velY = Math.max(0, cursor.velY - opts.velocityDrag);
            else if (cursor.velY < 0) cursor.velY = Math.min(0, cursor.velY + opts.velocityDrag);
        }

        // Wall bounce?
        if (opts.wallBounce == true) {
            if (cursor.x < 0) {
                cursor.velX *= -1;
                cursor.x = 0;
            } else if (cursor.x > renderer.canvas.width) {
                cursor.velX *= -1;
                cursor.x = renderer.canvas.width;
            }
            if (cursor.y < 0) {
                cursor.velY *= -1;
                cursor.y = 0;
            } else if (cursor.y > renderer.canvas.height) {
                cursor.velY *= -1;
                cursor.y = renderer.canvas.height;
            }
        }

        // Jiggle on no user movement?
        if (opts.jiggleOnNoMove == true && cursor.userMoved == false) {
            if (cursor.originalX == null) cursor.originalX = cursor.x;
            if (cursor.originalY == null) cursor.originalY = cursor.y;
            cursor.x = cursor.originalX + Math.cos(time*0.004) * 10;
            cursor.y = cursor.originalY + Math.sin(time * 0.004) * 10;
        }

        // Update last positions
        cursor.didMove = false;
        cursor.lastInput = { x: cursor.input.x, y: cursor.input.y };
    };
    // Debug
    cursor.debug = function (ctx) {
        // Render cursor
        var cursorSize = 6;
        ctx.fillStyle = "red";
        ctx.fillRect(cursor.input.x - cursorSize / 2, cursor.input.y - cursorSize / 2, cursorSize, cursorSize);
        ctx.fillStyle = "blue";
        ctx.fillRect(cursor.x - cursorSize / 2, cursor.y - cursorSize / 2, cursorSize, cursorSize);
    };
    // Update input
    cursor.updateInput = function (cx, cy) {
        cursor.input.x = cx * opts.scale;
        cursor.input.y = cy * opts.scale;
        cursor.didMove = true;
        cursor.userMoved = true;
    };
    // Bind events
    opts.containerElem.on("mousemove", function (event) {
        var cx = event.pageX - $(this).offset().left;
        var cy = event.pageY - $(this).offset().top;
        cursor.updateInput(cx, cy);
    });
    opts.containerElem.on("touchmove", function (event) {
        // On mobile layout, we need to be able to scroll the page, so we only allow touches if 
        // not in top or lower percentile of screen
        var touch = event.changedTouches[0];
        var cx = touch.pageX - $(this).offset().left;
        var cy = touch.pageY - $(this).offset().top;
        cursor.updateInput(cx, cy);
    });
    // Return
    return cursor;
};

/// <summary>
/// 
/// </summary>
Machinata.Drawing.debugText = function (ctx, text, x, y, centered) {
    ctx.save();
    var fontSize = 10;
    var padding = 3;
    var prevComp = ctx.globalCompositeOperation; 
    ctx.globalCompositeOperation = "source-over"; 
    ctx.font = fontSize+"px Arial";
    var metrics = ctx.measureText(text);
    if (x == null) x = 10;
    if (y == null) y = 10;
    if (centered == null) centered = true;
    if (centered == true) x = x - metrics.width / 2;
    // BG
    ctx.fillStyle = "rgba(0,0,0,0.5)";
    ctx.fillRect(
        x - padding,
        y - fontSize ,
        metrics.width + padding + padding,
        fontSize + padding
    );
    // Text
    ctx.fillStyle = "white";
    ctx.fillText(text, x, y);
    ctx.globalCompositeOperation = prevComp; 
    ctx.restore();
};



/// <summary>
/// 
/// </summary>
Machinata.Drawing.cardinalSplinePathForPoints = function (ctx, points, tension, isClosed, numOfSegments) {
    // See https://en.wikipedia.org/wiki/Cubic_Hermite_spline#Cardinal_spline
    // See https://github.com/gdenisov/cardinal-spline-js/blob/master/src/curve_func.js

    // options or defaults
    tension = (typeof tension === 'number') ? tension : 0.5;
    numOfSegments = numOfSegments ? numOfSegments : 25;

    var pts,									// for cloning point array
        i = 1,
        l = points.length,
        rPos = 0,
        rLen = (l - 2) * numOfSegments + 2 + (isClosed ? 2 * numOfSegments : 0),
        res = new Float32Array(rLen),
        cache = new Float32Array((numOfSegments + 2) * 4),
        cachePtr = 4;

    pts = points.slice(0);

    if (isClosed) {
        pts.unshift(points[l - 1]);				// insert end point as first point
        pts.unshift(points[l - 2]);
        pts.push(points[0], points[1]); 		// first point as last point
    }
    else {
        pts.unshift(points[1]);					// copy 1. point and insert at beginning
        pts.unshift(points[0]);
        pts.push(points[l - 2], points[l - 1]);	// duplicate end-points
    }

    // cache inner-loop calculations as they are based on t alone
    cache[0] = 1;								// 1,0,0,0

    for (; i < numOfSegments; i++) {

        var st = i / numOfSegments,
            st2 = st * st,
            st3 = st2 * st,
            st23 = st3 * 2,
            st32 = st2 * 3;

        cache[cachePtr++] = st23 - st32 + 1;	// c1
        cache[cachePtr++] = st32 - st23;		// c2
        cache[cachePtr++] = st3 - 2 * st2 + st;	// c3
        cache[cachePtr++] = st3 - st2;			// c4
    }

    cache[++cachePtr] = 1;						// 0,1,0,0

    // calc. points
    parse(pts, cache, l);

    if (isClosed) {
        //l = points.length;
        pts = [];
        pts.push(points[l - 4], points[l - 3], points[l - 2], points[l - 1]); // second last and last
        pts.push(points[0], points[1], points[2], points[3]); // first and second
        parse(pts, cache, 4);
    }

    function parse(pts, cache, l) {

        for (var i = 2, t; i < l; i += 2) {

            var pt1 = pts[i],
                pt2 = pts[i + 1],
                pt3 = pts[i + 2],
                pt4 = pts[i + 3],

                t1x = (pt3 - pts[i - 2]) * tension,
                t1y = (pt4 - pts[i - 1]) * tension,
                t2x = (pts[i + 4] - pt1) * tension,
                t2y = (pts[i + 5] - pt2) * tension;

            for (t = 0; t < numOfSegments; t++) {

                var c = t << 2, //t * 4;

                    c1 = cache[c],
                    c2 = cache[c + 1],
                    c3 = cache[c + 2],
                    c4 = cache[c + 3];

                res[rPos++] = c1 * pt1 + c2 * pt3 + c3 * t1x + c4 * t2x;
                res[rPos++] = c1 * pt2 + c2 * pt4 + c3 * t1y + c4 * t2y;
            }
        }
    }

    // add last point
    l = isClosed ? 0 : points.length - 2;
    res[rPos++] = points[l];
    res[rPos] = points[l + 1];

    // add lines to path
    for (i = 0, l = res.length; i < l; i += 2)
        ctx.lineTo(res[i], res[i + 1]);

    return res;
};


/// <summary>
/// Draws a rounded rectangle using the current state of the canvas.
/// @param {CanvasRenderingContext2D} ctx
/// @param {Number} x The top left x coordinate
/// @param {Number} y The top left y coordinate
/// @param {Number} width The width of the rectangle
/// @param {Number} height The height of the rectangle
/// @param {Number} [radius = 5] The corner radius; It can also be an object
///                 to specify different radii for corners
/// @param {Number} [radius.tl = 0] Top left
/// @param {Number} [radius.tr = 0] Top right
/// @param {Number} [radius.br = 0] Bottom right
/// @param {Number} [radius.bl = 0] Bottom left
/// @param {Boolean} [fill = false] Whether to fill the rectangle.
/// @param {Boolean} [stroke = true] Whether to stroke the rectangle.
/// </summary>
Machinata.Drawing.roundedRect = function(ctx, x, y, width, height, radius) {
    // See https://stackoverflow.com/questions/1255512/how-to-draw-a-rounded-rectangle-on-html-canvas
    if (typeof radius === 'undefined') {
        radius = 5;
    }
    if (typeof radius === 'number') {
        radius = { tl: radius, tr: radius, br: radius, bl: radius };
    } else {
        var defaultRadius = { tl: 0, tr: 0, br: 0, bl: 0 };
        for (var side in defaultRadius) {
            radius[side] = radius[side] || defaultRadius[side];
        }
    }
    //ctx.beginPath();
    ctx.moveTo(x + radius.tl, y);
    ctx.lineTo(x + width - radius.tr, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
    ctx.lineTo(x + width, y + height - radius.br);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
    ctx.lineTo(x + radius.bl, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
    ctx.lineTo(x, y + radius.tl);
    ctx.quadraticCurveTo(x, y, x + radius.tl, y);
    //ctx.closePath();
}