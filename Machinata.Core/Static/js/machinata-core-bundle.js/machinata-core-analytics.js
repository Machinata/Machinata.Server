

/// <summary>
/// Utilities for page analytics.
/// </summary>
/// <type>namespace</type>
Machinata.Analytics = {};

/// <summary>
/// If true, analytics will be enabled (as well as tracking events like start or registerEvent).
/// </summary>
Machinata.Analytics.enabled = false;

/// <summary>
/// Holds the options used by Machinata.Analytics.configure
/// </summary>
Machinata.Analytics.opts = null;

/// <summary>
/// If true, indicates that analytics tracking has started
/// </summary>
Machinata.Analytics.isStarted = false;

/// <summary>
/// Holds the reference to the Google Analytics gtag.js gtag function
/// </summary>
Machinata.Analytics.googleAnalyticsTracker = null;

/// <summary>
/// Configures analytics using the options.
/// Options:
///  - enabled: indicates whether or not analytics is enabled
///  - autoStart: if true, tracking automatically starts
///  - googleAnalytics4ID: the Google Analytics 4 tracking id (starts with G-1234456)
///  - googleAnalyticsUniversalID: the Google Analytics Universal tracking id (starts with UA-1234456) (deprecated)
/// </summary>
Machinata.Analytics.configure = function (opts) {
    // Init
    if (opts == null) opts = {};
    if (opts.enabled == null) opts.enabled = true;
    if (opts.enabled == "true") opts.enabled = true;
    if (opts.enabled == "false") opts.enabled = false;
    if ($("body").hasClass("no-analytics")) opts.enabled = false;
    if (opts.autoStart == null) opts.autoStart = true;
    if (opts.googleAnalytics4ID == null) opts.googleAnalytics4ID = null;
    if (opts.googleAnalyticsUniversalID == null) opts.googleAnalyticsUniversalID = null;
    if (opts.googleTagManagerID == null) opts.googleTagManagerID = null;
    if (opts.hubSpotID == null) opts.hubSpotID = null;
    if (opts.hotJarID == null) opts.hotJarID = null;
    if (opts.trackingConsentRequired == null) opts.trackingConsentRequired = true;
    Machinata.Analytics.opts = opts;
    Machinata.Analytics.enabled = opts.enabled;
    // Validate
    if (opts.enabled == false) return;
    if (opts.googleAnalyticsUniversalID != null) {
        if ((new Date() > new Date(2023, 6, 1))) { // ensure we only track up until cutoff date of GA UA
            opts.googleAnalyticsUniversalID = null; // no longer supported
        }
    }
    // Google Products
    {
        // Helper functions
        _googleAnalyticsIsSetup = false;
        function setupGoogleAnalytics() {
            // Gate
            if (_googleAnalyticsIsSetup == true) return;
            _googleAnalyticsIsSetup = true;
            // Setup
            window.dataLayer = window.dataLayer || [];
            window.gtag = function () { dataLayer.push(arguments); }
            Machinata.Analytics.googleAnalyticsTracker = window.gtag;
            Machinata.Analytics.googleAnalyticsTracker('js', new Date());
            if (opts.trackingConsentRequired == true) {
                // Set the default to denied for now
                // https://developers.google.com/tag-platform/security/guides/consent?consentmode=advanced#default-consent
                Machinata.Analytics.googleAnalyticsTracker('consent', 'default', {
                    'ad_storage': 'denied',
                    'ad_user_data': 'denied',
                    'ad_personalization': 'denied',
                    'analytics_storage': 'denied'
                });
            }
        }
        // Register
        if (opts.googleAnalyticsUniversalID != null) {
            setupGoogleAnalytics();
            Machinata.Analytics.googleAnalyticsTracker('config', opts.googleAnalyticsUniversalID, { 'anonymize_ip': true });
        }
        if (opts.googleAnalytics4ID != null) {
            setupGoogleAnalytics();
            Machinata.Analytics.googleAnalyticsTracker('config', opts.googleAnalytics4ID, { 'anonymize_ip': true });
        }
        if (opts.googleTagManagerID != null) {
            // Google Tag Manager JS documentation:
            // https://developers.google.com/tag-platform/gtagjs/configure
            // Consent mode documentation:
            // https://support.google.com/tagmanager/answer/14546213?sjid=767643099451691954-EU
            setupGoogleAnalytics();
            // The gtm.start is from the standard GTM snippet
            window.dataLayer.push({
                'gtm.start': new Date().getTime(), event: 'gtm.js'
            });
            Machinata.Analytics.googleAnalyticsTracker('config', opts.googleTagManagerID, { 'anonymize_ip': true });
        }
    }

    // Startup?
    if (opts.autoStart == true) Machinata.Analytics.start();
};

/// <summary>
/// Starts the analytics tracking.
/// Note: if Machinata.Analytics.configure is called with ```autoStart: true```, then start is automatically called.
/// ## Opt-In Tracking
/// If tracking should only be started via opt-in, then start should be manually called. For example:
/// ```
/// Machinata.Privacy.registerOption("analytical_cookies", "cookie", "checkbox", true, false, null, "{text.privacy.analytical-cookies.title}", "{text.privacy.analytical-cookies.description}", function (val, option) {
///     if (val == true) Machinata.Analytics.start();
/// });
///  ```
/// </summary>
Machinata.Analytics.start = function () {
    if (Machinata.Analytics.enabled == false) return;

    var opts = Machinata.Analytics.opts;

    // Google Products
    {
        if (opts.googleAnalytics4ID != null) {
            $("head").append("<script async src='https://www.googletagmanager.com/gtag/js'></script>");
        }
        if (opts.googleTagManagerID != null) {
            $("head").append("<script async src='https://www.googletagmanager.com/gtm.js?id=" + opts.googleTagManagerID +"'></script>");
        }
        if (Machinata.Analytics.googleAnalyticsTracker != null) {
            // Add the gtag.js script
            $("head").append("<script async src='https://www.googletagmanager.com/gtag/js'></script>");
            // Enable tracking (consent)
            // https://developers.google.com/tag-platform/security/guides/consent?consentmode=advanced#default-consent
            Machinata.Analytics.googleAnalyticsTracker('consent', 'update', {
                'ad_storage': 'granted',
                'ad_user_data': 'granted',
                'ad_personalization': 'granted',
                'analytics_storage': 'granted'
            });
        }
    }

    // HubSpot 
    if (opts.hubSpotID != null) {
        // <script type="text/javascript" id="hs-script-loader" async defer src="//js-eu1.hs-scripts.com/139640637.js"></script>
        //TODO: hubspot server js-eu1?
        $("head").append(`<script type="text/javascript" id="hs-script-loader" async defer src="//js-eu1.hs-scripts.com/${opts.hubSpotID}.js"></script>`);
    }

    // Hotjar
    if (opts.hotJarID != null) {
        //TODO: server?
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () { (h.hj.q = h.hj.q || []).push(arguments) };
            h._hjSettings = { hjid: opts.hotJarID, hjsv: 6 };
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
    }
    
    Machinata.Analytics.isStarted = true;
};


/// <summary>
/// Registers a tracking event to all trackers.
/// Language: The language setting of a user�s browser or device, displayed as the ISO 639 language code.
///           Example value: "en-us".
/// Note: this method is based on the older Universal Analytics event tracking format, not the newer GA4 custom events...
/// </summary>
Machinata.Analytics.registerEvent = function (action, category, label, value, language) {
    if (Machinata.Analytics.enabled == false) return;
    if (Machinata.Analytics.googleAnalyticsTracker != null) {
        var data = {};
        if (category != null) data['event_category'] = category;
        if (category != null) data['event_label'] = label;
        if (category != null) data['value'] = value;
        if (category != null && language != null) data['language'] = language; // default params "might" be possible to override. seems to work. https://support.google.com/analytics/answer/9234069?sjid=5099330799652040749-EU
        // Sanity
        if (action != null && action.length > 40) console.warn("Machinata.Analytics.registerEvent: event name should not be greater than 40 characters",action);
        // Track
        Machinata.Analytics.googleAnalyticsTracker('event', action, data);
    }
};


/// <summary>
/// Registers a custom tracking event to all trackers.
///
/// ## Google Analytics 4 Custom Events:
///
/// Before you create a custom event, make sure the event you want to create isn't already collected
/// through an automatically collected event(https://support.google.com/analytics/answer/9234069) or
/// recommended as a recommended event(https://support.google.com/analytics/answer/9267735).
/// It's always better to use an existing event because these events automatically populate dimensions
/// and metrics that are used in your reports.
///
/// Before you name a custom event, make sure the name adheres to the event naming rules (e.g., the
/// name is case sensitive, cannot be a reserved name, and starts with a letter) and event naming
/// limits(i.e., the name must be fewer than 40 characters in length) to ensure that Google Analytics
/// collects and processes the event.
///
/// To access the different values assigned to an event parameter in your reports, you should create
/// a custom dimension or metric.A custom dimension or metric lets you see the information you
/// collected from an event parameter.For example, if you set up a 'value' event parameter, you could
/// create a custom metric called 'Value' that allows you to see each value assigned to the event
/// parameter.Learn more about custom dimensions and metrics.
///
/// See https://support.google.com/analytics/answer/12229021?hl=en&ref_topic=13367566&sjid=16986929134949673932-EU
/// See https://developers.google.com/analytics/devguides/collection/ga4/events?client_type=gtag
/// 
/// ## Notes:
/// Language: The language setting of a user�s browser or device, displayed as the ISO 639 language code.
///           Example value: "en-us".
/// </summary>
Machinata.Analytics.registerCustomEvent = function (eventName, data, language) {
    if (Machinata.Analytics.enabled == false) return;
    if (Machinata.Analytics.googleAnalyticsTracker != null) {
        if (data == null) data = {};
        if (language != null) data['language'] = language; // default params "might" be possible to override. seems to work. https://support.google.com/analytics/answer/9234069?sjid=5099330799652040749-EU
        Machinata.Analytics.googleAnalyticsTracker('event', eventName, data);
    }
};

/// <summary>
/// Sets a new random user id (guid) for all trackers.
/// https://developers.google.com/analytics/devguides/collection/ga4/user-id?client_type=gtag
/// </summary>
Machinata.Analytics.setNewRandomUserId = function () {
    if (Machinata.Analytics.enabled == false) return;
    if (Machinata.Analytics.googleAnalyticsTracker != null) {
        let userId = MachinataJS.guid();
        Machinata.Analytics.googleAnalyticsTracker('set', { 'user_id': userId });
    }
};



/// <summary>
/// Registers a conversion to all trackers.
/// </summary>
Machinata.Analytics.registerConversion = function (adsId, conversionId) {
    if (Machinata.Analytics.enabled == false) return;
    if (Machinata.Analytics.googleAnalyticsTracker != null) {
        Machinata.Analytics.googleAnalyticsTracker('config', adsId);
        Machinata.Analytics.googleAnalyticsTracker('event', 'conversion', { 'send_to': conversionId });
    }
};