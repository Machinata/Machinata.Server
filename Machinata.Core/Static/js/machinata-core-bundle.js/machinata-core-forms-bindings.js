




/// <summary>
/// Email form binding
/// </summary>
/// <hidden/>
Machinata.onFormInputBind(function (propertyType, type, form, input) {
    if (type == "emailaddress") {
        input.attr("type", "email");
        return true;
    } 
});


/// <summary>
/// DateTime/DateRange form binding
/// </summary>
/// <hidden/>
Machinata.onFormInputBind(function (propertyType, type, form, input) {
    if (propertyType == "DateTime" || propertyType == "DateRange") {
        Machinata.debug("Creating Form Input " + propertyType + " for " + input.attr("name"));
        // Init
        input.attr("type", "text"); // Make sure the browser doesnt do it's own date field
        input.attr("format", ""); // Make sure the browser doesnt do format
        input.val(input.attr("data-property-original-value")); // Make sure the browser doesnt do format
        var isDateRange = propertyType == "DateRange";
        var format = input.attr("data-property-format");
        var dateFormat = Machinata.DEFAULT_DATE_FORMAT_JAVASCRIPT_UI;
        var timeFormat = Machinata.DEFAULT_TIME_FORMAT_JAVASCRIPT_UI;
        var formatSegs = format.split(" ");
        if (formatSegs.length == 2) {
            dateFormat = formatSegs[0];
            timeFormat = formatSegs[1];
        } else {
            if (String.isEmpty(format)) format = dateFormat + " " + timeFormat;
            else {
                if (format.indexOf("HH") != -1 || format.indexOf("mm") != -1) {
                    // Its just a time
                    dateFormat = null;
                    timeFormat = format;
                } else {
                    // Its just a date
                    dateFormat = format;
                    timeFormat = null;
                }
            }
        }
        // Date range
        if (isDateRange == true) {
            //timeFormat = null;
            //format = dateFormat;
        }
        Machinata.debug("  format: " + format);
        Machinata.debug("  dateFormat: " + dateFormat);
        Machinata.debug("  timeFormat: " + timeFormat);
        Machinata.debug("  isDateRange: " + isDateRange);
        // Convert for picker
        if (dateFormat != null) dateFormat = Machinata.convertDotNetTimeFormatToDatepickerFormat(dateFormat);
        if (timeFormat != null) timeFormat = Machinata.convertDotNetTimeFormatToDatepickerFormat(timeFormat);
        // Show date/time picker when clicking
        input.click(function () {
            Machinata.dateDialog(null, null, null, input.val(), dateFormat, timeFormat, isDateRange)
                .input(function (datetime, id) {
                    if (id == "clear") {
                        input.val("");
                        input.attr("data-property-universal-value", "");
                        return;
                    }
                    if (isDateRange == true) {
                        Machinata.debug("DateRange selected: " + datetime);
                        Machinata.debug("  start: " + datetime.start);
                        Machinata.debug("  end: " + datetime.end);
                        var utcmsStart = Machinata.UTCMillisecondsFromDate(datetime.start);
                        var utcmsEnd = Machinata.UTCMillisecondsFromDate(datetime.end);
                        Machinata.debug("  utcms start: " + utcmsStart);
                        Machinata.debug("  utcms end: " + utcmsEnd);
                        input.attr("data-property-universal-value", utcmsStart + Machinata.DEFAULT_DATERANGE_SEPARATOR + utcmsEnd);
                        input.val(Machinata.formattedDate(datetime.start, format) + Machinata.DEFAULT_DATERANGE_SEPARATOR + Machinata.formattedDate(datetime.end, format));
                    } else {
                        Machinata.debug("DateTime selected: " + datetime);
                        var utcms = Machinata.UTCMillisecondsFromDate(datetime);
                        Machinata.debug("  utcms: " + utcms);
                        input.attr("data-property-universal-value", utcms);
                        input.val(Machinata.formattedDate(datetime, format));
                    }
                })
                .show();
        });
        return true;
    }
});

/// <summary>
/// Image/Upload form binding
/// </summary>
/// <hidden/>
Machinata.onFormInputBind(function (propertyType, type, form, input) {
    if (type == "imageurl" || type == "upload") {
        input.hide();
        var autodectImages = false;
        var fileCategory = null;
        if (type == "imageurl") {
            autodectImages = true;
            fileCategory = "image";
        }
        var fileURL = input.val();
        var previewContainer = $("<div class='preview-container'/>");
        var filePreview = Machinata.Content.createFilePreview(fileURL, fileURL, autodectImages);
        filePreview.addClass("cursor-pointer");
        previewContainer.append(filePreview);
        previewContainer.append("<div class='clear'/>");
        var source = input.attr("data-content-source") || Machinata.FORMS_DEFAULT_CONTENT_UPLOAD_SOURCE;
        if (source == null || source == "") source = input.attr("data-property-parent-type"); // Fallback
        var required = input.attr("data-property-required") == "true";
        Machinata.Content.bindUploadDialogForFilePreview(filePreview, {
            type: type,
            fileSource: source,
            fileCategory: fileCategory,
            inputElem: input,
            autodectImages: autodectImages,
            required: required
        });
        input.parent().append(previewContainer);
        return true;
    }
});

/// <summary>
/// Country form binding
/// </summary>
/// <hidden/>
Machinata.onFormInputBind(function (propertyType, type, form, input) {
    if (type == "country") {
        input.hide();
        var selectElem = $("<select/>");
        var universalValue = input.attr("data-property-universal-value");
        var required = input.attr("data-property-required");
        if (required == "false") {
            var optElem = $("<option/>");
            optElem.text("");
            optElem.attr("value", "");
            selectElem.append(optElem);
        }
        var didSetVal = false;
        for (var i = 0; i < Machinata.COUNTRIES.length; i++) {
            var c = Machinata.COUNTRIES[i];
            var optElem = $("<option/>");
            optElem.text(c.name);
            optElem.attr("value", c.code.toLowerCase());
            if (c.code.toLowerCase() == input.val() || c.code.toLowerCase() == universalValue) {
                optElem.attr("selected", "selected");
                didSetVal = true;
            }
            selectElem.append(optElem);
        }
        input.parent().append(selectElem);
        /*selectElem.selectmenu();
        input.val(selectElem.val());
        selectElem.on("selectmenuchange", function () {
            input.val(selectElem.val());
        });*/
        input.val(selectElem.val());
        selectElem.on("change", function () {
            input.val(selectElem.val());
        });
        return true;
    }
});

/// <summary>
/// List/Select form binding
/// </summary>
/// <hidden/>
Machinata.onFormInputBind(function (propertyType, type, form, input) {
    if (type == "list" || type == "select") {
        input.hide();
        var selectElem = $("<select/>");
        selectElem.attr("title", input.attr("data-property-tooltip"));
        var opts = []; // array of {key:"Title",value:"Value"}
        if (input.attr("data-property-options") != null && input.attr("data-property-options") != "") {
            opts = JSON.parse(input.attr("data-property-options"));
        }
        var universalValue = input.attr("data-property-universal-value");
        var rawValue = input.attr("data-property-raw-value");
        var didSetVal = false;
        var hasCustomVal = false;
        if (input.attr("data-property-required") == "false") {
            opts.splice(0, 0, {key:"",value:""});
        }
        for (var i = 0; i < opts.length; i++) {
            var optVal = opts[i].value;
            var optTitle = opts[i].key;
            if (optTitle == null) optTitle = optVal; // use value as title
            var optElem = $("<option/>");
            optElem.text(optTitle);
            optElem.attr("value", optVal);
            if (optVal == "*") hasCustomVal = true;
            if (optVal == input.val() || optVal == universalValue || optVal == rawValue) {
                optElem.attr("selected", "selected");
                didSetVal = true;
            }
            selectElem.append(optElem);
        }
        var originalValue = input.val();
        input.parent().append(selectElem);
        input.val(selectElem.val());
        selectElem.on("change", function () {
            if (selectElem.val() == "*") {
                input.trigger("updateCustomInput");
            } else {
                input.trigger("updateCustomInput");
                input.val(selectElem.val());
                input.trigger("change");
            }
        });
        input.on("updateCustomInput", function () {
            var customElem = input.parent().find(".custom-input");
            var customElemInput = input.parent().find(".custom-input input");
            if (selectElem.val() == "*") {
                // Show the custom field, also updaet the current value to it...
                input.val(customElemInput.val());
                customElem.show();
                if (input.attr("data-property-required") == "true") {
                    customElemInput.attr("required", "required");
                }
            } else {
                customElem.hide();
                if (input.attr("data-property-required") == "true") {
                    customElemInput.attr("required", null);
                }
            }
        });

        if (hasCustomVal == true) {
            // Setup the custom ui
            var customElem = $("<div class='custom-input'><input type='text'/></div>");
            var customElemInput = customElem.find("input");
            customElemInput.on("change", function () {
                input.val($(this).val());
            });
            if (didSetVal == false) {
                customElemInput.val(originalValue);
            }
            customElemInput.attr("title", input.attr("data-property-tooltip"));
            customElemInput.attr("placeholder", input.attr("data-property-placeholder"));
            customElem.hide(); // prevent flicker
            input.parent().append(customElem);
            input.trigger("updateCustomInput");
        }
        return true;
    } 
});

/// <summary>
/// Toggle/Bool/Selectable form binding
/// </summary>
/// <hidden/>
Machinata.onFormInputBind(function (propertyType, type, form, input) {
    if (type == "toggle" || type == "bool" || type == "radiolist" || type == "checkboxlist") {
        input.hide();
        var buttonsElem = $("<div class='buttons bb-button-set'>");
        var opts = []; // array of {key:"Title",value:"Value"}
        if (input.attr("data-property-options") != null && input.attr("data-property-options") != "") {
            opts = JSON.parse(input.attr("data-property-options"));
        }
        if (input.attr("data-property-options-titles") != null) optsTitles = input.attr("data-property-options-titles").split(",");
        var inputType = "radio";
        var vals = input.val().split(",");
        if (type == "checkboxlist") inputType = "checkbox";
        for (var i = 0; i < opts.length; i++) {
            var opt = opts[i];
            var optVal = opts[i].value;
            var optTitle = opts[i].key;
            if (optTitle == null) optTitle = optVal; // use value as title
            var optElem = $("<input class='bb-" + inputType + "'/>");
            optElem.attr("value", optVal)
            optElem.attr("label", optTitle);
            optElem.attr("name", input.attr("name") + "_opt");
            optElem.attr("type", inputType);
            for (var ii = 0; ii < vals.length; ii++) {
                if (optVal == vals[ii]) {
                    optElem.attr("selected", "selected");
                    optElem.addClass("selected");
                }
            }
            optElem.click(function (e) {
                // Toggle UI
                var newVal = "";
                buttonsElem.find("input").each(function () {
                    if ($(this).is(':checked')) {
                        if (newVal != "") newVal += ",";
                        newVal += $(this).attr("value");
                    }
                });
                input.val(newVal);
                input.trigger("change");
            });
            buttonsElem.append(optElem);
        }
        input.parent().append(buttonsElem);
        Machinata.UI.bind(buttonsElem.parent());
        return true;
    }
});

/// <summary>
/// Properties form binding
/// </summary>
/// <hidden/>
Machinata.onFormInputBind(function (propertyType, type, form, input) {
    if (propertyType == "Properties") {
        var rawJSON = input.attr("data-property-raw-value");
        if (String.isEmpty(rawJSON)) {
            rawJSON = "{}";
        }
        var json = JSON.parse(rawJSON);
        var keys = Object.keys(json);
        keys.sort();
        // Table form or newer ui-form-row?
        if (input.parent().parent().hasClass("ui-form-row")) {
            var originalRow = input.closest(".ui-form-row");
            for (i = 0; i < keys.length; i++) {
                var key = keys[i];
                var val = json[key];
                var newRow = originalRow.clone();
                newRow.find(".ui-label").text(key.toSentence());
                var newInput = newRow.find(".ui-input input");
                newInput.val(val);
                newInput.attr("name", input.attr("name") + "_" + key);
                newInput.attr("properties-field", input.attr("name"));
                newInput.attr("properties-key", key);
                if (typeof (val) == "number") {
                    newInput.attr("type", "number");
                    newInput.attr("data-property-type", "Integer");
                } else if (typeof (val) == "boolean") {
                // Boolean?
                    newInput.attr("data-property-type", "Boolean");
                    var buttonsElem = $("<div class='buttons bb-button-set'>");
                    var opts = ["False","True"];
                    var optsTitles = ["{text.no}", "{text.yes}"];
                    for (var ii = 0; ii < opts.length; ii++) {
                        var opt = opts[ii];
                        var title = optsTitles[ii];
                        var optElem = $("<input class='bb-radio'/>");
                        optElem.attr("value", opt)
                        optElem.attr("label", title);
                        optElem.attr("name", newInput.attr("name") + "_opt");
                        optElem.attr("type", "radio");
                        if (opt.toLowerCase() == val.toString().toLowerCase()) {
                            optElem.attr("selected", "selected");
                            optElem.addClass("selected");
                        }
                        optElem.click(function (e) {
                            var newVal = "";
                            var inputToUse = $(this).closest(".ui-input").children("input").first();
                            $(this).parent().find("input").each(function () {
                                if ($(this).is(':checked')) {
                                    if (newVal != "") newVal += ",";
                                    newVal += $(this).attr("value");
                                }
                            });
                            inputToUse.val(newVal);
                            inputToUse.trigger("change");
                        });
                        buttonsElem.append(optElem);
                    }
                    newInput.parent().append(buttonsElem);
                    newInput.hide();
                }
                newRow.insertBefore(originalRow);
                
            }
            originalRow.hide();
        } else {
            // Just plain table row (tr)
            var originalRow = input.closest("tr");
            for (i = 0; i < keys.length; i++) {
                var key = keys[i];
                var val = json[key];
                var newRow = originalRow.clone();
                newRow.find("td.label").text(key.toSentence());
                var newInput = newRow.find("td.input input");
                newInput.val(val);
                newInput.attr("name", input.attr("name") + "_" + key);
                newInput.attr("properties-field", input.attr("name"));
                newInput.attr("properties-key", key);
                newRow.insertBefore(originalRow);
            }
            originalRow.hide();
        }
        return true;
    }
});

/// <summary>
/// ContentNode form binding
/// </summary>
/// <hidden/>
Machinata.onFormInputBind(function (propertyType, type, form, input) {
    if (propertyType == "ContentNode") {
        input.hide();
        // The content node type is handled by the template
        return true;
    }
});

/// <summary>
/// TranslatableText form binding
/// </summary>
/// <hidden/>
Machinata.onFormInputBind(function (propertyType, type, form, input) {
    if (propertyType == "TranslatableText") {
        input.hide();
        // Init
        var rawJSON = input.attr("data-property-raw-value");
        var customFormat = input.attr("data-property-format");
        var isRichText = customFormat == "html";
        var isMultiLineText = customFormat == "multilinetext";
        var isFile = (customFormat == "upload" || customFormat == "image");
        if (String.isEmpty(rawJSON)) {
            rawJSON = "{}";
        }
        var fileSource = input.attr("data-content-source") || Machinata.FORMS_DEFAULT_CONTENT_UPLOAD_SOURCE;
        if (fileSource == null || fileSource == "") fileSource = input.attr("data-property-parent-type"); // Fallback
        // Parse out json
        var json = JSON.parse(rawJSON);
        // Initial decode? (not needed, as it is always stored decoded...)
        /*if (json["uri-encoded"] == true) {
            var newJSON = {};
            var keys = Object.keys(json);
            for (var i = 0; i < keys.length; i++) {
                var key = keys[i];
                newJSON[key] = decodeURIComponent(json[key]);
            }
            json = newJSON;
        }*/
        // Encoding helper function
        function encodeJSONAsString(json) {
            if (isRichText == true) {
                // Make sure to URI encode, otherwise the sever will reject the request...
                var newJSON = {};
                var keys = Object.keys(json);
                for (var i = 0; i < keys.length; i++) {
                    var key = keys[i];
                    if (json[key] != null) {
                        newJSON[key] = encodeURIComponent(json[key]);
                    }
                }
                newJSON["uri-encoded"] = true;
                return JSON.stringify(newJSON);
            } else {
                return JSON.stringify(json);
            }
        }
        // Set input
        input.val(encodeJSONAsString(json));
        // Build UI for translatable text
        Machinata.Content.buildTranslatableTextUI(input.parent(), json, {
            richText: isRichText,
            multiLineText: isMultiLineText,
            file: isFile,
            fileSource: fileSource,
            fileType: customFormat,
            dataChangedCallback: function (json) {
                input.val(encodeJSONAsString(json));
            }
        });
        return true;
    }
});

/// <summary>
/// Multiline Text form binding
/// </summary>
/// <hidden/>
Machinata.onFormInputBind(function (propertyType, type, form, input) {
    if (type == "multilinetext") {
        //input.hide();
        var textareaElem = $("<textarea></textarea>");
        textareaElem.attr("name", input.attr("name"));
        input.parent().append(textareaElem);
        textareaElem.val(input.attr("data-property-raw-value"));
        input.remove();
        return true;
    }
});


/// <summary>
/// LatLon form binding
/// </summary>
/// <hidden/>
Machinata.onFormInputBind(function (propertyType, type, form, input) {
    if (type == "latlon") {
        input.val(input.attr("data-property-raw-value"));
        input.on("click", function () {
            var opts = {
                address: input.val()
            }
            Machinata.locationDialog("{text.maps.coordinates}", null, opts)
                .input(function (val, id) {
                    if (id == "okay") {
                        input.val(val);
                    }
                    else if (id == "clear") {
                        input.val("");
                    }
                });
        });

        return true;
    }
});


/// <summary>
/// Range form binding
/// </summary>
/// <hidden/>
Machinata.onFormInputBind(function (propertyType, type, form, input) {
    if (type == "range") {
        // Alter and create ui...
        input.attr("type", "number");
        var rangeInput = $("<input type='range' class='ui-range'/>");
        rangeInput.attr("value", input.attr("value"));
        rangeInput.attr("min", input.attr("min"));
        rangeInput.attr("max", input.attr("max"));
        rangeInput.attr("step", input.attr("step"));
        var rangeInputWrapper = $("<div></div>").append(rangeInput);
        rangeInputWrapper.insertAfter(input);
        var numberInput = input;
        // Change bindings
        numberInput.on("change", function () {
            rangeInput.val(input.val());
        });
        rangeInput.on("change", function () {
            numberInput.val(rangeInput.val());
        });
        rangeInput.on("input", function () {
            numberInput.val(rangeInput.val());
        });
        return true;
    }
    else if (propertyType == "IntRange") {

        // Helper function
        function getValuesFromString(str) {
            // Parse out the str 'A - B', where A and B can be flaots
            var regex = new RegExp('([-+]?[0-9]*\.?[0-9]+) ?- ?([-+]?[0-9]*\.?[0-9]+)', 'gm'); // see https://regex101.com/r/QuGm0A/1
            var matches = regex.exec(str);
            if (matches == null || matches.length != 3) throw "IntRange invalid value "+str;
            // Parse the numbers
            var valA = parseFloat(matches[1].trim());
            var valB = parseFloat(matches[2].trim());
            if (isNaN(valA)) throw "IntRange invalid value A";
            if (isNaN(valB)) throw "IntRange invalid value B";
            return [valA, valB];
        }

        // Alter and create ui...
        var rangeSliderElem = $("<div class='ui-input-wrapper'><div class='ui-ab-range'/></div>");
        var originalValues = getValuesFromString(input.val());

        // Init slider options
        var lastGoodValues = originalValues;
        var sliderOpts = {};
        sliderOpts.range = true;
        sliderOpts.values = originalValues;
        sliderOpts.slide = function (event, ui) {
            input.val(ui.values[0] + " - " + ui.values[1]);
            lastGoodValues = ui.values;
        };
        sliderOpts.min = parseFloat(input.attr("data-property-min-value"));
        sliderOpts.max = parseFloat(input.attr("data-property-max-value"));
        if (isNaN(sliderOpts.min)) sliderOpts.min = 0;
        if (isNaN(sliderOpts.max)) sliderOpts.max = null;

        // Create jQ UI slider...
        rangeSliderElem.find(".ui-ab-range").slider(sliderOpts);
        rangeSliderElem.insertAfter(input);

        // Bind input change so that its two way...
        input.on("change", function () {
            try {
                var newValues = getValuesFromString(input.val());
                if (sliderOpts.min != null && newValues[0] < sliderOpts.min) newValues[0] = sliderOpts.min; //throw "invalid A val (less than min)";
                if (sliderOpts.min != null && newValues[1] < sliderOpts.min) newValues[1] = sliderOpts.min; //throw "invalid B val (less than min)";
                if (sliderOpts.max != null && newValues[0] > sliderOpts.max) newValues[0] = sliderOpts.max; //throw "invalid A val (greater than max)";
                if (sliderOpts.max != null && newValues[1] > sliderOpts.max) newValues[1] = sliderOpts.max; //throw "invalid B val (greater than max)";
                if (newValues[0] > newValues[1]) newValues[0] = newValues[1];//throw "invalid A val (greater than B)";
                if (newValues[1] < newValues[0]) newValues[1] = newValues[0];// throw "invalid B val (greater than A)";
                rangeSliderElem.find(".ui-ab-range").slider("values", 0, newValues[0]);
                rangeSliderElem.find(".ui-ab-range").slider("values", 1, newValues[1]);
                lastGoodValues = newValues;
                input.val(lastGoodValues[0] + " - " + lastGoodValues[1]);
            } catch (e) {
                console.warn(e);
                input.val(lastGoodValues[0] + " - " + lastGoodValues[1]);
            }
        });

        return true;
    }
});



/// <summary>
/// emails adress book form binding
/// </summary>
/// <hidden/>

/// <summary>
/// 
/// </summary>
Machinata.onFormInputBind(function (propertyType, type, form, input) {
    if (type == "addressbook") {
        input.on("click", function () {
            var diag = Machinata.entityListingDialogForAPICall("{text.address-book-dialog.title}", "{text.address-book-dialog.message}", "/api/admin/address/address-book/search", {
                entitiesKey: "addresses",
                entityIdKey: "email",
                multipleSelection: true,
                selectedEntityId: input.val(),
                returnEntityIds: true,
                search: true,
                searchOnKeyStroke: true,
                allowClear: true,
                properties: [{ key: "name", title: "name" }, { key: "email", title: "email" }],
                requireSearchForResults: false
            });
            diag.input(function (val, id) {
                if (id == "okay") input.val(val);
                else if (id == "clear") input.val("");
            });
        });

        return true;
    }
});






/// <summary>
/// HTML string input
/// </summary>
/// <hidden/>
Machinata.onFormInputBind(function (propertyType, type, form, input) {
    if (propertyType == "String" && input.attr("data-property-format") == "html") {
        // Modify original input
        input.hide();
        // Create HTML input UI
        var containerElem = $('<div class="ui-content-editor"><div class="content-node type-html"><div class="content-value page-contents ui-standard-input-element"></div></div></div>').insertAfter(input);;
        var htmlInput = $('<div class="translation-value translation-input" contenteditable="true"/>').appendTo(containerElem.find(".content-value"));
        htmlInput.html(decodeURIComponent(input.val()));
        htmlInput.notebook({
            autoFocus: false,
            placeholder: null,
            mode: 'multiline', // multiline or inline
            //modifiers: ['bold', 'italic', 'underline', 'h1', 'h2', 'ol', 'ul', 'anchor','image']
        });
        // Bind input change
        htmlInput.on('contentChange', function () {
            var html = htmlInput.html();
            html = html.replaceAll("<p> </p>", "");
            html = html.replaceAll("<h1> </h1>", "");
            html = html.replaceAll("<h2> </h2>", "");
            var encoded = encodeURIComponent(html);
            input.val(encoded);
        });
        return true;
    }
});