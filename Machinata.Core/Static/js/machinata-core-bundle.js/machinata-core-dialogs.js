


/// <summary>
/// Library for displaying dialogs of all sorts.
/// </summary>
/// <type>namespace</type>
Machinata.Dialogs = {};

/// <summary>
/// 
/// </summary>
Machinata.Dialogs.MAX_DIALOG_WIDTH = 420;

/// <summary>
/// 
/// </summary>
Machinata.Dialogs.MAX_DIALOG_WIDTH_WIDE = 560;

/// <summary>
/// 
/// </summary>
Machinata.Dialogs.DIALOG_ADDITIONAL_CLASSES = null;

/// <summary>
/// 
/// </summary>
Machinata.Dialogs.DEFAULT_API_CALL_CONTENT_FILES_CREATE = "/api/admin/content/files/create";

/// <summary>
/// 
/// </summary>
Machinata.Dialogs.DEFAULT_API_CALL_CONTENT_PAGES_LIST = "/api/admin/content/pages/list";

/// <summary>
/// 
/// </summary>
Machinata.Dialogs.DEFAULT_API_CALL_CONTENT_FILES_SEARCH = "/api/admin/content/files/search";

/// <summary>
/// 
/// </summary>
/// <hidden/>
Machinata.Dialogs._dialogUID = 0;

/// <summary>
/// 
/// </summary>
/// <type>class</type>
var Dialog = function (title, contents, buttons) {
    // Init
    this.uid = Machinata.Dialogs._dialogUID++; 
    this.id = "dialog_" + this.uid;
    Machinata.debug("Dialog: creating "+this.id+"...");
    this.title = title;
    this.contents = contents;
    this.buttons = buttons;
    this._noButtons = false;
    this.options = [];
    this._desiredWidth = null; // if set, defines the desired width, otherwise MAX_DIALOG_WIDTH is used
    this._onDidShows = [];
    this._onDidClose = [];
    this._onOkays = [];
    this._onCancels = [];
    this._onInputs = [];
    this._inputSanitizers = [];
    this._inputValue = null;
    this._closeOnOverlayClick = true;
    this._autoReposition = false;
    this._autoRepositionMinimumDistanceFromPosition = 10; // defines the minumum distance from the proper position before a reposition is invoked...
    this._isOpen = false;
    this._position = { 'my': 'center', 'at': 'center', 'of': window, 'collision': 'fit' };
    this._positionIfTooLarge = { 'my': 'top', 'at': 'top', 'of': window, 'collision': 'fit' };
    this._draggable = true;
    this._fullwindow = false;
    // Create elem
    this.elem = $('<div id="' + this.id + '">');
    this.elem.attr("title", this.title);
    this.elem.attr("class", "bb-dialog ma-dialog");
    this.elem.css("display", "none");
    this.elem.html(contents);
    $("body").append(this.elem);
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.fullwindow = function () {
    this._fullwindow = true;
    return this;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.setDesiredWidth = function (desiredWidth) {
    this._desiredWidth = desiredWidth;
    return this;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.setMinHeight = function (minHeight) {
    this._minHeight = minHeight;
    return this;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.setDraggable = function (draggable) {
    this._draggable = draggable;
    return this;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.getButton = function (id) {
    return this.elem.parent().find("#"+id);
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.close = function () {
    this.elem.dialog("close");
    return this;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.didShow = function (onDidShowFunction) {
    this._onDidShows.push(onDidShowFunction);
    return this;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.didClose = function (onDidCloseFunction) {
    this._onDidClose.push(onDidCloseFunction);
    return this;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.okay = function (onOkayFunction) {
    Machinata.debug("Dialog: register onOkayFunction");
    this._onOkays.push(onOkayFunction);
    return this;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.cancel = function (onCancelFunction) {
    Machinata.debug("Dialog: register onCancelFunction");
    this._onCancels.push(onCancelFunction);
    return this;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.input = function (onInputFunction) {
    Machinata.debug("Dialog: register onInputFunction");
    this._onInputs.push(onInputFunction);
    return this;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.inputSanitizer = function (inputSanitizerFunc) {
    Machinata.debug("Dialog: register inputSanitizer");
    this._inputSanitizers.push(inputSanitizerFunc);
    return this;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.option = function (title, id, click, isCancel) {
    Machinata.debug("Dialog: adding option '" + title + "'");
    var self = this;
    if (id == null) id = title.toLowerCase();
    var wrapper = self.elem.find(".ui-dialog-options");
    if (wrapper.length == 0) {
        wrapper = $("<div class='ui-dialog-options'></div>");
        self.elem.append(wrapper);
    }
    if (title == "-") {
        wrapper.append($("<div class='ui-spacer'></div>"));
        return this;
    }
    var button = $("<button type='button'></button>");
    var clearer = $("<div class='clear'></div>");
    button.text(title);
    button.click(function () {
        self.elem.dialog("close");
        if (click) click(id, self, this);
        if (isCancel == true) for (var i = 0; i < self._onCancels.length; i++) self._onCancels[i](id, self, this);
        else for (var i = 0; i < self._onOkays.length; i++) self._onOkays[i](id, self, this);
    });
    button.button();
    wrapper.append(button);
    wrapper.append(clearer);
    self.options.push(button);
    return this;
};


/// <summary>
/// 
/// </summary>
Dialog.prototype.toggleOption = function (title, id, selected, click) {
    Machinata.debug("Dialog: adding toggle option '" + title + "'");
    var self = this;
    if (id == null) id = title.toLowerCase();
    var wrapper = self.elem.find(".ui-dialog-options");
    if (wrapper.length == 0) {
        wrapper = $("<div class='ui-dialog-options'></div>");
        self.elem.append(wrapper);
    }
    if (title == "-") {
        wrapper.append($("<div class='ui-spacer'></div>"));
        return this;
    }
    var button = $("<button type='button'></button>");
    var clearer = $("<div class='clear'></div>");
    button.text(title);
    button.click(function () {
        //self.elem.dialog("close");
        if (click) click(id, self, this);
        $(this).toggleClass("selected");
    });
    if (selected == true) button.addClass("selected");
    button.attr("data-option",id);
    button.button();
    wrapper.append(button);
    wrapper.append(clearer);
    //self.options.push(button);
    return this;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.button = function (title,id,click,isCancel,closeOnClick) {
    Machinata.debug("Dialog: adding button '" + title + "'");
    var self = this;
    if (id == null) id = title.toLowerCase();
    if (self.buttons == null) self.buttons = [];
    self.buttons.push({
        id: id,
        text: title, 
        closeOnClick: closeOnClick,
        click: function () {
            // Close dialog by default
            if (closeOnClick != false) $(this).dialog("close");
            //TODO: is this really the button?
            if (click) click(id, self, this);
            if (isCancel == true) for (var i = 0; i < self._onCancels.length; i++) self._onCancels[i](id, self, this);
            else {
                var val = null;
                if (self.elem.find("input").length > 0 || self._inputValue != null) {
                    var val = null;
                    if (self._inputValue != null) val = self._inputValue;
                    else val = self.elem.find("input").val();
                    for (var i = 0; i < self._inputSanitizers.length; i++) val = self._inputSanitizers[i](val);
                    for (var i = 0; i < self._onInputs.length; i++) self._onInputs[i](val, id, self, this);
                } 
                for (var i = 0; i < self._onOkays.length; i++) self._onOkays[i](id, self, this, val);
            }
        }
    });
    return this;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.triggerOkay = function (id, val) {
    var self = this;
    for (var i = 0; i < self._onOkays.length; i++) self._onOkays[i](id, self, null, val);
    if ($("#" + self.id).length > 0) $("#" + self.id).dialog("close");
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.triggerInput = function (id, val) {
    var self = this;
    for (var i = 0; i < self._inputSanitizers.length; i++) val = self._inputSanitizers[i](val);
    for (var i = 0; i < self._onInputs.length; i++) self._onInputs[i](val, id, self, null);
    if ($("#" + self.id).length > 0) $("#" + self.id).dialog("close");
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.cancelButton = function (title, id, click) {
    if (title == null) title = "{text.cancel}";
    return this.button(title, id, click, true);
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.okayButton = function (title, id, click) {
    if (title == null) title = "{text.okay}";
    return this.button(title, id, click, false);
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.design = function (className) {
    this.elem.addClass(className);
    this._design = className;
    return this;
};
Dialog.prototype.setDesign = Dialog.prototype.design;

/// <summary>
/// 
/// </summary>
Dialog.prototype.wide = function () {
    this.setDesiredWidth(Machinata.Dialogs.MAX_DIALOG_WIDTH_WIDE);
    this.design("wide");
    return this;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.setScrollable = function (maxContentHeight) {
    if (maxContentHeight == null) maxContentHeight = $(window).height() * 0.6;

    this.maxContentHeight = maxContentHeight;

    return this;
};



/// <summary>
/// 
/// </summary>
Dialog.prototype.noButtons = function () {
    return this._noButtons = true;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.data = function (key,val) {
    if (val == null) {
        return this.elem.data(key);
    } else {
        return this.elem.data(key, val);
    }
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.setInputValue = function (val) {
    Machinata.debug("Setting dialog input value to " + val);
    return this._inputValue = val;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.closeOnOverlayClick = function (val) {
    this._closeOnOverlayClick = val;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.show = function () {
    Machinata.debug("Dialog: showing " + this.id + "...");
    // Validate buttons
    var self = this;
    if ((self.buttons == null || self.buttons.length == 0) && self.options.length == 0 && this._noButtons != true) {
        Machinata.debug("Dialog: adding default button");
        self.button("{text.okay}");
    }
    // Get width
    var windowWidth = $(window).width();
    var desiredWidth = this._desiredWidth || Machinata.Dialogs.MAX_DIALOG_WIDTH;
    var dialogWidth = Math.min(windowWidth - (2 * parseFloat("{theme.padding-half}")), desiredWidth);
    var dialogHeight = null;
    var dialogMaxHeight = null;
    // Max content height? Pre-emptively set the max-height
    if (this.maxContentHeight != null) {
        dialogMaxHeight = $(window).height() * 0.9;
    }
    // Full window?
    if (this._fullwindow == true) {
        // Init
        //TODO: all this scrollable stuff should be seperate from fullscreen!
        var scrollableElem = self.elem;
        scrollableElem.addClass("option-scrollable ui-effect-scroll-fade");
        var scrollHintElem = null;
        this._position = null;
        // Resize helper method
        function updateSize() {
            self.elem.parent().css({
                'width': $(window).width() - (2 * parseFloat("{theme.padding-half}".replace("px", ""))),
                'height': $(window).height() - (2 * parseFloat("{theme.padding-half}".replace("px", ""))),
            });
            var dialogHeight = self.elem.parent().height();
            var titleHeight = self.elem.parent().find(".ui-dialog-title").outerHeight(true) || 0;
            var buttonsHeight = self.elem.parent().find(".ui-dialog-buttonset").outerHeight(true) || 0;
            var contentsHeight = dialogHeight - titleHeight - buttonsHeight - parseFloat("{theme.padding}".replace("px", ""));
            self.elem.css({
                'height': contentsHeight
            });
            // Scroll hint
            var isScrollable = scrollableElem[0].scrollHeight > scrollableElem[0].clientHeight;
            if (isScrollable == true) {
                if (scrollHintElem == null) {
                    scrollHintElem = $("<div class='ui-dialog-scroll-hint ui-effect-scroll-hint option-hidden'/>");
                    setTimeout(function () {
                        scrollHintElem.removeClass("option-hidden");
                    }, 6000);
                    self.elem.parent().append(scrollHintElem);
                    scrollableElem.on("scroll", function () {
                        scrollHintElem.addClass("option-did-scroll");
                    });
                }
                scrollHintElem.position({
                    my: "right bottom",
                    at: "left-{theme.padding} bottom".replace("px", ""),
                    of: self.elem, 
                    collision: "fit"
                });
            }
        }
        // Bind events to size the dialog 
        self.elem.on("dialogopen", function (event, ui) {
            self.elem.parent().css({
                'left': '{theme.padding-half}'.replace("px","")+"px",
                'top': '{theme.padding-half}'.replace("px", "") +"px",
                'max-width': 'unset',
                'position': 'fixed',
            });
            self.elem.css({
                'padding-bottom': "{theme.padding}".replace("px", "") + "px"
            });
            scrollableElem.css({
                'overflow': "scroll"
            });
            updateSize();
            Machinata.UI.bindScrollFadeForElement(scrollableElem);
        });
        $(window).resize(function () {
            updateSize();
        });
    }
    // Dialog position
    var dialogPosition = this._position;
    if ($(window.height) < 300) dialogPosition = this._positionIfTooLarge; // tiny screens (most likely keyboard showing)
    // Use jquery ui to show the dialog
    var dialogOpts = {
        modal: true,
        dialogClass: Machinata.Dialogs.DIALOG_ADDITIONAL_CLASSES,
        buttons: this.buttons,
        width: this._forcedWidth || dialogWidth, // used to be auto
        position: dialogPosition,
        draggable: this._draggable,
        open: function (event, ui) {
            self._isOpen = true;
            if (Machinata.Dialogs.DIALOG_ADDITIONAL_CLASSES != null) {
                $('.ui-widget-overlay').addClass(Machinata.Dialogs.DIALOG_ADDITIONAL_CLASSES + "-overlay");
            }
            if (self._closeOnOverlayClick == true) {
                $('.ui-widget-overlay').on('click', function () {
                    self.elem.dialog('close');
                });
            }
            if (self._autoReposition == true && self._fullwindow == false) {
                var uiElem = self.elem.closest(".ui-dialog");
                uiElem.addClass("option-auto-reposition");
                function getCenter() {
                    var x = uiElem.offset().left - $(window).scrollLeft();
                    var y = uiElem.offset().top - $(window).scrollTop();
                    return { x: x, y: y };
                }
                self._lastUICenter = getCenter();
                self._autoRepositionTimer = setInterval(function () {
                    if (self._isOpen == true && $("#" + self.id).dialog('isOpen') == true) {
                        // Calculate distance to last center
                        var currentUICenter = getCenter();
                        var distance = Machinata.Math.vec2Dist(currentUICenter.x, currentUICenter.y, self._lastUICenter.x, self._lastUICenter.y);
                        if (distance > self._autoRepositionMinimumDistanceFromPosition && uiElem.height() < $(window).height()) {
                            uiElem.position(self._position);
                            self._lastUICenter = getCenter();
                        }
                    }
                }, 500);
            }
            // Callback
            for (var i = 0; i < self._onDidShows.length; i++) self._onDidShows[i](self);
        },
        close: function (event, ui) {
            self._isOpen = false;
            // Callback
            for (var i = 0; i < self._onDidClose.length; i++) self._onDidClose[i](self, event, ui);
            // Cleanup
            self.elem.remove();
            if (self._autoRepositionTimer != null) clearInterval(self._autoRepositionTimer);
        },
    };
    if (dialogHeight != null) dialogOpts.height = dialogHeight;
    if (dialogMaxHeight != null) dialogOpts.maxHeight = dialogMaxHeight;
    if (this._minHeight != null) dialogOpts.minHeight = this._minHeight; 
   
    // Show the dialog via jquery
    $("#" + this.id).dialog(dialogOpts);
    if (this._minHeight == null) this.elem.css("minHeight", ""); // overwrite the jquery css minheight
    if (this._design != null) this.elem.parent().addClass(this._design);
    // Max content height? Unfortunately we have to reposition...
    if (this.maxContentHeight != null) {
        this.elem.css("overflow-y", "auto");
        this.elem.css("max-height", this.maxContentHeight);
        this.reposition();
    }
    // Window resize event
    $(window).resize(function () {
        self.reposition();
    });
    return this;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.setWidth = function (newWidth) {
    this._forcedWidth = newWidth;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.reposition = function () {
    var self = this;
    Machinata.debug("Dialog: repositioning " + self.id + "...");
    var positionToUse = self._position;
    var diagElem = $("#" + self.id);
    if (diagElem.height() > $(window).height()) positionToUse = self._positionIfTooLarge;
    diagElem.dialog({
        position: positionToUse
    });
    return this;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.autoReposition = function (opts) {
    if (opts == null) opts = {};
    if (opts.minimumDistanceFromPosition != null) this._autoRepositionMinimumDistanceFromPosition = opts.minimumDistanceFromPosition;
    this._autoReposition = true;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.setWidth = function (newWidth) {
    this._forcedWidth = newWidth;
};

/// <summary>
/// 
/// </summary>
Dialog.prototype.getToggledOptions = function () {
    var ret = [];
    this.elem.find(".ui-dialog-options button.selected").each(function () {
        ret.push($(this).attr("data-option"));
    });
    return ret;
};



/// <summary>
/// Dialog: generic
/// </summary>
Machinata.dialog = function (title, contents, buttons) {
    Machinata.debug("showDialog(" + title + "," + JSON.stringify(buttons) + ")");
    var diag = new Dialog(title, contents, buttons);
    return diag;
};

/// <summary>
/// Dialog: message
/// </summary>
Machinata.messageDialog = function (title, message, buttons) {
    var contents = "";
    contents += "<p>";
    if (message != null) contents += message;
    contents += "</p>";
    var diag = Machinata.dialog(title, contents, buttons);
    return diag;
};

/// <summary>
/// Dialog: full-window-size
/// Use the full-sized .dialog-contents div to add to the dialog.
/// </summary>
Machinata.fullwindowDialog = function (title, buttons) {
    //var contents = "<div class='dialog-contents' style='width:100%;position:relative;'></div>";
    var diag = Machinata.dialog(title, null, buttons);
    diag._fullwindow = true;
    return diag;
};

/// <summary>
/// Dialog: confirm
/// </summary>
Machinata.confirmDialog = function (title, message, buttons) {
    var diag = Machinata.messageDialog(title, message, buttons);
    diag.cancelButton("{text.cancel}");
    diag.button("{text.okay}");
    diag.closeOnOverlayClick(false);
    return diag;
};


/// <summary>
/// Dialog: yes No
/// </summary>
Machinata.yesNoDialog = function (title, message, buttons) {
    var diag = Machinata.messageDialog(title, message, buttons);
    diag.cancelButton("{text.no}");
    diag.button("{text.yes}");
    diag.closeOnOverlayClick(false);
    return diag;
};

/// <summary>
/// Dialog: input
/// </summary>
Machinata.inputDialog = function (title, message, defaultValue, placeholder, type, buttons) {
    // Init contents
    var contents = "";
    if (!String.isEmpty(message)) {
        contents += "<p>";
        contents += message;
        contents += "</p>";
    }
    contents += "<div class='ui-form'>";
    contents += "<input/>";
    contents += "</div>";
    // Create the dialog
    var diag = Machinata.dialog(title, contents, buttons);
    var inputElem = diag.elem.find("input");
    if (defaultValue != null) inputElem.val(defaultValue);
    if (placeholder != null) inputElem.attr("placeholder", placeholder);
    if (type != null) inputElem.attr("type", type);
    else inputElem.attr("type", "text");
    diag.cancelButton("{text.cancel}");
    diag.button("{text.okay}");
    // Bind input
    inputElem.keypress(function (e) {
        if (e.which == 13) {
            var okayButton = diag.getButton("okay");
            okayButton.trigger("click");
        }
    });
    return diag;
};

/// <summary>
/// Dialog: input
/// </summary>
Machinata.numberDialog = function (title, message, defaultValue, placeholder, buttons) {
    return Machinata.inputDialog(title, message, defaultValue, placeholder, "number", buttons);
};


/// <summary>
/// Dialog: options
/// </summary>
Machinata.optionsDialog = function (title, message, options, buttons) {
    // Create the dialog
    var diag = Machinata.messageDialog(title, message, buttons);
    for (var key in options) {
        diag.option(options[key], key);
    }
    diag.cancelButton("{text.cancel}");
    //diag.button("{text.okay}");
    return diag;
};

/// <summary>
/// Dialog: toggle options
/// ```
/// var options = [
///     {
///         "id": "one-column",
///         "title": "One Column",
///         "selected": false
///      }
/// ]
/// ```
/// </summary>
Machinata.toggleOptionsDialog = function (title, message, options, buttons) {
    // Create the dialog
    var diag = Machinata.messageDialog(title, message, buttons);
    for (var i in options) {
        diag.toggleOption(options[i].title, options[i].id, options[i].selected);
    }
    diag.cancelButton("{text.cancel}");
    diag.button("{text.okay}");
    return diag;
};


/// <summary>
/// Dialog: progress
/// </summary>
Machinata.progressDialog = function (title, message, buttons) {
    // Init diag
    var contents = "";
    contents += "<div class='ui-progressbar'>";
    contents += "  <div class='progress'>";
    contents += "  </div>";
    contents += "</div>";
    var diag = Machinata.dialog(title, contents, buttons);
    // Prepare progressbar
    var progressBar = diag.elem.find(".ui-progressbar");
    // Update event
    diag.updateProgress = function (percent) {
        progressBar.find(".progress").css("width", percent+"%");
    };
    diag.closeOnOverlayClick(false);
    // Return
    return diag;
};


/// <summary>
/// Dialog: upload
/// </summary>
Machinata.uploadDialog = function (title, source, category, buttons, apiCall, apiCallSuccess) {
    // Validate
    if (source == null) {
        console.log("You must specify a source when using Machinata.uploadDialog!");
        return;
    }
    // Set api call
    if (apiCall == null) apiCall = Machinata.Dialogs.DEFAULT_API_CALL_CONTENT_FILES_CREATE;
    // Init diag
    var diag = Machinata.progressDialog(title, null, buttons);
    diag.noButtons();
    // Do API Call
    var call = Machinata.apiCall(apiCall);
    var data = {};
    data["source"] = source;
    data["category"] = category;
    call.data(data);
    call.success(function (message) {
        diag.close();
    });
    if (apiCallSuccess == null) {
        call.success(function (message) {
            var contentURL = message.data["content-file"]["content-url"];
            diag.triggerInput("upload", contentURL);
        });
    } else {
        call.success(apiCallSuccess);
    }
    call.progress(function (percent) {
        diag.updateProgress(percent);
    });
    call.genericError();
    call.error(function () {
        diag.close();
    });
    // Get file via a input elem by invoking it's click
    var inputAccept = "";
    if (category == "image") inputAccept = "image/*";
    else if (category == "json") inputAccept = "json/*";
    var inputElem = $("<input type='file' accept='" + inputAccept + "' style='position:absolute;left:-10000px;top:0px;'/>");
    diag.elem.append(inputElem);
    inputElem.on("change", function () {
        // Show diag
        diag.show();
        // Attach file to call and send
        var fileNative = $(this)[0];
        call.attachFileInput(fileNative.files[0], name, fileNative.files[0].name);
        call.send();
    });
    inputElem.trigger("click");
    // Return self
    diag.closeOnOverlayClick(false);
    return diag;
};


/// <summary>
/// Dialog: file
/// </summary>
Machinata.fileDialog = function (title, source, category, buttons, apiSearchCall, apiUploadCall) {
    // Validate
    if (source == null) {
        console.log("You must specify a source when using Machinata.fileDialog!");
        return;
    }
    // Find filetype from category
    var filetype = null;
    if (category != null) {
        if (category.indexOf('/') > 0) {
            var catType = category.split('/');
            if (catType.length == 2) {
                category = catType[0];
                filetype = catType[1];
            }
        }
    }
    // Init diag
    var contents = "";
    contents += "<div class='content-file-browser'>";
    contents += "   <div class='ui-form'><input type='text' class='filter' placeholder='Filter'/></div>";
    contents += "   <div class='content-files ui-file-panel'>";
    contents += "   </div>";
    contents += "</div>";
    var diag = Machinata.dialog(title, contents, buttons);
    diag.design("wide");
    diag.button("{text.cancel}", "cancel", null, true);
    if (source == "*") {
        
    } else {
        diag.button("{text.upload}", "upload", function () {
            Machinata.uploadDialog("{text.upload-file=Upload File}", source, category, buttons, apiUploadCall )
                .input(function (val) {
                    diag.triggerOkay(val);
                });
        }, true);
    }
    diag.updateFiles = function () { //TODO: this should be diag.data(...)
        var self = this;
        self.elem.addClass("loading");
        var contentFilesElem = self.elem.find(".content-files");
        contentFilesElem.text("");
        contentFilesElem.append($("<p>{text.loading-dotdotdot=Loading...}</p>"));
        // Prepare api call
        if (apiSearchCall == null) apiSearchCall = Machinata.Dialogs.DEFAULT_API_CALL_CONTENT_FILES_SEARCH;
        var call = Machinata.apiCall(apiSearchCall);
        var data = {};
        data["category"] = category;
        data["source"] = source;
        data["filter"] = self.elem.find("input.filter").val();
      
        if (filetype != null) data["filetype"] = filetype;
        call.data(data);
        call.success(function (message) {
            // Remove loading
            self.elem.removeClass("loading");
            contentFilesElem.text("");
            // Add contetns
            for (var i = 0; i < message.data.length; i++) {
                var cf = message.data[i];
                var thumbnail = Machinata.Content.createFilePreview(cf["content-url"], cf["file-name"], true);
                thumbnail.click(function () {
                    diag.triggerOkay($(this).attr("content-url"));
                });
                contentFilesElem.append(thumbnail);
            }
            if (message.data.length == 0) {
                contentFilesElem.append($("<p>{text.no-results=No Results}</p>"));
            }
        })
        call.error(function (message) {
            self.elem.removeClass("loading");
        });
        call.genericError();
        // Send
        call.send();
    };
    diag.updateFiles();
    // Bind input enter
    diag.elem.find("input.filter").keypress(function (e) {
        if (e.which == 13) {
            diag.updateFiles();
            return false;
        }
    });
    // Return
    return diag;
};


/// <summary>
/// Dialog: page
/// </summary>
Machinata.cmsPageDialog = function (title, path, buttons, apiCall, apiCallSuccess) {
    // Init diag
    var currentPath = path;
    var contents = "";
    contents += "<div class='content-file-browser'>";
    contents += "   <div class='ui-form'><input type='text' class='filter' placeholder='{text.search}'/></div>";
    contents += "   <div style='height:8px;'></div>";
    contents += "   <ul class='ui-breadcrumb breadcrumb'></ul>";
    contents += "   <div style='height:8px;'></div>";
    contents += "   <div class='content-files ui-file-panel'></div>";
    contents += "</div>";
    var diag = Machinata.dialog(title, contents, buttons);
    diag.design("wide");
    diag.button("{text.cancel}", "cancel", null, true);
    diag.button("{text.select}", "input", function () {
        diag.triggerInput("path", currentPath);
    }, true);
    diag.updatePages = function () { //TODO: this should be diag.data(...)
        var self = this;
        // Update breadcrumb
        var crumbPathToSplit = currentPath;
        if (crumbPathToSplit == "/") crumbPathToSplit = "";
        var crumbs = crumbPathToSplit.split("/");
        self.elem.find(".breadcrumb").empty();
        var crumbPath = "";
        for (var i = 0; i < crumbs.length; i++) {
            var crumb = $("<li><a class='item'></a></li>");
            if (i == 0) {
                crumb.find("a").text("Content");
            } else {
                crumb.find("a").text(crumbs[i]);
                crumbPath += "/" + crumbs[i];
            }
            crumb.attr("node-path", crumbPath);
            crumb.click(function () {
                currentPath = $(this).attr("node-path");
                diag.updatePages();
            });
            self.elem.find(".breadcrumb").append(crumb);
        }
        // Update sub pages
        self.elem.addClass("loading");
        var contentFilesElem = self.elem.find(".content-files");
        contentFilesElem.text("");
        contentFilesElem.append($("<p>{text.loading-dotdotdot=Loading...}</p>"));
        // Prepare api call
        if (apiCall == null) apiCall = Machinata.Dialogs.DEFAULT_API_CALL_CONTENT_PAGES_LIST;
        var call = Machinata.apiCall(apiCall);
        var data = {};
        data["path"] = currentPath;
        data["search"] = diag.data("search");
        call.data(data);
        call.success(function (message) {
            // Remove loading
            self.elem.removeClass("loading");
            contentFilesElem.text("");
            // Clear previous serach
            diag.data("search", null);
            // Add contetns
            for (var i = 0; i < message.data.pages.length; i++) {
                var page = message.data.pages[i];
                var thumbnail = Machinata.Content.createGenericPreview(
                    page["name"],
                    null,
                    null,
                    "{text.page}",
                    "document",
                    "page"
                );
                thumbnail.data("node-path", page["path"]);
                thumbnail.click(function () {
                    currentPath = $(this).data("node-path");
                    diag.updatePages();
                });
                contentFilesElem.append(thumbnail);
            }
            if (message.data.length == 0) {
                contentFilesElem.append($("<p>{text.no-results=No Results}</p>"));
            }
        })
        call.error(function (message) {
            self.elem.removeClass("loading");
        });
        call.genericError();
        // Send
        call.send();
    };
    diag.updatePages();
    // Bind input enter
    diag.elem.find("input.filter").keypress(function (e) {
        console.warn("TODO: not yet supported");
        return;
        //if (e.which == 13) {
        //    diag.updatePages();
        //    return false;
        //}
    });
    // Return
    return diag;
};



/// <summary>
/// Dialog: product
/// </summary>
Machinata.productDialog = function (title, buttons) {
    // Init diag
    var contents = "";
    contents += "<div class='content-file-browser'>";
    contents += "   <ul class='breadcrumb auto-select-last wide'>";
    contents += "   </ul>";
    contents += "   <div style='height:8px;'>";
    contents += "   </div>";
    contents += "   <div class='content-files'>";
    contents += "   </div>";
    contents += "</div>";
    var diag = Machinata.dialog(title, contents, buttons);
    var path = "/";
    diag.data("path", path);
    diag.design("wide");
    diag.button("{text.cancel}", "cancel", null, true);
    diag.updatePages = function () { //TODO: this should be diag.data(...)
        var self = this;
        // Update breadcrumb
        var crumbPathToSplit = diag.data("path");
        if (crumbPathToSplit == "/") crumbPathToSplit = "";
        var crumbs = crumbPathToSplit.split("/");
        self.elem.find(".breadcrumb").empty();
        var crumbPath = "/";
        for (var i = 0; i < crumbs.length; i++) {
            var crumb = $("<li><a class='item'></a></li>");
            if (i == 0) {
                crumb.find("a").text("Catalog Groups");
            } else {
                crumb.find("a").text(crumbs[i]);
                if(i != 1) crumbPath += "/";
                crumbPath += crumbs[i];
            }
            crumb.attr("node-path", crumbPath);
            crumb.click(function () {
                diag.data("path", $(this).attr("node-path"));
                diag.updatePages();
            });
            self.elem.find(".breadcrumb").append(crumb);
        }
        // Update sub pages
        self.elem.addClass("loading");
        var contentFilesElem = self.elem.find(".content-files");
        contentFilesElem.text("");
        contentFilesElem.append($("<p>{text.loading-dotdotdot=Loading...}</p>"));
        // Prepare api call
        var call = Machinata.apiCall("/api/admin/shop/browse-catalog");
        var data = {};
        data["path"] = diag.data("path");
        call.data(data);
        call.success(function (message) {
            // Remove loading
            self.elem.removeClass("loading");
            contentFilesElem.text("");
            // Clear previous serach
            diag.data("search", null);
            // Add contetns
            for (var i = 0; i < message.data.pages.length; i++) {
                var data = message.data.pages[i];
                var thumbnail = Machinata.Content.createGenericPreview(
                    data["name"],
                    data["catalog-name"],
                    null,
                    data["type"],
                    "tag",
                    "product"
                );
                thumbnail.data("data", data);
                thumbnail.click(function () {
                    var data = $(this).data("data");
                    if (data["product-name"] != null) {
                        diag.triggerInput("product-public-id", data);
                    } else {
                        diag.data("path", data["path"]);
                        diag.updatePages();
                    }
                });
                contentFilesElem.append(thumbnail);
            }
            if (message.data.length == 0) {
                contentFilesElem.append($("<p>{text.no-results=No Results}</p>"));
            }
        })
        call.error(function (message) {
            self.elem.removeClass("loading");
        });
        call.genericError();
        // Send
        call.send();
    };
    diag.updatePages();
    // Return
    return diag;
};


/// <summary>
/// Dialog: link
/// </summary>
Machinata.linkDialog = function (title, message, defaultValue, placeholder, buttons) {
    if (title == null) title = "{text.dialog-link-title=Link}";
    //if (message == null) message = "{text.dialog-link-message=Enter a domain or link:}";
    if (placeholder == null) placeholder = "www.domain.com";
    var diag = Machinata.inputDialog(title, message, defaultValue, placeholder, null, buttons);
    diag.inputSanitizer(function (val) {
        // Sanity
        Machinata.debug("Dialog: link sanitizing '"+val+"'");
        if(val == null || val == "") return null;
        // If it is already a link just return
        if (val.startsWith("http://") || val.startsWith("https://")) return val;
        if (val.startsWith("mailto:")) return val;
        if (val.startsWith("tel:")) return val;
        if (val.startsWith("skype:")) return val;
        if (val.startsWith("javascript:")) return val;
        if (val.startsWith("/")) return val;
        if (val.startsWith("{")) return val;
        if (val.startsWith("#")) return val;
        // Email?
        if (val.indexOf("@") > -1) return "mailto:"+val;
        // Add default http
        return "http://" + val;
    });
    return diag;
};

/// <summary>
/// Dialog: month
/// </summary>
Machinata.monthDialog = function (title, message, buttons, format) {
    
    var contents = "";
    if (message != null) contents += "<p>";
    if (message != null) contents += message;
    if (message != null) contents += "</p>";
    contents += "<div class='month-picker'>";
    contents += "   <select class=\"year\" style='min-width:100px'></select>";
    contents += "   <select class=\"month\" style='min-width:50px'></select>";
    contents += "</div>";
    var diag = Machinata.dialog(title, contents, buttons);
    var today = new Date();
    for (var yy = today.getFullYear(); yy >= 1960; yy--) {
        diag.elem.find("select.year").append("<option value='"+yy+"'>" + yy + "</option>");
    }
    for (var mm = 1; mm <= 12; mm++) {
        var optElem = $("<option value='" + mm + "'>" + mm + "</option>");
        if (mm == today.getMonth()+1) optElem.attr("selected", "selected");
        diag.elem.find("select.month").append(optElem);
    }
    diag.cancelButton();
    //diag.button("{text.clear}", "clear");
    diag.okayButton();

    // Bind interactions
    function updateInputValue() {
        var year = parseInt(diag.elem.find("select.year").val());
        var month = parseInt(diag.elem.find("select.month").val());
        var date = new Date(year, month-1, 1, 0, 0, 0, 0);
        if (format == null) {
            diag.setInputValue(date);
        } else {
            var formatted = Machinata.formattedDate(date, format);
            diag.setInputValue(formatted);
        }
    }
    diag.elem.find("select").on("change", updateInputValue);
    updateInputValue();
    
    // Return
    return diag;
};



/// <summary>
/// Dialog: calendar week
/// </summary>
Machinata.calendarWeekDialog = function (title, message, buttons,format) {

    var contents = "";
    if (message != null) contents += "<p>";
    if (message != null) contents += message;
    if (message != null) contents += "</p>";
    contents += "<div class='cw-picker'>";
    contents += "   <select class=\"year\" style='min-width:100px'/>";
    contents += "   <select class=\"week\" style='min-width:80px'/>";
    contents += "</div>";
    var diag = Machinata.dialog(title, contents, buttons);
    var today = new Date();
    for (var yy = today.getFullYear(); yy >= 1960; yy--) {
        diag.elem.find("select.year").append("<option value='" + yy + "'>" + yy + "</option>");
    }
    for (var ww = 1; ww <= 52; ww++) {
        var optElem = $("<option value='" + ww + "'>" + ww + "</option>");
        if (ww == today.iso8601CalendarWeek()) optElem.attr("selected", "selected");
        diag.elem.find("select.week").append(optElem);
    }
    diag.cancelButton();
    //diag.button("{text.clear}", "clear");
    diag.okayButton();

    // Bind interactions
    function updateInputValue() {
        var year = parseInt(diag.elem.find("select.year").val());
        var week = parseInt(diag.elem.find("select.week").val());
        var days = (week-1) * 7;
        var date = new Date(year, 0, 1, 0, 0, 0, 0);
        date.setDate(date.getDate() + days);
        if (format == null) {
            diag.setInputValue(date);
        } else {
            var formatted = Machinata.formattedDate(date, format);
            diag.setInputValue(formatted);
        }
    }
    diag.elem.find("select").on("change", updateInputValue);
    updateInputValue();

    // Return
    return diag;
};

/// <summary>
/// Dialog: date
/// </summary>
Machinata.dateDialog = function (title, message, buttons, currentDateString, dateFormat, timeFormat, isDateRange) {
    Machinata.debug("Creating date dialog with '" + currentDateString + "'");
    Machinata.debug("  Date format: '" + dateFormat + "'");
    Machinata.debug("  Time format: '" + timeFormat + "'");
    Machinata.debug("  Current date: '" + currentDateString + "'");
    // Convert string to date
    if (dateFormat != null) dateFormat = Machinata.convertDotNetTimeFormatToDatepickerFormat(dateFormat);
    else dateFormat = Machinata.convertDotNetTimeFormatToDatepickerFormat(Machinata.DEFAULT_DATE_FORMAT_JAVASCRIPT_UI); // default
    var format = dateFormat;
    if (timeFormat != null) format = dateFormat + " " + timeFormat;
    var currentDate1 = null;
    var currentDate2 = null;
    // Range?
    if (isDateRange == true) {
        // Split on dash
        if (currentDateString != null && currentDateString != "") {
            var segs = currentDateString.split(Machinata.DEFAULT_DATERANGE_SEPARATOR);
            if (segs.length != 2) {
                alert("invalid date range "+currentDateString);
                return;
            }
            currentDate1 = Machinata.dateFromString(segs[0], format);
            currentDate2 = Machinata.dateFromString(segs[1], format);
        }
    } else {
        // Single date
        currentDate1 = Machinata.dateFromString(currentDateString, format);
    }
    Machinata.debug("  Converted date: '" + currentDate1 + "'");
    // Init diag
    if (title == null) {
        if (isDateRange == true) title = "{text.select-a-date-range=Select a Date Range}";
        else title = "{text.select-a-date=Select a Date}";
    }
    var contents = "";
    contents += "<div class=''>";
    contents += "   <div class=\"datetimepicker1\"></div>";
    contents += "   <div class='datetimepickerSpacer' style='height:1em;'></div>";
    contents += "   <div class=\"datetimepicker2\"></div>";
    contents += "</div>";
    var diag = Machinata.dialog(title, contents, buttons);
    diag.cancelButton();
    diag.button("{text.clear}", "clear");
    diag.okayButton();
    // Init picker elems
    var dateTimePickerElem1 = diag.elem.find(".datetimepicker1");
    var dateTimePickerElemSpacer = diag.elem.find(".datetimepickerSpacer");
    var dateTimePickerElem2 = diag.elem.find(".datetimepicker2");
    // Create options
    var opts = {};
    opts.stepHour = 1;
    opts.stepMinute = 30;
    opts.showTime = false;
    opts.showMinute = false;
    opts.showHour = false;
    opts.showButtonPanel = false;
    opts.dateFormat = dateFormat;
    opts.timeFormat = timeFormat;
    opts.showTimezone = false;
    opts.onSelect = function (selectedDateTime, datepickerInstance) {
        if (isDateRange == true) {
            // Update value
            var date1 = dateTimePickerElem1.datepicker("getDate");
            var date2 = dateTimePickerElem2.datepicker("getDate");
            // Do constraints
            if (datepickerInstance != null) {
                if (datepickerInstance.id == dateTimePickerElem2.attr("id")) dateTimePickerElem1.datepicker("option", "maxDate", date2);
                if (datepickerInstance.id == dateTimePickerElem1.attr("id")) dateTimePickerElem2.datepicker("option", "minDate", date1);
            }
            diag.setInputValue({start:date1,end:date2});
        } else {
            // Single date
            diag.setInputValue(dateTimePickerElem1.datepicker("getDate"));
        }
    }
    if (timeFormat == null) opts.showTimepicker = false;

    

    // Create the picker
    if (isDateRange == true) {
        opts.minDate = null;
        if (currentDate2 != null) opts.maxDate = currentDate2;
        dateTimePickerElem1.datetimepicker(opts);
        opts.maxDate = null;
        if (currentDate1 != null) opts.minDate = currentDate1;
        dateTimePickerElem2.datetimepicker(opts);
        // If dates are null, set default to noon today (otherwise widget will set to 00:00 today)
        if (currentDate1 == null) {
            currentDate1 = new Date();
            currentDate1.setHours(12);
            currentDate1.setMinutes(0);
            currentDate1.setSeconds(0);
            currentDate1.setMilliseconds(0);
        }
        if (currentDate2 == null) {
            currentDate2 = new Date();
            currentDate2.setHours(12);
            currentDate2.setMinutes(0);
            currentDate2.setSeconds(0);
            currentDate2.setMilliseconds(0);
        }
        dateTimePickerElem1.datetimepicker("setDate", currentDate1);
        dateTimePickerElem2.datetimepicker("setDate", currentDate2);
    } else {
        // Only need one picker...
        dateTimePickerElemSpacer.hide();
        dateTimePickerElem1.datetimepicker(opts);
        // If dates are null, set default to noon today (otherwise widget will set to 00:00 today)
        if (currentDate1 == null) {
            currentDate1 = new Date();
            currentDate1.setHours(12);
            currentDate1.setMinutes(0);
            currentDate1.setSeconds(0);
            currentDate1.setMilliseconds(0);
        }
        dateTimePickerElem1.datetimepicker("setDate", currentDate1);
    }
    // Initial value
    if (isDateRange == true) {
        diag.setInputValue({ start: currentDate1, end: currentDate2 });
    } else {
        diag.setInputValue(currentDate1);
    }
    // Return
    return diag;
};

/// <summary>
/// Dialog: language change
/// </summary>
Machinata.showLanguageChangeDialog = function (callback) {
    Machinata.apiCall("/api/admin/config/core/supported-languages")
        .genericError()
        .success(function(message){
            var sourceLanguage = null;
            var targetLanguage = null;
            var diagFrom = Machinata.messageDialog("{text.export-translation}", "{text.export-translation-source=Source Language}:")
                .cancelButton()
                .okay(function (id) {
                    sourceLanguage = id;
                    var diagTo = Machinata.messageDialog("{text.export-translation}", "{text.export-translation-target=Target Language}:")
                        .cancelButton()
                        .okay(function (id) {
                            targetLanguage = id;
                            callback(sourceLanguage, targetLanguage);
                        });
                    for (var i = 0; i < message.data.value.length; i++) {
                        var lang = message.data.value[i];
                        if (lang == sourceLanguage) continue;
                        diagTo.option(lang.toUpperCase(), lang);
                    }
                    diagTo.show();
                });
            for (var i = 0; i < message.data.value.length; i++) {
                var lang = message.data.value[i];
                diagFrom.option(lang.toUpperCase(), lang);
            }
            diagFrom.show();
        })
        .send();
};


/// <summary>
/// Dialog: load a page and display its contents as a dialog
/// Example:
/// ```Machinata.showPageAsDialog("/help/popup/xyz","#content")```
/// </summary>
Machinata.pageDialog = function (url, selector, maxContentHeight) {
    if (maxContentHeight == null) maxContentHeight = $(window).height() * 0.5;
    var urlToForLoad = url;
    if (selector != null) urlToForLoad += " " + selector;

    Machinata.showLoading(true);
    var tempNode = $("<div class='page-dialog'></div>");
    tempNode.load(urlToForLoad, function () {
        var title = tempNode.find("h1").first().text();
        tempNode.find("h1").first().remove();
        var diag = Machinata.dialog(title, tempNode.html());
        diag.show();
        if (maxContentHeight != null && maxContentHeight != 0) {
            diag.elem.css("overflow-y", "auto");
            diag.elem.css("max-height", maxContentHeight + "px");
        }
        Machinata.UI.bind(diag.elem); 
        diag.reposition();
        Machinata.showLoading(false);
    });
    
};

/// <deprecated/>
Machinata.showPageAsDialog = function (url, selector, maxContentHeight) {
    Machinata.pageDialog(url, selector, maxContentHeight);
};



/// <summary>
/// 
/// </summary>
Machinata.colorDialog = function (title, message, colorSystems, opts) {
    if (opts == null) opts = {};

    // Create swatch
    //opts.onSelect = function (data) {
    //    diag.setInputValue({ code: data.code, hex: data.hex });
    //};
    //var colorSwatches = Machinata.UI.createSwatchPicker(colorSystems, opts);

    // Create loading elem
    //var loadingElem = $("<div class='loader'>{text.loading}...</div>");
    var loadingElem = $("<div class='loader'>{text.loading}...<div class='bb-color-swatch ui-color-swatch'><div class='colors' style='border:none'></div></div></div>");

    // Setup dialog
    var diag = Machinata.messageDialog(title, message);
    //diag.elem.append(colorSwatches);
    diag.elem.append(loadingElem);
    diag.cancelButton("{text.cancel}");
    diag.button("{text.okay}");
    diag.didShow(function (diag) {
        // Create swatch
        setTimeout(function () {

            opts.onSelect = function (data) {
                diag.setInputValue({ code: data.code, hex: data.hex });
            };
            var colorSwatches = Machinata.UI.createSwatchPicker(colorSystems, opts);
            diag.elem.find(".loader").empty();
            diag.elem.append(colorSwatches);
            // Reposition
            //setTimeout(function () {
            //    diag.reposition();
            //}, 10);
        },10);
    });
    diag.show();

    return diag;
};



/// <summary>
/// 
/// </summary>
Machinata.locationDialog = function (title, message, opts) {
    if (opts == null) opts = {};
    if (opts.readonly == null) opts.readonly = false;

    // Create custom input
    var formElem = $("<div class='ui-form'><input type='text' placeholder='{text.maps.address-or-latlon}'/></div>");
    var inputElem = formElem.find("input");
    inputElem.attr("title", "{text.maps.address-or-latlon}. {text.maps.press-enter-to-search}");

    // Create map
    var mapElem = $("<div class='ui-map'></div>");
    mapElem.css("width", "100%");
    mapElem.css("height", "200px");
    
    // Setup dialog
    var diag = Machinata.messageDialog(title, message);
    if (opts.readonly != true) diag.elem.append(formElem);
    diag.elem.append(mapElem);
    if (opts.readonly != true) diag.cancelButton("{text.cancel}");
    if (opts.readonly != true) diag.button("{text.clear}", "clear");
    diag.button("{text.okay}");
    diag.show();

    // Initial state
    diag.setInputValue(opts.address);
    inputElem.val(Machinata.String.readAddressFromLocationString(opts.address));

    // Helper function update coords
    function updateCoords(newLatLon) {
        var newLocationString = Machinata.String.updateLatLonInLocationString(inputElem.val(), newLatLon);
        var newAddressString = Machinata.String.readAddressFromLocationString(newLocationString);
        inputElem.val(newAddressString);
        diag.setInputValue(newLocationString);
        console.log(newLocationString);
    }

    // Create map
    var mapOpts = {
        address: opts.address,
        doubleClickZoom: false,
        scrollWheelZoom: true
    };
    var map = Machinata.maps.createMap(mapElem, mapOpts);
    map.on("dblclick", function (event) {
        mapElem.trigger("update", {
            address: event.latlng.lat + "," + event.latlng.lng,
            callback: function (result) {
                updateCoords(result.lat + "," + result.lng);
                //inputElem.val(result.lat + "," + result.lng);
                //diag.setInputValue(result.lat + "," + result.lng);
            }
        });
    });
    // Bind interaction
    if (opts.readonly != true) {
        mapElem.on("markermove", function (event, args) {
            updateCoords(args.lat + "," + args.lng);
            //inputElem.val(args.lat + "," + args.lng);
            //diag.setInputValue(args.lat + "," + args.lng);
        });
    }
    inputElem.on("keypress", function (event) {
        if (event.which == 13) {
            mapElem.trigger("update", {
                address: inputElem.val(),
                callback: function (result) {
                    updateCoords(result.lat + "," + result.lng);
                    //inputElem.val(result.lat + "," + result.lng);
                    //diag.setInputValue(result.lat + "," + result.lng);
                }
            });
        }
    });

    return diag;
};




/// <summary>
/// Get a list of entities via an API call and displays them using ```Machinata.entityListingDialog```.
/// Parameters:
///  - ```apiCall```: defines the server endpoint to call for the entieis data. 
///    If the url has```{search}``` in it, the dialog box will automatically set the option 
///    ```apiCallForSearch``` to the same url and do an initial update with a blank filter. The server can choose if it should return
///    some initial entities for the empty search (smaller list) or return no entities for empty search (very large lists).
///  - ```opts```: defines a configuration object with all the different options
/// Options:
///  - ```entitiesKey```: defines the key where the array of entities is stored in the api call returned message data
/// See ```Machinata.entityListingDialog``` for further details and options
/// </summary>
/// <example>
/// ```
/// var diag = Machinata.entityListingDialogForAPICall("Select entity", "Please select an entity below", "/api/portal/easy-dfi/contents/list", {
///     entitiesKey: "contents",
///     entityIdKey: "public-id",
///     selectedEntityId: "hTKaaQ",
///     search: true,
/// });
/// diag.input(function (entity) {
///     console.log(entity); // this was selected
/// });
/// ```
/// </example>
Machinata.entityListingDialogForAPICall = function (title, message, apiCall, opts) {
    // Init diag, we need to already return this to the caller...
    var diag = Machinata.messageDialog(title, message);
    if (opts == null) opts = {};
    opts.diag = diag;
    if (opts.apiCallForSearch == null && apiCall.indexOf("{search}") > -1) {
        opts.apiCallForSearch = apiCall;
    }
    // Do API Call
    var url = apiCall.replace("{search}", "");
    var call = Machinata.apiCall(url);
    call.success(function (message) {
        var entities = message.data[opts.entitiesKey];
        Machinata.entityListingDialog(title, message, entities, opts);
    });
    call.genericError();
    call.send();
    // Return to caller
    return diag;
};

/// <summary>
/// Generic and flexible entity listing chooser dialog. 
/// Will automatically build a listing of choosable entities based on an array ```entities```.
/// Unless specifically specified, the dialog will automatically create a UI based on the properties
/// of the entity objects (except the id key).
/// Options:
///  - ```entityIdKey```: defines which key to use as the entity id
///  - ```selectedEntityId```: optional, defines which entity is currently selected
///  - ```properties```: optional, defines a custom properties to display, must be an arrow of objects with ```key``` and ```title```
///  - ```search```: optional, if true a search box will allow quick filtering
///  - ```multipleSelection```: optional, if true multiple items will be selectable
///  - ```returnEntityIds```: optional, if true instead of returning the entity object, entityIds will be returned
///  - ```allowClear```: optional, if true a clear button is added with return value 'clear'
///  - ```requireSearchForResults```: optional, if true a search is required before any results are shown
///  - ```apiCallForSearch```: optional, if set instead of searching using the provided entities the search is performed using a new result from the server where the parameter variable {search} can be used
/// </summary>
/// <example>
/// ```
/// var entities = [
///     {
///         "public-id": "hTKaaQ",
///         "name": "Test content 1",
///         "user": "hBcd9s",
///         "type": "Timetable"
///     },
///     {
///         "public-id": "4nj_RQ",
///         "name": "Test content 2",
///         "user": "hBcd9s",
///         "type": "Timetable"
///     },
///     {
///         "public-id": "z6bQIQ",
///         "name": "Test content 3",
///         "user": "hBcd9s",
///         "type": "Infoscreen"
///     }
/// ];
/// var diag = Machinata.entityListingDialog("Select entity", "Please select an entity below", entities, {
///     entityIdKey: "public-id", // defines which key should be used as the entity id
///     selectedEntityId: "4nj_RQ", // allows a pre-selection based on the id
///     search: true, // adds a realtime searchbox
///     header: true, // adds a header to the listing
///     properties: [ // defines a custom properties to display (in this case we dont include the user field)
///         {key:"name", title:"Name"},
///         {key:"type", title:"Type"}
///     ]
/// });
/// diag.input(function (entity) {
///     console.log(entity); // this is the object selected
/// });
/// diag.show();
/// ```
/// </example>
Machinata.entityListingDialog = function (title, message, entities, opts) {
    if (opts == null) opts = {};
    if (opts.header == null) opts.header = false;
    if (opts.entityIdKey == null) opts.entityIdKey = "public-id";
    if (opts.returnEntityIds == null) opts.returnEntityIds = false;
    if (opts.multipleSelection == null) opts.multipleSelection = false;
    if (opts.search == null) opts.search = false;
    if (opts.searchOnKeyStroke == null) opts.searchOnKeyStroke = false;
    if (opts.requireSearchForResults == null) opts.requireSearchForResults = false;
    if (opts.allowClear == null) opts.allowClear = false;
    if (opts.apiCallForSearch == null) opts.apiCallForSearch = null;
    if (opts.fullwindow == null) opts.fullwindow = true;

    // Validate
    if (opts.multipleSelection == true && opts.returnEntityIds == false && opts.apiCallForSearch != null) {
        // The problem here is that we might not have all the data to actually get the entity already, so if the user does nothing
        alert("WARNING: Machinata.entityListingDialog() opts.multipleSelection == true && opts.returnEntityIds == false && opts.apiCallForSearch != null is not supported!");
    }


    // Init initial selection
    var currentlySelectedIds = null;
    var currentlySelectedEntities = null;
    if (opts.selectedEntityId != null && opts.selectedEntityId != "") {
        currentlySelectedIds = opts.selectedEntityId.split(",");
        currentlySelectedEntities = []; //TODO: not really supported on lazy load api call...
    } else {
        currentlySelectedIds = [];
        currentlySelectedEntities = [];
    }
    
    // Setup dialog
    var diag = opts.diag;
    if (diag == null) diag = Machinata.messageDialog(title, message);

    if (opts.fullwindow == true) {
        diag.fullwindow();
    }

    // Create custom input
    var formElem = $("<div class='ui-form'><input type='text' placeholder='{text.search}'/></div>");
    var inputElem = formElem.find("input");

    // Create table
    var tableWrapperElem = $("<div class=''></div>");
    tableWrapperElem.css("width", "100%");
    if (opts.fullwindow != true) tableWrapperElem.css("height", "200px");
    tableWrapperElem.css("overflow-y", "auto");
    var tableElem = $("<table class='ui-table'><thead></thead><tbody></tbody></table>");
    tableElem.css("width", "100%");
    tableElem.appendTo(tableWrapperElem);
    var tableHeadElem = tableElem.find("thead");
    var tableBodyElem = tableElem.find("tbody");

    // Helper function for selecting/deselecting an item
    function updateSelectionForRow(rowElem,selected) {
        //currentlySelectedEntities;
        if (selected == true) {
            rowElem.addClass("selected");
            currentlySelectedIds.push(rowElem.data("key"));
            currentlySelectedEntities.push(rowElem.data("entity"));
        } else {
            rowElem.removeClass("selected");
            {
                var index = currentlySelectedIds.indexOf(rowElem.data("key"));
                currentlySelectedIds.splice(index, 1);
            }
            {
                var index = -1;
                for (var i = 0; i < currentlySelectedEntities.length; i++) {
                    if (currentlySelectedEntities[i][opts.entityIdKey] == rowElem.data("key")) index = i;
                }
                if(index != -1) currentlySelectedEntities.splice(index, 1);
            }
        }
        updateSelection();
    }
    function updateSelection() {
        if (opts.returnEntityIds == true) {
            diag.setInputValue(currentlySelectedIds.join(","));
        } else {
            if (opts.multipleSelection == true) {
                diag.setInputValue(currentlySelectedEntities);
            } else {
                diag.setInputValue(currentlySelectedEntities[0]);
            }
        }
        //console.log("currentlySelectedIds", currentlySelectedIds);
        //console.log("currentlySelectedEntities", currentlySelectedEntities);
    }


    // Helper function for building all entities rows
    function rebuildTableRows(entities) {
        // Remove all existing
        tableBodyElem.empty();

        // Auto properties?
        var props = opts.properties;
        if (props == null) {
            props = [];
            if (entities.length > 0) {
                Machinata.Util.each(entities[0], function (key, val) {
                    if (key == opts.entityIdKey) return;
                    props.push({
                        key: key,
                        title: key
                    });
                });
            }
        }

        // Header
        if (opts.header == true) {
            var rowElem = $("<tr></tr>");
            Machinata.Util.each(props, function (index, prop) {
                var columnElem = $("<th></th>");
                columnElem.text(prop.title);
                rowElem.append(columnElem);
            });
            tableHeadElem.empty();
            tableHeadElem.append(rowElem);
        }

        // Add all entity rows
        Machinata.Util.each(entities, function (index, entity) {
            var rowElem = $("<tr class='link'></tr>");
            // Columns
            var keywords = "";
            Machinata.Util.each(props, function (index, prop) {
                var columnElem = $("<td></td>");
                columnElem.text(entity[prop.key]);
                rowElem.append(columnElem);
                var keyword = entity[prop.key];
                if (keyword != null) {
                    keywords += keyword.toString().toLowerCase() + " ";
                }
            });
            rowElem.attr("data-key", entity[opts.entityIdKey]);
            rowElem.data("key", entity[opts.entityIdKey]);
            rowElem.data("entity", entity);
            rowElem.data("keywords", keywords);
            rowElem.click(function () {
                if (opts.multipleSelection == false) {
                    // Unselect all
                    $(this).closest("table").find("tr.selected").removeClass("selected");
                    currentlySelectedIds = [];
                    currentlySelectedEntities = [];
                    // Select this row
                    updateSelectionForRow($(this), true);
                } else {
                    // Just toggle this id
                    if ($(this).hasClass("selected"))   updateSelectionForRow($(this), false);
                    else                                updateSelectionForRow($(this), true);
                }
            });
            if (currentlySelectedIds != null && currentlySelectedIds.length > 0) {
                for (var i = 0; i < currentlySelectedIds.length; i++) {
                    if (currentlySelectedIds[i] == rowElem.data("key")) {
                        rowElem.addClass("selected");
                        currentlySelectedEntities.push(rowElem.data("entity"));
                    }
                }
            }
            if (opts.requireSearchForResults == true) rowElem.hide();
            tableBodyElem.append(rowElem);
        });
        updateNoResultsInfo(entities.length, "{text.list-empty}");
    }



    // No results
    var noResultsElem = $("<div><p></p></div>");
    noResultsElem.css("height", "200px");
    function updateNoResultsInfo(count, text) {
        if (count == 0) {
            tableWrapperElem.hide();
            noResultsElem.find("p").text(text);
            noResultsElem.show();
        } else {
            tableWrapperElem.show();
            noResultsElem.hide();
        }
    }

    // Initial rebuild
    rebuildTableRows(entities);
    updateSelection();
    

    // Search helper function
    function performSearch(search) {
        if (opts.apiCallForSearch != null) {
            performSearchOnServer(search);
        } else {
            performSearchInMemory(search);
        }
    }
    function performSearchOnServer(search) {
        // Do API Call
        var url = opts.apiCallForSearch.replace("{search}", encodeURIComponent(search));
        var call = Machinata.apiCall(url);
        call.success(function (message) {
            var entities = message.data[opts.entitiesKey];
            rebuildTableRows(entities);
        });
        call.genericError();
        call.send();
    }
    function performSearchInMemory(search) {
        Machinata.showLoading(true);
        if (search == null || search == "") {
            tableBodyElem.find("tr").show();
            updateNoResultsInfo(entities.length, "{text.list-empty}");
            Machinata.showLoading(false);
        } else {
            search = search.toLowerCase();
            var searchSegs = search.split(' ');
            var matches = 0;
            tableBodyElem.find("tr").each(function () {
                var rowElem = $(this);
                var allMatch = true;
                var keywords = rowElem.data("keywords");
                for (var i = 0; i < searchSegs.length; i++) {
                    if (keywords.indexOf(searchSegs[i]) == -1) {
                        allMatch = false;
                        break;
                    }
                }
                if (allMatch == true) { rowElem.show(); matches++ }
                else rowElem.hide();
            });
            updateNoResultsInfo(matches, "{text.search-empty}");
            Machinata.showLoading(false);
        }
    }

    // Bind interaction
    if (opts.search == true) {
        inputElem.on("keyup", function (event) {
            if (event.keyCode == 13) {
                performSearch(inputElem.val())
            }
        });
    }
    if (opts.search == true && opts.searchOnKeyStroke == true) {
        inputElem.on("input", function (event) {
            performSearch(inputElem.val())
        });
    }

    // Finalize diag
    diag.elem.append(formElem);
    diag.elem.append(tableWrapperElem);
    diag.elem.append(noResultsElem);
    diag.cancelButton("{text.cancel}");
    if(opts.allowClear == true) diag.button("{text.clear}", "clear");
    diag.button("{text.okay}");
    diag.show();

    return diag;
};


/// <summary>
/// 
/// </summary>
Machinata.copyLinkDialog = function (url, title, message, opts) {
    if (title == null) title = "{text.copy-link}";
    if (message == null) message = null;
    var diag = Machinata.messageDialog(title, message);
    diag.elem.append($("<pre class='link' style='font-size:1em;word-break:break-all;font-family:monospace'></pre>").text(url));
    diag.button("{text.copy}", "copy", function () {
        Machinata.Reporting.Tools.copyElementToClipboard(diag.elem.find(".link"), "{text.copy-link}", "{text.copy-link.success}");
        diag.close();
    }, false, false);
    diag.okayButton();
    diag.show();
};