


/// <summary>
/// Library for extending web forms to make them more powerful and user-friendly.
/// </summary>
/// <type>namespace</type>
Machinata.Forms = {};

/// <summary>
/// 
/// </summary>
/// <hidden/>
Machinata._onFormInputBinds = [];

/// <summary>
/// Register new form input binding routines through this method.
/// The function passed as an argument should have the following signature:
/// ```
/// function(propertyType, type, form, input) 
/// ```
/// Example:
/// ```
/// Machinata.onFormInputBind(function (propertyType, type, form, input) {
///   // Your code here...
/// });
/// ```
/// </summary>
Machinata.onFormInputBind = function (fn) {
    Machinata._onFormInputBinds.push(fn); //TODO: instead of push, insert at 0 to allow overriding?
};



/// <summary>
/// 
/// </summary>
/// <hidden/>
Machinata.onInit(function () {

    // Find all forms that need auto-bind on input-properties
    Machinata.Forms.bindFormBuilderUIForForms($("form.auto-bind-input-properties"));
    Machinata.Forms.bindApiCallForForms($("form[api-call]"));
 
    // Find submit buttons
    //$("form .submit-form").click(function () {
    //   $(this).closest("form").submit();
    //});
    $("form .submit-form").each(function () {
        var elem = $(this);
        var form = $(this).closest("form");
        if (elem.is("a")
            || elem.attr("type") == "submit" 
            || elem.attr("type") == "button") {
            // Button input (bind click)
            elem.click(function () {
                Machinata.debug("Click detected on .submit-form submit/a/button, will submit form...");
                form.submit();
            });
        } else {
            // Text input (bind enter)
            elem.keypress(function(event) {
                var keycode = event.keyCode || event.which;
                if (keycode == '13') {
                    Machinata.debug("Enter key detected on .submit-form, will submit form...");
                    form.submit();
                }
            });
        }
    });
    // Find submit buttons
    $("form[autocomplete='off']").each(function () {
        var form = $(this);
        setTimeout(function () {
            form.find("input[autocomplete='off']").val("");
        }, 100);
        
    });
    // Find all inputs with on change event
    $("input[api-call-on-change]").each(function () {
        var input = $(this);
        var apiCall = input.attr("api-call-on-change");
        input.on('change', function () {
            input.addClass("loading");
            var type = input.attr("type");
            var val = input.val();
            if (type == "checkbox") {
                //val = $(this).is(':checked');
                val = $(this).prop('checked');
            }
            if (type == "radio") {
                //val = $(this).is(':checked');
                val = $(this).prop('checked');
                var others = input.closest(".entity-list").find("input[type='radio']").not($(this));
                others.prop('checked', false);
                //others.button("refresh");
                others.trigger("selected");
            }
            // Don't do a API call if we dont ahve one (but do the UI update)
            if (String.isEmpty(apiCall)) {
                return;
            }
            // Init data to send
            var call = Machinata.apiCall(apiCall);
            var data = {};
            data["value"] = val;
            call.data(data);
            // Prepare api call
            call.success(function (message) {
                input.removeClass("loading");
            })
            call.error(function (message) {
                input.removeClass("loading");
                // Rollback value
                if (type == "checkbox") {
                    input.prop('checked', !val);
                    //input.button("refresh");
                    input.trigger("selected");
                } else if (type == "radio") {
                    //TODO reselect last item
                    input.prop('checked', false);
                    //input.button("refresh");
                    input.trigger("selected");
                }
            });
            call.genericError();
            // Send
            call.send();
        });


    });
    
});


/// <summary>
/// 
/// </summary>
Machinata.apiCallForForm = function (apiCall, form, onSuccess, onError) {
    // Intercept submit
    form.submit(function (e) {
        Machinata.debug("Machinata.apiCallForForm: intercepting submit...");
        // Stop browser
        e.preventDefault();
        // Do we have a validation function?
        // This will first execute, and if it doesnt pass then we abort the api call...
        var validateFunction = form.data("on-validate");
        if (validateFunction != null) {
            Machinata.debug("Machinata.apiCallForForm: executing pre-validation...");
            var validateReturn = validateFunction(form);
            Machinata.debug("Machinata.apiCallForForm: pre-validation: "+validateReturn);
            if (validateReturn == false) {
                Machinata.debug("Machinata.apiCallForForm: pre-validation failed, will abort.");
                return;
            }
        }
        // Init api call
        var call = Machinata.apiCall(apiCall);
        // Init data to send
        var nativeForm = this;
        var data = {};
        form.addClass("loading");
        form.find("input,textarea,select").each(function () {
            var type = $(this).attr("type");
            var propertyType = $(this).attr("data-property-type");
            var name = $(this).attr("name");
            if (!String.isEmpty(name)) {
                if (type == "file") {
                    // Special file upload (we only support one file per input)
                    var fileNative = $(this)[0];
                    if (fileNative.files.length > 0) {
                        call.attachFileInput(fileNative.files[0], name, fileNative.files[0].name);
                    }
                } else if (propertyType == "ContentNode") {
                    var editorElem = $(".content-editor[editor-bind-to-input='" + name + "']");
                    var editor = Machinata.Content.getContentEditor(editorElem);
                    var val = editor.compileContentAsJSON();
                    data[name] = JSON.stringify(val);
                } else if (propertyType == "DateTime" || propertyType == "DateRange") {
                    // Convert to UTCMS
                    var val = $(this).val();
                    var utcms = $(this).attr("data-property-universal-value");
                    if (utcms != null && utcms != "") {
                        val = utcms;
                        Machinata.debug("Using utcms for " + propertyType + " prop: " + val);
                    }
                    data[name] = utcms;
                } else if (propertyType == "Properties") {
                    var props = $("input[properties-field='" + name + "']");
                    var json = {};
                    props.each(function () {
                        var key = $(this).attr("properties-key");
                        var type = $(this).attr("data-property-type");
                        var val = $(this).val();
                        //if (val == "") val = null;
                        if (type == "Boolean" && val != null && val.toLowerCase() == "true") val = true;
                        else if (type == "Boolean" && val != null && val.toLowerCase() == "false") val = false;
                        else if (type == "Integer" && val != null) val = parseInt(val);
                        json[key] = val;
                    });
                    data[name] = JSON.stringify(json);
                } else {
                    // Just regular form
                    var val = $(this).val();
                    // Skip if the input is a checkbox and we are not checked
                    if (type == "checkbox" || type == "radio") {
                        if ($(this).prop('checked') == false) return;
                    }
                    // Exists already?
                    if (data[name] != null) {
                        if (data[name].constructor === Array) {
                            data[name].push(val);
                        } else {
                            var newArray = [];
                            newArray.push(data[name]);
                            newArray.push(val);
                            data[name] = newArray;
                        }
                    } else {
                        data[name] = val;
                    }
                }
            }
        });
        // Focused submit button with action?
        var submitButton = $(this).find("input[type=submit]:focus"); // Note: doesnt work on OSX!
        if (submitButton.length == 0) submitButton = $(this).find("input[type=submit]"); // fallback to default single button
        if (submitButton.length > 0) {
            if (!String.isEmpty(submitButton.attr("action"))) {
                data["action"] = submitButton.attr("action");
            }
        }
        call.data(data);
        // Prepare api call
        call.success(function (message) {
            form.removeClass("loading");
        })
        call.error(function (message) {
            form.removeClass("loading");
        });
        // Function handlers given via data variables?
        var successDataFunction = form.data("on-success");
        if (successDataFunction != null) call.success(function () {
            successDataFunction(form);
        }); 
        // Function handlers given via parameters?
        if (onSuccess) call.success(onSuccess);
        if (onError) call.error(onError);

        // Send
        call.send();
    });
};

/// <summary>
/// 
/// </summary>
Machinata.Forms.bindFormBuilderUIForForms = function (elements) {
    elements.each(function () {
        var form = $(this);
        var inputs = form.find("input").not(".bb-ui-bound").addClass("bb-ui-bound");
        function bindTextAttribute(input, nameFrom, nameTo, valueIfExists) {
            if (!String.isEmpty(input.attr(nameFrom))) {
                var val = input.attr(nameFrom);
                if (valueIfExists != null) val = valueIfExists;
                input.attr(nameTo, val);
            }
        };
        function bindBoolAttribute(input, nameFrom, nameTo, valueIfExists) {
            if (input.attr(nameFrom) == "true") {
                var val = input.attr(nameFrom);
                if (valueIfExists != null) val = valueIfExists;
                input.attr(nameTo, val);
            }
        };
        inputs.each(function () {
            // Bind all standard attributes to the standard html5 attributes
            var input = $(this);
            //bindTextAttribute(input, "data-property-min-length", "min-length");
            //bindTextAttribute(input, "data-property-max-length", "max-length");
            bindTextAttribute(input, "data-property-placeholder", "placeholder");
            bindBoolAttribute(input, "data-property-required", "required", "required");
            bindBoolAttribute(input, "data-property-disabled", "disabled", "disabled");
            bindBoolAttribute(input, "data-property-readonly", "disabled", "disabled");
            bindTextAttribute(input, "data-property-form-type", "type");
            bindTextAttribute(input, "data-property-format", "format"); //BUG: Browsers (webkit based) now validate this much more stricly, we disable it for now
            bindTextAttribute(input, "data-property-autocomplete", "autocomplete");
            if (!String.isEmpty(input.attr("data-property-tooltip"))) {
                form.find(".ui-label[data-form-name='" + input.attr("name") + "']").attr("title", input.attr("data-property-tooltip"));
                bindTextAttribute(input, "data-property-tooltip", "title");
            }
            bindTextAttribute(input, "data-property-min-value", "min");
            bindTextAttribute(input, "data-property-max-value", "max");
            // Do min/max len
            // pattern=".{3,}"  pattern=".{5,10}"   pattern=".{0}|.{5,10}"    pattern=".{0}|.{8,}"
            var req = input.attr("data-property-required");
            var minLen = input.attr("data-property-min-length");
            var maxLen = input.attr("data-property-max-length");
            var pattern = input.attr("data-property-pattern");
            if (pattern == null) pattern = "";
            //if (req != "true") pattern += ".{0}";
            if (!String.isEmpty(minLen) || !String.isEmpty(maxLen)) {
                if (pattern == "") pattern += "."; // Make sure this prefixes the pattern
                pattern += "{";
                if (!String.isEmpty(minLen)) pattern += minLen;
                pattern += ",";
                if (!String.isEmpty(maxLen)) pattern += maxLen;
                pattern += "}";
                input.attr("pattern", pattern);
            }
            // Call all binders to see if one can process the type...
            for (var i = 0; i < Machinata._onFormInputBinds.length; i++) {
                var ret = Machinata._onFormInputBinds[i](input.attr("data-property-type"), input.attr("data-property-form-type"), form, input);
                if (ret == true) break;
            }
        });
    });
};


/// <summary>
/// 
/// </summary>
Machinata.Forms.bindApiCallForForms = function (elements) {
    // Find all forms bound to a api-call
    elements.each(function () {
        var form = $(this);
        var apiCall = form.attr("api-call");
        var successCallback = form.attr("on-success");
        var errorCallback = form.attr("on-error");
        Machinata.apiCallForForm(apiCall, form, function (message) {
            Machinata.processCallback(successCallback, message);
        }, function (message) {
            if (String.isEmpty(errorCallback)) Machinata.apiErrorForGeneric(message.data.code, message.data.message);
            else Machinata.processCallback(errorCallback, message);
        });
    });
};


/// <summary>
/// 
/// </summary>
Machinata.Forms.currencyInput = function (elem, currency, locale, digits, zeroCents, min, max) {
    // Load settings
    if (currency == null) currency = elem.attr("data-currency");
    if (currency == null) currency = Machinata.Currency.DEFAULT_CURRENCY; // default
    if (locale == null) locale = elem.attr("data-locale");
    if (locale == null) locale = Machinata.Currency.DEFAULT_LOCALE; // default
    if (digits == null) digits = 0; // default
    // Attach to element
    elem.data("currency", currency);
    // Helper function
    function setFormattedValue() {
        // Get the value
        var val = elem.val();
        if (val == "") return null;
        val = val.replaceAll(elem.data("currency"), "");
        if (zeroCents != null) val = val.replaceAll(zeroCents, ".00");
        if (zeroCents != null) val = val.replaceAll(".-", ".00");
        if (zeroCents != null) val = val.replaceAll(".–", ".00");
        val = val.replaceAll("'", "");
        val = val.replaceAll("’", "");
        var rawVal = parseFloat(val);
        if (isNaN(rawVal)) rawVal = min;
        if (min != null && rawVal < min) rawVal = min;
        if (max != null && rawVal > max) rawVal = max;
        var newVal = Machinata.Currency.getFormattedString(rawVal, elem.data("currency"), locale, digits, zeroCents);
        elem.val(newVal);
        elem.attr("data-raw-value", rawVal);
        elem.data("raw-value", rawVal);
        return rawVal;
    }
    // Bind change and do initial update
    elem.on("change update", function () { setFormattedValue(); });
    setFormattedValue();
};

/// <summary>
/// 
/// </summary>
Machinata.Forms.percentInput = function (elem, min, max) {
    // Load settings
    
    // Helper function
    function setFormattedValue() {
        // Get the value
        var val = elem.val();
        if (val == "") return null;
        val = val.replaceAll("%", "");
        var rawVal = parseFloat(val);
        if (isNaN(rawVal)) rawVal = min;
        if (min != null && rawVal < min) rawVal = min;
        if (max != null && rawVal > max) rawVal = max;
        var newVal = rawVal + "%";
        elem.val(newVal);
        elem.attr("data-raw-value", rawVal);
        elem.data("raw-value", rawVal);
        return rawVal / 100.0;
    }
    // Bind change and do initial update
    elem.on("change update", function () { setFormattedValue(); });
    setFormattedValue();
};


/// <summary>
/// 
/// </summary>
Machinata.Forms.numberInput = function (elem, min, max, format, locale, opts) {
    // Load settings

    // Helper function
    function setFormattedValue() {
        // Get the value
        var val = elem.val();
        if (val == "") return null;
        var rawVal = parseFloat(val);
        if (isNaN(rawVal)) rawVal = min;
        if (min != null && rawVal < min) rawVal = min;
        if (max != null && rawVal > max) rawVal = max;
        var newVal = rawVal;
        if (format != null) {
            newVal = Machinata.Formatting.formatNumber(rawVal, format, locale, opts);
        }
        elem.val(newVal);
        elem.attr("data-raw-value", rawVal);
        elem.data("raw-value", rawVal);
        return rawVal;
    }
    // Bind change and do initial update
    elem.on("change update", function () { setFormattedValue(); });
    setFormattedValue();
};