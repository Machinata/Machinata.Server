/// <summary>
/// Extensions for automatically caching requests or objects.
/// </summary>


/// <summary>
/// </summary>
Machinata.CACHE_ENABLED = true;


/// <summary>
/// </summary>
/// <hidden/>
Machinata._resetCache = function (storage, prefix) {
    if (!storage) return null;
    if (prefix == null) {
        // All
        return storage.clear();
    } else {
        // Only those with the prefix
        var keysToRemove = [];
        for (var i = 0; i < storage.length; i++) {
            var key = storage.key(i);
            if (key.startsWith(prefix)) keysToRemove.push(key);
        }
        for (var i = 0; i < keysToRemove.length; i++) {
            var key = keysToRemove[i];
            storage.removeItem(key);
        }
    }
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata._getCache = function (storage, key) {
    if (!storage) return null;
    return JSON.parse(storage.getItem(key));
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata._setCache = function (storage, key, value) {
    if (!storage) return null;
    return storage.setItem(key, JSON.stringify(value));
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata._getOrSetCache = function (storage, key, generator, callback) {
    var ret = Machinata._getCache(storage, key);
    if (ret == null) {
        generator(function (val) {
            Machinata._setCache(storage, key, val);
            callback(val);
        });
    } else {
        callback(ret);
    }
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata._getOrSetCacheWithHashedKey = function (storage, type, key, generator, callback) {
    Machinata._getOrSetCache(storage, type + "_" + key.hashCode(), generator, callback);
};







/// <summary>
/// Resets the entire cache. If a prefix is given, then only the cache
/// items with a key with the prefix are removed.
/// Uses the browsers localStorage.
/// </summary>
Machinata.resetCache = function (prefix) {
    let storage = null;
    try {
        storage = window.localStorage;
    } catch (error) {
        console.warn("Machinata.resetCache: could not load localStorage: ",error);
    }
    if (storage == null) return;
    Machinata._resetCache(window.localStorage, prefix);
};

/// <summary>
/// Gets a value from the cache using a key.
/// Uses the browsers localStorage.
/// </summary>
Machinata.getCache = function (key) {
    let storage = null;
    try {
        storage = window.localStorage;
    } catch (error) {
        console.warn("Machinata.getCache: could not load localStorage: ", error);
    }
    if (storage == null) return null;
   return Machinata._getCache(window.localStorage, key);
};

/// <summary>
/// Sets a value in the cache using the key.
/// Uses the browsers localStorage.
/// </summary>
Machinata.setCache = function (key, value) {
    let storage = null;
    try {
        storage = window.localStorage;
    } catch (error) {
        console.warn("Machinata.setCache: could not load localStorage: ", error);
    }
    if (storage == null) return;
   return Machinata._setCache(window.localStorage, key, value);
};

/// <summary>
/// Helper method to automatically either get the cached value by key (if it exists),
/// or generate and set the cache value (if it does not exist).
/// Uses the browsers localStorage.
/// Example:
/// Machinata.getOrSetCacheWithHashedKey("GEOCODE", search, function (setCacheValue) {
///     // Generator
///     var val = calculateSomeComplicatedValue();
///     setCacheValue(val);
/// }, function (val) {
///     // Callback
///     val.doSomething();
/// });
/// </summary>
Machinata.getOrSetCache = function (key, generator, callback) {
    Machinata._getOrSetCache(window.localStorage, key, generator, callback);
};

/// <summary>
/// Same as ```Machinata.getOrSetCache```, but the key is automaticalled hashed using ```{type}_{key}```.
/// Uses the browsers localStorage.
/// </summary>
Machinata.getOrSetCacheWithHashedKey = function (type, key, generator, callback) {
    Machinata._getOrSetCacheWithHashedKey(window.localStorage, type, key, generator, callback);
};










/// <summary>
/// Resets the entire cache. If a prefix is given, then only the cache
/// items with a key with the prefix are removed.
/// Uses the browsers sessionStorage.
/// </summary>
Machinata.resetSessionCache = function (prefix) {
    Machinata._resetCache(window.sessionStorage, prefix);
};

/// <summary>
/// Gets a value from the cache using a key.
/// Uses the browsers sessionStorage.
/// </summary>
Machinata.getSessionCache = function (key) {
   return Machinata._getCache(window.sessionStorage, key);
};

/// <summary>
/// Sets a value in the cache using the key.
/// Uses the browsers sessionStorage.
/// </summary>
Machinata.setSessionCache = function (key, value) {
    return Machinata._setCache(window.sessionStorage, key, value);
};

/// <summary>
/// Helper method to automatically either get the cached value by key (if it exists),
/// or generate and set the cache value (if it does not exist).
/// Uses the browsers sessionStorage.
/// Example:
/// Machinata.getOrSetCacheWithHashedKey("GEOCODE", search, function (setCacheValue) {
///     // Generator
///     var val = calculateSomeComplicatedValue();
///     setCacheValue(val);
/// }, function (val) {
///     // Callback
///     val.doSomething();
/// });
/// </summary>
Machinata.getOrSetSessionCache = function (key, generator, callback) {
    Machinata._getOrSetCache(window.sessionStorage, key, generator, callback);
};

/// <summary>
/// Same as ```Machinata.getOrSetCache```, but the key is automaticalled hashed using ```{type}_{key}```.
/// Uses the browsers sessionStorage.
/// </summary>
Machinata.getOrSetSessionCacheWithHashedKey = function (type, key, generator, callback) {
    Machinata._getOrSetCacheWithHashedKey(window.sessionStorage, type, key, generator, callback);
};