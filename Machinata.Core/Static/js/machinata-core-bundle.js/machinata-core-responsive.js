

/// <summary>
/// An extensive library for handling responsive UI layouts using paremeterized
/// and easy to use attributes in markup.
/// Currently, this implementation uses inline attributes on responsive elements, however
/// it would be easily ported to use CSS in the document head.
///
/// To quickly force an update of responsive elements, use 
///    Machinata.Responsive.updateResponsiveElements();
/// </summary>
/// <type>namespace</type>
Machinata.Responsive = {};

/// <summary>
/// 
/// </summary>
Machinata.DISABLE_CORE_RESPONSIVE = false;

/// <summary>
/// 
/// </summary>
Machinata.Responsive.definitions = [];

/// <summary>
/// 
/// </summary>
Machinata.Responsive.media = [];

/// <summary>
/// 
/// </summary>
Machinata.Responsive.responsiveIds = {};

/// <summary>
/// 
/// </summary>
Machinata.Responsive.containerElem = null;

/// <summary>
/// 
/// </summary>
Machinata.Responsive.VIEWPORT_MAX_WIDTH_MOBILE = 580;

/// <summary>
/// 
/// </summary>
Machinata.Responsive.VIEWPORT_MAX_WIDTH_TABLET = 900;

/// <summary>
/// 
/// </summary>
Machinata.Responsive.FORCE_LAYOUT = null;

/// <summary>
/// 
/// </summary>
Machinata.Responsive.DATAURL_TRANSPARENT_GIF = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";

/// <summary>
/// 
/// </summary>
Machinata.Responsive.SNAP_TO_GRID = null;

/// <summary>
/// 
/// </summary>
Machinata.Responsive.currentLayout = "desktop";

/// <summary>
/// 
/// </summary>
Machinata.Responsive.verboseDebug = false;

/// <summary>
/// 
/// </summary>
Machinata.Responsive.isDesktopLayout = function () {
    return Machinata.Responsive.currentLayout == "desktop";
};

/// <summary>
/// 
/// </summary>
Machinata.Responsive.isTabletLayout = function () {
    return Machinata.Responsive.currentLayout == "tablet";
};

/// <summary>
/// 
/// </summary>
Machinata.Responsive.isMobileLayout = function () {
    return Machinata.Responsive.currentLayout == "mobile";
};

/// <summary>
/// 
/// </summary>
/// <hidden/>
Machinata.Responsive._onLayoutChange = [];

/// <summary>
/// 
/// </summary>
Machinata.Responsive.onLayoutChange = function (fn) {
    Machinata.Responsive._onLayoutChange.push(fn);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Responsive._onResize = [];

/// <summary>
/// 
/// </summary>
Machinata.Responsive.onResize = function (fn) {
    Machinata.Responsive._onResize.push(fn);
};


/// <summary>
/// </summary>
/// <hidden/>
Machinata.Responsive._onResized = [];

/// <summary>
/// 
/// </summary>
Machinata.Responsive.onResized = function (fn) {
    Machinata.Responsive._onResized.push(fn);
};

/// <summary>
/// 
/// </summary>
Machinata.Responsive.bindCoverImages = function (elements) {
    if (Machinata.DISABLE_CORE_RESPONSIVE == true) return;

    // Coverize images
    elements.find(".bb-cover-images img").addClass("bb-ui-bound").each(function () {
        var img = $(this);
        var src = img.attr("src");
        img.css("background-size", "cover");
        img.css("background-position", "50% 50%");
        img.css("background-image", "url(\"" + src + "\")");
        img.attr("src", Machinata.Responsive.DATAURL_TRANSPARENT_GIF);
    });
};

/// <summary>
/// 
/// </summary>
/// <hidden/>
Machinata.Responsive._registerResponsiveDefinition = function (definition) {

    if (definition.globalSelector == null) definition.globalSelector = true;

    function preparseDefinitionParam(name) {

        // Validate null
        if (definition[name] == null) definition[name] = {};
        // Validate if it is a shorthand definition
        else if ($.type(definition[name]) === "string") {
            var defaultVal = definition[name];
            definition[name] = {};
            definition[name]["default"] = defaultVal;
        }
        // Validate further shortcuts...
        if (definition[name + "-mobile"] != null) definition[name]["mobile"] = definition[name + "-mobile"]
        if (definition[name + "-tablet"] != null) definition[name]["tablet"] = definition[name + "-tablet"]
        if (definition[name + "-desktop"] != null) definition[name]["desktop"] = definition[name + "-desktop"]
        // Process replacement variables
        definition[name]["default"] = Machinata.Responsive._preparseSizeAttribute(definition[name]["default"]);
        definition[name]["mobile"] = Machinata.Responsive._preparseSizeAttribute(definition[name]["mobile"]);
        definition[name]["tablet"] = Machinata.Responsive._preparseSizeAttribute(definition[name]["tablet"]);
        definition[name]["desktop"] = Machinata.Responsive._preparseSizeAttribute(definition[name]["desktop"]);
        
    }
    // Max
    preparseDefinitionParam("max-width");
    preparseDefinitionParam("max-height");
    // Min
    preparseDefinitionParam("min-width");
    preparseDefinitionParam("min-height");
    // Top/Left
    preparseDefinitionParam("top");
    preparseDefinitionParam("left");
    // Margins
    preparseDefinitionParam("margin-left");
    preparseDefinitionParam("margin-right");
    // Aspect ratio
    preparseDefinitionParam("aspect-ratio");
    // Cover
    preparseDefinitionParam("cover");
    // Font size
    preparseDefinitionParam("font-size");

    Machinata.Responsive.definitions.push(definition);

};

/// <summary>
/// 
/// </summary>
Machinata.Responsive.registerResponsiveDefinitions = function (definitions) {

    for (var i = 0; i < definitions.length; i++) {
        var definition = definitions[i];
        Machinata.Responsive._registerResponsiveDefinition(definition);
    }

    Machinata.Responsive.detectNewResponsiveElements();
    Machinata.Responsive.updateResponsiveElements();

};

/// <summary>
/// 
/// </summary>
Machinata.Responsive.bindResponsiveElements = function (elements) {
    if (Machinata.DISABLE_CORE_RESPONSIVE == true) return;

    // Helper function for transposing the attributes to the definition structure:
    function populateDefinitionParamsForAttribute(name, elem, definition) {
        definition[name] = {};
        definition[name]["default"]     = elem.attr("responsive-" + name);
        definition[name]["mobile"]      = elem.attr("responsive-" + name + "-mobile");
        definition[name]["tablet"]      = elem.attr("responsive-" + name + "-tablet");
        definition[name]["desktop"]     = elem.attr("responsive-" + name + "-desktop");
    }

    elements.find(".bb-responsive").addClass("bb-ui-bound").each(function () {
        // Init
        var elem = $(this);
        // Do we have a ID?
        var responsiveId = elem.attr("responsive-id");
        if (!String.isEmpty(responsiveId)) {
            // Only process the element if it is unqiue id (ie, don't do the id's twice)
            if (Machinata.Responsive.responsiveIds[responsiveId] != null) return;
            // Register the id
            Machinata.Responsive.responsiveIds[responsiveId] = elem;
        }
        // Cached data?
        var definition = jQuery.data(elem, "responsive-definition");
        if (definition == null) {
            // Init
            definition = {};
            // Get selector
            //definition.self = elem; //TODO
            definition.bindingElem = elem; //TODO
            definition.selector = elem.attr("responsive-selector");
            if (definition.selector == "") definition.selector = null;
            if (elem.attr("responsive-global") == "true") definition.globalSelector = true;
            else definition.globalSelector = false;
            // Get elems
            definition.elems = null;
            // Max
            populateDefinitionParamsForAttribute("max-width", elem, definition);
            populateDefinitionParamsForAttribute("max-height", elem, definition);
            // Min
            populateDefinitionParamsForAttribute("min-width", elem, definition);
            populateDefinitionParamsForAttribute("min-height", elem, definition);
            // Top/Left
            populateDefinitionParamsForAttribute("top", elem, definition);
            populateDefinitionParamsForAttribute("left", elem, definition);
            // Margins
            populateDefinitionParamsForAttribute("margin-left", elem, definition);
            populateDefinitionParamsForAttribute("margin-right", elem, definition);
            // Aspect ratio
            populateDefinitionParamsForAttribute("aspect-ratio", elem, definition);
            // Cover
            populateDefinitionParamsForAttribute("cover", elem, definition);
            // Font size
            populateDefinitionParamsForAttribute("font-size", elem, definition);
            // Update
            jQuery.data(elem, "responsive-definition", definition);

        }
        // Register elemenet
        Machinata.Responsive._registerResponsiveDefinition(definition);
    });

    Machinata.Responsive.detectNewResponsiveElements();
    Machinata.Responsive.updateResponsiveElements();
};




/// <summary>
/// 
/// </summary>
Machinata.Responsive.bindResponsiveMedia = function (elements) {
    if (Machinata.DISABLE_CORE_RESPONSIVE == true) return;
    function convertAttributeToData(elem, name) {
        elem.data(name+"-default", elem.attr("data-"+name+"-default"));
        if (elem.data(name + "-default") == null) elem.data(name+"-default", elem.attr(name));
        elem.data(name + "-mobile", elem.attr("data-" + name + "-mobile"));
        elem.data(name + "-desktop", elem.attr("data-" + name + "-desktop"));
        elem.data(name + "-tablet", elem.attr("data-" + name + "-tablet"));
    }
    elements.find(".bb-responsive-media").addClass("bb-ui-bound").each(function () {
        // Init
        var elem = $(this);
        // Cache settings
        convertAttributeToData(elem, "style");
        convertAttributeToData(elem, "src");
        // Register elemenet
        Machinata.Responsive.media.push(elem);
    });
    // Monitor layout changes
    Machinata.Responsive.updateResponsiveMedia();
    Machinata.Responsive.onLayoutChange(function () {
        Machinata.Responsive.updateResponsiveMedia();
    });
};

/// <summary>
/// 
/// </summary>
Machinata.Responsive.updateResponsiveMedia = function () {
    // Skip if we have none
    if (Machinata.Responsive.media == null || Machinata.Responsive.media.length == 0) return;
    // Helper function
    function getAttributeForLayout(elem,name) {
        // Get layout specific attribute
        var attr = elem.data(name+"-"+Machinata.Responsive.currentLayout); // style-mobile
        if (attr != null && attr != "") return attr;
        // Get default
        return elem.data("style-default");
    }
    // Process all
    Machinata.debug("Machinata.Responsive: updateResponsiveMedia");
    for (var i = 0; i < Machinata.Responsive.media.length; i++) {
        // Init
        var elem = Machinata.Responsive.media[i];
        // Process style
        var style = getAttributeForLayout(elem, "style");
        if (style != null) {
            Machinata.debug("Machinata.Responsive: swapping media style to "+style);
            elem.attr("style", style);
        }
        // Process src
        var src = getAttributeForLayout(elem, "src");
        if (src != null) {
            Machinata.debug("Machinata.Responsive: swapping media src to " + src);
            elem.attr("src", src);
        }
    }
};




/// <summary>
/// 
/// </summary>
Machinata.Responsive.updateElementsForResponsiveElement = function (definition) {
    if (definition.selector == null) {
        definition.elems = definition.bindingElem; //self
    } else {
        if (definition.globalSelector == true) {
            definition.elems = $(definition.selector);
        } else {
            definition.elems = definition.bindingElem.find(definition.selector);
        }
    }
    return definition.elems;
};


/// <summary>
/// 
/// </summary>
Machinata.Responsive.detectNewResponsiveElements = function () {
    // Skip if we have none
    if (Machinata.Responsive.definitions == null || Machinata.Responsive.definitions.length == 0) return;
    // Process all
    for (var i = 0; i < Machinata.Responsive.definitions.length; i++) {
        //var elem = Machinata.Responsive.definitions[i];
        //var definition = jQuery.data(elem, "responsive-definition");
        var definition = Machinata.Responsive.definitions[i];
        Machinata.Responsive.updateElementsForResponsiveElement(definition);
    }
};


/// <summary>
/// 
/// </summary>
Machinata.Responsive._preparseSizeAttribute = function (attr) {
    if (attr == null || attr == "") return null;
    var ret = attr;
    var useAlternativeWindowHeightForMobiles = false; // Not nice on iOS
    ret = ret.replaceAll("{width}", "elem.width()");
    ret = ret.replaceAll("{height}", "elem.height()");
    ret = ret.replaceAll("{window.width}", "$(window).width()");
    if (useAlternativeWindowHeightForMobiles && Machinata.Device.isMobileOS()) {
        // https://stackoverflow.com/questions/14388367/mobile-safari-window-height-url-bar-discrepancy
        ret = ret.replaceAll("{window.height}", "window.innerHeight");
    } else {
        ret = ret.replaceAll("{window.height}", "$(window).height()");
    }
    ret = ret.replaceAll("{parent.width}", "elem.parent().width()");
    ret = ret.replaceAll("{parent.height}", "elem.parent().height()");
    ret = ret.replaceAll("{parent.floored-width}", "Math.floor(elem.parent().width())");
    ret = ret.replaceAll("{parent.floored-height}", "Math.floor(elem.parent().height())");
    ret = ret.replaceAll("{parent.even-width}", "Machinata.Math.makeEven(elem.parent().width())");
    ret = ret.replaceAll("{parent.even-height}", "Machinata.Math.makeEven(elem.parent().height())");
    ret = ret.replaceAll("{parent.snapped-width}", "Machinata.Math.makeEven(Machinata.Responsive.snapToGrid(elem.parent().width()))");
    ret = ret.replaceAll("{parent.snapped-height}", "Machinata.Math.makeEven(Machinata.Responsive.snapToGrid(elem.parent().height()))");
    ret = ret.replaceAll("{grandparent.width}", "elem.parent().parent().width()");
    ret = ret.replaceAll("{grandparent.height}", "elem.parent().parent().height()");
    ret = ret.replaceAll("{header.width}", "$('#header').width()");
    ret = ret.replaceAll("{header.height}", "$('#header').height()");
    ret = ret.replaceAll("{footer.width}", "$('#footer').width()");
    ret = ret.replaceAll("{footer.height}", "$('#footer').height()");
    ret = ret.replaceAll("{container.width}", "$('#container').width()");
    ret = ret.replaceAll("{container.height}", "$('#container').height()");
    ret = ret.replaceAll("{body.width}", "$('body').width()");
    ret = ret.replaceAll("{body.height}", "$('body').height()");
    return ret;
};



/// <summary>
/// 
/// </summary>
Machinata.Responsive.snapToGrid = function (val) {
    if (Machinata.Responsive.SNAP_TO_GRID != null) {
        val = Math.floor(val);
        return val - (val % Machinata.Responsive.SNAP_TO_GRID);
    } else {
        return val;
    }
};


/// <summary>
/// 
/// </summary>
Machinata.Responsive.getCSSStyleForAttribute = function (val) {
    if (typeof (val) == "number") {
        var ret = Math.floor(val);
        if (Machinata.Responsive.SNAP_TO_GRID != null) {
            ret = Machinata.Responsive.snapToGrid(ret);
        }
        return ret + "px";
    }
    else return val;
};


/// <summary>
/// 
/// </summary>
Machinata.Responsive.calculateSizeForAttribute = function (elem, rule) {
    if (rule == "none") return "";
    if (rule == "auto") return "auto";
    var ret = eval(rule); // Note: we use eval here with shortcuts to the parameter elem, so don't change that.
    return ret;
};


/// <summary>
/// 
/// </summary>
Machinata.Responsive.getParamForLayout = function (definition, name, layout) {
    if (layout == null) throw "Machinata.Responsive.getParamForLayout: layout is null";
    if (definition[name][layout] != null) return definition[name][layout];
    else return definition[name]["default"];
};


/// <summary>
/// Note: this method is run once before Machinata.init(), thus it should never access Machinata methods or properties that are dependent
/// on the init routine.
/// Note: this method should be extremely fast, as it needs to be run before the first render to avoid
/// flickering of elements that are dependent on the CSS body variables body.mobile, body.desktop etc...
/// </summary>
Machinata.Responsive.updateBodyTags = function () {
    if (document.body.clientWidth < Machinata.Responsive.VIEWPORT_MAX_WIDTH_MOBILE || Machinata.Responsive.FORCE_LAYOUT ==  "mobile") {
        // Mobile
        Machinata.Responsive.currentLayout = "mobile";
        Machinata.Responsive.containerElem
            .addClass("mobile")
            .addClass("tablet-or-mobile")
            .removeClass("desktop-or-tablet")
            .removeClass("desktop")
            .removeClass("tablet");
    } else if (document.body.clientWidth < Machinata.Responsive.VIEWPORT_MAX_WIDTH_TABLET || Machinata.Responsive.FORCE_LAYOUT == "tablet") {
        // Tablet
        Machinata.Responsive.currentLayout = "tablet";
        Machinata.Responsive.containerElem
            .addClass("tablet")
            .addClass("tablet-or-mobile")
            .addClass("desktop-or-tablet")
            .removeClass("desktop")
            .removeClass("mobile");
    } else {
        // Desktop
        Machinata.Responsive.currentLayout = "desktop";
        Machinata.Responsive.containerElem
            .addClass("desktop")
            .addClass("desktop-or-tablet")
            .removeClass("tablet-or-mobile")
            .removeClass("tablet")
            .removeClass("mobile");
    }
    Machinata.Responsive.containerElem.addClass("responsive-layout-set")
};


/// <summary>
/// 
/// </summary>
Machinata.Responsive.updateResponsiveElements = function () {
    // Mobile or tablet or desktop?
    var oldLayout = Machinata.Responsive.currentLayout;
    Machinata.Responsive.updateBodyTags();

    // Layout change?
    if (oldLayout != Machinata.Responsive.currentLayout) {
        Machinata.debug("Machinata.Responsive: layout change from " + oldLayout + " to " + Machinata.Responsive.currentLayout);
        for (var i = 0; i < Machinata.Responsive._onLayoutChange.length; i++) Machinata.Responsive._onLayoutChange[i]();
    }

    // Call resizes
    for (var i = 0; i < Machinata.Responsive._onResize.length; i++) Machinata.Responsive._onResize[i]();

    // Skip if we have none
    if (Machinata.Responsive.definitions != null && Machinata.Responsive.definitions.length != 0) {

        // Process all
        for (var i = 0; i < Machinata.Responsive.definitions.length; i++) {

            /*// Init
            var elem = Machinata.Responsive.definitions[i];
            // Get cached definition
            var definition = jQuery.data(elem, "responsive-definition");*/

            // Init
            var definition = Machinata.Responsive.definitions[i];
            //var elem = definition.bindingElem;

            // Elems cached?
            if (definition["allow-selector-caching"] == false) {
                Machinata.Responsive.updateElementsForResponsiveElement(definition);
            }


            if (Machinata.Responsive.verboseDebug == true) Machinata.debug("Machinata.Responsive: processing " + definition.selector);
            if (Machinata.Responsive.verboseDebug == true) Machinata.debug("  " + definition.elems.length + " elems");
            // Process each of the elements of the selector
            definition.elems.each(function () {
                var responsiveElem = $(this);


                // Get the layout to use
                var layout = Machinata.Responsive.currentLayout;

                // Get layout via definition callback?
                if (definition["layout-handler"] != null) {
                    layout = definition["layout-handler"](responsiveElem, definition);
                    if (layout == null) {
                        console.warn("***WARNING: Machinata.Responsive.updateResponsiveElements got null from definition layout-handler, falling back to page layout.")
                        layout = Machinata.Responsive.currentLayout;
                    }
                }

                // Set max widht and height
                var newWidth = null;
                var newHeight = null;
                var newTop = null;
                var newLeft = null;
                var newMarginLeft = null;
                var newMarginRight = null;
                var newAspectRatio = null;
                var paramMinWidth = Machinata.Responsive.getParamForLayout(definition, "min-width", layout);
                var paramMaxWidth = Machinata.Responsive.getParamForLayout(definition, "max-width", layout);
                if (paramMaxWidth != null) {
                    newWidth = Machinata.Responsive.calculateSizeForAttribute(responsiveElem, paramMaxWidth);
                    if (paramMinWidth != null) {
                        var minWidth = Machinata.Responsive.calculateSizeForAttribute(responsiveElem, paramMinWidth);
                        if (newWidth < minWidth) newWidth = minWidth;
                    }
                    if (paramMaxWidth != "elem.width()") responsiveElem.css("width", Machinata.Responsive.getCSSStyleForAttribute(newWidth));
                }
                var paramMinHeight = Machinata.Responsive.getParamForLayout(definition, "min-height", layout);
                var paramMaxHeight = Machinata.Responsive.getParamForLayout(definition, "max-height", layout);
                if (paramMaxHeight != null) {
                    newHeight = Machinata.Responsive.calculateSizeForAttribute(responsiveElem, paramMaxHeight);
                    if (paramMinHeight != null) {
                        var minHeight = Machinata.Responsive.calculateSizeForAttribute(responsiveElem, paramMinHeight);
                        if (newHeight < minHeight) newHeight = minHeight;
                    }
                    if (paramMaxHeight != "elem.height()") responsiveElem.css("height", Machinata.Responsive.getCSSStyleForAttribute(newHeight));
                }
                // Cover fill/fit
                var paramCover = Machinata.Responsive.getParamForLayout(definition, "cover", layout);
                var paramAspectRatio = Machinata.Responsive.getParamForLayout(definition, "aspect-ratio", layout);
                if (paramAspectRatio != null && paramCover != null) {
                    if (newAspectRatio == null) newAspectRatio = Machinata.Responsive.calculateSizeForAttribute(responsiveElem, paramAspectRatio);
                    var pw = responsiveElem.parent().width();
                    var ph = responsiveElem.parent().height();
                    var ear = newAspectRatio;
                    var par = pw / ph;
                    if (paramCover == "fill") {
                        if (ear < par) {
                            // Fit to width
                            newWidth = pw;
                            newHeight = responsiveElem.parent().width() / ear;
                        } else {
                            // Fit to height
                            newWidth = ear * responsiveElem.parent().height();
                            newHeight = ph;
                        }
                    } else if (paramCover == "fit") {
                        if (ear < par) {
                            // Fit to height
                            newWidth = ear * responsiveElem.parent().height();
                            newHeight = ph;
                        } else {
                            // Fit to width
                            newWidth = pw;
                            newHeight = responsiveElem.parent().width() / ear;
                        }
                    }
                    // Center
                    var newX = pw / 2 - newWidth / 2;
                    var newY = ph / 2 - newHeight / 2;
                    // Set
                    responsiveElem.css("left", Machinata.Responsive.getCSSStyleForAttribute(newX));
                    responsiveElem.css("top", Machinata.Responsive.getCSSStyleForAttribute(newY));
                    responsiveElem.css("width", Machinata.Responsive.getCSSStyleForAttribute(newWidth));
                    responsiveElem.css("height", Machinata.Responsive.getCSSStyleForAttribute(newHeight));
                }
                if (paramAspectRatio != null && newWidth == null) {
                    if (newAspectRatio == null) newAspectRatio = Machinata.Responsive.calculateSizeForAttribute(responsiveElem, paramAspectRatio);
                    newWidth = newAspectRatio * newHeight;
                    responsiveElem.css("width", Machinata.Responsive.getCSSStyleForAttribute(newWidth));
                }
                if (paramAspectRatio != null && newHeight == null) {
                    if (newAspectRatio == null) newAspectRatio = Machinata.Responsive.calculateSizeForAttribute(responsiveElem, paramAspectRatio);
                    newHeight = newWidth / newAspectRatio;
                    responsiveElem.css("height", Machinata.Responsive.getCSSStyleForAttribute(newHeight));
                }
                // Top
                var paramTop = Machinata.Responsive.getParamForLayout(definition, "top", layout);
                if (paramTop != null) {
                    newTop = Machinata.Responsive.calculateSizeForAttribute(responsiveElem, paramTop);
                    responsiveElem.css("top", Machinata.Responsive.getCSSStyleForAttribute(newTop));
                }
                // Left
                var paramLeft = Machinata.Responsive.getParamForLayout(definition, "left", layout);
                if (paramLeft != null) {
                    newLeft = Machinata.Responsive.calculateSizeForAttribute(responsiveElem, paramLeft);
                    responsiveElem.css("left", Machinata.Responsive.getCSSStyleForAttribute(newLeft));
                }
                // Margins
                var paramMarginLeft = Machinata.Responsive.getParamForLayout(definition, "margin-left", layout);
                if (paramMarginLeft != null) {
                    newMarginLeft = Machinata.Responsive.calculateSizeForAttribute(responsiveElem, paramMarginLeft);
                    responsiveElem.css("margin-left", Machinata.Responsive.getCSSStyleForAttribute(newMarginLeft));
                }
                var paramMarginRight = Machinata.Responsive.getParamForLayout(definition, "margin-right", layout);
                if (paramMarginRight != null) {
                    newMarginRight = Machinata.Responsive.calculateSizeForAttribute(responsiveElem, paramMarginRight);
                    responsiveElem.css("margin-right", Machinata.Responsive.getCSSStyleForAttribute(newMarginRight));
                }
                // Font
                var paramFontSize = Machinata.Responsive.getParamForLayout(definition, "font-size", layout);
                if (paramFontSize != null) {
                    var newFontSize = Machinata.Responsive.calculateSizeForAttribute(responsiveElem, paramFontSize);
                    responsiveElem.css("font-size", Machinata.Responsive.getCSSStyleForAttribute(newFontSize));
                }
                // Sanity
                if (newWidth < 1) console.warn("A element for responsive defintion " + definition.id + " has new width of " + newWidth);
                if (newHeight < 1) console.warn("A element for responsive defintion " + definition.id + " has new height of " + newHeight);
            });
        }
    }



    // Call post-resizes
    for (var i = 0; i < Machinata.Responsive._onResized.length; i++) Machinata.Responsive._onResized[i]();

};

/// <summary>
/// 
/// </summary>
/// <hidden/>
Machinata.Responsive.preInit = function () {
    // Register some permamant settings
    Machinata.Responsive.containerElem = $("body");
    // Update body tags
    Machinata.Responsive.updateBodyTags();
    // Update touch settings
    if (Machinata.Device.isTouchEnabled() == true) {
        Machinata.Responsive.containerElem.addClass("touch-enabled");
    } else {
        Machinata.Responsive.containerElem.addClass("not-touch-enabled");
    }
};

/// <summary>
/// The pre-init is called as soon as the DOM is ready
/// </summary>
/// <hidden/>
$(document).ready(function () {
    Machinata.Responsive.preInit();
});

/// <summary>
/// 
/// </summary>
/// <hidden/>
Machinata.onInit(function () {
    if (Machinata.DISABLE_CORE_RESPONSIVE == true) return;

    // Register bind events
    Machinata.UI.onUIBind(Machinata.Responsive.bindResponsiveMedia);
    Machinata.UI.onUIBind(Machinata.Responsive.bindCoverImages);
    Machinata.UI.onUIBind(Machinata.Responsive.bindResponsiveElements);

    // Hook on resize
    $(window).resize(function () {
        if (Machinata.DISABLE_CORE_RESPONSIVE == true) return;
        Machinata.Responsive.updateResponsiveElements();
    });
});