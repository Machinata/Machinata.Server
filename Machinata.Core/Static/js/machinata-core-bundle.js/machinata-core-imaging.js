

/// <summary>
/// Collection of powerful imaging tools.
/// </summary>
/// <type>namespace</type>
Machinata.Imaging = {};

/// <summary>
/// 
/// </summary>
Machinata.Imaging.createDrawingCanvas = function(width, height, canvasEngine) {
    if (typeof (require) !== "undefined") {
        // NodeJS environment
        var canvasLib = require('canvas');
        if (canvasEngine == "png") canvasEngine = null;
        if (canvasEngine == "jpg") canvasEngine = null;
        if (canvasEngine == null) {
            return new canvasLib.Canvas(width, height);
        } else {
            return new canvasLib.Canvas(width, height, canvasEngine);
        }
    } else {
        // Browser DOM
        if (canvasEngine != null) throw "Machinata.Imaging.createDrawingCanvas: canvasEngine is not supported on browsers."
        var canvas = document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;
        return canvas;
    }
};


/// <summary>
/// 
/// </summary>
Machinata.Imaging.measureText = function (text, font, size, context, allowMetricsCalibration) {
    // Init
    if (allowMetricsCalibration == null) allowMetricsCalibration = true;
    if (context == null) {
        var canvas = Machinata.Imaging.createDrawingCanvas(1, 1);
        context = canvas.getContext('2d');
    }
    var fontString = size+"px " + font;
    context.font = fontString;
    // Perform measurement
    var metrics = context.measureText(text);
    return metrics;
};


/// <summary>
/// 
/// </summary>
Machinata.Imaging.getCalibrationMetricsForFont = function (fontName, opts) {
    // Init
    if (opts == null) opts = {};
    if (opts.calibrationText == null) opts.calibrationText = "abcdefghijklmnopqstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()";
    var results = {};

    var measureCanvas = Machinata.Imaging.createDrawingCanvas(1, 1, opts.canvasEngine);
    var measureContext = measureCanvas.getContext('2d');

    for (var i = 6; i <= 24; i++) {
        // Initial measurement
        var fontSize = i;
        var initialMetrics = Machinata.Imaging.measureText(opts.calibrationText, fontName, fontSize, measureContext);
        console.log(initialMetrics);
        var fontString = fontSize + "px " + fontName;
        console.log(opts.calibrationText);
        console.log(fontString);
        // Create canvas for actual draw
        var paddingPixels = 20;
        var drawingWidth = initialMetrics.width;
        var drawingHeight = initialMetrics.actualBoundingBoxAscent + initialMetrics.actualBoundingBoxDescent;
        console.log(drawingWidth, drawingHeight);
        drawingWidth = drawingWidth + +paddingPixels*2;
        drawingHeight = drawingHeight + paddingPixels*2;
        console.log(drawingWidth, drawingHeight);
        drawingWidth = Math.ceil(drawingWidth);
        drawingHeight = Math.ceil(drawingHeight);
        console.log(drawingWidth, drawingHeight);
        console.log("will create drawing canvas...")
        var drawingCanvas = Machinata.Imaging.createDrawingCanvas(drawingWidth, drawingHeight, opts.canvasEngine);
        var drawingContext = drawingCanvas.getContext('2d');
        drawingContext.font = fontString;
        drawingContext.textBaseline = 'top';

        // Draw intial meas box
        drawingContext.lineWidth = "1px";
        drawingContext.strokeStyle = "red";
        drawingContext.beginPath();
        drawingContext.rect(paddingPixels, paddingPixels, Math.ceil(initialMetrics.width), Math.ceil(initialMetrics.actualBoundingBoxAscent + initialMetrics.actualBoundingBoxDescent));
        drawingContext.stroke();

        // Draw baseline
        drawingContext.strokeStyle = "blue";
        drawingContext.beginPath();
        drawingContext.moveTo(paddingPixels, paddingPixels + Math.ceil(initialMetrics.actualBoundingBoxAscent));
        drawingContext.lineTo(paddingPixels + Math.ceil(initialMetrics.width), paddingPixels + Math.ceil(initialMetrics.actualBoundingBoxAscent));
        drawingContext.stroke();

        // Draw text
        drawingContext.fillText(opts.calibrationText, paddingPixels, paddingPixels);


        // Now scan all pixels
        var minBlackX = drawingWidth;
        var maxBlackX = 0;
        for (var px = 0; px < drawingWidth; px++) {
            for (var py = 0; py < drawingHeight; py++) {
                var p = drawingContext.getImageData(px, py, 1, 1).data; // [r,g,b,a]
                if (p[0] == 0 && p[1] == 0 && p[2] == 0 && p[3] > 0) {
                    // black (text)
                    if (px < minBlackX) minBlackX = px;
                    if (px > maxBlackX) maxBlackX = px;
                }
            }
        }
        console.log(minBlackX, maxBlackX);

        // Draw min/max
        drawingContext.strokeStyle = "green";
        drawingContext.beginPath();
        drawingContext.moveTo(minBlackX, 0);
        drawingContext.lineTo(minBlackX, drawingHeight);
        drawingContext.stroke();
        drawingContext.beginPath();
        drawingContext.moveTo(maxBlackX, 0);
        drawingContext.lineTo(maxBlackX, drawingHeight);
        drawingContext.stroke();

        var adjustedMetrics = {
            width: maxBlackX - minBlackX

        };
        var delta = adjustedMetrics.width - initialMetrics.width;

        console.log("width api", initialMetrics.width);
        console.log("width scan", adjustedMetrics.width);
        console.log("delta", delta);

        // Track results
        results[fontString] = {
            apiWidth: initialMetrics.width,
            scanWidth: adjustedMetrics.width,
            delta: delta
        };

        // Show what happened
        if (opts.addCanvasesToSelector != null) {
            $(opts.addCanvasesToSelector).append(drawingCanvas);
        }

        // Write file to disk
        if (opts.addCanvasesToDir != null) {
            // Only supported on NodeJS
            var filetype = opts.canvasEngine || "png";
            var fs = require('fs');
            var filepath = opts.addCanvasesToDir + '/FontCalibration_' + fontString + '_delta' + delta + '.' + filetype;
            var out = fs.createWriteStream(filepath);
            if (filetype == "png") {
                var stream = drawingCanvas.createPNGStream();
                stream.pipe(out);
            } else if (filetype == "pdf") {
                console.log("will create pdf " + filepath);
                fs.writeFileSync(filepath, drawingCanvas.toBuffer());
            } else if (filetype == "svg") {
                console.log("will create svg " + filepath);
                fs.writeFileSync(filepath, drawingCanvas.toBuffer());
            } 
        }

    }

    // Results
    console.log(results);
    return results;
};


/// <summary>
/// 
/// </summary>
Machinata.Imaging.loadImageWithCallback = function (src, containerElem, callbackSingle, callbackAll) {
    // Init
    if (containerElem == null) {
        containerElem = $("#ma-load-image-with-callback-container");
        if (containerElem == null) containerElem = $("<div id='ma-load-image-with-callback-container' style='display:none;'></div>").appendTo("body");
    }
    var imgElem = $("<img style='display:none;'/>");
    containerElem.append(imgElem);
    Machinata.Imaging._loadImageWithCallbackLookupTable[src] = false;
    imgElem[0].onload = function () {
        Machinata.Imaging._loadImageWithCallbackLookupTable[src] = true;
        if (callbackSingle != null) callbackSingle(imgElem);
        if (callbackAll != null) {
            var allLoaded = true;
            Machinata.Util.each(Machinata.Imaging._loadImageWithCallbackLookupTable, function (key) {
                var val = Machinata.Imaging._loadImageWithCallbackLookupTable[key];
                if (val == false) allLoaded = false;
            });
            if (allLoaded == true) callbackAll(imgElem);
        }
    };
    imgElem[0].src = src;
    return imgElem;
};

/// <summary>
/// 
/// </summary>
Machinata.Imaging._loadImageWithCallbackLookupTable = {};

/// <summary>
/// 
/// </summary>
Machinata.Imaging.testImagePixel = function (imgObject, x, y) {
    // Init
    if (x == null) x = 0;
    if (y == null) y = 0;
    var canvas = document.createElement("canvas");
    var ww = imgObject.width;
    var hh = imgObject.height;
    canvas.width = ww;
    canvas.height = hh;
    // Get the requested pixel
    var ctx = canvas.getContext("2d");
    ctx.drawImage(imgObject, 0, 0, ww, hh);
    var pixel = ctx.getImageData(x, y, 1, 1).data; // [r,g,b,a]
    return pixel;
};

/// <summary>
/// 
/// </summary>
Machinata.Imaging.testImageTransparence = function (imgObject, tolerance) {
    // Init
    if (tolerance == null) tolerance = 0;
    var canvas = document.createElement("canvas");
    var ww = imgObject.width;
    var hh = imgObject.height;
    canvas.width = ww;
    canvas.height = hh;

    // Draw to canvas
    var ctx = canvas.getContext("2d");
    ctx.drawImage(imgObject, 0, 0, ww, hh);

    // Get data
    var map = ctx.getImageData(0, 0, ww, hh);
    var rawdata = map.data;

    // Convert to grayscale
    var r, g, b, a;
    var total = 0;
    for (var p = 0, len = rawdata.length; p < len; p += 4) {
        //r = rawdata[p]
        //g = rawdata[p + 1];
        //b = rawdata[p + 2];
        a = rawdata[p + 3];
        if(a <= tolerance) total += 1;
    }
    return total;
};

/// <summary>
/// 
/// </summary>
Machinata.Imaging.changeColor = function (imgObject, imgColor) {
    // Init
    var canvas = document.createElement("canvas");
    var ww = imgObject.width;
    var hh = imgObject.height;
    canvas.width = ww;
    canvas.height = hh;

    // Draw to canvas
    var ctx = canvas.getContext("2d");
    ctx.drawImage(imgObject, 0, 0, ww, hh);

    // Get data
    var map = ctx.getImageData(0, 0, ww, hh);
    var rawdata = map.data;

    // Convert to grayscale
    var r, g, b, avg;
    for (var p = 0, len = rawdata.length; p < len; p += 4) {
        r = rawdata[p]
        g = rawdata[p + 1];
        b = rawdata[p + 2];
        avg = Math.floor((r + g + b) / 3);
        rawdata[p] = rawdata[p + 1] = rawdata[p + 2] = avg;
    }
    ctx.putImageData(map, 0, 0);

    // Overlay using source-atop
    ctx.globalCompositeOperation = "source-atop"
    ctx.fillStyle = imgColor;
    ctx.fillRect(0, 0, ww, hh);

    // Return
    return canvas;
};


/// <summary>
/// Use the two last parameters to offset the image:
///     var offsetX = 0.5;   // center x
///     var offsetY = 0.5;   // center y
/// Original By Ken Fyrstenberg Nilsen
/// Via https://stackoverflow.com/questions/21961839/simulation-background-size-cover-in-canvas/21961894#21961894
/// </summary>
Machinata.Imaging.drawImageCoverFill = function(ctx, imgObject, x, y, w, h, offsetX, offsetY) {
    // Init
    if (arguments.length === 2) {
        x = y = 0;
        w = ctx.canvas.width;
        h = ctx.canvas.height;
    }
    offsetX = typeof offsetX === "number" ? offsetX : 0.5;
    offsetY = typeof offsetY === "number" ? offsetY : 0.5;

    // keep bounds [0.0, 1.0]
    if (offsetX < 0) offsetX = 0;
    if (offsetY < 0) offsetY = 0;
    if (offsetX > 1) offsetX = 1;
    if (offsetY > 1) offsetY = 1;

    var iw = imgObject.width,
        ih = imgObject.height,
        r = Math.min(w / iw, h / ih),
        nw = iw * r,   // new prop. width
        nh = ih * r,   // new prop. height
        cx, cy, cw, ch, ar = 1;

    // Decide which gap to fill    
    if (nw < w) ar = w / nw;
    if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh;  // updated
    nw *= ar;
    nh *= ar;

    // Position
    cw = iw / (nw / w);
    ch = ih / (nh / h);
    cx = (iw - cw) * offsetX;
    cy = (ih - ch) * offsetY;

    // Validate
    if (cx < 0) cx = 0;
    if (cy < 0) cy = 0;
    if (cw > iw) cw = iw;
    if (ch > ih) ch = ih;

    // Draw
    ctx.drawImage(imgObject, cx, cy, cw, ch, x, y, w, h);
};


/// <summary>
/// 
/// </summary>
Machinata.Imaging.drawImageContainFit = function (ctx, imgObject, x, y, w, h) {
    // Init
    if (arguments.length === 2) {
        x = y = 0;
        w = ctx.canvas.width;
        h = ctx.canvas.height;
    }
    var newImageAR = w/h;
    var overlayAR = imgObject.width / imgObject.height;
    var newOverlayW = imgObject.width;
    var newOverlayH = imgObject.height;

    // Scale to fit
    if(overlayAR > newImageAR) {
        // Fit to width
        newOverlayW = w;
        newOverlayH = newOverlayW / overlayAR;
    } else {
        // Fit to height
        newOverlayH = h;
        newOverlayW = overlayAR * newOverlayH;
    }

    // Position
    var cx = x + (w/2.0 - newOverlayW/2.0); 
    var cy = y + (h/2.0 - newOverlayH/2.0); 
    var cw = newOverlayW; 
    var ch = newOverlayH;
    var iw = imgObject.width;
    var ih = imgObject.height;
       
    // Clip bounds
    if (cx < 0) cx = 0;
    if (cy < 0) cy = 0;
    if (cw > iw) cw = iw;
    if (ch > ih) ch = ih;

    // Draw
    ctx.drawImage(imgObject, 0, 0, iw, ih, cx, cy, cw, ch);
};

/// <summary>
/// 
/// </summary>
Machinata.Imaging.wrapText = function (ctx, text, x, y, maxWidth, lineHeight) {
    // From https://www.html5canvastutorials.com/tutorials/html5-canvas-wrap-text-tutorial/
    var words = text.split(' ');
    var line = '';
    for (var n = 0; n < words.length; n++) {
        var testLine = line + words[n] + ' ';
        var metrics = ctx.measureText(testLine);
        var testWidth = metrics.width;
        if (testWidth > maxWidth && n > 0) {
            ctx.fillText(line, x, y);
            line = words[n] + ' ';
            y += lineHeight;
        } else {
            line = testLine;
        }
    }
    ctx.fillText(line, x, y);
};


/// <summary>
/// 
/// </summary>
Machinata.Imaging.exportSVGasPNG = function (svgElem, title) {
    // via https://stackoverflow.com/questions/3975499/convert-svg-to-image-jpeg-png-etc-in-the-browser
    downloadSvg(svgElem[0], title);
    function copyStylesInline(destinationNode, sourceNode) {
        var containerElements = ["svg", "g"];
        for (var cd = 0; cd < destinationNode.childNodes.length; cd++) {
            var child = destinationNode.childNodes[cd];
            if (containerElements.indexOf(child.tagName) != -1) {
                copyStylesInline(child, sourceNode.childNodes[cd]);
                continue;
            }
            var style = sourceNode.childNodes[cd].currentStyle || window.getComputedStyle(sourceNode.childNodes[cd]);
            if (style == "undefined" || style == null) continue;
            for (var st = 0; st < style.length; st++) {
                child.style.setProperty(style[st], style.getPropertyValue(style[st]));
            }
        }
    }

    function triggerDownload(imgURI, fileName) {
        var evt = new MouseEvent("click", {
            view: window,
            bubbles: false,
            cancelable: true
        });
        var a = document.createElement("a");
        a.setAttribute("download", fileName);
        a.setAttribute("href", imgURI);
        a.setAttribute("target", '_blank');
        a.dispatchEvent(evt);
    }

    function downloadSvg(svg, fileName) {
        var copy = svg.cloneNode(true);
        copyStylesInline(copy, svg);
        var canvas = document.createElement("canvas");
        var bbox = svg.getBBox();
        canvas.width = bbox.width + 30; //TODO: some sort of styling bug
        canvas.height = bbox.height + 30; //TODO: some sort of styling bug
        canvas.width = svgElem.width();
        canvas.height = svgElem.height();
        var ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, bbox.width, bbox.height);
        var data = (new XMLSerializer()).serializeToString(copy);
        var DOMURL = window.URL || window.webkitURL || window;
        var img = new Image();
        var svgBlob = new Blob([data], { type: "image/svg+xml;charset=utf-8" });
        var url = DOMURL.createObjectURL(svgBlob);
        img.onload = function () {
            ctx.drawImage(img, 0, 0);
            DOMURL.revokeObjectURL(url);
            if (typeof navigator !== "undefined" && navigator.msSaveOrOpenBlob) {
                var blob = canvas.msToBlob();
                navigator.msSaveOrOpenBlob(blob, fileName);
            }
            else {
                var imgURI = canvas
                    .toDataURL("image/png")
                    .replace("image/png", "image/octet-stream");
                triggerDownload(imgURI, fileName);
            }
            document.removeChild(canvas);
        };
        img.src = url;
    }
};
