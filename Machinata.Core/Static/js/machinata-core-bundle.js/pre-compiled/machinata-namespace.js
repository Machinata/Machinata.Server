var MachinataJS = window["MACHINATA"];
/// <summary>
/// ***WORK IN PROGRESS***
/// New TS root namespace...
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
var Machinata;
(function (Machinata) {
    Machinata.TYPESCRIPT_ENABLED = true;
    //export var UI = MachinataJS.UI;
    Machinata.Math = MachinataJS.Math;
    Machinata.Responsive = MachinataJS.Responsive;
})(Machinata || (Machinata = {}));
//Machinata.UI = window["MACHINATA"].UI;
//Machinata.Math = window["MACHINATA"].Math;
