/// <summary>
/// </summary>
/// <namespace></namespace>
/// <name>Machinata.ModelObject</name>
/// <type>namespace</type>
var Machinata;
(function (Machinata) {
    var Core;
    (function (Core) {
        var ModelObject = /** @class */ (function () {
            function ModelObject() {
                this.Type = "ModelObject";
            }
            ModelObject.prototype.fromJSON = function (json) {
                if (json["PublicId"] != null) {
                    this.PublicId = json["PublicId"];
                }
                else {
                    this.PublicId = null;
                }
                if (json["Created"] != null) {
                    this.Created = new Date(json["Created"]);
                }
                else {
                    this.Created = null;
                }
            };
            ModelObject.prototype.toJSON = function () {
                var json = {};
                json["Type"] = this.Type;
                if (this.PublicId != null) {
                    json["PublicId"] = this.PublicId;
                }
                else
                    json["PublicId"] = null;
                if (this.Created != null) {
                    json["Created"] = this.Created.getTime();
                }
                else
                    json["Created"] = null;
                return json;
            };
            return ModelObject;
        }());
        Core.ModelObject = ModelObject;
    })(Core = Machinata.Core || (Machinata.Core = {}));
})(Machinata || (Machinata = {}));
(function (Machinata) {
    var Core;
    (function (Core) {
        var Model;
        (function (Model) {
            var ContentFileAsset = /** @class */ (function () {
                function ContentFileAsset(sourceURL, assetURL, hash) {
                    this.SourceURL = sourceURL;
                    this.AssetURL = assetURL;
                    this.Hash = hash;
                }
                ContentFileAsset.prototype.fromJSON = function (json) {
                    if (json["SourceURL"] != null)
                        this.SourceURL = json["SourceURL"];
                    if (json["AssetURL"] != null)
                        this.AssetURL = json["AssetURL"];
                    if (json["Hash"] != null)
                        this.Hash = json["Hash"];
                };
                ContentFileAsset.prototype.toJSON = function () {
                    var json = {};
                    json["SourceURL"] = this.SourceURL;
                    json["AssetURL"] = this.AssetURL;
                    json["Hash"] = this.Hash;
                    return json;
                };
                return ContentFileAsset;
            }());
            Model.ContentFileAsset = ContentFileAsset;
            var Price = /** @class */ (function () {
                function Price(value, currency) {
                    this.Value = value;
                    this.Currency = currency;
                }
                Price.prototype.toString = function () {
                    var currencyString = this.Currency;
                    var valueString = this.Value != null ? this.Value.toFixed(2) : null;
                    var ret = "";
                    if ((valueString == null || valueString == "") && (currencyString == null || currencyString == "")) {
                        ret = "";
                    }
                    else if (currencyString == null || currencyString == "") {
                        ret = "".concat(valueString);
                    }
                    else if (valueString == null || valueString == "") {
                        ret = "";
                    }
                    else {
                        ret = "".concat(currencyString, " ").concat(valueString);
                    }
                    if (this.Currency == "CHF") {
                        ret = ret.replace("0.00", "0.–");
                        ret = ret.replace(".00", ".–");
                    }
                    return ret;
                };
                Price.prototype.fromString = function (currencyAndValue) {
                    var self = this;
                    // Helper methods
                    function segIsDecimal(seg) {
                        return !isNaN(parseFloat(seg));
                    }
                    function segIsInt64(seg) {
                        return !isNaN(parseFloat(seg));
                    }
                    // Special handling if null string
                    if (currencyAndValue == null || currencyAndValue == "") {
                        this.Value = null;
                        this.Currency = null;
                    }
                    else {
                        // We have something, trim it
                        currencyAndValue = currencyAndValue.trim();
                        try {
                            var segs = currencyAndValue.split(' ');
                            if (segs.length > 2)
                                throw ("price has too many parts to it.");
                            var didSetValue = false;
                            var didSetCurrency = false;
                            for (var _i = 0, segs_1 = segs; _i < segs_1.length; _i++) {
                                var segRaw = segs_1[_i];
                                // Create formatted seg...
                                var seg = segRaw;
                                // Change thousands seperator
                                //TODO: @dan finish porting C# code...
                                //ORIG C#: if (!string.IsNullOrEmpty(Core.Config.CurrencyThousandsSeparatorChar)) seg = seg.Replace(Core.Config.CurrencyThousandsSeparatorChar, ",");
                                seg = MachinataJS.String.replaceAll(seg, "'", ",");
                                // Change .- to .00
                                //ORIG C#: if (seg.EndsWith(".-")) seg = seg.ReplaceSuffix(".-", ".00");
                                seg = MachinataJS.String.replaceAll(seg, ".-", ".00");
                                seg = MachinataJS.String.replaceAll(seg, ".—", ".00");
                                //ORIG C#: if (!string.IsNullOrEmpty(Core.Config.CurrencyZeroCentsChar) && seg.EndsWith("." + Core.Config.CurrencyZeroCentsChar)) seg = seg.ReplaceSuffix("." + Core.Config.CurrencyZeroCentsChar, ".00");
                                // Decimal?
                                if (segIsDecimal(seg)) {
                                    if (didSetValue)
                                        throw "two values where found.";
                                    this.Value = parseFloat(seg);
                                    didSetValue = true;
                                }
                                else if (segIsInt64(seg)) {
                                    if (didSetValue)
                                        throw "two values where found.";
                                    this.Value = parseFloat(seg);
                                    didSetValue = true;
                                }
                                else {
                                    // Must be currency
                                    if (didSetCurrency)
                                        throw "two currencies where found.";
                                    this.Currency = seg;
                                    didSetCurrency = true;
                                }
                            }
                            if (!didSetValue)
                                throw "no valid value was given.";
                            if (!didSetCurrency)
                                this.Currency = null;
                        }
                        catch (e) {
                            throw "Could not parse the price '" + currencyAndValue + "': " + e;
                        }
                    }
                    return this;
                };
                Price.prototype.fromJSON = function (json) {
                    if (json["Currency"] != null)
                        this.Currency = json["Currency"];
                    if (json["Value"] != null)
                        this.Currency = json["Value"];
                };
                Price.prototype.toJSON = function () {
                    var json = {};
                    json["Value"] = this.Value;
                    json["Currency"] = this.Currency;
                    return json;
                };
                return Price;
            }());
            Model.Price = Price;
            var TranslatableText = /** @class */ (function () {
                function TranslatableText(data) {
                    if (data == null)
                        data = {};
                    this.Data = data;
                }
                TranslatableText.prototype.fromJSON = function (json) {
                    this.Data = json;
                };
                TranslatableText.prototype.toJSON = function () {
                    return this.Data;
                };
                TranslatableText.prototype.toString = function () {
                    return this.get();
                };
                TranslatableText.prototype.getForLanguage = function (lang) {
                    //TODO: @dan test
                    var ret = this.Data[lang];
                    if (ret != null)
                        return ret;
                    return this.getDefault();
                };
                TranslatableText.prototype.getDefault = function () {
                    //TODO: @dan test
                    // First try explicit default value
                    if (this.Data["default"] != null) {
                        return this.Data["default"];
                    }
                    else {
                        // Return any value
                        if (Object.keys(this.Data).length > 0) {
                            return this.Data[Object.keys(this.Data)[0]];
                        }
                        else {
                            // No value, return null
                            return null;
                        }
                    }
                };
                TranslatableText.prototype.setDefault = function (text) {
                    //TODO: @dan test
                    this.Data["default"] = text;
                    return this;
                };
                TranslatableText.prototype.get = function () {
                    //TODO: @dan test
                    return this.getForLanguage("{language}");
                };
                TranslatableText.prototype.set = function (text) {
                    //TODO: @dan test
                    this.setForLanguage(text, "{language}");
                    return this;
                };
                TranslatableText.prototype.setForLanguage = function (text, language) {
                    //TODO: @dan test
                    if (language == null)
                        this.setDefault(text);
                    else
                        this.Data[language] = text;
                    return this;
                };
                return TranslatableText;
            }());
            Model.TranslatableText = TranslatableText;
            var Properties = /** @class */ (function () {
                function Properties(data) {
                    this.Data = data;
                }
                Properties.prototype.set = function (key, val) {
                    var json = this._deserialize();
                    json[key] = val;
                    this._serialize(json);
                };
                Properties.prototype.get = function (key) {
                    return this._deserialize()[key];
                };
                Properties.prototype._serialize = function (json) {
                    if (json == null)
                        this.Data = null;
                    this.Data = JSON.stringify(json);
                };
                Properties.prototype._deserialize = function () {
                    if (this.Data == null)
                        return {};
                    return JSON.parse(this.Data);
                };
                Properties.prototype.fromJSON = function (json) {
                    this.Data = json;
                };
                Properties.prototype.toJSON = function () {
                    return this.Data;
                };
                return Properties;
            }());
            Model.Properties = Properties;
        })(Model = Core.Model || (Core.Model = {}));
    })(Core = Machinata.Core || (Machinata.Core = {}));
})(Machinata || (Machinata = {}));
