declare namespace Machinata.Core {
    class ModelObject {
        Type: string;
        PublicId: string;
        Created: Date;
        constructor();
        fromJSON(json: any): void;
        toJSON(): any;
    }
}
declare namespace Machinata.Core.Model {
    class ContentFileAsset {
        SourceURL: string;
        AssetURL: string;
        Hash: string;
        constructor(sourceURL: string, assetURL: string, hash: string);
        fromJSON(json: any): void;
        toJSON(): any;
    }
    class Price {
        Currency: string;
        Value: number;
        constructor(value: number, currency: string);
        toString(): string;
        fromString(currencyAndValue: string): Price;
        fromJSON(json: any): void;
        toJSON(): any;
    }
    class TranslatableText {
        Data: any;
        constructor(data?: any);
        fromJSON(json: any): void;
        toJSON(): any;
        toString(): string;
        getForLanguage(lang: string): string;
        getDefault(): string;
        setDefault(text: string): TranslatableText;
        get(): string;
        set(text: string): TranslatableText;
        setForLanguage(text: string, language: string): TranslatableText;
    }
    class Properties {
        Data: string;
        constructor(data: string);
        set(key: string, val: any): void;
        get(key: string): any;
        _serialize(json: any): any;
        _deserialize(): any;
        fromJSON(json: any): void;
        toJSON(): any;
    }
}
