

/// <summary>
/// API Utilities for communicating with Machinata Server
/// </summary>
/// <type>namespace</type>
Machinata.API = {};


/// <summary>
/// Callback function which must return true in order for the API call to be processed (sent)
/// </summary>
/// <example>
/// ```
/// Machinata.API.apiCallProcessValidatorCallback = function (callback) {
///     if (requestIsValid) {
///         callback(true);
///     } else {
///         callback(false, "{text.error.api-call.validation.blocked.spam}");
///     }
/// }
/// ```
/// </example>
Machinata.API.apiCallProcessValidatorCallback = null;

/// <summary>
/// Extensions for Machinata API Calls.
/// </summary>
/// <hidden/>
Machinata.onInit(function () {
    // Find all links bound to a api-call
    $("a[api-call]").each(function () {
        var link = $(this);
        var apiCall = link.attr("api-call");
        var confirm = link.attr("confirm-message");
        var input = link.attr("input-message");
        var successCallback = link.attr("on-success");
        var errorCallback = link.attr("on-error");
        var inputName = link.attr("input-name");
        var inputType = link.attr("input-type");
        var confirmDelete = link.attr("confirm-delete");
        var title = null;
        if (link.attr("confirm-title") != null) title = link.attr("confirm-title");
        if (link.attr("input-title") != null) title = link.attr("input-title");
        if (!String.isEmpty(confirmDelete)) {
            var text1 = "{text.are-you-sure-you-want-to-delete-pre}";
            var text2 = "{text.are-you-sure-you-want-to-delete-post}";
            confirm = text1 + " " + confirmDelete + " " + text2;
            title = "{text.delete}";
        }
        Machinata.apiCallForLink(apiCall, link, title, confirm, input, function (message) {
            Machinata.processCallback(successCallback, message);
        }, function (message) {
            if (String.isEmpty(errorCallback)) Machinata.apiErrorForGeneric(message.data.code, message.data.message);
            else Machinata.processCallback(errorCallback, message);
        }, inputName);
    });
});


/// <summary>
/// </summary>
Machinata.apiCallForLink = function (apiCall, linkElem, title, confirm, input, onSuccess, onError, inputName) {
    function doCall(val) {
        // Init api call
        apiCall = apiCall.replace("{value}", val);
        var call = Machinata.apiCall(apiCall);
        // Init data to send
        if (inputName == null) inputName = "input";
        var data = {};
        if (val != null) data[inputName] = val;
        call.data(data);
        // Prepare api call
        call.success(function (message) {
            linkElem.removeClass("loading");
        })
        call.error(function (message) {
            linkElem.removeClass("loading");
        });
        if (onSuccess) call.success(onSuccess);
        if (onError) call.error(onError);
        // Send
        call.send();
    }
    // Intercept submit
    linkElem.click(function (e) {
        // Stop browser
        e.preventDefault();
        // Show confirm diag?
        if (!String.isEmpty(confirm) || !String.isEmpty(input)) {

            if (!String.isEmpty(input)) {
                // Show input dialog
                var dialogTitle = title;
                var dialogMessage = input;
                var inputType = null;
                var inputValue = null;
                var inputPlaceholder = null;
                if (!String.isEmpty(linkElem.attr("input-type"))) inputType = linkElem.attr("input-type");
                if (!String.isEmpty(linkElem.attr("input-value"))) inputValue = linkElem.attr("input-value");
                if (!String.isEmpty(linkElem.attr("input-placeholder"))) inputPlaceholder = linkElem.attr("input-placeholder");
                if (String.isEmpty(dialogTitle)) {
                    dialogTitle = input;
                    dialogMessage = null;
                }
                Machinata.inputDialog(dialogTitle, dialogMessage, inputValue, inputPlaceholder, inputType)
                    .input(function (val, id) {
                        doCall(val);
                    })
                    .show();
            } else {
                // Just show confirm dialog
                var dialogTitle = title;
                var dialogMessage = confirm;
                if (String.isEmpty(dialogTitle)) {
                    dialogTitle = confirm;
                    dialogMessage = null;
                }
                Machinata.confirmDialog(dialogTitle, dialogMessage)
                    .okay(function () { doCall(); })
                    .show();
            }

        } else {
            doCall();
        }
    });
};


