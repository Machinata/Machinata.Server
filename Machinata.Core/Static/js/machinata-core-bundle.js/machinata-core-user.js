

/// <summary>
/// Extensions related to use login and logout.
/// </summary>

/// <summary>
///
/// </summary>
/// <hidden/>
Machinata._onLogins = [];

/// <summary>
/// Bind a handler to a login event.
/// </summary>
Machinata.onLogin = function (fn) {
    Machinata._onLogins.push(fn);
};

/// <summary>
///
/// </summary>
/// <hidden/>
Machinata._onLogouts = [];

/// <summary>
/// Bind a handler to a logout event.
/// </summary>
Machinata.onLogout = function (fn) {
    Machinata._onLogouts.push(fn);
};

/// <summary>
/// Executes the standard login procedure after recieving a login API packet.
/// This can be used on a login form.
/// If redirect is false, the redirect or reload is never done.
/// </summary>
Machinata.doLogin = function (message, redirect) {
    Machinata.registerAuthToken(message);

    // if no redirect from param try redirect from query parameter
    if (redirect == null && redirect != false) {
        if (Machinata.queryParameter("redirect") != "") {
            redirect = Machinata.queryParameter("redirect");
        }
    }
    // if still no redirect try reading redirect from message
    if (redirect == null && redirect != false) {
        if (message.data != null && message.data.redirect != null && message.data.redirect != "") {
            redirect = message.data.redirect;
        }
    }

    if (redirect != false) {
        if (redirect == null) Machinata.reload(false);
        else Machinata.goToPage(redirect);
    }
};

/// <summary>
/// </summary>
Machinata.doVerifiedLogin = function (message, redirect, verifyPage) {
    Machinata.registerAuthToken(message, { sameSite: "None", secure: true });
    if (redirect == null) {
        if (Machinata.queryParameter("redirect") != "") {
            redirect = Machinata.queryParameter("redirect");
        }
    }
    if (redirect == null) redirect = window.location;
    if (verifyPage == null) verifyPage = "/admin/verify-login";

    window.location = verifyPage + "?redirect=" + encodeURIComponent(redirect);
};

/// <summary>
/// 
/// </summary>
Machinata.logout = function (redirect) {
    Machinata.apiCall("/api/admin/logout").success(function (message) {
        Machinata.doLogout(message, redirect)
    }).send();
};

/// <summary>
/// 
/// </summary>
Machinata.doLogout = function (message, redirect) {
    Machinata.unregisterAuthToken();
    if (redirect == null) window.location.reload();
    else window.location = redirect;
};

/// <summary>
/// 
/// </summary>
Machinata.registerAuthToken = function (message, opts) {
    if (opts == null) opts = {};
    Machinata.debug("registerAuthToken",opts);

    // Compile cookie options
    var expires = null;
    var expiresStr = message.data["auth-token"]["expires"];
    if (expiresStr != null && expiresStr != "") {
        expires = new Date(expiresStr); // set to datetime obj
    } else {
        // Fallback (backwards compatibility
        var expiresInDaysStr = message.data["auth-token"]["expires-in-days"];
        if (expiresStr != null && expiresStr != "") {
            var expiresInDays = parseInt(expiresInDaysStr);
            expires = expiresInDays; // set to days (number)
        }
    }
    if (opts.expires == null) opts.expires = expires;
    if (opts.path == null) opts.path = "/";
    // Set cookies
    $.cookie("AuthTokenHash", message.data["auth-token"]["hash"], opts);
    $.cookie("AuthTokenCreated", message.data["auth-token"]["created"], opts);
    $.cookie("AuthTokenExpires", message.data["auth-token"]["expires"], opts);
    if (message.data["user"] != null && message.data["user"]["public-id"] != null) $.cookie("UserPublicId", message.data["user"]["public-id"], opts);
    // Trigger events
    for (var i = 0; i < Machinata._onLogins.length; i++) Machinata._onLogins[i]();
};

/// <summary>
/// 
/// </summary>
Machinata.unregisterAuthToken = function () {
    Machinata.debug("unregisterAuthToken");
    $.cookie("AuthTokenHash", null, { path: '/' });
    $.cookie("AuthTokenCreated", null, { path: '/' });
    $.cookie("AuthTokenExpires", null, { path: '/' });
    $.cookie("UserPublicId", null, { path: '/' });
    for (var i = 0; i < Machinata._onLogouts.length; i++) Machinata._onLogouts[i]();
};

/// <summary>
/// 
/// </summary>
Machinata.getAuthToken = function () {
    Machinata.debug("getAuthToken");
    return {
        Hash: $.cookie("AuthTokenHash"),
        Created: $.cookie("AuthTokenCreated"),
        Expires: $.cookie("AuthTokenExpires"),
    }
};

/// <summary>
/// 
/// </summary>
Machinata.getUser = function () {
    Machinata.debug("getUser");
    return {
        PublicId: $.cookie("UserPublicId")
    }
};


/// <summary>
/// 
/// </summary>
Machinata.getTwoFactorAuthenticationPINFromUser = function (opts) {
    if (opts == null) opts = {
        onSuccess: null,
        setupQRCodeLink: null,
        setupCode: null
    }

    let title = "{text.account.two-factor.ask}";
    let message = "{text.account.two-factor.ask.instructions}";
    if (opts.setupQRCodeLink != null) {
        title = "{text.account.two-factor.required}";
        message = "{text.account.two-factor.required.instructions}";
    }

    let diag1 = Machinata.messageDialog(title, message);
    if (opts.setupQRCodeLink != null) {
        let qrCodeLinkElem = $("<a></a>");
        if (opts.setupCode != null) qrCodeLinkElem.click(function () {
            let keyElem = $("<pre/>");
            keyElem.text(opts.setupCode);
            //keyElem.css();
            let diag2 = Machinata.messageDialog("{text.account.two-factor.setup-code}", "{text.account.two-factor.setup-code.message}");
            diag2.elem.append(keyElem);
            diag2.button("{text.copy-to-clipboard}", "copy", function () {
                Machinata.UI.copyTextToClipboard(opts.setupCode, true);
            }, false, false);
            diag2.okayButton();
            diag2.show();
        });
        let qrCodeImageElem = $("<img style='aspect-ratio:1; width:100%;'/>").attr("src", "/static/qrcode?data=" + encodeURIComponent(opts.setupQRCodeLink)).appendTo(qrCodeLinkElem);
        diag1.elem.append(qrCodeLinkElem);
    }
    diag1.elem.append($("<div style='height:1em;'/>"));
    diag1.elem.append($("<div style='text-align:center;'><input class='ui-input' placeholder='PIN' type='text' pattern='[0-9]*' maxlength='6' style='max-width:150px;font-size:1.5em;letter-spacing: 0.15em;text-align:center'/></div>"));
    if (opts.setupQRCodeLink != null) {
        var linkElem = $("<p style='font-size: small;'><a>{text.account.two-factor.install-an-app}</a></p>");
        linkElem.insertAfter(diag1.elem.find("p"));
        linkElem.find("a").click(function () {
            let diag2 = Machinata.messageDialog("{text.account.two-factor.install-an-app}", "{text.account.two-factor.install-an-app.message}");
            diag2.elem.append($("<p style='text-align:center'><a class='ui-button' href='https://apps.apple.com/us/app/google-authenticator/id388497605' target='_blank'>Google Authenticator for iOS</a></p>"));
            diag2.elem.append($("<p style='text-align:center'><a class='ui-button' href='https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2' target='_blank'>Google Authenticator for Android</a></p>"));
            diag2.elem.append($("<p style='text-align:center'><a class='ui-button' href='https://apps.apple.com/us/app/microsoft-authenticator/id983156458' target='_blank'>Microsoft Authenticator for iOS</a></p>"));
            diag2.elem.append($("<p style='text-align:center'><a class='ui-button' href='https://play.google.com/store/apps/details?id=com.azure.authenticator' target='_blank'>Microsoft Authenticator for Android</a></p>"));
            diag2.show();
        });
    }
    diag1.cancelButton();

    function enterPIN() {
        let pin = diag1.elem.find("input").val();
        if (pin == null || pin == "") return;
        // Send the pin to the on success callback...
        opts.onSuccess(pin);
    }
    diag1.button("{text.continue}", "continue", function () {
        enterPIN();
    }, false, false);
    diag1.elem.find("input").keypress(function (e) {
        if (e.which == 13) {
            enterPIN();
            return false;
        }
    });

    diag1.show();

    return diag1;
};