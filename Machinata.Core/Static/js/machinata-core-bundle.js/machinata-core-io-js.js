
/// <summary>
/// JS: Loads the file contents given at ```pathOrURL``` as text.
/// </summary>
Machinata.IO.loadFileContentsAsText = function (pathOrURL, opts) {
    if (opts == null) opts = {};
    if (opts.encoding == null) opts.encoding = "utf8";

    // Use fetch API...
    if (pathOrURL.startsWith("https://") || pathOrURL.startsWith("http://")) {
        return new Promise((resolve, reject) => {
            fetch(pathOrURL)
                .then(res => resolve(res.text()))
                .catch(err => reject(err));
        });
    } else {
        // Disk or relative?
        // For now we use the fetch api...
        return new Promise((resolve, reject) => {
            fetch(pathOrURL)
                .then(res => resolve(res.text()))
                .catch(err => reject(err));
        });
    }
};