

/// <summary>
/// Useful javascript functions.
/// Some of the util methods are directly taken from jQuery, or 1:1 compatible with jQuery, which allows a headless
/// profile (such as ```nodejs```) to run without jQuery.
/// </summary>
/// <type>namespace</type>
Machinata.Util = {};


/// <summary>
/// Clones a JSON object, creating a new identical copy.
/// The incomming data must be a pure JSON and cannot contain cyclic references.
/// </summary>
Machinata.Util.cloneJSON = function (json) {
    //TODO: @dan is there a faster way we can do this?
    return JSON.parse(JSON.stringify(json)); 
    //return Machinata.Util.extend({}, json);
};

/// <summary>
/// Merge the contents of two or more objects together into the first object, exactly the same as jQuery does.
/// See https://api.jquery.com/jQuery.extend/ for more details.
/// </summary>
Machinata.Util.extend = function () {
    var options, name, src, copy, copyIsArray, clone,
		target = arguments[0] || {},
		i = 1,
		length = arguments.length,
		deep = false;

    // Handle a deep copy situation
    if (typeof target === "boolean") {
        deep = target;

        // Skip the boolean and the target
        target = arguments[i] || {};
        i++;
    }

    // Handle case when target is a string or something (possible in deep copy)
    if (typeof target !== "object" && typeof target !== "function") {
        target = {};
    }

    // Extend jQuery itself if only one argument is passed
    if (i === length) {
        target = this;
        i--;
    }

    for (; i < length; i++) {

        // Only deal with non-null/undefined values
        if ((options = arguments[i]) != null) {

            // Extend the base object
            for (name in options) {
                copy = options[name];

                // Prevent Object.prototype pollution
                // Prevent never-ending loop
                if (name === "__proto__" || target === copy) {
                    continue;
                }

                // Recurse if we're merging plain objects or arrays
                if (deep && copy && (Machinata.Util.isPlainObject(copy) ||
					(copyIsArray = Array.isArray(copy)))) {
                    src = target[name];

                    // Ensure proper type for the source value
                    if (copyIsArray && !Array.isArray(src)) {
                        clone = [];
                    } else if (!copyIsArray && !Machinata.Util.isPlainObject(src)) {
                        clone = {};
                    } else {
                        clone = src;
                    }
                    copyIsArray = false;

                    // Never move original objects, clone them
                    target[name] = Machinata.Util.extend(deep, clone, copy);

                    // Don't bring in undefined values
                } else if (copy !== undefined) {
                    target[name] = copy;
                }
            }
        }
    }

    // Return the modified object
    return target;
};


/// <summary>
/// Check to see if an object is a plain object (created using "{}" or "new Object").
/// See https://api.jquery.com/jQuery.isPlainObject/#jQuery-isPlainObject-object for more details.
/// </summary>
Machinata.Util.isPlainObject = function (obj) {
    var proto, Ctor;

    // Detect obvious negatives
    // Use toString instead of jQuery.type to catch host objects
    if (!obj || Machinata.Util._toString.call(obj) !== "[object Object]") {
        return false;
    }

    proto = Machinata.Util._getProto(obj);

    // Objects with no prototype (e.g., `Object.create( null )`) are plain
    if (!proto) {
        return true;
    }

    // Objects with prototype are plain iff they were constructed by a global Object function
    Ctor = Machinata.Util._hasOwn.call(proto, "constructor") && proto.constructor;
    return typeof Ctor === "function" && Machinata.Util._fnToString.call(Ctor) === Machinata.Util._ObjectFunctionString();
};

/// <summary>
/// A generic iterator function, which can be used to seamlessly iterate over 
/// both objects and arrays. Arrays and array-like objects with a length property 
/// (such as a function's arguments object) are iterated by numeric index, 
/// from 0 to length-1. Other objects are iterated via their named properties.
/// See https://api.jquery.com/jQuery.each/#jQuery-each-object-callback for more details.
/// </summary>
Machinata.Util.each = function (obj, callback) {
    var length, i = 0;

    if (Machinata.Util._isArrayLike(obj)) {
        length = obj.length;
        for (; i < length; i++) {
            if (callback.call(obj[i], i, obj[i]) === false) {
                break;
            }
        }
    } else {
        for (i in obj) {
            if (callback.call(obj[i], i, obj[i]) === false) {
                break;
            }
        }
    }

    return obj;
};

Machinata.each = Machinata.Util.each; // shortcut

/// <summary>
/// Internal Util method based on jQuery util libs.
/// </summary>
/// <hidden/>
Machinata.Util._isArrayLike = function (obj) {

    var length = !!obj && obj.length,
		type = Machinata.Util._toType(obj);

    if (typeof obj === "function" || Machinata.Util._isWindow(obj)) {
        return false;
    }

    return type === "array" || length === 0 ||
		typeof length === "number" && length > 0 && (length - 1) in obj;
};

/// <summary>
/// Internal Util method based on jQuery util libs.
/// </summary>
/// <hidden/>
Machinata.Util._isWindow = function( obj ) {
    return obj != null && obj === obj.window;
};

/// <summary>
/// Internal Util method based on jQuery util libs.
/// </summary>
/// <hidden/>
Machinata.Util._toType = function (obj) {
    if (obj == null) {
        return obj + "";
    }

    return typeof obj === "object" ?
		Machinata.Util._class2type[Machinata.Util._toString.call(obj)] || "object" :
		typeof obj;
};

/// <summary>
/// Internal Util method based on jQuery util libs.
/// </summary>
/// <hidden/>
Machinata.Util._class2type = {};
{
    var classes = "Boolean Number String Function Array Date RegExp Object Error Symbol".split(" ");
    for(var i = 0; i < classes.length; i++) {
        var name = classes[i];
        Machinata.Util._class2type["[object " + name + "]"] = name.toLowerCase();
    }
}

/// <summary>
/// Internal Util method based on jQuery util libs.
/// </summary>
/// <hidden/>
Machinata.Util._toString = Machinata.Util._class2type.toString;

/// <summary>
/// Internal Util method based on jQuery util libs.
/// </summary>
/// <hidden/>
Machinata.Util._hasOwn = Machinata.Util._class2type.hasOwnProperty;

/// <summary>
/// Internal Util method based on jQuery util libs.
/// </summary>
/// <hidden/>
Machinata.Util._getProto = Object._getPrototypeOf;

/// <summary>
/// Internal Util method based on jQuery util libs.
/// </summary>
/// <hidden/>
Machinata.Util._fnToString = Machinata.Util._hasOwn.toString;

/// <summary>
/// Internal Util method based on jQuery util libs.
/// </summary>
/// <hidden/>
Machinata.Util._ObjectFunctionString = function () {
    return Machinata.Util._fnToString.call(Object);
};

/// <summary>
/// Returns true if the value is a function
/// </summary>
/// <hidden/>
Machinata.Util.isFunction = function (value) {
    // See https://stackoverflow.com/questions/5999998/check-if-a-variable-is-of-function-type
    return value && (Object.prototype.toString.call(value) === "[object Function]" || "function" === typeof value || value instanceof Function);
};

/// <summary>
/// Returns true if the value is a string
/// </summary>
/// <hidden/>
Machinata.Util.isString = function (val) {
    if (typeof val === 'string' || val instanceof String) return true;
    else return false;
};



/// <summary>
/// 
/// </summary>
Machinata.Util.changeNodeName = function (oldNode, newTagName) {
    var newNode = document.createElement(newTagName),node,nextNode;
    node = oldNode.firstChild;
    while (node) {
        nextNode = node.nextSibling;
        newNode.appendChild(node);
        node = nextNode;
    }
    newNode.className = oldNode.className;
    newNode.id = oldNode.id; // (Not invalid, they're not both in the tree at the same time)
    oldNode.parentNode.replaceChild(newNode, oldNode);
};

/// <summary>
/// 
/// </summary>
Machinata.Util.changeNodeNameByTag = function (oldTagName, newTagName) {
    var nodes = document.getElementsByTagName(oldTagName);
    for (var i = 0; i < nodes.length; i++) Machinata.Util.changeNodeName(nodes[i],newTagName);
};


/// <summary>
/// Creates a watchdog that monitors all iframes and automatically reloades them if they fail to load.
/// </summary>
Machinata.Util.createIFrameWatchdog = function (iframes, opts) {
    if (opts == null) opts = {};
    if (opts.selector == null) opts.selector = "iframe";
    if (opts.interval == null) opts.interval = 1000;
    if (opts.cacheBust == null) opts.cacheBust = false;
    iframes = $(opts.selector);

    iframes.each(function () {
        $(this).attr("orig-src", $(this).attr("src"));
    });

    function validateFrame(frame) {
        var ret = true;
        try {
            if(frame.contentWindow.document == null) throw "iframe document is null";
        } catch (e) {
            ret = false;
        }
        return ret;
    }

    function validateFrames() {
        iframes.each(function () {
            var frameElem = $(this);
            var frame = frameElem[0];
            var valid = validateFrame(frame);
            if (valid == false) {
                console.log("***iFrame invalid***");
                console.log("Reloading...");
                var newUrl = frameElem.attr("orig-src");
                if (opts.cacheBust == true) newUrl = Machinata.updateQueryString("rnd", Math.random(), newUrl);
                frame.src = newUrl;
            }
        });
    }

    setInterval(function () {
        validateFrames();
    }, opts.interval);
};

/// <summary>
/// 
/// </summary>
Machinata.Util.getLanguageNameForId = function (langId) {
    if (langId == "en") return "{text.lang-en}";
    else if (langId == "de") return "{text.lang-de}";
    else if (langId == "fr") return "{text.lang-fr}";
    else if (langId == "it") return "{text.lang-it}";
    else if (langId == "es") return "{text.lang-es}";
    else if (langId == "cn") return "{text.lang-cn}";
    else if (langId == "default") return "{text.lang-default}";
    else "";
};

/// <summary>
/// 
/// </summary>
Machinata.Util.getCSSVariableValue = function (name) {
    return getComputedStyle(document.body).getPropertyValue(name);
};

/// <summary>
/// 
/// </summary>
Machinata.Util.setCSSVariableValue = function (name,val) {
    document.documentElement.style.setProperty(name, val);
};