

/* ======== Machinata Masonry ======== */
/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (typeof Machinata === "undefined") var Machinata = ((typeof global !== 'undefined') ? global : window).MACHINATA;


/// <summary>
/// Machinata Masonry JS library
/// </summary>
/// <type>namespace</type>
Machinata.Shuffle = {};


/// <summary>
/// ## Options
///  - ```container```: container selector
///  - ```columnsDesktop```: num columns
///  - ```columnsTablet```: num columns
///  - ```columnsMobile```: num columns
/// ## External Reference
/// See https://vestride.github.io/Shuffle/
/// </summary>
Machinata.Shuffle.createShuffleLayout = function (container, opts) {
    if (container == null || container.length == 0) return null;
    var containerId = Machinata.UI.autoUIDElem(container);
    if (opts == null) opts = {};
    if (opts.maxItems == null) opts.maxItems = null;
    if (opts.itemSelector == null) opts.itemSelector = ".ma-shuffle-item";
    //if (opts.sizer == null) opts.sizer = ".ma-shuffle-sizer";
    if (opts.delimiter == null) opts.delimiter = ",";
    if (opts.breakAt == null) {
        opts.breakAt = {};
        if (opts.columnsDesktop != null) opts.columns = opts.columnsDesktop;
        if (opts.columnsTablet != null) opts.breakAt[Machinata.Responsive.VIEWPORT_MAX_WIDTH_TABLET] = opts.columnsTablet;
        if (opts.columnsMobile != null) opts.breakAt[Machinata.Responsive.VIEWPORT_MAX_WIDTH_MOBILE] = opts.columnsMobile;
    }

    // Init shuffle
    Shuffle.FILTER_ATTRIBUTE_KEY = 'filter-groups';
    var shuffle = new Shuffle(container[0], opts);
    container.data("shuffle", shuffle);

    // Event handling helper method
    var lastNumItems = null;
    function updateShuffleAttributes() {
        container.attr("data-shuffle-num-items", shuffle.items.length);
        if (lastNumItems != shuffle.items.length) {
            if (lastNumItems != null) {
                // Event
                if (opts.onItemsChanged) opts.onItemsChanged(shuffle);
            }
            lastNumItems = shuffle.items.length;
        }

    }

    // Shuffle events
    shuffle.on(Shuffle.EventType.LAYOUT, function (data) {
        updateShuffleAttributes(); // because there is no add event
        if (opts.onLayout) opts.onLayout(shuffle, data);
    });
    shuffle.on(Shuffle.EventType.REMOVED, function (data) {
        updateShuffleAttributes();
        if (opts.onRemoved) opts.onRemoved(shuffle, data);
    });

    // Initial update
    updateShuffleAttributes();

    return shuffle;
};