/* ======== Machinata Spam ======== */
/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (!Machinata) Machinata = {};

/// <summary>
/// Machinata Spam detection
/// </summary>
/// <type>namespace</type>
Machinata.Spam = {};

/// <summary>
/// If true, every Machinata API call must first be validated against bot detection, otherwise it won't be sent.
/// </summary>
Machinata.Spam.automaticAPICallBotProtectionEnabled = true;

/// <summary>
/// Bind to Machinata.API.apiCallProcessValidatorCallback callback if automaticAPICallBotProtectionEnabled is enabled...
/// </summary>
/// <hidden/>
Machinata.onInit(function () {
    if (Machinata.Spam.automaticAPICallBotProtectionEnabled == true) {
        Machinata.API.apiCallProcessValidatorCallback = function (callback) {
            Machinata.debug("Machinata.Spam: loading Botd...");
            Botd.load()
                .then((botd) => {
                    Machinata.debug("Machinata.Spam: loaded! Detecting...");
                    var ret = botd.detect();
                    Machinata.debug("Machinata.Spam: detection done!");
                    return ret;
                })
                .then((result) => {
                    Machinata.debug("Machinata.Spam: result:", result);
                    if (result.bot == false) {
                        Machinata.debug("Machinata.Spam: result: not a bot!");
                        callback(true);
                    } else {
                        Machinata.debug("Machinata.Spam: result: bot detected!");
                        callback(false, "{text.error.api-call.validation.blocked.spam}");
                    }
                })
                .catch((error) => {
                    Machinata.debug("Machinata.Spam: error! " + error);
                    callback(false, "{text.error.api-call.validation.blocked.spam}: " + error);
                });
        }
    }
});


