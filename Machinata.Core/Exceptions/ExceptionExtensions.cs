using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Exceptions {

    public static class ExceptionExtensions {

        /// <summary>
        /// Gets the full description including all inner messages and inner stack traces.
        /// </summary>
        /// <param name="e">The e.</param>
        /// <returns></returns>
        public static string GetFullDescriptionIncludingTrace(this Exception e) {
            return (e.GetFullMessageTrace().Trim() + "\n" + e.GetFullStackTrace().Trim()).Trim();
        }

        /// <summary>
        /// Gets the full stack trace by crawling each inner exception.
        /// </summary>
        /// <param name="e">The e.</param>
        /// <returns></returns>
        public static string GetFullStackTrace(this Exception e) {
            string stackTrace = "";
            Exception currentError = e;
            while (currentError != null) {
                stackTrace += currentError.StackTrace + "\n";
                currentError = currentError.InnerException;
            }
            return stackTrace;
        }

        /// <summary>
        /// Gets the full stack trace by crawling each inner exception,
        /// ONLY IF ErrorUnmaskingEnabled is enabled.
        /// Otherwise returns "".
        /// </summary>
        /// <param name="e">The e.</param>
        /// <returns></returns>
        public static string GetMaskedFullStackTrace(this Exception e) {
            if (Core.Config.ErrorUnmaskingEnabled) {
                return e.GetFullStackTrace();
            } else {
                return "";
            }
        }

        /// <summary>
        /// Gets the full message trace by crawling each inner exception..
        /// </summary>
        /// <param name="e">The e.</param>
        /// <returns></returns>
        public static string GetFullMessageTrace(this Exception e) {
            string messageTrace = "";
            Exception currentError = e;
            while (currentError != null) {
                //messageTrace += currentError.Message + " ("+ currentError.TargetSite?.ReflectedType?.FullName + ")" + "\n";
                messageTrace += currentError.Message + "\n";
                currentError = currentError.InnerException;
            }
            return messageTrace;
        }

        /// <summary>
        /// Gets the full message trace by crawling each inner exception,
        /// ONLY IF ErrorUnmaskingEnabled is enabled.
        /// Otherwise returns "".
        /// </summary>
        /// <param name="e">The e.</param>
        /// <returns></returns>
        public static string GetMaskedFullMessageTrace(this Exception e) {
            if (Core.Config.ErrorUnmaskingEnabled) {
                return e.GetFullMessageTrace();
            } else {
                if (e.Message == null) return "";
                else return e.Message;
            }
        }

        /// <summary>
        /// To preserve stacktrace if exception gets re-thrown after
        /// https://stackoverflow.com/questions/4555599/how-to-rethrow-the-inner-exception-of-a-targetinvocationexception-without-losing
        /// Hint: the new suggested  An official API has been added to .NET 4.5: ExceptionDispatchInfo.Capture(ex); does not seem to work
        /// </summary>
        /// <param name="e"></param>
        public static void PreserveStackTrace(this Exception e) {
            typeof(Exception).GetMethod("PrepForRemoting",
                 BindingFlags.NonPublic | BindingFlags.Instance)
                 .Invoke(e, new object[0]);
        }

    }
}
