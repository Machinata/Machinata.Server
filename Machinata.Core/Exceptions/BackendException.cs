using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Exceptions {

    public class BackendException : Exception
    {
        public const string CODE_AUTHTOKEN_NONEXIST = "authtoken-nonexist";
        public const string CODE_UNKNOWN_ERROR = "unknown-error";
        
        public const string ERROR_HTTP_HEADER_STATUS = "X-Error-Status";
        public const string ERROR_HTTP_HEADER_CODE = "X-Error-Code";
        public const string ERROR_HTTP_HEADER_MESSAGE = "X-Error-Message";

        public string Code;
        public int Status;
        public object Errors;

        private Dictionary<string,string> _customData;

        public BackendException(string code, string message, Exception innerException = null, int status = 500, object errors = null)
            : base(message, innerException) {
            this.Code = code;
            this.Status = status;
            this.Errors = errors;
        }

        /// <summary>
        /// Automaticall wraps the exception with a backend exception (if it is not already).
        /// This ads a layer of security by always wrapping unknown non-machinata exceptions with a 'anonymous'
        /// unknown error exception.
        /// </summary>
        /// <param name="e">The e.</param>
        /// <returns></returns>
        public static BackendException WrapExceptionWithBackendException(Exception e) {
            if(typeof(BackendException).IsAssignableFrom(e.GetType())) {
                return (BackendException)e;
            } else {
                return new BackendException(CODE_UNKNOWN_ERROR, "An unknown exception has occured.", e);
            }
        }

        public BackendException CustomData(string key, string val) {
            if (this._customData == null) this._customData = new Dictionary<string, string>();
            this._customData[key] = val;
            return this;
        }

        public Dictionary<string, string> GetCustomData() {
            return this._customData;
        }

        /// <summary>
        /// Gets the most recent log trace from the MemoryTarget logger, if any.
        /// </summary>
        /// <param name="numLogs">The number logs.</param>
        /// <returns></returns>
        public string GetLogTrace(int numLogs = 40) {
            var memoryTarget = Core.Logging.MemoryTarget;
            if (memoryTarget == null) return null;
            string logTrace = "";
            var logTraceBuilder = new StringBuilder();
            foreach(var line in memoryTarget.Logs.Skip(Core.Logging.MemoryTarget.Logs.Count - numLogs)) {
                logTraceBuilder.Insert(0,line+"\n");
            }
            logTrace = logTraceBuilder.ToString();
            return logTrace;
        }
    }
}
