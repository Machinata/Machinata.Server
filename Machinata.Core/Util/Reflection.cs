using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {

    public static class Reflection {

        /// <summary>
        /// If the type is a Nullable<> type, then it returns the underlying type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static Type UnwrapNullableType(Type type) {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>)) { 
                return Nullable.GetUnderlyingType(type);
            } else {
                return type;
            }
        }

        /// <summary>
        /// Returns true if the child inherits from the parent.
        /// </summary>
        public static bool TypeInheritsFrom(Type child, Type parent) {
            return (parent.IsAssignableFrom(child));
        }

    }
}
