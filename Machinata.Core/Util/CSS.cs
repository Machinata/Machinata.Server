﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Util {
    public class CSS {


        public static string Minify(string css) {
            var minified = css;
            // Split into lines - its easier to handle line by line
            var lines = minified.Split('\n');
            for(var i = 0; i < lines.Length; i++) {
                // Remove comments /* xyz */
                // See https://www.thetopsites.net/article/50565187.shtml
                lines[i] = Regex.Replace(lines[i], @"/\*.+?\*/", string.Empty, RegexOptions.Singleline);
                // Trim line
                lines[i] = lines[i].Trim();
            }
            minified = string.Join("", lines);
            // Remove white space
            //TODO
            return minified;
        }

    }
}
