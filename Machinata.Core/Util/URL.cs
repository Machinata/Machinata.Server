using Machinata.Core.Exceptions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace Machinata.Core.Util {

    public class URL {

        public static string RemoveQueryParameter(string url, string key) {
            var uri = new UriBuilder(url);
            var queryParams = HttpUtility.ParseQueryString(uri.Query);
            queryParams.Remove(key); // remove
            uri.Query = queryParams.ToString();
            return uri.Uri.ToString();
        }

        public static string GetQueryParameter(string url, string key) {
            var uri = new UriBuilder(url);
            var queryParams = HttpUtility.ParseQueryString(uri.Query);
            return queryParams[key];
        }

        public static string SetQueryParameter(string url, string key, string val) {
            var uri = new UriBuilder(url);
            var queryParams = HttpUtility.ParseQueryString(uri.Query);
            queryParams[key] = val; // add or update
            uri.Query = queryParams.ToString();
            return uri.Uri.ToString();
        }

        /// <summary>
        /// </summary>
        private static string _getAccessCodeForURL(string url) {
            // Get input (clean out any existing access code
            var input = Core.Util.URL.RemoveQueryParameter(url, "access-code");
            // Return code
            return Core.Encryption.AccessCodeGenerator.GetAccessCodeForString(input);
        }

        /// <summary>
        /// </summary>
        public static string GetURLWithAccessCodeForURL(string url) {
            var accessCode = _getAccessCodeForURL(url);
            var urlWithAccessCode = Core.Util.URL.SetQueryParameter(url, "access-code", accessCode);
            return urlWithAccessCode;
        }

        /// <summary>
        /// </summary>
        public static bool ValidateURLWithAccessCodeForURL(string url, bool throwException = false) {
            // Get the access code
            var accessCodeInURL = Core.Util.URL.GetQueryParameter(url, "access-code");
            if (string.IsNullOrEmpty(accessCodeInURL)) throw new BackendException("invalid-access-code", "The access code is missing in the URL.");
            // Get input
            var urlWithoutAccessCode = Core.Util.URL.RemoveQueryParameter(url, "access-code");
            var expectedAccessCode = _getAccessCodeForURL(urlWithoutAccessCode);
            // Validate
            if (accessCodeInURL != expectedAccessCode) {
                if (throwException) throw new BackendException("invalid-access-code", "The access code is not correct.");
                return false;
            } else {
                return true;
            }
        }
        /// <summary>
        /// Get protocol host and port (if needed) uf a url
        /// https://stackoverflow.com/questions/21640/net-get-protocol-host-and-port
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetProtocolHostAndPort(string url) {
            Uri uri = new Uri(url);
            string output = uri.GetLeftPart(UriPartial.Authority);
            return output;
        }

    }
}
