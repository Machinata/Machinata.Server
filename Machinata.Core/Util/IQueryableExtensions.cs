using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {

    public static class IQueryableExtensions {

        public static IQueryable<T> Paginate<T>(this IQueryable<T> query, int pageNumber, int pageSize = 10) {
            return query.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

        public static IEnumerable<T> Paginate<T>(this IEnumerable<T> query, int pageNumber, int pageSize = 10) {
            return query.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

        public static IQueryable<T> Published<T>(this IQueryable<T> query) where T:class,Core.Model.IPublishedModelObject {
            return query.Where(e => e.Published == true);
        }

      
        public static IEnumerable<T> Published<T>(this IEnumerable<T> query) where T:class,Core.Model.IPublishedModelObject {
            return query.Where(e => e.Published == true);
        }

        public static IQueryable<T> Enabled<T>(this IQueryable<T> query) where T:class,Core.Model.IEnabledModelObject {
            return query.Where(e => e.Enabled == true);
        }

        /// <summary>
        /// Gets the by public identifier.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <param name="publicId">The public identifier.</param>
        /// <param name="throwExceptions">if set to <c>false</c> [throw exceptions] <c>null</c> will be returned if entity not found or Id is not in correct format.</param>
        /// <returns></returns>
        /// <exception cref="Backend404Exception">entity-not-found</exception>
        public static T GetByPublicId<T>(this IQueryable<T> query, string publicId , bool throwExceptions = true) where T:Core.Model.ModelObject {
            try {
                int id = Core.Model.ModelObject.GetIdForPublicId(publicId);
                var ret = query.Where(e => e.Id == id).SingleOrDefault();
                if (ret == null) throw new Backend404Exception("entity-not-found", $"The entity {typeof(T).Name} with ID {publicId} could not be found.");
                return ret;
            } catch (BackendException e) {
                if (throwExceptions) {
                    throw e;
                }
                return null;
            }
        }

        public static IQueryable<T> GetByPublicIds<T>(this IQueryable<T> query, IEnumerable<string> publicIds, bool throwException = true) where T : Core.Model.ModelObject {
            try {
                var ids = publicIds.Select(pi => Core.Model.ModelObject.GetIdForPublicId(pi)).ToList();
                return query.Where(e => ids.Contains(e.Id));
            }
            catch (BackendException e) {
                if (throwException) {
                    throw e;
                }
                return new List<T>().AsQueryable();
            }
        }

        public static T RemoveByPublicId<T>(this DbSet<T> set, string publicId) where T:Core.Model.ModelObject {
            var entity = set.GetByPublicId(publicId);
            if (entity == null) throw new BackendException("invalid-entity",$"The entity {typeof(T).Name} with ID {publicId} could not be found.");
            return set.Remove(entity);
        }
        
        public static T GetByShortURL<T>(this IQueryable<T> query, string shortURL, bool throwExceptions = true) where T:class,Core.Model.IShortURLModelObject {
            var ret = query.Where(e => e.ShortURL == shortURL).SingleOrDefault();
            if (ret == null && throwExceptions == true) throw new Backend404Exception("entity-not-found", $"The entity {typeof(T).Name} with Short URL {shortURL} could not be found.");
            return ret;
        }
        
        public static T GetByShortURL<T>(this IEnumerable<T> query, string shortURL, bool throwExceptions = true) where T:class,Core.Model.IShortURLModelObject {
            var ret = query.Where(e => e.ShortURL == shortURL).SingleOrDefault();
            if (ret == null && throwExceptions == true) throw new Backend404Exception("entity-not-found", $"The entity {typeof(T).Name} with Short URL {shortURL} could not be found.");
            return ret;
        }
        
        public static void Add<T>(this LinkedList<T> list, T item) {
            list.AddLast(item);
        }
        
        /// <summary>
        /// 
        /// WARNING: THIS MAY HAVE SIDE EFFECTS ON ANY PRIOR Include() WHERE
        /// SUCH INCLUDES ARE SOMEHOW NO LONGER WORKING.
        /// 
        /// Returns a query that is ordered by a random id.
        /// On databases, this will execute the stored function 'NewGuid()', which
        /// is automatically created on ModelSeed.Seed().
        /// 
        /// SQL Script required (on MYSQL databases): 
        /// "CREATE FUNCTION `NewGuid`() RETURNS DOUBLE RETURN RAND();"
        /// </summary>
        public static IQueryable<T> Randomize<T>(this IQueryable<T> query) where T: Model.ModelObject {
            //return (from e in query orderby Guid.NewGuid() select e);
            return query.OrderBy(e => Guid.NewGuid()).Distinct();
        }

        public static List<T> Randomize<T>(this List<T> query) {
            return query.OrderBy(e => Guid.NewGuid()).ToList();
        }

        public static T Random<T>(this List<T> query) {
            var index = Core.Util.Math.RandomInt(0, query.Count);
            return query.ElementAt(index);
        }

        /// <summary>
        /// Orders a IQueryable set dynamically using LINQ expressions.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <param name="orderByProperty">The order by property.</param>
        /// <param name="desc">if set to <c>true</c> [desc].</param>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> query, string orderByProperty, bool desc = false) where T:Core.Model.ModelObject {
            string command = desc ? "OrderByDescending" : "OrderBy";
            var type = typeof(T);
            // Try to get the property 'ArtistId' first
            var property = type.GetProperties().Where(p => p.Name.ToLower() == orderByProperty.ToLower()+"_id" || p.Name.ToLower() == orderByProperty.ToLower()+"id")
                .Where(p=>p.IsDerived() == false) // derived wont work
                .SingleOrDefault();
            if (property == null) {
                // Try to get the normal prop 'Artist'
                property = type.GetProperties().Where(p => p.Name.ToLower() == orderByProperty.ToLower()).SingleOrDefault();
            }
            // Create the expression tree
            if (property == null) throw new Exception($"The property '{orderByProperty}' does not exist on type {type.Name}.");
            var parameter = Expression.Parameter(type, "p");
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var orderByExpression = Expression.Lambda(propertyAccess, parameter);
            var resultExpression = Expression.Call(
                typeof(Queryable), 
                command, 
                new Type[] { type, property.PropertyType },
                query.Expression, Expression.Quote(orderByExpression)
            );
            var ret = query.Provider.CreateQuery<T>(resultExpression) as IOrderedQueryable<T>;
            // Make sure to order then by the id to create consistent results
            if (orderByProperty.ToLower() != "id") {
                if (desc == true) return ret.ThenByDescending(e => e.Id);
                else return ret.ThenBy(e => e.Id);
            } else {
                return ret;
            }
        }


        /// <summary>
        /// Filters a IQueryable set using the specified filter property by creating a expression tree.
        /// The query can contain multiple words (seperated by space), and must match AND.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <param name="filterProperty">The filter property.</param>
        /// <param name="filterQuery">The filter query.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        public static IQueryable<T> Filter<T>(this IQueryable<T> query, IEnumerable<string> filterProperties, string filterQuery) where T:Core.Model.ModelObject {
            // Init
            filterQuery = filterQuery.ToLower();
            var filterQueryWords = filterQuery.Split(' ');
            var type = typeof(T);

            //DONE: Support multiple properties using OR -> only or if multiple

            var expressions = new List<Expression>();
            var parameterExp = Expression.Parameter(typeof(T), "type");

            foreach (var filterProperty in filterProperties) {

                // Try to get the property 'ArtistId' first
                var property = type.GetProperties().Where(p => p.Name.ToLower() == filterProperty.ToLower() + "_id" || p.Name.ToLower() == filterProperty.ToLower() + "id").SingleOrDefault();
                if (property == null) {
                    // Try to get the normal prop 'Artist'
                    property = type.GetProperties().Where(p => p.Name.ToLower() == filterProperty.ToLower()).SingleOrDefault();
                }
                if (property == null) throw new Exception($"The property '{filterProperty}' does not exist on type {type.Name}.");

                // See https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/expression-trees/how-to-use-expression-trees-to-build-dynamic-queries
                // See https://stackoverflow.com/questions/278684/how-do-i-create-an-expression-tree-to-represent-string-containsterm-in-c

                // Prepare common expressions
                var propertyExp = Expression.Property(parameterExp, property.Name);
                MethodInfo methodContains = typeof(string).GetMethod("Contains", new[] { typeof(string) });

                // Make sure value is not null
                var nullValueExp = Expression.Constant(null, typeof(string));
                Expression finalExpression = Expression.NotEqual(propertyExp, nullValueExp);

                // Call contains expresion on each filter word
                foreach (var word in filterQueryWords) {
                    var queryValueExp = Expression.Constant(word, typeof(string));
                    var containsMethodExp = Expression.Call(propertyExp, methodContains, queryValueExp);
                    finalExpression = Expression.AndAlso(finalExpression, containsMethodExp);
                }
             
                expressions.Add(finalExpression);

            }

            // Default behaviour only one property
            if (expressions.Count == 1) {
                // Create an expression tree that represents the expression  
                MethodCallExpression whereCallExpression = CreateMethodCallExpression(query, expressions.First(), parameterExp);
                // Create the query using the expression and return
                var results = query.Provider.CreateQuery<T>(whereCallExpression);
                return results;
            } else {
                // Multiple: concat with OR
                var finalExpressionORed = expressions.First();
                foreach(var expression in expressions.Skip(1)) {
                    finalExpressionORed = Expression.Or(finalExpressionORed, expression);
                }

                // Create an expression tree that represents the expression  
                MethodCallExpression whereCallExpression = CreateMethodCallExpression(query, finalExpressionORed, parameterExp);
                var results = query.Provider.CreateQuery<T>(whereCallExpression);
                return results;
            }
        }

        private static MethodCallExpression CreateMethodCallExpression<T>(IQueryable<T> query, Expression expression, ParameterExpression parameterExp) where T : ModelObject {
            return Expression.Call(
                typeof(Queryable),
                "Where",
                new Type[] { query.ElementType },
                query.Expression,
                Expression.Lambda<Func<T, bool>>(expression, new ParameterExpression[] { parameterExp })
            );
        }


        #region To SQL

        // See http://rion.io/2016/10/19/accessing-entity-framework-core-queries-behind-the-scenes-in-asp-net-core/

        /*
    private static readonly TypeInfo QueryCompilerTypeInfo = typeof(QueryCompiler).GetTypeInfo();

    private static readonly FieldInfo QueryCompilerField = typeof(EntityQueryProvider).GetTypeInfo().DeclaredFields.First(x => x.Name == "_queryCompiler");

    private static readonly PropertyInfo NodeTypeProviderField = QueryCompilerTypeInfo.DeclaredProperties.Single(x => x.Name == "NodeTypeProvider");

    private static readonly MethodInfo CreateQueryParserMethod = QueryCompilerTypeInfo.DeclaredMethods.First(x => x.Name == "CreateQueryParser");

    private static readonly FieldInfo DataBaseField = QueryCompilerTypeInfo.DeclaredFields.Single(x => x.Name == "_database");

    private static readonly FieldInfo QueryCompilationContextFactoryField = typeof(Database).GetTypeInfo().DeclaredFields.Single(x => x.Name == "_queryCompilationContextFactory");

    public static string ToSql<TEntity>(this IQueryable<TEntity> query) where TEntity : class
    {
        if (!(query is EntityQueryable<TEntity>) && !(query is InternalDbSet<TEntity>))
        {
            throw new ArgumentException("Invalid query");
        }

        var queryCompiler = (IQueryCompiler)QueryCompilerField.GetValue(query.Provider);
        var nodeTypeProvider = (INodeTypeProvider)NodeTypeProviderField.GetValue(queryCompiler);
        var parser = (IQueryParser)CreateQueryParserMethod.Invoke(queryCompiler, new object[] { nodeTypeProvider });
        var queryModel = parser.GetParsedQuery(query.Expression);
        var database = DataBaseField.GetValue(queryCompiler);
        var queryCompilationContextFactory = (IQueryCompilationContextFactory)QueryCompilationContextFactoryField.GetValue(database);
        var queryCompilationContext = queryCompilationContextFactory.Create(false);
        var modelVisitor = (RelationalQueryModelVisitor)queryCompilationContext.CreateQueryModelVisitor();
        modelVisitor.CreateQueryExecutor<TEntity>(queryModel);
        var sql = modelVisitor.Queries.First().ToString();

        return sql;
    }*/

        #endregion

    }
}
