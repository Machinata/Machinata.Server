﻿using HtmlAgilityPack;
using Machinata.Core.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.UI.HtmlControls;

namespace Machinata.Core.Util {
    public class MetaTags {

        public static string ParseMetaTagProperty(string data, string name, string defaultValue) {


            //var match = Regex.Match(data, "<meta property=\"" + name + "\" content=\"([^</]*)\" ?\\/>", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            //if (match != null && match.Groups.Count > 1) {
            //    return match.Groups[1].Value;
            //} else {
            //    return defaultValue;
            //}

            //HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();

            //// Parse the HTML 
            //htmlDoc.LoadHtml(data);

            //// Get meta data 
            //HtmlNodeCollection nodes = htmlDoc.DocumentNode.SelectNodes("//meta");

            //if (nodes != null) {
            //    foreach (HtmlNode node in nodes) {
            //        string metaname = node.Attributes["property"].Value;
            //        string metacontent = node.Attributes["content"].Value;
            //        //  Console.WriteLine("Meta Name : {0}, Content : {1}", metaname, metacontent);
            //        return metacontent;
            //    }
            //}

            var htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(data);

            // Parse the HTML 
            return htmlDoc.DocumentNode.SelectSingleNode("//meta[@property='" + name + "']")?.GetAttributeValue("content", defaultValue);

        }
        public static string ParseMetaTagName(string data, string name, string defaultValue) {
           var htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(data); 

            // Parse the HTML 
            return htmlDoc.DocumentNode.SelectSingleNode("//meta[@name='"+ name+"']")?.GetAttributeValue("content", defaultValue);

        }
    }
}
