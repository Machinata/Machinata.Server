using Machinata.Core.Exceptions;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {

    public class XLSX {

        #region private Members /////////////////////////////////////////////////////////////////////////////
        private bool _compiled = false;
        private List<Dictionary<string, string>> _rows;
        private List<Dictionary<string, string>> _specialRows;


       
        private IXLWorksheet _worksheet;
        private XLWorkbook _wb;

        // Allow formulas to be evaluated when importing
        private bool _allowFormulas = false;
        private int? _dataRowStart;

        #endregion

        /// <summary>
        /// Name of first worksheet (only one supported)
        /// </summary>
        public string Name
        {
            get
            {
                return _worksheet?.Name;
            }
        }


        public int? DataRowStart {
            get {
                return _dataRowStart;
            }
        }

        /// <summary>
        /// Creates new empty document
        /// </summary>
        /// <param name="sheetname">The sheetname.</param>
        public XLSX(string sheetname) {
            _wb = new XLWorkbook();
            _worksheet = _wb.AddWorksheet(sheetname);
            _rows = new List<Dictionary<string, string>>();
        }
        

        /// <summary>
        /// Creates document from 
        /// </summary>
        /// <param name="stream">The stream.</param>
        public XLSX(Stream stream, bool allowFormulas) {
            _wb = new XLWorkbook(stream);
            _worksheet = _wb.Worksheet(1);
            _rows = new List<Dictionary<string, string>>();
            _allowFormulas = allowFormulas;
            var columns = new List<string>();
            int r = 0;
            foreach (IXLRow row in _worksheet.Rows()) {
                //Use the first row to add columns to DataTable.
                if (r++ == 0) {
                    foreach (IXLCell cell in row.Cells()) {
                        if(string.IsNullOrEmpty(cell.Value.ToString())){
                            break;
                        }
                        columns.Add(cell.Value.ToString());
                    }

                } else {
                    //Add rows to DataTable.
                    var cellsPerRow = new Dictionary<string, string>();
                    var cells = row.Cells();
                    foreach (IXLCell cell in cells) {

                        // Break if cell doesn't match a column from the first row
                        if (cell.Address.ColumnNumber > columns.Count + 1) {
                            break;
                        }
                       
                        string key = columns[cell.Address.ColumnNumber - 1]; 
                        if (cell.HasFormula && _allowFormulas == false) {
                             throw new BackendException("import-error", "Import of Cells with formulas is not activated");
                        }

                        if (_dataRowStart == null) {
                            _dataRowStart = r;
                        }

                        try {
                            cellsPerRow.Add(key, cell.Value.ToString());
                        }catch (Exception e) {
                            throw new BackendException("import-error", $"Could not get value of cell {cell.Address}",e);
                        }
                      
                    }
                    foreach (var column in columns) {
                        if (!cellsPerRow.ContainsKey(column)) {
                            cellsPerRow[column] = string.Empty;
                        }
                    }

                    // Hint: removed, why cant we just iterate til the end of rows?
                    // quit if all rows are empty 
                    //if (cellsPerRow.All(c => string.IsNullOrEmpty(c.Value?.ToString()))) {
                    //    break;
                    //}

                    // Add row only if any values
                    if (cellsPerRow.All(c => string.IsNullOrEmpty(c.Value?.ToString())) == false) {
                        _rows.Add(cellsPerRow);
                    } else {
                        var emptyRow = true;
                    }
                        

                }


            }
        }


        public XLSX(Stream stream, bool allowFormulas, string ignoreRowKeyword = null, string specialRowKeyword = null) {
            _wb = new XLWorkbook(stream);
            _worksheet = _wb.Worksheet(1);
            _rows = new List<Dictionary<string, string>>();
            _specialRows = new List<Dictionary<string, string>>();
            _allowFormulas = allowFormulas;
            _dataRowStart = null;
            var columns = new Dictionary<int, string>();
            int r = 0;
            foreach (IXLRow row in _worksheet.Rows()) {
                //Use the first row to add columns to DataTable.
                if (r++ == 0) {

                    int cellsUsed = row.CellsUsed().Count();
                    foreach (IXLCell cell in row.Cells(usedCellsOnly: true)) {
                        if (string.IsNullOrEmpty(cell.Value.ToString())) {
                            // ignore this cell/column
                        } else {
                            columns.Add(cell.Address.ColumnNumber, cell.Value.ToString());
                        }
                    }

                } else {
                    //Add rows to DataTable.
                    var cellsPerRow = new Dictionary<string, string>();
                    var cellsPerSpecialRow = new Dictionary<string, string>();
                    var cells = row.Cells();
                    var firstCell = cells.FirstOrDefault();
                    var addToSpecialRow = false;

                    foreach (IXLCell cell in cells) {

                        try {

                            // Ignore this row
                            if (ignoreRowKeyword != null && ignoreRowKeyword == cell.Value.ToString() && firstCell == cell) {
                                break;
                            }

                            // Set special row
                            if (specialRowKeyword != null && specialRowKeyword == cell.Value.ToString() && firstCell == cell) {
                                addToSpecialRow = true;
                                break;
                            }


                            // Break if cell doesn't match a column from the first row
                            //if (cell.Address.ColumnNumber > columns.Count + 1) {
                            //    break;
                            //}
                            if (_dataRowStart == null) {
                                _dataRowStart = r;
                            }


                            // Not an imported cell
                            if (columns.ContainsKey(cell.Address.ColumnNumber) == false) {
                                continue;
                            }

                            string key = columns[cell.Address.ColumnNumber];
                            if (cell.HasFormula && _allowFormulas == false) {
                                throw new BackendException("import-error", "Import of Cells with formulas is not not activated");
                            }


                            // Special Row?
                            if (addToSpecialRow == true) {
                                cellsPerSpecialRow.Add(key, cell.Value.ToString());
                            } else {
                                cellsPerRow.Add(key, cell.Value.ToString());
                            }

                        } catch (Exception e) {
                            throw new BackendException("import-error", $"Could not get value of cell {cell.Address}", e);
                        }

                    }
                    foreach (var column in columns.Values) {
                        if (!cellsPerRow.ContainsKey(column)) {
                            cellsPerRow[column] = string.Empty;
                        }
                    }

                    // Hint: removed, why cant we just iterate til the end of rows?
                    // quit if all rows are empty 
                    //if (cellsPerRow.All(c => string.IsNullOrEmpty(c.Value?.ToString()))) {
                    //    break;
                    //}

                    // Add row only if any values
                    if (cellsPerRow.All(c => string.IsNullOrEmpty(c.Value?.ToString())) == false) {
                        _rows.Add(cellsPerRow);
                    } else {
                        var emptyRow = true;
                    }

                    // Special Rows
                    if (cellsPerSpecialRow.Any()) {
                        _specialRows.Add(cellsPerSpecialRow);
                    }

                }


            }
        }

        public void AddRow(Dictionary<string, string> keyValues) {
            _rows.Add(keyValues);
        }

        public IEnumerable<Dictionary<string,string>> GetRows() {
            return _rows;
        }

        public void RemoveRowFromWorkSheet(int row) {
            _worksheet.Row(row).Delete();
        }

        public IEnumerable<Dictionary<string, string>> GetSpecialRows() {
            return _specialRows;
        }

        public byte[] GetContent() {
            if (!_compiled) _compile();
            using (var ms = new System.IO.MemoryStream()) {
                _wb.SaveAs(ms, true);

                ms.Position = 0;
                byte[] byteArray = ms.ToArray();

                //Clean up the memory stream
                ms.Flush();
                ms.Close();
                return byteArray;
            }
        }

        public void _compile() {
            // Get all columns
            List<string> columns = new List<string>();

            foreach (var row in _rows) {
                foreach (var keyval in row) {
                    if (!columns.Contains(keyval.Key)) {
                        columns.Add(keyval.Key);
                    }
                }
            }

            int rowIndex = 0;
            int columnIndex = 0;


            // Write header
            for (; columnIndex < columns.Count; columnIndex++) {
                var headerCell = _worksheet.Cell(rowIndex + 1, columnIndex + 1);
                headerCell.Value = columns[columnIndex];
                headerCell.Style.Font.Bold = true;
            }

            // Write rows
            foreach (var row in _rows) {
                columnIndex = 0;
                rowIndex++;
                for (; columnIndex < columns.Count; columnIndex++) {

                    var key = columns[columnIndex];
                    var val = "";
                    if (row.ContainsKey(key)) {
                        val = row[key] ?? string.Empty;
                    }
                    var cell = _worksheet.Cell(rowIndex + 1, columnIndex + 1);
                    try {
                        cell.Value = (val);
                    }
                    catch(Exception e) {
                        throw  new Exception($"There was e problem setting value of cell {key} row {rowIndex}, column {columnIndex}, value: {val}",e);
                    }
                    // Set every cell to string
                    //cell.DataType = XLCellValues.Text;

                }

            }

            // Adjust Column Widths
            _worksheet.Columns().AdjustToContents();
            foreach ( var column in _worksheet.Columns()) {

            }

            _compiled = true;
        }

        public static Dictionary<string, string> CreateBlankRow() {
            return new Dictionary<string, string>();
        }


        
    }

}

