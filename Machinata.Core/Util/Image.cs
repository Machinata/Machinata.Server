﻿using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {
    public static class Image {

       


        /// <summary>
        /// Gets the format of an Image
        /// 
        /// https://stackoverflow.com/questions/5065371/how-to-identify-cmyk-images-in-asp-net-using-c-sharp
        /// </summary>
        /// <param name="bitmap"></param>
        /// <returns></returns>
        public static ImageColorFormat GetColorFormat(Bitmap bitmap) {
            const int pixelFormatIndexed = 0x00010000;
            const int pixelFormat32bppCMYK = 0x200F;
            const int pixelFormat16bppGrayScale = (4 | (16 << 8));

            // Check image flags
            var flags = (ImageFlags)bitmap.Flags;
            if (flags.HasFlag(ImageFlags.ColorSpaceCmyk) || flags.HasFlag(ImageFlags.ColorSpaceYcck)) {
                return ImageColorFormat.Cmyk;
            }
            else if (flags.HasFlag(ImageFlags.ColorSpaceGray)) {
                return ImageColorFormat.Grayscale;
            }

            // Check pixel format
            var pixelFormat = (int)bitmap.PixelFormat;
            if (pixelFormat == pixelFormat32bppCMYK) {
                return ImageColorFormat.Cmyk;
            }
            else if ((pixelFormat & pixelFormatIndexed) != 0) {
                return ImageColorFormat.Indexed;
            }
            else if (pixelFormat == pixelFormat16bppGrayScale) {
                return ImageColorFormat.Grayscale;
            }

            // Default to RGB
            return ImageColorFormat.Rgb;
        }

        public enum ImageColorFormat {
            Rgb,
            Cmyk,
            Indexed,
            Grayscale
        }
    }
}
