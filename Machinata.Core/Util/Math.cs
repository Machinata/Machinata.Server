using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {
    public class Math {

        private static Random _rnd = new Random((int)DateTime.Now.Ticks);

        /// <summary>
        /// Returns a random int between a min and max (excluding the max).
        /// This method automatically uses a static unique seed.
        /// </summary>
        /// <param name="min">The minimum.</param>
        /// <param name="max">The maximum.</param>
        /// <returns></returns>
        public static int RandomInt(int min, int max) {
            return _rnd.Next(min, max);
        }

        /// <summary>
        /// Returns a random int.
        /// This method automatically uses a static unique seed.
        /// </summary>
        /// <returns></returns>
        public static int RandomInt() {
            return _rnd.Next();
        }

        /// <summary>
        /// Rounds a decimal to the given places (1 by default), but snaps the result if it rounds to ".0".
        /// </summary>
        /// <param name="val">The value.</param>
        /// <param name="decimalPlaces">The decimal places.</param>
        /// <returns></returns>
        public static string SnapRoundToString(decimal val, int decimalPlaces = 1) {
            var roundedVal = decimal.Round(val, decimalPlaces);
            if (roundedVal == (int)val) return ((int)val).ToString();
            else return roundedVal.ToString();
        }

        /// <summary>
        /// Returns a random int between a min and max (excluding the max).
        /// This method automatically uses a static unique seed.
        /// </summary>
        public static double RandomDouble(double min, double max) {
            return _rnd.NextDouble() * (max - min) + min;
        }


        /// <summary>
        /// Returns a random large big integer in the range int.MaxValue*0.75 to int.MaxValue
        /// </summary>
        public static int GenerateRandomBigInteger() {
            var rnd = new Random((int)(DateTime.Now.Ticks % int.MaxValue));
            var ret = rnd.Next((int)(int.MaxValue * 0.75), int.MaxValue);
            return ret;
        }

        /// <summary>
        /// Generates a large prime number within the range int.MaxValue*0.75 to int.MaxValue
        /// </summary>
        public static int GenerateLargePrimeNumber() {
            var rnd = new Random((int)(DateTime.Now.Ticks % int.MaxValue));
            long attempts = 0;
            while (true) {
                attempts++;
                var ret = rnd.Next((int)(int.MaxValue * 0.75), int.MaxValue);
                if (IsPrimeNumber(ret)) {
                    //Console.WriteLine("got prime " + ret + " after " + attempts + " attempts");
                    //Console.WriteLine(ret);
                    return ret;
                };
            }
        }

        /// <summary>
        /// Returns true if the input numer is a prime number.
        /// </summary>
        public static bool IsPrimeNumber(int number) {
            // via https://stackoverflow.com/questions/15743192/check-if-number-is-prime-number
            if (number == 1) return false;
            if (number == 2 || number == 3 || number == 5) return true;
            if (number % 2 == 0 || number % 3 == 0 || number % 5 == 0) return false;

            var boundary = (int)System.Math.Floor(System.Math.Sqrt(number));

            // You can do less work by observing that at this point, all primes 
            // other than 2 and 3 leave a remainder of either 1 or 5 when divided by 6. 
            // The other possible remainders have been taken care of.
            int i = 6; // start from 6, since others below have been handled.
            while (i <= boundary) {
                if (number % (i + 1) == 0 || number % (i + 5) == 0)
                    return false;

                i += 6;
            }

            return true;
        }
    }
}
