using Machinata.Core.Builder;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Util {
    public class Git {

        public class GitInfo {
            public bool IsDirty;
            public string Branch;
            public string Commit;
            public string Sha;
            public string Commits;
            public string Tag;
            public string BaseTag;
            public override string ToString() {
                var str = new StringBuilder();
                str.AppendLine("Branch: " + Branch);
                str.AppendLine("Commit: " + Commit);
                str.AppendLine("Sha: " + Sha);
                str.AppendLine("IsDirty: " + IsDirty);
                str.AppendLine("Commits: " + Commits);
                str.AppendLine("Tag: " + Tag);
                str.AppendLine("BaseTag: " + BaseTag);
                return str.ToString();
            }
        }

        public class GitLog : Core.Model.ModelObject {

            [FormBuilder]
            public string Branch { get; set; }

            [FormBuilder]
            public string CommitHash { get; set; }

            [FormBuilder]
            [FormBuilder(Forms.Admin.LISTING)]
            public string CommitAuthor { get; set; }

            [FormBuilder]
            [FormBuilder(Forms.Admin.LISTING)]
            public DateTime CommitDate { get; set; }

            [FormBuilder]
            [FormBuilder(Forms.Admin.LISTING)]
            public string CommitSubject { get; set; }

            public string SearchText { get; set; }

            public bool IsUpstream {
                get {
                    return this.CommitSubject != null && this.CommitSubject.StartsWith("upstream:");
                }
            }

            public bool IsBreakingChange {
                get {
                    return this.CommitSubject != null && this.CommitSubject.Contains("breaking change");
                }
            }

            public void CompileSearchText() {
                this.SearchText =
                    this.Branch?.ToLower() + " " +
                    this.CommitSubject?.ToLower() + " " +
                    this.CommitHash?.ToLower() + " " +
                    this.CommitDate.ToLongDateString()?.ToLower() + " " +
                    this.CommitSubject?.ToLower();
                if (this.ChangedFiles != null) this.SearchText += " " + string.Join(" ", this.ChangedFiles);
            }

            public List<string> ChangedFiles { get; set; }
        }

        /// <summary>
        /// Gets the git information about the current the branch
        /// Encapsulates https://github.com/GitTools/GitVersion
        /// </summary>
        /// <returns></returns>
        public static GitInfo GetInfo() {
            var info = new GitInfo();

            /*
            info.Commit = ThisAssembly.Git.Commit;
            info.Branch = ThisAssembly.Git.Branch; 
            info.Sha = ThisAssembly.Git.Sha;
            info.Commits = ThisAssembly.Git.Commits;
            info.Tag = ThisAssembly.Git.Tag;
            info.IsDirty = ThisAssembly.Git.IsDirty;
            info.BaseTag = ThisAssembly.Git.BaseTag;
            */

            info.Commit = "NO LONGER SUPPORTED";
            info.Branch = "NO LONGER SUPPORTED";
            info.Sha = "NO LONGER SUPPORTED";
            info.Commits = "NO LONGER SUPPORTED";
            info.Tag = "NO LONGER SUPPORTED";
            info.IsDirty = false;
            info.BaseTag = "NO LONGER SUPPORTED";

            return info;
        }

        public static IEnumerable<GitLog> ParsePrunedLogs(string keyword, string logFile = null) {
            return ParseLogs(logFile).Where(e => e.SearchText.Contains(keyword) && !e.SearchText.Contains("merge branch"));
        }

        public static IEnumerable<GitLog> ParseProductSpecificLogs(string keyword, bool includeUpstream = true, string logFile = null, string excludeFromUpstream = null) {
            var allLogs = ParseLogs(logFile);
            var ret = new List<GitLog>();
            foreach (var log in allLogs) {
                if (log.CommitSubject.Contains(keyword)) {
                    ret.Add(log);
                } else if (includeUpstream) {
                    if (excludeFromUpstream == null || log.CommitSubject.Contains(excludeFromUpstream) == false) {
                        log.CommitSubject = "upstream: " + log.CommitSubject;
                        ret.Add(log);
                    }
                }
            }
            return ret;
        }


        public static string GetLogs(string dir, string branch = null) {
            // git --no-pager log --all --decorate --source --name-only product/csam-reporting > $(ProjectDir)\Config\$(ProjectName).gitlog

            // Setup process start info
            var startInfo = new ProcessStartInfo();
            startInfo.FileName = "git";
            startInfo.Arguments = "--no-pager log --all --decorate --source";
            startInfo.WorkingDirectory = dir;
            startInfo.UseShellExecute = false;
            if (branch != null) startInfo.Arguments += " --name-only " + branch;
            startInfo.RedirectStandardOutput = true;
            // Start the process, capturing output
            var proc = new Process();
            proc.StartInfo = startInfo;
            proc.Start();
            var ret = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();
            // Return
            return ret;
        }


        /// <summary>
        /// Parses a log file generated by git and returns individual git log entities.
        /// The log file should be generated via a command similar to:
        /// ```git --no-pager log --all --decorate --source --name-only product/csam-reporting > $(ProjectDir)\Config\$(ProjectName).gitlog```
        /// and registered in a pre-build event.
        /// </summary>
        /// <returns></returns>
        public static List<GitLog> ParseLogs(string logFile = null) {
            var ret = new List<GitLog>();
            if (logFile == null) logFile = DefaultLogFile();
            var logContents = System.IO.File.ReadAllLines(logFile);

            GitLog currentLog = null;
            foreach (var line in logContents) {
                var lineTrimmed = line.Trim();
                var lineSegs = lineTrimmed.Split(' ', '\t');
                if (line.StartsWith("commit ")) {
                    // Commit - New log!

                    // Save current
                    if (currentLog != null) {
                        currentLog.CompileSearchText();
                        ret.Add(currentLog);
                    }

                    // Setup new
                    currentLog = new GitLog();
                    currentLog.CommitHash = lineSegs.ElementAt(1);
                    currentLog.Branch = lineSegs.ElementAt(2);
                } else {
                    // Skip until we find a new log
                    if (currentLog == null) continue;
                }

                if (line.StartsWith("Author: ")) {
                    // Author
                    currentLog.CommitAuthor = string.Join(" ", lineSegs.Skip(1));
                } else if (line.StartsWith("Date: ")) {
                    // Date
                    var dateString = string.Join(" ", lineSegs.Skip(1)).Trim();
                    currentLog.CommitDate = DateTime.ParseExact(dateString, "ddd MMM d HH:mm:ss yyyy K", CultureInfo.InvariantCulture);
                    currentLog.CommitDate = DateTime.SpecifyKind(currentLog.CommitDate, DateTimeKind.Local);
                    currentLog.CommitDate = currentLog.CommitDate.ToUniversalTime();


                } else if (line.StartsWith("    ")) {
                    // Subject
                    currentLog.CommitSubject = lineTrimmed.Replace("{", "[").Replace("}", "]");
                } else {
                    // Must be a file
                    if (currentLog.ChangedFiles == null) currentLog.ChangedFiles = new List<string>();
                    currentLog.ChangedFiles.Add(line);
                }
            }

            /*
            commit 278a9fc1f51508c0d4ef2e62e590ebc1d28bfbbe	refs/remotes/origin/product/kets (tag: product/kets/2020-01-27/09.25, origin/product/kets)
            Author: Micha Aprile <micha@nerves.ch>
            Date:   Mon Jan 27 09:23:48 2020 +0100

                kontakt formulare: re-add address to konakt forms

            Machinata.Product.KETSWebsite/Templates/Machinata.Email/anmeldung.address.lehrgang.htm
            Machinata.Product.KETSWebsite/Templates/Machinata.Email/anmeldung.address.workshop.htm
            Machinata.Product.KETSWebsite/Templates/Machinata.Product.KETSWebsite/default/form.address.lehrgang.htm
            Machinata.Product.KETSWebsite/Templates/Machinata.Product.KETSWebsite/default/form.address.workshop.htm
*/

            return ret;
        }

        public static string DefaultLogFile() {
            return Core.Config.BinPath + "Config" + Core.Config.PathSep + Core.Config.ProjectPackage + ".gitlog";
        }

        public static bool HasDefaultLogFile() {
            return File.Exists(DefaultLogFile());
        }

        public static IEnumerable<Git.GitLog> GetGitLog(string keyword, bool includeUpstream = true, bool filterOutMergeCommits = true) {
            try {
                var gitLogs = Core.Util.Git.ParseProductSpecificLogs(keyword, includeUpstream, null).DistinctBy(l => l.CommitSubject);
                if (filterOutMergeCommits == true) { 
                    gitLogs = gitLogs.Where(l =>
                        l.CommitSubject.Contains("Merge commit") == false
                        && l.CommitSubject.Contains("Merge branch") == false
                        && l.CommitSubject.Contains("See merge request") == false
                        && l.CommitSubject.Contains("This reverts commit") == false
                        && l.CommitSubject.StartsWith("upstream: #") == false
                        && l.CommitSubject.StartsWith("#") == false);
            }
                return gitLogs;
            } catch (Exception e){

                return null;
            }
        }

    }
}
