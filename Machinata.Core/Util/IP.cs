﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace Machinata.Core.Util {
    public class IP {

        public static bool IsInSameSubnet(string ip1, string ip2, string subnetMask) {

            IPAddress getNetworkAddress(IPAddress address, IPAddress subnetMask2) {
                byte[] ipAdressBytes = address.GetAddressBytes();
                byte[] subnetMaskBytes = subnetMask2.GetAddressBytes();

                if (ipAdressBytes.Length != subnetMaskBytes.Length)
                    throw new ArgumentException("Lengths of IP address and subnet mask do not match.");

                byte[] broadcastAddress = new byte[ipAdressBytes.Length];
                for (int i = 0; i < broadcastAddress.Length; i++) {
                    broadcastAddress[i] = (byte)(ipAdressBytes[i] & (subnetMaskBytes[i]));
                }
                return new IPAddress(broadcastAddress);
            }

            IPAddress address1 = IPAddress.Parse(ip1);
            IPAddress address2 = IPAddress.Parse(ip2);
            IPAddress subnetMaskAddress = IPAddress.Parse(subnetMask);
            IPAddress network1 = getNetworkAddress(address1, subnetMaskAddress);
            IPAddress network2 = getNetworkAddress(address2, subnetMaskAddress);

            return network1.Equals(network2);
        }

        public static string CIDRToMask(int cidr) {
            var mask = (cidr == 0) ? 0 : uint.MaxValue << (32 - cidr);
            var bytes = BitConverter.GetBytes(mask).Reverse().ToArray();
            return new IPAddress(bytes).ToString();
        }

        public static bool DoesIPMatcheAnyAddressOrSubnetInList(string ip, List<string> ipsOrSubnets) {
            foreach (var ipOrSubnet in ipsOrSubnets) {
                // Is it a subnet?
                if(ipOrSubnet.Contains("/")) {
                    var segs = ipOrSubnet.Split('/');
                    if(segs.Length != 2) { throw new Exception("invalid subnet"); }
                    var subnetIP = segs.First();
                    var subnetCIDR = int.Parse(segs.Last());
                    var subnetMask = CIDRToMask(subnetCIDR);
                    if(IsInSameSubnet(ip, subnetIP, subnetMask)) return true; 
                } else {
                    // Direct ip
                    if (ip == ipOrSubnet) return true;
                }
            }
            return false;
        }

    }
}
