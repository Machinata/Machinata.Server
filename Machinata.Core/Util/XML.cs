using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Machinata.Core.Util {

    public class XML {

        public static string XMLEncodeString(string value) {
            if (value == null) {
                return null;
            } else {
                value = value.Replace("&", "&amp;");
                value = value.Replace("<", "&lt;");
                value = value.Replace(">", "&gt;");
                value = value.Replace("\"", "&quot;");
                return value;
            }
        }

        public static string XMLDecodeString(string value) {
            if (value == null) {
                return null;
            } else {
                value = value.Replace("&quot;", "\"");
                value = value.Replace("&gt;", ">");
                value = value.Replace("&lt;", "<");
                value = value.Replace("&amp;", "&");
                return value;
            }
        }

        public static string HTMLDecodeString(string value) {
            return System.Net.WebUtility.HtmlDecode(value);
        }

      

    }


    public static class XElementExtensions {

        public static IEnumerable<XElement> ElementsAnyNS<T>(this IEnumerable<T> source, string localName) where T : XContainer {
            return source.Elements().Where(e => e.Name.LocalName == localName);
        }

        public static IEnumerable<XElement> ElementsAnyNS<T>(this T source, string localName) where T : XElement {
            return source.Elements().Where(e => e.Name.LocalName == localName);
        }


        public static XElement ElementAnyNS<T>(this T source, string localName) where T : XElement {
            return source.Elements().FirstOrDefault(e => e.Name.LocalName == localName);
        }

        public static XElement ElementWithAttribute<T>(this IEnumerable<T> source, string attrName, string attrValue) where T : XElement {
            return source.FirstOrDefault(el => el.Attribute(attrName) != null &&
                         el.Attribute(attrName).Value == attrValue);
        }

       

   
    }
}
