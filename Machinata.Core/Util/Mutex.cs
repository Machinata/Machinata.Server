using Machinata.Core.Threading;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Machinata.Core.Util {
    public static class Mutex {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        private static KeyedMutexStore _mutexStore = new KeyedMutexStore();

        public static KeyedMutex CreateKeyedMutexForFilepath(string filepath) {
            string mutexKey = filepath;
            return _mutexStore.GetMutexForKey(mutexKey);
        }

        public static KeyedMutex CreateKeyedMutexForId(string id) {
            string mutexKey = id;
            return _mutexStore.GetMutexForKey(mutexKey);
        }

        public static KeyedMutexStore GetStore() {
            return _mutexStore;
        }

        #region Old Mutex using OS Mutex (deprecated)

        /*
        public static System.Threading.Mutex CreateKeyedMutexForFilepath(string filepath) {
            // Init
            string mutexKey = filepath.Replace("/", "_").Replace(Core.Config.PathSep, "_").Replace(":", "_");
            if (mutexKey.Length > 260) {
                _logger.Warn($"CreateKeyedMutexForFilepath got a filepath longer than 260 characters: {filepath}. The start of the string will be trimmed.");
                mutexKey = mutexKey.Substring(mutexKey.Length - 260, 260);
            }
            // Get a mutex on the cachepath derived mutexkey to make sure two threads dont try to
            // create the cache at the same time
            var ret = new System.Threading.Mutex(false, mutexKey);
            return ret;
        }

        public static System.Threading.Mutex CreateKeyedMutexForId(string id) {
            // Init
            string mutexKey = id.Replace("/", "_").Replace(Core.Config.PathSep, "_").Replace(":", "_");
            if (mutexKey.Length > 260) {
                _logger.Warn($"CreateKeyedMutexForId got a filepath longer than 260 characters: {id}. The start of the string will be trimmed.");
                mutexKey = mutexKey.Substring(mutexKey.Length - 260, 260);
            }
            var ret = new System.Threading.Mutex(false, mutexKey);
            return ret;
        }*/

        #endregion
    }
}
