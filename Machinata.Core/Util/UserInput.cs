using Machinata.Core.Exceptions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace Machinata.Core.Util {

    public class UserInput {


        /// <summary>
        /// Spits out a MSISDN formatted number for the given input.
        /// The originCountry can help detect shorthands for the following countries:
        ///  - ```ch```: Switzerland (only mobiles)
        /// Without a originCountry implementation, this routine only does very basic parsing, but more a cleansing.
        /// </summary>
        /// <returns></returns>
        public static string FormatPhoneNumberAsMSISDN(string input, string originCountry = null) {
            // Sanity
            if (input == null || input == "") return null;
            // Ensure to catch all errors
            try {

                // Clean
                // Strip all non numbers
                var inputCleaned = input.Trim();
                inputCleaned = Regex.Replace(inputCleaned, "[^0-9]", "");

                // Now we begin trying to guess the full MSISDN...
                var msisdnNumber = inputCleaned;

                // Country specific guesses
                if(originCountry == "ch") {
                    if (msisdnNumber.StartsWith("0041")) msisdnNumber = msisdnNumber.ReplacePrefix("0041", "41");

                    if (msisdnNumber.StartsWith("41075")) msisdnNumber = msisdnNumber.ReplacePrefix("41075", "4175");
                    if (msisdnNumber.StartsWith("41076")) msisdnNumber = msisdnNumber.ReplacePrefix("41076", "4176");
                    if (msisdnNumber.StartsWith("41077")) msisdnNumber = msisdnNumber.ReplacePrefix("41077", "4177");
                    if (msisdnNumber.StartsWith("41078")) msisdnNumber = msisdnNumber.ReplacePrefix("41078", "4178");
                    if (msisdnNumber.StartsWith("41079")) msisdnNumber = msisdnNumber.ReplacePrefix("41079", "4179");

                    if (msisdnNumber.StartsWith("075")) msisdnNumber = msisdnNumber.ReplacePrefix("075", "4175");
                    if (msisdnNumber.StartsWith("076")) msisdnNumber = msisdnNumber.ReplacePrefix("076", "4176");
                    if (msisdnNumber.StartsWith("077")) msisdnNumber = msisdnNumber.ReplacePrefix("077", "4177");
                    if (msisdnNumber.StartsWith("078")) msisdnNumber = msisdnNumber.ReplacePrefix("078", "4178");
                    if (msisdnNumber.StartsWith("079")) msisdnNumber = msisdnNumber.ReplacePrefix("079", "4179");
                    
                    if (msisdnNumber.StartsWith("75") && msisdnNumber.Length == 9) msisdnNumber = msisdnNumber.ReplacePrefix("75", "4175");
                    if (msisdnNumber.StartsWith("76") && msisdnNumber.Length == 9) msisdnNumber = msisdnNumber.ReplacePrefix("76", "4176");
                    if (msisdnNumber.StartsWith("77") && msisdnNumber.Length == 9) msisdnNumber = msisdnNumber.ReplacePrefix("77", "4177");
                    if (msisdnNumber.StartsWith("78") && msisdnNumber.Length == 9) msisdnNumber = msisdnNumber.ReplacePrefix("78", "4178");
                    if (msisdnNumber.StartsWith("79") && msisdnNumber.Length == 9) msisdnNumber = msisdnNumber.ReplacePrefix("79", "4179");
                }

                // Convert 00 to +
                if (msisdnNumber.StartsWith("00")) msisdnNumber = msisdnNumber.ReplacePrefix("00", "");

                // Return with +
                return "+"+msisdnNumber;

            } catch(Exception e) {
                // Failsafe
                return input;
            }
        }


        public static bool IsValidEmailAddress(string email) {
            try {

          
                var test1 = new System.Net.Mail.MailAddress(email).Address == email;
                var test2 = new System.ComponentModel.DataAnnotations.EmailAddressAttribute().IsValid(email);

                if (test1 && test2) {
                    return true;
                }

                return false;
            } catch {
                return false;
            }

        }

    }
}
