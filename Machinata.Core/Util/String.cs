using Machinata.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Core.Util {

    public class String {
        /// <summary>
        /// Checks for spam links.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <param name="throwException">if set to <c>true</c> will throw an exception.</param>
        /// <returns>True if the value contains a spam link.</returns>
        /// <exception cref="BackendException">
        /// invalid-link;Sorry, links are not allowed.
        /// or
        /// invalid-link;Sorry, links are not allowed.
        /// or
        /// invalid-link;Sorry, links are not allowed.
        /// </exception>
        public static bool CheckForSpamLinks(string val, bool throwException = true) {
            // See http://www.regexr.com/ for testing
            // Check for any URL http://example.com with protocol
            if (System.Text.RegularExpressions.Regex.IsMatch(val, "(http(s)?://)(([0-9a-zA-Z-])+(\\.))?([0-9a-zA-Z-])+(\\.)([a-z]){2,8}")) {
                if (throwException) throw new BackendException("invalid-link", "Sorry, links are not allowed.");
                return true;
            }
            // Check for any short-hand URL like bit.ly/389233 - this one requires a slash to make sure text like 'hi.how are you' doesnt trigger
            if (System.Text.RegularExpressions.Regex.IsMatch(val, "([0-9a-zA-Z.-]){2,30}(\\.)([a-z]){2,8}\\/([0-9a-zA-Z-_?=&#]){1,30}")) {
                if (throwException) throw new BackendException("invalid-link", "Sorry, links are not allowed.");
                return true;
            }
            // Standard www. and .com domains
            //TODO: this blocks strings like 'aaawwwww.'... Create regest for xxx|xx.aaaaaaaaaaaa.yyy|yy
            if (val.StartsWith("www.") || val.EndsWith(".com") || val.EndsWith(".org") || val.EndsWith(".net")) {
                if (throwException) throw new BackendException("invalid-link", "Sorry, links are not allowed.");
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets the a key/value dictionary from a string.
        /// The default seperator is ';' for key/values and '=' for key and value.
        /// Example: test=xyz;val2=abc
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public static Dictionary<string,string> GetKeyValuesFromString(string str) {
            var ret = new Dictionary<string, string>();
            if (str == null) return ret;
            foreach(var keyVal in str.Split(';')) {
                var k = keyVal.Split('=').FirstOrDefault();
                var v = keyVal.Split('=').LastOrDefault();
                if (k != null) ret.Add(k, v);
            }
            return ret;
        }


        /// <summary>
        /// Creates a summarized text of the post text for the given length. Can automatically add trailing dots, and clean out any non-ascii chars.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="length">The length.</param>
        /// <param name="includeTrail">if set to <c>true</c> [include trail].</param>
        /// <param name="allowSpecialCharacters">if set to <c>true</c> [allow special characters].</param>
        /// <returns></returns>
        public static string CreateSummarizedText(string text, int length, bool includeTrail = false, bool allowSpecialCharacters = false, bool breakAtWordIfPossible = true) {
            // Remove special characters
            if (!allowSpecialCharacters) {
                text = ReplaceUmlauts(text);
                text = RemoveDiacritics(text);
                text = System.Text.RegularExpressions.Regex.Replace(text, @"[^A-Za-z0-9 -]+", " ");
            }
            // Remove double spaces
            text = System.Text.RegularExpressions.Regex.Replace(text, @"\s+", " "); //double spaces
            // Trim to length, if longer
            if (text.Length > length) {
                var trimLength = length;
                if(breakAtWordIfPossible) {
                    // Try to find a length to cutoff at a word
                    var tolerance = 12;
                    for(var i = 0; i <= tolerance; i++) {
                        var c1 = length - i;
                        var c2 = length + i;
                        if(c1 > 0 && c1 < text.Length && text[c1] == ' ') {
                            trimLength = c1;
                            break;
                        }
                        if(c2 > 0 && c2 < text.Length && text[c2] == ' ') {
                            trimLength = c2;
                            break;
                        }
                    }
                }
                // Cut strimg
                text = text.Substring(0, trimLength);
                // Add trail
                if (includeTrail) {
                    text = text.TrimEnd('.');
                    text += "...";
                }
            }
            return text.Trim();
        }

        /// <summary>
        /// Removes Unicode Categories Symbol&Punctuation characters, and changes ö to o
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string RemoveDiacritics(string text, bool removePunctiations = true) {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString) {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (removePunctiations && char.IsPunctuation(c)) {
                    stringBuilder.Append(' ');
                } else if (unicodeCategory != UnicodeCategory.NonSpacingMark && !char.IsSymbol(c)) {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public static string ReplaceUmlauts(string text) {
            text = text.Replace( "ä", "ae" );
            text = text.Replace( "ö", "oe" );
            text = text.Replace( "ü", "ue" );
            text = text.Replace( "Ä", "Ae" );
            text = text.Replace( "Ö", "Oe" );
            text = text.Replace( "Ü", "Ue" );
            text = text.Replace( "ß", "ss" );
            return text;
        }

        /// <summary>
        /// Replaces only first occurance of an a found patter with the replace pattern
        /// https://stackoverflow.com/questions/8809354/replace-first-occurrence-of-pattern-in-a-string
        /// </summary>
        /// <param name="text"></param>
        /// <param name="search"></param>
        /// <param name="replace"></param>
        /// <returns></returns>
        public static string ReplaceFirstOccurance(string text, string search, string replace) {
            int pos = text.IndexOf(search);
            if (pos < 0) {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }

        /// <summary>
        /// Removes the punctuations except for '_' and '-'.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static string RemovePunctuations(string text) {
            var stringBuilder = new StringBuilder();
            foreach (var c in text) {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (char.IsPunctuation(c) && c != '_' && c != '-') {
                    continue;
                } else {
                    stringBuilder.Append(c);
                }
            }
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Creates the a short-url for a string name by removing punctuations, trimming, and replacing spaces with a dash.
        /// Always returns in lowercase.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static string CreateShortURLForName(string name, bool makeLowercase = true, bool removeSpaces = true) {
            if (name == null) return null;
            name = Core.Util.String.ReplaceUmlauts(name); 
            name = Core.Util.String.RemoveDiacritics(name,false); // Replace special chars with space
            name = Core.Util.String.RemovePunctuations(name); // Remove punctuations except _ and -
            name = name.Trim();
            if(removeSpaces) name = name.Replace(" ", "-");
            name = Regex.Replace(name, "-+", "-"); // Removing repeating ---
            //name = name.Trim('-'); //Note: this causes issues with entity nodes starting ids with dash -12345
            if(makeLowercase) name = name.ToLower();
            return name;
        }

        public static string CreateShortURLForCamelCased(string name, bool makeLowercase = true, bool removeSpaces = true) {
            name = Regex.Replace(name, "(\\B[A-Z]+)", "-$1");
            name = CreateShortURLForName(name, makeLowercase, removeSpaces);
            return name;
        }


        public static string HexEncode(string asciiString) {
            string hex = "";
            foreach (char c in asciiString) {
                int tmp = c;
                hex += string.Format("{0:X2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
            }
            return hex;
        }

        public static string HexDecode(string HexValue) {
            string StrValue = "";
            while (HexValue.Length > 0) {
                StrValue += System.Convert.ToChar(System.Convert.ToUInt32(HexValue.Substring(0, 2), 16)).ToString();
                HexValue = HexValue.Substring(2, HexValue.Length - 2);
            }
            return StrValue;
        }

        // Function to detect the encoding for UTF-7, UTF-8/16/32 (bom, no bom, little
        // & big endian), and local default codepage, and potentially other codepages.
        // 'taster' = number of bytes to check of the file (to save processing). Higher
        // value is slower, but more reliable (especially UTF-8 with special characters
        // later on may appear to be ASCII initially). If taster = 0, then taster
        // becomes the length of the file (for maximum reliability). 'text' is simply
        // the string with the discovered encoding applied to the file.
        public static Encoding DetectTextEncoding(string text, int taster = 1000) {
            byte[] b = System.Text.Encoding.Default.GetBytes(text);

            //////////////// First check the low hanging fruit by checking if a
            //////////////// BOM/signature exists (sourced from http://www.unicode.org/faq/utf_bom.html#bom4)
            if (b.Length >= 4 && b[0] == 0x00 && b[1] == 0x00 && b[2] == 0xFE && b[3] == 0xFF) { text = Encoding.GetEncoding("utf-32BE").GetString(b, 4, b.Length - 4); return Encoding.GetEncoding("utf-32BE"); }  // UTF-32, big-endian 
            else if (b.Length >= 4 && b[0] == 0xFF && b[1] == 0xFE && b[2] == 0x00 && b[3] == 0x00) { text = Encoding.UTF32.GetString(b, 4, b.Length - 4); return Encoding.UTF32; }    // UTF-32, little-endian
            else if (b.Length >= 2 && b[0] == 0xFE && b[1] == 0xFF) { text = Encoding.BigEndianUnicode.GetString(b, 2, b.Length - 2); return Encoding.BigEndianUnicode; }     // UTF-16, big-endian
            else if (b.Length >= 2 && b[0] == 0xFF && b[1] == 0xFE) { text = Encoding.Unicode.GetString(b, 2, b.Length - 2); return Encoding.Unicode; }              // UTF-16, little-endian
            else if (b.Length >= 3 && b[0] == 0xEF && b[1] == 0xBB && b[2] == 0xBF) { text = Encoding.UTF8.GetString(b, 3, b.Length - 3); return Encoding.UTF8; } // UTF-8
            else if (b.Length >= 3 && b[0] == 0x2b && b[1] == 0x2f && b[2] == 0x76) { text = Encoding.UTF7.GetString(b,3,b.Length-3); return Encoding.UTF7; } // UTF-7
            

            //////////// If the code reaches here, no BOM/signature was found, so now
            //////////// we need to 'taste' the file to see if can manually discover
            //////////// the encoding. A high taster value is desired for UTF-8
            if (taster == 0 || taster > b.Length) taster = b.Length;    // Taster size can't be bigger than the filesize obviously.


            // Some text files are encoded in UTF8, but have no BOM/signature. Hence
            // the below manually checks for a UTF8 pattern. This code is based off
            // the top answer at: https://stackoverflow.com/questions/6555015/check-for-invalid-utf8
            // For our purposes, an unnecessarily strict (and terser/slower)
            // implementation is shown at: https://stackoverflow.com/questions/1031645/how-to-detect-utf-8-in-plain-c
            // For the below, false positives should be exceedingly rare (and would
            // be either slightly malformed UTF-8 (which would suit our purposes
            // anyway) or 8-bit extended ASCII/UTF-16/32 at a vanishingly long shot).
            int i = 0;
            bool utf8 = false;
            while (i < taster - 4)
            {
                if (b[i] <= 0x7F) { i += 1; continue; }     // If all characters are below 0x80, then it is valid UTF8, but UTF8 is not 'required' (and therefore the text is more desirable to be treated as the default codepage of the computer). Hence, there's no "utf8 = true;" code unlike the next three checks.
                if (b[i] >= 0xC2 && b[i] <= 0xDF && b[i + 1] >= 0x80 && b[i + 1] < 0xC0) { i += 2; utf8 = true; continue; }
                if (b[i] >= 0xE0 && b[i] <= 0xF0 && b[i + 1] >= 0x80 && b[i + 1] < 0xC0 && b[i + 2] >= 0x80 && b[i + 2] < 0xC0) { i += 3; utf8 = true; continue; }
                if (b[i] >= 0xF0 && b[i] <= 0xF4 && b[i + 1] >= 0x80 && b[i + 1] < 0xC0 && b[i + 2] >= 0x80 && b[i + 2] < 0xC0 && b[i + 3] >= 0x80 && b[i + 3] < 0xC0) { i += 4; utf8 = true; continue; }
                utf8 = false; break;
            }
            if (utf8 == true) {
                return Encoding.UTF8;
            }


            // The next check is a heuristic attempt to detect UTF-16 without a BOM.
            // We simply look for zeroes in odd or even byte places, and if a certain
            // threshold is reached, the code is 'probably' UF-16.          
            double threshold = 0.1; // proportion of chars step 2 which must be zeroed to be diagnosed as utf-16. 0.1 = 10%
            int count = 0;
            for (int n = 0; n < taster; n += 2) if (b[n] == 0) count++;
            if (((double)count) / taster > threshold) { text = Encoding.BigEndianUnicode.GetString(b); return Encoding.BigEndianUnicode; }
            count = 0;
            for (int n = 1; n < taster; n += 2) if (b[n] == 0) count++;
            if (((double)count) / taster > threshold) { text = Encoding.Unicode.GetString(b); return Encoding.Unicode; } // (little-endian)


            // Finally, a long shot - let's see if we can find "charset=xyz" or
            // "encoding=xyz" to identify the encoding:
            for (int n = 0; n < taster-9; n++)
            {
                if (
                    ((b[n + 0] == 'c' || b[n + 0] == 'C') && (b[n + 1] == 'h' || b[n + 1] == 'H') && (b[n + 2] == 'a' || b[n + 2] == 'A') && (b[n + 3] == 'r' || b[n + 3] == 'R') && (b[n + 4] == 's' || b[n + 4] == 'S') && (b[n + 5] == 'e' || b[n + 5] == 'E') && (b[n + 6] == 't' || b[n + 6] == 'T') && (b[n + 7] == '=')) ||
                    ((b[n + 0] == 'e' || b[n + 0] == 'E') && (b[n + 1] == 'n' || b[n + 1] == 'N') && (b[n + 2] == 'c' || b[n + 2] == 'C') && (b[n + 3] == 'o' || b[n + 3] == 'O') && (b[n + 4] == 'd' || b[n + 4] == 'D') && (b[n + 5] == 'i' || b[n + 5] == 'I') && (b[n + 6] == 'n' || b[n + 6] == 'N') && (b[n + 7] == 'g' || b[n + 7] == 'G') && (b[n + 8] == '='))
                    )
                {
                    if (b[n + 0] == 'c' || b[n + 0] == 'C') n += 8; else n += 9;
                    if (b[n] == '"' || b[n] == '\'') n++;
                    int oldn = n;
                    while (n < taster && (b[n] == '_' || b[n] == '-' || (b[n] >= '0' && b[n] <= '9') || (b[n] >= 'a' && b[n] <= 'z') || (b[n] >= 'A' && b[n] <= 'Z')))
                    { n++; }
                    byte[] nb = new byte[n-oldn];
                    Array.Copy(b, oldn, nb, 0, n-oldn);
                    try {
                        string internalEnc = Encoding.ASCII.GetString(nb);
                        text = Encoding.GetEncoding(internalEnc).GetString(b);
                        return Encoding.GetEncoding(internalEnc);
                    }
                    catch { break; }    // If C# doesn't recognize the name of the encoding, break.
                }
            }


            // If all else fails, the encoding is probably (though certainly not
            // definitely) the user's local codepage! One might present to the user a
            // list of alternative encodings as shown here: https://stackoverflow.com/questions/8509339/what-is-the-most-common-encoding-of-each-language
            // A full list can be found using Encoding.GetEncodings();
            return Encoding.Default;
        }

        /// <summary>
        /// Returns the latlon coordinate from a location string.
        /// Thus when ```8003 Zürich (47.3744489,8.5410422)``` is input,
        /// this method will return ```47.3744489,8.5410422```
        /// 
        /// A location string looks like:
        /// -```47.3744489,8.5410422```
        /// -```8003 Zürich (47.3744489,8.5410422)```
        /// -```8003 Zürich```
        /// 
        /// See also Machinata.String.readLatLonFromLocationString (Core JS)
        /// </summary>
        public static string ReadLatLonFromLocationString(string location) {
            // Sanity
            if (location == null || location == "") return null;
            // Parse out coord pair
            var latlons = Regex.Matches(location, @"(\d*\.\d*,\d*\.\d*)");

            if (latlons != null && latlons.Count > 0) {
                return latlons[latlons.Count-1].Groups[0].Value; // make sure to get last match
            } else {
                return null;
            }
        }

        /// <summary>
        /// Reads the address portion from the a coordinate string.
        /// Thus when ```8003 Zürich (47.3744489,8.5410422)``` is input,
        /// this method will return ```8003 Zürich```
        ////
        /// A location string looks like:
        /// -```47.3744489,8.5410422```
        /// -```8003 Zürich (47.3744489,8.5410422)```
        /// -```8003 Zürich```
        /// 
        /// See also Machinata.String.readAddressFromLocationString (Core JS)
        /// </summary>
        public static string ReadAddressFromLocationString(string location) {
            // Sanity
            if (location == null || location == "") return null;
            // Get latlon
            var latlon = ReadLatLonFromLocationString(location);
            if (latlon == null) {
                return location;
            } else {
                // Replace it
                var ret = location.Replace("(" + latlon + ")", ""); // try with () first
                ret = ret.Replace(latlon, ""); // try without
                ret = ret.Trim();
                // Nothing left?
                if (ret == "") return location.Trim();
                else return ret;
            }
        }
    }
}
