using Machinata.Core.Exceptions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace Machinata.Core.Util {

    public class Number {


        /// <summary>
        /// Formats a decimal to a string without decimals and with the Config.CurrencyThousandsSeparatorChar 
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public static string FormatWithThousandsNoDecimal(decimal amount) {
            var res = amount.ToString("#,##0");
            if (Core.Config.CurrencyThousandsSeparatorChar != null) {
                res = res.Replace(",", Core.Config.CurrencyThousandsSeparatorChar);
            }
            return res;
        }

    }
}
