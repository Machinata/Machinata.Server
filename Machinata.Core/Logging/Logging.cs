using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NLog;

namespace Machinata.Core {

    /// <summary>
    /// Utility class for setting up different log targets using the server configuration.
    /// </summary>
    public class Logging {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        public const string LOG_SEPARATOR = "=================================================================";
        
        private static Dictionary<string,NLog.Targets.Target> _loadedTargets = new Dictionary<string, NLog.Targets.Target>();
        private static Dictionary<string,NLog.Config.LoggingRule> _loadedRules = new Dictionary<string, NLog.Config.LoggingRule>();

        private static List<string> _startupLogs = null;
        
        public static NLog.Targets.LimitedMemoryTarget MemoryTarget {
            get {
                if (_loadedTargets.ContainsKey("memory")) return _loadedTargets["memory"] as NLog.Targets.LimitedMemoryTarget;
                else return null;
            }
        }

        public static NLog.Targets.LimitedMemoryTarget StartupTarget {
            get {
                if (_loadedTargets.ContainsKey("startup")) return _loadedTargets["startup"] as NLog.Targets.LimitedMemoryTarget;
                else return null;
            }
        }

        public static void CaptureStartupLogs() {
            _loadLoggerForConfiguration("startup","*","Info",int.MaxValue, null);
        }

        public static List<string> GetStartupLogs() {
            return _startupLogs;
        }

       

        public static void RegisterStartupLogs() {
            if (StartupTarget == null) {
                _startupLogs = new List<string>();
                return;
            }
            // Get logs
            _startupLogs = StartupTarget.Logs.ToList(); // copy
            // Shut it down
            LogManager.Configuration.RemoveTarget("startup");
            LogManager.ReconfigExistingLoggers();
        }
    
        public static void OnApplicationStartup() {

            
            // Find all config keys that match environment and load the logger
            foreach (var key in Core.Config.FindForEnvironement("LogTarget-")) {
                _loadLoggerForConfiguration(key);
            }
        }

        private static void _loadLoggerForConfiguration(string configKey) {
            var logConfig = Core.Config.GetKeyValueSetting(configKey);
            int maxLines = 0;
            if(logConfig.ContainsKey("max-lines")) {
                maxLines = int.Parse(logConfig["max-lines"]);
            }
            string name = null;
            if (logConfig.ContainsKey("name")) {
                name = logConfig["name"];
            }

            _loadLoggerForConfiguration(logConfig["target"],logConfig["pattern"],logConfig["min-level"],maxLines , logConfig, name);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="target">file or memory or startup</param>
        /// <param name="pattern"></param>
        /// <param name="logLevel"></param>
        /// <param name="maxLines"></param>
        /// <param name="logConfig"></param>
        /// <param name="targetName">name of the logger to be called with GetLogger("Name of logger")</param>
        private static void _loadLoggerForConfiguration(string target, string pattern, string logLevel, int maxLines, Dictionary<string,string> logConfig, string targetName = null) {
            // Init
            _logger.Info($"Loading logger '{target}' with name '{targetName}' with pattern '{pattern}' and log level {logLevel}...");
            NLog.Targets.Target logTarget = null;

            var targetKey = target;
            var ruleKey = targetKey + "_" + pattern + "_" + logLevel;

            // Rule key
            if (string.IsNullOrEmpty(targetName) == false) {
                ruleKey = ruleKey +"_" + targetName;
            }

            // Target Key  (target + the name of the target, we can have several)
            if (string.IsNullOrEmpty(targetName) == false) {
                targetKey = targetKey + "_" + targetName;
            }


            // Rule already exist?
            if (_loadedRules.ContainsKey(ruleKey)) {
                return;
            }

            _logger.Info($"target key '{targetKey}'");
            // Do we need a config?
            if (LogManager.Configuration == null) LogManager.Configuration = new NLog.Config.LoggingConfiguration();
            // Does the target exist?
            if (!_loadedTargets.ContainsKey(targetKey)) {
                // Create new target
                if(target == "memory" || target == "startup") {
                    // Memory
                    _logger.Info($"  Target is memory");
                    var memoryTarget = new NLog.Targets.LimitedMemoryTarget();
                    if(maxLines > 0) memoryTarget.Limit = maxLines;
                    LogManager.Configuration.AddTarget(targetKey, memoryTarget);
                    logTarget = memoryTarget;
                } else if (target == "console") {
                    // Console
                    _logger.Info ($"  Target is console");
                    var consoleTarget = new NLog.Targets.ColoredConsoleTarget ();
                    LogManager.Configuration.AddTarget (targetKey, consoleTarget);
                    logTarget = consoleTarget;
                } else {
                    // File
                    _logger.Info($"  Target is file");
                    var fileName = targetKey; // fallback

                    // Filename from target name 
                    if (string.IsNullOrEmpty(targetName) == false) {
                        fileName = targetName;
                    }

                    // File name explicitly specified?
                    {
                        const string fileNameKey = "file-name";
                        if (logConfig.ContainsKey(fileNameKey) && string.IsNullOrWhiteSpace(logConfig[fileNameKey]) == false) {
                            fileName = logConfig[fileNameKey];
                        }
                    }


                    fileName = fileName.Replace("{yyyy}", DateTime.UtcNow.ToString("yyyy"));
                    fileName = fileName.Replace("{mm}", DateTime.UtcNow.ToString("MM"));
                    fileName = fileName.Replace("{dd}", DateTime.UtcNow.ToString("dd"));
                    fileName = fileName.Replace("{project-name}", Core.Config.ProjectID);
                    fileName = fileName.Replace("{server-url}", Core.Config.ServerURL?.Replace("https://", "").Replace("http://", "").Replace("/", ""));
                    fileName = fileName.Replace("{environment}", Core.Config.Environment);
                    fileName = fileName.Replace("{machine-id}", Core.Config.MachineID);
                    fileName = fileName.Replace("{log-name}", targetName);

                    _logger.Info($"  Filename: " + fileName);


                    var fileTarget = new NLog.Targets.FileTarget();
                    // Max files
                    {
                        int maxFiles = 10;
                      
                        int.TryParse(logConfig["max-files"], out maxFiles);
                        _logger.Info($"  MaxArchiveFiles: "+ maxFiles);
                        fileTarget.MaxArchiveFiles = maxFiles;
                    }

                    // Max file size
                    {
                        int maxFileSize = 5000000;
                        int.TryParse(logConfig["max-file-size"], out maxFileSize);
                        _logger.Info($"  ArchiveAboveSize: " + maxFileSize);
                        fileTarget.ArchiveAboveSize = maxFileSize;
                    }
                                      

                   
                    // Archive numbering
                    fileTarget.ArchiveNumbering = NLog.Targets.ArchiveNumberingMode.DateAndSequence;

                    // Set filename on target
                    fileTarget.FileName = Core.Config.LogPath + Core.Config.PathSep + fileName;

                    // Add target to LogManager
                    LogManager.Configuration.AddTarget(targetKey, fileTarget);

                    logTarget = fileTarget;
                }

               
                // Register the target
                _loadedTargets[targetKey] = logTarget;

            }
            // The target exists, now create the log rule

            NLog.Config.LoggingRule logRule = null;

            if (targetName == null) {
                // the name is the pattern
                logRule = new NLog.Config.LoggingRule(pattern, LogLevel.FromString(logLevel), logTarget);

            } else {
                // the name is the filter
                logRule = new NLog.Config.LoggingRule(targetName, LogLevel.FromString(logLevel), logTarget);
            }

           

            LogManager.Configuration.LoggingRules.Add(logRule);


            _loadedRules[ruleKey] = logRule;
            // Activate the configuration
            LogManager.ReconfigExistingLoggers();
        }

    }
}
