using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Chart colors
/// </summary>
namespace Machinata.Core.Charts {
    public class ChartColors {

        public static string[] D3_CATEGORY_10 = new string[] {
                "#1f77b4",
                "#ff7f0e",
                "#2ca02c",
                "#d62728",
                "#9467bd",
                "#8c564b",
                "#e377c2",
                "#7f7f7f",
                "#bcbd22",
                "#17becf"
            };

    }
}
