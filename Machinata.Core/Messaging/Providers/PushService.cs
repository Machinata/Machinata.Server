using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;
using WebPush;

namespace Machinata.Core.Messaging.Providers {
    public class PushService {

        public class VAPIDKeys {
            public string PublicKey { get; set; }
            public string PrivateKey { get; set; }


            /// <summary>
            /// Generates a new pair of VAPID keys
            /// </summary>
            /// <returns></returns>
            public static VAPIDKeys GenerateKeys() {
                VapidDetails vapidKeys = VapidHelper.GenerateVapidKeys();
                return new VAPIDKeys() {
                    PublicKey = vapidKeys.PublicKey,
                    PrivateKey = vapidKeys.PrivateKey
                };
            }
        }

        public class PushSubscriptionParams {
            public string Endpoint { get; set; }
            public string P256dh { get; set; }
            public string Auth { get; set; }

            public PushSubscriptionParams() {
             
            }

            public PushSubscriptionParams(string endpoint, string p256dh, string auth) {
                Endpoint = endpoint;
                P256dh = p256dh;
                Auth = auth;
            }

            public static PushSubscriptionParams LoadTestValues() {
             
                var pushEndpoint = @"https://fcm.googleapis.com/fcm/send/ebvL7G316NA:APA91bHbQgeAAu7yRy5rTDqjBw7JXSfni59f0UhfR9QRnLacYF-Ft3ZbzElYBrJswSeZgE1npd86KvzqnpYLZS-QEGxMD1qtSpuJDmW73joqi175mXTbDc4TCCPxoCReWQMhSX7lAf_V";
                var p256dh = @"BB6NcvBUSEhMYuiCiFWB1un1X-4I2-PZdN2ueY_LEiznINpiU-rvvD1J7Tszo3YfCiTe0Fxo4ku9bsJKoVilJWI";
                var auth = @"Cx1zcMrfjcE4hxiJIpgt9Q";

                // set the test values
                var pushSubscriptionParams = new PushSubscriptionParams(pushEndpoint, p256dh, auth);
                return pushSubscriptionParams;
            }

           
        }

        public static void SendWebPushMessage(PushSubscriptionParams pushSubscriptionparams, string payload) {

            // https://developer.mozilla.org/en-US/docs/Web/API/Push_API
            // https://github.com/web-push-libs/web-push-csharp

            // Generate keys
            // https://web-push-codelab.glitch.me/

            //var pushEndpoint = @"https://fcm.googleapis.com/fcm/send/ebvL7G316NA:APA91bHbQgeAAu7yRy5rTDqjBw7JXSfni59f0UhfR9QRnLacYF-Ft3ZbzElYBrJswSeZgE1npd86KvzqnpYLZS-QEGxMD1qtSpuJDmW73joqi175mXTbDc4TCCPxoCReWQMhSX7lAf_V";
            //var p256dh = @"BB6NcvBUSEhMYuiCiFWB1un1X-4I2-PZdN2ueY_LEiznINpiU-rvvD1J7Tszo3YfCiTe0Fxo4ku9bsJKoVilJWI";
            //var auth = @"Cx1zcMrfjcE4hxiJIpgt9Q";


            //var publicKey = @"BI8-xz0UaDb9NATQikpLPHbBaXeRh0Lv1SGt6KQ80nvd5fV_JF0DK7OpivAgNlnQsfHOMnWWETsBjUEJ4--iFos";
            //var privateKey = @"6SvbtKbv2kiSm16mAXiauOnCglDnNhKyYT3x3Q-8h10";


            /*Note that we also included a "mailto:" string. This string needs to be either a URL or a mailto email address. 
             * This piece of information will actually be sent to the web push service as part of the request to trigger a push.
             * The reason this is done is so that if a web push service needs to get in touch with the sender, they have some information that will enable them to.*/

            //var subject = @"mailto:example@example.com";
            var subject = @"mailto:" + Core.Config.WebPushVAPIDSubject;

            // Keys generated once
            var publicKey = Core.Config.WebPushVAPIDPublicKey;
            var privateKey = Core.Config.WebPushVAPIDPrivateKey;

            var subscription = new PushSubscription(pushSubscriptionparams.Endpoint, pushSubscriptionparams.P256dh, pushSubscriptionparams.Auth);
            var vapidDetails = new VapidDetails(subject, publicKey, privateKey);

            var webPushClient = new WebPushClient();
          
            webPushClient.SendNotification(subscription, payload, vapidDetails);
           

        }

    }
}
