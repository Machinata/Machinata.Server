using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Messaging.Providers {
    public class SMSService {

        // Default implemation with Twilio

        public static void SendSMS(string toPhoneNumber, string message) {
            string accountSid = "Your_Twilio_Account_Sid";
            string authToken = "Your_Auth_Token";
            string fromPhoneNumber = "Your_Twilio_Phone_Number";
            

            string twilioSmsUrl = $"https://api.twilio.com/2010-04-01/Accounts/{accountSid}/Messages.json";

            using (HttpClient client = new HttpClient()) {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                    "Basic",
                    Convert.ToBase64String(Encoding.ASCII.GetBytes($"{accountSid}:{authToken}")));

                var content = new StringContent(
                    $"Body={Uri.EscapeDataString(message)}&From={Uri.EscapeDataString(fromPhoneNumber)}&To={Uri.EscapeDataString(toPhoneNumber)}",
                    Encoding.UTF8,
                    "application/x-www-form-urlencoded");

                HttpResponseMessage response = client.PostAsync(twilioSmsUrl, content).Result;
                var responseString = response.Content.ReadAsStringAsync().Result;
            }
        }
    }
    
}
