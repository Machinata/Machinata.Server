using DocumentFormat.OpenXml.Bibliography;
using Google.Authenticator;
using Machinata.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// 
/// </summary>
namespace Machinata.Core.Encryption {

    /// <summary>
    /// The default encryption provider for Machinata.
    /// </summary>
    public class DefaultHasher {

        /// <summary>
        /// Hashes the input string using the default provider.
        /// Per default, the default provier is SHA256.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public static string HashString(string input) {
            // Note: this code is protected and should not change once in master!
            return Core.Encryption.Providers.SHA256Hasher.HashString(input);
        }

        /// <summary>
        /// Salts the and hashes the input string using the default hash provider and the
        /// system global salts.
        /// This method can be used to generate salted hash codes for a unque input, such as, for example,
        /// verifying the authenticity of a unsubscribe link.
        /// Note: this method should not be used for hashing passwords. Hashing passwords should use a custom method.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public static string SaltAndHashString(string input) {
            // Note: this code is protected and should not change once in master!
            // Make sure we have a string, otherwise it could reveal the salt and hash backwards.
            if (string.IsNullOrEmpty(input)) throw new Exception("String to SaltAndHashString cannot be null or empty.");
            var str = Core.Config.EncryptionHashSalt2 + input + Core.Config.EncryptionHashSalt1 + HashString(Core.Config.EncryptionHashSalt2 + Core.Config.EncryptionHashSalt1);
            return HashString(str);
        }


        public static string ComputeEmailOrUsernameHash(string email) {
            var globalSalt1 = Core.Config.EncryptionHashSalt1;
            var globalSalt2 = Core.Config.EncryptionHashSalt2;
            var stringToHash = globalSalt1 + email + globalSalt2;
            return Core.Encryption.DefaultHasher.HashString(stringToHash);
        }
    }

    public class DefaultRandomNumberGenerator {

        public static string GeneratePassword(int length) {
            return System.Web.Security.Membership.GeneratePassword(length, length / 3);
        }

        public static string GeneratePasswordXMLSafe(int length) {
            var pwd = GeneratePassword(length);
            while (pwd.Contains("<") || pwd.Contains(">") || pwd.Contains("&") || pwd.Contains("\"")) pwd = GeneratePassword(length);
            return pwd;
        }

    }

    /// <summary>
    /// The default encryption provider for Machinata.
    /// </summary>
    public class DefaultEncryption {

        /// <summary>
        /// Encrypts the string using AES then HMAC. The encryption keys are stored in the config.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public static string EncryptString(string input) {
            return Core.Encryption.Providers.AESThenHMAC.SimpleEncrypt(input, Core.Config.EncryptionCryptKeyBytes, Core.Config.EncryptionAuthKeyBytes);
        }

        /// <summary>
        /// Decrypts the string using AES then HMAC. The encryption keys are stored in the config.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public static string DecryptString(string input) {
            if (input == null) return null;
            return Core.Encryption.Providers.AESThenHMAC.SimpleDecrypt(input, Core.Config.EncryptionCryptKeyBytes, Core.Config.EncryptionAuthKeyBytes);
        }
    }

    /// <summary>
    /// </summary>
    public class AccessCodeGenerator {

        /// <summary>
        /// </summary>
        public static string GetAccessCodeForString(string input) {
            var output = Core.Encryption.DefaultHasher.SaltAndHashString(input);
            return output;
        }

        /// <summary>
        /// </summary>
        public static bool ValidateAccessCodeForString(string input, string accessCode, bool throwException = false) {
            bool isValid = true;
            if (string.IsNullOrEmpty(input)) isValid = false;
            try {
                var expected = GetAccessCodeForString(input);
                if (expected != accessCode) isValid = false;
            } catch (Exception e) {
                return false;
            }
            return isValid;
        }

    }

    /// <summary>
    /// </summary>
    public class FrontendToBackendRequestValidator {

        /// <summary>
        /// </summary>
        public static void ValidateCheckValues(string expectedValue, string givenValue, string givenTimestamp, string givenHash) {
            // Init
            var machinatachecka = givenValue;
            var machinatacheckb = givenTimestamp;
            var machinatacheckc = givenHash;
            var timestampMS = Core.Util.Time.GetDateTimeFromUTCMilliseconds(machinatacheckb);
            var utcNow = DateTime.UtcNow;
            var minutesValid = 15;
            // Calculate hash
            var prehash = Core.Config.ValidateFrontendToBackendRequestsHashSalt2 + machinatachecka + machinatacheckb + Core.Config.ValidateFrontendToBackendRequestsHashSalt1 + Providers.SHA256Hasher.HashStringUTF8(Core.Config.ValidateFrontendToBackendRequestsHashSalt2 + Core.Config.ValidateFrontendToBackendRequestsHashSalt1);
            var hash = Providers.SHA256Hasher.HashStringUTF8(prehash);
            // Validate hash
            if (hash != machinatacheckc) {
                throw new BackendException("spam-detection", "Spammer detetcted");
            }
            // Validate timestampMS (for replay attacks)
            if (timestampMS < utcNow.AddMinutes(-minutesValid)) {
                throw new BackendException("spam-detection", "Spammer detetcted");
            }
            // Validate the given value
            if (expectedValue == null) throw new Exception("ValidateCheckValues: expectedValue was null");
            if (givenValue == null) throw new Exception("ValidateCheckValues: givenValue was null");
            if (givenValue != expectedValue) {
                throw new BackendException("spam-detection", "Spammer detetcted");
            }
        }


    }

    /// <summary>
    /// </summary>
    public class TwoFactorCodeGenerator {

        public static string GetIssuer() {
            return new Uri(Core.Config.PublicURL).Host;
        }

        public static bool ValidateAuthenticationPIN(string accountSecretKey, string pin) {
            var twoFactorAuthenticator = new TwoFactorAuthenticator();

            //var timeTolerance = new TimeSpan(0, 1, 0); // one minute
            return twoFactorAuthenticator.ValidateTwoFactorPIN(accountSecretKey, pin); // default time tolerance in this library is 5min
        }

        public static string GenerateAuthenticationQRCodeLink(string issuer, string accountSecretKey, string username) {
            var code = GenerateAuthenticationSetupCode(issuer, accountSecretKey, username);
            var link = $"otpauth://totp/{issuer}:{username}?secret={code}&issuer={issuer}";
            return link;
        }

        public static string GenerateAuthenticationSetupCode(string issuer, string accountSecretKey, string username) {
            var twoFactorAuthenticator = new TwoFactorAuthenticator();

            var setupInfo = twoFactorAuthenticator.GenerateSetupCode(
                // name of the application - the name shown in the Authenticator
                issuer,
                // an account identifier - shouldn't have spaces
                username,
                // the secret key that also is used to validate later
                accountSecretKey,
                // Base32 Encoding (odd this was left in)
                false,
                // resolution for the QR Code - larger number means bigger image
                10);

            return setupInfo.ManualEntryKey;
        }

    }
}