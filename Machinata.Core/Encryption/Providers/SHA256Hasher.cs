using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Encryption.Providers {
    public class SHA256Hasher {

        public static string HashString(string input) {
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] hashValue;
            byte[] message = UE.GetBytes(input);
            SHA256Managed hashString = new SHA256Managed();
            string hex = "";
            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue) {
                hex += String.Format("{0:x2}", x);
            }
            return hex;
        }

        public static string HashStringUTF8(string input) {
            var encoding = new UTF8Encoding();
            byte[] hashValue;
            byte[] message = encoding.GetBytes(input);
            SHA256Managed hashString = new SHA256Managed();
            string hex = "";
            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue) {
                hex += String.Format("{0:x2}", x);
            }
            return hex;
        }

    }
}
