using Machinata.Core.Model;
using Machinata.Core.Util;
using System.IO;
using System;
using System.Web;
using Machinata.Core.Exceptions;
using System.Linq;

namespace Machinata.Core.Data {
    public class DataCenter {

        public enum StorageProviderType {
            S3,
            FileSystem
        }


        /// <summary>
        /// Uploads a file to the datastore
        /// Hint: has to be saved and the contentFiled added to the context to work properly
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="data"></param>
        /// <param name="source"></param>
        /// <param name="providerName"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static ContentFile UploadFile(string filename, byte[] data, string source, string providerName, User user) {
            return UploadFile(filename, new MemoryStream(data), user, source, providerName);
        }

        public static ContentFile UploadFile(HttpPostedFile file, User user, string source, string providerName = null) {
            return UploadFile(file.FileName, file.InputStream, user, source, providerName);
        }

        /// <summary>
        /// Uploads file to provider (s3|file)
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="dataStream">The data stream.</param>
        /// <param name="user">The user.</param>
        /// <param name="source">The source. Gets or sets the source where the file came from.
        /// This is important, because it allows for tracking of files to 
        /// different modules or classes which keeps the store more organized.</param>
        /// <param name="providerName">name of the provider (if empty, default setting from Web.config)</param>
        /// <returns></returns>
        public static ContentFile UploadFile(string filename, Stream dataStream, User user, string source, string providerName = null) {
            var extension = Files.GetFileExtensionWithoutDot(filename);
            var name = Files.GetFileNameWithoutExtension(filename);

            // Set default source
            if (source == null) source = "Unknown";

            // Clean the name
            name = Core.Util.String.ReplaceUmlauts(name);
            name = Core.Util.String.RemoveDiacritics(name, true);
            name = name.Trim();
            name = name.Replace(" ", "-");

            // Normalize extension
            extension = extension.ToLower();
            if (extension == "jpeg") extension = "jpg";
            
            if (string.IsNullOrEmpty(providerName)) { providerName = Core.Config.FileStorageProviderName; }
            var contentFile = new ContentFile();
            contentFile.FileName = name;
            contentFile.FileExtension = extension;
            contentFile.FileCategory = GetCategoryFromFilename(filename);
            contentFile.User = user;
            contentFile.Source = source;
            
            var storageProvider = GetStorageProvider(providerName);
            contentFile.StorageProvider = storageProvider.GetProviderName();
            
            var key = storageProvider.PutFile(filename,extension, dataStream);
            contentFile.StorageKey = key;

            return contentFile;
        }

        public static ContentFile.ContentCategory GetCategoryFromFilename(string fileName) {
            var mimMapping = Files.GetMimeType(fileName);
            if (mimMapping.StartsWith("image")) {
                return ContentFile.ContentCategory.Image;
            } else if (mimMapping.StartsWith("video")) {
                return ContentFile.ContentCategory.Video;
            } else if (mimMapping.StartsWith("audio")) {
                return ContentFile.ContentCategory.Audio;
            } else if (mimMapping.StartsWith("text")) {
                return ContentFile.ContentCategory.Text;
            }

            return ContentFile.ContentCategory.Other;
        }

        /// <summary>
        /// Gets the path of the file from its storage provider
        /// Not really a download in FileSystemStorageProvider
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static string DownloadFile(ModelContext db, string contentPath) {
            // Extract public id (from /content/{category}/{publicId}/{fullFilename})
            ContentFile contentFile = GetContentFileForPath(db, contentPath);
            // Pass on
            return DownloadFile(contentFile);
        }

        public static ContentFile GetContentFileForPath(ModelContext db, string contentPath) {
            var pathSegs = contentPath.Trim('/').Split('/');
            // Get content file
            var contentFile = db.ContentFiles().GetByPublicId(pathSegs[2]);
            if (contentFile == null) throw new Backend404Exception("file-not-found", "File not found");
            return contentFile;
        }

        //public static ContentFile UploadFile(User user, byte[] content, string filename) {


        //    ContentFile contentFile = null;
        //    using (ModelContext db = ModelContext.GetModelContext(null)) {
        //        var uploadingUsing = db.Users().FirstOrDefault(u => u.Id == user.Id);
        //        contentFile = Core.Data.DataCenter.UploadFile(filename, bytes, nameof(ContentNode), Core.Config.FileStorageProviderName, uploadingUsing);
        //        db.ContentFiles().Add(contentFile);
        //        db.SaveChanges();
        //    }

        //    return contentFile;
        //}



        public static string DownloadFile( string contentPath) {
            // Extract public id (from /content/{category}/{publicId}/{fullFilename})
            using (var db = Core.Model.ModelContext.GetModelContext(null)) {
                var result = DownloadFile(db, contentPath);
                return result;
            }
        }

        /// <summary>
        /// Gets the path of the file from its storage provider
        /// Not really a download in FileSystemStorageProvider
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static string DownloadFile(ContentFile entity) {
            var storageProvider = GetStorageProvider(entity.StorageProvider);
            var path = storageProvider.GetFile(entity.StorageKey,entity.FileExtension);
            return path;
        }

        public static byte[] DownloadFileContent(ContentFile entity) {
            var path = DownloadFile(entity);
            return File.ReadAllBytes(path);
        }

        public static bool DeleteFile(ModelContext db, ContentFile contentFile) {
            var storageProvider = GetStorageProvider(contentFile.StorageProvider);
            var result = storageProvider.DeleteFile(contentFile.StorageKey,contentFile.FileExtension);
            db.ContentFiles().Remove(contentFile);
            return result;
        }

        public static void DeleteFileByContentURL(ModelContext db, string contentURL, string source) {
            if (string.IsNullOrEmpty(contentURL) == true) {
                throw new Exception("DeleteFileByContentURL: no contentURL provided");
            }

            var splits = contentURL.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            if (splits.Count() != 4) {
                throw new Exception("DeleteFileByContentURL: wrong contentURL format provided: " + contentURL);
            }

            var id = splits.Skip(2).First();

            var contentFile = db.ContentFiles().Where(c => c.Source ==source).GetByPublicId(id);
            if (contentFile != null && contentFile.ContentURL == contentURL && contentFile.Source == source) {

                // Double check contenURL
                if (contentFile.ContentURL != contentURL) {
                    throw new Exception("DeleteFileByContentURL: could not find correct content file for: " + contentURL);
                }


                DataCenter.DeleteFile(db, contentFile);
            } 
            
            
        }




        public static IStorageProvider GetStorageProvider(string name="default") {
            if (name == "s3") {
                return new S3StorageProvider();
            }
            else if (name == "file") {
                return new FileSystemStorageProvider();
            }
            else if (name == "default") {
                return new FileSystemStorageProvider();
            }
            throw new NotImplementedException($"No storage provider with name {name} found");
        }
    }
}