using System.IO;

namespace Machinata.Core.Data {

    public interface IStorageProvider {

        string GetProviderName();
        string GetSchema();
        string GetFile(string key,string extension);
        string PutFile(string filename, string extension, Stream dataStream);
        bool DeleteFile(string key, string extension);

       
    }
}