using Machinata.Core.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.StructuredData {

    public class PostalAddressStructuredData : StructuredData {

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string StreetAddress { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AddressLocality { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string PostalCode { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AddressRegion { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AddressCountry { get; set; }


        public override string Type
        {
            get
            {
                return "PostalAddress";
            }
        }

        public static PostalAddressStructuredData Load(Address addressModelObject) {
            var address = new PostalAddressStructuredData();
            address.AddressCountry = addressModelObject.CountryCode?.ToUpperInvariant();
            address.PostalCode = addressModelObject.ZIP;
            address.AddressLocality = addressModelObject.City;
            address.AddressRegion = addressModelObject.StateCode?.ToUpperInvariant();
            address.StreetAddress = addressModelObject.Address1;
            address.Name = addressModelObject.Company;
            return address;
        }

      
    }

  
}
