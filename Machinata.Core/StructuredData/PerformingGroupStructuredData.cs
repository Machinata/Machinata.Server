using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.StructuredData {
    public class PerformingGroupStructuredData : OrganizationStructuredData {

      
        public override string Type
        {
            get
            {
                return "PerformingGroup";
            }
        }

       
    }

  
}
