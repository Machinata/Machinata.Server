using Machinata.Core.Templates;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.StructuredData {

    public class EventStructuredData : StructuredData {

       
        public PlaceStructuredData Location { get; set; }
        public OfferStructuredData Offers { get; set; }
        public OrganizationStructuredData Performer { get; set; }
        public OrganizationStructuredData Organizer { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }


        /// <summary>
        /// Actually this should be in Thing>Intangible>Enumerations
        /// </summary>
        public enum EventAttendanceModes {
            MixedEventAttendanceMode,
            OfflineEventAttendanceMode,
            OnlineEventAttendanceMode
        }


        public enum EventStatusTypes {
            EventCancelled,
            EventMovedOnline,
            EventPostponed,
            EventRescheduled,
            EventScheduled,
        }

        public override string Type
        {
            get
            {
                return "Event";
            }
        }

       

        public string EventAttendanceMode {
            get {
                return  NS_PREFIX + _eventAttendanceMode;
            }
        }

        public string EventStatus {
            get {
                return NS_PREFIX + _eventStatusTypes;
            }
        }


        public void SetAttendanceMode(EventAttendanceModes mode) {
            _eventAttendanceMode = mode;
        }

        public void SetEventStatusType(EventStatusTypes type) {
            _eventStatusTypes = type;
        }

        private EventAttendanceModes _eventAttendanceMode;
        private EventStatusTypes _eventStatusTypes;
    }


}
