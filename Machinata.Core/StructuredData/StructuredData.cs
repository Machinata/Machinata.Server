using DocumentFormat.OpenXml.Wordprocessing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// 
/// </summary>
namespace Machinata.Core.StructuredData {

    /// <summary>
    /// Basic class for Structured Data matches 'Thing' of Schema.org
    /// 'Inspired' by https://github.com/idenys/MXTires.Microdata/blob/master/Thing.cs
    /// 
    /// </summary>

    public abstract class StructuredData  {

        object context = "http://schema.org";
        public const string NS_PREFIX = "http://schema.org/";

        /// <summary>
        /// Context
        /// </summary>
        [JsonProperty("@context", Order = -100)]
        public virtual object Context { get { return context; } set { context = value; } }

        
        /// <summary>
        /// Type tag
        /// </summary>
        [JsonProperty("@type", Order = -99)]
        public abstract string Type { get; }

        /// <summary>
        /// ID
        /// </summary>
        [JsonProperty("@id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        
        /// <summary>
        /// Text 	A short description of the item.
        /// </summary>
        [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        /// <summary>
        /// URL  to an image of the item. This can be a URL or a fully described ImageObject.
        /// </summary>
        [JsonProperty("image", NullValueHandling = NullValueHandling.Ignore)]
        public string Image { get; set; }

        /// <summary>
        /// Text 	The name of the item.
        /// </summary>
        [JsonProperty("name", Order = -98, NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
        /// <summary>
        /// Action - Indicates a potential Action, which describes an idealized action in which this thing would play an 'object' role.
        /// </summary>
      

        /// <summary>
        ///	URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
        /// </summary>
        [JsonProperty("sameAs", NullValueHandling = NullValueHandling.Ignore)]
        public IList<string> SameAs { get; set; }

        /// <summary>
        /// URL of the item.
        /// </summary>
        [JsonProperty("url", NullValueHandling = NullValueHandling.Ignore)]
        public string Url { get; set; }


        /// <summary>
        /// Creates an JSON-LD (including script tag) of the current object
        /// </summary>
        /// <param name="indentFormatting"></param>
        /// <returns></returns>
        public string ToJsonLD(bool indentFormatting = true) {
            var data = Core.JSON.SerializeForStructuredData(this, indentFormatting);
            data = data.Replace("Context", "@context");
            if (data.Equals("Type")) data = data.Replace("Type", "@type");
            return "<script type=\"application/ld+json\">" + data + "</script>";
        }

        public override string ToString() {
            return ToJsonLD(true);
        }


        public static JObject ExtractFromHTML(string html,string type) {
            var htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(html);

            // Parse the HTML 
            var nodes = htmlDoc.DocumentNode.SelectNodes("//script[@type='application/ld+json']");
            if (nodes == null) return null;
            foreach ( var node in nodes ) {
                var jobject = JObject.Parse(node.InnerText);
                if (jobject["@type"]?.ToString() == type) {
                    return jobject;
                }
            }
            return null;
        }
    }

  
}
