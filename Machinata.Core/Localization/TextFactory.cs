using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinata.Core.Localization {

    public partial class Text {

        /// <summary>
        /// Defines the default language for inline language definitions or any text definition 
        /// where the language is not set.
        /// </summary>
        public const string DEFAULT_INLINE_LANGUAGE_ID = "en"; // This should not be changed

        [Core.Lifecycle.OnApplicationStartup]
        public static void OnApplicationStartup() {
            LoadAllLocalizationTextsFromFiles();
            // Note: DB texts was moved to OnApplicationStartup in Core.Model.LocalizationText
        }


        public static void LoadAllLocalizationTextsFromFiles() {
            // Discover all localization files
            // These are stored as txt files for each package
            string textExtension = ".txt";
            _logger.Trace("Searching in "+Core.Config.LocalizationPath+" for "+textExtension+"...");
            string[] localizationFiles = Core.Util.Files.GetFiles(
                Core.Config.LocalizationPath, 
                "*"+textExtension,
                SearchOption.AllDirectories);
            foreach(var templateFilePath in localizationFiles) {
                // Get the filename
                var templateFile = templateFilePath.Replace(Core.Config.LocalizationPath, "").TrimStart(Core.Config.PathSep[0]);
                templateFile = templateFile.Replace(Core.Config.PathSep, "/");
                string currentLine = null;
                try {
                    // Break it down into package and file
                    var templateFileSegs = templateFile.Split('/');
                    var templatePackage = templateFileSegs.First();
                    var templateFileName = string.Join("/", templateFileSegs.Skip(1).Take(templateFileSegs.Length - 1).ToArray());
                    var templateName = Core.Util.Files.GetPathWithoutExtension(templateFileName);
                    var templateExtension = Core.Util.Files.GetFileExtension(templateFileName);
                    _logger.Trace("  Discovered " + templateName + " (" + templateExtension + ") in " + templatePackage);
                    // Load the file
                    foreach (var line in System.IO.File.ReadAllLines(templateFilePath)) {
                        currentLine = line;
                        var parsed = ParseLocalizationFileLineForTextIdAndValue(line);
                        if (parsed == null) continue;
                        // Register
                        Text.RegisterText(templateFileName.Replace(".txt","").ToSentence().Capitalize() + " Localization File", templatePackage, parsed.Id, parsed.Value, parsed.Language, SourceTypes.LocalizationFile);
                    }
                }catch(Exception e) {
                    throw new Exception("Failed to load localization file "+templateFile +" at line \""+currentLine+"\": "+e.Message,e);
                }
                
            }
        }

        public class TextIdAndValue {
            public string Id;
            public string Value;
            public string Language;
        }


        public static TextIdAndValue ParseLocalizationFileLineForTextIdAndValue(string line) {
            if (line.Trim() == "" || line.StartsWith("#")) return null;
            // Find key
            int equalIndex = line.IndexOf('=');
            string key = line.Substring(0, equalIndex);
            string value = line.Substring(equalIndex + 1);
            // Extract language from key
            string language = null;
            string id = key;
            if (Regex.IsMatch(key, "(.*)\\.[a-zA-Z-][a-zA-Z-]")) {
                id = key.Substring(0, key.LastIndexOf('.'));
                language = key.Substring(key.LastIndexOf('.') + 1);
            }
            return new TextIdAndValue() { 
                Id = id,
                Language = language,
                Value = value,
            };
        }

        /// <summary>
        /// Loads all localization texts from database.
        /// </summary>
        /// <param name="db">The database.</param>
        public static void LoadAllLocalizationTextsFromDB(ModelContext db) {
            _logger.Info("Loading Localizations from DB...");
            var texts = db.LocalizationTexts();
            foreach (var text in texts) {
                _logger.Trace($"Registring text variable: {text.TextId}, {text.Language}, {text.Language}");
                Text.RegisterText(text.Source, text.Package, text.TextId, text.Value, text.Language, SourceTypes.Database);
            }
            _logger.Info($"Loaded {texts.Count()} text variables from db.");
        }


        private static int _textDiscoverySort = 0;

        /// <summary>
        /// Registers a text in the cache using package, id, value and language.
        /// </summary>
        /// <param name="package">The package.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="value">The value.</param>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        public static Text RegisterText(string source, string package, string id, string value, string language, SourceTypes sourceType) {
            // Init
            if (language == null) language = DEFAULT_INLINE_LANGUAGE_ID;
            // Validate
            if (language.Length > 3) {
                var msg = $"The language '{language}' is not valid (text id {id} in package {package} in {source}).";
                _logger.Error(msg);
                //throw new Exception(msg);
                return null;
            }
            // Construct text object
            Text text = new Text();
            text.Id = id;
            text.Package = package;
            text.Language = language.ToLower();
            text.Value = value;
            text.Source = source;
            text.SourceType = sourceType;
            text.Sort = _textDiscoverySort++;
            // Pass on
            return RegisterText(text);
        }

        /// <summary>
        /// Registers a text in the cache using a Text object. If a default is not set, then
        /// the default will be set using the id.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        private static Text RegisterText(Text text) {
            // Validate
            if (text.Language == null) throw new Exception("You cannot register a text without a language set.");
            // Register by id on path, but only if the langauge is set
            if (text.Language != null) {
              
                if (!_texts.ContainsKey(text.Id)) {
                    _texts[text.Id] = new Dictionary<string, List<Text>>();
                }

                if (!string.IsNullOrEmpty(text.Value)) {
                    if (!_texts[text.Id].ContainsKey(text.Language)) {
                        _texts[text.Id][text.Language] = new List<Text>();
                    }
                       _texts[text.Id][text.Language].Add(text);
                }
               
            }
            return text;
        }


        public static string GetTranslatedRoute(string id, string language) {
            if (string.IsNullOrEmpty(language)) {
                language = Core.Config.LocalizationDefaultLanguage;
            }
            var translated = GetTranslatedTextByIdForLanguage(id, language, null);
            if (translated == null) {
                return id;
            }
            return translated;
        }

        /// <summary>
        /// Gets the text by identifier for the language. If the language does not contain the text by id,
        /// then the default is returned (just the id). If that is not found, then the default value is
        /// returned (usually null).
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="language">The language.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static string GetTranslatedTextByIdForLanguage(string id, string language = "en", string defaultValue = null) {
            // Try to get full translation for language
            if (language == null) {
                throw new BackendException("no-language", "No language defined");
            }

            if (_texts.ContainsKey(id) && _texts[id].ContainsKey(language) && _texts[id][language].Any(t=>!string.IsNullOrEmpty(t.Value))) {
                return _InsertVariablesForTranslatedTextValue(_texts[id][language].Last(t => !string.IsNullOrEmpty(t.Value)).Value);
            }
            // Try to get a fallback translation using the default language
            if (Core.Config.LocalizationEnableFallback) {
                if (_texts.ContainsKey(id)) {
                    if (_texts[id].ContainsKey(Core.Config.LocalizationDefaultLanguage) && _texts[id][Core.Config.LocalizationDefaultLanguage].Any(t => !string.IsNullOrEmpty(t.Value))) {
                        return _InsertVariablesForTranslatedTextValue(_texts[id][Core.Config.LocalizationDefaultLanguage].Last(t => !string.IsNullOrEmpty(t.Value)).Value);
                    }
                    if (_texts[id].ContainsKey(DEFAULT_INLINE_LANGUAGE_ID) && _texts[id][DEFAULT_INLINE_LANGUAGE_ID].Any(t => !string.IsNullOrEmpty(t.Value))) {
                        return _InsertVariablesForTranslatedTextValue(_texts[id][DEFAULT_INLINE_LANGUAGE_ID].Last(t => !string.IsNullOrEmpty(t.Value)).Value);
                    }
                }
            }
            // Return the provided default
            return _InsertVariablesForTranslatedTextValue(defaultValue);
        }

        public static TranslatableText GetTranslatableTextForSupportedLanguages(string id) {
            var translatableText = new TranslatableText();
            foreach (var lang in Core.Config.LocalizationSupportedLanguages) {
                //var url = challenge.GetFrontendUrl(lang);
                var valueForLang = GetTranslatedTextByIdForLanguage(id,lang);
                if (lang == Core.Config.LocalizationDefaultLanguage) {
                    translatableText.SetDefault(valueForLang);
                } else {
                    translatableText.SetForLanguage(lang, valueForLang);
                }
            }
            return translatableText;
        }

        private static string _InsertVariablesForTranslatedTextValue(string text) {
            if (text == null) return text;
            text = text.Replace("{empty}", "");
            text = text.Replace("[[empty]]", "");
            text = text.Replace("{space}", " ");
            text = text.Replace("[[space]]", " ");
            text = text.Replace("{soft-hyphen}", "\u00AD");
            text = text.Replace("[[soft-hyphen]]", "\u00AD");
            text = text.Replace("{new-line}", "ญญญญญ\n");
            text = text.Replace("[[new-line]]", "\n");
            return text;
        }

        /// <summary>
        /// Gets the text by identifier in the configured localization default language
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static string GetTranslatedTextById(string id, string defaultValue = null) {
            return GetTranslatedTextByIdForLanguage(id, Core.Config.LocalizationDefaultLanguage, defaultValue);
        }


        /// <summary>
        /// Translates a text variable like {text.finance} (only this exact format)
        /// </summary>
        /// <param name="text"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string GetTranslatedTextByTextVariable(string text, string defaultValue = null) {

            text = Core.Util.StringExtensions.TrimStart(text, "{text.");
            text = Core.Util.StringExtensions.TrimEnd(text, "}");

            return GetTranslatedTextByIdForLanguage(text, Core.Config.LocalizationDefaultLanguage, defaultValue);
        }

        public static string GetTranslatedTextByIdDefaultLanguage(string id) {
            var lang = Core.Config.LocalizationDefaultLanguage;
            var  trans = GetTranslatedTextByIdForLanguage(id, lang, null);
            if (trans == null) {
                trans = "[TRANS:" + id + "." + lang + "]";
            }

            return trans;
        }

        public static string[] GetTranslatedTextByIdsDefaultLang(string[] ids) {
            var result = new List<string>();
            foreach (var id in ids) {
                result.Add(GetTranslatedTextByIdDefaultLanguage(id));
            }
            return result.ToArray();
        }


        /// <summary>
        /// Gets the texts by identifiers in the configured localization default language
        /// </summary>
        /// <param name="ids">The identifiers.</param>
        /// <param name="defaultValues">The default value.</param>
        /// <returns></returns>
        public static string[] GetTranslatedTextByIds(string[] ids, string[] defaultValues = null) {
            if (defaultValues != null && ids.Count() != defaultValues.Count()) {
                throw new Exception("Cannot have a different number in defaultValues than in ids");
            }
            var result = new List<string>();
            int i = 0;
            foreach (var id in ids) {

                var defaultValue = defaultValues != null ? defaultValues[i] : null;
                result.Add(GetTranslatedTextByIdForLanguage(id, Core.Config.LocalizationDefaultLanguage, defaultValue));
                i++;
            }

            return result.ToArray();
        }

        /// <summary>
        /// Unregisters a text from cache
        /// </summary>
        /// <param name="textId">The text identifier.</param>
        /// <param name="language">The language.</param>
        /// <param name="type">The type.</param>
        public static void UnRegisterText(string textId, string language, SourceTypes type) {
            if (_texts.ContainsKey(textId) && _texts[textId].ContainsKey(language)) {
                _texts[textId][language].RemoveAll(tl => tl.SourceType == type);
                // Remove key if no more entries
                if (!_texts[textId][language].Any()) {
                    _texts[textId].Remove(language);
                }
            }
        }

    }

}
