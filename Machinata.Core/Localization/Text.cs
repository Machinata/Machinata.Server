using Machinata.Core.Builder;
using Machinata.Core.Templates;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Localization {

    /// <summary>
    /// Represents a localization text object. This class most importantly
    /// consists of a Id (or key), language and translation.
    /// Using the factory methods allows for quick hashtable lookups with
    /// automatic fallbacks.
    /// </summary>
    public partial class Text : Model.ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion


        /// <summary>
        /// The localization texts [textId][language]{"localization-file-value1","localization-file-value2","template-value1","template-value2","database-value"}
        /// The last entry in the [textId][language] is 
        /// </summary>
        private static Dictionary<string, Dictionary<string,List<Text>>> _texts = new Dictionary<string, Dictionary<string,List<Text>>>();

        public enum SourceTypes {
            LocalizationFile,
            Template,
            Database
        }

        /// <summary>
        /// The source (file or db, for example) where the text originated from.
        /// </summary>
        [FormBuilder()]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Source { get; set; }
        [FormBuilder(Forms.Admin.LISTING)]
        public string Package { get; set; }
        [FormBuilder(Forms.Admin.LISTING)]
        public string Id { get; set; }
        [FormBuilder(Forms.Admin.LISTING)]
        public string Language { get; set; }
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EDIT)]
        public string Value { get; set; }
        [FormBuilder(Forms.Admin.LISTING)]
        public SourceTypes SourceType { get; set; }

        public int Sort { get; set; }

        public override string PublicId
        {
            get
            {
                return $"{this.Id}-{this.Language}";
            }
        }

        public static List<string> RegisteredLanguages() {
            return _texts.Values.SelectMany(v => v.Keys).Distinct().ToList();
        }

        public static List<string> SupportedLanguages() {
            return Core.Config.LocalizationSupportedLanguages;
        }

        public static List<string> RegisteredPackages() {
            return _texts.Values.SelectMany(v=>v.Values).SelectMany(v=>v).GroupBy(t => t.Package).Select(t => t.Key).ToList();
        }

        /// <summary>
        /// Provides the highest prio texts per textid language -> 
        /// </summary>
        /// <param name="package">The package.</param>
        /// <returns></returns>
        public static List<Text> RegisteredTexts(string package = null) {
            if(package == null) return _texts.Values.SelectMany(v => v.Values).Select(v=>v.Last()).ToList(); 
            else return _texts.Values.SelectMany(v => v.Values).Select(v => v.Last()).Where(t => t.Package == package).ToList(); 
        }

        /// <summary>
        /// Gets all localizations for this id...overloads and languages
        /// </summary>
        /// <param name="textId">The text identifier.</param>
        /// <returns></returns>
        public static List<Text> GetTexts(string textId) {
            return _texts.Values.SelectMany(v=>v.Values).SelectMany(v=>v).Where(t=>t.Id == textId).ToList();
        }

        public static List<Text> GetTexts(string textId, string language) {
            if (_texts[textId].ContainsKey(language)) {
                return _texts[textId][language];
            }
            return null;
        }

        public static List<Text> GetAllTexts() {
            return _texts.Values.SelectMany(v => v.Values).SelectMany(v => v).ToList();
        }

        public static List<string> GetAllTextIds() {
            return _texts.Keys.ToList();
        }

        public static List<Text> GetDuplicates(string textId) {
            var duplicates = new List<Text>();
            foreach (var textsLanguage in _texts[textId]) {
                foreach(var text in textsLanguage.Value) {
                    if (text != textsLanguage.Value.Last()) {
                        duplicates.Add(text);
                    }
                }
            }
            return duplicates;
        }


        /// <summary>
        /// Inserts the text and translations in a text
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        public static string InsertVariables(string text, string language = null) {
            var template = new PageTemplate(text);
            template.Language = language??Core.Config.LocalizationDefaultLanguage;
            template.DiscoverVariables();
            template.InsertTextVariables();
            return template.Data.ToString();
        }

        /// <summary>
        /// Searches in Values of Texts
        /// </summary>
        /// <param name="search">The search.</param>
        /// <returns></returns>
        public static List<Text> Search(string search, IEnumerable<string> textIds) {
            var found = new List<Text>();
            foreach (var entry in _texts.Where(t=>textIds.Contains(t.Key)).Select(t=>t.Value)) {
                foreach( var langEntry in entry.Values) {
                    found.AddRange(langEntry.Where(le=>le.Value.ToLower().Contains(search.ToLower())));
                }
            }
          
            return found;
        }

        public static List<string> GetMissing(string textId, List<string> languages) {
            var missing = new List<string>();
            foreach (var language in languages) {
                var textsForLang = Text.GetTexts(textId, language);
                if (textsForLang == null || textsForLang.All(t => string.IsNullOrEmpty(t.Value))) {
                    missing.Add(textId);
                    break;
                }
            }
            return missing;
        }

        public static List<Text> GetBadwords(string[] languages, string keyPrefix = "badword-list.") {
            var badwordKeys = GetAllTextIds().Where(k => k.StartsWith(keyPrefix));
            var badwordList = new List<Text>();
            foreach(var key in badwordKeys) {
                foreach(var language in languages) {
                    var texts = GetTexts(key, language);
                    if (texts != null && texts.Any() == true) {
                        badwordList.Add(texts.Last());
                    }
                    
                }
            }
            return badwordList;
        }

        public static bool CheckIsBadWord(string word, string[] languages ) {
            word = Core.Util.String.RemoveDiacritics(word);
            var badWords = GetBadwords(languages);
            var matches = badWords.Where(bw => bw.Value.ToLowerInvariant() == word).ToList();
            if (matches.Any() == true ) {
                return true;
            }
            return false;
        }

        public static bool ContainsBadword(string text, string[] languages, out string badword) {
            var textCleaned = Core.Util.String.RemoveDiacritics(text);
            textCleaned = textCleaned.ToLowerInvariant();
            var words = textCleaned.Split(' ');
            foreach (var word in words) {
                var isBadword = Core.Localization.Text.CheckIsBadWord(word, languages);
                if (isBadword == true) {
                    badword = word;
                    return true;
                }
            }
            badword = null;
            return false;
        }

    }

}
