﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// TODO
/// ### Language Files
/// Language files (.txt) in a Product or Module's Localization folder are automatically loaded
/// on application startup and mapped by the Localization Text Factory. Such a text file
/// can contain any number of translations and typically looks like:
/// ```
/// order.en=Order
/// order.de=Bestellung
/// orders.de=Bestellungen
/// order-costs.en=Costs
/// order-costs.de=Kosten
/// order-save.en=Save Order
/// order-save.de=Speichern Order
/// ```
/// To translate entity properties and enums, one can use the following pre-defined translation ids 
/// (listed as priority used in finding the correct translation):
/// ```
/// {entity.type-name}.{property.name}.{form.id}
/// {entity.type-name}.{property.name}
/// {property.name}.{form.id}
/// {property.name}
/// ```
/// For example, to translate Order.Status one could set:
/// ```
/// order.status.de=Status
/// ```
/// In addition, to translate only a specific property on a specific form, one can use:
/// ```
/// order.status.edit.de=Change Status
/// ```
/// To translate the Order.Status enums one could set:
/// ```
/// order.status-type.confirmed.de=Bestaetigt
/// ```
/// Translating enums can have the following formats:
/// ```
/// {class.type-name}.{enum.type-name}.{enum.value}
/// {enum.type-name}.{enum.value}
/// enum.{enum.value}
/// ```
/// 
/// ### Templates
/// To include a translation in a template, simply use the ```text``` variable namespace:
/// 
/// ```
/// <span class="button">{text.order-save}</span>
/// ```
/// 
/// A translation can be defined inline:
/// 
/// ```
/// <span class="button">{text.order-save.en=Save Order}</span>
/// ```

/// </summary>
/// </summary>
namespace Machinata.Core.Localization {
   
}
