﻿

error.cannot-reset-universal-login-user.en=The account {0} is a universal login user. You must reset the password on {1}.
error.cannot-reset-universal-login-user.de=Das Konto {0} ist ein universeller Login-Benutzer. Sie müssen das Passwort auf {1} zurücksetzen.

error.login-required.en=You must be logged in to access this page.
error.login-required.de=Sie müssen eingeloggt sein, um auf diese Seite zugreifen zu können.
error.login-required.fr=Vous devez être connecté pour accéder à cette page.
error.login-required.it=Per accedere a questa pagina è necessario aver effettuato il login.


error.file-not-found.en=The file {0} could not be found.
error.file-not-found.de=Die Datei {0} konnte nicht gefunden werden.

error.username-invalid.en=The username '{0}' does not exist.
error.username-invalid.de=Der Benutzername '{0}' existiert nicht.

error.invalid-path.en=Sorry, the page '{0}' does not exist.
error.invalid-path.de=Sorry, die Seite '{0}' existiert nicht.
error.invalid-path.fr=Désolé, la page '{0}' n'existe pas.
error.invalid-path.it=Spiacente, la pagina '{0}' non esiste.

error.invalid-verb.en=Sorry, no page was found for the '{0}' with verb {1}.

error.api-call.validation.blocked.en=Sorry, the API call to the server was blocked
error.api-call.validation.blocked.spam.en=Bot was detected - please disable any blocker extensions or try in a different browser