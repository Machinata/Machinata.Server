using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Ids {

    public interface IIdSpreaderProvider {

        int SpreadId(int input);
        int UnspreadId(int input);

    }
}
