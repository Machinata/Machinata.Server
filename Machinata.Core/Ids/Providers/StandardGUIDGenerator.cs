using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Ids.Providers {


    public class StandardGUIDGenerator : IGUIDProvider {
        
        #region Interface Implementation

        
        public string GenerateGUID() {
            return Guid.NewGuid().ToString();
        }
        

        #endregion

        
    }
}
