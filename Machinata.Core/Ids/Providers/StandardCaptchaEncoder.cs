using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Ids.Providers {


    public class StandardCaptchaEncoder : ICaptchaEncoderProvider {
        
        private const string CAPTCHA_PREPEND = "h3{{UTeND(AO2q]KZX2s*7d17";
        private const string CAPTCHA_APPEND = "iF3aokl$8T{)@nBTalPIyMRV17";

        #region Interface Implementation

        public string RandomCode(int length = 6) {
            var random = new Random();
            const string chars = "ACDEFGHKLMNPQRSTUVWXYZ23456789";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public string EncodeCode(string input) {
            return Core.Encryption.DefaultEncryption.EncryptString(input);
        }

        public string DecodeCode(string input) {
            var decoded = Core.Encryption.DefaultEncryption.DecryptString(input);
            return decoded;
        }

        public bool ValidateCode(string encoded, string usercode, bool throwException = true) {
            var valid = DecodeCode(encoded).ToLower() == usercode.ToLower();
            if (throwException && !valid) throw new Exceptions.BackendException("invalid-captcha","Sorry, the captcha code you entered is not correct. Please try again.");
            return valid;
        }

        #endregion

        
    }
}
