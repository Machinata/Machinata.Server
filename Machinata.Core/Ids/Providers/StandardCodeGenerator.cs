using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Ids.Providers {


    public class StandardCodeGenerator : ICodeProvider {
        
        #region Interface Implementation

        // ACDEFGHKLMNPQRSTUVWXYZ2345679
        // Excludes B, I, J, O, 0, 1, 8
        // The padding character is B
        private static char[] _alphabet = new char[] { 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '2', '3', '4', '5', '6', '7', '9' };
        private static char _paddingChar = 'B';

        public string GenerateCodeForId(int id) {
            if (id == 0) throw new Exception("Id cannot be zero for generating a code.");

            // CodeGeneratorStandardSecretPrime: Big prime number (10 digits)
            // CodeGeneratorStandardSecretRandomXOR: Random big integer number, not bigger than _maxId
            int distributeId = ((id * Core.Config.CodeGeneratorStandardSecretPrime) & int.MaxValue) ^ Core.Config.CodeGeneratorStandardSecretRandomXOR;
            
            string code = _intToStringFast(distributeId);
            code = code.PadLeft(7, _paddingChar);
            return code;
        }
        
        
        /// <summary>
        /// Via https://stackoverflow.com/questions/923771/quickest-way-to-convert-a-base-10-number-to-any-base-in-net
        /// </summary>
        private static string _intToString(int value) {
            string result = string.Empty;
            int targetBase = _alphabet.Length;
            do
            {
                result = _alphabet[value % targetBase] + result;
                value = value / targetBase;
            } 
            while (value > 0);

            return result;
        }

        /// <summary>
        /// An optimized method using an array as buffer instead of 
        /// string concatenation. This is faster for return values having 
        /// a length > 1.
        /// Via https://stackoverflow.com/questions/923771/quickest-way-to-convert-a-base-10-number-to-any-base-in-net
        /// </summary>
        private static string _intToStringFast(int value)
        {
            // 32 is the worst cast buffer size for base 2 and int.MaxValue
            int i = 32;
            char[] buffer = new char[i];
            int targetBase= _alphabet.Length;

            do
            {
                buffer[--i] = _alphabet[value % targetBase];
                value = value / targetBase;
            }
            while (value > 0);

            char[] result = new char[32 - i];
            Array.Copy(buffer, i, result, 0, 32 - i);

            return new string(result);
        }

        #endregion

        
    }
}
