using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Ids {


    /// <summary>
    /// Reserved Serial ID Type Numbers:
    ///  - NotImplemented: 0 (MODELOBJECT_NO_TYPENUMBER)
    ///  - Business: 10
    ///  - Order: 20
    ///  - OrderSubscription: 26
    ///  - Invoice: 30
    /// </summary>
    public interface ISerialIdProvider {

        string GetSerialIdForEntity(Model.ModelObject entity);

    }
}
