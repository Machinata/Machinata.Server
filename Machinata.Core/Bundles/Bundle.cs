using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Machinata.Core.Util.Git;

/// <summary>
/// 
/// </summary>
namespace Machinata.Core.Bundles {
    public class Bundle {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        public static string CachePath = Core.Config.CachePath + Core.Config.PathSep + "static";
        public static string CachePathBundles = CachePath + "-bundles";
        public static string CachePathImages = CachePath + "-images";

        public static string GetBundleCachePath(ModelContext db, string packageName, string bundlePath, string language = null, string themeName = "default", string build = null, bool debug = false, string profile = null, string outputFormat = null) {
            if (build == null) build = Core.Config.BuildID;

            // Path valid?
            CheckFilePath(bundlePath);

            string fileType = Core.Util.Files.GetFileExtensionWithoutDot(bundlePath);
            string bundleName = bundlePath.Replace("/", "_").Replace(Core.Config.PathSep, "_").Replace(".", "_");
            string cachePath = CachePathBundles + Core.Config.PathSep + bundleName + "_" + build + "_" + themeName.Replace("-", "").Replace("_", "") + "_" + language + (debug ? "_debug" : "") + (profile != null ? "_" + profile : "") + (outputFormat != null ? "_" + outputFormat : "") + "." + fileType;


            // Language too long?
            if (language != null && language.Length > 5) {
                throw new BackendException("language-too-long", "The requested bundle language is too long");
            }

            // Path too long?
            if (cachePath != null && cachePath.Length > 248) { // Max length of directories
                throw new BackendException("name-too-long", "The requested bundle name is too long");
            }

            // Path valid?
            CheckFilePath(cachePath);


            // Create the bundle as cached if needed
            bool forceCreate = false;
            if (!Core.Config.StaticBundleCacheEnabled || Core.Config.HotSwappingEnabled) {
                forceCreate = true;
            }

            // Override for specific types, if set
            // This allows one to have no caching for all bundles, but set some to always cache...
            if (Core.Config.GetStringSetting("StaticBundleCacheEnabled-" + fileType.ToUpper(), null) == "true") {
                forceCreate = false;
            }
            Core.Caching.FileCacheGenerator.CreateIfNeeded(cachePath, () => {
                // Init
                _logger.Info("Creating bundle " + bundleName);

                // Create the bundle generator
                Core.Bundles.BundleGenerator generator = null;
                if (fileType == "js") generator = new Core.Bundles.JSBundleGenerator();
                else if (fileType == "css") generator = new Core.Bundles.CSSBundleGenerator();
                else if (fileType == "svg") generator = new Core.Bundles.SVGBundleGenerator();
                else generator = new Core.Bundles.JSBundleGenerator();

                // Load the theme safely with fallback
                Core.Model.Theme theme = Theme.GetThemeByName(themeName);
                // Load, compress and write
                generator.Load(packageName, bundlePath, theme, language, debug, profile, outputFormat, forceCreate == true);
                //generator.Compress();
                generator.WriteToFile(cachePath);

            }, forceCreate);

            return cachePath;
        }

        /// <summary>
        /// Check if this is a valid path on file system
        /// </summary>
        /// <param name="cachePath"></param>
        /// <exception cref="BackendException"></exception>
        private static void CheckFilePath(string cachePath) {
            //https://stackoverflow.com/questions/3137097/check-if-a-string-is-a-valid-windows-directory-folder-path
            try {
                System.IO.Path.GetFullPath(cachePath);
            } catch {
                throw new BackendException("path-invalid", "The requested path is invalid");
            }
        }

        /// <summary>
        /// Deletes all bundles in the 'CachePath + "-bundles"' directory
        /// </summary>
        public static void CleanAllCacheBundles() {

            var cacheBundleDirectory = new DirectoryInfo(CachePathBundles);

            _logger.Info($"CleanAllCacheBundles(): Deleting cache bundles in - {cacheBundleDirectory.FullName}");

            var filesInDirectory = cacheBundleDirectory.GetFiles();
            foreach (var file in filesInDirectory) {
                _logger.Info("Deleting " + file.FullName );
                try {
                    file.Delete();
                } catch (Exception e) {
                    _logger.Warn(e, $"Could not delete: {file.FullName}");
                }
            }

        }
    }
}
