using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Bundles {

    public class JSBundleGenerator : BundleGenerator {

        public override void PreProcess(BundleMeta bundleMeta, StringBuilder contents) {
            // Pre-process for pre-compiled (typescript) environments
            // In this case, each .ts files is precompiled to the folder pre-compiled
            foreach (var bundleFile in bundleMeta.Files) {
                if (bundleFile.Filepath.EndsWith(".ts")) {
                    // Derive the pre-compiled path, this will be the asset we use for the bundle...
                    var fileInfo = new System.IO.FileInfo(bundleFile.Filepath);
                    var preCompiledFilename = bundleFile.Filename.ReplaceSuffix(".ts", ".js");
                    var preCompiledFilepath = fileInfo.DirectoryName + Core.Config.PathSep + "pre-compiled" + Core.Config.PathSep + preCompiledFilename;
                    if(!System.IO.File.Exists(preCompiledFilepath)) {
                        throw new Exception("Could not find the pre-compiled file "+preCompiledFilename + " in the pre-compiled folder. Are you sure the proper files have been added to the project (also the pre-compiled files) and the proper pre-compiling settings have been setup (such as a tsconfig.json in the bundle)?");
                    }
                    // Override the bundle file paths
                    bundleFile.Filepath = preCompiledFilepath;
                    bundleFile.Filename = preCompiledFilename;
                    bundleFile.SourceType = "ts";
                }
            }

            base.PreProcess(bundleMeta, contents);
        }

        public override bool ShouldInsertConfigVariables(string file) {
            return true;
        }

        public override bool ShouldInsertTextVariables(string file) {
            if (file.StartsWith("machinata-")) return true;
            else return base.ShouldInsertTextVariables(file);
        }

        public override bool ShouldCompressFile(string file) {
            if (!file.Contains(".min.js")) return true;
            else return base.ShouldCompressFile(file);
        }
        
        public override void InsertVariable(StringBuilder contents, string name, string value) {
            contents.Replace("{" + name + "}", value);
        }

        public override void Compress(StringBuilder contents) {
            // See https://github.com/trullock/NUglify
            try {
                var codeSettings = new NUglify.JavaScript.CodeSettings();
                codeSettings.AmdSupport = false; // Don't minify AMD stuff
                codeSettings.TermSemicolons = true; // Always add trailing ; since we are conat files
                //codeSettings.RemoveUnneededCode = false;
                codeSettings.EvalTreatment = NUglify.JavaScript.EvalTreatment.MakeImmediateSafe; // Make sure functions with eval in them can access the local vars and arguments
                var ret = NUglify.Uglify.Js(contents.ToString(), codeSettings);
                contents.Clear();
                contents.Append(ret);
            } catch (Exception e) {
                throw new Exception("Javascript compressor error: ", e);
            }
        }

        public override string AlterBundleFile(string bundleContents, BundleMeta bundleMeta, BundleMetaFile bundleFile) {
            
            // Typescript namespace hack?
            if(bundleFile.SourceType == "ts") {
                bundleContents = bundleContents.Replace("var Machinata;","//var Machinata; // automatically disabled by JSBUndleGenerator.cs due to nesting behavior of TS namespaces...");
                bundleContents = bundleContents.Replace("var $ = require(\"./typings/jquery\")", "/* var $ = require(\"./typings/jquery\") //automatically disabled by JSBUndleGenerator.cs due to nesting behavior of TS namespaces */");
                bundleContents = bundleContents.Replace("var $ = require(\"../../../../Machinata.Server/Machinata.Core/Static/js/machinata-core-bundle.js/typings/jquery\")", "/* var $ = require(\"./typings/jquery\") //automatically disabled by JSBUndleGenerator.cs due to nesting behavior of TS namespaces */");
            }

            // Remove CommonJS loader? 
            // This is used by profiles such as reactjs that bundle all the JS vendor assets into one namespace and package everythin into a single importable bundle
            if (bundleFile.JSON != null 
                && bundleFile.JSON["profile-" + this.Profile] != null
                && bundleFile.JSON["profile-" + this.Profile]["remove-commonjs-loader"] != null
                && bundleFile.JSON["profile-" + this.Profile]["remove-commonjs-loader"].ToString().ToLower() == "true") {

                // List of known loader variants that we try to remove
                var loaderVariants = new List<Tuple<string,string>>();
                // Very specific variants
                //loaderVariants.Add(new Tuple<string, string>("typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :\r\ntypeof define === 'function' && define.amd ? define(['exports'], factory) :\r\n(global = global || self, factory(global.d3 = global.d3 || { }));", "factory(global.d3 = global.d3 || { });")); // D3 type loader, is completely removed
                // Uncompressed variants...
                loaderVariants.Add(new Tuple<string, string>("typeof module === \"object\" && typeof module.exports === \"object\"","false"));
                loaderVariants.Add(new Tuple<string, string>("typeof module === 'object' && typeof module.exports === 'object'", "false"));
                loaderVariants.Add(new Tuple<string, string>("typeof exports === 'object'", "false"));
                loaderVariants.Add(new Tuple<string, string>("\"object\" == typeof module", "false"));
                // Compressed variants...
                loaderVariants.Add(new Tuple<string, string>("\"object\"==typeof exports&&\"undefined\"!=typeof module", "false"));
                loaderVariants.Add(new Tuple<string, string>("\"object\"==typeof exports&&\"object\"==typeof module", "false"));
                // Odd variants, mostly removals
                loaderVariants.Add(new Tuple<string, string>("typeof globalThis !== 'undefined' ? globalThis : ", "")); // vega globalThis
                loaderVariants.Add(new Tuple<string, string>("}(this, (function (exports) { 'use strict';", "}(((typeof global !== 'undefined') ? global : window), (function (exports) { 'use strict';")); // patch vega this argument to factory loader
                if(bundleFile.JSON["profile-" + this.Profile]["remove-custom-loaders"] != null) {
                    foreach(var customLoaders in bundleFile.JSON["profile-" + this.Profile]["remove-custom-loaders"]) {
                        loaderVariants.Add(new Tuple<string, string>(customLoaders["loader-script"].ToString(), customLoaders["loader-replacement"].ToString()));
                    }
                }

                // Try each loader variant
                // As soon as one of the loaders is removed, we finish
                bool loaderRemoved = false;
                foreach(var loaderVariantTuple in loaderVariants) {
                    // We only replace the first occurance in the bundle file contents...
                    var search = loaderVariantTuple.Item1;
                    var replace = loaderVariantTuple.Item2;
                    int pos = bundleContents.IndexOf(search);
                    if (pos >= 0) {
                        // Match, loader found, disable it
                        var disabledLoaderVariant = replace+" /* Note: CommonJS loader automatically stripped by Machinata "+this.Profile+" profile: "+ search.Replace("/*","").Replace("*/", "") + " */";
                        // Swap it out
                        bundleContents = bundleContents.Substring(0, pos) + disabledLoaderVariant + bundleContents.Substring(pos + search.Length);
                        loaderRemoved = true;
                    } else {
                        // No match, loader not found
                    }
                }
                if (loaderRemoved == false) throw new Exception("Could not find any matching CommonJS loader to remove in "+bundleFile.Filename+". The JSBundleGenerator might need to be updated to support the flavour of CommonJS loader used in this bundle. Alternatively, the 'remove-common-js' flag can be removed to disable this feature.");
            }

            // Remove source map definiations if not in debug mode
            if (this.Debug == false) {
                bundleContents = bundleContents.Replace("//# sourceMappingURL=", "//# DISABLED_sourceMappingURL=");
            }

            // Just return the contents as they are
            return bundleContents;
        }

    }
}
