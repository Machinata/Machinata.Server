using Machinata.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Imaging {

    /// <summary>
    /// Handles the entire lifecycle for working with images, including caching any of the imaging functions.
    /// </summary>
    public class ImageFactory {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        public const int exifOrientationID = 0x112; //274

        public static string CachePathImageFactory = Core.Config.CachePath + Core.Config.PathSep + "image-factory";

        /// <summary>
        /// Shortcut method, see the main method for documentation.
        /// 
        /// 
        /// Automatically processes a image for the current request, returning a new filepath that is cached.
        /// All parameters are optional and can be passed either blank or null.
        /// The processed image is automatically cached to disk for future reference.

        /// Examples when used by the content handler:

        /// Crop the image to the exact size 650x234 (keeping aspect) as a JPG 95%, at 2x retina
        /// ```
        /// /content/image/dlBqVQ/Amber.jpg?crop=650x234&format=jpg&quality=95&scale=2
        /// ```
        /// 
        /// Resize image to exactly 100x100 (not keeping aspect) as a PNG
        /// ```
        /// /content/image/dlBqVQ/Amber.jpg?size=100x100&format=png
        /// ```
        /// 
        /// Scale(or reduce) image to exact width of 300, keeping the aspect ratio
        /// ```
        /// /content/image/dlBqVQ/Amber.jpg?size=300
        /// ```
        /// 
        /// Scale(or reduce) image to exact height of 600, keeping the aspect ratio
        /// ```
        /// /content/image/dlBqVQ/Amber.jpg?size=x600
        /// ```
        /// 
        /// Recolor an image while preserving the alpha channel
        /// ```
        /// /content/image/zhei83/Logo.png?recolor=%26FF00FF
        /// ```
        /// 
        /// The following context variables(via GET or POST) are supported:
        /// 
        /// - format: Specifies the image type(either png or jpg). If no type is specified, then the current type of the image is kept.
        /// - quality: The quality of the image type(only applies to JPG, number between 1 and 100).
        /// - size: The size of the image to force in format of 'WxH' where W and H are numbers.This will stretch the image to the given size. If either W or H is '' or '?', then the image aspect ratio is kept.
        /// - crop: If specified, automatically crops the image to the given format of 'WxH' here W and H are numbers. If the original is either smaller or larger than the given crop, then the image is automatically scaled. The resulting crop is always centered both horizontally and vertically.
        /// - scale: The scale of the resulting image in the format of 'Sx' where S is a number greater than or equal to 1. If you specify '2x', for example, you would get a image twice as large as the given size or crop.
        /// - recolor: The color to fill the new image with while preserving the alpha channel.
        /// 
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="handler">The handler.</param>
        /// <returns></returns>
        public static string ProcessImageForRequest(string filePath, Handler.Handler handler) {
            return ProcessImage(
                filePath: filePath,
                handler: handler,
                format: handler.Params.String("format"),
                quality: handler.Params.String("quality"),
                size: handler.Params.String("size"),
                maxsize: handler.Params.String("maxsize"),
                crop: handler.Params.String("crop"),
                fit: handler.Params.String("fit"),
                scale: handler.Params.String("scale"),
                recolor: handler.Params.String("recolor"),
                flood: handler.Params.String("flood"),
                fill: handler.Params.String("fill"),
                cut: handler.Params.String("cut"),
                overlay: handler.Params.String("overlay"),
                cache: handler.Params.Bool("cache",true)
            );
        }

        /// <summary>
        /// Processes a image for the given parameters.
        /// All parameters are optional and can be passed either blank or null.
        /// The processed image is automatically cached to disk for future reference.
        /// Examples when used by the content handler:
        /// 
        /// Crop the image to the exact size 650x234 (keeping aspect) as a JPG 95%, at 2x retina
        /// /content/image/dlBqVQ/Amber.jpg?crop=650x234&format=jpg&quality=95&scale=2
        /// 
        /// Resize image to exactly 100x100 (not keeping aspect) as a PNG
        /// /content/image/dlBqVQ/Amber.jpg?size=100x100&format=png
        /// 
        /// Scale (or reduce) image to exact width of 300, keeping the aspect ratio
        /// /content/image/dlBqVQ/Amber.jpg?size=300
        /// 
        /// Scale (or reduce) image to exact height of 600, keeping the aspect ratio
        /// /content/image/dlBqVQ/Amber.jpg?size=x600
        /// 
        /// </summary>
        /// <param name="filePath">The file path or the image file. Must be a jpg or png.</param>
        /// <param name="handler">The handler.</param>
        /// <param name="format">Specifies the image type (either png or jpg). If no type is specified, then the current type of the image is kept.</param>
        /// <param name="quality">The quality of the image type (only applies to JPG, number between 1 and 100).</param>
        /// <param name="size">The size of the image to force in format of 'WxH' where W and H are numbers. This will stretch the image to the given size. If either W or H is '' or '?', then the image aspect ratio is kept.</param>
        /// <param name="crop">If specified, automatically crops the image to the given format of 'WxH' here W and H are numbers. If the original is either smaller or larger than the given crop, then the image is automatically scaled. The resulting crop is always centered both horizontally and vertically.</param>
        /// <param name="scale">The scale of the resulting image in the format of 'Sx' where S is a number greater than or equal to 1. If you specify '2x', for example, you would get a image twice as large as the given size or crop.</param>
        /// <param name="recoler">If set, applies a new color to the image while respecting the alpha channel.</param>
        /// <returns>The new cached path.</returns>
        public static string ProcessImage(
            string filePath, 
            Handler.Handler handler,
            string format,
            string quality,
            string size,
            string maxsize,
            string crop,
            string fit,
            string scale,
            string recolor,
            string flood,
            string fill,
            string cut,
            string overlay,
            bool cache = true) {
            // Validate if we even are changing anything
            bool changeDetected = false;
            if (!string.IsNullOrEmpty(format)
                || !string.IsNullOrEmpty(size)
                || !string.IsNullOrEmpty(maxsize)
                || !string.IsNullOrEmpty(crop)
                || !string.IsNullOrEmpty(fit)
                || !string.IsNullOrEmpty(scale)
                || !string.IsNullOrEmpty(recolor)
                || !string.IsNullOrEmpty(flood)
                || !string.IsNullOrEmpty(fill)
                || !string.IsNullOrEmpty(overlay)
                || !string.IsNullOrEmpty(cut)) changeDetected = true;
            if (!changeDetected) return filePath;

            // Ignored image type?
            if (filePath.EndsWith(".gif")) return filePath;
            if (filePath.EndsWith(".svg")) return filePath;
            //if (filePath.EndsWith(".webp")) return filePath;

            //TODO: Validate all parameters to ensure they don't contain dangerous characters...

            // Generate cache path
            format = _cleanParamter(format);
            quality = _cleanParamter(quality);
            size = _cleanParamter(size);
            maxsize = _cleanParamter(maxsize);
            crop = _cleanParamter(crop);
            fit = _cleanParamter(fit);
            cut = _cleanParamter(cut);
            scale = _cleanParamter(scale);
            string overlayCacheParam = "";
            if (overlay != null) overlayCacheParam = "_OL" + _cleanParamter(overlay.ToLower()).Replace(" ", "");
            string fileName = Core.Util.Files.GetFileNameWithoutExtension(filePath);
            string fileExtension = Core.Util.Files.GetFileExtensionWithoutDot(filePath);
            string newFileExtension = Core.Util.Files.GetFileExtensionWithoutDot(filePath);
            if(format != null && format != "") newFileExtension = format;
            string cachePath = $"{CachePathImageFactory}{Core.Config.PathSep}{fileName}_F{format}_Q{quality}_S{size}_MS{maxsize}_C{crop}_F{fit}_S{scale}_R{_cleanParamter(recolor)}_FI{_cleanParamter(fill)}_FL{_cleanParamter(flood)}_X{_cleanParamter(cut)}{overlayCacheParam}.{newFileExtension}";


            // Create the bundle as cached if needed
            var forceCreate = false;
            if(!cache) forceCreate = true;
            if(!Core.Config.ContentImageCacheEnabled) forceCreate = true;
            Core.Caching.FileCacheGenerator.CreateIfNeeded(cachePath, ()=> {
                // Init
                //_logger.Info("Creating bar code "+data);

                // Load image
                // Note: our version of .net does not support webp, so we encode/decode it manually
                Image image = null;
                if (fileExtension.ToLower() == "webp") {
                    // Get all bytes of file
                    var bytes = System.IO.File.ReadAllBytes(filePath);
                    var webpBitmap = Dynamicweb.WebP.Decoder.Decode(bytes);
                    image = webpBitmap;
                } else {
                    image = Bitmap.FromFile(filePath);
                }
                //int colorDepth = int.Parse(image.PixelFormat.ToString());

                // Init format
                if(format == null) {
                    format = fileExtension.ToLower();
                }
                int intQuality = 100;
                if(quality != null) {
                   try {
                        intQuality = int.Parse(quality);
                    } catch(Exception e) {
                        throw new BackendException("invalid-quality", $"The quality parameter '{quality}' is not valid. It must be a number between 1 and 100.", e);
                    }
                }

                // EXIF rotation?
                if (image.PropertyIdList.Contains(exifOrientationID)) {
                    // This image is tagged with a EXIF rotation, we need to manually rotate it...
                    var prop = image.GetPropertyItem(exifOrientationID);
                    int val = BitConverter.ToUInt16(prop.Value, 0);
                    // Set the new rotation
                    var rot = RotateFlipType.RotateNoneFlipNone;
                    if (val == 3 || val == 4) rot = RotateFlipType.Rotate180FlipNone;
                    else if (val == 5 || val == 6) rot = RotateFlipType.Rotate90FlipNone;
                    else if (val == 7 || val == 8) rot = RotateFlipType.Rotate270FlipNone;
                    if (val == 2 || val == 4 || val == 5 || val == 7) rot |= RotateFlipType.RotateNoneFlipX;
                    // Perform the rotation
                    if (rot != RotateFlipType.RotateNoneFlipNone) image.RotateFlip(rot);
                }

                // Init dimensions
                int newSizeW = image.Width; // Default
                int newSizeH = image.Height; // Default
                int newDrawW = image.Width; // Default
                int newDrawH = image.Height; // Default
                int newDrawX = 0; // Default
                int newDrawY = 0; // Default
                double originalRatio = (double)image.Width / (double)image.Height;
                
                // Calculate resize
                if (size != null) {
                    try {
                        var newSizeSegs = size.Split('x');
                        if (newSizeSegs.Length == 2) {
                            if (newSizeSegs[0] == "?" || newSizeSegs[0] == "") newSizeW = 0;
                            else newSizeW = int.Parse(newSizeSegs[0]);
                            if (newSizeSegs[1] == "?" || newSizeSegs[1] == "") newSizeH = 0;
                            else newSizeH = int.Parse(newSizeSegs[1]);
                        } else if (newSizeSegs.Length == 1) {
                            newSizeW = int.Parse(newSizeSegs[0]);
                            newSizeH = 0;
                        } else {
                            throw new Exception("Invalid size.");
                        }
                        // Validate
                        if (newSizeW == 0 && newSizeH == 0) throw new Exception("Both W and H cannot be ?.");
                        // Adjust to scale
                        if (newSizeW == 0) newSizeW = (int)(originalRatio * newSizeH);
                        if (newSizeH == 0) newSizeH = (int)(newSizeW / originalRatio);
                        newDrawW = newSizeW;
                        newDrawH = newSizeH;
                    } catch(Exception e) {
                        throw new BackendException("invalid-size", $"The size parameter '{size}' is not valid. It must be in the format 'WxH' where W and H are numbers. To fit to ratio, you can use ? instead of a number.", e);
                    }
                }

                // Calculate resize
                if (maxsize != null) {
                    try {
                        var newSizeSegs = maxsize.Split('x');
                        if (newSizeSegs.Length == 2) {
                            if (newSizeSegs[0] == "?" || newSizeSegs[0] == "") newSizeW = 0;
                            else newSizeW = int.Parse(newSizeSegs[0]);
                            if (newSizeSegs[1] == "?" || newSizeSegs[1] == "") newSizeH = 0;
                            else newSizeH = int.Parse(newSizeSegs[1]);
                        } else if (newSizeSegs.Length == 1) {
                            newSizeW = int.Parse(newSizeSegs[0]);
                            newSizeH = 0;
                        } else {
                            throw new Exception("Invalid maxsize.");
                        }
                        // Validate
                        if (newSizeW == 0 && newSizeH == 0) throw new Exception("Both W and H cannot be ?.");
                        // Resize?
                        if(image.Width > newSizeW && newSizeW != 0) {
                            newSizeH = (int)(newSizeW / originalRatio);
                        } else if(image.Height > newSizeH && newSizeH != 0) {
                            newSizeW = (int)(originalRatio * newSizeH);
                        } else {
                            newSizeW = image.Width;
                            newSizeH = image.Height;
                        }
                        newDrawW = newSizeW;
                        newDrawH = newSizeH;
                    } catch (Exception e) {
                        throw new BackendException("invalid-maxsize", $"The maxsize parameter '{size}' is not valid. It must be in the format 'WxH' where W and H are numbers. To fit to ratio, you can use ? instead of a number.", e);
                    }
                }

                // Calculate crop/thumbnail
                if (crop != null) {
                    try {
                        var newCropSegs = crop.Split('x');
                        if (newCropSegs.Length == 2) {
                            newSizeW = int.Parse(newCropSegs[0]);
                            newSizeH = int.Parse(newCropSegs[1]);
                        } else {
                            throw new Exception("Invalid crop.");
                        }
                        // Scale up or down to fit
                        double newRatio = (double)newSizeW / (double)newSizeH;
                        if(newRatio < originalRatio) {
                            newDrawH = newSizeH;
                            newDrawW = (int)(originalRatio * newDrawH);
                        } else {
                            newDrawW = newSizeW;
                            newDrawH = (int)(newDrawW / originalRatio);
                        }
                        // Center
                        newDrawX = (newSizeW - newDrawW) / 2;
                        newDrawY = (newSizeH - newDrawH) / 2;
                    } catch(Exception e) {
                        throw new BackendException("invalid-crop", $"The crop parameter '{crop}' is not valid. It must be in the format 'WxH' where W and H are numbers. To fit to ratio, you can use ? instead of a number.", e);
                    }
                }

                
                // Calculate crop/thumbnail
                if (fit != null) {
                    try {
                        var newFitSegs = fit.Split('x');
                        if (newFitSegs.Length == 2) {
                            newSizeW = int.Parse(newFitSegs[0]);
                            newSizeH = int.Parse(newFitSegs[1]);
                        } else {
                            throw new Exception("Invalid fit.");
                        }
                        // Scale up or down to fit
                        double newRatio = (double)newSizeW / (double)newSizeH;
                        if(newRatio > originalRatio) {
                            newDrawH = newSizeH;
                            newDrawW = (int)(originalRatio * newDrawH);
                        } else {
                            newDrawW = newSizeW;
                            newDrawH = (int)(newDrawW / originalRatio);
                        }
                        // Center
                        newDrawX = (newSizeW - newDrawW) / 2;
                        newDrawY = (newSizeH - newDrawH) / 2;
                    } catch(Exception e) {
                        throw new BackendException("invalid-fit", $"The fit parameter '{fit}' is not valid. It must be in the format 'WxH' where W and H are numbers.", e);
                    }
                }

                // Calculate cut
                if (cut != null) {
                    try {
                        //newDrawH = int.Parse(cut);
                        newSizeH = int.Parse(cut);
                    } catch(Exception e) {
                        throw new BackendException("invalid-cut", $"The cut parameter '{cut}' is not valid. It must be in the format 'H' where H are numbers.", e);
                    }
                }

                // Calculate scale
                if(scale != null) {
                    int intScale = 1;
                    try {
                        scale = scale.Replace("x", "");
                        intScale = int.Parse(scale);
                    } catch(Exception e) {
                        throw new BackendException("invalid-scale", $"The scale parameter '{scale}' is not valid. It must be in the format 'Sx' where S is a number greater or equal to 1.", e);
                    }
                    newSizeW = newSizeW * intScale; 
                    newSizeH = newSizeH * intScale; 
                    newDrawW = newDrawW * intScale;
                    newDrawH = newDrawH * intScale;
                    newDrawX = newDrawX * intScale; 
                    newDrawY = newDrawY * intScale; 
                }

                // Create a new canvas for drawing onto
                //TODO: at this stage we lose our EXIF data from image
                // Get the new format...
                // Typically we want the same, but indexed doesn't allow for rgb manipulation, so we do some exceptions
                var newPixelFormat = image.PixelFormat;
                if (newPixelFormat == PixelFormat.Format1bppIndexed) newPixelFormat = PixelFormat.Format32bppPArgb;
                if (newPixelFormat == PixelFormat.Format4bppIndexed) newPixelFormat = PixelFormat.Format32bppPArgb;
                if (newPixelFormat == PixelFormat.Format8bppIndexed) newPixelFormat = PixelFormat.Format32bppPArgb;
                Bitmap bitmap = new Bitmap(newSizeW, newSizeH, newPixelFormat);
                if (image.HorizontalResolution != 0 && image.VerticalResolution != 0) bitmap.SetResolution(image.HorizontalResolution, image.VerticalResolution);

                // Create image attributes to avoid a thin black border when resizing
                // Setting the wrap mode fixes this border
                var imageAttrs = new ImageAttributes();
                imageAttrs.SetWrapMode(WrapMode.TileFlipXY);

                // Init drawing canvas
                Graphics g = Graphics.FromImage(bitmap);
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                // Fill
                if (fill != null && fill != "") {
                    g.Clear(Color.FromName(fill));
                }

                // Draw image
                var destRect = new Rectangle(newDrawX, newDrawY, newDrawW, newDrawH); 
                var srcRect = new Rectangle(0, 0, image.Width, image.Height); 
                g.DrawImage(image, destRect, srcRect.X, srcRect.Y, srcRect.Width, srcRect.Height, GraphicsUnit.Pixel, imageAttrs);
                double newImageAR = (double)newSizeW / (double)newSizeH;

                // Recolor
                if(recolor != null && recolor != "") {
                    bitmap = ColorFillBitamp(bitmap, recolor);
                }

                // Flood
                if(flood != null) {
                    bitmap = FloodFillBitamp(bitmap, flood);
                }

                // Overlay
                if(overlay != null) {
                    // Get overlay image
                    var overlayFilepath = Core.Config.StaticPath + Core.Config.PathSep + "images" + Core.Config.PathSep + overlay;
                    if (!System.IO.File.Exists(overlayFilepath)) throw new BackendException("invalid-overlay", "Could not find the overlay image '" + overlay + "'. Overlay images must be in the static images folder.");
                    var overlayImage = Bitmap.FromFile(overlayFilepath);
                    double overlayAR = (double)overlayImage.Width / (double)overlayImage.Height;
                    int newOverlayW = overlayImage.Width;
                    int newOverlayH = overlayImage.Height;
                    // Scale to fit
                    if(overlayAR > newImageAR) {
                        // Fit to width
                        newOverlayW = newDrawW;
                        newOverlayH = (int)(newOverlayW / overlayAR);
                    } else {
                        // Fit to height
                        newOverlayH = newDrawH;
                        newOverlayW = (int)(overlayAR * newOverlayH);
                    }
                    Rectangle overlayDestRect = new Rectangle(
                        (int)(newDrawW/2.0 - newOverlayW/2.0), 
                        (int)(newDrawH/2.0 - newOverlayH/2.0), 
                        newOverlayW, 
                        newOverlayH
                    );
                    // Draw ontop
                    g.DrawImage(overlayImage, overlayDestRect, 0, 0, overlayImage.Width, overlayImage.Height, GraphicsUnit.Pixel, imageAttrs);
                }

                // Write out

                // Find a match for the encoder
                // Note: our version of .net does not support webp, so we encode/decode it manually
                if (format.ToLower() == "jpg") format = "jpeg";
                if (format.ToLower() == "webp") {
                    var bytes = Dynamicweb.WebP.Encoder.Encode(bitmap, intQuality);
                    System.IO.File.WriteAllBytes(cachePath, bytes);
                } else {
                    bool didFindMatch = false;
                    foreach (ImageCodecInfo encoder in ImageCodecInfo.GetImageEncoders())
                    {
                        // Match?
                        if (encoder.FormatDescription.ToLower() == format.ToLower())
                        {
                            // We got a match, create our encoder parameters
                            EncoderParameters parameters = new EncoderParameters(1);
                            parameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, intQuality);
                            //parameters.Param[1] = new EncoderParameter(System.Drawing.Imaging.Encoder.ColorDepth, colorDepth);
                            // Save the image
                            bitmap.Save(cachePath, encoder, parameters);
                            didFindMatch = true;
                        }
                    }
                    if (!didFindMatch) throw new BackendException("invalid-format", $"The image format ${format} is not supported.");
                }
                
                

            }, forceCreate);


            return cachePath;
        }

        #region Bitmap Transformers 

        public delegate void PixelTransformBitmapDelegate(byte[] pixelBuffer);

        public static Bitmap PixelTransformBitmap(Bitmap source, PixelTransformBitmapDelegate transform) {
            // Get unlocked pixels
            BitmapData sourceData = source.LockBits(new Rectangle(0, 0, source.Width, source.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb); 
            byte[] pixelBuffer = new byte[sourceData.Stride * sourceData.Height];
            System.Runtime.InteropServices.Marshal.Copy(sourceData.Scan0, pixelBuffer, 0, pixelBuffer.Length); 
            source.UnlockBits(sourceData); 
            // Call transform delegate
            transform(pixelBuffer);
            // Get result
            Bitmap resultBitmap = new Bitmap(source.Width, source.Height); 
            BitmapData resultData = resultBitmap.LockBits(new Rectangle(0, 0, resultBitmap.Width, resultBitmap.Height), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
            System.Runtime.InteropServices.Marshal.Copy(pixelBuffer, 0, resultData.Scan0, pixelBuffer.Length); 
            resultBitmap.UnlockBits(resultData); 
            return resultBitmap; 
        }

        public static Bitmap ColorShadeBitamp(Bitmap source, string color) {
            Color c = ColorTranslator.FromHtml(color);
            double blueShade = c.B / 255.0;
            double greenShade = c.G / 255.0;
            double redShade = c.R / 255.0;
            return PixelTransformBitmap(source, delegate(byte[] pixelBuffer) {
                double blue = 0; 
                double green = 0; 
                double red = 0; 
                for (int k = 0; k + 4 < pixelBuffer.Length; k += 4) { 
                    blue = pixelBuffer[k] * blueShade; 
                    green = pixelBuffer[k + 1] * greenShade; 
                    red = pixelBuffer[k + 2] * redShade; 
                    if (blue < 0) blue = 0; 
                    if (green < 0) green = 0;
                    if (red < 0) red = 0; 
                    pixelBuffer[k] = (byte)blue; 
                    pixelBuffer[k + 1] = (byte)green; 
                    pixelBuffer[k + 2] = (byte)red; 
                } 
            });
        } 

        public static Bitmap ColorFillBitamp(Bitmap source, string color) {
            Color c = ColorTranslator.FromHtml(color);
            return PixelTransformBitmap(source, delegate(byte[] pixelBuffer) {
                for (int k = 0; k + 4 < pixelBuffer.Length; k += 4) { 
                    pixelBuffer[k] = c.B; 
                    pixelBuffer[k + 1] = c.G; 
                    pixelBuffer[k + 2] = c.R; 
                } 
            });
        }
        
        public static Bitmap GreenscreenBitamp(Bitmap source, string color, int tolerance = 20) {
            Point selectPixel = new Point(0, 0);
            Color searchColor = source.GetPixel(selectPixel.X, selectPixel.Y);
            byte sb = searchColor.B;
            byte sg = searchColor.G;
            byte sr = searchColor.R;
            byte sa = searchColor.A;
            Color replacementColor = ColorTranslator.FromHtml(color);
            return PixelTransformBitmap(source, delegate(byte[] pixelBuffer) {
                for (int y = 0; y < source.Height; y++) {
                    for(int x = 0; x < source.Width; x++) {
                        var k = (y*(source.Width*4)) + (x*4); // pixel index
                        byte b = pixelBuffer[k];
                        byte g = pixelBuffer[k+1];
                        byte r = pixelBuffer[k+2];
                        byte a = pixelBuffer[k+3];
                        int dist = Math.Abs( (b - sb) + (g - sg) + (r - sr) + (a - sa) );
                        if (dist <= tolerance) {
                            pixelBuffer[k] = replacementColor.B;
                            pixelBuffer[k + 1] = replacementColor.G;
                            pixelBuffer[k + 2] = replacementColor.R;
                            pixelBuffer[k + 3] = replacementColor.A;
                        }
                    }
                }
            });
        }

        public static long GetPixelIndexAtCoordinate(int w, int x, int y) {
            return (y*(w*4)) + (x*4); // pixel index
        }
        
        public static Bitmap FloodFillBitamp(Bitmap source, string color, int tolerance = 20) {
            //TODO: WORK IN PROGRESS
            Point selectPixel = new Point(0, 0);
            Color searchColor = source.GetPixel(selectPixel.X, selectPixel.Y);
            byte sb = searchColor.B;
            byte sg = searchColor.G;
            byte sr = searchColor.R;
            byte sa = searchColor.A;
            Color replacementColor = ColorTranslator.FromHtml(color);
            return PixelTransformBitmap(source, delegate(byte[] pixelBuffer) {
                for (int y = 0; y < source.Height; y++) {
                    for(int x = 0; x < source.Width; x++) {
                        //TODO: check all neighboring pixels backwards from scanner (ie top & left), if it is the replacement color then also replace this...
                        long k = GetPixelIndexAtCoordinate(source.Width, x, y);
                        byte b = pixelBuffer[k];
                        byte g = pixelBuffer[k+1];
                        byte r = pixelBuffer[k+2];
                        byte a = pixelBuffer[k+3];
                        int dist = Math.Abs( (b - sb) + (g - sg) + (r - sr) + (a - sa) );
                        if (dist <= tolerance) {
                            pixelBuffer[k] = replacementColor.B;
                            pixelBuffer[k + 1] = replacementColor.G;
                            pixelBuffer[k + 2] = replacementColor.R;
                            pixelBuffer[k + 3] = replacementColor.A;
                        }
                    }
                }
            });
        }

        /* NOTE: Way too slow...
        public static Bitmap FloodFillBitamp(Bitmap source, string color) {
            //TODO: THis is slow because of get/setPixel, refactor to PixelTransformBitmap
            // From https://simpledevcode.wordpress.com/2015/12/29/flood-fill-algorithm-using-c-net/
            Point selectPixel = new Point(0, 0);
            Color replacementColor = ColorTranslator.FromHtml(color);
            Color targetColor = source.GetPixel(selectPixel.X, selectPixel.Y);
            if (targetColor.ToArgb().Equals(replacementColor.ToArgb()))
            {
                return source;
            }
 
            Stack<Point> pixels = new Stack<Point>();
             
            pixels.Push(selectPixel);
            while (pixels.Count != 0)
            {
                Point temp = pixels.Pop();
                int y1 = temp.Y;
                while (y1 >= 0 && source.GetPixel(temp.X, y1) == targetColor)
                {
                    y1--;
                }
                y1++;
                bool spanLeft = false;
                bool spanRight = false;
                while (y1 < source.Height && source.GetPixel(temp.X, y1) == targetColor)
                {
                    source.SetPixel(temp.X, y1, replacementColor);
 
                    if (!spanLeft && temp.X > 0 && source.GetPixel(temp.X - 1, y1) == targetColor)
                    {
                        pixels.Push(new Point(temp.X - 1, y1));
                        spanLeft = true;
                    }
                    else if(spanLeft && temp.X - 1 == 0 && source.GetPixel(temp.X - 1, y1) != targetColor)
                    {
                        spanLeft = false;
                    }
                    if (!spanRight && temp.X < source.Width - 1 && source.GetPixel(temp.X + 1, y1) == targetColor)
                    {
                        pixels.Push(new Point(temp.X + 1, y1));
                        spanRight = true;
                    }
                    else if (spanRight && temp.X < source.Width - 1 && source.GetPixel(temp.X + 1, y1) != targetColor)
                    {
                        spanRight = false;
                    } 
                    y1++;
                }
 
            }

            return source;
        }*/

        #endregion

        #region Helper Methods

        private static string _cleanParamter(string param) {
            if (string.IsNullOrEmpty(param)) return null;
            
            param = Core.Util.String.ReplaceUmlauts(param);
            param = Core.Util.String.RemoveDiacritics(param, true);
            param = param.Replace("/", "_");
            param = param.Replace("\\", "_");
            param = param.Replace(".", "_");
            return param;
        }

        #endregion

    }
}
