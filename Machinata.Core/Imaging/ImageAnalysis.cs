using Machinata.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Imaging {

    public class ImageAnalysis {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion
        
        public class ImageDiffResults {
            public Bitmap Bitmap;
            public int TotalPixels = 0;
            public int DifferentPixels = 0;
            public int DifferentColor = 0;
            public bool SizeIsSame = true;
        }

        public static ImageDiffResults GetImageDiff(Image a, Image b) {
            // Init
            ImageDiffResults results = new ImageDiffResults();
            var newSizeW = Math.Max(a.Width, b.Width);
            var newSizeH = Math.Max(a.Height, b.Height);
            DirectBitmap diff = new DirectBitmap(newSizeW, newSizeH);

            // Mark same size
            if (a.Width != b.Width) results.SizeIsSame = false;
            if (a.Height != b.Height) results.SizeIsSame = false;

            // Load direct access bitmaps for faster reads
            var da = new DirectBitmap(a.Width, a.Height);
            using (var g = Graphics.FromImage(da.Bitmap)) {
                g.DrawImage(a, 0, 0);
            }
            var db = new DirectBitmap(b.Width, b.Height);
            using (var g = Graphics.FromImage(db.Bitmap)) {
                g.DrawImage(b, 0, 0);
            }

            // Scan all pixels
            for (var y = 0; y < newSizeH; y++) {
                for (var x = 0; x < newSizeW; x++) {
                    // Bookkeeping
                    results.TotalPixels++;

                    // Get a/b color
                    Color pa = Color.Transparent;
                    Color pb = Color.Transparent;
                    if (x < da.Width && y < da.Height) pa = da.GetPixel(x, y);
                    if (x < db.Width && y < db.Height) pb = db.GetPixel(x, y);

                    // Calculate deltas
                    var deltaR = Math.Abs(pb.R - pa.R);
                    var deltaG = Math.Abs(pb.G - pa.G);
                    var deltaB = Math.Abs(pb.B - pa.B);
                    var deltaA = Math.Abs(pb.A - pa.A);
                    var delta = deltaR + deltaG + deltaB + deltaA;
                    results.DifferentColor += delta;

                    // Mark diff
                    if (delta > 0) {
                        results.DifferentPixels++;
                        diff.SetPixel(x, y, Color.FromArgb(255, 255, 0, 0));
                    } else {
                        diff.SetPixel(x, y, Color.Transparent);
                    }
                }
            }

            // Return results
            results.Bitmap = diff.Bitmap;
            return results;
        }

        public static ImageDiffResults GetImageDiff(string fileA, string fileB) {
            return GetImageDiff(Bitmap.FromFile(fileA), Bitmap.FromFile(fileB));
        }

        public static ImageDiffResults WriteImageDiffToFile(string fileA, string fileB, string fileOut, string format = "png") {
            var results = GetImageDiff(Bitmap.FromFile(fileA), Bitmap.FromFile(fileB));

            if (format.ToLower() == "jpg") format = "jpeg";
            foreach (ImageCodecInfo encoder in ImageCodecInfo.GetImageEncoders()) {
                if (encoder.FormatDescription.ToLower() == format.ToLower()) {
                    EncoderParameters parameters = new EncoderParameters(1);
                    parameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100);
                    results.Bitmap.Save(fileOut, encoder, parameters);
                    results.Bitmap = null; // we wrote it out to disk, release the handle here already
                    return results;
                }
            }
            throw new Exception($"No encoder found for {format}");
        }

        public static ImageDiffResults WriteImageDiffToStream(string fileA, string fileB, System.IO.Stream streamOut, string format = "png") {
            var results = GetImageDiff(Bitmap.FromFile(fileA), Bitmap.FromFile(fileB));

            if (format.ToLower() == "jpg") format = "jpeg";
            foreach (ImageCodecInfo encoder in ImageCodecInfo.GetImageEncoders()) {
                if (encoder.FormatDescription.ToLower() == format.ToLower()) {
                    EncoderParameters parameters = new EncoderParameters(1);
                    parameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100);
                    results.Bitmap.Save(streamOut, encoder, parameters);
                    results.Bitmap = null; // we wrote it out to disk, release the handle here already
                    return results;
                }
            }
            throw new Exception($"No encoder found for {format}");
        }

    }
}
