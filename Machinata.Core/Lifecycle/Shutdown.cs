using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Lifecycle {

    /// <summary>
    /// Specify this attribute on a static void method to be called when the application
    /// this signature is required: public static void c()
    /// shuts down.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    public class OnApplicationShutdownAttribute : Attribute {
    }

    public class Shutdown {



        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        /// <summary>
        /// Internal list of called shutdown routines to ensure that none are called twice.
        /// </summary>
        private static List<string> _routineCalled = new List<string>();

       

        /// <summary>
        /// Calls the lifecycle startup routine 'static void OnApplicationStartup()' for the type.
        /// </summary>
        /// <param name="type">The type.</param>
        private static void _callLifecycleRoutine(Type type) {
            if (_routineCalled.Contains(type.Name)) return;
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            _logger.Info(Core.Logging.LOG_SEPARATOR);
            _logger.Info("Initializing " + type.Name + "...");
            try {
                // Register
                _routineCalled.Add(type.Name);
                // Invoke 'static void OnApplicationStartup()'
                type.GetMethod("OnApplicationShutdown").Invoke(null, null);
            } catch (Exception e) {
                _logger.Error("Could not call OnApplicationShutdown for " + type.Name + ": " + e.Message);
                _logger.Error(e);
                if (e.InnerException != null) _logger.Error(e.InnerException);
            }
            stopWatch.Stop();
            _logger.Info("Initializing " + type.Name + " Done (" + stopWatch.ElapsedMilliseconds + " ms)");
        }

        public static void Run() {
            _logger.Info("Application Stopping...");

            // Now load the rest using reflection
            foreach (var type in Core.Reflection.Methods.GetMachinataMethodsWithAttribute(typeof(OnApplicationShutdownAttribute))
                      .Select(mi => mi.DeclaringType)) {
                _callLifecycleRoutine(type);
            }
                                
          
            _logger.Info("Application Stopped");
           
        }

    }
}
