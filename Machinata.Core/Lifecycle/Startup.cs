using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core.Lifecycle {

    /// <summary>
    /// Specify this attribute on a static void method to be called when the application
    /// this signature is required: public static void OnApplicationStartup()
    /// starts up.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    public class OnApplicationStartupAttribute : Attribute {
    }

    public class Startup {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        /// <summary>
        /// Internal list of called startup routines to ensure that none are called twice.
        /// </summary>
        private static List<string> _routineCalled = new List<string>();

        private static Dictionary<string,long> _routineStartupTime = new Dictionary<string,long>();

        /// <summary>
        /// Calls the lifecycle startup routine 'static void OnApplicationStartup()' for the type.
        /// </summary>
        /// <param name="type">The type.</param>
        private static void _callLifecycleRoutine(Type type) {
            if (_routineCalled.Contains(type.Name)) return;
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            _logger.Info(Core.Logging.LOG_SEPARATOR);
            _logger.Info("Initializing "+type.Name+"...");
            try {
                // Register
                _routineCalled.Add(type.Name);
                // Invoke 'static void OnApplicationStartup()'
                type.GetMethod("OnApplicationStartup").Invoke(null, null);
            }catch(Exception e) {
                _logger.Error("Could not call OnApplicationStartup for "+type.Name+": "+e.Message );
                _logger.Error(e);
                if(e.InnerException != null) _logger.Error(e.InnerException);
            }
            stopWatch.Stop();
            _logger.Info("Initializing "+type.Name+" Done ("+stopWatch.ElapsedMilliseconds+" ms)");
            _routineStartupTime.Add(type.Name, stopWatch.ElapsedMilliseconds);
        }

        public static void Run() {
            
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            // JSON default settings
            Core.JSON.LoadDefaultSettings();

            // Set invariant culture to prevent unwanted obscurities
            System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;

            // Enable all SSL Versions
            Core.Util.HTTP.EnableAllSSLVersions();

            // Maintenance mode?
            // Note: Maintenance mode settings (due to their low-level nature) must set in either Web.config or ISS
            if (ConfigurationManager.AppSettings["MaintenanceMode"] == "true") {
                Core.Config.MaintenanceMode = true;
                if(!string.IsNullOrEmpty(ConfigurationManager.AppSettings["MaintenanceDetails"])) {
                    Core.Config.MaintenanceDetails = ConfigurationManager.AppSettings["MaintenanceDetails"];
                }
                return;
            }

            // We first pre-initialize the logger as a failsafe, even before config is loaded...
            Core.Logging.CaptureStartupLogs();
            _logger.Info("Application Starting...");

            // Now load the config and then re-load the logger (since we now have the config)
            _callLifecycleRoutine(typeof(Core.Config));
            _callLifecycleRoutine(typeof(Core.Logging));

            // Now load DB and other vital startup services
            // Also load the handler since most rely on this being initialized...
            _callLifecycleRoutine(typeof(Core.Model.ModelContext));
            _callLifecycleRoutine(typeof(Core.Localization.Text)); // note: this no longer loads the db texts... this is in a speperate routine
            _callLifecycleRoutine(typeof(Core.Handler.Handler));
            //_callLifecycleRoutine(typeof(Core.TaskManager.TaskManager));

            // Now load the rest using reflection
            var USE_PARALLEL_LOADING_EXPERIMENTAL = true;
            if(USE_PARALLEL_LOADING_EXPERIMENTAL) {
                var tasks = new List<Task>();
                foreach (var type in Core.Reflection.Methods.GetMachinataMethodsWithAttribute(typeof(OnApplicationStartupAttribute)).Select(mi => mi.DeclaringType)) {
                    var task = Task.Factory.StartNew(() => {
                        _callLifecycleRoutine(type);
                    });
                    tasks.Add(task);
                }
                Task.WaitAll(tasks.ToArray());
            } else {
                foreach (var type in Core.Reflection.Methods.GetMachinataMethodsWithAttribute(typeof(OnApplicationStartupAttribute)).Select(mi => mi.DeclaringType)) {
                    _callLifecycleRoutine(type);
                }
            }
            

            // Stop timer
            stopWatch.Stop();

            // Save the startup logs
            foreach (var key in _routineStartupTime.Keys) {
                var ms = _routineStartupTime[key];
                var p = Math.Round(((double)ms / (double)stopWatch.ElapsedMilliseconds) * 100.0);
                _logger.Info("Startup Time "+key+": "+ms+" ms" + " ("+ p+"%)");
            }
            _logger.Info("Total Time: "+stopWatch.ElapsedMilliseconds+" ms");
            _logger.Info("Application Started");
            Core.Logging.RegisterStartupLogs();
        }

    }
}
