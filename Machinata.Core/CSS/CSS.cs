using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Core {
    public class CSS {

        public static string InlineCSSForHTMLDocument(string html, string css, string ignoreElements = null, bool removeComments = true, bool makeXMLValid = true) {
            var res = PreMailer.Net.PreMailer.MoveCssInline(
                html: html, 
                removeStyleElements: false, 
                ignoreElements: ignoreElements, 
                css: css, 
                stripIdAndClassAttributes: false, 
                removeComments: removeComments
            );
            var ret = res.Html;
            if(makeXMLValid) {
                ret = ret.Replace("<br>", "<br/>");
            }
            return ret;
        }

        public static string InlineCSSForHTMLSnippet(string html, string css, string ignoreElements = null, bool removeComments = true) {
            // This is a hack to avoid PreMailer from adding a html, head, and body 
            var st = "{begin-inline-elmenets}";
            var et = "{end-inline-elements}";
            var res = InlineCSSForHTMLDocument(st+html+et, css, ignoreElements, removeComments);
            var si = res.IndexOf(st) + st.Length;
            var ei = res.IndexOf(et);
            var trimmed = res.Substring(si, ei - si);
            return trimmed;
        }
    }
}
