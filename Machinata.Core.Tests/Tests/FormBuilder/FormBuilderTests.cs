using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using System.Reflection;
using System.Collections.Generic;

namespace Machinata.Core.Tests {
    [TestClass]
    public class FormBuilderTests {

      
        private class TestDefault : ModelObject {
            [FormBuilder("test")]
            public string Test { get; set; }

            [FormBuilder("test")]
            public string TestComplex { get; set; }

            [FormBuilder(Forms.Admin.WILDCARD)]
            public string TestAny { get; set; }
        }

        [TestMethod]
        public void Core_FormBuilder_DefaultFormname() {

            var entity = new TestDefault();
            var form = new FormBuilder("test");
            var properties = entity.GetPropertiesForForm(form);
            var results = new Dictionary<string, string>();
            foreach (var prop in properties) {
                var formProperty = new EntityFormBuilderProperty(entity, prop, form);
                results[prop.Name] = formProperty.GetFormName();
            }

            Assert.AreEqual(results["Test"], "test");
            Assert.AreEqual(results["TestComplex"], "testcomplex");
            Assert.IsFalse(results.ContainsKey("TestAny"));
        }

        private class TestOverrideObject : ModelObject {
            [FormBuilder("test")]
            public string Test { get; set; }

            [FormBuilder("test","Extra strange name")]
            public string TestOverride { get; set; }

            [FormBuilder("test")]
            public string TestComplex { get; set; }

            [FormBuilder(Forms.Admin.WILDCARD)]
            public string TestAny { get; set; }
        }

        [TestMethod]
        public void Core_FormBuilder_OverrideFormname() {

            var entity = new TestOverrideObject();
            var form = new FormBuilder("test");
            var properties = entity.GetPropertiesForForm(form);
            var results = new Dictionary<string, string>();
            foreach (var prop in properties) {
                var formProperty = new EntityFormBuilderProperty(entity, prop, form);
                results[prop.Name] = formProperty.GetFormName();
            }

            Assert.AreEqual(results["Test"], "test");
            Assert.AreEqual(results["TestComplex"], "testcomplex");
            Assert.AreEqual(results["TestOverride"], "extra strange name");
            Assert.IsFalse(results.ContainsKey("TestAny"));
        }



    }
}
