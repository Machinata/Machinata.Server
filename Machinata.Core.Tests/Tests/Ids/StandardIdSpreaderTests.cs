using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Util;
using System.Collections.Generic;

namespace Machinata.Core.Tests {

    [TestClass]
    public class StandardIdSpreaderTests {
        

        //[TestMethod]
        public void Core_Ids_StandardIdSpreader_ValidateEdgeCases() {
            //TODO
        }

        private void _Core_Ids_StandardIdSpreader_TestUniqueness(int start, int end) {
            var collected = new HashSet<int>();
            for(int id = start; id < end; id++) {
                var obfuscated = Core.Ids.Spreader.Default.SpreadId(id);
                Assert.IsFalse(collected.Contains(obfuscated));
                collected.Add(obfuscated);
                var deobfuscated = Core.Ids.Spreader.Default.UnspreadId(obfuscated);
                Assert.AreEqual(id, deobfuscated);
            }
        }
        
        [TestMethod]
        public void Core_Ids_StandardIdSpreader_TestUniqueness_100K() {
            _Core_Ids_StandardIdSpreader_TestUniqueness(1, 100000);
        }
        
        //[TestMethod]
        //NOTE: This test method needs TONS of memory
        public void Core_Ids_StandardIdSpreader_TestUniqueness_All() {
            _Core_Ids_StandardIdSpreader_TestUniqueness(1, int.MaxValue);
        }
        
        
        

    }
}
