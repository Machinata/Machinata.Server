using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Util;
using System.Text;
using System.Security.Cryptography;

namespace Machinata.Core.Tests {
    [TestClass] 
    public class APIAuthTests {  
        
        private const string API_KEY_ID = "8a1832eebc57409897b150211617c2e1882ba29ab00c4f718bf1f4ec5ff2924d24f82a6565ed4457a638f2486f55cc83aac3c59358a141318185181f3f1fcc21";
        private const string API_KEY_SECRET = "4040d52984d04e60a463504c3556d876ce656f6ef24c4efbb751c1e7fc0bc2cf2f225e988b5c4d8aab281ac53a212fdb7b688d249bde4c89b8fdf763b81eac38";
        
        [TestMethod]
        public void Core_API_APIAuth_Digest1() {
            var date = new DateTime(2018, 06, 14, 10, 55, 00, DateTimeKind.Utc);
            var timestamp = Core.Util.Time.GetUTCMillisecondsFromDateTime(date);
            {
                long expected = 1528973700000;
                long generated = timestamp;
                Assert.AreEqual(expected, generated);
            }
            {
                // With old encoding it was: t6DWEJIxxVo2Flrd7nPX4BhNFi4Qo5qa7PlL5L/5WF4p0OrveO1oCmKd+YxEMytw1UBd00hdj3lhNK8O6h4adg==
                string expected = "Y3a4fmjpBfPcKXGYTxfQ8NiSPlcGILB/AX2oAhkozAfM23vKrzzyqro5H0ZLo2xQwL0Y5mNn+yh1UyO5hlBY5A==";
                string generated = Core.API.APIAuth.GenerateDigest(API_KEY_ID, API_KEY_SECRET, "GET", "/sandbox/test", timestamp);
                Assert.AreEqual(expected, generated);
            }
        }

        [TestMethod]
        public void Core_API_APIAuth_Digest2() {
            var date = new DateTime(2021, 04, 07, 17, 02, 07, DateTimeKind.Utc);
            var timestamp = Core.Util.Time.GetUTCMillisecondsFromDateTime(date);
            {
                long expected = 1617814927000;
                long generated = timestamp;
                Assert.AreEqual(expected, generated);
            }
            {
                string expected = "eYBKV8Q0wJB3sjOcRiocGLo8G6yVJa2t/jSXsh8mf63gHlMyqgySmBMjsjjMEqiyLkrIy4V12iCaCkBKOGwOsQ==";
                string generated = Core.API.APIAuth.GenerateDigest("934a52ce77e94a72852fafbdb84c6b13c192d30a27dc4584ba81e7650b2df9af281b324aa3b84d14abc86ee5054265f1f617257bf49f4d5fa0488b02ec519662", "da6d32da80394ede994258498049955f356a4bd0d77349ef8461016859411c7540ad0581aca84e3d8ae9520856834ccada492f6192e04312a76f405575019e7a", "GET", "/api/test?debug=true", timestamp);
                Assert.AreEqual(expected, generated);
            }
        }

        [TestMethod]
        public void Core_API_APIAuth_AuthenticationHeader() {
            var date = new DateTime(2018, 06, 14, 10, 55, 00, DateTimeKind.Utc);
            var timestamp = Core.Util.Time.GetUTCMillisecondsFromDateTime(date);
            var expected = "machinata 8a1832eebc57409897b150211617c2e1882ba29ab00c4f718bf1f4ec5ff2924d24f82a6565ed4457a638f2486f55cc83aac3c59358a141318185181f3f1fcc21:1528973700000:Y3a4fmjpBfPcKXGYTxfQ8NiSPlcGILB/AX2oAhkozAfM23vKrzzyqro5H0ZLo2xQwL0Y5mNn+yh1UyO5hlBY5A==";
            var generated = Core.API.APIAuth.GenerateAuthenticationHeader(API_KEY_ID, API_KEY_SECRET, "GET", "/sandbox/test", timestamp);
            Assert.AreEqual(expected, generated);
        }

        [TestMethod]
        public void Core_API_APIAuth_HMACSHA512() {

            // Init
            var secret = "secret";
            var input = "input";
            var encoding = new UTF8Encoding();
            HMACSHA512 hmac = new HMACSHA512(encoding.GetBytes(secret));
            hmac.Initialize();

            // Test input encoding
            byte[] bytes = encoding.GetBytes(input);
            {
                string generated = string.Join(",",bytes); 
                string expected = "105,110,112,117,116";
                Assert.AreEqual(expected, generated);

            }

            // Test hash
            byte[] hashBytes = hmac.ComputeHash(bytes);
            {
                string generated = Convert.ToBase64String(hashBytes);
                string expected = "Ksle03F+BCxwZKX6fDGCMM022F4G+P+Dc9BMoX42Fingn0a38VH/OCo/SMWxkSFEbkXCWI8P8d6fdLBADa74Hw==";
                Assert.AreEqual(expected, generated);
            }

        }
    }
}
