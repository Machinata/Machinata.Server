using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Util;

namespace Machinata.Core.Tests {
    [TestClass]
    public class URLTests {

        [TestMethod]
        public void Core_Util_URL_RemoveQueryParameter() {
            {
                var url = "https://nerves.icasa.office.nerves.ch/admin/projects/project/report/sMWgLA/barry-callebaut/f-c-design-guidelines?timerange=2021-2022&business=nerves&access-key=lksajf982r";
                var expected = "https://nerves.icasa.office.nerves.ch/admin/projects/project/report/sMWgLA/barry-callebaut/f-c-design-guidelines?timerange=2021-2022&business=nerves";
                var converted = Core.Util.URL.RemoveQueryParameter(url, "access-key");
                Assert.AreEqual(converted, expected);
            }
            {
                var url = "https://nerves.icasa.office.nerves.ch/test";
                var expected = "https://nerves.icasa.office.nerves.ch/test";
                var converted = Core.Util.URL.RemoveQueryParameter(url, "access-key");
                Assert.AreEqual(converted, expected);
            }
            {
                var url = "https://nerves.icasa.office.nerves.ch/test?access-key=test";
                var expected = "https://nerves.icasa.office.nerves.ch/test";
                var converted = Core.Util.URL.RemoveQueryParameter(url, "access-key");
                Assert.AreEqual(converted, expected);
            }
        }

        [TestMethod]
        public void Core_Util_URL_GetQueryParameter() {
            {
                var url = "https://nerves.icasa.office.nerves.ch/admin/projects/project/report/sMWgLA/barry-callebaut/f-c-design-guidelines?timerange=2021-2022&business=nerves&access-key=lksajf982r";
                var expected = "lksajf982r";
                var results = Core.Util.URL.GetQueryParameter(url, "access-key");
                Assert.AreEqual(results, expected);
            }
            {
                var url = "https://nerves.icasa.office.nerves.ch/admin/projects/project/report/sMWgLA/barry-callebaut/f-c-design-guidelines?timerange=2021-2022&business=nerves&test2=asfasf";
                string expected = null;
                var results = Core.Util.URL.GetQueryParameter(url, "access-key");
                Assert.AreEqual(results, expected);
            }
        }

        [TestMethod]
        public void Core_Util_URL_SetQueryParameter() {
            {
                var url = "https://nerves.icasa.office.nerves.ch/admin/projects/project/report/sMWgLA/barry-callebaut/f-c-design-guidelines?timerange=2021-2022&business=nerves&access-key=lksajf982r";
                var expected = "https://nerves.icasa.office.nerves.ch/admin/projects/project/report/sMWgLA/barry-callebaut/f-c-design-guidelines?timerange=2021-2022&business=nerves&access-key=newvalue";
                var results = Core.Util.URL.SetQueryParameter(url, "access-key", "newvalue");
                Assert.AreEqual(results, expected);
            }
            {
                var url = "https://nerves.icasa.office.nerves.ch/";
                string expected = "https://nerves.icasa.office.nerves.ch/?access-key=newvalue";
                var results = Core.Util.URL.SetQueryParameter(url, "access-key", "newvalue");
                Assert.AreEqual(results, expected);
            }
        }

        [TestMethod]
        public void Core_Util_URL_ValidateURLWithAccessCodeForURL() {
            {
                var url = "https://nerves.icasa.office.nerves.ch/admin/projects/project/report/sMWgLA/barry-callebaut/f-c-design-guidelines?timerange=2021-2022&business=nerves";
                var urlWithAccessCode = Core.Util.URL.GetURLWithAccessCodeForURL(url);
                var results = Core.Util.URL.ValidateURLWithAccessCodeForURL(urlWithAccessCode);
                var expected = true;
                Assert.AreEqual(results, expected);
            }
            {
                var url = "https://nerves.icasa.office.nerves.ch/admin/projects/project/report/sMWgLA/barry-callebaut/f-c-design-guidelines?timerange=2021-2022&business=nerves";
                var urlWithAccessCode = Core.Util.URL.GetURLWithAccessCodeForURL(url);
                var results = Core.Util.URL.ValidateURLWithAccessCodeForURL(Core.Util.URL.SetQueryParameter(urlWithAccessCode,"access-code","fake"));
                var expected = false;
                Assert.AreEqual(results, expected);
            }
        }
    }
}
