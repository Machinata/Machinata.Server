using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Util;
using System.Collections.Generic;

namespace Machinata.Core.Tests {
    [TestClass]
    public class IPTests {

        [TestMethod]
        public void Core_Util_IP_IsInSameSubnet() {
            {
                Assert.IsTrue(Core.Util.IP.IsInSameSubnet("192.168.2.10", "192.168.2.30","255.255.255.0"));
                Assert.IsFalse(Core.Util.IP.IsInSameSubnet("192.168.2.10", "192.168.1.30","255.255.255.0"));
            }
            {
                Assert.IsTrue(Core.Util.IP.IsInSameSubnet("192.168.2.10", "192.168.1.30", "255.255.0.0"));
                Assert.IsFalse(Core.Util.IP.IsInSameSubnet("192.168.2.10", "192.161.1.30", "255.255.0.0"));
            }
        }

        [TestMethod]
        public void Core_Util_IP_DoesIPMatcheAnyAddressOrSubnetInList() {
            {
                var ipsOrSubnets = new List<string>() {
                    "165.225.94.0/23",
                    "165.225.201.0/24",
                    "136.226.202.0/23",
                    "217.111.139.241",
                    "192.168.0.1",
                };

                Assert.IsTrue(Core.Util.IP.DoesIPMatcheAnyAddressOrSubnetInList("136.226.202.81", ipsOrSubnets));
                Assert.IsTrue(Core.Util.IP.DoesIPMatcheAnyAddressOrSubnetInList("192.168.0.1", ipsOrSubnets));
                Assert.IsTrue(Core.Util.IP.DoesIPMatcheAnyAddressOrSubnetInList("165.225.201.55", ipsOrSubnets));
                
                Assert.IsFalse(Core.Util.IP.DoesIPMatcheAnyAddressOrSubnetInList("136.226.199.81", ipsOrSubnets));
                Assert.IsFalse(Core.Util.IP.DoesIPMatcheAnyAddressOrSubnetInList("192.168.0.2", ipsOrSubnets));
                Assert.IsFalse(Core.Util.IP.DoesIPMatcheAnyAddressOrSubnetInList("165.225.200.55", ipsOrSubnets));
            }
        }

    }
}
