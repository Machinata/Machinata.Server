using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinata.Core.Util;

namespace Machinata.Core.Tests {
    [TestClass]
    public class UserInputTests {
        

        [TestMethod]
        public void Core_Util_UserInput_SnapRoundToString() {
            Assert.AreEqual("+41791232299", Core.Util.UserInput.FormatPhoneNumberAsMSISDN("+41791232299", "ch"));
            Assert.AreEqual("+41791232299", Core.Util.UserInput.FormatPhoneNumberAsMSISDN("+41 79 123 2299", "ch"));
            Assert.AreEqual("+41791232299", Core.Util.UserInput.FormatPhoneNumberAsMSISDN("0791232299","ch"));
            Assert.AreEqual("+41791232299", Core.Util.UserInput.FormatPhoneNumberAsMSISDN("0791A232z299","ch"));
            Assert.AreEqual("+41791232299", Core.Util.UserInput.FormatPhoneNumberAsMSISDN("079 123 2299","ch"));
            Assert.AreEqual("+41791232299", Core.Util.UserInput.FormatPhoneNumberAsMSISDN("079-123-2299","ch"));
            Assert.AreEqual("+41791232299", Core.Util.UserInput.FormatPhoneNumberAsMSISDN("0041(079)-123-2299","ch"));
            Assert.AreEqual("+41791232299", Core.Util.UserInput.FormatPhoneNumberAsMSISDN("+41791232299","ch"));
            
            Assert.AreEqual("+491711234567", Core.Util.UserInput.FormatPhoneNumberAsMSISDN("+491711234567", "ch"));
            Assert.AreEqual("+491711234567", Core.Util.UserInput.FormatPhoneNumberAsMSISDN("+49 171 1234567", "ch"));
            Assert.AreEqual("+491711234567", Core.Util.UserInput.FormatPhoneNumberAsMSISDN("+49-171-1234567", "ch"));
            Assert.AreEqual("+491711234567", Core.Util.UserInput.FormatPhoneNumberAsMSISDN("0049-171-1234567", "ch"));
            Assert.AreEqual("+491711234567", Core.Util.UserInput.FormatPhoneNumberAsMSISDN("00491711234567", "ch"));
            
            Assert.AreEqual("+12035541234", Core.Util.UserInput.FormatPhoneNumberAsMSISDN("001 (203) 554-1234", "ch"));

            Assert.AreEqual("+41791232299", Core.Util.UserInput.FormatPhoneNumberAsMSISDN("791232299", "ch"));
            Assert.AreNotEqual("+417912322990", Core.Util.UserInput.FormatPhoneNumberAsMSISDN("7912322990", "ch")); // 79 start must be exactly 9 chars

        }


    }
}
