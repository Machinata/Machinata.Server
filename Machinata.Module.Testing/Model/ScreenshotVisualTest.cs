using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.IO;
using Machinata.Core.Builder;
using Machinata.Core.Data;
using Machinata.Core.Model;
using Machinata.Module.Headless;

namespace Machinata.Module.Testing.Model {

    public class ScreenshotVisualTest : VisualTest {

        private static DateTime? _lastScreenShot;

        public string URL { get; set; }
        public BrowserSize BrowserSize { get; set; }
        public bool Fullpage { get; set; }
        public string AwaitSelector { get; set; }

        protected override TestExecutionResult Run(ModelContext db) {

            var result = new TestExecutionResult();

            var fileName = Guid.NewGuid() + ".png";
            string screenshot = MakeScreenshot(fileName);

            var bytes = System.IO.File.ReadAllBytes(screenshot);
            var user = db.Users().SystemUser();
            var contentFile = DataCenter.UploadFile(fileName, bytes, "VisualTests", null, user);
            db.ContentFiles().Add(contentFile);
            db.SaveChanges();

            File.Delete(screenshot);

            result.DataResult = contentFile.ContentURL;

            return result;
        }

        private string MakeScreenshot(string fileName) {
            var directory = Core.Config.CachePath + @"screenshots" + Core.Config.PathSep;
            var url = this.URL;

            url = url.Replace("{server-url}", Core.Config.ServerURL);
            url = url.Replace("{public-url}", Core.Config.PublicURL);

            var path = directory + fileName;

            Directory.CreateDirectory(directory);


            var screenshot = NervesScreenshot.GetScreenshot(url, "png", path, this.BrowserSize.Width, this.BrowserSize.Height, this.AwaitSelector, this.Fullpage);

            _lastScreenShot = DateTime.UtcNow;
            return screenshot;
        }

        public override void WarmUp(ModelContext db) {
            base.WarmUp(db);
            try {

                if (_lastScreenShot == null || _lastScreenShot.Value.AddHours(1) < DateTime.UtcNow) {
                    // To warm up the screenshot server
                    var fileName = Guid.NewGuid() + ".png";
                    string screenshot = MakeScreenshot(fileName);
                }
            }
            catch { }
        }
    }

   
}