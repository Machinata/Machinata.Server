using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

using Machinata.Core.Builder;
using Machinata.Core.Model;

namespace Machinata.Module.Testing.Model {


    public abstract class VisualTest : Test {

        public static readonly BrowserSize Desktop = new BrowserSize() { Width =1000, Height = 1200};
        public static readonly BrowserSize Mobile = new BrowserSize() { Width = 400, Height = 900 };

    }

    public struct BrowserSize {
        public int Width { get; set; }
        public int Height { get; set; }

        public override string ToString() {
            return $"{this.Width}x{this.Height}";
        }
    }

   
}