
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core;
using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Module.Admin.Handler;
using Machinata.Module.Testing.Model;
using Machinata.Module.Testing.Logic;


namespace Machinata.Module.Testing.Handler {


    public class TestingAdminAPIHandler : CRUDAdminAPIHandler<TestResult> {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("testing");
        }

        #endregion

        #region  CRUD

        //[RequestHandler("/api/admin/config/settings/create")]
        //public void Create() {
        //    CRUDCreate();
        //}

        //[RequestHandler("/api/admin/config/setting/{publicId}/delete")]
        //public void Delete(string publicId) {
        //    CRUDDelete(publicId);
        //}
        
        [RequestHandler("/api/admin/testing/result/{publicId}/edit")]
        public void Edit(string publicId) {

            var form = TestingAdminHandler.GetEditForm();
            CRUDEdit(publicId, form);
           
        }



        [RequestHandler("/api/admin/testing/result/{publicId}/accept")]
        public void Accept(string publicId) {

            var testResult = this.DB.TestResults().GetByPublicId(publicId);

            var revisionAccteptedBefore = ResultLoader.LoadTestResultsByRevision(this.DB).FirstOrDefault(r => r.BuildGUID == testResult.BuildGUID)?.IsAccepted;
            testResult.Accepted = true;

            var revisionResults = ResultLoader.LoadTestResultsByRevision(this.DB).FirstOrDefault(r => r.BuildGUID == testResult.BuildGUID);
            if (revisionResults?.IsAccepted == true && revisionAccteptedBefore == false) {
                TestExecution.SendNewAcceptedVersionEmail(this.DB, testResult.BuildGUID);
            }

            this.DB.SaveChanges();
            this.SendAPIMessage("success");

        }
        #endregion

        [RequestHandler("/api/admin/testing/version/{buildGUID}/accept")]
        public void AcceptVersion(string buildGUID) {

            var accepted = this.Params.Bool("accepted", true);

            var testResults = this.DB.TestResults().Where(tr => tr.BuildGUID == buildGUID);
            foreach(var testResult in testResults) {
                testResult.Accepted = accepted;
            }

            this.DB.SaveChanges();

            TestExecution.SendNewAcceptedVersionEmail(this.DB, buildGUID);

            this.SendAPIMessage("success");

        }


        [RequestHandler("/api/admin/testing/version/{buildGUID}/delete")]
        public void DeleteVersion(string buildGUID) {

            var testResults = this.DB.TestResults().Where(tr => tr.BuildGUID == buildGUID).ToList();
            foreach (var testResult in testResults) {
                this.DB.DeleteEntity(testResult);
            }

            this.DB.SaveChanges();

            this.SendAPIMessage("success");

        }

        [RequestHandler("/api/admin/testing/version/{buildGUID}/send-summary")]
        public void SendSummary(string buildGUID) {


            var simulateTaskEmail = this.Params.Bool("simulate-task", false);

            var results =  ResultLoader.LoadTestResultByRevision(this.DB, buildGUID);


            if (simulateTaskEmail == false) {
                TestExecution.SendNotificationEmail(this.DB, results.TestResults, buildGUID, "Test Result Summary: " + results.BuildVersion);
            } else {

                TestExecution.SendNotificationEmail(this.DB, results.TestResults, buildGUID, "Test Result Summary: " + results.BuildVersion,true,true);
            }

            this.SendAPIMessage("success");

        }


    }
}
