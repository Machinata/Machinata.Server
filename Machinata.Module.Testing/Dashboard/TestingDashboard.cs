﻿using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Module.Admin.Dashboard;
using Machinata.Module.Reporting.Model;
using Machinata.Module.Testing.Logic;
using Machinata.Module.Testing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Testing.Dashboard {
    class TestingDashboard {

        [DashboardLoader]
        public static void LoadDashboard(DashboardLoader loader) {

            var section = new SectionNode();
            section.SetTitle("Testing");
            section.Style = "W_H2";


            // Metric Status Last Result
            {
                var status = ResultLoader.GetTestingStatus(loader.DB);

                var metrics = new MetricsNode();
                metrics.Screen = false;
                metrics.SetTitle("Last Test Result");
                metrics.Id = "testing-metrics";
                metrics.Style = "Growing";
                {
                    var group = metrics.AddGroup();
                    group.CreateItem().SetLabel("Build Version").SetValue(status.BuildVersion);
                    group.CreateItem().SetLabel("Build Date").SetValue(status.BuildDate);
                    group.CreateItem().SetLabel("Tested Build Date").SetValue(status.LastTestBuildDate.HasValue ? status.LastTestBuildDate.Value.ToDefaultTimezoneDateTimeString() : "na");
                    group.CreateItem().SetLabel("").SetValue("");
                }
                {
                    var group = metrics.AddGroup();
                    group.CreateItem().SetLabel("Tests Results Count").SetValue(status.LastTestCount.ToString());
                    group.CreateItem().SetLabel("Accepted").SetValue(status.LastTestAccepted.ToString());
                    group.CreateItem().SetLabel("Open Tests").SetValue(status.MissingTests.ToString());
                    group.CreateItem().SetLabel("Failed Tests").SetValue(status.FailedTests.ToString());
                }
                metrics.AddTo(section);

            }
            {

                var results = ResultLoader.LoadTestResultsByRevision(loader.DB).OrderBy(tr => tr.BuildDate).Take(20);
                var vbarnode = new VBarNode();
                vbarnode.Theme = "rating";
                vbarnode.SetTitle("Results");
                vbarnode.SetSubTitle("over time");

                vbarnode.StackedSeries = true;
                vbarnode.Series = new List<CategorySerie>();


                //var result = new Dictionary<DateTime, IEnumerable<Vote>>();

                DateTime minDate = DateTime.UtcNow;
                DateTime maxDate = DateTime.UtcNow;

                if (results.Any()) {
                    minDate = results.Where(v => v.BuildDate.HasValue).Select(v => v.BuildDate.Value).Min();
                    maxDate = results.Where(v => v.BuildDate.HasValue).Select(v => v.BuildDate.Value).Max();
                }

                var minDateLocalDate = Core.Util.Time.ConvertToDefaultTimezone(minDate).Date;

                var sameSerie = new CategorySerie();

                sameSerie.Id = nameof(sameSerie);
                sameSerie.Title = "Same";
                sameSerie.ColorShade = "positive2";
                sameSerie.YAxis = new Axis();
                sameSerie.YAxis.Format = "1f";
                vbarnode.Series.Add(sameSerie);


                var changeSerie = new CategorySerie();

                changeSerie.Id = nameof(changeSerie);
                changeSerie.Title = "Changed";
                changeSerie.ColorShade = "neutral";
                changeSerie.YAxis = new Axis();
                changeSerie.YAxis.Format = "1f";
                vbarnode.Series.Add(changeSerie);


                var errorSerie = new CategorySerie();

                errorSerie.Id = nameof(errorSerie);
                errorSerie.Title = "Error";
                errorSerie.ColorShade = "negative2"; // "dark";
                errorSerie.YAxis = new Axis();
                errorSerie.YAxis.Format = "1f";
                vbarnode.Series.Add(errorSerie);


                foreach (var result in results) {


                    // Same
                    {
                        var res = result.TestResults.Count(t => t.ResultStatus == ResultStatuses.Same);
                        var fact = new CategoryFact(res, res.ToString("0.##"), result.BuildDate?.ToDateTimeString());
                        sameSerie.Facts.Add(fact);
                    }

                    // Changed
                    {
                        var res = result.TestResults.Count(t => t.ResultStatus == ResultStatuses.Changed);
                        var fact = new CategoryFact(res, res.ToString("0.##"), result.BuildDate?.ToDateTimeString());
                        changeSerie.Facts.Add(fact);
                    }

                    // Error
                    {
                        var res = result.TestResults.Count(t => t.ResultStatus == ResultStatuses.Error);
                        var fact = new CategoryFact(res, res.ToString("0.##"), result.BuildDate?.ToDateTimeString());
                        errorSerie.Facts.Add(fact);
                    }

                }

                section.AddChild(vbarnode);
            }


            loader.AddItem(section, "/admin/testing");


        }

    }
}
