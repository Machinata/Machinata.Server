﻿using Machinata.Core.Data;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Module.Testing.Logic;
using Machinata.Module.Testing.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Testing.Tasks {
    public class TestExecutor : Machinata.Core.TaskManager.Task {
        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = false;
            config.Install = true;
            config.Interval = "1d";
            return config;
        }

        public override void Process() {

            // Pause to let the system startup if not everything is loaded entirely
            // especially the page template cache
            System.Threading.Thread.Sleep(2000);

            RunTests();

            RunCleanup();
        }

        private void RunCleanup() {
            Log("Cleaning up old test results...");

            var testResultsGroupedToDelete = ResultLoader.LoadTestResultsByRevision(this.DB);
            var allGroups = testResultsGroupedToDelete.ToList();

            var allTests = testResultsGroupedToDelete.Count();
            var buildInfo = Core.Util.Build.GetProductBuildInfo();
            var availableTests = Logic.TestLoader.LoadAllTests(this.DB);
            var lastAcceptedTests = new List<int>();
            var minKeepDate = DateTime.UtcNow.AddMonths(-3);

            // Keep last 10
            {
                testResultsGroupedToDelete = testResultsGroupedToDelete.Skip(10);
            }

            // Keep last accepted
            {
                var lastAcceptedGroup = testResultsGroupedToDelete.FirstOrDefault(tr => tr.IsAccepted == true);
                if (lastAcceptedGroup != null) {
                    testResultsGroupedToDelete = testResultsGroupedToDelete.Where(g => g != lastAcceptedGroup);
                }
            }

            // Keep newer 3 months
            {
                testResultsGroupedToDelete = testResultsGroupedToDelete.Where(tr => tr.BuildDate < minKeepDate);
            }

            // Log the test results we keep
            {
                var notDeletingGroups = allGroups.Except(testResultsGroupedToDelete);
                Log($"NOT DELETING {notDeletingGroups.Count()} old TestResults Groups");
                foreach (var testResultGroup in notDeletingGroups.ToList()) {
                    Log($"\tNot deleting Results for BUILD : {testResultGroup.BuildDate} - {testResultGroup.BuildGUID} - {testResultGroup.BuildMachine} - {testResultGroup.BuildUser} - Accepted={testResultGroup.Accepted}:");
                }
            }

            // Do delete
            {
                var toDelete = testResultsGroupedToDelete.Count();

                Log($"DELETING {toDelete} old TestResults Groups");

                foreach (var testResultGroup in testResultsGroupedToDelete.ToList()) {
                    Log($"\tDeleting Results for BUILD : {testResultGroup.BuildDate} - {testResultGroup.BuildGUID} - {testResultGroup.BuildMachine} - {testResultGroup.BuildUser} - Accepted={testResultGroup.Accepted}:");

                    foreach (var testResult in testResultGroup.TestResults) {
                        Log($"\t\tDeleting test result : {testResult.Name} - {testResult.Category} - {testResult.SubCategory} - {testResult.Id} - Accepted={testResult.Accepted} - Created={testResult.Created}");
                        this.DB.DeleteEntity(testResult);
                        this.DB.SaveChanges();
                    }
                   
                }
            }
        }

        private void RunTests() {
            Log("Loading tests...");

            var user = this.DB.Users().SystemUser();
            var buildInfo = Core.Util.Build.GetProductBuildInfo();
            var currentBuildVersion = Core.Config.BuildVersion;
            var currentBuildGUID = buildInfo.BuildGUID;
            var currentBuildDate = Core.Util.Time.ConvertToUTCTimezone(Core.Config.BuildTimestamp);

            Log("Current version: " + currentBuildVersion);
            Log("Current build date: " + currentBuildDate);

            var missingTests = new List<Test>();
            var completedTests = new List<TestResult>();
            var availableTests = Logic.TestLoader.LoadAllTests(this.DB).ToList();
            var buildTestResults = this.DB.TestResults().Where(tr => tr.BuildGUID == currentBuildGUID).ToList();


            // Find missing test for this version
            foreach (var availableTest in availableTests) {
                var result = buildTestResults.FirstOrDefault(tr => tr.TestID == availableTest.Id && tr.Category == availableTest.Category && tr.SubCategory == availableTest.SubCategory);
                if (result == null) {
                    missingTests.Add(availableTest);
                }
            }

            Log($"Found {missingTests.Count()} unexecuted tests...");

            // Process Test
            var i = 0;
            foreach (var test in missingTests) {
                i++;
                ProcessTest(user, currentBuildVersion, currentBuildGUID, currentBuildDate, completedTests, test);
                RegisterProgress(i, missingTests.Count());
            }


            // Notification Test Results
            if (missingTests.Any()) {
                Log($"Sending Test Result Summary");
                TestExecution.SendNotificationEmail(this.DB, completedTests, currentBuildGUID, "Test Result Summary: " + currentBuildVersion, ignoreAccepted: true, successMessage: true);
            }

            // Notification new Accepted Version
            if (missingTests.Any()) {
                if (completedTests.All(tr=>tr.Accepted == true)) {
                    Log($"Sending new accepted version email");
                    TestExecution.SendNewAcceptedVersionEmail(this.DB,  currentBuildGUID);
                }
                
               
            }
        }


        private void ProcessTest(User user, string currentBuildVersion, string currentBuildGUID, DateTime currentBuildDate, List<TestResult> completedTests,  Test test) {
            Log($"\tRunning test {completedTests.Count}: {test.Name} - {test.Category} - {test.SubCategory} - {test.Id}");
            TestResult result = TestExecution.IntitializeTestResult(currentBuildVersion, currentBuildGUID, currentBuildDate, test);

            var startTime = DateTime.UtcNow;

            try {

                test.WarmUp(this.DB);
                var executionResult = test.RunTest(this.DB);

                result.Status = TestStatuses.Success;
                result.DataResult = executionResult.DataResult;
                result.BodyResult = executionResult.BodyResult;

                // Diff & AutoAccept
                var lastAccepted = ResultLoader.GetLastAcceptedVersion(this.DB, currentBuildDate, result.TestID, result.Category, result.SubCategory);
                if (lastAccepted != null) {
                    TestExecution.CompareAndAutoAccept(this.DB, user, result, lastAccepted, test);
                }

              

            } catch (Exception e) {
                // TODO: Expected Exception? if we want an exception to happen

                var exceptionResult = ErrorHandler.WrapException(this.Context, e);
                result.ErrorResult = exceptionResult.StackTrace;
                result.Properties[TestResult.PROPERTIES_EXCEPTION_TYPE] = exceptionResult.Type;
                result.Status = TestStatuses.Error;
                result.ResultStatus = ResultStatuses.Error;
            } finally {
                var endTime = DateTime.UtcNow;
                result.Started = startTime;
                result.Finished = endTime;
                completedTests.Add(result);
                this.DB.TestResults().Add(result);
                this.DB.SaveChanges();
               
            }

        }

    }
}
