﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_nodes_.paragraphnode.html
/// </summary>
namespace Machinata.Module.Reporting.Model {

    [Serializable]
    public class ParagraphNode : ThemeableNode {

        public ParagraphNode(string title, string subtitle) {
            this.MainTitle = title;
            this.SubTitle = subtitle;
        }


        [JsonProperty("content", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Content { get; set; } = null;
    }

   
}
