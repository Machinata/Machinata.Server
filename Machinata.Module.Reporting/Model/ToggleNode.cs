﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    /// </summary>
    [Serializable]
    public class ToggleNode : Node {

        public ToggleNode() {
            this.ToggleTitles = new List<TranslatableString>();
        }

        [JsonProperty("toggleTitles")]
        public List<TranslatableString> ToggleTitles { get; set; }

        [JsonProperty("tabSelection")]
        public bool TabSelection { get; set; } = true;

        [JsonProperty("listSelection")]
        public bool ListSelection { get; set; } = false;

        [JsonProperty("menuSelection")]
        public bool MenuSelection { get; set; } = false;





    }

}
