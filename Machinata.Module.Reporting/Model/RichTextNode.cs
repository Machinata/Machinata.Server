﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Machinata.Module.Reporting.Model {

    [Serializable]
    public class RichTextNode : Node {

        public RichTextNode() {
            
        }

        [JsonProperty("html")]
        public TranslatableString HTML { get; set; }
    }

}
