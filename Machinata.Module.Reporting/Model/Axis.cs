﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    [Serializable]
    public class Axis {

        [JsonProperty("orient", NullValueHandling = NullValueHandling.Ignore)]
        public string Orient { get; set; }

        [JsonProperty("format", NullValueHandling = NullValueHandling.Ignore)]
        public string Format { get; set; }

        [JsonProperty("formatType", NullValueHandling = NullValueHandling.Ignore)]
        public string FormatType { get; set; }

        [JsonProperty("values", NullValueHandling = NullValueHandling.Ignore)]
        public List<decimal> Values { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Title { get; set; }

        [JsonProperty("minValue", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? MinValue { get; set; } = null;

        [JsonProperty("maxValue", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? MaxValue { get; set; } = null;

        [JsonProperty("tickCount", NullValueHandling = NullValueHandling.Ignore)]
        public int? TickCount { get; set; } = null;


       

        /*
        "orient": "left",
                "format": ".2%",
                "formatType": "number"
        */

    }
}
