﻿using Machinata.Core.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    /// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_tables_.dimensionelement.html
    /// </summary>
    /// 
    [Serializable]
    public class DimensionElement {

        public DimensionElement() {

        }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public TranslatableString Name { get; set; }



        [JsonProperty("expanded", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Expanded { get; set; } = null;

        [JsonProperty("facts", NullValueHandling = NullValueHandling.Ignore)]
        public List<TableFact> Facts { get; set; }

        [JsonProperty("importance")]
        public int Importance { get; set; }

        [JsonIgnore()]
        public ValueTypes? ValueTypeEnum { get; set; }

        [JsonProperty("valueType", NullValueHandling = NullValueHandling.Ignore)]
        public string ValueType { get { return ValueTypesHelper.ValueTypeMapping(this.ValueTypeEnum); } }

        [JsonProperty("colorShade", NullValueHandling = NullValueHandling.Ignore)]
        public string ColorShade { get; set; } = null;

        [JsonProperty("display", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Display { get; set; } = null;

        [JsonProperty("childRows", NullValueHandling = NullValueHandling.Ignore)]
        public List<DimensionElement> ChildRows { get; set; }

        public DimensionElement SetImportance(int imp) {
            this.Importance = imp;
            return this;
        }

        public DimensionElement AddRowValue(object value, string resolved = null, string alertHint = null, string alertIcon = null, string columnId = null) {
            var fact = new TableFact();
            fact.Val = value;

            // Auto column ids
            if (columnId == null) {
                fact.Col = "M" + (this.Facts.Count + 1);
            } else {
                fact.Col = columnId;
            }
            fact.Resolved = resolved ?? value?.ToString();
            if (alertHint != null || alertIcon != null) fact.SetAlert(alertHint, alertIcon);
            this.Facts.Add(fact);
            this.ValueTypeEnum = ValueTypes.Amount;
            return this;
        }

        public DimensionElement AddRowValueInfo(object value, string infoText, string infoTitle = "Info", string resolved = null, bool? wideDialog = null) {
            var fact = new TableFact();
            fact.Val = value;
            fact.Col = "M" + (this.Facts.Count + 1);
            fact.Resolved = resolved ?? value.ToString();
            //if (alertHint != null || alertIcon != null) fact.SetAlert(alertHint, alertIcon);

            if (infoText != null) {
                fact.Info = new NodeInfo();
                fact.Info.Title = infoTitle;
                fact.Info.Text = infoText;
                fact.Info.WideDialog = wideDialog;
            }


            this.Facts.Add(fact);
            this.ValueTypeEnum = ValueTypes.Amount;
            return this;
        }

        public DimensionElement AddRowTagFact(string tag, string hint, string icon, string colorShade, object value = null, string infoText = null, string resolvedValue = null, ValueTypes valueType = ValueTypes.Text, int? sort = null) {
            var fact = new TableFact();
            fact.Val = value;
            fact.Col = "M" + (this.Facts.Count + 1);
            fact.SetTag(tag, hint, icon, colorShade);
            fact.Resolved = resolvedValue;
            fact.Sort = sort;

            if (infoText != null) {
                fact.Info = new NodeInfo();
                fact.Info.Text = infoText;
            }

            this.Facts.Add(fact);
            this.ValueTypeEnum = valueType;
            return this;
        }

        public DimensionElement AddTextRowValue(string text, string alertHint = null , string alertIcon = null) {
            var fact = new TableFact();
            fact.Val = text;
            fact.Col = "M" + (this.Facts.Count + 1);
            fact.Resolved = text;
            if (alertHint != null || alertIcon != null) {
                fact.SetAlert(alertHint, alertIcon);
            }

            this.Facts.Add(fact);
            this.ValueTypeEnum = ValueTypes.Text;
            return this;
        }

        public DimensionElement AddTextRowValue(object obj) {
            return AddTextRowValue(obj.ToString());
        }

        public DimensionElement AddRandomIntRowValue(int min= 0, int max=1000, Random rnd = null) {
            if (rnd == null) {
                rnd = new Random();
            }
            var fact = new TableFact();
            fact.Val = rnd.Next(min, max);
            fact.Col = "M" + (this.Facts.Count + 1);
            fact.Resolved = fact.Val.ToString();
            this.Facts.Add(fact);
            this.ValueTypeEnum = ValueTypes.PAbs;
            return this;
        }

        public DimensionElement AddRandomPercentageRelativeRowValue(double min, double max, Random rnd) {
            if (rnd == null) {
                rnd = new Random();
            }
            var fact = new TableFact();
            fact.Format = ".0%";
            var val = Core.Util.Math.RandomDouble(min,max);
            fact.Val = val;
            fact.Col = "M" + (this.Facts.Count + 1);
            fact.Resolved = val.ToString("#0.##%");
            this.Facts.Add(fact);
            this.ValueTypeEnum = ValueTypes.PAbs;
            return this;
        }

        public DimensionElement AddRandomPercentageRowValue(double min, double max, Random rnd) {
            if (rnd == null) {
                rnd = new Random();
            }
            var fact = new TableFact();
            fact.Format = ".0%";
            var val = Core.Util.Math.RandomDouble(min, max);
            fact.Val = val;
            fact.Col = "M" + (this.Facts.Count + 1);
            fact.Resolved = val.ToString("#0.##%");
            this.Facts.Add(fact);
            this.ValueTypeEnum = ValueTypes.PAbs;
            return this;
        }

        public DimensionElement AddRowValue(object value, ValueTypes valueType) {
            var fact = new TableFact();
            fact.Format = ".0%";
            var val = value;
            fact.Val = val;
            fact.Col = "M" + (this.Facts.Count + 1);

            if (value is double) {
                fact.Resolved = ((double)value).ToString("#0.##%");
            } else {
                fact.Resolved = value.ToString();
            }


            this.Facts.Add(fact);
            this.ValueTypeEnum = valueType;
            return this;
        }

        public DimensionElement AddPercentageRowValue(decimal val, string color = null, string alertHint = null , string alertIcon = null) {
         
            var fact = new TableFact();
            fact.Format = ".0%";
            fact.Val = val;
            fact.Col = "M" + (this.Facts.Count + 1);
            fact.Color = color;
            fact.Resolved = val.ToString("#0.##%");

            if (alertHint != null || alertIcon !=null) {
                fact.SetAlert(alertHint, alertIcon);
            }

            this.Facts.Add(fact);
            this.ValueTypeEnum = ValueTypes.PAbs;

            return this;
        }

        public DimensionElement AddPercentageRowValue(decimal? val) {
            if (val.HasValue) {
                AddPercentageRowValue(val.Value);
            } else {
                AddRowValue(string.Empty);
            }

            return this;
        }

        public DimensionElement AddChildRow(string id, string name, string label ="Label", int importance = 2) {
            var childRow = new DimensionElement();
            childRow.ValueTypeEnum = null;
            childRow.Importance = importance;
            childRow.Id = id;
            childRow.ChildRows = new List<DimensionElement>();
            childRow.Name = new TranslatableString(name, label);
            childRow.Facts = new List<TableFact>();

            if (this.ChildRows == null) {
                this.ChildRows = new List<DimensionElement>();
            }
            this.ChildRows.Add(childRow);
            return childRow;
        }
    }


}
