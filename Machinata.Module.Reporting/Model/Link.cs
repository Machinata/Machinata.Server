﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {



    [Serializable]
    public class Link {

        public Link(string tooltip = null) {
            this.Tooltip = tooltip;
        }

        public Link(string url, string tooltip = null) {
            this.Tooltip = tooltip;
            this.URL = url;
        }

        [JsonProperty("tooltip", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Tooltip { get; set; }

        [JsonProperty("chapterId", NullValueHandling = NullValueHandling.Ignore)]
        public string ChapterId { get; set; }

        [JsonProperty("nodeId", NullValueHandling = NullValueHandling.Ignore)]
        public string NodeId { get; set; }

        [JsonProperty("target", NullValueHandling = NullValueHandling.Ignore)]
        public string Target { get; set; }

        [JsonProperty("url", NullValueHandling = NullValueHandling.Ignore)]
        public string URL { get; set; }

        [JsonProperty("drilldown", NullValueHandling = NullValueHandling.Ignore)]
        public Drilldown Drilldown { get; set; }



    }


}
