﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    /// GraphFact
    /// </summary>
    /// 
    [Serializable]
    public class Fact {

        [JsonProperty("x")]
        public decimal? X { get; set; }

        [JsonProperty("y")]
        public decimal? Y { get; set; }

        [JsonProperty("z", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Z { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Title { get; set; } = null;


    }
}
