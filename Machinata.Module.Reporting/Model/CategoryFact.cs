﻿using Newtonsoft.Json;
using System;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    /// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_charts_.categoryfact.html
    /// </summary>
    [Serializable]
    public class CategoryFact {

        public CategoryFact() {
        }
        public CategoryFact(decimal val, string resolvedValue, string category, string formatType = null, string format = null, string formattingHint = null) {
            this.Value = val;
            this.Resolved = resolvedValue;
            this.Category = category;
            this.FormatType = formatType;
            this.Format = format;
            this.FormattingHint = formattingHint;
        }

        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("val", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Value { get; set; }

        // Can be 0.0 - 1.0 (usually opacity)
        [JsonProperty("strength", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Strength { get; set; }

        [JsonProperty("resolved", NullValueHandling = NullValueHandling.Ignore)]
        public string Resolved { get; set; }

        [JsonProperty("category", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Category { get; set; }

        [JsonProperty("formatType", NullValueHandling = NullValueHandling.Ignore)]
        public string FormatType { get;  set; }

        [JsonProperty("format", NullValueHandling = NullValueHandling.Ignore)]
        public string Format { get; set; }

        [JsonProperty("formattingHint", NullValueHandling = NullValueHandling.Ignore)]
        public string FormattingHint { get; set; }

        [JsonProperty("mapKey", NullValueHandling = NullValueHandling.Ignore)]
        public string MapKey { get; set; }

        [JsonProperty("factStyle", NullValueHandling = NullValueHandling.Ignore)]
        public string FactStyle { get; set; }

        [JsonProperty("colorShade", NullValueHandling = NullValueHandling.Ignore)]
        public string ColorShade { get; set; }

        // Experimental
        [JsonProperty("label", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Label { get; set; }

        // Experimental
        [JsonProperty("tooltip", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Tooltip { get; set; }

        [JsonProperty("symbolType", NullValueHandling = NullValueHandling.Ignore)]
        public string SymbolType {
            get {
                return _symbolType?.ToString()?.ToLowerInvariant();
            }
        }


        [JsonIgnore()]
        public ValueTypes? ValueTypeEnum { get; set; }

        [JsonProperty("valueType", NullValueHandling = NullValueHandling.Ignore)]
        public string ValueType { get { return ValueTypesHelper.ValueTypeMapping(this.ValueTypeEnum); } }

        [JsonProperty("link", NullValueHandling = NullValueHandling.Ignore)]
        public Link Link { get; set; } = null;


        /// <summary>
        /// For newtonsoft serizalization reasons, because we can't influence the serialization otherwise
        /// </summary>
        /// <param name="type"></param>
        public void SetSymbolType (SymbolTypes type) {
            _symbolType = type;
        }

        [JsonIgnore]
        private SymbolTypes? _symbolType;
    }

    // Where should this actually be placed?
    public enum SymbolTypes {
       
        Dot,
      
        Line,
        
        Max,
       
        Min
    }
}
