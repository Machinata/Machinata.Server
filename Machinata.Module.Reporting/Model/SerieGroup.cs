﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {


    /// <summary>
    /// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_charts_.graphseriesgroup.html
    /// </summary>
    /// 
    [Serializable]
    public class SerieGroup {

        public SerieGroup() {
            this.XAxis = new Axis();
            this.XAxis.Format = "%b %y";
            this.XAxis.FormatType = "time";

            this.YAxis = new Axis();
            this.YAxis.Orient = "left";
            this.YAxis.Format = ".1%";
            this.YAxis.FormatType = "number";

            this.Series = new List<Serie>();
        }

        /*
        "id": "lines",
        "xAxis": {
                "format": "%d.%m.%y",
                "formatType": "time"
            },
            "yAxis": {
                "orient": "left",
                "format": ".2%",
                "formatType": "number"
            },
        */

        [JsonProperty("xAxis")]
        public Axis XAxis { get; set; }

        [JsonProperty("yAxis")]
        public Axis YAxis { get; set; }

        [JsonProperty("series")]
        public List<Serie> Series { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }


        public Serie AddSerie(string chartType, string title) {
            var serie = new Serie();
            serie.ChartType = chartType;
            serie.Title = title;
            serie.Id = "serie" + this.Series.Count;
            serie.ColorShade = "dark";
            this.Series.Add(serie);
            return serie;
        }

        public Serie AddColumnSerie(  Serie serie) {
           
            serie.ChartType = "column";
           // serie.Title = title;
            serie.Id = "serie" + this.Series.Count;
           // serie.ColorShade = "dark";
            this.Series.Add(serie);
            return serie;
        }

        public Serie AddLineSerie(string title, Serie serie) {

            //serie.ChartType = "line";
            //serie.Title = title;
            //serie.Id = "serie" + this.Series.Count;
            //this.Series.Add(serie);
            //return serie;

            this.AddLineSerie(serie);
            serie.Title = title;
            return serie;

        }

        public Serie AddLineSerie( Serie serie) {
            serie.ChartType = "line";
            serie.Id = "serie" + this.Series.Count;
            this.Series.Add(serie);
            return serie;
        }


        public SerieGroup XAxisQuarters() {
            this.XAxis.Format = Config.QuarterFormatD3;
            this.XAxis.FormatType = "time";
            return this;
        }

        public SerieGroup XAxisMonths() {
            this.XAxis.Format = Config.MonthFormatD3;
            this.XAxis.FormatType = "time";
            return this;
        }

        public SerieGroup XAxisDays() {
            this.XAxis.Format = Config.DateFormatD3;
            this.XAxis.FormatType = "time";
            return this;
        }

        public SerieGroup XAxisWeeks() {
            this.XAxis.Format = Config.WeekFormatD3; 
            this.XAxis.FormatType = "time";
            return this;
        }

        public SerieGroup YAxisNumberThousands() {
            this.YAxis.Format = Config.NumberThousandsFormatD3; //",.2r";
            this.YAxis.FormatType = "number";
            return this;
        }

        //public SerieGroup YAxisNumberThousandsInteger() {
        //    this.YAxis.Format = Config.NumberThousandsIntegerFormatD3;//",";
        //    this.YAxis.FormatType = "number";
        //    return this;
        //}
    }
}
