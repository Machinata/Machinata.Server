﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    [Serializable]
    public class ThermometerNode : Node {

        [JsonProperty("thermometerValue")]
        public decimal? ThermometerValue { get; set; } = null;

        [JsonProperty("shades")]
        public List<ThermometerShade> Shades { get; set; }

        [JsonProperty("labels")]
        public List<ThermometerLabel> Labels { get; set; }

        public ThermometerNode AddShade(string colorShade, decimal startVal, decimal endVal) {
            if (this.Shades == null) {
                this.Shades = new List<ThermometerShade>();
            }
            var shade = new ThermometerShade() {
                ColorShade = colorShade,
                StartVal = startVal,
                EndVal = endVal
            };
            this.Shades.Add(shade);
            return this;
        }

        public ThermometerNode AddLabel(string colorShade, decimal val, string resolved, string title = null, string subtitle = null, string orient = null) {
            if (this.Labels == null) {
                this.Labels = new List<ThermometerLabel>();
            }
            var label = new ThermometerLabel() {
                ColorShade = colorShade,
                Value = val,
                Resolved = resolved,
                Orient = orient
            };

            if (title != null) {
                label.Title = title;
            }

            if (subtitle != null) {
                label.Subtitle = subtitle;
            }
            this.Labels.Add(label);
            return this;
        }
    }

    [Serializable]
    public class ThermometerLabel {

        [JsonProperty("colorShade", NullValueHandling = NullValueHandling.Ignore)]
        public string ColorShade { get; set; }

        [JsonProperty("val", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Value { get; set; }

        [JsonProperty("resolved")]
        public string Resolved { get; set; }

        [JsonProperty("orient", NullValueHandling = NullValueHandling.Ignore)]
        public string Orient { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Title { get; set; }

        [JsonProperty("subTitle", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Subtitle { get; set; }


    }

    [Serializable]
    public class ThermometerShade {

        [JsonProperty("colorShade", NullValueHandling = NullValueHandling.Ignore)]
        public string ColorShade { get; set; }

        [JsonProperty("startVal")]
        public decimal? StartVal { get; set; }

        [JsonProperty("endVal", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? EndVal { get; set; }
    }
}
