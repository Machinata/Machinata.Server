﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Machinata.Module.Reporting.Model {

    [Serializable]
    public class InfoCardNode : Node {

        public InfoCardNode() {
            this.Front = new InfoCardNodeItem();
            this.Back = null;
        }

        [JsonProperty("front")]
        public InfoCardNodeItem Front { get; set; }

        [JsonProperty("back")]
        public InfoCardNodeItem Back { get; set; }
    }

    public class InfoCardNodeItem {

        [JsonProperty("html")]
        public TranslatableString HTML { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }
    }
}
