﻿using Newtonsoft.Json;
using System;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    /// </summary>
    [Serializable]
    public class GraphLink {

        public GraphLink(decimal val, string sourceId, string targetId) {
            this.Value = val;
            this.SourceId = sourceId;
            this.TargetId = targetId;
        }

        [JsonProperty("val", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Value { get; set; }

        [JsonProperty("source", NullValueHandling = NullValueHandling.Ignore)]
        public string SourceId { get; set; }

        [JsonProperty("target", NullValueHandling = NullValueHandling.Ignore)]
        public string TargetId { get; set; }

    }

}
