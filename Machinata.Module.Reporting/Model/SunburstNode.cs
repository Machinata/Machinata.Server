﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    [Serializable]
    public class SunburstNode : CategoryChartNode {

        [JsonProperty("sliceLabelsShowPercentage", NullValueHandling = NullValueHandling.Ignore)]
        public bool? SliceLabelsShowPercentage { get; set; } = null; 

        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public GaugeStatusClass Status { get; set; } = null;


    }
}
