﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    
    [Serializable]
    public class ContactNode : ThemeableNode {

        public ContactNode() {
            this.Contact = new Contact();
        }


        [JsonProperty("contact", NullValueHandling = NullValueHandling.Ignore)]
        public Contact Contact { get; set; } = null;

        [JsonProperty("roundPhoto", NullValueHandling = NullValueHandling.Ignore)]
        public bool? RoundPhoto { get; set; } = null;

    }

    [Serializable]
    public class Contact {

        public Contact SetPhoto(string url) {
            this.Photo = new Asset();
            this.Photo.Encoding = "url";
            this.Photo.Data = url;
            return this;
        }

        [JsonProperty("photo", NullValueHandling = NullValueHandling.Ignore)]
        public Asset Photo { get; set; } = null;

        [JsonProperty("phone", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Phone { get; set; } = null;

        [JsonProperty("email", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Email { get; set; } = null;

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Name { get; set; } = null;
    }

   
}
