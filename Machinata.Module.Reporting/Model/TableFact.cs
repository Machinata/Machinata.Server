﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    /// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_tables_.fact.html
    /// </summary>
    [Serializable]
    public class TableFact {

        [JsonProperty("val")]
        public object Val { get; set; }

        [JsonProperty("col")]
        public string Col { get; set; }

        [JsonProperty("resolved")]
        public string Resolved { get; set; }


        [JsonProperty("format")]
        public string Format { get; set; }

        [JsonProperty("color", NullValueHandling = NullValueHandling.Ignore)]
        public string Color { get; set; }

        [JsonProperty("alert", NullValueHandling = NullValueHandling.Ignore)]
        public TableFactAlert Alert { get; set; } = null;

        [JsonProperty("tag", NullValueHandling = NullValueHandling.Ignore)]
        public TableFactTag Tag { get; set; } = null;

        [JsonIgnore()]
        public ValueTypes? ValueTypeEnum { get; set; }

        [JsonProperty("valueType", NullValueHandling = NullValueHandling.Ignore)]
        public string ValueType { get { return ValueTypesHelper.ValueTypeMapping(this.ValueTypeEnum); } }

        [JsonProperty("info", NullValueHandling = NullValueHandling.Ignore)]
        public NodeInfo Info { get; set; } = null;

        [JsonProperty("sort", NullValueHandling = NullValueHandling.Ignore)]
        public int? Sort { get; set; } = null;

        public TableFact SetAlert(string hint, string icon) {
            this.Alert = new TableFactAlert();
            this.Alert.Hint = hint;
            this.Alert.Icon = icon;
            return this;
        }

        public TableFact SetTag(string tag, string hint, string icon, string colorShade) {
            this.Tag = new TableFactTag();
            this.Tag.Title = tag;
            this.Tag.Hint = hint;
            this.Tag.Icon = icon;
            this.Tag.ColorShade = colorShade;
            return this;
        }


    }

    /// <summary>
    /// </summary>
    [Serializable]
    public class TableFactAlert {

        [JsonProperty("hint", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Hint { get; set; }

        [JsonProperty("icon", NullValueHandling = NullValueHandling.Ignore)]
        public string Icon { get; set; }
    }

    /// <summary>
    /// </summary>
    [Serializable]
    public class TableFactTag {

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Title { get; set; }

        [JsonProperty("hint", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Hint { get; set; }

        [JsonProperty("icon", NullValueHandling = NullValueHandling.Ignore)]
        public string Icon { get; set; }

        [JsonProperty("colorShade", NullValueHandling = NullValueHandling.Ignore)]
        public string ColorShade { get; set; }
    }
}
