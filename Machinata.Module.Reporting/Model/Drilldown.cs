﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {



    [Serializable]
    public class Drilldown {

        public Drilldown() {
            this.Filters = new List<DrilldownFilter>();
        }

        public Drilldown AddFilter(string columnId, string value, string name = null) {
            var filter = new DrilldownFilter();
            filter.ColumnId = columnId;
            filter.Value = value;
            filter.Name = name;
            this.Filters.Add(filter);
            return this;
        }

        [JsonProperty("filters")]
        public List<DrilldownFilter> Filters { get; set; }

    }

    [Serializable]
    public class DrilldownFilter {

        public DrilldownFilter() {

        }

        [JsonProperty("columnId", NullValueHandling = NullValueHandling.Ignore)]
        public string ColumnId { get; set; }
        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

    }

}
