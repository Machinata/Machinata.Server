﻿using Machinata.Core.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    /// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_tables_.dimension.html
    /// </summary>
    [Serializable]
    public class Dimension {

        public Dimension() {
            this.DimensionGroups = new List<DimensionGroup>();
          
        }

        [JsonProperty("dimensionGroups")]
        public List<DimensionGroup> DimensionGroups { get; set; }

     

    }
}
