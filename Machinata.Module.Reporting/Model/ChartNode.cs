﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Machinata.Module.Reporting.Model {

    [Serializable]
    public abstract class ChartNode : Node {

        ///  - ```left-top```: Place the legend to the top-left of the chart, in the top corner.
        ///  - ```right-top```: Place the legend to the right of the chart, in the top corner.
        ///  - ```top-left```: Place the legend above the top of the chart, on the left side.
        ///  - ```bottom-left```: Place the legend below the bottom of the chart, on the left side.
        [JsonProperty("legendPosition", NullValueHandling = NullValueHandling.Ignore)]
        public string LegendPosition { get; set; } = null;

        [JsonProperty("legendColumns", NullValueHandling = NullValueHandling.Ignore)]
        public int? LegendColumns { get; set; } = null;

        [JsonProperty("legendReversed", NullValueHandling = NullValueHandling.Ignore)]
        public bool? LegendReversed { get; set; } = null;

        [JsonProperty("legendMinRows", NullValueHandling = NullValueHandling.Ignore)]
        public int? LegendMinRows { get; set; } = null;

        [JsonProperty("legendSiblingsExpectedMinItems", NullValueHandling = NullValueHandling.Ignore)]
        public int? LegendSiblingsExpectedMinItems { get; set; } = null;

        [JsonProperty("legendSiblingsExpectedMaxItems", NullValueHandling = NullValueHandling.Ignore)]
        public int? LegendSiblingsExpectedMaxItems { get; set; } = null;


        [JsonProperty("insertLegend", NullValueHandling = NullValueHandling.Ignore)]
        public bool? InsertLegend { get; set; } = null;


        [JsonProperty("xAxisHidden", NullValueHandling = NullValueHandling.Ignore)]
        public bool? XAxisHidden { get; set; } = null;

        [JsonProperty("yAxisHidden", NullValueHandling = NullValueHandling.Ignore)]
        public bool? YAxisHidden { get; set; } = null;

        public virtual int GetNumberOfLegendItems() {
            throw new Exception("GetNumberOfLegendItems not yet implemented by chart!");
        }
    }
}
