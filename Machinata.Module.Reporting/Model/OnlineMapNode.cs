﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Machinata.Module.Reporting.Model.OfflineMapNode;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    ///https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_charts_.offlinemapnode.html
    /// </summary>
    /// 
    [Serializable]
    public class OnlineMapNode : ChartNode {

        [JsonProperty("projection")]
        public MapProjection Projection { get; set; }

        [JsonProperty("facts")]
        public List<GeoFact> Facts { get; set; }

        [JsonProperty("markersClustered", NullValueHandling = NullValueHandling.Ignore)]
        public bool? MarkersClustered { get; set; } = null;

        [JsonProperty("markersJitter", NullValueHandling = NullValueHandling.Ignore)]
        public float? MarkersJitter { get; set; } = null;

        [JsonProperty("mapMinZoom", NullValueHandling = NullValueHandling.Ignore)]
        public int? MapMinZoom { get; set; } = null;

        [JsonProperty("mapMaxZoom", NullValueHandling = NullValueHandling.Ignore)]
        public int? MapMaxZoom { get; set; } = null;

        public OnlineMapNode() {
            this.Facts = new List<GeoFact>();
        }

        public override Node LoadSampleData() {
            return LoadSampleData();
        }

        public OnlineMapNode LoadSampleData(int facts = 10) {
            this.Facts = new List<GeoFact>();
            this.MainTitle = "OnlineMapNode Map Node  (random)";
            this.SubTitle = "";
            //var random = new Random();
           // IEnumerable<string> categories = new List<string>() { "CAD", "USD", "RUB", "CHF", "EUR", "GBP", "JPY", "AUD", "ZAR", "BRL", "INR", "BRL", "CNY", "Other" };
            var coordinateFacts = CategorySerie.CreateRandomGeoFacts(facts / 2).ToList();
            var addressFacts = CategorySerie.CreateAddressGeoFacts(facts / 2);

            this.Facts.AddRange(coordinateFacts);
            this.Facts.AddRange(addressFacts);


            return this;
        }

        public void AddGeoFact(string address, string name, string link) {
            this.Facts.Add(new GeoFact() { Address = address, Link = new Link(link, name) });
        }
    }

  
}
