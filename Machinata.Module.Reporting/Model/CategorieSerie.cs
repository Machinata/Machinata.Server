﻿using Machinata.Core.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    [Serializable]
    public class CategorySerie {

        public CategorySerie() {
            this.Facts = new List<CategoryFact>();
            this.Title = new TranslatableString(null);
        }

        [JsonProperty("facts")]
        public List<CategoryFact> Facts { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public TranslatableString Title { get; set; }

        [JsonProperty("colorShade")]
        public string ColorShade { get; set; }

        [JsonProperty("xAxis", NullValueHandling = NullValueHandling.Ignore)]
        public Axis XAxis { get; set; } = null;

        [JsonProperty("yAxis", NullValueHandling = NullValueHandling.Ignore)]
        public Axis YAxis { get; set; } = null;

        public CategorySerie AddFact(decimal val, string category) {
            var fact = new CategoryFact(val, null, category);
            this.Facts.Add(fact);
            return this;
        }

        public CategorySerie AddFactAbsolute(decimal val, string category, string format="0") {
            var resolved = val.ToString(format);
            var fact = new CategoryFact(val, resolved, category);
            this.Facts.Add(fact);
            return this;
        }

        public CategorySerie AddFactFormatWithThousandsNoDecimal(decimal val, string category) {
            var resolved = Number.FormatWithThousandsNoDecimal(val);
            var fact = new CategoryFact(val, resolved, category);
          
            this.Facts.Add(fact);
            return this;
        }

        public CategorySerie AddFact(decimal val, string valResolved, string category) {
            var fact = new CategoryFact(val, valResolved, category);
            this.Facts.Add(fact);
            return this;
        }
        public CategorySerie AddFact(decimal val, string valResolved, string category, string label) {
            var fact = new CategoryFact(val, valResolved, category);
            fact.Label = label;
            this.Facts.Add(fact);
            return this;
        }
        public CategorySerie AddFact(decimal val, string valResolved, string category, string label, decimal strength) {
            var fact = new CategoryFact(val, valResolved, category);
            fact.Label = label;
            fact.Strength = strength;
            this.Facts.Add(fact);
            return this;
        }
        public CategorySerie AddFact(decimal val, string valResolved, string category, string label, string colorShade) {
            var fact = new CategoryFact(val, valResolved, category);
            fact.Label = label;
            fact.ColorShade = colorShade;
            this.Facts.Add(fact);
            return this;
        }

        public CategorySerie CalculateFactTitlesAsPercentages() {
            var total = this.Facts.Sum(f => f.Value);
            foreach (var fact in this.Facts) {
                fact.Resolved = ((int)((fact.Value / total) * 100)).ToString() + "%";
            }
            return this;
        }

        public CategorySerie CreateFactTitlesFormattedAsPercentages() {
            foreach (var fact in this.Facts) {
                fact.Resolved = ((int)((fact.Value ) * 100)).ToString() + "%";
            }
            return this;
        }

        /// <summary>
        ///  percentage of overall sum of facts
        /// </summary>
        /// <returns></returns>
        public CategorySerie CalculateFactValuesAsPercentages() {
            var total = this.Facts.Sum(f => f.Value);
            foreach (var fact in this.Facts) {
                fact.Value = fact.Value / total;
            }
            return this;
        }

        public CategorySerie SortByFactTitleAscending() {
            this.Facts = this.Facts.AsQueryable().OrderBy(f => f.Category.Resolved).ToList();
            return this;
        }
        public CategorySerie SortByFactTitleDescending() {
            this.Facts = this.Facts.AsQueryable().OrderByDescending(f => f.Category.Resolved).ToList();
            return this;
        }
        public CategorySerie SortByFactValueAscending() {
            this.Facts = this.Facts.AsQueryable().OrderBy(f => f.Value).ToList();
            return this;
        }
        public CategorySerie SortByFactValueDescending() {
            this.Facts = this.Facts.AsQueryable().OrderByDescending(f => f.Value).ToList();
            return this;
        }

        public void  YAxisNumberThoundsandsNoDecimal() {
            if (this.YAxis == null) {
                this.YAxis = new Axis();
            }
            this.YAxis.Format = Config.NumberThousandsFormatD3;
            this.YAxis.FormatType = "number";
        }

        public void XAxisNumberThoundsandsNoDecimal() {
            if (this.XAxis == null) {
                this.XAxis = new Axis();
            }
            this.XAxis.Format = Config.NumberThousandsFormatD3;
            this.XAxis.FormatType = "number";
        }


        public static IEnumerable<CategoryFact> CreateRandomCategorieFacts(int bars, double minValue, double maxValue) {
            var random = new System.Random();
            for (int i = 1; i <= bars; i++) {
                var val = random.NextDouble() * (maxValue - minValue) + minValue;
                yield return new CategoryFact((decimal)val, val.ToString("P"), "Label " + i, "number", ".0%", "percent: 1");
            }
        }

        public static IEnumerable<CategoryFact> CreateRandomCategorieSymboledFacts(double minValue, double maxValue) {

            var facts = new List<CategoryFact>();

            {
                var min = Core.Util.Math.RandomDouble(minValue, maxValue);
                var minFact = new CategoryFact((decimal)min, min.ToString("P"), "Label min", "number", ".0%", "percent: 1");
                minFact.SetSymbolType(SymbolTypes.Min);
                facts.Add(minFact);
            }
            {
                var max = Core.Util.Math.RandomDouble(minValue, maxValue);
                var maxFact = new CategoryFact((decimal)max, max.ToString("P"), "Label max", "number", ".0%", "percent: 1");
                maxFact.SetSymbolType(SymbolTypes.Max);
                facts.Add(maxFact);
            }
            {
                var line = Core.Util.Math.RandomDouble(minValue, maxValue);
                var lineFact = new CategoryFact((decimal)line, line.ToString("P"), "Label line", "number", ".0%", "percent: 1");
                lineFact.SetSymbolType(SymbolTypes.Line);
                facts.Add(lineFact);
            }
            {
                var diamond = Core.Util.Math.RandomDouble(minValue, maxValue);
                var diamondFact = new CategoryFact((decimal)diamond, diamond.ToString("P"), "Label diamond", "number", ".0%", "percent: 1");
                diamondFact.SetSymbolType(SymbolTypes.Dot);
                facts.Add(diamondFact);
            }

            return facts;
        }

        public static IEnumerable<CategoryFact> CreateRandomCategorieFactsAddingToOne(int count, Random random = null) {
            if (random == null) {
                random = new System.Random();
            }
            var total = 0.0;
            for (int i = 1; i <= count; i++) {
                var val = random.NextDouble() / count * 1.5;
                total += val;
                if (i == count && total < 1) {
                    val = 1 - total;
                }
                yield return new CategoryFact((decimal)val, val.ToString("P"), "Label " + i, "number", ".0%", "percent: 1");
            }
        }

        public static IEnumerable<CategoryFact> CreateRandomCategorieFactsAddingToOne(IEnumerable<string> categories, Random random = null) {
            if (random == null) {
                random = new System.Random();
            }
            var total = 0.0;
            var count = categories.Count();
            int i = 0;
            foreach (var category in categories) {
                var val = random.NextDouble() / count * 1.5;
                total += val;
                if (i == count && total < 1) {
                    val = 1 - total;
                }
                i++;
                yield return new CategoryFact((decimal)val, val.ToString("P"), category, "number", ".0%", "percent: 1");
            }
        }

        public static IEnumerable<CategoryFact> CreateRandomPercentageCategorieFacts(IEnumerable<string> categories, double min, double max) {
            foreach (var category in categories) {
                var val = Core.Util.Math.RandomDouble(min, max);
                yield return new CategoryFact((decimal)val, val.ToString("P"), category, "number", ".0%", "percent: 1");
            }
        }


        /// <summary>
        /// Series for VBarNodes
        /// </summary>
        /// <param name="categories"></param>
        /// <returns></returns>
        public static IEnumerable<CategoryFact> CreateCategorieFacts(Dictionary<string, decimal> categories, bool inPercent = true) {

            foreach (var category in categories) {
                if (inPercent) {
                    yield return new CategoryFact((decimal)category.Value, category.Value.ToString("P"), category.Key, "number", ".0%", "percent: 1");
                } else {
                    yield return new CategoryFact((decimal)category.Value, category.Value.ToString(), category.Key, "number", ".0f", "abs");
                }
            }
        }


        /// <summary>
        /// For absolute numbers with format {0:0}
        /// </summary>
        /// <param name="categories"></param>
        /// <returns></returns>
        public static IEnumerable<CategoryFact> CreateCategorieFacts(IOrderedEnumerable<KeyValuePair<string, decimal>> categories) {
            foreach (var category in categories) {
                //yield return new CategoryFact((decimal)category.Value, category.Value.ToString("P"), category.Key, "number", ".0%", "percent: 1");

                yield return new CategoryFact((decimal)category.Value, category.Value.ToString("0"), category.Key, "number", "0.0");
            }
        }


        /// <summary>
        /// Co
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public static IEnumerable<GeoFact> CreateRandomGeoFacts(int count) {
            for (int i = 0; i < count; i++ ) {
                var lat = Core.Util.Math.RandomDouble(-90, 90);
                var lon = Core.Util.Math.RandomDouble(-180, 180);
                yield return new GeoFact() {X = (decimal)lat, Y = (decimal)lon };
            }
        }

        public static IEnumerable<GeoFact> CreateAddressGeoFacts(int count) {

            var facts = new List<GeoFact>();

            facts.Add(new GeoFact() { Address = "Räffelstrasse, Zürich" , Link = new Link("https://nerves.ch", "Nerves GmnbH")});
            facts.Add(new GeoFact() { Address = "Brahmstrasse, Zürich", Link = new Link("https://google.ch") });
            facts.Add(new GeoFact() { Address = "Bundeshaus Bern", Link = new Link("https://admin.ch") });
            facts.Add(new GeoFact() { Address = "Bärengraben, Bern" });
            facts.Add(new GeoFact() { Address = "Herisau"});
            facts.Add(new GeoFact() { Address = "Kentishtown"});
            facts.Add(new GeoFact() { Address = "Sydney Australia"});

            return facts.Take(count);
        }

       
    }
}
