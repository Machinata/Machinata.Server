﻿using Machinata.Core.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    /// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_charts_.scatterplotnode.html
    /// </summary>
    /// 
    [Serializable]
    public class ScatterPlotNode : GraphNode {

        public ScatterPlotNode() {
           
        }


        public Node LoadSampleData(int numberPerType = 2, int columnFacts = 12, int lineFacts = 80, double valueMin = 0, double valueMax = 0.02, long? start = null, long? end = null) {
            if (start == null) {
                start = Core.Util.Time.GetUTCMillisecondsFromDateTime(DateTime.UtcNow.StartOfYear());
            }
            if (end == null) {
                end = Core.Util.Time.GetUTCMillisecondsFromDateTime(DateTime.UtcNow.EndOfYear());
            }

            this.Enabled = true;
            this.Screen = true;
            this.Chrome = "solid";

            this.SeriesGroups = new List<SerieGroup>();

            SerieGroup group = AddSeriesGroupPercentageNumber();

            this.MainTitle = "ScatterPlotNode Example";
            this.SubTitle = "with two groups (seperate y-axis, forced x-axis values)";

            var random = new System.Random();

            // Series
            for (int i = 1; i <= numberPerType; i++) {
                var serie = new Serie();
                serie.Facts = Serie.GenerateFacts(columnFacts, start.Value, end.Value, valueMin, valueMax, random);
                serie.Id = "columnSerie" + i;
                serie.Title = "Serie" + i;
                serie.Title.Text = "portfolioGrossMonthly" + i;
                serie.ChartType = "scatterplot";
                serie.ColorShade = "bright";
                group.Series.Add(serie);

            }


            this.SeriesGroups.Add(group);
            return this;
        }

        public SerieGroup AddSeriesGroupNumberPercentage(string xAxis = null, string yAxis = null) {
            var group = new SerieGroup();
            group.Id = "columns";
            group.XAxis.Format = ".1";
            group.XAxis.FormatType = "number";
            group.XAxis.Title = xAxis;
           

            group.YAxis.Orient = "left";
            group.YAxis.Format = ".1%";
            group.YAxis.FormatType = "percent";
            group.YAxis.Title = yAxis;


            this.SeriesGroups.Add(group);
            return group;
        }

        public SerieGroup AddSeriesGroupPercentageNumber(string xAxis = null, string yAxis = null) {
            var group = new SerieGroup();
            group.Id = "columns";
            group.XAxis.Format = ".1%";
            group.XAxis.FormatType = "percent";
            group.XAxis.Title = xAxis;


            group.YAxis.Orient = "left";
            group.YAxis.Format = ".1";
            group.YAxis.FormatType = "number";
            group.YAxis.Title = yAxis;


            this.SeriesGroups.Add(group);
            return group;
        }

    }
}
