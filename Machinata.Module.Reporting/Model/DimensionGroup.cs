﻿using Machinata.Core.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    /// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_tables_.dimensiongroup.html
    /// </summary>
    /// 
    [Serializable]
    public class DimensionGroup {

        public DimensionGroup() {
            this.GroupTypeEnum = GroupTypes.Table;
        }

        [JsonProperty("dimensionGroups")]
        public List<DimensionGroup> DimensionGroups { get; set; }


        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public TranslatableString Name { get; set; }

        [JsonProperty("importance")]
        public int Importance { get; set; }

        //TODO: Is this really correct?
        [JsonProperty("expanded", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Expanded { get; set; } = null;

        [JsonProperty("dimensionElements")]
        public List<DimensionElement> DimensionElements { get; set; }

        [JsonProperty("headerFacts")]
        public List<TableFact> HeaderFacts { get; set; }

        [JsonIgnore]
        public GroupTypes GroupTypeEnum { get; set; }

        [JsonProperty("groupType")]
        public string GroupType { get { return this.GroupTypeEnum.ToString().ToLower(); } }

        public DimensionElement AddRow(string name = null) {
           

            var elem = new DimensionElement();
            elem.Name = name;
            elem.Facts = new List<TableFact>();
            elem.ValueTypeEnum = ValueTypes.PAbs;
            this.DimensionElements.Add(elem);

            return elem;

        }

        public DimensionGroup AddGroupSubtotal (string columnName, DimensionGroup colGroup, bool isPercent = false ) {
            var dimensionElement = colGroup.DimensionElements.FirstOrDefault(de => de.Name.Resolved == columnName);
            if (dimensionElement == null) {
                throw new Exception("columnName not found");
            }
            if (this.HeaderFacts == null) {
                this.HeaderFacts = new List<TableFact>();
            }

            var fact = new TableFact();
            fact.Col = dimensionElement.Id;

            var colFacts = this.DimensionElements.SelectMany(d => d.Facts).Where(f => f.Col == dimensionElement.Id && f.Val != null).Select(f => System.Convert.ToDecimal(f.Val));

            var val = colFacts.Sum();
            fact.Val = val;

            if (isPercent == false) {
                fact.Resolved = val.ToString();
            } else {
                fact.Resolved = val.ToString("P");
                fact.Format = ".0%";
            }

            this.HeaderFacts.Add(fact);

            return this;
        }

        /// <summary>
        /// Adds header fact and auto sums row values if not deactivated in param
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="colGroup"></param>
        /// <param name="fact"></param>
        /// <returns></returns>
        public DimensionGroup AddHeaderFact(string columnName, DimensionGroup colGroup, TableFact fact, bool autoSum = true ) {
            var dimensionElement = colGroup.DimensionElements.FirstOrDefault(de => de.Name.Resolved == columnName);
            if (dimensionElement == null) {
                throw new Exception("columnName not found");
            }
            if (this.HeaderFacts == null) {
                this.HeaderFacts = new List<TableFact>();
            }

            if (fact.Col == null) {
                fact.Col = dimensionElement.Id;
            }


            if (autoSum == true) {
                var colFacts = this.DimensionElements.SelectMany(d => d.Facts).Where(f => f.Col == dimensionElement.Id && f.Val != null).Select(f => System.Convert.ToDecimal(f.Val));
                var val = colFacts.Sum();
                fact.Val = val;
            }

            this.HeaderFacts.Add(fact);

            return this;
        }


    }

    public enum GroupTypes {
        Table,
        Bars
    }

}
