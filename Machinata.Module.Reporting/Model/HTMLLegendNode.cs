﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Machinata.Module.Reporting.Model {

    [Serializable]
    public class HTMLLegendNode : Node {

        public HTMLLegendNode() {
            
        }

        [JsonProperty("legendData")]
        public List<LegendDataItem> LegendData { get; set; } = new List<LegendDataItem>();

        public HTMLLegendNode AddLegendItem(string title, string symbol, string colorShade = null) {
            var newItem = new LegendDataItem();
            newItem.Title = title;
            newItem.Symbol = symbol;
            newItem.ColorShade = colorShade;
            this.LegendData.Add(newItem);
            return this;
        }
    }

}
