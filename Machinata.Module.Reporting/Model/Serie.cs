﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    [Serializable]
    public class Serie {

        public Serie() {
            this.Facts = new List<Fact>();
            this.Title = new TranslatableString(null);
        }

        [JsonProperty("facts")]
        public List<Fact> Facts { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public TranslatableString Title { get; set; }

        [JsonProperty("chartType")]
        public string ChartType { get; set; }

        [JsonProperty("colorShade")]
        public string ColorShade { get; set; }

        [JsonProperty("debug")]
        public string Debug { get; set; }


        /// <summary>
        /// Generates random factos over a time y axis
        /// </summary>
        /// <param name="count"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="random"></param>
        /// <returns></returns>
        public static List<Fact> GenerateFacts(int count, long start, long end, double min, double max, Random random = null) {
            var facts = new List<Fact>();
            if (random == null) {
                random = new System.Random();
            }
            var timeFrame = (end - start) / count;
            for (int i = 1; i <= count; i++) {
                var value = random.NextDouble() * (max - min) + min;
                var date = start + (i - 1) * timeFrame;
                var fact = new Fact();
                fact.X = date;
                fact.Y = (decimal?)value;
                facts.Add(fact);
            }
            return facts;
        }

        public static List<Fact> GenerateFacts(List<decimal> columns,double min, double max, Random random= null) {
            var facts = new List<Fact>();
            if (random == null) {
                random = new System.Random();
            }
       
           foreach(var column in columns) {
                var value = random.NextDouble() * (max - min) + min;
                var fact = new Fact();
                fact.X = column;
                fact.Y = (decimal?)value;
                facts.Add(fact);
            }
            return facts;
        }


        public static List<Fact> GenerateAccumulatedFacts(int count, long start, long end, double min, double max, Random random = null) {
            var facts = new List<Fact>();
            if (random == null) {
                random = new System.Random();
            }
            var timeFrame = (end - start) / count;
            var currentValue = min;
            for (int i = 1; i <= count; i++) {
                var value = (random.NextDouble() * (max - min) + min) / count * 2.5;
                var date = start + (i - 1) * timeFrame;
                var fact = new Fact();
                fact.X = date;

                currentValue += value;
                fact.Y = (decimal?)currentValue;
                facts.Add(fact);
            }
            return facts;
        }



        public Serie GenerateRamp(int count, long start, long end, double min, double max, double jitter = 0, Random rnd = null) {

            if(rnd == null) rnd = new System.Random((int)DateTime.Now.Ticks);

            var timeFrame = (end - start) / count;
            var valueRange = max-min;
            var valueStep = valueRange / count;
            for (int i = 0; i < count; i++) {
                var value = min + (i * valueStep);
                if(jitter != 0 && i != 0 && i != count-1) {
                    value += ((rnd.NextDouble()-0.5) * 2.0) * jitter;
                }

                var date = start + (i * timeFrame);
                
                var fact = new Fact();
                fact.X = date;
                fact.Y = (decimal?)value;
                this.Facts.Add(fact);
            }
            return this;
        }

        public Serie GenerateRamp(int count, DateTime start, DateTime end, double min, double max, double jitter = 0, Random rnd = null) {
            return GenerateRamp(count, Core.Util.Time.GetUTCMillisecondsFromDateTime(start), Core.Util.Time.GetUTCMillisecondsFromDateTime(end), min, max, jitter, rnd);
        }


        /// <summary>
        /// Adds a simple X,Y,Z fact to the serie
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public Serie AddFact(decimal x, decimal y, decimal? z=null, string title = null) {

            var fact = new Fact();
            fact.X = x;
            fact.Y = y;
            fact.Z = z;
            fact.Title = title;

            this.Facts.Add(fact);

            return this;
        }

        /// <summary>
        /// Adds a simple X fact with y as date
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public Serie AddFact(DateTime x, decimal y, decimal? z = null, string title = null) {

            
            var fact = new Fact();

            fact.X = Core.Util.Time.GetUTCMillisecondsFromDateTime(x);
            fact.Y= y;
            
            fact.Z = z;
            fact.Title = title;

            this.Facts.Add(fact);

            return this;
        }

        /// <summary>
        /// Updates the Y values to integer
        /// </summary>
        public void RoundFactsToInteger() {
            if (this.Facts != null) {
                foreach (var fact in this.Facts) {
                    if (fact.Y.HasValue) {
                        fact.Y = Math.Round(fact.Y.Value, 0);
                    }
                }
            }
        }
    }
}