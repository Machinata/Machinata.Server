﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Machinata.Module.Reporting.Model {

    [Serializable]
    public class PlaceholderNode : Node { // TODO: this the correct hierarchy?

        public PlaceholderNode() {
            
        }

        [JsonProperty("text")]
        public TranslatableString Text { get; set; }




    }

}
