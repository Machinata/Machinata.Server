﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    /// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_charts_.gaugenode.html
    /// </summary>
    /// 
    [Serializable]
    public class GaugeNode : ChartNode {

        public GaugeNode() {
            this.Status = new GaugeStatusClass();
            this.Segments = new List<GaugeSegement>();
            this.Facts = new List<CategoryFact>();
        }

        [JsonProperty("segments")]
        public List<GaugeSegement> Segments { get; set; }

        [JsonProperty("facts")]
        public List<CategoryFact> Facts { get; set; }

        [JsonProperty("status")]
        public GaugeStatusClass Status { get; set; }


        public GaugeNode SetStatus(string title, decimal value) {
            this.Status.Title = new TranslatableString(title);
            this.Status.Value = value;
            return this;
        }
        public GaugeNode AddSegment(string title, bool active, string tooltip = null, decimal? val = null) {
            var seg = new GaugeSegement();
            seg.SetTitle(title);
            seg.SetActive(active);
            if(tooltip != null) seg.SetTooltip(tooltip);
            if(val != null) seg.SetValue(val.Value);
            this.Segments.Add(seg);
            return this;
        }


        public override Node LoadSampleData() {
            return LoadSampleData();
        }

        public GaugeNode LoadSampleData(int numberSegements = 7) {
            this.MainTitle = "Gauge Node  (random)";
            this.SubTitle = "";
            var random = new Random();
            var value = random.Next(0, numberSegements);
            this.Status = new GaugeStatusClass() { Title = "Segmented Gauge Test", Value = value };
            this.Segments = GenerateSegements(numberSegements, value);
            return this;
        }


        private static List<GaugeSegement> GenerateSegements(int numberOfSegements, int active) {
            var segs = new List<GaugeSegement>();

            for (int i = 1; i <= numberOfSegements; i++) {
                var seg = new GaugeSegement();
                seg.Active = i <= active;
                seg.Title = i.ToString();
                seg.Tooltip = $"{i} / {numberOfSegements}";
                segs.Add(seg);
            }

            return segs;
        }





    }
    [Serializable]
    public class GaugeSegement {

        public GaugeSegement() {
        }

        

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Title { get; set; }

        [JsonProperty("val", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Value { get; set; }

        [JsonProperty("active", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Active { get; set; }

        [JsonProperty("opacity", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Opacity { get; set; }

        [JsonProperty("colorShade", NullValueHandling = NullValueHandling.Ignore)]
        public string ColorShade { get; set; }

        [JsonProperty("tooltip", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Tooltip { get; set; }


        public GaugeSegement SetTitle(string text) {
            this.Title = new TranslatableString(text);
            return this;
        }
        public GaugeSegement SetValue(decimal val) {
            this.Value = val;
            return this;
        }
        public GaugeSegement SetTooltip(string text) {
            this.Tooltip = new TranslatableString(text);
            return this;
        }
        public GaugeSegement SetActive(bool active) {
            this.Active = active;
            return this;
        }
    }

    [Serializable]
    public class GaugeStatusClass {

        [JsonProperty("title")]
        public TranslatableString Title { get; set; }

        [JsonProperty("value")]
        public decimal Value { get; set; }
    }
}
