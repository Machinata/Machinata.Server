﻿using Machinata.Core.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    /// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_charts_.linecolumnnode.html
    /// </summary>
    [Serializable]
    public class LineColumnNode : GraphNode {



        public override Node LoadSampleData() {
            return this.LoadSampleData();
        }

        public Node LoadSampleData(int numberPerType = 2, int columnFacts = 12, int lineFacts = 80, double valueMin = 0, double valueMax = 0.02, long? start = null, long? end = null) {
            if (start == null) {
                start = Core.Util.Time.GetUTCMillisecondsFromDateTime(DateTime.UtcNow.StartOfYear());
            }
            if (end == null) {
                end = Core.Util.Time.GetUTCMillisecondsFromDateTime(DateTime.UtcNow.EndOfYear());
            }

            this.Enabled = true;
            this.Screen = true;
            this.Chrome = "solid";
            this.MainTitle = "Line and Column Example";
            this.SubTitle = "with two groups (seperate y-axis, forced x-axis values)";
            this.SeriesGroups = new List<SerieGroup>();

            var group = new SerieGroup();
            group.Id = "columns";
            group.XAxis.Format = "%b";
            group.XAxis.FormatType = "time";

            var xValues = new List<decimal>() { start.Value };
            var timeSpan = (end - start) / columnFacts;
            for (int i = 1; i < columnFacts; i++) {
                xValues.Add(i * timeSpan.Value);
            }
            group.XAxis.Values = xValues;
            group.YAxis.Orient = "left";
            group.YAxis.Format = ".1%";
            group.YAxis.FormatType = "number";

            var random = new System.Random();

            // columnSerie1
            for (int i = 1; i <= numberPerType; i++) {
                var serie = new Serie();
                serie.Facts = Serie.GenerateFacts(columnFacts, start.Value, end.Value, valueMin, valueMax, random);
                serie.Id = "columnSerie" + i;
                serie.Title = "Monthly" + i;
                serie.Title.Text = "portfolioGrossMonthly" + i;
                serie.ChartType = "column";
                serie.ColorShade = "bright";
                group.Series.Add(serie);

            }

            // lineSerie1
            for (int i = 1; i <= numberPerType; i++) {
                var serie = new Serie();
                serie.Facts = Serie.GenerateAccumulatedFacts(lineFacts, start.Value, end.Value, valueMin, valueMax, random);
                serie.Id = "lineSerie" + i;
                serie.Title = "Portfolio - cumulated" + i;
                serie.Title.Text = "portfolioGrossCumulated" + i;
                serie.ChartType = "line";
                serie.ColorShade = "dark";
                group.Series.Add(serie);

            }

            this.SeriesGroups.Add(group);
            return this;
        }
    }

}
