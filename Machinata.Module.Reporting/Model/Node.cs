﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {


    /// <summary>
    /// Should this be ReportNode?
    /// 
    /// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_nodes_.reportnode.html
    /// </summary>
    /// 
    [Serializable]
    public abstract class Node {

        /*
        "enabled": true,
    "screen": true,
    "chrome": "solid",
    "mainTitle": { "resolved": "Line and Column Example" },
    "subTitle": { "resolved": "only lines (sparse, forced y-axis values)" },
        */

        public Node() {
            //this.Id = Core.Ids.GUID.Default.GenerateGUID();
            this.Children = new List<Node>();
        }

        [JsonProperty("theme", NullValueHandling = NullValueHandling.Ignore)]
        public string Theme { get; set; }

        [JsonProperty("screen", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Screen { get; set; } = null;

        [JsonProperty("addStandardTools", NullValueHandling = NullValueHandling.Ignore)]
        public bool? AddStandardTools { get; set; } = null;

        [JsonProperty("enabled")]
        public bool Enabled { get; set; } = true;

        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("supportsToolbar", NullValueHandling = NullValueHandling.Ignore)]
        public bool? SupportsToolbar;

        [JsonProperty("chrome", NullValueHandling = NullValueHandling.Ignore)]
        public string Chrome;

        [JsonProperty("info", NullValueHandling = NullValueHandling.Ignore)]
        public NodeInfo Info { get; set; }

        [JsonProperty("link", NullValueHandling = NullValueHandling.Ignore)]
        public Link Link { get; set; }

        [JsonProperty("mainTitle", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString MainTitle { get; set; }

        [JsonProperty("subTitle", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString SubTitle { get; set; }

        [JsonProperty("toggleTitle", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString ToggleTitle { get; set; }

        [JsonProperty("responsiveSize", NullValueHandling = NullValueHandling.Ignore)]
        public NodeResponsiveSize ResponsiveSize { get; set; }

        [JsonProperty("nodeType")]
        public string NodeType { get { return this.GetType().Name; } }

        [JsonProperty("children")]
        public List<Node> Children { get; set; }

        [JsonProperty("style")]
        public string Style { get; set; }

        [JsonProperty("styles", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Styles { get; set; } = null;


        public virtual Node LoadSampleData() {
            throw new NotImplementedException($"'{this.GetType()}' does not implement LoadSampleData() yet");
        }

        public virtual JObject GetJObject() {
            return JObject.FromObject(this);
        }


        public virtual Node SolidChrome() {
            this.Chrome = "solid";
            return this;
        }
        public virtual Node LightChrome() {
            this.Chrome = "light";
            return this;
        }
        public virtual Node DarkChrome() {
            this.Chrome = "dark";
            return this;
        }

        public Node DefaultTheme() {
            this.Theme = "default";
            return this;
        }

        public virtual Node SetInfo(string title, string text, string tooltip, bool? wideDialog = null) {
            this.Info = new NodeInfo();
            this.Info.Title = title;
            this.Info.Text = text;
            this.Info.Tooltip = tooltip;
            this.Info.WideDialog = wideDialog;
            return this;
        }

        public virtual Node SetChapterLink(string chapterId, string tooltip = null) {
            this.Link = new Link();
            this.Link.ChapterId = chapterId;
            this.Link.Tooltip = tooltip;
            return this;
        }

        public virtual Node SetTitle(string title) {
            this.MainTitle = new TranslatableString(title);
            return this;
        }
        public virtual Node SetSubTitle(string title) {
            this.SubTitle = new TranslatableString(title);
            return this;
        }
        public virtual Node AddStyle(string style) {
            if (this.Styles == null) this.Styles = new List<string>();
            this.Styles.Add(style);
            return this;
        }
        public virtual Node AddChild(Node node) {
            this.Children.Add(node);
            return this;
        }
        public virtual Node AddTo(Node node) {
            node.Children.Add(this);
            return this;
        }

        public virtual Node RandomID() {
            this.Id = Guid.NewGuid().ToString();
            return this;
        }

        public virtual Node DisableStandardToolbarTools() {
            this.AddStandardTools = false;
            return this;
        }

        public void AutoBalanceChildrenLegends() {
            // Calculate min/max legend items for all charts
            var legendMinItems = int.MaxValue;
            var legendMaxItems = int.MinValue;
            foreach(var child in this.Children) {
                if (Core.Util.Reflection.TypeInheritsFrom(child.GetType(), typeof(ChartNode))) {
                    var chart = child as ChartNode;
                    var chartItems = chart.GetNumberOfLegendItems();
                    if (chartItems < legendMinItems) legendMinItems = chartItems;
                    if (chartItems > legendMaxItems) legendMaxItems = chartItems;
                }
            }
            // Assign the expeted hints...
            foreach (var child in this.Children) {
                if (Core.Util.Reflection.TypeInheritsFrom(child.GetType(), typeof(ChartNode))) {
                    var chart = child as ChartNode;
                    chart.LegendSiblingsExpectedMinItems = legendMinItems;
                    chart.LegendSiblingsExpectedMaxItems = legendMaxItems;
                }
            }
        }

        /// <summary>
        /// Make sure every Sub Property and Class is marked with [Serialized]
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Clone<T>() where T : Node {

            BinaryFormatter bf = new BinaryFormatter();

            byte[] serialized = null;

            using (MemoryStream ms = new MemoryStream()) {
                bf.Serialize(ms, this);
                serialized = ms.ToArray();

            }

            using (MemoryStream memStream = new MemoryStream()) {
                BinaryFormatter binForm = new BinaryFormatter();
                memStream.Write(serialized, 0, serialized.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                var obj = (T)binForm.Deserialize(memStream);
                return obj;
            }
           
           
        }

    }



    public class TypeNameSerializationBinder : SerializationBinder {
        public string TypeFormat { get; private set; }

        public TypeNameSerializationBinder(string typeFormat) {
            TypeFormat = typeFormat;
        }

        public override void BindToName(Type serializedType, out string assemblyName, out string typeName) {
            assemblyName = null;
            typeName = serializedType.Name;
        }

        public override Type BindToType(string assemblyName, string typeName) {
            var resolvedTypeName = string.Format(TypeFormat, typeName);
            return Type.GetType(resolvedTypeName, true);
        }
    }

    [Serializable]
    public class NodeResponsiveSizeLayout {

        public NodeResponsiveSizeLayout() {
        }

        public NodeResponsiveSizeLayout(object width, object height) {
            this.Width = width;
            this.Height = height;
        }

        [JsonProperty("size", NullValueHandling = NullValueHandling.Ignore)]
        public object Size { get; set; } = null; // string or number

        [JsonProperty("width", NullValueHandling = NullValueHandling.Ignore)]
        public object Width { get; set; } = null; // string or number

        [JsonProperty("height", NullValueHandling = NullValueHandling.Ignore)]
        public object Height { get; set; } = null; // string or number

    }

   
    [Serializable]
    public class NodeResponsiveSize {

        public NodeResponsiveSize() {

        }

        public NodeResponsiveSize(string defaultSize, string desktopSize = null, string tabletSize = null, string mobileSize = null) {
            if (defaultSize != null) {
                this.Default = new NodeResponsiveSizeLayout();
                this.Default.Size = defaultSize;
            }
            if (desktopSize != null) {
                this.Desktop = new NodeResponsiveSizeLayout();
                this.Desktop.Size = desktopSize;
            }
            if (tabletSize != null) {
                this.Tablet = new NodeResponsiveSizeLayout();
                this.Tablet.Size = tabletSize;
            }
            if (mobileSize != null) {
                this.Mobile = new NodeResponsiveSizeLayout();
                this.Mobile.Size = mobileSize;
            }
        }

        public NodeResponsiveSize(NodeResponsiveSizeLayout defaultSizeLayout) {
            this.Default = defaultSizeLayout;
        }

        public NodeResponsiveSize(object defaultWidth, object defaultHeight = null) {
            this.Default = new NodeResponsiveSizeLayout();
            this.Default.Width = defaultWidth;
            this.Default.Height = defaultHeight;
        }

        [JsonProperty("default", NullValueHandling = NullValueHandling.Ignore)]
        public NodeResponsiveSizeLayout Default { get; set; }

        [JsonProperty("desktop", NullValueHandling = NullValueHandling.Ignore)]
        public NodeResponsiveSizeLayout Desktop { get; set; }

        [JsonProperty("tablet", NullValueHandling = NullValueHandling.Ignore)]
        public NodeResponsiveSizeLayout Tablet { get; set; }

        [JsonProperty("mobile", NullValueHandling = NullValueHandling.Ignore)]
        public NodeResponsiveSizeLayout Mobile { get; set; }

    }


    [Serializable]
    public class NodeInfo {

        public NodeInfo() {

        }

        public NodeInfo(string title, string text, string tooltip) {
            this.Tooltip = tooltip;
            this.Title = title;
            this.Text = text;
        }

        [JsonProperty("tooltip", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Tooltip { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Title { get; set; }

        [JsonProperty("text", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Text { get; set; }

        [JsonProperty("wideDialog", NullValueHandling = NullValueHandling.Ignore)]
        public bool? WideDialog { get; set; } = null;
    }
}
