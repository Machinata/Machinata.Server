﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    [Serializable]
    public class SectionNode : Node {




        [JsonProperty("titleLevel", NullValueHandling = NullValueHandling.Ignore)]
        public int? TitleLevel { get; set; } = null;


        public SectionNode SetTheme(string theme) {
            this.Theme = theme;
            return this;
        }

        public SectionNode NewSection(string title) {
            var section = new SectionNode();
            section.SetTitle(title);
            this.AddChild(section);
            return section;
        }

        public SectionNode AddInfoNodeSubsection(string message, string infoType = "info") {
            var infoNode = new InfoNode();
            infoNode.Message = message;
            infoNode.InfoType = infoType;
            this.AddChild(infoNode);
            return this;
        }

    }
}
