﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    [Serializable]
    public class MetricsNode : Node {

        public MetricsNode() {
            ItemGroups = new List<MetricsNodeItemGroup>();
        }

        [JsonProperty("itemGroups")]
        public List<MetricsNodeItemGroup> ItemGroups { get; set; }

        public MetricsNodeItemGroup AddGroup() {
            var newGroup = new MetricsNodeItemGroup();
            this.ItemGroups.Add(newGroup);
            return newGroup;
        }

        public MetricsNodeItem AddNumberItem(string number) {
            var newGroup = new MetricsNodeItemGroup();
            this.ItemGroups.Add(newGroup);
            var item = newGroup.CreateItem();
            item.SetNumber(number);
            return item;
        }

        public void Growing() {
          this.Style = "Growing";
        }
    }

    public class MetricsNodeItemGroup {

        public MetricsNodeItemGroup() {
            Items = new List<MetricsNodeItem>();
        }

        [JsonProperty("items")]
        public List<MetricsNodeItem> Items { get; set; }

        public MetricsNodeItem CreateItem() {
            var newItem = new MetricsNodeItem();
            Items.Add(newItem);
            return newItem;
        }

        public MetricsNodeItemGroup AddItem() {
            var newItem = new MetricsNodeItem();
            Items.Add(newItem);
            return this;
        }


        public MetricsNodeItemGroup AddItem(string label, string value, string subLabel = null, string subValue = null) {
            var newItem = new MetricsNodeItem();
            if(label != null) newItem.Label = label;
            if (value != null) newItem.Value = value;
            if (subLabel != null) newItem.SubLabel = subLabel;
            if (subValue != null) newItem.SubValue = subValue;
            Items.Add(newItem);
            return this;
        }
    }

    public class MetricsNodeItem {

        [JsonProperty("label", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Label { get; set; } = null;

        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Value { get; set; } = null;

        [JsonProperty("subLabel", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString SubLabel { get; set; } = null;

        [JsonProperty("subValue", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString SubValue { get; set; } = null;

        [JsonProperty("subDelta", NullValueHandling = NullValueHandling.Ignore)]
        public MetricsNodeItemSubDelta SubDelta { get; set; } = null;

        [JsonProperty("number", NullValueHandling = NullValueHandling.Ignore)]
        public string Number { get; set; } = null;

        [JsonProperty("alert", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Alert { get; set; } = null; //TODO

        public MetricsNodeItem SetLabel(string text) {
            this.Label = new TranslatableString(text);
            return this;
        }
        public MetricsNodeItem SetValue(string text) {
            this.Value = new TranslatableString(text);
            return this;
        }
        public MetricsNodeItem SetSubLabel(string text) {
            this.SubLabel = new TranslatableString(text);
            return this;
        }
        public MetricsNodeItem SetSubValue(string text) {
            this.SubValue = new TranslatableString(text);
            return this;
        }
        public MetricsNodeItem SetSubDelta(string label, string icon = null, string color = null) {
            this.SubDelta = new MetricsNodeItemSubDelta(label, icon, color);
            return this;
        }
        public MetricsNodeItem SetNumber(string text) {
            this.Number = text;
            return this;
        }
    }

    public class MetricsNodeItemSubDelta {

        public MetricsNodeItemSubDelta(string label, string icon, string color) {
            this.Label = label;
            this.Color = color;
            this.Icon = icon;
        }

        [JsonProperty("label")]
        public TranslatableString Label { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }
    }
}
