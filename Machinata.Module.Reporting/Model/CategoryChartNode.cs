﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    /// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_charts_.categorychartnode.html
    /// </summary>
    /// 
    [Serializable]
    public abstract class CategoryChartNode : ChartNode {

        public CategoryChartNode() {
            Series = new List<CategorySerie>();
        }

        [JsonProperty("series")]
        public List<CategorySerie> Series { get; set; }

        public CategorySerie AddSerie(string title) {
            var serie = new CategorySerie();
            serie.Title = title;
            serie.Id = "serie" + this.Series.Count;
            this.Series.Add(serie);
            return serie;
        }

        public void RemoveZeroValueFacts() {
            foreach(var s in this.Series) {
                var factsToRemove = new List<CategoryFact>();
                foreach (var f in s.Facts) {
                    if(f.Value == null || f.Value == 0m) factsToRemove.Add(f);
                }
                foreach (var f in factsToRemove) s.Facts.Remove(f);
            }
        }



      
    }
}
