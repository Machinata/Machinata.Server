﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    /// </summary>
    /// 
    [Serializable]
    public class MultiGaugeNode : ChartNode {

        public MultiGaugeNode() {
            this.Facts = new List<CategoryFact>();
        }


        [JsonProperty("facts")]
        public List<CategoryFact> Facts { get; set; }

        [JsonProperty("maximumStatusValue")]
        public decimal? MaximumStatusValue { get; set; } = null;

        [JsonProperty("gaugeRows")]
        public int? GaugeRows { get; set; } = null;

        [JsonProperty("gaugeColumns")]
        public int? GaugeColumns { get; set; } = null;


        public MultiGaugeNode AddFact(string category, decimal val, string valResolved = null) {
            var fact = new CategoryFact(val, valResolved, category);
            this.Facts.Add(fact);
            return this;
        }


        





    }

}
