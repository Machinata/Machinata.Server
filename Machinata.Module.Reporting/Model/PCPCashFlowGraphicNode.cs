﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    //
    [Serializable]
    public class PCPCashFlowGraphicNode : Node {


        [JsonProperty("buckets", NullValueHandling = NullValueHandling.Ignore)]
        public List<FactBucket> Buckets { get; set; } = new List<FactBucket>();

    }

    [Serializable]
    public class FactBucket {


        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Title { get; set; }

        [JsonProperty("bucketSymbol", NullValueHandling = NullValueHandling.Ignore)]
        public string BucketSymbol { get; set; }

        [JsonProperty("bucketStyle", NullValueHandling = NullValueHandling.Ignore)]
        public string BucketStyle { get; set; }

        [JsonProperty("textAlign", NullValueHandling = NullValueHandling.Ignore)]
        public string TextAlign { get; set; }

        [JsonProperty("facts", NullValueHandling = NullValueHandling.Ignore)]
        public List<CategoryFact> Facts { get; set; } = new List<CategoryFact>();

    }
}
