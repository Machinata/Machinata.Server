﻿using Machinata.Core.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    [Serializable]
    public abstract class GraphNode : ChartNode {

        public GraphNode() {
            this.SeriesGroups = new List<SerieGroup>();
        }

        [JsonProperty("seriesGroups")]
        public List<SerieGroup> SeriesGroups { get; set; }

        [JsonProperty("insertYZeroRule")]
        public bool InsertYZeroRule { get; set; } = true;

        [JsonProperty("autoSnapYAxis")]
        public bool AutoSnapYAxis { get; set; } = true;

        [JsonProperty("autoSnapYAxisSnapToZero")]
        public bool AutoSnapYAxisSnapToZero { get; set; } = true;

        [JsonProperty("autoSnapYAxisStategy")]
        public string AutoSnapYAxisStategy { get; set; } = "proportional";

        public SerieGroup AddSerieGroup() {
            var serieGroup = new SerieGroup();
            serieGroup.Id = "group" + this.SeriesGroups.Count;
            this.SeriesGroups.Add(serieGroup);
            return serieGroup;
        }

       

    }
}
