﻿using Machinata.Core.Model;
using Machinata.Core.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    [Serializable]
    public class DonutNode : CategoryChartNode {

        [JsonProperty("legendLabelsShowValue")]
        public bool LegendLabelsShowValue { get; set; } = false;

        [JsonProperty("legendLabelsShowCategory")]
        public bool LegendLabelsShowCategory { get; set; } = true;

        [JsonProperty("sliceLabelsShowValue")]
        public bool SliceLabelsShowValue { get; set; } = true;

        [JsonProperty("sliceLabelsShowCategory")]
        public bool SliceLabelsShowCategory { get; set; } = false;

        [JsonProperty("sortFacts", NullValueHandling = NullValueHandling.Ignore)]
        public string SortFacts { get; set; } = null; // can be ascending or descending

        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public ChartStatus Status { get; set; } = null;

        public override Node LoadSampleData() {
            CategorySerie serie = CreateSampleDataSerie();

            serie.Facts.AddRange(CategorySerie.CreateRandomCategorieFactsAddingToOne(8));
            this.Series.Add(serie);
            return this;
        }

        public Node LoadSampleData(List<string> categories, Random rnd=null) {
            CategorySerie serie = CreateSampleDataSerie();

            serie.Facts.AddRange(CategorySerie.CreateRandomCategorieFactsAddingToOne(categories, rnd));
            this.Series.Add(serie);
            return this;
        }

        public Node LoadSampleData( Random rnd = null) {
            CategorySerie serie = CreateSampleDataSerie();

            serie.Facts.AddRange(CategorySerie.CreateRandomCategorieFactsAddingToOne(8, rnd));
            this.Series.Add(serie);
            return this;
        }

        private CategorySerie CreateSampleDataSerie() {
            this.LegendLabelsShowValue = true;
            this.LegendLabelsShowCategory = true;
            this.SliceLabelsShowValue = false;
            this.SliceLabelsShowCategory = true;
            this.LegendColumns = 2;
            this.MainTitle = "Donut test";
            this.SubTitle = "Show value in legend, show category in slices";
            this.Series = new List<CategorySerie>();

            var serie = new CategorySerie();
            serie.Id = "s0";    
            serie.Title = "Series title";
            return serie;
        }

        [Obsolete]
        public DonutNode AddSlice(decimal val, string category) {
            if(this.Series.Count == 0) {
                this.Series.Add(new CategorySerie());
            }
            var slice = new CategoryFact(val, "", category);
            this.Series.Last().Facts.Add(slice);
            return this;
        }

        public CategoryChartNode AddSlicePercentage(decimal val, string category) {
            if (this.Series.Count == 0) {
                this.Series.Add(new CategorySerie());
            }

            var resolvedValue = val.ToString("P");

            var slice = new CategoryFact(val, resolvedValue, category, "number", ".0%", "percent: 1");
            this.Series.Last().Facts.Add(slice);
            return this;
        }


        public DonutNode AddSliceNumber(decimal val, string category, string colorShade = null) {
            if (this.Series.Count == 0) {
                this.Series.Add(new CategorySerie());
            }
            var slice = new CategoryFact(val, val.ToString(), category);
            slice.ColorShade = colorShade;
            this.Series.Last().Facts.Add(slice);
            return this;
        }


        public DonutNode AddSliceNumberResolved(decimal val, string category, string resolved, string colorShade= null) {
            if (this.Series.Count == 0) {
                this.Series.Add(new CategorySerie());
            }


            var serie = this.Series.Last();

           

            var slice = new CategoryFact(val, resolved, category);
            if (colorShade != null) {
                slice.ColorShade = colorShade;
            }

            serie.Facts.Add(slice);
            return this;
        }

        public DonutNode AddSliceFormatWithThousandsNoDecimal(decimal value, string category, string colorShade= null ) {
            var formatted = Number.FormatWithThousandsNoDecimal(value);
            return AddSliceNumberResolved(value, category, formatted, colorShade);
        }

        public void InsertGroupedEntitiesAsPercentageSlices<T>(IQueryable<T> entities, Func<T, string> groupBySelector, Func<T, decimal> sumSelector, decimal totalSum, int maxNumberOfGroups, Func<decimal, string> valueAsLabelFormatter = null) where T : ModelObject {
            var groups = entities.GroupBy(e => groupBySelector(e)).OrderByDescending(g => g.Sum(e => sumSelector(e)));
            if (groups.Any() && totalSum > 0) {
                foreach (var group in groups.Take(maxNumberOfGroups)) {
                    var groupSum = group.Sum(e => sumSelector(e));
                    var percent = groupSum / totalSum;
                    var key = "NA";
                    if (group.Key != null && group.Key != "") {
                        key = group.Key.ToString();
                    }
                    var groupSumFormatted = groupSum.ToString();
                    if (valueAsLabelFormatter != null) {
                        groupSumFormatted = valueAsLabelFormatter(groupSum);
                    }
                    this.AddSlicePercentage(percent, key + ": " + groupSumFormatted);
                }
            }

            // others 
            {
                var rest = groups.Skip(maxNumberOfGroups);
                // only if any
                if (rest.Any()) {
                    var groupSum = rest.Sum(e => e.Sum(e2 => sumSelector(e2)));
                    var percent = groupSum / totalSum;
                    var groupSumFormatted = groupSum.ToString();
                    if (valueAsLabelFormatter != null) {
                        groupSumFormatted = valueAsLabelFormatter(groupSum);
                    }
                    this.AddSlicePercentage(percent, "Others : " + groupSumFormatted);
                }

            }
        }

        public override int GetNumberOfLegendItems() {
            var total = 0;
            foreach(var s in this.Series) {
                if (s.Facts == null) continue;
                total += s.Facts.Count;
            }
            return total;
        }
    }
}
    