﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    [Serializable]
    public class InfoNode : Node {

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("infoType", NullValueHandling = NullValueHandling.Ignore)]
        public string InfoType { get; set; } = null;


    }
}
