﻿using Machinata.Core.Model;
using Machinata.Core.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {
    public class Report {

        public Report() {
            //this.ReportId = Core.Ids.GUID.Default.GenerateGUID();
            this.Body = new NoOpNode();
            this.Body.Children = new List<Node>();
        }

        [JsonProperty("reportId")]
        public string ReportId { get; set; }

        [JsonProperty("reportTitle")]
        public TranslatableString ReportTitle { get; set; }

        [JsonProperty("reportSubtitle")]
        public TranslatableString ReportSubtitle { get; set; }

        [JsonProperty("reportNotice")]
        public TranslatableString ReportNotice { get; set; }

        [JsonProperty("body")]
        public Node Body { get; set; }





        public Report SetTitle(string title) {
            this.ReportTitle = title;
            if (this.ReportId == null) this.ReportId = Core.Util.String.CreateShortURLForName(title);
            return this;
        }
        public Report SetSubtitle(string title) {
            this.ReportSubtitle = title;
            return this;
        }
        public Report SetNotice(string text) {
            this.ReportNotice = text;
            return this;
        }

        public SectionNode NewChapter(string title, string theme = "default") {
            var section = new SectionNode();
            section.SetTitle(title);
            section.Theme = theme;
            section.Id = title.ToDashedLower();
            AddToBody(section);
            return section;
        }




        /// <summary>
        /// Creates a new report with title and body
        /// </summary>
        /// <returns></returns>
        public static Report CreateReport(string title) {
            var report = new Report();
            report.SetTitle(title);
            report.Body = new NoOpNode();
            report.Body.Children = new List<Node>();
            return report;
        }


        public void AddToBody(Node node) {
            this.Body.Children.Add(node);
        }

    }

    public class ReportTitleClass {
        [JsonProperty("translations")]
        public Dictionary<string, string> Translations = new Dictionary<string, string>();
    }

}
