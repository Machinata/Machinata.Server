﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Machinata.Module.Reporting.Model {
    [Serializable]
    public class TileGridNode : Node {

        public TileGridNode() {
            this.Items = new List<TileGridNodeItem>();
        }

        [JsonProperty("items")]
        public List<TileGridNodeItem> Items { get; set; }

        [JsonProperty("tileColumns", NullValueHandling = NullValueHandling.Ignore)]
        public int? TileColumns { get; set; } = null;

        [JsonProperty("flipOnHover", NullValueHandling = NullValueHandling.Ignore)]
        public bool? FlipOnHover { get; set; } = null;

        [JsonProperty("zoomOnHover", NullValueHandling = NullValueHandling.Ignore)]
        public bool? ZoomOnHover { get; set; } = null;
    }

    [Serializable]
    public class TileGridNodeItem {

        [JsonProperty("front", NullValueHandling = NullValueHandling.Ignore)]
        public TileGridNodeItemSide Front { get; set; }

        [JsonProperty("back", NullValueHandling = NullValueHandling.Ignore)]
        public TileGridNodeItemSide Back { get; set; }

        [JsonProperty("tooltip", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Tooltip { get; set; }
    }
    [Serializable]
    public class TileGridNodeItemSide {

        [JsonProperty("html", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString HTML { get; set; }

        [JsonProperty("image", NullValueHandling = NullValueHandling.Ignore)]
        public string Image { get; set; }

        [JsonProperty("colorShade", NullValueHandling = NullValueHandling.Ignore)]
        public string ColorShade { get; set; }
    }
}