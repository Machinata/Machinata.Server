﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    [Serializable]
    public class LayoutNode : Node {

        public LayoutNode() {
            this.SlotChildren = new Dictionary<string, int>();
        }
      
        [JsonProperty("slotChildren")]
        public Dictionary<string, int> SlotChildren { get; set; }

        public LayoutNode AddChildToSlot(Node node, string slotKey) {
            base.AddChild(node);
            var slotNumber = this.SlotChildren.Count;
            if (SlotChildren.ContainsKey(slotKey)) {
                throw new Exception("LayoutNode already contains child with slotKey: " + slotKey);
            }
            this.SlotChildren.Add(slotKey, slotNumber);
            return this;
        }


        /// <summary>
        /// Makes a CS_W_Two_Columns Layout with a {left} and {right} child
        /// </summary>
        /// <returns></returns>
        public LayoutNode TwoColumns() {
            this.Style = "CS_W_Two_Columns";
            //var left = new NoOpNode();
            //this.AddChildToSlot(left, "{left}");
            //var right = new NoOpNode();
            //this.AddChildToSlot(right, "{right}");
            return this;
        }

        public LayoutNode AddLeft(Node node) {
            this.AddChildToSlot(node, "{left}");
            return this;
        }

        public LayoutNode AddRight(Node node) {
            this.AddChildToSlot(node, "{right}");
            return this;
        }



    }
}
