﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    [Serializable]
    public class ForceDirectedGraphNode : CategoryChartNode {

        public ForceDirectedGraphNode() {
            this.Links = new List<GraphLink>();
        }

        [JsonProperty("links")]
        public List<GraphLink> Links { get; set; }

    }
}
