﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_tables_.verticaltablenode.html
/// </summary>
namespace Machinata.Module.Reporting.Model {
    [Serializable]
    public class VerticalTableNode : ThemeableNode {

        public VerticalTableNode() {
        }

        public VerticalTableNode(string title, string subtitle) {
            this.MainTitle = title;
            this.SubTitle = subtitle;
        }

        [JsonProperty("allowColumnConfiguration", NullValueHandling = NullValueHandling.Ignore)]
        public bool? AllowColumnConfiguration { get; set; } = null;

        [JsonProperty("allowRowFiltering", NullValueHandling = NullValueHandling.Ignore)]
        public bool? AllowRowFiltering { get; set; } = null;

        [JsonProperty("showRowFiltering", NullValueHandling = NullValueHandling.Ignore)]
        public bool? ShowRowFiltering { get; set; } = null;

        [JsonProperty("allowRowSorting", NullValueHandling = NullValueHandling.Ignore)]
        public bool? AllowRowSorting { get; set; } = null;

        [JsonProperty("groupedRowsAreInteractive", NullValueHandling = NullValueHandling.Ignore)]
        public bool? GroupedRowsAreInteractive { get; set; } = null;

        [JsonProperty("nestedRowsAreInteractive", NullValueHandling = NullValueHandling.Ignore)]
        public bool? NestedRowsAreInteractive { get; set; } = null;

        [JsonProperty("maxColumnImportanceForDisplay")]
        public int? MaxColumnImportanceForDisplay { get; set; } = null;

        [JsonProperty("columnDimension")]
        public Dimension ColumnDimension { get; set; }

        [JsonProperty("rowDimension")]
        public Dimension RowDimension { get; set; }

        public DimensionGroup AddColumns(string groupName,string groupId, int groupImportance, string [] colums, ValueTypes [] columnTypes, GroupTypes? groupType = null, List<string> colorShades = null, string[] columnIds = null ) {
            
            if (this.ColumnDimension == null) {
                this.ColumnDimension = new Dimension();
            }

            if (columnTypes.Count() != colums.Count()) {
                throw new Exception("columns count is not equal to column Types count");
            }

            if (colorShades != null && colorShades.Count != colums.Count()) {
                throw new Exception("colorShades count is not equal to column count");
            }

            if (columnIds != null && columnIds.Count() != colums.Count()) {
                throw new Exception("columnIds count is not equal to column count");
            }

            var group = this.ColumnDimension.DimensionGroups.FirstOrDefault(dg => dg.Id == Id);

            var columns = this.ColumnDimension.DimensionGroups.Sum(d => d.DimensionElements.Count);

            if (group== null) {
                group = new DimensionGroup();
                if (groupType!= null) {
                    group.GroupTypeEnum = groupType.Value;
                }
                group.Name = groupName;
                group.Id = groupId;
                group.Importance = groupImportance;

                group.DimensionElements = new List<DimensionElement>();
                int i = 0;
                foreach(var colum in colums) {
                    var elem = new DimensionElement();
                    elem.Name = colum;
                    elem.ValueTypeEnum = columnTypes.ElementAt(i);
                    i++;
                    // Auto column ids
                    if (columnIds == null) {
                        elem.Id = "M" + (i + columns);
                    } else {
                        elem.Id = columnIds.ElementAt(i-1);
                    }
                    elem.Importance = groupImportance;

                    if (colorShades != null) {
                        elem.ColorShade = colorShades.ElementAt(i-1);
                    }

                    group.DimensionElements.Add(elem);
                }

                this.ColumnDimension.DimensionGroups.Add(group);

            }

            return group;
        }   


        public DimensionGroup AddRowGroup(string groupName, string groupId, int groupImportance) {



            if (this.RowDimension == null) {
                this.RowDimension = new Dimension();
            }

            var group = this.RowDimension.DimensionGroups.FirstOrDefault(dg => dg.Id == Id);
            if (group == null) {
                group = new DimensionGroup();
                group.Name = groupName;
                group.Id = groupId;
                group.Importance = groupImportance;

                group.DimensionElements = new List<DimensionElement>();
             

                this.RowDimension.DimensionGroups.Add(group);

            }

            return group;


        }

       
    }

   
}
