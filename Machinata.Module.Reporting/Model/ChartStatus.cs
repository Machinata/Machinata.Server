﻿using Newtonsoft.Json;
using System;

namespace Machinata.Module.Reporting.Model {

    /// <summary>
    /// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_charts_.categoryfact.html
    /// </summary>
    [Serializable]
    public class ChartStatus {

        public ChartStatus() {
        }
        public ChartStatus(string title, string subTitle = null, string tooltip = null) {
            if (title != null) this.Title = title;
            if (subTitle != null) this.SubTitle = subTitle;
            if (tooltip != null) this.Tooltip = tooltip;
        }


        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Title { get; set; }

        [JsonProperty("subTitle", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString SubTitle { get; set; }

        [JsonProperty("tooltip", NullValueHandling = NullValueHandling.Ignore)]
        public TranslatableString Tooltip { get; set; }

    }

}
