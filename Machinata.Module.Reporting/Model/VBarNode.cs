﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    //https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_charts_.vbarnode.html
    [Serializable]
    public class VBarNode : CategoryChartNode {

        [JsonProperty("xAxisShowTooltipsInLabel")]
        public bool? XAxisShowTooltipsInLabel { get; set; } = null;


        // hierarchy: Wrong in docu link
        [JsonProperty("stackedSeries", NullValueHandling = NullValueHandling.Ignore)]
        public bool? StackedSeries { get; set; } = null;

        [JsonProperty("stackedSeriesLabels", NullValueHandling = NullValueHandling.Ignore)]
        public bool? StackedSeriesLabels { get; set; } = null;

        [JsonProperty("stackedSeriesLabelsShowTitle", NullValueHandling = NullValueHandling.Ignore)]
        public bool? StackedSeriesLabelsShowTitle { get; set; } = null;

        [JsonProperty("stackedSeriesLabelsShowValue", NullValueHandling = NullValueHandling.Ignore)]
        public bool? StackedSeriesLabelsShowValue { get; set; } = null;

        public override Node LoadSampleData() {
            return LoadSampleData();
        }

        public VBarNode LoadSampleData(int bars = 15, double minValue = -0.04, double maxValue = 0.1) {
          
            this.MainTitle = "V-Bar Test  (random)";
            this.SubTitle = "";
            this.Series = new List<CategorySerie>();
            var serie = new CategorySerie();

            serie.Id = "serie1";
            serie.Title = "Title";
            serie.ColorShade = "bright";
            serie.Facts.AddRange(CategorySerie.CreateRandomCategorieFacts(bars, minValue, maxValue));
            this.Series.Add(serie);
            return this;
        }

        //public VBarNode AddPercentageSerie(decimal val, string category) {

        //    var serie = new CategorySerie();
        //    this.Series.Add(serie);


        //    var resolvedValue = val.ToString("P");

        //    var slice = new CategoryFact(val, resolvedValue, category, "number", ".0%", "percent: 1");
        //    this.Series.Last().Facts.Add(slice);
        //    return this;

        //}


       
    }
}
