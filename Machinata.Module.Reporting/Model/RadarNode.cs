﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Model {

    [Serializable]
    public class RadarNode : CategoryChartNode {

        public override Node LoadSampleData() {
            return LoadSampleData();
        }

        public RadarNode LoadSampleData(int bars = 15, double minValue = -0.04, double maxValue = 0.1) {
          
            this.MainTitle = "Radar Test (random)";
            this.SubTitle = "";
            this.Series = new List<CategorySerie>();
            {
                var serie = new CategorySerie();
                serie.Id = "serie1";
                serie.Title = "Serie 1";
                serie.ColorShade = "bright";
                serie.Facts.AddRange(CategorySerie.CreateRandomCategorieFacts(bars, minValue, maxValue));
                this.Series.Add(serie);
            }
            {
                var serie = new CategorySerie();
                serie.Id = "serie2";
                serie.Title = "Serie 2";
                serie.ColorShade = "dark";
                serie.Facts.AddRange(CategorySerie.CreateRandomCategorieFacts(bars, minValue, maxValue));
                this.Series.Add(serie);
            }
            return this;
        }

       
    }
}
