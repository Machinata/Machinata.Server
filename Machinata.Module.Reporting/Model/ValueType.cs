﻿using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_api_src_model_values_.valuetype.html
/// </summary>
namespace Machinata.Module.Reporting.Model {
    public enum ValueTypes {

        Amount,
        BP,
        Count,
        Date,
        FxRate,
        MV,
        MV_M,
        MV_T,
        MV_M2D,
        Month,
        PAbs,
        PL,
        PRel,
        Percent,
        Text,
        Year
    }

    public class ValueTypesHelper {

        /// <summary>
        /// Mapping of value type to js valuetype name
        /// </summary>
        /// <param name="valueType"></param>
        /// <returns></returns>
        public static string ValueTypeMapping(ValueTypes? valueType) {

            if (valueType == null) {
                return null;
            }

            if (valueType == ValueTypes.Amount) {
                return "amount";
            } else if (valueType == ValueTypes.BP) {
                return "bp";
            } else if (valueType == ValueTypes.Count) {
                return "count";
            } else if (valueType == ValueTypes.Date) {
                return "date";
            } else if (valueType == ValueTypes.FxRate) {
                return "fxrate";
            } else if (valueType == ValueTypes.MV) {
                return "mv";
            } else if (valueType == ValueTypes.MV_M) {
                return "mv_m";
            } else if (valueType == ValueTypes.MV_T) {
                return "mv_t";
            } else if (valueType == ValueTypes.MV_T) {
                return "mv_m2d";
            } else if (valueType == ValueTypes.Month) {
                return "month";
            } else if (valueType == ValueTypes.PAbs) {
                return "p-abs";
            } else if (valueType == ValueTypes.PL) {
                return "pl";
            } else if (valueType == ValueTypes.PRel) {
                return "p-rel";
            } else if (valueType == ValueTypes.Percent) {
                return "percent";
            } else if (valueType == ValueTypes.Text) {
                return "text";
            } else if (valueType == ValueTypes.Year) {
                return "year";
            }

            return valueType.ToString().ToDashedLower();

            //TODO : DO EXPLICIT MAPPINGS FOR EACH TYPE
        }


    }
}
