﻿using Machinata.Core.Handler;
using Machinata.Module.Reporting.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Extensions {
    public static class HandlerExtensions {

        /// <summary>
        /// Sends a Reporting Node with the correct serialization method to the API
        /// in test environments or if debug==true it will indend the json
        /// </summary>
        /// <param name="handler"></param>
        /// <param name="node"></param>
        public static void SendReportingAPIMessage(this Core.Handler.APIHandler handler, Model.Node node) {
            SendAPIMessage(handler, node);
        }

        /// <summary>
        /// Sends a Reporting Report with the correct serialization method to the API
        /// in test environments or if debug==true it will indend the json
        /// </summary>
        /// <param name="handler"></param>
        /// <param name="node"></param>
        public static void SendReportingAPIMessage(this Core.Handler.APIHandler handler, Model.Report report) {
            SendAPIMessage(handler, report);
        }

        private static void SendAPIMessage(APIHandler handler, object node) {
            var indend = Core.Config.IsTestEnvironment;

            if (handler.Params.Bool("debug", false)) {
                indend = true;
            }
            string jsonPayload = Core.JSON.Serialize(JObject.FromObject(node), true, false, indend);
            handler.SendRawAPIMessage("node-data", jsonPayload, false);
        }
    }
}



