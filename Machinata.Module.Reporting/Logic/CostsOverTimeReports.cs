﻿using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Module.Reporting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting.Logic {
    public static class CostsOverTimeReports {


        // These are deprecated: use Config.xyz formats instead
       // public const string Y_AXIS_NUMBER_FORMAT_NO_DECIMALS = ",.2r";
       // public const string X_AXIS_CALENDER_WEEKFORMAT = "%V";


        /// <summary>
        /// Vbar Node
        /// HINT entities has to a Queryable but already loaded
        /// Possible Improvment:  some kind of expression has to be used
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities"></param>
        /// <param name="dateRange"></param>
        /// <param name="price"></param>
        /// <param name="accumulate"></param>
        /// <returns></returns>
        public static LineColumnNode GetMonthlyCosts<T>(IQueryable<T> entities, string title, Func<T, DateTime?> dateTime, Func<T, decimal?> price, string unit, bool? accumulate = null, decimal? burnRate = null) where T : ModelObject {

            var node = new LineColumnNode();
            node.RandomID();
            node.SetTitle(title);
            node.SetSubTitle("by Month" + GetUnitSuffix(unit));
            node.Theme = "default";


            // Toggle 
            if (accumulate == true) {
                node.ToggleTitle = "Accumulated";
            } else {
                node.ToggleTitle = title;
            }

            var serie = CreateMonthlyCostsSerie(entities, title, dateTime, price, accumulate, burnRate);
            var group = node.AddSerieGroup();
            group.AddColumnSerie(serie);
            //node.Series.Add(serie);

            group.XAxisMonths();
            group.YAxisNumberThousands();

            return node;


        }

        /// <summary>
        /// Vbar Node
        /// HINT entities has to a Queryable but already loaded
        /// Possible Improvment:  some kind of expression has to be used
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities"></param>
        /// <param name="dateRange"></param>
        /// <param name="price"></param>
        /// <param name="accumulate"></param>
        /// <returns></returns>
        public static LineColumnNode GetDaily<T>(IQueryable<T> entities, string title, Func<T, DateTime?> dateTime, Func<T, decimal?> amount, string unit, bool? accumulate = null, decimal? burnRate = null, bool lineSeries = false) where T : ModelObject {

            var node = new LineColumnNode();
            node.RandomID();
            node.SetTitle(title);
            node.SetSubTitle("by Day" + GetUnitSuffix(unit));
            node.Theme = "default";



            // Toggle 
            if (accumulate == true) {
                node.ToggleTitle = "Accumulated";
            } else {
                node.ToggleTitle = title;
            }

            var serie = CreateDailyCostsSerie(entities, title, dateTime, amount, accumulate, burnRate);
            var group = node.AddSerieGroup();

            if (lineSeries == false) {
                group.AddColumnSerie(serie);
            } else {
                group.AddLineSerie(serie);
            };


            group.XAxisDays();
           
            group.YAxisNumberThousands();
         

            return node;


        }



        public static Serie CreateMonthlyCostsSerie<T>(IQueryable<T> entities, string title, Func<T, DateTime?> dateTime, Func<T, decimal?> price, bool? accumulate, decimal? burnRate = null) where T : ModelObject {
            var serie = new Serie();
            serie.Title = title;
            serie.Id = "serie" + title;
         
            var costs = entities.Where(c => dateTime(c) != null && dateTime(c) > DateTime.MinValue);


            var accumulated = 0m;
            if (costs.Any(c => dateTime(c) != null && dateTime(c).HasValue == true)) {
                var minDateLocal = costs.Min(c => dateTime(c).Value).ToDefaultTimezone().StartOfMonth();
                var maxDateLocal = costs.Max(c => dateTime(c).Value).ToDefaultTimezone();
                for (DateTime date = minDateLocal; date <= maxDateLocal; date = date.AddMonths(1)) {

                    var startUTC = date.ToUniversalTime();
                    var endUTC = date.EndOfMonth().ToUniversalTime();

                    var costsMonth = costs.Where(c => dateTime(c) >= startUTC && dateTime(c) <= endUTC);

                    var costsMonthSum = costsMonth.Where(w => price(w).HasValue).Sum(w => price(w));
                    if (costsMonthSum == null) {
                        costsMonthSum = 0m;
                    }

                    
                    if (burnRate != null) {
                        costsMonthSum = costsMonthSum - burnRate;
                    }

                    var costsMonthSumDecimal = costsMonthSum.Value;

                    if (accumulate == true) {
                        costsMonthSumDecimal += accumulated;
                        accumulated += costsMonthSum.Value;
                    }

                    serie.AddFact(date, costsMonthSumDecimal);

                }
            }

            return serie;
        }

        public static CategorySerie CreateMonthlyCostsCatogorieSerie<T>(IQueryable<T> entities, string title, Func<T, DateTime?> dateTime, Func<T, decimal?> price, bool? accumulate, decimal? burnRate = null) where T : ModelObject {
            var serie = new CategorySerie();
            serie.Title = title;
            serie.Id = "serie" + title;

            var costs = entities.Where(c => dateTime(c) != null && dateTime(c) > DateTime.MinValue);


            var accumulated = 0m;
            if (costs.Any(c => dateTime(c) != null && dateTime(c).HasValue == true)) {
                var minDateLocal = costs.Min(c => dateTime(c).Value).ToDefaultTimezone().StartOfMonth();
                var maxDateLocal = costs.Max(c => dateTime(c).Value).ToDefaultTimezone();
                for (DateTime date = minDateLocal; date <= maxDateLocal; date = date.AddMonths(1)) {

                    var startUTC = date.ToUniversalTime();
                    var endUTC = date.EndOfMonth().ToUniversalTime();

                    var costsMonth = costs.Where(c => dateTime(c) >= startUTC && dateTime(c) <= endUTC);

                    var costsMonthSum = costsMonth.Where(w => price(w).HasValue).Sum(w => price(w));
                    if (costsMonthSum == null) {
                        costsMonthSum = 0m;
                    }


                    if (burnRate != null) {
                        costsMonthSum = costsMonthSum - burnRate;
                    }

                    var costsMonthSumDecimal = costsMonthSum.Value;

                    if (accumulate == true) {
                        costsMonthSumDecimal += accumulated;
                        accumulated += costsMonthSum.Value;
                    }

                    // serie.AddFact(date, costsMonthSumDecimal);
                    serie.AddFactFormatWithThousandsNoDecimal(costsMonthSumDecimal, date.ToString(Config.DateFormatNet));

                }
            }

            return serie;
        }

        /// <summary>
        /// Todo rename
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities"></param>
        /// <param name="title"></param>
        /// <param name="dateTime"></param>
        /// <param name="amount"></param>
        /// <param name="accumulate"></param>
        /// <param name="burnRate"></param>
        /// <returns></returns>
        public static Serie CreateDailyCostsSerie<T>(IQueryable<T> entities, string title, Func<T, DateTime?> dateTime, Func<T, decimal?> amount, bool? accumulate, decimal? burnRate = null) where T : ModelObject {
            var serie = new Serie();
            serie.Title = title;
            serie.Id = "serie" + title;
            var costs = entities.Where(c => dateTime(c) != null && dateTime(c) > DateTime.MinValue);


            var accumulated = 0m;
            if (costs.Any(c => dateTime(c) != null && dateTime(c).HasValue == true)) {
                var minDateLocal = costs.Min(c => dateTime(c).Value).ToDefaultTimezone().StartOfDay();
                var maxDateLocal = costs.Max(c => dateTime(c).Value).ToDefaultTimezone();
                for (DateTime date = minDateLocal; date <= maxDateLocal; date = date.AddDays(1)) {

                    var startUTC = date.ToUniversalTime();
                    var endUTC = date.EndOfDay().ToUniversalTime();

                    var costsTimeSpan = costs.Where(c => dateTime(c) >= startUTC && dateTime(c) <= endUTC);
                 
                    var costTimeSpanSum = costsTimeSpan.Where(w => amount(w).HasValue).Sum(w => amount(w));
                    var costTimeSpanSumDecimal = costTimeSpanSum.Value;

                    if (burnRate.HasValue == true) {
                        costTimeSpanSum = costTimeSpanSum - burnRate;
                    }

                    if (accumulate == true) {
                        costTimeSpanSumDecimal += accumulated;
                        accumulated += costTimeSpanSum.Value;
                    }

                    //serie.AddFact(costsMonthSumDecimal, Number.FormatWithThousandsNoDecimal(costsMonthSumDecimal), date.ToMonthString());
                    serie.AddFact(startUTC, costTimeSpanSumDecimal);
                    

                }
            }

            return serie;
        }


        public static CategorySerie CreateDailyCategorySerie<T>(IQueryable<T> entities, string title, Func<T, DateTime?> dateTime, Func<T, decimal?> amount, bool? accumulate, decimal? burnRate = null) where T : ModelObject {
            var serie = new CategorySerie();
            serie.Title = title;
            serie.Id = "serie" + title;
            serie.YAxis = new Axis();
            serie.YAxis.Format = Config.NumberThousandsFormatD3;
            serie.YAxis.FormatType = "number";
            var costs = entities.Where(c => dateTime(c) != null && dateTime(c) > DateTime.MinValue);


            var accumulated = 0m;
            if (costs.Any(c => dateTime(c) != null && dateTime(c).HasValue == true)) {
                var minDateLocal = costs.Min(c => dateTime(c).Value).ToDefaultTimezone().StartOfDay();
                var maxDateLocal = costs.Max(c => dateTime(c).Value).ToDefaultTimezone();
                for (DateTime date = minDateLocal; date <= maxDateLocal; date = date.AddDays(1)) {

                    var startUTC = date.ToUniversalTime();
                    var endUTC = date.EndOfDay().ToUniversalTime();
                    var startLocal = Core.Util.Time.ToDefaultTimezone(startUTC);

                    var costsTimeSpan = costs.Where(c => dateTime(c) >= startUTC && dateTime(c) <= endUTC);

                    var costTimeSpanSum = costsTimeSpan.Where(w => amount(w).HasValue).Sum(w => amount(w));
                    var costTimeSpanSumDecimal = costTimeSpanSum.Value;

                    if (burnRate.HasValue == true) {
                        costTimeSpanSum = costTimeSpanSum - burnRate;
                    }

                    if (accumulate == true) {
                        costTimeSpanSumDecimal += accumulated;
                        accumulated += costTimeSpanSum.Value;
                    }


                    serie.AddFactFormatWithThousandsNoDecimal(costTimeSpanSumDecimal, startLocal.ToString(Config.DateFormatNet)); 

                    //serie.AddFactFormatWithThousandsNoDecimal(costTimeSpanSumDecimal, startLocal.ToDateTimeString());
                    //serie.AddFact(costsMonthSumDecimal, Number.FormatWithThousandsNoDecimal(costsMonthSumDecimal), date.ToMonthString());
                    // serie.AddFact(date, costTimeSpanSumDecimal);



                }
            }

            return serie;
        }


        public static LineColumnNode GetQuarterlyCosts<T>(IQueryable<T> entities, string title, Func<T, DateTime?> dateTime, Func<T, decimal?> amount, string unit, bool? accumulate = null, DateTime? endDate = null, decimal? burnRate = null) where T : ModelObject {

            var node = new LineColumnNode();
            node.SetTitle(title);
            node.SetSubTitle("by Quarter" + GetUnitSuffix(unit));
            node.RandomID();
            node.Theme = "default";

            // Toggle 
            if (accumulate == true) {
                node.ToggleTitle = "Accumulated";
            } else {
                node.ToggleTitle = title;
            }
                     
            var serie = CreateQuarterlyCostsSerie(entities, title, dateTime, amount, accumulate, endDate, burnRate);
            var group = node.AddSerieGroup();
            group.XAxisQuarters();
            group.YAxisNumberThousands();
            group.AddColumnSerie(serie);

            return node;


        }

        public static Serie CreateQuarterlyCostsSerie<T>(IQueryable<T> entities, string title, Func<T, DateTime?> dateTime, Func<T, decimal?> amount, bool? accumulate, DateTime? endDate, decimal? burnRate = null) where T : ModelObject {
            var serie = new Serie();
            serie.Title = title;
            serie.Id = "serie" + title;
            var debug = new StringBuilder();
           
         
            var costs = entities.Where(c => dateTime(c) != null && dateTime(c) > DateTime.MinValue);
            var accumulated = 0m;
            if (costs.Any(c => dateTime(c) != null && dateTime(c).HasValue == true)) {
                var minDateLocal = costs.Min(c => dateTime(c).Value).ToDefaultTimezone().StartOfQuarter();
                var maxDateLocal = costs.Max(c => dateTime(c).Value).ToDefaultTimezone();

                // future Date with no data?
                if (endDate != null) {
                    var endDateLocal = endDate.Value.ToDefaultTimezone();
                    if (maxDateLocal < endDateLocal) {
                        maxDateLocal = endDateLocal;
                    }
                }

                for (DateTime date = minDateLocal; date <= maxDateLocal; date = date.AddMonths(3)) {

                    var startUTC = date.ToUniversalTime();
                    var endUTC = date.EndOfQuarterTime().ToUniversalTime();
                    var costsQuarter = costs.Where(c => dateTime(c) >= startUTC && dateTime(c) <= endUTC);
                    debug.AppendLine("slice: " + Config.GetQuarterFormat(date));
                    debug.AppendLine("startUTC: " + startUTC);
                    debug.AppendLine("endUTC: " + endUTC);
                    debug.AppendLine("entities: " + string.Join(", ", costsQuarter));
                    debug.AppendLine();
                  



                    var costsQuarterSum = costsQuarter.Where(w => amount(w).HasValue).Sum(w => amount(w));
                    if (costsQuarterSum == null) {
                        costsQuarterSum = 0m;
                    }

                    if (burnRate != null) {
                        costsQuarterSum = costsQuarterSum - burnRate;
                    }

                    var costsQuarterSumDecimal = costsQuarterSum.Value;

                    if (accumulate == true) {
                        costsQuarterSumDecimal += accumulated;
                        accumulated += costsQuarterSum.Value;
                    }

                    //serie.AddFact( date, costsMonthSumDecimal, null, date.Year + " Q" + date.QuarterNumber());
                    serie.AddFact(date, costsQuarterSumDecimal, null, Config.GetQuarterFormat(date));


                }
            }
            serie.Debug = debug.ToString();
            return serie;
        }


        public static Serie CreateBurnRateLine(string title, DateTime start, DateTime end, Price burnRate = null)  {
            var serie = new Serie();
            serie.Title = title;
            serie.Id = "serie" + title;
            serie.ChartType = "line";
            serie.AddFact(start, burnRate.Value.Value);
            serie.AddFact(end, burnRate.Value.Value);


            return serie;
        }

        /// <summary>
        /// VBar Node
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities"></param>
        /// <param name="dateRange"></param>
        /// <param name="price"></param>
        /// <param name="accumulate"></param>
        /// <returns></returns>
        public static VBarNode GetWeeklyCosts<T>(IQueryable<T> entities, string title, Func<T, DateTime?> dateTime, Func<T, decimal?> amount, string unit, bool? accumulate = null) where T : ModelObject {

            var vbar = new VBarNode();
            vbar.SetTitle(title);
            vbar.SetSubTitle("by Week" + GetUnitSuffix(unit));
            vbar.Theme = "default";
           

            // Toggle 
            if (accumulate == true) {
                vbar.ToggleTitle = "Accumulated";
            } else {
                vbar.ToggleTitle = title;
            }

            entities = entities.Where(c => dateTime(c) != null);

            var  serie =  CreateWeeklyCategorySerie(entities, title, dateTime, amount, accumulate);
            serie.YAxis.Format = Config.NumberThousandsFormatD3;
            serie.YAxis.FormatType = "number";
            vbar.Series.Add(serie);

            return vbar;

        }

        public static CategorySerie CreateWeeklyCategorySerie<T>(IQueryable<T> entities, string title, Func<T, DateTime?> dateTime, Func<T, decimal?> amount, bool? accumulate = false) where T : ModelObject {
            var serie = new CategorySerie();
            serie.Title = title;
            serie.Id = "serie" + title;
            serie.YAxis = new Axis();
            serie.YAxis.Format = Config.NumberThousandsFormatD3;
            //serie.XAxis = new Axis();
            //serie.XAxis.Format = X_AXIS_CALENDER_WEEKFORMAT;

            if (entities.Any(c => dateTime(c) != null && dateTime(c).HasValue == true)) {
                var minDateLocal = entities.Min(c => dateTime(c).Value).ToDefaultTimezone().StartOfWeek();
                var maxDateLocal = entities.Max(c => dateTime(c).Value).ToDefaultTimezone();

                var accumulated = 0m;

                for (DateTime date = minDateLocal; date <= maxDateLocal; date = date.AddDays(7)) {

                    var startUTC = date.ToUniversalTime();
                    var endUTC = date.EndOfWeek().ToUniversalTime();

                    var costsWeek = entities.Where(c => dateTime(c) >= startUTC && dateTime(c) <= endUTC);

                    var costsWeekSum = costsWeek.Where(w => amount(w).HasValue).Sum(w => amount(w));
                    var costsWeekSumDecimal = costsWeekSum.Value;

                    if (accumulate == true) {
                        costsWeekSumDecimal += accumulated;
                        accumulated += costsWeekSum.Value;
                    }


                    // serie.AddFactFormatWithThousandsNoDecimal(costsWeekSumDecimal, date.GetIso8601WeekYear(yearFormat: "yy"));
                    serie.AddFactFormatWithThousandsNoDecimal(costsWeekSumDecimal, Config.GetWeekYearFormat(date));

                }
            }
            return serie;
        }

        public static Serie CreateWeeklySerie<T>(IQueryable<T> entities, string title, Func<T, DateTime?> dateTime, Func<T, decimal?> amount, bool? accumulate = false) where T : ModelObject {
            var serie = new Serie();
            serie.Title = title;
            serie.Id = "serie" + title;
            
         

            if (entities.Any(c => dateTime(c) != null && dateTime(c).HasValue == true)) {
                var minDateLocal = entities.Min(c => dateTime(c).Value).ToDefaultTimezone().StartOfWeek();
                var maxDateLocal = entities.Max(c => dateTime(c).Value).ToDefaultTimezone();

                var accumulated = 0m;

                for (DateTime date = minDateLocal; date <= maxDateLocal; date = date.AddDays(7)) {

                    var startUTC = date.ToUniversalTime();
                    var endUTC = date.EndOfWeek().ToUniversalTime();

                    var costsWeek = entities.Where(c => dateTime(c) >= startUTC && dateTime(c) <= endUTC);

                    var costsWeekSum = costsWeek.Where(w => amount(w).HasValue).Sum(w => amount(w));
                    var costsWeekSumDecimal = costsWeekSum.Value;

                    if (accumulate == true) {
                        costsWeekSumDecimal += accumulated;
                        accumulated += costsWeekSum.Value;
                    }


                    

                    //serie.AddFactFormatWithThousandsNoDecimal(costsWeekSumDecimal, date.GetIso8601WeekYear());
                    serie.AddFact(date, costsWeekSumDecimal, null, Config.GetWeekYearFormat(date));

                }
            }
            return serie;
        }


        public static string GetUnitSuffix(string unit = null) {
            return ", in " + unit;
        }

        /// <summary>
        /// e.g. W11 2022 
        /// </summary>
        /// <returns></returns>
        public static string GetIso8601WeekYear(this DateTime time, string yearFormat = "yyyy") {
            var week = time.GetIso8601WeekOfYear();
            var year = time.ToString(yearFormat);
            return $"W{week} {year}";
        }

      
    }
}
