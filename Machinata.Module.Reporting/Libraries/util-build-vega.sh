#!/bin/sh



sh util-build-d3format.sh



echo ""
echo "============================================================="
echo "Removing vega/node_modules/d3-format..."
echo "============================================================="

rm -rf dankrusi/vega/node_modules/d3-format


echo ""
echo "============================================================="
echo "Installing bmpi/d3-format into vega/node_modules/"
echo "============================================================="

cp -r bmpi/d3-format dankrusi/vega/node_modules




echo ""
echo "============================================================="
echo "Building vega..."
echo "============================================================="
echo ""
echo "IMPORTANT: Make sure dankrusi/vega submodule is on dk/machinata-reporting-final branch!" 

cd dankrusi/vega
yarn
yarn build
cd ..
cd ..

