


Note: You must do a git submodule update command:
git submodule update




## bmpi Custom d3-format (bmpi/d3-format)

Contains some extra (custom) format specifiers, via https://bitbucket.org/bmpi/ngr/src/master/d3-format/





## Vega Submodule (dankrusi/vega)

### Branches:

```machinata-reporting-final```: The branch to use to build and copy over vega (this is the final version for Machinata). This should include all upstream fixes and patches, as well as specific patches that are only for Machinata
```features-and-fixes-for-upstream```: The branch to use to push to upstream vega.

IMPORTANT: Make sure dankrusi/vega submodule is on dk/machinata-reporting-final branch when building vega!

### Feature Patches (not for upstream!):

```feature-legend-leftbottom```
```feature-small-font-metrics-measurements```
```feature-allow-nodejs-canvas-global```
```feature-strict-nodejs-canvas-validation```




## Vega Tooltips Submodule (dankrusi/vega-tooltip)

No longer integrated - all updates are in vega tooltip master on github!


## D3 Submodule (dankrusi/d3)

Work in progress
See https://stackoverflow.com/questions/40399554/how-to-bundle-d3-v4-using-webpack on building d3