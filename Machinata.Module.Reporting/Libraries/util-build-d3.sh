#!/bin/sh



sh util-build-d3format.sh



echo ""
echo "============================================================="
echo "Removing d3/node_modules/d3-format..."
echo "============================================================="

rm -rf dankrusi/d3/node_modules/d3-format


echo ""
echo "============================================================="
echo "Installing bmpi/d3-format into d3/node_modules/"
echo "============================================================="

cp -r bmpi/d3-format dankrusi/d3/node_modules




echo ""
echo "============================================================="
echo "Building vega..."
echo "============================================================="
echo ""
echo "IMPORTANT: Make sure dankrusi/d3 submodule is on dk/machinata-reporting-final branch!" 

cd dankrusi/d3
yarn
yarn build
cd ..
cd ..

