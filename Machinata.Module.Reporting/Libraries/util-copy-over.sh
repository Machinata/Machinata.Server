#!/bin/sh

echo ""
echo "============================================================="
echo "Copying libraries to machinata-reporting-bundle.js..." 
echo "============================================================="
read -p "Press enter to continue"


echo "Copying vega.js to machinata-reporting-bundle.js..." 
cp -f dankrusi/vega/packages/vega/build/vega.js ../Static/js/machinata-reporting-bundle.js/

echo "Copying vega.min.js to machinata-reporting-bundle.js..." 
cp -f dankrusi/vega/packages/vega/build/vega.min.js ../Static/js/machinata-reporting-bundle.js/

echo "Copying vega-interpreter.js to machinata-reporting-bundle.js..." 
cp -f dankrusi/vega/packages/vega-interpreter/build/vega-interpreter.js ../Static/js/machinata-reporting-bundle.js/

echo "Copying vega-interpreter.min.js to machinata-reporting-bundle.js..." 
cp -f dankrusi/vega/packages/vega-interpreter/build/vega-interpreter.min.js ../Static/js/machinata-reporting-bundle.js/

echo "Copying d3-format.js to machinata-reporting-bundle.js..." 
cp -f dankrusi/vega/node_modules/d3-format/dist/d3-format.js ../Static/js/machinata-reporting-bundle.js/

echo "Copying d3-format.min.js to machinata-reporting-bundle.js..." 
cp -f dankrusi/vega/node_modules/d3-format/dist/d3-format.min.js ../Static/js/machinata-reporting-bundle.js/

echo "Copying d3-time-format.js to machinata-reporting-bundle.js..." 
cp -f dankrusi/vega/node_modules/d3-time-format/dist/d3-time-format.js ../Static/js/machinata-reporting-bundle.js/

echo "Copying d3-time-format.min.js to machinata-reporting-bundle.js..." 
cp -f dankrusi/vega/node_modules/d3-time-format/dist/d3-time-format.min.js ../Static/js/machinata-reporting-bundle.js/