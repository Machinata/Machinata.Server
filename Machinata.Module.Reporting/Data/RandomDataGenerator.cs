﻿using Machinata.Core.Util;
using Machinata.Module.Reporting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Machinata.Module.Reporting.Model.Report;

namespace Machinata.Module.Reporting.Data {
    public class RandomDataGenerator {
        private static Random _rnd = new System.Random((int)DateTime.Now.Ticks);

        public static Report ChartTypes() {

            Report report = CreateReport("Chart Type Examples");

            {
                var section = CreateSectionNode("Line Columns");
                section.Children.Add(new LineColumnNode().LoadSampleData());
                report.AddToBody(section);
               
            }
            {
                var section = CreateSectionNode("Donut");
                section.Children.Add(new DonutNode().LoadSampleData());
                report.AddToBody(section);
            }
            {
                var section = CreateSectionNode("Pie");
                section.Children.Add(new PieNode().LoadSampleData());
                report.AddToBody(section);
            }
            {
                var section = CreateSectionNode("HBar");
                section.Children.Add(new HBarNode().LoadSampleData());
                report.AddToBody(section);
            }
            {
                var section = CreateSectionNode("VBar");
                section.Children.Add(new VBarNode().LoadSampleData());
                report.AddToBody(section);
            }
            {
                var section = CreateSectionNode("Gauges");
                section.Children.Add(new GaugeNode().LoadSampleData());
                report.AddToBody(section);
            }
            {
                var section = CreateSectionNode("Symboled VBars");
                section.Children.Add(new SymboledVBarNode().LoadSampleData());
                report.AddToBody(section);
            }
            {
                var section = CreateSectionNode("Offline Map");
                section.Children.Add(new OfflineMapNode().LoadSampleData());
                report.AddToBody(section);
            }

            return report;

        }

      
        public static Dictionary<string,decimal> CreateRandomDistributionTo100Percent(IEnumerable<string> items) {
            var distribution = new Dictionary<string, decimal>();
            foreach (var region in items) {
                distribution[region] = _rnd.Next(1, 10);
            }
            var total = distribution.Sum(e => e.Value);
            foreach (var region in items) {
                distribution[region] = distribution[region] / total;
            }
            return distribution;
        }
      

        public static Dictionary<string, decimal> CreateRandomDistributionTo(IEnumerable<string> items, decimal expectedTotal ) {
            var distribution = new Dictionary<string, decimal>();
            foreach (var region in items) {
                distribution[region] = _rnd.Next(1, 10);
            }
            var total = distribution.Sum(e => e.Value);

            decimal totalTrack = 0m;
            foreach (var region in items) {
                var distr = System.Math.Round(distribution[region] / total,2);

                if (region == items.Last()) {
                    distribution[region] = expectedTotal - totalTrack;
                } else {
                    distribution[region] = distr;
                    totalTrack += distr;
                }



            }

           
            return distribution;
        }


        public static SectionNode CreateSectionNode(string title) {
            var chapter = new SectionNode();
            chapter.MainTitle = new TranslatableString(title, "BR_PO_Title");
            chapter.Theme = "blue";
            chapter.Children = new List<Node>();

            var infoNode = new InfoNode();
            infoNode.Message = $"This is the chapter " + title;
            chapter.Children.Add(infoNode);
            return chapter;
        }

        public static string GetRandomSector() {
            var sectors = @"Electronic Equipment
Money Center Banks
Catalog & Mail Order Houses
Telecom Services
Internet Information Providers
Major Airlines
Aluminum
Credit Services
Agricultural Chemicals
Specialty Retail, Other
Medical Appliances & Equipment
Textile
Semiconductor Equipment & Materials
Biotechnology
Major Airlines
Property & Casualty Insurance
Application Software
Internet Information Providers
Information Technology Services
Drug Manufacturers
Steel & Iron
Independent Oil & Gas
Business Software & Services
Independent Oil & Gas
Internet Information Providers
";
            return sectors.Split("\n").ToList().Random();
        }
     
        public static string GetRandomFund() {
            var funds = @"Apple Inc.
Bank of America Corporation
Amazon.com, Inc.
AT&T Inc.
Alphabet Inc.
Altria Group, Inc.
Delta Air Lines, Inc.
Alcoa Corporation
American Express Company
E. I. du Pont de Nemours and Company
Alibaba Group Holding Limited
Abbott Laboratories
Under Armour, Inc.
Applied Materials, Inc.
Amgen Inc.
American Airlines Group Inc.
American International Group, Inc.
The Allstate Corporation
Adobe Systems Incorporated
Alphabet Inc.
Accenture plc
AbbVie Inc.
ArcelorMittal
Eli Lilly and Company
Allergan plc
Apache Corporation
Automatic Data Processing, Inc.
Anadarko Petroleum Corporation
Akamai Technologies, Inc.
Annaly Capital Management, Inc.
Barrick Gold Corporation
Activision Blizzard, Inc.
Autodesk, Inc.
Archer-Daniels-Midland Company
Baumart Holdings Limited
Walgreens Boots Alliance, Inc.
Arena Pharmaceuticals, Inc.
Southwest Airlines Co.
ACADIA Pharmaceuticals Inc.
Palo Alto Networks, Inc.
Advanced Micro Devices, Inc.
Aetna Inc.
American Electric Power Company, Inc.
Alexion Pharmaceuticals, Inc.
Calamos Asset Management Inc.
Broadcom Limited
Electronic Arts Inc.
Deutsche Bank Aktiengesellschaft
Reynolds American Inc.
Agnico Eagle Mines Limited
Air Products and Chemicals, Inc.
Ambarella, Inc.
Novartis AG
Apollo Education Group, Inc.
Abercrombie & Fitch Co.
Lululemon Athletica Inc.
Rite Aid Corporation
Brookside Energy Limited
Array BioPharma Inc.
AGNC Investment Corp.
JetBlue Airways Corporation
Agilent Technologies, Inc.
O'Reilly Automotive, Inc.
Amicus Therapeutics, Inc.
AutoZone, Inc.
Atmel Corporation
AutoNation, Inc.
AstraZeneca PLC
The AES Corporation
AGL Resources Inc.
Anheuser-Busch InBev SA/NV
ARMOUR Residential REIT, Inc.
Becton, Dickinson and Company
AK Steel Holding Corporation
AllianceBernstein Holding L.P.
Acorda Therapeutics, Inc.
Credit Suisse Group AG
Aflac Incorporated
Analog Devices, Inc.
Aegerion Pharmaceuticals, Inc.
ACI Worldwide, Inc.
Ameriprise Financial, Inc.
Avon Products, Inc.
TD Ameritrade Holding Corporation
American Eagle Outfitters, Inc.
American Water Works Company, Inc.
Novo Nordisk A/S
Altera Corp.
Alaska Air Group, Inc.
Plains All American Pipeline, L.P.
M2 Group Ltd
Ares Capital Corporation
Advance Auto Parts, Inc.
Nordic American Tankers Limited
Federal National Mortgage Association
First Trust Dividend and Income Fund
Apartment Investment and Management Company
Agios Pharmaceuticals, Inc.
Ameren Corporation
UBS Group AG
Anavex Life Sciences Corp.
Alliance Resource Partners, L.P.
Anthem, Inc.
Agrium Inc.
First Majestic Silver Corp.
AmTrust Financial Services, Inc.
AmerisourceBergen Corporation
Statoil ASA
Allegheny Technologies Incorporated
The ADT Corporation
AvalonBay Communities, Inc.
Atwood Oceanics, Inc.
Alnylam Pharmaceuticals, Inc.
Laboratory Corporation of America Holdings
Avery Dennison Corporation
Yamana Gold Inc.
Ashland Global Holdings Inc.
ARM Holdings plc
ARIAD Pharmaceuticals, Inc.
Alpha Natural Resources, Inc.
Apollo Investment Corporation
Acxiom Corporation
Achillion Pharmaceuticals, Inc.
Aceto Corporation
ABIOMED, Inc.
ABM Industries Incorporated
Virgin America Inc.
aTyr Pharma, Inc.
Atmos Energy Corporation
Atlas Resource Partners, L.P.
Aon plc
Advaxis, Inc.
Agree Realty Corporation
AmeriGas Partners, L.P.
Spirit Airlines, Inc.
Aviva plc
Akorn, Inc.
Alliance Data Systems Corporation
Abaxis, Inc.
Acuity Brands, Inc.
Allied World Assurance Company Holdings, AG
ASML Holding N.V.
American Tower Corporation
Alder Biopharmaceuticals, Inc.
AECOM
DreamWorks Animation SKG Inc.
Antares Pharma, Inc.
Arrow Electronics, Inc.
Apollo Commercial Real Estate Finance, Inc.
Airgas, Inc.
Antero Resources Corporation
Applied Micro Circuits Corporation
AMC Entertainment Holdings, Inc.
Air Lease Corporation
Agenus Inc.
Aaron's, Inc.
Aqua America, Inc.
Fiat Chrysler Automobiles N.V.
Booz Allen Hamilton Holding Corporation
Abraxas Petroleum Corporation
Avnet, Inc.
Albemarle Corporation
Assurant, Inc.
Science Applications International Corporation
Avis Budget Group, Inc.
Axiall Corporation
AngloGold Ashanti Limited
Aéropostale, Inc.
Amphenol Corporation
Anthera Pharmaceuticals, Inc.
América Móvil, S.A.B. de C.V.
Amedica Corporation
Arlington Asset Investment Corp.
The Advisory Board Company
Western Asset Mortgage Capital Corporation
Mustera Property Group Limited
Jack Henry & Associates, Inc.
AeroVironment, Inc.
Amkor Technology, Inc.
Alon USA Energy, Inc.
Aluminum Corporation Of China Limited
Australian Potash Limited
Wells Fargo Advantage Multi-Sector Income Fund
Bioptix, Inc.
Anacor Pharmaceuticals, Inc.
Advanced Energy Industries, Inc.
Alleghany Corporation
MTGE Investment Corp.
Century Aluminum Company
Altisource Portfolio Solutions S.A.
Amarin Corporation plc
Ampio Pharmaceuticals, Inc.
AMAG Pharmaceuticals, Inc.
Alkermes plc
Affymetrix Inc.
Advanced Emissions Solutions, Inc.
Arctic Cat Inc.
AAON, Inc.
Acceleron Pharma Inc.
Verisk Analytics, Inc.
Voxeljet AG
Orbital ATK, Inc.
Atlas Energy Group, LLC
Preferred Apartment Communities, Inc.
Apollo Global Management, LLC
Alaska Communications Systems Group, Inc.
Alamo Group Inc.
A.H. Belo Corporation
Acacia Research Corporation
American Capital, Ltd.
Ritchie Bros. Auctioneers Incorporated
Mid-America Apartment Communities, Inc.
Brookfield Asset Management Inc.
athenahealth, Inc.
Atlantic Power Corporation
Advanced Semiconductor Engineering, Inc.
Arcos Dorados Holdings Inc.
Arista Networks, Inc.
Access National Corporation
AAR Corp.
Astoria Financial Corporation
Westinghouse Air Brake Technologies Corporation
Reliance Steel & Aluminum Co.
Packaging Corporation of America
Cash America International, Inc.
Accelerate Diagnostics, Inc.
AV Homes, Inc.
Avista Corporation
Athersys, Inc.
Arrowhead Pharmaceuticals, Inc.
Angie's List, Inc.
Affiliated Managers Group, Inc.
Allison Transmission Holdings, Inc.
Align Technology, Inc.
Akebia Therapeutics, Inc.
Assured Guaranty Ltd.
Aeterna Zentaris Inc.
AcelRx Pharmaceuticals, Inc.
Rockwell Automation Inc.
Gaming and Leisure Properties, Inc.
Dividend and Income Fund
AZZ Inc.
AtriCure, Inc.
ARRIS International plc
Aramark
A. O. Smith Corporation
Amira Nature Foods Ltd.
American Midstream Partners, LP
AMC Networks Inc.
Alimera Sciences, Inc.
ALLETE, Inc.
Ashford Hospitality Trust, Inc.
Accuride Corp.
ABB Ltd
PT Astra Agro Lestari Tbk
Spirit AeroSystems Holdings, Inc.
Sealed Air Corporation
Elizabeth Arden, Inc.
Pan American Silver Corp.
Delphi Automotive PLC
Adama Technologies Corporation
CBL & Associates Properties, Inc.
Banco Bilbao Vizcaya Argentaria, S.A.
Aircastle Limited
Apogee Enterprises, Inc.
The Andersons, Inc.
American Superconductor Corporation
Amyris, Inc.
Amedisys, Inc.
Alexander & Baldwin, Inc.
Alico, Inc.
Addus HomeCare Corporation
Actua Corporation
Acasti Pharma Inc.
Atlas Air Worldwide Holdings, Inc.
Advanced Drainage Systems, Inc.
Penske Automotive Group, Inc.
Allscripts Healthcare Solutions, Inc.
Kulicke and Soffa Industries, Inc.
Ethan Allen Interiors Inc.
Aspen Technology, Inc.
Anixter International Inc.
API Technologies Corp.
Apricus Biosciences, Inc.
American Homes 4 Rent
AMETEK, Inc.
Arthur J. Gallagher & Co.
Alliance Healthcare Services, Inc.
Alliance Holdings GP, L.P.
AGCO Corporation
Aerie Pharmaceuticals, Inc.
Ares Commercial Real Estate Corporation
Abeona Therapeutics Inc.
Western Alliance Bancorporation
Syngenta AG
A. Schulman, Inc.
Northern Oil and Gas, Inc.
Healthcare Trust of America, Inc.
G-III Apparel Group, Ltd.
Famous Dave's of America, Inc.
CPI Aerostructures, Inc.
Alon Blue Square Israel Ltd
HomeAway, Inc.
Actuant Corporation
Ascent Solar Technologies, Inc.
Approach Resources, Inc.
Alexandria Real Estate Equities, Inc.
Ansys, Inc.
American National Bankshares Inc.
Amarantus Bioscience Holdings, Inc
Alere Inc.
Aixtron SE
Albany International Corp.
Aspen Insurance Holdings Limited
Argan, Inc.
AEGON N.V.
ADTRAN, Inc.
Adamas Pharmaceuticals, Inc.
Axcelis Technologies, Inc.
Asbury Automotive Group, Inc.
China Zenix Auto International Limited
Nuveen New Jersey Quality Municipal Income Fund
KapStone Paper and Packaging Corporation
HollySys Automation Technologies, Ltd.
Group 1 Automotive, Inc.
Enersis Américas S.A.
B/E Aerospace, Inc.
Axalta Coating Systems Ltd.
American States Water Company
Armstrong World Industries, Inc.
AVEO Pharmaceuticals, Inc.
AU Optronics Corp.
Autohome Inc.
America First Multifamily Investors, L.P.
Avino Silver & Gold Mines Ltd.
Archrock, Inc.
Anworth Mortgage Asset Corporation
Alexander's, Inc.
AMN Healthcare Services Inc.
Alamos Gold Inc.
AerCap Holdings N.V.
Adams Resources & Energy, Inc.
Roche Holding AG
Aratana Therapeutics, Inc.
Oil-Dri Corporation of America
Nordic American Offshore Ltd.
KAR Auction Services, Inc.
Houston American Energy Corp.
Aerohive Networks, Inc.
Fresenius Medical Care AG & Co. KGAA
Amdocs Limited
CoreCivic, Inc.
Azure Midstream Partners, LP
AXIS Capital Holdings Limited
American Axle & Manufacturing Holdings, Inc.
Avalon Holdings Corporation
Avid Technology, Inc.
Ascena Retail Group, Inc.
On Assignment, Inc.
Ardelyx, Inc.
Accuray Incorporated
Aquinox Pharmaceuticals, Inc.
Alpha Pro Tech, Ltd.
Applied DNA Sciences, Inc.
Ambac Financial Group, Inc.
Allegiant Travel Company
Alon USA Partners, LP
Applied Industrial Technologies, Inc.
Air Methods Corporation
Altra Industrial Motion Corp.
American Equity Investment Life Holding Company
Aehr Test Systems
Alcobra Ltd.
Acura Pharmaceuticals, Inc.
Acadia Healthcare Company, Inc.
Atlantic Coast Financial Corporation
ACCO Brands Corporation
Atlantica Yield plc
TravelCenters of America LLC
Retail Properties of America, Inc.
Manhattan Associates, Inc.
Lamar Advertising Company
Kayne Anderson MLP Investment Company
Aoxing Pharmaceutical Company, Inc.
Astronics Corporation
ATN International, Inc.
ARC Group Worldwide, Inc.
American Public Education, Inc.
Ampco-Pittsburgh Corporation
Anika Therapeutics, Inc.
AngioDynamics, Inc.
Apollo Residential Mortgage, Inc.
AmSurg Corp.
Allied Motion Technologies Inc.
Antero Midstream Partners LP
Autoliv, Inc.
Analogic Corporation
Acadia Realty Trust
Aegion Corporation
Adidas AG
ZELTIQ Aesthetics, Inc.
World Acceptance Corporation
AMERCO";
            return funds.Split("\n").ToList().Random().Trim();
        }
     

      

       

    }
}
