using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Reporting {
    public class Config {

        public static CultureInfo FormattingCulture = CultureInfo.CreateSpecificCulture("de-CH"); //   TODO: REFACTOR TO CORE CONFIG?

        public const string DateFormatD3 = "%d.%m.%y";
        public const string DateFormatNet = "dd.MM.yy";

        public const string WeekFormatD3 = "W%W %y";
        public const string WeekFormatNet = "W{week} yy";

        public const string MonthFormatD3 = "%b %y";
        //public const string MonthFormatNet = ""; TODO

        public const string QuarterFormatD3 = "Q%q %y";
        public const string QuarterFormatNet = "Q{quarter} yy";

        public static string NumberThousandsFormatD3 = ",.2r";
        public static string NumberThousandsFormatNet = "N0"; // needs "de-CH" Culture for upper ' 




        internal static string GetWeekYearFormat(DateTime date) {
            var week = date.GetIso8601WeekOfYear().ToString();
            var format = WeekFormatNet.Replace("{week}", week);
            var result = date.ToString(format);
            return result;
        }

        internal static string GetQuarterFormat(DateTime date) {
            var quarter = date.QuarterNumber().ToString();
            var format = QuarterFormatNet.Replace("{quarter}", quarter);
            var result = date.ToString(format);
            return result;  
        }

        /// <summary>
        /// Returns a string with the number formatted with thousands separator and currency as suffix
        /// </summary>
        /// <param name="value"></param>
        /// <param name="currency">if null default currency is used</param>
        /// <returns></returns>
        public static string GetNumberThousandsCurrency(decimal value, string currency = null) {
            if (currency == null) {
                currency = Core.Config.CurrencyDefault;
            }
            
            var result = value.ToString(NumberThousandsFormatNet, Reporting.Config.FormattingCulture) + " " + currency;
            return result;
        }
    }

}
