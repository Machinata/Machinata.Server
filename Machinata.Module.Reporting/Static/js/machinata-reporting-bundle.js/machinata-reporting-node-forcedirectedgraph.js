


/// <summary>
/// </summary>
/// <example>
/// See ```example-report-forcedirectedgraph.json``` for complete example usages.
/// </example>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.ForceDirectedGraphNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ForceDirectedGraphNode.defaults = {};

/// <summary>
/// 
/// </summary>
Machinata.Reporting.Node.ForceDirectedGraphNode.defaults.chrome = "light";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.ForceDirectedGraphNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.ForceDirectedGraphNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.ForceDirectedGraphNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_4x2, Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1];

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.ForceDirectedGraphNode.defaults.supportsToolbar = true;

/// <summary>
/// 
/// </summary>
Machinata.Reporting.Node.ForceDirectedGraphNode.defaults.insertLegend = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ForceDirectedGraphNode.getVegaSpec = function (instance, config, json, nodeElem) {
    // Create flat version of all facts and links
    var facts = []; 
    var links = []; 
    // Create all series facts...
    for (var s = 0; s < json.series.length; s++) {
        var serie = json.series[s];
        for (var f = 0; f < serie.facts.length; f++) {
            var fact = serie.facts[f];
            fact.serieId = serie.id;
            facts.push(fact);
        }
    }
    // Create all links
    for (var l = 0; l < json.links.length; l++) {
        var link = json.links[l];
        links.push(link);
    }
    return {
        "data": [
            {
                "name": "facts",
                "values": facts,
                "transform": [
                    {
                        "type": "formula",
                        "initonly": true,
                        "as": "categoryResolved",
                        "expr": "datum.category.resolved"
                    },
                    {
                        "type": "formula",
                        "initonly": true,
                        "as": "tooltipResolved",
                        "expr": "datum.tooltip ? datum.tooltip.resolved : datum.categoryResolved"
                    },
                ]
            },
            {
                "name": "links",
                "values": links
            },
        ],
        "signals": [
            { "name": "cx", "update": "width / 2" },
            { "name": "cy", "update": "height / 2" },
            {
                "name": "nodeRadius", "value": 16,
            },
            {
                "name": "nodeCharge", "value": -100,
            },
            {
                "name": "linkDistance", "value": 80,
            },
            {
                "name": "static", "value": true,
            },
            {
                "name": "minNodeSizeMultiplier", "value": 1,
            },
            {
                "name": "maxNodeSizeMultiplier", "value": 2,
            },
            {
                "description": "State variable for active node fix status.",
                "name": "fix", "value": false,
                "on": [
                    {
                        "events": "symbol:mouseout[!event.buttons], window:mouseup",
                        "update": "false"
                    },
                    {
                        "events": "symbol:mouseover",
                        "update": "fix || true"
                    },
                    {
                        "events": "[symbol:mousedown, window:mouseup] > window:mousemove!",
                        "update": "xy()",
                        "force": true
                    }
                ]
            },
            {
                "description": "Graph node most recently interacted with.",
                "name": "node", "value": null,
                "on": [
                    {
                        "events": "symbol:mouseover",
                        "update": "fix === true ? item() : node"
                    }
                ]
            },
            {
                "description": "Flag to restart Force simulation upon data changes.",
                "name": "restart", "value": false,
                "on": [
                    { "events": { "signal": "fix" }, "update": "fix && fix.length" }
                ]
            }
        ],
        "scales": [
            {
                "name": "color",
                "type": "ordinal",
                "domain": { "data": "facts", "field": "serieId" },
                "range": { "scheme": json.theme + "-bg" }
            },
            {
                "name": "textColor",
                "type": "ordinal",
                "domain": { "data": "facts", "field": "serieId" },
                "range": { "scheme": json.theme + "-fg" }
            },
            {
                "name": "nodeSizeMultiplier",
                "type": "linear",
                "domain": { "data": "facts", "field": "val" },
                "range": { "signal": "[minNodeSizeMultiplier,maxNodeSizeMultiplier]" },
                "zero": false,
            },
        ],
        "marks": [
            {
                "name": "nodes",
                "type": "symbol",
                "zindex": 2,
                "from": { "data": "facts" },
                "on": [
                    {
                        "trigger": "fix",
                        "modify": "node",
                        "values": "fix === true ? {fx: node.x, fy: node.y} : {fx: fix[0], fy: fix[1]}"
                    },
                    {
                        "trigger": "!fix",
                        "modify": "node", "values": "{fx: null, fy: null}"
                    }
                ],

                "encode": {
                    "enter": {
                        "fill": { "scale": "color", "field": "serieId" },
                        //"fill": { "value": "black" },
                        "stroke": { "value": "white" },
                        "tooltip": { "field": "tooltipResolved" }
                    },
                    "update": {
                        "size": { "signal": "(2 * (nodeRadius * scale('nodeSizeMultiplier',datum.val)) * (nodeRadius * scale('nodeSizeMultiplier',datum.val))) " },
                        "cursor": { "value": "pointer" }
                    }
                },
                "transform": [
                    {
                        "type": "force",
                        "iterations": 300,
                        "restart": { "signal": "restart" },
                        "static": { "signal": "static" },
                        "signal": "force",
                        "forces": [
                            { "force": "center", "x": { "signal": "cx" }, "y": { "signal": "cy" } },
                            { "force": "collide", "radius": { "signal": "nodeRadius" } },
                            { "force": "nbody", "strength": { "signal": "nodeCharge" } },
                            { "force": "link", "links": "links", "id": "datum.id", "distance": { "signal": "linkDistance" } }
                        ]
                    }
                ]
            },
            {
                "type": "text",
                "zindex": 1,
                "interactive": false,
                "from": { "data": "nodes" },
                "encode": {
                    "enter": {
                        //"fill": { "scale": "color", "field": "serieId" },
                        //"stroke": { "value": "white" },
                        "limit": { "signal": "linkDistance * 1.5" },
                        "align": { "value": "center" },
                        "fontWeight": { "signal": "scale('nodeSizeMultiplier',datum.datum.val) == maxNodeSizeMultiplier ? 'bold' : 'normal'" },
                        "baseline": { "value": "top" },
                        //"tooltip": { "signal": "scale('nodeSizeMultiplier',datum.datum.val)" },
                        "text": { "field": "datum.categoryResolved" },
                        "dy": { "signal": "nodeRadius*scale('nodeSizeMultiplier',datum.datum.val)" },
                    },
                    "update": {
                        "xc": { "signal": "datum.x" },
                        "yc": { "signal": "datum.y" },
                    }
                },
            },
            {
                "type": "path",
                "from": { "data": "links" },
                "transform": [
                    {
                        "type": "linkpath",
                        "require": { "signal": "force" },
                        "shape": "line",
                        "sourceX": "datum.source.x", "sourceY": "datum.source.y",
                        "targetX": "datum.target.x", "targetY": "datum.target.y"
                    }
                ],
                "interactive": false,
                "encode": {
                    "update": {
                        "stroke": { "value": "#ccc" },
                        "strokeWidth": { "value": config.lineSize }
                    }
                }
            }
        ]
    };
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ForceDirectedGraphNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    //spec["data"][0].values = json.series;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ForceDirectedGraphNode.init = function (instance, config, json, nodeElem) {
    
    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.ForceDirectedGraphNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);

    Machinata.Reporting.Node.VegaNode.debugOutputDataSet(nodeElem, 'factsLegend');
    Machinata.Reporting.Node.VegaNode.debugOutputDataSet(nodeElem, 'factsResolved');
};
Machinata.Reporting.Node.ForceDirectedGraphNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};








