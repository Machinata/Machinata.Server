


/// <summary>
/// Abstract class for all nodes that need to draw a bunch of shapes on a graph
/// 
/// </summary>
/// <example>
/// </example>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.GraphicalCompositionNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.GraphicalCompositionNode.defaults = {};

/// <summary>
/// By default charts are on solid chrome.
/// </summary>
Machinata.Reporting.Node.GraphicalCompositionNode.defaults.chrome = "light";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.GraphicalCompositionNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.GraphicalCompositionNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.GraphicalCompositionNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_4x2];

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.GraphicalCompositionNode.defaults.supportsToolbar = true;

/// <summary>
/// By default we insert legends, if any graphics items have a legend item.
/// </summary>
Machinata.Reporting.Node.GraphicalCompositionNode.defaults.insertLegend = true;

/// <summary>
/// If defined, sets the aspect ratio of the graphic that is automatically fit inside the view.
/// </summary>
Machinata.Reporting.Node.GraphicalCompositionNode.defaults.aspectRatio = null;

/// <summary>
/// </summary>
Machinata.Reporting.Node.GraphicalCompositionNode.getVegaSpec = function (instance, config, json, nodeElem) {
    var aspectRatioSignal = json.aspectRatio != null ? json.aspectRatio : "width / height";
    var spec = {
        "data": [
          {
              "name": "shapes",
              "values": null,
            }
        ],
        "signals": [
            {
                "name": "graphicAspectRatio",
                "update": aspectRatioSignal,
            },
            {
                "name": "viewAspectRatio",
                "update": "width / height",
            },
            {
                "name": "graphicWidth",
                "update": "viewAspectRatio > graphicAspectRatio ? graphicAspectRatio*height : width",
            },
            {
                "name": "graphicHeight",
                "update": "viewAspectRatio > graphicAspectRatio ? height : width/graphicAspectRatio",
            },
            {
                "name": "graphicSize",
                "update": "min(graphicWidth,graphicHeight)",
            }
        ],
        "scales": [
          {
              "name": "colorByKey",
              "type": "ordinal",
                "domain": { "data": "shapes", "field": "key" },
              "range": { "scheme": json.theme + "-bg" }
          },
          {
              "name": "color",
              "type": "ordinal",
              "domain": { "data": "shapes", "field": "key" },
              "range": { "data": "shapes", "field": "colorResolved" },
            },
            {
                "name": "xScale",
                "type": "linear",
                "domain": [0, 1],
                "range": "width",
                //"padding": config.padding, 
            },
            {
                "name": "yScale",
                "type": "linear",
                "domain": [0,1],
                "range": "height",
                "zero": false,
                //"padding": { "signal": "height * 0.1" },
            },
        ],
        "marks": [
            
        ]
    };
    // Debug frame
    if (false) {
        var debugMark = {
            "type": "group",
                "encode": {
                "update": {
                    "tooltip": {
                        "signal": "{'graphicAspectRatio':graphicAspectRatio,'viewAspectRatio':viewAspectRatio}"
                    },
                    "stroke": {
                        "signal": "'red'"
                    },
                    "fill": {
                        "signal": "'green'"
                    },
                    "xc": {
                        "signal": "width/2"
                    },
                    "yc": {
                        "signal": "height/2"
                    },
                    "width": {
                        "signal": "graphicWidth"
                    },
                    "height": {
                        "signal": "graphicHeight"
                    }
                }
            },
        }
        spec.marks.push(debugMark);
    }
    // Master group
    var masterGroup = {
        "name": "MASTER_GROUP",
        "type": "group",
        "encode": {
            "update": {
                "width": {
                    "signal": "width"
                },
                "height": {
                    "signal": "height"
                }
            }
        },
        "marks": [
            
        ]
    };
    spec.marks.push(masterGroup);
    // Process graphics
    Machinata.Util.each(json._state.graphics, function (i, graphic) {
        if (graphic.type != "legend") {
            var mark = Machinata.Reporting.Node.GraphicalCompositionNode.createMarkForGraphic(instance, config, json, nodeElem, graphic);
            masterGroup.marks.push(mark);
        }
    });
    // Return spec
    return spec;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.GraphicalCompositionNode.createMarkForGraphic = function (instance, config, json, nodeElem, graphic) {
    var mark = {};
    // Graphic:
    // {
    //    type
    // }
    // Generic properties
    var autosize = "none";
    if (json.autosize != null) autosize = json.autosize;
    if (graphic.autosize != null) autosize = graphic.autosize;
    var widthSignal = null;
    var heightSignal = null;
    var xSignal = null;
    var ySignal = null;
    var xcSignal = null;
    var ycSignal = null;
    var sizeBasis = "graphicSize";
    var widthBasis = "graphicSize"; 
    var heightBasis = "graphicSize";
    if (autosize == "stretch-x" || autosize == "stretch") widthBasis = "width";
    if (autosize == "stretch-y" || autosize == "stretch") heightBasis = "height";
    function getXCoordSignal(x) {
        if (typeof x == 'number') {
            return "width/2 + lerp([0," + widthBasis + "/2]," + x + ")";
        } else {
            return x;
        }
    }
    function getYCoordSignal(y) {
        if (typeof y == 'number') {
            return "height/2 + lerp([0," + heightBasis + "/2]," + y + ")";
        } else {
            return y;
        }
    }
    if (graphic.size != null) {
        if (typeof graphic.size == 'number') {
            widthSignal = "lerp([0," + sizeBasis + "]," + graphic.size + ")";
            heightSignal = "lerp([0," + sizeBasis + "]," + graphic.size + ")";
        } else {
            widthSignal = graphic.size;
            heightSignal = graphic.size;
        }
    }
    if (graphic.width != null) {
        if (typeof graphic.width == 'number') {
            widthSignal = "lerp([0," + widthBasis + "]," + graphic.width + ")";
        } else {
            widthSignal = graphic.width;
        }
    }
    if (graphic.height != null) {
        if (typeof graphic.height == 'number') {
            heightSignal = "lerp([0," + heightBasis + "]," + graphic.height + ")";
        } else {
            heightSignal = graphic.height;
        }
    }
    if (graphic.x != null) {
        if (typeof graphic.x == 'number') {
            xSignal = getXCoordSignal(graphic.x);
        } else {
            xSignal = graphic.x;
        }
    }
    if (graphic.y != null) {
        if (typeof graphic.y == 'number') {
            ySignal = getYCoordSignal(graphic.y);
        } else {
            ySignal = graphic.y;
        }
    }
    if (graphic.xc != null) {
        if (typeof graphic.xc == 'number') {
            xcSignal = getXCoordSignal(graphic.xc);
        } else {
            xcSignal = graphic.xc;
        }
    }
    if (graphic.yc != null) {
        if (typeof graphic.yc == 'number') {
            ycSignal = getYCoordSignal(graphic.yc);
        } else {
            ycSignal = graphic.yc;
        }
    }
    // Resolve color
    var colorResolved = null;
    if (colorResolved == null && graphic.color != null) colorResolved = graphic.color;
    if (colorResolved == null && graphic.colorShade != null) colorResolved = Machinata.Reporting.getThemeBGColorByShade(config, json.theme, graphic.colorShade);
    if (colorResolved == null && graphic.colorShadeBG != null) colorResolved = Machinata.Reporting.getThemeBGColorByShade(config, json.theme, graphic.colorShadeBG);
    if (colorResolved == null && graphic.colorShadeFG != null) colorResolved = Machinata.Reporting.getThemeFGColorByShade(config, json.theme, graphic.colorShadeFG);
    var strokeColorResolved = null;
    if (strokeColorResolved == null && graphic.strokeColor != null) strokeColorResolved = graphic.strokeColor;
    if (strokeColorResolved == null && graphic.strokeColorShade != null) strokeColorResolved = Machinata.Reporting.getThemeBGColorByShade(config, json.theme, graphic.strokeColorShade);
    if (strokeColorResolved == null && graphic.strokeColorShadeBG != null) strokeColorResolved = Machinata.Reporting.getThemeBGColorByShade(config, json.theme, graphic.strokeColorShadeBG);
    if (strokeColorResolved == null && graphic.strokeColorShadeFG != null) strokeColorResolved = Machinata.Reporting.getThemeFGColorByShade(config, json.theme, graphic.strokeColorShadeFG);
    // Get mark based on type
    if (graphic.type == "rect") {
        // Rect
        // https://vega.github.io/vega/docs/marks/rect/
        mark.type = "rect";
        mark.encode = {
            "enter": {
                "fill": { "value": colorResolved != null ? colorResolved : null },
                "stroke": { "value": strokeColorResolved != null ? strokeColorResolved : null },
            },
            "update": {
                "width": widthSignal != null ? { "signal": widthSignal } : null,
                "height": heightSignal != null ? { "signal": heightSignal } : null,
                "x": xSignal != null ? { "signal": xSignal } : null,
                "y": ySignal != null ? { "signal": ySignal } : null,
                "xc": xcSignal != null ? { "signal": xcSignal } : null,
                "yc": ycSignal != null ? { "signal": ycSignal } : null,
            },
        };
    } else if (graphic.type == "group") {
        // Rect
        // https://vega.github.io/vega/docs/marks/group/
        mark.type = "group";
        mark.encode = {
            "enter": {
                "fill": { "value": colorResolved != null ? colorResolved : null },
                "clip": { "value": graphic.clip },
            },
            "update": {
                "width": widthSignal != null ? { "signal": widthSignal } : null,
                "height": heightSignal != null ? { "signal": heightSignal } : null,
                "x": xSignal != null ? { "signal": xSignal } : null,
                "y": ySignal != null ? { "signal": ySignal } : null,
                "xc": xcSignal != null ? { "signal": xcSignal } : null,
                "yc": ycSignal != null ? { "signal": ycSignal } : null,
            },
        };
        mark.marks = [];
        if (graphic.children != null) {
            for (var i = 0; i < graphic.children.length; i++) {
                mark.marks.push(Machinata.Reporting.Node.GraphicalCompositionNode.createMarkForGraphic(instance, config, json, nodeElem, graphic.children[i]));
            }
        }
    } else if (graphic.type == "image") {
        // Image Graphic
        // https://vega.github.io/vega/docs/marks/image/
        mark.type = "image";
        // Get image asset
        var imageURLToUse = Machinata.Reporting.Asset.resolve(instance, config, graphic.image);
        mark.encode = {
            "enter": {
                "aspect": { "value": false }, // true is not supported by bmpi due to Aspose needing to inline the SVGs...
                "url": { "value": imageURLToUse },
                "align": { "value": graphic.align != null ? graphic.align : "center" },
                "baseline": { "value": graphic.baseline != null ? graphic.baseline :"middle" },
            },
            "update": {
                "width": widthSignal != null ? { "signal": widthSignal } : null,
                "height": heightSignal != null ? { "signal": heightSignal } : null,
                "x": xSignal != null ? { "signal": xSignal } : null,
                "xc": xcSignal != null ? { "signal": xcSignal } : null,
                "y": ySignal != null ? { "signal": ySignal } : null,
                "yc": ycSignal != null ? { "signal": ycSignal } : null,
            },
        };
    } else if (graphic.type == "text") {
        // Text
        // https://vega.github.io/vega/docs/marks/text/
        mark.type = "text";
        // Determine font size
        var fontSizeSignal = null;
        if (graphic.fontSize != null) {
            if (graphic.fontSize == "h1") fontSizeSignal = "config_h1Size";
            else if (graphic.fontSize == "chart") fontSizeSignal = "config_chartTextSize";
            else if (graphic.fontSize == "legend") fontSizeSignal = "config_legendSize";
            else if (graphic.fontSize == "title") fontSizeSignal = "config_titleSize";
            else if (graphic.fontSize == "subtitle") fontSizeSignal = "config_subtitleSize";
            else if (graphic.fontSize == "label") fontSizeSignal = "config_labelSize";
            else if (graphic.fontSize < 1.0) fontSizeSignal = "lerp([0," + sizeBasis + "]," + graphic.fontSize + ")";
            else fontSizeSignal = graphic.fontSize;
        }
        // Determine text
        var textResolved = Machinata.Reporting.Text.resolve(instance, graphic.text);
        var textResolvedArray = Machinata.Reporting.Tools.lineBreakString(textResolved, {
            returnArray: true,
            maxCharsPerLine: graphic.maxCharsPerLine,
            maxLines: graphic.maxLines
        });
        mark.encode = {
            "enter": {
                "text": { "value": textResolvedArray },
                //"text": { "signal": "size" },
                "align": { "value": graphic.align != null ? graphic.align : "center" },
                "fontWeight": { "value": graphic.fontWeight != null ? graphic.fontWeight : "normal" },
                "baseline": { "value": graphic.baseline != null ? graphic.baseline : "middle" },
                "fill": colorResolved != null ? { "value": colorResolved } : {"signal":"chartTextColor"},
            },
            "update": {
                //"width": widthSignal != null ? { "signal": widthSignal } : null,
                //"height": heightSignal != null ? { "signal": heightSignal } : null,
                "fontSize": fontSizeSignal != null ? { "signal": fontSizeSignal } : null,
                "x": xSignal != null ? { "signal": xSignal } : null,
                "y": ySignal != null ? { "signal": ySignal } : null,
                "dy": { "signal": "(0-((" + fontSizeSignal + " || 0) * config_lineHeight * (" + textResolvedArray.length + "-1)) / 2) " + (graphic.dy != null ? " + ("+graphic.dy+")" : "") },
            },
        };
    } else if (graphic.type == "rule") {
        // Rule
        // https://vega.github.io/vega/docs/marks/rule/
        mark.type = "rule";
        mark.encode = {
            "enter": {
                "stroke": { "value": colorResolved != null ? colorResolved : 'black' },
                "strokeWidth": { "value": graphic.strokeWidth != null ? graphic.strokeWidth : config.lineSize },
            },
            "update": {
                "x": { "signal": getXCoordSignal(graphic.x1) },
                "y": { "signal": getYCoordSignal(graphic.y1) },
                "x2": { "signal": getXCoordSignal(graphic.x2) },
                "y2": { "signal": getYCoordSignal(graphic.y2) },
            },
        };
    } else if (graphic.type == "legend") {
        // Legend
        // Nothing to do, handled by legendData
    } else {
        throw "Machinata.Reporting.Node.GraphicalCompositionNode.createMarkForGraphic: graphic type "+graphic.type+" not supported!"
    }
    // Return
    return mark;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.GraphicalCompositionNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    
};



/// <summary>
/// </summary>
Machinata.Reporting.Node.GraphicalCompositionNode.init = function (instance, config, json, nodeElem) {

    // Init
    var legendData = [];
    if (json.graphics != null) json._state.graphics = json.graphics;
    if (json._state.graphics == null) json._state.graphics = [];

    // Pre-process graphics
    Machinata.Util.each(json._state.graphics, function (i, graphic) {
        // Assign ids
        if (graphic.key == null) graphic.key = "graphic" + i;
        // Legend
        if (graphic.legend != null) {
            if (graphic.legend.key == null) graphic.legend.key = graphic.key;
            legendData.push(graphic.legend);
        } else if (graphic.type == "legend") {
            legendData.push(graphic);
        }
    });

    // Generate legend data
    Machinata.Reporting.Node.VegaNode.Util.createLegendForLegendData(instance, config, json, legendData);


    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.GraphicalCompositionNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
    //Machinata.Reporting.Node.VegaNode.debugOutputDataSet(nodeElem, "allShapePoints");
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.GraphicalCompositionNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.GraphicalCompositionNode.exportExcelFormat = function (instance, config, json, nodeElem, format, filename) {
    //var workbook = Machinata.Reporting.Export.createExcelWorkbook(instance, config, json);
    //workbook = Machinata.Reporting.Export.createExcelDataForSeriesGroupsChart(instance, config, json, workbook, format);
    //Machinata.Reporting.Export.saveExcelWorkbook(instance, config, json, workbook, filename);
    //return true;
};








