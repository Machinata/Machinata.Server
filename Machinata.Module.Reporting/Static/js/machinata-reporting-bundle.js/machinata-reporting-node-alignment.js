


/// <summary>
/// A Alignment Table visualization.
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.GraphicalCompositionNode</inherits>
Machinata.Reporting.Node.AlignmentTableNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.AlignmentTableNode.defaults = {};

/// <summary>
/// By default charts are on solid chrome.
/// </summary>
Machinata.Reporting.Node.AlignmentTableNode.defaults.chrome = "light";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.AlignmentTableNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.AlignmentTableNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.AlignmentTableNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_4x4, Machinata.Reporting.Layouts.BLOCK_4x2];

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.AlignmentTableNode.defaults.supportsToolbar = true;

/// <summary>
/// By default we insert the legend of all the alignments columns.
/// </summary>
Machinata.Reporting.Node.AlignmentTableNode.defaults.insertLegend = true;

/// <summary>
/// If ```true```, the alignment facts are stacked as a single bar.
/// </summary>
Machinata.Reporting.Node.AlignmentTableNode.defaults.stackedBars = true;

/// <summary>
/// If ```true```, the alignment facts are stacked using absolute values to fill the bar space horizontally.
/// If ```false```, the alignment facts are stacked using relative values, which always fill to 100% space horizontally.
/// </summary>
Machinata.Reporting.Node.AlignmentTableNode.defaults.stackedBarsUseAbsoluteValue = true;

/// <summary>
/// Defines the minimum fact value needed to be able to place a label on the bar.
/// </summary>
Machinata.Reporting.Node.AlignmentTableNode.defaults.barLabelMinimumValueRequiredToShow = 0.04;

/// <summary>
/// Defines the minimum fact value needed to be able to place a label on the bar.
/// </summary>
Machinata.Reporting.Node.AlignmentTableNode.defaults.barLabelYOffsetFactor = 0.0;

/// <summary>
/// Defines the amount of padding to place around each row group.
/// </summary>
Machinata.Reporting.Node.AlignmentTableNode.defaults.barHeightFactor = 1.4;
/// <summary>
/// Defines the amount of padding to place around each row group.
/// </summary>
Machinata.Reporting.Node.AlignmentTableNode.defaults.rowGroupPaddingMultiplier = 2;

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.AlignmentTableNode.getVegaSpec = function (instance, config, json, nodeElem) {
    // Call parent
    return Machinata.Reporting.Node["GraphicalCompositionNode"].getVegaSpec(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.AlignmentTableNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    // Call parent
    Machinata.Reporting.Node["GraphicalCompositionNode"].applyVegaData(instance, config, json, nodeElem, spec);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.AlignmentTableNode.init = function (instance, config, json, nodeElem) {
    // Init
    var ALIGNMENT_DEBUG = false;
    json._state.graphics = [];
    var stackedMode = json.stackedBars == true; //TODO

    // Compile columns
    var columns = [];
    var columnTotalCellWidth = 0;
    Machinata.Util.each(json.columnDimension.dimensionGroups, function (groupIndex, groupJSON) {
        var groupColumn = null;
        if (stackedMode && groupJSON.groupType == "alignments") {
            // In stacked mode we add a column for the alignments group
            groupColumn = {};
            groupColumn.titleResolved = Machinata.Reporting.Text.resolve(instance, groupJSON.name);
            groupColumn.id = groupJSON.id;
            groupColumn.groupType = groupJSON.groupType;
            groupColumn.stackedColumns = []; // will store all the columns in this stack (group)
            groupColumn.sizeFactor = groupJSON.sizeFactor;
            if (groupColumn.sizeFactor == null) groupColumn.sizeFactor = 1;
            groupColumn.sizeFactor = groupJSON.dimensionElements.length * groupColumn.sizeFactor;
            columnTotalCellWidth += groupColumn.sizeFactor;
            columns.push(groupColumn);
        }
        Machinata.Util.each(groupJSON.dimensionElements, function (columnIndex, columnJSON) {
            var column = {};
            column.titleResolved = Machinata.Reporting.Text.resolve(instance,columnJSON.name);
            column.id = columnJSON.id;
            column.valueType = columnJSON.valueType;
            column.groupType = groupJSON.groupType;
            column.colorShade = columnJSON.colorShade;
            column.sizeFactor = columnJSON.sizeFactor;
            if (column.sizeFactor == null) column.sizeFactor = 1;
            if (stackedMode && groupColumn != null) {
                groupColumn.stackedColumns.push(column);
            } else {
                columnTotalCellWidth += column.sizeFactor;
                columns.push(column);
            }
            // Legend?
            if (json.insertLegend != false && groupJSON.groupType == "alignments") {
                json._state.graphics.push({
                    "type": "legend",
                    "titleResolved": column.titleResolved,
                    "colorShade": columnJSON.colorShade,
                });
            }
        });
    });

    // Stats
    var headerHeight = 10;
    var numColumns = columns.length;
    var numRowGroups = 0;
    for (var i = 0; i < json.rowDimension.dimensionGroups.length; i++) {
        if (json.rowDimension.dimensionGroups[i].enabled == false) continue;
        numRowGroups++;
    }

    function xSignalRel(val) {
        return "(width * " + val + ")";
    }
    function ySignalRel(val) {
        return "(height * " + val + ")";
    }
    function ySignalRelWithoutHeader(val) {
        return "(" + headerHeight + " + ((height - " + headerHeight + ") * " + val + "))";
    }
    function xSignalAbs(val) {
        return "" + val;
    }
    function ySignalAbs(val) {
        return "" + val;
    }

    // Apply column dimensions
    var barHeight = "(config_chartTextSize * " + json.barHeightFactor+")";
    var columnWidthRel = 1 / columnTotalCellWidth;
    var currentCell = 0;
    for (var i = 0; i < columns.length; i++) {
        columns[i].x1Rel = currentCell * columnWidthRel;
        columns[i].x2Rel = columns[i].x1Rel + columnWidthRel * columns[i].sizeFactor;
        columns[i].xCRel = (columns[i].x1Rel + columns[i].x2Rel) / 2; // center
        columns[i].xLCRel = columns[i].xCRel; // label center
        columns[i].index = i;
        currentCell += columns[i].sizeFactor;
        // Column alignment
        columns[i].labelAlign = null;
        if (stackedMode == true) {
            // Text alignment
            columns[i].labelAlign = "left";
            if (i == 0) columns[i].labelAlign = "left";
            if (i == numColumns - 1) columns[i].labelAlign = "right";
        } else {
            // Text alignment
            columns[i].labelAlign = "left";
            if (column.groupType == "alignments") columns[i].labelAlign = "center";
            if (i == 0) columns[i].labelAlign = "left";
            if (i == numColumns - 1) columns[i].labelAlign = "right";
        }
        // Label positining
        if (columns[i].labelAlign == "left") columns[i].xLCRel = columns[i].x1Rel;
        else if (columns[i].labelAlign == "right") columns[i].xLCRel = columns[i].x2Rel;
        else columns[i].xLCRel = columns[i].xCRel;
    }
    if (ALIGNMENT_DEBUG == true) console.log("columns", columns);
    if (ALIGNMENT_DEBUG == true) console.log("columnTotalCellWidth", columnTotalCellWidth);

    // Header
    {
        Machinata.Util.each(columns, function (columnIndex, column) {
            // Column label
            json._state.graphics.push({
                "type": "text",
                "x": xSignalRel(column.xLCRel),
                "y": ySignalRel(0.0),
                "align": column.labelAlign,
                "fontWeight": "normal",
                "fontSize": "chart",
                "text": { "resolved": column.titleResolved }
            });
        });
        // Rule
        json._state.graphics.push({
            "type": "rule",
            "x1": xSignalRel(0),
            "y1": ySignalAbs(headerHeight),
            "x2": xSignalRel(1),
            "y2": ySignalAbs(headerHeight),
            "color": "black",
            "strokeWidth": 1.0 
        });
    }

    // Process row groups
    var groupHeightRel = 1.0 / numRowGroups;
    var groupLabelImagePaddingSignal = "(config_padding * 3)";
    var groupLabelImageSizeSignal = "(" + ySignalRel(groupHeightRel) + " - " + groupLabelImagePaddingSignal+")";
    if (ALIGNMENT_DEBUG == true) console.log("groupLabelImageSizeSignal", groupLabelImageSizeSignal);
    Machinata.Util.each(json.rowDimension.dimensionGroups, function (groupIndex, groupJSON) {
        // Enabled?
        if (groupJSON.enabled == false) return;

        // Group dimensions
        var groupY1Rel = groupIndex * groupHeightRel;
        var groupY2Rel = groupY1Rel + groupHeightRel;
        var groupPaddingSignal = "(config_padding*" + json.rowGroupPaddingMultiplier+")";
        var numRows = groupJSON.dimensionElements.length;
        var rowHeightSignal = "( (" + ySignalRel(groupHeightRel) + "-(" + groupPaddingSignal+"*2)) / "+  numRows+")";

        // Process rows
        Machinata.Util.each(groupJSON.dimensionElements, function (rowIndex, rowJSON) {
            var rowY1Signal = "( (" + ySignalRelWithoutHeader(groupY1Rel) + "+" + groupPaddingSignal+")" + " + (" + rowIndex +"*"+ rowHeightSignal + ") )";
            var rowY2Signal = "(" + rowY1Signal + " + " + rowHeightSignal+")";
            var rowYCSignal = "( ( (" + rowY1Signal + ") + (" + rowY2Signal + ") ) / 2 )";
            if (ALIGNMENT_DEBUG == true) console.log("row", { rowYCSignal: rowYCSignal, rowJSON: rowJSON });

            // Helper function lookup row
            function getFactByColumnId(id) {
                for (var i = 0; i < rowJSON.facts.length; i++) {
                    if (rowJSON.facts[i].col == id) return rowJSON.facts[i];
                }
                return null;
            }

            // Process column cells
            Machinata.Util.each(columns, function (columnIndex, column) {
                
                // Debug
                if (ALIGNMENT_DEBUG == true) {
                    json._state.graphics.push({
                        "type": "rect",
                        "x": xSignalRel(column.x1Rel) ,
                        "y": rowY1Signal,
                        "width": xSignalRel(column.x2Rel - column.x1Rel),
                        "height": rowHeightSignal,
                        "color": "red",
                        "strokeColor": "green",
                    });
                    json._state.graphics.push({
                        "type": "rect",
                        "xc": xSignalRel(column.xCRel),
                        "yc": rowYCSignal,
                        "width": xSignalAbs(3),
                        "height": ySignalAbs(3),
                        "color": "blue",
                    });
                }
                // What type do we have?
                if (column.groupType == "alignments") {
                    // Stacked?
                    if (stackedMode == true && column.stackedColumns != null) {
                        // Stacked mode - each fact (in group) is stacked as a stacked hbar

                        // Get all facts
                        var stackFacts = [];
                        var stackTotalVal = 0;
                        Machinata.Util.each(column.stackedColumns, function (stackedColumnIndex, stackedColumn) {
                            // Get matching fact
                            var factJSON = getFactByColumnId(stackedColumn.id);
                            if (factJSON != null) {
                                if (factJSON.colorShade == null) factJSON.colorShade = stackedColumn.colorShade;
                                stackTotalVal += factJSON.val;
                                stackFacts.push(factJSON);
                            }

                        });
                        if (ALIGNMENT_DEBUG == true) console.log("stackFacts", column.id, stackFacts);
                        if (ALIGNMENT_DEBUG == true) console.log("stackTotalVal", stackTotalVal);

                        // Draw all stack facts
                        var currentXRel = column.x1Rel;
                        Machinata.Util.each(stackFacts, function (stackFactIndex, factJSON) {
                            // Bar & Label
                            var factValRel = (column.x2Rel - column.x1Rel) * (factJSON.val / stackTotalVal);
                            if(json.stackedBarsUseAbsoluteValue == true) factValRel = (column.x2Rel - column.x1Rel) * (factJSON.val);
                            var barWidth = "(" + xSignalRel(factValRel) + ")";
                            var graphic = {
                                "type": "group",
                                "x": xSignalRel(currentXRel),
                                "yc": rowYCSignal,
                                "width": barWidth,
                                "clip": true,
                                "height": barHeight,
                                "colorShade": factJSON.colorShade,
                                "children": [],
                            };
                            if (json.barLabelMinimumValueRequiredToShow == null || factJSON.val >= json.barLabelMinimumValueRequiredToShow) {
                                graphic.children.push({
                                    "type": "text",
                                    "x": barWidth + "/2",
                                    "y": barHeight + "/2",
                                    "dy": "config_chartTextSize * " + json.barLabelYOffsetFactor,
                                    "align": "center",
                                    "fontWeight": "normal",
                                    "fontSize": "chart",
                                    "colorShadeFG": factJSON.colorShade,
                                    "text": { "resolved": factJSON.resolved }
                                });
                            }
                            json._state.graphics.push(graphic);
                            // Move x pos
                            currentXRel += factValRel;
                        });

                    } else {
                        // Regular mode - each fact is in its own column...

                        // Get matching fact
                        var factJSON = getFactByColumnId(column.id);

                        // Sanity - we must have a fact
                        if (factJSON == null) {
                            console.warn("Machinata.Reporting.Node.AlignmentTableNode.init: could not find fact with id", column.col);
                            return;
                        }

                        // Text Label (behind, the label ontop is cut, allowing a color change)
                        if (false) {
                            json._state.graphics.push({
                                "type": "text",
                                "x": xSignalRel(column.xCRel),
                                "y": rowYCSignal,
                                "align": "center",
                                "fontWeight": "normal",
                                "fontSize": "chart",
                                "text": { "resolved": factJSON.resolved }
                            });
                        }
                        // Bar & Label
                        var barWidth = "(" + xSignalRel((column.x2Rel - column.x1Rel) * factJSON.val) + ")";
                        var graphic = {
                            "type": "group",
                            "xc": xSignalRel(column.xCRel),
                            "yc": rowYCSignal,
                            "width": barWidth,
                            "clip": true,
                            "height": "config_chartTextSize * 1.5",
                            "colorShade": column.colorShade,
                            "children": [],
                        };
                        if (json.barLabelMinimumValueRequiredToShow == null || factJSON.val >= json.barLabelMinimumValueRequiredToShow) {
                            graphic.children.push({
                                "type": "text",
                                "x": barWidth + "/2",
                                "y": barHeight + "/2",
                                "align": "center",
                                "fontWeight": "normal",
                                "fontSize": "chart",
                                "colorShadeFG": column.colorShade,
                                "text": { "resolved": factJSON.resolved }
                            });
                        }
                        json._state.graphics.push(graphic);
                    }
                } else {

                    // This is just a text cell...

                    // Get matching fact
                    var factJSON = getFactByColumnId(column.id);

                    // Sanity - we must have a fact
                    if (factJSON == null) {
                        console.warn("Machinata.Reporting.Node.AlignmentTableNode.init: could not find fact with id", column.col);
                        return;
                    }

                    // Position
                    var labelXRel = column.xLCRel; // by default based on header pos for column
                    var labelXRelOffset = "";
                    if (groupJSON.labelImage != null && columnIndex == 0) {
                        labelXRelOffset = " + " + groupLabelImageSizeSignal + " + config_padding";
                    }

                    // Text Label
                    json._state.graphics.push({
                        "type": "text",
                        "x": xSignalRel(labelXRel) + labelXRelOffset,
                        "y": rowYCSignal,
                        "align": column.labelAlign,
                        "fontWeight": "normal",
                        "fontSize": "chart",
                        "text": { "resolved": factJSON.resolved }
                    });
                }
            });
        });

        // Group Label Image
        if (groupJSON.labelImage) {
            // Image BG
            json._state.graphics.push({
                "type": "rect",
                "x": xSignalRel(0),
                "yc": ySignalRelWithoutHeader((groupY1Rel + groupY2Rel)/2),
                "width": groupLabelImageSizeSignal,
                "height": groupLabelImageSizeSignal,
                "colorShade": groupJSON.labelImage.background.colorShade
            });
            // Image
            json._state.graphics.push({
                "type": "image",
                "x": xSignalRel(0) + " + "+ groupLabelImageSizeSignal+"/2",
                "y": ySignalRelWithoutHeader((groupY1Rel + groupY2Rel) / 2),
                "width": groupLabelImageSizeSignal,
                "height": groupLabelImageSizeSignal,
                "image": groupJSON.labelImage.image
            });
        }

        // Group Rule
        json._state.graphics.push({
            "type": "rule",
            "x1": xSignalRel(0),
            "y1": ySignalRelWithoutHeader(groupY2Rel),
            "x2": xSignalRel(1),
            "y2": ySignalRelWithoutHeader(groupY2Rel),
            "color": "black",
            "strokeWidth": 1.0
        });
    });

    if (ALIGNMENT_DEBUG == true)console.log("json._state.graphics", json._state.graphics);

    // Call parent
    Machinata.Reporting.Node["GraphicalCompositionNode"].init(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.AlignmentTableNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["GraphicalCompositionNode"].draw(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.AlignmentTableNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["GraphicalCompositionNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};







