/// <summary>
/// Collection of SVG-related utilities.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.SVG = {};

/// <summary>
/// </summary>
Machinata.Reporting.SVG.replaceUnsupportedCharacters = function (config, svgString) {
    // Remove soft-hyphens which have not been interpreted or used...
    svgString = Machinata.String.replaceAll(svgString, Machinata.SOFT_HYPHEN, "");
    return svgString;
};

/// <summary>
/// Inlines SVG resources inside a SVG.
/// Currently supported inlined SVG resources are:
///  -```<image/>```: converted to ```<use/>``` with resource in ```<defs>```
/// </summary>
Machinata.Reporting.SVG.inlineSVGResourcesInSVG = function (config, svgString) {

    return new Promise((resolve, reject) => {

        async function process() { // async wrapper
            // Init
            var DEBUG_SVG_INLINING = false;

            // Create svg doc
            var svgDoc = $(svgString);

            // Create defs in root
            var svgDefs = svgDoc.children("defs");
            if (svgDefs.length == 0) {
                svgDefs = $("<defs></defs>");
                svgDoc.prepend(svgDefs);
            }
            if (DEBUG_SVG_INLINING == true) console.log("------------------------------------------");
            if (DEBUG_SVG_INLINING == true) console.log(svgDoc[0].outerHTML);
            if (DEBUG_SVG_INLINING == true) console.log("------------------------------------------");

            // Find all images
            var svgImages = svgDoc.find("image");
            for (var i = 0; i < svgImages.length; i++) {
                // Get the image
                var svgImage = $(svgImages[i]);
                // Generate ID
                var id = Machinata.Reporting.uid();
                // Resolve href
                var href = svgImage.attr("xlink:href");
                // Validate if we can even inline it...
                var canInlineImage = false; // assume false by default
                if (href != null && href.startsWith("data:image/svg+xml") == true) canInlineImage = true; // data uri encoded svg
                else if (href != null && href.endsWith(".svg") == true) canInlineImage = true; // svg file
                if (canInlineImage == false) continue;
                // Get the actual svg data
                var svgData = null;
                try {
                    svgData = await Machinata.Reporting.Asset.resolveData(config, href);
                } catch (err) {
                    throw err;
                }
                if (DEBUG_SVG_INLINING == true) console.log("------------------------------------------");
                if (DEBUG_SVG_INLINING == true) console.log(svgData);
                if (DEBUG_SVG_INLINING == true) console.log("------------------------------------------");
                // Create svg def
                var svgDef = $(svgData); //TODO: remove comments
                svgDef.attr("id", id);
                svgDefs.append(svgDef);
                // Create use
                var svgUse = $("<use/>");
                svgUse.attr("xlink:href", "#" + id);
                // Sync all use attributes with original image
                // See https://developer.mozilla.org/en-US/docs/Web/SVG/Element/image for supported attributes
                function syncAttr(attrName) {
                    if (svgImage.attr(attrName) != null) {
                        svgUse.attr(attrName, svgImage.attr(attrName));
                    }
                }
                syncAttr("width");
                syncAttr("height");
                syncAttr("transform");
                syncAttr("class");
                syncAttr("style");
                syncAttr("x");
                syncAttr("y");
                svgUse.insertAfter(svgImage);
                // Remove image
                svgImage.remove();
            }

            if (DEBUG_SVG_INLINING == true) console.log("------------------------------------------");
            if (DEBUG_SVG_INLINING == true) console.log(svgDoc[0].outerHTML);
            if (DEBUG_SVG_INLINING == true) console.log("------------------------------------------");
            resolve(svgDoc[0].outerHTML);
        }
        try {
            process();
        } catch (err) {
            reject(err);
        }

    });

};
