


/// <summary>
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.RatingNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.RatingNode.defaults = {};

/// <summary>
/// ```solid``` chrome by default.
/// </summary>
Machinata.Reporting.Node.RatingNode.defaults.chrome = "solid";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.RatingNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.RatingNode.defaults.supportsDashboard = true;

/// <summary>
/// We do support a toolbar.
/// </summary>
Machinata.Reporting.Node.RatingNode.defaults.supportsToolbar = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.RatingNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_1x1];

/// <summary>
/// Legends are not supported.
/// </summary>
Machinata.Reporting.Node.RatingNode.defaults.insertLegend = false;


/// <summary>
/// Exports are not necessary.
/// </summary>
Machinata.Reporting.Node.RatingNode.defaults.disableExport = true;

/// <summary>
/// Controls the height of the segments relative to the default layout.
/// 1.0 is about half of the chart height.
/// By default ```1.0```.
/// </summary>
Machinata.Reporting.Node.RatingNode.defaults.layoutHeightFactor = 1.0;

/// <summary>
/// Controls the size of the arrow relative to the default layout.
/// By default ```1.0```.
/// </summary>
Machinata.Reporting.Node.RatingNode.defaults.arrowSizeFactor = 1.0;

/// <summary>
/// Controls the height of the active segments relative to the default layout.
/// By default ```1```.
/// </summary>
Machinata.Reporting.Node.RatingNode.defaults.layoutActiveHeightMultiplier = 1.0;

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.RatingNode.getVegaSpec = function (instance, config, json, nodeElem) {
    // Create segments
    var segments = json.segments;
    // Create facts
    var facts = json.facts;
    if (facts == null || json.facts.length == 0) {
        // Fallback to status
        facts = [];
        facts.push({
            val: json.status.val || json.status.value,
            label: json.status.title,
            tooltip: json.status.tooltip
        });
    }
    // Process segments
    var segmentsTotal = 0;
    for (var i = 0; i < segments.length; i++) {
        var segment = segments[i];
        if (segment.val == null) segment.val = 1;
        segment.startVal = segmentsTotal;
        segmentsTotal += segment.val;
        segment.endVal = segmentsTotal;
        segment.index = i;
    }
    // Process facts
    var factsTotal = 0;
    for (var i = 0; i < facts.length; i++) {
        var fact = facts[i];
        factsTotal += fact.val;
        fact.index = i;
        fact.position = fact.index % 2 != 0 ? "top" : "bottom";
        // Find matching segment
        for (var ii = 0; ii < segments.length; ii++) {
            var segment = segments[ii];
            if (fact.val >= segment.startVal && fact.val <= segment.endVal) {
                fact.active = segment.active;
                break;
            }
        }
    }
    // Specs
    return {
        "data": [
          {
                "name": "segments",
                "values": segments,
              "transform": [
                  {
                      "type": "formula",
                      "as": "colorBGResolved",
                      "expr": "datum.colorShade ? scale('theme-bg',datum.colorShade) : (datum.active == true ? scale('theme-bg','dark') : scale('theme-bg','gray3'))"
                  },
                  {
                      "type": "formula",
                      "as": "colorFGResolved",
                      "expr": "datum.colorShade ? scale('theme-fg',datum.colorShade) : (datum.active == true ? scale('theme-fg','dark') : scale('theme-fg','gray3'))"
                  },
                  {
                      "type": "formula",
                      "as": "titleResolved",
                      "expr": "datum.title.resolved"
                  },
                  {
                      "type": "formula",
                      "as": "tooltipResolved",
                      "expr": "datum.tooltip != null ? datum.tooltip.resolved : null"
                  },
              ]
            },
            {
                "name": "facts",
                "values": facts,
                "transform": [
                    {
                        "type": "formula",
                        "as": "colorShadeResolved",
                        "expr": "datum.colorShade ? datum.colorShade : 'bright'"
                    },
                    {
                        "type": "formula",
                        "as": "colorResolved",
                        "expr": "scale('theme-bg',datum.colorShadeResolved)"
                    },
                    {
                        "type": "formula",
                        "as": "categoryResolved",
                        "expr": "datum.category != null ? datum.category.resolved : null"
                    },
                    {
                        "type": "formula",
                        "as": "labelResolved",
                        "expr": "datum.label != null ? datum.label.resolved : null"
                    },
                    {
                        "type": "formula",
                        "as": "tooltipResolved",
                        "expr": "datum.tooltip != null ? datum.tooltip.resolved : null"
                    },
                    {
                        "type": "formula",
                        "as": "positionMultiplier",
                        "expr": "(datum.position=='top'?1:-1)"
                    },
                ]
            },
        ],
        "signals": [
            {
                "name": "segmentsTotal",
                "init": segmentsTotal,
            },
            {
                "name": "layoutWidth",
                "update": "width",
            },
            {
                "name": "layoutHeightFactor",
                "init": "0.5 * " + json.layoutHeightFactor,
            },
            {
                "name": "layoutHeight",
                "update": "height * layoutHeightFactor",
            },
            {
                "name": "arrowSizeFactor",
                "update": "2 * " + json.arrowSizeFactor,
            },
            {
                "name": "arrowSize",
                "update": "arrowLabelSize*arrowLabelSize * arrowSizeFactor",
            },
            {
                "name": "arrowOffsetY",
                "update": "sqrt(arrowSize)/arrowSizeFactor + config_padding",
            },
            {
                "name": "arrowLabelOffsetY",
                "update": "arrowOffsetY + (sqrt(arrowSize)/arrowSizeFactor)/2 + (config_padding*(arrowSizeFactor/2)) + (arrowLabelSize*config_lineHeight)",
            },
            {
                "name": "arrowLabelSize",
                "update": "config_chartTextSize",
            },
            {
                "name": "layoutPadding",
                "init": "config_padding/2",
            },
            {
                "name": "layoutActiveHeightMultiplier",
                "init": "0.4 * " + json.layoutActiveHeightMultiplier,
            },
            {
                "name": "layoutInactiveHeightMultiplier",
                "init": "1.0 - layoutActiveHeightMultiplier/2",
            },
        ],
        "scales": [
        ],
        "marks": [
            {
                // Master group
                "type": "group",
                "encode": {
                    "enter": {
                        
                        //"opacity": { "field": "opacity" },
                        //"tooltip": { "field": "titleResolved" },
                        "tooltip": { "signal": "datum" },
                    },
                    "update": {
                        "xc": { "signal": "width/2" },
                        "yc": { "signal": "height/2" },
                        "width": { "signal": "layoutWidth" },
                        "height": { "signal": "layoutHeight" },
                    }
                },
                "marks": [
                    {
                        // SEGMENTS: group
                        "type": "group",
                        "from": { "data": "segments" },
                        "signals": [
                        ],
                        "encode": {
                            "enter": {
                                "fill": { "field": "colorBGResolved" },
                                //"opacity": { "field": "opacity" },
                                //"tooltip": { "field": "titleResolved" },
                                "tooltip": { "signal": "datum.tooltipResolved" },
                            },
                            "update": {
                                "x": { "signal": "(datum.startVal/segmentsTotal) * layoutWidth + (datum.index != 0 ? layoutPadding/2 : 0)" },
                                "x2": { "signal": "(datum.endVal/segmentsTotal) * layoutWidth - (datum.index != data('segments').length-1 ? layoutPadding/2 : 0)" },
                                "yc": { "signal": "layoutHeight/2" },
                                "height": { "signal": "datum.active ? layoutHeight : layoutHeight*layoutInactiveHeightMultiplier" },
                                
                            }
                        },
                        "marks": [
                            {
                                // SEGMENT: label
                                "type": "text",
                                "encode": {
                                    "enter": {
                                        "baseline": { "value": "middle" },
                                        "align": { "value": "center" },
                                        "fill": { "signal": "parent.colorFGResolved" },
                                        "text": { "signal": "parent.titleResolved" },
                                        "tooltip": { "signal": "parent.tooltipResolved" },
                                        "fontSize": { "signal": "config_labelSize" },
                                        //"fontWeight": { "signal": "'bold'" },
                                    },
                                    "update": {
                                        "angle": { "signal": "item.mark.group.width/item.mark.group.height < 1 ? -90 : null" },
                                        "x": { "signal": "item.mark.group.width/2" },
                                        "y": { "signal": "item.mark.group.height/2" },
                                    }
                                },
                            }
                        ],
                    },
                    {
                        // FACTS: group
                        "type": "group",
                        "from": { "data": "facts" },
                        "encode": {
                            "enter": {
                            },
                            "update": {
                                "x": { "signal": "(datum.val/segmentsTotal) * layoutWidth" },
                                "y": { "signal": "layoutHeight/2 + ((layoutHeight*(datum.active?1:layoutInactiveHeightMultiplier))/2)*(datum.positionMultiplier)" },
                            }
                        },
                        "marks": [
                            {
                                // FACT: sizer
                                "type": "rect",
                                "encode": {
                                    "enter": {
                                        "fill": { "value": "transparent" },
                                        "tooltip": { "signal": "parent.tooltipResolved" },
                                    },
                                    "update": {
                                        "xc": { "value": 0 },
                                        "y": { "signal": "0" },
                                        "height": { "signal": "(arrowLabelOffsetY+arrowLabelSize) * parent.positionMultiplier" },
                                        "width": { "signal": "sqrt(arrowSize)*4" },
                                    },
                                },
                            },
                            {
                                // FACT: arrow
                                "type": "symbol",
                                "encode": {
                                    "enter": {
                                        "fill": { "signal": "chartTextColor" },
                                        "shape": { "signal": "parent.position=='top' ? 'triangle-up' : 'triangle-down'" },
                                        "xc": { "value": 0 },
                                        "size": { "signal": "arrowSize" },
                                        "tooltip": { "signal": "parent.tooltipResolved" },
                                    },
                                    "update": {
                                        "y": { "signal": "(arrowOffsetY) * parent.positionMultiplier" },
                                    },
                                },
                            },
                            {
                                // FACT: label
                                "type": "text",
                                "encode": {
                                    "enter": {
                                        "baseline": { "signal": "parent.position=='top' ? 'alphabetic' : 'top'" },
                                        "align": { "value": "center" },
                                        "text": { "signal": "parent.labelResolved" },
                                        "tooltip": { "signal": "parent.tooltipResolved" },
                                        "fontSize": { "signal": "arrowLabelSize" },
                                        //"fontWeight": { "signal": "'bold'" },
                                    },
                                    "update": {
                                        "x": { "signal": "0" },
                                        "y": { "signal": "(arrowLabelOffsetY) * parent.positionMultiplier" },
                                    }
                                },
                            }
                        ],
                    }
                ]
            },
            
          
        ]
    };
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.RatingNode.applyVegaData = function (instance, config, json, nodeElem, spec) {

};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.RatingNode.init = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.RatingNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.RatingNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};







