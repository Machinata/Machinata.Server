/// <summary>
/// EXPERIMENTAL: NOT FOR PRODUCTION USE
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.InfoCardNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.InfoCardNode.defaults = {
    chrome: "solid",
    supportsToolbar: true,
    addStandardTools: false
};

/// <summary>
/// The resolved text to display in the node. If not text is provided, then the default
/// text ```Placeholder``` is displayed.
/// </summary>
Machinata.Reporting.Node.InfoCardNode.defaults.text = null;

Machinata.Reporting.Node.InfoCardNode.init = function (instance, config, json, nodeElem) {
    var containerElem = $("<div class='node-bg sizer flippable'></div>").appendTo(nodeElem);

    var flippableElem = $("<div class='flippable-contents'></div>").appendTo(containerElem);
    var frontElem = $("<div class='flippable-front'></div>").appendTo(flippableElem);
    var backElem = $("<div class='flippable-back'></div>").appendTo(flippableElem);

        
    function buildPanel(elem, item) {
        var contentsElem = $("<div class='contents'></div>").appendTo(frontElem);


        var textElem = $("<div class='text'></div>").appendTo(contentsElem);
        textElem.html(Machinata.Reporting.Text.resolve(instance, item.html));
        
        $("<div class='clear'/>").insertAfter(textElem.find("h2,h3,h4").last());

        if (item.image != null) {
            var imgElem = $("<img class='DISABLED_effect-zoomable option-zoom-on-hover'/>");
            imgElem.attr("src", item.image);
            textElem.prepend(imgElem);
        }

        /*
        if (item.image != null) {
            var imgElem = $("<img class='image'/>").appendTo(contentsElem);
            imgElem.attr("src", item.image);
        }*/
    }

    buildPanel(frontElem, json.front);
    if (json.back != null) buildPanel(backElem, json.back);

    containerElem.find("img").addClass("dark-bg");
    //containerElem.addClass("gray-bg");


    // Apply colors
    nodeElem.find(".dark-bg").css("background-color", Machinata.Reporting.getThemeBGColorByIndex("gray", 0));
    nodeElem.find(".negative-bg").css("background-color", Machinata.Reporting.getThemeBGColorByIndex("default", 1));
    nodeElem.find(".gray-bg").css("background-color", Machinata.Reporting.getThemeBGColorByIndex("default", 5));


};
Machinata.Reporting.Node.InfoCardNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here
};
Machinata.Reporting.Node.InfoCardNode.resize = function (instance, config, json, nodeElem) {
    // Nothing to do here

    var ww = nodeElem.width();
    var hh = ww / (16/9);

    nodeElem.find(".sizer").height(hh);
};








