


/// <summary>
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.SymboledVBarNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults = {};

/// <summary>
/// By default charts are ```solid```.
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.chrome = "solid";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_4x2];

/// <summary>
/// Yes, we support toolbar.
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.supportsToolbar = true;

/// <summary>
/// No legends for this chart.
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.insertLegend = true;

/// <summary>
/// Sets the number of columns to use for legends. By default we use ```2```.
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.legendColumns = 2;

/// <summary>
/// Defines the amount of padding (or spacing) for bar-groups (in percent relative to bandwidth).
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.barBandPadding = 0.2;

/// <summary>
/// Defines the size of the line values relative to its min/max bar (in decimal percent).
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.lineFactSize = 1.0;

/// <summary>
/// Defines the size of the symbol values.
/// By default this is ```Machinata.Reporting.Config.graphSymbolSize```.
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.symbolFactSize = Machinata.Reporting.Config.graphSymbolSize;;

/// <summary>
/// Defines the maximum width of a bar in pixels. If there is not enough space, then the bar may be
/// smaller.
/// By default this is set to ```Machinata.Reporting.Config.verticalBarMaxSize * 2```.
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.barMaxSize = Machinata.Reporting.Config.verticalBarMaxSize * 2;

/// <summary>
/// Can be:
///  - ```greedy```
///  - ```parity```
///  - ```none```
/// By default we use a ```greedy``` overlap strategy.
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.xAxisLabelOverlap = "greedy";

/// <summary>
/// If set and xAxisLabelOverlap is ```greedy```, defines the axis minimum label 
/// seperation from its neighbors for the label to be shown.
/// By default this is null, which uses the vega default.
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.xAxisLabelSeperation = null;

/// <summary>
/// Defines the maximum characters a x-axis label can have before is line-breaked.
/// Note that x-axis labels are allowed maximum ```xAxisLabelLimitLines``` lines.
/// By default ```12```
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.xAxisLabelLimitChars = 12;

/// <summary>
/// The maximumum number of lines a label can span. If the maximum lines exceed, the 
/// ```xAxisLabelLimitChars``` is automatically postfixed.
/// By default ```2```.
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.xAxisLabelLimitLines = 3;

/// <summary>
/// If ```true```, the legend data is automatically sorted by symbol type.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.automaticallySortLegendBySymbolType = true;

/// <summary>
/// When ```true```, facts can have tooltips which are displayed on the x-axis labels.
/// If a lable was automatically truncated, a tooltip is automatically generated (if not already set).
/// By default ```false```.
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.xAxisShowTooltipsInLabel = false;

/// <summary>
/// If set, defines the target number of ticks to display for the axis.
/// Note: this number is only a hint, the actual number of ticks will 
/// match the best possible and nicest numbers for the value domain, targetting
/// the given number of ticks as close as possible.
/// By default ```null```.
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.yAxisTickCount = null;

/// <summary>
/// If ```true```, includes the slice category in the label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.legendLabelsShowCategory = true;

/// <summary>
/// If ```true```, includes the slice value in the legend label.
/// By default ```false```.
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.defaults.legendLabelsShowValue = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.getVegaSpec = function (instance, config, json, nodeElem) {

    return {
        "data": [
            {
                "name": "series",
                "values": null
            },
            {
                "name": "seriesTitles", // we have to provide these pre-computed, because there is no way around vega not providing a ordinal range from data with duplicate values...
                "values": null
            },
          {
              "name": "seriesResolved",
              "source": "series",
              "transform": [
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "key",
                    "expr": "datum.id"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "titleResolved",
                      "expr": "datum.title.resolved"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "tooltipResolved",
                      "expr": "datum.tooltip != null ? datum.tooltip.resolved : null"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "xAxisTitleResolved",
                      "expr": "datum.titleLineBreaked != null ? datum.titleLineBreaked : datum.title.resolved"
                  },
              ]
          },
          {
              "name": "facts",
              "source": "seriesResolved",
              "transform": [
                {
                    "type": "flatten",
                    "fields": ["facts"],
                    "as": ["fact"]
                }
              ]
          },
          {
              "name": "factsResolved",
              "source": "facts",
              "transform": [

                {
                    "type": "formula",
                    "initonly": true,
                    "as": "key",
                    "expr": "datum.id" //TODO
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "category",
                    "expr": "datum.fact.category"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "val",
                    "expr": "datum.fact.val"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "categoryResolved",
                      "expr": "datum.category.resolved"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "symbolType",
                      "expr": "datum.fact.symbolType"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "showLabel",
                      "expr": "datum.fact.showLabel == null ? false : datum.fact.showLabel"
                  },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "titleResolved",
                    "expr": "datum.title.resolved"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "valResolved",
                    "expr": "datum.fact.resolved"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "formatType",
                    "expr": "datum.formatType ? datum.formatTyped : 'number'" // default
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "format",
                    "expr": "datum.format ? datum.format : '.1%'" // default
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "serieId",
                    "expr": "datum.id"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "legendKey",
                    "expr": "datum.categoryResolved"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "colorResolved",
                    "expr": "datum.fact.colorShade != null ? scale('theme-bg',datum.fact.colorShade) : ( datum.symbolType == 'line' ? scale('theme-bg','gray4') : (datum.symbolType == 'dot' ? scale('theme-bg','mid') : ( datum.symbolType == 'diamond' ? scale('theme-bg','dark') : scale('theme-bg','gray3') )   ) )"
                }
              ]
          },
          {
              "name": "factsSymbolTypes",
              "source": "factsResolved",
              "transform": [
                {
                    "type": "filter", 
                    "expr": "datum.symbolType != 'max'"
                },
                {
                    "type": "aggregate",
                    //"groupby": ["symbolType", "categoryResolved", "colorResolved"]
                    "groupby": ["legendKey", "colorResolved"]
                }
              ]
          },
          {
              "name": "factsStats",
              "source": "factsResolved",
              "transform": [
                {
                    "type": "aggregate",
                    "fields": ["val", "val"],
                    "ops": ["min", "max"],
                    "as": ["valMin", "valMax"]
                }
              ]
          }
        ],
        "signals": [
            {
                "name": "factsValMin",
                /*"_comment": "Just gets the min value from the aggregate data",*/
                "init": "data('factsStats')[0].valMin"
            },
            {
                "name": "factsValMax",
                /*"_comment": "Just gets the max value from the aggregate data",*/
                "init": "data('factsStats')[0].valMax"
            },
            {
                "name": "factsValAbsMinMax",
                /*"_comment": "Gets the absolute max domain value",*/
                "init": "max(abs(factsValMin),abs(factsValMax))"
            },
            {
                "name": "factsDomainMin",
                /*"_comment": "If our domain spans minus and positive, then we use the factsValAbsMinMax value, otherwise we use the 0 or a minus value",*/
                "init": "if(factsValMin < 0 && factsValMax > 0   ,   -factsValAbsMinMax,  min(0, factsValMin)  ) "
            },
            {
                "name": "factsDomainMax",
                /*"_comment": "Just like factsDomainMax, just in reverse",*/
                "init": "if(factsValMin < 0 && factsValMax > 0   ,   +factsValAbsMinMax,   max(0, factsValMax)  )"
            },
            {
                "name": "configAxisSize",
                "init": config.axisSize
            },
            {
                "name": "configdomainLineSize",
                "init": config.domainLineSize
            },
        ],
        "scales": [
            {
                // This is a workaround/hack for Vega using a fixed scale
                "name": "yAxisFormat",
                "type": "ordinal",
                "domain": ["yAxisFormat"],
                "range": { "data": "factsResolved", "field": "format" }
            },
            {
                // This is a workaround/hack for Vega using a fixed scale
                "name": "yAxisFormatType",
                "type": "ordinal",
                "domain": ["yAxisFormatType"],
                "range": { "data": "factsResolved", "field": "formatType" }
            },
          {
              /*"_comment": "This is the main y scale bandwidth for groups of bars WITHOUT padding",*/
              "name": "yScaleNoPadding",
              "type": "band",
              "domain": {
                  "data": "factsResolved",
                  "field": "categoryResolved"
              },
              "range": "height",
              "padding": 0
          },
          {
              // Provide the proper color for the legend
              "name": "legendColor",
              "type": "ordinal",
              "domain": { "data": "factsSymbolTypes", "field": "legendKey" },
              "range": { "data": "factsSymbolTypes", "field": "colorResolved" }, // BUG: if two keys have same color, the ordinal scale ignores it
              //"range": { "signal": "data('factsSymbolTypes')" },
            },
            {
                // Provide the proper tooltip for the axis
                "name": "xScaleTooltip",
                "type": "ordinal",
                "domain": { "data": "seriesResolved", "field": "key" },
                "range": { "data": "seriesResolved", "field": "tooltipResolved" },
            },
            {
                // Provide the proper label for the x-axis
                // Note: range must be unique if using data, else must provide a signal!
                "name": "xScaleLabel",
                "type": "ordinal",
                "domain": { "data": "seriesResolved", "field": "key" },
                "range": { "signal": "data('seriesTitles')" },
                //"range": { "data": "seriesResolved", "field": "xAxisTitleResolved" }
            },
            /*{
                // Provide the proper index of the xscale ticks
                "name": "xScaleIndex",
                "type": "band",
                "domain": { "data": "seriesResolved", "field": "key" },
                "range": { "step": 1 }
            },*/
            {
                "name": "xScale",
                "type": "band",
                "domain": {
                    "data": "seriesResolved",
                    "field": "key"
                },
                "padding": json.barBandPadding,
                "range": "width",
                "round": true, // setting round to false will cause a strange decoupled yaxis where the zero round is floating off
                "zero": true,
                "nice": true
                //"nice": { "signal": "scale('yAxisTickCount','yAxisTickCount')" }
                //"nice": true // setting nice to true will cause the axis domain to snap to a value, leading to a side affect that two axis' no longer align, setting it to false will cause the top/bottom of the bars/lines to hang over the edge of the yaxis
            },
            {
                "name": "yScale",
                "type": "linear",
                "domain": {
                    "data": "factsResolved",
                    "field": "val"
                },
                "domainMin": {
                    "signal": "data('series')[0].yAxis != null ? data('series')[0].yAxis.minValue : factsDomainMin",
                },
                "domainMax": {
                    "signal": "data('series')[0].yAxis != null ? data('series')[0].yAxis.maxValue : factsDomainMax",
                },
                "padding": config.yAxisLabelPadding,
                "range": "height",
                "round": true, // setting round to false will cause a strange decoupled yaxis where the zero round is floating off
                "zero": true,
                "nice": true
            }
            
        ],
        "axes": [     
            Machinata.Reporting.Node.VegaNode.Util.applySettingsToVegaAxis(instance, config, json,{
                "orient": "bottom",
                "scale": "xScale",
                //"values": { "signal": "data('series')[0].xAxis != null? data('series')[0].xAxis.values : null" },
                //"tickValues": { "signal": "data('series')[0].xAxis != null? data('series')[0].xAxis.tickValues : null" },
                //"labelValues": { "signal": "data('series')[0].xAxis != null? data('series')[0].xAxis.labelValues : null" },
                "domain": false,
                "labelOverlap": json.xAxisLabelOverlap, // The strategy to use for resolving overlap of axis labels. If false (the default), no overlap reduction is attempted. If set to true or "parity", a strategy of removing every other label is used (this works well for standard linear axes). If set to "greedy", a linear scan of the labels is performed, removing any label that overlaps with the last visible label (this often works better for log-scaled axes).
                "labelSeparation": json.xAxisLabelSeperation, // The minimum separation that must be between label bounding boxes for them to be considered non-overlapping (default 0). This property is ignored if labelOverlap resolution is not enabled.
                "encode": {
                    "labels": {
                        "update": {
                            "text": { "signal": "scale('xScaleLabel',datum.value)" },
                            "dy": { "signal": ((json.xAxisLabelAngle == -90 && json.xAxisLabelLimitChars > 0) ? "(scale('xScaleLabel',datum.value).length-1) * -1 * configAxisSize * 0.7" : "0") } // since vega doesnt center multiline labels properly, we have to do it ourselves...
                        }
                    }
                }
            }),
            {
                "orient": "left",
                "scale": "yScale",
                "tickSize": 0,
                "tickRound": true,
                "tickCount": json.yAxisTickCount,
                //"values": { "signal": "data('series')[0].yAxis != null? data('series')[0].yAxis.values : null" },
                //"tickValues": { "signal": "data('series')[0].yAxis != null? data('series')[0].yAxis.tickValues : null" },
                //"labelValues": { "signal": "data('series')[0].yAxis != null? data('series')[0].yAxis.labelValues : null" },
                //"labelPadding": 4,
                //"format": { "signal": "scale('yAxisFormat','yAxisFormat')" },
                "format": { "signal": "(data('series')[0].yAxis != null && data('series')[0].yAxis.format != null) ? data('series')[0].yAxis.format : scale('yAxisFormat','yAxisFormat')" },
                //"formatType": { "signal": "scale('yAxisFormatType','yAxisFormatType')" },
                "formatType": { "signal": "(data('series')[0].yAxis != null && data('series')[0].yAxis.formatType != null) ? data('series')[0].yAxis.formatType : scale('yAxisFormatType','yAxisFormatType')" },
                "zindex": 1,
                "grid": true,
                "domain": false
            }
        ],
        "marks": [
          {
              "type": "group",
              "zindex": 4,
              "from": {
                  "facet": {
                      "name": "facet",
                      "data": "factsResolved",
                      "groupby": "key"
                  }
              },
              "data": [
                  {
                      "name": "facetSymbols",
                      "source": "facet",
                      "transform": [
                        { "type": "filter", "expr": "datum.symbolType == 'dot' || datum.symbolType == 'diamond'" }
                      ]
                  },
                  {
                      "name": "facetMin",
                      "source": "facet",
                      "transform": [
                        { "type": "filter", "expr": "datum.symbolType == 'min'" },
                          { "type": "formula", "as": "yOffset", "expr": "(datum.val == 0) ? (-configdomainLineSize/2) : 0" } // the offset is in pixels and helps the design if the bandwidths start/stop exactly on the zero-axis
                      ]
                  },
                  {
                      "name": "facetMax",
                      "source": "facet",
                      "transform": [
                          { "type": "filter", "expr": "datum.symbolType == 'max'" },
                          { "type": "formula", "as": "yOffset", "expr": "(datum.val == 0 || datum.val==domain('yScale')[1]) ? (+configdomainLineSize/2) : 0" } // the offset is in pixels and helps the design if the bandwidths start/stop exactly on the zero-axis
                      ]
                  },
                  {
                      "name": "facetLine",
                      "source": "facet",
                      "transform": [
                          { "type": "filter", "expr": "datum.symbolType == 'line'" }
                      ]
                  },
                  {
                      "name": "facetLineLabels",
                      "source": "facetLine",
                      "transform": [
                          { "type": "filter", "expr": "datum.showLabel == true" }
                      ]
                  },
                  {
                      "name": "facetSymbolsLabels",
                      "source": "facetSymbols",
                      "transform": [
                          { "type": "filter", "expr": "datum.showLabel == true" }
                      ]
                  },
              ],
              "encode": {
                  "update": {
                      "x": { "scale": "xScale", "field": "key" }
                  }
              },
              "signals": [
                    {
                        /*"_comment": "This signal limits the bar size by automatically limiting the total space allowed",*/
                        "name": "width",
                        "update": "(bandwidth('xScale')) > " + json.barMaxSize + " ? " + json.barMaxSize + " : bandwidth('xScale')"
                    },
                    { "name": "fullWidth", "update": "bandwidth('xScale')" },
              ],
              "marks": [
                  
                {
                    "name": "bars",
                    "type": "group",
                    "encode": {
                        "update": {
                            "x": { "signal": "fullWidth/2 - width/2" },
                            "width": { "signal": "width" }
                        }
                    },
                    "marks": [
                        {
                            "type": "rect",
                            "from": {
                                "data": "facetMin"
                            },
                            "encode": {
                                "enter": {
                                    "fill": { "signal": "datum.colorResolved" },
                                    "tooltip": { "signal": "datum.titleResolved + ' ' + datum.categoryResolved + ': ' + datum.valResolved + ' - ' + data('facetMax')[0].valResolved" }
                                },
                                "update": {
                                    "x": { "signal": "0" },
                                    "x2": { "signal": "width" },
                                    "y": { "signal": "scale('yScale',data('facetMax')[0].val) + data('facetMax')[0].yOffset" }, // max
                                    "y2": { "signal": "scale('yScale',datum.val) + datum.yOffset" }, // min
                                }
                            }
                        },
                        {
                            "type": "rect",
                            "name": "lineRects",
                            "from": {
                                "data": "facetLine"
                            },
                            "encode": {
                                "enter": {
                                    "fill": { "signal": "datum.colorResolved" },
                                    "tooltip": { "signal": "datum.titleResolved + ' ' + datum.categoryResolved + ': ' + datum.valResolved" }
                                },
                                "update": {
                                    "x": { "signal": "width*" + (1.0-json.lineFactSize) },
                                    "x2": { "signal": "width - width*" + (1.0 - json.lineFactSize) },
                                    "y": { "signal": "scale('yScale',datum.val)-" + (config.graphLineSize) }, 
                                    "y2": { "signal": "scale('yScale',datum.val)+" + (config.graphLineSize) }, 
                                }
                            }
                        },
                        {
                            "type": "symbol",
                            "from": {
                                "data": "facetSymbols"
                            },
                            "encode": {
                                "enter": {
                                    "shape": { "signal": "datum.symbolType == 'dot' ? 'circle' : datum.symbolType" },
                                    "fill": { "signal": "datum.colorResolved" },
                                    "tooltip": { "signal": "datum.titleResolved + ' ' + datum.categoryResolved + ': ' + datum.valResolved" }
                                },
                                "update": {
                                    "x": { "signal": "width/2" },
                                    "y": { "scale": "yScale", "field": "val" },
                                    "size": { "signal": json.symbolFactSize * json.symbolFactSize }
                                }
                            }
                        },
                        {
                            "type": "text",
                            "from": {
                                "data": "facetLineLabels"
                            },
                            "encode": {
                                "enter": {
                                    "text": { "signal": "datum.valResolved" },
                                    "align": { "value": "center" },
                                    "baseline": { "value": "bottom" },
                                    //"fill": { "signal": "datum.colorResolved" },
                                    "tooltip": { "signal": "datum.titleResolved + ' ' + datum.categoryResolved + ': ' + datum.valResolved" }
                                },
                                "update": {
                                    "x": { "signal": "width/2" },
                                    "y": { "signal": "scale('yScale',datum.val) - (config_chartTextSize/2)" },
                                }
                            }
                        },
                        {
                            "type": "text",
                            "from": {
                                "data": "facetSymbolsLabels"
                            },
                            "encode": {
                                "enter": {
                                    "text": { "signal": "datum.valResolved" },
                                    "align": { "value": "center" },
                                    "baseline": { "value": "bottom" },
                                    "tooltip": { "signal": "datum.titleResolved + ' ' + datum.categoryResolved + ': ' + datum.valResolved" }
                                },
                                "update": {
                                    "x": { "signal": "width/2" },
                                    "y": { "signal": "scale('yScale',datum.val) - (" + (json.symbolFactSize *0.75) +") - (config_chartTextSize/2*0)" },
                                }
                            }
                        },
                    ]
                }
              ]
          },
          {
              /*"_comment": "Y-ZERO GROUP - we need a group so that we can dynamically toggle it on off due to vegas limitations - we do this via the clip property on the group. Note that we don't want to clip if the zero is on the edge (since the line has a middle-out thickness)",*/
              "name": "yzerotoggle",
              "zindex": 3,
              "type": "group",
              "encode": {
                  "enter": {
                      "clip": { "signal": "(scale('yScale',0) > height || scale('yScale',0) < 0) ? true : false" }
                  },
                  "update": {
                      "x": { "value": 0 },
                      "y": { "value": 0 },
                      "width": { "signal": "width" },
                      "height": { "signal": "height" }
                  }
              },
              "marks": [
                  {
                      /*"_comment": "Y-ZERO RULE",*/
                      "name": "yzero",
                      "type": "rule",
                      "encode": {
                          "enter": {
                              "stroke": { "value": "black" },
                              "strokeWidth": { "value": config.domainLineSize }
                          },
                          "update": {
                              "x": { "value": 0 },
                              "x2": { "signal": "width" },
                              "y": { "signal": "scale('yScale',0)" }
                          }
                      }
                  }
              ]
            }
            
        ]
    };
};
Machinata.Reporting.Node.SymboledVBarNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    // Insert series
    spec["data"][0].values = json.series;
    spec["data"][1].values = json._state.allSeriesTitles;
};
Machinata.Reporting.Node.SymboledVBarNode.init = function (instance, config, json, nodeElem) {

    // Preprocess
    for (var s = 0; s < json.series.length; s++) {
        var serie = json.series[s];
        // Preprocess axis
        Machinata.Reporting.Node.VegaNode.Util.preprocessAxisProperties(serie.xAxis);
        Machinata.Reporting.Node.VegaNode.Util.preprocessAxisProperties(serie.yAxis);
    }

    // Convert serie titles into multilines...
    var maxXAxisLabelLines = 0;
    if (json.xAxisLabelLimitChars != null && json.xAxisLabelLimitChars > 0) {
        Machinata.Util.each(json.series, function (index, jsonSerie) {
            var titleResolved = Machinata.Reporting.Text.resolve(instance,jsonSerie.title);
            var titleLineBreaked = null;
            titleLineBreaked = Machinata.Reporting.Tools.lineBreakString(titleResolved, {
                maxCharsPerLine: json.xAxisLabelLimitChars,
                maxLines: json.xAxisLabelLimitLines,
                wordSeparator: " ",
                ellipsis: config.textTruncationEllipsis,
                appendEllipsisOnLastLine: true,
                returnArray: true
            });
            jsonSerie.titleLineBreaked = titleLineBreaked;
            maxXAxisLabelLines = Math.max(maxXAxisLabelLines, jsonSerie.titleLineBreaked.length);
            if (jsonSerie.tooltip == null && titleLineBreaked.length > 1) {
                jsonSerie.tooltip = {
                    resolved: titleResolved
                }
            }
        });
    }
    json._state.allSeriesTitles = [];
    Machinata.Util.each(json.series, function (index, jsonSerie) {
        json._state.allSeriesTitles.push(jsonSerie.titleLineBreaked || jsonSerie.title.resolved);
    });

    // Generate legend data
    Machinata.Reporting.Node.VegaNode.Util.createLegendForSeriesFacts(instance, config, json, json.series);
    
    // Convert legend data to common legend symbols
    for (var i = 0; i < json.legendData.length; i++) {
        var legendDatum = json.legendData[i];
        if (legendDatum.symbol == "max") {

            legendDatum.symbol = "square";
            legendDatum.symbolOrder = "01_a_max";

        } else if (legendDatum.symbol == "min") {

            legendDatum.symbol = "square";
            legendDatum.symbolOrder = "01_b_min";

        } else if (legendDatum.symbol == "line") {

            legendDatum.symbol = "square";
            legendDatum.symbolOrder = "02_square";

        } else if (legendDatum.symbol == "dot") {

            legendDatum.symbol = "dot";
            legendDatum.symbolOrder = "03_dot";

        } else if (legendDatum.symbol == "diamond") {

            legendDatum.symbol = "diamond";
            legendDatum.symbolOrder = "04_diamond";

        } else {

            legendDatum.symbol = "square" // fallback
            legendDatum.symbolOrder = "01_unknown" // fallback

        }
    }

    // Sort
    if (json.automaticallySortLegendBySymbolType == true) {
        json.legendData.sort(function (a, b) {
            return (a.symbolOrder > b.symbolOrder);
        });
    }

    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.SymboledVBarNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.SymboledVBarNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.SymboledVBarNode.exportExcelFormat = function (instance, config, json, nodeElem, format, filename) {

    var workbook = Machinata.Reporting.Export.createExcelWorkbook(instance, config, json);
    workbook = Machinata.Reporting.Export.createExcelDataForCategoryChart(instance, config, json, workbook, format);
    Machinata.Reporting.Export.saveExcelWorkbook(instance, config, json, workbook, filename);
    return true;
};






