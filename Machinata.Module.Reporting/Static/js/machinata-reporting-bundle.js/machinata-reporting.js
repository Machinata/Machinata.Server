/// <summary>
/// This package is part of the Machinata Core JS library.
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (typeof Machinata === "undefined") throw "Machinata Core JS (machinata-core-bundle.js) must be loaded."


/// <summary>
/// Library for dynamically building interactive online reports including powerfull charting framework.
/// 
/// ## Documentation
/// This JS library is completely documented inline. This means you can read the documentation directly
/// within the source code. Alternatively, you can browse the online version at 
/// https://nerves.ch/documentation/machinata-reporting-bundle.js
///
/// ## Dependencies
/// This library has the following dependencies: 
///  - machinata-core-bundle.js
/// 
/// ## Structure
/// Reporting is split into these main blocks:
/// 
///  1. Configuration
///  2. Data
///  3. Index and Building
///  4. Nodes
/// 
/// ## Configuration
/// System for configuring the reporting engine.
/// 
/// ## Data
/// Tools for loading data from different sources, definition of common data format for layouting reports and providing chart data.
/// 
/// ## Index and Building
/// System for automatically compiling the desired and configured report or sub-report based on the full report.
/// 
/// ## Nodes
/// Building blocks that make up a report, including everything from tables to paragraphs to charts.
/// 
/// ## Catalogs and Content
/// The reporting framework not only will display a report, but can also manage the interface and display of catalogs of reports. 
/// These modes can even be combined to create a self-sustainable reporting UI based off of JSON files.
///
/// ## Report Lifecycle
/// Typically the routine cycle of a report should look like this:
/// - Machinata.Reporting.init()
///    - Machinata.Reporting.Data.loadData() >> Load Catalog JSON (if any)
///    - Machinata.Reporting.Data.loadData() >> Load Report JSON
///    - Machinata.Reporting.buildIndex() >> Recursively called
///        - Machinata.Reporting.buildIndex()
///            - Machinata.Reporting.buildIndex()
///                - ...
///    - Machinata.Reporting.buildContents() >> Recursively called
///        - Machinata.Reporting.Node.build()
///            - Machinata.Reporting.Node.ImplementationXYZ.preBuild()
///            - Machinata.Reporting.Node.ImplementationXYZ.init()
///            - Machinata.Reporting.Node.ImplementationXYZ.postBuild()
///        - Machinata.Reporting.buildContents()
///            - Machinata.Reporting.Node.build()
///                - Machinata.Reporting.Node.ImplementationXYZ.preBuild()
///                - Machinata.Reporting.Node.ImplementationXYZ.init()
///                - Machinata.Reporting.Node.ImplementationXYZ.postBuild()
///        - ...
///    - Machinata.Reporting.Node.ImplementationXYZ.draw() >> Draw is called stacked when everything is built
///    - Machinata.Reporting.Node.ImplementationXYZ.draw()
///    - Machinata.Reporting.Node.ImplementationXYZ.draw()
///    - Machinata.Reporting.Node.ImplementationXYZ.draw()
///    - ...
///
/// ## Required HTML
/// To get a report injected into your page, you must include the following element and call the init() routine:
/// ```
/// <div class="machinata-reporting-report">
///    <!-- Your report will inject here -->
/// </div>
/// <script>
///     var config = {
///         // Your config...
///     };
///     Machinata.ready(function () {
///         Machinata.Reporting.init(config);
///     });
/// </script>
/// ```
///
/// ## Debugging and Stats
/// You can get additional debugging information and features by enabling debugging
/// before initializing a report:
/// ```
/// Machinata.Reporting.setDebugEnabled(true);
/// ```
/// If the configuration setting ```allowConfigurationParametersViaQueryString```
/// is enabled, then any page can include the debug flag in the query string to
/// automatically set the debug to enabled:
/// ```
/// https://domain/your-report?debug=true
/// ```
/// In addition, loading statistics can be show through the ```stats``` flag:
/// ```
/// https://domain/your-report?stats=true
/// ```
/// The following timing statistics are captured:
/// ```
/// report-init             7ms     0ms...7ms
/// catalog-download        0ms     7ms...7ms
/// report-download         29ms    7ms...36ms
/// report-preinit          0ms     36ms...36ms
/// report-buildindex       3ms     36ms...39ms
/// report-buildcontents    191ms   40ms...231ms
/// report-firstdraw        78ms    152ms...230ms
/// report-postinit         0ms     231ms...231ms
/// ```
///
/// ## Automated Testing
/// For advanced automated testing systems which run within a virtualized browser,
/// the following special HTML nodes are inserted into the page which can be used
/// to assist testing systems:
///  - ```<div class='machinata-reporting-init-complete'/>```: signals the report has been fully initialized and loaded
/// 
/// ## Technical Integration Best Practices
/// 
/// ### Without using Machinata Core JS Responsive and UI module:
/// 
/// Disable some core features by setting the appropriate Machinata flags before the document is ready:
/// ```
/// // Disable some core features
/// Machinata.DISABLE_CORE_RESPONSIVE = true;
/// Machinata.DISABLE_CORE_UI = true;
/// ```
/// 
/// ### Disable automatic debug flag in query string:
/// ```
/// // Disable automatic debug flag in query string
/// Machinata.DISABLE_AUTOMATIC_DEBUG_QUERY_PARAMETER = false;
/// ```
/// 
/// </summary>
/// <namespace>Machinata</namespace>
/// <name>Machinata.Reporting</name>
/// <type>namespace</type>



/* ======== Constants ========================================================== */

/// <summary>
/// </summary>
Machinata.Reporting.TRACKING_CATEGORY_NODE = "node";

/// <summary>
/// </summary>
Machinata.Reporting.TRACKING_CATEGORY_REPORT = "report";

/// <summary>
/// </summary>
Machinata.Reporting.TRACKING_ACTION_NODE_EXPORT_SUCCESS = "node_export_success";

/// <summary>
/// </summary>
Machinata.Reporting.TRACKING_ACTION_NODE_SHARE = "node_share";

/// <summary>
/// </summary>
Machinata.Reporting.TRACKING_ACTION_NODE_ASKAQUESTION = "node_askaquestion";

/// <summary>
/// </summary>
Machinata.Reporting.TRACKING_ACTION_NODE_ADDTODASHBOARD = "node_addtodashboard";

/// <summary>
/// </summary>
Machinata.Reporting.TRACKING_ACTION_NODE_ADDTOPRESENTATION = "node_addtopresentation";

/// <summary>
/// </summary>
Machinata.Reporting.TRACKING_ACTION_NODE_SHOWINFO = "node_showinfo";

/// <summary>
/// </summary>
Machinata.Reporting.TRACKING_ACTION_NODE_COPYTOCLIPBOARD = "node_copytoclipboard";



/* ======== Variables ========================================================== */



/// <summary>
/// Indicates that a cold start is about to happen.
/// This allows some processes to be skipped on either a cold or warm start to increase performance.
/// </summary>
Machinata.Reporting._coldStart = true;

/// <summary>
/// The UID tracker for reporting nodes. This gets automatically increment as a UID is consumed.
/// </summary>
Machinata.Reporting._uid = 0;

/// <summary>
/// Stores the currently available and registered themes.
/// A mapping from theme key/id to array of colors.
/// </summary>
Machinata.Reporting._themes = {};
Machinata.Reporting._themesNew = {};

/// <summary>
/// Stores the currently available and registered layouts.
/// A mapping from layout key/id to the layout JSON definition.
/// </summary>
Machinata.Reporting._layouts = {};




/// <summary>
/// Stores the currently available and registered layouts.
/// A mapping from layout key/id to the layout JSON definition.
/// </summary>
Machinata.Reporting._debugEnabled = false;


/// <summary>
/// </summary>
Machinata.Reporting.setDebugEnabled = function (enabled) {
    Machinata.Reporting._debugEnabled = enabled;
};
/// <summary>
/// </summary>
Machinata.Reporting.isDebugEnabled = function () {
    return Machinata.Reporting._debugEnabled;
};

/// <summary>
/// </summary>
Machinata.Reporting.debug = function (str1, str2, str3, str4) {
    if (Machinata.Reporting._debugEnabled == true) Machinata.debug(str1, str2, str3, str4);
};





/* ======== Helper Functions ========================================================== */


/// <summary>
/// Generate a new (unique) reporting UID.
/// </summary>
Machinata.Reporting.uid = function() {
    Machinata.Reporting._uid++;
    return "machinata_reporting_autoid_"+Machinata.Reporting._uid;
};



/* ========  Functions ========================================================== */


/// <summary>
/// Automaticall merges in the given config parameters together with the default configs and the profile configs.
/// This method will also automatically then load the config, thus it should be your final stop for setting up a configuration.
/// </summary>
Machinata.Reporting.mergeAndLoadConfigWithDefaults = function (config, profile) {

    // Load the branding defaults configuration, if any
    if (config != null && config["brandingConfiguration"] != null) {
        Machinata.Reporting.Branding.loadBrandingConfiguration(config["brandingConfiguration"]);
    }

    // Create a new config
    var ret = null;
    var profileConfig = Machinata.Reporting.Profiles.Configs[profile];
    if (profileConfig != null) {
        Machinata.Reporting.debug("Machinata.Reporting.mergeAndLoadConfigWithDefaults: " + profile);
        ret = Machinata.Util.extend({}, Machinata.Reporting.Config, profileConfig, config);
    } else {
        ret = Machinata.Util.extend({}, Machinata.Reporting.Config, config);
    }
    ret["profile"] = profile;

    // Load the config
    Machinata.Reporting.loadConfig(config);

    // Return the newly loaded and merged config...
    return ret;
};


/// <summary>
/// Applies any of the needed configuration parameters that are not used on-the-fly. If 
/// the configuration has changed for any reason, you should call this method again to ensure
/// the new configuration is properly applied.
/// </summary>
Machinata.Reporting.loadConfig = function (config) {
    Machinata.Reporting.debug("Machinata.Reporting.loadConfig");
    // Set locales
    if (config.numberFormatLocaleDefinition != null) {
        Machinata.Reporting.debug("  " + "setting custom numberFormatLocaleDefinition");
        vega.formatLocale(config.numberFormatLocaleDefinition);
        d3.formatLocale(config.numberFormatLocaleDefinition);
    }
    if (config.timeFormatLocaleDefinition != null) {
        Machinata.Reporting.debug("  " + "setting custom timeFormatLocaleDefinition");
        vega.timeFormatLocale(config.timeFormatLocaleDefinition);
        d3.timeFormatLocale(config.timeFormatLocaleDefinition);
    }
};

/// <summary>
/// Creates empty instance object with all the empty handles needed to represent a instance of a report.
/// </summary>
/// <hidden/>
Machinata.Reporting._createEmptyInstance = function() {
    var instance = {
        uid: Machinata.Reporting.uid(),
        config: null, // Holds the currently merged and applied config JSON.
        catalogData: null, // Holds the currently compiled catalog data (if any).
        reportData: null, // Holds the currently compiled report data (if any).
        rootNode: null, // Holds the current root node of the report.
        catalogCurrentMandate: null, // If using a catalog, stores the currently selected mandate JSON.
        catalogCurrentReport: null, // If using a catalog, stores the currently selected report JSON.
        idToNodeElementLookupTable: {}, // A map from node id to node DOM element.
        idToNodeJSONLookupTable: {}, // A map from node id to node JSON.
        idToNodeOriginalJSONLookupTable: {}, // A map from node id to node JSON (only when debug enabled).
        screensToShow: null, // The screens to be displayed. int array
        screens: [], // The discovered screens in the report. Node JSON array
        chapters: [], // The discovered chapters in the report. Node JSON array
        currentScreen: null, // The currently displayed screen (if set)
        totalScreens: null, // The total number of screens.
        focusKey: null, // Parameter from query string (key) which defines a node or section to focus on.
        elems: {
            content: null, // The main report content nodes (root of all reporting content).
            catalog: null, // The catalog UI, if any.
            header: null, // The header of the page, if any. This can be set once globally at any time the DOM is loaded.
            navigation: null, // The navigation of the page, if any. This can be set once globally at any time the DOM is loaded.
            fullscreen: null, // The current fullscreen container for a sub-report, if any
        }
    };
    return instance;
};

/// <summary>
/// Initialize a report, build its toc and content, and create all underlying content and nodes.
/// This will return a instance object with all the relavant handles.
/// </summary>
Machinata.Reporting.init = function (config, onSuccess) {

    // Init and reset
    Machinata.Debugging.startTimer("report-init");

    // Instance object for all relavant handles...
    var instance = Machinata.Reporting._createEmptyInstance();
    Machinata.Reporting.setLoading(instance, true, true); // locked loading...

    // Cold start registration
    var thisInitIsColdStart = Machinata.Reporting._coldStart;
    Machinata.Reporting._coldStart = false;

    // Machinata core settings
    Machinata.Dialogs.MAX_DIALOG_WIDTH = 480;
    Machinata.Dialogs.DIALOG_ADDITIONAL_CLASSES = "machinata-reporting-dialog";

    // Resolve the profile
    var profileName = config.profile || "web";
    if (config != null && config.allowConfigurationParametersViaQueryString == true && Machinata.hasQueryParameter("profile")) {
        profileName = Machinata.queryParameter("profile");
    }

    // Create merged config
    instance.initConfig = Machinata.Util.cloneJSON(config);
    config = Machinata.Reporting.mergeAndLoadConfigWithDefaults(config, profileName);
    instance.config = config;

    // Apply some config values from query string...
    if (config.allowConfigurationParametersViaQueryString == true && Machinata.queryParameter("prevent-caching") == "true") {
        config.preventDataCaching = true;
    }
    if (config.allowConfigurationParametersViaQueryString == true && Machinata.queryParameter("debug") == "true") {
        Machinata.Reporting._debugEnabled = true;
    }
    if (config.allowConfigurationParametersViaQueryString == true && Machinata.queryParameter("automated-testing") == "true") {
        config.automatedTesting = true;
    }

    // Cache some selectors
    instance.elems.header = $(config.headerSelector);
    instance.elems.navigation = $(config.navigationSelector);
    instance.elems.content = $(config.contentSelector);
    instance.elems.catalog = $(config.catalogSelector);

    // Allow dom context to get the instance via the content node (machinata-reporting-report)
    instance.elems.content.data("instance", instance);

    // Register the report class
    instance.elems.content.addClass("machinata-reporting-report");

    // Clear DOM
    // Note: migrated to buildReport
    //if (thisInitIsColdStart == false) {
    //    instance.elems.content.children().filter(".machinata-reporting-node").remove();
    //}

    // Initial layout
    Machinata.Reporting.Responsive.updateReportLayout(instance);

    // Url?
    if (config.url == null) {
        config.url = window.location.href;
        config.url = Machinata.removeQueryParameter("chapter", config.url);
        config.url = Machinata.removeQueryParameter("focus", config.url);
        config.url = Machinata.removeQueryParameter("ui", config.url);
    }

    // Profile?
    if (Machinata.Reporting.Handler.setupForProfile != null) Machinata.Reporting.Handler.setupForProfile(config.profile);
    instance.elems.content.addClass("machinata-reporting-profile-"+config.profile);
    // Note: the body classes are still needed by webprint...
    if ($("body").attr("data-machinata-reporting-profile") == null) {
        $("body").addClass("machinata-reporting-profile-" + config.profile);
        $("body").attr("data-machinata-reporting-profile", config.profile);
    }

    // Cache some parameters
    if (config.allowConfigurationParametersViaQueryString == true) {
        instance.focusKey = Machinata.queryParameter("focus");
        if (instance.focusKey == "") instance.focusKey = null;
    }

    // Show loading in nav
    var rootNavItemElem = null;
    if (config.automaticallyInsertChapterNavigation == true && Machinata.Reporting.Handler.addNavigationItem != null) rootNavItemElem = Machinata.Reporting.Handler.addNavigationItem(instance, "Loading...", "");
    else rootNavItemElem = $("<div/>");

    // Debugging
    Machinata.Reporting.debug("Machinata.Reporting.init:");

    // Injected CSS
    if (config.injectThemeCSSIntoPage == true) {
        // Already injected?
        var cssElem = $("#machinata-reporting-css");
        if (cssElem.length == 0) {
            // Add a inline stylesheet into page
            cssElem = $("<style id='machinata-reporting-css'>" + Machinata.Reporting.Tools.generateThemeCode(config,"css")+"</style>");
            cssElem.appendTo($("head"));
        }
    }

    // Build UI: register the container and styles to reporting
    instance.elems.content.addClass("machinata-reporting-styling");
    instance.elems.content.addClass("machinata-reporting-container");

    

    // Init screen selector
    if (config.allowConfigurationParametersViaQueryString == true && config.screens == "auto") {
        Machinata.Reporting.debug("    screens is auto:");
        var screensQueryString = Machinata.queryParameter("screen"); 
        if (screensQueryString == null || screensQueryString == "") {
            instance.screensToShow = null;
            Machinata.Reporting.debug("        no screens in query, fallback to all");
        } else if (screensQueryString == "printcomposer") {
            Machinata.Reporting.debug("        screens from printcomposer");
            config.screens = "printcomposer";
            // Note: we now have to do this later since we need the report data
            //instance.screensToShow = Machinata.Reporting.PrintComposer.getScreensAsIntArray(instance);
        } else {
            Machinata.Reporting.debug("        screens from query: " + screensQueryString);
            var screenSegsString = screensQueryString.split(",");
            instance.screensToShow = [];
            for (var i = 0; i < screenSegsString.length; i++) {
                instance.screensToShow.push(parseInt(screenSegsString[i]));
            }
        }
    } else if (config.screens == "printcomposer") {
        // We have to wait until all data is loaded...?
        //instance.screensToShow = Machinata.Reporting.PrintComposer.getScreensAsIntArray(instance);
    } else if (config.screens == "*") {
        // Just show all...
        instance.screensToShow = null;
    } else if (config.screens != null && Array.isArray(config.screens)) {
        // Array of screen numbers
        instance.screensToShow = config.screens;
    } else if (config.screens != null && !isNaN(config.screens)) {
        // Single screen number
        instance.screensToShow = [parseInt(config.screens)];
    } else if (config.screens != null && Machinata.Util.isString(config.screens) && config.screens.indexOf(",") != -1) {
        // Comma separated list
        var screenSegsString = config.screens.split(",");
        instance.screensToShow = [];
        for (var i = 0; i < screenSegsString.length; i++) {
            instance.screensToShow.push(parseInt(screenSegsString[i]));
        }
    }
    if (instance.screensToShow != null) instance.currentScreen = instance.screensToShow[0];

    // Debugging
    Machinata.Reporting.debug("    chapters: " + config.chapters);
    Machinata.Reporting.debug("    screensToShow: " + instance.screensToShow);
    Machinata.Reporting.debug("    currentScreen: " + instance.currentScreen);
    Machinata.Reporting.debug("    initialChapter: " + config.initialChapter);

    // Cache some attributes needed by other modules
    //instance.elems.content.attr("data-title", json.title);
    //instance.elems.content.attr("data-subtitle", json.subtitle);

    //TODO: build and show loader, expose common loading routines...

    // Figure out catalog source
    if (config.allowConfigurationParametersViaQueryString == true && config.catalogSource == "auto") {
        config.catalogSource = Machinata.queryParameter("catalog");
        if (config.catalogSource == "") config.catalogSource = null;
    }

    Machinata.Debugging.stopTimer("report-init");
    
    // Load catalog data
    Machinata.Debugging.startTimer("catalog-download");
    if (config.loadCatalog == false) config.catalogSource = null;
    Machinata.Reporting.Data.loadData(instance, config.catalogSource, function (catalogData) {
        // Success - catalog loaded
        Machinata.Debugging.stopTimer("catalog-download");

        // Register data singleton instance
        instance.catalogData = catalogData;

        // Compile catalog and it's UI
        if (catalogData != null) {
            Machinata.Reporting.Catalog.processCatalog(instance, config, catalogData);
        }
        if (config.automaticallyBuildCatalogUI == true && catalogData != null) {
            Machinata.Reporting.Catalog.compileCatalogUI(instance, config, catalogData);
        }

        // Set dataSource if missing
        if (config.allowConfigurationParametersViaQueryString == true && config.dataSource == "auto") {
            config.dataSource = Machinata.queryParameter("data");
            if (config.dataSource == "") config.dataSource = Machinata.Reporting.Config.dataSource;
        }

        // Add chapter in nav
        var chapterNavItemElem = null;
        if (config.automaticallyInsertChapterNavigation == true && Machinata.Reporting.Handler.addNavigationItem != null) chapterNavItemElem = Machinata.Reporting.Handler.addNavigationItem(instance, "Loading...", "");
        else chapterNavItemElem = $("<div/>");

        // Load report data - at this point we can't proceed without the index and meta data
        if (config.loadReport == false) {
            config.dataSource = null;
            Machinata.Reporting.setLoading(instance, false, true); // unlock loading...
        }
        Machinata.Debugging.startTimer("report-download");
        Machinata.Reporting.Data.loadData(instance, config.dataSource, function (reportData) {
            // Success - data loaded
            Machinata.Debugging.stopTimer("report-download");
            Machinata.Debugging.startTimer("report-preinit");

            // Register data singleton instance
            instance.reportData = reportData;

            // Validate we even have anything
            if (reportData == null) {
                // Rollback the navigation loader...
                var sepElem = rootNavItemElem.prev().prev();
                var posElem = rootNavItemElem.prev();
                sepElem.remove();
                posElem.remove();
                rootNavItemElem.remove();
                return;
            }

            // If we want printcomposer screen, we can get them now since we now know the reportid
            if (config.screens == "printcomposer") {
                instance.screensToShow = Machinata.Reporting.PrintComposer.getScreensAsIntArray(instance);
            }

            // Apply custom config from data
            if (reportData["config"] != null) {
                config = Machinata.Util.extend({}, config, reportData["config"]);
                instance.config = config;
                Machinata.Reporting.loadConfig(config); // we unfortunately need to reload the config...
            }

            // Cache some common attributes
            config.reportName = Machinata.Reporting.Text.resolve(instance,reportData["reportName"]);
            if (reportData.reportTitle != null) config.reportTitle = Machinata.Reporting.Text.resolve(instance,reportData.reportTitle);
            if (reportData.reportSubtitle != null) config.reportSubtitle = Machinata.Reporting.Text.resolve(instance,reportData.reportSubtitle);
            if (reportData.reportBadge != null) config.reportBadge = reportData.reportBadge;
            Machinata.Reporting.debug("    reportName: " + config.reportName);
            Machinata.Reporting.debug("    reportTitle: " + config.reportTitle);
            Machinata.Reporting.debug("    reportSubtitle: " + config.reportSubtitle);

            // Debug dom elem?
            if (Machinata.DebugEnabled == true) {
                //TODO: re-enable debug features
                //instance.elems.content.append(Machinata.UI.createDebugPanel("Build", Machinata.buildInfo()));
                //instance.elems.content.append(Machinata.UI.createDebugPanel("Report", config));
            }

            // Initial UI update (for report meta data)
            $(".machinata-reporting-attribute-report-title").text(config.reportTitle);
            $(".machinata-reporting-attribute-report-subtitle").text(config.reportSubtitle);
            window.document.title = window.document.title.replace("Loading...", config.reportTitle);
            rootNavItemElem.find(".link").text(config.reportTitle);
            if (instance.catalogCurrentMandate != null) {
                rootNavItemElem.find(".link").attr("href", instance.catalogCurrentMandate.catalogURLResolved);
            } else {
                rootNavItemElem.find(".link").attr("href", config.url);
            }

            // UI update for data status
            if (config.reportBadge != null) {
                var badgeElem = $(".machinata-reporting-attribute-report-badge");
                if (config.reportBadge.color != null) badgeElem.addClass("option-" + config.reportBadge.color);
                badgeElem.text(Machinata.Reporting.Text.resolve(instance,config.reportBadge.label));
                if (config.reportBadge.tooltip != null) badgeElem.attr("title", Machinata.Reporting.Text.resolve(instance,config.reportBadge.tooltip));
                badgeElem.addClass("has-content");
            } else {
                $(".machinata-reporting-attribute-report-badge").remove();
            }

            // Set window title
            // Here we use the config reportWindowTitle, unless we are in webprint, in which case we try to use the filename...
            if (config.reportWindowTitle != null) {
                var windowTitleTemplateString = config.reportWindowTitle;
                if (config.profile == Machinata.Reporting.Profiles.PROFILE_WEBPRINT_KEY && reportData != null && reportData.fileName != null) {
                    windowTitleTemplateString = reportData.fileName;
                }
                var windowTitle = Machinata.Reporting.Tools.createTitleString(instance, config, windowTitleTemplateString);
                if (windowTitle != null) window.document.title = windowTitle;
            }
            
            // Build index recursively
            Machinata.Debugging.stopTimer("report-preinit");
            Machinata.Debugging.startTimer("report-buildindex");
            instance.rootNode = reportData["body"];
            Machinata.Reporting.debug("Machinata.Reporting.buildIndex:");
            Machinata.Reporting.buildIndex(instance, config, instance.rootNode, null);
            Machinata.Debugging.stopTimer("report-buildindex");

            // Validate we have at least one chapter
            //if (instance.chapters.length == 0) {
            //    Machinata.Reporting.debug("   **Setting default chapter");
            //    instance.chapters.push(rootNode.children[0]);
            //}

            // Show what happend
            Machinata.Reporting.debug("    index complete!");
            Machinata.Reporting.debug("    totalScreens: " + instance.totalScreens);
            Machinata.Reporting.debug("    totalChapters: " + instance.chapters.length);

            // Page tools
            if (config.automaticallyAddPageTools == true) {
                Machinata.Reporting.initPageTools(instance);
            }

            // Auto-chapter?
            if (config.chapters == "auto") {
                config.chapters = [];
                // Do we have from query param?
                if (config.allowConfigurationParametersViaQueryString == true && Machinata.hasQueryParameter("chapter")) {
                    config.chapters.push(Machinata.queryParameter("chapter"));
                } else if (config.initialChapter != null) {
                    if (typeof config.initialChapter == 'number') {
                        config.chapters.push(instance.chapters[config.initialChapter - 1].id);
                    } else {
                        config.chapters.push(config.initialChapter);
                    }
                } else {
                    if (instance.chapters.length > 0) {
                        config.chapters.push(instance.chapters[0].id);
                    }
                }
            }

            // Load the chapter title, if we have selected only a single chapter
            if (config.chapters != null && config.chapters.length == 1) {
                Machinata.Util.each(instance.chapters, function (index, chapterJSON) {
                    if (config.chapters[0] == chapterJSON.id) {
                        if (config.reportChapter == null) {
                            config.reportChapter = Machinata.Reporting.Text.resolve(instance,chapterJSON.mainTitle);
                        }
                    }
                });
            }
            if (config.chapters == "*") {
                if (chapterNavItemElem != null) {
                    chapterNavItemElem.prevAll(".path-sep").first().remove();
                    chapterNavItemElem.remove();
                }
            } 
            if (config.reportChapter == null) {
                config.reportChapter == ""; //fallback
            }

            // Update some attributes that might have only come up after build index
            $(".machinata-reporting-attribute-report-disclaimer").text(config.reportDisclaimer);
            $(".machinata-reporting-attribute-report-legal-entity").text(config.reportLegalEntity);
            $(".machinata-reporting-attribute-report-chapter").text(config.reportChapter);
            $(".machinata-reporting-attribute-catalog-url").attr("href", config.catalogURL);


            // Create chapters UI
            if (config.automaticallyFoldChaptersIntoPages == true) {
                $(config.chapterSelector).each(function () {
                    var chapterTabsElem = $(this);
                    var numChapters = 0;
                    var selectedChapterNumber = 0;
                    Machinata.Util.each(instance.chapters, function (index, chapterJSON) {
                        if (config.chapters != null && config.chapters.length == 1) {
                            if (config.chapters[0] == chapterJSON.id) {
                                selectedChapterNumber = index;
                            }
                        }
                        numChapters++;
                    });
                    Machinata.Util.each(instance.chapters, function (index, chapterJSON) {
                        var chapterTitle = Machinata.Reporting.Text.resolve(instance,chapterJSON.mainTitle);
                        // Create chapter button
                        var chapterButtonElem = $("<a></a>").attr("data-chapter", chapterJSON.chapterId);
                        chapterButtonElem.text(chapterTitle);
                        chapterButtonElem.attr("href", chapterJSON.chapterURL);
                        // Manage selection
                        if (config.chapters != null && config.chapters.length == 1) {
                            if (config.chapters[0] == chapterJSON.id) {
                                chapterButtonElem.addClass("selected");
                            }
                        }
                        // Add chapter elem
                        var addChapterToUI = true;
                        if (chapterTabsElem.hasClass("option-prev-next-only")) {
                            // Toggle usage
                            if (index < selectedChapterNumber - 1 || index > selectedChapterNumber + 1) addChapterToUI = false;
                            // Add icon
                            if (index < selectedChapterNumber) {
                                chapterButtonElem.prepend(Machinata.Reporting.buildIcon(instance, "chevron-left"));
                            } else if(index > selectedChapterNumber) {
                                chapterButtonElem.append(Machinata.Reporting.buildIcon(instance, "chevron-right"));
                            }
                        }
                        if (addChapterToUI == true) {
                            chapterTabsElem.append(chapterButtonElem);
                        }
                    });
                    if (numChapters <= 1) chapterTabsElem.hide();
                    // Autobalance the tabs
                    chapterTabsElem.find("a").css("width", (100 / chapterTabsElem.find("a").length) + "%");
                });
            }

            if (config.automaticallyInsertChapterNavigation && chapterNavItemElem != null) {
                Machinata.Util.each(instance.chapters, function (index, chapterJSON) {
                    var chapterTitle = Machinata.Reporting.Text.resolve(instance,chapterJSON.mainTitle);
                    // Manage selection
                    if (config.chapters != null && config.chapters.length == 1) {
                        if (config.chapters[0] == chapterJSON.id) {
                            chapterNavItemElem.find(".link").text(chapterTitle);
                        }
                    }
                    // Register sibling
                    if (instance.chapters.length == 1) return;
                    if (Machinata.Reporting.Handler.addNavigationSibling != null) {
                        var siblingNavItemElem = Machinata.Reporting.Handler.addNavigationSibling(instance, chapterTitle, chapterJSON.chapterURL);
                    }
                });
                if (instance.chapters != null && instance.chapters.length == 0) {
                    chapterNavItemElem.remove(); //TODO: what about the slash in CS?
                }
            } else {
                //TODO: handle alternative?
            }

            // Build report content

            // Build content recursively
            if (config.buildContents == true) {
                Machinata.Debugging.startTimer("report-buildcontents");
                Machinata.Reporting.buildReport(instance, config);
                Machinata.Debugging.stopTimer("report-buildcontents");
            }


            Machinata.Debugging.startTimer("report-postinit");

            // Initial focus?
            if (instance.focusKey != null) {
                // Currently we only support nodeIds...
                var linkJSON = {
                    nodeId: instance.focusKey
                };
                 // Deffered call
                setTimeout(function () {
                    Machinata.Reporting.Tools.drilldownOnNode(instance, linkJSON);
                }, 10);
            }

            // Fullscreen profile?
            if (config.profile == Machinata.Reporting.Profiles.PROFILE_FULLSCREEN_KEY) {
                // Bind arrow keys (only for fullscreen)
                $(document).keydown(function (e) {
                    if (e.which == 37) {
                        // LEFT
                        e.preventDefault();
                        Machinata.Reporting.Tools.prevScreen(instance);
                    } else if (e.which == 39) {
                        // RIGHT
                        e.preventDefault();
                        Machinata.Reporting.Tools.nextScreen(instance);
                    }

                });
            }

            // Webprint profile?
            if (config.profile == Machinata.Reporting.Profiles.PROFILE_WEBPRINT_KEY) {
                if (config.automaticallyOpenBrowserPrintDialog == true || (config.allowConfigurationParametersViaQueryString == true && Machinata.queryParameter("print-dialog") == "true")) {
                    setTimeout(function () {
                        Machinata.Reporting.Tools.openBrowserPrintDialog(instance);
                    }, 300); // seems the browser needs some breathing room so that it can properly detect the margins etc...
                }
            }

            // Insert a dom element for headless and automated systems to detect (like screenshot services)
            // We wait from some MS so that the automated test has some time to load things like images etc...
            setTimeout(function () {
                instance.elems.content.append("<div class='machinata-reporting-init-complete'/>");
            }, 250);
            
            // Success
            Machinata.Reporting.setLoading(instance, false, true); // unlock loading...
            if (onSuccess) onSuccess(instance,config);
            
            Machinata.Debugging.stopTimer("report-postinit");

            // Insert stats?
            if (config.allowConfigurationParametersViaQueryString == true && Machinata.queryParameter("stats") == "true") {
                instance.elems.content.prepend(Machinata.Debugging.createTimersDebugPanel("Stats").addClass("machinata-reporting-debug-panel"));
            }

            // Notice?
            if (config.automatedTesting != true && reportData != null && reportData["reportNotice"] != null) {
                var key = "noticeshowed_" + reportData.reportId;
                if (Machinata.getCache(key) == null) {
                    setTimeout(function () {
                        Machinata.setCache(key, true);
                        Machinata.messageDialog(Machinata.Reporting.Text.translate(instance, "reporting.notice"), Machinata.Reporting.Text.resolve(instance,reportData["reportNotice"])).show();

                    }, 600);
                }
            }

        });

    });

    // Cold start UI bindings
    // These happend immediately and must remain valid throughout multiple inits
    if (thisInitIsColdStart == true) {

        // Bind tooltips via handler, if any
        if (Machinata.Reporting.Handler.bindTooltipsUI != null) {
            Machinata.Reporting.Handler.bindTooltipsUI(instance, $(".machinata-reporting-styling")); // bind to root of document
        }

        // Register window resize events
        $(window).resize(function () {
            $(".machinata-reporting-report").each(function () {
                Machinata.Reporting.Responsive.updateForContainerSize($(this).data("instance"));
            });
        });

        // Bind resizes
        /*
        Machinata.Responsive.onResize(function () {
            // Note: this is called before onResize
            // Update all report sizes
            $(".machinata-reporting-report").each(function () {
                Machinata.Reporting.Responsive.updateReportLayout($(this).data("instance"));
            });
        });
        Machinata.Responsive.onResized(function () {
            // Note: this is called after onResize
            // Update all nodes
            TODO: should we cache this selector?
            EXPENSIVE:
            $(".machinata-reporting-node").trigger("machinata-reporting-redraw-node");
        });*/

        // Readprogress
        $(".machinata-reporting-readprogress.option-autoscroll").each(function () {
            var elem = $(this);
            var progressElem = elem.find(".progress");
            var subtract = 0;
            if ($("#footer").length != 0) subtract += $("#footer").height();
            if ($("#legal").length != 0) subtract += $("#legal").height();
            $(window).scroll(function () {
                var h = document.documentElement,
                    b = document.body,
                    st = 'scrollTop',
                    sh = 'scrollHeight';
                var percent = (h[st] || b[st]) / ((h[sh] || b[sh]) - h.clientHeight - subtract) * 100;
                if (percent == 0) elem.addClass("is-zero");
                else elem.removeClass("is-zero");
                progressElem.css("width", percent + "%");
            });
        });

        // IE 11 support
        if (config.compatibilityIE11Support == true) {
            Machinata.Compatibility.addIESupport();
        }

    }


    return instance;

};

/// <summary>
/// This function updates and binds all the page tools that reside outside of the machinata-reporting main container.
/// </summary>
Machinata.Reporting.initPageTools = function (instance) {
    // General page tools
    // Fullscreen
    if (Machinata.Reporting.Handler.addPageTool != null && instance.totalScreens > 0) {
        Machinata.Reporting.Handler.addPageTool(instance, "View fullscreen", "fullscreen", function () {
            Machinata.Reporting.Tools.openFullscreenReport(instance);
        });
    }
    
    // Resources
    if (instance.reportData != null && instance.reportData.reportResources != null && Machinata.Reporting.Handler.addPageTool != null) {
        Machinata.Util.each(instance.reportData.reportResources, function (index, resourceJSON) {
            Machinata.Reporting.Handler.addPageTool(
                instance,
                Machinata.Reporting.Text.resolve(instance,resourceJSON.tooltip),
                resourceJSON.icon,
                function () {
                    if (resourceJSON.link != null) {
                        // Link
                        Machinata.Reporting.Tools.openLink(instance,resourceJSON.link);
                    } else if (resourceJSON.attachment != null) {
                        // Attachment
                        if (resourceJSON.downloadAttachmentToBrowser == true) {
                            Machinata.Data.exportDataAsBlob(
                                resourceJSON.attachment.data,
                                resourceJSON.attachment.mimeType,
                                resourceJSON.attachment.filename
                            );
                        } else {
                            Machinata.Data.openDataInNewWindow(
                                resourceJSON.attachment.data,
                                resourceJSON.attachment.mimeType,
                                resourceJSON.attachment.filename
                            );
                        }
                    }
            });
        });
    }
    // Printing (only if we have screens)
    if (Machinata.Reporting.Handler.addPageTool != null && instance.screens != null && instance.screens.length > 0) {
        if (Machinata.Reporting.Handler.printReport != null) {
            Machinata.Reporting.Handler.addPageTool(instance, Machinata.Reporting.Text.translate(instance, "reporting.printing.print"), "print", function () {
                Machinata.Reporting.Handler.printReport(instance);
            });
        }
    }
    // Change date
    // NOTE: The calendar change date tool must be the last tool to be added!
    if (Machinata.Reporting.Handler.addPageTool != null && instance.catalogCurrentMandate != null && Machinata.Reporting.Handler.changeReportingDate != null) {
        // Add the calendar tool
        Machinata.Reporting.Handler.addPageTool(instance, Machinata.Reporting.Text.translate(instance, "reporting.change-date"), "calendar", function () {
            Machinata.Reporting.Handler.changeReportingDate(instance, instance.catalogData, instance.catalogCurrentMandate, instance.catalogCurrentReport);
        });
        // Add subtitle as text tool
        if (instance.config.insertReportSubtitleAsPageTool == true && instance.reportData != null && instance.reportData.reportSubtitle != null) {
            var subtitle = Machinata.Reporting.Text.resolve(instance, instance.reportData.reportSubtitle);
            Machinata.Reporting.Handler.addPageTool(instance, subtitle, null, function () {
                Machinata.Reporting.Handler.changeReportingDate(instance, instance.catalogData, instance.catalogCurrentMandate, instance.catalogCurrentReport);
            }, Machinata.Reporting.Text.translate(instance, "reporting.change-date"),false);
        }
    }
    
};


/// <summary>
/// Recursivley builds the index of the report. This especially has to do with figuring
/// out the logical screen numbers for a report, which allows one to seperate
/// the chapters out in regular display, but still speak of univeral screen numbers
/// across the entire report.
/// </summary>
Machinata.Reporting.buildIndex = function (instance, config, json, parent) {
    // Re-init
    instance.screens = [];

    // Recursively build index
    Machinata.Reporting._buildIndexRecursively(instance, config, json, parent);

    // Register num screens
    instance.totalScreens = instance.screens.length;

    return json;
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting._buildIndexRecursively = function (instance, config, json, parent) {

    if (json == null) json = instance.rootNode;

    if (json.enabled == false) return;

    // ID
    if (json.id == null) json.id = Machinata.Reporting.uid();
    instance.idToNodeJSONLookupTable[json.id] = json;
    

    // Tags?
    if (json.tags != null) {
        for (var i = 0; i < json.tags.length; i++) {
            var tag = json.tags[i];
            if (tag == config.tagDisclaimer) {
                json.exclude = true;
                Machinata.Reporting.debug("    Report_Disclaimer tag, excluding");
                config.reportDisclaimer = Machinata.Reporting.Text.resolve(instance,json.content);
            } else if (tag == config.tagLegalEntity) {
                json.exclude = true;
                Machinata.Reporting.debug("    Report_LegalEntity tag, excluding");
                config.reportLegalEntity = Machinata.Reporting.Text.resolve(instance,json.content);
            } else if (tag == config.tagScreen) {
                Machinata.Reporting.debug("    Screen tag, marking as screen");
                json.screen = true;
            } else if (tag == config.tagDarkChrome) {
                Machinata.Reporting.debug("    Dark tag, setting chrome to dark");
                json.chrome = "dark";
            } else if (tag == config.tagLightChrome) {
                Machinata.Reporting.debug("    Light tag, setting chrome to dark");
                json.chrome = "light";
            } else if (tag == config.tagSolidChrome) {
                Machinata.Reporting.debug("    Solid tag, setting chrome to solid");
                json.chrome = "solid";
            } else if (tag == config.tagWebPrintOnly) {
                if (config.profile != Machinata.Reporting.Profiles.PROFILE_WEBPRINT_KEY) json.exclude = true;
            } else if (tag == config.tagWebOnly) {
                if (config.profile != Machinata.Reporting.Profiles.PROFILE_WEB_KEY) json.exclude = true;
            } else if (tag == config.tagPrintOnly) {
                if (config.profile != Machinata.Reporting.Profiles.PROFILE_PRINT_KEY) json.exclude = true;
            } else if (tag == config.tagFullscreenOnly) {
                if (config.profile != Machinata.Reporting.Profiles.PROFILE_FULLSCREEN_KEY) json.exclude = true;
            }
        }
    }

    // Level
    if (parent == null) {
        json.level = 0;
        json.screenNumber = null;
        json.chapterId = null;
    } else {
        json.level = parent.level + 1;
        json.screenNumber = parent.screenNumber;
        json.chapterId = parent.chapterId;
    }

    // Chapter book keeping
    if (json.level == config.chapterLevel && json.nodeType == "SectionNode") {
        if(json.mainTitle != null) {
            json.chapterId = json.id;
            json.chapter = true; // helper marker
            if (config.headless == false) {
                json.chapterURL = Machinata.updateQueryString("chapter", json.chapterId, config.url);
                json.chapterURL = Machinata.removeQueryParameter("focus", json.chapterURL);
                json.chapterURL = Machinata.removeQueryParameter("screen", json.chapterURL);
                json.chapterURL = Machinata.removeQueryParameter("ui", json.chapterURL);
            }
            instance.chapters.push(json);
            Machinata.Reporting.debug("    new chapter: " + json.chapterId);
            if (Machinata.Reporting.Handler.addChapterNavigation != null) {
                var infos = {};
                infos.chapterURL = json.chapterURL;
                infos.chapterId = json.chapterId;
                infos.chapterTitle = Machinata.Reporting.Text.resolve(instance,json.mainTitle);
                Machinata.Reporting.Handler.addChapterNavigation(instance, infos, json);
            }
        }
    }


    // Screen number book keeping
    if (json.level == config.screenLevel && config.automaticallyGenerateScreenNumbers == true) {
        json.screenNumber = instance.screens.length;
        instance.screens.push(json);
        Machinata.Reporting.debug("    new screen: " + json.screenNumber);
    } else if(json.screen == true) {
        json.screenNumber = instance.screens.length;
        instance.screens.push(json);
        Machinata.Reporting.debug("    new screen: " + json.screenNumber);
    }

    // Inherit theme
    if (json.theme == null && parent != null) json.theme = parent.theme;

    // Validate theme
    if (json.theme != null && Machinata.Reporting.getTheme(json.theme) == null) {
        console.warn("The theme '"+json.theme+"' for node "+json.id+" is not valid. Please make sure each node has a valid theme set. Falling back to default.");
        json.theme = "default";
    }

    // URL
    if (config.headless == false && json.url == null) {
        json.url = Machinata.updateQueryString("focus", json.id, config.url);
        json.url = Machinata.updateQueryString("chapter", json.chapterId, json.url);
    }

    // Preserve copy of original? For debugging purposes...
    if (Machinata.Reporting.isDebugEnabled() && instance.idToNodeOriginalJSONLookupTable != null) {
        instance.idToNodeOriginalJSONLookupTable[json.id] = JSON.parse(JSON.stringify(json));
    }

    // Debug
    Machinata.Reporting.debug("    id: " + json.id);
    Machinata.Reporting.debug("        level: " + json.level);
    Machinata.Reporting.debug("        screen: " + json.screenNumber);

    
    //TODO: fullscreen API: migrate to buildContent
    /*
    // Matches?
    if (instance.screensToShow != null) {
        var matches = false;
        for (var i = 0; i < instance.screensToShow.length; i++) {
            if (json.screenNumber == instance.screensToShow[i]) {
                matches = true;
                Machinata.Reporting.debug("    ***matches " + instance.screensToShow[i]);
                break;
            }
        }
        if (matches == false) {
            json.exclude = true;
            Machinata.Reporting.debug("    no match, excluded");
        } else {
            json.exclude = false;
            Machinata.Reporting.debug("    ***match!");
        }
    }
    */

    // Children
    Machinata.Util.each(json.children, function (index, childJSON) {
        Machinata.Reporting._buildIndexRecursively(instance, config, childJSON, json);
        // Inherit child exclude to make sure titles trickle up on screen matching
        //TODO: fullscreen API: migrate to buildContent
        //if (instance.screensToShow != null && childJSON.exclude == false) {
        //    json.exclude = false;
        //}
    });
};

/// <summary>
/// Builds a report for the configuration
/// </summary>
Machinata.Reporting.buildReport = function (instance, config, onSuccess) {

    // Update loading state
    var changedLoadingState = false;
    if (instance._isLoading == false) {
        Machinata.Reporting.setLoading(instance, true, true); // locked loading...
        changedLoadingState = true;
    }

    //var reportData = Machinata.Util.cloneJSON(instance.rootNode);
    var reportData = instance.rootNode;

    // Recursive routine to update the screen inclusion
    // Note: this is not as straight forward as one might think, as the screen
    // flag will trickle back up to the root node to allow a screen to include a section title
    // (ie setting screen=true on a subnode will automatically select all the parent nodes to ensure the subnode is included)
    function _updateScreenInclusion(instance, config, json, parent) {
        if (instance.screensToShow != null) {
            // Matches?
            var matches = false;
            for (var i = 0; i < instance.screensToShow.length; i++) {
                if (json.screenNumber == instance.screensToShow[i]) {
                    matches = true;
                    Machinata.Reporting.debug("    ***matches " + instance.screensToShow[i]);
                    break;
                }
            }
            if (matches == false) {
                json.exclude = true;
                Machinata.Reporting.debug("    no match, excluded");
            } else {
                json.exclude = false;
                Machinata.Reporting.debug("    ***match!");
            }
        } else {
            json.exclude = false;
        }
        // Recursively update each child, based on the same logic
        Machinata.Util.each(json.children, function (index, childJSON) {
            // Inherit child exclude to make sure titles trickle up on screen matching
            _updateScreenInclusion(instance, config, childJSON, json);
            // If the child was marked as include, mark self as include as well
            if (childJSON.exclude == false) {
                json.exclude = false;
            }
        });
    };
    _updateScreenInclusion(instance, config, reportData, null);
    
    // Remove all existing within content selector
    instance.elems.content.children().filter(".machinata-reporting-node").remove();

    // Build content recursively
    if (config.buildContents == true) {
        Machinata.Debugging.startTimer("report-buildcontents");
        Machinata.Reporting.buildContents(instance, config, reportData, null);
        Machinata.Debugging.stopTimer("report-buildcontents");
    }

    // Bind tooltips via handler, if any
    if (Machinata.Reporting.Handler.bindTooltipsUI != null) {
        Machinata.Reporting.Handler.bindTooltipsUI(instance, $(".machinata-reporting-styling")); 
    }

    // Bind new responsive elements
    //Machinata.Debugging.startTimer("report-responsive");
    //Machinata.Responsive.detectNewResponsiveElements();
    //Machinata.Responsive.updateResponsiveElements();
    //Machinata.Debugging.stopTimer("report-responsive");

    // Trigger initial redraw
    //EXPENSIVE:
    Machinata.Debugging.startTimer("report-firstdraw");
    //instance.elems.content.find(".machinata-reporting-node").trigger("machinata-reporting-redraw-node");
    Machinata.Reporting.Responsive.updateForContainerSize(instance);
    Machinata.Debugging.stopTimer("report-firstdraw");

    // On success callback
    if (onSuccess) {
        // Allow browser to schedule a frame update inbetween
        setTimeout(function () {
            onSuccess(instance, config);
        }, 10);
    }

    // Update loading state
    if (changedLoadingState == true) {
        Machinata.Reporting.setLoading(instance, false, true); // unlock loading...
    }
};

/// <summary>
/// Recursivley builds the actual contents of the report, making sure
/// to respect any configuration regarding screen or chapter selection.
/// </summary>
Machinata.Reporting.buildContents = function (instance, config, json, parent, insertionElem) {

    // Disabled? If so, skip
    if (json.enabled == false) return;
    if (json.exclude == true) return;

    //json = Machinata.Util.cloneJSON(json); // <<< very expensive

    // ID sanity - not all nodes come from a built index, some are built on the fly, so we 
    // assign an id if not already
    if (json.id == null) {
        json.id = Machinata.Reporting.uid();
    }
    instance.idToNodeJSONLookupTable[json.id] = json;

    Machinata.Reporting.debug("Machinata.Reporting.buildContents:");
    Machinata.Reporting.debug("    type: " + json.nodeType);
    Machinata.Reporting.debug("    id: " + json.id);
    Machinata.Reporting.debug("    level: " + json.level);

    // Include via chapters param?
    // Note: we only exclude if we are past the chapters level (since we need the root node for a chapter)
    var include = true;
    if (json.level >= config.chapterLevel && config.chapters != null && config.chapters != "*" && config.chapters.length != 0) {
        include = false;
        for (var i = 0; i < config.chapters.length; i++) {
            if (config.chapters[i] == json.chapterId) {
                include = true;
                Machinata.Reporting.debug("    excluding due to chapter mismatch!");
                break;
            }
        }
    }
    if (include == false) {
        return;
    }

    // Parent elem
    // If we dont specify the insertion point, we get the childrens elem from the parent json dom elem
    var parentElem = null;
    if (insertionElem != null) parentElem = insertionElem;
    if (parent != null && parentElem == null) {
        if (parent._domElem != null && parent._domElem.data("children") != null) {
            parentElem = parent._domElem.data("children");
        } else {
            parentElem = parent._domElem;
        }
    }
    
    if (parentElem == null) parentElem = instance.elems.content;

    // Create node elem
    var sizerElem = null;
    var nodeElem = null;
    if (config.useSizerParentElemForNodes == true) {
        sizerElem = $("<div class='machinata-reporting-node-sizer'></div>").appendTo(parentElem);
        nodeElem = Machinata.Reporting.Node.build(instance, config, json, sizerElem);
    } else {
        nodeElem = Machinata.Reporting.Node.build(instance, config, json, parentElem);
    }
    json._domElem = nodeElem;

    // Children...
    if (json.processChildren != false) { // could be null
        Machinata.Util.each(json.children, function (index, childJSON) {
            if (childJSON.nodeType == "StopProcessNode") return false; // For debugging only, special node type that stops the tree from resolving...
            Machinata.Reporting.buildContents(instance, config, childJSON, json);
        });
    }

    // Post build?
    var impl = Machinata.Reporting.Node[json.nodeType];
    if (impl != null && impl.postBuild != null) {
        impl.postBuild(instance, config, json, nodeElem);
    }

    // Clearer
    //$("<div class='machinata-reporting-section-end'></div>").addClass("level-" + level).appendTo(sectionElem);

    // Focus target?
    //TODO
    //if (instance.focusKey != null && json.key != null && instance.focusKey == json.key) {
    //    Machinata.Reporting.Elems.focusElement = sectionElem;
    //    Machinata.Reporting.debug("***found focus element (section): " + json.key);
    //};

    return nodeElem;
};

/// <summary>
/// Registers multiple layouts at the same time by providing an key value map of layouts
/// where each key is the layout name and the value is the layout JSON.
/// See ```Machinata.Reporting.registerLayout```
/// </summary>
Machinata.Reporting.registerLayouts = function (layoutsJSON) {
    Machinata.Reporting.debug("Machinata.Reporting.registerLayouts:");
    Machinata.Util.each(layoutsJSON, function (key, val) {
        Machinata.Reporting.debug("  "+key);
        Machinata.Reporting.registerLayout(key, val);
    });
};


/// <summary>
/// Registers a new layout with the layouting system. Layouts registered here
/// a primarily used by the LayoutNode, though any component in the reporting system
/// may make use of layouts.
/// 
/// The layoutJSON contains an array of children definitions called ```children```. This
/// property is used to define (extend, overwrite) each slot child's properties according. Typically this
/// achieved by defining the ```style``` and ```chrome``` of each child slot. Each slot definition must also
/// contain the ```slotKey``` property that defines the name of the slot.
/// </summary>
/// <example>
/// ```
/// ### Layout JSON (definition)
/// var layoutJSON = {
///     "style": "CS_CustomStyleToAddToLayout",
///     "children": [
///         {
///             "slotKey": "{left}",
///             "style": "CS_W_Block1x1"
///         },
///         {
///             "slotKey": "{center}",
///             "style": "CS_W_Block2x1"
///         },
///         {
///             "slotKey": "{right}",
///             "style": "CS_W_Block1x1"
///         }
///     ]
/// };
/// Machinata.Reporting.registerLayout("CS_W_Overview_PanelTrailerSmall",layoutJSON);
/// ```
///
/// ### Layout Node (usage)
/// ```
/// {
///     "nodeType": "LayoutNode",
///     "style": "CS_W_Overview_PanelTrailerSmall",
///     "slotChildren": {
///         "{left}": 0,
///         "{center}": 1,
///         "{right}": 2
///     },
///     "children": [
///         {
///             "nodeType": "PlaceholderNode",
///             "mainTitle": {
///                 "resolved": "Left node"
///             }
///         },
///         {
///             "nodeType": "PlaceholderNode",
///             "mainTitle": {
///                 "resolved": "Center node"
///             }
///         },
///         {
///             "nodeType": "PlaceholderNode",
///             "mainTitle": {
///                 "resolved": "Right node"
///             }
///         }
///     ]
/// }
/// ```
/// </example>
Machinata.Reporting.registerLayout = function (name, layoutJSON) {
    // Register
    layoutJSON.name = name;
    Machinata.Reporting._layouts[name] = layoutJSON;
};

/// <summary>
/// </summary>
Machinata.Reporting.getLayout = function (name) {
    return Machinata.Reporting._layouts[name];
};



/// <summary>
/// Register a new global theme for the report. This should be called before the ```init()``` method.
/// Each theme used by the node property ```theme``` should be registered here so that
/// charting libraries can access the themes.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.registerTheme(
///     'blue', 
///     ['#003868', '#004c97', '#0072ce', '#adc8e9', '#dadada', '#a8a8a7'],
///     ['white', 'white', 'white', 'black', 'black', 'black'],
///     ['dark', 'mid', 'bright', 'light', 'gray3', 'gray4]
/// );
/// ```
/// </example>
Machinata.Reporting.registerTheme = function (name, colorsBackground, colorsForeground, colorNames) {
    if (colorsForeground == null || colorNames == null) {
        console.warn("Machinata.Reporting.registerTheme: foreground colors and color names are required for registering themes. (theme " + name + ")");
    } else {
        // Validate lengths
        var lengthDiff = (colorsForeground.length + colorsBackground.length + colorNames.length) / 3 - colorsBackground.length;
        if (lengthDiff != 0) {
            console.warn("Machinata.Reporting.registerTheme: colors and names must match exactly in number of items (length of arrays). (theme " + name + ")");
        }
    }
    // Create theme object
    var theme = {
        "name": name,
        "colorsBG": colorsBackground,
        "colorsFG": colorsForeground,
        "colorNames": colorNames,
    };
    // Compile some shortcut lookup tables
    theme.colorsBGByShade = {};
    theme.colorsFGByShade = {};
    for (var i = 0; i < colorNames.length; i++) {
        theme.colorsBGByShade[colorNames[i]] = theme.colorsBG[i];
        theme.colorsFGByShade[colorNames[i]] = theme.colorsFG[i];
    }
    // Register with us
    Machinata.Reporting._themesNew[name] = theme;
    // Register with Vega
    vega.scheme(name + "-bg", colorsBackground); // NEW
    vega.scheme(name + "-fg", colorsForeground); // NEW
    vega.scheme(name + "-names", colorNames); // NEW

    // OLD - for backwards compat - TODO: remove
    //Machinata.Reporting._themes[name] = colorsBackground;
    //Machinata.Reporting._themes["text-on-" + name] = colorsForeground;
    //vega.scheme(name, colorsBackground); 
    //vega.scheme("text-on-" + name, colorsForeground); 

};

/// <summary>
/// </summary>
Machinata.Reporting.getTheme = function (name) {
    return Machinata.Reporting._themesNew[name];
};

/// <summary>
/// </summary>
Machinata.Reporting.getThemeBGColorByIndex = function (name, index) {
    var theme = Machinata.Reporting.getTheme(name);
    var c = theme.colorsBG[(index % theme.colorsBG.length)];
    return c;
};

/// <summary>
/// </summary>
Machinata.Reporting.getThemeBGColorByShade = function (config, name, shade) {
    // Init
    if (name == null) name = "default";
    var theme = Machinata.Reporting.getTheme(name);
    if (theme == null) return null; // sanity
    // Virtual shades
    if (shade == "transparent") return "transparent";
    // Get color
    var color = theme.colorsBGByShade[shade];
    if (color == null) console.warn("Machinata.Reporting.getThemeBGColorByShade: could not get shade '" + shade + "' for theme '" + name + "'.");
    return color;
};

/// <summary>
/// </summary>
Machinata.Reporting.getThemeFGColorByShade = function (config, name, shade) {
    // Init
    if (name == null) name = "default";
    var theme = Machinata.Reporting.getTheme(name);
    if (theme == null) return null; // sanity
    // Virtual shades
    if (shade == "transparent") return "transparent";
    // Get color
    var color = theme.colorsFGByShade[shade];
    if (color == null) console.warn("Machinata.Reporting.getThemeFGColorByShade: could not get shade '" + shade + "' for theme '" + name + "'.");
    return color;
};



/// <summary>
/// Get a full icon class through the standardized icon key.
/// Currently the following standard icons exist:
///  - ```alert```
///  - ```info```
///  - ```trend-up```
///  - ```trend-down```
///  - ```fullscreen```
///  - ```print```
///  - ```check```
///  - ```copy```
///  - ```filter```
///  - ```export```
///  - ```download```
///  - ```report```
///  - ```arrow-left```
///  - ```arrow-right```
/// </summary>
Machinata.Reporting.icon = function (instance,key) {
    if (Machinata.Reporting.Handler.getIconByKey == null) return null;
    return Machinata.Reporting.Handler.getIconByKey(instance,key);
};

/// <summary>
/// </summary>
Machinata.Reporting.buildIcon = function (instance, key) {
    if (instance.config.iconsBundle == null) throw "Machinata.Reporting.buildIcon: iconsBundle cannot be null";
    var iconsBundle = instance.config.iconsBundle.replace("{" + "assets-path" + "}", instance.config.assetsPath);
    var iconName = Machinata.Reporting.Handler.getIconByKey(instance,key);
    var iconURL = iconsBundle + "#" + iconName;
    return Machinata.UI.buildSVGIcon(iconName, iconURL).addClass("icon-"+key);
};

/// <summary>
/// </summary>
Machinata.Reporting.buildCheckbox = function (instance, title, enabled, type, name, value) {
    if (type == null) type = "checkbox";
    var checkboxId = Machinata.Reporting.uid();
    if (name == null) name = checkboxId;
    var checkboxUIElem = $('<div class="machinata-reporting-checkbox"><input type="' + type +'" name="' + name+ '"/><label></label></div>');
    if (name != null) checkboxUIElem.find("input").attr("value", value);
    checkboxUIElem.find("label").append(Machinata.Reporting.buildIcon(instance, "check"));
    checkboxUIElem.find("label")
        .attr("for", checkboxId)
        .append(title);
    checkboxUIElem.find("input")
        .attr("id", checkboxId);
    if (enabled != false) checkboxUIElem.find("input").attr("checked", "checked");
    return checkboxUIElem;
};

/// <summary>
/// Creates a toolbar and returns the element.
/// </summary>
Machinata.Reporting.buildToolbar = function (instance, title, subtitle) {
    var toolbarElem = $('<div class="machinata-reporting-toolbar DEPRECATED_ui-toolbar">');
    //if (config.chrome != null) toolbarElem.addClass("option-" + config.chrome);
    if (title != null) {
        var titleLabelElem = $('<label class="title theme-headline-font"></label>').text(title);
        toolbarElem.append(titleLabelElem);
        Machinata.UI.autoShowTextAsTooltipOnTruncated(titleLabelElem);
    }
    if (subtitle != null) {
        var subtitleLabelElem = $('<label class="subtitle"></label>').text(subtitle);
        toolbarElem.append(subtitleLabelElem);
        Machinata.UI.autoShowTextAsTooltipOnTruncated(subtitleLabelElem);
    }
    toolbarElem.append($('<div class="tabs"></div>'));
    toolbarElem.append($('<div class="tools"></div>'));
    return toolbarElem;
};


/// <summary>
/// Creates a toolbar and returns the element.
/// </summary>
Machinata.Reporting.buildMenubar = function (instance, options) {
    if (options == null) options = {};
    var menubarElem = $('<div class="machinata-reporting-menubar"><div class="tabs menu-items"></div></div>');
    if (options.softMenuBar == true) menubarElem.addClass("option-soft");
    else menubarElem.addClass("option-hard");
    return menubarElem;
};


/// <summary>
/// Creates a subtoolbar and returns the element.
/// </summary>
/// <deprecated/>
Machinata.Reporting.buildSubToolbar = function (instance, options) {
    var toolbarElem = $('<div class="machinata-reporting-subtoolbar DEPRECATED_ui-toolbar option-small">');
    //if (config.chrome != null) toolbarElem.addClass("option-" + config.chrome);
    toolbarElem.append($('<div class="tabs"></div>'));
    toolbarElem.append($('<label class="hint"></label>'));
    toolbarElem.append($('<div class="tools"></div>'));
    return toolbarElem;
};

/// <summary>
/// </summary>
Machinata.Reporting.showToolbarToolsAsModalDialog = function (instance, toolbarElem, title) {
    // Build a options dialog with all the tools that are hidden
    var opts = {};
    toolbarElem.find(".tool").not(".all-tools").each(function () {
        opts[$(this).attr("data-tool-id")] = $(this).attr("title");
    });
    Machinata.optionsDialog(title, "", opts)
        .okay(function (id, val) {
            $("[data-tool-id='" + id + "']").trigger("click");
        })
        .show();
};

/// <summary>
/// </summary>
Machinata.Reporting.addMenubarButton = function (instance, elem, title, action, options) {
    
    if (elem == null) return; //TODO: handle better?
    if (options == null) options = {};

    var additionalClasses = null;
    if (!elem.hasClass("machinata-reporting-node")) elem = elem.closest(".machinata-reporting-node");
    
    // Get the nodes menubar
    var menubarElem = elem.data("menubar");
    if (menubarElem == null) {
        // Create a new menubar
        menubarElem = Machinata.Reporting.buildMenubar(instance,options);
        elem.data("menubar", menubarElem); 
    }
    if (menubarElem != null) {
        // Create new button
        var buttonElem = $("<a></a>");
        buttonElem.click(function () { action(buttonElem); });
        buttonElem.text(title);
        if (options.tooltip != null) buttonElem.find("a").attr("title", options.tooltip);
        // Add to menubar
        menubarElem.find(".menu-items").append(buttonElem);
        return buttonElem;
    } else {
        return null;
    }
};


/// <summary>
/// Adds a tool to the node toolbar.
///     action: click handler
///     options: space seperated string of options (classes)
/// </summary>
Machinata.Reporting.addTool = function (instance, elem, title, icon, action, options) {
    //if (instance.config["suppress-tools"] == true) return; // DEPRECATED: seems unused
    if (elem == null) return; //TODO: handle better?

    var additionalClasses = null;
    if (!elem.hasClass("machinata-reporting-node")) elem = elem.closest(".machinata-reporting-node");
    var ownerId = elem.attr("id");
    if (elem.hasClass("option-parent-toolbar")) {
        elem = elem.parent().closest(".machinata-reporting-node");
        additionalClasses = "belongs-to-child-node";
    }
    var toolbarElem = elem.data("toolbar");
    if (toolbarElem != null) {
        var toolElem = Machinata.Reporting.addToolToToolbar(instance, elem, toolbarElem, title, icon, action, options);
        toolElem.attr("data-owner-id", ownerId);
        toolElem.addClass(additionalClasses);
        return toolElem;
    } else {
        return null;
    }
};

/// <summary>
/// </summary>
Machinata.Reporting.addSubTool = function (instance, elem, title, icon, action, options) {
    if (!elem.hasClass("machinata-reporting-node")) elem = elem.closest(".machinata-reporting-node");
    var toolbarElem = elem.data("subtoolbar");
    if (toolbarElem != null) {
        return Machinata.Reporting.addToolToToolbar(instance, elem, toolbarElem, title, icon, action, options);
    } else {
        return null;
    }
};

/// <summary>
/// </summary>
Machinata.Reporting.addSubTab = function (instance, elem, title, icon, action, options) {
    if (!elem.hasClass("machinata-reporting-node")) elem = elem.closest(".machinata-reporting-node");
    var toolbarElem = elem.data("subtoolbar");
    if (toolbarElem == null) {
        // Not yet created, create it
        alert("no toolbar yet");
    }
    if (toolbarElem != null) {
        return Machinata.Reporting.addTabToToolbar(instance, elem, toolbarElem, title, icon, action, options);
    } else {
        return null;
    }
};

/// <summary>
/// </summary>
Machinata.Reporting.addToolToToolbar = function (instance, elem, toolbarElem, title, icon, action, options) {
    function createToolElem(icon, action, options) {
        var toolElem = $("<div class='tool'></div>").appendTo(toolbarElem.find(".tools"));
        toolElem.append(Machinata.Reporting.buildIcon(instance, icon));
        toolElem.click(function () { action(toolElem); });
        toolElem.addClass("tool-" + icon);
        toolElem.attr("data-tool-id", Machinata.Reporting.uid());
        toolbarElem.addClass("option-has-tool");
        if (options != null) toolElem.addClass(options);
        return toolElem;
    }
    // Icon
    var toolElem = createToolElem(icon, action, options);
    if (toolbarElem == elem.data("toolbar")) elem.addClass("option-has-tool");
    if (toolbarElem == elem.data("subtoolbar")) elem.addClass("option-has-subtool");
    // Title interaction
    if (title != null) {
        toolElem.attr("title", title);
    }
    // Bookkeeping
    var numTools = toolbarElem.find(".tools .tool").not(".all-tools").length;
    if (numTools > 1) {
        toolElem.addClass("option-desktop-only");
    }
    if (numTools > 1 && toolbarElem.find(".tools .tool.all-tools").length == 0) {
        var allToolsElem = createToolElem("more", function (toolElem) { }, "all-tools option-mobile-tablet-only");
        allToolsElem.click(function () {
            Machinata.Reporting.showToolbarToolsAsModalDialog(instance, $(this).closest(".machinata-reporting-toolbar"));
        });
    }
    var numToolsDesktop = toolbarElem.find(".tools .tool").not(".option-mobile-tablet-only").length;
    var numToolsMobile = toolbarElem.find(".tools .tool").not(".option-desktop-only").length;
    toolbarElem.attr("data-num-tools", numTools);
    toolbarElem.attr("data-num-tools-desktop", numToolsDesktop);
    toolbarElem.attr("data-num-tools-mobile", numToolsMobile);
    toolbarElem.attr("data-num-tools-tablet", numToolsMobile);
    return toolElem;
};

/// <summary>
/// </summary>
Machinata.Reporting.addTabToToolbar = function (instance, elem, toolbarElem, title, icon, action, options) {
    // Icon
    var tabElem = $("<div class='tab'></div>").text(title).appendTo(toolbarElem.find(".tabs"));
    tabElem.click(function () { action(tabElem); });
    if (options != null) tabElem.addClass(options);
    toolbarElem.addClass("option-has-tab");
    if (toolbarElem == elem.data("toolbar")) elem.addClass("option-has-tab");
    if (toolbarElem == elem.data("subtoolbar")) elem.addClass("option-has-subtab");
    return tabElem;
};



/// <summary>
/// Sets the loading status for the report via the loading boolean parameter.
/// If lockOrUnlock is true, the loading state is locked or unlocked according to the loading parameter. 
/// This can be used to ensure a series of operations are locked as loading even if the sub-operations request their own
/// loading on/off, for example during the init routine...
/// </summary>
Machinata.Reporting.setLoading = function (instance, loading, lockOrUnluck) {
    // Init state
    if (instance == null) return; // sanity
    var uiLoadingUpdateRequired = (instance._isLoading != loading);
    if (lockOrUnluck == true && loading == true) instance._isLoadingLocked = true;
    else if (lockOrUnluck != true && loading == false && instance._isLoadingLocked == true) return;
    else if (lockOrUnluck == true && loading == false) instance._isLoadingLocked = false;
    instance._isLoading = loading;
    // Update UI if needed
    if (uiLoadingUpdateRequired == true) {
        if (loading == true) {
            if (instance.elems != null && instance.elems.content != null) instance.elems.content.addClass("loading");
            if (Machinata.Reporting.Handler.showLoading) Machinata.Reporting.Handler.showLoading(instance, loading);
        } else {
            if (instance.elems != null && instance.elems.content != null) instance.elems.content.removeClass("loading");
            if (Machinata.Reporting.Handler.showLoading) Machinata.Reporting.Handler.showLoading(instance, loading);
        }
    }
};


/// <summary>
/// 
/// </summary>
Machinata.Reporting.cleanup = function (instance) {
    // Tell all nodes to cleanup
    instance.elems.content.find(".machinata-reporting-node").trigger(Machinata.Reporting.Events.CLEANUP_NODE_SIGNAL);

    // Tell all nodes to remove
    instance.elems.content.find(".machinata-reporting-node").trigger(Machinata.Reporting.Events.REMOVE_NODE_SIGNAL);

    // Flush dom
    if (instance.elems.content != null) instance.elems.content.remove();
    if (instance.elems.catalog != null) instance.elems.catalog.remove();

    // Flush instance references
    instance.catalogData = null;
    instance.reportData = null;
    instance.rootNode = null;
    instance.catalogCurrentMandate = null;
    instance.catalogCurrentReport = null;
    instance.idToNodeElementLookupTable = null;
    instance.screensToShow = null;
    instance.chapters = null;
    instance.currentScreen = null;
    instance.totalScreens = null;
    instance.focusKey = null;

}











