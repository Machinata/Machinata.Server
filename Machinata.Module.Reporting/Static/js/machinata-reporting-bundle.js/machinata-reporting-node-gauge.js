


/// <summary>
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.GaugeNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults = {};

/// <summary>
/// ```solid``` chrome by default.
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.chrome = "solid";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.supportsDashboard = true;

/// <summary>
/// We do support a toolbar.
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.supportsToolbar = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_1x1];

/// <summary>
/// Legends are not necessary by default.
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.insertLegend = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.innerRadius = 0.5;

/// <summary>
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.outerRadius = 1.0;

/// <summary>
/// By default we use the ```magenta``` theme.
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.theme = "magenta";

/// <summary>
/// Exports are not necessary.
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.disableExport = true;

/// <summary>
/// The stroke width of the status arc line and it's arrow head, in pixels.
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.statusArcLineSize = 2; // was 4

/// <summary>
/// If true, the first and last segment are joined together.
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.firstAndLastSegmentJoin = true;

/// <summary>
/// If ```true```, includes the slice category in the label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.legendLabelsShowCategory = true;

/// <summary>
/// If ```true```, includes the slice value in the legend label.
/// By default ```false```.
/// </summary>
Machinata.Reporting.Node.GaugeNode.defaults.legendLabelsShowValue = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.GaugeNode.getVegaSpec = function (instance, config, json, nodeElem) {
    // Create facts
    var facts = json.facts;
    if (facts == null || json.facts.length == 0) {
        // Fallback to status
        facts = [];
        facts.push({
            val: json.status.val || json.status.value,
            //label: json.status.title,
            tooltip: json.status.tooltip
        });
    }
    // Totals
    var segmentsTotal = 0;
    for (var i = 0; i < json.segments.length; i++) {
        segmentsTotal += (json.segments[i].val ? json.segments[i].val : 1.0); // segments always have a value of 1
    }
    var factsTotal = 0;
    for (var i = 0; i < facts.length; i++) {
        factsTotal += facts[i].val;
    }
    // Status
    var status = {
        title: null,
        titleNumChars: 0,
        tooltip: null,
    };
    if (json.status != null) {
        if (json.status.title != null) {
            status.titleNumChars = json.status.title.resolved.length;
            status.title = Machinata.Reporting.Tools.lineBreakString(Machinata.Reporting.Text.resolve(instance, json.status.title), { returnArray: true });
        }
        if (json.status.tooltip != null) status.tooltip = Machinata.Reporting.Text.resolve(instance, json.status.tooltip);
    }
    // Calculate facts total
    return {
        "data": [
          {
              "name": "status",
                "values": [status],
          },
          {
              "name": "segments",
              "values": null,
              "transform": [
                {
                    "type": "identifier",
                    "as": "index",
                },
                {
                    "type": "formula",
                    "as": "value",
                    "expr": "datum.val ? datum.val : 1.0"
                },
                {
                    "type": "formula",
                    "as": "colorShadeResolved",
                    "expr": "datum.colorShade ? datum.colorShade : (datum.active == true ? 'dark' : 'gray3')"
                },
                {
                    "type": "formula",
                    "as": "titleResolved",
                    "expr": "datum.title.resolved"
                },
                {
                    "type": "pie",
                    "field": "value",
                    "startAngle": 0,
                    "endAngle": { "signal": "PI * 2" },
                    "sort": false
                }
              ]
            },
            {
                "name": "facts",
                "values": facts,
                "transform": [
                    {
                        "type": "identifier",
                        "as": "index",
                    },
                    {
                        "type": "formula",
                        "as": "colorShadeResolved",
                        "expr": "datum.colorShade ? datum.colorShade : 'bright'"
                    },
                    {
                        "type": "formula",
                        "as": "colorResolved",
                        "expr": "scale('theme-bg',datum.colorShadeResolved)"
                    },
                    {
                        "type": "formula",
                        "as": "categoryResolved",
                        "expr": "datum.category != null ? datum.category.resolved : null"
                    },
                    {
                        "type": "formula",
                        "as": "titleResolved",
                        "expr": "datum.label != null ? datum.label.resolved : null"
                    },
                    {
                        "type": "formula",
                        "as": "tooltipResolved",
                        "expr": "datum.tooltip != null ? datum.tooltip.resolved : null"
                    },
                    {
                        "type": "pie",
                        "field": "val",
                        "startAngle": 0,
                        "endAngle": { "signal": "(PI*2)*("+factsTotal+"/"+segmentsTotal+")" },
                        "sort": false
                    },
                ]
            },
        ],
        "signals": [
            {
                "name": "getLabelText",
                "init": "data('status')[0].title",
            },
            {
                "name": "getLabelTextLength",
                "init": "data('status')[0].titleNumChars",
            },
            {
                "name": "getLabelTooltip",
                "init": "data('status')[0].tooltip",
            },
            {
                // Defines the inset for each fact (only if we have multiple)
                "name": "factArcInset",
                "init": "data('facts').length > 1 ? (1*(PI/180)) : 0" // one degree minus
            },
            {
                "name": "arrowInset",
                "init": "(1*(PI/180))" // one degree minus
            },
            {
                "name": "getLabelFontSize",
                "update": "getLabelTextLength <= 3 ? getLargeLabelFontSize : getDynamicLabelFontSize",
            },
            {
                "name": "getDynamicLabelFontSize",
                "update": "max((gaugeArea/2)*0.1, config_chartTextSize)",
            },
            {
                "name": "getLargeLabelFontSize",
                "init": "config_subtitleSize",
            },
            {
                "name": "_firstAndLastSegmentJoin",
                "init": json.firstAndLastSegmentJoin,
            },
            {
                "name": "_allSegmentsJoined",
                "init": json.allSegmentsJoined,
            },
            {
                "name": "gaugeArea",
                "update": "min(width,height)",
            },
        ],
        "scales": [
            {
                "name": "legendColor",
                "type": "ordinal",
                "domain": { "data": "facts", "field": "categoryResolved" },
                "range": { "data": "facts", "field": "colorResolved" }
            },
        ],

        "marks": [
            {
              // SEGMENTS: background
              "type": "arc",
              "from": { "data": "segments" },
              "encode": {
                  "enter": {
                      "fill": { "scale": "theme-bg", "field": "colorShadeResolved" },
                      "opacity": { "field": "opacity" },
                      "tooltip": { "field": "titleResolved" },
                      "strokeCap": { "value": "square" }
                  },
                  "update": {
                      "x": { "signal": "width / 2" },
                      "y": { "signal": "height / 2" },
                      "startAngle": { "field": "startAngle" },
                      "endAngle": { "field": "endAngle" },
                      "innerRadius": { "signal": "(gaugeArea / 2)*" + json.innerRadius },
                      "outerRadius": { "signal": "(gaugeArea / 2)*" + json.outerRadius },
                  }
              }
          },
            {
              // SEGMENTS: seperation lines (chart bg color)
              "type": "arc",
              "from": { "data": "segments" },
              "encode": {
                  "enter": {
                      "stroke": { "signal": "(_allSegmentsJoined || (datum.index == 1 && _firstAndLastSegmentJoin)) ? 'transparent' : chartBackgroundColor" },
                      "strokeWidth": { "value": json.statusArcLineSize },
                      "tooltip": { "field": "resolved" },
                      "strokeCap": { "value": "square" }
                  },
                  "update": {
                      "x": { "signal": "width / 2" },
                      "y": { "signal": "height / 2" },
                      "startAngle": { "field": "startAngle" },
                      "endAngle": { "field": "startAngle" },
                      "innerRadius": { "signal": "(gaugeArea / 2)*" + json.innerRadius + " - " + (json.statusArcLineSize / 2) },
                      "outerRadius": { "signal": "(gaugeArea / 2)*" + json.outerRadius + " + " + (json.statusArcLineSize / 2) },
                  }
              }
          },
          {
              // FACTS: background arc (thick line)
              "type": "arc",
              "from": { "data": "facts" },
              "encode": {
                  "enter": {
                      "fill": { "scale": "theme-bg", "field": "colorShadeResolved" },
                      "tooltip": { "signal": "datum.tooltipResolved" }
                  },
                  "update": {
                      "x": { "signal": "width / 2" },
                      "y": { "signal": "height / 2" },
                      "startAngle": { "signal": "datum.startAngle" },
                      "endAngle": { "signal": "datum.endAngle-factArcInset" },
                      "innerRadius": { "signal": "(gaugeArea / 2)*" + (json.innerRadius + 0.1) },
                      "outerRadius": { "signal": "(gaugeArea / 2)*" + (json.outerRadius - 0.1) }
                  }
              }
          },
          {
              // FACTS: line arc (thin line)
              "type": "arc",
              "from": { "data": "facts" },
              "encode": {
                  "enter": {
                      "fill": { "scale": "theme-gray-bg", "value": 'gray2' },
                      "tooltip": { "signal": "datum.tooltipResolved" }
                  },
                  "update": {
                      "x": { "signal": "width / 2" },
                      "y": { "signal": "height / 2" },
                      "startAngle": { "signal": "datum.startAngle" },
                      "endAngle": { "signal": "datum.endAngle-factArcInset-arrowInset" },
                      "innerRadius": { "signal": "(gaugeArea / 2)*" + ((json.innerRadius + json.outerRadius) / 2) + " -" + (json.statusArcLineSize / 2) },
                      "outerRadius": { "signal": "(gaugeArea / 2)*" + ((json.innerRadius + json.outerRadius) / 2) + " +" + (json.statusArcLineSize / 2) }
                  }
              }
          },
          {
              // FACTS: arrow head (thin line)
              "type": "symbol",
              "from": { "data": "facts" },
              "encode": {
                  "enter": {
                      "stroke": { "scale": "theme-gray-bg", "value": 'gray2' },
                      "strokeWidth": { "value": (json.statusArcLineSize) },
                      "shape": { "value": "M-1,1L0,0L-1,-1" },
                      "tooltip": { "signal": "datum.tooltipResolved" }
                  },
                  "update": {
                      "size": { "signal": "(gaugeArea / 2)*" + ((json.innerRadius + json.outerRadius) / 2) + " *4.0" }, // at normal res should be about 1000
                      "angle": { "signal": "(datum.endAngle-factArcInset-arrowInset) * (180/PI)" }, // in degrees
                      "x": { "signal": "width/2 + sin((datum.endAngle-factArcInset-arrowInset))*  (gaugeArea / 2)*" + ((json.innerRadius + json.outerRadius) / 2) + "" },
                      "y": { "signal": "height/2 - cos((datum.endAngle-factArcInset-arrowInset))*  (gaugeArea / 2)*" + ((json.innerRadius + json.outerRadius) / 2) + "" },
                  }
              }
          },
          {
              // MAIN STATUS: text
              "type": "text",
              "style": "text",
              "encode": {
                  "enter": {
                      "fontWeight": { "value": "bold" },
                      "text": { "signal": "getLabelText" },
                      "tooltip": { "signal": "getLabelTooltip" },
                      "align": { "value": "center" },
                      "baseline": { "value": "middle" },
                  },
                  "update": {
                      "fontSize": { "signal": "getLabelFontSize" }, // Dynamically size the font, but make sure we meet the minimum font size requirement
                      "x": { "signal": "width/2" },
                      "y": { "signal": "height/2" },
                      "dy": { "signal": "-((getLabelText.length-1)*getLabelFontSize)/2" }, // v-align multi line label
                  }
              }
          }
        ]
    };
};
Machinata.Reporting.Node.GaugeNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    // Apply to spec
    spec["data"][1].values = json.segments;
};

Machinata.Reporting.Node.GaugeNode.init = function (instance, config, json, nodeElem) {
    // Legend
    if (json.insertLegend == true) {
        Machinata.Reporting.Node.VegaNode.Util.createLegendForCategoryFacts(instance, config, json, json.facts);
    }

    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.GaugeNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.GaugeNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};







