/// <summary>
/// A powerful radar chart which supports both spider radars as well as pie radars.
///
/// ## Series
/// If needed each serie can be highly customized with the following additional series properties:
///  - ```colorShade```: string, if set defines the color shade to use for the series elements
///  - ```fillOpacity```: number, if set defines the fill opacity of the series shape
///  - ```zindex```: integer, if set defines the z-index of this series elements in the chart (see also ```gridZIndex``` and ```gridLabelsZIndex```)
///  - ```showFactValuesInCategoryLabels```: boolean, if true shows this series fact values in the category labels
///  - ```showFactValuesOnShape```: boolean, if true shows this series fact values on the outer edge 
///
/// ## Weighted Mode
/// If ```radarPie``` is true, it is possible to weight each category using the series facts ```weight``` property. This adjusts each pie slice in the radar according to the weight.
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.RadarNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults = {};

/// <summary>
/// By default this chart is infographic-style ```light```.
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults.chrome = "light";
/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_4x2];

/// <summary>
/// Yes, we support toolbar.
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults.supportsToolbar = true;

/// <summary>
/// Legends for this chart.
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults.insertLegend = true;

/// <summary>
/// Sets the number of columns to use for legends. 
/// By default we use```2```.
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults.legendColumns = 2;

/// <summary>
/// If ```true```, includes the serie title in the legend label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults.legendLabelsShowSerie = true;

/// <summary>
/// Defines the outer radius in decimal percent.
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults.outerRadius = 1.0;

/// <summary>
/// Defines the line curve to use. Can be either ```linear``` or ```cardinal```.
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults.radarLineCurve = "linear";


/// <summary>
/// Defines the grid z-index. This is useful if a serie is to be placed before or behind the grid.
/// Can be used in combination with a serie ```zindex``` property.
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults.gridZIndex = 1;

/// <summary>
/// Defines the grid labels z-index. 
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults.gridLabelsZIndex = 9;

/// <summary>
/// If ```true```, the radar chart is displayed as a pie radar chart, where each category is displayed as a slice.
/// If ```false```, the radar chart is displayed as a spider radar chart, where each category is displayed as a edge. Category weights are not supported in spider radar charts.
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults.radarPie = false;

/// <summary>
/// Defines the relative size of the value-labels.
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults.radarValueLabelSize = 1.0;

/// <summary>
/// If ```true```, the radar chart is displayed as a pie radar chart, where each category is displayed as a slice.
/// If ```false```, the radar chart is displayed as a spider radar chart, where each category is displayed as a edge. Category weights are not supported in spider radar charts.
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults.gridColorShade = null;

/// <summary>
/// Defines the maximum number of characters per line for a category label.
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults.categoryLabelLimitChars = 18;

/// <summary>
/// Defines the maximum number of lines for a category label.
/// </summary>
Machinata.Reporting.Node.RadarNode.defaults.categoryLabelMaxLines = 3;

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.RadarNode.getVegaSpec = function (instance, config, json, nodeElem) {

    // Init
    var RADAR_NODE_DEBUG_ENABLED = false;

    // Figure out the custom domain, if any (this is a yaxis)
    var radarAxis = null;
    Machinata.Util.each(json.series, function (index, jsonSerie) {
        if (jsonSerie.yAxis != null && radarAxis == null) radarAxis = jsonSerie.yAxis;
    });
    if (radarAxis == null) radarAxis = {};
    if (radarAxis.minValue == null) radarAxis.minValue = 0;
    if (radarAxis.maxValue == null) radarAxis.maxValue = null; // automatic
    if (radarAxis.tickCount == null) radarAxis.tickCount = Math.round(config.axisTargetTickCount/2);
    if (radarAxis.orient == null) radarAxis.orient = "left"; 
    if (radarAxis.showDomain == null) radarAxis.showDomain = true;

    // Series settings
    var serieHasFactValuesOnShape = false;
    for (var s = 0; s < json.series.length; s++) {
        var serie = json.series[s];
        if (serie.showFactValuesOnShape == true) serieHasFactValuesOnShape = true;
    }

    // Figure out grid color
    var gridColor = config.gridColor;
    if (json.gridColorShade != null) gridColor = Machinata.Reporting.getThemeBGColorByShade(config, json.theme, json.gridColorShade);

    var spec = {
        "data": [
            {
                "name": "series",
                "values": null
            },
            {
                "name": "categories",
                "values": null
            },
            {
                "name": "empty",
                "description": "Used to dynamically remove a group (since vega still can't do this via a property)",
                "values": []
            },
          {
              "name": "facts",
              "source": "series",
              "transform": [
                {
                    "type": "flatten",
                    "fields": ["facts"],
                    "as": ["fact"]
                }
              ]
          },
          {
              "name": "factsResolved",
              "source": "facts",
              "transform": [

                {
                    "type": "formula",
                    "initonly": true,
                    "as": "category",
                    "expr": "datum.fact.category"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "val",
                      "expr": "datum.fact.val"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "categoryResolved",
                      "expr": "datum.category.resolved"
                  },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "titleResolved",
                    "expr": "datum.title.resolved"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "tooltipResolved",
                      "expr": "datum.fact.categoryInfo.tooltip"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "colorResolved",
                      "expr": "datum.colorShade != null ? scale('theme-bg',datum.colorShade) : scale('color',datum.id)"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "textColorResolved",
                      "expr": "datum.colorShade != null ? scale('theme-fg',datum.colorShade) : scale('textColor',datum.id)"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "fillOpacityResolved",
                      "expr": "datum.fillOpacity != null ? datum.fillOpacity : 0"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "radarKey",
                      "expr": "datum.categoryResolved"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "radarCategory",
                      "expr": "datum.id" // serie id
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "radarValue",
                      "expr": "datum.val"
                  }
              ]
            },
            {
                "name": "categoriesResolved",
                "source": "categories",
                "transform": [
                    
                ]
            },
            {
                "name": "yAxisTickValues",
                "values": radarAxis.values
            }
        ],
        "signals": [
            { "name": "serieHasFactValuesOnShape", "init": serieHasFactValuesOnShape },
            { "name": "radarValueLabelTextSize", "init": "isLayoutedOnHalfMediumWidth ? config_legendSize : config_chartTextSize" },
            { "name": "radarValueLabelSizeFactor", "init": json.radarValueLabelSize },
            { "name": "valueLabelBGSize", "init": "(serieHasFactValuesOnShape ? 3 : 2) * radarValueLabelTextSize * radarValueLabelSizeFactor" },
            { "name": "categoryLabelOffsetFromRadius", "init": "0.1" },
            { "name": "categoryLabelOffsetFromRadiusPercent", "init": "0.1" },
            { "name": "maxCategoryLabelLength", "init": json._state.maxCategoryLabelLength },
            { "name": "leftRightPadding", "init":  "( (maxCategoryLabelLength * config_chartTextSize * (config_fontSizeToCharacterWidthRatio*0.9)) + (valueLabelBGSize*0.8*0.5) ) * 2" },
            { "name": "topBottomPadding", "update": "(config_chartTextSize + (valueLabelBGSize*0.8) ) * 2" },
            { "name": "outerRadius", "init": json.outerRadius },
            { "name": "lineInterpolation", "init": "'" + json.radarLineCurve+"-closed'" },
            { "name": "radarAngleShift", "init": "-PI/2" }, // was PI/2
        ],
        "scales": [
            {
                "name": "color",
                "type": "ordinal",
                "domain": { "data": "series", "field": "id" },
                "range": { "scheme": json.theme + "-bg" }
            },
            {
                "name": "textColor",
                "type": "ordinal",
                "domain": { "data": "series", "field": "id" },
                "range": { "scheme": json.theme + "-fg" }
            },
            {
              // Provide the proper color for the legend
              "name": "legendColor",
              "type": "ordinal",
              "domain": { "data": "factsResolved", "field": "radarCategory" },
              "range": { "data": "factsResolved", "field": "colorResolved" },
            },
        ],
        "marks": [
            {
                // Master group
                "type": "group",
                "encode": {
                    "update": {
                        // Move to center
                        "x": { "signal": "width/2" },
                        "y": { "signal": "height/2" }
                    }
                },
                "signals": [
                    { "name": "radius", "update": "(min(width-leftRightPadding, height-topBottomPadding) / 2) * outerRadius" }
                ],
                "scales": [
                    {
                        "name": "radial",
                        "type": "linear",
                        "range": { "signal": "[0, radius]" },
                        "zero": true,
                        "nice": false,
                        "domain": { "data": "factsResolved", "field": "radarValue" },
                        "domainMin": radarAxis.minValue,
                        "domainMax": radarAxis.maxValue,
                    },
                ],
                "marks": [
                    // Dynamically inserted below...
                ]
            },
        ]
    };
    // Create all the marks for the master group
    // We need to do this out-of-spec because of the complexity of the z-index
    var masterGroupMarks = spec.marks[0].marks;

    // Y-Axis DEPRECATED
    if (false) {
        masterGroupMarks.push({
            "type": "group",
            "zindex": json.gridZIndex,
            //"zindex": 0,
            "axes": [
                Machinata.Reporting.Node.VegaNode.Util.applySettingsToVegaAxis(instance, config, json, {
                    "scale": "radial",
                    "orient": "left",
                    "format": radarAxis.format ? radarAxis.format : null,
                    "formatType": radarAxis.formatType ? radarAxis.formatType : null,
                    "values": radarAxis.values ? radarAxis.values : null,
                    "tickCount": radarAxis.tickCount ? radarAxis.tickCount : null,
                    //"tickCount": { "value": 6 },
                    "ticks": true,
                    "domain": radarAxis.showDomain,
                    "grid": false
                }),
            ],
        });
    }

    // Radial grid (y)
    if (true) {
        masterGroupMarks.push(
            {
                "type": "rule",
                "zindex": json.gridZIndex,
                "name": "radial-grid-y",
                "from": { "data": "categoriesResolved" },
                "encode": {
                    "enter": {
                        "x": { "value": 0 },
                        "y": { "value": 0 },
                        "stroke": { "value": gridColor },
                        "strokeWidth": { "value": config.gridLineSize },
                    },
                    "update": {
                        "x2": { "signal": "radius * cos(radarAngleShift + datum.angleStart)" },
                        "y2": { "signal": "radius * sin(radarAngleShift + datum.angleStart)" },
                    }
                }
            }
        );
    }

    // Radial grid (x)
    if (true) {
        masterGroupMarks.push({
            "type": "arc",
            "zindex": json.gridZIndex,
            "name": "radial-grid-x",
            "from": { "data": "yAxisTickValues" },
            "encode": {
                "enter": {
                    "stroke": { "value": gridColor },
                    "strokeWidth": { "value": config.gridLineSize },
                    "x": { "value": "0" },
                    "y": { "value": "0" },
                },
                "update": {
                    "innerRadius": { "signal": "scale('radial',datum.data)" },
                    "outerRadius": { "signal": "scale('radial',datum.data)" },
                    "startAngle": { "signal": "0" },
                    "endAngle": { "signal": "2*PI" },
                }
            }
        });
    }

    // Radial grid (x) labels
    if (true) {
        if(false)masterGroupMarks.push({
            "type": "symbol",
            "zindex": json.gridLabelsZIndex,
            "name": "radial-grid-x-labels-bg",
            "from": { "data": "radial-grid-x" },
            "encode": {
                "enter": {
                    "shape": { "value": "circle" },
                    "size": { "signal": "(6 * config_chartTextSize*config_chartTextSize)" },
                    "fill": { "signal": "chartBackgroundColor" },
                },
                "update": {
                    "x": { "value": "0" },
                    "y": { "signal": "-datum.innerRadius" },
                }
            }
        });
        masterGroupMarks.push({
            "type": "text",
            "zindex": json.gridLabelsZIndex,
            "name": "radial-grid-x-labels-text",
            "from": { "data": "radial-grid-x" },
            "encode": {
                "enter": {
                    "text": { "signal": "datum.datum.data == 0 ? '' : format(datum.datum.data,'" + radarAxis.format + "')" },
                    "baseline": { "value": "top" },
                    "align": { "value": "left" },
                    "fontSize": { "signal": "config_legendSize" },
                },
                "update": {
                    "x": { "signal": "0 + (config_legendSize * 0.3)" },
                    "y": { "signal": "-datum.innerRadius + (config_legendSize * 0.4)" },
                }
            }
        });
    }

    // Series
    if (true) {
        for (var i = 0; i < json.series.length; i++) {
            var serie = json.series[i];
            var serieGroupId = "serie-" + serie.id;
            var seriesMarks = [];
            seriesMarks.push({
                "type": "group",
                "signals": [
                    { "name": "serieId", "init": "'" + serie.id + "'" },
                    { "name": "serieColorShade", "init": "'" + serie.colorShade + "'" },
                    { "name": "serieFillOpacity", "init": serie.fillOpacity },
                ],
                "marks": [
                ]
            });
            if (json.radarPie != true) seriesMarks[0].marks.push(
                {
                    // Area fill on line (translucent)
                    "type": "line",
                    "name": "category-fill",
                    "from": { "data": "factsResolved-filtered" },
                    "encode": {
                        "enter": {
                            "interpolate": { "signal": "lineInterpolation" },
                            "fill": { "signal": "serieColorShade != null ? scale('theme-bg',serieColorShade) : scale('color',serieId)" },
                            "fillOpacity": { "signal": "serieFillOpacity != null ? serieFillOpacity : 0" },
                        },
                        "update": {
                            "x2": { "signal": "scale('radial', datum.fact.val) * cos(radarAngleShift + datum.fact.categoryInfo.angleStart)" },
                            "y2": { "signal": "scale('radial', datum.fact.val) * sin(radarAngleShift + datum.fact.categoryInfo.angleStart)" },
                        }
                    }
                },
                {
                    // Line (solid)
                    "type": "line",
                    "name": "category-line",
                    "from": { "data": "factsResolved-filtered" },
                    "encode": {
                        "enter": {
                            "interpolate": { "signal": "lineInterpolation" },
                            "stroke": { "signal": "serieColorShade != null ? scale('theme-bg',serieColorShade) : scale('color',serieId)" },
                            "strokeWidth": { "value": config.graphLineSize },
                            "strokeCap": { "value": "round" },
                        },
                        "update": {
                            "x": { "signal": "scale('radial', datum.fact.val) * cos(radarAngleShift + datum.fact.categoryInfo.angleStart)" },
                            "y": { "signal": "scale('radial', datum.fact.val) * sin(radarAngleShift + datum.fact.categoryInfo.angleStart)" },
                        }
                    }
                }
            );
            if (json.radarPie == true) seriesMarks[0].marks.push(
                {
                    // Arc (curve fills)
                    "type": "arc",
                    "name": "category-arc-fill",
                    "from": { "data": "factsResolved-filtered" },
                    "encode": {
                        "enter": {
                            "fill": { "signal": "serieColorShade != null ? scale('theme-bg',serieColorShade) : scale('color',serieId)" },
                            "fillOpacity": { "signal": "serieFillOpacity != null ? serieFillOpacity : 0" },
                            "tooltip": { "signal": "datum.tooltipResolved" },
                        },
                        "update": {
                            "startAngle": { "signal": "radarAngleShift + datum.fact.categoryInfo.angleStart + PI/2" },
                            "endAngle": { "signal": "radarAngleShift + datum.fact.categoryInfo.angleEnd + PI/2" },
                            "innerRadius": { "value": 0 },
                            "outerRadius": { "signal": "scale('radial', datum.fact.val)" },
                        }
                    }
                },
                {
                    // Arc (curves)
                    "type": "arc",
                    "name": "category-arc-curve",
                    "from": { "data": "factsResolved-filtered" },
                    "encode": {
                        "enter": {
                            "stroke": { "signal": "serieColorShade != null ? scale('theme-bg',serieColorShade) : scale('color',serieId)" },
                            "strokeWidth": { "value": config.graphLineSize },
                            "strokeCap": { "value": "round" },
                            "tooltip": { "signal": "datum.tooltipResolved" },
                        },
                        "update": {
                            "startAngle": { "signal": "radarAngleShift + datum.fact.categoryInfo.angleStart + PI/2" },
                            "endAngle": { "signal": "radarAngleShift + datum.fact.categoryInfo.angleEnd + PI/2" },
                            "innerRadius": { "signal": "scale('radial', datum.fact.val)" },
                            "outerRadius": { "signal": "scale('radial', datum.fact.val)" },
                        }
                    }
                },
                {
                    // Arc (joins) - these are straight lines along the grid that join the current arc with the previous
                    "type": "rule",
                    "name": "category-arc-joins",
                    "from": { "data": "factsResolved-filtered" },
                    "encode": {
                        "enter": {
                            "stroke": { "signal": "serieColorShade != null ? scale('theme-bg',serieColorShade) : scale('color',serieId)" },
                            "strokeWidth": { "value": config.graphLineSize },
                            "strokeCap": { "value": "round" },
                        },
                        "update": {
                            "x": { "signal": "scale('radial', datum.fact.val) * cos(radarAngleShift + datum.fact.categoryInfo.angleStart)" },
                            "y": { "signal": "scale('radial', datum.fact.val) * sin(radarAngleShift + datum.fact.categoryInfo.angleStart)" },
                            "x2": { "signal": "scale('radial', datum.fact.prevVal) * cos(radarAngleShift + datum.fact.categoryInfo.angleStart)" },
                            "y2": { "signal": "scale('radial', datum.fact.prevVal) * sin(radarAngleShift + datum.fact.categoryInfo.angleStart)" },
                        }
                    }
                }
            );
            if (serie.showFactValuesOnShape) seriesMarks.push({
                // Fact Values (bg)
                "type": "symbol",
                "name": "value-bg",
                "from": { "data": "factsResolved-filtered" },
                "encode": {
                    "enter": {
                        "shape": { "value": "circle" },
                        "size": { "signal": "(valueLabelBGSize*valueLabelBGSize)" },
                        "fill": { "signal": "datum.colorResolved" },
                        "tooltip": { "signal": "datum.tooltipResolved" },
                    },
                    "update": {
                        "x": { "signal": "scale('radial', datum.radarValue) * cos(radarAngleShift + datum.fact.categoryInfo.angleLabel)" },
                        "y": { "signal": "scale('radial', datum.radarValue) * sin(radarAngleShift + datum.fact.categoryInfo.angleLabel)" },
                    }
                }
            });
            if (serie.showFactValuesOnShape) seriesMarks.push({
                // Fact Values (text)
                "type": "text",
                "name": "value-text",
                "from": { "data": "factsResolved-filtered" },
                "encode": {
                    "enter": {
                        "fontSize": { "signal": "radarValueLabelTextSize" },
                        "align": { "value": "center" },
                        "baseline": { "value": "middle" },
                        "fill": { "signal": "datum.textColorResolved" },
                        "text": { "signal": "datum.fact.resolved" },
                        "tooltip": { "signal": "datum.tooltipResolved" },
                    },
                    "update": {
                        "x": { "signal": "scale('radial', datum.radarValue) * cos(radarAngleShift + datum.fact.categoryInfo.angleLabel)" },
                        "y": { "signal": "scale('radial', datum.radarValue) * sin(radarAngleShift + datum.fact.categoryInfo.angleLabel)" },
                    }
                }
            });
            masterGroupMarks.push({
                "type": "group",
                "name": serieGroupId,
                "zindex": serie.zindex,
                "data": [
                    {
                        "name": "factsResolved-filtered",
                        "source": "factsResolved",
                        "transform": [
                            {
                                "type": "filter",
                                "expr": "datum.radarCategory == '"+serie.id+"'",
                            }
                        ]
                    }
                ],
                "marks": seriesMarks
            });
        }
    }


    // Category labels
    if (true) {
        masterGroupMarks.push({
            // For debugging purposes
            // Always active (because the labels attach to these symbols), but invisible
            "type": "symbol",
            "name": "category-labels-symbols",
            "from": { "data": "categoriesResolved" },
            "encode": {
                "enter": {
                    "fill": { "value": RADAR_NODE_DEBUG_ENABLED ? "black" : "transparent" },
                    "tooltip": RADAR_NODE_DEBUG_ENABLED ? { "signal": "abs(sin(datum.angleLabel))" } : null,
                    "size": { "value": 20 },
                },
                "update": {
                    "xc": { "signal": "(radius + (valueLabelBGSize*0.8)) * cos(radarAngleShift + datum.angleLabel)" },
                    "yc": { "signal": "(radius + (valueLabelBGSize*0.8)) * sin(radarAngleShift + datum.angleLabel)" },

                }
            }
        });
        masterGroupMarks.push({
            "type": "text",
            "name": "category-labels",
            "zindex": 99,
            "from": { "data": "category-labels-symbols" },
            "encode": {
                "enter": {
                    //"text": { "signal": "datum.datum.titleLineBreaked" },
                    "text": { "signal": "datum.datum.titleLineBreaked" },
                    "tooltip": { "signal": "datum.datum.tooltipResolved" },
                    "align": [
                        {
                            "test": "(datum.datum.angleLabel) == 0",
                            "value": "center"
                        },
                        {
                            "test": "(datum.datum.angleLabel) == PI",
                            "value": "center"
                        },
                        {
                            "test": "abs(radarAngleShift + datum.datum.angleLabel) > PI / 2",
                            "value": "right"
                        },
                        {
                            "value": "left"
                        }
                    ],
                    "baseline": [
                        {
                            "value": "middle"
                        }
                    ],
                    //"dy": { "signal": "-((datum.datum.titleLineBreaked.length-1)*config_chartTextSize)/2 * (cos(datum.datum.angleLabel)*0.5)" } // Because vega can't middle baseline multi-line, we need to do this ourselves. cos(labelAngle) goes from 1 (top) to -1 (bottom), which we use to shift back away from the middle since our multilines grow into the bottom or top of the value labels...
                    "dy": { "signal": "-((datum.datum.titleLineBreaked.length-1)*config_chartTextSize*config_lineHeight)/2  - (cos(datum.datum.angleLabel)*0.3*(datum.datum.titleLineBreaked.length*config_chartTextSize*config_lineHeight))" } // Because vega can't middle baseline multi-line, we need to do this ourselves. cos(labelAngle) goes from 1 (top) to -1 (bottom), which we use to shift back away from the middle since our multilines grow into the bottom or top of the value labels...
                },
                "update": {
                    "x": { "signal": "datum.xc" },
                    "y": { "signal": "datum.yc" },

                }
            }
        });
    }

    return spec;
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.RadarNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    // Insert series
    spec["data"][0].values = json.series;
    spec["data"][1].values = json._state.factCategories;
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.RadarNode.init = function (instance, config, json, nodeElem) {


    // Create fact categories
    var factCategories = [];
    var factCategoriesMap = {};
    var maxCategoryLabelLength = 0;
    for (var s = 0; s < json.series.length; s++) {
        var serie = json.series[s];
        for (var f = 0; f < serie.facts.length; f++) {
            var fact = serie.facts[f];
            var categoryId = Machinata.Reporting.Text.resolve(instance, fact.category);
            var categoryTitle = categoryId;
            // Use fact val as category label?
            if (serie.showFactValuesInCategoryLabels == true) {
                categoryTitle += ": " +fact.resolved;
            }
            // Create category info
            if (factCategoriesMap[categoryId] == null) {
                var titleLineBreaked = null;
                titleLineBreaked = Machinata.Reporting.Tools.lineBreakString(categoryTitle, {
                    maxCharsPerLine: json.categoryLabelLimitChars,
                    maxLines: json.categoryLabelMaxLines,
                    wordSeparator: " ",
                    ellipsis: config.textTruncationEllipsis,
                    appendEllipsisOnLastLine: true,
                    returnArray: true
                });
                factCategoriesMap[categoryId] = {
                    id: categoryId,
                    title: categoryTitle,
                    titleLineBreaked: titleLineBreaked
                };
                factCategories.push(factCategoriesMap[categoryId]);
            }
            // Create tooltip
            if (factCategoriesMap[categoryId].tooltip == null) {
                factCategoriesMap[categoryId].tooltip = {
                    title: factCategoriesMap[categoryId].title
                };
            }
            factCategoriesMap[categoryId].tooltip[Machinata.Reporting.Text.resolve(instance, serie.title)] = fact.resolved;
            // Use fact val as category label?
            //if (serie.showFactValuesInCategoryLabels == true) {
            //    factCategoriesMap[categoryId].titleLineBreaked.push(fact.resolved);
            //}
            // Set weight
            if (factCategoriesMap[categoryId].weight == null) {
                if (fact.categoryWeight != null) {
                    factCategoriesMap[categoryId].weight = fact.categoryWeight;
                    //factCategoriesMap[categoryId].tooltip[Machinata.Reporting.Text.translate(instance, "reporting.nodes.radar.weight")] = fact.categoryWeight;
                } else {
                    factCategoriesMap[categoryId].weight = 1;
                }
            }
            // Register back onto fact
            fact.categoryInfo = factCategoriesMap[categoryId];
        }
    }
    // Get total weight
    var totalFactCategoryWeight = 0;
    for (var i = 0; i < factCategories.length; i++) {
        var category = factCategories[i];
        totalFactCategoryWeight += category.weight;
    }
    // Apply minimum weight
    var minimumFactCategoryWeightInPercent = 0.01;
    if (minimumFactCategoryWeightInPercent > 0) {
        for (var i = 0; i < factCategories.length; i++) {
            var category = factCategories[i];
            var weightInPercent = category.weight / totalFactCategoryWeight;
            if (weightInPercent < minimumFactCategoryWeightInPercent) {
                category.weight = minimumFactCategoryWeightInPercent * totalFactCategoryWeight;
            }
        }
        // Get total weight (after adjusting for minimum)
        totalFactCategoryWeight = 0;
        for (var i = 0; i < factCategories.length; i++) {
            var category = factCategories[i];
            totalFactCategoryWeight += category.weight;
        }
    }
    // Calculate angles
    var totalAngle = Math.PI * 2;
    var lastAngle = 0;
    for (var i = 0; i < factCategories.length; i++) {
        var category = factCategories[i];
        category.weightInPercent = category.weight / totalFactCategoryWeight;
        category.angle = category.weightInPercent * totalAngle;
        category.angleStart = lastAngle;
        category.angleEnd = lastAngle + category.angle;
        if (json.radarPie == true) {
            category.angleLabel = (category.angleStart + category.angleEnd) / 2;
        } else {
            category.angleLabel = category.angleStart;
        }
        lastAngle = category.angleEnd;
    }
    // Get the max category label line length
    for (var i = 0; i < factCategories.length; i++) {
        var category = factCategories[i];
        for (var l = 0; l < category.titleLineBreaked.length; l++) {
            if (category.titleLineBreaked[l].length > maxCategoryLabelLength) maxCategoryLabelLength = category.titleLineBreaked[l].length;
        }
    }
    // Create the series slices or lines
    for (var s = 0; s < json.series.length; s++) {
        var serie = json.series[s];
        for (var f = 0; f < serie.facts.length; f++) {
            var fact = serie.facts[f];
            // Link prev/next fact
            if (f == 0) fact.prevVal = serie.facts[serie.facts.length - 1].val;
            else fact.prevVal = serie.facts[f - 1].val;
        }
    }
    // Register categories
    json._state.factCategories = factCategories;
    json._state.maxCategoryLabelLength = maxCategoryLabelLength;

    // Generate legend data
    Machinata.Reporting.Node.VegaNode.Util.createLegendForSeries(instance, config, json, json.series);

    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.RadarNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
    // Debugging
    //Machinata.Reporting.Node.VegaNode.debugOutputDataSet(nodeElem, "factsResolved");
    //Machinata.Reporting.Node.VegaNode.debugOutputDataSet(nodeElem, "categoriesResolved");
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.RadarNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.RadarNode.exportExcelFormat = function (instance, config, json, nodeElem, format, filename) {
    var workbook = Machinata.Reporting.Export.createExcelWorkbook(instance, config, json);
    workbook = Machinata.Reporting.Export.createExcelDataForCategoryChart(instance, config, json, workbook, format);
    Machinata.Reporting.Export.saveExcelWorkbook(instance, config, json, workbook, filename);
    return true;
};




