/// <summary>
/// EXPERIMENTAL: NOT FOR PRODUCTION USE
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.TileGridNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.TileGridNode.defaults = {
    chrome: "solid",
    supportsToolbar: true,
    addStandardTools: false,
    tileColumns: 6,
    zoomOnHover: false,
    flipOnHover: false,
    zoomAmount: 2.5,
};

/// <summary>
/// The resolved text to display in the node. If not text is provided, then the default
/// text ```Placeholder``` is displayed.
/// </summary>
Machinata.Reporting.Node.TileGridNode.defaults.text = null;

Machinata.Reporting.Node.TileGridNode.init = function (instance, config, json, nodeElem) {
    var containerElem = $("<div class='node-bg'></div>").appendTo(nodeElem);
    var tilesElem = $("<div class='tiles'></div>").appendTo(containerElem);

    tilesElem.addClass("tile-hover-acton-" + json.hoverAction);


    for (var i = 0; i < json.items.length; i++) {

        var item = json.items[i];

        var tileElem = $("<div class='tile effect-flippable effect-zoomable'></div>").appendTo(tilesElem);
        var paddingElem = $("<div class='tile-padding flippable-contents zoomable-contents'></div>").appendTo(tileElem);
        var frontElem = $("<div class='tile-front flippable-front'></div>").appendTo(paddingElem);
        var backElem = $("<div class='tile-back flippable-back'></div>").appendTo(paddingElem);

        if (json.flipOnHover == true && item.back != null) {
            tileElem.addClass("option-flip-on-hover");
        }
        if (json.zoomOnHover == true) {
            var tileZoomAmount = json.zoomAmount;
            if (json.flipOnHover == true) tileZoomAmount = 1.5;
            console.log(json.flipOnHover,json.zoomAmount,tileZoomAmount);
            tileElem.addClass("option-zoom-on-hover");
            tileElem.addClass("option-zoom-amount-" + tileZoomAmount.toString().replace(".","-"));
        }

        // Build front and back
        function setupFace(faceElem, faceJSON) {
            if (faceJSON.image != null) {
                var imgElem = $("<img/>")
                    .attr("src", faceJSON.image)
                    .appendTo(faceElem);
            }
            if (faceJSON.html) {
                var textElem = $("<div class='text'></div>")
                    .html(Machinata.Reporting.Text.resolve(instance,faceJSON.html))
                    .appendTo(faceElem);
            }
            if (faceJSON.colorShade != null) {
                faceElem.css("background-color", Machinata.Reporting.getThemeBGColorByShade(config, json.theme || "default", faceJSON.colorShade));
                if (faceJSON.html) {
                    faceElem.find(".text").css("color", Machinata.Reporting.getThemeFGColorByShade(config, json.theme || "default", faceJSON.colorShade));
                }
            }
        }
        if (item.front != null) setupFace(frontElem, item.front);
        if (item.back != null) setupFace(backElem, item.back);
        

        if (item.tooltip != null) tileElem.attr("title", Machinata.Reporting.Text.resolve(instance, item.tooltip));

    }
    tilesElem.append("<div class='clear'/>");

};
Machinata.Reporting.Node.TileGridNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here
};
Machinata.Reporting.Node.TileGridNode.resize = function (instance, config, json, nodeElem) {
    //nodeElem.height(400);

    var ww = nodeElem.find(".tiles").width();
    var tileW = ww / json.tileColumns;
    var tileH = tileW; // titleW+12+12+1;

    if (tileW < 80) nodeElem.addClass("is-compressed");
    else nodeElem.removeClass("is-compressed");

    nodeElem.find(".tile").width(tileW).height(tileH);

    //BUG: in this special auto mode we use the tileH - this needs to be resolved somehow properly before going live...
    if (json.responsiveSize != null && json.responsiveSize.default != null && json.responsiveSize.default.height == "auto") nodeElem.height(tileH + 4 + 4);
};








