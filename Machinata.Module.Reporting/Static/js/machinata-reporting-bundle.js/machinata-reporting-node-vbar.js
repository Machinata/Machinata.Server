/// <summary>
/// ## Axis's
/// Scaling Engine: ```Machinata.Reporting.Node.VegaNode.Util.autoSnapSeriesAxis```
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.VBarNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults = {};

/// <summary>
/// By default charts are ```solid```.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.chrome = "solid";
/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_4x2];

/// <summary>
/// Yes, we support toolbar.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.supportsToolbar = true;

/// <summary>
/// Legends for this chart.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.insertLegend = true;

/// <summary>
/// Sets the number of columns to use for legends. 
/// By default we use```2```.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.legendColumns = 2;

/// <summary>
/// If ```true```, includes the slice value in the legend label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.legendLabelsShowCategory = true;

/// <summary>
/// If ```true```, includes the slice value in the legend label.
/// Note: this should only be used if the chart has a single category across all series.
/// By default ```false```.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.legendLabelsShowValue = false;

/// <summary>
/// Defines the amount of padding (or spacing) for bar-groups (in percent relative to bandwidth).
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.barBandPadding = 0.2;

/// <summary>
/// Defines the maximum width of a bar in pixels. If there is not enough space, then the bar may be
/// smaller.
/// By default ```Machinata.Reporting.Config.verticalBarMaxSize```
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.barMaxSize = Machinata.Reporting.Config.verticalBarMaxSize;

/// <summary>
/// When ```true```, the Y-Axis domain is automatically adjusted through different mechanisms
/// to make the axis' as pretty and nice as possible.
/// 
/// Note: If a series contains a yAxis definitions with a minValue
/// and maxValue, then the autoSnap is not performed on this series.
///
/// By default ```true```.
///  
/// See also:
///  - Machinata.Reporting.Node.HBarNode.defaults.autoSnapYAxisSnapToZero
///  - Machinata.Reporting.Node.HBarNode.defaults.autoSnapYAxisNiceDomain
///  - Machinata.Reporting.Node.HBarNode.defaults.autoSnapYAxisMargin
///  - Machinata.Reporting.Node.HBarNode.defaults.autoSnapYAxisBalance
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.autoSnapYAxis = true;

/// <summary>
/// When ```autoSnapYAxis``` is ```true```, snaps the axis' domain min value to zero. This only happens
/// if all values are either ```> 0``` or all values are ```< 0```.
/// By default ```true``` 
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.autoSnapYAxisSnapToZero = true;

/// <summary>
/// When ```autoSnapYAxis``` is ```true```, rounds the axis' domain to nice numbers.
/// Uses D3's domain scale ```nice()```.
/// By default ```true```
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.autoSnapYAxisNiceDomain = true;

/// <summary>
/// When ```autoSnapYAxis``` is ```true```, defines the margin to add to the axis domain
/// in decimal percent.
/// By default ```0.05```, or 5%.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.autoSnapYAxisMargin = 0.05;

/// <summary>
/// When ```autoSnapYAxis``` is ```true```, this feature will automaticall balance the domain on negative and positive. 
/// Only applies to domains that span from negative to positive.
/// By default ```true``` 
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.autoSnapYAxisBalance = true;

/// <summary>
/// When enabled, the bars will automatically stack within their categories. 
/// All fact values must be positive, 
/// or the chart will not render.
/// If ```autoSnapYAxis``` is enabled, the y-axis domain will automatically be adjusted to account
/// for the new limits. If it is disabled, you must ensure that the y-axis domain accounts for largest
/// category total.
/// By default ```false``` 
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.stackedSeries = false;

/// <summary>
/// If ```stackedSeries``` is ```true```, then the multiple defines the total width of each stacked bar
/// relative to what a single bar width would take. This ensures a consistent design while allowing for a more
/// practical bar width in stacked mode.
/// By default ```4``` 
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.stackedSeriesBarWidthMultiple = 4;

/// <summary>
/// If ```true```, displays labels ontop of the series bars for non-stacked mode, 
/// or inside the series bars(for those with enough width / area) for stacked - mode.
/// If the series is stacked, then the label is limited to the bar width and otherwise truncated automatically.
/// The label value is either automatic using the options ```barLabelsShowValue``` and ```barLabelsShowTitle```, or
/// if the fact has a resolvable text property ```label``` that will be used (for custom labels).
/// Note: Depending on the label text setup and chart data, it is up to the report designer to ensure that the labels make sense 
/// sizewise for the chart. Furthermore, if labels are placed ontop of the bars (default), then it is worthwhile to consider the grid behind the
/// bars and how they collide with the labels. In this case it might make sense to disable the grid, or ensure a yAxis tick count
/// that allows for the labels to have enough space between the grid.
/// Note: By default, the barMaxSize will be automatically increased (see ```barLabelsAutomaticallyInceaseBarMaxSize```)
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.barLabels = false;

/// <summary>
/// If ```true```, includes the series fact value in the label when ```barLabels``` is enabled.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.barLabelsShowValue = true;

/// <summary>
/// If ```true```, includes the series title in the label when ```barLabels``` is enabled.
/// By default ```false```.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.barLabelsShowTitle = false;

/// <summary>
/// If ```barLabels``` is enabled, defines the font size in pixels for the labels.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.barLabelsSize = Machinata.Reporting.Config.chartTextSize;

/// <summary>
/// Makes sense for stacked series - this way the legend optically matches the stack...
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.barLabelsLegendAutomaticallyReversedForVerticalLegends = true;

/// <summary>
/// If ```barLabels``` is enabled, setting this to ```true``` will place the labels inside of the bars.
/// Note: It is up to the report designer to ensure that labels are selected that match the dimensions of the bar widths. 
/// This especially applies to multi-series bars, where the bar width is shared for all series and thus greatly reduced.
/// Note: This setting does not apply to stackedSeries mode, as these labels are always placed inside the bar.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.barLabelsPlaceInsideOfBars = false;

/// <summary>
/// If ```barLabels``` and ```barLabelsPlaceInsideOfBars``` is  is enabled, setting this to ```true``` will
/// automatically increase the barMaxSize to double the value if the ```barMaxSize``` setting uses the default value
/// ```Machinata.Reporting.Config.verticalBarMaxSize```
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.barLabelsAutomaticallyInceaseBarMaxSize = true;

/// <summary>
/// Deprecated: see ```barLabels```.
/// </summary>
/// <deprecated/>
Machinata.Reporting.Node.VBarNode.defaults.stackedSeriesLabels = null;

/// <summary>
/// Deprecated: see ```barLabelsShowValue```.
/// </summary>
/// <deprecated/>
Machinata.Reporting.Node.VBarNode.defaults.stackedSeriesLabelsShowValue = null;

/// <summary>
/// Deprecated: see ```barLabelsShowTitle```.
/// </summary>
/// <deprecated/>
Machinata.Reporting.Node.VBarNode.defaults.stackedSeriesLabelsShowTitle = null;

/// <summary>
/// Deprecated: see ```barLabelsSize```.
/// </summary>
/// <deprecated/>
Machinata.Reporting.Node.VBarNode.defaults.stackedSeriesLabelsSize = null;

/// <summary>
/// Deprecated: see ```barLabelsLegendAutomaticallyReversedForVerticalLegends```.
/// </summary>
/// <deprecated/>
Machinata.Reporting.Node.VBarNode.defaults.stackedSeriesLabelsLegendAutomaticallyReversedForVerticalLegends = null;

/// <summary>
/// By default we use a ```greedy``` overlap strategy.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.axisLabelOverlap = "greedy";

/// <summary>
/// By default ```false```
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.xAxisHidden = false;

/// <summary>
/// By default ```false```
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.yAxisHidden = false;

/// <summary>
/// Defines the maximum characters a x-axis label can have before is line-breaked.
/// Note that x-axis labels are allowed maximum two lines.
/// By default ```10```
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.xAxisLabelLimitChars = 12;

/// <summary>
/// The maximumum number of lines a label can span. If the maximum lines exceed, the 
/// ```xAxisLabelLimitChars``` is automatically postfixed.
/// By default ```2```.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.xAxisLabelLimitLines = 3;

/// <summary>
/// When ```true```, facts can have tooltips which are displayed on the x-axis labels.
/// If a lable was automatically truncated, a tooltip is automatically generated (if not already set).
/// By default ```false```.
/// </summary>
Machinata.Reporting.Node.VBarNode.defaults.xAxisShowTooltipsInLabel = false;

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.VBarNode.getVegaSpec = function (instance, config, json, nodeElem) {
    // Calculate bandwidth
    var stackedSeriesBandwidthSize = json.stackedSeriesBarWidthMultiple;
    var stackedSeriesFakeDomain = [];
    for (var i = 0; i < stackedSeriesBandwidthSize; i++) stackedSeriesFakeDomain.push(i);

    // Create stacked label template
    var barLabelsTemplate = "''";
    if (json.barLabels == true && json.barLabelsShowTitle == true) barLabelsTemplate += " + datum.titleResolved";
    if (json.barLabels == true && json.barLabelsShowTitle == true && json.barLabelsShowValue == true) barLabelsTemplate += " + ': '";
    if (json.barLabels == true && json.barLabelsShowValue == true) barLabelsTemplate += " + datum.fact.resolved";

    // Bar max size
    var barMaxSize = json.barMaxSize;
    if (json.barLabels == true && json.stackedSeries == false) {
        if (barMaxSize == config.verticalBarMaxSize && json.barLabelsAutomaticallyInceaseBarMaxSize != false) {
            barMaxSize = barMaxSize * 2;
        }
    }
    
    return {
        "data": [
          {
              "name": "series",
              "values": null
          },
          {
              "name": "domain",
              "values": null
          },

            {
                "name": "empty",
                "values": []
            },
          {
              "name": "facts",
              "source": "series",
              "transform": [
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "yAxisFormatType",
                      "expr": "(datum.yAxis && datum.yAxis.formatType) ? datum.yAxis.formatType : 'number'" // default
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "yAxisFormat",
                      "expr": "(datum.yAxis && datum.yAxis.format) ? datum.yAxis.format : '.1%'" // default
                  },
                {
                    "type": "flatten",
                    "fields": ["facts"],
                    "as": ["fact"]
                }
              ]
          },
          {
              "name": "factsResolved",
              "source": "facts",
              "transform": [

                {
                    "type": "formula",
                    "initonly": true,
                    "as": "key",
                    "expr": "datum.id" //TODO
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "category",
                    "expr": "datum.fact.category"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "val",
                    "expr": "datum.fact.val"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "categoryResolved",
                    "expr": "datum.category.resolved"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "xAxisCategoryResolved",
                      "expr": "datum.fact.categoryLineBreaked != null ? datum.fact.categoryLineBreaked : datum.category.resolved"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "titleResolved",
                      "expr": "datum.title.resolved"
                  },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "valResolved",
                    "expr": "datum.title != null ? (datum.titleResolved + ': ' + datum.fact.resolved) : (datum.fact.resolved)"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "tooltipResolved",
                      "expr": "datum.fact.tooltip != null ? datum.fact.tooltip.resolved : null"
                  },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "groupResolved",
                    "expr": "datum.group ? datum.group.resolved : datum.category.resolved"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "serieId",
                    "expr": "datum.id"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "colorResolved",
                      "expr": "datum.colorShade != null ? scale('theme-bg',datum.colorShade) : scale('color',datum.serieId)"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "labelColorResolved",
                      "expr": "datum.colorShade != null ? scale('theme-fg',datum.colorShade) : scale('textColor',datum.serieId)"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "barLabelResolved",
                      "expr": json.barLabels == true ? ("datum.fact.showLabel == false ? null : (datum.fact.label != null ? datum.fact.label.resolved : " + barLabelsTemplate + ")") : ("''")
                  },
              ]
            },
        ],
        "signals": [
            
        ],
        "scales": [
            {
                "name": "color",
                "type": "ordinal",
                "domain": { "data": "series", "field": "id" },
                "range": { "scheme": json.theme + "-bg" }
            },
            {
                "name": "textColor",
                "type": "ordinal",
                "domain": { "data": "series", "field": "id" },
                "range": { "scheme": json.theme + "-fg" }
            },
            {
                // This is a workaround/hack for Vega using a fixed scale
                "name": "yAxisFormat",
                "type": "ordinal",
                "domain": ["yAxisFormat"],
                "range": { "data": "factsResolved", "field": "yAxisFormat" }
            },
            {
                // This is a workaround/hack for Vega using a fixed scale
                "name": "yAxisFormatType",
                "type": "ordinal",
                "domain": ["yAxisFormatType"],
                "range": { "data": "factsResolved", "field": "yAxisFormatType" }
            },
          {
              /*"_comment": "This is the main y scale bandwidth for groups of bars WITHOUT padding",*/
              "name": "yScaleNoPadding",
              "type": "band",
              "domain": {
                  "data": "factsResolved",
                  "field": "categoryResolved"
              },
              "range": "height",
              "padding": 0
          },
          {
              // Provide the proper color for the legend
              "name": "legendColor",
              "type": "ordinal",
              "domain": { "data": "factsResolved", "field": "serieId" },
              "range": { "data": "factsResolved", "field": "colorResolved" },
          },
            {
                // Provide the proper tooltip for the axis
                "name": "xScaleTooltip",
                "type": "ordinal",
                "domain": { "data": "factsResolved", "field": "categoryResolved" },
                "range": { "data": "factsResolved", "field": "tooltipResolved" },
            },
            {
                // Provide the proper label for the x-axis
                "name": "xScaleLabel",
                "type": "ordinal",
                "domain": { "data": "factsResolved", "field": "categoryResolved" },
                "range": { "data": "factsResolved", "field": "xAxisCategoryResolved" }
            },
            {
                "name": "xScale",
                "type": "band",
                "domain": {
                    "data": "factsResolved",
                    "field": "categoryResolved"
                },
                "padding": json.barBandPadding,
                "range": "width",
                "round": true, 
                "zero": false, 
                "nice": false 
            },
            {
                "name": "yScale",
                "type": "linear",
                "domain": {
                    "data": "factsResolved",
                    "field": "val"
                },
                //"domainMin": {
                //    "signal": "factsDomainMin",
                //},
                //"domainMax": {
                //    "signal": "factsDomainMax",
                //},
                "domainMin": {
                    "signal": "data('series')[0].yAxis != null ? data('series')[0].yAxis.minValue : null",
                },
                "domainMax": {
                    "signal": "data('series')[0].yAxis != null ? data('series')[0].yAxis.maxValue : null",
                },
                "padding": config.yAxisLabelPadding,
                "range": "height",
                "round": true, // setting round to false will cause a strange decoupled yaxis where the zero round is floating off
                "zero": true,
                "nice": true
            },
            {
                "name": "xScaleBar",
                /*"_comment": "Defines the individual bar scale for each category. If we are stacking the series, then we don't care about how many series we have, but we still use this scale to create a multiple of the size of what a single bar will look like...",*/
                "type": "band",
                "range": "width",
                "domain": json.stackedSeries == true ? stackedSeriesFakeDomain : { "data": "factsResolved", "field": "key" }
            }
            
        ],
        "axes": [
            Machinata.Reporting.Node.VegaNode.Util.applySettingsToVegaAxis(instance, config, json, {
                "orient": "bottom",
                "scale": "xScale",
                "domain": false,
                //"values": { "signal": "data('series')[0].xAxis != null? data('series')[0].xAxis.values : null" },
                //"tickValues": { "signal": "data('series')[0].xAxis != null? data('series')[0].xAxis.tickValues : null" },
                //"labelValues": { "signal": "data('series')[0].xAxis != null? data('series')[0].xAxis.labelValues : null" },
                "ticks": json.xAxisHidden ? false : null, // special handling for hidden x-axis
                "labels": json.xAxisHidden ? false : null, // special handling for hidden x-axis
                "encode": {
                    "labels": {
                        "update": {
                            "text": { "signal": "scale('xScaleLabel',datum.value)" }
                        }
                    }
                }
            }),
            {
                "orient": "left",
                "ticks": json.yAxisHidden ? false : null, // special handling for hidden y-axis
                "labels": json.yAxisHidden ? false : null, // special handling for hidden y-axis
                "scale": "yScale",
                "tickSize": 0,
                "tickRound": true,
                "tickCount": { "signal": "data('series')[0].yAxis != null ? data('series')[0].yAxis.tickCount : null" },
                //"values": { "signal": "data('series')[0].yAxis != null ? data('series')[0].yAxis.values : null" },
                //"tickValues": { "signal": "data('series')[0].yAxis != null ? data('series')[0].yAxis.tickValues : null" },
                //"labelValues": { "signal": "data('series')[0].yAxis != null ? data('series')[0].yAxis.labelValues : null" },
                "format": { "signal": "scale('yAxisFormat','yAxisFormat')" },
                "formatType": { "signal": "scale('yAxisFormatType','yAxisFormatType')" },
                "zindex": 1,
                "grid": true,
                "domain": false
            }
        ],
        "marks": [
          {
              /*"_comment": "BARS GROUPS",*/
              "type": "group",
              "zindex": 2,
              "from": {
                  "facet": {
                      "data": "factsResolved",
                      "name": "facet",
                      "groupby": "categoryResolved"
                  }
              },
                "encode": {
                    "enter": {
                        // DEBUGGING:
                        //"fill": { "signal": "'red'" },
                        //"tooltip": { "signal": json.barLabelsSize }
                    },
                    "update": {
                        "x": { "scale": "xScale", "field": "categoryResolved" },
                    }
              },
              "signals": [
                  {
                      "name": "numCategories",
                      "update": "domain('xScaleBar').length"
                  },
                    {
                        /*"_comment": "This signal limits the bar size by automatically limiting the total space allowed",*/
                        "name": "width",
                        "update": "(bandwidth('xScale') / numCategories) > " + barMaxSize + " ? numCategories*" + barMaxSize + " : bandwidth('xScale')"
                    },
                    { "name": "fullWidth", "update": "bandwidth('xScale')" }
              ],
              "scales": [
                {
                    "name": "seriesPos",
                    "type": "band",
                    "range": "width",
                    "domain": { "data": "factsResolved", "field": "key" }
                }
              ],
              "marks": [
                {
                    /*"_comment": "SINGLE BARS - side by side",*/
                      "name": "bars",
                      "from": { "data": json.stackedSeries != true ? "facet" : "empty" },
                    "type": "rect",
                    "encode": {
                        "enter": {
                            "fill": { "field": "colorResolved" },
                            "tooltip": { "signal": "datum.valResolved" },
                            //"tooltip": { "signal": "scale('seriesPos',datum.key)" }, // DEBUGGING
                            "opacity": { "signal": "datum.fact.strength" },
                        },
                        "update": {
                            "x": { "signal": "scale('seriesPos',datum.key) + (fullWidth-width)/2" },
                            "width": { "scale": "seriesPos", "band": 1 },
                            "y": { "scale": "yScale", "field": "val" },
                            "y2": { "scale": "yScale", "value": 0 }
                        }
                    }
                  },
                  {
                      /*"_comment": "STACKED BARS",*/
                      "name": "bars_stacked",
                      "from": { "data": json.stackedSeries == true ? "facet" : "empty" },
                      "type": "rect",
                      "encode": {
                          "enter": {
                              "fill": { "field": "colorResolved" },
                              "opacity": { "signal": "datum.fact.strength" },
                              "tooltip": { "signal": "datum.valResolved" },
                          },
                          "update": {
                              "x": { "signal": "scale('seriesPos',datum.key)*0 + (fullWidth-width)/2" },
                              "width": { "signal": "width" },
                              "y": { "signal": "scale('yScale',datum.fact.stackedValA)" },
                              "y2": { "signal": "scale('yScale',datum.fact.stackedValB)" },
                              //"tooltip": { "signal": "'DEBUG:'+' A='+scale('yScale',datum.fact.stackedValA)+' B='+scale('yScale',datum.fact.stackedValB)" },
                          }
                      }
                  },
                  json.barLabelsPlaceInsideOfBars == false ? {
                      // Non stacked: bar labels ONTOP of bars
                      "name": "bars_labels",
                      "type": "text",
                      "from": {
                          "data": json.stackedSeries == false && json.barLabels == true ? "facet" : "empty"
                      },
                      "encode": {
                          "enter": {
                              "tooltip": {
                                  "signal": "datum.valResolved"
                              },
                              //"fill": { "field": "labelColorResolved" },
                              "fontSize": { "value": json.barLabelsSize },
                              //"fontWeight": { "value": "bold" },
                              "align": {"signal": "'center'" },
                              "baseline": { "signal": "datum.val > 0 ? 'bottom' : 'top'"},
                              "text": { "signal": "datum.barLabelResolved" },
                          },
                          "update": {
                              //"limit": { "signal": "width" },
                              "x": { "signal": "scale('seriesPos',datum.key) + (fullWidth-width)/2 + bandwidth('seriesPos')/2" },
                              "y": { "signal": "scale('yScale',datum.val) + (datum.val > 0 ? (-config_padding/2) : (config_padding/2))" },
                          }
                      }
                  } : {
                      // Non stacked: bar labels INSIDE of bars
                      "name": "bars_labels",
                      "type": "text",
                      "from": {
                          "data": json.stackedSeries == false && json.barLabels == true ? "facet" : "empty"
                      },
                      "encode": {
                          "enter": {
                              "tooltip": {
                                  "signal": "datum.valResolved"
                              },
                              "fill": { "field": "labelColorResolved" },
                              "fontSize": { "value": json.barLabelsSize },
                              "align": { "signal": "datum.val > 0 ? 'left' : 'right'" },
                              "baseline": { "signal": "'middle'" },
                              "text": { "signal": "datum.barLabelResolved" },
                              "angle": { "value": "-90" },
                          },
                          "update": {
                              //"limit": { "signal": "width" },
                              "x": { "signal": "scale('seriesPos',datum.key) + (fullWidth-width)/2 + bandwidth('seriesPos')/2" },
                              "y": { "signal": "scale('yScale',0) + (datum.val > 0 ? (-config_padding/2) : (+config_padding/2)) " },
                          }
                      }
                  },
                  {
                      "name": "bars_stacked_labels",
                      "type": "text",
                      "from": {
                          "data": json.stackedSeries == true && json.barLabels == true ? "facet" : "empty"
                      },
                      "encode": {
                          "enter": {
                              "tooltip": {
                                  "signal": "datum.valResolved"
                              },
                              "fill": { "field": "labelColorResolved" },
                              "fontSize": { "value": json.barLabelsSize },
                              "align": {
                                  "signal": "'center'"
                              },
                              "baseline": {
                                  "signal": "'middle'"
                              },
                          },
                          "update": {
                              "limit": { "signal": "width" },
                              "text": { "signal": "(scale('yScale',datum.fact.stackedValA)-scale('yScale',datum.fact.stackedValB)) < " + json.barLabelsSize+"*1.2 ? null : datum.barLabelResolved"},
                              "x": { "signal": "scale('seriesPos',datum.key)*0 + (fullWidth-width)/2 + width/2" },
                              "y": { "signal": "(scale('yScale',datum.fact.stackedValA) + scale('yScale',datum.fact.stackedValB))/2" },
                          }
                      }
                  },
                  {
                      // Total labels at the top of the chart
                      //TODO: these don't take into account that the labels may overlap the stacks - the y-domain would need to be padded by this text-y-size amount some
                      "name": "bars_stacked_totals",
                      "type": "text",
                      "encode": {
                          "enter": {
                              "fill": { "value": config.gridColor},
                              "text": { "signal": "data('facet')[0] != null && data('facet')[0].fact != null && data('facet')[0].fact.label != null ? data('facet')[0].fact.label.resolved : null" },
                              //"tooltip": { "signal": "data('facet')[0].fact" },
                              "fontSize": { "value": json.barLabelsSize },
                              "align": {
                                  "signal": "'center'"
                              },
                              "baseline": {
                                  "signal": "'top'"
                              },
                              "dy": {
                                  "signal": json.barLabelsSize+" / 2"
                              },
                          },
                          "update": {
                              "limit": { "signal": "width" },
                              "x": { "signal": "fullWidth/2" },
                              "y": { "signal": "range('yScale')[1]" },
                          }
                      }
                  }
              ]
          },
          {
              /*"_comment": "Y-ZERO RULE",*/
              "name": "yzero",
              "zindex": 3,
              "type": "rule",
              "encode": {
                  "enter": {
                      "stroke": { "value": config.domainLineColor },
                      "strokeWidth": { "value": config.domainLineSize }
                  },
                  "update": {
                      "x": { "value": 0 },
                      "x2": { "signal": "width" },
                      "y": { "signal": "scale('yScale',0)" }
                  }
              }
          }
        ]
    };
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.VBarNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    // Insert series
    spec["data"][0].values = json.series;
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.VBarNode.init = function (instance, config, json, nodeElem) {

    // Backwards compatibility
    if (json.stackedSeriesLabels != null) json.barLabels = json.stackedSeriesLabels;
    if (json.stackedSeriesLabelsShowValue != null) json.barLabelsShowValue = json.stackedSeriesLabelsShowValue;
    if (json.stackedSeriesLabelsShowTitle != null) json.barLabelsShowTitle = json.stackedSeriesLabelsShowTitle;
    if (json.stackedSeriesLabelsSize != null) json.barLabelsSize = json.stackedSeriesLabelsSize;
    if (json.stackedSeriesLabelsLegendAutomaticallyReversedForVerticalLegends != null) json.barLabelsLegendAutomaticallyReversedForVerticalLegends = json.stackedSeriesLabelsLegendAutomaticallyReversedForVerticalLegends;

    // Preprocess
    for (var s = 0; s < json.series.length; s++) {
        var serie = json.series[s];
        // Preprocess axis
        Machinata.Reporting.Node.VegaNode.Util.preprocessAxisProperties(serie.xAxis);
        Machinata.Reporting.Node.VegaNode.Util.preprocessAxisProperties(serie.yAxis);
        console.log(serie.yAxis);
    }

    // Convert serie titles into multilines...
    if (json.xAxisLabelLimitChars != null && json.xAxisLabelLimitChars > 0) {
        Machinata.Util.each(json.series, function (index, jsonSerie) {
            Machinata.Util.each(jsonSerie.facts, function (index, jsonFact) {
                var catResolved = Machinata.Reporting.Text.resolve(instance,jsonFact.category);
                var catLineBreaked = null;
                catLineBreaked = Machinata.Reporting.Tools.lineBreakString(catResolved, {
                    maxCharsPerLine: json.xAxisLabelLimitChars,
                    maxLines: json.xAxisLabelLimitLines,
                    wordSeparator: " ",
                    ellipsis: config.textTruncationEllipsis,
                    appendEllipsisOnLastLine: true,
                    returnArray: true
                });
                jsonFact.categoryLineBreaked = catLineBreaked;
                if (jsonFact.tooltip == null && catLineBreaked.length > 1) {
                    jsonFact.tooltip = {
                        resolved: catResolved
                    }
                }
            });
        });
    }

    // Generate legend data
    if (json.barLabelsLegendAutomaticallyReversedForVerticalLegends == true && json.stackedSeries == true && json.legendAutomaticallyReversedForVerticalLegends == null) {
        json.legendAutomaticallyReversedForVerticalLegends = true;
    }
    Machinata.Reporting.Node.VegaNode.Util.createLegendForSeries(instance, config, json, json.series);

    // Use our own axis snapping tool?
    if (json.autoSnapYAxis == true) {
        Machinata.Reporting.Node.VegaNode.Util.autoSnapSeriesAxis(
            instance, 
            config,
            json,
            json.series,
            {
                axisKey: "yAxis",
                snapToZero: json.autoSnapYAxisSnapToZero,
                niceDomain: json.autoSnapYAxisNiceDomain,
                margin: json.autoSnapYAxisMargin,
                balance: json.autoSnapYAxisBalance,
                stackedSeries: json.stackedSeries,
                tickCount: json.autoSnapYAxisTickCount
            }
        );
    }

    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.VBarNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.VBarNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.VBarNode.exportExcelFormat = function (instance, config, json, nodeElem, format, filename) {
    var workbook = Machinata.Reporting.Export.createExcelWorkbook(instance, config, json);
    workbook = Machinata.Reporting.Export.createExcelDataForCategoryChart(instance, config, json, workbook, format);
    Machinata.Reporting.Export.saveExcelWorkbook(instance, config, json, workbook, filename);
    return true;
};




