


/// <summary>
/// ## Axis's
/// 
/// </summary>
/// <example>
/// See ```example-report-fundingratio.json``` for complete example usages.
/// </example>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.FundingRatioNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults = {};

/// <summary>
/// This is considered a infographic chart, thus it has a light chrome.
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.chrome = "light";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_4x2, Machinata.Reporting.Layouts.BLOCK_2x2];

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.supportsToolbar = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.fluctuationReserveDefaultColorShade = "mid";

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.estimatedRatioDefaultColorShade = "dark";

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.expectedRatiosFirstRatioColorShade = "bright";

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.expectedRatiosSecondRatioColorShade = "bright";

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.expectedRatiosFirstRiskColorShade = "gray6";

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.expectedRatiosFirstRiskBGColorShade = null;

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.expectedRatiosFirstRiskBGOpacity = 0.6;

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.expectedRatiosFirstRiskStrokeDash = "4,4";

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.expectedRatiosSecondRiskColorShade = "transparent";

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.expectedRatiosSecondRiskBGColorShade = "gray3";

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.expectedRatiosSecondRiskBGOpacity = 1.0;

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.expectedRatiosSecondRiskStrokeDash = "4,4";

/// <summary>
/// Yes, insert a legend by default.
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.insertLegend = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.showXAxisGrid = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.showYAxisGrid = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.legendColumns = 3;

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.defaults.legendReversed = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.getVegaSpec = function (instance, config, json, nodeElem) {
    return Machinata.Reporting.Node.ShapeGraphNode.getVegaSpec(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    Machinata.Reporting.Node.ShapeGraphNode.applyVegaData(instance, config, json, nodeElem, spec);
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.init = function (instance, config, json, nodeElem) {

    // See:
    // https://bmpi.atlassian.net/browse/NGR-2684
    // https://bmpi.atlassian.net/browse/NGR-2903

    // Init
    var shapes = [];

    // Risk Parabola Expected Ratio (D1+D2)
    if (true) {

        for (var i = json.expectedRatios.length-1; i >= 0; i--) {
            var ratio = json.expectedRatios[i];
            // General
            var tn = ratio.startTime;
            var DGn = ratio.startValue; // Deckungsgrad zu Beginn tn des Zeitabschnitts n  
            var MILLISECONDS_PER_YEAR = 365 * 24 * 3600 * 1000;
            // B
            // Note: B is not needed as it is pre-calculated in estimatedRatio.facts
            /*
            function r(t) {
                return 0; //TODO
            }
            function vB(t) {
                // v  :=  (1 + r(t))/(1 + r(tn))
                // 1 + erzielte Rendite von tn bis t
                return (1.0 + r(t)) / (1.0 + r(tn));
            }
            function wB(t) {
                // w  :=  (1 + s)u 
                // 1 + Sollrendite von tn bis t
                return Math.pow(1 + ratio.targetPerformance, u(t));
            }
            function B(t) {
                // B(t)  :=  DGn  �  v  /  w
                // St�tzstellen dieser Kurve, diese Kurve kann so genau gezeichnet werden, wie Renditedaten r(t) verf�gbar sind.
                return DGn * vB(t) / wB(t);
            }*/
            // C
            function vC(t) {
                // v  :=  (1 + p)u 
                // 1 + erwartete Rendite von tn bis t
                return Math.pow(1 + ratio.expectedPerformance, u(t)); 
            }
            function wC(t) {
                // w  :=  (1 + s)u 
                // 1 + Sollrendite von tn bis t
                return Math.pow(1 + ratio.targetPerformance, u(t)); 
            }
            function u(t) {
                // u  :=  t � tn 
                // Dauer von tn bis t (in Anteil des Jahres)
                return (t - tn) / MILLISECONDS_PER_YEAR;
            }
            function f(t) {
                // f(t)  :=  3.31144115599 � var � u�
                // �Abstand� zwischen D1 und C resp. C und D2
                return ratio.riskCoefficient * Math.sqrt(u(t));
            }
            function C(t) {
                // C(t)  :=  DGn  �  v  /  w
                return DGn * vC(t) / wC(t); 
            }
            // D1 / D2
            function D1(t) {
                // D1(t)  :=  C(t) / (1 + f(t))
                // Untere Grenze des 95%-Schwankungsbereichs
                return C(t) / (1.0 + f(t)); 
            }
            function D2(t) {
                // D2(t)  :=  C(t) � (1 + f(t))
                // Obere Grenze des 95 % -Schwankungsbereichs
                return C(t) * (1.0 + f(t)); 
            }
            var timeRange = ratio.endTime - ratio.startTime;
            var numPointsD1D2 = 20; // 20
            var timeStepD1D2 = timeRange / (numPointsD1D2 - 1);
            var numPointsC = 10;
            var timeStepC = timeRange / (numPointsC - 1);

            
            // D1/D2 curve combined (D1=bottom, D2=top)
            // Risiko
            if(ratio.showRisk == true) {
                // We combine the two curves into one
                // We start at bottom right and go to starting point and then go to top right
                var colorShade = ratio.riskColorShade || (i == 0 ? json.expectedRatiosFirstRiskColorShade : json.expectedRatiosSecondRiskColorShade);
                var colorShadeBG = ratio.riskBGColorShade || (i == 0 ? json.expectedRatiosFirstRiskBGColorShade : json.expectedRatiosSecondRiskBGColorShade);
                var strokeDash = ratio.riskStrokeDash || (i == 0 ? json.expectedRatiosFirstRiskStrokeDash : json.expectedRatiosSecondRiskStrokeDash);
                var fillOpacity = ratio.riskBGOpacity || (i == 0 ? json.expectedRatiosFirstRiskBGOpacity : json.expectedRatiosSecondRiskBGOpacity);
                var legendColorShade = colorShade;
                if (legendColorShade == "transparent") legendColorShade = colorShadeBG;
                var points = [];
                for (var pi = numPointsD1D2 - 1; pi >= 0; pi--) {
                    var t = ratio.startTime + Math.round(pi * timeStepD1D2);
                    points.push({ x: t, y: D1(t) });
                }
                for (var pi = 1; pi < numPointsD1D2; pi++) { // skip first, since it is common with D1
                    var t = ratio.startTime + Math.round(pi * timeStepD1D2);
                    points.push({ x: t, y: D2(t) });
                }
                var legend = null;
                if (ratio.riskTitle != null) {
                    legend = {
                        titleResolved: Machinata.Reporting.Text.resolve(instance, ratio.riskTitle),
                        colorShade: legendColorShade
                    }
                }
                var shape = Machinata.Reporting.Node.ShapeGraphNode.createShapeDataForPoints(instance,json,points, {
                    type: "line",
                    strokeColorShade: colorShade,
                    fillColorShade: colorShadeBG,
                    fillOpacity: fillOpacity,
                    interpolate: "natural",
                    strokeDash: strokeDash,
                    //strokeWidth: config.lineSize*2,
                    legend: legend,
                });
                shapes.push(shape);
            }

            // C curve 
            // Erwarteter Deckungsgradverlauf(ED):
            if(ratio.showRatio == true) {
                var colorShade = ratio.ratioColorShade || (i == 0 ? json.expectedRatiosFirstRatioColorShade : json.expectedRatiosSecondRatioColorShade);
                var points = [];
                for (var pi = 0; pi < numPointsC; pi++) {
                    var t = ratio.startTime + Math.round(pi * timeStepC);
                    points.push({ x: t, y: C(t) });
                }
                var legend = null;
                if (ratio.ratioTitle != null) {
                    legend = {
                        titleResolved: Machinata.Reporting.Text.resolve(instance, ratio.ratioTitle),
                        colorShade: colorShade
                    }
                }
                var shape = Machinata.Reporting.Node.ShapeGraphNode.createShapeDataForPoints(instance,json,points, {
                    type: "line",
                    strokeColorShade: colorShade,
                    legend: legend,
                });
                shapes.push(shape);
            }
        }


    }


    // Fluctuation Reserves (A)
    // Wertschwankungsreserve
    {
        Machinata.Util.each(json.fluctuationReserves.facts, function (i, fluctuationReserve) {
            var colorShade = fluctuationReserve.colorShade || json.fluctuationReserveDefaultColorShade;
            var points = [];
            points.push({ x: fluctuationReserve.startTime, y: fluctuationReserve.value });
            points.push({ x: fluctuationReserve.endTime, y: fluctuationReserve.value });
            var legend = null;
            if (i == 0 && json.fluctuationReserves.title != null) {
                legend = {
                    titleResolved: Machinata.Reporting.Text.resolve(instance, json.fluctuationReserves.title),
                    colorShade: colorShade
                }
            }
            var data = Machinata.Reporting.Node.ShapeGraphNode.createShapeDataForPoints(instance, json,points, {
                type: "line",
                strokeColorShade: colorShade,
                legend: legend
            });
            shapes.push(data);
        });
    }

    // Estimated Ratio (B)
    // Effektiver Deckungsgradverlauf:
    {
        var colorShade = json.estimatedRatio.colorShade || json.estimatedRatioDefaultColorShade;
        var points = [];
        Machinata.Util.each(json.estimatedRatio.facts, function (i, fact) {
            points.push({ x: fact.x, y: fact.y });
        });
        var legend = null;
        if (json.estimatedRatio.title != null) {
            legend = {
                titleResolved: Machinata.Reporting.Text.resolve(instance, json.estimatedRatio.title),
                colorShade: colorShade
            }
        }
        var shape = Machinata.Reporting.Node.ShapeGraphNode.createShapeDataForPoints(instance, json,points, {
            type: "line",
            strokeColorShade: colorShade,
            legend: legend
        });
        shapes.push(shape);
    }

    Machinata.Reporting.Node.ShapeGraphNode.init(instance, config, json, nodeElem, shapes);

};

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node.ShapeGraphNode.draw(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node.ShapeGraphNode.exportFormat(instance, config, json, nodeElem, format, filename);
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.FundingRatioNode.exportExcelFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node.ShapeGraphNode.exportExcelFormat(instance, config, json, nodeElem, format, filename);
};








