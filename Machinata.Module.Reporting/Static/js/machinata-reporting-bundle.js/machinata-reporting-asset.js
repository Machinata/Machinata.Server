

/// <summary>
/// Reporting asset tools for resolving assets so that they are accessible within the report.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Asset = {};




/// <summary>
/// Get the resolved asset for a JSON object.
///
/// ## Images
/// Image assets can be directly encoded as a base64. This allows for image assets to be embedded in the
/// report JSON. Image assets can also be provided as a URL, either absolute (fully qualified) or relative.
///
/// ## Assets Path
/// The configuration parameter ```Machinata.Reporting.Config.assetsPath``` can be automatically inserted into
/// the assets data for file or url encodings. In this case the variable ```{assets-path}``` can be used, such as 
/// ```{assets-path}/image/GYssOw/Nerves-New-Business-Card.jpg```.
///
/// Note: assets that are encoded and embedded into the report JSON must adhere to the browsers limitations for blobs and data-URIs. 
/// In practice this means that files should not be larger than 2MB.
/// Note: the mimetype is important to allow certain features such as downloading a asset via the browser.
/// </summary>
/// <example>
/// ### Example Embedded Asset:
/// ```
/// {
///     "filename": "QRCodeTest.png",
///     "mimeType": "image/png",
///     "encoding": "base64",
///     "data": "iVBORw0KGgoAAAANSUhEUgAAABUAAAAVAQMAAACT2TfVAAAABlBMVEUAAAD///+l2Z/dAAAAAnRSTlP//8i138cAAAAJcEhZcwAACxIAAAsSAdLdfvwAAABcSURBVAiZY2Bcws5Qq/udwdVUnMF1LRCXijPUpn5nYAxhZ/jP/p9BZ44/g9De8wwHg58zXHvfzsBoGs/wn0+dgdHqPEPtI3sG1x52Btf8dgbX6+UMtZHxDIzu/AAr6RmSv9DdfQAAAABJRU5ErkJggg=="
/// }
/// ```
/// ### Example Downloadable Asset:
/// ```
/// {
///     "filename": "QRCodeTest.png",
///     "mimeType": "image/png",
///     "encoding": "base64",
///     "data": "iVBORw0KGgoAAAANSUhEUgAAABUAAAAVAQMAAACT2TfVAAAABlBMVEUAAAD///+l2Z/dAAAAAnRSTlP//8i138cAAAAJcEhZcwAACxIAAAsSAdLdfvwAAABcSURBVAiZY2Bcws5Qq/udwdVUnMF1LRCXijPUpn5nYAxhZ/jP/p9BZ44/g9De8wwHg58zXHvfzsBoGs/wn0+dgdHqPEPtI3sG1x52Btf8dgbX6+UMtZHxDIzu/AAr6RmSv9DdfQAAAABJRU5ErkJggg=="
/// }
/// ```
/// ### Example Linked Asset:
/// ```
/// {
///     "filename": "QRCodeTest.png",
///     "mimeType": "image/png",
///     "encoding": "url",
///     "data": "https://nerves.ch/content/image/GYssOw/Nerves-New-Business-Card.jpg"
/// }
/// ```
/// ### Example File Asset:
/// ```
/// {
///     "filename": "QRCodeTest.png",
///     "mimeType": "image/png",
///     "encoding": "file",
///     "data": "/C/users/testuser/Nerves-New-Business-Card.jpg"
/// }
/// ```
/// </example>
Machinata.Reporting.Asset.resolve = function (instance, config, json, encodeForURL) {
    if (json == null) return null;

    if (json.encoding == "file") {
        var ret = json.data;
        if (ret != null) ret = ret.replace("{" + "assets-path" + "}", config.assetsPath);
        return ret;
    } else if (json.encoding == "url") {
        var ret = json.data;
        if (ret != null) ret = ret.replace("{" + "assets-path" + "}", config.assetsPath);
        return ret;
    } else if (json.encoding == "base64") {
        var ret = "data:" + json.mimeType + ";base64," ;
        if (encodeForURL == true) {
            ret = ret + encodeURIComponent(json.data);
        } else {
            ret = ret + (json.data);
        }
        return ret;
    } else if (json.encoding != null) {
        throw "Machinata.Reporting.Asset.resolve: the encoding type " + json.encoding + " is not supported!";
    }
    
    // Fallback to resolved version
    return json.resolved;
};



/// <summary>
/// Get the resolved asset data for a resolved asset JSON object 
/// by either downloading or decoding the target.
/// The resolved location can be retrieved through the method ```Machinata.Reporting.Asset.resolve```.
/// Under the hood, this method will use ```Machinata.IO.loadFileContentsAsText``` for files on disk
/// or on a web server.
/// </example>
Machinata.Reporting.Asset.resolveData = function (config, resolvedLocation) {
    return new Promise((resolve, reject) => {
        try {
            // Sanity
            if (resolvedLocation == null) throw "Machinata.Reporting.Asset.resolveData: resolvedLocation is null!";
            if (resolvedLocation == "") throw "Machinata.Reporting.Asset.resolveData: resolvedLocation is '' (empty string)!";

            // Data URI?
            if (resolvedLocation.startsWith("data:")) {
                var dataURI = resolvedLocation;
                var header = dataURI.split(',')[0];
                var data = dataURI.split(',')[1];
                var byteString = Buffer.from(data, "base64").toString(config.defaultFileEncoding); //todo properly support different than just base64...
                resolve(byteString);
            } else {
                Machinata.IO.loadFileContentsAsText(resolvedLocation, {
                    encoding: config.defaultFileEncoding
                }).then(function (data) {
                    resolve(data);
                });
            }
        } catch (err) {
            reject(err);
        }
    });
};