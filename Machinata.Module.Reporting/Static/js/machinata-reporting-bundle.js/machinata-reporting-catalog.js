/// <summary>
/// Catalog namespace. 
/// Utilities for displaying catalogs of reports dynamically.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Catalog = {};


/// <summary>
/// </summary>
Machinata.Reporting.Catalog.processCatalog = function (instance, config, catalogData) {

    Machinata.Debugging.startTimer("catalog-process");

    // Pre-process
    var catalogDateParser = d3.timeParse(config.catalogDateFormat);
    var catalogDateFormatter = d3.timeFormat(config.catalogDateDisplayFormat);
    Machinata.Util.each(catalogData.mandates, function (index, mandateJSON) {
        // Process the mandate
        mandateJSON.titleResolved = Machinata.Reporting.Text.resolve(instance, mandateJSON.title);
        mandateJSON.dateToReportMap = {};
        mandateJSON.publishedDateRaw = mandateJSON.publishedDate;
        mandateJSON.publishedDateResolved = mandateJSON.publishedDateRaw;
        if (mandateJSON.publishedDateRaw != null) {
            mandateJSON.publishedDate = catalogDateParser(mandateJSON.publishedDateRaw);
            mandateJSON.publishedDateResolved = catalogDateFormatter(mandateJSON.publishedDate);
        }
        var mandateURL = null;
        Machinata.Util.each(mandateJSON.reports, function (index, reportJSON) {
            // Process the report
            reportJSON.dateRaw = reportJSON.parameters.reportingDate;
            reportJSON.dateResolved = reportJSON.dateRaw;
            reportJSON.date = null;
            if (reportJSON.dateRaw != null) {
                reportJSON.date = catalogDateParser(reportJSON.dateRaw);
                reportJSON.dateResolved = catalogDateFormatter(reportJSON.date);
            }
            if (reportJSON.date != null) {
                mandateJSON.dateToReportMap[reportJSON.date] = reportJSON;
            }
            if (config.dataSource == null || config.dataSource == "auto") {
                config.dataSource = reportJSON.dataSource;
                if (instance.catalogCurrentMandate == null) instance.catalogCurrentMandate = mandateJSON;
                if (instance.catalogCurrentReport == null) instance.catalogCurrentReport = reportJSON;
            }
            if (config.allowConfigurationParametersViaQueryString == true && Machinata.queryParameter("report") == reportJSON.id) {
                config.dataSource = reportJSON.dataSource;
                instance.catalogCurrentMandate = mandateJSON;
                instance.catalogCurrentReport = reportJSON;
                config.reportTitle = Machinata.Reporting.Text.resolve(instance, mandateJSON.title);
                //config.reportSubtitle = reportJSON.dateResolved; //TODO: @dan @bmpi 
            }
            // Create proper urls
            var reportURL = Machinata.updateQueryString("report", reportJSON.id, config.reportURL);
            reportURL = Machinata.removeQueryParameter("chapter", reportURL);
            reportURL = Machinata.removeQueryParameter("focus", reportURL);
            reportURL = Machinata.removeQueryParameter("ui", reportURL);
            reportJSON.urlResolved = reportURL;
            if (mandateURL == null) {
                mandateURL = Machinata.updateQueryString("report", reportJSON.id, config.reportURL);
                mandateURL = Machinata.removeQueryParameter("chapter", mandateURL);
                mandateURL = Machinata.removeQueryParameter("focus", mandateURL);
            }
        });
        mandateJSON.urlResolved = mandateURL;
        // Create a url to list all mandate reports
        var mandateCatalogURL = Machinata.updateQueryString("mandate", mandateJSON.id, config.catalogURL);
        var mandateCatalogURL = Machinata.updateQueryString("reports", "true", mandateCatalogURL);
        mandateJSON.catalogURLResolved = mandateCatalogURL;
    });

    Machinata.Debugging.stopTimer("catalog-process");
};

/// <summary>
/// </summary>
Machinata.Reporting.Catalog.compileCatalogUI = function (instance, config, catalogData) {

    Machinata.Debugging.startTimer("catalog-compile");

    // Set window title
    // Here we use the config reportWindowTitle, unless we are in webprint, in which case we try to use the filename...
    if (config.catalogWindowTitle != null) {
        var windowTitle = Machinata.Reporting.Tools.createTitleString(instance, config, config.catalogWindowTitle);
        if (windowTitle != null) window.document.title = windowTitle;
    }

    // Attributes
    $(".machinata-reporting-attribute-catalog-title").text(Machinata.Reporting.Text.resolve(instance,catalogData["catalogTitle"]));

    // Add all mandate siblings for catalog navigation
    if (config.automaticallyInsertCatalogNavigation == true && Machinata.Reporting.Handler.addNavigationSibling != null) {
        Machinata.Util.each(catalogData.mandates, function (index, mandateJSON) {
            if (index >= config.navigationMaxSiblings) return;
            var siblingNavItemElem = Machinata.Reporting.Handler.addNavigationSibling(instance, mandateJSON.titleResolved, mandateJSON.urlResolved);
        });
        var siblingNavItemElem = Machinata.Reporting.Handler.addNavigationSibling(instance, Machinata.Reporting.Text.translate(instance, "reporting.view-all"), config.catalogURL);
    }
    // Add all date siblings for report date navigation
    //NOTE: disabled due to UX overcomplication
    if (false) {
        if (instance.catalogCurrentMandate != null && instance.catalogCurrentReport != null) {
            var dateNavItemElem = Machinata.Reporting.Handler.addNavigationItem(instance, instance.catalogCurrentReport.dateResolved, instance.catalogCurrentReport.urlResolved);
            Machinata.Util.each(instance.catalogCurrentMandate.reports, function (index, reportJSON) {
                if (index >= config.navigationMaxSiblings) return;
                if (instance.catalogCurrentMandate.reports.length == 1) return;
                var siblingNavItemElem = Machinata.Reporting.Handler.addNavigationSibling(instance, reportJSON.dateResolved, reportJSON.urlResolved);
            });
            if (instance.catalogCurrentMandate != null) {
                var siblingNavItemElem = Machinata.Reporting.Handler.addNavigationSibling(instance, Machinata.Reporting.Text.translate(instance, "reporting.view-all"), instance.catalogCurrentMandate.catalogURLResolved);
            }
        }
    }

    // Compile a table/listing UI of catalog/report items 
    if (instance.elems.catalog != null && instance.elems.catalog.length > 0) {
        // Init
        var tableElem = instance.elems.catalog.closest("table");
        var showReports = config.allowConfigurationParametersViaQueryString == true && Machinata.queryParameter("reports") == "true";
        var mandateFilter = config.allowConfigurationParametersViaQueryString == true && Machinata.queryParameter("mandate");
        if (mandateFilter == "") mandateFilter = null;
        if (showReports == true) {
            tableElem.addClass("option-show-reports");
        }
        // Get the template
        var templateMandateElem = $(config.catalogItemMandateTemplateSelector);
        var templateReportElem = $(config.catalogItemReportTemplateSelector);
        // Add a row for each mandate
        Machinata.Util.each(catalogData.mandates, function (index, mandateJSON) {
            // Filter out?
            if (mandateFilter != null && mandateJSON.id != mandateFilter) {
                return;
            } else if (mandateFilter != null && mandateJSON.id == mandateFilter) {
                $(".machinata-reporting-attribute-catalogs-title").hide();
                $(".machinata-reporting-attribute-reports-title").show();
                $(".machinata-reporting-attribute-catalog-title").text(mandateJSON.titleResolved);
                Machinata.Reporting.Handler.addNavigationItem(instance, mandateJSON.titleResolved, "");
            }
            // Compile mandate item
            var itemElem = templateMandateElem.clone();
            itemElem.find(".attribute-link").attr("href", mandateJSON.urlResolved);
            if (itemElem.hasClass("attribute-link")) itemElem.attr("href", mandateJSON.urlResolved);
            itemElem.find(".attribute-title").text(mandateJSON.titleResolved);
            var mandateDate = null;
            var mandateDateResolved = null;
            if (mandateJSON.reports != null && mandateJSON.reports.length > 0) {
                mandateDateResolved = mandateJSON.reports[0].dateResolved; // latest report
                mandateDate = mandateJSON.reports[0].date; // latest report
            }
            itemElem.find(".attribute-reporting-date").text(mandateDateResolved);
            if (mandateJSON.investor != null) itemElem.find(".attribute-investor-name").text(mandateJSON.investor.name);
            if (mandateJSON.custodyBank != null) itemElem.find(".attribute-custody-bank-name").text(mandateJSON.custodyBank.name);
            if (mandateJSON.custodyBank != null) itemElem.find(".attribute-custody-bank-account").text(mandateJSON.custodyBank.account);
            if (mandateJSON.portfolioManager != null) itemElem.find(".attribute-pm-name").text(mandateJSON.portfolioManager.firstName + " " + mandateJSON.portfolioManager.lastName);
            if (mandateJSON.portfolioManager != null) itemElem.find(".attribute-pm-email").text(mandateJSON.portfolioManager.email);
            if (mandateJSON.portfolioManager != null) itemElem.find(".attribute-pm-phone").text(mandateJSON.portfolioManager.phone);
            if (mandateJSON.clientPortfolioManager != null) itemElem.find(".attribute-cpm-name").text(mandateJSON.clientPortfolioManager.firstName + " " + mandateJSON.clientPortfolioManager.lastName);
            if (mandateJSON.clientPortfolioManager != null) itemElem.find(".attribute-cpm-email").text(mandateJSON.clientPortfolioManager.email);
            if (mandateJSON.clientPortfolioManager != null) itemElem.find(".attribute-cpm-phone").text(mandateJSON.clientPortfolioManager.phone);
            if (mandateJSON.relationshipManager != null) itemElem.find(".attribute-rm-name").text(mandateJSON.relationshipManager.firstName + " " + mandateJSON.relationshipManager.lastName);
            if (mandateJSON.relationshipManager != null) itemElem.find(".attribute-rm-email").text(mandateJSON.relationshipManager.email);
            if (mandateJSON.relationshipManager != null) itemElem.find(".attribute-rm-phone").text(mandateJSON.relationshipManager.phone);
            if (mandateJSON.totalAssets != null) itemElem.find(".attribute-total-assets-amount").text(mandateJSON.totalAssets.amount.resolved);
            if (mandateJSON.totalAssets != null) itemElem.find(".attribute-total-assets-amount-value").attr("data-text", mandateJSON.totalAssets.amount.value);
            if (mandateJSON.totalAssets != null) itemElem.find(".attribute-total-assets-currency").text(mandateJSON.totalAssets.currency);
            if (mandateJSON.publishedDateResolved != null) itemElem.find(".attribute-published-date").text(mandateJSON.publishedDateResolved);
            if (mandateJSON.id != null) itemElem.find(".attribute-id").text(mandateJSON.id);
            itemElem.find(".table-cell.type-tools").append(Machinata.Reporting.buildIcon(instance, "list").addClass("machinata-reporting-show-reports").attr("title", Machinata.Reporting.Text.translate(instance, "reporting.show-all-reports")));
            itemElem.find(".table-cell.type-tools").append(Machinata.Reporting.buildIcon(instance, "calendar").addClass("machinata-reporting-choose-report-date").attr("title", Machinata.Reporting.Text.translate(instance, "reporting.choose-date")));
            itemElem.find(".table-cell.type-tools").append(Machinata.Reporting.buildIcon(instance, "arrow-right").addClass("machinata-reporting-open-report").attr("title", Machinata.Reporting.Text.translate(instance, "reporting.view-latest-report")));
            itemElem.find(".attribute-type").text(Machinata.Reporting.Text.resolve(instance,mandateJSON.mandateType));
            itemElem.find(".machinata-reporting-choose-report-date").click(function (evt) {
                evt.stopPropagation();
                evt.preventDefault();
                Machinata.Reporting.Handler.changeReportingDate(instance, instance.catalogData, mandateJSON, null, mandateDate, Machinata.Reporting.Text.translate(instance, "reporting.choose-date"));
            });
            itemElem.find(".machinata-reporting-show-reports").click(function (evt) {
                evt.stopPropagation();
                evt.preventDefault();
                Machinata.goToPage(mandateJSON.catalogURLResolved);
            });
            var badgeElem = itemElem.find(".attribute-badge");
            if (mandateJSON.reportBadge != null) {
                if (mandateJSON.reportBadge.color != null) badgeElem.addClass("option-" + mandateJSON.reportBadge.color);
                badgeElem.text(Machinata.Reporting.Text.resolve(instance,mandateJSON.reportBadge.label));
                badgeElem.addClass("has-content")
                if (mandateJSON.reportBadge.tooltip != null) badgeElem.attr("title", Machinata.Reporting.Text.resolve(instance,mandateJSON.reportBadge.tooltip));
            } else {
                badgeElem.remove();
            }
            itemElem.removeClass(config.catalogItemMandateTemplateSelector.replace(".", ""));
            if (showReports) itemElem.addClass("static").attr("data-row-index", 0);
            instance.elems.catalog.append(itemElem);
            // Add all reports?
            if (showReports) {
                instance.elems.catalog.addClass("has-reports");
                var maxReportsToList = config.catalogMaxReportsToShow;
                Machinata.Util.each(mandateJSON.reports, function (index, reportJSON) {
                    // Compile report item
                    var itemElem = templateReportElem.clone();
                    if (maxReportsToList == null || index < maxReportsToList) {
                        // Report specifics
                        itemElem.find(".attribute-link").attr("href", reportJSON.urlResolved);
                        if (itemElem.hasClass("attribute-link")) itemElem.attr("href", reportJSON.urlResolved);
                        itemElem.find(".attribute-reporting-date").text(reportJSON.dateResolved).attr("data-sort", reportJSON.date.getTime());
                        // Mandate specifics
                        itemElem.find(".attribute-title").text(mandateJSON.titleResolved);
                        if (mandateJSON.investor != null) itemElem.find(".attribute-investor-name").text(mandateJSON.investor.name);
                        if (mandateJSON.custodyBank != null) itemElem.find(".attribute-custody-bank-name").text(mandateJSON.custodyBank.name);
                        if (mandateJSON.custodyBank != null) itemElem.find(".attribute-custody-bank-account").text(mandateJSON.custodyBank.account);
                        if (mandateJSON.portfolioManager != null) itemElem.find(".attribute-pm-name").text(mandateJSON.portfolioManager.firstName + " " + mandateJSON.portfolioManager.lastName);
                        if (mandateJSON.portfolioManager != null) itemElem.find(".attribute-pm-email").text(mandateJSON.portfolioManager.email);
                        if (mandateJSON.portfolioManager != null) itemElem.find(".attribute-pm-phone").text(mandateJSON.portfolioManager.phone);
                        if (mandateJSON.clientPortfolioManager != null) itemElem.find(".attribute-cpm-name").text(mandateJSON.clientPortfolioManager.firstName + " " + mandateJSON.clientPortfolioManager.lastName);
                        if (mandateJSON.clientPortfolioManager != null) itemElem.find(".attribute-cpm-email").text(mandateJSON.clientPortfolioManager.email);
                        if (mandateJSON.clientPortfolioManager != null) itemElem.find(".attribute-cpm-phone").text(mandateJSON.clientPortfolioManager.phone);
                        if (mandateJSON.relationshipManager != null) itemElem.find(".attribute-rm-name").text(mandateJSON.relationshipManager.firstName + " " + mandateJSON.relationshipManager.lastName);
                        if (mandateJSON.relationshipManager != null) itemElem.find(".attribute-rm-email").text(mandateJSON.relationshipManager.email);
                        if (mandateJSON.relationshipManager != null) itemElem.find(".attribute-rm-phone").text(mandateJSON.relationshipManager.phone);
                        if (mandateJSON.totalAssets != null) itemElem.find(".attribute-total-assets-amount").text(mandateJSON.totalAssets.amount.resolved);
                        if (mandateJSON.totalAssets != null) itemElem.find(".attribute-total-assets-amount-value").attr("data-text", mandateJSON.totalAssets.amount.value);
                        if (mandateJSON.totalAssets != null) itemElem.find(".attribute-total-assets-currency").text(mandateJSON.totalAssets.currency);
                        // Status
                        var badgeElem = itemElem.find(".attribute-badge");
                        if (reportJSON.reportBadge != null) {
                            if (reportJSON.reportBadge.color != null) badgeElem.addClass("option-" + reportJSON.reportBadge.color);
                            badgeElem.text(Machinata.Reporting.Text.resolve(instance,reportJSON.reportBadge.label));
                            if (reportJSON.reportBadge.tooltip != null) badgeElem.attr("title", Machinata.Reporting.Text.resolve(instance,reportJSON.reportBadge.tooltip));
                        } else {
                            badgeElem.remove();
                        }
                        itemElem.find(".table-cell.type-tools").append(Machinata.Reporting.buildIcon(instance, "arrow-right").addClass("machinata-reporting-open-report option-only-on-hover").attr("title", Machinata.Reporting.Text.translate(instance, "reporting.view-report")));
                        itemElem.find(".attribute-type").text(Machinata.Reporting.Text.resolve(instance,mandateJSON.mandateType));
                        itemElem.removeClass(config.catalogItemReportTemplateSelector.replace(".", ""));
                        instance.elems.catalog.append(itemElem);
                    } else if (index == maxReportsToList) {
                        itemElem.find(".attribute-reporting-date").text(Machinata.Reporting.Text.translate(instance, "reporting.view-all-dates"));
                        itemElem.click(function (evt) {
                            evt.stopPropagation();
                            evt.preventDefault();
                            Machinata.Reporting.Handler.changeReportingDate(instance, instance.catalogData, mandateJSON, null, mandateDate, Machinata.Reporting.Text.translate(instance, "reporting.choose-date"));
                        });
                        itemElem.removeClass(config.catalogItemReportTemplateSelector.replace(".", ""));
                        instance.elems.catalog.append(itemElem);
                    }
                });
            }

        });
        // Remove templates
        templateMandateElem.remove();
        templateReportElem.remove();
        // Table UI UX
        Machinata.Reporting.Handler.bindTableUI(instance,tableElem);
    }
    Machinata.Debugging.stopTimer("catalog-compile");
};