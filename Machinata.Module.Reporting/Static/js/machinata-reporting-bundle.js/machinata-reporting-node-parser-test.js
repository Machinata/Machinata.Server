/// <summary>
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.ParserTestNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ParserTestNode.defaults = {};

/// <summary>
/// This node does not support a toolbar.
/// </summary>
Machinata.Reporting.Node.ParserTestNode.defaults.supportsToolbar = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ParserTestNode.defaults.chrome = "solid";


Machinata.Reporting.Node.ParserTestNode.init = function (instance, config, json, nodeElem) {

    var filter = json.filter;
    if (json.isAny != null) filter = Machinata.Parser.convertIsAnyArrayToTextFilter(json.isAny);
    if (json.notAny != null) filter = Machinata.Parser.convertNotAnyArrayToTextFilter(json.notAny);

    var result = null;
    try {
        result = Machinata.Parser.parseTextFilter(filter, json.input);
    } catch (error) {
        result = "error: " + error;
    }

    nodeElem.addClass("type-InfoNode");
    var bgElem = $("<div class='node-bg'></div>").appendTo(nodeElem);
    $("<div class='message'></div>").text("Input: " + json.input).appendTo(bgElem);
    $("<div class='message'></div>").text("Filter: " + filter + (json.isAny != null ? " (isAny:" + JSON.stringify(json.isAny) + ")" : "") + (json.notAny != null ? " (notAny:" + JSON.stringify(json.notAny) + ")" : "")).appendTo(bgElem);
    $("<div class='message'></div>").text("Expected: " + json.expected + (json.expected == result ? "" : " !!! <<<<<<")).appendTo(bgElem);
    $("<div class='message'></div>").text("Result: " + result).appendTo(bgElem);
    if (json.expected != result) bgElem.css("background-color", "red");

};
Machinata.Reporting.Node.ParserTestNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...
};








