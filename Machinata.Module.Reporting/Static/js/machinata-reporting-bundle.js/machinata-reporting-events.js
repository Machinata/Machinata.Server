

/// <summary>
/// 
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Events = {};


/* ======== Signals ========================================================== */

/// <summary>
/// </summary>
Machinata.Reporting.Events.REDRAW_NODE_SIGNAL = "machinata-reporting-redraw-node";

/// <summary>
/// </summary>
Machinata.Reporting.Events.REDRAW_NODE_AND_CHILDREN_SIGNAL = "machinata-reporting-redraw-node-and-children";

/// <summary>
/// </summary>
Machinata.Reporting.Events.CLEANUP_NODE_SIGNAL = "machinata-reporting-cleanup-node";

/// <summary>
/// </summary>
Machinata.Reporting.Events.REMOVE_NODE_SIGNAL = "machinata-reporting-remove-node";

/// <summary>
/// </summary>
Machinata.Reporting.Events.REBUILD_TABLE_SIGNAL = "machinata-reporting-rebuild-table";

/// <summary>
/// </summary>
Machinata.Reporting.Events.SHOW_TAB_SIGNAL = "machinata-reporting-show-tab";
