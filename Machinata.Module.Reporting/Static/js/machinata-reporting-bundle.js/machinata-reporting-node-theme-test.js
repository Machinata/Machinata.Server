


/// <summary>
/// A very basic node to test themes. This node will generate a pallet for the theme including
/// a text overlay in the proper theme shade overlay color.
/// 
/// Note: This node should not be used in production.
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.ThemeTestNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThemeTestNode.defaults = {};

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.ThemeTestNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThemeTestNode.defaults.chrome = "solid";

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThemeTestNode.getVegaSpec = function (instance, config, json, nodeElem) {

    // Create marks for each shade for each theme
    // Due to time contrainsts we just generate the grid manually and don't use any of the vega data...
    var themeKeys = Object.keys(Machinata.Reporting._themesNew);
    var themeMarks = [];
    for (var i = 0; i < themeKeys.length; i++) {
        var themeKey = themeKeys[i];
        var theme = Machinata.Reporting._themesNew[themeKey];
        var yp = i / themeKeys.length;
        var numberOfThemes = themeKeys.length;
        for (var ii = 0; ii < theme.colorsBG.length; ii++) {
            var xp = ii / theme.colorsBG.length;
            var numberOfColors = theme.colorsBG.length;
            var themeBG = theme.colorsBG[ii];
            var themeFG = theme.colorsFG[ii];
            var themeName = theme.name;
            var colorName = theme.colorNames[ii];
            themeMarks.push({
                "type": "group",
                "signals": [
                    {
                        "name": "xp",
                        "init": xp
                    },
                    {
                        "name": "numberOfColors",
                        "init": numberOfColors
                    },
                    {
                        "name": "numberOfThemes",
                        "init": numberOfThemes
                    },
                    {
                        "name": "yp",
                        "init": yp
                    },
                    {
                        "name": "themeBG",
                        "init": "'" + themeBG + "'"
                    },
                    {
                        "name": "themeFG",
                        "init": "'" + themeFG + "'"
                    },
                    {
                        "name": "colorName",
                        "init": "'" + colorName + "'"
                    },
                    {
                        "name": "themeName",
                        "init": "'" + themeName + "'"
                    }
                ],
                "marks": [
                    {
                        "type": "group",
                        "encode": {
                            "enter": {
                                "fill": { "signal": "themeBG" },
                                //"tooltip": { "signal": "tooltip" }
                            },
                            "update": {
                                "x": { "signal": "xp * width" },
                                "width": { "signal": "width / numberOfColors" },
                                "y": { "signal": "yp * height" },
                                "height": { "signal": "height / numberOfThemes" }
                            }
                        },
                        "signals": [
                            {
                                "name": "width2",
                                "update": "width / numberOfColors"
                            },
                            {
                                "name": "height2",
                                "update": "height / numberOfThemes"
                            },
                            {
                                "name": "tooltip",
                                "init": "{Theme:themeName,Shade:colorName,BG:themeBG,FG:themeFG}"
                            },
                        ],
                        "marks": [
                            {
                                // This is the canary that tests the vega scheme as well
                                "type": "rect",
                                "name": "canary1",
                                "encode": {
                                    "update": {
                                        "x": { "signal": "width2/2" },
                                        "width": { "signal": "width2/2" },
                                        "y": { "signal": "0" },
                                        "height": { "signal": "height2/2" },
                                    },
                                    "enter": {
                                        "fill": { "signal": "scale('theme-'+themeName+'-bg',colorName)" },
                                        "tooltip": { "signal": "tooltip" }
                                    }
                                }
                            },
                            {
                                // This is the canary that tests the internal util methods as well
                                "type": "rect",
                                "name": "canary2",
                                "encode": {
                                    "update": {
                                        "x": { "signal": "0" },
                                        "width": { "signal": "width2/2" },
                                        "y": { "signal": "0" },
                                        "height": { "signal": "height2/2" },
                                    },
                                    "enter": {
                                        "fill": { "value": Machinata.Reporting.getThemeBGColorByShade(config,themeName,colorName) },
                                        "tooltip": { "signal": "tooltip" }
                                    }
                                }
                            },
                            {
                                "type": "text",
                                "encode": {
                                    "update": {
                                        "x": { "signal": "width2/2" },
                                        "y": { "signal": "height2/2" },
                                    },
                                    "enter": {
                                        "tooltip": { "signal": "tooltip" },
                                        "dy": { "signal": "-(14*(2-1))/2" },
                                        "align": { "value": "center" },
                                        "baseline": { "value": "middle" },
                                        "fontWeight": { "value": "bold" },
                                        "fontSize": { "value": 14 },
                                        "fill": { "signal": "themeFG" },
                                        "text": { "signal": Machinata.Reporting.isDebugEnabled() ? "[themeName,colorName,'BG: '+themeBG,'FG: '+themeFG]" : "[themeName,colorName]" }
                                    }
                                }
                            }
                        ]
                    }
                    
                ]
            });
        }
    }
    return {
        "data": [
        ],
        "marks": themeMarks
    };
};
Machinata.Reporting.Node.ThemeTestNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    
};
Machinata.Reporting.Node.ThemeTestNode.init = function (instance, config, json, nodeElem) {
    
    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);

    // Automatically set the height of the node according to number of themes
    if (nodeElem != null) {
        var numThemes = Object.keys(Machinata.Reporting._themesNew).length;
        nodeElem.find(".node-bg").css("height", numThemes * 200);
    }

};
Machinata.Reporting.Node.ThemeTestNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.ThemeTestNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};







