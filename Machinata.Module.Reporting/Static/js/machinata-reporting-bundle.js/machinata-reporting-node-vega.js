/// <summary>
/// This abstract Node type provides the infrastructure needed for displaying
/// and managing Vega-based chart Nodes.
/// This Node implementation also provides some useful helper functions, such as
/// createVegaLegend(), for doing mundane Vega tasks. Through the applyGlobalVegaSpec(),
/// implementing Nodes only need to provide the Node-specific Vega specs - common Vega
/// spec stuff such as config and styles are provided by VegaNode automatically.
/// Nodes inheriting the VegaNode should call the parent methods before executing their own.
///
/// ## Chart Data
/// Data for the vega charts are (for obvious reasons) not integrated into the specs.
/// The data is automatically bound in using the ```Machinata.Reporting.Node.VegaNode.applyVegaData``` interface.
/// Here the data from the node JSON is bound into the spec when needed.
///
/// ### JSON Date/Times
/// Date/times should always be provided in Unix Epoch Milliseconds long format.
///
/// ### Themes
/// All Vega-Nodes must have a valid theme registered for it.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.VegaNode = {};

/// <summary>
/// Supported defaults for VegaNode Node types.
/// Note that due to the nature of JavaScript these are actually not automatically
/// inherited to implementing Nodes - you must redefine them.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults = {};

/// <summary>
/// If true, a legend will automatically be inserted via createVegaLegend().
/// To use the automatic legend, you must implement in the vega spec the 
/// following scales which use the mark group key to resolve:
///  - ```legendLabel```
///  - ```legendColor```
/// Optionally, the vega spec data set 'legendSelected' can be provided to
/// create a legend that is interactive (toggable).
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.insertLegend = false;

/// <summary>
/// For charts that support legends, this defines the number of columns.
/// Implementing chart types will define their own default settings.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendColumns = 1;

/// <summary>
/// For charts that support legends, this property optionally defines the 
/// minimum number of rows (in space) the legend should occupy.
/// This is especially useful for aligning the size of the chart area across multiple charts if
/// the legend data varies (here it is recommended to find the common ```legendColumns``` and ```legendMinRows```
/// that suits all the charts).
/// By default ```null```.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendMinRows = null;

/// <summary>
/// For charts that support legends, this property defines the position of the chart legend.
/// Supported positions:
///  - ```left-top```: Place the legend to the left of the chart, in the top corner.
///  - ```right-top```: Place the legend to the right of the chart, in the top corner.
///  - ```top-left```: Place the legend above the top of the chart, on the left side.
///  - ```bottom-left```: Place the legend below the bottom of the chart, on the left side.
/// By default ```null```.
/// If the chart node does not define this setting, then the global default ```Machinata.Reporting.Config.legendPosition``` is used.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendPosition = null;

/// <summary>
/// For render modes that have fixed chart dimensions (such as print), the legend position
/// can differ depending on the orientation using the ```legendPositionPortrait``` 
/// and ```legendPositionLandscape``` properties.
/// See ```legendPosition``` for supported positions.
/// By default ```null```.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendPositionPortrait = null;

/// <summary>
/// For render modes that have fixed chart dimensions (such as print), the legend position
/// can differ depending on the orientation using the ```legendPositionPortrait``` 
/// and ```legendPositionLandscape``` properties.
/// See ```legendPosition``` for supported positions.
/// By default ```null```.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendPositionLandscape = null;

/// <summary>
/// For charts that support legends, if ```legendSiblingsAutoBalance``` is not explicitly ```false``` then
/// chart legends are automatically balanced with regards to their siblings (ie charts next to 
/// the current chart).
/// When using this feature, you do not have to provide ```legendColumns``` or ```legendMinRows```,
/// as this feature will automatically set these settings. You can optionally set ```legendColumns``` to
/// force a specific look.
/// Currently this only works if the following properties are defined as well:
///  - ```legendSiblingsExpectedMinItems```
///  - ```legendSiblingsExpectedMaxItems```
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendSiblingsAutoBalance = true;

/// <summary>
/// For charts that support legends and are auto-balanced, this property
/// hints to how the legend should be sized. 
/// This property must equal the minimum number of legend items across all
/// neighboring siblings (including itself).
/// By default ```null```.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendSiblingsExpectedMinItems = null;

/// <summary>
/// For charts that support legends and are auto-balanced, this property
/// hints to how the legend should be sized. 
/// This property must equal the maximum number of legend items across all
/// neighboring siblings (including itself).
/// By default ```null```.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendSiblingsExpectedMaxItems = null;

/// <summary>
/// If set to ```true```, the legend will be reversed in order.
/// By default ```false```.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendReversed = false;

/// <summary>
/// If set to ```true```, the legend will be automatically reversed in order if the legend position has a vertical display.
/// Node implementations should set this default value if needed.
/// By default ```false```.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendAutomaticallyReversedForVerticalLegends = false;

/// <summary>
/// If ```true```, legend item labels are automatically linebreaked if too long to fit
/// in the given line-space.
/// Note: this feature should only be used on static (non responsive) mediums such as print, as
/// the line-breaking does not run in render-time, but in the pre-processing.
/// Note: this feature is not compatible with legendSiblingsAutoBalance, as there is a race-condition
/// between finding out the max-width of a legend label, and balancing the legend. Both can be enabled,
/// but the results may be unbalanced und visually unpleasing.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendLabelsLineBreakEnabled = false;

/// <summary>
/// If ```legendLabelsLineBreakEnabled``` is enabled, defines the max characters per line.
/// If ```null```, this value is automatically determined by the legend item max width.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendLabelsLineBreakMaxCharsPerLine = null;

/// <summary>
/// If ```legendLabelsLineBreakEnabled``` is enabled, defines the max lines per legend item that a label can use.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendLabelsLineBreakMaxLines = null;

/// <summary>
/// If ```true```, each legend label has a tooltip which is either a custom tooltip provided
/// by the chart, or it's title. This is useful if one must ensure that truncated tooltips are always
/// accessible to view the full data on digital mediums.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.legendLabelsTooltipsEnabled = true;

/// <summary>
/// If set, defines the padding area around the chart. Can be either a number or object in format of:
/// {"left": 5, "top": 5, "right": 5, "bottom": 5}
/// If this value is not set, then the configuration default ```Machinata.Reporting.Config.chartPadding``` is used.
/// Typically the padding should always be zero, and is only used for special cases.
/// By default ```null```.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.chartPadding = null;

/// <summary>
/// If set, defines the max drawing area for the main chart drawing 
/// (excluding auxiliary things such as legends).
/// This is useful if one wants to restrict a series of charts to visually have 
/// similar layout, regardless of the size of the legend or other components driving
/// the main chart drawing size.
/// If the value is set to below 1.0, then a ratio of the total chart size is defined.
/// If the value is set to abovote 1.0, then a size in pixels is defined.
/// By default ```null```.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.maxDrawingWidth = null;

/// <summary>
/// If set, defines the max drawing area for the main chart drawing 
/// (excluding auxiliary things such as legends).
/// This is useful if one wants to restrict a series of charts to visually have 
/// similar layout, regardless of the size of the legend or other components driving
/// the main chart drawing size.
/// If the value is set to below 1.0, then a ratio of the total chart size is defined.
/// If the value is set to abovote 1.0, then a size in pixels is defined.
/// By default ```null```.
/// </summary>
Machinata.Reporting.Node.VegaNode.defaults.maxDrawingHeight = null;

/// <summary>
/// Utility method to replace/update a signal within a vega spec. Applies only to root signals.
/// </summary>
Machinata.Reporting.Node.VegaNode.updateSignalInSpec = function (instance, config, spec, newSignal) {
    for (var i = 0; i < spec.signals.length; i++) {
        var signal = spec.signals[i];
        if (signal.name == newSignal.name) {
            spec.signals[i] = newSignal;
            return spec;
        }
    }
    // Not found, add it
    spec.signals.push(newSignal);
    return spec;
};

/// <summary>
/// Applies the common vega spec for all VegaNode implementations.
/// You do not need to call this from an implementing node, this is done automatically by the init.
/// </summary>
Machinata.Reporting.Node.VegaNode.applyGlobalVegaSpec = function (instance, config, json, nodeElem, spec) {
    // Init
    var self = this;
    // General
    spec["$schema"] = "https://vega.github.io/schema/vega/v5.json";
    spec["padding"] = json.chartPadding || config.chartPadding; // fixes some small inaccuraces in font measuring, especially in nodejs environment
    spec["autosize"] = {
        // Sets how the visualization size should be determined. If a string, should be one of pad (default), fit, fit-x, fit-y, or none. Object values can additionally specify parameters for content sizing and automatic resizing. See the autosize section below for more. If signal-valued ≥ 5.10, the provided expression is used as the update property for the underlying autosize signal definition, and should evaluate to a complete autosize object.
        // fit: Automatically adjust the layout in an attempt to force the total visualization size to fit within the given width, height and padding values. This setting causes the plotting region to be made smaller in order to accommodate axes, legends and titles. As a result, the value of the width and height signals may be changed to modify the layout. Though effective for many plots, the fit method can not always ensure that all content remains visible. For example, if the axes and legends alone require more space than the specified width and height, some of the content will be clipped. Similar to none, by default the total width will be width + padding.left + padding.right, relative to the original, unmodified width value. If autosize.contains is set to "padding", the total width will instead be the original width.
        "type": "fit", 
        "contains": "padding" // Determines how size calculation should be performed, one of content (default) or padding. The default setting (content) interprets the width and height settings as the data rectangle (plotting) dimensions, to which padding is then added. In contrast, the padding setting includes the padding within the view size calculations, such that the width and height settings indicate the total intended size of the view.
    };
    // Standard data
    if (spec["data"] == null) spec["data"] = [];
    // Standard scales
    if (spec["scales"] == null) spec["scales"] = [];
    // Signals
    if (spec["signals"] == null) spec["signals"] = [];
    spec["signals"].push({
        "name": "widthInPercentOfMedium",
        "update": "width / " + config.mediumContentWidthSize
    });
    spec["signals"].push({
        "name": "isLayoutedOnHalfMediumWidth",
        "update": "widthInPercentOfMedium < 0.6"
    });
    spec["signals"].push({
        "name": "aspectRatio",
        "update": "width / height"
    });
    // Raw width/height
    // We can use this because vega's height signal with autofit varies depending on legend size etc...
    var rawWidthSignal = "width"; // fallback
    if (nodeElem != null) rawWidthSignal = "containerSize()[0] - " + (config.padding * 2);
    if (json.width != null) rawWidthSignal = json.width;
    if (json.forcedWidth != null) rawWidthSignal = json.forcedWidth;
    var rawHeightSignal = "height"; // fallback
    if (nodeElem != null) rawHeightSignal = "containerSize()[1] - " + (config.padding*2);
    if (json.height != null) rawHeightSignal = json.height;
    if (json.forcedHeight != null) rawHeightSignal = json.forcedHeight;
    spec["signals"].push({
        "name": "rawWidth",
        "update": rawWidthSignal
    });
    spec["signals"].push({
        "name": "rawHeight",
        "update": rawHeightSignal
    });
    // Max drawing width/height signal
    var maxDrawingWidthSignal = "width";
    var drawingWidthSignal = "width";
    if (json.maxDrawingWidth != null) {
        if (json.maxDrawingWidth < 1) {
            maxDrawingWidthSignal = json.maxDrawingWidth + " * rawWidth";
        } else {
            maxDrawingWidthSignal = "" + json.maxDrawingWidth + "";
        }
        drawingWidthSignal = "maxDrawingWidth > width ? width: maxDrawingWidth";
    }
    var maxDrawingHeightSignal = "width";
    var drawingHeightSignal = "height";
    if (json.maxDrawingHeight != null) {
        if (json.maxDrawingHeight < 1) {
            maxDrawingHeightSignal = json.maxDrawingHeight + " * rawHeight";
        } else {
            maxDrawingHeightSignal = ""+json.maxDrawingHeight+"";
        }
        drawingHeightSignal = "maxDrawingHeight > height ? height: maxDrawingHeight";
    }
    spec["signals"].push({
        "name": "maxDrawingWidth",
        "update": maxDrawingWidthSignal
    });
    spec["signals"].push({
        "name": "maxDrawingHeight",
        "update": maxDrawingHeightSignal
    });
    spec["signals"].push({
        "name": "drawingWidth",
        "update": drawingWidthSignal
    });
    spec["signals"].push({
        "name": "drawingHeight",
        "update": drawingHeightSignal
    });
    spec["signals"].push({
        "name": "drawingX2",
        "update": null
    });
    spec["signals"].push({
        "name": "drawingY2",
        "update": null
    });
    // Figure out the chart bg color
    var chartTextColor = config.chartTextColorSolidChrome; 
    var chartBackgroundColor = config.chartBackgroundColorSolidChrome; // was "scale('theme-gray-bg','gray1')"; // fallback TODO
    if (json.chrome == "light") {
        chartTextColor = config.chartTextColorLightChrome;
        chartBackgroundColor = config.chartBackgroundColorLightChrome; // was chartBackgroundColor = "'white'"; //TODO
    } else if (json.chrome == "dark") {
        chartTextColor = config.chartTextColorDarkChrome;
        chartBackgroundColor = config.chartBackgroundColorDarkChrome; // was chartBackgroundColor = "'black'"; //TODO
    }
    if (chartBackgroundColor == "unset") chartBackgroundColor = config.pageColor;
    // Register all settings
    spec["signals"].push({
        "name": "chartBackgroundColor",
        "init": "\"" + chartBackgroundColor + "\""
    });
    spec["signals"].push({
        "name": "chartTextColor",
        "init": "\"" + chartTextColor + "\""
    });
    spec["signals"].push({
        "name": "config_profile",
        "init": "'" + config.profile + "'"
    });
    spec["signals"].push({
        "name": "config_medium",
        "init": "'" + config.medium + "'"
    });
    spec["signals"].push({
        "name": "responsiveLayout", // needs to be updated whenever the view changes size
        "init": "'" + Machinata.Reporting.Responsive.getReportLayout(instance) + "'"
    });
    spec["signals"].push({
        "name": "responsiveSize", // needs to be updated whenever the view changes size
        "update": "config_medium == 'screen' && width < 400 ? 'tiny' : 'regular'"
    });
    spec["signals"].push({
        "name": "config_fontSizeToCharacterWidthRatio",
        "init": config.fontSizeToCharacterWidthRatio
    });
    spec["signals"].push({
        "name": "config_padding",
        "init": config.padding
    });
    spec["signals"].push({
        "name": "config_h1Size",
        "init": config.h1Size
    });
    spec["signals"].push({
        "name": "config_chartTextSize",
        "init": config.chartTextSize
    });
    spec["signals"].push({
        "name": "config_legendSize",
        "init": config.legendSize
    });
    spec["signals"].push({
        "name": "config_lineHeight",
        "init": config.lineHeight
    });
    spec["signals"].push({
        "name": "config_lineSize",
        "init": config.lineSize
    });
    spec["signals"].push({
        "name": "config_titleSize",
        "update": "responsiveLayout == 'mobile' ? (" + config.titleSize + " * " + config.mobileTitleSizeFactor + ") : " + config.titleSize
    });
    spec["signals"].push({
        "name": "config_subtitleSize",
        "update": "responsiveLayout == 'mobile' ? (" + config.subtitleSize + " * " + config.mobileSubtitleSizeFactor + ") : " + config.subtitleSize
    });
    spec["signals"].push({
        "name": "config_labelSize",
        "update": "responsiveLayout == 'mobile' ? (" + config.labelSize + " * " + config.mobileLabelSizeFactor + ") : " + config.labelSize
    });
    // Default configs
    spec["config"] = {
        "mark": {
            "font": config.textFont,
            "fontSize": config.chartTextSize
        },
        "text": {
            "font": config.textFont,
            "fontSize": config.chartTextSize
        },
        "style": {
            "text": {
                "font": config.textFont,
                //"fontWeight": "bold",
                "fontSize": config.chartTextSize
            },
            "subtitle": {
                "font": config.subtitleFont,
                //"fontWeight": "bold",
                "fontSize": config.subtitleSize
            },
            "label": {
                "font": config.labelFont,
                //"fontWeight": "bold",
                "fontSize": config.labelSize
            },
            "number": {
                "font": config.numberFont
            }
        },
        "axis": {
            "ticks": false,
            "tickWidth": config.gridLineSize,
            "offset": 0, // The orthogonal offset in pixels by which to displace the axis from its position along the edge of the chart.
            "gridOpacity": 1.0,
            "gridColor": config.gridColor,
            "gridWidth": config.gridLineSize,
            "labelLimit": json.axisLabelLimit || config.axisLabelLimit, // The maximum allowed length in pixels of axis tick labels.
            "labelSeparation": json.axisLabelSeparation || config.axisLabelSeparation, // The minimum separation that must be between label bounding boxes for them to be considered non-overlapping (default 0). This property is ignored if labelOverlap resolution is not enabled. ≥ 5.0
            "labelOverlap": json.axisLabelOverlap || config.axisLabelOverlap, // The strategy to use for resolving overlap of axis labels. If false (the default), no overlap reduction is attempted. If set to true or "parity", a strategy of removing every other label is used (this works well for standard linear axes). If set to "greedy", a linear scan of the labels is performed, removing any label that overlaps with the last visible label (this often works better for log-scaled axes).
            "labelPadding": json.axisLabelPadding || config.axisLabelPadding, // The padding in pixels between labels and ticks.
            "labelFlushOffset": 0, // Indicates the number of pixels by which to offset flush-adjusted labels (default 0). For example, a value of 2 will push flush-adjusted labels 2 pixels outward from the center of the axis. Offsets can help the labels better visually group with corresponding axis ticks.
            "labelFont": config.axisFont,
            "labelFontSize": config.axisSize,
            "labelFontWeight": config.axisLabelFontWeight,
            //"labelOffset": 20, // Position offset in pixels to apply to labels, in addition to tickOffset.
            "titleFont": config.textFont,
            "titleFontSize": config.chartTextSize,
            "titleFontWeight": "normal"
        },
        "legend": {
            "labelFont": config.legendFont,
            "labelFontSize": config.legendSize,
            "labelColor": chartTextColor,
            "padding": 0,
            "layout": {
                // See https://vega.github.io/vega/docs/config/#legends-layout
                //anchor	String	An anchor value determining the placement of the legends relative to the nearest axis.One of "start"(default ), "middle", or "end".For example, for legends with orient "top", these values respectively correspond to anchoring the legends to the left edge, center, or right edge of the charting area.This property only applies to axes with an orient value of "left", "right", "top", or "bottom".
                //bounds	String	The type of bounding box calculation to use for calculating legend extents.One of "flush"(the default , for using legend width and height values only) or "full"(to use the full bounding box, for example including border stroke widths).
                //center	Boolean	A boolean flag(default false) indicating if legends should be centered within the respective layout area.For example, given a vertical direction, two legends will share a left edge by default.If center is true, the smaller legends will be centered in the space spanned by all the legends.
                //direction	String	The direction in which subsequent legends should be spatially positioned.One of "horizontal" or "vertical".
                //margin	Number	Margin, in pixels, to place between consecutive legends with the same orient value.
                "offset": json.legendOffset || config.legendOffset, //offset	Number	Offset, in pixels, of the legend from the chart body. (trial and error shows default is 18)
            }
        }
    };
    // Themes
    // All themes are registered as theme-NAME-bg, theme-NAME-fg
    // The node's theme is registered as theme-bg, theme-fg
    function registerThemeIntoSpec(theme, scaleName) {
        // Data
        if (spec["data"] == null) spec["data"] = [];
        spec["data"].push({
            "name": scaleName+"-shades",
            "values": theme.colorNames
        });
        // Standard scales
        if (spec["scales"] == null) spec["scales"] = [];
        spec["scales"].push({
            "name": scaleName +"-bg",
            "type": "ordinal",
            "domain": theme.colorNames, //{ "data": scaleName +"-shades", "field": "data" },
            "range": { "scheme": theme.name + "-bg" }
        });
        spec["scales"].push({
            "name": scaleName + "-fg",
            "type": "ordinal",
            "domain": theme.colorNames, // { "data": scaleName + "-shades", "field": "data" },
            "range": { "scheme": theme.name + "-fg" }
        });
    };
    var nodeTheme = Machinata.Reporting.getTheme(json.theme);
    registerThemeIntoSpec(nodeTheme, "theme"); // node's theme
    var themeKeys = Object.keys(Machinata.Reporting._themesNew);
    for (var i = 0; i < themeKeys.length; i++) {
        var theme = Machinata.Reporting._themesNew[themeKeys[i]];
        registerThemeIntoSpec(theme, "theme-" + theme.name); // all themes
    }
    return spec;
};


/// <summary>
/// Applies the common vega legend spec for VegaNode implementations that use the automatic legend.
/// You do not need to call this from an implementing node, this is done automatically by the init.
/// </summary>
Machinata.Reporting.Node.VegaNode.applyLegendVegaSpec = function (instance, config, json, nodeElem, spec) {
    var self = this;
    var insertLegend = json.insertLegend;

    // Resolve titles
    if (json.insertLegend == true && json.legendData != null) {
        for (var i = 0; i < json.legendData.length; i++) {
            if (json.legendData[i].titleResolved == null && json.legendData[i].title != null) {
                json.legendData[i].titleResolved = Machinata.Reporting.Text.resolve(instance, json.legendData[i].title);
            }
        }
    }

    // All legend data "empty", or no legend data at all? 
    // If so, we turn off the legend
    if (json.insertLegend == true && json.legendData == null) {
        insertLegend = false; // no legend data
    } if (json.insertLegend == true && json.legendData != null) {
        var allEmpty = true;
        for (var i = 0; i < json.legendData.length; i++) {
            if (json.legendData[i].title != null && json.legendData[i].title != "") {
                allEmpty = false;
                break;
            } else if (json.legendData[i].titleResolved != null && json.legendData[i].titleResolved != "") {
                allEmpty = false;
                break;
            }
        }
        if (allEmpty == true) {
            insertLegend = false;
        }
    }

    if (insertLegend == true) {

        Machinata.Reporting.debug("Machinata.Reporting.Node.VegaNode.applyLegendVegaSpec");
        Machinata.Reporting.debug("  " + "node id:" + json.id);

        // Legend position
        var legendWidthAllottedSpace = 1.0;
        var legendHeightAllottedSpace = 1.0;
        var legendForceColumns = null;
        var legendForceRows = null;
        var legendIsVertical = false;
        function getLegendOrient(key) {
            // First, test for the key (ie legendPositionPortrait)
            var legendOrient = Machinata.Reporting.Config[key]; // fallback
            if (json[key] != null) legendOrient = json[key];
            else if (config[key] != null) legendOrient = config[key];
            // If nothing, fallback to legendPosition
            if (legendOrient == null) {
                legendOrient = Machinata.Reporting.Config.legendPosition; // fallback
                if (json.legendPosition != null) legendOrient = json.legendPosition;
                else if (config.legendPosition != null) legendOrient = config.legendPosition;
            }
            // Convert to vega position
            if (legendOrient == "left-top") {
                legendOrient = "left";
                legendForceColumns = 1;
                legendWidthAllottedSpace = 0.35;
                legendIsVertical = true;
            } else if (legendOrient == "right-top") {
                legendOrient = "right";
                legendForceColumns = 1;
                legendWidthAllottedSpace = 0.35;
                legendIsVertical = true;
            } else if (legendOrient == "top-left") {
                legendOrient = "top";
            } else if (legendOrient == "bottom-left") {
                legendOrient = "bottom";
            } else {
                legendOrient = "bottom";
            }
            return legendOrient;
        }
        var legendOrient = getLegendOrient("legendPosition");
        if (json.width != null && json.height != null) {
            if ((json.width / json.height) > 1.0) legendOrient = getLegendOrient("legendPositionLandscape");
            else legendOrient = getLegendOrient("legendPositionPortrait");
        }
        if (legendForceColumns != null) json.legendColumns = legendForceColumns;

        // Update drawing dimensions based on legend orient
        if (legendOrient == "left") {
            Machinata.Reporting.Node.VegaNode.updateSignalInSpec(instance, config, spec, {
                "name": "drawingX2",
                "update": "width"
            });
        }

        // Reverse legend order?
        var legendReversed = json.legendReversed;
        if (json.legendAutomaticallyReversedForVerticalLegends == true && legendIsVertical == true) {
            legendReversed = true;
        }
        var legendData = [];
        for (var i = 0; i < json.legendData.length; i++) {
            var legendDataItem = json.legendData[i];
            // Register the key
            if (legendReversed == true) {
                legendData.splice(0, 0, legendDataItem); // insert (reverse)
            } else {
                legendData.push(legendDataItem); // push
            }
        }
        var actualLegendItemCount = legendData.length;

        // Tooltips?
        if (json.legendLabelsTooltipsEnabled != false) {
            for (var i = 0; i < legendData.length; i++) {
                var legendDataItem = legendData[i];
                if (legendDataItem.tooltipResolved == null) legendDataItem.tooltipResolved = legendDataItem.titleResolved; // fallback
            }
        }

        // Auto balance?
        if (json.legendSiblingsAutoBalance != false && json.legendSiblingsExpectedMaxItems != null && json.legendMinRows == null) {
            var USE_OLD_AUTOBALANCE_METHOD = false;
            if (USE_OLD_AUTOBALANCE_METHOD == true) {
                // Old method: not so good because it forces legend columns even if not needed
                // Set columns, if not explicitly set.
                if (json.legendColumns == null) {
                    json.legendColumns = 1; // fallback
                    if (json.legendSiblingsExpectedMaxItems > 2) json.legendColumns = 2;
                    if (json.legendSiblingsExpectedMaxItems > 8) json.legendColumns = 3;
                }
                // Set rows
                json.legendMinRows = Math.ceil(json.legendSiblingsExpectedMaxItems / json.legendColumns);
            } else {
                // New method: better because it will only set the columns needed, but still allows infinite growth vertically (rows)
                // Init
                var maxItems = json.legendSiblingsExpectedMaxItems;
                var targetColumns = 1; // fallback
                if (maxItems > 2) targetColumns = 2;
                if (maxItems > 8) targetColumns = 3;
                // Calculate rows
                var targetMinRows = Math.ceil(maxItems / targetColumns);
                var neededColumns = Math.ceil(actualLegendItemCount / targetMinRows);
                // Set settings, if not explicitly
                if (json.legendColumns == null) json.legendColumns = neededColumns;
                if (json.legendMinRows == null) json.legendMinRows = targetMinRows;
            }
        }


        // Apply min rows
        // Here we add fake data to ensure that we reach the minimum number of rows
        //TODO: incorporate columns
        if (json.legendMinRows != null) {
            var columns = (json.legendColumns || 1);
            while (legendData.length < json.legendMinRows * columns) {
                Machinata.Reporting.debug("  " + "Adding blank legend item for min rows: " + json.legendMinRows);
                legendData.push({
                    key: "BLANK_" + legendData.length,
                    symbol: "blank"
                });
            }
        }

        // Clip columns down if not needed
        if (json.legendMinRows == null) {
            if (json.legendColumns > 1 && actualLegendItemCount == 1) {
                json.legendColumns = 1;
            }
        }

        

        // Compile an array of all legend keys (we need this for our custom scales)
        var legendKeys = [];
        for (var i = 0; i < legendData.length; i++) {
            // Apply default type
            if (legendData[i].symbol == null) legendData[i].symbol = "square";
            legendKeys.push(legendData[i].key);
        } 

        
        

        // Label max width
        var labelMaxWidthSignal = "(" + config.legendLabelMaxWidth + ")";
        var legendColumnsSafe = 1;
        if (json.legendColumns != null) legendColumnsSafe = json.legendColumns; // legendColumns is not always defined
        var legendSymbolSpace = (legendColumnsSafe) * (config.legendSymbolSize + config.legendSymbolStroke + + config.legendSymbolStroke);
        var legendColumnPaddingSpace = (legendColumnsSafe - 1) * (config.legendColumnPadding);
        function labelMaxWidthPrecomputed(widthToUse) {
            var ret = Math.min(config.legendLabelMaxWidth, Math.floor((((widthToUse * legendWidthAllottedSpace) - legendSymbolSpace - legendColumnPaddingSpace) / legendColumnsSafe) - (0.5 * config.legendSymbolSize)));
            if (isNaN(ret)) console.warn("Machinata.Reporting.Node.VegaNode.applyLegendVegaSpec: labelMaxWidthPrecomputed return NaN! Either legendSymbolSpace or legendColumnPaddingSpace or legendColumns could not be determined!");
            return ret;
        }
        if (true) {
            // Automatically clip label text by reverse engineering the vega spacing used for the legend
            // Figure out our width to use
            var widthToUse = null;
            if (json.width != null) widthToUse = json.width;
            if (json.forcedWidth != null) widthToUse = json.forcedWidth;
            // Do we have a fixed width?
            if (widthToUse != null) {
                // Javascript calculation
                labelMaxWidthSignal = "(" + labelMaxWidthPrecomputed(widthToUse) + ")";
            } else {
                // Vega signal of same calculation
                labelMaxWidthSignal = "min( " + config.legendLabelMaxWidth + ", floor( ((width*" + legendWidthAllottedSpace + " - " + legendSymbolSpace + " - " + legendColumnPaddingSpace + ") / " + legendColumnsSafe + ") - (0.5 * " + config.legendSymbolSize + ") ) )";
            }
        }

        // Line break?
        var lineBreakingEnabled = json.legendLabelsLineBreakEnabled; //todo
        if (lineBreakingEnabled == true) {
            var LINEBREAK_DEBUG = false;
            for (var i = 0; i < legendData.length; i++) {
                var legendDataItem = legendData[i];
                if (legendDataItem.titleResolved != null) {
                    // Calculate max chars per line...
                    var maxCharsPerLine = json.legendLabelsLineBreakMaxCharsPerLine;
                    if (maxCharsPerLine == null) {
                        // Figure out our width to use
                        var widthToUse = null;
                        if (json.width != null) widthToUse = json.width;
                        if (json.forcedWidth != null) widthToUse = json.forcedWidth;
                        if (widthToUse == null) {
                            widthToUse = config.legendLabelMaxWidth; // fallback
                            console.warn("Machinata.Reporting.Node.VegaNode.applyLegendVegaSpec: could not derive the chart width for line-wrapping! Using fallback of config.legendLabelMaxWidth px per line...");
                        }
                        var labelMaxWidthPX = labelMaxWidthPrecomputed(widthToUse);
                        maxCharsPerLine = Machinata.Reporting.Tools.convertPixelstoCharactersApproximately(config, labelMaxWidthPX, config.legendSize, config.legendFont);
                        if (isNaN(maxCharsPerLine)) console.warn("Machinata.Reporting.Node.VegaNode.applyLegendVegaSpec: maxCharsPerLine returned NaN! Either labelMaxWidthPX or config.legendSize could not be determined!");
                        if (LINEBREAK_DEBUG == true) console.log("labelMaxWidthPX", labelMaxWidthPX);
                        if (LINEBREAK_DEBUG == true) console.log("maxCharsPerLine", maxCharsPerLine);
                    }
                    var originalTitleResolved = legendDataItem.titleResolved;
                    legendDataItem.titleResolved = Machinata.Reporting.Tools.lineBreakString(legendDataItem.titleResolved, {
                        maxCharsPerLine: maxCharsPerLine,
                        maxLines: json.legendLabelsLineBreakMaxLines,
                        wordSeparator: " ",
                        ellipsis: config.textTruncationEllipsis,
                        appendEllipsisOnLastLine: true,
                        returnArray: true
                    });
                    if (LINEBREAK_DEBUG == true) {
                        console.log(originalTitleResolved, ">>", legendDataItem.titleResolved, {
                            maxCharsPerLine: maxCharsPerLine,
                            maxLines: json.legendLabelsLineBreakMaxLines,
                            wordSeparator: " ",
                            ellipsis: config.textTruncationEllipsis,
                            appendEllipsisOnLastLine: true,
                            returnArray: true
                        });
                    }
                    // Disable labelMaxWidthSignal
                    labelMaxWidthSignal = 999999;
                }
            }
        }

        // Legend data definitions
        spec["data"].push({
            "name": "legendData",
            "values": legendData
        });
        spec["data"].push({
            "name": "legendSelected",
            "on": []
        });

        // Legend scale definitions
        spec["scales"].push({
            "name": "legendDataScale",
            "type": "ordinal",
            "domain": legendKeys,
            "range": legendKeys
        });

        // Create legend spec
        if (spec["legends"] == null) spec["legends"] = [];
        spec["legends"].push({
            "fill": "legendDataScale", 
            "direction": "vertical",
            "orient": legendOrient, // Note: bottom-left doesnt work, only suppported is bottom
            "gridAlign": "none",
            "symbolType": { "signal": "data('legendData')[datum.index].symbol == 'dot' ? '" + config.legendSymbolForDot + "' : data('legendData')[datum.index].symbol == 'diamond' ? '" + config.legendSymbolForDiamond + "' : data('legendData')[datum.index].symbol == 'line' ? '" + config.legendSymbolForLine + "' : data('legendData')[datum.index].symbol == 'bar'? '" + config.legendSymbolForBar + "' : '" + config.legendSymbolForUnknown + "'"  },
            "symbolSize": (config.legendSymbolSize * config.legendSymbolSize), // The area in pixels of the symbols bounding box. Note that this value sets the area of the symbol; the side lengths will increase with the square root of this value.
            "symbolStrokeColor": null,
            "symbolStrokeWidth": config.legendSymbolStroke,
            "labelLimit": { "signal": labelMaxWidthSignal}, // precompiled signal
            "columns": json.legendColumns,
            "columnPadding": config.legendColumnPadding,
            "rowPadding": config.legendRowPadding,
            "padding": config.legendPadding,
            "clipHeight": config.legendSymbolSize + config.legendSymbolStroke, // Note: the stroke exceeds the symbol bounds since the vega stroke always pushes out from the boundary
            "encode": {
                "title": {
                    "update": {

                    }
                },
                "labels": {
                    "name": "legendLabel",
                    "interactive": true,
                    "enter": {
                        "tooltip": {
                            "signal": json.legendLabelsTooltipsEnabled != false ? "data('legendData')[datum.index].tooltipResolved" : "null"
                        }
                    },
                    "update": {
                        "dy": { value: config.legendLabelOffsetY },
                        "text": {
                            "signal": "data('legendData')[datum.index].titleResolved || scale('legendLabel',datum.value)"
                        }
                    }
                },
                "symbols": {
                    "name": "legendSymbol",
                    "interactive": false,
                    "update": {
                        "fill": [
                            {
                                "test": "data('legendData')[datum.index].symbol == 'line'",
                                "value": "transparent"
                            },
                            {
                                "test": "data('legendData')[datum.index].color != null",
                                "signal": "data('legendData')[datum.index].color"
                            },
                            {
                                "test": "data('legendData')[datum.index].colorShade != null",
                                "signal": "scale('theme-bg',data('legendData')[datum.index].colorShade)"
                            },
                            {
                                "scale": "legendColor",
                                "field": "value"
                            },
                        ],
                        "stroke": [
                            {
                                "test": "data('legendData')[datum.index].color != null",
                                "signal": "data('legendData')[datum.index].color"
                            },
                            {
                                "test": "data('legendData')[datum.index].colorShade != null",
                                "signal": "scale('theme-bg',data('legendData')[datum.index].colorShade)"
                            },
                            {
                                "scale": "legendColor",
                                "field": "value"
                            },
                        ],
                    }
                },
                "legend": {
                }
            }
        });

    } else {

        // Some charts rely on the legend datas to exist...
        spec["data"].push({
            "name": "legendSelected",
            "on": []
        });
    }
};

/// <summary>
/// Returns a JSON vega spec. The returned JSON should not have any data values, as these
/// are bound in by applyVegaData(). When defining a data source, use ```"values": null```.
/// Every VegaNode implemention must implement this method. 
/// </summary>
Machinata.Reporting.Node.VegaNode.getVegaSpec = function (instance, config, json, nodeElem) { };

/// <summary>
/// Applys the data from the node JSON into the vega spec (since the spec is 'dataless').
/// For example, if your node as a array property 'facts', you would implement
///     ```spec["data"][0].values = json.facts;```
/// Every VegaNode implemention must implement this method. 
///
/// TODO: DEPRECATE!!!
///
/// </summary>
Machinata.Reporting.Node.VegaNode.applyVegaData = function (instance, config, json, nodeElem, spec) { };

/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.compileVegaSpec = function (instance, config, json, nodeElem) {
    // Get implementation
    var self = this;
    var impl = Machinata.Reporting.Node[json.nodeType];

    // Get spec and apply data
    var spec = impl.getVegaSpec(instance, config, json, nodeElem);
    impl.applyVegaData(instance, config, json, nodeElem, spec);

    // Apply global spec and parse
    self.applyGlobalVegaSpec(instance, config, json, nodeElem, spec);
    self.applyLegendVegaSpec(instance, config, json, nodeElem, spec);
    
    return spec;
};

/// <summary>
/// Sets up the node for containing a responsive Vega view, including parsing and compiling the spec,
/// and registering common functionality such as tooltips.
/// Every VegaNode implemention must call this method at the begginning of its own
/// init(). Typically, a VegaNode implementation does not need to do anything since
/// most is handled by the getVegaSpec() method.
/// </summary>
Machinata.Reporting.Node.VegaNode.init = function (instance, config, json, nodeElem) {

    // Basic validation
    if (config == null) throw "Machinata.Reporting.Node.VegaNode.init: config JSON cannot be null (parameter config)!";
    if (json == null) throw "Machinata.Reporting.Node.VegaNode.init: node JSON cannot be null (parameter json)!";
    if (json.theme == null) throw "Machinata.Reporting.Node.VegaNode.init: node theme cannot be null (json.theme)! Please make sure to set a theme.";

    // Validate theme
    if (json.theme != null && Machinata.Reporting.getTheme(json.theme) == null) {
        console.warn("Machinata.Reporting.Node.VegaNode: the theme '" + json.theme + "' for node " + json.id + " is not valid. Please make sure any VegaNode has a proper theme set.");
        json.theme = "default";
    }

    // Headless?
    if (config.headless == true && nodeElem == null) {
        //nodeElem = $("<div></div>").appendTo($("body"));
        return;
    }

    // Non-headless validation
    if (nodeElem == null) throw "Machinata.Reporting.Node.VegaNode.init: node Elem cannot be null (parameter nodeElem)!";

    // Tools
    Machinata.Reporting.Node.registerExportOptions(instance, config, json, nodeElem, {
        "svg": "SVG",
        "png": "PNG",
        "xlsx": "Excel",
        "csv": "CSV",
        //"jpeg": "JPG", // vega doesnt support this
    });

    // Get spec and apply data
    var spec = Machinata.Reporting.Node.VegaNode.compileVegaSpec(instance, config, json, nodeElem);
    var parserOptions = {};
    if (config.cspCompliance == true) parserOptions.ast = true;
    var parsedVegaSpec = vega.parse(spec, null, parserOptions);
    //nodeElem.data("vega-parsed-specs", parsedVegaSpec);

    // Create container element
    nodeElem.addClass("type-VegaNode");
    nodeElem.addClass("avoid-page-break");
    var vegaContainer = $("<div class='node-bg vegaview'></div>");
    nodeElem.append(vegaContainer);

    // Create vega view
    var vegaView = new vega.View(parsedVegaSpec, {
        expr: config.cspCompliance == true ? vega.expressionInterpreter : null, // https://vega.github.io/vega/usage/interpreter/
        renderer: 'svg',
        container: vegaContainer[0],
        responsive: false, // has no effect
        hover: true,
    });
    if (Machinata.Reporting.isDebugEnabled() == true) vegaView.logLevel(vega.Info);


    // Register custom tooltip handler
    // Our custom sanitize allows us to build tabular tooltips. We don't use the standard mechanism since
    // it is not possible to modify the keys in runtime (it must be a string)
    if (true) {
        var tooltipOptions = {
            theme: 'custom',
            sanitize: function (value) {
                return String(value)
                    .replace(/&/g, '&amp;')
                    .replace(/</g, '&lt;')
                    .replace(/\n/g, '<br/>')
                    .replace(/\[break\]/g, '<br/>')
                    .replace(/\[colon\]/g, ':&nbsp;')
                    .replace(/\[title\]/g, '<div class="title">')
                    .replace(/\[\/title\]/g, '</div>')
                    .replace(/\[items\]/g, '<table>')
                    .replace(/\[\/items\]/g, '</table>')
                    .replace(/\[key\]/g, '<td class="key">')
                    .replace(/\[\/key\]/g, ':&nbsp;</td>')
                    .replace(/\[val\]/g, '<td class="value">')
                    .replace(/\[\/val\]/g, '</td>')
                    .replace(/\[item\]/g, '<tr>')
                    .replace(/\[\/item\]/g, '</tr>');
            }
        };
        var tooltipHandler = new vegaTooltip.Handler(tooltipOptions);
        vegaView.tooltip(tooltipHandler.call);
    }

    // Bind user events
    // https://vega.github.io/vega/docs/event-streams/
    // https://stackoverflow.com/questions/53949805/react-vega-get-data-on-click-event-add-event-listener-for-click-events
    var impl = Machinata.Reporting.Node[json.nodeType];
    if (impl != null && impl.onMarkClick != null) {
        Machinata.Reporting.debug("VegaNode.onMarkClick", "binding click for " + json.nodeType);
        vegaView.addEventListener('click', function (event, item) {
            if (item != null && item.datum != null) Machinata.Reporting.debug("VegaNode.onMarkClick",item.datum);
            impl.onMarkClick(instance, config, json, nodeElem, event, item);
        });
    }

    // Register with node
    nodeElem.data("vega-container", vegaContainer);
    nodeElem.data("vega-view", vegaView);
};



/// <summary>
/// Draws the vega view.
/// Every VegaNode implemention must call this method at the begginning of its own
/// draw().
/// </summary>
Machinata.Reporting.Node.VegaNode.draw = function (instance, config, json, nodeElem) {
    var VERBOSE_VEGA_DRAW_LOGGING = false;

    // Extract vega elements
    var vegaContainer = nodeElem.data("vega-container");
    if (VERBOSE_VEGA_DRAW_LOGGING) console.log("Machinata.Reporting.Node.VegaNode.draw", "container size ==>", vegaContainer.width(), vegaContainer.height());

    // Forced size?
    if (json.forcedWidth != null) vegaContainer.width(Machinata.Reporting.Tools.convertStringOrNumberToPixels(config,json.forcedWidth));
    if (json.forcedHeight != null) vegaContainer.height(Machinata.Reporting.Tools.convertStringOrNumberToPixels(config,json.forcedHeight));

    // Update the view
    var vegaView = nodeElem.data("vega-view");
    vegaView.signal("responsiveLayout", Machinata.Reporting.Responsive.getReportLayout(instance)); // update the new responsive layout
    if (nodeElem.data("vega-firstdraw") != false) {
        // First draw
        if (VERBOSE_VEGA_DRAW_LOGGING) console.log("Machinata.Reporting.Node.VegaNode.draw", "vega-firstdraw == true");
        nodeElem.data("vega-firstdraw", false);
        vegaView
            .width(vegaContainer.width())
            .height(vegaContainer.height())
            .run();
        
    } else {
        // Subsequent draws (resize)
        if (VERBOSE_VEGA_DRAW_LOGGING) console.log("Machinata.Reporting.Node.VegaNode.draw", "vega-firstdraw ==>", false, "vegaView.width ==> ", vegaView.width());
        vegaView
            .width(vegaContainer.width())
            .height(vegaContainer.height())
            .resize() // subsequent draws we call the vega view resize...
            .run();
        if (VERBOSE_VEGA_DRAW_LOGGING) console.log("Machinata.Reporting.Node.VegaNode.draw", "vegaView.width ==> ", vegaView.width(), "width signal ==>", vegaView.signal("width"));
    }

    
    
};

/// <summary>
/// Outputs a dataset to the console.
/// Note: Machinata.Reporting.isDebugEnabled() must be enabled via Machinata.Reporting.setDebugEnabled()
/// </summary>
Machinata.Reporting.Node.VegaNode.debugOutputDataSet = function (nodeElem, dataset) {
    if (Machinata.Reporting.isDebugEnabled() == true) {
        console.log("Machinata.Reporting.Node.VegaNode.debugOutputDataSet","dataset",nodeElem.data("vega-view").data(dataset));
    }
};




/// <summary>
/// Helper method to export common chart formats.
/// </summary>
Machinata.Reporting.Node.VegaNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    var self = this;

    if (format == "svg" || format == "png") {
        // Create a new view, disjoint from the currently rendered one...

        // Get spec and apply data
        var spec = Machinata.Reporting.Node.VegaNode.compileVegaSpec(instance, config, json, nodeElem);
        var parserOptions = {};
        if (config.cspCompliance == true) parserOptions.ast = true;
        var parsedVegaSpec = vega.parse(spec, null, parserOptions);

        // Create vega view
        var vegaContainer = nodeElem.data("vega-container");
        var vegaView = new vega.View(parsedVegaSpec, {
            expr: config.cspCompliance == true ? vega.expressionInterpreter : null, // https://vega.github.io/vega/usage/interpreter/
            renderer: 'svg',
            container: null,
            responsive: false, // has no effect
            hover: false,
        });

        // Set background
        //TODO: should come from theme
        var backgroundColor = "white"; //fallback
        if (json.chrome == "dark") {
            try {
                backgroundColor = window.getComputedStyle(vegaContainer[0]).getPropertyValue("background-color");
            } catch (error) {
                backgroundColor = "white"; //fallback
            }
        }
        if (backgroundColor == "rgba(0, 0, 0, 0)") backgroundColor = "white"; //fallback sanity
        vegaView.background(backgroundColor);

        // Do initial resize and render
        vegaView
            .width(vegaContainer.width())
            .height(vegaContainer.height())
            .run();

        // Generate a PNG/SVG snapshot and then download the image
        if (format == "png") {
            vegaView.toImageURL(format).then(function (url) {
                if (Machinata.Device.isMSIE() == true) {
                    throw Machinata.Reporting.Text.translate(instance, "reporting.unsupported-browser-feature")
                }
                // Create a link to the blob and invoke the click, so that a nice download type event is triggered for the user
                var link = document.createElement('a');
                link.setAttribute('href', url);
                link.setAttribute('target', '_blank');
                link.setAttribute('download', filename);
                link.dispatchEvent(new MouseEvent('click'));
            }).catch(function (err) {
                console.warn(err);
                Machinata.messageDialog(Machinata.Reporting.Text.translate(instance, "reporting.nodes.export"), Machinata.Reporting.Text.translate(instance, "reporting.nodes.export.error") + " " + err).show()
            });
        }
        if (format == "svg") {
            vegaView.toSVG().then(function (svgString) {
                if (Machinata.Device.isMSIE() == true) {
                    throw Machinata.Reporting.Text.translate(instance, "reporting.unsupported-browser-feature")
                }
                // Inline resources
                Machinata.Reporting.SVG.inlineSVGResourcesInSVG(config, svgString).then(function (svgString) {
                    var url = "data:image/svg+xml;base64," + btoa(svgString);
                    // Create a link to the blob and invoke the click, so that a nice download type event is triggered for the user
                    var link = document.createElement('a');
                    link.setAttribute('href', url);
                    link.setAttribute('target', '_blank');
                    link.setAttribute('download', filename);
                    link.dispatchEvent(new MouseEvent('click'));
                }).catch(function (err) {
                    console.warn(err);
                    Machinata.messageDialog(Machinata.Reporting.Text.translate(instance, "reporting.nodes.export"), Machinata.Reporting.Text.translate(instance, "reporting.nodes.export.error") + " " + err).show()
                });

            }).catch(function (err) {
                console.warn(err);
                Machinata.messageDialog(Machinata.Reporting.Text.translate(instance, "reporting.nodes.export"), Machinata.Reporting.Text.translate(instance, "reporting.nodes.export.error") + " " + err).show()
            });
        }
        return true;
    } else if (format == "xlsx" || format == "csv") {
        // Load the implementation and call it's specific excel export
        var impl = Machinata.Reporting.Node[json.nodeType];
        if (impl.exportExcelFormat == null) return false; // make sure user is notified
        return impl.exportExcelFormat(instance, config, json, nodeElem, format, filename);
    }
    return false;
};




