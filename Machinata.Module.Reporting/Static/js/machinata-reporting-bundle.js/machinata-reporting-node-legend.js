/// <summary>
/// A simple chart node with only a legend.
/// BETA: Automatic sizing is not yet supported.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.LegendNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.LegendNode.defaults = {};

/// <summary>
/// By default charts are on solid chrome.
/// </summary>
Machinata.Reporting.Node.LegendNode.defaults.chrome = "light";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.LegendNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.LegendNode.defaults.supportsDashboard = false;

/// <summary>
/// Typically we don't want the toolbar
/// </summary>
Machinata.Reporting.Node.LegendNode.defaults.supportsToolbar = false;

/// <summary>
/// Yes, we want the legend.
/// </summary>
Machinata.Reporting.Node.LegendNode.defaults.insertLegend = true;


/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.LegendNode.getVegaSpec = function (instance, config, json, nodeElem) {
    // Call parent
    return Machinata.Reporting.Node["GraphicalCompositionNode"].getVegaSpec(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.LegendNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    // Call parent
    Machinata.Reporting.Node["GraphicalCompositionNode"].applyVegaData(instance, config, json, nodeElem, spec);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.LegendNode.init = function (instance, config, json, nodeElem) {
    
    // Create graphics legend items
    json._state.graphics = [];
    for (var i = 0; i < json.legendItems.length; i++) {
        var legendItem = json.legendItems[i];
        legendItem.type = "legend";
        json._state.graphics.push(legendItem);
    }
    // Call parent
    Machinata.Reporting.Node["GraphicalCompositionNode"].init(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.LegendNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["GraphicalCompositionNode"].draw(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.LegendNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["GraphicalCompositionNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};







