


/// <summary>
/// 
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.GraphicalCompositionNode</inherits>
Machinata.Reporting.Node.MSCIThermometerNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.MSCIThermometerNode.defaults = {};

/// <summary>
/// By default charts are on solid chrome.
/// </summary>
Machinata.Reporting.Node.MSCIThermometerNode.defaults.chrome = "light";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.MSCIThermometerNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.MSCIThermometerNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.MSCIThermometerNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_4x2];

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.MSCIThermometerNode.defaults.supportsToolbar = true;

/// <summary>
/// No legend
/// </summary>
Machinata.Reporting.Node.MSCIThermometerNode.defaults.insertLegend = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.MSCIThermometerNode.defaults.aspectRatio = 0.75;

/// <summary>
/// </summary>
Machinata.Reporting.Node.MSCIThermometerNode.defaults.imageBase = {
    "filename": "MSCI_Thermometer_Base-Reporting.svg",
    "mimeType": "image/svg",
    "encoding": "url",
    "data": "/static/file/assets/blkb/images/MSCI_Thermometer_Base-Reporting.svg"
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.MSCIThermometerNode.defaults.imageArrow = {
    "filename": "MSCI_Thermometer_Arrow.svg",
    "mimeType": "image/svg",
    "encoding": "url",
    "data": "/static/file/assets/blkb/images/MSCI_Thermometer_Arrow.svg"
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.MSCIThermometerNode.getVegaSpec = function (instance, config, json, nodeElem) {
    // Call parent
    return Machinata.Reporting.Node["GraphicalCompositionNode"].getVegaSpec(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.MSCIThermometerNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    // Call parent
    Machinata.Reporting.Node["GraphicalCompositionNode"].applyVegaData(instance, config, json, nodeElem, spec);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.MSCIThermometerNode.init = function (instance, config, json, nodeElem) {
    // Calculate the Y position of the portfolio label
    // 0C   => 0.6
    // 1.5C => 0.085
    // 2C   => -0.08
    // 3C   => -0.4
    // 4C   => -0.73
    // 5C   => -0.9
    var yPositionPortfolioValue = 0;
    if (json.portfolioValue != null) yPositionPortfolioValue = Machinata.Math.map(json.portfolioValue, 1.5, 4, 0.085, -0.735);
    var yPositionBenchmarkValue = 0;
    if (json.benchmarkValue != null) yPositionBenchmarkValue = Machinata.Math.map(json.benchmarkValue, 1.5, 4, 0.085, -0.735);
    // Create default graphics
    json._state.graphics = [
        {
            // Main image
            "type": "image",
            "x": 0.0,
            "y": 0.0,
            "width": 0.314,
            "height": 1.2,
            "image": json.imageBase
        },
        {
            // Line for Paris 1.5c
            "type": "rule",
            "x1": -1.0,
            "y1": 0.085,
            "x2": -0.16,
            "y2": 0.085,
            "color": "#393f4b"
        },
        {
            "type": "text",
            "x": 0.0,
            "y": 0.90,
            "align": "center",
            "fontWeight": "normal",
            "fontSize": "chart",
            "color": "white",
            "maxCharsPerLine": 16,
            "text": json.labelBulb
        },
        {
            "type": "text",
            "x": -0.16,
            "y": 0.61,
            "align": "right",
            "baseline": "bottom",
            "fontWeight": "normal",
            "fontSize": "chart",
            "color": "#393f4b",
            "text": json.labelPlus0C
        },
        {
            "type": "text",
            "x": -0.16,
            "y": 0.195,
            "align": "right",
            "fontWeight": "bold",
            "fontSize": "chart",
            "color": "#393f4b",
            "text": json.labelPlus15C
        },
        {
            "type": "text",
            "x": -0.16,
            "y": -0.08,
            "align": "right",
            "baseline": "middle",
            "fontWeight": "normal",
            "fontSize": "chart",
            "color": "#393f4b",
            "text": json.labelPlus2C
        },
        {
            "type": "text",
            "x": -0.16,
            "y": -0.40,
            "align": "right",
            "baseline": "middle",
            "fontWeight": "normal",
            "fontSize": "chart",
            "color": "#393f4b",
            "text": json.labelPlus3C
        },
        {
            "type": "text",
            "x": -0.16,
            "y": -0.73,
            "align": "right",
            "baseline": "middle",
            "fontWeight": "normal",
            "fontSize": "chart",
            "color": "#393f4b",
            "text": json.labelPlus4C
        }
    ];
    // Add portfolio/benchmark graphics
    if (json.portfolioValue != null && json.benchmarkValue != null) {
        // Benchmark
        {
            var baseline = (json.benchmarkValue < json.portfolioValue) ? "top" : "bottom";
            var dyLabel = 0;
            var align = "left";
            var xLabel = +0.16;
            if (baseline == "top") dyLabel = 0.04;
            else dyLabel = -0.02;
            if (json.benchmarkValue < 0.25) {
                xLabel = +1.0;
                align = "right";
            }
            json._state.graphics.push({
                "type": "rule",
                "x1": +1.0,
                "y1": yPositionBenchmarkValue,
                "x2": +0.16,
                "y2": yPositionBenchmarkValue,
                "color": "#393f4b"
            });
            json._state.graphics.push({
                "type": "text",
                "x": xLabel,
                "y": yPositionBenchmarkValue + dyLabel,
                "align": align,
                "baseline": baseline,
                "fontWeight": "bold",
                "fontSize": "chart",
                "color": "#393f4b",
                "text": json.labelBenchmark
            });
        }
        // Portfolio
        {
            var baseline = (json.benchmarkValue < json.portfolioValue) ? "bottom" : "top";
            var align = "left";
            var xLabel = +0.16;
            var dyLabel = 0;
            if (baseline == "top") dyLabel = 0.04;
            else dyLabel = -0.02;
            if (json.portfolioValue < 0.25) {
                xLabel = +1.0;
                align = "right";
            }
            json._state.graphics.push({
                "type": "rule",
                "x1": +1.0,
                "y1": yPositionPortfolioValue,
                "x2": +0.16,
                "y2": yPositionPortfolioValue,
                "color": "#393f4b",
                "strokeWidth": 2.5
            });
            json._state.graphics.push({
                "type": "text",
                "x": xLabel,
                "y": yPositionPortfolioValue + dyLabel,
                "align": align,
                "baseline": baseline,
                "fontWeight": "bold",
                //"fontSize": "chart",
                "fontSize": "0.04",
                "color": "#393f4b",
                "text": json.labelPortfolio
            });
        }
    } else {
        // Just arrow image with label
        json._state.graphics.push({
            "type": "image",
            "x": 0.55,
            "y": yPositionPortfolioValue,
            "width": 0.4,
            "height": 0.165,
            "image": json.imageArrow
        });
        json._state.graphics.push({
            "type": "text",
            "x": 0.33,
            "y": yPositionPortfolioValue + 0.01,
            "align": "left",
            "baseline": "middle",
            "fontWeight": "bold",
            "fontSize": "0.04",
            "color": "white",
            "text": json.labelPortfolio
        }); 
    }
    // Call parent
    Machinata.Reporting.Node["GraphicalCompositionNode"].init(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.MSCIThermometerNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["GraphicalCompositionNode"].draw(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.MSCIThermometerNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["GraphicalCompositionNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};







