
//TODO: register all currencies in the world

/// <summary>
/// Powerful map that will automatically populate a world map with data points.
/// This chart can automatically translate facts with currencies to their geographic
/// locations, taking into account that some currencies (such as EUR) cover multiple
/// disjoint regions.
///
/// This node is designed for use with offline data (static maps). See OnlineMapNode for
/// more interactive version using map tiles.
///
/// The map facts should always have numbers (values) represented by percentages. The resolved format
/// can be customized if wanted, however the ```autoFormatEdgeNumbers``` must be turned off. Facts with 
/// long category texts are not supported.
/// 
/// ## Maps
/// The map node ships out-of-the-box with all country 110m borders and an
/// extensive list of world properties (such as currencies, names, latlons).
/// You must match facts with this database using the fact ```mapKey``` (or
/// the fact category) and the node setting ```keyProperty```. For example,
/// if the ```keyProperty``` is set to ```currency```, then facts should have a 
/// ```mapKey``` (or fact category) that matches currencies.
/// 
/// The following maps are supported:
/// 
/// ### World Map
///
///  - Map id: ```world``` (for ```map``` node setting)
///  - Default keyProperty: ```currency```
///
/// #### Properties
/// The following properties in ```keyProperty``` can be used:
///  - ```currency```: "XYZ" (ISO 4217)
///  - ```iso_n3```: 756,
///  - ```sovereignt```: "Switzerland",
///  - ```sov_a3```: "CHE",
///  - ```admin```: "Switzerland",
///  - ```adm0_a3```: "CHE",
///  - ```geounit```: "Switzerland",
///  - ```gu_a3```: "CHE",
///  - ```subunit```: "Switzerland",
///  - ```su_a3```: "CHE",
///  - ```name```: "Switzerland",
///  - ```name_long```: "Switzerland",
///  - ```brk_a3```: "CHE",
///  - ```brk_name```: "Switzerland",
///  - ```abbrev```: "Switz.",
///  - ```postal```: "CH",
///  - ```formal_en```: "Swiss Confederation",
///  - ```iso_a2```: "CH",
///  - ```iso_a3```: "CHE",
///  - ```un_a3```: 756,
///  - ```wb_a2```: "CH",
///  - ```wb_a3```: "CHE",
///  - ```adm0_a3_is```: "CHE",
///  - ```adm0_a3_us```: "CHE",
///  - ```continent```: "Europe",
///  - ```region_un```: "Europe",
///  - ```subregion```: "Western Europe",
///  - ```region_wb```: "Europe & Central Asia"
///
/// The following have special attached virtual layouts:
///  - ```currency```
///  - ```continent```
/// 
/// ### Switzerland Map
///
///  - Map id: ```switzerland``` (for ```map``` node setting)
///  - Default keyProperty: ```canton```
///
/// #### Properties
/// The following properties in ```keyProperty``` can be used:
///  - ```title```: "Zurich"
///  - ```canton```: "ZH",
///  - ```id```: 1
/// 
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.OfflineMapNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults = {};

/// <summary>
/// Maps should always be on a light chrome.
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.chrome = "light";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_4x2, Machinata.Reporting.Layouts.BLOCK_2x2];

/// <summary>
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.supportsToolbar = true;

/// <summary>
/// Maps don't have legends.
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.insertLegend = false;

/// <summary>
/// If set to ```true```, flags are shown for each fact.
/// By default ```true```
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.showFlags = true;

/// <summary>
/// Defines the color theme to use for the territories which have underlying facts.
/// By default ```infographic```
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.territoryTheme = "infographic";

/// <summary>
/// If ```true```, territories opacity is set using the underlying fact val.
/// The shade```territoryOpacityFillShade``` is used as a basis color, with
/// the opacity applied via the fact value on a scale from ```territoryOpacityMinValue``` to ```1.0``` (across the domain of all the fact values).
/// By default ```false```
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.territoryOpacityUsesFactVal = false;

/// <summary>
/// If ```territoryOpacityUsesFactVal``` is true, then this defines the minimum opacity to display.
/// By default ```0.1``` (10%)
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.territoryOpacityMinValue = 0.1;

/// <summary>
/// If ```territoryOpacityUsesFactVal``` is true, then this defines which color shade to use.
/// By default ```gray4```
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.territoryOpacityFillShade = "gray4";

/// <summary>
/// Multiplier used to make all flags higher or shorter.
/// By default ```1.0```
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.flagPoleSize = 1.0;

/// <summary>
/// Multiplier used to make all flags wider.
/// By default ```1.0```
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.flagWidth = 1.0;

/// <summary>
/// If a fact value cannot be looked up or mapped to a geographical location (such as 'Others'),
/// this is used.
/// By default this is provided by the map settings (see Machinata.Reporting.Node.OfflineMapNode.defaults.map).
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.fallbackLatitude = null;

/// <summary>
/// If a fact value cannot be looked up or mapped to a geographical location (such as 'Others'),
/// this is used.
/// By default this is provided by the map settings (see Machinata.Reporting.Node.OfflineMapNode.defaults.map).
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.fallbackLongitude = null;

/// <summary>
/// The ```projection``` object is optional and defines how the world
/// projection is done. If not projection is defined, then a western-centered mercator projection
/// is used.
/// 
/// ## Projection JSON
/// - ```type```: defines the projection type. Either ```mercator``` or ```orthographic``` (string)
/// - ```scale```: defines the projection scale (size) (int). This alters the underlying projection scale, which can be interpreted differently by each projection type. Set the scale to 'auto' to have the scale automatically set so that the projection fits (more or less)
/// - ```zoom```: defines the projection zoom (size), relative to the scale. This is typically much easier to set than using the scale. At ```1.0``` with scale set to ```auto```, the map should fit nicely in the chart area.
/// - ```center```: defines the center of the map, ie this will translate the map (must be an int array with two values, the first for latitude -180 to 180 and the second for longitude -90 to 90) (int[])
/// </summary>
/// <example>
/// ```
/// "projection": {
///     "type": "orthographic",
///     "scale": "auto",
///     "zoom": 1.0,
///     "center": [0, 20]
/// }
/// ```
/// </example>
Machinata.Reporting.Node.OfflineMapNode.defaults.projection = null;

/// <summary>
/// The ```facts``` object is required and defines the data points to display.
/// This property is bound into vega as a dataset.
///
/// The facts are highly pre-processed within vega and organized to automatically assign
/// a discrete color from the ```infographic``` theme as well as adjust the flag banner size.
///
/// Facts that can't be matched to a corresponding location on the map (such as a category 'Others') are placed 
/// on ```fallbackLatitude``` and ```fallbackLongitude```.
/// 
/// ## Fact JSON
/// - ```mapKey```: serves to pinpoint where on the map the fact should be placed. The resolved category should always match a corresponding ```keyProperty``` in the country datase.  If wanting to display currency categories on a map, make sure the this is a ISO 4217 upper-case currency, and the node setting ```keyProperty``` is set to ```currency```.
/// - ```val```: the value of the data point (float/int)
/// - ```category```: translatable text that labels the data point. If no ```mapKey``` is provided, this resolved value is used as a fallback.
/// - ```resolved```: the resolved value to display (string)
/// - ```latlon```: optional float[] with two numbers latitude and longitude (for example, ```"latlon": [7.971241, -130.341857]```. If defined, this will fix the data point marker to this location.
/// </summary>
/// <example>
/// ### Automatic Position with keyProperty set to 'currency'
/// ```
/// {
///     "val": 0.22,
///     "category": { "resolved": "EUR" },
///     "mapKey": "EUR",
///     "resolved": "22%"
/// }
/// ```
/// ### Automatic Position with keyProperty set to 'iso_a2'
/// ```
/// {
///     "val": 0.22,
///     "category": { "resolved": "Germany" },
///     "mapKey": "DE",
///     "resolved": "22%"
/// }
/// ```
/// ### Automatic Position with keyProperty set to 'currency' and fallback to resolved category
/// ```
/// {
///     "val": 0.22,
///     "category": { "resolved": "EUR" },
///     "resolved": "22%"
/// }
/// ```
/// ### Automatic Position with keyProperty set to 'iso_a2' and fallback to resolved category
/// ```
/// {
///     "val": 0.22,
///     "category": { "resolved": "DE" },
///     "resolved": "22%"
/// }
/// ```
/// ### Custom Position
/// ```
/// {
///     "val": 0.72,
///     "category": { "resolved": "Pacific" },
///     "resolved": "72%",
///     "latlon": [7.971241, -130.341857]
/// }
/// ```
/// </example>
Machinata.Reporting.Node.OfflineMapNode.defaults.facts = null;

/// <summary>
/// Defines which map to use.
/// 
/// ## Maps Available
///  - ```world```
///  - ```switzerland```
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.map = "world";

/// <summary>
/// Defines which feature to use for the defined map. If ```null```, the feature will automatically
/// be set by the map type (for example for ```world```, the features will be ```countries```).
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.features = null;

/// <summary>
/// Defines the lookup property used to resolve a fact's mapKey (or category value) to its topojson feature. 
/// The default is ```null```, which lets the map settings automatically select the proper keyProperty.
/// 
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.keyProperty = null;

/// <summary>
/// If enabled, if the fact is between -1% and 1% (very small) or greater than 99.5% (very large, but not 100%),
/// we use the facts full formatting, otherwise we use a standard full number percent format
/// that is simplified (ie 43% instead of 43.04%)...
/// For this setting to work, the fact format specified must be of type percent (ie '.2%').
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.automaticallyFormatEdgeNumbers = true;

/// <summary>
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.automaticallySetFlagMinWidth = true;

/// <summary>
/// If not ```null```, will specify the label size for the flags and thus all derived dimensions.
/// By default flag labels will use the standard text label size.
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.defaults.labelSize = null;

/// <summary>
/// </summary>
Machinata.Reporting.Node.OfflineMapNode.getVegaSpec = function (instance, config, json, nodeElem) {
    // Initialize some shortcuts for all the different dimensions we need
    // All dimensions are based off of the flag value label font size
    // Note: these have been ported to signals...
    //var flagTextSize = json.labelSize;
    //if (flagTextSize == null || flagTextSize == "auto") flagTextSize = config.labelSize;
    //var flagTextPadding = Math.round(flagTextSize * 0.1);
    //var flagHeight = flagTextPadding + flagTextSize + flagTextPadding;
    //var flagLabelTextSize = flagHeight;
    //var flagPoleHeight = flagHeight * 3.0;
    //var flagMinWidth = flagHeight * 1.5;
    //var flagMaxWidth = flagHeight * 5.5 * json.flagWidth;


    // Normalize facts to rounded percentages, but only if fact is very edge of boundaries...
    if (json.automaticallyFormatEdgeNumbers == true) {
        Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatEdgeNumbers(json.facts);
    }

    // Validate flagMinWidth
    var flagMinWidthCalculated = null;
    if (json.automaticallySetFlagMinWidth == true) {
        flagMinWidthCalculated = 0;
        var flagTextSize = json.labelSize;
        if (flagTextSize == null || flagTextSize == "auto") flagTextSize = config.labelSize;
        for (var i = 0; i < json.facts.length; i++) {
            var fact = json.facts[i];
            var label = fact.resolved;
            var approxLen = Machinata.Reporting.Tools.measureStringApproximately(config, label,flagTextSize);
            if (approxLen > flagMinWidthCalculated) flagMinWidthCalculated = approxLen;
        }
        flagMinWidthCalculated = Math.round(flagMinWidthCalculated*0.9);
    }

    // Get the map default settings (and features/properties) from the TOPOJSON dictionary of known maps...
    var mapDefaultSettings = Machinata.Reporting.Node.OfflineMapNode.TOPOJSON_MAPS[json.map];
    if (mapDefaultSettings == null) {
        console.warn("Machinata.Reporting.Node.OfflineMapNode.getVegaSpec: map '+json.map+' is not valid!");
    }

    // Create a local copy to use to ensure we don't corrupt the default settings (dont use clone or merge due to perforamnce on features)
    var mapSettings = {
        features: mapDefaultSettings.features,
        properties: mapDefaultSettings.properties,
        featuresFeature: mapDefaultSettings.featuresFeature,
        featuresIdKey: mapDefaultSettings.featuresIdKey,
        factToPropertiesKey: mapDefaultSettings.factToPropertiesKey,
        factFallbackCoordinates: mapDefaultSettings.factFallbackCoordinates,
        propertiesIdKey: mapDefaultSettings.propertiesIdKey,
        projectionWidth: mapDefaultSettings.projectionWidth,
        projectionHeight: mapDefaultSettings.projectionHeight,
        projectionZeroScale: mapDefaultSettings.projectionZeroScale,
        projectionType: mapDefaultSettings.projectionType,
        projectionScale: mapDefaultSettings.projectionScale,
        projectionZoom: mapDefaultSettings.projectionZoom,
        projectionCenter: mapDefaultSettings.projectionCenter,
    };
    if (json.projection != null) {
        if (json.projection.type != null) mapSettings.projectionType = json.projection.type;
        if (json.projection.scale != null) mapSettings.projectionScale = json.projection.scale;
        if (json.projection.zoom != null) mapSettings.projectionZoom = json.projection.zoom;
        if (json.projection.center != null) mapSettings.projectionCenter = json.projection.center;
    }
    if (json.keyProperty != null) {
        mapSettings.factToPropertiesKey = json.keyProperty;
    }
    if (json.fallbackLatitude != null && json.fallbackLongitude != null) {
        mapSettings.factFallbackCoordinates = [json.fallbackLatitude, json.fallbackLongitude];
    }

    // Round everything
    //flagTextPadding = Math.round(flagTextPadding);
    //flagLabelTextSize = Math.round(flagLabelTextSize);
    //flagPoleHeight = Math.round(flagPoleHeight);
    //flagMinWidth = Math.round(flagMinWidth);
    //flagMaxWidth = Math.round(flagMaxWidth);


    return {
        "data": [

          {
              "name": "facts",
              "values": null
          },
          {
              "name": "worldProperties",
              /*"_comment": "Provides country properties for the TOPOJSON features. These can be joined on the key 'iso_a2', for example.",*/
              "values": mapSettings.properties
          },
          {
              "name": "factsResolved",
              "descriptions": "Resolves the facts somewhat based on our incoming data",
              "source": "facts",
              "transform": [
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "key",
                      "expr": "datum.mapKey != null ? datum.mapKey : datum.category.resolved"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "categoryResolved",
                    "expr": "datum.category.resolved"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "valResolved",
                    "expr": "datum.resolved"
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "formatType",
                    "expr": "datum.formatType ? datum.formatTyped : 'number'" // default
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "format",
                    "expr": "datum.format ? datum.format : '.1%'" // default
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "tooltipResolved",
                    "expr": "datum.categoryResolved + ': ' + datum.valResolved + (datum.link != null && datum.link.tooltip ? '[break]'+datum.link.tooltip.resolved : '')" // default
                  },
              ]
          },
          {
              "name": "factsResolvedWithISON3",
              /*"_comment": "factsResolved joined with worldProperties so we now the iso_n3 and centroidOffset...",*/
              "source": "factsResolved",
              "transform": [
                  {
                      // Only show facts with the flag setup
                      "type": "filter", "expr": "datum.showFlag != false"
                },
                {
                    /*"_comment": "Join to the properties on key == currency. We need the properties in order to get the iso_n3, which the 110M_Features only contains.",*/
                    "type": "lookup",
                    "from": "worldProperties",
                    "key": mapSettings.factToPropertiesKey,
                    "fields": ["key"],
                    "values": ["iso_n3", "latlon", "flagHeightMultiplier"],
                    "as":     ["iso_n3", "latlonDefault", "flagHeightMultiplier"]
                },
                {
                    /*"_comment": "If there is no lat lon registered, then make sure at least we have a default to fallback",*/
                    "type": "formula",
                    "initonly": true,
                    "as": "latlonResolved",
                    "expr": "datum.latlon ? datum.latlon : (datum.latlonDefault ? datum.latlonDefault : [" + mapSettings.factFallbackCoordinates[0] + "," + mapSettings.factFallbackCoordinates[1]+"])" // default
                },
                {
                    "type": "formula",
                    "expr": "datum.latlonResolved[0]",
                    "as": "lat"
                },
                {
                    "type": "formula",
                    "expr": "datum.latlonResolved[1]",
                    "as": "lon"
                },
                {
                    "type": "geopoint",
                    "projection": "projection",
                    "fields": ["lon", "lat"]
                },
                {
                    "type": "formula",
                    "expr": "round((flagPoleHeight * " + json.flagPoleSize + " * (datum.flagHeightMultiplier ? datum.flagHeightMultiplier : 1)) * flagPoleHeightMultiplierForSmallCharts)",
                    "as": "flagPoleHeightResolved"
                }
              ]
          },
          {
              "name": "factsExploded",
              /*"_comment": "This is actually worldProperties joined with factsResolved so that we have the inverse relation of each matching worldProperty for each fact key. This allows us to match multiple features in the end for each fact (for example, EUR will match multiple countries!)",*/
              "source": "worldProperties",
              "transform": [
                {
                    /*"_comment": "Join to the properties on key == json.keyProperty (currency for example). We need the properties in order to get the iso_n3, which the 110M_Features only contains.",*/
                    "type": "lookup",
                    "from": "factsResolved",
                    "key": "key",
                    "fields": [mapSettings.factToPropertiesKey],
                    "values": ["val", "key", "valResolved", "categoryResolved", "tooltipResolved", "rankResolved", "link"]
                },
                { "type": "filter", "expr": "datum.key != null" }
              ]
          },
          {
              /*"_comment": "These are the 110M TOPOJSON features (shapes). We have country outlines here which we select. The id of each feature is according to ISO-N3",*/
              "name": "worldFeatures",
              "values": mapSettings.features,
              "format": {
                  "type": "topojson",
                  "feature": mapSettings.featuresFeature
              },
          },
          {
              /*"_comment": "These are the 110M TOPOJSON features (shapes). We have country outlines here which we select.",*/
              "name": "worldFeaturesExplodedFacts",
              "source": "worldFeatures",
              "transform": [
                  {
                      /*"_comment": "Join on our facts",*/
                      "type": "lookup",
                      "from": "factsExploded",
                      "key": mapSettings.propertiesIdKey,
                      "fields": [mapSettings.featuresIdKey],
                      "values": ["val", "key", "valResolved", "categoryResolved", "tooltipResolved", "rankResolved", "iso_a2" , "link"]
                  },
              ]
          },
        ],
        "signals": [
            {
                "name": "flagTextSize",
                "update": json.labelSize ? json.labelSize : "config_labelSize"
            },
            {
                "name": "flagTextPadding",
                "update": "round(flagTextSize * 0.1)"
            },
            {
                "name": "flagWidth",
                "update": json.flagWidth
            },
            {
                "name": "flagHeight",
                "update": "flagTextPadding + flagTextSize + flagTextPadding"
            },
            {
                "name": "flagLabelTextSize",
                "update": "round(flagHeight)"
            },
            {
                "name": "flagPoleHeight",
                "update": "round(flagHeight * 3.0)"
            },
            {
                "name": "flagMinWidth",
                "update": flagMinWidthCalculated != null ? flagMinWidthCalculated : "round(flagHeight * 1.5)"
            },
            {
                "name": "flagMaxWidth",
                "update": "round(flagHeight * 5.5 * flagWidth)"
            },

            {
                "name": "flagWidthMultiplierForSmallCharts",
                "update": "isLayoutedOnHalfMediumWidth ? 0.5 : 1.0"
            },
            {
                "name": "flagPoleHeightMultiplierForSmallCharts",
                "update": "isLayoutedOnHalfMediumWidth ? 0.7 : 1.0"
            },
            {
                // The range is the flagMinWidth to flagMaxWidth*factor, but at least flagMinWidth to flagMinWidth
                "name": "flagWidthRange",
                "update": "[flagMinWidth, max(flagMinWidth,flagMaxWidth*flagWidthMultiplierForSmallCharts)]"
            },
            {
                "name": "projectionTypeFromData",
                "init": "'" + mapSettings.projectionType + "'"
            },
            {
                "name": "projectionScaleFromData",
                "init": mapSettings.projectionScale == "auto" ? "'auto'" : mapSettings.projectionScale
            },
            {
                "name": "projectionZoomFromData",
                "init": mapSettings.projectionZoom
            },
            {
                "name": "chartSize",
                "update": "min(width,height)"
            },
            {
                "name": "projectionWidth",
                "update": "projectionTypeFromData == 'mercator' ? " + mapSettings.projectionWidth+" : 500"
            },
            {
                "name": "projectionHeight",
                "update": "projectionTypeFromData == 'mercator' ? " + mapSettings.projectionHeight +" : 500"
            },
            {
                "name": "projectionZeroScale",
                "update": "projectionTypeFromData == 'mercator' ? " + mapSettings.projectionZeroScale+" : 100"
            },
            {
                "name": "projectionAR",
                "update": "projectionWidth/projectionHeight" // < 1.0 is portrait, > 1.0 is landscape
            },
            {
                "name": "chartAR",
                "update": "width/height" // < 1.0 is portrait, > 1.0 is landscape
            },
            {
                "name": "projectionResizeFactor",
                "update": "(projectionAR >= chartAR ? (width/projectionWidth) : (height/projectionHeight))"
            },
            {
                "name": "projectionScaleAuto",
                "update": "projectionResizeFactor * projectionZeroScale"
            },
            {
                "name": "projectionScale",
                "update": "projectionScaleFromData == 'auto' ? projectionScaleAuto : projectionScaleFromData"
            },
            {
                "name": "projectionCenter",
                "init": mapSettings.projectionCenter
            },
            {
                "name": "projectionTranslate",
                "update": "[width / 2, height / 2]"
            },


            // Events
            {
                "name": "featureClick",
                "on": [
                    { "events": "shape:click", "update": "null" } // we handle it outside of vega in onMarkClick
                ]
            }


        ],
        "projections": [
            {
                "name": "projection",
                "type": { "signal": "projectionTypeFromData" },
                "scale": { "signal": "projectionScale * projectionZoomFromData" },
                //"rotate": [{ "signal": "rotateX" }, 0, 0],
                "center": { "signal": "projectionCenter" },
                "translate": { "signal": "projectionTranslate" },
                "clipExtent": { "signal": "[[0, 0], [width, height]]" }
            }
        ],
        "scales": [
            {
                "name": "colorFill",
                "type": "quantize",
                "domain": { "data": "factsResolved", "field": "rankResolved" },
                "range": { "scheme": json.territoryTheme + "-bg" },
                "reverse": true
            },
            {
                "name": "opacityFill",
                "type": "linear",
                "domain": { "data": "factsResolved", "field": "val" },
                "range": [json.territoryOpacityMinValue,1]
            },
            {
                "name": "flagWidth",
                "type": "linear",
                "domain": { "data": "factsResolved", "field": "val" },
                "range": { "signal":"flagWidthRange"}
            }
        ],
        "marks": [
            {
                "clip": true, // makes sure that when we scale it doesnt push open the chart size
                "type": "group",
                "encode": {
                    "update": {
                        "width": { "signal": "width" },
                        "height": { "signal": "height" }
                    }
                },
                "marks": [

                    {
                        /*"_comment": "These are the country outlines and shapes",*/
                        "type": "shape",
                        "from": { "data": "worldFeaturesExplodedFacts" },
                        "encode": {
                            "enter": {
                                "strokeWidth": { "value": config.lineSize },
                                "stroke": { "value": "white" },
                                //debug:"fill": { "signal": "datum.val != null ? 'red' : 'gray'" },
                                "fill":
                                    json.territoryOpacityUsesFactVal == true ?
                                        { "signal": "scale('theme-gray-bg','" + json.territoryOpacityFillShade +"')"}
                                          :
                                        { "signal": "datum.val != null ? scale('colorFill',datum.rankResolved) : scale('theme-gray-bg','gray1')" },
                                "opacity":
                                    json.territoryOpacityUsesFactVal == true ?
                                        { "signal": "datum.val == null ? " + json.territoryOpacityMinValue + " : scale('opacityFill',datum.val)" }
                                        :
                                        { "value": 1 },
                                "tooltip": { "signal": "datum.tooltipResolved" + (Machinata.Reporting.isDebugEnabled() ? " + '[break]iso_n3='+datum.id + '[break]iso_a2='+datum.iso_a2 + '[break]scale='+projectionScale+'[break]rankResolved='+datum.rankResolved + '[break]val='+datum.val" : "") },
                                "cursor": { "signal": "datum.link != null ? 'pointer' : null"},
                            },
                            "update": {
                                "width": { "signal": "width" },
                                "height": { "signal": "height" },
                            }
                        },
                        "transform": [
                            { "type": "geoshape", "projection": "projection" }
                        ]
                    },
                    (json.showFlags != true) ? { "type": "group"} : {
                        "type": "group",
                        /*"_comment": "This is the first (FLAG POLE) of two master groups for the flag symbols. There are two groups due to the overlapping nature that works much better this way (ie poles never overlap flags/labels)",*/
                        "from": {
                            "facet": {
                                "data": "factsResolvedWithISON3",
                                "name": "factsResolvedWithISON3Faceted",
                                "groupby": "key"
                            }
                        },

                        "marks": [

                            {
                                "type": "group",
                                /*"_comment": "This is the group for the flag symbol. It is translated for the projection and center of the feature (times offset, if any)",*/
                                "from": {
                                    "data": "factsResolvedWithISON3Faceted"
                                },
                                "encode": {
                                    "enter": {
                                        "cursor": { "signal": "datum.link != null ? 'pointer' : null" },
                                    },
                                    "update": {
                                        "x": { "signal": "round(datum.x)" }, // uses the values calculated by the geoshape transform
                                        "y": { "signal": "round(datum.y)" }
                                    }
                                },
                                "marks": [
                                    {
                                        /*"_comment": "This is the FLAGPOLE",*/
                                        "from": {
                                            "data": "factsResolvedWithISON3Faceted"
                                        },
                                        "type": "rect",
                                        "encode": {
                                            "enter": {
                                                "x": { "value": 0 },
                                                "width": { "value": config.lineSize },
                                                "fill": { "value": "black" }
                                            },
                                            "update": {
                                                "y": { "signal": "-datum.flagPoleHeightResolved" },
                                                "height": { "signal": "datum.flagPoleHeightResolved" },
                                            }
                                        }
                                    }
                                ]
                            },
                        ]
                    },
                    (json.showFlags != true) ? { "type": "group" } : {
                        "type": "group",
                        /*"_comment": "This is the second (FLAG + LABELS) of two master groups for the flag symbols. There are two groups due to the overlapping nature that works much better this way (ie poles never overlap flags/labels)",*/
                        "from": {
                            "facet": {
                                "data": "factsResolvedWithISON3",
                                "name": "factsResolvedWithISON3Faceted",
                                "groupby": "key"
                            }
                        },
                        "marks": [
                            {
                                "type": "group",
                                /*"_comment": "This is the master group for the flag symbol. It is translated for the projection and center of the feature (times offset, if any)",*/
                                "from": {
                                    "data": "factsResolvedWithISON3Faceted"
                                },
                                "encode": {
                                    "update": {
                                        "x": { "signal": "round(datum.x)" }, // uses the values calculated by the geoshape transform
                                        "y": { "signal": "round(datum.y)" }
                                    }
                                },
                                "marks": [
                                    {
                                        /*"_comment": "This is the FLAG",*/
                                        "from": {
                                            "data": "factsResolvedWithISON3Faceted"
                                        },
                                        "type": "rect",
                                        "encode": {
                                            "enter": {
                                                "x": { "value": 0 },
                                                "fill": { "signal": "scale('theme-bg','mid')" },
                                                "tooltip": { "field": "tooltipResolved" },
                                                "cursor": { "signal": "datum.link != null ? 'pointer' : null" },
                                            },
                                            "update": {
                                                "y": { "signal": "-datum.flagPoleHeightResolved" }, // -1 is for font problems
                                                "width": { "signal": "round(scale('flagWidth',datum.val))" },
                                                "height": { "signal": "flagHeight" },
                                            }
                                        }
                                    },
                                    {
                                        /*"_comment": "This is the VAL LABEL",*/
                                        "from": {
                                            "data": "factsResolvedWithISON3Faceted"
                                        },
                                        "type": "text",
                                        "encode": {
                                            "enter": {
                                                "interactive": false,
                                                "fill": { "signal": "scale('theme-fg','mid')" },
                                                "baseline": { "value": "middle" },
                                                "x": { "value": 0 },
                                                "text": { "field": "valResolved" },
                                                "tooltip": { "field": "tooltipResolved" },
                                                "cursor": { "signal": "datum.link != null ? 'pointer' : null" },
                                            },
                                            "update": {
                                                "dx": { "signal": "flagTextPadding" },
                                                "fontSize": { "signal": "flagTextSize" },
                                                "y": { "signal": "-datum.flagPoleHeightResolved + (flagHeight / 2)" },
                                            }
                                        }
                                    },
                                    {
                                        /*"_comment": "This is the CATEGORY LABEL",*/
                                        "from": {
                                            "data": "factsResolvedWithISON3Faceted"
                                        },
                                        "type": "text",
                                        "encode": {
                                            "enter": {
                                                "interactive": false,
                                                "align": { "value": "right" },
                                                "baseline": { "value": "middle" },
                                                "x": { "value": 0 },
                                                "text": { "field": "categoryResolved" },
                                                "cursor": { "signal": "datum.link != null ? 'pointer' : null" },
                                            },
                                            "update": {
                                                "fontSize": { "signal": "flagLabelTextSize" },
                                                "dx": { "signal": "-flagTextPadding*2" },
                                                "y": { "signal": "-datum.flagPoleHeightResolved + (flagHeight / 2)" },
                                            }
                                        }
                                    }
                                ]
                            },
                        ]
                    },
                ]
            }
        ]
    };
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.OfflineMapNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    spec["data"][0].values = json.facts;
};



/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.OfflineMapNode.init = function (instance, config, json, nodeElem) {
    
    /*
    // Automatic region?
    if (json.keyProperty == "region") {
        json.keyProperty = "region_wb"; //TODO: migrate to own property!
        if (json.facts != null) {
            for (var i = 0; i < json.facts.length; i++) {
                var fact = json.facts[i];
                if (fact.category.resolved == "EMEA") {
                    fact.mapKey = "Europe & Central Asia";
                    fact.latlon = [44.266725, 17.879451];
                } else if (fact.category.resolved == "America") {
                    fact.mapKey = "North America";
                    fact.latlon = [37.988251, -103.241359];
                } else if (fact.category.resolved == "Africa") {
                    fact.mapKey = "Sub-Saharan Africa";
                    fact.latlon = [2.836701, 19.934625];
                } else if (fact.category.resolved == "Asia") {
                    fact.mapKey = "East Asia & Pacific";
                    fact.latlon = [25.574377, 105.438416];
                } else if (fact.category.resolved == "Oceania") {
                    fact.mapKey = "Oceania";
                    fact.latlon = [-26.317768, 89.172179];
                } else if (fact.category.resolved == "Other") {
                    fact.mapKey = "Other";
                    fact.latlon = [-14.018309, -119.910662];
                }
            }
        }
    }*/

    // Automatic continent shortcut for world map
    //TODO: temporary for ESG report
    if (json.map == "world" && json.keyProperty == "continent") {
        json.keyProperty = "continent"; //TODO: migrate to own property!
        if (json.facts != null) {
            for (var i = 0; i < json.facts.length; i++) {
                var fact = json.facts[i];
                if (fact.category.resolved == "Europe") {
                    fact.mapKey = "Europe";
                    fact.latlon = [44.875712, 5.006955];
                } else if (fact.category.resolved == "North America") {
                    fact.mapKey = "North America";
                    fact.latlon = [34.559544, -95.248948];
                } else if (fact.category.resolved == "South America") {
                    fact.mapKey = "South America";
                    fact.latlon = [-22.422489, -62.743144];
                } else if (fact.category.resolved == "Africa") {
                    fact.mapKey = "Africa";
                    fact.latlon = [1.987171, 20.565401];
                } else if (fact.category.resolved == "Asia") {
                    fact.mapKey = "Asia";
                    fact.latlon = [26.293407, 84.842430];
                } else if (fact.category.resolved == "Oceania" || fact.category.resolved == "Australia/Oceania") {
                    fact.mapKey = "Oceania";
                    fact.latlon = [-26.317768, 89.172179];
                } else if (fact.category.resolved == "Other") {
                    fact.mapKey = "Other";
                    fact.latlon = [-44.018309, -119.910662];
                }
            }
        }
    }

    // Auto rank facts
    // This looks rather complicated, but its actually simple: 
    // for each fact unique value we need a rank for the value (in order)
    // This is all we do here... We find all the unique values, then sort them, then 
    // assign the index of the sort results to each fact's value.
    // We need this so that we can create a good shading system. If we were to just
    // shade based on a scale, then any fact with a large weight (like 50%) will push all
    // facts with lower numbers out of the scale to basically one flat color... This way with 
    // the ranking system we distribute the color no matter how the numbers are...
    if (true) {
        var valDictionary = {};
        var valArray =[];
        for (var i = 0; i < json.facts.length; i++) {
            var fact = json.facts[i];
            if (valDictionary[fact.val] == null) valArray.push(fact.val);
            valDictionary[fact.val] = 0;
        }
        valArray.sort(function (a, b) {
            return a - b;
        });
        for (var i = 0; i < Object.keys(valDictionary).length; i++) {
            var key = Object.keys(valDictionary)[i];
            valDictionary[key] = valArray.indexOf(parseFloat(key));
        }
        for (var i = 0; i < json.facts.length; i++) {
            var fact = json.facts[i];
            fact.rankResolved = valDictionary[fact.val];
        }
    }

    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.OfflineMapNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.OfflineMapNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.OfflineMapNode.onMarkClick = function (instance, config, json, nodeElem, event, item) {
    // If the item has a link json attached to it, we "open" it
    if (item != null && item.datum != null && item.datum.link != null) {
        Machinata.Reporting.Tools.openLink(instance, item.datum.link);
    }
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.OfflineMapNode.exportExcelFormat = function (instance, config, json, nodeElem, format, filename) {
    var workbook = Machinata.Reporting.Export.createExcelWorkbook(instance, config, json);
    workbook = Machinata.Reporting.Export.createExcelDataForCategoryFacts(instance, config, json, workbook, format);
    Machinata.Reporting.Export.saveExcelWorkbook(instance, config, json, workbook, filename);
    return true;
};



/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.OfflineMapNode.detectMissingCurrencies = function () {
    // via https://www.worlddata.info/currencies/
    var currencies = ["AED", "AFN", "ALL", "AMD", "ANG", "AOA", "ARS", "AUD", "AWG", "AZN", "BAM", "BBD", "BDT", "BGN", "BHD", "BIF", "BMD", "BND", "BOB", "BRL", "BSD", "BTN", "BWP", "BYR", "BZD", "CAD", "CDF", "CHF", "CKD", "CLP", "CNY", "COP", "CRC", "CUP", "CVE", "CZK", "DJF", "DKK", "DOP", "DZD", "EGP", "ERN", "ETB", "FJD", "FKP", "FOK", "GBP", "GEL", "GGP", "GHS", "GIP", "GMD", "GNF", "GTQ", "GYD", "HKD", "HNL", "HTG", "HUF", "IDR", "ILS", "IMP", "INR", "IQD", "IRR", "ISK", "JEP", "JMD", "JOD", "JPY", "KES", "KGS", "KHR", "KID", "KMF", "KPW", "KRW", "KWD", "KYD", "KZT", "LAK", "LBP", "LKR", "LRD", "LSL", "LYD", "MAD", "MDL", "MGA", "MKD", "MMK", "MNT", "MOP", "MRO", "MUR", "MVR", "MWK", "MXN", "MYR", "MZN", "NAD", "NGN", "NIO", "NOK", "NPR", "NZD", "OMR", "PAB", "PEN", "PGK", "PHP", "PKR", "PLN", "PYG", "QAR", "RON", "RSD", "RUB", "RWF", "SAR", "SBD", "SCR", "SDG", "SEK", "SGD", "SHP", "SLL", "SOS", "SRD", "SSP", "STD", "SYP", "SZL", "THB", "TJS", "TMT", "TND", "TOP", "TRY", "TTD", "TVD", "TWD", "TZS", "UAH", "UGX", "USD", "UYU", "UZS", "VND", "VUV", "WST", "XAF", "XCD", "XOF", "XPF", "YER", "ZAR", "ZMW", "ZWL"];
    var props = Machinata.Reporting.Node.OfflineMapNode.TOPOJSON_110M_PROPERTIES;
    var missingCurrencies = "";
    for (var c = 0; c < currencies.length; c++) {
        var currency = currencies[c];
        var currencyFound = false;
        for (var p = 0; p < props.length; p++) {
            var prop = props[p];
            if (prop.currency == currency) currencyFound = true;
        }
        if (currencyFound == false) {
            console.warn("Could not find currency", currency);
            missingCurrencies += currency;
            missingCurrencies += "\n";
        }
    }
    console.warn("Missing currencies:");
    console.warn(missingCurrencies);
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.OfflineMapNode.TOPOJSON_110M_PROPERTIES = [
   {
       "iso_n3": -99,
       "sovereignt": "Kosovo",
       "sov_a3": "KOS",
       "admin": "Kosovo",
       "adm0_a3": "KOS",
       "geounit": "Kosovo",
       "gu_a3": "KOS",
       "subunit": "Kosovo",
       "su_a3": "KOS",
       "name": "Kosovo",
       "name_long": "Kosovo",
       "brk_a3": "B57",
       "brk_name": "Kosovo",
       "abbrev": "Kos.",
       "postal": "KO",
       "formal_en": "Republic of Kosovo",
       "iso_a2": "-99",
       "iso_a3": "-99",
       "un_a3": -99,
       "wb_a2": "KV",
       "wb_a3": "KSV",
       "adm0_a3_is": "SRB",
       "adm0_a3_us": "KOS",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Southern Europe",
       "region_wb": "Europe & Central Asia"
   },
   {
       "iso_n3": -99,
       "sovereignt": "Somaliland",
       "sov_a3": "SOL",
       "admin": "Somaliland",
       "adm0_a3": "SOL",
       "geounit": "Somaliland",
       "gu_a3": "SOL",
       "subunit": "Somaliland",
       "su_a3": "SOL",
       "name": "Somaliland",
       "name_long": "Somaliland",
       "brk_a3": "B30",
       "brk_name": "Somaliland",
       "abbrev": "Solnd.",
       "postal": "SL",
       "formal_en": "Republic of Somaliland",
       "iso_a2": "-99",
       "iso_a3": "-99",
       "un_a3": -99,
       "wb_a2": "-99",
       "wb_a3": "-99",
       "adm0_a3_is": "SOM",
       "adm0_a3_us": "SOM",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Eastern Africa",
       "region_wb": "Sub-Saharan Africa"
   },
   {
       "iso_n3": -99,
       "sovereignt": "Northern Cyprus",
       "sov_a3": "CYN",
       "admin": "Northern Cyprus",
       "adm0_a3": "CYN",
       "geounit": "Northern Cyprus",
       "gu_a3": "CYN",
       "subunit": "Northern Cyprus",
       "su_a3": "CYN",
       "name": "N. Cyprus",
       "name_long": "Northern Cyprus",
       "brk_a3": "B20",
       "brk_name": "N. Cyprus",
       "abbrev": "N. Cy.",
       "postal": "CN",
       "formal_en": "Turkish Republic of Northern Cyprus",
       "iso_a2": "-99",
       "iso_a3": "-99",
       "un_a3": -99,
       "wb_a2": "-99",
       "wb_a3": "-99",
       "adm0_a3_is": "CYP",
       "adm0_a3_us": "CYP",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Europe & Central Asia"
   },
   {
       "currency": "AFN",
       "latlon": [
          33,
          65
       ],
       "iso_n3": 4,
       "sovereignt": "Afghanistan",
       "sov_a3": "AFG",
       "admin": "Afghanistan",
       "adm0_a3": "AFG",
       "geounit": "Afghanistan",
       "gu_a3": "AFG",
       "subunit": "Afghanistan",
       "su_a3": "AFG",
       "name": "Afghanistan",
       "name_long": "Afghanistan",
       "brk_a3": "AFG",
       "brk_name": "Afghanistan",
       "abbrev": "Afg.",
       "postal": "AF",
       "formal_en": "Islamic State of Afghanistan",
       "iso_a2": "AF",
       "iso_a3": "AFG",
       "un_a3": 4,
       "wb_a2": "AF",
       "wb_a3": "AFG",
       "adm0_a3_is": "AFG",
       "adm0_a3_us": "AFG",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Southern Asia",
       "region_wb": "South Asia"
   },
   {
       "currency": "AOA",
       "latlon": [
          -12.5,
          18.5
       ],
       "iso_n3": 24,
       "sovereignt": "Angola",
       "sov_a3": "AGO",
       "admin": "Angola",
       "adm0_a3": "AGO",
       "geounit": "Angola",
       "gu_a3": "AGO",
       "subunit": "Angola",
       "su_a3": "AGO",
       "name": "Angola",
       "name_long": "Angola",
       "brk_a3": "AGO",
       "brk_name": "Angola",
       "abbrev": "Ang.",
       "postal": "AO",
       "formal_en": "People's Republic of Angola",
       "iso_a2": "AO",
       "iso_a3": "AGO",
       "un_a3": 24,
       "wb_a2": "AO",
       "wb_a3": "AGO",
       "adm0_a3_is": "AGO",
       "adm0_a3_us": "AGO",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Middle Africa",
       "region_wb": "Sub-Saharan Africa"
   },
   {
       "currency": "ALL",
       "latlon": [
          41,
          20
       ],
       "iso_n3": 8,
       "sovereignt": "Albania",
       "sov_a3": "ALB",
       "admin": "Albania",
       "adm0_a3": "ALB",
       "geounit": "Albania",
       "gu_a3": "ALB",
       "subunit": "Albania",
       "su_a3": "ALB",
       "name": "Albania",
       "name_long": "Albania",
       "brk_a3": "ALB",
       "brk_name": "Albania",
       "abbrev": "Alb.",
       "postal": "AL",
       "formal_en": "Republic of Albania",
       "iso_a2": "AL",
       "iso_a3": "ALB",
       "un_a3": 8,
       "wb_a2": "AL",
       "wb_a3": "ALB",
       "adm0_a3_is": "ALB",
       "adm0_a3_us": "ALB",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Southern Europe",
       "region_wb": "Europe & Central Asia"
   },
   {
       "currency": "AED",
       "latlon": [
          24,
          54
       ],
       "iso_n3": 784,
       "sovereignt": "United Arab Emirates",
       "sov_a3": "ARE",
       "admin": "United Arab Emirates",
       "adm0_a3": "ARE",
       "geounit": "United Arab Emirates",
       "gu_a3": "ARE",
       "subunit": "United Arab Emirates",
       "su_a3": "ARE",
       "name": "United Arab Emirates",
       "name_long": "United Arab Emirates",
       "brk_a3": "ARE",
       "brk_name": "United Arab Emirates",
       "abbrev": "U.A.E.",
       "postal": "AE",
       "formal_en": "United Arab Emirates",
       "iso_a2": "AE",
       "iso_a3": "ARE",
       "un_a3": 784,
       "wb_a2": "AE",
       "wb_a3": "ARE",
       "adm0_a3_is": "ARE",
       "adm0_a3_us": "ARE",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Middle East & North Africa"
   },
   {
       "currency": "ARS",
       "latlon": [
          -34,
          -64
       ],
       "iso_n3": 32,
       "sovereignt": "Argentina",
       "sov_a3": "ARG",
       "admin": "Argentina",
       "adm0_a3": "ARG",
       "geounit": "Argentina",
       "gu_a3": "ARG",
       "subunit": "Argentina",
       "su_a3": "ARG",
       "name": "Argentina",
       "name_long": "Argentina",
       "brk_a3": "ARG",
       "brk_name": "Argentina",
       "abbrev": "Arg.",
       "postal": "AR",
       "formal_en": "Argentine Republic",
       "iso_a2": "AR",
       "iso_a3": "ARG",
       "un_a3": 32,
       "wb_a2": "AR",
       "wb_a3": "ARG",
       "adm0_a3_is": "ARG",
       "adm0_a3_us": "ARG",
       "continent": "South America",
       "region_un": "Americas",
       "subregion": "South America",
       "region_wb": "Latin America & Caribbean"
   },
   {
       "currency": "AMD",
       "latlon": [
          40,
          45
       ],
       "iso_n3": 51,
       "sovereignt": "Armenia",
       "sov_a3": "ARM",
       "admin": "Armenia",
       "adm0_a3": "ARM",
       "geounit": "Armenia",
       "gu_a3": "ARM",
       "subunit": "Armenia",
       "su_a3": "ARM",
       "name": "Armenia",
       "name_long": "Armenia",
       "brk_a3": "ARM",
       "brk_name": "Armenia",
       "abbrev": "Arm.",
       "postal": "ARM",
       "formal_en": "Republic of Armenia",
       "iso_a2": "AM",
       "iso_a3": "ARM",
       "un_a3": 51,
       "wb_a2": "AM",
       "wb_a3": "ARM",
       "adm0_a3_is": "ARM",
       "adm0_a3_us": "ARM",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Europe & Central Asia"
   },
   {
       "latlon": [
          -90,
          0
       ],
       "iso_n3": 10,
       "sovereignt": "Antarctica",
       "sov_a3": "ATA",
       "admin": "Antarctica",
       "adm0_a3": "ATA",
       "geounit": "Antarctica",
       "gu_a3": "ATA",
       "subunit": "Antarctica",
       "su_a3": "ATA",
       "name": "Antarctica",
       "name_long": "Antarctica",
       "brk_a3": "ATA",
       "brk_name": "Antarctica",
       "abbrev": "Ant.",
       "postal": "AQ",
       "formal_en": "",
       "iso_a2": "AQ",
       "iso_a3": "ATA",
       "un_a3": -99,
       "wb_a2": "-99",
       "wb_a3": "-99",
       "adm0_a3_is": "ATA",
       "adm0_a3_us": "ATA",
       "continent": "Antarctica",
       "region_un": "Antarctica",
       "subregion": "Antarctica",
       "region_wb": "Antarctica"
   },
   {
       "_comment": "TODO",
       "iso_n3": 260,
       "sovereignt": "France",
       "sov_a3": "FR1",
       "admin": "French Southern and Antarctic Lands",
       "adm0_a3": "ATF",
       "geounit": "French Southern and Antarctic Lands",
       "gu_a3": "ATF",
       "subunit": "French Southern and Antarctic Lands",
       "su_a3": "ATF",
       "name": "Fr. S. Antarctic Lands",
       "name_long": "French Southern and Antarctic Lands",
       "brk_a3": "ATF",
       "brk_name": "Fr. S. and Antarctic Lands",
       "abbrev": "Fr. S.A.L.",
       "postal": "TF",
       "formal_en": "Territory of the French Southern and Antarctic Lands",
       "iso_a2": "TF",
       "iso_a3": "ATF",
       "un_a3": -99,
       "wb_a2": "-99",
       "wb_a3": "-99",
       "adm0_a3_is": "ATF",
       "adm0_a3_us": "ATF",
       "continent": "Seven seas (open ocean)",
       "region_un": "Seven seas (open ocean)",
       "subregion": "Seven seas (open ocean)",
       "region_wb": "Sub-Saharan Africa",
       "currency": "EUR",
       "latlon": [
          -49.25,
          69.167
       ]
   },
   {
       "currency": "AUD",
       "latlon": [
          -27,
          133
       ],
       "iso_n3": 36,
       "sovereignt": "Australia",
       "sov_a3": "AU1",
       "admin": "Australia",
       "adm0_a3": "AUS",
       "geounit": "Australia",
       "gu_a3": "AUS",
       "subunit": "Australia",
       "su_a3": "AUS",
       "name": "Australia",
       "name_long": "Australia",
       "brk_a3": "AUS",
       "brk_name": "Australia",
       "abbrev": "Auz.",
       "postal": "AU",
       "formal_en": "Commonwealth of Australia",
       "iso_a2": "AU",
       "iso_a3": "AUS",
       "un_a3": 36,
       "wb_a2": "AU",
       "wb_a3": "AUS",
       "adm0_a3_is": "AUS",
       "adm0_a3_us": "AUS",
       "continent": "Oceania",
       "region_un": "Oceania",
       "subregion": "Australia and New Zealand",
       "region_wb": "East Asia & Pacific"
   },
   {
       "iso_n3": 40,
       "sovereignt": "Austria",
       "sov_a3": "AUT",
       "admin": "Austria",
       "adm0_a3": "AUT",
       "geounit": "Austria",
       "gu_a3": "AUT",
       "subunit": "Austria",
       "su_a3": "AUT",
       "name": "Austria",
       "name_long": "Austria",
       "brk_a3": "AUT",
       "brk_name": "Austria",
       "abbrev": "Aust.",
       "postal": "A",
       "formal_en": "Republic of Austria",
       "iso_a2": "AT",
       "iso_a3": "AUT",
       "un_a3": 40,
       "wb_a2": "AT",
       "wb_a3": "AUT",
       "adm0_a3_is": "AUT",
       "adm0_a3_us": "AUT",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Western Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "EUR",
       "latlon": [
          47.33333333,
          13.33333333
       ]
   },
   {
       "iso_n3": 31,
       "sovereignt": "Azerbaijan",
       "sov_a3": "AZE",
       "admin": "Azerbaijan",
       "adm0_a3": "AZE",
       "geounit": "Azerbaijan",
       "gu_a3": "AZE",
       "subunit": "Azerbaijan",
       "su_a3": "AZE",
       "name": "Azerbaijan",
       "name_long": "Azerbaijan",
       "brk_a3": "AZE",
       "brk_name": "Azerbaijan",
       "abbrev": "Aze.",
       "postal": "AZ",
       "formal_en": "Republic of Azerbaijan",
       "iso_a2": "AZ",
       "iso_a3": "AZE",
       "un_a3": 31,
       "wb_a2": "AZ",
       "wb_a3": "AZE",
       "adm0_a3_is": "AZE",
       "adm0_a3_us": "AZE",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Europe & Central Asia",
       "currency": "AZN",
       "latlon": [
          40.5,
          47.5
       ]
   },
   {
       "iso_n3": 108,
       "sovereignt": "Burundi",
       "sov_a3": "BDI",
       "admin": "Burundi",
       "adm0_a3": "BDI",
       "geounit": "Burundi",
       "gu_a3": "BDI",
       "subunit": "Burundi",
       "su_a3": "BDI",
       "name": "Burundi",
       "name_long": "Burundi",
       "brk_a3": "BDI",
       "brk_name": "Burundi",
       "abbrev": "Bur.",
       "postal": "BI",
       "formal_en": "Republic of Burundi",
       "iso_a2": "BI",
       "iso_a3": "BDI",
       "un_a3": 108,
       "wb_a2": "BI",
       "wb_a3": "BDI",
       "adm0_a3_is": "BDI",
       "adm0_a3_us": "BDI",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Eastern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "BIF",
       "latlon": [
          -3.5,
          30
       ]
   },
   {
       "iso_n3": 56,
       "sovereignt": "Belgium",
       "sov_a3": "BEL",
       "admin": "Belgium",
       "adm0_a3": "BEL",
       "geounit": "Belgium",
       "gu_a3": "BEL",
       "subunit": "Belgium",
       "su_a3": "BEL",
       "name": "Belgium",
       "name_long": "Belgium",
       "brk_a3": "BEL",
       "brk_name": "Belgium",
       "abbrev": "Belg.",
       "postal": "B",
       "formal_en": "Kingdom of Belgium",
       "iso_a2": "BE",
       "iso_a3": "BEL",
       "un_a3": 56,
       "wb_a2": "BE",
       "wb_a3": "BEL",
       "adm0_a3_is": "BEL",
       "adm0_a3_us": "BEL",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Western Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "EUR",
       "latlon": [
          50.83333333,
          4
       ]
   },
   {
       "iso_n3": 204,
       "sovereignt": "Benin",
       "sov_a3": "BEN",
       "admin": "Benin",
       "adm0_a3": "BEN",
       "geounit": "Benin",
       "gu_a3": "BEN",
       "subunit": "Benin",
       "su_a3": "BEN",
       "name": "Benin",
       "name_long": "Benin",
       "brk_a3": "BEN",
       "brk_name": "Benin",
       "abbrev": "Benin",
       "postal": "BJ",
       "formal_en": "Republic of Benin",
       "iso_a2": "BJ",
       "iso_a3": "BEN",
       "un_a3": 204,
       "wb_a2": "BJ",
       "wb_a3": "BEN",
       "adm0_a3_is": "BEN",
       "adm0_a3_us": "BEN",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Western Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "XOF",
       "latlon": [
          9.5,
          2.25
       ]
   },
   {
       "iso_n3": 854,
       "sovereignt": "Burkina Faso",
       "sov_a3": "BFA",
       "admin": "Burkina Faso",
       "adm0_a3": "BFA",
       "geounit": "Burkina Faso",
       "gu_a3": "BFA",
       "subunit": "Burkina Faso",
       "su_a3": "BFA",
       "name": "Burkina Faso",
       "name_long": "Burkina Faso",
       "brk_a3": "BFA",
       "brk_name": "Burkina Faso",
       "abbrev": "B.F.",
       "postal": "BF",
       "formal_en": "Burkina Faso",
       "iso_a2": "BF",
       "iso_a3": "BFA",
       "un_a3": 854,
       "wb_a2": "BF",
       "wb_a3": "BFA",
       "adm0_a3_is": "BFA",
       "adm0_a3_us": "BFA",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Western Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "XOF",
       "latlon": [
          13,
          -2
       ]
   },
   {
       "iso_n3": 50,
       "sovereignt": "Bangladesh",
       "sov_a3": "BGD",
       "admin": "Bangladesh",
       "adm0_a3": "BGD",
       "geounit": "Bangladesh",
       "gu_a3": "BGD",
       "subunit": "Bangladesh",
       "su_a3": "BGD",
       "name": "Bangladesh",
       "name_long": "Bangladesh",
       "brk_a3": "BGD",
       "brk_name": "Bangladesh",
       "abbrev": "Bang.",
       "postal": "BD",
       "formal_en": "People's Republic of Bangladesh",
       "iso_a2": "BD",
       "iso_a3": "BGD",
       "un_a3": 50,
       "wb_a2": "BD",
       "wb_a3": "BGD",
       "adm0_a3_is": "BGD",
       "adm0_a3_us": "BGD",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Southern Asia",
       "region_wb": "South Asia",
       "currency": "BDT",
       "latlon": [
          24,
          90
       ]
   },
   {
       "iso_n3": 100,
       "sovereignt": "Bulgaria",
       "sov_a3": "BGR",
       "admin": "Bulgaria",
       "adm0_a3": "BGR",
       "geounit": "Bulgaria",
       "gu_a3": "BGR",
       "subunit": "Bulgaria",
       "su_a3": "BGR",
       "name": "Bulgaria",
       "name_long": "Bulgaria",
       "brk_a3": "BGR",
       "brk_name": "Bulgaria",
       "abbrev": "Bulg.",
       "postal": "BG",
       "formal_en": "Republic of Bulgaria",
       "iso_a2": "BG",
       "iso_a3": "BGR",
       "un_a3": 100,
       "wb_a2": "BG",
       "wb_a3": "BGR",
       "adm0_a3_is": "BGR",
       "adm0_a3_us": "BGR",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Eastern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "BGN",
       "latlon": [
          43,
          25
       ]
   },
   {
       "iso_n3": 44,
       "sovereignt": "The Bahamas",
       "sov_a3": "BHS",
       "admin": "The Bahamas",
       "adm0_a3": "BHS",
       "geounit": "The Bahamas",
       "gu_a3": "BHS",
       "subunit": "The Bahamas",
       "su_a3": "BHS",
       "name": "Bahamas",
       "name_long": "Bahamas",
       "brk_a3": "BHS",
       "brk_name": "Bahamas",
       "abbrev": "Bhs.",
       "postal": "BS",
       "formal_en": "Commonwealth of the Bahamas",
       "iso_a2": "BS",
       "iso_a3": "BHS",
       "un_a3": 44,
       "wb_a2": "BS",
       "wb_a3": "BHS",
       "adm0_a3_is": "BHS",
       "adm0_a3_us": "BHS",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Caribbean",
       "region_wb": "Latin America & Caribbean",
       "currency": "BSD",
       "latlon": [
          24.25,
          -76
       ]
   },
   {
       "iso_n3": 70,
       "sovereignt": "Bosnia and Herzegovina",
       "sov_a3": "BIH",
       "admin": "Bosnia and Herzegovina",
       "adm0_a3": "BIH",
       "geounit": "Bosnia and Herzegovina",
       "gu_a3": "BIH",
       "subunit": "Bosnia and Herzegovina",
       "su_a3": "BIH",
       "name": "Bosnia and Herz.",
       "name_long": "Bosnia and Herzegovina",
       "brk_a3": "BIH",
       "brk_name": "Bosnia and Herz.",
       "abbrev": "B.H.",
       "postal": "BiH",
       "formal_en": "Bosnia and Herzegovina",
       "iso_a2": "BA",
       "iso_a3": "BIH",
       "un_a3": 70,
       "wb_a2": "BA",
       "wb_a3": "BIH",
       "adm0_a3_is": "BIH",
       "adm0_a3_us": "BIH",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Southern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "BAM",
       "latlon": [
          44,
          18
       ]
   },
   {
       "iso_n3": 112,
       "sovereignt": "Belarus",
       "sov_a3": "BLR",
       "admin": "Belarus",
       "adm0_a3": "BLR",
       "geounit": "Belarus",
       "gu_a3": "BLR",
       "subunit": "Belarus",
       "su_a3": "BLR",
       "name": "Belarus",
       "name_long": "Belarus",
       "brk_a3": "BLR",
       "brk_name": "Belarus",
       "abbrev": "Bela.",
       "postal": "BY",
       "formal_en": "Republic of Belarus",
       "iso_a2": "BY",
       "iso_a3": "BLR",
       "un_a3": 112,
       "wb_a2": "BY",
       "wb_a3": "BLR",
       "adm0_a3_is": "BLR",
       "adm0_a3_us": "BLR",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Eastern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "BYR",
       "latlon": [
          53,
          28
       ]
   },
   {
       "iso_n3": 84,
       "sovereignt": "Belize",
       "sov_a3": "BLZ",
       "admin": "Belize",
       "adm0_a3": "BLZ",
       "geounit": "Belize",
       "gu_a3": "BLZ",
       "subunit": "Belize",
       "su_a3": "BLZ",
       "name": "Belize",
       "name_long": "Belize",
       "brk_a3": "BLZ",
       "brk_name": "Belize",
       "abbrev": "Belize",
       "postal": "BZ",
       "formal_en": "Belize",
       "iso_a2": "BZ",
       "iso_a3": "BLZ",
       "un_a3": 84,
       "wb_a2": "BZ",
       "wb_a3": "BLZ",
       "adm0_a3_is": "BLZ",
       "adm0_a3_us": "BLZ",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Central America",
       "region_wb": "Latin America & Caribbean",
       "currency": "BZD",
       "latlon": [
          17.25,
          -88.75
       ]
   },
   {
       "iso_n3": 68,
       "sovereignt": "Bolivia",
       "sov_a3": "BOL",
       "admin": "Bolivia",
       "adm0_a3": "BOL",
       "geounit": "Bolivia",
       "gu_a3": "BOL",
       "subunit": "Bolivia",
       "su_a3": "BOL",
       "name": "Bolivia",
       "name_long": "Bolivia",
       "brk_a3": "BOL",
       "brk_name": "Bolivia",
       "abbrev": "Bolivia",
       "postal": "BO",
       "formal_en": "Plurinational State of Bolivia",
       "iso_a2": "BO",
       "iso_a3": "BOL",
       "un_a3": 68,
       "wb_a2": "BO",
       "wb_a3": "BOL",
       "adm0_a3_is": "BOL",
       "adm0_a3_us": "BOL",
       "continent": "South America",
       "region_un": "Americas",
       "subregion": "South America",
       "region_wb": "Latin America & Caribbean",
       "currency": "BOB",
       "latlon": [
          -17,
          -65
       ]
   },
   {
       "iso_n3": 76,
       "sovereignt": "Brazil",
       "sov_a3": "BRA",
       "admin": "Brazil",
       "adm0_a3": "BRA",
       "geounit": "Brazil",
       "gu_a3": "BRA",
       "subunit": "Brazil",
       "su_a3": "BRA",
       "name": "Brazil",
       "name_long": "Brazil",
       "brk_a3": "BRA",
       "brk_name": "Brazil",
       "abbrev": "Brazil",
       "postal": "BR",
       "formal_en": "Federative Republic of Brazil",
       "iso_a2": "BR",
       "iso_a3": "BRA",
       "un_a3": 76,
       "wb_a2": "BR",
       "wb_a3": "BRA",
       "adm0_a3_is": "BRA",
       "adm0_a3_us": "BRA",
       "continent": "South America",
       "region_un": "Americas",
       "subregion": "South America",
       "region_wb": "Latin America & Caribbean",
       "currency": "BRL",
       "latlon": [
          -10,
          -55
       ]
   },
   {
       "iso_n3": 96,
       "sovereignt": "Brunei",
       "sov_a3": "BRN",
       "admin": "Brunei",
       "adm0_a3": "BRN",
       "geounit": "Brunei",
       "gu_a3": "BRN",
       "subunit": "Brunei",
       "su_a3": "BRN",
       "name": "Brunei",
       "name_long": "Brunei Darussalam",
       "brk_a3": "BRN",
       "brk_name": "Brunei",
       "abbrev": "Brunei",
       "postal": "BN",
       "formal_en": "Negara Brunei Darussalam",
       "iso_a2": "BN",
       "iso_a3": "BRN",
       "un_a3": 96,
       "wb_a2": "BN",
       "wb_a3": "BRN",
       "adm0_a3_is": "BRN",
       "adm0_a3_us": "BRN",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "South-Eastern Asia",
       "region_wb": "East Asia & Pacific",
       "currency": "BND",
       "latlon": [
          4.5,
          114.66666666
       ]
   },
   {
       "iso_n3": 64,
       "sovereignt": "Bhutan",
       "sov_a3": "BTN",
       "admin": "Bhutan",
       "adm0_a3": "BTN",
       "geounit": "Bhutan",
       "gu_a3": "BTN",
       "subunit": "Bhutan",
       "su_a3": "BTN",
       "name": "Bhutan",
       "name_long": "Bhutan",
       "brk_a3": "BTN",
       "brk_name": "Bhutan",
       "abbrev": "Bhutan",
       "postal": "BT",
       "formal_en": "Kingdom of Bhutan",
       "iso_a2": "BT",
       "iso_a3": "BTN",
       "un_a3": 64,
       "wb_a2": "BT",
       "wb_a3": "BTN",
       "adm0_a3_is": "BTN",
       "adm0_a3_us": "BTN",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Southern Asia",
       "region_wb": "South Asia",
       "currency": "BTN",
       "latlon": [
          27.5,
          90.5
       ]
   },
   {
       "iso_n3": 72,
       "sovereignt": "Botswana",
       "sov_a3": "BWA",
       "admin": "Botswana",
       "adm0_a3": "BWA",
       "geounit": "Botswana",
       "gu_a3": "BWA",
       "subunit": "Botswana",
       "su_a3": "BWA",
       "name": "Botswana",
       "name_long": "Botswana",
       "brk_a3": "BWA",
       "brk_name": "Botswana",
       "abbrev": "Bwa.",
       "postal": "BW",
       "formal_en": "Republic of Botswana",
       "iso_a2": "BW",
       "iso_a3": "BWA",
       "un_a3": 72,
       "wb_a2": "BW",
       "wb_a3": "BWA",
       "adm0_a3_is": "BWA",
       "adm0_a3_us": "BWA",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Southern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "BWP",
       "latlon": [
          -22,
          24
       ]
   },
   {
       "iso_n3": 140,
       "sovereignt": "Central African Republic",
       "sov_a3": "CAF",
       "admin": "Central African Republic",
       "adm0_a3": "CAF",
       "geounit": "Central African Republic",
       "gu_a3": "CAF",
       "subunit": "Central African Republic",
       "su_a3": "CAF",
       "name": "Central African Rep.",
       "name_long": "Central African Republic",
       "brk_a3": "CAF",
       "brk_name": "Central African Rep.",
       "abbrev": "C.A.R.",
       "postal": "CF",
       "formal_en": "Central African Republic",
       "iso_a2": "CF",
       "iso_a3": "CAF",
       "un_a3": 140,
       "wb_a2": "CF",
       "wb_a3": "CAF",
       "adm0_a3_is": "CAF",
       "adm0_a3_us": "CAF",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Middle Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "XAF",
       "latlon": [
          7,
          21
       ]
   },
   {
       "currency": "CAD",
       "latlon": [
           62,
           -116
       ],
       "iso_n3": 124,
       "sovereignt": "Canada",
       "sov_a3": "CAN",
       "admin": "Canada",
       "adm0_a3": "CAN",
       "geounit": "Canada",
       "gu_a3": "CAN",
       "subunit": "Canada",
       "su_a3": "CAN",
       "name": "Canada",
       "name_long": "Canada",
       "brk_a3": "CAN",
       "brk_name": "Canada",
       "abbrev": "Can.",
       "postal": "CA",
       "formal_en": "Canada",
       "iso_a2": "CA",
       "iso_a3": "CAN",
       "un_a3": 124,
       "wb_a2": "CA",
       "wb_a3": "CAN",
       "adm0_a3_is": "CAN",
       "adm0_a3_us": "CAN",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Northern America",
       "region_wb": "North America"
   },
   {
       "currency": "CHF",
       "latlon": [
          47,
          8
       ],
       "flagHeightMultiplier": 2.05,
       "iso_n3": 756,
       "sovereignt": "Switzerland",
       "sov_a3": "CHE",
       "admin": "Switzerland",
       "adm0_a3": "CHE",
       "geounit": "Switzerland",
       "gu_a3": "CHE",
       "subunit": "Switzerland",
       "su_a3": "CHE",
       "name": "Switzerland",
       "name_long": "Switzerland",
       "brk_a3": "CHE",
       "brk_name": "Switzerland",
       "abbrev": "Switz.",
       "postal": "CH",
       "formal_en": "Swiss Confederation",
       "iso_a2": "CH",
       "iso_a3": "CHE",
       "un_a3": 756,
       "wb_a2": "CH",
       "wb_a3": "CHE",
       "adm0_a3_is": "CHE",
       "adm0_a3_us": "CHE",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Western Europe",
       "region_wb": "Europe & Central Asia"
   },
   {
       "iso_n3": 152,
       "sovereignt": "Chile",
       "sov_a3": "CHL",
       "admin": "Chile",
       "adm0_a3": "CHL",
       "geounit": "Chile",
       "gu_a3": "CHL",
       "subunit": "Chile",
       "su_a3": "CHL",
       "name": "Chile",
       "name_long": "Chile",
       "brk_a3": "CHL",
       "brk_name": "Chile",
       "abbrev": "Chile",
       "postal": "CL",
       "formal_en": "Republic of Chile",
       "iso_a2": "CL",
       "iso_a3": "CHL",
       "un_a3": 152,
       "wb_a2": "CL",
       "wb_a3": "CHL",
       "adm0_a3_is": "CHL",
       "adm0_a3_us": "CHL",
       "continent": "South America",
       "region_un": "Americas",
       "subregion": "South America",
       "region_wb": "Latin America & Caribbean",
       "currency": "CLF",
       "latlon": [
          -30,
          -71
       ]
   },
   {
       "currency": "CNY",
       "latlon": [
          35,
          105
       ],
       "iso_n3": 156,
       "sovereignt": "China",
       "sov_a3": "CH1",
       "admin": "China",
       "adm0_a3": "CHN",
       "geounit": "China",
       "gu_a3": "CHN",
       "subunit": "China",
       "su_a3": "CHN",
       "name": "China",
       "name_long": "China",
       "brk_a3": "CHN",
       "brk_name": "China",
       "abbrev": "China",
       "postal": "CN",
       "formal_en": "People's Republic of China",
       "iso_a2": "CN",
       "iso_a3": "CHN",
       "un_a3": 156,
       "wb_a2": "CN",
       "wb_a3": "CHN",
       "adm0_a3_is": "CHN",
       "adm0_a3_us": "CHN",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Eastern Asia",
       "region_wb": "East Asia & Pacific"
   },
   {
       "iso_n3": 384,
       "sovereignt": "Ivory Coast",
       "sov_a3": "CIV",
       "admin": "Ivory Coast",
       "adm0_a3": "CIV",
       "geounit": "Ivory Coast",
       "gu_a3": "CIV",
       "subunit": "Ivory Coast",
       "su_a3": "CIV",
       "name": "Côte d'Ivoire",
       "name_long": "Côte d'Ivoire",
       "brk_a3": "CIV",
       "brk_name": "Côte d'Ivoire",
       "abbrev": "I.C.",
       "postal": "CI",
       "formal_en": "Republic of Ivory Coast",
       "iso_a2": "CI",
       "iso_a3": "CIV",
       "un_a3": 384,
       "wb_a2": "CI",
       "wb_a3": "CIV",
       "adm0_a3_is": "CIV",
       "adm0_a3_us": "CIV",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Western Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "XOF",
       "latlon": [
          8,
          -5
       ]
   },
   {
       "iso_n3": 120,
       "sovereignt": "Cameroon",
       "sov_a3": "CMR",
       "admin": "Cameroon",
       "adm0_a3": "CMR",
       "geounit": "Cameroon",
       "gu_a3": "CMR",
       "subunit": "Cameroon",
       "su_a3": "CMR",
       "name": "Cameroon",
       "name_long": "Cameroon",
       "brk_a3": "CMR",
       "brk_name": "Cameroon",
       "abbrev": "Cam.",
       "postal": "CM",
       "formal_en": "Republic of Cameroon",
       "iso_a2": "CM",
       "iso_a3": "CMR",
       "un_a3": 120,
       "wb_a2": "CM",
       "wb_a3": "CMR",
       "adm0_a3_is": "CMR",
       "adm0_a3_us": "CMR",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Middle Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "XAF",
       "latlon": [
          6,
          12
       ]
   },
   {
       "iso_n3": 180,
       "sovereignt": "Democratic Republic of the Congo",
       "sov_a3": "COD",
       "admin": "Democratic Republic of the Congo",
       "adm0_a3": "COD",
       "geounit": "Democratic Republic of the Congo",
       "gu_a3": "COD",
       "subunit": "Democratic Republic of the Congo",
       "su_a3": "COD",
       "name": "Dem. Rep. Congo",
       "name_long": "Democratic Republic of the Congo",
       "brk_a3": "COD",
       "brk_name": "Democratic Republic of the Congo",
       "abbrev": "D.R.C.",
       "postal": "DRC",
       "formal_en": "Democratic Republic of the Congo",
       "iso_a2": "CD",
       "iso_a3": "COD",
       "un_a3": 180,
       "wb_a2": "ZR",
       "wb_a3": "ZAR",
       "adm0_a3_is": "COD",
       "adm0_a3_us": "COD",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Middle Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "CDF",
       "latlon": [
          0,
          25
       ]
   },
   {
       "iso_n3": 178,
       "sovereignt": "Republic of Congo",
       "sov_a3": "COG",
       "admin": "Republic of Congo",
       "adm0_a3": "COG",
       "geounit": "Republic of Congo",
       "gu_a3": "COG",
       "subunit": "Republic of Congo",
       "su_a3": "COG",
       "name": "Congo",
       "name_long": "Republic of Congo",
       "brk_a3": "COG",
       "brk_name": "Republic of Congo",
       "abbrev": "Rep. Congo",
       "postal": "CG",
       "formal_en": "Republic of Congo",
       "iso_a2": "CG",
       "iso_a3": "COG",
       "un_a3": 178,
       "wb_a2": "CG",
       "wb_a3": "COG",
       "adm0_a3_is": "COG",
       "adm0_a3_us": "COG",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Middle Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "XAF",
       "latlon": [
          -1,
          15
       ]
   },
   {
       "iso_n3": 170,
       "sovereignt": "Colombia",
       "sov_a3": "COL",
       "admin": "Colombia",
       "adm0_a3": "COL",
       "geounit": "Colombia",
       "gu_a3": "COL",
       "subunit": "Colombia",
       "su_a3": "COL",
       "name": "Colombia",
       "name_long": "Colombia",
       "brk_a3": "COL",
       "brk_name": "Colombia",
       "abbrev": "Col.",
       "postal": "CO",
       "formal_en": "Republic of Colombia",
       "iso_a2": "CO",
       "iso_a3": "COL",
       "un_a3": 170,
       "wb_a2": "CO",
       "wb_a3": "COL",
       "adm0_a3_is": "COL",
       "adm0_a3_us": "COL",
       "continent": "South America",
       "region_un": "Americas",
       "subregion": "South America",
       "region_wb": "Latin America & Caribbean",
       "currency": "COP",
       "latlon": [
          4,
          -72
       ]
   },
   {
       "iso_n3": 188,
       "sovereignt": "Costa Rica",
       "sov_a3": "CRI",
       "admin": "Costa Rica",
       "adm0_a3": "CRI",
       "geounit": "Costa Rica",
       "gu_a3": "CRI",
       "subunit": "Costa Rica",
       "su_a3": "CRI",
       "name": "Costa Rica",
       "name_long": "Costa Rica",
       "brk_a3": "CRI",
       "brk_name": "Costa Rica",
       "abbrev": "C.R.",
       "postal": "CR",
       "formal_en": "Republic of Costa Rica",
       "iso_a2": "CR",
       "iso_a3": "CRI",
       "un_a3": 188,
       "wb_a2": "CR",
       "wb_a3": "CRI",
       "adm0_a3_is": "CRI",
       "adm0_a3_us": "CRI",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Central America",
       "region_wb": "Latin America & Caribbean",
       "currency": "CRC",
       "latlon": [
          10,
          -84
       ]
   },
   {
       "iso_n3": 192,
       "sovereignt": "Cuba",
       "sov_a3": "CUB",
       "admin": "Cuba",
       "adm0_a3": "CUB",
       "geounit": "Cuba",
       "gu_a3": "CUB",
       "subunit": "Cuba",
       "su_a3": "CUB",
       "name": "Cuba",
       "name_long": "Cuba",
       "brk_a3": "CUB",
       "brk_name": "Cuba",
       "abbrev": "Cuba",
       "postal": "CU",
       "formal_en": "Republic of Cuba",
       "iso_a2": "CU",
       "iso_a3": "CUB",
       "un_a3": 192,
       "wb_a2": "CU",
       "wb_a3": "CUB",
       "adm0_a3_is": "CUB",
       "adm0_a3_us": "CUB",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Caribbean",
       "region_wb": "Latin America & Caribbean",
       "currency": "CUC",
       "latlon": [
          21.5,
          -80
       ]
   },
   {
       "iso_n3": 196,
       "sovereignt": "Cyprus",
       "sov_a3": "CYP",
       "admin": "Cyprus",
       "adm0_a3": "CYP",
       "geounit": "Cyprus",
       "gu_a3": "CYP",
       "subunit": "Cyprus",
       "su_a3": "CYP",
       "name": "Cyprus",
       "name_long": "Cyprus",
       "brk_a3": "CYP",
       "brk_name": "Cyprus",
       "abbrev": "Cyp.",
       "postal": "CY",
       "formal_en": "Republic of Cyprus",
       "iso_a2": "CY",
       "iso_a3": "CYP",
       "un_a3": 196,
       "wb_a2": "CY",
       "wb_a3": "CYP",
       "adm0_a3_is": "CYP",
       "adm0_a3_us": "CYP",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Europe & Central Asia",
       "currency": "EUR",
       "latlon": [
          35,
          33
       ]
   },
   {
       "iso_n3": 203,
       "sovereignt": "Czech Republic",
       "sov_a3": "CZE",
       "admin": "Czech Republic",
       "adm0_a3": "CZE",
       "geounit": "Czech Republic",
       "gu_a3": "CZE",
       "subunit": "Czech Republic",
       "su_a3": "CZE",
       "name": "Czech Rep.",
       "name_long": "Czech Republic",
       "brk_a3": "CZE",
       "brk_name": "Czech Rep.",
       "abbrev": "Cz. Rep.",
       "postal": "CZ",
       "formal_en": "Czech Republic",
       "iso_a2": "CZ",
       "iso_a3": "CZE",
       "un_a3": 203,
       "wb_a2": "CZ",
       "wb_a3": "CZE",
       "adm0_a3_is": "CZE",
       "adm0_a3_us": "CZE",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Eastern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "CZK",
       "latlon": [
          49.75,
          15.5
       ]
   },
   {
       "currency": "EUR",
       "latlon": [
          51,
          9
       ],
       "iso_n3": 276,
       "sovereignt": "Germany",
       "sov_a3": "DEU",
       "admin": "Germany",
       "adm0_a3": "DEU",
       "geounit": "Germany",
       "gu_a3": "DEU",
       "subunit": "Germany",
       "su_a3": "DEU",
       "name": "Germany",
       "name_long": "Germany",
       "brk_a3": "DEU",
       "brk_name": "Germany",
       "abbrev": "Ger.",
       "postal": "D",
       "formal_en": "Federal Republic of Germany",
       "iso_a2": "DE",
       "iso_a3": "DEU",
       "un_a3": 276,
       "wb_a2": "DE",
       "wb_a3": "DEU",
       "adm0_a3_is": "DEU",
       "adm0_a3_us": "DEU",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Western Europe",
       "region_wb": "Europe & Central Asia"
   },
   {
       "iso_n3": 262,
       "sovereignt": "Djibouti",
       "sov_a3": "DJI",
       "admin": "Djibouti",
       "adm0_a3": "DJI",
       "geounit": "Djibouti",
       "gu_a3": "DJI",
       "subunit": "Djibouti",
       "su_a3": "DJI",
       "name": "Djibouti",
       "name_long": "Djibouti",
       "brk_a3": "DJI",
       "brk_name": "Djibouti",
       "abbrev": "Dji.",
       "postal": "DJ",
       "formal_en": "Republic of Djibouti",
       "iso_a2": "DJ",
       "iso_a3": "DJI",
       "un_a3": 262,
       "wb_a2": "DJ",
       "wb_a3": "DJI",
       "adm0_a3_is": "DJI",
       "adm0_a3_us": "DJI",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Eastern Africa",
       "region_wb": "Middle East & North Africa",
       "currency": "DJF",
       "latlon": [
          11.5,
          43
       ]
   },
   {
       "iso_n3": 208,
       "sovereignt": "Denmark",
       "sov_a3": "DN1",
       "admin": "Denmark",
       "adm0_a3": "DNK",
       "geounit": "Denmark",
       "gu_a3": "DNK",
       "subunit": "Denmark",
       "su_a3": "DNK",
       "name": "Denmark",
       "name_long": "Denmark",
       "brk_a3": "DNK",
       "brk_name": "Denmark",
       "abbrev": "Den.",
       "postal": "DK",
       "formal_en": "Kingdom of Denmark",
       "iso_a2": "DK",
       "iso_a3": "DNK",
       "un_a3": 208,
       "wb_a2": "DK",
       "wb_a3": "DNK",
       "adm0_a3_is": "DNK",
       "adm0_a3_us": "DNK",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Northern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "DKK",
       "latlon": [
          56,
          10
       ]
   },
   {
       "iso_n3": 214,
       "sovereignt": "Dominican Republic",
       "sov_a3": "DOM",
       "admin": "Dominican Republic",
       "adm0_a3": "DOM",
       "geounit": "Dominican Republic",
       "gu_a3": "DOM",
       "subunit": "Dominican Republic",
       "su_a3": "DOM",
       "name": "Dominican Rep.",
       "name_long": "Dominican Republic",
       "brk_a3": "DOM",
       "brk_name": "Dominican Rep.",
       "abbrev": "Dom. Rep.",
       "postal": "DO",
       "formal_en": "Dominican Republic",
       "iso_a2": "DO",
       "iso_a3": "DOM",
       "un_a3": 214,
       "wb_a2": "DO",
       "wb_a3": "DOM",
       "adm0_a3_is": "DOM",
       "adm0_a3_us": "DOM",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Caribbean",
       "region_wb": "Latin America & Caribbean",
       "currency": "DOP",
       "latlon": [
          19,
          -70.66666666
       ]
   },
   {
       "iso_n3": 12,
       "sovereignt": "Algeria",
       "sov_a3": "DZA",
       "admin": "Algeria",
       "adm0_a3": "DZA",
       "geounit": "Algeria",
       "gu_a3": "DZA",
       "subunit": "Algeria",
       "su_a3": "DZA",
       "name": "Algeria",
       "name_long": "Algeria",
       "brk_a3": "DZA",
       "brk_name": "Algeria",
       "abbrev": "Alg.",
       "postal": "DZ",
       "formal_en": "People's Democratic Republic of Algeria",
       "iso_a2": "DZ",
       "iso_a3": "DZA",
       "un_a3": 12,
       "wb_a2": "DZ",
       "wb_a3": "DZA",
       "adm0_a3_is": "DZA",
       "adm0_a3_us": "DZA",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Northern Africa",
       "region_wb": "Middle East & North Africa",
       "currency": "DZD",
       "latlon": [
          28,
          3
       ]
   },
   {
       "iso_n3": 218,
       "sovereignt": "Ecuador",
       "sov_a3": "ECU",
       "admin": "Ecuador",
       "adm0_a3": "ECU",
       "geounit": "Ecuador",
       "gu_a3": "ECU",
       "subunit": "Ecuador",
       "su_a3": "ECU",
       "name": "Ecuador",
       "name_long": "Ecuador",
       "brk_a3": "ECU",
       "brk_name": "Ecuador",
       "abbrev": "Ecu.",
       "postal": "EC",
       "formal_en": "Republic of Ecuador",
       "iso_a2": "EC",
       "iso_a3": "ECU",
       "un_a3": 218,
       "wb_a2": "EC",
       "wb_a3": "ECU",
       "adm0_a3_is": "ECU",
       "adm0_a3_us": "ECU",
       "continent": "South America",
       "region_un": "Americas",
       "subregion": "South America",
       "region_wb": "Latin America & Caribbean",
       "currency": "USD_DISABLED",
       "latlon": [
          -2,
          -77.5
       ]
   },
   {
       "iso_n3": 818,
       "sovereignt": "Egypt",
       "sov_a3": "EGY",
       "admin": "Egypt",
       "adm0_a3": "EGY",
       "geounit": "Egypt",
       "gu_a3": "EGY",
       "subunit": "Egypt",
       "su_a3": "EGY",
       "name": "Egypt",
       "name_long": "Egypt",
       "brk_a3": "EGY",
       "brk_name": "Egypt",
       "abbrev": "Egypt",
       "postal": "EG",
       "formal_en": "Arab Republic of Egypt",
       "iso_a2": "EG",
       "iso_a3": "EGY",
       "un_a3": 818,
       "wb_a2": "EG",
       "wb_a3": "EGY",
       "adm0_a3_is": "EGY",
       "adm0_a3_us": "EGY",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Northern Africa",
       "region_wb": "Middle East & North Africa",
       "currency": "EGP",
       "latlon": [
          27,
          30
       ]
   },
   {
       "iso_n3": 232,
       "sovereignt": "Eritrea",
       "sov_a3": "ERI",
       "admin": "Eritrea",
       "adm0_a3": "ERI",
       "geounit": "Eritrea",
       "gu_a3": "ERI",
       "subunit": "Eritrea",
       "su_a3": "ERI",
       "name": "Eritrea",
       "name_long": "Eritrea",
       "brk_a3": "ERI",
       "brk_name": "Eritrea",
       "abbrev": "Erit.",
       "postal": "ER",
       "formal_en": "State of Eritrea",
       "iso_a2": "ER",
       "iso_a3": "ERI",
       "un_a3": 232,
       "wb_a2": "ER",
       "wb_a3": "ERI",
       "adm0_a3_is": "ERI",
       "adm0_a3_us": "ERI",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Eastern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "ERN",
       "latlon": [
          15,
          39
       ]
   },
   {
       "iso_n3": 732,
       "sovereignt": "Western Sahara",
       "sov_a3": "SAH",
       "admin": "Western Sahara",
       "adm0_a3": "SAH",
       "geounit": "Western Sahara",
       "gu_a3": "SAH",
       "subunit": "Western Sahara",
       "su_a3": "SAH",
       "name": "W. Sahara",
       "name_long": "Western Sahara",
       "brk_a3": "B28",
       "brk_name": "W. Sahara",
       "abbrev": "W. Sah.",
       "postal": "WS",
       "formal_en": "Sahrawi Arab Democratic Republic",
       "iso_a2": "EH",
       "iso_a3": "ESH",
       "un_a3": 732,
       "wb_a2": "-99",
       "wb_a3": "-99",
       "adm0_a3_is": "MAR",
       "adm0_a3_us": "SAH",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Northern Africa",
       "region_wb": "Middle East & North Africa",
       "currency": "MAD",
       "latlon": [
          24.5,
          -13
       ]
   },
   {
       "currency": "EUR",
       "latlon": [
          40,
          -4
       ],
       "iso_n3": 724,
       "sovereignt": "Spain",
       "sov_a3": "ESP",
       "admin": "Spain",
       "adm0_a3": "ESP",
       "geounit": "Spain",
       "gu_a3": "ESP",
       "subunit": "Spain",
       "su_a3": "ESP",
       "name": "Spain",
       "name_long": "Spain",
       "brk_a3": "ESP",
       "brk_name": "Spain",
       "abbrev": "Sp.",
       "postal": "E",
       "formal_en": "Kingdom of Spain",
       "iso_a2": "ES",
       "iso_a3": "ESP",
       "un_a3": 724,
       "wb_a2": "ES",
       "wb_a3": "ESP",
       "adm0_a3_is": "ESP",
       "adm0_a3_us": "ESP",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Southern Europe",
       "region_wb": "Europe & Central Asia"
   },
   {
       "iso_n3": 233,
       "sovereignt": "Estonia",
       "sov_a3": "EST",
       "admin": "Estonia",
       "adm0_a3": "EST",
       "geounit": "Estonia",
       "gu_a3": "EST",
       "subunit": "Estonia",
       "su_a3": "EST",
       "name": "Estonia",
       "name_long": "Estonia",
       "brk_a3": "EST",
       "brk_name": "Estonia",
       "abbrev": "Est.",
       "postal": "EST",
       "formal_en": "Republic of Estonia",
       "iso_a2": "EE",
       "iso_a3": "EST",
       "un_a3": 233,
       "wb_a2": "EE",
       "wb_a3": "EST",
       "adm0_a3_is": "EST",
       "adm0_a3_us": "EST",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Northern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "EUR",
       "latlon": [
          59,
          26
       ]
   },
   {
       "iso_n3": 231,
       "sovereignt": "Ethiopia",
       "sov_a3": "ETH",
       "admin": "Ethiopia",
       "adm0_a3": "ETH",
       "geounit": "Ethiopia",
       "gu_a3": "ETH",
       "subunit": "Ethiopia",
       "su_a3": "ETH",
       "name": "Ethiopia",
       "name_long": "Ethiopia",
       "brk_a3": "ETH",
       "brk_name": "Ethiopia",
       "abbrev": "Eth.",
       "postal": "ET",
       "formal_en": "Federal Democratic Republic of Ethiopia",
       "iso_a2": "ET",
       "iso_a3": "ETH",
       "un_a3": 231,
       "wb_a2": "ET",
       "wb_a3": "ETH",
       "adm0_a3_is": "ETH",
       "adm0_a3_us": "ETH",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Eastern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "ETB",
       "latlon": [
          8,
          38
       ]
   },
   {
       "iso_n3": 246,
       "sovereignt": "Finland",
       "sov_a3": "FI1",
       "admin": "Finland",
       "adm0_a3": "FIN",
       "geounit": "Finland",
       "gu_a3": "FIN",
       "subunit": "Finland",
       "su_a3": "FIN",
       "name": "Finland",
       "name_long": "Finland",
       "brk_a3": "FIN",
       "brk_name": "Finland",
       "abbrev": "Fin.",
       "postal": "FIN",
       "formal_en": "Republic of Finland",
       "iso_a2": "FI",
       "iso_a3": "FIN",
       "un_a3": 246,
       "wb_a2": "FI",
       "wb_a3": "FIN",
       "adm0_a3_is": "FIN",
       "adm0_a3_us": "FIN",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Northern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "EUR",
       "latlon": [
          64,
          26
       ]
   },
   {
       "iso_n3": 242,
       "sovereignt": "Fiji",
       "sov_a3": "FJI",
       "admin": "Fiji",
       "adm0_a3": "FJI",
       "geounit": "Fiji",
       "gu_a3": "FJI",
       "subunit": "Fiji",
       "su_a3": "FJI",
       "name": "Fiji",
       "name_long": "Fiji",
       "brk_a3": "FJI",
       "brk_name": "Fiji",
       "abbrev": "Fiji",
       "postal": "FJ",
       "formal_en": "Republic of Fiji",
       "iso_a2": "FJ",
       "iso_a3": "FJI",
       "un_a3": 242,
       "wb_a2": "FJ",
       "wb_a3": "FJI",
       "adm0_a3_is": "FJI",
       "adm0_a3_us": "FJI",
       "continent": "Oceania",
       "region_un": "Oceania",
       "subregion": "Melanesia",
       "region_wb": "East Asia & Pacific",
       "currency": "FJD",
       "latlon": [
          -18,
          175
       ]
   },
   {
       "iso_n3": 238,
       "sovereignt": "United Kingdom",
       "sov_a3": "GB1",
       "admin": "Falkland Islands",
       "adm0_a3": "FLK",
       "geounit": "Falkland Islands",
       "gu_a3": "FLK",
       "subunit": "Falkland Islands",
       "su_a3": "FLK",
       "name": "Falkland Is.",
       "name_long": "Falkland Islands",
       "brk_a3": "B12",
       "brk_name": "Falkland Is.",
       "abbrev": "Flk. Is.",
       "postal": "FK",
       "formal_en": "Falkland Islands",
       "iso_a2": "FK",
       "iso_a3": "FLK",
       "un_a3": 238,
       "wb_a2": "-99",
       "wb_a3": "-99",
       "adm0_a3_is": "FLK",
       "adm0_a3_us": "FLK",
       "continent": "South America",
       "region_un": "Americas",
       "subregion": "South America",
       "region_wb": "Latin America & Caribbean",
       "currency": "FKP",
       "latlon": [
          -51.75,
          -59
       ]
   },
   {
       "currency": "EUR",
       "latlon": [
          46,
          2
       ],
       "iso_n3": 250,
       "sovereignt": "France",
       "sov_a3": "FR1",
       "admin": "France",
       "adm0_a3": "FRA",
       "geounit": "France",
       "gu_a3": "FRA",
       "subunit": "France",
       "su_a3": "FRA",
       "name": "France",
       "name_long": "France",
       "brk_a3": "FRA",
       "brk_name": "France",
       "abbrev": "Fr.",
       "postal": "F",
       "formal_en": "French Republic",
       "iso_a2": "FR",
       "iso_a3": "FRA",
       "un_a3": 250,
       "wb_a2": "FR",
       "wb_a3": "FRA",
       "adm0_a3_is": "FRA",
       "adm0_a3_us": "FRA",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Western Europe",
       "region_wb": "Europe & Central Asia"
   },
   {
       "iso_n3": 266,
       "sovereignt": "Gabon",
       "sov_a3": "GAB",
       "admin": "Gabon",
       "adm0_a3": "GAB",
       "geounit": "Gabon",
       "gu_a3": "GAB",
       "subunit": "Gabon",
       "su_a3": "GAB",
       "name": "Gabon",
       "name_long": "Gabon",
       "brk_a3": "GAB",
       "brk_name": "Gabon",
       "abbrev": "Gabon",
       "postal": "GA",
       "formal_en": "Gabonese Republic",
       "iso_a2": "GA",
       "iso_a3": "GAB",
       "un_a3": 266,
       "wb_a2": "GA",
       "wb_a3": "GAB",
       "adm0_a3_is": "GAB",
       "adm0_a3_us": "GAB",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Middle Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "XAF",
       "latlon": [
          -1,
          11.75
       ]
   },
   {
       "currency": "GBP",
       "flagHeightMultiplier": 1.1,
       "iso_n3": 826,
       "sovereignt": "United Kingdom",
       "sov_a3": "GB1",
       "admin": "United Kingdom",
       "adm0_a3": "GBR",
       "geounit": "United Kingdom",
       "gu_a3": "GBR",
       "subunit": "United Kingdom",
       "su_a3": "GBR",
       "name": "United Kingdom",
       "name_long": "United Kingdom",
       "brk_a3": "GBR",
       "brk_name": "United Kingdom",
       "abbrev": "U.K.",
       "postal": "GB",
       "formal_en": "United Kingdom of Great Britain and Northern Ireland",
       "iso_a2": "GB",
       "iso_a3": "GBR",
       "un_a3": 826,
       "wb_a2": "GB",
       "wb_a3": "GBR",
       "adm0_a3_is": "GBR",
       "adm0_a3_us": "GBR",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Northern Europe",
       "region_wb": "Europe & Central Asia",
       "latlon": [
          54,
          -2
       ]
   },
   {
       "iso_n3": 268,
       "sovereignt": "Georgia",
       "sov_a3": "GEO",
       "admin": "Georgia",
       "adm0_a3": "GEO",
       "geounit": "Georgia",
       "gu_a3": "GEO",
       "subunit": "Georgia",
       "su_a3": "GEO",
       "name": "Georgia",
       "name_long": "Georgia",
       "brk_a3": "GEO",
       "brk_name": "Georgia",
       "abbrev": "Geo.",
       "postal": "GE",
       "formal_en": "Georgia",
       "iso_a2": "GE",
       "iso_a3": "GEO",
       "un_a3": 268,
       "wb_a2": "GE",
       "wb_a3": "GEO",
       "adm0_a3_is": "GEO",
       "adm0_a3_us": "GEO",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Europe & Central Asia",
       "currency": "GEL",
       "latlon": [
          42,
          43.5
       ]
   },
   {
       "iso_n3": 288,
       "sovereignt": "Ghana",
       "sov_a3": "GHA",
       "admin": "Ghana",
       "adm0_a3": "GHA",
       "geounit": "Ghana",
       "gu_a3": "GHA",
       "subunit": "Ghana",
       "su_a3": "GHA",
       "name": "Ghana",
       "name_long": "Ghana",
       "brk_a3": "GHA",
       "brk_name": "Ghana",
       "abbrev": "Ghana",
       "postal": "GH",
       "formal_en": "Republic of Ghana",
       "iso_a2": "GH",
       "iso_a3": "GHA",
       "un_a3": 288,
       "wb_a2": "GH",
       "wb_a3": "GHA",
       "adm0_a3_is": "GHA",
       "adm0_a3_us": "GHA",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Western Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "GHS",
       "latlon": [
          8,
          -2
       ]
   },
   {
       "iso_n3": 324,
       "sovereignt": "Guinea",
       "sov_a3": "GIN",
       "admin": "Guinea",
       "adm0_a3": "GIN",
       "geounit": "Guinea",
       "gu_a3": "GIN",
       "subunit": "Guinea",
       "su_a3": "GIN",
       "name": "Guinea",
       "name_long": "Guinea",
       "brk_a3": "GIN",
       "brk_name": "Guinea",
       "abbrev": "Gin.",
       "postal": "GN",
       "formal_en": "Republic of Guinea",
       "iso_a2": "GN",
       "iso_a3": "GIN",
       "un_a3": 324,
       "wb_a2": "GN",
       "wb_a3": "GIN",
       "adm0_a3_is": "GIN",
       "adm0_a3_us": "GIN",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Western Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "GNF",
       "latlon": [
          11,
          -10
       ]
   },
   {
       "iso_n3": 270,
       "sovereignt": "Gambia",
       "sov_a3": "GMB",
       "admin": "Gambia",
       "adm0_a3": "GMB",
       "geounit": "Gambia",
       "gu_a3": "GMB",
       "subunit": "Gambia",
       "su_a3": "GMB",
       "name": "Gambia",
       "name_long": "The Gambia",
       "brk_a3": "GMB",
       "brk_name": "Gambia",
       "abbrev": "Gambia",
       "postal": "GM",
       "formal_en": "Republic of the Gambia",
       "iso_a2": "GM",
       "iso_a3": "GMB",
       "un_a3": 270,
       "wb_a2": "GM",
       "wb_a3": "GMB",
       "adm0_a3_is": "GMB",
       "adm0_a3_us": "GMB",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Western Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "GMD",
       "latlon": [
          13.46666666,
          -16.56666666
       ]
   },
   {
       "iso_n3": 624,
       "sovereignt": "Guinea Bissau",
       "sov_a3": "GNB",
       "admin": "Guinea Bissau",
       "adm0_a3": "GNB",
       "geounit": "Guinea Bissau",
       "gu_a3": "GNB",
       "subunit": "Guinea Bissau",
       "su_a3": "GNB",
       "name": "Guinea-Bissau",
       "name_long": "Guinea-Bissau",
       "brk_a3": "GNB",
       "brk_name": "Guinea-Bissau",
       "abbrev": "GnB.",
       "postal": "GW",
       "formal_en": "Republic of Guinea-Bissau",
       "iso_a2": "GW",
       "iso_a3": "GNB",
       "un_a3": 624,
       "wb_a2": "GW",
       "wb_a3": "GNB",
       "adm0_a3_is": "GNB",
       "adm0_a3_us": "GNB",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Western Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "XOF",
       "latlon": [
          12,
          -15
       ]
   },
   {
       "iso_n3": 226,
       "sovereignt": "Equatorial Guinea",
       "sov_a3": "GNQ",
       "admin": "Equatorial Guinea",
       "adm0_a3": "GNQ",
       "geounit": "Equatorial Guinea",
       "gu_a3": "GNQ",
       "subunit": "Equatorial Guinea",
       "su_a3": "GNQ",
       "name": "Eq. Guinea",
       "name_long": "Equatorial Guinea",
       "brk_a3": "GNQ",
       "brk_name": "Eq. Guinea",
       "abbrev": "Eq. G.",
       "postal": "GQ",
       "formal_en": "Republic of Equatorial Guinea",
       "iso_a2": "GQ",
       "iso_a3": "GNQ",
       "un_a3": 226,
       "wb_a2": "GQ",
       "wb_a3": "GNQ",
       "adm0_a3_is": "GNQ",
       "adm0_a3_us": "GNQ",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Middle Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "XAF",
       "latlon": [
          2,
          10
       ]
   },
   {
       "iso_n3": 300,
       "sovereignt": "Greece",
       "sov_a3": "GRC",
       "admin": "Greece",
       "adm0_a3": "GRC",
       "geounit": "Greece",
       "gu_a3": "GRC",
       "subunit": "Greece",
       "su_a3": "GRC",
       "name": "Greece",
       "name_long": "Greece",
       "brk_a3": "GRC",
       "brk_name": "Greece",
       "abbrev": "Greece",
       "postal": "GR",
       "formal_en": "Hellenic Republic",
       "iso_a2": "GR",
       "iso_a3": "GRC",
       "un_a3": 300,
       "wb_a2": "GR",
       "wb_a3": "GRC",
       "adm0_a3_is": "GRC",
       "adm0_a3_us": "GRC",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Southern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "EUR",
       "latlon": [
          39,
          22
       ]
   },
   {
       "iso_n3": 304,
       "sovereignt": "Denmark",
       "sov_a3": "DN1",
       "admin": "Greenland",
       "adm0_a3": "GRL",
       "geounit": "Greenland",
       "gu_a3": "GRL",
       "subunit": "Greenland",
       "su_a3": "GRL",
       "name": "Greenland",
       "name_long": "Greenland",
       "brk_a3": "GRL",
       "brk_name": "Greenland",
       "abbrev": "Grlnd.",
       "postal": "GL",
       "formal_en": "Greenland",
       "iso_a2": "GL",
       "iso_a3": "GRL",
       "un_a3": 304,
       "wb_a2": "GL",
       "wb_a3": "GRL",
       "adm0_a3_is": "GRL",
       "adm0_a3_us": "GRL",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Northern America",
       "region_wb": "Europe & Central Asia",
       "currency": "DKK",
       "latlon": [
          72,
          -40
       ]
   },
   {
       "iso_n3": 320,
       "sovereignt": "Guatemala",
       "sov_a3": "GTM",
       "admin": "Guatemala",
       "adm0_a3": "GTM",
       "geounit": "Guatemala",
       "gu_a3": "GTM",
       "subunit": "Guatemala",
       "su_a3": "GTM",
       "name": "Guatemala",
       "name_long": "Guatemala",
       "brk_a3": "GTM",
       "brk_name": "Guatemala",
       "abbrev": "Guat.",
       "postal": "GT",
       "formal_en": "Republic of Guatemala",
       "iso_a2": "GT",
       "iso_a3": "GTM",
       "un_a3": 320,
       "wb_a2": "GT",
       "wb_a3": "GTM",
       "adm0_a3_is": "GTM",
       "adm0_a3_us": "GTM",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Central America",
       "region_wb": "Latin America & Caribbean",
       "currency": "GTQ",
       "latlon": [
          15.5,
          -90.25
       ]
   },
   {
       "iso_n3": 328,
       "sovereignt": "Guyana",
       "sov_a3": "GUY",
       "admin": "Guyana",
       "adm0_a3": "GUY",
       "geounit": "Guyana",
       "gu_a3": "GUY",
       "subunit": "Guyana",
       "su_a3": "GUY",
       "name": "Guyana",
       "name_long": "Guyana",
       "brk_a3": "GUY",
       "brk_name": "Guyana",
       "abbrev": "Guy.",
       "postal": "GY",
       "formal_en": "Co-operative Republic of Guyana",
       "iso_a2": "GY",
       "iso_a3": "GUY",
       "un_a3": 328,
       "wb_a2": "GY",
       "wb_a3": "GUY",
       "adm0_a3_is": "GUY",
       "adm0_a3_us": "GUY",
       "continent": "South America",
       "region_un": "Americas",
       "subregion": "South America",
       "region_wb": "Latin America & Caribbean",
       "currency": "GYD",
       "latlon": [
          5,
          -59
       ]
   },
   {
       "iso_n3": 340,
       "sovereignt": "Honduras",
       "sov_a3": "HND",
       "admin": "Honduras",
       "adm0_a3": "HND",
       "geounit": "Honduras",
       "gu_a3": "HND",
       "subunit": "Honduras",
       "su_a3": "HND",
       "name": "Honduras",
       "name_long": "Honduras",
       "brk_a3": "HND",
       "brk_name": "Honduras",
       "abbrev": "Hond.",
       "postal": "HN",
       "formal_en": "Republic of Honduras",
       "iso_a2": "HN",
       "iso_a3": "HND",
       "un_a3": 340,
       "wb_a2": "HN",
       "wb_a3": "HND",
       "adm0_a3_is": "HND",
       "adm0_a3_us": "HND",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Central America",
       "region_wb": "Latin America & Caribbean",
       "currency": "HNL",
       "latlon": [
          15,
          -86.5
       ]
   },
   {
       "iso_n3": 191,
       "sovereignt": "Croatia",
       "sov_a3": "HRV",
       "admin": "Croatia",
       "adm0_a3": "HRV",
       "geounit": "Croatia",
       "gu_a3": "HRV",
       "subunit": "Croatia",
       "su_a3": "HRV",
       "name": "Croatia",
       "name_long": "Croatia",
       "brk_a3": "HRV",
       "brk_name": "Croatia",
       "abbrev": "Cro.",
       "postal": "HR",
       "formal_en": "Republic of Croatia",
       "iso_a2": "HR",
       "iso_a3": "HRV",
       "un_a3": 191,
       "wb_a2": "HR",
       "wb_a3": "HRV",
       "adm0_a3_is": "HRV",
       "adm0_a3_us": "HRV",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Southern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "HRK",
       "latlon": [
          45.16666666,
          15.5
       ]
   },
   {
       "iso_n3": 332,
       "sovereignt": "Haiti",
       "sov_a3": "HTI",
       "admin": "Haiti",
       "adm0_a3": "HTI",
       "geounit": "Haiti",
       "gu_a3": "HTI",
       "subunit": "Haiti",
       "su_a3": "HTI",
       "name": "Haiti",
       "name_long": "Haiti",
       "brk_a3": "HTI",
       "brk_name": "Haiti",
       "abbrev": "Haiti",
       "postal": "HT",
       "formal_en": "Republic of Haiti",
       "iso_a2": "HT",
       "iso_a3": "HTI",
       "un_a3": 332,
       "wb_a2": "HT",
       "wb_a3": "HTI",
       "adm0_a3_is": "HTI",
       "adm0_a3_us": "HTI",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Caribbean",
       "region_wb": "Latin America & Caribbean",
       "currency": "HTG",
       "latlon": [
          19,
          -72.41666666
       ]
   },
   {
       "iso_n3": 348,
       "sovereignt": "Hungary",
       "sov_a3": "HUN",
       "admin": "Hungary",
       "adm0_a3": "HUN",
       "geounit": "Hungary",
       "gu_a3": "HUN",
       "subunit": "Hungary",
       "su_a3": "HUN",
       "name": "Hungary",
       "name_long": "Hungary",
       "brk_a3": "HUN",
       "brk_name": "Hungary",
       "abbrev": "Hun.",
       "postal": "HU",
       "formal_en": "Republic of Hungary",
       "iso_a2": "HU",
       "iso_a3": "HUN",
       "un_a3": 348,
       "wb_a2": "HU",
       "wb_a3": "HUN",
       "adm0_a3_is": "HUN",
       "adm0_a3_us": "HUN",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Eastern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "HUF",
       "latlon": [
          47,
          20
       ]
   },
   {
       "iso_n3": 360,
       "sovereignt": "Indonesia",
       "sov_a3": "IDN",
       "admin": "Indonesia",
       "adm0_a3": "IDN",
       "geounit": "Indonesia",
       "gu_a3": "IDN",
       "subunit": "Indonesia",
       "su_a3": "IDN",
       "name": "Indonesia",
       "name_long": "Indonesia",
       "brk_a3": "IDN",
       "brk_name": "Indonesia",
       "abbrev": "Indo.",
       "postal": "INDO",
       "formal_en": "Republic of Indonesia",
       "iso_a2": "ID",
       "iso_a3": "IDN",
       "un_a3": 360,
       "wb_a2": "ID",
       "wb_a3": "IDN",
       "adm0_a3_is": "IDN",
       "adm0_a3_us": "IDN",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "South-Eastern Asia",
       "region_wb": "East Asia & Pacific",
       "currency": "IDR",
       "latlon": [
          -5,
          120
       ]
   },
   {
       "iso_n3": 356,
       "sovereignt": "India",
       "sov_a3": "IND",
       "admin": "India",
       "adm0_a3": "IND",
       "geounit": "India",
       "gu_a3": "IND",
       "subunit": "India",
       "su_a3": "IND",
       "name": "India",
       "name_long": "India",
       "brk_a3": "IND",
       "brk_name": "India",
       "abbrev": "India",
       "postal": "IND",
       "formal_en": "Republic of India",
       "iso_a2": "IN",
       "iso_a3": "IND",
       "un_a3": 356,
       "wb_a2": "IN",
       "wb_a3": "IND",
       "adm0_a3_is": "IND",
       "adm0_a3_us": "IND",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Southern Asia",
       "region_wb": "South Asia",
       "currency": "INR",
       "flagHeightMultiplier": 0.7,
       "latlon": [
          20,
          77
       ]
   },
   {
       "iso_n3": 372,
       "sovereignt": "Ireland",
       "sov_a3": "IRL",
       "admin": "Ireland",
       "adm0_a3": "IRL",
       "geounit": "Ireland",
       "gu_a3": "IRL",
       "subunit": "Ireland",
       "su_a3": "IRL",
       "name": "Ireland",
       "name_long": "Ireland",
       "brk_a3": "IRL",
       "brk_name": "Ireland",
       "abbrev": "Ire.",
       "postal": "IRL",
       "formal_en": "Ireland",
       "iso_a2": "IE",
       "iso_a3": "IRL",
       "un_a3": 372,
       "wb_a2": "IE",
       "wb_a3": "IRL",
       "adm0_a3_is": "IRL",
       "adm0_a3_us": "IRL",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Northern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "EUR",
       "latlon": [
          53,
          -8
       ]
   },
   {
       "iso_n3": 364,
       "sovereignt": "Iran",
       "sov_a3": "IRN",
       "admin": "Iran",
       "adm0_a3": "IRN",
       "geounit": "Iran",
       "gu_a3": "IRN",
       "subunit": "Iran",
       "su_a3": "IRN",
       "name": "Iran",
       "name_long": "Iran",
       "brk_a3": "IRN",
       "brk_name": "Iran",
       "abbrev": "Iran",
       "postal": "IRN",
       "formal_en": "Islamic Republic of Iran",
       "iso_a2": "IR",
       "iso_a3": "IRN",
       "un_a3": 364,
       "wb_a2": "IR",
       "wb_a3": "IRN",
       "adm0_a3_is": "IRN",
       "adm0_a3_us": "IRN",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Southern Asia",
       "region_wb": "Middle East & North Africa",
       "currency": "IRR",
       "latlon": [
          32,
          53
       ]
   },
   {
       "iso_n3": 368,
       "sovereignt": "Iraq",
       "sov_a3": "IRQ",
       "admin": "Iraq",
       "adm0_a3": "IRQ",
       "geounit": "Iraq",
       "gu_a3": "IRQ",
       "subunit": "Iraq",
       "su_a3": "IRQ",
       "name": "Iraq",
       "name_long": "Iraq",
       "brk_a3": "IRQ",
       "brk_name": "Iraq",
       "abbrev": "Iraq",
       "postal": "IRQ",
       "formal_en": "Republic of Iraq",
       "iso_a2": "IQ",
       "iso_a3": "IRQ",
       "un_a3": 368,
       "wb_a2": "IQ",
       "wb_a3": "IRQ",
       "adm0_a3_is": "IRQ",
       "adm0_a3_us": "IRQ",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Middle East & North Africa",
       "currency": "IQD",
       "latlon": [
          33,
          44
       ]
   },
   {
       "iso_n3": 352,
       "sovereignt": "Iceland",
       "sov_a3": "ISL",
       "admin": "Iceland",
       "adm0_a3": "ISL",
       "geounit": "Iceland",
       "gu_a3": "ISL",
       "subunit": "Iceland",
       "su_a3": "ISL",
       "name": "Iceland",
       "name_long": "Iceland",
       "brk_a3": "ISL",
       "brk_name": "Iceland",
       "abbrev": "Iceland",
       "postal": "IS",
       "formal_en": "Republic of Iceland",
       "iso_a2": "IS",
       "iso_a3": "ISL",
       "un_a3": 352,
       "wb_a2": "IS",
       "wb_a3": "ISL",
       "adm0_a3_is": "ISL",
       "adm0_a3_us": "ISL",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Northern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "ISK",
       "latlon": [
          65,
          -18
       ]
   },
   {
       "iso_n3": 376,
       "sovereignt": "Israel",
       "sov_a3": "ISR",
       "admin": "Israel",
       "adm0_a3": "ISR",
       "geounit": "Israel",
       "gu_a3": "ISR",
       "subunit": "Israel",
       "su_a3": "ISR",
       "name": "Israel",
       "name_long": "Israel",
       "brk_a3": "ISR",
       "brk_name": "Israel",
       "abbrev": "Isr.",
       "postal": "IS",
       "formal_en": "State of Israel",
       "iso_a2": "IL",
       "iso_a3": "ISR",
       "un_a3": 376,
       "wb_a2": "IL",
       "wb_a3": "ISR",
       "adm0_a3_is": "ISR",
       "adm0_a3_us": "ISR",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Middle East & North Africa",
       "currency": "ILS",
       "latlon": [
          31.47,
          35.13
       ]
   },
   {
       "iso_n3": 380,
       "sovereignt": "Italy",
       "sov_a3": "ITA",
       "admin": "Italy",
       "adm0_a3": "ITA",
       "geounit": "Italy",
       "gu_a3": "ITA",
       "subunit": "Italy",
       "su_a3": "ITA",
       "name": "Italy",
       "name_long": "Italy",
       "brk_a3": "ITA",
       "brk_name": "Italy",
       "abbrev": "Italy",
       "postal": "I",
       "formal_en": "Italian Republic",
       "iso_a2": "IT",
       "iso_a3": "ITA",
       "un_a3": 380,
       "wb_a2": "IT",
       "wb_a3": "ITA",
       "adm0_a3_is": "ITA",
       "adm0_a3_us": "ITA",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Southern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "EUR",
       "latlon": [
          42.83333333,
          12.83333333
       ]
   },
   {
       "iso_n3": 388,
       "sovereignt": "Jamaica",
       "sov_a3": "JAM",
       "admin": "Jamaica",
       "adm0_a3": "JAM",
       "geounit": "Jamaica",
       "gu_a3": "JAM",
       "subunit": "Jamaica",
       "su_a3": "JAM",
       "name": "Jamaica",
       "name_long": "Jamaica",
       "brk_a3": "JAM",
       "brk_name": "Jamaica",
       "abbrev": "Jam.",
       "postal": "J",
       "formal_en": "Jamaica",
       "iso_a2": "JM",
       "iso_a3": "JAM",
       "un_a3": 388,
       "wb_a2": "JM",
       "wb_a3": "JAM",
       "adm0_a3_is": "JAM",
       "adm0_a3_us": "JAM",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Caribbean",
       "region_wb": "Latin America & Caribbean",
       "currency": "JMD",
       "latlon": [
          18.25,
          -77.5
       ]
   },
   {
       "iso_n3": 400,
       "sovereignt": "Jordan",
       "sov_a3": "JOR",
       "admin": "Jordan",
       "adm0_a3": "JOR",
       "geounit": "Jordan",
       "gu_a3": "JOR",
       "subunit": "Jordan",
       "su_a3": "JOR",
       "name": "Jordan",
       "name_long": "Jordan",
       "brk_a3": "JOR",
       "brk_name": "Jordan",
       "abbrev": "Jord.",
       "postal": "J",
       "formal_en": "Hashemite Kingdom of Jordan",
       "iso_a2": "JO",
       "iso_a3": "JOR",
       "un_a3": 400,
       "wb_a2": "JO",
       "wb_a3": "JOR",
       "adm0_a3_is": "JOR",
       "adm0_a3_us": "JOR",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Middle East & North Africa",
       "currency": "JOD",
       "latlon": [
          31,
          36
       ]
   },
   {
       "iso_n3": 392,
       "sovereignt": "Japan",
       "sov_a3": "JPN",
       "admin": "Japan",
       "adm0_a3": "JPN",
       "geounit": "Japan",
       "gu_a3": "JPN",
       "subunit": "Japan",
       "su_a3": "JPN",
       "name": "Japan",
       "name_long": "Japan",
       "brk_a3": "JPN",
       "brk_name": "Japan",
       "abbrev": "Japan",
       "postal": "J",
       "formal_en": "Japan",
       "iso_a2": "JP",
       "iso_a3": "JPN",
       "un_a3": 392,
       "wb_a2": "JP",
       "wb_a3": "JPN",
       "adm0_a3_is": "JPN",
       "adm0_a3_us": "JPN",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Eastern Asia",
       "region_wb": "East Asia & Pacific",
       "currency": "JPY",
       "flagHeightMultiplier": 1.65,
       "latlon": [
          36,
          138
       ]
   },
   {
       "iso_n3": 398,
       "sovereignt": "Kazakhstan",
       "sov_a3": "KAZ",
       "admin": "Kazakhstan",
       "adm0_a3": "KAZ",
       "geounit": "Kazakhstan",
       "gu_a3": "KAZ",
       "subunit": "Kazakhstan",
       "su_a3": "KAZ",
       "name": "Kazakhstan",
       "name_long": "Kazakhstan",
       "brk_a3": "KAZ",
       "brk_name": "Kazakhstan",
       "abbrev": "Kaz.",
       "postal": "KZ",
       "formal_en": "Republic of Kazakhstan",
       "iso_a2": "KZ",
       "iso_a3": "KAZ",
       "un_a3": 398,
       "wb_a2": "KZ",
       "wb_a3": "KAZ",
       "adm0_a3_is": "KAZ",
       "adm0_a3_us": "KAZ",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Central Asia",
       "region_wb": "Europe & Central Asia",
       "currency": "KZT",
       "latlon": [
          48,
          68
       ]
   },
   {
       "iso_n3": 404,
       "sovereignt": "Kenya",
       "sov_a3": "KEN",
       "admin": "Kenya",
       "adm0_a3": "KEN",
       "geounit": "Kenya",
       "gu_a3": "KEN",
       "subunit": "Kenya",
       "su_a3": "KEN",
       "name": "Kenya",
       "name_long": "Kenya",
       "brk_a3": "KEN",
       "brk_name": "Kenya",
       "abbrev": "Ken.",
       "postal": "KE",
       "formal_en": "Republic of Kenya",
       "iso_a2": "KE",
       "iso_a3": "KEN",
       "un_a3": 404,
       "wb_a2": "KE",
       "wb_a3": "KEN",
       "adm0_a3_is": "KEN",
       "adm0_a3_us": "KEN",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Eastern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "KES",
       "latlon": [
          1,
          38
       ]
   },
   {
       "iso_n3": 417,
       "sovereignt": "Kyrgyzstan",
       "sov_a3": "KGZ",
       "admin": "Kyrgyzstan",
       "adm0_a3": "KGZ",
       "geounit": "Kyrgyzstan",
       "gu_a3": "KGZ",
       "subunit": "Kyrgyzstan",
       "su_a3": "KGZ",
       "name": "Kyrgyzstan",
       "name_long": "Kyrgyzstan",
       "brk_a3": "KGZ",
       "brk_name": "Kyrgyzstan",
       "abbrev": "Kgz.",
       "postal": "KG",
       "formal_en": "Kyrgyz Republic",
       "iso_a2": "KG",
       "iso_a3": "KGZ",
       "un_a3": 417,
       "wb_a2": "KG",
       "wb_a3": "KGZ",
       "adm0_a3_is": "KGZ",
       "adm0_a3_us": "KGZ",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Central Asia",
       "region_wb": "Europe & Central Asia",
       "currency": "KGS",
       "latlon": [
          41,
          75
       ]
   },
   {
       "iso_n3": 116,
       "sovereignt": "Cambodia",
       "sov_a3": "KHM",
       "admin": "Cambodia",
       "adm0_a3": "KHM",
       "geounit": "Cambodia",
       "gu_a3": "KHM",
       "subunit": "Cambodia",
       "su_a3": "KHM",
       "name": "Cambodia",
       "name_long": "Cambodia",
       "brk_a3": "KHM",
       "brk_name": "Cambodia",
       "abbrev": "Camb.",
       "postal": "KH",
       "formal_en": "Kingdom of Cambodia",
       "iso_a2": "KH",
       "iso_a3": "KHM",
       "un_a3": 116,
       "wb_a2": "KH",
       "wb_a3": "KHM",
       "adm0_a3_is": "KHM",
       "adm0_a3_us": "KHM",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "South-Eastern Asia",
       "region_wb": "East Asia & Pacific",
       "currency": "KHR",
       "latlon": [
          13,
          105
       ]
   },
   {
       "iso_n3": 410,
       "sovereignt": "South Korea",
       "sov_a3": "KOR",
       "admin": "South Korea",
       "adm0_a3": "KOR",
       "geounit": "South Korea",
       "gu_a3": "KOR",
       "subunit": "South Korea",
       "su_a3": "KOR",
       "name": "Korea",
       "name_long": "Republic of Korea",
       "brk_a3": "KOR",
       "brk_name": "Republic of Korea",
       "abbrev": "S.K.",
       "postal": "KR",
       "formal_en": "Republic of Korea",
       "iso_a2": "KR",
       "iso_a3": "KOR",
       "un_a3": 410,
       "wb_a2": "KR",
       "wb_a3": "KOR",
       "adm0_a3_is": "KOR",
       "adm0_a3_us": "KOR",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Eastern Asia",
       "region_wb": "East Asia & Pacific",
       "currency": "KRW",
       "latlon": [
          37,
          127.5
       ]
   },
   {
       "iso_n3": 414,
       "sovereignt": "Kuwait",
       "sov_a3": "KWT",
       "admin": "Kuwait",
       "adm0_a3": "KWT",
       "geounit": "Kuwait",
       "gu_a3": "KWT",
       "subunit": "Kuwait",
       "su_a3": "KWT",
       "name": "Kuwait",
       "name_long": "Kuwait",
       "brk_a3": "KWT",
       "brk_name": "Kuwait",
       "abbrev": "Kwt.",
       "postal": "KW",
       "formal_en": "State of Kuwait",
       "iso_a2": "KW",
       "iso_a3": "KWT",
       "un_a3": 414,
       "wb_a2": "KW",
       "wb_a3": "KWT",
       "adm0_a3_is": "KWT",
       "adm0_a3_us": "KWT",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Middle East & North Africa",
       "currency": "KWD",
       "latlon": [
          29.5,
          45.75
       ]
   },
   {
       "iso_n3": 418,
       "sovereignt": "Laos",
       "sov_a3": "LAO",
       "admin": "Laos",
       "adm0_a3": "LAO",
       "geounit": "Laos",
       "gu_a3": "LAO",
       "subunit": "Laos",
       "su_a3": "LAO",
       "name": "Lao PDR",
       "name_long": "Lao PDR",
       "brk_a3": "LAO",
       "brk_name": "Laos",
       "abbrev": "Laos",
       "postal": "LA",
       "formal_en": "Lao People's Democratic Republic",
       "iso_a2": "LA",
       "iso_a3": "LAO",
       "un_a3": 418,
       "wb_a2": "LA",
       "wb_a3": "LAO",
       "adm0_a3_is": "LAO",
       "adm0_a3_us": "LAO",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "South-Eastern Asia",
       "region_wb": "East Asia & Pacific",
       "currency": "LAK",
       "latlon": [
          18,
          105
       ]
   },
   {
       "iso_n3": 422,
       "sovereignt": "Lebanon",
       "sov_a3": "LBN",
       "admin": "Lebanon",
       "adm0_a3": "LBN",
       "geounit": "Lebanon",
       "gu_a3": "LBN",
       "subunit": "Lebanon",
       "su_a3": "LBN",
       "name": "Lebanon",
       "name_long": "Lebanon",
       "brk_a3": "LBN",
       "brk_name": "Lebanon",
       "abbrev": "Leb.",
       "postal": "LB",
       "formal_en": "Lebanese Republic",
       "iso_a2": "LB",
       "iso_a3": "LBN",
       "un_a3": 422,
       "wb_a2": "LB",
       "wb_a3": "LBN",
       "adm0_a3_is": "LBN",
       "adm0_a3_us": "LBN",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Middle East & North Africa",
       "currency": "LBP",
       "latlon": [
          33.83333333,
          35.83333333
       ]
   },
   {
       "iso_n3": 430,
       "sovereignt": "Liberia",
       "sov_a3": "LBR",
       "admin": "Liberia",
       "adm0_a3": "LBR",
       "geounit": "Liberia",
       "gu_a3": "LBR",
       "subunit": "Liberia",
       "su_a3": "LBR",
       "name": "Liberia",
       "name_long": "Liberia",
       "brk_a3": "LBR",
       "brk_name": "Liberia",
       "abbrev": "Liberia",
       "postal": "LR",
       "formal_en": "Republic of Liberia",
       "iso_a2": "LR",
       "iso_a3": "LBR",
       "un_a3": 430,
       "wb_a2": "LR",
       "wb_a3": "LBR",
       "adm0_a3_is": "LBR",
       "adm0_a3_us": "LBR",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Western Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "LRD",
       "latlon": [
          6.5,
          -9.5
       ]
   },
   {
       "iso_n3": 434,
       "sovereignt": "Libya",
       "sov_a3": "LBY",
       "admin": "Libya",
       "adm0_a3": "LBY",
       "geounit": "Libya",
       "gu_a3": "LBY",
       "subunit": "Libya",
       "su_a3": "LBY",
       "name": "Libya",
       "name_long": "Libya",
       "brk_a3": "LBY",
       "brk_name": "Libya",
       "abbrev": "Libya",
       "postal": "LY",
       "formal_en": "Libya",
       "iso_a2": "LY",
       "iso_a3": "LBY",
       "un_a3": 434,
       "wb_a2": "LY",
       "wb_a3": "LBY",
       "adm0_a3_is": "LBY",
       "adm0_a3_us": "LBY",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Northern Africa",
       "region_wb": "Middle East & North Africa",
       "currency": "LYD",
       "latlon": [
          25,
          17
       ]
   },
   {
       "iso_n3": 144,
       "sovereignt": "Sri Lanka",
       "sov_a3": "LKA",
       "admin": "Sri Lanka",
       "adm0_a3": "LKA",
       "geounit": "Sri Lanka",
       "gu_a3": "LKA",
       "subunit": "Sri Lanka",
       "su_a3": "LKA",
       "name": "Sri Lanka",
       "name_long": "Sri Lanka",
       "brk_a3": "LKA",
       "brk_name": "Sri Lanka",
       "abbrev": "Sri L.",
       "postal": "LK",
       "formal_en": "Democratic Socialist Republic of Sri Lanka",
       "iso_a2": "LK",
       "iso_a3": "LKA",
       "un_a3": 144,
       "wb_a2": "LK",
       "wb_a3": "LKA",
       "adm0_a3_is": "LKA",
       "adm0_a3_us": "LKA",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Southern Asia",
       "region_wb": "South Asia",
       "currency": "LKR",
       "latlon": [
          7,
          81
       ]
   },
   {
       "iso_n3": 426,
       "sovereignt": "Lesotho",
       "sov_a3": "LSO",
       "admin": "Lesotho",
       "adm0_a3": "LSO",
       "geounit": "Lesotho",
       "gu_a3": "LSO",
       "subunit": "Lesotho",
       "su_a3": "LSO",
       "name": "Lesotho",
       "name_long": "Lesotho",
       "brk_a3": "LSO",
       "brk_name": "Lesotho",
       "abbrev": "Les.",
       "postal": "LS",
       "formal_en": "Kingdom of Lesotho",
       "iso_a2": "LS",
       "iso_a3": "LSO",
       "un_a3": 426,
       "wb_a2": "LS",
       "wb_a3": "LSO",
       "adm0_a3_is": "LSO",
       "adm0_a3_us": "LSO",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Southern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "LSL",
       "latlon": [
          -29.5,
          28.5
       ]
   },
   {
       "iso_n3": 440,
       "sovereignt": "Lithuania",
       "sov_a3": "LTU",
       "admin": "Lithuania",
       "adm0_a3": "LTU",
       "geounit": "Lithuania",
       "gu_a3": "LTU",
       "subunit": "Lithuania",
       "su_a3": "LTU",
       "name": "Lithuania",
       "name_long": "Lithuania",
       "brk_a3": "LTU",
       "brk_name": "Lithuania",
       "abbrev": "Lith.",
       "postal": "LT",
       "formal_en": "Republic of Lithuania",
       "iso_a2": "LT",
       "iso_a3": "LTU",
       "un_a3": 440,
       "wb_a2": "LT",
       "wb_a3": "LTU",
       "adm0_a3_is": "LTU",
       "adm0_a3_us": "LTU",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Northern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "EUR",
       "latlon": [
          56,
          24
       ]
   },
   {
       "iso_n3": 442,
       "sovereignt": "Luxembourg",
       "sov_a3": "LUX",
       "admin": "Luxembourg",
       "adm0_a3": "LUX",
       "geounit": "Luxembourg",
       "gu_a3": "LUX",
       "subunit": "Luxembourg",
       "su_a3": "LUX",
       "name": "Luxembourg",
       "name_long": "Luxembourg",
       "brk_a3": "LUX",
       "brk_name": "Luxembourg",
       "abbrev": "Lux.",
       "postal": "L",
       "formal_en": "Grand Duchy of Luxembourg",
       "iso_a2": "LU",
       "iso_a3": "LUX",
       "un_a3": 442,
       "wb_a2": "LU",
       "wb_a3": "LUX",
       "adm0_a3_is": "LUX",
       "adm0_a3_us": "LUX",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Western Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "EUR",
       "latlon": [
          49.75,
          6.16666666
       ]
   },
   {
       "iso_n3": 428,
       "sovereignt": "Latvia",
       "sov_a3": "LVA",
       "admin": "Latvia",
       "adm0_a3": "LVA",
       "geounit": "Latvia",
       "gu_a3": "LVA",
       "subunit": "Latvia",
       "su_a3": "LVA",
       "name": "Latvia",
       "name_long": "Latvia",
       "brk_a3": "LVA",
       "brk_name": "Latvia",
       "abbrev": "Lat.",
       "postal": "LV",
       "formal_en": "Republic of Latvia",
       "iso_a2": "LV",
       "iso_a3": "LVA",
       "un_a3": 428,
       "wb_a2": "LV",
       "wb_a3": "LVA",
       "adm0_a3_is": "LVA",
       "adm0_a3_us": "LVA",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Northern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "EUR",
       "latlon": [
          57,
          25
       ]
   },
   {
       "iso_n3": 504,
       "sovereignt": "Morocco",
       "sov_a3": "MAR",
       "admin": "Morocco",
       "adm0_a3": "MAR",
       "geounit": "Morocco",
       "gu_a3": "MAR",
       "subunit": "Morocco",
       "su_a3": "MAR",
       "name": "Morocco",
       "name_long": "Morocco",
       "brk_a3": "MAR",
       "brk_name": "Morocco",
       "abbrev": "Mor.",
       "postal": "MA",
       "formal_en": "Kingdom of Morocco",
       "iso_a2": "MA",
       "iso_a3": "MAR",
       "un_a3": 504,
       "wb_a2": "MA",
       "wb_a3": "MAR",
       "adm0_a3_is": "MAR",
       "adm0_a3_us": "MAR",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Northern Africa",
       "region_wb": "Middle East & North Africa",
       "currency": "MAD",
       "latlon": [
          32,
          -5
       ]
   },
   {
       "iso_n3": 498,
       "sovereignt": "Moldova",
       "sov_a3": "MDA",
       "admin": "Moldova",
       "adm0_a3": "MDA",
       "geounit": "Moldova",
       "gu_a3": "MDA",
       "subunit": "Moldova",
       "su_a3": "MDA",
       "name": "Moldova",
       "name_long": "Moldova",
       "brk_a3": "MDA",
       "brk_name": "Moldova",
       "abbrev": "Mda.",
       "postal": "MD",
       "formal_en": "Republic of Moldova",
       "iso_a2": "MD",
       "iso_a3": "MDA",
       "un_a3": 498,
       "wb_a2": "MD",
       "wb_a3": "MDA",
       "adm0_a3_is": "MDA",
       "adm0_a3_us": "MDA",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Eastern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "MDL",
       "latlon": [
          47,
          29
       ]
   },
   {
       "iso_n3": 450,
       "sovereignt": "Madagascar",
       "sov_a3": "MDG",
       "admin": "Madagascar",
       "adm0_a3": "MDG",
       "geounit": "Madagascar",
       "gu_a3": "MDG",
       "subunit": "Madagascar",
       "su_a3": "MDG",
       "name": "Madagascar",
       "name_long": "Madagascar",
       "brk_a3": "MDG",
       "brk_name": "Madagascar",
       "abbrev": "Mad.",
       "postal": "MG",
       "formal_en": "Republic of Madagascar",
       "iso_a2": "MG",
       "iso_a3": "MDG",
       "un_a3": 450,
       "wb_a2": "MG",
       "wb_a3": "MDG",
       "adm0_a3_is": "MDG",
       "adm0_a3_us": "MDG",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Eastern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "MGA",
       "latlon": [
          -20,
          47
       ]
   },
   {
       "iso_n3": 484,
       "sovereignt": "Mexico",
       "sov_a3": "MEX",
       "admin": "Mexico",
       "adm0_a3": "MEX",
       "geounit": "Mexico",
       "gu_a3": "MEX",
       "subunit": "Mexico",
       "su_a3": "MEX",
       "name": "Mexico",
       "name_long": "Mexico",
       "brk_a3": "MEX",
       "brk_name": "Mexico",
       "abbrev": "Mex.",
       "postal": "MX",
       "formal_en": "United Mexican States",
       "iso_a2": "MX",
       "iso_a3": "MEX",
       "un_a3": 484,
       "wb_a2": "MX",
       "wb_a3": "MEX",
       "adm0_a3_is": "MEX",
       "adm0_a3_us": "MEX",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Central America",
       "region_wb": "Latin America & Caribbean",
       "currency": "MXN",
       "latlon": [
          23,
          -102
       ]
   },
   {
       "iso_n3": 807,
       "sovereignt": "Macedonia",
       "sov_a3": "MKD",
       "admin": "Macedonia",
       "adm0_a3": "MKD",
       "geounit": "Macedonia",
       "gu_a3": "MKD",
       "subunit": "Macedonia",
       "su_a3": "MKD",
       "name": "Macedonia",
       "name_long": "Macedonia",
       "brk_a3": "MKD",
       "brk_name": "Macedonia",
       "abbrev": "Mkd.",
       "postal": "MK",
       "formal_en": "Former Yugoslav Republic of Macedonia",
       "iso_a2": "MK",
       "iso_a3": "MKD",
       "un_a3": 807,
       "wb_a2": "MK",
       "wb_a3": "MKD",
       "adm0_a3_is": "MKD",
       "adm0_a3_us": "MKD",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Southern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "MKD",
       "latlon": [
          41.83333333,
          22
       ]
   },
   {
       "iso_n3": 466,
       "sovereignt": "Mali",
       "sov_a3": "MLI",
       "admin": "Mali",
       "adm0_a3": "MLI",
       "geounit": "Mali",
       "gu_a3": "MLI",
       "subunit": "Mali",
       "su_a3": "MLI",
       "name": "Mali",
       "name_long": "Mali",
       "brk_a3": "MLI",
       "brk_name": "Mali",
       "abbrev": "Mali",
       "postal": "ML",
       "formal_en": "Republic of Mali",
       "iso_a2": "ML",
       "iso_a3": "MLI",
       "un_a3": 466,
       "wb_a2": "ML",
       "wb_a3": "MLI",
       "adm0_a3_is": "MLI",
       "adm0_a3_us": "MLI",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Western Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "XOF",
       "latlon": [
          17,
          -4
       ]
   },
   {
       "iso_n3": 104,
       "sovereignt": "Myanmar",
       "sov_a3": "MMR",
       "admin": "Myanmar",
       "adm0_a3": "MMR",
       "geounit": "Myanmar",
       "gu_a3": "MMR",
       "subunit": "Myanmar",
       "su_a3": "MMR",
       "name": "Myanmar",
       "name_long": "Myanmar",
       "brk_a3": "MMR",
       "brk_name": "Myanmar",
       "abbrev": "Myan.",
       "postal": "MM",
       "formal_en": "Republic of the Union of Myanmar",
       "iso_a2": "MM",
       "iso_a3": "MMR",
       "un_a3": 104,
       "wb_a2": "MM",
       "wb_a3": "MMR",
       "adm0_a3_is": "MMR",
       "adm0_a3_us": "MMR",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "South-Eastern Asia",
       "region_wb": "East Asia & Pacific",
       "currency": "MMK",
       "latlon": [
          22,
          98
       ]
   },
   {
       "iso_n3": 499,
       "sovereignt": "Montenegro",
       "sov_a3": "MNE",
       "admin": "Montenegro",
       "adm0_a3": "MNE",
       "geounit": "Montenegro",
       "gu_a3": "MNE",
       "subunit": "Montenegro",
       "su_a3": "MNE",
       "name": "Montenegro",
       "name_long": "Montenegro",
       "brk_a3": "MNE",
       "brk_name": "Montenegro",
       "abbrev": "Mont.",
       "postal": "ME",
       "formal_en": "Montenegro",
       "iso_a2": "ME",
       "iso_a3": "MNE",
       "un_a3": 499,
       "wb_a2": "ME",
       "wb_a3": "MNE",
       "adm0_a3_is": "MNE",
       "adm0_a3_us": "MNE",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Southern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "EUR",
       "latlon": [
          42.5,
          19.3
       ]
   },
   {
       "iso_n3": 496,
       "sovereignt": "Mongolia",
       "sov_a3": "MNG",
       "admin": "Mongolia",
       "adm0_a3": "MNG",
       "geounit": "Mongolia",
       "gu_a3": "MNG",
       "subunit": "Mongolia",
       "su_a3": "MNG",
       "name": "Mongolia",
       "name_long": "Mongolia",
       "brk_a3": "MNG",
       "brk_name": "Mongolia",
       "abbrev": "Mong.",
       "postal": "MN",
       "formal_en": "Mongolia",
       "iso_a2": "MN",
       "iso_a3": "MNG",
       "un_a3": 496,
       "wb_a2": "MN",
       "wb_a3": "MNG",
       "adm0_a3_is": "MNG",
       "adm0_a3_us": "MNG",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Eastern Asia",
       "region_wb": "East Asia & Pacific",
       "currency": "MNT",
       "latlon": [
          46,
          105
       ]
   },
   {
       "iso_n3": 508,
       "sovereignt": "Mozambique",
       "sov_a3": "MOZ",
       "admin": "Mozambique",
       "adm0_a3": "MOZ",
       "geounit": "Mozambique",
       "gu_a3": "MOZ",
       "subunit": "Mozambique",
       "su_a3": "MOZ",
       "name": "Mozambique",
       "name_long": "Mozambique",
       "brk_a3": "MOZ",
       "brk_name": "Mozambique",
       "abbrev": "Moz.",
       "postal": "MZ",
       "formal_en": "Republic of Mozambique",
       "iso_a2": "MZ",
       "iso_a3": "MOZ",
       "un_a3": 508,
       "wb_a2": "MZ",
       "wb_a3": "MOZ",
       "adm0_a3_is": "MOZ",
       "adm0_a3_us": "MOZ",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Eastern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "MZN",
       "latlon": [
          -18.25,
          35
       ]
   },
   {
       "iso_n3": 478,
       "sovereignt": "Mauritania",
       "sov_a3": "MRT",
       "admin": "Mauritania",
       "adm0_a3": "MRT",
       "geounit": "Mauritania",
       "gu_a3": "MRT",
       "subunit": "Mauritania",
       "su_a3": "MRT",
       "name": "Mauritania",
       "name_long": "Mauritania",
       "brk_a3": "MRT",
       "brk_name": "Mauritania",
       "abbrev": "Mrt.",
       "postal": "MR",
       "formal_en": "Islamic Republic of Mauritania",
       "iso_a2": "MR",
       "iso_a3": "MRT",
       "un_a3": 478,
       "wb_a2": "MR",
       "wb_a3": "MRT",
       "adm0_a3_is": "MRT",
       "adm0_a3_us": "MRT",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Western Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "MRU",
       "latlon": [
          20,
          -12
       ]
   },
   {
       "iso_n3": 454,
       "sovereignt": "Malawi",
       "sov_a3": "MWI",
       "admin": "Malawi",
       "adm0_a3": "MWI",
       "geounit": "Malawi",
       "gu_a3": "MWI",
       "subunit": "Malawi",
       "su_a3": "MWI",
       "name": "Malawi",
       "name_long": "Malawi",
       "brk_a3": "MWI",
       "brk_name": "Malawi",
       "abbrev": "Mal.",
       "postal": "MW",
       "formal_en": "Republic of Malawi",
       "iso_a2": "MW",
       "iso_a3": "MWI",
       "un_a3": 454,
       "wb_a2": "MW",
       "wb_a3": "MWI",
       "adm0_a3_is": "MWI",
       "adm0_a3_us": "MWI",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Eastern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "MWK",
       "latlon": [
          -13.5,
          34
       ]
   },
   {
       "iso_n3": 458,
       "sovereignt": "Malaysia",
       "sov_a3": "MYS",
       "admin": "Malaysia",
       "adm0_a3": "MYS",
       "geounit": "Malaysia",
       "gu_a3": "MYS",
       "subunit": "Malaysia",
       "su_a3": "MYS",
       "name": "Malaysia",
       "name_long": "Malaysia",
       "brk_a3": "MYS",
       "brk_name": "Malaysia",
       "abbrev": "Malay.",
       "postal": "MY",
       "formal_en": "Malaysia",
       "iso_a2": "MY",
       "iso_a3": "MYS",
       "un_a3": 458,
       "wb_a2": "MY",
       "wb_a3": "MYS",
       "adm0_a3_is": "MYS",
       "adm0_a3_us": "MYS",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "South-Eastern Asia",
       "region_wb": "East Asia & Pacific",
       "currency": "MYR",
       "latlon": [
          2.5,
          112.5
       ]
   },
   {
       "iso_n3": 516,
       "sovereignt": "Namibia",
       "sov_a3": "NAM",
       "admin": "Namibia",
       "adm0_a3": "NAM",
       "geounit": "Namibia",
       "gu_a3": "NAM",
       "subunit": "Namibia",
       "su_a3": "NAM",
       "name": "Namibia",
       "name_long": "Namibia",
       "brk_a3": "NAM",
       "brk_name": "Namibia",
       "abbrev": "Nam.",
       "postal": "NA",
       "formal_en": "Republic of Namibia",
       "iso_a2": "NA",
       "iso_a3": "NAM",
       "un_a3": 516,
       "wb_a2": "NA",
       "wb_a3": "NAM",
       "adm0_a3_is": "NAM",
       "adm0_a3_us": "NAM",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Southern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "NAD",
       "latlon": [
          -22,
          17
       ]
   },
   {
       "iso_n3": 540,
       "sovereignt": "France",
       "sov_a3": "FR1",
       "admin": "New Caledonia",
       "adm0_a3": "NCL",
       "geounit": "New Caledonia",
       "gu_a3": "NCL",
       "subunit": "New Caledonia",
       "su_a3": "NCL",
       "name": "New Caledonia",
       "name_long": "New Caledonia",
       "brk_a3": "NCL",
       "brk_name": "New Caledonia",
       "abbrev": "New C.",
       "postal": "NC",
       "formal_en": "New Caledonia",
       "iso_a2": "NC",
       "iso_a3": "NCL",
       "un_a3": 540,
       "wb_a2": "NC",
       "wb_a3": "NCL",
       "adm0_a3_is": "NCL",
       "adm0_a3_us": "NCL",
       "continent": "Oceania",
       "region_un": "Oceania",
       "subregion": "Melanesia",
       "region_wb": "East Asia & Pacific",
       "currency": "XPF",
       "latlon": [
          -21.5,
          165.5
       ]
   },
   {
       "iso_n3": 562,
       "sovereignt": "Niger",
       "sov_a3": "NER",
       "admin": "Niger",
       "adm0_a3": "NER",
       "geounit": "Niger",
       "gu_a3": "NER",
       "subunit": "Niger",
       "su_a3": "NER",
       "name": "Niger",
       "name_long": "Niger",
       "brk_a3": "NER",
       "brk_name": "Niger",
       "abbrev": "Niger",
       "postal": "NE",
       "formal_en": "Republic of Niger",
       "iso_a2": "NE",
       "iso_a3": "NER",
       "un_a3": 562,
       "wb_a2": "NE",
       "wb_a3": "NER",
       "adm0_a3_is": "NER",
       "adm0_a3_us": "NER",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Western Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "XOF",
       "latlon": [
          16,
          8
       ]
   },
   {
       "iso_n3": 566,
       "sovereignt": "Nigeria",
       "sov_a3": "NGA",
       "admin": "Nigeria",
       "adm0_a3": "NGA",
       "geounit": "Nigeria",
       "gu_a3": "NGA",
       "subunit": "Nigeria",
       "su_a3": "NGA",
       "name": "Nigeria",
       "name_long": "Nigeria",
       "brk_a3": "NGA",
       "brk_name": "Nigeria",
       "abbrev": "Nigeria",
       "postal": "NG",
       "formal_en": "Federal Republic of Nigeria",
       "iso_a2": "NG",
       "iso_a3": "NGA",
       "un_a3": 566,
       "wb_a2": "NG",
       "wb_a3": "NGA",
       "adm0_a3_is": "NGA",
       "adm0_a3_us": "NGA",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Western Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "NGN",
       "latlon": [
          10,
          8
       ]
   },
   {
       "iso_n3": 558,
       "sovereignt": "Nicaragua",
       "sov_a3": "NIC",
       "admin": "Nicaragua",
       "adm0_a3": "NIC",
       "geounit": "Nicaragua",
       "gu_a3": "NIC",
       "subunit": "Nicaragua",
       "su_a3": "NIC",
       "name": "Nicaragua",
       "name_long": "Nicaragua",
       "brk_a3": "NIC",
       "brk_name": "Nicaragua",
       "abbrev": "Nic.",
       "postal": "NI",
       "formal_en": "Republic of Nicaragua",
       "iso_a2": "NI",
       "iso_a3": "NIC",
       "un_a3": 558,
       "wb_a2": "NI",
       "wb_a3": "NIC",
       "adm0_a3_is": "NIC",
       "adm0_a3_us": "NIC",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Central America",
       "region_wb": "Latin America & Caribbean",
       "currency": "NIO",
       "latlon": [
          13,
          -85
       ]
   },
   {
       "iso_n3": 528,
       "sovereignt": "Netherlands",
       "sov_a3": "NL1",
       "admin": "Netherlands",
       "adm0_a3": "NLD",
       "geounit": "Netherlands",
       "gu_a3": "NLD",
       "subunit": "Netherlands",
       "su_a3": "NLD",
       "name": "Netherlands",
       "name_long": "Netherlands",
       "brk_a3": "NLD",
       "brk_name": "Netherlands",
       "abbrev": "Neth.",
       "postal": "NL",
       "formal_en": "Kingdom of the Netherlands",
       "iso_a2": "NL",
       "iso_a3": "NLD",
       "un_a3": 528,
       "wb_a2": "NL",
       "wb_a3": "NLD",
       "adm0_a3_is": "NLD",
       "adm0_a3_us": "NLD",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Western Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "EUR",
       "latlon": [
          52.5,
          5.75
       ]
   },
   {
       "iso_n3": 578,
       "sovereignt": "Norway",
       "sov_a3": "NOR",
       "admin": "Norway",
       "adm0_a3": "NOR",
       "geounit": "Norway",
       "gu_a3": "NOR",
       "subunit": "Norway",
       "su_a3": "NOR",
       "name": "Norway",
       "name_long": "Norway",
       "brk_a3": "NOR",
       "brk_name": "Norway",
       "abbrev": "Nor.",
       "postal": "N",
       "formal_en": "Kingdom of Norway",
       "iso_a2": "NO",
       "iso_a3": "NOR",
       "un_a3": 578,
       "wb_a2": "NO",
       "wb_a3": "NOR",
       "adm0_a3_is": "NOR",
       "adm0_a3_us": "NOR",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Northern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "NOK",
       "latlon": [
          62,
          10
       ]
   },
   {
       "iso_n3": 524,
       "sovereignt": "Nepal",
       "sov_a3": "NPL",
       "admin": "Nepal",
       "adm0_a3": "NPL",
       "geounit": "Nepal",
       "gu_a3": "NPL",
       "subunit": "Nepal",
       "su_a3": "NPL",
       "name": "Nepal",
       "name_long": "Nepal",
       "brk_a3": "NPL",
       "brk_name": "Nepal",
       "abbrev": "Nepal",
       "postal": "NP",
       "formal_en": "Nepal",
       "iso_a2": "NP",
       "iso_a3": "NPL",
       "un_a3": 524,
       "wb_a2": "NP",
       "wb_a3": "NPL",
       "adm0_a3_is": "NPL",
       "adm0_a3_us": "NPL",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Southern Asia",
       "region_wb": "South Asia",
       "currency": "NPR",
       "latlon": [
          28,
          84
       ]
   },
   {
       "iso_n3": 554,
       "sovereignt": "New Zealand",
       "sov_a3": "NZ1",
       "admin": "New Zealand",
       "adm0_a3": "NZL",
       "geounit": "New Zealand",
       "gu_a3": "NZL",
       "subunit": "New Zealand",
       "su_a3": "NZL",
       "name": "New Zealand",
       "name_long": "New Zealand",
       "brk_a3": "NZL",
       "brk_name": "New Zealand",
       "abbrev": "N.Z.",
       "postal": "NZ",
       "formal_en": "New Zealand",
       "iso_a2": "NZ",
       "iso_a3": "NZL",
       "un_a3": 554,
       "wb_a2": "NZ",
       "wb_a3": "NZL",
       "adm0_a3_is": "NZL",
       "adm0_a3_us": "NZL",
       "continent": "Oceania",
       "region_un": "Oceania",
       "subregion": "Australia and New Zealand",
       "region_wb": "East Asia & Pacific",
       "currency": "NZD",
       "latlon": [
          -41,
          174
       ]
   },
   {
       "iso_n3": 512,
       "sovereignt": "Oman",
       "sov_a3": "OMN",
       "admin": "Oman",
       "adm0_a3": "OMN",
       "geounit": "Oman",
       "gu_a3": "OMN",
       "subunit": "Oman",
       "su_a3": "OMN",
       "name": "Oman",
       "name_long": "Oman",
       "brk_a3": "OMN",
       "brk_name": "Oman",
       "abbrev": "Oman",
       "postal": "OM",
       "formal_en": "Sultanate of Oman",
       "iso_a2": "OM",
       "iso_a3": "OMN",
       "un_a3": 512,
       "wb_a2": "OM",
       "wb_a3": "OMN",
       "adm0_a3_is": "OMN",
       "adm0_a3_us": "OMN",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Middle East & North Africa",
       "currency": "OMR",
       "latlon": [
          21,
          57
       ]
   },
   {
       "iso_n3": 586,
       "sovereignt": "Pakistan",
       "sov_a3": "PAK",
       "admin": "Pakistan",
       "adm0_a3": "PAK",
       "geounit": "Pakistan",
       "gu_a3": "PAK",
       "subunit": "Pakistan",
       "su_a3": "PAK",
       "name": "Pakistan",
       "name_long": "Pakistan",
       "brk_a3": "PAK",
       "brk_name": "Pakistan",
       "abbrev": "Pak.",
       "postal": "PK",
       "formal_en": "Islamic Republic of Pakistan",
       "iso_a2": "PK",
       "iso_a3": "PAK",
       "un_a3": 586,
       "wb_a2": "PK",
       "wb_a3": "PAK",
       "adm0_a3_is": "PAK",
       "adm0_a3_us": "PAK",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Southern Asia",
       "region_wb": "South Asia",
       "currency": "PKR",
       "latlon": [
          30,
          70
       ]
   },
   {
       "iso_n3": 591,
       "sovereignt": "Panama",
       "sov_a3": "PAN",
       "admin": "Panama",
       "adm0_a3": "PAN",
       "geounit": "Panama",
       "gu_a3": "PAN",
       "subunit": "Panama",
       "su_a3": "PAN",
       "name": "Panama",
       "name_long": "Panama",
       "brk_a3": "PAN",
       "brk_name": "Panama",
       "abbrev": "Pan.",
       "postal": "PA",
       "formal_en": "Republic of Panama",
       "iso_a2": "PA",
       "iso_a3": "PAN",
       "un_a3": 591,
       "wb_a2": "PA",
       "wb_a3": "PAN",
       "adm0_a3_is": "PAN",
       "adm0_a3_us": "PAN",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Central America",
       "region_wb": "Latin America & Caribbean",
       "currency": "PAB",
       "latlon": [
          9,
          -80
       ]
   },
   {
       "iso_n3": 604,
       "sovereignt": "Peru",
       "sov_a3": "PER",
       "admin": "Peru",
       "adm0_a3": "PER",
       "geounit": "Peru",
       "gu_a3": "PER",
       "subunit": "Peru",
       "su_a3": "PER",
       "name": "Peru",
       "name_long": "Peru",
       "brk_a3": "PER",
       "brk_name": "Peru",
       "abbrev": "Peru",
       "postal": "PE",
       "formal_en": "Republic of Peru",
       "iso_a2": "PE",
       "iso_a3": "PER",
       "un_a3": 604,
       "wb_a2": "PE",
       "wb_a3": "PER",
       "adm0_a3_is": "PER",
       "adm0_a3_us": "PER",
       "continent": "South America",
       "region_un": "Americas",
       "subregion": "South America",
       "region_wb": "Latin America & Caribbean",
       "currency": "PEN",
       "latlon": [
          -10,
          -76
       ]
   },
   {
       "iso_n3": 608,
       "sovereignt": "Philippines",
       "sov_a3": "PHL",
       "admin": "Philippines",
       "adm0_a3": "PHL",
       "geounit": "Philippines",
       "gu_a3": "PHL",
       "subunit": "Philippines",
       "su_a3": "PHL",
       "name": "Philippines",
       "name_long": "Philippines",
       "brk_a3": "PHL",
       "brk_name": "Philippines",
       "abbrev": "Phil.",
       "postal": "PH",
       "formal_en": "Republic of the Philippines",
       "iso_a2": "PH",
       "iso_a3": "PHL",
       "un_a3": 608,
       "wb_a2": "PH",
       "wb_a3": "PHL",
       "adm0_a3_is": "PHL",
       "adm0_a3_us": "PHL",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "South-Eastern Asia",
       "region_wb": "East Asia & Pacific",
       "currency": "PHP",
       "latlon": [
          13,
          122
       ]
   },
   {
       "iso_n3": 598,
       "sovereignt": "Papua New Guinea",
       "sov_a3": "PNG",
       "admin": "Papua New Guinea",
       "adm0_a3": "PNG",
       "geounit": "Papua New Guinea",
       "gu_a3": "PNG",
       "subunit": "Papua New Guinea",
       "su_a3": "PN1",
       "name": "Papua New Guinea",
       "name_long": "Papua New Guinea",
       "brk_a3": "PN1",
       "brk_name": "Papua New Guinea",
       "abbrev": "P.N.G.",
       "postal": "PG",
       "formal_en": "Independent State of Papua New Guinea",
       "iso_a2": "PG",
       "iso_a3": "PNG",
       "un_a3": 598,
       "wb_a2": "PG",
       "wb_a3": "PNG",
       "adm0_a3_is": "PNG",
       "adm0_a3_us": "PNG",
       "continent": "Oceania",
       "region_un": "Oceania",
       "subregion": "Melanesia",
       "region_wb": "East Asia & Pacific",
       "currency": "PGK",
       "latlon": [
          -6,
          147
       ]
   },
   {
       "iso_n3": 616,
       "sovereignt": "Poland",
       "sov_a3": "POL",
       "admin": "Poland",
       "adm0_a3": "POL",
       "geounit": "Poland",
       "gu_a3": "POL",
       "subunit": "Poland",
       "su_a3": "POL",
       "name": "Poland",
       "name_long": "Poland",
       "brk_a3": "POL",
       "brk_name": "Poland",
       "abbrev": "Pol.",
       "postal": "PL",
       "formal_en": "Republic of Poland",
       "iso_a2": "PL",
       "iso_a3": "POL",
       "un_a3": 616,
       "wb_a2": "PL",
       "wb_a3": "POL",
       "adm0_a3_is": "POL",
       "adm0_a3_us": "POL",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Eastern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "PLN",
       "latlon": [
          52,
          20
       ]
   },
   {
       "iso_n3": 630,
       "sovereignt": "United States of America",
       "sov_a3": "US1",
       "admin": "Puerto Rico",
       "adm0_a3": "PRI",
       "geounit": "Puerto Rico",
       "gu_a3": "PRI",
       "subunit": "Puerto Rico",
       "su_a3": "PRI",
       "name": "Puerto Rico",
       "name_long": "Puerto Rico",
       "brk_a3": "PRI",
       "brk_name": "Puerto Rico",
       "abbrev": "P.R.",
       "postal": "PR",
       "formal_en": "Commonwealth of Puerto Rico",
       "iso_a2": "PR",
       "iso_a3": "PRI",
       "un_a3": 630,
       "wb_a2": "PR",
       "wb_a3": "PRI",
       "adm0_a3_is": "PRI",
       "adm0_a3_us": "PRI",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Caribbean",
       "region_wb": "Latin America & Caribbean",
       "currency": "USD_DISABLED",
       "latlon": [
          18.25,
          -66.5
       ]
   },
   {
       "iso_n3": 408,
       "sovereignt": "North Korea",
       "sov_a3": "PRK",
       "admin": "North Korea",
       "adm0_a3": "PRK",
       "geounit": "North Korea",
       "gu_a3": "PRK",
       "subunit": "North Korea",
       "su_a3": "PRK",
       "name": "Dem. Rep. Korea",
       "name_long": "Dem. Rep. Korea",
       "brk_a3": "PRK",
       "brk_name": "Dem. Rep. Korea",
       "abbrev": "N.K.",
       "postal": "KP",
       "formal_en": "Democratic People's Republic of Korea",
       "iso_a2": "KP",
       "iso_a3": "PRK",
       "un_a3": 408,
       "wb_a2": "KP",
       "wb_a3": "PRK",
       "adm0_a3_is": "PRK",
       "adm0_a3_us": "PRK",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Eastern Asia",
       "region_wb": "East Asia & Pacific",
       "currency": "KPW",
       "latlon": [
          40,
          127
       ]
   },
   {
       "iso_n3": 620,
       "sovereignt": "Portugal",
       "sov_a3": "PRT",
       "admin": "Portugal",
       "adm0_a3": "PRT",
       "geounit": "Portugal",
       "gu_a3": "PRT",
       "subunit": "Portugal",
       "su_a3": "PR1",
       "name": "Portugal",
       "name_long": "Portugal",
       "brk_a3": "PR1",
       "brk_name": "Portugal",
       "abbrev": "Port.",
       "postal": "P",
       "formal_en": "Portuguese Republic",
       "iso_a2": "PT",
       "iso_a3": "PRT",
       "un_a3": 620,
       "wb_a2": "PT",
       "wb_a3": "PRT",
       "adm0_a3_is": "PRT",
       "adm0_a3_us": "PRT",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Southern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "EUR",
       "latlon": [
          39.5,
          -8
       ]
   },
   {
       "iso_n3": 600,
       "sovereignt": "Paraguay",
       "sov_a3": "PRY",
       "admin": "Paraguay",
       "adm0_a3": "PRY",
       "geounit": "Paraguay",
       "gu_a3": "PRY",
       "subunit": "Paraguay",
       "su_a3": "PRY",
       "name": "Paraguay",
       "name_long": "Paraguay",
       "brk_a3": "PRY",
       "brk_name": "Paraguay",
       "abbrev": "Para.",
       "postal": "PY",
       "formal_en": "Republic of Paraguay",
       "iso_a2": "PY",
       "iso_a3": "PRY",
       "un_a3": 600,
       "wb_a2": "PY",
       "wb_a3": "PRY",
       "adm0_a3_is": "PRY",
       "adm0_a3_us": "PRY",
       "continent": "South America",
       "region_un": "Americas",
       "subregion": "South America",
       "region_wb": "Latin America & Caribbean",
       "currency": "PYG",
       "latlon": [
          -23,
          -58
       ]
   },
   {
       "iso_n3": 275,
       "sovereignt": "Israel",
       "sov_a3": "ISR",
       "admin": "Palestine",
       "adm0_a3": "PSX",
       "geounit": "Palestine",
       "gu_a3": "PSX",
       "subunit": "Palestine",
       "su_a3": "PSX",
       "name": "Palestine",
       "name_long": "Palestine",
       "brk_a3": "PSX",
       "brk_name": "Palestine",
       "abbrev": "Pal.",
       "postal": "PAL",
       "formal_en": "West Bank and Gaza",
       "iso_a2": "PS",
       "iso_a3": "PSE",
       "un_a3": 275,
       "wb_a2": "GZ",
       "wb_a3": "WBG",
       "adm0_a3_is": "PSE",
       "adm0_a3_us": "PSX",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Middle East & North Africa",
       "currency": "ILS",
       "latlon": [
          31.9,
          35.2
       ]
   },
   {
       "iso_n3": 634,
       "sovereignt": "Qatar",
       "sov_a3": "QAT",
       "admin": "Qatar",
       "adm0_a3": "QAT",
       "geounit": "Qatar",
       "gu_a3": "QAT",
       "subunit": "Qatar",
       "su_a3": "QAT",
       "name": "Qatar",
       "name_long": "Qatar",
       "brk_a3": "QAT",
       "brk_name": "Qatar",
       "abbrev": "Qatar",
       "postal": "QA",
       "formal_en": "State of Qatar",
       "iso_a2": "QA",
       "iso_a3": "QAT",
       "un_a3": 634,
       "wb_a2": "QA",
       "wb_a3": "QAT",
       "adm0_a3_is": "QAT",
       "adm0_a3_us": "QAT",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Middle East & North Africa",
       "currency": "QAR",
       "latlon": [
          25.5,
          51.25
       ]
   },
   {
       "iso_n3": 642,
       "sovereignt": "Romania",
       "sov_a3": "ROU",
       "admin": "Romania",
       "adm0_a3": "ROU",
       "geounit": "Romania",
       "gu_a3": "ROU",
       "subunit": "Romania",
       "su_a3": "ROU",
       "name": "Romania",
       "name_long": "Romania",
       "brk_a3": "ROU",
       "brk_name": "Romania",
       "abbrev": "Rom.",
       "postal": "RO",
       "formal_en": "Romania",
       "iso_a2": "RO",
       "iso_a3": "ROU",
       "un_a3": 642,
       "wb_a2": "RO",
       "wb_a3": "ROM",
       "adm0_a3_is": "ROU",
       "adm0_a3_us": "ROU",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Eastern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "RON",
       "latlon": [
          46,
          25
       ]
   },
   {
       "currency": "RUB",
       "latlon": [
          70,
          120
       ],
       "iso_n3": 643,
       "sovereignt": "Russia",
       "sov_a3": "RUS",
       "admin": "Russia",
       "adm0_a3": "RUS",
       "geounit": "Russia",
       "gu_a3": "RUS",
       "subunit": "Russia",
       "su_a3": "RUS",
       "name": "Russia",
       "name_long": "Russian Federation",
       "brk_a3": "RUS",
       "brk_name": "Russia",
       "abbrev": "Rus.",
       "postal": "RUS",
       "formal_en": "Russian Federation",
       "iso_a2": "RU",
       "iso_a3": "RUS",
       "un_a3": 643,
       "wb_a2": "RU",
       "wb_a3": "RUS",
       "adm0_a3_is": "RUS",
       "adm0_a3_us": "RUS",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Eastern Europe",
       "region_wb": "Europe & Central Asia"
   },
   {
       "iso_n3": 646,
       "sovereignt": "Rwanda",
       "sov_a3": "RWA",
       "admin": "Rwanda",
       "adm0_a3": "RWA",
       "geounit": "Rwanda",
       "gu_a3": "RWA",
       "subunit": "Rwanda",
       "su_a3": "RWA",
       "name": "Rwanda",
       "name_long": "Rwanda",
       "brk_a3": "RWA",
       "brk_name": "Rwanda",
       "abbrev": "Rwa.",
       "postal": "RW",
       "formal_en": "Republic of Rwanda",
       "iso_a2": "RW",
       "iso_a3": "RWA",
       "un_a3": 646,
       "wb_a2": "RW",
       "wb_a3": "RWA",
       "adm0_a3_is": "RWA",
       "adm0_a3_us": "RWA",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Eastern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "RWF",
       "latlon": [
          -2,
          30
       ]
   },
   {
       "iso_n3": 682,
       "sovereignt": "Saudi Arabia",
       "sov_a3": "SAU",
       "admin": "Saudi Arabia",
       "adm0_a3": "SAU",
       "geounit": "Saudi Arabia",
       "gu_a3": "SAU",
       "subunit": "Saudi Arabia",
       "su_a3": "SAU",
       "name": "Saudi Arabia",
       "name_long": "Saudi Arabia",
       "brk_a3": "SAU",
       "brk_name": "Saudi Arabia",
       "abbrev": "Saud.",
       "postal": "SA",
       "formal_en": "Kingdom of Saudi Arabia",
       "iso_a2": "SA",
       "iso_a3": "SAU",
       "un_a3": 682,
       "wb_a2": "SA",
       "wb_a3": "SAU",
       "adm0_a3_is": "SAU",
       "adm0_a3_us": "SAU",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Middle East & North Africa",
       "currency": "SAR",
       "latlon": [
          25,
          45
       ]
   },
   {
       "iso_n3": 729,
       "sovereignt": "Sudan",
       "sov_a3": "SDN",
       "admin": "Sudan",
       "adm0_a3": "SDN",
       "geounit": "Sudan",
       "gu_a3": "SDN",
       "subunit": "Sudan",
       "su_a3": "SDN",
       "name": "Sudan",
       "name_long": "Sudan",
       "brk_a3": "SDN",
       "brk_name": "Sudan",
       "abbrev": "Sudan",
       "postal": "SD",
       "formal_en": "Republic of the Sudan",
       "iso_a2": "SD",
       "iso_a3": "SDN",
       "un_a3": 729,
       "wb_a2": "SD",
       "wb_a3": "SDN",
       "adm0_a3_is": "SDN",
       "adm0_a3_us": "SDN",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Northern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "SDG",
       "latlon": [
          15,
          30
       ]
   },
   {
       "iso_n3": 686,
       "sovereignt": "Senegal",
       "sov_a3": "SEN",
       "admin": "Senegal",
       "adm0_a3": "SEN",
       "geounit": "Senegal",
       "gu_a3": "SEN",
       "subunit": "Senegal",
       "su_a3": "SEN",
       "name": "Senegal",
       "name_long": "Senegal",
       "brk_a3": "SEN",
       "brk_name": "Senegal",
       "abbrev": "Sen.",
       "postal": "SN",
       "formal_en": "Republic of Senegal",
       "iso_a2": "SN",
       "iso_a3": "SEN",
       "un_a3": 686,
       "wb_a2": "SN",
       "wb_a3": "SEN",
       "adm0_a3_is": "SEN",
       "adm0_a3_us": "SEN",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Western Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "XOF",
       "latlon": [
          14,
          -14
       ]
   },
   {
       "iso_n3": 90,
       "sovereignt": "Solomon Islands",
       "sov_a3": "SLB",
       "admin": "Solomon Islands",
       "adm0_a3": "SLB",
       "geounit": "Solomon Islands",
       "gu_a3": "SLB",
       "subunit": "Solomon Islands",
       "su_a3": "SLB",
       "name": "Solomon Is.",
       "name_long": "Solomon Islands",
       "brk_a3": "SLB",
       "brk_name": "Solomon Is.",
       "abbrev": "S. Is.",
       "postal": "SB",
       "formal_en": "",
       "iso_a2": "SB",
       "iso_a3": "SLB",
       "un_a3": 90,
       "wb_a2": "SB",
       "wb_a3": "SLB",
       "adm0_a3_is": "SLB",
       "adm0_a3_us": "SLB",
       "continent": "Oceania",
       "region_un": "Oceania",
       "subregion": "Melanesia",
       "region_wb": "East Asia & Pacific",
       "currency": "SBD",
       "latlon": [
          -8,
          159
       ]
   },
   {
       "iso_n3": 694,
       "sovereignt": "Sierra Leone",
       "sov_a3": "SLE",
       "admin": "Sierra Leone",
       "adm0_a3": "SLE",
       "geounit": "Sierra Leone",
       "gu_a3": "SLE",
       "subunit": "Sierra Leone",
       "su_a3": "SLE",
       "name": "Sierra Leone",
       "name_long": "Sierra Leone",
       "brk_a3": "SLE",
       "brk_name": "Sierra Leone",
       "abbrev": "S.L.",
       "postal": "SL",
       "formal_en": "Republic of Sierra Leone",
       "iso_a2": "SL",
       "iso_a3": "SLE",
       "un_a3": 694,
       "wb_a2": "SL",
       "wb_a3": "SLE",
       "adm0_a3_is": "SLE",
       "adm0_a3_us": "SLE",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Western Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "SLL",
       "latlon": [
          8.5,
          -11.5
       ]
   },
   {
       "iso_n3": 222,
       "sovereignt": "El Salvador",
       "sov_a3": "SLV",
       "admin": "El Salvador",
       "adm0_a3": "SLV",
       "geounit": "El Salvador",
       "gu_a3": "SLV",
       "subunit": "El Salvador",
       "su_a3": "SLV",
       "name": "El Salvador",
       "name_long": "El Salvador",
       "brk_a3": "SLV",
       "brk_name": "El Salvador",
       "abbrev": "El. S.",
       "postal": "SV",
       "formal_en": "Republic of El Salvador",
       "iso_a2": "SV",
       "iso_a3": "SLV",
       "un_a3": 222,
       "wb_a2": "SV",
       "wb_a3": "SLV",
       "adm0_a3_is": "SLV",
       "adm0_a3_us": "SLV",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Central America",
       "region_wb": "Latin America & Caribbean",
       "currency": "SVC",
       "latlon": [
          13.83333333,
          -88.91666666
       ]
   },
   {
       "iso_n3": 706,
       "sovereignt": "Somalia",
       "sov_a3": "SOM",
       "admin": "Somalia",
       "adm0_a3": "SOM",
       "geounit": "Somalia",
       "gu_a3": "SOM",
       "subunit": "Somalia",
       "su_a3": "SOM",
       "name": "Somalia",
       "name_long": "Somalia",
       "brk_a3": "SOM",
       "brk_name": "Somalia",
       "abbrev": "Som.",
       "postal": "SO",
       "formal_en": "Federal Republic of Somalia",
       "iso_a2": "SO",
       "iso_a3": "SOM",
       "un_a3": 706,
       "wb_a2": "SO",
       "wb_a3": "SOM",
       "adm0_a3_is": "SOM",
       "adm0_a3_us": "SOM",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Eastern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "SOS",
       "latlon": [
          10,
          49
       ]
   },
   {
       "iso_n3": 688,
       "sovereignt": "Republic of Serbia",
       "sov_a3": "SRB",
       "admin": "Republic of Serbia",
       "adm0_a3": "SRB",
       "geounit": "Republic of Serbia",
       "gu_a3": "SRB",
       "subunit": "Republic of Serbia",
       "su_a3": "SRB",
       "name": "Serbia",
       "name_long": "Serbia",
       "brk_a3": "SRB",
       "brk_name": "Serbia",
       "abbrev": "Serb.",
       "postal": "RS",
       "formal_en": "Republic of Serbia",
       "iso_a2": "RS",
       "iso_a3": "SRB",
       "un_a3": 688,
       "wb_a2": "YF",
       "wb_a3": "SRB",
       "adm0_a3_is": "SRB",
       "adm0_a3_us": "SRB",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Southern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "RSD",
       "latlon": [
          44,
          21
       ]
   },
   {
       "iso_n3": 728,
       "sovereignt": "South Sudan",
       "sov_a3": "SDS",
       "admin": "South Sudan",
       "adm0_a3": "SDS",
       "geounit": "South Sudan",
       "gu_a3": "SDS",
       "subunit": "South Sudan",
       "su_a3": "SDS",
       "name": "S. Sudan",
       "name_long": "South Sudan",
       "brk_a3": "SDS",
       "brk_name": "S. Sudan",
       "abbrev": "S. Sud.",
       "postal": "SS",
       "formal_en": "Republic of South Sudan",
       "iso_a2": "SS",
       "iso_a3": "SSD",
       "un_a3": 728,
       "wb_a2": "SS",
       "wb_a3": "SSD",
       "adm0_a3_is": "SSD",
       "adm0_a3_us": "SDS",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Eastern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "SSP",
       "latlon": [
          7,
          30
       ]
   },
   {
       "iso_n3": 740,
       "sovereignt": "Suriname",
       "sov_a3": "SUR",
       "admin": "Suriname",
       "adm0_a3": "SUR",
       "geounit": "Suriname",
       "gu_a3": "SUR",
       "subunit": "Suriname",
       "su_a3": "SUR",
       "name": "Suriname",
       "name_long": "Suriname",
       "brk_a3": "SUR",
       "brk_name": "Suriname",
       "abbrev": "Sur.",
       "postal": "SR",
       "formal_en": "Republic of Suriname",
       "iso_a2": "SR",
       "iso_a3": "SUR",
       "un_a3": 740,
       "wb_a2": "SR",
       "wb_a3": "SUR",
       "adm0_a3_is": "SUR",
       "adm0_a3_us": "SUR",
       "continent": "South America",
       "region_un": "Americas",
       "subregion": "South America",
       "region_wb": "Latin America & Caribbean",
       "currency": "SRD",
       "latlon": [
          4,
          -56
       ]
   },
   {
       "iso_n3": 703,
       "sovereignt": "Slovakia",
       "sov_a3": "SVK",
       "admin": "Slovakia",
       "adm0_a3": "SVK",
       "geounit": "Slovakia",
       "gu_a3": "SVK",
       "subunit": "Slovakia",
       "su_a3": "SVK",
       "name": "Slovakia",
       "name_long": "Slovakia",
       "brk_a3": "SVK",
       "brk_name": "Slovakia",
       "abbrev": "Svk.",
       "postal": "SK",
       "formal_en": "Slovak Republic",
       "iso_a2": "SK",
       "iso_a3": "SVK",
       "un_a3": 703,
       "wb_a2": "SK",
       "wb_a3": "SVK",
       "adm0_a3_is": "SVK",
       "adm0_a3_us": "SVK",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Eastern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "EUR",
       "latlon": [
          48.66666666,
          19.5
       ]
   },
   {
       "iso_n3": 705,
       "sovereignt": "Slovenia",
       "sov_a3": "SVN",
       "admin": "Slovenia",
       "adm0_a3": "SVN",
       "geounit": "Slovenia",
       "gu_a3": "SVN",
       "subunit": "Slovenia",
       "su_a3": "SVN",
       "name": "Slovenia",
       "name_long": "Slovenia",
       "brk_a3": "SVN",
       "brk_name": "Slovenia",
       "abbrev": "Slo.",
       "postal": "SLO",
       "formal_en": "Republic of Slovenia",
       "iso_a2": "SI",
       "iso_a3": "SVN",
       "un_a3": 705,
       "wb_a2": "SI",
       "wb_a3": "SVN",
       "adm0_a3_is": "SVN",
       "adm0_a3_us": "SVN",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Southern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "EUR",
       "_comment": "Since this is the EUR marker, we have offset it",
       "latlon_ACTUAL": [
          46.11666666,
          14.81666666
       ],
       "flagHeightMultiplier": 0.90,
       "latlon": [45.203223, -4.754679]
   },
   {
       "iso_n3": 752,
       "sovereignt": "Sweden",
       "sov_a3": "SWE",
       "admin": "Sweden",
       "adm0_a3": "SWE",
       "geounit": "Sweden",
       "gu_a3": "SWE",
       "subunit": "Sweden",
       "su_a3": "SWE",
       "name": "Sweden",
       "name_long": "Sweden",
       "brk_a3": "SWE",
       "brk_name": "Sweden",
       "abbrev": "Swe.",
       "postal": "S",
       "formal_en": "Kingdom of Sweden",
       "iso_a2": "SE",
       "iso_a3": "SWE",
       "un_a3": 752,
       "wb_a2": "SE",
       "wb_a3": "SWE",
       "adm0_a3_is": "SWE",
       "adm0_a3_us": "SWE",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Northern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "SEK",
       "latlon": [
          62,
          15
       ]
   },
   {
       "iso_n3": 748,
       "sovereignt": "Swaziland",
       "sov_a3": "SWZ",
       "admin": "Swaziland",
       "adm0_a3": "SWZ",
       "geounit": "Swaziland",
       "gu_a3": "SWZ",
       "subunit": "Swaziland",
       "su_a3": "SWZ",
       "name": "Swaziland",
       "name_long": "Swaziland",
       "brk_a3": "SWZ",
       "brk_name": "Swaziland",
       "abbrev": "Swz.",
       "postal": "SW",
       "formal_en": "Kingdom of Swaziland",
       "iso_a2": "SZ",
       "iso_a3": "SWZ",
       "un_a3": 748,
       "wb_a2": "SZ",
       "wb_a3": "SWZ",
       "adm0_a3_is": "SWZ",
       "adm0_a3_us": "SWZ",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Southern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "SZL",
       "latlon": [
          -26.5,
          31.5
       ]
   },
   {
       "iso_n3": 760,
       "sovereignt": "Syria",
       "sov_a3": "SYR",
       "admin": "Syria",
       "adm0_a3": "SYR",
       "geounit": "Syria",
       "gu_a3": "SYR",
       "subunit": "Syria",
       "su_a3": "SYR",
       "name": "Syria",
       "name_long": "Syria",
       "brk_a3": "SYR",
       "brk_name": "Syria",
       "abbrev": "Syria",
       "postal": "SYR",
       "formal_en": "Syrian Arab Republic",
       "iso_a2": "SY",
       "iso_a3": "SYR",
       "un_a3": 760,
       "wb_a2": "SY",
       "wb_a3": "SYR",
       "adm0_a3_is": "SYR",
       "adm0_a3_us": "SYR",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Middle East & North Africa",
       "currency": "SYP",
       "latlon": [
          35,
          38
       ]
   },
   {
       "iso_n3": 148,
       "sovereignt": "Chad",
       "sov_a3": "TCD",
       "admin": "Chad",
       "adm0_a3": "TCD",
       "geounit": "Chad",
       "gu_a3": "TCD",
       "subunit": "Chad",
       "su_a3": "TCD",
       "name": "Chad",
       "name_long": "Chad",
       "brk_a3": "TCD",
       "brk_name": "Chad",
       "abbrev": "Chad",
       "postal": "TD",
       "formal_en": "Republic of Chad",
       "iso_a2": "TD",
       "iso_a3": "TCD",
       "un_a3": 148,
       "wb_a2": "TD",
       "wb_a3": "TCD",
       "adm0_a3_is": "TCD",
       "adm0_a3_us": "TCD",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Middle Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "XAF",
       "latlon": [
          15,
          19
       ]
   },
   {
       "iso_n3": 768,
       "sovereignt": "Togo",
       "sov_a3": "TGO",
       "admin": "Togo",
       "adm0_a3": "TGO",
       "geounit": "Togo",
       "gu_a3": "TGO",
       "subunit": "Togo",
       "su_a3": "TGO",
       "name": "Togo",
       "name_long": "Togo",
       "brk_a3": "TGO",
       "brk_name": "Togo",
       "abbrev": "Togo",
       "postal": "TG",
       "formal_en": "Togolese Republic",
       "iso_a2": "TG",
       "iso_a3": "TGO",
       "un_a3": 768,
       "wb_a2": "TG",
       "wb_a3": "TGO",
       "adm0_a3_is": "TGO",
       "adm0_a3_us": "TGO",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Western Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "XOF",
       "latlon": [
          8,
          1.16666666
       ]
   },
   {
       "iso_n3": 764,
       "sovereignt": "Thailand",
       "sov_a3": "THA",
       "admin": "Thailand",
       "adm0_a3": "THA",
       "geounit": "Thailand",
       "gu_a3": "THA",
       "subunit": "Thailand",
       "su_a3": "THA",
       "name": "Thailand",
       "name_long": "Thailand",
       "brk_a3": "THA",
       "brk_name": "Thailand",
       "abbrev": "Thai.",
       "postal": "TH",
       "formal_en": "Kingdom of Thailand",
       "iso_a2": "TH",
       "iso_a3": "THA",
       "un_a3": 764,
       "wb_a2": "TH",
       "wb_a3": "THA",
       "adm0_a3_is": "THA",
       "adm0_a3_us": "THA",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "South-Eastern Asia",
       "region_wb": "East Asia & Pacific",
       "currency": "THB",
       "latlon": [
          15,
          100
       ]
   },
   {
       "iso_n3": 762,
       "sovereignt": "Tajikistan",
       "sov_a3": "TJK",
       "admin": "Tajikistan",
       "adm0_a3": "TJK",
       "geounit": "Tajikistan",
       "gu_a3": "TJK",
       "subunit": "Tajikistan",
       "su_a3": "TJK",
       "name": "Tajikistan",
       "name_long": "Tajikistan",
       "brk_a3": "TJK",
       "brk_name": "Tajikistan",
       "abbrev": "Tjk.",
       "postal": "TJ",
       "formal_en": "Republic of Tajikistan",
       "iso_a2": "TJ",
       "iso_a3": "TJK",
       "un_a3": 762,
       "wb_a2": "TJ",
       "wb_a3": "TJK",
       "adm0_a3_is": "TJK",
       "adm0_a3_us": "TJK",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Central Asia",
       "region_wb": "Europe & Central Asia",
       "currency": "TJS",
       "latlon": [
          39,
          71
       ]
   },
   {
       "iso_n3": 795,
       "sovereignt": "Turkmenistan",
       "sov_a3": "TKM",
       "admin": "Turkmenistan",
       "adm0_a3": "TKM",
       "geounit": "Turkmenistan",
       "gu_a3": "TKM",
       "subunit": "Turkmenistan",
       "su_a3": "TKM",
       "name": "Turkmenistan",
       "name_long": "Turkmenistan",
       "brk_a3": "TKM",
       "brk_name": "Turkmenistan",
       "abbrev": "Turkm.",
       "postal": "TM",
       "formal_en": "Turkmenistan",
       "iso_a2": "TM",
       "iso_a3": "TKM",
       "un_a3": 795,
       "wb_a2": "TM",
       "wb_a3": "TKM",
       "adm0_a3_is": "TKM",
       "adm0_a3_us": "TKM",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Central Asia",
       "region_wb": "Europe & Central Asia",
       "currency": "TMT",
       "latlon": [
          40,
          60
       ]
   },
   {
       "iso_n3": 626,
       "sovereignt": "East Timor",
       "sov_a3": "TLS",
       "admin": "East Timor",
       "adm0_a3": "TLS",
       "geounit": "East Timor",
       "gu_a3": "TLS",
       "subunit": "East Timor",
       "su_a3": "TLS",
       "name": "Timor-Leste",
       "name_long": "Timor-Leste",
       "brk_a3": "TLS",
       "brk_name": "Timor-Leste",
       "abbrev": "T.L.",
       "postal": "TL",
       "formal_en": "Democratic Republic of Timor-Leste",
       "iso_a2": "TL",
       "iso_a3": "TLS",
       "un_a3": 626,
       "wb_a2": "TP",
       "wb_a3": "TMP",
       "adm0_a3_is": "TLS",
       "adm0_a3_us": "TLS",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "South-Eastern Asia",
       "region_wb": "East Asia & Pacific",
       "currency": "USD_DISABLED",
       "latlon": [
          -8.83333333,
          125.91666666
       ]
   },
   {
       "iso_n3": 780,
       "sovereignt": "Trinidad and Tobago",
       "sov_a3": "TTO",
       "admin": "Trinidad and Tobago",
       "adm0_a3": "TTO",
       "geounit": "Trinidad and Tobago",
       "gu_a3": "TTO",
       "subunit": "Trinidad and Tobago",
       "su_a3": "TTO",
       "name": "Trinidad and Tobago",
       "name_long": "Trinidad and Tobago",
       "brk_a3": "TTO",
       "brk_name": "Trinidad and Tobago",
       "abbrev": "Tr.T.",
       "postal": "TT",
       "formal_en": "Republic of Trinidad and Tobago",
       "iso_a2": "TT",
       "iso_a3": "TTO",
       "un_a3": 780,
       "wb_a2": "TT",
       "wb_a3": "TTO",
       "adm0_a3_is": "TTO",
       "adm0_a3_us": "TTO",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Caribbean",
       "region_wb": "Latin America & Caribbean",
       "currency": "TTD",
       "latlon": [
          11,
          -61
       ]
   },
   {
       "iso_n3": 788,
       "sovereignt": "Tunisia",
       "sov_a3": "TUN",
       "admin": "Tunisia",
       "adm0_a3": "TUN",
       "geounit": "Tunisia",
       "gu_a3": "TUN",
       "subunit": "Tunisia",
       "su_a3": "TUN",
       "name": "Tunisia",
       "name_long": "Tunisia",
       "brk_a3": "TUN",
       "brk_name": "Tunisia",
       "abbrev": "Tun.",
       "postal": "TN",
       "formal_en": "Republic of Tunisia",
       "iso_a2": "TN",
       "iso_a3": "TUN",
       "un_a3": 788,
       "wb_a2": "TN",
       "wb_a3": "TUN",
       "adm0_a3_is": "TUN",
       "adm0_a3_us": "TUN",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Northern Africa",
       "region_wb": "Middle East & North Africa",
       "currency": "TND",
       "latlon": [
          34,
          9
       ]
   },
   {
       "iso_n3": 792,
       "sovereignt": "Turkey",
       "sov_a3": "TUR",
       "admin": "Turkey",
       "adm0_a3": "TUR",
       "geounit": "Turkey",
       "gu_a3": "TUR",
       "subunit": "Turkey",
       "su_a3": "TUR",
       "name": "Turkey",
       "name_long": "Turkey",
       "brk_a3": "TUR",
       "brk_name": "Turkey",
       "abbrev": "Tur.",
       "postal": "TR",
       "formal_en": "Republic of Turkey",
       "iso_a2": "TR",
       "iso_a3": "TUR",
       "un_a3": 792,
       "wb_a2": "TR",
       "wb_a3": "TUR",
       "adm0_a3_is": "TUR",
       "adm0_a3_us": "TUR",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Europe & Central Asia",
       "currency": "TRY",
       "latlon": [
          39,
          35
       ]
   },
   {
       "iso_n3": 158,
       "sovereignt": "Taiwan",
       "sov_a3": "TWN",
       "admin": "Taiwan",
       "adm0_a3": "TWN",
       "geounit": "Taiwan",
       "gu_a3": "TWN",
       "subunit": "Taiwan",
       "su_a3": "TWN",
       "name": "Taiwan",
       "name_long": "Taiwan",
       "brk_a3": "B77",
       "brk_name": "Taiwan",
       "abbrev": "Taiwan",
       "postal": "TW",
       "formal_en": "",
       "iso_a2": "TW",
       "iso_a3": "TWN",
       "un_a3": -99,
       "wb_a2": "-99",
       "wb_a3": "-99",
       "adm0_a3_is": "TWN",
       "adm0_a3_us": "TWN",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Eastern Asia",
       "region_wb": "East Asia & Pacific",
       "currency": "TWD",
       "latlon": [
          23.5,
          121
       ]
   },
   {
       "iso_n3": 834,
       "sovereignt": "United Republic of Tanzania",
       "sov_a3": "TZA",
       "admin": "United Republic of Tanzania",
       "adm0_a3": "TZA",
       "geounit": "Tanzania",
       "gu_a3": "TZA",
       "subunit": "Tanzania",
       "su_a3": "TZA",
       "name": "Tanzania",
       "name_long": "Tanzania",
       "brk_a3": "TZA",
       "brk_name": "Tanzania",
       "abbrev": "Tanz.",
       "postal": "TZ",
       "formal_en": "United Republic of Tanzania",
       "iso_a2": "TZ",
       "iso_a3": "TZA",
       "un_a3": 834,
       "wb_a2": "TZ",
       "wb_a3": "TZA",
       "adm0_a3_is": "TZA",
       "adm0_a3_us": "TZA",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Eastern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "TZS",
       "latlon": [
          -6,
          35
       ]
   },
   {
       "iso_n3": 800,
       "sovereignt": "Uganda",
       "sov_a3": "UGA",
       "admin": "Uganda",
       "adm0_a3": "UGA",
       "geounit": "Uganda",
       "gu_a3": "UGA",
       "subunit": "Uganda",
       "su_a3": "UGA",
       "name": "Uganda",
       "name_long": "Uganda",
       "brk_a3": "UGA",
       "brk_name": "Uganda",
       "abbrev": "Uga.",
       "postal": "UG",
       "formal_en": "Republic of Uganda",
       "iso_a2": "UG",
       "iso_a3": "UGA",
       "un_a3": 800,
       "wb_a2": "UG",
       "wb_a3": "UGA",
       "adm0_a3_is": "UGA",
       "adm0_a3_us": "UGA",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Eastern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "UGX",
       "latlon": [
          1,
          32
       ]
   },
   {
       "iso_n3": 804,
       "sovereignt": "Ukraine",
       "sov_a3": "UKR",
       "admin": "Ukraine",
       "adm0_a3": "UKR",
       "geounit": "Ukraine",
       "gu_a3": "UKR",
       "subunit": "Ukraine",
       "su_a3": "UKR",
       "name": "Ukraine",
       "name_long": "Ukraine",
       "brk_a3": "UKR",
       "brk_name": "Ukraine",
       "abbrev": "Ukr.",
       "postal": "UA",
       "formal_en": "Ukraine",
       "iso_a2": "UA",
       "iso_a3": "UKR",
       "un_a3": 804,
       "wb_a2": "UA",
       "wb_a3": "UKR",
       "adm0_a3_is": "UKR",
       "adm0_a3_us": "UKR",
       "continent": "Europe",
       "region_un": "Europe",
       "subregion": "Eastern Europe",
       "region_wb": "Europe & Central Asia",
       "currency": "UAH",
       "latlon": [
          49,
          32
       ]
   },
   {
       "iso_n3": 858,
       "sovereignt": "Uruguay",
       "sov_a3": "URY",
       "admin": "Uruguay",
       "adm0_a3": "URY",
       "geounit": "Uruguay",
       "gu_a3": "URY",
       "subunit": "Uruguay",
       "su_a3": "URY",
       "name": "Uruguay",
       "name_long": "Uruguay",
       "brk_a3": "URY",
       "brk_name": "Uruguay",
       "abbrev": "Ury.",
       "postal": "UY",
       "formal_en": "Oriental Republic of Uruguay",
       "iso_a2": "UY",
       "iso_a3": "URY",
       "un_a3": 858,
       "wb_a2": "UY",
       "wb_a3": "URY",
       "adm0_a3_is": "URY",
       "adm0_a3_us": "URY",
       "continent": "South America",
       "region_un": "Americas",
       "subregion": "South America",
       "region_wb": "Latin America & Caribbean",
       "currency": "UYI",
       "latlon": [
          -33,
          -56
       ]
   },
   {
       "iso_n3": 840,
       "currency": "USD",
       "latlon": [
          38,
          -108
       ],
       "flagHeightMultiplier": 1.0,
       "sovereignt": "United States of America",
       "sov_a3": "US1",
       "admin": "United States of America",
       "adm0_a3": "USA",
       "geounit": "United States of America",
       "gu_a3": "USA",
       "subunit": "United States of America",
       "su_a3": "USA",
       "name": "United States",
       "name_long": "United States",
       "brk_a3": "USA",
       "brk_name": "United States",
       "abbrev": "U.S.A.",
       "postal": "US",
       "formal_en": "United States of America",
       "iso_a2": "US",
       "iso_a3": "USA",
       "un_a3": 840,
       "wb_a2": "US",
       "wb_a3": "USA",
       "adm0_a3_is": "USA",
       "adm0_a3_us": "USA",
       "continent": "North America",
       "region_un": "Americas",
       "subregion": "Northern America",
       "region_wb": "North America"
   },
   {
       "iso_n3": 860,
       "sovereignt": "Uzbekistan",
       "sov_a3": "UZB",
       "admin": "Uzbekistan",
       "adm0_a3": "UZB",
       "geounit": "Uzbekistan",
       "gu_a3": "UZB",
       "subunit": "Uzbekistan",
       "su_a3": "UZB",
       "name": "Uzbekistan",
       "name_long": "Uzbekistan",
       "brk_a3": "UZB",
       "brk_name": "Uzbekistan",
       "abbrev": "Uzb.",
       "postal": "UZ",
       "formal_en": "Republic of Uzbekistan",
       "iso_a2": "UZ",
       "iso_a3": "UZB",
       "un_a3": 860,
       "wb_a2": "UZ",
       "wb_a3": "UZB",
       "adm0_a3_is": "UZB",
       "adm0_a3_us": "UZB",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Central Asia",
       "region_wb": "Europe & Central Asia",
       "currency": "UZS",
       "latlon": [
          41,
          64
       ]
   },
   {
       "iso_n3": 862,
       "sovereignt": "Venezuela",
       "sov_a3": "VEN",
       "admin": "Venezuela",
       "adm0_a3": "VEN",
       "geounit": "Venezuela",
       "gu_a3": "VEN",
       "subunit": "Venezuela",
       "su_a3": "VEN",
       "name": "Venezuela",
       "name_long": "Venezuela",
       "brk_a3": "VEN",
       "brk_name": "Venezuela",
       "abbrev": "Ven.",
       "postal": "VE",
       "formal_en": "Bolivarian Republic of Venezuela",
       "iso_a2": "VE",
       "iso_a3": "VEN",
       "un_a3": 862,
       "wb_a2": "VE",
       "wb_a3": "VEN",
       "adm0_a3_is": "VEN",
       "adm0_a3_us": "VEN",
       "continent": "South America",
       "region_un": "Americas",
       "subregion": "South America",
       "region_wb": "Latin America & Caribbean",
       "currency": "VES",
       "latlon": [
          8,
          -66
       ]
   },
   {
       "iso_n3": 704,
       "sovereignt": "Vietnam",
       "sov_a3": "VNM",
       "admin": "Vietnam",
       "adm0_a3": "VNM",
       "geounit": "Vietnam",
       "gu_a3": "VNM",
       "subunit": "Vietnam",
       "su_a3": "VNM",
       "name": "Vietnam",
       "name_long": "Vietnam",
       "brk_a3": "VNM",
       "brk_name": "Vietnam",
       "abbrev": "Viet.",
       "postal": "VN",
       "formal_en": "Socialist Republic of Vietnam",
       "iso_a2": "VN",
       "iso_a3": "VNM",
       "un_a3": 704,
       "wb_a2": "VN",
       "wb_a3": "VNM",
       "adm0_a3_is": "VNM",
       "adm0_a3_us": "VNM",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "South-Eastern Asia",
       "region_wb": "East Asia & Pacific",
       "currency": "VND",
       "latlon": [
          16.16666666,
          107.83333333
       ]
   },
   {
       "iso_n3": 548,
       "sovereignt": "Vanuatu",
       "sov_a3": "VUT",
       "admin": "Vanuatu",
       "adm0_a3": "VUT",
       "geounit": "Vanuatu",
       "gu_a3": "VUT",
       "subunit": "Vanuatu",
       "su_a3": "VUT",
       "name": "Vanuatu",
       "name_long": "Vanuatu",
       "brk_a3": "VUT",
       "brk_name": "Vanuatu",
       "abbrev": "Van.",
       "postal": "VU",
       "formal_en": "Republic of Vanuatu",
       "iso_a2": "VU",
       "iso_a3": "VUT",
       "un_a3": 548,
       "wb_a2": "VU",
       "wb_a3": "VUT",
       "adm0_a3_is": "VUT",
       "adm0_a3_us": "VUT",
       "continent": "Oceania",
       "region_un": "Oceania",
       "subregion": "Melanesia",
       "region_wb": "East Asia & Pacific",
       "currency": "VUV",
       "latlon": [
          -16,
          167
       ]
   },
   {
       "iso_n3": 887,
       "sovereignt": "Yemen",
       "sov_a3": "YEM",
       "admin": "Yemen",
       "adm0_a3": "YEM",
       "geounit": "Yemen",
       "gu_a3": "YEM",
       "subunit": "Yemen",
       "su_a3": "YEM",
       "name": "Yemen",
       "name_long": "Yemen",
       "brk_a3": "YEM",
       "brk_name": "Yemen",
       "abbrev": "Yem.",
       "postal": "YE",
       "formal_en": "Republic of Yemen",
       "iso_a2": "YE",
       "iso_a3": "YEM",
       "un_a3": 887,
       "wb_a2": "RY",
       "wb_a3": "YEM",
       "adm0_a3_is": "YEM",
       "adm0_a3_us": "YEM",
       "continent": "Asia",
       "region_un": "Asia",
       "subregion": "Western Asia",
       "region_wb": "Middle East & North Africa",
       "currency": "YER",
       "latlon": [
          15,
          48
       ]
   },
   {
       "iso_n3": 710,
       "sovereignt": "South Africa",
       "sov_a3": "ZAF",
       "admin": "South Africa",
       "adm0_a3": "ZAF",
       "geounit": "South Africa",
       "gu_a3": "ZAF",
       "subunit": "South Africa",
       "su_a3": "ZAF",
       "name": "South Africa",
       "name_long": "South Africa",
       "brk_a3": "ZAF",
       "brk_name": "South Africa",
       "abbrev": "S.Af.",
       "postal": "ZA",
       "formal_en": "Republic of South Africa",
       "iso_a2": "ZA",
       "iso_a3": "ZAF",
       "un_a3": 710,
       "wb_a2": "ZA",
       "wb_a3": "ZAF",
       "adm0_a3_is": "ZAF",
       "adm0_a3_us": "ZAF",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Southern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "ZAR",
       "latlon": [
          -29,
          24
       ]
   },
   {
       "iso_n3": 894,
       "sovereignt": "Zambia",
       "sov_a3": "ZMB",
       "admin": "Zambia",
       "adm0_a3": "ZMB",
       "geounit": "Zambia",
       "gu_a3": "ZMB",
       "subunit": "Zambia",
       "su_a3": "ZMB",
       "name": "Zambia",
       "name_long": "Zambia",
       "brk_a3": "ZMB",
       "brk_name": "Zambia",
       "abbrev": "Zambia",
       "postal": "ZM",
       "formal_en": "Republic of Zambia",
       "iso_a2": "ZM",
       "iso_a3": "ZMB",
       "un_a3": 894,
       "wb_a2": "ZM",
       "wb_a3": "ZMB",
       "adm0_a3_is": "ZMB",
       "adm0_a3_us": "ZMB",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Eastern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "ZMK",
       "latlon": [
          -15,
          30
       ]
   },
   {
       "iso_n3": 716,
       "sovereignt": "Zimbabwe",
       "sov_a3": "ZWE",
       "admin": "Zimbabwe",
       "adm0_a3": "ZWE",
       "geounit": "Zimbabwe",
       "gu_a3": "ZWE",
       "subunit": "Zimbabwe",
       "su_a3": "ZWE",
       "name": "Zimbabwe",
       "name_long": "Zimbabwe",
       "brk_a3": "ZWE",
       "brk_name": "Zimbabwe",
       "abbrev": "Zimb.",
       "postal": "ZW",
       "formal_en": "Republic of Zimbabwe",
       "iso_a2": "ZW",
       "iso_a3": "ZWE",
       "un_a3": 716,
       "wb_a2": "ZW",
       "wb_a3": "ZWE",
       "adm0_a3_is": "ZWE",
       "adm0_a3_us": "ZWE",
       "continent": "Africa",
       "region_un": "Africa",
       "subregion": "Eastern Africa",
       "region_wb": "Sub-Saharan Africa",
       "currency": "USD_DISABLED",
       "latlon": [
          -20,
          30
       ]
    },
    {
        "iso_n3": 702,
        "sovereignt": "Singapore",
        "sov_a3": "SGP",
        "admin": "Singapore",
        "adm0_a3": "SGP",
        "geounit": "Singapore",
        "gu_a3": "SGP",
        "subunit": "Singapore",
        "su_a3": "SGP",
        "name": "Singapore",
        "name_long": "Singapore",
        "brk_a3": "SGP",
        "brk_name": "Singapore",
        "abbrev": "Sing.",
        "postal": "SGP",
        "formal_en": "Republic of Singapore",
        "iso_a2": "SG",
        "iso_a3": "SGP",
        "un_a3": 702,
        "wb_a2": "SG",
        "wb_a3": "SGP",
        "adm0_a3_is": "SGP",
        "adm0_a3_us": "SGP",
        "continent": "Asia",
        "region_un": "Asia",
        "subregion": "South-Eastern Asia",
        "region_wb": "East Asia & Pacific",
        "currency": "SGD",
        "latlon": [
            1.361210, 103.804563
        ],
        "flagHeightMultiplier": 0.6,
    },
    {
        "iso_n3": 344,
        "sovereignt": "Hong Kong",
        "sov_a3": "HKG",
        "admin": "Hong Kong",
        "adm0_a3": "HKG",
        "geounit": "Hong Kong",
        "gu_a3": "HKG",
        "subunit": "Hong Kong",
        "su_a3": "HKG",
        "name": "Hong Kong",
        "name_long": "Hong Kong",
        "brk_a3": "HKG",
        "brk_name": "Hong Kong",
        "abbrev": "HKSAR",
        "postal": "HKG",
        "formal_en": "Hong Kong Special Administrative Region of the People's Republic of China",
        "iso_a2": "HK",
        "iso_a3": "HKG",
        "un_a3": 344,
        "wb_a2": "HK",
        "wb_a3": "HKG",
        "adm0_a3_is": "HKG",
        "adm0_a3_us": "HKG",
        "continent": "Asia",
        "region_un": "Asia",
        "subregion": "Eastern Asia",
        "region_wb": "East Asia & Pacific",
        "currency": "HKD",
        "latlon": [
            22.3193, 114.1694
        ],
        "flagHeightMultiplier": 1.0,
    }
];

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.OfflineMapNode.TOPOJSON_110M_FEATURES = {"type":"Topology","objects":{"countries":{"type":"GeometryCollection","geometries":[{"type":"Polygon","arcs":[[0,1,2,3,4,5]],"id":4},{"type":"MultiPolygon","arcs":[[[6,7,8,9]],[[10,11,12]]],"id":24},{"type":"Polygon","arcs":[[13,14,15,16,17]],"id":8},{"type":"Polygon","arcs":[[18,19,20,21,22]],"id":784},{"type":"MultiPolygon","arcs":[[[23,24]],[[25,26,27,28,29,30]]],"id":32},{"type":"Polygon","arcs":[[31,32,33,34,35]],"id":51},{"type":"MultiPolygon","arcs":[[[36]],[[37]],[[38]],[[39]],[[40]],[[41]],[[42]],[[43]]],"id":10},{"type":"Polygon","arcs":[[44]],"id":260},{"type":"MultiPolygon","arcs":[[[45]],[[46]]],"id":36},{"type":"Polygon","arcs":[[47,48,49,50,51,52,53]],"id":40},{"type":"MultiPolygon","arcs":[[[54,-35]],[[55,56,-33,57,58]]],"id":31},{"type":"Polygon","arcs":[[59,60,61]],"id":108},{"type":"Polygon","arcs":[[62,63,64,65,66]],"id":56},{"type":"Polygon","arcs":[[67,68,69,70,71]],"id":204},{"type":"Polygon","arcs":[[72,73,74,-70,75,76]],"id":854},{"type":"Polygon","arcs":[[77,78,79]],"id":50},{"type":"Polygon","arcs":[[80,81,82,83,84,85]],"id":100},{"type":"MultiPolygon","arcs":[[[86]],[[87]],[[88]]],"id":44},{"type":"Polygon","arcs":[[89,90,91]],"id":70},{"type":"Polygon","arcs":[[92,93,94,95,96]],"id":112},{"type":"Polygon","arcs":[[97,98,99]],"id":84},{"type":"Polygon","arcs":[[100,101,102,103,-31]],"id":68},{"type":"Polygon","arcs":[[-27,104,-103,105,106,107,108,109,110,111,112]],"id":76},{"type":"Polygon","arcs":[[113,114]],"id":96},{"type":"Polygon","arcs":[[115,116]],"id":64},{"type":"Polygon","arcs":[[117,118,119,120]],"id":72},{"type":"Polygon","arcs":[[121,122,123,124,125,126,127]],"id":140},{"type":"MultiPolygon","arcs":[[[128]],[[129]],[[130]],[[131]],[[132]],[[133]],[[134]],[[135]],[[136]],[[137]],[[138,139,140,141]],[[142]],[[143]],[[144]],[[145]],[[146]],[[147]],[[148]],[[149]],[[150]],[[151]],[[152]],[[153]],[[154]],[[155]],[[156]],[[157]],[[158]],[[159]],[[160]]],"id":124},{"type":"Polygon","arcs":[[-51,161,162,163]],"id":756},{"type":"MultiPolygon","arcs":[[[-24,164]],[[-30,165,166,-101]]],"id":152},{"type":"MultiPolygon","arcs":[[[167]],[[168,169,170,171,172,173,-117,174,175,176,177,-4,178,179,180,181,182,183]]],"id":156},{"type":"Polygon","arcs":[[184,185,186,187,-73,188]],"id":384},{"type":"Polygon","arcs":[[189,190,191,192,193,194,-128,195]],"id":120},{"type":"Polygon","arcs":[[196,197,-60,198,199,200,201,-10,202,-13,203,-126,204]],"id":180},{"type":"Polygon","arcs":[[-12,205,206,-196,-127,-204]],"id":178},{"type":"Polygon","arcs":[[207,208,209,210,211,-107,212]],"id":170},{"type":"Polygon","arcs":[[213,214,215,216]],"id":188},{"type":"Polygon","arcs":[[217]],"id":192},{"type":"Polygon","arcs":[[218,219]],"id":-99},{"type":"Polygon","arcs":[[220,-220]],"id":196},{"type":"Polygon","arcs":[[-53,221,222,223]],"id":203},{"type":"Polygon","arcs":[[224,225,-222,-52,-164,226,227,-64,228,229,230]],"id":276},{"type":"Polygon","arcs":[[231,232,233,234]],"id":262},{"type":"MultiPolygon","arcs":[[[235]],[[-231,236]]],"id":208},{"type":"Polygon","arcs":[[237,238]],"id":214},{"type":"Polygon","arcs":[[239,240,241,242,243,244,245,246]],"id":12},{"type":"Polygon","arcs":[[247,-208,248]],"id":218},{"type":"Polygon","arcs":[[249,250,251,252,253]],"id":818},{"type":"Polygon","arcs":[[254,255,256,-235]],"id":232},{"type":"Polygon","arcs":[[257,258,259,260]],"id":724},{"type":"Polygon","arcs":[[261,262,263]],"id":233},{"type":"Polygon","arcs":[[-234,264,265,266,267,268,269,-255]],"id":231},{"type":"Polygon","arcs":[[270,271,272,273]],"id":246},{"type":"MultiPolygon","arcs":[[[274]],[[275]]],"id":242},{"type":"Polygon","arcs":[[276]],"id":238},{"type":"MultiPolygon","arcs":[[[281,-227,-163,282,283,-259,284,-66]]],"id":250},{"type":"Polygon","arcs":[[285,286,-190,-207]],"id":266},{"type":"MultiPolygon","arcs":[[[287,288]],[[289]]],"id":826},{"type":"Polygon","arcs":[[290,291,-58,-32,292]],"id":268},{"type":"Polygon","arcs":[[293,-189,-77,294]],"id":288},{"type":"Polygon","arcs":[[295,296,297,298,299,300,-187]],"id":324},{"type":"Polygon","arcs":[[301,302]],"id":270},{"type":"Polygon","arcs":[[303,304,-299]],"id":624},{"type":"Polygon","arcs":[[305,-191,-287]],"id":226},{"type":"MultiPolygon","arcs":[[[306]],[[307,-15,308,-84,309]]],"id":300},{"type":"Polygon","arcs":[[310]],"id":304},{"type":"Polygon","arcs":[[311,312,-100,313,314,315]],"id":320},{"type":"Polygon","arcs":[[316,317,-109,318]],"id":328},{"type":"Polygon","arcs":[[319,320,-315,321,322]],"id":340},{"type":"Polygon","arcs":[[323,-92,324,325,326,327]],"id":191},{"type":"Polygon","arcs":[[-239,328]],"id":332},{"type":"Polygon","arcs":[[-48,329,330,331,332,-328,333]],"id":348},{"type":"MultiPolygon","arcs":[[[334]],[[335,336]],[[337]],[[338]],[[339]],[[340]],[[341]],[[342]],[[343,344]],[[345]],[[346]],[[347,348]],[[349]]],"id":360},{"type":"Polygon","arcs":[[-177,350,-175,-116,-174,351,-80,352,353]],"id":356},{"type":"Polygon","arcs":[[354,-288]],"id":372},{"type":"Polygon","arcs":[[355,-6,356,357,358,359,-55,-34,-57,360]],"id":364},{"type":"Polygon","arcs":[[361,362,363,364,365,366,-359]],"id":368},{"type":"Polygon","arcs":[[367]],"id":352},{"type":"Polygon","arcs":[[368,369,370,-254,371,372,373]],"id":376},{"type":"MultiPolygon","arcs":[[[374]],[[375]],[[376,377,-283,-162,-50]]],"id":380},{"type":"Polygon","arcs":[[378]],"id":388},{"type":"Polygon","arcs":[[-369,379,-365,380,381,-371,382]],"id":400},{"type":"MultiPolygon","arcs":[[[383]],[[384]],[[385]]],"id":392},{"type":"Polygon","arcs":[[386,387,388,389,-181,390]],"id":398},{"type":"Polygon","arcs":[[391,392,393,394,-267,395]],"id":404},{"type":"Polygon","arcs":[[-391,-180,396,397]],"id":417},{"type":"Polygon","arcs":[[398,399,400,401]],"id":116},{"type":"Polygon","arcs":[[402,403]],"id":410},{"type":"Polygon","arcs":[[-18,404,405,406]],"id":-99},{"type":"Polygon","arcs":[[407,408,-363]],"id":414},{"type":"Polygon","arcs":[[409,410,-172,411,-400]],"id":418},{"type":"Polygon","arcs":[[-373,412,413]],"id":422},{"type":"Polygon","arcs":[[414,415,-296,-186]],"id":430},{"type":"Polygon","arcs":[[416,-247,417,418,-252,419,420]],"id":434},{"type":"Polygon","arcs":[[421]],"id":144},{"type":"Polygon","arcs":[[422]],"id":426},{"type":"Polygon","arcs":[[423,424,425,-93,426]],"id":440},{"type":"Polygon","arcs":[[-228,-282,-65]],"id":442},{"type":"Polygon","arcs":[[427,-264,428,-94,-426]],"id":428},{"type":"Polygon","arcs":[[-244,429,430]],"id":504},{"type":"Polygon","arcs":[[431,432]],"id":498},{"type":"Polygon","arcs":[[433]],"id":450},{"type":"Polygon","arcs":[[434,-98,-313,435,436]],"id":484},{"type":"Polygon","arcs":[[-407,437,-85,-309,-14]],"id":807},{"type":"Polygon","arcs":[[438,-241,439,-74,-188,-301,440]],"id":466},{"type":"Polygon","arcs":[[441,-78,-352,-173,-411,442]],"id":104},{"type":"Polygon","arcs":[[443,-325,-91,444,-405,-17]],"id":499},{"type":"Polygon","arcs":[[445,-183]],"id":496},{"type":"Polygon","arcs":[[446,447,448,449,450,451,452,453]],"id":508},{"type":"Polygon","arcs":[[454,455,456,-242,-439]],"id":478},{"type":"Polygon","arcs":[[-454,457,458]],"id":454},{"type":"MultiPolygon","arcs":[[[459,460]],[[-348,461,-115,462]]],"id":458},{"type":"Polygon","arcs":[[463,-8,464,-119,465]],"id":516},{"type":"Polygon","arcs":[[466]],"id":540},{"type":"Polygon","arcs":[[-75,-440,-240,-417,467,-194,468,-71]],"id":562},{"type":"Polygon","arcs":[[469,-72,-469,-193]],"id":566},{"type":"Polygon","arcs":[[470,-323,471,-215]],"id":558},{"type":"Polygon","arcs":[[-229,-63,472]],"id":528},{"type":"MultiPolygon","arcs":[[[473,-274,474,475]],[[476]],[[477]],[[478]]],"id":578},{"type":"Polygon","arcs":[[-351,-176]],"id":524},{"type":"MultiPolygon","arcs":[[[479]],[[480]]],"id":554},{"type":"MultiPolygon","arcs":[[[481,482,-22,483]],[[-20,484]]],"id":512},{"type":"Polygon","arcs":[[-178,-354,485,-357,-5]],"id":586},{"type":"Polygon","arcs":[[486,-217,487,-210]],"id":591},{"type":"Polygon","arcs":[[-167,488,-249,-213,-106,-102]],"id":604},{"type":"MultiPolygon","arcs":[[[489]],[[490]],[[491]],[[492]],[[493]],[[494]],[[495]]],"id":608},{"type":"MultiPolygon","arcs":[[[496]],[[497]],[[-344,498]],[[499]]],"id":598},{"type":"Polygon","arcs":[[-226,500,501,-427,-97,502,503,-223]],"id":616},{"type":"Polygon","arcs":[[504]],"id":630},{"type":"Polygon","arcs":[[505,506,-404,507,-169]],"id":408},{"type":"Polygon","arcs":[[-261,508]],"id":620},{"type":"Polygon","arcs":[[-104,-105,-26]],"id":600},{"type":"Polygon","arcs":[[-383,-370]],"id":275},{"type":"Polygon","arcs":[[509,510]],"id":634},{"type":"Polygon","arcs":[[511,-433,512,513,-81,514,-332]],"id":642},{"type":"MultiPolygon","arcs":[[[515]],[[-502,516,-424]],[[517]],[[518]],[[519]],[[520]],[[521]],[[-506,-184,-446,-182,-390,522,-59,-292,523,524,-95,-429,-263,525,-271,-474,526]],[[527]],[[528]],[[529]]],"id":643},{"type":"Polygon","arcs":[[530,-61,-198,531]],"id":646},{"type":"Polygon","arcs":[[-243,-457,532,-430]],"id":732},{"type":"Polygon","arcs":[[533,-381,-364,-409,534,-511,535,-23,-483,536]],"id":682},{"type":"Polygon","arcs":[[537,538,-123,539,-420,-251,540,-256,-270,541]],"id":729},{"type":"Polygon","arcs":[[542,-268,-395,543,-205,-125,544,-538]],"id":728},{"type":"Polygon","arcs":[[545,-455,-441,-300,-305,546,-303]],"id":686},{"type":"MultiPolygon","arcs":[[[547]],[[548]],[[549]],[[550]],[[551]]],"id":90},{"type":"Polygon","arcs":[[552,-297,-416]],"id":694},{"type":"Polygon","arcs":[[553,-316,-321]],"id":222},{"type":"Polygon","arcs":[[-265,-233,554,555]],"id":-99},{"type":"Polygon","arcs":[[-396,-266,-556,556]],"id":706},{"type":"Polygon","arcs":[[-86,-438,-406,-445,-90,-324,-333,-515]],"id":688},{"type":"Polygon","arcs":[[557,-279,558,-110,-318]],"id":740},{"type":"Polygon","arcs":[[-504,559,-330,-54,-224]],"id":703},{"type":"Polygon","arcs":[[-49,-334,-327,560,-377]],"id":705},{"type":"Polygon","arcs":[[-475,-273,561]],"id":752},{"type":"Polygon","arcs":[[562,-450]],"id":748},{"type":"Polygon","arcs":[[-380,-374,-414,563,564,-366]],"id":760},{"type":"Polygon","arcs":[[-468,-421,-540,-122,-195]],"id":148},{"type":"Polygon","arcs":[[565,-295,-76,-69]],"id":768},{"type":"Polygon","arcs":[[566,-461,567,-443,-410,-399]],"id":764},{"type":"Polygon","arcs":[[-397,-179,-3,568]],"id":762},{"type":"Polygon","arcs":[[-356,569,-388,570,-1]],"id":795},{"type":"Polygon","arcs":[[571,-336]],"id":626},{"type":"Polygon","arcs":[[572]],"id":780},{"type":"Polygon","arcs":[[-246,573,-418]],"id":788},{"type":"MultiPolygon","arcs":[[[-293,-36,-360,-367,-565,574]],[[-310,-83,575]]],"id":792},{"type":"Polygon","arcs":[[576]],"id":158},{"type":"Polygon","arcs":[[-393,577,-447,-459,578,-201,579,-199,-62,-531,580]],"id":834},{"type":"Polygon","arcs":[[-532,-197,-544,-394,-581]],"id":800},{"type":"Polygon","arcs":[[-525,581,-513,-432,-512,-331,-560,-503,-96]],"id":804},{"type":"Polygon","arcs":[[-113,582,-28]],"id":858},{"type":"MultiPolygon","arcs":[[[583]],[[584]],[[585]],[[586]],[[587]],[[588,-437,589,-139]],[[590]],[[591]],[[592]],[[-141,593]]],"id":840},{"type":"Polygon","arcs":[[-571,-387,-398,-569,-2]],"id":860},{"type":"Polygon","arcs":[[594,-319,-108,-212]],"id":862},{"type":"Polygon","arcs":[[595,-401,-412,-171]],"id":704},{"type":"MultiPolygon","arcs":[[[596]],[[597]]],"id":548},{"type":"Polygon","arcs":[[598,-537,-482]],"id":887},{"type":"Polygon","arcs":[[-466,-118,599,-451,-563,-449,600],[-423]],"id":710},{"type":"Polygon","arcs":[[-458,-453,601,-120,-465,-7,-202,-579]],"id":894},{"type":"Polygon","arcs":[[-600,-121,-602,-452]],"id":716}]},"land":{"type":"GeometryCollection","geometries":[{"type":"MultiPolygon","arcs":[[[595,401,566,459,567,441,78,352,485,357,361,407,534,509,535,18,484,20,483,598,533,381,249,540,256,231,554,556,391,577,447,600,463,8,202,10,205,285,305,191,469,67,565,293,184,414,552,297,303,546,301,545,455,532,430,244,573,418,252,371,412,563,574,290,523,581,513,81,575,307,15,443,325,560,377,283,259,508,257,284,66,472,229,236,224,500,516,424,427,261,525,271,561,475,526,506,402,507,169],[123,544,538],[199,579],[542,268,541],[388,522,55,360,569]],[[24,164]],[[582,28,165,488,247,208,486,213,470,319,553,311,435,589,139,593,141,588,434,98,313,321,471,215,487,210,594,316,557,279,111],[558,277]],[[36]],[[37]],[[38]],[[39]],[[40]],[[41]],[[42]],[[43]],[[44]],[[45]],[[46]],[[86]],[[87]],[[88]],[[461,113,462,348]],[[128]],[[129]],[[130]],[[131]],[[132]],[[133]],[[134]],[[135]],[[136]],[[137]],[[142]],[[143]],[[144]],[[145]],[[146]],[[147]],[[148]],[[149]],[[150]],[[151]],[[152]],[[153]],[[154]],[[155]],[[156]],[[157]],[[158]],[[159]],[[160]],[[167]],[[217]],[[218,220]],[[235]],[[237,328]],[[274]],[[275]],[[276]],[[280]],[[288,354]],[[289]],[[306]],[[310]],[[334]],[[336,571]],[[337]],[[338]],[[339]],[[340]],[[341]],[[342]],[[344,498]],[[345]],[[346]],[[349]],[[367]],[[374]],[[375]],[[378]],[[383]],[[384]],[[385]],[[421]],[[433]],[[466]],[[476]],[[477]],[[478]],[[479]],[[480]],[[489]],[[490]],[[491]],[[492]],[[493]],[[494]],[[495]],[[496]],[[497]],[[499]],[[504]],[[515]],[[517]],[[518]],[[519]],[[520]],[[521]],[[527]],[[528]],[[529]],[[547]],[[548]],[[549]],[[550]],[[551]],[[572]],[[576]],[[583]],[[584]],[[585]],[[586]],[[587]],[[590]],[[591]],[[592]],[[596]],[[597]]]}]}},"arcs":[[[67002,71642],[284,-224],[209,79],[58,268],[219,89],[157,180],[55,472],[234,114],[44,211],[131,-158],[84,-19]],[[68477,72654],[154,-4],[210,-124]],[[68841,72526],[85,-72],[201,189],[93,-114],[90,271],[166,-12],[43,86],[29,239],[120,205],[150,-134],[-30,-181],[84,-28],[-26,-496],[110,-194],[97,125],[123,58],[173,265],[192,-44],[286,-1]],[[70827,72688],[50,-169]],[[70877,72519],[-162,-67],[-141,-109],[-319,-68],[-298,-124],[-163,-258],[66,-250],[32,-294],[-139,-248],[12,-227],[-76,-213],[-265,18],[110,-390],[-177,-150],[-118,-356],[15,-355],[-108,-166],[-103,55],[-212,-77],[-31,-166],[-207,1],[-154,-334],[-10,-503],[-361,-246],[-194,52],[-56,-129],[-166,75],[-278,-88],[-465,301]],[[66909,68203],[252,536],[-23,380],[-210,100],[-22,375],[-91,472],[119,323],[-121,87],[76,430],[113,736]],[[56642,44124],[29,-184],[-32,-286],[49,-277],[-41,-222],[24,-203],[-579,7],[-13,-1880],[188,-483],[181,-369]],[[56448,40227],[-510,-241],[-673,83],[-192,284],[-1126,-26],[-42,-41],[-166,267],[-180,17],[-166,-100],[-134,-113]],[[53259,40357],[-26,372],[38,519],[96,541],[15,254],[90,532],[66,243],[159,386],[90,263],[29,438],[-15,335],[-83,211],[-74,358],[-68,355],[15,122],[85,235],[-84,570],[-57,396],[-139,374],[26,115]],[[53422,46976],[115,79],[80,-11],[98,71],[820,-8],[68,-440],[80,-354],[64,-191],[106,-309],[184,47],[91,83],[154,-83],[42,148],[69,344],[172,23],[15,103],[142,2],[-24,-213],[337,5],[5,-372],[56,-228],[-41,-356],[21,-363],[93,-219],[-15,-703],[68,54],[121,-15],[172,89],[127,-35]],[[53383,47159],[-74,444]],[[53309,47603],[112,255],[84,100],[104,-203]],[[53609,47755],[-101,-124],[-45,-152],[-9,-258],[-71,-62]],[[55719,75309],[-35,-201],[39,-254],[115,-144]],[[55838,74710],[-5,-155],[-91,-85],[-16,-192],[-129,-287]],[[55597,73991],[-48,41],[-5,130],[-154,199],[-24,281],[23,403],[38,184],[-47,93]],[[55380,75322],[-18,188],[120,291],[18,-111],[75,52]],[[55575,75742],[59,-159],[66,-60],[19,-214]],[[64327,64904],[49,29],[11,-162],[217,93],[230,-15],[168,-18],[190,400],[207,379],[176,364]],[[65575,65974],[52,-202]],[[65627,65772],[38,-466]],[[65665,65306],[-142,-3],[-23,-384],[50,-82],[-126,-117],[-1,-241],[-81,-245],[-7,-238]],[[65335,63996],[-56,-125],[-835,298],[-106,599],[-11,136]],[[31400,18145],[-168,16],[-297,1],[0,1319]],[[30935,19481],[106,-274],[139,-443],[361,-355],[389,-147],[-125,-296],[-264,-29],[-141,208]],[[32587,37434],[511,-964],[227,-89],[339,-437],[286,-231],[40,-261],[-273,-898],[280,-160],[312,-91],[220,95],[252,453],[45,521]],[[34826,35372],[138,114],[139,-341],[-6,-472],[-234,-326],[-186,-241],[-314,-573],[-370,-806]],[[33993,32727],[-70,-473],[-74,-607],[3,-588],[-61,-132],[-21,-382]],[[33770,30545],[-19,-308],[353,-506],[-38,-408],[173,-257],[-14,-289],[-267,-757],[-412,-317],[-557,-123],[-305,59],[59,-352],[-57,-442],[51,-298],[-167,-208],[-284,-82],[-267,216],[-108,-155],[39,-587],[188,-178],[152,186],[82,-307],[-255,-183],[-223,-367],[-41,-595],[-66,-316],[-262,-2],[-218,-302],[-80,-443],[273,-433],[266,-119],[-96,-531],[-328,-333],[-180,-692],[-254,-234],[-113,-276],[89,-614],[185,-342],[-117,30]],[[30952,19680],[-257,93],[-672,79],[-115,344],[6,443],[-185,-38],[-98,214],[-24,626],[213,260],[88,375],[-33,299],[148,504],[101,782],[-30,347],[122,112],[-30,223],[-129,118],[92,248],[-126,224],[-65,682],[112,120],[-47,720],[65,605],[75,527],[166,215],[-84,576],[-1,543],[210,386],[-7,494],[159,576],[1,544],[-72,108],[-128,1020],[171,607],[-27,572],[100,537],[182,555],[196,367],[-83,232],[58,190],[-9,985],[302,291],[96,614],[-34,148]],[[31359,37147],[231,534],[364,-144],[163,-427],[109,475],[316,-24],[45,-127]],[[62106,74858],[386,92]],[[62492,74950],[57,-155],[106,-103],[-56,-148],[148,-202],[-78,-189],[118,-160],[124,-97],[7,-410]],[[62918,73486],[-101,-17]],[[62817,73469],[-113,342],[1,91],[-123,-2],[-82,159],[-58,-16]],[[62442,74043],[-109,172],[-207,147],[27,288],[-47,208]],[[33452,3290],[-82,-301],[-81,-266],[-582,81],[-621,-35],[-348,197],[0,23],[-152,174],[625,-23],[599,-58],[207,243],[147,208],[288,-243]],[[5775,3611],[-533,-81],[-364,208],[-163,209],[-11,35],[-180,162],[169,220],[517,-93],[277,-185],[212,-209],[76,-266]],[[37457,4468],[342,-255],[120,-359],[33,-254],[11,-301],[-430,-186],[-452,-150],[-522,-139],[-582,-116],[-658,35],[-365,197],[49,243],[593,162],[239,197],[174,254],[126,220],[168,209],[180,243],[141,0],[414,127],[419,-127]],[[16330,7154],[359,-93],[332,104],[-158,-208],[-261,-151],[-386,47],[-278,208],[60,197],[332,-104]],[[15122,7165],[425,-231],[-164,23],[-359,58],[-381,162],[202,127],[277,-139]],[[22505,8080],[305,-81],[304,69],[163,-335],[-217,46],[-337,-23],[-343,23],[-376,-35],[-283,116],[-146,243],[174,104],[353,-81],[403,-46]],[[30985,8657],[33,-266],[-49,-231],[-76,-220],[-326,-81],[-311,-116],[-364,11],[136,232],[-327,-81],[-310,-81],[-212,174],[-16,243],[305,231],[190,70],[321,-23],[82,301],[16,219],[-6,475],[158,278],[256,93],[147,-220],[65,-220],[120,-267],[92,-254],[76,-267]],[[0,529],[16,-5],[245,344],[501,-185],[32,21],[294,188],[38,-7],[32,-4],[402,-246],[352,246],[63,34],[816,104],[265,-138],[130,-71],[419,-196],[789,-151],[625,-185],[1072,-139],[800,162],[1181,-116],[669,-185],[734,174],[773,162],[60,278],[-1094,23],[-898,139],[-234,231],[-745,128],[49,266],[103,243],[104,220],[-55,243],[-462,162],[-212,209],[-430,185],[675,-35],[642,93],[402,-197],[495,173],[457,220],[223,197],[-98,243],[-359,162],[-408,174],[-571,35],[-500,81],[-539,58],[-180,220],[-359,185],[-217,208],[-87,672],[136,-58],[250,-185],[457,58],[441,81],[228,-255],[441,58],[370,127],[348,162],[315,197],[419,58],[-11,220],[-97,220],[81,208],[359,104],[163,-196],[425,115],[321,151],[397,12],[375,57],[376,139],[299,128],[337,127],[218,-35],[190,-46],[414,81],[370,-104],[381,11],[364,81],[375,-57],[414,-58],[386,23],[403,-12],[413,-11],[381,23],[283,174],[337,92],[349,-127],[331,104],[300,208],[179,-185],[98,-208],[180,-197],[288,174],[332,-220],[375,-70],[321,-162],[392,35],[354,104],[418,-23],[376,-81],[381,-104],[147,254],[-180,197],[-136,209],[-359,46],[-158,220],[-60,220],[-98,440],[213,-81],[364,-35],[359,35],[327,-93],[283,-174],[119,-208],[376,-35],[359,81],[381,116],[342,70],[283,-139],[370,46],[239,451],[224,-266],[321,-104],[348,58],[228,-232],[365,-23],[337,-69],[332,-128],[218,220],[108,209],[278,-232],[381,58],[283,-127],[190,-197],[370,58],[288,127],[283,151],[337,81],[392,69],[354,81],[272,127],[163,186],[65,254],[-32,244],[-87,231],[-98,232],[-87,231],[-71,209],[-16,231],[27,232],[130,220],[109,243],[44,231],[-55,255],[-32,232],[136,266],[152,173],[180,220],[190,186],[223,173],[109,255],[152,162],[174,151],[267,34],[174,186],[196,115],[228,70],[202,150],[157,186],[218,69],[163,-151],[-103,-196],[-283,-174],[-120,-127],[-206,92],[-229,-58],[-190,-139],[-202,-150],[-136,-174],[-38,-231],[17,-220],[130,-197],[-190,-139],[-261,-46],[-153,-197],[-163,-185],[-174,-255],[-44,-220],[98,-243],[147,-185],[229,-139],[212,-185],[114,-232],[60,-220],[82,-232],[130,-196],[82,-220],[38,-544],[81,-220],[22,-232],[87,-231],[-38,-313],[-152,-243],[-163,-197],[-370,-81],[-125,-208],[-169,-197],[-419,-220],[-370,-93],[-348,-127],[-376,-128],[-223,-243],[-446,-23],[-489,23],[-441,-46],[-468,0],[87,-232],[424,-104],[311,-162],[174,-208],[-310,-185],[-479,58],[-397,-151],[-17,-243],[-11,-232],[327,-196],[60,-220],[353,-220],[588,-93],[500,-162],[398,-185],[506,-186],[690,-92],[681,-162],[473,-174],[517,-197],[272,-278],[136,-220],[337,209],[457,173],[484,186],[577,150],[495,162],[691,12],[680,-81],[560,-139],[180,255],[386,173],[702,12],[550,127],[522,128],[577,81],[614,104],[430,150],[-196,209],[-119,208],[0,220],[-539,-23],[-571,-93],[-544,0],[-77,220],[39,440],[125,128],[397,138],[468,139],[337,174],[337,174],[251,231],[380,104],[376,81],[190,47],[430,23],[408,81],[343,116],[337,139],[305,139],[386,185],[245,197],[261,173],[82,232],[-294,139],[98,243],[185,185],[288,116],[305,139],[283,185],[217,232],[136,277],[202,163],[331,-35],[136,-197],[332,-23],[11,220],[142,231],[299,-58],[71,-220],[331,-34],[360,104],[348,69],[315,-34],[120,-243],[305,196],[283,105],[315,81],[310,81],[283,139],[310,92],[240,128],[168,208],[207,-151],[288,81],[202,-277],[157,-209],[316,116],[125,232],[283,162],[365,-35],[108,-220],[229,220],[299,69],[326,23],[294,-11],[310,-70],[300,-34],[130,-197],[180,-174],[304,104],[327,24],[315,0],[310,11],[278,81],[294,70],[245,162],[261,104],[283,58],[212,162],[152,324],[158,197],[288,-93],[109,-208],[239,-139],[289,46],[196,-208],[206,-151],[283,139],[98,255],[250,104],[289,197],[272,81],[326,116],[218,127],[228,139],[218,127],[261,-69],[250,208],[180,162],[261,-11],[229,139],[54,208],[234,162],[228,116],[278,93],[256,46],[244,-35],[262,-58],[223,-162],[27,-254],[245,-197],[168,-162],[332,-70],[185,-162],[229,-162],[266,-35],[223,116],[240,243],[261,-127],[272,-70],[261,-69],[272,-46],[277,0],[229,-614],[-11,-150],[-33,-267],[-266,-150],[-218,-220],[38,-232],[310,12],[-38,-232],[-141,-220],[-131,-243],[212,-185],[321,-58],[321,104],[153,232],[92,220],[153,185],[174,174],[70,208],[147,289],[174,58],[316,24],[277,69],[283,93],[136,231],[82,220],[190,220],[272,151],[234,115],[153,197],[157,104],[202,93],[277,-58],[250,58],[272,69],[305,-34],[201,162],[142,393],[103,-162],[131,-278],[234,-115],[266,-47],[267,70],[283,-46],[261,-12],[174,58],[234,-35],[212,-127],[250,81],[300,0],[255,81],[289,-81],[185,197],[141,196],[191,163],[348,439],[179,-81],[212,-162],[185,-208],[354,-359],[272,-12],[256,0],[299,70],[299,81],[229,162],[190,174],[310,23],[207,127],[218,-116],[141,-185],[196,-185],[305,23],[190,-150],[332,-151],[348,-58],[288,47],[218,185],[185,185],[250,46],[251,-81],[288,-58],[261,93],[250,0],[245,-58],[256,-58],[250,104],[299,93],[283,23],[316,0],[255,58],[251,46],[76,290],[11,243],[174,-162],[49,-266],[92,-244],[115,-196],[234,-105],[315,35],[365,12],[250,35],[364,0],[262,11],[364,-23],[310,-46],[196,-186],[-54,-220],[179,-173],[299,-139],[310,-151],[360,-104],[375,-92],[283,-93],[315,-12],[180,197],[245,-162],[212,-185],[245,-139],[337,-58],[321,-69],[136,-232],[316,-139],[212,-208],[310,-93],[321,12],[299,-35],[332,12],[332,-47],[310,-81],[288,-139],[289,-116],[195,-173],[-32,-232],[-147,-208],[-125,-266],[-98,-209],[-131,-243],[-364,-93],[-163,-208],[-360,-127],[-125,-232],[-190,-220],[-201,-185],[-115,-243],[-70,-220],[-28,-266],[6,-220],[158,-232],[60,-220],[130,-208],[517,-81],[109,-255],[-501,-93],[-424,-127],[-528,-23],[-234,-336],[-49,-278],[-119,-220],[-147,-220],[370,-196],[141,-244],[239,-219],[338,-197],[386,-186],[419,-185],[636,-185],[142,-289],[800,-128],[53,-45],[208,-175],[767,151],[636,-186],[-99520,-142]],[[69148,21851],[179,-186],[263,-74],[9,-112],[-77,-269],[-427,-38],[-7,314],[41,244],[19,121]],[[90387,26479],[269,-204],[151,81],[217,113],[166,-39],[20,-702],[-95,-203],[-29,-476],[-97,162],[-193,-412],[-57,32],[-171,19],[-171,505],[-38,390],[-160,515],[7,271],[181,-52]],[[89877,42448],[100,-464],[179,223],[92,-250],[133,-231],[-29,-262],[60,-506],[42,-295],[70,-72],[75,-505],[-27,-307],[90,-400],[301,-309],[197,-281],[186,-257],[-37,-143],[159,-371],[108,-639],[111,130],[113,-256],[68,91],[48,-626],[197,-363],[129,-226],[217,-478],[78,-475],[7,-337],[-19,-365],[132,-502],[-16,-523],[-48,-274],[-75,-527],[6,-339],[-55,-423],[-123,-538],[-205,-290],[-102,-458],[-93,-292],[-82,-510],[-107,-294],[-70,-442],[-36,-407],[14,-187],[-159,-205],[-311,-22],[-257,-242],[-127,-229],[-168,-254],[-230,262],[-170,104],[43,308],[-152,-112],[-243,-428],[-240,160],[-158,94],[-159,42],[-269,171],[-179,364],[-52,449],[-64,298],[-137,240],[-267,71],[91,287],[-67,438],[-136,-408],[-247,-109],[146,327],[42,341],[107,289],[-22,438],[-226,-504],[-174,-202],[-106,-470],[-217,243],[9,313],[-174,429],[-147,221],[52,137],[-356,358],[-195,17],[-267,287],[-498,-56],[-359,-211],[-317,-197],[-265,39],[-294,-303],[-241,-137],[-53,-309],[-103,-240],[-236,-15],[-174,-52],[-246,107],[-199,-64],[-191,-27],[-165,-315],[-81,26],[-140,-167],[-133,-187],[-203,23],[-186,0],[-295,377],[-149,113],[6,338],[138,81],[47,134],[-10,212],[34,411],[-31,350],[-147,598],[-45,337],[12,336],[-111,385],[-7,174],[-123,235],[-35,463],[-158,467],[-39,252],[122,-255],[-93,548],[137,-171],[83,-229],[-5,303],[-138,465],[-26,186],[-65,177],[31,341],[56,146],[38,295],[-29,346],[114,425],[21,-450],[118,406],[225,198],[136,252],[212,217],[126,46],[77,-73],[219,220],[168,66],[42,129],[74,54],[153,-14],[292,173],[151,262],[71,316],[163,300],[13,236],[7,321],[194,502],[117,-510],[119,118],[-99,279],[87,287],[122,-128],[34,449],[152,291],[67,233],[140,101],[4,165],[122,-69],[5,148],[122,85],[134,80],[205,-271],[155,-350],[173,-4],[177,-56],[-59,325],[133,473],[126,155],[-44,147],[121,338],[168,208],[142,-70],[234,111],[-5,302],[-204,195],[148,86],[184,-147],[148,-242],[234,-151],[79,60],[172,-182],[162,169],[105,-51],[65,113],[127,-292],[-74,-316],[-105,-239],[-96,-20],[32,-236],[-81,-295],[-99,-291],[20,-166],[221,-327],[214,-189],[143,-204],[201,-350],[78,1],[145,-151],[43,-183],[265,-200],[183,202],[55,317],[56,262],[34,324],[85,470],[-39,286],[20,171],[-32,339],[37,445],[53,120],[-43,197],[67,313],[52,325],[7,168],[104,222],[78,-289],[19,-371],[70,-71],[11,-249],[101,-300],[21,-335],[-10,-214]],[[54716,79012],[-21,-241],[-156,-2],[53,-128],[-92,-380]],[[54500,78261],[-53,-100],[-243,-14],[-140,-134],[-229,45]],[[53835,78058],[-398,153],[-62,205],[-274,-102],[-32,-113],[-169,84]],[[52900,78285],[-142,16],[-125,108],[42,145],[-10,104]],[[52665,78658],[83,33],[141,-164],[39,156],[245,-25],[199,106],[133,-18],[87,-121],[26,100],[-40,385],[100,75],[98,272]],[[53776,79457],[206,-190],[157,242],[98,44],[215,-180],[131,30],[128,-111]],[[54711,79292],[-23,-75],[28,-205]],[[62817,73469],[-190,78],[-141,273],[-44,223]],[[63495,75281],[146,-311],[141,-419],[130,-28],[85,-159],[-228,-47],[-49,-459],[-48,-207],[-101,-138],[7,-293]],[[63578,73220],[-69,-29],[-173,309],[95,292],[-82,174],[-104,-44],[-327,-436]],[[62492,74950],[68,96],[207,-169],[149,-36],[38,70],[-136,319],[72,82]],[[62890,75312],[78,-20],[191,-359],[122,-40],[48,150],[166,238]],[[58149,47921],[-17,713],[-70,268]],[[58062,48902],[169,-46],[85,336],[147,-38]],[[58463,49154],[16,-233],[60,-134],[3,-192],[-69,-124],[-108,-308],[-101,-214],[-115,-28]],[[50920,80916],[204,-47],[257,123],[176,-258],[153,-138]],[[51710,80596],[-32,-400]],[[51678,80196],[-72,-22],[-30,-331]],[[51576,79843],[-243,269],[-143,-46],[-194,279],[-129,237],[-129,10],[-40,207]],[[50698,80799],[222,117]],[[50747,54278],[-229,-69]],[[50518,54209],[-69,407],[13,1357],[-56,122],[-11,290],[-96,207],[-85,174],[35,311]],[[50249,57077],[96,67],[56,258],[136,56],[61,176]],[[50598,57634],[93,173],[100,2],[212,-340]],[[51003,57469],[-11,-197],[62,-350],[-54,-238],[29,-159],[-135,-366],[-86,-181],[-52,-372],[7,-376],[-16,-952]],[[49214,56277],[-190,152],[-130,-22],[-97,-149],[-125,125],[-49,195],[-125,129]],[[48498,56707],[-18,343],[76,250],[-7,200],[221,490],[41,405],[76,144],[134,-79],[116,120],[38,152],[216,265],[53,184],[259,246],[153,84],[70,-114],[178,3]],[[50104,59400],[-22,-286],[37,-269],[156,-386],[9,-286],[320,-134],[-6,-405]],[[50249,57077],[-243,13]],[[50006,57090],[-128,47],[-90,-96],[-123,43],[-482,-27],[-7,-336],[38,-444]],[[75742,63602],[-6,-424],[-97,90],[18,-476]],[[75657,62792],[-79,308],[-16,301],[-53,285],[-116,344],[-256,23],[25,-243],[-87,-329],[-118,120],[-41,-108],[-78,65],[-108,53]],[[74730,63611],[-43,486],[-96,444],[47,356],[-171,159],[62,215],[173,220],[-200,313],[98,401],[220,-255],[133,-30],[24,-410],[265,-81],[257,8],[160,-101],[-128,-500],[-124,-34],[-86,-336],[152,-306],[46,377],[76,2],[147,-937]],[[56293,76715],[80,-243],[108,43],[213,-92],[408,-31],[138,150],[327,138],[202,-215],[163,-62]],[[57932,76403],[-144,-245],[-101,-422],[89,-337]],[[57776,75399],[-239,79],[-283,-186]],[[57254,75292],[-3,-294],[-252,-56],[-196,206],[-222,-162],[-206,17]],[[56375,75003],[-20,391],[-139,189]],[[56216,75583],[46,84],[-30,70],[47,188],[105,185],[-135,255],[-24,216],[68,134]],[[28462,64617],[-68,-29],[-70,340],[-104,171],[60,375],[84,-23],[97,-491],[1,-343]],[[28383,66284],[-303,-95],[-19,219],[130,47],[184,-18],[8,-153]],[[28611,66290],[-48,-420],[-51,75],[4,309],[-124,234],[-1,67],[220,-265]],[[55279,77084],[100,2],[-69,-260],[134,-227],[-41,-278],[-65,-27]],[[55338,76294],[-52,-53],[-90,-138],[-41,-325]],[[55155,75778],[-246,224],[-105,247],[-106,130],[-127,221],[-61,183],[-136,277],[59,245],[99,-136],[60,123],[130,13],[239,-98],[192,8],[126,-131]],[[56523,82432],[268,-4],[302,223],[64,333],[228,190],[-26,264]],[[57359,83438],[169,100],[298,228]],[[57826,83766],[293,-149],[39,-146],[146,70],[272,-141],[27,-277],[-60,-159],[174,-387],[113,-108],[-16,-107],[187,-104],[80,-157],[-108,-129],[-224,20],[-54,-55],[66,-196],[68,-379]],[[58829,81362],[-239,-35],[-85,-129],[-18,-298],[-111,57],[-250,-28],[-73,138],[-104,-103],[-105,86],[-218,12],[-310,141],[-281,47],[-215,-14],[-152,-160],[-133,-23]],[[56535,81053],[-6,263],[-85,274],[166,121],[2,235],[-77,225],[-12,261]],[[25238,61101],[-2,87],[33,27],[51,-70],[99,357],[53,8]],[[25472,61510],[1,-87],[53,-3],[-5,-160],[-45,-256],[24,-91],[-29,-212],[18,-56],[-32,-299],[-55,-156],[-50,-19],[-55,-205]],[[25297,59966],[-83,0],[22,667],[2,468]],[[31359,37147],[-200,-81],[-109,814],[-150,663],[88,572],[-146,250],[-37,426],[-136,402]],[[30669,40193],[175,638],[-119,496],[63,199],[-49,219],[108,295],[6,503],[13,415],[60,200],[-240,951]],[[30686,44109],[206,-50],[143,13],[62,179],[243,239],[147,222],[363,100],[-29,-443],[34,-227],[-23,-396],[302,-529],[311,-98],[109,-220],[188,-117],[115,-172],[175,6],[161,-175],[12,-342],[55,-172],[3,-255],[-81,-10],[107,-688],[533,-24],[-41,-342],[30,-233],[151,-166],[66,-367],[-49,-465],[-77,-259],[27,-337],[-87,-122]],[[33842,38659],[-4,182],[-259,302],[-258,9],[-484,-172],[-133,-520],[-7,-318],[-110,-708]],[[34826,35372],[54,341],[38,350],[0,325],[-100,107],[-104,-96],[-103,26],[-33,228],[-26,541],[-52,177],[-187,160],[-114,-116],[-293,113],[18,802],[-82,329]],[[30686,44109],[-157,-102],[-126,68],[18,898],[-228,-348],[-245,15],[-105,315],[-184,34],[59,254],[-155,359],[-115,532],[73,108],[0,250],[168,171],[-28,319],[71,206],[20,275],[318,402],[227,114],[37,89],[251,-28]],[[30585,48040],[125,1620],[6,256],[-43,339],[-123,215],[1,430],[156,97],[56,-61],[9,226],[-162,61],[-4,370],[541,-13],[92,203],[77,-187],[55,-349],[52,73]],[[31423,51320],[153,-312],[216,38],[54,181],[206,138],[115,97],[32,250],[198,168],[-15,124],[-235,51],[-39,372],[12,396],[-125,153],[52,55],[206,-76],[221,-148],[80,140],[200,92],[310,221],[102,225],[-37,167]],[[33129,53652],[145,26],[64,-136],[-36,-259],[96,-90],[63,-274],[-77,-209],[-44,-502],[71,-299],[20,-274],[171,-277],[137,-29],[30,116],[88,25],[126,104],[90,157],[154,-50],[67,21]],[[34294,51702],[151,-48],[25,120],[-46,118],[28,171],[112,-53],[131,61],[159,-125]],[[34854,51946],[121,-122],[86,160],[62,-25],[38,-166],[133,42],[107,224],[85,436],[164,540]],[[35650,53035],[95,28],[69,-327],[155,-1033],[149,-97],[7,-408],[-208,-487],[86,-178],[491,-92],[10,-593],[211,388],[349,-212],[462,-361],[135,-346],[-45,-327],[323,182],[540,-313],[415,23],[411,-489],[355,-662],[214,-170],[237,-24],[101,-186],[94,-752],[46,-358],[-110,-977],[-142,-385],[-391,-822],[-177,-668],[-206,-513],[-69,-11],[-78,-435],[20,-1107],[-77,-910],[-30,-390],[-88,-233],[-49,-790],[-282,-771],[-47,-610],[-225,-256],[-65,-355],[-302,2],[-437,-227],[-195,-263],[-311,-173],[-327,-470],[-235,-586],[-41,-441],[46,-326],[-51,-597],[-63,-289],[-195,-325],[-308,-1040],[-244,-468],[-189,-277],[-127,-562],[-183,-337]],[[35174,30629],[-77,334],[122,280],[-160,402],[-218,327],[-286,379],[-103,-18],[-279,457],[-180,-63]],[[81723,53254],[110,221],[236,323]],[[82069,53798],[-13,-291],[-16,-377],[-133,19],[-58,-202],[-126,307]],[[75471,66988],[113,-189],[-20,-363],[-227,-17],[-234,39],[-175,-92],[-252,224],[-6,119]],[[74670,66709],[184,439],[150,150],[198,-137],[147,-14],[122,-159]],[[58175,37528],[-393,-435],[-249,-442],[-93,-393],[-83,-222],[-152,-47],[-48,-283],[-28,-184],[-178,-138],[-226,29],[-133,166],[-117,71],[-135,-137],[-68,-283],[-132,-177],[-139,-264],[-199,-60],[-62,207],[26,360],[-165,562],[-75,88]],[[55526,35946],[0,1725],[274,20],[8,2105],[207,19],[428,207],[106,-243],[177,231],[85,2],[156,133]],[[56967,40145],[50,-44]],[[57017,40101],[107,-473],[56,-105],[87,-342],[315,-649],[119,-64],[0,-208],[82,-375],[215,-90],[177,-267]],[[54244,54965],[229,44],[52,152],[46,-11],[69,-134],[350,226],[118,230],[145,207],[-28,208],[78,54],[269,-36],[261,273],[201,645],[141,239],[176,101]],[[56351,57163],[31,-253],[160,-369],[1,-241],[-45,-246],[18,-184],[96,-170]],[[56612,55700],[212,-258]],[[56824,55442],[152,-239],[2,-192],[187,-308],[116,-255],[70,-355],[208,-234],[44,-187]],[[57603,53672],[-91,-63],[-178,14],[-209,62],[-104,-51],[-41,-143],[-90,-18],[-110,125],[-309,-295],[-127,60],[-38,-46],[-83,-357],[-207,115],[-203,59],[-177,218],[-229,200],[-149,-190],[-108,-300],[-25,-412]],[[55125,52650],[-178,33],[-188,99],[-166,-313],[-146,-550]],[[54447,51919],[-29,172],[-12,269],[-127,190],[-103,305],[-23,212],[-132,309],[23,176],[-28,249],[21,458],[67,107],[140,599]],[[32315,78082],[202,-79],[257,16],[-137,-242],[-102,-38],[-353,250],[-69,198],[105,183],[97,-288]],[[32831,79592],[-135,-11],[-360,186],[-258,279],[96,49],[365,-148],[284,-247],[8,-108]],[[15692,79240],[-140,-82],[-456,269],[-84,209],[-248,207],[-50,168],[-286,107],[-107,321],[24,137],[291,-129],[171,-89],[261,-63],[94,-204],[138,-280],[277,-244],[115,-327]],[[34407,80527],[-184,-517],[181,199],[187,-126],[-98,-206],[247,-162],[128,144],[277,-182],[-86,-433],[194,101],[36,-313],[86,-367],[-117,-520],[-125,-22],[-183,111],[60,484],[-77,75],[-322,-513],[-166,21],[196,277],[-267,144],[-298,-35],[-539,18],[-43,175],[173,208],[-121,160],[234,356],[287,941],[172,336],[241,204],[129,-26],[-54,-160],[-148,-372]],[[13005,82584],[131,-76],[267,47],[-84,-671],[242,-475],[-111,1],[-167,270],[-103,272],[-140,184],[-51,260],[16,188]],[[27981,87304],[-108,-310],[-123,50],[-73,176],[13,41],[107,177],[114,-13],[70,-121]],[[27250,87631],[-325,-326],[-196,13],[-61,160],[207,273],[381,-6],[-6,-114]],[[26344,89371],[51,-259],[143,91],[161,-155],[304,-203],[318,-184],[25,-281],[204,46],[199,-196],[-247,-186],[-432,142],[-156,266],[-275,-314],[-396,-306],[-95,346],[-377,-57],[242,292],[35,465],[95,542],[201,-49]],[[28926,90253],[-312,-30],[-69,289],[118,331],[255,82],[217,-163],[3,-253],[-32,-82],[-180,-174]],[[23431,91410],[-173,-207],[-374,179],[-226,-65],[-380,266],[245,183],[194,256],[295,-168],[166,-106],[84,-112],[169,-226]],[[31350,77248],[-181,334],[0,805],[-123,171],[-187,-100],[-92,155],[-212,-446],[-84,-460],[-99,-269],[-118,-91],[-89,-30],[-28,-146],[-512,0],[-422,-4],[-125,-109],[-294,-425],[-34,-46],[-89,-231],[-255,1],[-273,-3],[-125,-93],[44,-116],[25,-181],[-5,-60],[-363,-293],[-286,-93],[-323,-316],[-70,0],[-94,93],[-31,85],[6,61],[61,207],[131,325],[81,349],[-56,514],[-59,536],[-290,277],[35,105],[-41,73],[-76,0],[-56,93],[-14,140],[-54,-61],[-75,18],[17,59],[-65,58],[-27,155],[-216,189],[-224,197],[-272,229],[-261,214],[-248,-167],[-91,-6],[-342,154],[-225,-77],[-269,183],[-284,94],[-194,36],[-86,100],[-49,325],[-94,-3],[-1,-227],[-575,0],[-951,0],[-944,0],[-833,0],[-834,0],[-819,0],[-847,0],[-273,0],[-825,0],[-788,0]],[[15878,79530],[-38,1],[-537,581],[-199,255],[-503,244],[-155,523],[40,363],[-356,252],[-48,476],[-336,429],[-6,304]],[[13740,82958],[154,285],[-7,373],[-473,376],[-284,674],[-173,424],[-255,266],[-187,242],[-147,306],[-279,-192],[-270,-330],[-247,388],[-194,259],[-271,164],[-273,17],[1,3364],[2,2193]],[[10837,91767],[518,-142],[438,-285],[289,-54],[244,247],[336,184],[413,-72],[416,259],[455,148],[191,-245],[207,138],[62,278],[192,-63],[470,-530],[369,401],[38,-449],[341,97],[105,173],[337,-34],[424,-248],[650,-217],[383,-100],[272,38],[374,-300],[-390,-293],[502,-127],[750,70],[236,103],[296,-354],[302,299],[-283,251],[179,202],[338,27],[223,59],[224,-141],[279,-321],[310,47],[491,-266],[431,94],[405,-14],[-32,367],[247,103],[431,-200],[-2,-559],[177,471],[223,-16],[126,594],[-298,364],[-324,239],[22,653],[329,429],[366,-95],[281,-261],[378,-666],[-247,-290],[517,-120],[-1,-604],[371,463],[332,-380],[-83,-438],[269,-399],[290,427],[202,510],[16,649],[394,-46],[411,-87],[373,-293],[17,-293],[-207,-315],[196,-316],[-36,-288],[-544,-413],[-386,-91],[-287,178],[-83,-297],[-268,-498],[-81,-259],[-322,-399],[-397,-39],[-220,-250],[-18,-384],[-323,-74],[-340,-479],[-301,-665],[-108,-466],[-16,-686],[409,-99],[125,-553],[130,-448],[388,117],[517,-256],[277,-225],[199,-279],[348,-163],[294,-248],[459,-34],[302,-58],[-45,-511],[86,-594],[201,-661],[414,-561],[214,192],[150,607],[-145,934],[-196,311],[445,276],[314,415],[154,411],[-23,395],[-188,502],[-338,445],[328,619],[-121,535],[-93,922],[194,137],[476,-161],[286,-57],[230,155],[258,-200],[342,-343],[85,-229],[495,-45],[-8,-496],[92,-747],[254,-92],[201,-348],[402,328],[266,652],[184,274],[216,-527],[362,-754],[307,-709],[-112,-371],[370,-333],[250,-338],[442,-152],[179,-189],[110,-500],[216,-78],[112,-223],[20,-664],[-202,-222],[-199,-207],[-458,-210],[-349,-486],[-470,-96],[-594,125],[-417,4],[-287,-41],[-233,-424],[-354,-262],[-401,-782],[-320,-545],[236,97],[446,776],[583,493],[415,58],[246,-289],[-262,-397],[88,-637],[91,-446],[361,-295],[459,86],[278,664],[19,-429],[180,-214],[-344,-387],[-615,-351],[-276,-239],[-310,-426],[-211,44],[-11,500],[483,488],[-445,-19],[-309,-72]],[[18287,93781],[-139,-277],[618,179],[386,-298],[314,302],[254,-194],[227,-580],[140,244],[-197,606],[244,86],[276,-94],[311,-239],[175,-575],[86,-417],[466,-293],[502,-279],[-31,-260],[-456,-48],[178,-227],[-94,-217],[-503,93],[-478,160],[-322,-36],[-522,-201],[-704,-88],[-494,-56],[-151,279],[-379,161],[-246,-66],[-343,468],[185,62],[429,101],[392,-26],[362,103],[-537,138],[-594,-47],[-394,12],[-146,217],[644,237],[-428,-9],[-485,156],[233,443],[193,235],[744,359],[284,-114]],[[20972,93958],[-244,-390],[-434,413],[95,83],[372,24],[211,-130]],[[28794,93770],[25,-163],[-296,17],[-299,13],[-304,-80],[-80,36],[-306,313],[12,213],[133,39],[636,-63],[479,-325]],[[25955,93803],[219,-369],[256,477],[704,242],[477,-611],[-42,-387],[550,172],[263,235],[616,-299],[383,-282],[36,-258],[515,134],[290,-376],[670,-234],[242,-238],[263,-553],[-510,-275],[654,-386],[441,-130],[400,-543],[437,-39],[-87,-414],[-487,-687],[-342,253],[-437,568],[-359,-74],[-35,-338],[292,-344],[377,-272],[114,-157],[181,-584],[-96,-425],[-350,160],[-697,473],[393,-509],[289,-357],[45,-206],[-753,236],[-596,343],[-337,287],[97,167],[-414,304],[-405,286],[5,-171],[-803,-94],[-235,203],[183,435],[522,10],[571,76],[-92,211],[96,294],[360,576],[-77,261],[-107,203],[-425,286],[-563,201],[178,150],[-294,367],[-245,34],[-219,201],[-149,-175],[-503,-76],[-1011,132],[-588,174],[-450,89],[-231,207],[290,270],[-394,2],[-88,599],[213,528],[286,241],[717,158],[-204,-382]],[[22123,94208],[331,-124],[496,75],[72,-172],[-259,-283],[420,-254],[-50,-532],[-455,-229],[-268,50],[-192,225],[-690,456],[5,189],[567,-73],[-306,386],[329,286]],[[24112,93575],[-298,-442],[-317,22],[-173,519],[4,294],[145,251],[276,161],[579,-20],[530,-144],[-415,-526],[-331,-115]],[[16539,92755],[-731,-285],[-147,259],[-641,312],[119,250],[192,432],[241,388],[-272,362],[939,93],[397,-123],[709,-33],[270,-171],[298,-249],[-349,-149],[-681,-415],[-344,-414],[0,-257]],[[23996,94879],[-151,-229],[-403,44],[-337,155],[148,266],[399,159],[243,-208],[101,-187]],[[22639,95907],[212,-273],[9,-303],[-127,-440],[-458,-60],[-298,94],[5,345],[-455,-46],[-18,457],[299,-18],[419,201],[390,-34],[22,77]],[[19941,95601],[109,-210],[247,99],[291,-26],[49,-289],[-169,-281],[-940,-91],[-701,-256],[-423,-14],[-35,193],[577,261],[-1255,-70],[-389,106],[379,577],[262,165],[782,-199],[493,-350],[485,-45],[-397,565],[255,215],[286,-68],[94,-282]],[[23699,96131],[308,-190],[547,1],[240,-194],[-64,-222],[319,-134],[177,-140],[374,-26],[406,-50],[441,128],[566,51],[451,-42],[298,-223],[62,-244],[-174,-157],[-414,-127],[-355,72],[-797,-91],[-570,-11],[-449,73],[-738,190],[-96,325],[-34,293],[-279,258],[-574,72],[-322,183],[104,242],[573,-37]],[[17722,96454],[-38,-454],[-214,-205],[-259,-29],[-517,-252],[-444,-91],[-377,128],[472,442],[570,383],[426,-9],[381,87]],[[23933,96380],[-126,-17],[-521,38],[-74,165],[559,-9],[195,-109],[-33,-68]],[[19392,96485],[-518,-170],[-411,191],[224,188],[406,60],[392,-92],[-93,-177]],[[19538,97019],[-339,-115],[-461,1],[5,84],[285,177],[149,-27],[361,-120]],[[23380,96697],[-411,-122],[-226,138],[-119,221],[-22,245],[360,-24],[162,-39],[332,-205],[-76,-214]],[[22205,96856],[108,-247],[-453,66],[-457,192],[-619,21],[268,176],[-335,142],[-21,227],[546,-81],[751,-215],[212,-281]],[[25828,97644],[334,-190],[-381,-176],[-513,-445],[-492,-42],[-575,76],[-299,240],[4,215],[220,157],[-508,-4],[-306,196],[-176,268],[193,262],[192,180],[285,42],[-122,135],[646,30],[355,-315],[468,-127],[455,-112],[220,-390]],[[30972,99681],[742,-47],[597,-75],[508,-161],[-12,-157],[-678,-257],[-672,-119],[-251,-133],[605,3],[-656,-358],[-452,-167],[-476,-483],[-573,-98],[-177,-120],[-841,-64],[383,-74],[-192,-105],[230,-292],[-264,-202],[-429,-167],[-132,-232],[-388,-176],[39,-134],[475,23],[6,-144],[-742,-355],[-726,163],[-816,-91],[-414,71],[-525,31],[-35,284],[514,133],[-137,427],[170,41],[742,-255],[-379,379],[-450,113],[225,229],[492,141],[79,206],[-392,231],[-118,304],[759,-26],[220,-64],[433,216],[-625,68],[-972,-38],[-491,201],[-232,239],[-324,173],[-61,202],[413,112],[324,19],[545,96],[409,220],[344,-30],[300,-166],[211,319],[367,95],[498,65],[849,24],[148,-63],[802,100],[601,-38],[602,-37]],[[52900,78285],[-22,-242],[-122,-100],[-206,75],[-60,-239],[-132,-19],[-48,94],[-156,-200],[-134,-28],[-120,126]],[[51900,77752],[-95,259],[-133,-92],[5,267],[203,332],[-9,150],[126,-54],[77,101]],[[52074,78715],[236,-4],[57,128],[298,-181]],[[31400,18145],[-92,-239],[-238,-183],[-137,19],[-164,48],[-202,177],[-291,86],[-350,330],[-283,317],[-383,662],[229,-124],[390,-395],[369,-212],[143,271],[90,405],[256,244],[198,-70]],[[30952,19680],[-247,4],[-134,-145],[-250,-213],[-45,-552],[-118,-14],[-313,192],[-318,412],[-346,338],[-87,374],[79,346],[-140,393],[-36,1007],[119,568],[293,457],[-422,172],[265,522],[94,982],[309,-208],[145,1224],[-186,157],[-87,-738],[-175,83],[87,845],[95,1095],[127,404],[-80,576],[-22,666],[117,19],[170,954],[192,945],[118,881],[-64,885],[83,487],[-34,730],[163,721],[50,1143],[89,1227],[87,1321],[-20,967],[-58,832]],[[30452,39739],[143,151],[74,303]],[[80649,61615],[-240,-284],[-228,183],[-8,509],[137,267],[304,166],[159,-14],[62,-226],[-122,-260],[-64,-341]],[[86288,75628],[-179,348],[-111,-331],[-429,-254],[44,-312],[-241,22],[-131,185],[-191,-419],[-306,-318],[-227,-379]],[[84517,74170],[-388,-171],[-204,-277],[-300,-161],[148,274],[-58,230],[220,397],[-147,310],[-242,-209],[-314,-411],[-171,-381],[-272,-29],[-142,-275],[147,-400],[227,-97],[9,-265],[220,-173],[311,422],[247,-230],[179,-15],[45,-310],[-393,-165],[-130,-319],[-270,-296],[-142,-414],[299,-325],[109,-581],[169,-541],[189,-454],[-5,-439],[-174,-161],[66,-315],[164,-184],[-43,-481],[-71,-468],[-155,-53],[-203,-640],[-225,-775],[-258,-705],[-382,-545],[-386,-498],[-313,-68],[-170,-262],[-96,192],[-157,-294],[-388,-296],[-294,-90],[-95,-624],[-154,-35],[-73,429],[66,228],[-373,189],[-131,-96]],[[80013,63313],[-280,154],[-132,240],[44,340],[-254,108],[-134,222],[-236,-315],[-271,-68],[-221,3],[-149,-145]],[[78380,63852],[-144,-86],[42,-676],[-148,16],[-25,139]],[[78105,63245],[-9,244],[-203,-172],[-121,109],[-206,222],[81,490],[-176,115],[-66,544],[-293,-98],[33,701],[263,493],[11,487],[-8,452],[-121,141],[-93,348],[-162,-44]],[[77035,67277],[-300,89],[94,248],[-130,367],[-198,-249],[-233,145],[-321,-376],[-252,-439],[-224,-74]],[[74670,66709],[-23,465],[-170,-124]],[[74477,67050],[-324,57],[-314,136],[-225,259],[-216,117],[-93,284],[-157,84],[-280,385],[-223,182],[-115,-141]],[[72530,68413],[-386,413],[-273,374],[-78,651],[200,-79],[9,301],[-111,303],[28,482],[-298,692]],[[71621,71550],[-457,239],[-82,454],[-205,276]],[[70827,72688],[-42,337],[10,230],[-169,134],[-91,-59],[-70,546]],[[70465,73876],[79,136],[-39,138],[266,279],[192,116],[294,-80],[105,378],[356,70],[99,234],[438,320],[39,134]],[[72294,75601],[-22,337],[190,154],[-250,1026],[550,236],[143,131],[200,1058],[551,-194],[155,267],[13,592],[230,56],[212,393]],[[74266,79657],[109,49]],[[74375,79706],[73,-413],[233,-313],[396,-222],[192,-476],[-107,-690],[100,-256],[330,-101],[374,-83],[336,-368],[171,-66],[127,-544],[163,-351],[306,14],[574,-133],[369,82],[274,-88],[411,-359],[336,1],[123,-184],[324,318],[448,205],[417,22],[324,208],[200,316],[194,199],[-45,195],[-89,227],[146,381],[156,-53],[286,-120],[277,313],[423,229],[204,391],[195,168],[404,78],[219,-66],[30,210],[-251,413],[-223,189],[-214,-219],[-274,92],[-157,-74],[-72,241],[197,590],[135,446]],[[82410,80055],[333,-223],[392,373],[-3,260],[251,627],[155,189],[-4,326],[-152,141],[229,294],[345,106],[369,16],[415,-176],[244,-217],[172,-596],[104,-254],[97,-363],[103,-579],[483,-189],[329,-420],[112,-555],[423,-1],[240,233],[459,175],[-146,-532],[-107,-216],[-96,-647],[-186,-575],[-338,104],[-238,-208],[73,-506],[-40,-698],[-142,-16],[2,-300]],[[49206,53531],[-126,-7],[-194,116],[-178,-7],[-329,-103],[-193,-170],[-275,-217],[-54,15]],[[47857,53158],[22,487],[26,74],[-8,233],[-118,247],[-88,40],[-81,162],[60,262],[-28,286],[13,172]],[[47655,55121],[44,0],[17,258],[-22,114],[27,82],[103,71],[-69,473],[-64,245],[23,200],[55,46]],[[47769,56610],[36,54],[77,-89],[215,-5],[51,172],[48,-11],[80,67],[43,-253],[65,74],[114,88]],[[49214,56277],[74,-841],[-117,-496],[-73,-667],[121,-509],[-13,-233]],[[53632,51919],[-35,32],[-164,-76],[-169,79],[-132,-38]],[[53132,51916],[-452,13]],[[52680,51929],[40,466],[-108,391],[-127,100],[-56,265],[-72,85],[4,163]],[[52361,53399],[71,418],[132,570],[81,6],[165,345],[105,10],[156,-243],[191,199],[26,246],[63,238],[43,299],[148,243],[56,414],[59,132],[39,307],[74,377],[234,457],[14,196],[31,107],[-110,235]],[[53939,57955],[9,188],[78,34]],[[54026,58177],[111,-378],[18,-392],[-10,-393],[151,-537],[-155,6],[-78,-42],[-127,60],[-60,-279],[164,-345],[121,-100],[39,-245],[87,-407],[-43,-160]],[[54447,51919],[-20,-319],[-220,140],[-225,156],[-350,23]],[[58564,52653],[-16,-691],[111,-80],[-89,-210],[-107,-157],[-106,-308],[-59,-274],[-15,-475],[-65,-225],[-2,-446]],[[58216,49787],[-80,-165],[-10,-351],[-38,-46],[-26,-323]],[[58149,47921],[50,-544],[-27,-307]],[[58172,47070],[55,-343],[161,-330]],[[58388,46397],[150,-745]],[[58538,45652],[-109,60],[-373,-99],[-75,-71],[-79,-377],[62,-261],[-49,-699],[-34,-593],[75,-105],[194,-230],[76,107],[23,-637],[-212,5],[-114,325],[-103,252],[-213,82],[-62,310],[-170,-187],[-222,83],[-93,268],[-176,55],[-131,-15],[-15,184],[-96,15]],[[53422,46976],[-39,183]],[[53609,47755],[73,-60],[95,226],[152,-6],[17,-167],[104,-105],[164,370],[161,289],[71,189],[-10,486],[121,574],[127,304],[183,285],[32,189],[7,216],[45,205],[-14,335],[34,524],[55,368],[83,316],[16,357]],[[57603,53672],[169,-488],[124,-71],[75,99],[128,-39],[155,125],[66,-252],[244,-393]],[[53309,47603],[-228,626]],[[53081,48229],[212,326],[-105,391],[95,148],[187,73],[23,261],[148,-283],[245,-25],[85,279],[36,393],[-31,461],[-131,350],[120,684],[-69,117],[-207,-48],[-78,305],[21,258]],[[29063,50490],[-119,140],[-137,195],[-79,-94],[-235,82],[-68,255],[-52,-10],[-278,338]],[[28095,51396],[-37,183],[103,44],[-12,296],[65,214],[138,40],[117,371],[106,310],[-102,141],[52,343],[-62,540],[59,155],[-44,500],[-112,315]],[[28366,54848],[36,287],[89,-43],[52,176],[-64,348],[34,86]],[[28513,55702],[143,-18],[209,412],[114,63],[3,195],[51,500],[159,274],[175,11],[22,123],[218,-49],[218,298],[109,132],[134,285],[98,-36],[73,-156],[-54,-199]],[[30185,57537],[-178,-99],[-71,-295],[-107,-169],[-81,-220],[-34,-422],[-77,-345],[144,-40],[35,-271],[62,-130],[21,-238],[-33,-219],[10,-123],[69,-49],[66,-207],[357,57],[161,-75],[196,-508],[112,63],[200,-32],[158,68],[99,-102],[-50,-318],[-62,-199],[-22,-423],[56,-393],[79,-175],[9,-133],[-140,-294],[100,-130],[74,-207],[85,-589]],[[30585,48040],[-139,314],[-83,14],[179,602],[-213,276],[-166,-51],[-101,103],[-153,-157],[-207,74],[-163,620],[-129,152],[-89,279],[-184,280],[-74,-56]],[[26954,55439],[-151,131],[-56,124],[32,103],[-11,130],[-77,142],[-109,116],[-95,76],[-19,173],[-73,105],[18,-172],[-55,-141],[-64,164],[-89,58],[-38,120],[2,179],[36,187],[-78,83],[64,114]],[[26191,57131],[42,76],[183,-156],[63,77],[89,-50],[46,-121],[82,-40],[66,126]],[[26762,57043],[70,-321],[108,-238],[130,-252]],[[27070,56232],[-107,-53],[1,-238],[58,-88],[-41,-70],[10,-107],[-23,-120],[-14,-117]],[[27147,64280],[240,-42],[219,-7],[261,-201],[110,-216],[260,66],[98,-138],[235,-366],[173,-267],[92,8],[165,-120],[-20,-167],[205,-24],[210,-242],[-33,-138],[-185,-75],[-187,-29],[-191,46],[-398,-57],[186,329],[-113,154],[-179,39],[-96,171],[-66,336],[-157,-23],[-259,159],[-83,124],[-362,91],[-97,115],[104,148],[-273,30],[-199,-307],[-115,-8],[-40,-144],[-138,-65],[-118,56],[146,183],[60,213],[126,131],[142,116],[210,56],[67,65]],[[59092,71341],[19,3],[40,143],[200,-8],[253,176],[-188,-251],[21,-111]],[[59437,71293],[-30,21],[-53,-45],[-42,12],[-14,-22],[-5,59],[-20,37],[-54,6],[-75,-51],[-52,31]],[[59437,71293],[8,-48],[-285,-240],[-136,77],[-64,237],[132,22]],[[53776,79457],[-157,254],[-141,142],[-30,249],[-49,176],[202,129],[103,147],[200,114],[70,113],[73,-68],[124,62]],[[54171,80775],[132,-191],[207,-51],[-17,-163],[151,-122],[41,153],[191,-66],[26,-185],[207,-36],[127,-291]],[[55236,79823],[-82,-1],[-43,-106],[-64,-26],[-18,-134],[-54,-28],[-7,-55],[-95,-61],[-123,10],[-39,-130]],[[52756,83065],[4,-228],[281,-138],[-3,-210],[283,111],[156,162],[313,-233],[132,-189]],[[53922,82340],[64,-300],[-77,-158],[101,-210],[69,-316],[-22,-204],[114,-377]],[[52074,78715],[35,421],[140,404],[-400,109],[-131,155]],[[51718,79804],[16,259],[-56,133]],[[51710,80596],[-47,619],[167,0],[70,222],[69,541],[-51,200]],[[51918,82178],[54,125],[232,32],[52,-130],[188,291],[-63,222],[-13,335]],[[52368,83053],[210,-78],[178,90]],[[61966,58083],[66,-183],[-9,-245],[-158,-142],[119,-161]],[[61984,57352],[-102,-317]],[[61882,57035],[-62,106],[-67,-42],[-155,10],[-4,180],[-22,163],[94,277],[98,261]],[[61764,57990],[119,-51],[83,144]],[[53524,83435],[-166,-478],[-291,333],[-39,246],[408,195],[88,-296]],[[52368,83053],[-113,328],[-8,604],[46,159],[80,177],[244,37],[98,163],[223,167],[-9,-304],[-82,-192],[33,-166],[151,-89],[-68,-223],[-83,64],[-200,-425],[76,-288]],[[30080,62227],[34,101],[217,-3],[165,-152],[73,15],[50,-209],[152,11],[-9,-176],[124,-21],[136,-217],[-103,-240],[-132,128],[-127,-25],[-92,28],[-50,-107],[-106,-37],[-43,144],[-92,-85],[-111,-405],[-71,94],[-14,170]],[[30081,61241],[5,161],[-71,177],[68,99],[21,228],[-24,321]],[[53333,64447],[-952,-1126],[-804,-1161],[-392,-263]],[[51185,61897],[-308,-58],[-3,376],[-129,96],[-173,169],[-66,277],[-937,1289],[-937,1289]],[[48632,65335],[-1045,1431]],[[47587,66766],[6,114],[-1,40]],[[47592,66920],[-2,700],[449,436],[277,90],[227,159],[107,295],[324,234],[12,438],[161,51],[126,219],[363,99],[51,230],[-73,125],[-96,624],[-17,359],[-104,379]],[[49397,71358],[267,323],[300,102],[175,244],[268,180],[471,105],[459,48],[140,-87],[262,232],[297,5],[113,-137],[190,35]],[[52339,72408],[-57,-303],[44,-563],[-65,-487],[-171,-330],[24,-445],[227,-352],[3,-143],[171,-238],[118,-1061]],[[52633,68486],[90,-522],[15,-274],[-49,-482],[21,-270],[-36,-323],[24,-371],[-110,-247],[164,-431],[11,-253],[99,-330],[130,109],[219,-275],[122,-370]],[[27693,48568],[148,442],[-60,258],[-106,-275],[-166,259],[56,167],[-47,536],[97,89],[52,368],[105,381],[-20,241],[153,126],[190,236]],[[29063,50490],[38,-449],[-86,-384],[-303,-619],[-334,-233],[-170,-514],[-53,-398],[-157,-243],[-116,298],[-113,64],[-114,-47],[-8,216],[79,141],[-33,246]],[[59700,68010],[-78,-238],[-60,-446],[-75,-308],[-65,-103],[-93,191],[-125,263],[-198,847],[-29,-53],[115,-624],[171,-594],[210,-920],[102,-321],[90,-334],[249,-654],[-55,-103],[9,-384],[323,-530],[49,-121]],[[60240,63578],[-1102,0],[-1077,0],[-1117,0]],[[56944,63578],[0,2175],[0,2101],[-83,476],[71,365],[-43,253],[101,283]],[[56990,69231],[369,10],[268,-156],[275,-175],[129,-92],[214,188],[114,169],[245,49],[198,-75],[75,-293],[65,193],[222,-140],[217,-33],[137,149]],[[59518,69025],[182,-1015]],[[61764,57990],[-95,191],[-114,346],[-124,190],[-71,204],[-242,237],[-191,7],[-67,124],[-163,-139],[-168,268],[-87,-441],[-323,124]],[[60119,59101],[-30,236],[120,868],[27,393],[88,181],[204,97],[141,337]],[[60669,61213],[161,-684],[77,-542],[152,-288],[379,-558],[154,-336],[151,-341],[87,-203],[136,-178]],[[47490,75324],[14,420],[-114,257],[393,426],[340,-106],[373,3],[296,-101],[230,31],[449,-19]],[[49471,76235],[111,-230],[511,-268],[101,127],[313,-267],[322,77]],[[50829,75674],[15,-344],[-263,-393],[-356,-125],[-25,-199],[-171,-327],[-107,-481],[108,-338],[-160,-263],[-60,-384],[-210,-118],[-197,-454],[-352,-9],[-265,11],[-174,-209],[-106,-223],[-136,49],[-103,199],[-79,340],[-259,92]],[[47929,72498],[-23,195],[103,222],[38,161],[-96,175],[77,388],[-111,355],[120,48],[11,280],[45,86],[3,461],[129,160],[-78,296],[-162,21],[-47,-75],[-164,0],[-70,289],[-113,-86],[-101,-150]],[[56753,84725],[32,349],[-102,-75],[-176,210],[-24,340],[351,164],[350,86],[301,-97],[287,17]],[[57772,85719],[42,-103],[-198,-341],[83,-551],[-120,-187]],[[57579,84537],[-229,1],[-239,219],[-121,73],[-237,-105]],[[61882,57035],[-61,-209],[103,-325],[102,-285],[106,-210],[909,-702],[233,4]],[[63274,55308],[-785,-1773],[-362,-26],[-247,-417],[-178,-11],[-76,-186]],[[61626,52895],[-190,0],[-112,200],[-254,-247],[-82,-247],[-185,47],[-62,68],[-65,-16],[-87,6],[-352,502],[-193,0],[-95,194],[0,332],[-145,99]],[[59804,53833],[-164,643],[-127,137],[-48,236],[-141,288],[-171,42],[95,337],[147,14],[42,181]],[[59437,55711],[-4,531]],[[59433,56242],[82,618],[132,166],[28,241],[119,451],[168,293],[112,582],[45,508]],[[57942,91385],[-41,-414],[425,-394],[-256,-445],[323,-673],[-187,-506],[250,-440],[-113,-385],[411,-405],[-105,-301],[-258,-341],[-594,-755]],[[57797,86326],[-504,-47],[-489,-216],[-452,-125],[-161,323],[-269,193],[62,582],[-135,533],[133,345],[252,371],[635,640],[185,124],[-28,250],[-387,279]],[[56639,89578],[-93,230],[-8,910],[-433,402],[-371,289]],[[55734,91409],[167,156],[309,-312],[362,29],[298,-143],[265,262],[137,433],[431,200],[356,-235],[-117,-414]],[[99547,40335],[96,-171],[-46,-308],[-172,-81],[-153,73],[-27,260],[107,203],[126,-74],[69,98]],[[0,41087],[57,27],[-34,-284],[-23,-32],[99822,-145],[-177,-124],[-36,220],[139,121],[88,33],[-99836,184]],[[33000,19946],[333,354],[236,-148],[167,237],[222,-266],[-83,-207],[-375,-177],[-125,207],[-236,-266],[-139,266]],[[34854,51946],[70,252],[24,269],[48,253],[-107,349]],[[34889,53069],[-22,404],[144,508]],[[35011,53981],[95,-65],[204,-140],[294,-499],[46,-242]],[[52655,75484],[-92,-456],[-126,120],[-64,398],[56,219],[179,226],[47,-507]],[[51576,79843],[62,-52],[80,13]],[[51900,77752],[-11,-167],[82,-222],[-97,-180],[72,-457],[151,-75],[-32,-256]],[[52065,76395],[-252,-334],[-548,160],[-404,-192],[-32,-355]],[[49471,76235],[144,354],[53,1177],[-287,620],[-205,299],[-424,227],[-28,431],[360,129],[466,-152],[-88,669],[263,-254],[646,461],[84,484],[243,119]],[[53081,48229],[-285,596],[-184,488],[-169,610],[9,196],[61,189],[67,430],[56,438]],[[52636,51176],[94,35],[404,-6],[-2,711]],[[48278,82406],[-210,122],[-172,-9],[57,317],[-57,317]],[[47896,83153],[233,24],[298,-365],[-149,-406]],[[49165,85222],[-297,-639],[283,81],[304,-3],[-72,-481],[-250,-530],[287,-38],[22,-62],[248,-697],[190,-95],[171,-673],[79,-233],[337,-113],[-34,-378],[-142,-173],[111,-305],[-250,-310],[-371,6],[-473,-163],[-130,116],[-183,-276],[-257,67],[-195,-226],[-148,118],[407,621],[249,127],[-2,1],[-434,98],[-79,235],[291,183],[-152,319],[52,387],[413,-54],[1,0],[40,343],[-186,364],[-4,8],[-337,104],[-66,160],[101,264],[-92,163],[-149,-279],[-17,569],[-140,301],[101,611],[216,480],[222,-47],[335,49]],[[61542,75120],[42,252],[-70,403],[-160,218],[-154,68],[-102,181]],[[61098,76242],[34,70],[235,-101],[409,-96],[378,-283],[48,-110],[169,93],[259,-124],[85,-242],[175,-137]],[[62106,74858],[-268,290],[-296,-28]],[[50294,54083],[-436,-346],[-154,-203],[-250,-171],[-248,168]],[[50006,57090],[-20,-184],[116,-305],[-1,-429],[27,-466],[69,-215],[-61,-532],[22,-294],[74,-375],[62,-207]],[[47655,55121],[-78,15],[-57,-238],[-78,3],[-55,126],[19,237],[-116,362],[-73,-67],[-59,-13]],[[47158,55546],[-77,-34],[3,217],[-44,155],[9,171],[-60,249],[-78,211],[-222,1],[-65,-112],[-76,-13],[-48,-128],[-32,-163],[-148,-260]],[[46320,55840],[-122,349],[-108,232],[-71,76],[-69,118],[-32,261],[-41,130],[-80,97]],[[45797,57103],[123,288],[84,-11],[73,99],[61,1],[44,78],[-24,196],[31,62],[5,200]],[[46194,58016],[134,-6],[200,-144],[61,13],[21,66],[151,-47],[40,33]],[[46801,57931],[16,-216],[44,1],[73,78],[46,-19],[77,-150],[119,-48],[76,128],[90,79],[67,83],[55,-15],[62,-130],[33,-163],[114,-248],[-57,-152],[-11,-192],[59,58],[35,-69],[-15,-176],[85,-170]],[[45321,58350],[36,262]],[[45357,58612],[302,17],[63,140],[88,9],[110,-145],[86,-3],[92,99],[56,-170],[-120,-133],[-121,11],[-119,124],[-103,-136],[-50,-5],[-67,-83],[-253,13]],[[45797,57103],[-149,247],[-117,39],[-63,166],[1,90],[-84,125],[-18,127]],[[45367,57897],[147,96],[92,-19],[75,67],[513,-25]],[[52636,51176],[-52,90],[96,663]],[[56583,71675],[152,-199],[216,34],[207,-42],[-7,-103],[151,71],[-35,-175],[-400,-50],[3,98],[-339,115],[52,251]],[[57237,74699],[-169,17],[-145,56],[-336,-154],[192,-332],[-141,-96],[-154,-1],[-147,305],[-52,-130],[62,-353],[139,-277],[-105,-129],[155,-273],[137,-171],[4,-334],[-257,157],[82,-302],[-176,-62],[105,-521],[-184,-8],[-228,257],[-104,473],[-49,393],[-108,272],[-143,337],[-18,168]],[[55838,74710],[182,53],[106,129],[150,-12],[46,103],[53,20]],[[57254,75292],[135,-157],[-86,-369],[-66,-67]],[[37010,99398],[932,353],[975,-27],[354,218],[982,57],[2219,-74],[1737,-469],[-513,-227],[-1062,-26],[-1496,-58],[140,-105],[984,65],[836,-204],[540,181],[231,-212],[-305,-344],[707,220],[1348,229],[833,-114],[156,-253],[-1132,-420],[-157,-136],[-888,-102],[643,-28],[-324,-431],[-224,-383],[9,-658],[333,-386],[-434,-24],[-457,-187],[513,-313],[65,-502],[-297,-55],[360,-508],[-617,-42],[322,-241],[-91,-208],[-391,-91],[-388,-2],[348,-400],[4,-263],[-549,244],[-143,-158],[375,-148],[364,-361],[105,-476],[-495,-114],[-214,228],[-344,340],[95,-401],[-322,-311],[732,-25],[383,-32],[-745,-515],[-755,-466],[-813,-204],[-306,-2],[-288,-228],[-386,-624],[-597,-414],[-192,-24],[-370,-145],[-399,-138],[-238,-365],[-4,-415],[-141,-388],[-453,-472],[112,-462],[-125,-488],[-142,-577],[-391,-36],[-410,482],[-556,3],[-269,324],[-186,577],[-481,735],[-141,385],[-38,530],[-384,546],[100,435],[-186,208],[275,691],[418,220],[110,247],[58,461],[-318,-209],[-151,-88],[-249,-84],[-341,193],[-19,401],[109,314],[258,9],[567,-157],[-478,375],[-249,202],[-276,-83],[-232,147],[310,550],[-169,220],[-220,409],[-335,626],[-353,230],[3,247],[-745,346],[-590,43],[-743,-24],[-677,-44],[-323,188],[-482,372],[729,186],[559,31],[-1188,154],[-627,241],[39,229],[1051,285],[1018,284],[107,214],[-750,213],[243,235],[961,413],[404,63],[-115,265],[658,156],[854,93],[853,5],[303,-184],[737,325],[663,-221],[390,-46],[577,-192],[-660,318],[38,253]],[[24973,58695],[-142,103],[-174,11],[-127,117],[-149,244]],[[24381,59170],[7,172],[32,138],[-39,111],[133,481],[357,2],[7,201],[-45,36],[-31,128],[-103,136],[-103,198],[125,1],[1,333],[259,1],[257,-7]],[[25297,59966],[90,-107],[24,88],[82,-75]],[[25493,59872],[-127,-225],[-131,-166],[-20,-113],[22,-116],[-58,-150]],[[25179,59102],[-65,-37],[15,-69],[-52,-66],[-95,-149],[-9,-86]],[[33400,55523],[183,-217],[171,-385],[8,-304],[105,-14],[149,-289],[109,-205]],[[34125,54109],[-44,-532],[-169,-154],[15,-139],[-51,-305],[123,-429],[89,-1],[37,-333],[169,-514]],[[33129,53652],[-188,448],[75,163],[-5,273],[171,95],[69,110],[-95,220],[24,215],[220,347]],[[25745,58251],[-48,185],[-84,51]],[[25613,58487],[19,237],[-38,64],[-57,42],[-122,-70],[-10,79],[-84,95],[-60,118],[-82,50]],[[25493,59872],[29,-23],[61,104],[79,8],[26,-48],[43,29],[129,-53],[128,15],[90,66],[32,66],[89,-31],[66,-40],[73,14],[55,51],[127,-82],[44,-13],[85,-110],[80,-132],[101,-91],[73,-162]],[[26903,59440],[-95,12],[-38,-81],[-97,-77],[-70,0],[-61,-76],[-56,27],[-47,90],[-29,-17],[-36,-141],[-27,5],[-4,-121],[-97,-163],[-51,-70],[-29,-74],[-82,120],[-60,-158],[-58,4],[-65,-14],[6,-290],[-41,-5],[-35,-135],[-86,-25]],[[55230,77704],[67,-229],[89,-169],[-107,-222]],[[55155,75778],[-31,-100]],[[55124,75678],[-261,218],[-161,213],[-254,176],[-233,434],[56,45],[-127,248],[-5,200],[-179,93],[-85,-255],[-82,198],[6,205],[10,9]],[[53809,77462],[194,-20],[51,100],[94,-97],[109,-11],[-1,165],[97,60],[27,239],[221,157]],[[54601,78055],[88,-73],[208,-253],[229,-114],[104,89]],[[30081,61241],[-185,100],[-131,-41],[-169,43],[-130,-110],[-149,184],[24,190],[256,-82],[210,-47],[100,131],[-127,256],[2,226],[-175,92],[62,163],[170,-26],[241,-93]],[[54716,79012],[141,-151],[103,-65],[233,73],[22,118],[111,18],[135,92],[30,-38],[130,74],[66,139],[91,36],[297,-180],[59,61]],[[56134,79189],[155,-161],[19,-159]],[[56308,78869],[-170,-123],[-131,-401],[-168,-401],[-223,-111]],[[55616,77833],[-173,26],[-213,-155]],[[54601,78055],[-54,200],[-47,6]],[[83531,44530],[-117,-11],[-368,414],[259,116],[146,-180],[97,-180],[-17,-159]],[[84713,45326],[28,-117],[5,-179]],[[84746,45030],[-181,-441],[-238,-130],[-33,71],[25,201],[119,360],[275,235]],[[82749,45797],[100,-158],[172,48],[69,-251],[-321,-119],[-193,-79],[-149,5],[95,340],[153,5],[74,209]],[[84139,45797],[-41,-328],[-417,-168],[-370,73],[0,216],[220,123],[174,-177],[185,45],[249,216]],[[80172,46575],[533,-59],[61,244],[515,-284],[101,-383],[417,-108],[341,-351],[-317,-225],[-306,238],[-251,-16],[-288,44],[-260,106],[-322,225],[-204,59],[-116,-74],[-506,243],[-48,254],[-255,44],[191,564],[337,-35],[224,-231],[115,-45],[38,-210]],[[87423,46908],[-143,-402],[-27,445],[49,212],[58,200],[63,-173],[0,-282]],[[85346,48536],[-104,-196],[-192,108],[-54,254],[281,29],[69,-195]],[[86241,48752],[101,-452],[-234,244],[-232,49],[-157,-39],[-192,21],[65,325],[344,24],[305,-172]],[[89166,49043],[5,-1925],[4,-1925]],[[89175,45193],[-247,485],[-282,118],[-69,-168],[-352,-18],[118,481],[175,164],[-72,642],[-134,496],[-538,500],[-229,50],[-417,546],[-82,-287],[-107,-52],[-63,216],[-1,257],[-212,290],[299,213],[198,-11],[-23,156],[-407,1],[-110,352],[-248,109],[-117,293],[374,143],[142,192],[446,-242],[44,-220],[78,-955],[287,-354],[232,627],[319,356],[247,1],[238,-206],[206,-212],[298,-113]],[[84788,51419],[-223,-587],[-209,-113],[-267,115],[-463,-29],[-243,-85],[-39,-447],[248,-526],[150,268],[518,201],[-22,-272],[-121,86],[-121,-347],[-245,-229],[263,-757],[-50,-203],[249,-682],[-2,-388],[-148,-173],[-109,207],[134,484],[-273,-229],[-69,164],[36,228],[-200,346],[21,576],[-186,-179],[24,-689],[11,-846],[-176,-85],[-119,173],[79,544],[-43,570],[-117,4],[-86,405],[115,387],[40,469],[139,891],[58,243],[237,439],[217,-174],[350,-82],[319,25],[275,429],[48,-132]],[[85746,51249],[-15,-517],[-143,58],[-42,-359],[114,-312],[-78,-71],[-112,374],[-82,755],[56,472],[92,215],[20,-322],[164,-52],[26,-241]],[[80461,51765],[47,-395],[190,-334],[179,121],[177,-43],[162,299],[133,52],[263,-166],[226,126],[143,822],[107,205],[96,672],[319,0],[241,-100]],[[82744,53024],[-158,-533],[204,-560],[-48,-272],[312,-546],[-329,-70],[-93,-403],[12,-535],[-267,-404],[-7,-589],[-107,-903],[-41,210],[-316,-266],[-110,361],[-198,34],[-139,189],[-330,-212],[-101,285],[-182,-32],[-229,68],[-43,793],[-138,164],[-134,505],[-38,517],[32,548],[165,392]],[[79393,47122],[-308,-12],[-234,494],[-356,482],[-119,358],[-210,481],[-138,443],[-212,827],[-244,493],[-81,508],[-103,461],[-250,372],[-145,506],[-209,330],[-290,652],[-24,300],[178,-24],[430,-114],[246,-577],[215,-401],[153,-246],[263,-635],[283,-9],[233,-405],[161,-495],[211,-270],[-111,-482],[159,-205],[100,-15],[47,-412],[97,-330],[204,-52],[135,-374],[-70,-735],[-11,-914]],[[72530,68413],[-176,-268],[-108,-553],[269,-224],[262,-289],[362,-332],[381,-76],[160,-301],[215,-56],[334,-138],[231,10],[32,234],[-36,375],[21,255]],[[77035,67277],[20,-224],[-97,-108],[23,-364],[-199,107],[-359,-408],[8,-338],[-153,-496],[-14,-288],[-124,-487],[-217,135],[-11,-612],[-63,-201],[30,-251],[-137,-140]],[[74730,63611],[-39,-216],[-189,7],[-343,-122],[16,-445],[-148,-349],[-400,-398],[-311,-695],[-209,-373],[-276,-387],[-1,-271],[-138,-146],[-251,-212],[-129,-31],[-84,-450],[58,-769],[15,-490],[-118,-561],[-1,-1004],[-144,-29],[-126,-450],[84,-195],[-253,-168],[-93,-401],[-112,-170],[-263,552],[-128,827],[-107,596],[-97,279],[-148,568],[-69,739],[-48,369],[-253,811],[-115,1145],[-83,756],[1,716],[-54,553],[-404,-353],[-196,70],[-362,716],[133,214],[-82,232],[-326,501]],[[68937,64577],[185,395],[612,-2],[-56,507],[-156,300],[-31,455],[-182,265],[306,619],[323,-45],[290,620],[174,599],[270,593],[-4,421],[236,342],[-224,292],[-96,400],[-99,517],[137,255],[421,-144],[310,88],[268,496]],[[48278,82406],[46,-422],[-210,-528],[-493,-349],[-393,89],[225,617],[-145,601],[378,463],[210,276]],[[64978,72558],[244,114],[197,338],[186,-17],[122,110],[197,-55],[308,-299],[221,-65],[318,-523],[207,-21],[24,-498]],[[66909,68203],[137,-310],[112,-357],[266,-260],[7,-520],[133,-96],[23,-272],[-400,-305],[-105,-687]],[[67082,65396],[-523,179],[-303,136],[-313,76],[-118,725],[-133,105],[-214,-106],[-280,-286],[-339,196],[-281,454],[-267,168],[-186,561],[-205,788],[-149,-96],[-177,196],[-104,-231]],[[63490,68261],[-153,311],[-3,314],[-89,0],[46,428],[-143,449],[-340,324],[-193,562],[65,461],[139,204],[-21,345],[-182,177],[-180,705]],[[62436,72541],[-152,473],[55,183],[-87,678],[190,168]],[[63578,73220],[88,-436],[263,-123],[193,-296],[395,-102],[434,156],[27,139]],[[63490,68261],[-164,29]],[[63326,68290],[-187,49],[-204,-567]],[[62935,67772],[-516,47],[-784,1188],[-413,414],[-335,160]],[[60887,69581],[-112,720]],[[60775,70301],[615,614],[105,715],[-26,431],[152,146],[142,369]],[[61763,72576],[119,92],[324,-77],[97,-150],[133,100]],[[45969,89843],[-64,-382],[314,-403],[-361,-451],[-801,-405],[-240,-107],[-365,87],[-775,187],[273,261],[-605,289],[492,114],[-12,174],[-583,137],[188,385],[421,87],[433,-400],[422,321],[349,-167],[453,315],[461,-42]],[[59922,69905],[-49,-186]],[[59873,69719],[-100,82],[-58,-394],[69,-66],[-71,-81],[-12,-156],[131,80]],[[59832,69184],[7,-230],[-139,-944]],[[59518,69025],[80,194],[-19,34],[74,276],[56,446],[40,149],[8,6]],[[59757,70130],[93,-1],[25,104],[75,8]],[[59950,70241],[4,-242],[-38,-90],[6,-4]],[[54311,73167],[-100,-465],[41,-183],[-58,-303],[-213,222],[-141,64],[-387,300],[38,304],[325,-54],[284,64],[211,51]],[[52558,74927],[166,-419],[-39,-782],[-126,38],[-113,-197],[-105,156],[-11,713],[-64,338],[153,-30],[139,183]],[[53835,78058],[-31,-291],[67,-251]],[[53871,77516],[-221,86],[-226,-210],[15,-293],[-34,-168],[91,-301],[261,-298],[140,-488],[309,-476],[217,3],[68,-130],[-78,-118],[249,-214],[204,-178],[238,-308],[29,-111],[-52,-211],[-154,276],[-242,97],[-116,-382],[200,-219],[-33,-309],[-116,-35],[-148,-506],[-116,-46],[1,181],[57,317],[60,126],[-108,342],[-85,298],[-115,74],[-82,255],[-179,107],[-120,238],[-206,38],[-217,267],[-254,384],[-189,340],[-86,585],[-138,68],[-226,195],[-128,-80],[-161,-274],[-115,-43]],[[28453,61504],[187,-53],[147,-142],[46,-161],[-195,-11],[-84,-99],[-156,95],[-159,215],[34,135],[116,41],[64,-20]],[[59922,69905],[309,-234],[544,630]],[[60887,69581],[-53,-89],[-556,-296],[277,-591],[-92,-101],[-46,-197],[-212,-82],[-66,-213],[-120,-182],[-310,94]],[[59709,67924],[-9,86]],[[59832,69184],[41,173],[0,362]],[[87399,70756],[35,-203],[-156,-357],[-114,189],[-143,-137],[-73,-346],[-181,168],[2,281],[154,352],[158,-68],[114,248],[204,-127]],[[89159,72524],[-104,-472],[48,-296],[-145,-416],[-355,-278],[-488,-36],[-396,-675],[-186,227],[-12,442],[-483,-130],[-329,-279],[-325,-11],[282,-435],[-186,-1004],[-179,-248],[-135,229],[69,533],[-176,172],[-113,405],[263,182],[145,371],[280,306],[203,403],[553,177],[297,-121],[291,1050],[185,-282],[408,591],[158,229],[174,723],[-47,664],[117,374],[295,108],[152,-819],[-9,-479],[-256,-595],[4,-610]],[[89974,76679],[195,-126],[197,250],[62,-663],[-412,-162],[-244,-587],[-436,404],[-152,-646],[-308,-9],[-39,587],[138,455],[296,33],[81,817],[83,460],[326,-615],[213,-198]],[[69711,75551],[-159,-109],[-367,-412],[-121,-422],[-104,-4],[-76,280],[-353,19],[-57,484],[-135,4],[21,593],[-333,431],[-476,-46],[-326,-86],[-265,533],[-227,223],[-431,423],[-52,51],[-715,-349],[11,-2178]],[[65546,74986],[-142,-29],[-195,463],[-188,166],[-315,-123],[-123,-197]],[[64583,75266],[-15,144],[68,246],[-53,206],[-322,202],[-125,530],[-154,150],[-9,192],[270,-56],[11,432],[236,96],[243,-88],[50,576],[-50,365],[-278,-28],[-236,144],[-321,-260],[-259,-124]],[[63639,77993],[-142,96],[29,304],[-177,395],[-207,-17],[-235,401],[160,448],[-81,120],[222,649],[285,-342],[35,431],[573,643],[434,15],[612,-409],[329,-239],[295,249],[440,12],[356,-306],[80,175],[391,-25],[69,280],[-450,406],[267,288],[-52,161],[266,153],[-200,405],[127,202],[1039,205],[136,146],[695,218],[250,245],[499,-127],[88,-612],[290,144],[356,-202],[-23,-322],[267,33],[696,558],[-102,-185],[355,-457],[620,-1500],[148,309],[383,-340],[399,151],[154,-106],[133,-341],[194,-115],[119,-251],[358,79],[147,-361]],[[72294,75601],[-171,87],[-140,212],[-412,62],[-461,16],[-100,-65],[-396,248],[-158,-122],[-43,-349],[-457,204],[-183,-84],[-62,-259]],[[61551,49585],[-195,-236],[-68,-246],[-104,-44],[-40,-416],[-89,-238],[-54,-393],[-112,-195]],[[60889,47817],[-399,590],[-19,343],[-1007,1203],[-47,65]],[[59417,50018],[-3,627],[80,239],[137,391],[101,431],[-123,678],[-32,296],[-132,411]],[[59445,53091],[171,352],[188,390]],[[61626,52895],[-243,-670],[3,-2152],[165,-488]],[[70465,73876],[-526,-89],[-343,192],[-301,-46],[26,340],[303,-98],[101,182]],[[69725,74357],[212,-58],[355,425],[-329,311],[-198,-147],[-205,223],[234,382],[-83,58]],[[78495,57780],[-66,713],[178,492],[359,112],[261,-84]],[[79227,59013],[229,-232],[126,407],[246,-217]],[[79828,58971],[64,-394],[-34,-708],[-467,-455],[122,-358],[-292,-43],[-240,-238]],[[78981,56775],[-233,87],[-112,307],[-141,611]],[[85652,73393],[240,-697],[68,-383],[3,-681],[-105,-325],[-252,-113],[-222,-245],[-250,-51],[-31,322],[51,443],[-122,615],[206,99],[-190,506]],[[85048,72883],[17,54],[124,-21],[108,266],[197,29],[118,39],[40,143]],[[55575,75742],[52,132]],[[55627,75874],[66,43],[38,196],[50,33],[40,-84],[52,-36],[36,-94],[46,-28],[54,-110],[39,4],[-31,-144],[-33,-71],[9,-44]],[[55993,75539],[-62,-23],[-164,-91],[-13,-121],[-35,5]],[[63326,68290],[58,-261],[-25,-135],[89,-445]],[[63448,67449],[-196,-16],[-69,282],[-248,57]],[[79227,59013],[90,266],[12,500],[-224,515],[-18,583],[-211,480],[-210,40],[-56,-205],[-163,-17],[-83,104],[-293,-353],[-6,530],[68,623],[-188,27],[-16,355],[-120,182]],[[77809,62643],[59,218],[237,384]],[[78380,63852],[162,-466],[125,-537],[342,-5],[108,-515],[-178,-155],[-80,-212],[333,-353],[231,-699],[175,-520],[210,-411],[70,-418],[-50,-590]],[[59757,70130],[99,482],[138,416],[5,21]],[[59999,71049],[125,-31],[45,-231],[-151,-223],[-68,-323]],[[47857,53158],[-73,-5],[-286,282],[-252,449],[-237,324],[-187,381]],[[46822,54589],[66,189],[15,172],[126,320],[129,276]],[[54125,64088],[-197,-220],[-156,324],[-439,255]],[[52633,68486],[136,137],[24,250],[-30,244],[191,228],[86,189],[135,170],[16,454]],[[53191,70158],[326,-204],[117,51],[232,-98],[368,-264],[130,-526],[250,-114],[391,-248],[296,-293],[136,153],[133,272],[-65,452],[87,288],[200,277],[192,80],[375,-121],[95,-264],[104,-2],[88,-101],[276,-70],[68,-195]],[[56944,63578],[0,-1180],[-320,-2],[-3,-248]],[[56621,62148],[-1108,1131],[-1108,1132],[-280,-323]],[[72718,55024],[-42,-615],[-116,-168],[-242,-135],[-132,470],[-49,849],[126,959],[192,-328],[129,-416],[134,-616]],[[58049,33472],[96,-178],[-85,-288],[-47,-192],[-155,-93],[-51,-188],[-99,-59],[-209,454],[148,374],[151,232],[130,120],[121,-182]],[[56314,82678],[-23,150],[30,162],[-123,94],[-291,103]],[[55907,83187],[-59,497]],[[55848,83684],[318,181],[466,-38],[273,59],[39,-123],[148,-38],[267,-287]],[[56523,82432],[-67,182],[-142,64]],[[55848,83684],[10,445],[136,371],[262,202],[221,-442],[223,12],[53,453]],[[57579,84537],[134,-136],[24,-287],[89,-348]],[[47592,66920],[-42,0],[7,-317],[-172,-19],[-90,-134],[-126,0],[-100,76],[-234,-63],[-91,-460],[-86,-44],[-131,-745],[-386,-637],[-92,-816],[-114,-265],[-33,-213],[-625,-48],[-5,1]],[[45272,63236],[13,274],[106,161],[91,308],[-18,200],[96,417],[155,376],[93,95],[74,344],[6,315],[100,365],[185,216],[177,603],[5,8],[139,227],[259,65],[218,404],[140,158],[232,493],[-70,735],[106,508],[37,312],[179,399],[278,270],[206,244],[186,612],[87,362],[205,-2],[167,-251],[264,41],[288,-131],[121,-6]],[[57394,79070],[66,87],[185,58],[204,-184],[115,-22],[125,-159],[-20,-200],[101,-97],[40,-247],[97,-150],[-19,-88],[52,-60],[-74,-44],[-164,18],[-27,81],[-58,-47],[20,-106],[-76,-188],[-49,-203],[-70,-64]],[[57842,77455],[-50,270],[30,252],[-9,259],[-160,352],[-89,249],[-86,175],[-84,58]],[[63761,43212],[74,-251],[69,-390],[45,-711],[72,-276],[-28,-284],[-49,-174],[-94,347],[-53,-175],[53,-438],[-24,-250],[-77,-137],[-18,-500],[-109,-689],[-137,-814],[-172,-1120],[-106,-821],[-125,-685],[-226,-140],[-243,-250],[-160,151],[-220,211],[-77,312],[-18,524],[-98,471],[-26,425],[50,426],[128,102],[1,197],[133,447],[25,377],[-65,280],[-52,372],[-23,544],[97,331],[38,375],[138,22],[155,121],[103,107],[122,7],[158,337],[229,364],[83,297],[-38,253],[118,-71],[153,410],[6,356],[92,264],[96,-254]],[[23016,65864],[-107,-518],[-49,-426],[-20,-791],[-27,-289],[48,-322],[86,-288],[56,-458],[184,-440],[65,-337],[109,-291],[295,-157],[114,-247],[244,165],[212,60],[208,106],[175,101],[176,241],[67,345],[22,496],[48,173],[188,155],[294,137],[246,-21],[169,50],[66,-125],[-9,-285],[-149,-351],[-66,-360],[51,-103],[-42,-255],[-69,-461],[-71,152],[-58,-10]],[[24381,59170],[-314,636],[-144,191],[-226,155],[-156,-43],[-223,-223],[-140,-58],[-196,156],[-208,112],[-260,271],[-208,83],[-314,275],[-233,282],[-70,158],[-155,35],[-284,187],[-116,270],[-299,335],[-139,373],[-66,288],[93,57],[-29,169],[64,153],[1,204],[-93,266],[-25,235],[-94,298],[-244,587],[-280,462],[-135,368],[-238,241],[-51,145],[42,365],[-142,138],[-164,287],[-69,412],[-149,48],[-162,311],[-130,288],[-12,184],[-149,446],[-99,452],[5,227],[-201,234],[-93,-25],[-159,163],[-44,-240],[46,-284],[27,-444],[95,-243],[206,-407],[46,-139],[42,-42],[37,-203],[49,8],[56,-381],[85,-150],[59,-210],[174,-300],[92,-550],[83,-259],[77,-277],[15,-311],[134,-20],[112,-268],[100,-264],[-6,-106],[-117,-217],[-49,3],[-74,359],[-181,337],[-201,286],[-142,150],[9,432],[-42,320],[-132,183],[-191,264],[-37,-76],[-70,154],[-171,143],[-164,343],[20,44],[115,-33],[103,221],[10,266],[-214,422],[-163,163],[-102,369],[-103,388],[-129,472],[-113,531]],[[17464,69802],[316,46],[353,64],[-26,-116],[419,-287],[634,-416],[552,4],[221,0],[0,244],[481,0],[102,-210],[142,-186],[165,-260],[92,-309],[69,-325],[144,-178],[230,-177],[175,467],[227,11],[196,-236],[139,-404],[96,-346],[164,-337],[61,-414],[78,-277],[217,-184],[197,-130],[108,18]],[[55993,75539],[95,35],[128,9]],[[46619,59216],[93,107],[47,348],[88,14],[194,-165],[157,117],[107,-39],[42,131],[1114,9],[62,414],[-48,73],[-134,2550],[-134,2550],[425,10]],[[51185,61897],[1,-1361],[-152,-394],[-24,-364],[-247,-94],[-379,-51],[-102,-210],[-178,-23]],[[46801,57931],[13,184],[-24,229],[-104,166],[-54,338],[-13,368]],[[77375,56448],[-27,439],[86,452],[-94,350],[23,644],[-113,306],[-90,707],[-50,746],[-121,490],[-183,-297],[-315,-421],[-156,53],[-172,138],[96,732],[-58,554],[-218,681],[34,213],[-163,76],[-197,481]],[[77809,62643],[-159,-137],[-162,-256],[-196,-26],[-127,-639],[-117,-107],[134,-519],[177,-431],[113,-390],[-101,-514],[-96,-109],[66,-296],[185,-470],[32,-330],[-4,-274],[108,-539],[-152,-551],[-135,-607]],[[55380,75322],[-58,46],[-78,192],[-120,118]],[[55338,76294],[74,-101],[40,-82],[91,-63],[106,-123],[-22,-51]],[[74375,79706],[292,102],[530,509],[423,278],[242,-182],[289,-8],[186,-276],[277,-22],[402,-148],[270,411],[-113,348],[288,612],[311,-244],[252,-69],[327,-152],[53,-443],[394,-248],[263,109],[351,78],[279,-78],[272,-284],[168,-302],[258,6],[350,-96],[255,146],[366,98],[407,416],[166,-63],[146,-198],[331,49]],[[59599,43773],[209,48],[334,-166],[73,74],[193,16],[99,177],[167,-10],[303,230],[221,342]],[[61198,44484],[45,-265],[-11,-588],[34,-519],[11,-923],[49,-290],[-83,-422],[-108,-410],[-177,-366],[-254,-225],[-313,-287],[-313,-634],[-107,-108],[-194,-420],[-115,-136],[-23,-421],[132,-448],[54,-346],[4,-177],[49,29],[-8,-579],[-45,-275],[65,-101],[-41,-245],[-116,-211],[-229,-199],[-334,-320],[-122,-219],[24,-248],[71,-40],[-24,-311]],[[59119,34780],[-211,5]],[[58908,34785],[-24,261],[-41,265]],[[58843,35311],[-23,212],[49,659],[-72,419],[-133,832]],[[58664,37433],[292,671],[74,426],[42,53],[31,348],[-45,175],[12,442],[54,409],[0,748],[-145,190],[-132,43],[-60,146],[-128,125],[-232,-12],[-18,220]],[[58409,41417],[-26,421],[843,487]],[[59226,42325],[159,-284],[77,54],[110,-149],[16,-237],[-59,-274],[21,-417],[181,-365],[85,410],[120,124],[-24,760],[-116,427],[-100,191],[-97,-9],[-77,768],[77,449]],[[46619,59216],[-184,405],[-168,435],[-184,157],[-133,173],[-155,-6],[-135,-129],[-138,51],[-96,-189]],[[45426,60113],[-24,318],[78,291],[34,557],[-30,583],[-34,294],[28,295],[-72,281],[-146,255]],[[45260,62987],[60,197],[1088,-4],[-53,853],[68,304],[261,53],[-9,1512],[911,-31],[1,895]],[[59226,42325],[-147,153],[85,549],[87,205],[-53,490],[56,479],[47,160],[-71,501],[-131,264]],[[59099,45126],[273,-110],[55,-164],[95,-275],[77,-804]],[[78372,54256],[64,-56],[164,-356],[116,-396],[16,-398],[-29,-269],[27,-203],[20,-349],[98,-163],[109,-523],[-5,-199],[-197,-40],[-263,438],[-329,469],[-32,301],[-161,395],[-38,489],[-100,322],[30,431],[-61,250]],[[77801,54399],[48,105],[227,-258],[22,-304],[183,71],[91,243]],[[80461,51765],[204,-202],[214,110],[56,500],[119,112],[333,128],[199,467],[137,374]],[[82069,53798],[214,411],[140,462],[112,2],[143,-299],[13,-257],[183,-165],[231,-177],[-20,-232],[-186,-29],[50,-289],[-205,-201]],[[54540,33696],[-207,446],[-108,432],[-62,575],[-68,428],[-93,910],[-7,707],[-35,322],[-108,243],[-144,489],[-146,708],[-60,371],[-226,577],[-17,453]],[[56448,40227],[228,134],[180,-34],[109,-133],[2,-49]],[[55526,35946],[0,-2182],[-248,-302],[-149,-43],[-175,112],[-125,43],[-47,252],[-109,162],[-133,-292]],[[96049,38125],[228,-366],[144,-272],[-105,-142],[-153,160],[-199,266],[-179,313],[-184,416],[-38,201],[119,-9],[156,-201],[122,-200],[89,-166]],[[54125,64088],[68,-919],[104,-153],[4,-188],[116,-203],[-60,-254],[-107,-1199],[-15,-769],[-354,-557],[-120,-778],[115,-219],[0,-380],[178,-13],[-28,-279]],[[53939,57955],[-52,-13],[-188,647],[-65,24],[-217,-331],[-215,173],[-150,34],[-80,-83],[-163,18],[-164,-252],[-141,-14],[-337,305],[-131,-145],[-142,10],[-104,223],[-279,221],[-298,-70],[-72,-128],[-39,-340],[-80,-238],[-19,-527]],[[52361,53399],[-289,-213],[-105,31],[-107,-132],[-222,13],[-149,370],[-91,427],[-197,389],[-209,-7],[-245,1]],[[26191,57131],[-96,186],[-130,238],[-61,200],[-117,185],[-140,267],[31,91],[46,-88],[21,41]],[[26903,59440],[-24,-57],[-14,-132],[29,-216],[-64,-202],[-30,-237],[-9,-261],[15,-152],[7,-266],[-43,-58],[-26,-253],[19,-156],[-56,-151],[12,-159],[43,-97]],[[50920,80916],[143,162],[244,869],[380,248],[231,-17]],[[58639,91676],[-473,-237],[-224,-54]],[[55734,91409],[-172,-24],[-41,-389],[-523,95],[-74,-329],[-267,2],[-183,-421],[-278,-655],[-431,-831],[101,-202],[-97,-234],[-275,10],[-180,-554],[17,-784],[177,-300],[-92,-694],[-231,-405],[-122,-341]],[[53063,85353],[-187,363],[-548,-684],[-371,-138],[-384,301],[-99,635],[-88,1363],[256,381],[733,496],[549,609],[508,824],[668,1141],[465,444],[763,741],[610,259],[457,-31],[423,489],[506,-26],[499,118],[869,-433],[-358,-158],[305,-371]],[[56867,96577],[-620,-241],[-490,137],[191,152],[-167,189],[575,119],[110,-222],[401,-134]],[[55069,97669],[915,-440],[-699,-233],[-155,-435],[-243,-111],[-132,-490],[-335,-23],[-598,361],[252,210],[-416,170],[-541,499],[-216,463],[757,212],[152,-207],[396,8],[105,202],[408,20],[350,-206]],[[57068,98086],[545,-207],[-412,-318],[-806,-70],[-819,98],[-50,163],[-398,11],[-304,271],[858,165],[403,-142],[281,177],[702,-148]],[[98060,26404],[63,-244],[198,239],[80,-249],[0,-249],[-103,-274],[-182,-435],[-142,-238],[103,-284],[-214,-7],[-238,-223],[-75,-387],[-157,-597],[-219,-264],[-138,-169],[-256,13],[-180,194],[-302,42],[-46,217],[149,438],[349,583],[179,111],[200,225],[238,310],[167,306],[123,441],[106,149],[41,330],[195,273],[61,-251]],[[98502,29218],[202,-622],[5,403],[126,-161],[41,-447],[224,-192],[188,-48],[158,226],[141,-69],[-67,-524],[-85,-345],[-212,12],[-74,-179],[26,-254],[-41,-110],[-105,-319],[-138,-404],[-214,-236],[-48,155],[-116,85],[160,486],[-91,326],[-299,236],[8,214],[201,206],[47,455],[-13,382],[-113,396],[8,104],[-133,244],[-218,523],[-117,418],[104,46],[151,-328],[216,-153],[78,-526]],[[64752,60417],[-91,413],[-217,975]],[[64444,61805],[833,591],[185,1182],[-127,418]],[[65665,65306],[125,-404],[155,-214],[203,-78],[165,-107],[125,-339],[75,-196],[100,-75],[-1,-132],[-101,-352],[-44,-166],[-117,-189],[-104,-404],[-126,31],[-58,-141],[-44,-300],[34,-395],[-26,-72],[-128,2],[-174,-221],[-27,-288],[-63,-125],[-173,5],[-109,-149],[1,-238],[-134,-165],[-153,56],[-186,-199],[-128,-34]],[[65575,65974],[80,201],[35,-51],[-26,-244],[-37,-108]],[[68937,64577],[-203,150],[-83,424],[-215,450],[-512,-111],[-451,-11],[-391,-83]],[[28366,54848],[-93,170],[-59,319],[68,158],[-70,40],[-52,196],[-138,164],[-122,-38],[-56,-205],[-112,-149],[-61,-20],[-27,-123],[132,-321],[-75,-76],[-40,-87],[-130,-30],[-48,353],[-36,-101],[-92,35],[-56,238],[-114,39],[-72,69],[-119,-1],[-8,-128],[-32,89]],[[27070,56232],[100,-212],[-6,-126],[111,-26],[26,48],[77,-145],[136,42],[119,150],[168,119],[95,176],[153,-34],[-10,-58],[155,-21],[124,-102],[90,-177],[105,-164]],[[30452,39739],[-279,340],[-24,242],[-551,593],[-498,646],[-214,365],[-115,488],[46,170],[-236,775],[-274,1090],[-262,1177],[-114,269],[-87,435],[-216,386],[-198,239],[90,264],[-134,563],[86,414],[221,373]],[[85104,55551],[28,-392],[16,-332],[-94,-540],[-102,602],[-130,-300],[89,-435],[-79,-277],[-327,343],[-78,428],[84,280],[-176,280],[-87,-245],[-131,23],[-205,-330],[-46,173],[109,498],[175,166],[151,223],[98,-268],[212,162],[45,264],[196,15],[-16,457],[225,-280],[23,-297],[20,-218]],[[84439,56653],[-100,-195],[-87,-373],[-87,-175],[-171,409],[57,158],[70,165],[30,367],[153,35],[-44,-398],[205,570],[-26,-563]],[[82917,56084],[-369,-561],[136,414],[200,364],[167,409],[146,587],[49,-482],[-183,-325],[-146,-406]],[[83856,57606],[166,-183],[177,1],[-5,-247],[-129,-251],[-176,-178],[-10,275],[20,301],[-43,282]],[[84861,57766],[78,-660],[-214,157],[5,-199],[68,-364],[-132,-133],[-11,416],[-84,31],[-43,357],[163,-47],[-4,224],[-169,451],[266,-13],[77,-220]],[[83757,58301],[-74,-510],[-119,295],[-142,450],[238,-22],[97,-213]],[[83700,61512],[171,-168],[85,153],[26,-150],[-46,-245],[95,-423],[-73,-491],[-164,-196],[-43,-476],[62,-471],[147,-65],[123,70],[347,-328],[-27,-321],[91,-142],[-29,-272],[-216,290],[-103,310],[-71,-217],[-177,354],[-253,-87],[-138,130],[14,244],[87,151],[-83,136],[-36,-213],[-137,340],[-41,257],[-11,566],[112,-195],[29,925],[90,535],[169,-1]],[[93299,46550],[-78,-59],[-120,227],[-122,375],[-59,450],[38,57],[30,-175],[84,-134],[135,-375],[131,-200],[-39,-166]],[[92217,47343],[-146,-48],[-44,-166],[-152,-144],[-142,-138],[-148,1],[-228,171],[-158,165],[23,183],[249,-86],[152,46],[42,283],[40,15],[27,-314],[158,45],[78,202],[155,211],[-30,348],[166,11],[56,-97],[-5,-327],[-93,-361]],[[89166,49043],[482,-407],[513,-338],[192,-302],[154,-297],[43,-349],[462,-365],[68,-313],[-256,-64],[62,-393],[248,-388],[180,-627],[159,20],[-11,-262],[215,-100],[-84,-111],[295,-249],[-30,-171],[-184,-41],[-69,153],[-238,66],[-281,89],[-216,377],[-158,325],[-144,517],[-362,259],[-235,-169],[-170,-195],[35,-436],[-218,-203],[-155,99],[-288,25]],[[92538,47921],[-87,-157],[-52,348],[-65,229],[-126,193],[-158,252],[-200,174],[77,143],[150,-166],[94,-130],[117,-142],[111,-248],[106,-189],[33,-307]],[[53922,82340],[189,174],[434,273],[350,200],[277,-100],[21,-144],[268,-7]],[[55461,82736],[342,-67],[511,9]],[[56535,81053],[139,-515],[-29,-166],[-138,-69],[-252,-491],[71,-266],[-60,35]],[[56266,79581],[-264,227],[-200,-84],[-131,61],[-165,-127],[-140,210],[-114,-81],[-16,36]],[[31588,61519],[142,-52],[50,-118],[-71,-149],[-209,4],[-163,-21],[-16,253],[40,86],[227,-3]],[[86288,75628],[39,-104]],[[86327,75524],[-106,36],[-120,-200],[-83,-202],[10,-424],[-143,-130],[-50,-105],[-104,-174],[-185,-97],[-121,-159],[-9,-256],[-32,-65],[111,-96],[157,-259]],[[85048,72883],[-135,112],[-34,-111],[-81,-49],[-10,112],[-72,54],[-75,94],[76,260],[66,69],[-25,108],[71,319],[-18,96],[-163,65],[-131,158]],[[47929,72498],[-112,-153],[-146,83],[-143,-65],[42,462],[-26,363],[-124,55],[-67,224],[22,386],[111,215],[20,239],[58,355],[-6,250],[-56,212],[-12,200]],[[64113,65205],[-18,430],[75,310],[76,64],[84,-185],[5,-346],[-61,-348]],[[64274,65130],[-77,-42],[-84,117]],[[56308,78869],[120,127],[172,-65],[178,-3],[129,-144],[95,91],[205,56],[69,139],[118,0]],[[57842,77455],[124,-109],[131,95],[126,-101]],[[58223,77340],[6,-152],[-135,-128],[-84,56],[-78,-713]],[[56293,76715],[-51,103],[65,99],[-69,74],[-87,-133],[-162,172],[-22,244],[-169,139],[-31,188],[-151,232]],[[89901,80562],[280,-1046],[-411,195],[-171,-854],[271,-605],[-8,-413],[-211,356],[-182,-457],[-51,496],[31,575],[-32,638],[64,446],[13,790],[-163,581],[24,808],[257,271],[-110,274],[123,83],[73,-391],[96,-569],[-7,-581],[114,-597]],[[55461,82736],[63,260],[383,191]],[[99999,92429],[-305,-30],[-49,187],[-99645,247],[36,24],[235,-1],[402,-169],[-24,-81],[-286,-141],[-363,-36],[99999,0]],[[89889,93835],[-421,-4],[-569,66],[-49,31],[263,234],[348,54],[394,-226],[34,-155]],[[91869,94941],[-321,-234],[-444,53],[-516,233],[66,192],[518,-89],[697,-155]],[[90301,95224],[-219,-439],[-1023,16],[-461,-139],[-550,384],[149,406],[366,111],[734,-26],[1004,-313]],[[65981,92363],[-164,-52],[-907,77],[-74,262],[-503,158],[-40,320],[284,126],[-10,323],[551,503],[-255,73],[665,518],[-75,268],[621,312],[917,380],[925,110],[475,220],[541,76],[193,-233],[-187,-184],[-984,-293],[-848,-282],[-863,-562],[-414,-577],[-435,-568],[56,-491],[531,-484]],[[63639,77993],[-127,-350],[-269,-97],[-276,-610],[252,-561],[-27,-398],[303,-696]],[[61098,76242],[-354,499],[-317,223],[-240,347],[202,95],[231,494],[-156,234],[410,241],[-8,129],[-249,-95]],[[60617,78409],[9,262],[143,165],[269,43],[44,197],[-62,326],[113,310],[-3,173],[-410,192],[-162,-6],[-172,277],[-213,-94],[-352,208],[6,116],[-99,256],[-222,29],[-23,183],[70,120],[-178,334],[-288,-57],[-84,30],[-70,-134],[-104,23]],[[57772,85719],[316,327],[-291,280]],[[58639,91676],[286,206],[456,-358],[761,-140],[1050,-668],[213,-281],[18,-393],[-308,-311],[-454,-157],[-1240,449],[-204,-75],[453,-433],[18,-274],[18,-604],[358,-180],[217,-153],[36,286],[-168,254],[177,224],[672,-368],[233,144],[-186,433],[647,578],[256,-34],[260,-206],[161,406],[-231,352],[136,353],[-204,367],[777,-190],[158,-331],[-351,-73],[1,-328],[219,-203],[429,128],[68,377],[580,282],[970,507],[209,-29],[-273,-359],[344,-61],[199,202],[521,16],[412,245],[317,-356],[315,391],[-291,343],[145,195],[820,-179],[385,-185],[1006,-675],[186,309],[-282,313],[-8,125],[-335,58],[92,280],[-149,461],[-8,189],[512,535],[183,537],[206,116],[736,-156],[57,-328],[-263,-479],[173,-189],[89,-413],[-63,-809],[307,-362],[-120,-395],[-544,-839],[318,-87],[110,213],[306,151],[74,293],[240,281],[-162,336],[130,390],[-304,49],[-67,328],[222,593],[-361,482],[497,398],[-64,421],[139,13],[145,-328],[-109,-570],[297,-108],[-127,426],[465,233],[577,31],[513,-337],[-247,492],[-28,630],[483,119],[669,-26],[602,77],[-226,309],[321,388],[319,16],[540,293],[734,79],[93,162],[729,55],[227,-133],[624,314],[510,-10],[77,255],[265,252],[656,242],[476,-191],[-378,-146],[629,-90],[75,-292],[254,143],[812,-7],[626,-289],[223,-221],[-69,-307],[-307,-175],[-730,-328],[-209,-175],[345,-83],[410,-149],[251,112],[141,-379],[122,153],[444,93],[892,-97],[67,-276],[1162,-88],[15,451],[590,-104],[443,4],[449,-312],[128,-378],[-165,-247],[349,-465],[437,-240],[268,620],[446,-266],[473,159],[538,-182],[204,166],[455,-83],[-201,549],[367,256],[2509,-384],[236,-351],[727,-451],[1122,112],[553,-98],[231,-244],[-33,-432],[342,-168],[372,121],[492,15],[525,-116],[526,66],[484,-526],[344,189],[-224,378],[123,262],[886,-165],[578,36],[799,-282],[-99610,-258],[681,-451],[728,-588],[-24,-367],[187,-147],[-64,429],[754,-88],[544,-553],[-276,-257],[-455,-61],[-7,-578],[-111,-122],[-260,17],[-212,206],[-369,172],[-62,257],[-283,96],[-315,-76],[-151,207],[60,219],[-333,-140],[126,-278],[-158,-251],[99997,-3],[-357,-260],[-360,44],[250,-315],[166,-487],[128,-159],[32,-244],[-71,-157],[-518,129],[-777,-445],[-247,-69],[-425,-415],[-403,-362],[-102,-269],[-397,409],[-724,-464],[-126,219],[-268,-253],[-371,81],[-90,-388],[-333,-572],[10,-239],[316,-132],[-37,-860],[-258,-22],[-119,-494],[116,-255],[-486,-302],[-96,-674],[-415,-144],[-83,-600],[-400,-551],[-103,407],[-119,862],[-155,1313],[134,819],[234,353],[14,276],[432,132],[496,744],[479,608],[499,471],[223,833],[-337,-50],[-167,-487],[-705,-649],[-227,727],[-717,-201],[-696,-990],[230,-362],[-620,-154],[-430,-61],[20,427],[-431,90],[-344,-291],[-850,102],[-914,-175],[-899,-1153],[-1065,-1394],[438,-74],[136,-370],[270,-132],[178,295],[305,-38],[401,-650],[9,-503],[-217,-590],[-23,-705],[-126,-945],[-418,-855],[-94,-409],[-377,-688],[-374,-682],[-179,-349],[-370,-346],[-175,-8],[-175,287],[-373,-432],[-43,-197]],[[79187,96845],[-1566,-228],[507,776],[229,66],[208,-38],[704,-336],[-82,-240]],[[64204,98169],[-373,-78],[-250,-45],[-39,-97],[-324,-98],[-301,140],[158,185],[-618,18],[542,107],[422,8],[57,-160],[159,142],[262,97],[412,-129],[-107,-90]],[[77760,97184],[-606,-73],[-773,170],[-462,226],[-213,423],[-379,117],[722,404],[600,133],[540,-297],[640,-572],[-69,-531]],[[58449,49909],[110,-333],[-16,-348],[-80,-74]],[[58216,49787],[67,-60],[166,182]],[[45260,62987],[12,249]],[[61883,60238],[-37,252],[-83,178],[-22,236],[-143,212],[-148,495],[-79,482],[-192,406],[-124,97],[-184,563],[-32,411],[12,350],[-159,655],[-130,231],[-150,122],[-92,339],[15,133],[-77,306],[-81,132],[-108,440],[-170,476],[-141,406],[-139,-3],[44,325],[12,206],[34,236]],[[63448,67449],[109,-510],[137,-135],[47,-207],[190,-249],[16,-243],[-27,-197],[35,-199],[80,-165],[37,-194],[41,-145]],[[64274,65130],[53,-226]],[[64444,61805],[-801,-226],[-259,-266],[-199,-620],[-130,-99],[-70,197],[-106,-30],[-269,60],[-50,59],[-321,-14],[-75,-53],[-114,153],[-74,-290],[28,-249],[-121,-189]],[[59434,56171],[-39,12],[5,294],[-33,203],[-143,233],[-34,426],[34,436],[-129,41],[-19,-132],[-167,-30],[67,-173],[23,-355],[-152,-324],[-138,-426],[-144,-61],[-233,345],[-105,-122],[-29,-172],[-143,-112],[-9,-122],[-277,0],[-38,122],[-200,20],[-100,-101],[-77,51],[-143,344],[-48,163],[-200,-81],[-76,-274],[-72,-528],[-95,-111],[-85,-65]],[[56635,55672],[-23,28]],[[56351,57163],[3,143],[-102,174],[-3,343],[-58,228],[-98,-34],[28,217],[72,246],[-32,245],[92,181],[-58,138],[73,365],[127,435],[240,-41],[-14,2345]],[[60240,63578],[90,-580],[-61,-107],[40,-608],[102,-706],[106,-145],[152,-219]],[[59433,56242],[1,-71]],[[59434,56171],[3,-460]],[[59445,53091],[-171,-272],[-195,1],[-224,-138],[-176,132],[-115,-161]],[[56824,55442],[-189,230]],[[45357,58612],[-115,460],[-138,210],[122,112],[134,415],[66,304]],[[45367,57897],[-46,453]],[[95032,44386],[78,-203],[-194,4],[-106,363],[166,-142],[56,-22]],[[94680,44747],[-108,-14],[-170,60],[-58,91],[17,235],[183,-93],[91,-124],[45,-155]],[[94910,44908],[-42,-109],[-206,512],[-57,353],[94,0],[100,-473],[111,-283]],[[94409,45654],[12,-119],[-218,251],[-152,212],[-104,197],[41,60],[128,-142],[228,-272],[65,-187]],[[93760,46238],[-56,-33],[-121,134],[-114,243],[14,99],[166,-250],[111,-193]],[[46822,54589],[-75,44],[-200,238],[-144,316],[-49,216],[-34,437]],[[25613,58487],[-31,-139],[-161,9],[-100,57],[-115,117],[-154,37],[-79,127]],[[61984,57352],[91,-109],[54,-245],[125,-247],[138,-2],[262,151],[302,70],[245,184],[138,39],[99,108],[158,20]],[[63596,57321],[-2,-9],[-1,-244],[0,-596],[0,-308],[-125,-363],[-194,-493]],[[63596,57321],[89,12],[128,88],[147,59],[132,202],[105,2],[6,-163],[-25,-344],[1,-310],[-59,-214],[-78,-639],[-134,-659],[-172,-755],[-238,-866],[-237,-661],[-327,-806],[-278,-479],[-415,-586],[-259,-450],[-304,-715],[-64,-312],[-63,-140]],[[34125,54109],[333,-119],[30,107],[225,43],[298,-159]],[[34889,53069],[109,-351],[-49,-254],[-24,-270],[-71,-248]],[[56266,79581],[-77,-154],[-55,-238]],[[53809,77462],[62,54]],[[56639,89578],[-478,-167],[-269,-413],[43,-361],[-441,-475],[-537,-509],[-202,-832],[198,-416],[265,-328],[-255,-666],[-289,-138],[-106,-992],[-157,-554],[-337,57],[-158,-468],[-321,-27],[-89,558],[-232,671],[-211,835]],[[58908,34785],[-56,-263],[-163,-63],[-166,320],[-2,204],[76,222],[26,172],[80,42],[140,-108]],[[59999,71049],[-26,452],[68,243]],[[60041,71744],[74,129],[75,130],[15,329],[91,-115],[306,165],[147,-112],[229,2],[320,222],[149,-10],[316,92]],[[50518,54209],[-224,-126]],[[78495,57780],[-249,271],[-238,-11],[41,464],[-245,-3],[-22,-650],[-150,-863],[-90,-522],[19,-428],[181,-18],[113,-539],[50,-512],[155,-338],[168,-69],[144,-306]],[[77801,54399],[-110,227],[-47,292],[-148,334],[-135,280],[-45,-347],[-53,328],[30,369],[82,566]],[[68841,72526],[156,598],[-60,440],[-204,140],[72,261],[232,-28],[132,326],[89,380],[371,137],[-58,-274],[40,-164],[114,15]],[[64978,72558],[-52,417],[40,618],[-216,200],[71,405],[-184,34],[61,498],[262,-145],[244,189],[-202,355],[-80,338],[-224,-151],[-28,-433],[-87,383]],[[65546,74986],[313,8],[-45,297],[237,204],[234,343],[374,-312],[30,-471],[106,-121],[301,27],[93,-108],[137,-609],[317,-408],[181,-278],[291,-289],[369,-253],[-7,-362]],[[84713,45326],[32,139],[239,133],[194,20],[87,74],[105,-74],[-102,-160],[-289,-258],[-233,-170]],[[32866,56937],[160,77],[58,-21],[-11,-440],[-232,-65],[-50,53],[81,163],[-6,233]],[[52339,72408],[302,239],[195,-71],[-9,-299],[236,217],[20,-113],[-139,-290],[-2,-273],[96,-147],[-36,-511],[-183,-297],[53,-322],[143,-10],[70,-281],[106,-92]],[[60041,71744],[-102,268],[105,222],[-169,-51],[-233,136],[-191,-340],[-421,-66],[-225,317],[-300,20],[-64,-245],[-192,-70],[-268,314],[-303,-11],[-165,588],[-203,328],[135,459],[-176,283],[308,565],[428,23],[117,449],[529,-78],[334,383],[324,167],[459,13],[485,-417],[399,-228],[323,91],[239,-53],[328,309]],[[57776,75399],[33,-228],[243,-190],[-51,-145],[-330,-33],[-118,-182],[-232,-319],[-87,276],[3,121]],[[83826,64992],[-167,-947],[-119,-485],[-146,499],[-32,438],[163,581],[223,447],[127,-176],[-49,-357]],[[60889,47817],[-128,-728],[16,-335],[178,-216],[8,-153],[-76,-357],[16,-180],[-18,-282],[97,-370],[115,-583],[101,-129]],[[59099,45126],[-157,177],[-177,100],[-111,99],[-116,150]],[[58388,46397],[-161,331],[-55,342]],[[58449,49909],[98,71],[304,-7],[566,45]],[[60617,78409],[-222,-48],[-185,-191],[-260,-31],[-239,-220],[16,-368],[136,-142],[284,35],[-55,-210],[-304,-103],[-377,-342],[-154,121],[61,277],[-304,173],[50,113],[265,197],[-80,135],[-432,149],[-19,221],[-257,-73],[-103,-325],[-215,-437]],[[35174,30629],[-121,-372],[-313,-328],[-205,118],[-151,-63],[-256,253],[-189,-19],[-169,327]],[[6794,61855],[-41,-99],[-69,84],[8,165],[-46,216],[14,65],[48,97],[-19,116],[16,55],[21,-11],[107,-100],[49,-51],[45,-79],[71,-207],[-7,-33],[-108,-126],[-89,-92]],[[6645,62777],[-94,-43],[-47,125],[-32,48],[-3,37],[27,50],[99,-56],[73,-90],[-23,-71]],[[6456,63091],[-9,-63],[-149,17],[21,72],[137,-26]],[[6207,63177],[-15,-34],[-19,8],[-97,21],[-35,133],[-11,24],[74,82],[23,-38],[80,-196]],[[5737,63567],[-33,-58],[-93,107],[14,43],[43,58],[64,-12],[5,-138]],[[31350,77248],[48,-194],[-296,-286],[-286,-204],[-293,-175],[-147,-351],[-47,-133],[-3,-313],[92,-313],[115,-15],[-29,216],[83,-131],[-22,-169],[-188,-96],[-133,11],[-205,-103],[-121,-29],[-162,-29],[-231,-171],[408,111],[82,-112],[-389,-177],[-177,-1],[8,72],[-84,-164],[82,-27],[-60,-424],[-203,-455],[-20,152],[-61,30],[-91,148],[57,-318],[69,-105],[5,-223],[-89,-230],[-157,-472],[-25,24],[86,402],[-142,225],[-33,491],[-53,-255],[59,-375],[-183,93],[191,-191],[12,-562],[79,-41],[29,-204],[39,-591],[-176,-439],[-288,-175],[-182,-346],[-139,-38],[-141,-217],[-39,-199],[-305,-383],[-157,-281],[-131,-351],[-43,-419],[50,-411],[92,-505],[124,-418],[1,-256],[132,-685],[-9,-398],[-12,-230],[-69,-361],[-83,-75],[-137,72],[-44,259],[-105,136],[-148,508],[-129,452],[-42,231],[57,393],[-77,325],[-217,494],[-108,90],[-281,-268],[-49,30],[-135,275],[-174,147],[-314,-75],[-247,66],[-212,-41],[-114,-92],[50,-157],[-5,-240],[59,-117],[-53,-77],[-103,87],[-104,-112],[-202,18],[-207,312],[-242,-73],[-202,137],[-173,-42],[-234,-138],[-253,-438],[-276,-255],[-152,-282],[-63,-266],[-3,-407],[14,-284],[52,-201]],[[17464,69802],[-46,302],[-180,340],[-130,71],[-30,169],[-156,30],[-100,159],[-258,59],[-71,95],[-33,324],[-270,594],[-231,821],[10,137],[-123,195],[-215,495],[-38,482],[-148,323],[61,489],[-10,507],[-89,453],[109,557],[34,536],[33,536],[-50,792],[-88,506],[-80,274],[33,115],[402,-200],[148,-558],[69,156],[-45,484],[-94,485]],[[7498,84325],[-277,-225],[-142,152],[-43,277],[252,210],[148,90],[185,-40],[117,-183],[-240,-281]],[[4006,85976],[-171,-92],[-182,110],[-168,161],[274,101],[220,-54],[27,-226]],[[2297,88264],[171,-113],[173,61],[225,-156],[276,-79],[-23,-64],[-211,-125],[-211,128],[-106,107],[-245,-34],[-66,52],[17,223]],[[13740,82958],[-153,223],[-245,188],[-78,515],[-358,478],[-150,558],[-267,38],[-441,15],[-326,170],[-574,613],[-266,112],[-486,211],[-385,-51],[-546,272],[-330,252],[-309,-125],[58,-411],[-154,-38],[-321,-123],[-245,-199],[-308,-126],[-39,348],[125,580],[295,182],[-76,148],[-354,-329],[-190,-394],[-400,-420],[203,-287],[-262,-424],[-299,-248],[-278,-180],[-69,-261],[-434,-305],[-87,-278],[-325,-252],[-191,45],[-259,-165],[-282,-201],[-231,-197],[-477,-169],[-43,99],[304,276],[271,182],[296,324],[345,66],[137,243],[385,353],[62,119],[205,208],[48,448],[141,349],[-320,-179],[-90,102],[-150,-215],[-181,300],[-75,-212],[-104,294],[-278,-236],[-170,0],[-24,352],[50,216],[-179,211],[-361,-113],[-235,277],[-190,142],[-1,334],[-214,252],[108,340],[226,330],[99,303],[225,43],[191,-94],[224,285],[201,-51],[212,183],[-52,270],[-155,106],[205,228],[-170,-7],[-295,-128],[-85,-131],[-219,131],[-392,-67],[-407,142],[-117,238],[-351,343],[390,247],[620,289],[228,0],[-38,-296],[586,23],[-225,366],[-342,225],[-197,296],[-267,252],[-381,187],[155,309],[493,19],[350,270],[66,287],[284,281],[271,68],[526,262],[256,-40],[427,315],[421,-124],[201,-266],[123,114],[469,-35],[-16,-136],[425,-101],[283,59],[585,-186],[534,-56],[214,-77],[370,96],[421,-177],[302,-83]],[[30185,57537],[-8,-139],[-163,-69],[91,-268],[-3,-309],[-123,-344],[105,-468],[120,38],[62,427],[-86,208],[-14,447],[346,241],[-38,278],[97,186],[100,-415],[195,-9],[180,-330],[11,-195],[249,-6],[297,61],[159,-264],[213,-74],[155,185],[4,149],[344,35],[333,9],[-236,-175],[95,-279],[222,-44],[210,-291],[45,-473],[144,13],[109,-139]],[[80013,63313],[-371,-505],[-231,-558],[-61,-410],[212,-623],[260,-772],[252,-365],[169,-475],[127,-1093],[-37,-1039],[-232,-389],[-318,-381],[-227,-492],[-346,-550],[-101,378],[78,401],[-206,335]],[[96623,40851],[-92,-78],[-93,259],[10,158],[175,-339]],[[96418,41756],[45,-476],[-75,74],[-58,-32],[-39,163],[-6,453],[133,-182]],[[64752,60417],[-201,-158],[-54,-263],[-6,-201],[-277,-249],[-444,-276],[-249,-417],[-122,-33],[-83,35],[-163,-245],[-177,-114],[-233,-30],[-70,-34],[-61,-156],[-73,-43],[-43,-150],[-137,13],[-89,-80],[-192,30],[-72,345],[8,323],[-46,174],[-54,437],[-80,243],[56,29],[-29,270],[34,114],[-12,257]],[[58175,37528],[113,-7],[134,-100],[94,71],[148,-59]],[[59119,34780],[-70,-430],[-32,-491],[-72,-267],[-190,-298],[-54,-86],[-118,-300],[-77,-303],[-158,-424],[-314,-609],[-196,-355],[-210,-269],[-290,-229],[-141,-31],[-36,-164],[-169,88],[-138,-113],[-301,114],[-168,-72],[-115,31],[-286,-233],[-238,-94],[-171,-223],[-127,-14],[-117,210],[-94,11],[-120,264],[-13,-82],[-37,159],[2,346],[-90,396],[89,108],[-7,453],[-182,553],[-139,501],[-1,1],[-199,768]],[[58409,41417],[-210,-81],[-159,-235],[-33,-205],[-100,-46],[-241,-486],[-154,-383],[-94,-13],[-90,68],[-311,65]]],"bbox":[-180,-85.60903777459767,180,83.64513000000001],"transform":{"scale":[0.0036000360003600037,0.00169255860333201],"translate":[-180,-85.60903777459767]}};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.OfflineMapNode.TOPOJSON_SWISSTOPO_PROPERTIES = [
    {
        "id": 1,
        "type": "canton",
        "title": "Zurich",
        "canton": "ZH",
        "latlon": [47.455619, 8.611638],
        "flagHeightMultiplier": 1.1,
    },
    {
        "id": 2,
        "type": "canton",
        "title": "Bern",
        "canton": "BE",
        "latlon": [46.542248, 7.518674],
        "flagHeightMultiplier": 0.7,
    }, 
    {
        "id": 3,
        "type": "canton",
        "title": "Luzern",
        "canton": "LU",
        "latlon": [47.100035, 8.093186],
    },
    {
        "id": 4,
        "type": "canton",
        "title": "Uri",
        "canton": "UR",
        "latlon": [46.636353, 8.531168],
        "flagHeightMultiplier": 0.7,
    },
    {
        "id": 5,
        "type": "canton",
        "title": "Schwyz",
        "canton": "SZ",
        "latlon": [47.014090, 8.657386],
        "flagHeightMultiplier": 0.9,
    },
    {
        "id": 6,
        "type": "canton",
        "title": "Obwalden",
        "canton": "OW",
        "latlon": [46.806171, 8.265746],
        "flagHeightMultiplier": 0.7,
    },
    {
        "id": 7,
        "type": "canton",
        "title": "Nidwalden",
        "canton": "NW",
        "latlon": [46.915550, 8.406949],
        "flagHeightMultiplier": 0.8,
    },
    {
        "id": 8,
        "type": "canton",
        "title": "Glarus",
        "canton": "GL",
        "latlon": [46.991023, 9.132280],
        "flagHeightMultiplier": 1.4,
    },
    {
        "id": 9,
        "type": "canton",
        "title": "Zug",
        "canton": "ZG",
        "latlon": [47.174622, 8.561308],
        "flagHeightMultiplier": 1.2,
    },
    {
        "id": 10,
        "type": "canton",
        "title": "Fribourg",
        "canton": "FR",
        "latlon": [46.713558, 7.132085],
    },
    {
        "id": 11,
        "type": "canton",
        "title": "Solothurn",
        "canton": "SO",
        "latlon": [47.222825, 7.515837],
    },
    {
        "id": 12,
        "type": "canton",
        "title": "Basel-City",
        "canton": "BS",
        "latlon": [47.568656, 7.620496],
        "flagHeightMultiplier": 1.2,
    },
    {
        "id": 13,
        "type": "canton",
        "title": "Basel-Country",
        "canton": "BL",
        "latlon": [47.506028, 7.537918],
        "flagHeightMultiplier": 0.95,
    },
    {
        "id": 14,
        "type": "canton",
        "title": "Schaffhausen",
        "canton": "SH",
        "latlon": [47.729534, 8.590271],
    },
    {
        "id": 15,
        "type": "canton",
        "title": "Appenzell Ausserrhoden",
        "canton": "AR",
        "latlon": [47.418533, 9.540398],
        "flagHeightMultiplier": 1.24,
    },
    {
        "id": 16,
        "type": "canton",
        "title": "Appenzell Innerrhoden",
        "canton": "AI",
        "latlon": [47.306468, 9.404361],
        "flagHeightMultiplier": 1.15,
    },
    {
        "id": 17,
        "type": "canton",
        "title": "St. Gallen",
        "canton": "SG",
        "latlon": [46.979891, 9.418409],
        "flagHeightMultiplier": 0.6,
    },
    {
        "id": 18,
        "type": "canton",
        "title": "Grisons",
        "canton": "GR",
        "latlon": [46.578184, 9.811454],
    },
    {
        "id": 19,
        "type": "canton",
        "title": "Aargau",
        "canton": "AG",
        "latlon": [47.328458, 8.159894],
        "flagHeightMultiplier": 1.1,
    },
    {
        "id": 20,
        "type": "canton",
        "title": "Thurgau",
        "canton": "TG",
        "latlon": [47.553740, 9.234235],
        "flagHeightMultiplier": 1.2,
    },
    {
        "id": 21,
        "type": "canton",
        "title": "Ticino",
        "canton": "TI",
        "latlon": [46.212487, 8.775499],
    },
    {
        "id": 22,
        "type": "canton",
        "title": "Vaud",
        "canton": "VD",
        "latlon": [46.542595, 6.384264],
    },
    {
        "id": 23,
        "type": "canton",
        "title": "Valais",
        "canton": "VS",
        "latlon": [46.072563, 7.399326],
    },
    {
        "id": 24,
        "type": "canton",
        "title": "Neuchâtel",
        "canton": "NE",
        "latlon": [46.988537, 6.777846],
    },
    {
        "id": 25,
        "type": "canton",
        "title": "Geneva",
        "canton": "GE",
        "latlon": [46.177542, 6.081361],
    },
    {
        "id": 26,
        "type": "canton",
        "title": "Jura",
        "canton": "JU",
        "latlon": [47.421971, 7.066423],
        "flagHeightMultiplier": 0.8,
    },
];

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.OfflineMapNode.TOPOJSON_SWISSTOPO_FEATURES = {"type":"Topology","arcs":[[[14393,16679],[29,-24],[-21,-80],[46,-65],[30,7]],[[14477,16517],[66,39]],[[14543,16556],[24,132],[-24,45],[21,95],[41,15],[26,-71],[29,-32],[75,-8],[12,-131],[24,-91],[27,-25],[116,75],[65,-22],[15,-39],[66,-49],[-9,-44],[-86,-6],[-50,-57],[-16,-70],[37,-70]],[[14936,16203],[81,-25],[-36,-176],[8,-32]],[[14989,15970],[155,-25],[157,-107],[54,-7]],[[15355,15831],[-13,35],[35,96],[-11,86],[-23,17]],[[15343,16065],[-129,19],[18,52],[-16,39],[59,30],[23,44],[75,1]],[[15373,16250],[-14,60],[54,30],[34,-117],[62,-105],[49,29],[67,-28],[16,-62]],[[15641,16057],[123,12],[-110,-156],[3,-39]],[[15657,15874],[48,-2],[71,-83],[8,-117]],[[15784,15672],[-1,-11]],[[15783,15661],[41,0],[78,-55],[236,69],[129,71],[95,77],[216,97],[188,-18],[85,-43],[122,-1],[62,-31],[61,-54],[112,-34],[23,3],[29,24],[53,-13]],[[17313,15753],[29,-45]],[[17342,15708],[40,-46],[63,9]],[[17445,15671],[27,13],[150,-19],[225,29],[747,-311],[546,-559],[61,-14]],[[19201,14810],[280,-63],[13,-347]],[[19494,14400],[17,-45],[66,-57]],[[19577,14298],[38,-44],[61,-142]],[[19676,14112],[48,-10],[43,57],[58,-97],[174,-26],[19,-53],[-39,-41]],[[19979,13942],[-35,-61],[36,-152]],[[19980,13729],[4,-96]],[[19984,13633],[45,-57],[73,-51],[-1,-79],[-59,-85],[-70,-29],[-135,-11]],[[19837,13321],[-129,-156],[-67,-238],[-36,-52],[-86,-56]],[[19519,12819],[-35,-33],[-85,-178]],[[19399,12608],[-74,-72]],[[19325,12536],[-2,-55],[-49,-150],[-96,-173],[-35,-130],[-32,-69]],[[19111,11959],[-22,-164],[50,-171],[45,-79]],[[19184,11545],[30,-58],[42,-202]],[[19256,11285],[18,-121],[-44,-120],[-202,-158]],[[19028,10886],[-14,-38],[20,-77]],[[19034,10771],[54,-17],[34,62],[106,-1],[29,29],[72,19]],[[19329,10863],[50,17],[64,-51],[28,-78]],[[19471,10751],[167,48]],[[19638,10799],[82,54],[78,-67],[104,-15]],[[19902,10771],[46,65],[71,-13],[128,32],[34,-56],[51,-23],[53,12],[-2,-47],[61,-38],[125,-6],[36,-46],[191,12]],[[20696,10663],[46,-55],[52,-21],[59,-59],[64,-18],[35,-42]],[[20952,10468],[119,16],[43,56],[84,-16],[22,-27],[-49,-40],[9,-53],[86,-47],[19,-84],[-50,-57],[-40,-78],[-24,-86],[41,-49],[-21,-54],[7,-128]],[[21198,9821],[99,-24],[64,-52],[32,7],[50,-59],[87,-45],[46,-8],[70,30],[41,-31],[61,30],[24,-78],[42,-31],[85,-29],[65,22],[47,-47],[86,-124],[55,-38],[-8,-96],[46,-26],[150,3],[28,-24],[23,-76]],[[22391,9125],[45,-64]],[[22436,9061],[76,26],[21,35],[88,-9],[43,33],[118,-6],[64,35],[69,93],[55,-15],[83,22],[73,-8],[0,43]],[[23126,9310],[11,106],[-39,45],[-10,46],[41,47],[20,114],[27,25],[-8,101],[92,-12]],[[23260,9782],[195,-65]],[[23455,9717],[39,23],[83,4]],[[23577,9744],[6,47],[-60,75],[17,81],[100,31],[10,81],[81,152],[10,56],[46,22],[92,-13],[62,69],[32,11],[50,-30],[19,-49],[56,-66],[76,-57],[-23,-128],[58,-29]],[[24209,9997],[87,-32],[34,4],[73,-47],[42,-49],[69,-26],[-22,-52],[16,-42],[-11,-87],[-42,-40],[-19,-132],[-56,-81],[25,-26],[11,-66],[-18,-88],[21,-108],[-43,-60],[14,-45],[-53,-46],[20,-46],[-52,-164],[-64,-54],[-51,-7],[-37,-69]],[[24153,8634],[60,-52],[43,-81],[11,-118],[-236,-199],[48,-35],[53,-76],[-15,-82],[-69,-28],[-54,-133],[-62,-39],[57,-105],[-7,-102],[16,-34]],[[23998,7550],[34,-70],[11,-75],[37,-14],[60,27],[125,8],[14,16],[90,-67],[117,-127],[42,-17],[-41,-83],[19,-127],[-17,-99],[-54,-87],[16,-76],[-36,-71],[6,-39],[-71,-18],[-34,-86],[-87,54],[-101,113],[-112,-60],[-71,50],[-74,31],[-100,14],[-10,-50],[-63,-55],[-74,73],[-77,-41],[-18,23],[-64,3],[-1,72],[-47,94],[-127,54],[-51,-46],[-54,53],[-11,112],[93,153],[-75,94]],[[23192,7286],[-31,107],[-83,-51],[-46,-98],[-49,31],[-126,26],[-112,-67],[-135,-40],[-48,-42],[-82,13],[-59,29]],[[22421,7194],[-6,-79],[-23,-63],[22,-34],[-31,-95],[-91,-18],[37,-64],[-77,-70],[-18,-67],[-34,-36],[-80,-15]],[[22120,6653],[-15,-53],[50,-46],[4,-74],[-13,-76],[-49,-27],[13,-132],[20,-48],[-25,-39],[7,-49]],[[22112,6109],[35,-42],[4,-92],[-57,-65],[0,-80]],[[22094,5830],[78,-14],[21,-101],[23,-20],[69,17],[17,-57],[65,14],[44,-11],[41,56],[114,28],[76,-28],[21,-43],[3,-71],[72,10],[31,-63],[-26,-34],[10,-104],[-42,-46],[-72,10],[-83,-69],[-7,-63],[17,-73],[-115,-82],[-20,-139],[46,-83],[20,-76],[118,-77],[-4,-29]],[[22611,4682],[90,-67],[3,-65],[42,-19],[5,-89],[46,-39],[25,-68],[-9,-29],[-130,-122],[-26,-74],[-74,-46],[-49,-9],[-76,16],[-31,23],[-64,-3],[-62,-28],[-52,-62],[-65,64],[-81,39],[50,44],[3,51],[39,55]],[[22195,4254],[-37,152],[-134,84],[-107,27],[-71,33],[-23,92],[48,43],[-31,103],[-80,73],[30,91],[56,65],[-12,82],[-47,-5],[-111,97],[-47,79],[18,37]],[[21647,5307],[-33,9],[-102,-35],[-51,-73],[-44,38],[-48,82]],[[21369,5328],[-129,-59],[-18,-40],[-67,-49],[-65,3],[-131,-15]],[[20959,5168],[-65,-83],[-57,-18],[-38,-35],[-97,-26]],[[20702,5006],[-32,-45],[-48,4],[-65,69],[-77,60],[-41,-12],[-69,-78],[21,-65],[-52,-71],[47,-35],[-9,-78],[-33,-30],[-21,-109],[-63,-17],[-113,51],[-29,48],[-39,-54],[-50,-2],[-56,-51],[-57,-30],[-79,19],[-31,-9],[-45,59],[-60,9],[-109,-7],[-33,34],[-101,56],[-27,-29],[-56,46],[-110,152],[-24,45],[-32,155],[-43,38],[-22,65],[-72,26],[-120,70],[42,105],[-38,189],[-29,30]],[[18927,5614],[-11,39],[15,130],[20,47],[-9,168],[35,52],[-18,87]],[[18959,6137],[-67,-3],[-4,35],[65,4]],[[18953,6173],[18,186]],[[18971,6359],[-164,-80],[-50,-76],[-2,-96],[-74,-80],[-113,51],[-25,73],[-40,15],[-51,85],[27,74]],[[18479,6325],[-60,48]],[[18419,6373],[-39,-43],[-87,-3],[-8,17],[-143,-15],[-110,-57],[-43,-3]],[[17989,6269],[-20,-75],[-29,-173],[23,-41],[-64,-77],[-103,-39],[15,-128],[59,-48],[15,-36],[59,-14],[33,-47],[6,-94],[-34,-53],[44,-92],[-45,-82],[5,-46]],[[17953,5224],[35,-81],[75,-18],[19,-97],[-41,-63],[42,-77],[-29,-74],[-51,-24]],[[18003,4790],[-20,-41],[18,-100],[-71,-60],[-22,-60],[-46,-29],[-12,-74],[-27,-17]],[[17823,4409],[-20,-161]],[[17803,4248],[8,-63],[-19,-63],[-59,12],[-79,-37]],[[17654,4097],[12,-41],[-23,-78],[-24,-28],[-66,-20]],[[17553,3930],[-40,-119],[18,-54],[-22,-69],[-70,-75],[-45,14]],[[17394,3627],[-22,3]],[[17372,3630],[-50,-14]],[[17322,3616],[-15,-60],[-120,-72],[-70,-150],[-91,-23],[-27,-43],[-57,-7],[-57,-37]],[[16885,3224],[-33,-26]],[[16852,3198],[90,-227],[1,-35],[-56,-80],[-4,-87],[-24,-21],[-72,-15],[-56,13],[-58,-27],[-57,-48],[-63,-27],[-1,-53],[-41,-48],[-10,-61],[81,-112]],[[16582,2370],[10,-122]],[[16592,2248],[22,-59],[-56,-49],[-37,2],[-21,-61],[-65,1],[2,-62],[-25,-10]],[[16412,2010],[15,-41],[108,-49],[18,-92],[-27,-43]],[[16526,1785],[55,-54],[-17,-71]],[[16564,1660],[57,-16]],[[16621,1644],[70,8],[36,-44],[57,-9],[-5,-36],[95,-41],[2,-98],[44,-27],[-39,-89],[-62,-78],[-50,-2],[-43,-102],[12,-57]],[[16738,1069],[-4,-27],[-55,-47],[-28,-55]],[[16651,940],[13,-34],[-37,-87],[-78,-55],[-98,40],[-23,27],[18,71]],[[16446,902],[-115,13],[-34,-34],[-46,32],[-30,53]],[[16221,966],[-49,-1]],[[16172,965],[-75,-31],[-21,-45],[-46,7],[-45,-31],[11,96],[99,152],[37,22]],[[16132,1135],[14,54],[-42,2],[-12,118],[-56,86],[17,64],[-24,43]],[[16029,1502],[-22,44]],[[16007,1546],[-49,27],[-78,124],[32,120],[-27,91],[-65,-17],[-82,48],[-36,28]],[[15702,1967],[-33,33]],[[15669,2000],[-60,90],[-60,53],[-43,-3]],[[15506,2140],[-52,22],[-98,14],[-54,-24],[39,154]],[[15341,2306],[22,33]],[[15363,2339],[47,79],[75,29]],[[15485,2447],[46,71]],[[15531,2518],[10,60],[29,18]],[[15570,2596],[-5,61],[52,-23],[57,106],[-14,114]],[[15660,2854],[-99,59],[-100,117],[-50,32],[-34,-53],[-90,-5],[-148,89]],[[15139,3093],[-73,139],[-96,-97],[-57,-106],[-86,36]],[[14827,3065],[-66,4],[-35,67],[-51,-13],[-66,32],[-52,86]],[[14557,3241],[-196,-14],[-12,91],[-76,81],[40,102],[-81,10],[-4,40],[-59,9],[-23,34],[-13,83]],[[14133,3677],[-32,54],[-53,19],[-2,48],[-60,43],[-33,56],[-21,114],[-59,29],[-35,-14],[-123,73],[-128,31],[-25,94]],[[13562,4224],[-91,27],[-27,51],[34,59],[40,19],[-33,86],[-73,92],[-47,102],[70,47]],[[13435,4707],[17,125],[55,14],[9,49],[53,53],[-6,88]],[[13563,5036],[-15,70],[47,75],[-18,47],[-2,95],[-31,54],[56,75],[-44,72],[29,56],[-62,93],[21,42],[-18,52],[48,64],[-23,65]],[[13551,5896],[-50,38],[-15,52],[-62,20],[-116,-44],[-47,-1],[-84,-58],[-44,5]],[[13133,5908],[-96,-2],[-61,-40],[-18,-43],[-146,-130],[-56,-16],[-41,18]],[[12715,5695],[-46,-95],[-54,-49]],[[12615,5551],[79,-46],[33,13],[33,-42],[-16,-177],[-69,-24],[-56,-40],[-29,-45],[-61,26],[-50,-23],[11,-99],[-70,-44],[-16,-46],[-79,-3],[-61,-48],[5,-65],[-56,-55],[-14,-84],[-36,-26]],[[12163,4723],[-36,-31],[-31,16],[-76,-55],[-95,-13],[-78,46],[-32,3]],[[11815,4689],[-109,-81],[-40,-87],[-43,-12],[-27,-48],[-79,-58],[-30,-49],[32,-51],[81,-15],[2,-24]],[[11602,4264],[46,0],[16,-87],[40,-22],[20,-52],[79,-28],[25,-118],[43,-71],[10,-95],[66,-72],[-2,-44],[-35,-23],[-47,-93],[-1,-46],[30,-75],[-59,-88],[-104,-12],[-53,-40],[-8,-105],[-30,-47],[-143,-50],[-68,4],[-81,-38],[-108,-4]],[[11238,3058],[-26,-91],[-2,-70],[-36,-50],[7,-96],[63,-153],[-72,-41],[-55,-63],[30,-53],[-36,-48],[8,-55],[-61,2],[-38,-100],[-27,-32],[-64,28],[-107,-27],[-43,12],[-91,-13],[-129,8]],[[10559,2216],[-81,-149],[-88,-38],[23,-80],[-37,-50],[12,-71],[-37,-14],[-10,-90],[46,-75],[-21,-54],[-197,11],[-88,42],[-113,-77],[-47,61],[-77,49],[-39,50],[-117,31],[-10,-50],[-58,-88],[-79,-2],[-42,36],[-29,54],[13,108],[-55,48],[-65,7],[-43,23],[-14,59],[-48,73],[-58,21],[-97,-53],[-107,18],[-53,-24],[-114,9],[-38,32],[-17,94],[-21,13]],[[8753,2140],[-69,-16],[-64,8],[-47,-85],[28,-105],[-21,-51],[-80,-3],[-56,47],[-55,-35],[-45,36],[-31,-55],[-32,1]],[[8281,1882],[-52,-27],[-35,-43],[19,-85],[-18,-21],[-70,12],[-15,-28],[-61,-3],[-55,-93],[-40,-40],[-88,-54],[-49,23],[-24,-54],[-80,-68],[-89,62],[-26,-7],[-91,97],[-74,-38],[-68,9],[-10,40],[-79,5],[-39,37]],[[7237,1606],[-49,-49],[-46,-122],[-29,-7],[-79,-94],[-40,40],[-48,-25],[-67,16],[-64,-26],[-4,-40],[-52,-30],[-45,-80],[17,-59],[-55,-36],[-84,34],[-72,70],[12,42],[-57,21]],[[6475,1261],[-99,-53],[-20,-48],[-74,-61],[-90,0],[-49,104],[17,31],[-83,76],[-18,80],[-67,39],[1,76],[-61,51],[-44,55],[-17,70],[-33,60],[15,52],[-5,77],[-104,48],[-50,74],[9,32],[55,24],[7,35]],[[5765,2083],[-55,55],[-8,81],[-59,15],[-81,48],[-20,123],[-98,83],[-71,172]],[[5373,2660],[-36,-6],[-39,34],[10,72],[-25,22],[-50,-16],[-80,-115],[-78,-20],[-21,-46],[-82,42],[-13,64],[41,113],[58,36]],[[5058,2840],[-17,39],[19,51],[-48,83],[67,105],[-11,39],[33,90]],[[5101,3247],[-49,11],[-44,-20],[-32,23],[-135,6],[-53,45]],[[4788,3312],[-120,-8],[-62,41],[-54,5],[-40,142],[10,72],[72,78],[37,70]],[[4631,3712],[-30,122]],[[4601,3834],[-18,54],[35,87],[63,78],[-5,53]],[[4676,4106],[52,35],[36,52],[16,62],[81,51],[-3,35]],[[4858,4341],[30,56],[-5,64],[31,55],[-32,80]],[[4882,4596],[-64,-3],[-69,79],[-23,51],[-58,75]],[[4668,4798],[-69,43],[-32,-6],[-23,105],[-65,1],[-27,93],[-25,23]],[[4427,5057],[-23,65],[6,48],[50,43],[59,5],[65,78],[11,52],[-22,31],[19,58]],[[4592,5437],[85,268]],[[4677,5705],[-752,221],[-880,16],[-506,-329],[-489,-98],[-445,-351],[-62,-137]],[[1543,5027],[-53,-114]],[[1490,4913],[-65,-143]],[[1425,4770],[118,-59]],[[1543,4711],[31,-35],[22,-100],[-38,-23]],[[1558,4553],[-34,-27],[-1,-42],[67,-116]],[[1590,4368],[58,-60],[31,-58],[98,55],[57,12]],[[1834,4317],[72,-8],[9,-89],[-85,-155],[-15,-12]],[[1815,4053],[-83,-66],[-59,-16],[-90,-68],[-21,13]],[[1562,3916],[-58,-2],[-56,-40]],[[1448,3874],[-52,-61],[-40,-14],[-46,-68],[-66,-45]],[[1244,3686],[15,-97],[-73,-66]],[[1186,3523],[-125,-53],[-30,-46]],[[1031,3424],[-61,-38],[-196,22],[-40,63]],[[734,3471],[-93,-19]],[[641,3452],[-123,16],[-22,-84],[-86,-13],[-41,11]],[[369,3382],[-49,18],[-62,-8],[-53,20],[-61,-18]],[[144,3394],[-34,-78],[-60,-24],[-50,20],[53,41],[-7,61],[63,127]],[[109,3541],[30,100],[35,-15],[22,49]],[[196,3675],[16,49],[-21,37],[-150,77],[50,45],[-20,34],[50,83],[79,-11],[38,37]],[[238,4026],[54,73],[82,33],[46,43],[68,-57]],[[488,4118],[80,111],[46,-33],[100,48],[74,-77]],[[788,4167],[39,18],[79,88]],[[906,4273],[-19,86]],[[887,4359],[-89,140],[-7,52],[59,76],[43,24]],[[893,4651],[-8,124],[33,41]],[[918,4816],[30,58]],[[948,4874],[42,81]],[[990,4955],[-3,38],[57,27],[48,67]],[[1092,5087],[7,54],[59,72]],[[1158,5213],[-63,106]],[[1095,5319],[-47,-17],[-76,84],[-120,98],[-44,-1],[-39,72],[-185,62],[59,126]],[[643,5743],[50,63]],[[693,5806],[1,63],[-52,49],[-11,100],[132,129]],[[763,6147],[85,229],[133,168],[36,-21],[50,70],[-26,22],[43,48]],[[1084,6663],[-246,255],[81,104],[69,62],[81,54],[40,50],[97,50],[7,33],[84,65],[21,49],[45,15],[61,71]],[[1424,7471],[45,26],[32,56],[183,174],[10,51],[74,63],[132,66]],[[1900,7907],[90,60],[97,35],[56,39],[42,65],[63,10],[-9,49],[83,14]],[[2322,8179],[33,49],[-17,41],[66,60],[89,17]],[[2493,8346],[74,35],[47,45],[-3,31]],[[2611,8457],[69,61]],[[2680,8518],[1,38],[36,79],[-129,111]],[[2588,8746],[-20,83],[52,32],[-5,64],[15,63],[95,160]],[[2725,9148],[15,194]],[[2740,9342],[9,121],[-36,85],[-119,168],[-16,55],[64,40],[80,79],[66,102]],[[2788,9992],[78,93]],[[2866,10085],[57,57],[50,-65],[69,38],[243,70],[153,92]],[[3438,10277],[23,14],[112,-7],[91,55],[55,66],[79,160],[105,72],[114,29]],[[4017,10666],[113,110],[-59,46],[-43,7],[-50,61],[76,61],[-16,62],[70,56],[55,27],[71,-11]],[[4234,11085],[38,49],[-33,99],[136,95],[55,5],[92,49]],[[4522,11382],[68,26],[55,42],[73,89],[116,82],[48,71]],[[4882,11692],[-74,9],[-21,41],[121,70],[54,77],[34,85],[51,41]],[[5047,12015],[113,106],[73,32],[20,52],[64,21],[18,28]],[[5335,12254],[3,29],[66,39],[-48,85],[25,51],[4,76],[-60,141],[67,48],[83,3],[43,29],[108,-2],[37,43],[22,-1]],[[5685,12795],[48,103],[-35,84],[133,33]],[[5831,13015],[61,-13],[59,62],[27,76],[-63,28],[14,36],[-19,82],[-79,15],[-4,34]],[[5827,13335],[-115,40]],[[5712,13375],[-34,-42],[-55,-34],[-69,1],[-47,-30],[-125,0],[-99,-11],[-65,-26],[-104,34],[-92,-54],[-31,38],[31,77],[-6,50],[64,55],[52,25]],[[5132,13458],[46,52],[10,64],[29,64],[96,10]],[[5313,13648],[17,78],[-14,143],[132,16]],[[5448,13885],[36,93],[61,15],[47,-9],[61,48],[-2,65]],[[5651,14097],[-4,41]],[[5647,14138],[-75,76],[11,90],[-12,51],[79,52],[127,34],[68,-54]],[[5845,14387],[127,-19],[59,-24],[19,-33]],[[6050,14311],[91,54],[109,2]],[[6250,14367],[87,69],[55,-12],[67,-40],[48,-52]],[[6507,14332],[54,-11],[171,36],[-55,-46],[-72,-162],[7,-33]],[[6612,14116],[-3,-49],[-42,-119]],[[6567,13948],[118,-38],[21,-27],[54,-4],[105,40],[56,-24],[14,-66],[35,-15],[3,-53]],[[6973,13761],[46,29],[89,25],[65,62],[52,-12],[58,45],[127,9]],[[7410,13919],[67,9],[46,-45],[182,-28]],[[7705,13855],[105,28],[34,39],[78,50]],[[7922,13972],[40,58],[9,47],[80,19]],[[8051,14096],[56,71]],[[8107,14167],[-62,65],[-78,33],[-21,48],[50,77],[107,-69],[41,-3],[47,-70]],[[8191,14248],[91,15]],[[8282,14263],[53,57]],[[8335,14320],[73,61],[0,47]],[[8408,14428],[-9,57],[-31,41],[71,19],[39,-15]],[[8478,14530],[41,102]],[[8519,14632],[-20,37],[-63,2]],[[8436,14671],[-74,-33],[-17,92],[28,25]],[[8373,14755],[80,26],[32,45],[162,105]],[[8647,14931],[13,65],[47,39],[100,-14],[23,115],[86,-41],[-2,-57],[79,-8],[29,21]],[[9022,15051],[101,97],[14,45],[116,-40],[75,-90]],[[9328,15063],[13,-79],[-72,-43],[-84,-28]],[[9185,14913],[-35,-20],[-76,10]],[[9074,14903],[61,-69]],[[9135,14834],[33,-46],[54,-17],[29,-67],[46,-25]],[[9297,14679],[59,-8]],[[9356,14671],[58,3],[91,53]],[[9505,14727],[55,22],[155,23]],[[9715,14772],[28,27],[138,40],[67,34],[20,42],[68,66],[37,132],[75,-4],[26,-32]],[[10174,15077],[52,-2],[72,40],[133,13],[70,-39]],[[10501,15089],[46,-58],[27,-64],[-22,-67],[6,-44]],[[10558,14856],[47,-62],[82,-7],[44,-19]],[[10731,14768],[44,8],[36,94],[51,1]],[[10862,14871],[71,-16],[115,9],[44,-11]],[[11092,14853],[5,-4]],[[11097,14849],[61,-33],[143,39]],[[11301,14855],[72,66],[65,6],[66,-54],[74,29]],[[11578,14902],[23,43],[8,79],[53,60],[72,-2]],[[11734,15082],[56,7],[11,54],[48,34],[64,-6]],[[11913,15171],[35,1],[48,62],[54,23]],[[12050,15257],[51,93],[42,33],[67,-3],[39,-36]],[[12249,15344],[15,-70]],[[12264,15274],[29,-12],[48,59],[109,20],[15,-14]],[[12465,15327],[21,-33],[80,18],[46,-10]],[[12612,15302],[50,-40],[-14,-88],[17,-47],[98,-80]],[[12763,15047],[44,-51],[69,-18]],[[12876,14978],[135,-8]],[[13011,14970],[115,-32],[47,77]],[[13173,15015],[40,17],[91,-51]],[[13304,14981],[54,-26]],[[13358,14955],[58,-4],[102,43],[44,-1]],[[13562,14993],[69,45],[37,-5]],[[13668,15033],[18,4]],[[13686,15037],[41,28]],[[13727,15065],[-41,50]],[[13686,15115],[-98,-27],[-64,108],[-2,41],[62,11],[52,57]],[[13636,15305],[30,24],[135,31],[-5,51],[49,66]],[[13845,15477],[117,-6],[3,-41],[101,-13],[49,-119],[-19,-85]],[[14096,15213],[106,-26],[71,76]],[[14273,15263],[43,46]],[[14316,15309],[8,39],[-41,105],[-11,111],[67,79],[33,-67],[-51,-21],[11,-36],[92,32],[26,62]],[[14450,15613],[-2,37],[-73,67]],[[14375,15717],[-44,61]],[[14331,15778],[-44,30],[-23,-46],[-88,-44],[-70,30],[-6,37]],[[14100,15785],[-89,-23]],[[14011,15762],[-38,-16],[7,-65],[-78,27]],[[13902,15708],[35,-80],[-10,-39],[-48,-6],[-51,20],[-109,-3],[-83,-20],[-56,-27],[-20,59],[-79,50]],[[13481,15662],[-67,21],[-70,77],[-62,-4],[-36,66],[81,76]],[[13327,15898],[-46,84],[-42,31],[67,97],[49,9],[105,96],[54,-2]],[[13514,16213],[10,53]],[[13524,16266],[-40,73],[34,37],[5,54],[53,35],[38,88],[83,70],[38,-21],[76,45],[54,-46]],[[13865,16601],[34,62],[34,-1],[108,54],[73,-54]],[[14114,16662],[59,28]],[[14173,16690],[-12,65],[-71,27],[10,101],[51,6],[12,-51],[68,24],[25,-32],[48,34],[68,-15],[44,-52],[-38,-79],[15,-39]],[[14096,15213],[-32,-16]],[[14064,15197],[-50,-38]],[[14014,15159],[-12,-6]],[[14002,15153],[-40,-33]],[[13962,15120],[-19,-55],[65,-59],[-27,-32],[33,-38],[38,-101]],[[14052,14835],[49,38]],[[14101,14873],[21,41]],[[14122,14914],[24,95]],[[14146,15009],[44,23],[-15,55],[32,74],[52,21]],[[14259,15182],[14,81]],[[14331,15778],[35,67],[60,-8]],[[14426,15837],[19,24],[-12,100]],[[14433,15961],[38,9]],[[14471,15970],[62,15],[107,-70]],[[14640,15915],[38,-9]],[[14678,15906],[-12,-58]],[[14666,15848],[47,-48],[6,-48]],[[14719,15752],[17,-72],[40,-61],[60,-20],[49,-40],[45,-3]],[[14930,15556],[80,2]],[[15010,15558],[60,6],[23,-32]],[[15093,15532],[57,35],[59,58],[41,-10],[53,51],[14,82],[54,-3]],[[15371,15745],[22,-1]],[[15393,15744],[12,-102],[116,-45],[-5,-78]],[[15516,15519],[-28,-63],[-37,-16],[-10,-68],[-36,-43],[-4,-46]],[[15401,15283],[-6,-57],[-78,17],[9,43],[-153,79],[-68,26]],[[15105,15391],[-19,-69],[-35,-38],[50,-66],[-3,-62]],[[15098,15156],[75,4]],[[15173,15160],[194,-63]],[[15367,15097],[45,-45],[22,-70]],[[15434,14982],[36,-7],[75,31],[42,-23]],[[15587,14983],[3,-45],[65,-15]],[[15655,14923],[-113,-83]],[[15542,14840],[22,-84],[37,-47],[-1,-36],[145,-19]],[[15745,14654],[32,-8]],[[15777,14646],[75,-10]],[[15852,14636],[60,-34],[-20,-37],[14,-68],[-15,-68]],[[15891,14429],[-11,-117],[-40,-53],[-9,-94],[37,-7],[16,-45]],[[15884,14113],[11,-4]],[[15895,14109],[26,-73],[46,-50],[-33,-31],[22,-45],[130,-56]],[[16086,13854],[-63,7]],[[16023,13861],[-34,-37]],[[15989,13824],[10,-77],[-11,-48],[-48,-83]],[[15940,13616],[13,-9]],[[15953,13607],[74,-14],[118,-100],[10,-93]],[[16155,13400],[40,-30],[53,-148],[60,-12],[7,-103],[32,-17],[-9,-86],[41,-62]],[[16379,12942],[-56,-101]],[[16323,12841],[-94,-20],[-45,-100],[-83,-30],[77,-74],[-20,-82],[-44,-70],[-131,-46],[-20,15],[-124,-32]],[[15839,12402],[-63,-10],[-18,-51]],[[15758,12341],[-79,-8],[-21,54],[-72,-33],[-53,29]],[[15533,12383],[-105,-20]],[[15428,12363],[-36,-10],[-37,-62]],[[15355,12291],[66,-151]],[[15421,12140],[-78,16],[-285,-88],[-142,-69]],[[14916,11999],[-111,-55],[-68,-103]],[[14737,11841],[4,-77],[54,-60]],[[14795,11704],[2,-25]],[[14797,11679],[-96,-34],[-48,2]],[[14653,11647],[-23,6],[-4,81],[-198,23],[21,52],[-55,103],[-13,58]],[[14381,11970],[-39,45],[-28,-16]],[[14314,11999],[-47,71],[-64,-6],[-23,62]],[[14180,12126],[-22,-14]],[[14158,12112],[-116,37],[-51,4]],[[13991,12153],[-120,-16],[-49,-38],[-74,-28]],[[13748,12071],[-46,-8]],[[13702,12063],[-54,-20]],[[13648,12043],[-51,40],[-49,1],[-26,40],[-56,40]],[[13466,12164],[-91,12],[-40,-13],[-21,83]],[[13314,12246],[-22,85]],[[13292,12331],[-19,32]],[[13273,12363],[-44,57],[12,43],[-18,64]],[[13223,12527],[5,36],[-40,43],[-18,69]],[[13170,12675],[-15,29]],[[13155,12704],[45,-17],[39,32],[56,13]],[[13295,12732],[78,58]],[[13373,12790],[-4,4]],[[13369,12794],[20,84],[21,30]],[[13410,12908],[28,5]],[[13438,12913],[43,61],[27,92]],[[13508,13066],[-23,-34],[-92,-33]],[[13393,12999],[-90,-16],[-19,34]],[[13284,13017],[20,69]],[[13304,13086],[-68,76],[16,44]],[[13252,13206],[-9,98]],[[13243,13304],[16,22]],[[13259,13326],[-42,31]],[[13217,13357],[-4,35]],[[13213,13392],[-29,58],[13,96],[-72,3],[-18,44],[-74,-6],[-21,28]],[[13012,13615],[120,31],[-18,35],[59,81],[-24,63]],[[13149,13825],[-70,33]],[[13079,13858],[50,21]],[[13129,13879],[-17,94],[32,24]],[[13144,13997],[-21,54],[-55,21],[-38,61]],[[13030,14133],[52,40],[-11,83]],[[13071,14256],[-53,-5]],[[13018,14251],[-4,80],[-27,96],[18,63]],[[13005,14490],[86,18],[24,101],[28,32]],[[13143,14641],[29,-30]],[[13172,14611],[7,4]],[[13179,14615],[13,52],[38,10]],[[13230,14677],[13,69],[67,32]],[[13310,14778],[-16,67],[21,84]],[[13315,14929],[43,26]],[[6139,9569],[33,-29],[-36,-66]],[[6136,9474],[-41,35]],[[6095,9509],[44,60]],[[6395,9608],[-55,-26],[-30,-40]],[[6310,9542],[-24,18]],[[6286,9560],[-29,67],[9,42]],[[6266,9669],[108,29],[21,-90]],[[8665,12966],[-56,-69]],[[8609,12897],[-6,-62],[-43,-34],[-36,-61],[-96,-16]],[[8428,12724],[-108,-35]],[[8320,12689],[-90,-79]],[[8230,12610],[-56,-105],[-119,-20]],[[8055,12485],[-58,-27]],[[7997,12458],[-101,-61]],[[7896,12397],[25,-76]],[[7921,12321],[-48,-16]],[[7873,12305],[-52,-46],[-140,-64],[-81,-61],[-35,9],[-79,-27]],[[7486,12116],[23,-83]],[[7509,12033],[52,-128],[61,20],[84,-82],[59,-142],[-52,-40]],[[7713,11661],[57,-9]],[[7770,11652],[44,21],[49,-6]],[[7863,11667],[46,73],[70,37],[-4,44]],[[7975,11821],[58,-4]],[[8033,11817],[44,36]],[[8077,11853],[41,30],[97,21]],[[8215,11904],[-24,-82],[53,-78],[30,17],[51,-18]],[[8325,11743],[-16,-67],[-69,-65]],[[8240,11611],[-65,-32],[-72,40]],[[8103,11619],[-78,-47]],[[8025,11572],[-3,-52],[-70,-25]],[[7952,11495],[-26,-41],[25,-38],[51,-20]],[[8002,11396],[-42,-41]],[[7960,11355],[-122,-44]],[[7838,11311],[-102,62]],[[7736,11373],[-83,-27]],[[7653,11346],[-10,-52],[52,-81],[15,-85]],[[7710,11128],[44,-26],[117,32]],[[7871,11134],[14,10]],[[7885,11144],[104,22],[20,-51],[-21,-43],[10,-60],[43,-56]],[[8041,10956],[126,35]],[[8167,10991],[46,70],[-45,68],[-29,100],[74,32]],[[8213,11261],[93,40]],[[8306,11301],[67,48]],[[8373,11349],[42,44],[14,57],[79,109],[-41,79]],[[8467,11638],[6,18]],[[8473,11656],[86,19]],[[8559,11675],[33,-22],[73,62]],[[8665,11715],[89,-30]],[[8754,11685],[36,-41],[24,-96]],[[8814,11548],[19,31]],[[8833,11579],[82,11]],[[8915,11590],[78,14]],[[8993,11604],[16,-12]],[[9009,11592],[97,6],[26,-37]],[[9132,11561],[39,17]],[[9171,11578],[4,91]],[[9175,11669],[39,48],[69,-1]],[[9283,11716],[4,32]],[[9287,11748],[24,86],[-37,15]],[[9274,11849],[19,51]],[[9293,11900],[-66,82],[-81,6]],[[9146,11988],[9,66]],[[9155,12054],[-4,9]],[[9151,12063],[0,63],[-44,25]],[[9107,12151],[-26,43],[-50,-6],[-27,23]],[[9004,12211],[-28,35]],[[8976,12246],[-102,93]],[[8874,12339],[-1,46]],[[8873,12385],[-14,105]],[[8859,12490],[-54,69]],[[8805,12559],[-23,34]],[[8782,12593],[145,33]],[[8927,12626],[33,5]],[[8960,12631],[192,9]],[[9152,12640],[17,7]],[[9169,12647],[48,34],[88,15]],[[9305,12696],[44,40]],[[9349,12736],[19,-29],[67,-31],[60,-102],[69,-36],[43,-84],[30,6]],[[9637,12460],[31,-22],[70,7]],[[9738,12445],[43,70]],[[9781,12515],[51,6],[8,-50],[50,-41]],[[9890,12430],[91,96],[67,-53],[58,32]],[[10106,12505],[25,-48],[-9,-48]],[[10122,12409],[-9,-61],[67,-95]],[[10180,12253],[-6,-88]],[[10174,12165],[50,-55],[12,-58],[39,-71]],[[10275,11981],[19,-16]],[[10294,11965],[3,-56],[79,-69],[29,-58]],[[10405,11782],[12,-30],[-33,-56]],[[10384,11696],[-4,-69],[31,-81]],[[10411,11546],[46,-55],[-1,-68],[-19,-30]],[[10437,11393],[-71,-29]],[[10366,11364],[-25,-38],[1,-66],[58,-130],[7,-49]],[[10407,11081],[-9,-91]],[[10398,10990],[-26,-137],[-42,-69],[11,-54]],[[10341,10730],[35,-46],[36,-3],[17,-63],[41,-22],[13,-65],[31,-33],[-19,-37],[28,-62]],[[10523,10399],[112,40],[40,-49],[59,2]],[[10734,10392],[57,9]],[[10791,10401],[24,-22],[-42,-79],[22,-100]],[[10795,10200],[-46,-34],[12,-68],[-75,-57],[-3,-90],[-55,-33],[-34,-96],[-39,-16],[-55,17],[-133,-78],[-34,-86],[70,-31],[18,-29],[-55,-143],[-78,-35]],[[10288,9421],[-7,-112],[66,-59],[-12,-105],[14,-32]],[[10349,9113],[39,-83],[245,-138],[131,-120],[23,-43]],[[10787,8729],[11,-45]],[[10798,8684],[6,-23]],[[10804,8661],[73,-50],[33,-58],[58,-27]],[[10968,8526],[71,60],[125,54],[58,10],[84,-25]],[[11306,8625],[12,7]],[[11318,8632],[59,-13],[48,18]],[[11425,8637],[31,-16],[82,9]],[[11538,8630],[6,-30],[62,-83]],[[11606,8517],[61,-4],[45,-24]],[[11712,8489],[132,-123]],[[11844,8366],[66,33],[48,54],[26,-15],[95,50],[35,-19],[64,12],[75,-69],[64,7],[28,63]],[[12345,8482],[110,-48],[50,-75],[80,-9],[176,137]],[[12761,8487],[92,87],[146,17],[48,41]],[[13047,8632],[46,-18],[97,-116]],[[13190,8498],[5,-49],[99,71],[46,-29],[75,14],[52,-17],[13,-47]],[[13480,8441],[-14,-63],[-41,-27],[22,-52],[-11,-77],[48,-62],[-13,-124],[35,-30]],[[13506,8006],[-9,-52],[16,-56],[-15,-71],[-60,-26],[-143,15],[-13,26],[-69,17],[-12,-69],[4,-82],[55,-62],[11,-109]],[[13271,7537],[-61,8],[-101,-161],[-42,-8]],[[13067,7376],[-22,-69],[11,-87],[-48,-104],[10,-70],[-15,-51],[21,-27],[-77,-115],[-74,-62],[-106,-16],[-23,-53],[-54,-57],[-83,-20],[-22,-39],[-103,-40],[-34,-31]],[[12448,6535],[-91,-23],[-84,3],[-110,-28],[-28,7],[-84,-29]],[[12051,6465],[-55,26],[-1,60],[-132,3],[-131,44],[-63,75]],[[11669,6673],[-156,42],[-51,32],[-71,-19],[-107,30],[-67,45],[-79,6],[-100,-40]],[[11038,6769],[-61,-84],[-89,-26],[-37,-45],[33,-99],[22,-23],[-28,-92],[-77,-39],[-62,-2],[-40,-36],[-10,-42]],[[10689,6281],[-95,-57],[-22,-35],[-85,-36],[-25,8],[-77,-39],[-179,-6]],[[10206,6116],[-87,-61],[-18,-30],[-143,-69],[-160,-147]],[[9798,5809],[-129,-94]],[[9669,5715],[-42,-35]],[[9627,5680],[-67,-22],[-63,-60],[-39,17]],[[9458,5615],[-66,70],[-18,-1],[-343,166],[-30,-53],[-59,-14],[3,-66],[-64,-58],[9,-49],[-32,-33],[-58,48]],[[8800,5625],[-114,-39],[-41,9]],[[8645,5595],[-113,-31],[-28,-75],[144,-93]],[[8648,5396],[-79,-43],[-12,-51],[-66,-24],[-103,11],[-27,-44],[-83,3]],[[8278,5248],[-18,87],[-54,17],[-20,-44],[-42,-16]],[[8144,5292],[-26,43],[-76,11],[-28,27],[-56,-45],[-57,-3],[-84,-49],[-11,19],[-94,-92]],[[7712,5203],[-22,-5],[-103,-92]],[[7587,5106],[-117,-54],[-120,-22],[-39,65],[25,149],[-13,39]],[[7323,5283],[-61,-69],[-46,4],[-153,-72],[-11,-112],[10,-50],[-47,-62],[-107,-33],[-66,22]],[[6842,4911],[8,157],[-20,68],[60,39],[-21,26],[-55,-24],[-49,96],[6,40],[-79,1]],[[6692,5314],[5,40],[71,155],[12,107]],[[6780,5616],[-92,142],[11,56]],[[6699,5814],[55,-12],[94,24],[18,31],[7,91],[21,74],[-32,91],[-6,75],[66,9],[-3,41],[47,69],[24,132],[-15,166],[-47,67],[-1,60]],[[6927,6732],[47,17],[76,61]],[[7050,6810],[44,83],[75,90],[91,-45]],[[7260,6938],[51,18],[24,63],[44,20],[-32,71],[19,127],[-33,46],[6,121]],[[7339,7404],[49,5],[40,36],[-41,108],[122,0],[24,-52],[52,-36],[92,94]],[[7677,7559],[-39,-4],[16,116],[23,43]],[[7677,7714],[-15,43],[26,28],[-5,74],[-45,0],[-47,43],[-79,7],[19,109],[-119,36],[-103,4],[-50,51],[-12,108],[44,109],[-20,108],[-30,17]],[[7241,8451],[-25,59],[74,82],[-7,48]],[[7283,8640],[46,168],[51,47],[-6,77]],[[7374,8932],[47,31],[13,38],[-44,59],[-79,47],[-32,45]],[[7279,9152],[53,29],[4,40],[59,3],[38,-27],[41,-66],[70,37],[42,72]],[[7586,9240],[-32,79],[3,109]],[[7557,9428],[-23,24]],[[7534,9452],[-128,40],[-99,-12],[-93,12],[-49,-36]],[[7165,9456],[-131,70]],[[7034,9526],[-69,15],[-63,-6],[-11,38]],[[6891,9573],[-40,-11],[-30,-42]],[[6821,9520],[-27,35],[-67,5],[-25,-28]],[[6702,9532],[-41,10],[108,74],[-13,101]],[[6756,9717],[31,43]],[[6787,9760],[2,34],[-42,58]],[[6747,9852],[32,39],[-10,47]],[[6769,9938],[17,62],[-24,32]],[[6762,10032],[-60,26]],[[6702,10058],[61,0],[43,61],[42,26]],[[6848,10145],[39,52]],[[6887,10197],[28,46],[-61,58]],[[6854,10301],[-34,106],[-162,-78]],[[6658,10329],[-46,-25]],[[6612,10304],[-42,-22]],[[6570,10282],[-57,-32]],[[6513,10250],[-37,-10]],[[6476,10240],[-38,-11]],[[6438,10229],[-46,-12]],[[6392,10217],[-241,-58],[-226,7]],[[5925,10166],[-64,22]],[[5861,10188],[-78,210]],[[5783,10398],[33,56],[12,63]],[[5828,10517],[-3,28]],[[5825,10545],[21,87]],[[5846,10632],[89,72]],[[5935,10704],[66,11],[61,52]],[[6062,10767],[62,62]],[[6124,10829],[-44,67],[-18,72]],[[6062,10968],[54,46]],[[6116,11014],[-16,52]],[[6100,11066],[-46,66],[-100,28],[-89,76],[-65,-43],[-28,69]],[[5772,11262],[91,97]],[[5863,11359],[-14,19]],[[5849,11378],[-66,-19],[-96,0],[-61,-43],[-67,-16]],[[5559,11300],[-197,-38],[-45,-21]],[[5317,11241],[-52,24],[-63,-61],[-277,-162]],[[4925,11042],[21,138],[56,50],[28,126]],[[5030,11356],[13,60],[-147,279]],[[4896,11695],[59,67],[39,-159],[22,-31],[122,55],[53,50]],[[5191,11677],[120,95],[9,42]],[[5320,11814],[29,41]],[[5349,11855],[37,16]],[[5386,11871],[22,-54],[77,5],[36,-52],[40,10]],[[5561,11780],[57,106]],[[5618,11886],[8,33],[106,12]],[[5732,11931],[52,25]],[[5784,11956],[43,80]],[[5827,12036],[7,17]],[[5834,12053],[51,99]],[[5885,12152],[17,43]],[[5902,12195],[60,28],[-9,79]],[[5953,12302],[6,11]],[[5959,12313],[115,49]],[[6074,12362],[91,-88],[33,48],[60,-9],[86,26],[115,11]],[[6459,12350],[-7,60],[-35,16],[32,108]],[[6449,12534],[108,46]],[[6557,12580],[8,68]],[[6565,12648],[-37,81]],[[6528,12729],[60,8]],[[6588,12737],[170,12],[24,-55]],[[6782,12694],[23,10],[96,-15],[57,-23],[106,15],[96,-22]],[[7160,12659],[147,29]],[[7307,12688],[52,19],[79,75],[30,47]],[[7468,12829],[59,-2]],[[7527,12827],[8,2]],[[7535,12829],[157,77],[70,-15],[109,2],[53,-53]],[[7924,12840],[101,13]],[[8025,12853],[12,2]],[[8037,12855],[94,-16],[79,2]],[[8210,12841],[86,-39]],[[8296,12802],[52,10],[164,105],[153,49]],[[8665,12966],[-144,-10],[-22,13]],[[8499,12969],[36,95],[83,52],[13,36]],[[8631,13152],[43,-23],[67,-110]],[[8741,13019],[-20,-36],[-56,-17]],[[6817,9765],[10,-27]],[[6827,9738],[87,41]],[[6914,9779],[1,29]],[[6915,9808],[2,41],[-96,-41],[-4,-43]],[[9429,11659],[-40,24]],[[9389,11683],[-42,16],[-27,-25]],[[9320,11674],[-28,-68],[85,-15],[52,68]],[[10180,12253],[81,-16],[76,39],[146,43]],[[10483,12319],[81,11],[68,-50]],[[10632,12280],[61,-15],[151,149]],[[10844,12414],[-52,93],[-6,74]],[[10786,12581],[29,16],[94,-20],[72,13],[59,30]],[[11040,12620],[38,-71]],[[11078,12549],[49,-72]],[[11127,12477],[17,-45],[-10,-100]],[[11134,12332],[81,-16],[113,13],[65,22],[-36,73]],[[11357,12424],[89,-4],[30,41],[54,23]],[[11530,12484],[57,-22],[39,-65],[16,-62]],[[11642,12335],[87,-26]],[[11729,12309],[25,17]],[[11754,12326],[71,31]],[[11825,12357],[93,14],[79,36],[29,-63]],[[12026,12344],[-56,-31],[-74,-20],[52,-88],[34,-2]],[[11982,12203],[14,-5]],[[11996,12198],[9,-47]],[[12005,12151],[53,6],[91,59],[28,145]],[[12177,12361],[66,39]],[[12243,12400],[43,166]],[[12286,12566],[77,-6]],[[12363,12560],[-1,52],[46,61],[43,8]],[[12451,12681],[16,-38],[49,7],[89,-35],[57,-57]],[[12662,12558],[-15,-64],[23,-70]],[[12670,12424],[37,-40]],[[12707,12384],[48,-88]],[[12755,12296],[20,-133]],[[12775,12163],[5,-62],[43,-40]],[[12823,12061],[15,-68]],[[12838,11993],[2,-44],[47,-101]],[[12887,11848],[8,-87],[37,-75],[58,-63]],[[12990,11623],[0,-28]],[[12990,11595],[20,-60]],[[13010,11535],[69,-24]],[[13079,11511],[6,-29],[76,3],[82,-17]],[[13243,11468],[38,25]],[[13281,11493],[25,-82],[-8,-50]],[[13298,11361],[27,-15]],[[13325,11346],[55,6],[61,49],[35,5],[19,-128],[84,36]],[[13579,11314],[71,62],[72,-102],[-4,-110]],[[13718,11164],[-82,50]],[[13636,11214],[-21,47],[-51,-9],[-80,24],[-68,-61],[-88,-37]],[[13328,11178],[-62,-76],[13,-54],[-29,-48],[-93,-76]],[[13157,10924],[2,-21]],[[13159,10903],[5,-55],[70,-3]],[[13234,10845],[145,36]],[[13379,10881],[60,-17],[26,-41],[85,-64]],[[13550,10759],[14,-16]],[[13564,10743],[58,-41]],[[13622,10702],[142,-155],[66,-18]],[[13830,10529],[-1,-77],[-25,-65],[-44,-29],[-146,-42]],[[13614,10316],[-30,7]],[[13584,10323],[5,104],[-222,64],[-9,-118]],[[13358,10373],[-2,-36],[-161,21],[-58,-20]],[[13137,10338],[-4,41]],[[13133,10379],[-5,116],[-83,18],[-180,-251],[-86,86],[-26,-6]],[[12753,10342],[-105,-3],[-33,-32],[-59,28],[-98,-14]],[[12458,10321],[-86,-83],[-34,47]],[[12338,10285],[-49,5],[-55,-94],[56,-62]],[[12290,10134],[0,-26],[-141,-25],[-84,39],[-136,-67]],[[11929,10055],[-65,-46],[5,-40],[75,-109],[-42,-48]],[[11902,9812],[-121,-90],[-66,-105],[-61,-135],[-26,45],[12,80],[-75,31],[-48,-48]],[[11517,9590],[-17,-52],[-70,-32]],[[11430,9506],[-15,-64],[19,-83]],[[11434,9359],[-26,-146],[-45,-14],[-7,-68],[-57,-43],[-18,-62],[120,-231],[-3,-42],[-49,-34],[7,-47],[-38,-40]],[[13480,8441],[95,45],[37,-16],[98,41]],[[13710,8511],[-29,90],[-42,42],[18,37],[9,119],[-28,42],[24,30],[22,113],[18,37],[76,41],[16,36]],[[13794,9098],[-49,67],[-54,-1],[-39,31],[-47,-17]],[[13605,9178],[-22,44],[18,58],[-26,59],[68,60],[-70,122],[-7,55]],[[13566,9576],[89,34],[26,80],[27,1]],[[13708,9691],[64,-34],[68,29],[23,39],[65,7],[66,34]],[[13994,9766],[20,58],[-21,35],[65,15],[8,52],[-13,126],[29,20],[11,60],[33,18],[30,71]],[[14156,10221],[-15,51]],[[14141,10272],[61,26],[79,-26],[33,-27],[-11,-94],[18,-189],[81,2]],[[14402,9964],[92,-20]],[[14494,9944],[180,-45],[124,23],[42,-116],[-16,-52],[57,-72]],[[14881,9682],[46,9],[68,73]],[[14995,9764],[45,44],[66,8],[115,61],[42,-72],[72,-15],[32,48],[69,38],[104,-81],[-12,-73]],[[15528,9722],[46,-120],[65,47],[-2,-182],[29,-45]],[[15666,9422],[35,34],[13,50],[64,-3],[70,56],[45,5],[33,41],[45,6],[46,41],[92,52]],[[16109,9704],[46,-114],[53,-44],[21,-104],[-7,-102],[-51,-18],[-75,-59],[-79,-41],[-52,6],[-65,-87],[-137,-71]],[[15763,9070],[18,-57],[40,-46],[-25,-131]],[[15796,8836],[-18,-37],[-76,-24],[-64,-92],[-73,7],[-36,-28],[0,-101],[33,-100],[-88,-39],[4,-27],[-46,-121],[17,-53],[-73,-48],[-95,-25]],[[15281,8148],[-67,130],[-22,7],[-96,-73],[-19,-59],[26,-67],[-124,-105],[-95,-69],[-45,35],[-41,-14]],[[14798,7933],[-35,-62],[-66,-16],[-2,-126]],[[14695,7729],[27,-101],[-128,-116],[15,-67],[-35,-49],[36,-77],[54,-48],[49,-7],[17,-107],[-13,-64],[23,-79],[-17,-76]],[[14723,6938],[-66,-21],[-100,-88],[-94,1],[-23,85],[-63,15]],[[14377,6930],[-19,-25],[-112,-13],[-50,29],[-43,54],[-101,-24],[-84,49]],[[13968,7000],[-43,-18],[-54,-59],[-1,-84],[-41,-66],[33,-161]],[[13862,6612],[-76,-31],[-123,-24],[-28,-37]],[[13635,6520],[-68,65],[-38,-18],[-157,82],[-23,62],[-42,162],[-37,50],[-21,71],[22,87],[-12,36],[51,78],[26,133]],[[13336,7328],[-19,69],[17,98],[-20,36],[-43,6]],[[15421,12140],[71,-69],[106,-32],[544,84]],[[16142,12123],[62,-14],[71,9]],[[16275,12118],[46,-40]],[[16321,12078],[-31,-13],[-20,-105],[-39,8],[22,-128]],[[16253,11840],[46,-38]],[[16299,11802],[66,34],[68,-33],[52,-47]],[[16485,11756],[-70,-89],[-96,-177],[-36,-30]],[[16283,11460],[10,-52],[-22,-69],[47,-54]],[[16318,11285],[-24,-58],[9,-34],[-61,-30],[-28,-71],[111,-44],[-49,-33],[-26,-82],[-55,-89],[-16,-92]],[[16179,10752],[-148,-92],[-35,16],[-68,-18],[11,-86],[-46,-49]],[[15893,10523],[83,-33],[4,-67],[37,-53],[57,-39],[49,-89],[-30,-35],[73,-122],[17,14]],[[16183,10099],[82,-104],[-10,-64],[-37,-75],[42,-100],[-80,-10],[-71,-42]],[[14141,10272],[-153,-86],[-127,2],[-123,-28],[-170,64],[16,99]],[[13718,11164],[151,-82]],[[13869,11082],[102,17],[28,18],[100,-14]],[[14099,11103],[1,-89]],[[14100,11014],[75,25]],[[14175,11039],[51,8]],[[14226,11047],[46,44],[66,6]],[[14338,11097],[22,25],[139,-14],[41,21],[24,61]],[[14564,11190],[56,61],[0,30],[119,86],[30,93]],[[14769,11460],[6,75],[69,42]],[[14844,11577],[-29,99],[-18,3]],[[13190,8498],[115,56],[54,-10],[-42,99],[-45,6],[-75,43],[-62,66],[14,73],[-75,30],[19,91],[-33,64],[-13,115],[7,77],[-48,27],[46,43],[18,56],[38,-4],[25,-65],[-53,-67],[51,-27],[77,82],[77,43],[29,-38],[2,-96],[63,-25],[45,26],[181,15]],[[12290,10134],[63,3],[69,51],[104,-10],[51,-22],[104,14],[33,-15]],[[12714,10155],[20,-55]],[[12734,10100],[-27,-107]],[[12707,9993],[21,-30],[-60,-46],[-57,-102]],[[12611,9815],[71,-33],[47,-43],[68,7],[84,32]],[[12881,9778],[16,-25],[-14,-63],[26,-29],[-35,-76],[5,-125]],[[12879,9460],[-51,-98],[30,-85],[-31,-171],[38,-146],[-72,-65],[24,-46],[89,-86],[11,-37],[76,-33],[54,-61]],[[16485,11756],[21,-8]],[[16506,11748],[248,-276],[67,-62],[29,-4]],[[16850,11406],[144,23]],[[16994,11429],[202,-7],[62,-25],[229,-42],[-6,-57]],[[17481,11298],[-14,-156],[25,-56],[-11,-45],[16,-101],[-51,-116],[-74,-68],[-82,-20],[-83,-67]],[[17207,10669],[98,-73],[65,-29],[66,-5],[115,75],[99,-17]],[[17650,10620],[55,-60],[33,-14],[35,-76],[0,-62],[28,-53]],[[17801,10355],[-24,-147],[16,-29],[-17,-100],[13,-69],[-49,-27],[-2,-79],[21,-47],[51,-28],[19,-68]],[[17829,9761],[-23,-89]],[[17806,9672],[-50,-65],[-71,-21],[-47,-88],[-26,-23]],[[17612,9475],[-48,-72],[-86,-54],[-59,21],[-132,6],[21,-48]],[[17308,9328],[-56,-22],[-33,-76],[-157,-92],[-35,22],[-7,53],[-48,64],[1,29],[-112,33],[-46,-4]],[[16815,9335],[-65,-61],[-41,-5],[-10,-90],[9,-43],[-45,-41],[-95,-225],[-59,-50],[-277,-55]],[[16232,8765],[-160,-64]],[[16072,8701],[-70,22],[-3,97]],[[15999,8820],[-43,-9],[-46,34],[-114,-9]],[[13281,11493],[17,39],[-17,63],[12,31]],[[13293,11626],[4,61],[-29,86],[-38,11],[-13,56]],[[13217,11840],[29,102],[-30,104],[2,55],[-22,37]],[[13196,12138],[-5,61],[57,72],[11,49]],[[13259,12320],[14,43]],[[4418,8335],[71,-30]],[[4489,8305],[-10,-99]],[[4479,8206],[-74,-120],[-44,-27],[-19,63],[-56,54]],[[4286,8176],[105,103],[27,56]],[[5007,8322],[-84,-136]],[[4923,8186],[-25,-68]],[[4898,8118],[-84,23],[-43,41],[-54,23],[-60,-18]],[[4657,8187],[-32,-42]],[[4625,8145],[-28,-8]],[[4597,8137],[-71,-11],[-25,19],[28,63]],[[4529,8208],[100,12]],[[4629,8220],[99,135],[-7,16],[83,77],[25,82],[101,-124],[50,-35],[27,-49]],[[4456,9156],[464,462]],[[4920,9618],[139,-185]],[[5059,9433],[65,-59],[39,9]],[[5163,9383],[55,-75],[-158,-52],[24,-29],[90,37],[93,-98]],[[5267,9166],[-58,-54],[-45,-66],[-32,-92]],[[5132,8954],[31,-123],[-19,-62]],[[5144,8769],[21,-14],[45,59],[64,-41],[-55,-71],[-25,-78]],[[5194,8624],[-30,-58]],[[5164,8566],[-25,28]],[[5139,8594],[-7,-14],[-138,-50],[-86,46],[1,27]],[[4909,8603],[-26,29],[-41,-17]],[[4842,8615],[-16,-40],[-46,-1],[-37,-51],[-42,-11],[-36,36]],[[4665,8548],[-30,14]],[[4635,8562],[-47,51],[-35,-23]],[[4553,8590],[-84,82]],[[4469,8672],[-35,-26]],[[4434,8646],[61,80],[-57,24]],[[4438,8750],[-51,51]],[[4387,8801],[-135,151],[204,204]],[[6139,9569],[-7,34]],[[6132,9603],[-32,44]],[[6100,9647],[-132,190]],[[5968,9837],[0,123]],[[5968,9960],[-9,111],[28,47],[-62,48]],[[6927,6732],[-75,-49],[-86,-121],[-35,72],[-47,40],[-51,-64],[-63,-48]],[[6570,6562],[-65,-45],[-58,10]],[[6447,6527],[-20,-15],[-48,-150],[-62,-72],[-75,-36]],[[6242,6254],[-54,-64]],[[6188,6190],[-123,15],[-28,-19],[-62,7],[-179,-166],[-42,-60],[-18,-96]],[[5736,5871],[-130,-71],[-48,-6]],[[5558,5794],[44,86],[-66,85],[-11,114]],[[5525,6079],[14,12]],[[5539,6091],[-36,51],[-2,69],[-61,60]],[[5440,6271],[-63,45],[-90,-7],[-49,50],[-69,42],[-57,2],[-29,-47]],[[5083,6356],[9,105],[-78,-56]],[[5014,6405],[-9,-7]],[[5005,6398],[-127,-157]],[[4878,6241],[-56,4],[-60,52],[-21,40]],[[4741,6337],[-15,59]],[[4726,6396],[-30,-1]],[[4696,6395],[-36,35],[-30,78]],[[4630,6508],[70,96],[39,-4]],[[4739,6600],[121,31],[34,-87]],[[4894,6544],[29,80]],[[4923,6624],[10,11]],[[4933,6635],[106,131],[30,11]],[[5069,6777],[49,15]],[[5118,6792],[-25,77]],[[5093,6869],[-100,-29],[-41,20]],[[4952,6860],[-34,62],[-70,39]],[[4848,6961],[-53,3]],[[4795,6964],[-44,-57],[-32,15]],[[4719,6922],[-79,68]],[[4640,6990],[-13,-40],[-67,-53]],[[4560,6897],[-23,9]],[[4537,6906],[22,68]],[[4559,6974],[12,70]],[[4571,7044],[-17,63]],[[4554,7107],[20,6],[8,172],[-35,91],[35,93]],[[4582,7469],[-25,38]],[[4557,7507],[67,8],[39,-18]],[[4663,7497],[52,99],[56,19],[31,-14]],[[4802,7601],[51,-44]],[[4853,7557],[99,146]],[[4952,7703],[-39,23]],[[4913,7726],[89,112]],[[5002,7838],[46,31],[21,48]],[[5069,7917],[-6,22]],[[5063,7939],[31,48]],[[5094,7987],[35,46],[86,63]],[[5215,8096],[9,13]],[[5224,8109],[73,79]],[[5297,8188],[30,95],[-59,73],[-53,-23],[-54,17]],[[5161,8350],[-12,35],[33,43]],[[5182,8428],[30,-23],[48,60]],[[5260,8465],[15,64],[39,51]],[[5314,8580],[61,9],[46,61]],[[5421,8650],[-14,26],[58,75],[-48,70],[4,72]],[[5421,8893],[38,64],[33,-31],[73,-17],[39,68]],[[5604,8977],[-35,35],[14,71],[-60,53],[17,64],[-3,67]],[[5537,9267],[49,58],[-16,23]],[[5570,9348],[-67,90],[-62,-26]],[[5441,9412],[-23,-22],[64,-116]],[[5482,9274],[-26,-16]],[[5456,9258],[-75,104],[-50,50],[-18,44]],[[5313,9456],[-68,25]],[[5245,9481],[20,56],[-42,47]],[[5223,9584],[-139,162]],[[5084,9746],[171,226]],[[5255,9972],[181,-206]],[[5436,9766],[57,-56],[34,20],[51,-42],[-31,-37],[12,-47]],[[5559,9604],[137,-152]],[[5696,9452],[-79,-88]],[[5617,9364],[70,-40],[47,61],[102,-59],[-29,-60],[53,-45],[-53,-63],[40,-59],[30,11],[39,62]],[[5916,9172],[67,101],[22,14]],[[6005,9287],[22,39],[-47,83],[97,84],[-29,11]],[[6048,9504],[47,5]],[[7922,13972],[22,-25],[68,24]],[[8012,13971],[58,-69],[34,-98],[-40,-90]],[[8064,13714],[-50,-14],[-58,13],[-70,-28],[-209,25]],[[7677,13710],[28,145]],[[8631,13152],[-83,40]],[[8548,13192],[25,45],[-10,35]],[[8563,13272],[-96,41],[-16,57]],[[8451,13370],[-133,-12]],[[8318,13358],[-194,1]],[[8124,13359],[-26,21]],[[8098,13380],[-73,56],[-22,-17]],[[8003,13419],[4,14]],[[8007,13433],[37,74],[-13,95],[65,24],[36,-26]],[[8132,13600],[24,17],[69,-11],[9,-93],[65,-34]],[[8299,13479],[8,-3]],[[8307,13476],[50,-5],[50,40],[35,-8]],[[8442,13503],[63,101]],[[8505,13604],[13,31],[-27,58]],[[8491,13693],[93,17],[199,5]],[[8783,13715],[-66,64],[3,113]],[[8720,13892],[38,2],[50,-66],[39,33],[87,1]],[[8934,13862],[41,-1]],[[8975,13861],[-3,40]],[[8972,13901],[6,61],[53,144]],[[9031,14106],[-36,36],[-77,33]],[[8918,14175],[16,91],[-8,43]],[[8926,14309],[-2,13]],[[8924,14322],[56,-5],[68,-36],[63,-15]],[[9111,14266],[77,34],[-25,63]],[[9163,14363],[84,18]],[[9247,14381],[12,-82]],[[9259,14299],[8,-10]],[[9267,14289],[162,-39],[56,-92]],[[9485,14158],[-55,-58],[-8,-53]],[[9422,14047],[-41,-22]],[[9381,14025],[-33,-41],[-12,-73],[12,-84]],[[9348,13827],[-28,-95],[-27,-17]],[[9293,13715],[-60,-40]],[[9233,13675],[-167,3]],[[9066,13678],[24,-194],[25,-46]],[[9115,13438],[1,-17]],[[9116,13421],[12,-91]],[[9128,13330],[235,33]],[[9363,13363],[69,8]],[[9432,13371],[149,-28],[6,-15]],[[9587,13328],[28,-75],[94,-108]],[[9709,13145],[80,-13],[15,-32],[70,6],[32,-16]],[[9906,13090],[30,16]],[[9936,13106],[23,132],[46,53]],[[10005,13291],[86,7],[62,34]],[[10153,13332],[32,54]],[[10185,13386],[125,64],[88,13]],[[10398,13463],[2,37],[-55,59],[57,51]],[[10402,13610],[48,45]],[[10450,13655],[66,-52],[48,-20]],[[10564,13583],[78,29],[68,49]],[[10710,13661],[21,49]],[[10731,13710],[88,39]],[[10819,13749],[26,25]],[[10845,13774],[-53,88],[-27,85]],[[10765,13947],[59,65],[-5,32]],[[10819,14044],[61,64],[32,-28]],[[10912,14080],[5,-62],[38,-46]],[[10955,13972],[28,-146]],[[10983,13826],[-132,-47]],[[10851,13779],[62,-25],[43,-48],[135,-52],[18,-98],[87,4]],[[11196,13560],[5,-20]],[[11201,13540],[18,-81],[-22,-96]],[[11197,13363],[-14,-29]],[[11183,13334],[-7,-23]],[[11176,13311],[-21,-6]],[[11155,13305],[-46,-59],[8,-57]],[[11117,13189],[-43,-36],[18,-49],[-34,-37]],[[11058,13067],[-50,-18]],[[11008,13049],[-89,-51],[5,-30],[-68,-15]],[[10856,12953],[-83,-25],[-2,122]],[[10771,13050],[-90,3]],[[10681,13053],[-71,19]],[[10610,13072],[-12,10]],[[10598,13082],[-49,40],[-54,-113],[9,-59],[-34,-34]],[[10470,12916],[-63,-39]],[[10407,12877],[-36,3],[-65,-39],[-61,-136]],[[10245,12705],[-7,-10]],[[10238,12695],[-46,-118],[-69,-61]],[[10123,12516],[-17,-11]],[[8408,14428],[45,-46]],[[8453,14382],[68,4]],[[8521,14386],[26,-101]],[[8547,14285],[-41,-57]],[[8506,14228],[-9,-33],[18,-103]],[[8515,14092],[-89,-4],[-30,-17],[-75,8]],[[8321,14079],[-56,-19]],[[8265,14060],[-118,-47]],[[8147,14013],[-44,-15]],[[8103,13998],[-52,98]],[[9074,14903],[-8,2]],[[9066,14905],[-80,-22],[23,-73],[-3,-60]],[[9006,14750],[-7,-25]],[[8999,14725],[-32,-2],[-107,-156]],[[8860,14567],[-29,8]],[[8831,14575],[-4,59],[-26,23]],[[8801,14657],[16,91],[-119,26],[-51,-8]],[[8647,14766],[19,69],[31,35],[-50,61]],[[7677,13710],[-68,5]],[[7609,13715],[-32,3]],[[7577,13718],[5,24],[-76,53],[-22,40],[-77,21]],[[7407,13856],[3,63]],[[9505,14727],[111,-76]],[[9616,14651],[14,-24],[70,-20]],[[9700,14607],[4,4]],[[9704,14611],[58,-1],[149,-45],[5,-117]],[[9916,14448],[16,-12]],[[9932,14436],[-10,-18]],[[9922,14418],[-12,-65]],[[9910,14353],[52,49]],[[9962,14402],[49,-18]],[[10011,14384],[40,62],[93,81]],[[10144,14527],[6,154]],[[10150,14681],[65,-11],[40,23]],[[10255,14693],[26,-17],[29,-71]],[[10310,14605],[7,-41],[69,27]],[[10386,14591],[8,-71],[31,-44],[55,-23]],[[10480,14453],[7,-28]],[[10487,14425],[50,-82]],[[10537,14343],[5,-59]],[[10542,14284],[150,-28],[71,31]],[[10763,14287],[-34,-187]],[[10729,14100],[47,13],[47,-40],[-4,-29]],[[8007,13433],[-136,6],[11,72]],[[7882,13511],[5,36],[-84,22],[-17,54],[-39,-3],[-25,76],[-45,14]],[[15783,15661],[-64,3]],[[15719,15664],[-28,-45],[-36,8]],[[15655,15627],[-32,23],[50,59],[-69,31],[-26,56]],[[15578,15796],[-109,53],[-65,3]],[[15404,15852],[-33,-16]],[[15371,15836],[-16,-5]],[[14989,15970],[-55,-16]],[[14934,15954],[-36,44],[4,39],[-39,100],[-39,4]],[[14824,16141],[-61,-42],[-48,36],[-62,-42],[43,-25],[15,-56],[-71,-97]],[[19801,13897],[-70,-2],[-180,-24],[-30,40]],[[19521,13911],[-40,-40]],[[19481,13871],[-1,-19]],[[19480,13852],[90,-48],[43,-43],[-1,-86]],[[19612,13675],[0,-7]],[[19612,13668],[-86,-65]],[[19526,13603],[-63,-35]],[[19463,13568],[-70,94],[108,3],[61,-10],[-8,47],[-72,42],[-22,68],[-42,25]],[[19418,13837],[-111,-53]],[[19307,13784],[-17,-15],[22,-88],[-90,-66]],[[19222,13615],[-10,-63]],[[19212,13552],[-7,-14]],[[19205,13538],[-28,-72],[3,-72],[-22,-108],[19,-61]],[[19177,13225],[3,-56]],[[19180,13169],[-85,23],[-44,-25]],[[19051,13167],[-72,-2],[-164,108]],[[18815,13273],[-27,85]],[[18788,13358],[-46,-23],[-55,8],[-51,42],[-7,31]],[[18629,13416],[-60,-2],[-101,53],[-70,8],[-46,-16]],[[18352,13459],[46,-42],[-10,-52],[46,-106],[-73,-28]],[[18361,13231],[-43,-23],[-2,-60]],[[18316,13148],[-52,-32],[-25,-81],[-95,-47],[31,-73],[0,-112],[-19,-94],[54,-62]],[[18210,12647],[104,-273]],[[18314,12374],[-60,0],[-46,-21]],[[18208,12353],[-78,63],[-1,35]],[[18129,12451],[-62,-12],[-71,67],[-56,35],[-94,-54],[-33,21],[-92,-2],[-88,42],[-58,46]],[[17575,12594],[58,65],[41,68],[-40,55],[4,66]],[[17638,12848],[-26,96],[-18,11]],[[17594,12955],[128,89]],[[17722,13044],[-46,85],[-100,-26],[-42,69]],[[17534,13172],[-16,46]],[[17518,13218],[-22,73]],[[17496,13291],[89,-4],[72,93]],[[17657,13380],[-14,92]],[[17643,13472],[72,25],[2,83]],[[17717,13580],[23,-15],[78,36],[39,-15],[73,18],[41,-23],[80,-10]],[[18051,13571],[51,-15],[88,21],[31,23]],[[18221,13600],[76,-6]],[[18297,13594],[77,30],[20,-10],[70,44],[82,-13],[30,-38],[96,9],[21,23],[70,15]],[[18763,13654],[38,55],[11,58],[-14,68],[14,41]],[[18812,13876],[91,-28]],[[18903,13848],[87,57]],[[18990,13905],[129,28],[90,62],[83,18]],[[19292,14013],[39,23],[-26,46]],[[19305,14082],[19,40]],[[19324,14122],[16,24]],[[19340,14146],[58,8],[14,-39]],[[19412,14115],[38,0]],[[19450,14115],[49,-10]],[[19499,14105],[79,1]],[[19578,14106],[68,-30]],[[19646,14076],[-2,-11]],[[19644,14065],[44,-11],[78,-57],[99,-42],[1,-35]],[[19866,13920],[-20,-7]],[[19846,13913],[-45,-16]],[[19180,13169],[-1,-81]],[[19179,13088],[-24,-152],[-31,-47],[19,-56]],[[19143,12833],[-38,-31],[-8,-111],[-15,-39]],[[19082,12652],[-128,-159],[-51,-30],[-76,-78],[-70,-31],[-54,-4],[-67,-71]],[[18636,12279],[-110,-30],[-63,49]],[[18463,12298],[-36,29],[-74,9],[-39,38]],[[19463,13568],[-42,-28],[-27,57],[-182,-45]],[[19801,13897],[-58,-45],[-102,-50],[6,-34],[83,-57]],[[19730,13711],[-118,-36]],[[19034,10771],[9,-19]],[[19043,10752],[41,-73]],[[19084,10679],[35,-55]],[[19119,10624],[82,-124],[18,-61]],[[19219,10439],[36,-69],[156,-207]],[[19411,10163],[-24,-20],[-95,-14]],[[19292,10129],[-34,-34],[6,-102],[-76,-18],[-33,-71]],[[19155,9904],[-25,-58],[-39,-18],[-5,-41],[-43,-80],[5,-40],[51,-27],[-68,-48],[-42,-77]],[[18989,9515],[-5,-88],[-67,-13]],[[18917,9414],[-1,-61],[-29,-33]],[[18887,9320],[-85,96],[-92,47],[-99,19],[-87,39],[-209,5]],[[18315,9526],[-24,31],[-90,-12],[-65,42],[-90,11],[-37,-16],[-60,16],[-47,-28]],[[17902,9570],[-56,39],[-40,63]],[[16155,13400],[116,72],[35,53]],[[16306,13525],[17,66],[-12,40],[24,51],[65,4],[28,152],[28,42]],[[16456,13880],[105,13],[18,40]],[[16579,13933],[31,-39],[56,38]],[[16666,13932],[50,23],[39,-9]],[[16755,13946],[44,26]],[[16799,13972],[-105,60]],[[16694,14032],[-86,31]],[[16608,14063],[6,30]],[[16614,14093],[9,45],[-113,49],[-3,55],[18,79]],[[16525,14321],[88,-6],[74,58]],[[16687,14373],[4,2]],[[16691,14375],[71,-70],[141,62]],[[16903,14367],[46,-103]],[[16949,14264],[45,25],[76,-8],[17,-40],[70,40]],[[17157,14281],[70,-1],[77,29],[53,130]],[[17357,14439],[18,-46],[46,-10]],[[17421,14383],[29,-47],[34,13],[93,-51],[13,-54]],[[17590,14244],[84,36],[68,-28]],[[17742,14252],[11,0]],[[17753,14252],[43,-41],[88,-10],[81,39],[15,29],[94,-47],[121,110]],[[18195,14332],[-20,77],[-56,16]],[[18119,14425],[55,52]],[[18174,14477],[-74,56],[-63,-73],[-109,10],[-14,32],[74,22],[-1,51],[61,16]],[[18048,14591],[100,39],[38,32],[52,-25],[58,28]],[[18296,14665],[-6,-60],[27,-36],[70,-3],[-92,-101]],[[18295,14465],[11,-29],[80,-26],[44,10]],[[18430,14420],[55,-16],[-35,-54]],[[18450,14350],[3,-45]],[[18453,14305],[-6,-22]],[[18447,14283],[28,9]],[[18475,14292],[43,4],[45,-40],[13,-40]],[[18576,14216],[42,13]],[[18618,14229],[38,127],[52,32]],[[18708,14388],[31,-7]],[[18739,14381],[20,38],[58,43]],[[18817,14462],[61,38],[323,310]],[[18888,14415],[3,-71]],[[18891,14344],[87,-35],[-8,-29]],[[18970,14280],[66,40]],[[19036,14320],[39,37],[-62,65],[-79,44],[-46,-51]],[[17322,3616],[-35,6],[-74,67]],[[17213,3689],[-41,42],[-3,38],[-66,97],[-42,37],[-118,60]],[[16943,3963],[-9,8]],[[16934,3971],[-44,163],[-19,36],[9,182],[-81,127]],[[16799,4479],[-48,132],[10,31]],[[16761,4642],[26,11],[51,84]],[[16838,4737],[28,55],[-62,102],[32,43],[-17,32],[16,58]],[[16835,5027],[10,0]],[[16845,5027],[93,49],[33,117],[4,73],[-43,128]],[[16932,5394],[48,90],[-31,54],[64,35],[-7,54],[-65,119],[3,38]],[[16944,5784],[-14,55],[23,39],[21,97],[-89,143]],[[16885,6118],[-27,18],[-71,-32],[-82,40],[6,44],[-40,40]],[[16671,6228],[6,19]],[[16677,6247],[-40,68],[-50,47],[-2,89],[-35,105],[34,47],[-20,79],[29,63],[11,86],[47,75],[11,55],[34,11]],[[16696,6972],[-5,44],[-119,117],[-24,0],[-55,63],[-57,6],[-64,-23],[-58,-50],[-59,-1],[-14,66],[18,60],[-30,99]],[[16229,7353],[-90,-95],[-95,36],[-85,-60],[-1,-77],[53,-77],[-6,-35],[-67,-63],[-147,-77],[-118,-40],[-61,-55],[-62,4],[-11,61],[-157,-66],[-84,35]],[[15298,6844],[-186,70]],[[15112,6914],[-63,-33],[-60,40],[-57,-48],[-66,21]],[[14866,6894],[-37,59],[-106,-15]],[[14678,15906],[42,22],[62,64],[64,-28]],[[14846,15964],[36,-20],[52,10]],[[13133,5908],[-14,62],[17,47],[38,19],[-35,62],[48,76],[-13,79],[47,-19],[100,2],[65,36],[61,-13],[48,100],[46,28],[94,133]],[[16283,1941],[45,-11]],[[16328,1930],[20,76],[-10,93]],[[16338,2099],[-2,3]],[[16336,2102],[-55,10],[-45,-157],[47,-14]],[[1543,5027],[-154,76]],[[1389,5103],[-59,15],[-23,44],[-55,0],[-40,36]],[[1212,5198],[-31,-18]],[[1181,5180],[-9,-62],[72,-43],[38,-45]],[[1282,5030],[75,-44]],[[1357,4986],[133,-73]],[[1425,4770],[-113,-222],[-153,80]],[[1159,4628],[-92,39],[-69,51]],[[998,4718],[-80,98]],[[2725,9148],[35,-31],[85,-10],[245,98]],[[3090,9205],[55,68],[136,70]],[[3281,9343],[132,69]],[[3413,9412],[57,-5],[46,26]],[[3516,9433],[45,18],[67,-2],[17,24]],[[3645,9473],[83,118],[9,36],[115,78],[284,129]],[[4136,9834],[14,-41],[-33,-124],[-4,-79],[23,-6],[103,-86],[-11,-83]],[[4228,9415],[8,-42]],[[4236,9373],[3,-59],[90,-26]],[[4329,9288],[127,-132]],[[4920,9618],[164,128]],[[6842,4911],[-113,-133]],[[6729,4778],[-61,-73]],[[6668,4705],[1,-72],[41,-47],[-57,-51],[11,-99],[-57,-18],[-33,-59],[-54,-59],[-36,-9],[-22,-68]],[[6462,4223],[-59,-52],[-53,-9]],[[6350,4162],[-30,0],[-12,-108],[-77,-96],[-65,-36],[-43,-46]],[[6123,3876],[-71,-17]],[[6052,3859],[-21,12]],[[6031,3871],[-207,-114]],[[5824,3757],[-55,102],[-53,48],[-19,122]],[[5697,4029],[-34,44]],[[5663,4073],[-5,36],[-72,118],[-12,59],[-41,41]],[[5533,4327],[-48,54]],[[5485,4381],[-32,42],[-15,88]],[[5438,4511],[-66,92]],[[5372,4603],[-72,60],[-23,208]],[[5277,4871],[-1,9]],[[5276,4880],[-12,40],[-56,68],[-97,-2],[-28,16]],[[5083,5002],[-77,126]],[[5006,5128],[27,76]],[[5033,5204],[-16,90],[-135,144]],[[4882,5438],[-205,267]],[[5255,9972],[402,272],[204,-56]],[[1143,5020],[40,33],[52,-6]],[[1235,5047],[-47,42],[-30,-39]],[[1158,5050],[-15,-30]],[[4882,11692],[14,3]],[[16029,1502],[48,24],[112,18]],[[16189,1544],[24,47],[-17,130]],[[16196,1721],[101,-88],[19,-117],[17,-40]],[[16333,1476],[27,57]],[[16360,1533],[85,-33],[83,95]],[[16528,1595],[12,51]],[[16540,1646],[24,14]],[[16007,1546],[64,43],[46,84],[48,47],[31,1]],[[16360,1533],[5,33],[-35,90],[-20,17]],[[16310,1673],[-50,80]],[[16260,1753],[8,124],[15,64]],[[16336,2102],[41,42],[112,44],[32,37],[71,23]],[[16582,2370],[-102,-96],[-57,-2],[-57,-33],[-40,0],[-43,43],[-27,-25],[-49,5],[-26,-72]],[[16181,2190],[43,-31],[2,-45]],[[16226,2114],[1,-67]],[[16227,2047],[-40,-86],[-1,-63],[28,-49],[-62,-10],[-35,-93]],[[16117,1746],[-48,-75]],[[16069,1671],[-78,-63],[-73,70],[-13,35],[32,108]],[[15937,1821],[5,56],[-25,77]],[[15917,1954],[4,86],[61,126]],[[15982,2166],[-25,37]],[[15957,2203],[-68,-57]],[[15889,2146],[0,-109]],[[15889,2037],[-52,-39],[20,-60],[-64,-28],[-33,17],[13,61],[-48,9]],[[15725,1997],[-23,-30]],[[15660,2854],[74,70]],[[15734,2924],[-8,46]],[[15726,2970],[67,216],[52,109],[29,18],[34,119],[217,-18]],[[16125,3414],[-50,-37],[50,-37],[57,-7],[15,-32],[50,24],[75,1]],[[16322,3326],[-19,-77],[79,-73]],[[16382,3176],[29,73],[64,51],[118,-38]],[[16593,3262],[40,-20],[44,12],[59,-22],[50,82]],[[16786,3314],[40,19]],[[16826,3333],[59,-109]],[[15139,3093],[38,23],[113,134],[67,37],[53,58],[85,37],[71,9],[111,66],[58,5]],[[15735,3462],[-54,25],[17,114]],[[15698,3601],[-1,31]],[[15697,3632],[-30,-47],[-52,64]],[[15615,3649],[-14,35],[-48,9],[-28,-30],[-91,-33]],[[15434,3630],[-45,-6]],[[15389,3624],[16,-61],[-11,-104],[-39,-7]],[[15355,3452],[-64,-31],[-94,73],[-143,-53]],[[15054,3441],[-37,-16],[-69,-108]],[[14948,3317],[-33,-103],[-88,-149]],[[13562,4224],[94,58],[97,9],[30,46],[72,0],[172,45],[26,35],[78,-8]],[[14131,4409],[120,-54],[31,8],[134,-39],[27,-64],[-1,-47],[122,-98],[50,21],[20,-26],[52,4],[27,-36],[108,-37],[18,-77],[32,-58]],[[14871,3906],[127,-10],[58,9]],[[15056,3905],[5,-35],[54,-33],[23,-55]],[[15138,3782],[79,1],[55,57],[53,-6]],[[15325,3834],[4,47]],[[15329,3881],[7,49]],[[15336,3930],[-9,16],[53,98],[-13,36]],[[15367,4080],[-14,32],[11,65],[-37,55],[-62,8],[-35,103]],[[15230,4343],[19,71],[-57,84],[-51,13],[-13,53],[-96,71],[-16,54],[-92,59],[-41,2],[-98,53],[-2,35],[-45,38]],[[14738,4876],[10,44],[40,28],[21,115],[102,52],[-10,40],[74,55],[51,87],[-21,114],[60,121]],[[15065,5532],[62,-4],[40,-54],[145,-41],[76,-42],[58,-5],[15,-31],[76,-45]],[[15537,5310],[10,-95],[65,-29]],[[15612,5186],[49,-92],[52,-20],[-14,-97],[-66,-85],[36,-48],[97,-4],[68,-79],[-1,-46],[81,20]],[[15914,4735],[61,-35],[7,-33],[-29,-63],[-7,-109],[45,-69],[0,-95],[59,-32]],[[16050,4299],[1,-51],[-40,-71],[77,-44]],[[16088,4133],[39,-2],[23,-48],[-37,-180],[-24,-59],[5,-81],[-27,-125],[15,-34],[-34,-37]],[[16048,3567],[-27,-6]],[[16021,3561],[9,-36]],[[16030,3525],[70,-17],[25,-94]],[[15298,6844],[-1,-57],[-82,-215],[-10,-73]],[[15205,6499],[58,-63],[118,39],[44,-34],[47,6],[214,-101],[33,-38]],[[15719,6308],[16,-30],[-21,-112],[44,-49],[-33,-49],[61,-150],[118,-151],[46,-82],[64,-69]],[[16014,5616],[62,-85]],[[16076,5531],[10,-67],[52,-71]],[[16138,5393],[30,-3],[11,-70],[29,-14]],[[16208,5306],[69,-37],[-38,-109],[-31,-35]],[[16208,5125],[-167,-173],[-57,-118],[-47,-23],[-23,-76]],[[15065,5532],[-21,174],[-66,20],[-15,36],[-77,56],[-9,100]],[[14877,5918],[29,48]],[[14906,5966],[-52,117],[-62,24]],[[14792,6107],[-55,-7],[-83,57],[-101,-3],[-75,70]],[[14478,6224],[-188,77],[-94,-8],[-31,-53],[-90,-9]],[[14075,6231],[-9,-105],[-51,-63],[-54,-34]],[[13961,6029],[-73,60],[-82,-64],[-63,2],[-71,-28],[2,-31],[-92,-32],[-31,-40]],[[16932,5394],[-78,7],[-64,39],[-95,16],[-60,29],[-99,4],[-71,-62],[-59,14],[-16,-51],[49,-34],[17,-52],[-60,-10],[-67,42],[0,60],[-84,14],[-50,-45],[13,-59]],[[16050,4299],[38,1],[5,50],[86,52],[60,5],[35,47],[53,24],[88,6],[-2,97],[29,24],[9,-134],[44,-20],[47,16],[-8,62],[22,35],[60,25],[84,64],[61,-11]],[[4592,5437],[128,-62],[104,16]],[[4824,5391],[53,-21],[5,68]],[[5533,4327],[-25,-30],[7,-89],[-10,-68],[-35,-30],[14,-47],[-51,-43],[-45,-98]],[[5388,3922],[-46,-17]],[[5342,3905],[-4,-49]],[[5338,3856],[-17,-39],[41,-121],[2,-53]],[[5364,3643],[-16,-5]],[[5348,3638],[-68,-33],[-60,-71],[-61,-35]],[[5159,3499],[-91,-39],[-51,-80],[-95,-11],[-48,22],[-86,-79]],[[13005,14490],[-90,16]],[[12915,14506],[-38,-33]],[[12877,14473],[-59,11],[-65,-26],[-116,26]],[[12637,14484],[-22,13]],[[12615,14497],[-36,31]],[[12579,14528],[-90,89],[29,61],[-36,47],[-2,46]],[[12480,14771],[-7,25]],[[12473,14796],[-120,-3],[-53,-20]],[[12300,14773],[-9,-29]],[[12291,14744],[-81,43],[-33,-5]],[[12177,14782],[-13,95]],[[12164,14877],[-85,12],[-61,38]],[[12018,14927],[-17,59]],[[12001,14986],[-88,104]],[[11913,15090],[-17,50],[17,31]],[[11117,13189],[88,-1],[86,-31]],[[11291,13157],[-22,-45],[12,-84]],[[11281,13028],[-48,4],[-58,-115],[22,-48]],[[11197,12869],[48,-18]],[[11245,12851],[-26,-81]],[[11219,12770],[94,-25],[41,16],[31,-27]],[[11385,12734],[26,-43]],[[11411,12691],[40,-48],[85,14],[10,-115]],[[11546,12542],[-23,-47]],[[11523,12495],[7,-11]],[[10862,14871],[-8,-57]],[[10854,14814],[-7,-73],[-39,-21],[-14,-50]],[[10794,14670],[23,-42]],[[10817,14628],[71,-1]],[[10888,14627],[22,-28],[54,-8]],[[10964,14591],[3,-57],[-32,-52]],[[10935,14482],[-52,-76],[-59,-13],[-45,-57]],[[10779,14336],[-16,-49]],[[12451,12681],[-21,86]],[[12430,12767],[90,17],[53,-32]],[[12573,12752],[-12,62]],[[12561,12814],[51,42],[-19,69],[15,43]],[[12608,12968],[-7,6]],[[12601,12974],[-63,84]],[[12538,13058],[62,15],[75,46],[13,39],[52,-32],[40,9]],[[12780,13135],[2,-62]],[[12782,13073],[46,-25],[66,-97]],[[12894,12951],[50,11]],[[12944,12962],[80,60]],[[13024,13022],[43,-36],[6,-56],[41,-32],[-6,-38]],[[13108,12860],[6,-31]],[[13114,12829],[-31,-34]],[[13083,12795],[72,-91]],[[12286,12566],[0,39]],[[12286,12605],[-74,282]],[[12212,12887],[-53,53],[-24,-27]],[[12135,12913],[-6,-90]],[[12129,12823],[-44,-18]],[[12085,12805],[-87,132]],[[11998,12937],[-12,30]],[[11986,12967],[-46,61]],[[11940,13028],[-48,26],[-48,-6]],[[11844,13048],[-18,18]],[[11826,13066],[-42,123],[8,55]],[[11792,13244],[-86,33],[15,94]],[[11721,13371],[-51,49]],[[11670,13420],[-40,85],[49,37]],[[11679,13542],[2,55]],[[11681,13597],[-15,41]],[[11666,13638],[-51,25],[0,33]],[[11615,13696],[81,-14],[193,38]],[[11889,13720],[31,18]],[[11920,13738],[11,43]],[[11931,13781],[-4,87],[17,73]],[[11944,13941],[42,-8]],[[11986,13933],[7,-51],[39,-18],[19,-51]],[[12051,13813],[17,-4]],[[12068,13809],[107,-24]],[[12175,13785],[75,-5],[39,35]],[[12289,13815],[37,-3]],[[12326,13812],[-76,-98],[-30,-20]],[[12220,13694],[31,-41],[64,-39]],[[12315,13614],[-3,-40]],[[12312,13574],[-31,-30]],[[12281,13544],[24,-112]],[[12305,13432],[-22,-21]],[[12283,13411],[47,-101],[-71,-136],[-61,-20]],[[12198,13154],[85,-67],[34,-81]],[[12317,13006],[-39,-111]],[[12278,12895],[44,-51]],[[12322,12844],[33,-42],[75,-35]],[[12018,14927],[-48,-70],[27,-112],[30,-27]],[[12027,14718],[-23,-41]],[[12004,14677],[-94,-29]],[[11910,14648],[-35,-28]],[[11875,14620],[-74,-29],[-90,12]],[[11711,14603],[-4,-62],[-49,8]],[[11658,14549],[-86,18]],[[11572,14567],[-66,-63]],[[11506,14504],[-44,-56],[-14,-70],[12,-57]],[[11460,14321],[60,-58],[36,-8]],[[11556,14255],[49,-65],[22,20]],[[11627,14210],[52,-58],[12,-68]],[[11691,14084],[-61,-28],[-35,10]],[[11595,14066],[-43,-9]],[[11552,14057],[-36,56],[-63,17]],[[11453,14130],[-168,1],[-76,-33]],[[11209,14098],[5,-82]],[[11214,14016],[21,-74],[-18,-55]],[[11217,13887],[-38,0],[-44,-40]],[[11135,13847],[-152,-21]],[[11281,13028],[11,-44]],[[11292,12984],[-5,-49]],[[11287,12935],[118,-54],[92,-18]],[[11497,12863],[24,34],[-6,68]],[[11515,12965],[3,69]],[[11518,13034],[28,31],[97,31],[10,-37]],[[11653,13059],[95,-11],[78,18]],[[12129,12823],[36,-187]],[[12165,12636],[78,-236]],[[12291,14744],[-22,-49],[45,-138]],[[12314,14557],[9,-84]],[[12323,14473],[1,-16]],[[12324,14457],[12,-106]],[[12336,14351],[14,-51],[-36,-49],[-47,-2],[46,-75]],[[12313,14174],[-4,-59],[-29,-28]],[[12280,14087],[108,-19],[58,-75]],[[12446,13993],[-49,-83],[19,-68]],[[12416,13842],[-19,-30],[-60,-24]],[[12337,13788],[-11,24]],[[11615,13696],[-30,88]],[[11585,13784],[-59,27],[-107,21]],[[11419,13832],[-6,23]],[[11413,13855],[-12,66],[68,16],[-3,51],[46,0],[2,56],[38,13]],[[12315,13614],[63,9],[27,27]],[[12405,13650],[68,-41]],[[12473,13609],[24,49]],[[12497,13658],[88,23]],[[12585,13681],[62,-89]],[[12647,13592],[25,-41],[39,-5],[6,-76]],[[12717,13470],[-10,-48]],[[12707,13422],[47,-29],[72,18]],[[12826,13411],[103,11]],[[12929,13422],[26,-27]],[[12955,13395],[68,61]],[[13023,13456],[12,-38]],[[13035,13418],[107,8],[75,-69]],[[5373,2660],[49,43],[42,118],[44,56],[88,43]],[[5596,2920],[5,1]],[[5601,2921],[44,90],[50,18],[39,47],[65,34]],[[5799,3110],[19,10]],[[5818,3120],[57,179],[48,61]],[[5923,3360],[71,-90],[22,-52]],[[6016,3218],[28,43],[-8,131],[87,134],[-33,39],[-12,60]],[[6078,3625],[20,44],[1,78],[-47,112]],[[6350,4162],[22,-57],[94,-124],[123,-70],[18,-35],[59,1],[59,-42],[55,-1],[74,-62],[-2,-83]],[[6852,3689],[35,-12],[32,33],[77,20]],[[6996,3730],[6,-8]],[[7002,3722],[56,-136],[79,-56],[91,-27],[15,-106],[-28,-60],[7,-56],[-22,-69],[-57,-36]],[[7143,3176],[15,-73],[-4,-76]],[[7154,3027],[-60,18],[-21,57],[-99,77],[-6,28],[-112,35]],[[6856,3242],[-65,-9],[-15,-25],[-102,-32],[-97,-14],[-38,23],[-79,-19]],[[6460,3166],[-62,-83],[-71,-45],[-182,-75]],[[6145,2963],[89,-14]],[[6234,2949],[-26,-60],[21,-146],[-10,-18]],[[6219,2725],[-29,5],[-83,-60],[-12,-56],[-92,-70],[-21,-54],[-55,-34]],[[5927,2456],[-65,-29]],[[5862,2427],[-1,-60],[-29,-67],[52,-77],[-32,-33],[37,-49],[-124,-58]],[[9458,5615],[9,-64],[-54,-99],[-43,-37],[20,-32]],[[9390,5383],[55,-41]],[[9445,5342],[29,-45],[-12,-71],[35,-56]],[[9497,5170],[36,9],[55,49],[30,-33],[114,14],[-3,-31]],[[9729,5178],[12,-48],[-35,-149],[2,-54],[-41,-75],[5,-132]],[[9672,4720],[-2,-33]],[[9670,4687],[-3,-125]],[[9667,4562],[-46,-107],[23,-99],[37,-43]],[[9681,4313],[25,-78],[-46,-117],[64,-13],[44,-63]],[[9768,4042],[-50,-67],[6,-71]],[[9724,3904],[-3,-8]],[[9721,3896],[-29,-39],[-63,-19],[-24,-145],[41,-55],[-33,-132],[20,-48],[-25,-66],[28,-20],[-10,-51],[52,-59]],[[9678,3262],[-25,-38],[-142,-24],[-21,-63]],[[9490,3137],[-45,40],[-68,24],[-29,61],[-59,64],[-18,77],[-43,70],[48,48],[7,65],[-68,67],[-20,62],[11,94],[19,26],[-10,171],[3,83],[-38,91]],[[9180,4180],[-57,41],[-2,29]],[[9121,4250],[-107,37],[-37,87],[-110,120],[-40,-3]],[[8827,4491],[-42,-12]],[[8785,4479],[-44,127],[-33,40],[-32,-7],[-7,92]],[[8669,4731],[-10,110],[-40,34]],[[8619,4875],[40,-7],[19,41],[52,14],[27,93]],[[8757,5016],[-26,28],[12,52],[-75,29],[12,95],[58,23]],[[8738,5243],[-4,46],[48,68]],[[8782,5357],[-91,-13],[-43,52]],[[8281,1882],[-38,63],[-71,174],[-67,53],[-15,-41],[-65,7],[-46,30]],[[7979,2168],[-77,18],[-27,-30],[-65,39],[-70,130],[-49,5],[-50,43],[14,68],[-31,52],[14,54],[-40,21],[-44,100],[6,65]],[[7560,2733],[-10,86],[68,101],[-45,117],[21,47],[-16,77],[-72,142],[34,37],[35,176],[37,24]],[[7612,3540],[22,47],[1,97]],[[7635,3684],[42,90],[26,112],[-8,112],[61,38],[70,22],[30,-12]],[[7856,4046],[33,65],[56,65]],[[7945,4176],[145,42],[31,-85],[93,-66],[60,-8],[36,-69],[95,-107],[113,74]],[[8518,3957],[-13,-84],[28,-52],[-59,-60],[-17,-124],[-23,-48]],[[8434,3589],[26,-67],[-30,-114]],[[8430,3408],[59,-46],[102,-166],[25,-76],[45,-34],[79,-109],[24,-53],[-21,-48],[71,-42],[31,-87],[47,-49],[60,-35],[-30,-43],[31,-101]],[[8953,2519],[-54,-192],[1,-71],[-69,-23],[-9,-29],[-69,-64]],[[8144,5292],[-104,-46],[-40,-51],[-73,-51],[106,-126],[46,-138],[-21,-89],[-83,-148],[-1,-57],[-39,-101],[-1,-59]],[[7934,4426],[-66,-82],[28,-41]],[[7896,4303],[-28,12],[-55,-32]],[[7813,4283],[-35,17],[8,77],[-26,26]],[[7760,4403],[19,111],[-33,175],[44,37],[-60,66],[-12,124],[-88,68],[-43,18]],[[7587,5002],[14,69],[-14,35]],[[12163,4723],[-35,44],[-77,205],[-64,47],[14,132],[-58,24],[-55,61]],[[11888,5236],[-18,5],[-31,89],[-79,10],[-39,-34],[-96,3]],[[11625,5309],[-2,57]],[[11623,5366],[-5,52],[-82,65],[23,37],[-81,63],[16,43]],[[11494,5626],[64,87]],[[11558,5713],[4,26],[-40,66],[-149,140],[-56,-19],[-79,119],[-71,71],[-97,-20],[-63,-74]],[[11007,6022],[-100,24],[-42,-10]],[[10865,6036],[-11,60],[-125,64],[-40,121]],[[7154,3027],[52,-58],[64,-39],[47,-51],[52,13],[68,-46],[11,-73],[39,-25],[73,-15]],[[6729,4778],[67,27],[64,-54],[270,27],[72,-18],[76,10],[85,-85],[43,-14],[25,-43],[-2,-116],[-42,-67],[-32,-123],[-36,-79],[13,-117]],[[7332,4126],[7,-59],[-43,-52],[39,-71]],[[7335,3944],[31,-42],[103,51],[24,-51]],[[7493,3902],[-38,-22],[-28,-77],[97,6]],[[7524,3809],[37,-18],[74,-107]],[[11238,3058],[-84,121],[-15,81]],[[11139,3260],[-23,16]],[[11116,3276],[4,56],[-28,44],[-9,109],[-15,32],[3,86]],[[11071,3603],[-41,38],[-2,55]],[[11028,3696],[-7,50]],[[11021,3746],[-21,82],[1,66],[-46,61],[4,77],[-15,70],[0,103],[25,55],[-19,54],[10,44]],[[10960,4358],[-26,26],[-91,35],[-40,-58],[-58,131],[12,46],[-45,20],[-36,-23]],[[10676,4535],[-26,57],[0,74],[-75,1]],[[10575,4667],[-8,32]],[[10567,4699],[-31,1]],[[10536,4700],[-102,-7],[-37,22]],[[10397,4715],[-60,10],[17,36],[44,27],[7,31]],[[10405,4819],[-4,167]],[[10401,4986],[32,10]],[[10433,4996],[-6,71],[76,36],[-1,93],[62,28],[22,112],[-35,38],[19,104],[-12,105],[-82,56]],[[10476,5639],[69,47],[106,122],[90,70],[74,127],[50,31]],[[11007,6022],[168,-71],[135,-113],[39,-16],[74,-74],[-39,-54],[-38,-110]],[[11346,5584],[-78,-44],[-52,-56],[-99,-66],[-53,-91],[2,-48]],[[11066,5279],[-35,-102],[3,-129],[26,-85],[62,-46]],[[11122,4917],[8,28],[67,74]],[[11197,5019],[5,16]],[[11202,5035],[81,-38],[143,-37],[73,-76],[50,25]],[[11549,4909],[17,-62],[45,-59]],[[11611,4788],[44,8],[78,-34],[82,-73]],[[13508,13066],[6,2]],[[13514,13068],[34,38],[-35,61],[35,24],[42,-15]],[[13590,13176],[29,44]],[[13619,13220],[66,-8]],[[13685,13212],[63,-68],[19,-63],[-7,-112],[13,-20]],[[13773,12949],[38,-93],[-19,-76],[18,-98]],[[13810,12682],[-17,-12]],[[13793,12670],[25,-24],[-16,-69],[-24,-10]],[[13778,12567],[79,-54],[73,-3],[19,-39]],[[13949,12471],[65,-106],[53,-45],[62,-124],[63,-47],[-12,-23]],[[18297,13594],[17,-17],[38,-118]],[[19222,13615],[-57,4],[-80,65],[-46,13],[-27,45]],[[19012,13742],[-53,-4]],[[18959,13738],[-56,110]],[[14916,11999],[47,-4]],[[14963,11995],[101,60],[161,-35],[20,17],[83,-11],[75,-33]],[[15403,11993],[-30,-86],[15,-87],[-43,-61]],[[15345,11759],[-110,-1]],[[15235,11758],[-54,40],[-44,-87],[-65,-23],[-64,13],[-26,-78],[-41,-40],[-97,-6]],[[13830,10529],[33,50],[157,-46]],[[14020,10533],[60,-98]],[[14080,10435],[-59,-91],[44,-71]],[[14065,10273],[-119,-17],[-96,45],[-114,-27],[-122,42]],[[15345,11759],[35,-43],[52,-23],[11,-59],[49,-27],[25,-79],[85,-31],[3,-84]],[[15605,11413],[32,-57],[-32,-57],[88,-52],[10,-50],[53,-31]],[[15756,11166],[-6,-55],[98,-154],[-6,-35]],[[15842,10922],[-21,17],[-62,-25],[-89,10],[-31,36],[-76,30],[-92,9],[-58,-33],[-77,-10],[-98,20],[-38,24],[-31,-85]],[[15169,10915],[-42,-44],[-47,-3]],[[15080,10868],[-39,144],[-76,19],[-4,35],[-85,25]],[[14876,11091],[-28,72],[7,57],[48,78],[-19,104],[-29,37],[-86,21]],[[15281,11601],[-25,-31],[-64,-6],[-37,-73],[31,-12],[60,-121],[3,-49],[45,-40],[-12,-72],[64,-38],[29,-58],[73,-41],[33,38],[-91,59],[-35,113],[5,78],[-50,162],[13,65],[-42,26]],[[14156,10221],[84,37],[44,-42],[-30,-65],[14,-69],[-50,-190]],[[14218,9892],[-28,-108],[88,-82],[-7,-22]],[[14271,9680],[-11,-48]],[[14260,9632],[-3,-46],[54,-132],[57,76]],[[14368,9530],[44,12],[16,53],[-22,38],[15,48],[-60,85]],[[14361,9766],[-10,23],[47,106],[4,69]],[[13564,10743],[54,54],[61,16],[-15,111],[32,78]],[[13696,11002],[87,-57],[66,-89],[54,71],[-34,155]],[[15842,10922],[-38,-44],[61,-78],[-52,-90],[17,-56],[-57,-49],[-5,-35]],[[15768,10570],[92,-56],[33,9]],[[14402,9964],[-54,167],[7,82]],[[14355,10213],[-17,76],[-83,28],[-30,41],[-78,-17],[-82,-68]],[[12734,10100],[-40,-10],[-101,-74],[-9,-30],[65,-27],[58,34]],[[12366,9389],[-7,67],[-35,-4],[-79,-113],[-130,-73],[-36,-86]],[[12079,9180],[7,-20]],[[12086,9160],[67,-50],[73,112],[124,100],[16,67]],[[15940,13616],[-60,5],[-53,65],[-30,-19],[-93,3],[-41,39],[65,18],[-122,131]],[[15606,13858],[-5,60],[-56,2]],[[15545,13920],[-1,-30],[-81,-13],[-68,-33]],[[15395,13844],[-53,77],[-48,6],[29,82],[-92,37]],[[15231,14046],[-68,40]],[[15163,14086],[-20,59],[-43,-50],[-67,16],[-78,-26],[-66,-85]],[[14889,14000],[-17,135],[-73,8]],[[14799,14143],[-41,1],[-66,-57]],[[14692,14087],[-38,12],[-74,78]],[[14580,14177],[18,25],[-22,81],[50,42]],[[14626,14325],[-14,61]],[[14612,14386],[-109,29],[-46,43]],[[14457,14458],[-55,63],[-8,60]],[[14394,14581],[-64,14]],[[14330,14595],[34,98]],[[14364,14693],[47,18],[74,-82]],[[14485,14629],[42,75],[56,45],[-34,100],[13,30]],[[14562,14879],[61,32],[39,0]],[[14662,14911],[31,-15],[35,-79],[42,15]],[[14770,14832],[22,7],[8,80],[27,-4]],[[14827,14915],[4,36]],[[14831,14951],[22,39]],[[14853,14990],[33,21],[2,55],[75,-60],[60,13]],[[15023,15019],[41,-39]],[[15064,14980],[44,33],[21,-35],[79,-27]],[[15208,14951],[-16,59],[-19,150]],[[14613,13756],[61,-42]],[[14674,13714],[14,7]],[[14688,13721],[58,-25],[151,-119],[108,-31]],[[15005,13546],[-34,-51],[23,-44]],[[14994,13451],[64,-34],[9,-46],[45,-19],[25,-49]],[[15137,13303],[-21,-36],[30,-80]],[[15146,13187],[-18,-33],[37,-34],[-25,-46]],[[15140,13074],[41,-42]],[[15181,13032],[1,-29]],[[15182,13003],[-34,5]],[[15148,13008],[-88,-49]],[[15060,12959],[-22,-73],[-41,-32],[62,-41],[-4,-50],[-60,-16]],[[14995,12747],[43,-51]],[[15038,12696],[-2,-31]],[[15036,12665],[-71,22],[-57,-34],[-33,-61],[-69,-6]],[[14806,12586],[-55,71]],[[14751,12657],[-38,32],[-54,89],[-26,15]],[[14633,12793],[-32,58]],[[14601,12851],[-8,19]],[[14593,12870],[23,27],[-30,104],[-39,26]],[[14547,13027],[-1,53],[-81,61],[-26,48]],[[14439,13189],[-8,37]],[[14431,13226],[-17,45]],[[14414,13271],[-21,56]],[[14393,13327],[-63,15],[-89,122],[-29,65],[65,38],[0,74]],[[14277,13641],[55,-14],[67,28]],[[14399,13655],[54,9]],[[14453,13664],[-36,128],[33,44]],[[14450,13836],[163,-80]],[[14608,13254],[60,-84],[35,-13],[39,-62],[46,-127],[35,-9]],[[14823,12959],[17,38]],[[14840,12997],[40,47],[-85,68],[-19,103]],[[14776,13215],[-42,74],[-39,24],[-40,57]],[[14655,13370],[-54,3]],[[14601,13373],[-18,-74],[25,-45]],[[16155,13400],[-44,-35],[-26,-55],[-58,-48],[-41,0],[-23,-76]],[[15963,13186],[-66,42],[-38,-27],[-92,43],[-28,43],[-53,26]],[[15686,13313],[-19,-86],[-33,-6],[-60,-61],[-80,-14]],[[15494,13146],[41,-39]],[[15535,13107],[-60,-20],[-96,29],[-24,-10],[-89,77]],[[15266,13183],[-44,40],[-76,-36]],[[14613,13756],[-15,64],[16,64]],[[14614,13884],[25,53],[-1,46],[84,42],[-30,62]],[[15355,12291],[-44,-38],[-71,0]],[[15240,12253],[-205,-11],[-115,49],[-43,55]],[[14877,12346],[-95,45],[-64,49]],[[14718,12440],[-58,27]],[[14660,12467],[-40,24],[-109,25],[-124,78],[-40,50]],[[14347,12644],[-63,89]],[[14284,12733],[-61,108]],[[14223,12841],[-41,60],[-44,141]],[[14138,13042],[-44,113]],[[14094,13155],[87,25],[45,33],[87,-16],[12,22],[106,7]],[[15036,12665],[17,-104],[37,-37]],[[15090,12524],[108,20],[77,-38]],[[15275,12506],[22,-66],[59,-72],[72,-5]],[[13773,12949],[112,32],[67,48]],[[13952,13029],[43,48]],[[13995,13077],[71,-163]],[[14066,12914],[32,-105]],[[14098,12809],[100,-163]],[[14198,12646],[40,-98]],[[14238,12548],[30,-58],[181,-92]],[[14449,12398],[42,-21],[33,20],[51,-20],[54,-87],[164,-145]],[[14793,12145],[55,-37],[68,-109]],[[13562,14993],[25,-58],[-68,-56]],[[13519,14879],[24,-19],[66,4],[97,-48],[51,-61]],[[13757,14755],[-51,-117]],[[13706,14638],[17,-62]],[[13723,14576],[-26,-90],[-44,-22],[34,-75]],[[13687,14389],[47,20],[80,6],[46,-42]],[[13860,14373],[20,14]],[[13880,14387],[28,-53],[62,-26]],[[13970,14308],[64,-153]],[[14034,14155],[17,-19]],[[14051,14136],[-51,-94],[3,-61],[49,-58]],[[14052,13923],[23,-46],[-19,-32]],[[14056,13845],[-160,19],[-82,-7],[-50,22]],[[13764,13879],[-86,-31],[25,-48],[-71,-41]],[[13632,13759],[-79,-10]],[[13553,13749],[-50,19]],[[13503,13768],[-46,57]],[[13457,13825],[-143,49]],[[13314,13874],[-12,2]],[[13302,13876],[-67,19]],[[13235,13895],[-28,-18],[-78,2]],[[14122,14914],[106,-59],[54,-63]],[[14282,14792],[21,-52],[61,-47]],[[14277,13641],[-132,54],[7,60]],[[14152,13755],[-53,-32],[-48,42],[5,80]],[[11201,13540],[-56,-82]],[[11145,13458],[-60,26],[-31,-54]],[[11054,13430],[-33,-110]],[[11021,13320],[-15,-24],[-110,37]],[[10896,13333],[-32,-4]],[[10864,13329],[6,-88],[-40,-35]],[[10830,13206],[-51,45],[-46,12]],[[10733,13263],[-66,-10]],[[10667,13253],[-58,93]],[[10609,13346],[-51,-62],[-14,-43],[-95,47],[-63,-27]],[[10386,13261],[-73,11]],[[10313,13272],[-78,-4]],[[10235,13268],[-17,-3]],[[10218,13265],[-62,43],[-3,24]],[[13524,16266],[84,7],[42,-34]],[[13650,16239],[37,-149],[-23,-26],[39,-53]],[[13703,16011],[-57,-26]],[[13646,15985],[-1,-24],[-88,-64]],[[13557,15897],[125,-67],[48,-69],[122,19],[50,-72]],[[15784,15672],[-65,-8]],[[14114,16662],[21,-136],[-6,-66]],[[14129,16460],[-39,-52],[-70,7]],[[14020,16415],[-30,-62]],[[13990,16353],[-12,-24],[70,-36]],[[14048,16293],[-54,-92]],[[13994,16201],[-46,-78],[-6,-61],[-41,5]],[[13901,16067],[-62,3]],[[13839,16070],[-95,152],[4,29],[65,40],[41,-17],[32,35]],[[13886,16309],[-72,53],[-28,-2],[-62,-75],[-74,-46]],[[14477,16517],[31,-35],[-35,-47],[-15,-61]],[[14458,16374],[44,-35]],[[14502,16339],[10,-72],[26,-31],[54,51],[30,-15],[51,50],[56,13]],[[14729,16335],[15,-31],[63,-36],[62,-62]],[[14869,16206],[-53,3],[8,-68]],[[14011,15762],[-11,55],[16,35],[-32,40],[14,60]],[[13998,15952],[90,16],[27,84],[-34,100],[-87,49]],[[9297,14679],[-25,-95],[2,-127],[-27,-76]],[[8918,14175],[-18,-75]],[[8900,14100],[-27,-117],[7,-20]],[[8880,13963],[-78,37],[-33,-41]],[[8769,13959],[1,45],[-111,55],[-15,30]],[[8644,14089],[-28,23],[-101,-20]],[[8975,13861],[9,-52],[-94,-85]],[[8890,13724],[31,-25],[98,8],[47,-29]],[[9128,13330],[-5,-71],[-29,-36],[-154,-70],[-36,-30],[-97,-36],[-43,-32]],[[8764,13055],[-23,-36]],[[8419,11945],[-20,99],[39,32]],[[8438,12076],[-14,48],[61,5]],[[8485,12129],[96,10]],[[8581,12139],[51,-69]],[[8632,12070],[-40,-80]],[[8592,11990],[-37,-33],[-40,22],[-30,-26],[-66,-8]],[[15403,11993],[56,-15],[18,-41],[75,0],[62,-29]],[[15614,11908],[38,18],[4,68],[55,19]],[[15711,12013],[134,34],[51,-33],[75,21]],[[15971,12035],[168,64],[3,24]],[[7921,12321],[70,25],[54,38],[49,9]],[[8094,12393],[113,33],[59,-31],[98,15],[72,31]],[[8436,12441],[14,21]],[[8450,12462],[72,73],[148,24]],[[8670,12559],[51,8]],[[8721,12567],[61,26]],[[9004,12211],[-31,-40],[-50,39],[-69,7]],[[8854,12217],[-29,-14]],[[8825,12203],[-65,-33],[-30,-40]],[[8730,12130],[-37,-16]],[[8693,12114],[-41,-3],[-20,-41]],[[8419,11945],[-14,1]],[[8405,11946],[-38,3],[-56,63],[-43,-17]],[[8268,11995],[2,-70],[-55,-21]],[[8405,11946],[21,-56],[-17,-55],[20,-30]],[[8429,11805],[-23,-33]],[[8406,11772],[67,-116]],[[4795,6964],[-2,64],[29,66]],[[4822,7094],[10,24]],[[4832,7118],[102,96],[24,-17],[43,106]],[[5001,7303],[40,10]],[[5041,7313],[40,-7],[63,-76]],[[5144,7230],[80,30],[40,-29],[67,15]],[[5331,7246],[53,-126]],[[5384,7120],[59,27]],[[5443,7147],[96,-183],[106,-73]],[[5645,6891],[-77,-87],[-22,-63],[48,-28],[27,16],[82,-62]],[[5703,6667],[-53,-95]],[[5650,6572],[-14,-136],[25,-78],[-63,-48],[5,-78],[-26,-79],[-38,-62]],[[7339,7404],[-55,34],[82,92],[-75,53],[-64,92],[-33,-2],[-47,-73]],[[7147,7600],[-66,13],[-62,74]],[[7019,7687],[-43,46],[-2,45],[30,53],[-70,166],[-90,-56],[-27,-58]],[[6817,7883],[-126,45]],[[6691,7928],[75,62],[5,38],[44,31]],[[6815,8059],[-2,121],[9,54],[-52,23],[-76,92]],[[6694,8349],[-22,22]],[[6672,8371],[-32,64]],[[6640,8435],[16,67],[48,17],[21,42]],[[6725,8561],[-41,39],[20,60],[-98,-20],[-10,40]],[[6596,8680],[37,39],[-5,34]],[[6628,8753],[-3,52]],[[6625,8805],[-51,90]],[[6574,8895],[-16,49],[-114,56]],[[6444,9000],[-16,115]],[[6428,9115],[100,35],[35,73],[91,108],[8,64]],[[6662,9395],[27,11]],[[6689,9406],[38,-14]],[[6727,9392],[94,128]],[[6100,9647],[53,63]],[[6153,9710],[35,8]],[[6188,9718],[51,47]],[[6239,9765],[58,58]],[[6297,9823],[63,65],[0,27]],[[6360,9915],[-70,88]],[[6290,10003],[-50,-1],[-232,-117],[-40,-48]],[[6428,9115],[-78,-77],[-45,67],[-57,29]],[[6248,9134],[-77,-50]],[[6171,9084],[-61,42],[-67,-54]],[[6043,9072],[-34,42],[-65,23]],[[5944,9137],[-28,35]],[[6691,7928],[-4,-6]],[[6687,7922],[-55,16],[-21,41],[-177,68],[-35,-18],[-59,14]],[[6340,8043],[-36,-4],[-44,46]],[[6260,8085],[-9,-26]],[[6251,8059],[-100,-72]],[[6151,7987],[-66,3],[-15,-56],[-67,-34],[-29,-51],[-71,12],[-22,-54]],[[5881,7807],[-17,-15]],[[5864,7792],[-85,80],[-45,84],[-33,-11],[-34,105],[48,45],[-32,54]],[[5683,8149],[-2,33]],[[5681,8182],[-34,-11],[-54,47]],[[5593,8218],[-65,77],[11,36]],[[5539,8331],[-11,29]],[[5528,8360],[7,31]],[[5535,8391],[-29,49],[22,22],[9,71]],[[5537,8533],[60,66],[18,51],[96,44],[-25,85],[103,66]],[[5789,8845],[-17,28]],[[5772,8873],[14,29],[73,32],[40,33]],[[5899,8967],[27,28],[-5,77],[23,65]],[[5331,7246],[52,32],[-19,60],[14,37],[-36,68],[27,116],[35,-5],[48,47],[62,9],[36,-51],[48,0]],[[5598,7559],[47,15]],[[5645,7574],[9,5]],[[5654,7579],[60,45]],[[5714,7624],[5,62],[44,48]],[[5763,7734],[101,58]],[[6151,7987],[15,-40],[-22,-57],[58,-74],[-99,-86],[3,-55],[-36,-29]],[[6070,7646],[54,-46]],[[6124,7600],[34,-48],[-20,-104],[5,-47]],[[6143,7401],[-6,-63],[29,-114]],[[6166,7224],[35,-18]],[[6201,7206],[21,16],[-11,143],[-16,31]],[[6195,7396],[-29,167],[38,62]],[[6204,7625],[-23,75],[109,101]],[[6290,7801],[0,32]],[[6290,7833],[-80,51],[-37,82],[56,40],[22,53]],[[5421,8650],[57,-25],[11,-39],[48,-53]],[[4387,8801],[219,228]],[[4606,9029],[144,76],[24,49],[102,64],[183,215]],[[5223,9584],[71,60]],[[5294,9644],[35,19],[107,103]],[[13869,11082],[-99,141],[-17,56]],[[13753,11279],[-27,79],[56,100],[-5,84],[31,62],[24,114],[-116,74],[-61,-10],[-33,39]],[[13622,11821],[-66,-19],[-47,-42]],[[13509,11760],[9,-52]],[[13518,11708],[41,-90],[-31,-73],[5,-43],[39,-27],[-10,-77],[17,-84]],[[14289,11455],[-6,-58],[-30,-44],[53,-13]],[[14306,11340],[53,-25]],[[14359,11315],[54,-31]],[[14413,11284],[52,-99],[43,8],[17,88],[-70,120],[-56,28],[-30,-10],[-50,43],[-30,-7]],[[16994,11429],[44,-37],[159,-12],[177,-63],[107,-19]],[[12753,10342],[17,-68],[-14,-76],[90,11],[3,-33]],[[12849,10176],[-10,-41],[-105,-35]],[[12707,9993],[132,87]],[[12839,10080],[39,43],[-18,71],[65,19],[80,100],[34,65],[94,1]],[[13358,10373],[158,-12],[9,-50],[-76,-5],[-130,-61],[0,-45]],[[13319,10200],[39,-43],[80,-7],[36,-20]],[[13474,10130],[106,-4],[71,-61],[82,-17],[96,13],[66,41]],[[13895,10102],[130,35],[131,84]],[[10123,12516],[-75,106]],[[10048,12622],[44,50]],[[10092,12672],[98,68]],[[10190,12740],[-55,129],[-31,22],[-28,74]],[[10076,12965],[-10,19]],[[10066,12984],[-14,52],[-53,71],[-63,-1]],[[10476,5639],[-70,-48],[-105,-50],[-55,-54],[-5,-56]],[[10241,5431],[39,-50],[9,-112],[-34,-123],[19,-26]],[[10274,5120],[58,-57],[26,-55],[43,-22]],[[10405,4819],[-16,-31],[-97,-15],[-91,-41]],[[10201,4732],[-10,-37]],[[10191,4695],[-28,-90],[4,-39]],[[10167,4566],[-20,-31],[28,-51],[56,-44]],[[10231,4440],[10,-31],[-70,-57],[-39,-10],[-99,-115],[-48,7],[-58,-21]],[[9927,4213],[11,-65]],[[9938,4148],[-31,-11],[-8,-83],[-82,-33],[-49,21]],[[9490,3137],[26,-75],[-51,-83],[-34,-108],[-53,-61],[-2,-42]],[[9376,2768],[-30,-79],[-45,-52],[10,-55],[-254,-120],[-59,65],[-45,-8]],[[7945,4176],[-17,25]],[[7928,4201],[-32,102]],[[1159,4628],[-14,-96],[20,-53],[-10,-46]],[[1155,4433],[-64,-123]],[[1091,4310],[-26,-76]],[[1065,4234],[-15,-144]],[[1050,4090],[19,-55],[-1,-135],[100,61]],[[1168,3961],[112,209]],[[1280,4170],[7,159],[25,51],[57,17]],[[1369,4397],[39,48]],[[1408,4445],[63,111],[43,39]],[[1514,4595],[33,63],[-4,53]],[[1389,5103],[-23,-40],[-9,-77]],[[6528,12729],[-54,-22],[-152,-5],[-2,19]],[[6320,12721],[19,48],[49,15]],[[6388,12784],[42,76],[-24,22],[66,90]],[[6472,12972],[53,64],[36,19],[65,99],[45,28]],[[6671,13182],[67,99],[48,47],[4,51]],[[6790,13379],[83,73]],[[6873,13452],[17,32],[-40,153],[71,48]],[[6921,13685],[-10,40],[62,36]],[[5831,13015],[48,-26],[9,-46],[54,-21],[59,38],[77,-67]],[[6078,12893],[93,0]],[[6171,12893],[47,77],[150,53],[7,53],[80,-19],[17,-85]],[[13685,13212],[-22,51],[-63,21],[-37,42],[32,59],[-120,50]],[[13475,13435],[56,17],[19,52],[53,35],[-34,45],[12,36]],[[13581,13620],[57,7],[-15,48],[-39,29],[48,55]],[[14094,13155],[-33,52],[-32,10],[-34,105],[-33,-17],[-5,-136],[38,-92]],[[9349,12736],[61,7],[29,39],[153,75]],[[9592,12857],[66,57],[18,39]],[[9676,12953],[16,38],[107,18],[36,19]],[[9835,13028],[42,18],[29,44]],[[9910,14353],[-21,-50],[38,-25]],[[9927,14278],[-35,-14]],[[9892,14264],[-46,-54]],[[9846,14210],[-10,-81],[19,-73]],[[9855,14056],[9,-54]],[[9864,14002],[-17,-47]],[[9847,13955],[-105,-24]],[[9742,13931],[-98,-150]],[[9644,13781],[-57,-16]],[[9587,13765],[-34,45]],[[9553,13810],[-22,-36],[-51,-11],[-63,-65]],[[9417,13698],[-49,24],[-75,-7]],[[10185,13386],[24,58],[-28,94]],[[10181,13538],[-68,81]],[[10113,13619],[9,143]],[[10122,13762],[7,23]],[[10129,13785],[-45,-12],[-56,46],[-58,-20],[-57,7]],[[9913,13806],[-11,20]],[[9902,13826],[-25,80],[-30,49]],[[16755,13946],[45,-87],[61,-11],[22,-155]],[[16883,13693],[137,7],[-6,-57],[99,0]],[[17113,13643],[123,-33]],[[17236,13610],[46,-68]],[[17282,13542],[64,-75],[50,16],[46,-37],[54,-155]],[[17518,13218],[16,-46]],[[18636,12279],[-5,-43]],[[18631,12236],[-65,-55],[23,-120],[19,-11],[-53,-98]],[[18555,11952],[-36,-15],[-30,-57],[-60,5],[-75,-29],[-151,-136],[-24,-120],[55,-63]],[[18234,11537],[-47,12],[-36,56],[-49,-9],[-85,9],[-115,-27],[-60,11],[-93,-31]],[[17749,11558],[-45,-31],[-54,4]],[[17650,11531],[24,87],[-28,28],[15,42],[48,27],[11,45],[-30,71]],[[17690,11831],[-94,-41],[-23,32],[-212,44],[-97,-43],[-102,-14],[-38,50]],[[17124,11859],[-47,107]],[[17077,11966],[-30,89],[-18,7]],[[17029,12062],[10,80],[-22,34],[-5,86],[-83,34],[1,47],[-71,33]],[[16859,12376],[-15,23],[-110,65],[-46,52]],[[16688,12516],[-7,55],[38,51],[0,50],[-53,3],[-65,31],[-31,63]],[[16570,12769],[-88,37],[-18,64],[-44,57],[-41,15]],[[18576,14216],[-68,30],[-33,46]],[[19577,14298],[34,-65],[-44,-60],[11,-67]],[[19324,14122],[-66,-15],[-147,-78],[-46,-13]],[[19065,14016],[-21,-26],[-176,-34],[-85,-35]],[[18783,13921],[-1,84]],[[18782,14005],[-23,19],[-127,-4]],[[18632,14020],[13,125],[39,68]],[[18684,14213],[-66,16]],[[18817,14462],[54,-9],[17,-38]],[[19036,14320],[35,-65]],[[19071,14255],[80,-22],[72,11]],[[19223,14244],[101,11]],[[19324,14255],[70,35],[16,41],[84,69]],[[17717,13580],[-70,29],[-23,85]],[[17624,13694],[21,119],[-35,49],[11,53],[51,58]],[[17672,13973],[74,-46],[41,96]],[[17787,14023],[-33,42],[22,59],[-39,72],[5,56]],[[19028,10886],[-49,-4],[-29,25],[-76,-18],[-50,21],[-39,-34]],[[18785,10876],[-84,128],[-66,8]],[[18635,11012],[-91,132]],[[18544,11144],[22,36],[58,10],[50,57]],[[18674,11247],[-49,80],[-28,86]],[[18597,11413],[-42,-1],[-24,40],[-141,-18],[-46,91],[-110,12]],[[19082,12652],[41,-40],[50,-9],[64,-55],[88,-12]],[[17481,11298],[107,-22],[45,15],[50,-30],[120,13],[39,24],[133,-25],[73,22]],[[18048,11295],[30,17],[8,84],[-123,10],[-31,-12],[-110,14]],[[17822,11408],[-200,-13],[-130,32]],[[17492,11427],[1,71],[105,36],[52,-3]],[[17492,11427],[-166,9],[-41,18],[-156,26],[-71,-24]],[[17058,11456],[-64,-27]],[[16142,12123],[29,49],[-156,5]],[[16015,12177],[-118,-56],[-73,-17],[-91,20],[-37,29],[-38,-46],[-93,2],[-49,44],[-50,4],[7,58],[-15,55],[-103,21]],[[13636,11214],[-63,-70],[33,-65],[90,-77]],[[13379,10881],[13,62],[35,48],[-16,24],[-92,-126],[-85,-44]],[[4882,5438],[42,-10],[55,18],[98,-9],[27,17],[105,-3]],[[5209,5451],[56,102]],[[5265,5553],[42,1],[207,189],[25,-6],[91,50],[74,11],[63,37]],[[5767,5835],[9,-21]],[[5776,5814],[-48,-53],[5,-92],[132,4]],[[5865,5673],[19,19],[122,6],[28,-20],[20,-61],[10,-98],[102,101],[116,-45],[48,-54],[60,-193]],[[6390,5328],[79,19],[141,-26],[39,-36],[43,29]],[[4418,8335],[20,49]],[[4438,8384],[30,79]],[[4468,8463],[35,31]],[[4503,8494],[50,96]],[[5059,9433],[56,56],[3,28],[105,67]],[[4554,7107],[-62,8],[-26,40],[13,46],[-27,36]],[[4452,7237],[-66,24]],[[4386,7261],[-43,-124],[-52,-100]],[[4291,7037],[-31,-16]],[[4260,7021],[-67,-52],[-86,29],[-31,61]],[[4076,7059],[6,37]],[[4082,7096],[27,70],[68,45],[54,89]],[[4231,7300],[13,128],[20,2],[50,78],[42,22]],[[4356,7530],[-15,36]],[[4341,7566],[16,55],[-69,102],[62,17],[20,-22],[74,55],[-10,48],[85,-10]],[[4519,7811],[72,83]],[[4591,7894],[-24,45]],[[4567,7939],[-50,-13],[-39,59],[119,152]],[[4529,8208],[-50,-2]],[[5436,9766],[98,78],[11,49]],[[5545,9893],[235,149],[60,79],[21,67]],[[5968,9837],[-56,-31],[-81,-78],[-2,-53],[38,-52]],[[5867,9623],[74,-40]],[[5941,9583],[53,-1],[106,65]],[[4076,7059],[-18,-103]],[[4058,6956],[-34,-2],[-98,85],[-48,24],[-38,51]],[[3840,7114],[-47,-13]],[[3793,7101],[-24,-42]],[[3769,7059],[0,-59],[21,-90]],[[3790,6910],[-93,-49],[-9,43],[-45,30]],[[3643,6934],[-5,3]],[[3638,6937],[-68,20]],[[3570,6957],[-13,76]],[[3557,7033],[-61,10],[-43,36]],[[3453,7079],[-52,-35]],[[3401,7044],[-28,-74]],[[3373,6970],[-23,-14]],[[3350,6956],[-4,-58]],[[3346,6898],[-114,-32],[-12,-37]],[[3220,6829],[0,-18]],[[3220,6811],[-97,-27],[-25,11]],[[3098,6795],[-29,51],[13,67]],[[3082,6913],[6,106]],[[3088,7019],[11,81]],[[3099,7100],[-35,-3]],[[3064,7097],[-40,124],[21,52]],[[3045,7273],[-37,20],[-3,69]],[[3005,7362],[-20,38]],[[2985,7400],[50,3],[12,50]],[[3047,7453],[125,-28]],[[3172,7425],[40,59],[36,7]],[[3248,7491],[74,121]],[[3322,7612],[50,-2]],[[3372,7610],[43,93],[-8,51]],[[3407,7754],[-6,15]],[[3401,7769],[7,71],[108,-6],[28,25],[81,17],[0,-36]],[[3625,7840],[31,84]],[[3656,7924],[-46,-10],[-28,55]],[[3582,7969],[24,62]],[[3606,8031],[42,0],[26,52],[7,151]],[[3681,8234],[22,39]],[[3703,8273],[81,-93]],[[3784,8180],[7,-32],[60,-61]],[[3851,8087],[-9,-62]],[[3842,8025],[51,-4]],[[3893,8021],[52,51],[24,52],[70,28]],[[4039,8152],[-12,-72],[49,-37]],[[4076,8043],[48,27],[61,-10]],[[4185,8060],[9,81]],[[4194,8141],[42,19],[31,50]],[[4267,8210],[19,-34]],[[4329,9288],[-17,-38],[-72,-48],[-53,-57],[-75,-35]],[[4112,9110],[-73,-91]],[[4039,9019],[-83,-49]],[[3956,8970],[-22,-44]],[[3934,8926],[-35,-29],[-94,-43],[-117,-100],[-10,-58]],[[3678,8696],[0,-5]],[[3678,8691],[65,-79],[63,4]],[[3806,8616],[49,12],[276,155]],[[4131,8783],[95,-10],[94,-25],[67,53]],[[3322,7612],[-55,22]],[[3267,7634],[-56,3],[0,142]],[[3211,7779],[-1,52]],[[3210,7831],[-140,-12],[-38,-67],[-120,-79]],[[2912,7673],[-36,19]],[[2876,7692],[-18,13]],[[2858,7705],[-56,-42]],[[2802,7663],[-30,9]],[[2772,7672],[-21,9]],[[2751,7681],[-6,2]],[[2745,7683],[-19,0],[-74,-73]],[[2652,7610],[-16,-1]],[[2636,7609],[-48,-41]],[[2588,7568],[-201,124]],[[2387,7692],[-39,-8]],[[2348,7684],[-123,-48],[-54,51],[-40,-21]],[[2131,7666],[-12,-45],[10,-120],[-18,-60],[14,-56]],[[2125,7385],[-69,-31],[-34,-54]],[[2022,7300],[-23,-40],[44,-19],[-199,-189],[10,-59]],[[1854,6993],[-1,-5]],[[1853,6988],[-31,-36]],[[1822,6952],[-25,2],[-71,-67],[-33,-101],[-24,-4]],[[1669,6782],[-53,-30],[-87,-98]],[[1529,6654],[-62,-113]],[[1467,6541],[-30,-54]],[[1437,6487],[-62,0]],[[1375,6487],[-92,60]],[[1283,6547],[-23,27],[-45,-18],[-43,11],[-88,96]],[[1616,7224],[74,73],[52,19],[214,201],[64,115],[-22,18]],[[1998,7650],[-47,-53],[-170,-125],[-158,-138]],[[1623,7334],[-81,-102],[16,-35],[58,27]],[[4058,6956],[78,-7],[-16,-64]],[[4120,6885],[-46,-67],[-61,-176]],[[4013,6642],[-39,12],[-35,-53],[-77,-43],[-89,-154],[-75,-20],[14,-52]],[[3712,6332],[-78,3],[-110,61],[-36,-3],[-52,49],[-27,-7]],[[3409,6435],[-11,26]],[[3398,6461],[26,59]],[[3424,6520],[66,-25]],[[3490,6495],[62,88],[-15,36],[12,83]],[[3549,6702],[-38,5]],[[3511,6707],[-6,-43],[-42,-21]],[[3463,6643],[-41,22]],[[3422,6665],[-14,52]],[[3408,6717],[-6,63],[35,102]],[[3437,6882],[-59,13],[-30,30],[25,45]],[[4696,6395],[-44,-53],[-49,-104],[4,-62],[-48,-5],[-2,-39]],[[4557,6132],[-1,-72]],[[4556,6060],[-66,18]],[[4490,6078],[-46,3]],[[4444,6081],[-95,46],[-55,57]],[[4294,6184],[-64,22],[-89,-28],[-107,83]],[[4034,6261],[-141,63]],[[3893,6324],[-44,3]],[[3849,6327],[-15,11],[-122,-6]],[[3098,6795],[-7,-13]],[[3091,6782],[41,-65]],[[3132,6717],[81,-64],[4,-20]],[[3217,6633],[5,-75],[-30,-19]],[[3192,6539],[-6,-88],[-20,-46]],[[3166,6405],[-11,-49]],[[3155,6356],[-92,58],[-51,2]],[[3012,6416],[-37,-2],[-50,-79],[-52,-28]],[[2873,6307],[-42,-85]],[[2831,6222],[-46,-1],[-42,-39],[-11,-65],[-34,5],[-34,-39],[-68,-26]],[[2596,6057],[-96,-26],[-72,-65],[-72,6]],[[2356,5972],[-66,35]],[[2290,6007],[-8,20]],[[2282,6027],[-33,10],[-24,76],[-25,2]],[[2200,6115],[-56,-29],[-31,50]],[[2113,6136],[-60,83]],[[2053,6219],[-14,12]],[[2039,6231],[-13,57]],[[2026,6288],[-76,1]],[[1950,6289],[-24,-59],[-54,-35],[-39,28],[-80,-53]],[[1753,6170],[-17,34]],[[1736,6204],[62,60]],[[1798,6264],[-50,41],[-25,53]],[[1723,6358],[60,38],[-5,41],[-46,19],[33,61],[-87,83],[-56,11],[-51,43],[-42,0]],[[2290,6007],[-151,-28]],[[2139,5979],[-34,-4],[-73,-91],[-63,-47]],[[1969,5837],[-57,-91]],[[1912,5746],[-68,-63]],[[1844,5683],[-32,-16],[-4,-67],[-47,-70],[3,-64],[-23,-19]],[[1741,5447],[-21,-40],[-82,18],[-57,-51]],[[1581,5374],[-7,-25],[-109,-136]],[[1465,5213],[-76,-110]],[[1357,4986],[-34,-107]],[[1323,4879],[-27,-68],[-44,-54]],[[1252,4757],[-30,-47]],[[1222,4710],[-63,-82]],[[3409,6435],[-34,3],[-83,-47],[-35,-35],[-102,0]],[[5265,5553],[-35,102]],[[5230,5655],[-31,64],[-45,7],[-27,67],[-129,26],[-18,32]],[[4980,5851],[-94,11],[-42,79]],[[4844,5941],[-89,31],[-20,55]],[[4735,6027],[-101,17]],[[4634,6044],[-78,16]],[[7509,12033],[-35,-31],[20,-96],[53,-28]],[[7547,11878],[-62,-54],[-61,11],[-141,-30]],[[7283,11805],[-53,-50]],[[7230,11755],[-1,33],[-115,-48],[-7,-24],[-88,0]],[[7019,11716],[-86,-52],[-57,-64]],[[6876,11600],[-87,-81],[-65,-11],[-46,-43]],[[6678,11465],[52,-29]],[[6730,11436],[-107,-77],[-35,13],[-106,-61],[-20,-42],[-59,-44]],[[6403,11225],[-13,-51],[25,-28],[-85,-77]],[[6330,11069],[40,-45]],[[6370,11024],[-100,-66],[-45,-79],[-101,-50]],[[6305,10797],[-12,19]],[[6293,10816],[76,61],[37,96],[27,-57],[-71,-80],[-57,-39]],[[7713,11661],[-30,3]],[[7683,11664],[-29,-50],[-34,22],[-77,-4],[30,-47],[-81,-2]],[[7492,11583],[-38,-75]],[[7454,11508],[-1,-25]],[[7453,11483],[30,-14],[17,-55]],[[7500,11414],[-14,-11]],[[7486,11403],[-46,-55],[7,-45]],[[7447,11303],[-59,-35]],[[7388,11268],[-27,27],[-60,3],[-39,37]],[[7262,11335],[-16,-45],[-74,-51]],[[7172,11239],[-11,-4]],[[7161,11235],[-66,-18]],[[7095,11217],[-64,-8],[-25,-78]],[[7006,11131],[-85,-32]],[[6921,11099],[-47,-6]],[[6874,11093],[-13,-62]],[[6861,11031],[1,-22]],[[6862,11009],[-81,-46]],[[6781,10963],[-56,57]],[[6725,11020],[17,65]],[[6742,11085],[6,30],[70,108],[17,67]],[[6835,11290],[45,65]],[[6880,11355],[36,54]],[[6916,11409],[-21,28],[-81,-57]],[[6814,11380],[-54,-60],[-109,-85],[-58,-71],[-106,-82]],[[6487,11082],[-117,-58]],[[6305,10797],[-12,19]],[[8041,10956],[-12,-51],[25,-39],[53,-1]],[[8107,10865],[17,-49]],[[8124,10816],[-20,-70],[-36,-21]],[[8068,10725],[-49,-87]],[[8019,10638],[-36,-27]],[[7983,10611],[-116,-8],[-10,-37]],[[7857,10566],[-36,-48]],[[7821,10518],[-54,-13]],[[7767,10505],[-24,28],[-68,-10],[-24,-24],[-76,-16]],[[7575,10483],[-46,-28],[-40,33],[-32,-37],[-119,-77]],[[7338,10374],[-58,17],[-72,-52],[13,-46],[-57,-47]],[[7164,10246],[7,-62],[-17,-36]],[[7154,10148],[-58,-38],[-56,42],[20,32]],[[7060,10184],[-30,-2]],[[7030,10182],[-30,34],[-39,-22],[-74,3]],[[5861,10188],[48,70],[-86,134],[-40,6]],[[6062,10767],[14,-34],[48,-4]],[[6124,10729],[65,4],[31,-61]],[[6220,10672],[64,-11],[113,47]],[[6397,10708],[49,31],[80,20],[62,60],[2,24]],[[6590,10843],[4,27]],[[6594,10870],[49,6],[82,144]],[[10341,10730],[-46,16],[-36,41],[-87,17]],[[10172,10804],[-100,5],[-39,-27],[-81,72]],[[9952,10854],[61,55],[-25,49],[-12,76],[55,82]],[[10031,11116],[-9,20]],[[10022,11136],[-42,25],[-80,-15]],[[9900,11146],[-64,-34],[-48,-54],[-31,61],[-103,-14],[-26,-47]],[[9628,11058],[-8,-1]],[[9620,11057],[34,110]],[[9654,11167],[1,41],[-57,5],[-2,60]],[[9596,11273],[-3,-3]],[[9593,11270],[-57,12],[-54,115],[-39,54],[-78,-24],[-22,-37],[-72,-13]],[[9271,11377],[-60,39],[-47,0],[-6,40]],[[9158,11456],[31,23],[-18,99]],[[10798,8684],[-53,10],[-295,-74],[-38,13],[-79,-20]],[[10333,8613],[-100,87],[-13,38],[-65,38],[-47,-5]],[[10108,8771],[-32,-12],[-127,5],[-103,-25]],[[9846,8739],[-66,51],[-45,83]],[[9735,8873],[-9,57],[-50,4],[-23,32],[-55,-19]],[[9598,8947],[18,48],[-69,-8],[-64,39]],[[9483,9026],[-1,65],[67,13],[-24,116],[-23,38]],[[9502,9258],[11,17],[-37,95],[99,82],[22,4]],[[9597,9456],[-11,91],[-41,55],[-55,30]],[[9490,9632],[-34,18],[6,95],[-45,57]],[[9417,9802],[0,49]],[[9417,9851],[1,11]],[[9418,9862],[39,68],[-16,73]],[[9441,10003],[4,76],[-76,14],[-20,36]],[[9349,10129],[-90,-46],[-64,-47]],[[9195,10036],[3,69],[25,36],[-23,57]],[[9200,10198],[-50,24],[-76,-18],[-27,40],[-66,-53],[-76,-26]],[[8905,10165],[-60,61]],[[8845,10226],[-67,10],[-23,49]],[[8755,10285],[-41,-42],[-55,12],[-31,31],[-30,-84]],[[8598,10202],[-45,-32],[-29,34],[-3,65],[35,69],[-12,39]],[[8544,10377],[4,6]],[[8548,10383],[31,39],[-13,115]],[[8566,10537],[-53,45]],[[8513,10582],[-31,72]],[[8482,10654],[37,108],[30,38]],[[8549,10800],[-3,46]],[[8546,10846],[33,97],[91,0],[42,-15]],[[8712,10928],[7,13]],[[8719,10941],[-28,64],[-50,53]],[[8641,11058],[-50,31],[-1,77],[-49,52]],[[8541,11218],[-21,60],[-40,56],[-35,-55],[-46,19],[-26,51]],[[9483,9026],[-157,-40],[-45,-23]],[[9281,8963],[-30,-31],[-70,-2],[-1,-53],[-60,-84]],[[9120,8793],[-110,-7]],[[9010,8786],[-9,-1]],[[9001,8785],[-45,25],[-29,-19]],[[8927,8791],[-77,-55]],[[8850,8736],[-52,-30]],[[8798,8706],[-67,25],[-11,61]],[[8720,8792],[-16,3]],[[8704,8795],[-59,-28],[30,-98]],[[8675,8669],[-19,-13]],[[8656,8656],[-79,74],[-79,-3]],[[8498,8727],[-3,27]],[[8495,8754],[-81,-11],[-66,10]],[[8348,8753],[-79,-15],[-48,-47],[84,-98],[3,-32]],[[8308,8561],[-35,-63],[-79,-8],[-63,-76],[7,-28],[70,-88],[11,-108]],[[8219,8190],[-56,-55]],[[8163,8135],[-62,-81],[50,-26],[-13,-54]],[[8138,7974],[-58,-15]],[[8080,7959],[-20,-47]],[[8060,7912],[-77,31],[-20,-33],[-50,-11],[-3,-37],[-61,-36]],[[7849,7826],[15,-42],[-36,-93],[-36,30],[-58,-38],[-57,31]],[[10333,8613],[4,-35],[-66,-47],[5,-54],[-14,-71]],[[10262,8406],[-88,-6],[-37,-20],[-52,20]],[[10085,8400],[-73,-80]],[[10012,8320],[-10,-75],[22,-43],[-125,-158],[-73,-38],[-34,-63],[-46,-23],[-48,-60],[-3,-37],[53,-23],[-10,-43]],[[9738,7757],[-61,26],[-21,86],[-96,104],[-188,74]],[[9372,8047],[-73,90],[-30,2],[-55,48]],[[9214,8187],[-117,110]],[[9097,8297],[-54,-49],[9,-34],[-8,-111]],[[9044,8103],[-34,-24],[60,-67],[-12,-21]],[[9058,7991],[6,-10]],[[9064,7981],[2,-136]],[[9066,7845],[0,-31],[-90,-74]],[[8976,7740],[-111,7],[-42,44],[-53,-4],[-25,42]],[[8745,7829],[-197,41]],[[8548,7870],[-27,57]],[[8521,7927],[-104,23],[-29,25],[-70,-15]],[[8318,7960],[-74,-11],[-55,26],[-51,-1]],[[7677,7559],[1,-97],[64,48],[78,24],[21,-57],[30,-12],[55,-92],[1,-107],[48,-25],[0,-57],[22,-93]],[[7997,7091],[-10,-7]],[[7987,7084],[115,-115],[-17,-31],[54,-36],[-2,-63],[-77,-53],[-35,-68],[10,-52],[53,-33],[-29,-56]],[[8059,6577],[197,-14],[16,-29],[71,-26],[52,-64],[88,-29]],[[8483,6415],[-40,-43],[-24,-58],[-73,-34],[-55,-7]],[[8291,6273],[12,-86],[-17,-61],[37,-115],[-20,-46],[71,-138],[127,-96],[49,-20],[95,-116]],[[9044,8103],[79,-48],[3,-43],[82,-66],[26,-52],[133,-39],[8,-32],[74,-48],[2,-69],[111,-46],[8,-16]],[[9570,7644],[106,-39]],[[9676,7605],[36,-101],[64,-76],[3,-62]],[[9779,7366],[57,-17],[26,-46],[74,-16]],[[9936,7287],[10,-53],[51,-33],[8,-36],[62,-86]],[[10067,7079],[-7,-28]],[[10060,7051],[47,-62],[1,-148],[-9,-66],[-95,-132],[25,-31],[16,-73],[33,-36],[2,-53],[38,-61],[-57,-110],[-36,-13]],[[10025,6266],[50,-60],[58,-11],[23,-57],[50,-22]],[[9676,7605],[75,-50],[67,3],[107,47]],[[9925,7605],[71,33],[60,-39],[60,37]],[[10116,7636],[-3,5]],[[10113,7641],[-6,51],[-136,97]],[[9971,7789],[-50,11],[-63,-25],[-89,-2],[-31,-16]],[[11039,8358],[-132,-83],[-21,-47],[-47,-18],[-96,-90]],[[10743,8120],[-130,-113]],[[10613,8007],[-98,-62],[-87,-75]],[[10428,7870],[20,-18]],[[10448,7852],[53,-27],[99,0],[122,84]],[[10722,7909],[65,70],[118,51],[109,88],[108,60]],[[11122,8178],[52,42],[71,25],[68,2],[-1,75],[-38,34],[-52,-9],[-86,35],[-97,-24]],[[18888,14415],[103,-44],[45,-51]],[[18048,14591],[-74,0],[-11,45],[-39,31],[-106,-5]],[[17818,14662],[17,25],[-37,62],[20,77],[54,11],[-7,108],[31,14]],[[17896,14959],[-8,17]],[[17888,14976],[3,34],[58,43]],[[17949,15053],[17,18],[103,-24]],[[18069,15047],[38,95],[-29,25],[35,43],[44,18]],[[18157,15228],[95,-81]],[[18252,15147],[140,-84]],[[18392,15063],[126,-72],[23,-28],[-22,-100]],[[18519,14863],[6,-30]],[[18525,14833],[28,-65],[102,-74]],[[18655,14694],[86,-95],[93,-47],[-17,-90]],[[15719,15664],[49,-26],[56,7],[70,-66]],[[15894,15579],[59,-3],[56,34],[32,-15],[48,46],[95,24]],[[16184,15665],[91,27],[90,90],[37,-6],[78,28]],[[16480,15804],[89,30],[25,-18],[72,11],[32,-16]],[[16698,15811],[-44,-16],[24,-63],[1,-70]],[[16679,15662],[-45,10]],[[16634,15672],[-3,-10]],[[16631,15662],[20,-54],[5,-91],[-91,-100],[7,-29]],[[16572,15388],[-4,-5]],[[16568,15383],[50,-107],[-45,-18],[-77,-126],[75,-31],[-10,-34]],[[16561,15067],[-34,-55],[-13,-110],[-44,-1]],[[16470,14901],[19,-121],[33,-28]],[[16522,14752],[3,-60]],[[16525,14692],[-96,-30],[-68,-7]],[[16361,14655],[-74,-45],[13,-70],[25,-23]],[[16325,14517],[-76,-7],[-45,-26],[-72,27]],[[16132,14511],[-46,-39]],[[16086,14472],[-101,39],[-25,131],[23,9]],[[15983,14651],[-29,84],[-80,-37],[-22,-62]],[[17445,15671],[53,-68],[75,-24]],[[17573,15579],[31,3],[42,-48]],[[17646,15534],[25,-31],[119,-27],[100,-66]],[[17890,15410],[89,-50]],[[17979,15360],[100,-54],[78,-78]],[[17888,14976],[-32,29],[-66,-23]],[[17790,14982],[-70,103],[1,50],[-94,27]],[[17627,15162],[-67,16]],[[17560,15178],[-105,92]],[[17455,15270],[-63,-132],[-69,-12],[-62,20],[-20,-37]],[[17241,15109],[-53,10],[-60,-38],[-70,22]],[[17058,15103],[-1,65],[-61,0],[8,85]],[[17004,15253],[-2,30]],[[17002,15283],[-17,38],[-151,1],[-103,-26],[-26,40],[46,71]],[[16751,15407],[-38,7],[-141,-26]],[[16698,15811],[28,14],[90,-4]],[[16816,15821],[60,-29],[67,22],[40,-60],[107,-7]],[[17090,15747],[51,9],[71,-26]],[[17212,15730],[49,25],[52,-2]],[[16525,14692],[-2,-115],[21,-65]],[[16544,14512],[51,30],[33,45],[85,22],[110,-36]],[[16823,14573],[96,-21]],[[16919,14552],[43,-10],[19,-58]],[[16981,14484],[0,-21]],[[16981,14463],[-6,-31],[-72,-65]],[[13133,10379],[60,8],[165,-14]],[[12623,10904],[34,-44],[93,53],[80,-20],[63,6]],[[12893,10899],[70,-47],[21,-39]],[[12984,10813],[-37,-94],[17,-58]],[[12964,10661],[-112,127],[-108,2],[16,-50],[95,-95]],[[12855,10645],[-83,-19],[-56,1],[-27,-62]],[[12689,10565],[-31,117],[-100,49],[-62,-9],[-80,-43],[-67,-15]],[[12349,10664],[-9,75],[55,48],[-94,75],[-114,40],[-17,28]],[[12170,10930],[59,30],[8,40],[44,28]],[[12281,11028],[70,-78],[16,-47],[104,-25],[57,64],[52,-43],[43,5]],[[13614,10316],[60,60],[-20,86],[-69,63]],[[13585,10525],[-76,18],[-39,41],[-60,23],[-82,-24],[-60,8],[-42,-27],[-34,24],[33,55],[89,68],[28,46]],[[13342,10757],[37,124]],[[11929,10055],[-39,51],[-48,9],[-32,170],[22,84],[-69,30],[-15,57]],[[11748,10456],[70,105],[43,110],[-88,33]],[[11773,10704],[49,27],[61,1],[96,36],[43,99],[36,-1],[51,41]],[[12109,10907],[61,23]],[[12855,10645],[34,-43],[-18,-51],[35,-50],[-4,-103],[-72,-43],[-41,32],[-21,57],[-51,-21],[36,-81]],[[13579,11314],[36,-29],[74,-4],[-3,-40],[-50,-27]],[[13234,10845],[-215,-219],[-55,35]],[[12623,10904],[145,119],[53,67]],[[12821,11090],[57,101],[4,62]],[[12882,11253],[58,15]],[[12940,11268],[33,13]],[[12973,11281],[46,33],[70,-1],[103,44]],[[13192,11357],[20,75]],[[13212,11432],[31,36]],[[12281,11028],[-18,51],[54,23]],[[12317,11102],[-23,27],[40,137],[9,90]],[[12343,11356],[-39,72]],[[12304,11428],[63,4],[41,85]],[[12408,11517],[-8,83],[-79,54]],[[12321,11654],[-17,67],[2,137],[-27,17],[-12,198]],[[12267,12073],[-34,103],[3,73]],[[12236,12249],[-11,96],[31,26]],[[12256,12371],[32,-17]],[[12288,12354],[-4,50],[30,90],[-28,72]],[[12526,11817],[24,29],[-20,72]],[[12530,11918],[-19,30]],[[12511,11948],[-40,146],[-85,22],[-16,-74],[74,-135]],[[12444,11907],[32,-65],[50,-25]],[[12243,12400],[13,-29]],[[11773,10704],[-81,16],[-84,80],[-86,-14]],[[11522,10786],[13,26],[-15,152],[-33,18]],[[11487,10982],[11,69]],[[11498,11051],[-86,131],[-66,25]],[[11346,11207],[-19,-18],[-72,33]],[[11255,11222],[-20,30],[12,50],[-59,25],[-47,71]],[[11141,11398],[57,42],[22,81],[68,38],[31,58],[59,18]],[[11378,11635],[5,56],[-35,73],[-70,28],[-34,-13]],[[11244,11779],[6,115]],[[11250,11894],[62,7]],[[11312,11901],[9,103],[28,72]],[[11349,12076],[-42,41],[-45,11],[-6,53],[-43,52],[-86,19]],[[11127,12252],[7,80]],[[11974,11343],[48,-33],[38,27]],[[12060,11337],[28,50],[-37,100],[-61,52]],[[11990,11539],[-39,18],[-122,123]],[[11829,11680],[-97,82]],[[11732,11762],[-19,-61]],[[11713,11701],[1,-99],[38,-61]],[[11752,11541],[140,-145],[82,-53]],[[11487,10982],[-40,39],[-59,-36],[31,-46],[-36,-90],[-131,-35],[-33,13],[-34,-83],[-42,-51],[87,-51]],[[11230,10642],[-96,-84],[-100,12],[-60,27],[-89,-3]],[[10885,10594],[-105,-93],[-7,-55],[18,-45]],[[20077,8163],[36,-53],[75,-71],[38,-60],[26,-157]],[[20252,7822],[50,17],[22,-71],[52,0],[-54,100],[52,49],[47,3],[16,-39],[44,6],[2,-61],[39,-66],[118,-82],[250,106],[58,-70],[32,18],[83,99],[54,31],[54,-54],[51,-17]],[[21222,7791],[-67,-113],[15,-42],[-36,-91],[66,-74],[61,-180]],[[21261,7291],[-59,-32],[-26,21],[-50,-91],[0,-56]],[[21126,7133],[-43,2],[-74,-68],[-70,12],[-23,-64],[13,-81],[26,-51]],[[20955,6883],[-74,-10],[-14,-23],[-79,-22],[-185,-32],[-65,-41]],[[20538,6755],[-74,8],[-76,-58]],[[20388,6705],[-53,-64],[-86,12],[-39,-25],[35,-75],[-24,-58],[22,-25],[-10,-57],[24,-24],[123,-11],[15,-17]],[[20395,6361],[-25,-66],[35,-19],[-8,-62],[22,-26],[-33,-137],[2,-66],[33,-30],[-20,-59]],[[20401,5896],[-38,-74],[-39,-25],[-58,18],[-138,-97],[-23,-47]],[[20105,5671],[-55,-17],[-3,-59],[-82,-2],[-52,-71],[-149,13],[-29,33]],[[19735,5568],[37,42],[-18,103],[-30,71],[16,38],[-11,68],[-55,111],[-43,-8],[-113,35],[-42,144],[-67,35],[-95,7],[-43,33]],[[19271,6247],[57,35],[4,36],[-69,116],[-81,-9],[-41,32],[16,277],[17,118],[-32,57],[-6,59]],[[19136,6968],[32,68],[-22,43],[0,56]],[[19146,7135],[42,82],[-30,90]],[[19158,7307],[12,26],[-4,112],[-63,54]],[[19103,7499],[127,97],[9,42],[-25,70]],[[19214,7708],[-19,36],[36,65],[-62,26]],[[19169,7835],[-27,27]],[[19142,7862],[-6,63],[49,67],[-26,25],[61,66],[15,50],[-32,90]],[[19203,8223],[39,123]],[[19242,8346],[111,10],[24,24],[27,97],[48,-30],[30,-77],[91,-45],[41,93],[65,76]],[[19679,8494],[39,-96]],[[19718,8398],[-28,-135]],[[19690,8263],[34,4],[56,-42]],[[19780,8225],[181,77],[95,108],[25,-30],[51,45],[70,-13],[-31,-69],[-97,-116],[-35,-24],[12,-44]],[[20051,8159],[26,4]],[[21647,5307],[61,11],[18,46],[41,20],[14,81],[35,40],[152,95],[26,139],[54,-24],[49,48],[-3,67]],[[18917,9414],[72,-51],[75,-125],[32,26],[37,-96],[-53,-67]],[[19080,9101],[37,-84],[-17,-14],[0,-90],[49,-34]],[[19149,8879],[-33,-55],[33,-88]],[[19149,8736],[-121,-23],[-55,54],[-35,59],[-37,-11],[-89,-66],[-33,19],[-78,-76]],[[18701,8692],[-41,-6]],[[18660,8686],[71,-200]],[[18731,8486],[-120,4],[-13,-43],[-82,-25]],[[18516,8422],[-4,34],[-58,57],[2,44]],[[18456,8557],[-30,49],[-63,-34],[-59,122],[12,39],[-68,35]],[[18248,8768],[-53,39]],[[18195,8807],[-72,-18]],[[18123,8789],[-38,52],[-39,13]],[[18046,8854],[-39,3],[-60,39],[-105,144],[-101,183],[0,64],[-83,111],[-39,23],[-7,54]],[[19902,10771],[-4,-128],[-55,5],[-57,-53],[3,-29]],[[19789,10566],[12,-46],[-80,-67]],[[19721,10453],[-55,-138],[20,-21],[-29,-68],[17,-36],[81,-54]],[[19755,10136],[12,-19],[-61,-96],[-9,-71],[-26,-32],[20,-37]],[[19691,9881],[50,-62],[-23,-69],[-38,-36]],[[19680,9714],[46,-57],[36,11],[53,54],[83,2]],[[19898,9724],[56,-59],[50,-191],[-48,-83],[-58,-32],[-19,-33]],[[19879,9326],[-51,-41],[-64,24],[-87,-31],[-35,-30]],[[19642,9248],[-38,-29],[-70,113],[-172,115],[13,71]],[[19375,9518],[-78,0],[-10,43],[-40,14],[-26,-38],[-64,-31],[-81,-13],[-87,22]],[[18927,5614],[139,127],[31,-1],[101,46],[59,-76],[31,-91],[-3,-73],[32,-71],[-8,-31],[36,-49],[99,-58],[84,95],[9,71],[67,21],[29,-16],[34,64],[68,-4]],[[21222,7791],[22,-29],[55,7],[78,32],[23,44],[85,40],[70,-20],[71,1]],[[21626,7866],[37,-53],[-15,-53],[128,-81],[120,-104],[74,-44],[62,-18],[180,-139],[-17,-70],[91,16],[89,-16],[10,-67],[36,-43]],[[16885,6118],[419,223],[219,7],[73,-14],[90,57],[19,36],[103,4],[77,26],[41,32],[20,-39],[-12,-59],[53,-25],[2,-97]],[[19879,9326],[30,-63],[33,-10],[163,6],[8,10]],[[20113,9269],[34,12],[58,-47],[52,-5]],[[20257,9229],[65,-29],[49,-54],[92,26],[98,50],[57,46]],[[20618,9268],[43,-16],[100,-78]],[[20761,9174],[57,-46],[-47,-97],[39,-40]],[[20810,8991],[13,-61],[-16,-51],[-55,-53],[-96,-35],[-40,-64],[-83,-27],[-26,-73],[-77,-51],[-53,-64],[45,-52],[-55,-32],[-40,-126],[-59,-27],[-16,-33],[-91,1],[-39,-58],[-45,-22]],[[19242,8346],[-38,87],[-51,33],[-36,75],[54,46],[-34,121],[12,28]],[[22391,9125],[-64,-49],[-56,-126],[-47,-45],[-21,-70],[-69,-7],[-30,-23],[-68,14],[-19,-84],[-38,-46],[-111,-7],[-39,-39],[-31,-135],[-72,-37],[-40,-45]],[[21686,8426],[-14,-64],[-59,-31],[-41,8],[-62,-18],[54,-158],[-8,-38],[61,-34],[31,-48],[-38,-81],[25,-53],[-9,-43]],[[18516,8422],[-18,-81],[18,-37],[-131,-234],[18,-89],[-16,-16]],[[18387,7965],[-115,-76],[-35,-75],[-61,-76],[58,-22],[64,-68],[13,-42],[-15,-114],[-26,-36],[43,-33]],[[18313,7423],[-58,-37],[-37,-1],[-111,-55],[9,-48],[-23,-46],[59,-41],[3,-92]],[[18155,7103],[-37,-100]],[[18118,7003],[-145,-35],[-60,-33],[-194,-31]],[[17719,6904],[-23,-68],[-94,-27],[-48,-76],[-59,3],[-63,-50],[-77,-21],[-58,-47],[-6,-56],[-166,-65],[-111,-128],[-121,-2],[-92,30],[-53,-13],[-42,-43],[-29,-94]],[[6124,10829],[-77,-32],[15,-30]],[[5783,10398],[-106,-24],[-67,48]],[[5610,10422],[-67,1]],[[5543,10423],[-57,-43]],[[5486,10380],[-57,-22],[-28,-44],[-155,-53],[-98,-83],[-67,-12]],[[5081,10166],[-107,-60],[-8,-112]],[[4966,9994],[-17,-82]],[[4949,9912],[-60,-40],[-74,-21],[-34,-46]],[[4781,9805],[-14,-57],[-62,-21],[-107,-57],[-139,-181],[-53,-29],[-31,-47],[-46,-125]],[[13778,12567],[-10,-96]],[[13768,12471],[-6,2]],[[13762,12473],[-75,-6],[-17,-26],[-64,46]],[[13606,12487],[25,46],[-25,41],[-5,84],[32,37]],[[13633,12695],[46,32],[40,-4],[74,-53]],[[13223,12527],[78,23],[62,57]],[[13363,12607],[48,-28],[49,-68]],[[13460,12511],[-58,-30],[-13,-71],[17,-50]],[[13406,12360],[-99,-4],[-15,-25]],[[13706,14638],[-11,-44],[-117,-14],[-30,-49]],[[13548,14531],[-46,34],[-4,49],[-64,102]],[[13434,14716],[82,88],[3,75]],[[11374,10314],[-80,8],[-55,23]],[[11239,10345],[-15,103],[42,77],[52,18],[43,43]],[[11361,10586],[50,30]],[[11411,10616],[4,-109],[-26,-98],[-21,-28],[6,-67]],[[11175,10161],[-44,-28],[-26,63]],[[11105,10196],[36,19]],[[11141,10215],[34,-54]],[[11517,9590],[-39,26],[13,49],[128,85],[83,23],[83,120],[-42,56],[-9,49],[-41,41],[-91,26],[-62,33],[-61,-2],[-49,28],[-51,105],[-5,85]],[[11411,10616],[2,7]],[[11413,10623],[132,14],[60,-34],[25,-37],[53,15],[12,-81],[-22,-32],[2,-62],[73,50]],[[10787,8729],[26,27],[-5,54],[-37,33],[54,166],[47,37],[35,71],[-84,248],[-1,53],[111,160],[80,57],[-2,24],[123,26]],[[11134,9685],[21,-27],[59,38],[28,-28],[82,-13],[65,-37]],[[11389,9618],[41,-112]],[[11389,9618],[45,55],[-30,41],[-1,105],[-109,10],[15,89],[-28,37],[27,99],[-110,119],[-23,-12]],[[11141,10215],[17,29],[75,48],[6,53]],[[11230,10642],[47,5],[84,-61]],[[11105,10196],[-49,-39],[-34,5],[-117,-56],[-71,31]],[[10834,10137],[-39,63]],[[11134,9685],[-19,54],[-73,-54],[-5,56],[-56,94],[-85,25],[-79,97],[-15,55],[57,29],[-79,49],[-14,34],[68,13]],[[11413,10623],[7,62],[-21,69],[23,63],[57,-30],[43,-1]],[[13626,14336],[0,-15]],[[13626,14321],[-106,-3],[-50,-38]],[[13470,14280],[-196,-15]],[[13274,14265],[15,27]],[[13289,14292],[56,32],[31,81],[-7,63],[-41,26],[12,60]],[[13340,14554],[36,-50],[63,26],[71,-33]],[[13510,14497],[55,-49],[11,-64],[60,-30]],[[13636,14354],[-10,-18]],[[13434,14716],[-30,-9],[-70,74],[-24,-3]],[[12363,12560],[45,-124],[74,-26]],[[12482,12410],[-61,-37],[-105,-39],[-28,20]],[[13010,11535],[-16,-91],[-123,-40]],[[12871,11404],[-38,31],[9,41],[-89,39]],[[12753,11515],[-27,63],[8,57]],[[12734,11635],[75,20],[136,-30],[45,-30]],[[12317,11102],[85,-59],[30,1],[148,90],[99,40],[56,42]],[[12735,11216],[44,32],[103,5]],[[12236,12249],[109,42],[33,-46],[59,-35],[-21,-40],[-46,-28],[-28,-77],[-61,-12]],[[12281,12053],[-14,20]],[[12735,11216],[-4,45],[-67,23],[-51,-33],[-38,14],[31,72]],[[12606,11337],[-40,41],[71,79],[-26,42]],[[12611,11499],[113,-10],[29,26]],[[12871,11404],[-20,-55],[41,-56],[48,-25]],[[12530,11918],[31,10],[27,-66],[40,-9],[37,-54],[13,-90],[56,-74]],[[12611,11499],[-37,2]],[[12574,11501],[31,88],[-40,12],[-16,66],[39,21],[-18,69],[-44,60]],[[12511,11948],[86,28],[0,63],[116,115],[62,9]],[[12408,11517],[35,-4],[44,50],[56,-57],[31,-5]],[[12606,11337],[-88,-35],[-25,25],[-98,-3],[-52,32]],[[12281,12053],[46,-7],[18,-60],[-18,-34],[28,-74],[89,29]],[[12670,12424],[-73,-65],[-115,51]],[[13159,10903],[-112,-46],[-63,-44]],[[12893,10899],[59,40],[17,51],[60,45]],[[13029,11035],[65,16]],[[13094,11051],[57,-56],[6,-71]],[[12973,11281],[39,0],[-18,-90],[38,-21]],[[13032,11170],[-43,-46]],[[12989,11124],[-64,-59],[-104,25]],[[13029,11035],[-7,71],[-33,18]],[[13032,11170],[19,23],[100,-53]],[[13151,11140],[-57,-89]],[[13212,11432],[79,-68]],[[13291,11364],[-29,-36],[-70,29]],[[13550,10759],[-72,-25],[-136,23]],[[13298,11361],[-7,3]],[[12458,10321],[-7,33],[70,31],[56,48],[97,33],[24,32],[-9,67]],[[12338,10285],[-26,30],[-13,69],[-68,82],[8,79],[-42,43]],[[12197,10588],[82,33],[70,43]],[[12197,10588],[-162,-41],[-28,5],[-105,-42],[-80,-53],[-74,-1]],[[13328,11178],[-32,75]],[[13296,11253],[29,93]],[[13296,11253],[-52,-62],[-43,-6],[-50,-45]],[[13585,10525],[22,78],[43,17],[-28,82]],[[12321,11654],[-8,-30],[-130,1]],[[12183,11625],[-57,37]],[[12126,11662],[-59,49],[-99,-5],[-55,27]],[[11913,11733],[-44,59],[-9,50],[-67,55],[-20,42]],[[11773,11939],[50,64]],[[11823,12003],[33,12],[40,73],[109,63]],[[11673,12087],[-39,-65],[-59,-27],[-45,-43]],[[11530,11952],[-7,46],[-54,117]],[[11469,12115],[35,18],[67,77]],[[11571,12210],[102,-123]],[[11346,11207],[28,60],[83,84],[106,140]],[[11563,11491],[40,-19]],[[11603,11472],[16,-44],[75,-60],[58,-81]],[[11752,11287],[1,-43],[-69,-4],[-1,-33],[-70,-53],[-73,-12],[-3,-66],[-39,-25]],[[11829,11680],[84,53]],[[12126,11662],[-136,-123]],[[11773,11939],[-127,-48]],[[11646,11891],[-105,27]],[[11541,11918],[-11,34]],[[11673,12087],[85,9],[18,-59],[39,4]],[[11815,12041],[8,-38]],[[11378,11635],[84,-11]],[[11462,11624],[69,-73],[32,-60]],[[12304,11428],[-67,60],[-19,36]],[[12218,11524],[-32,31],[-3,70]],[[11349,12076],[72,-15],[48,54]],[[11541,11918],[21,-69],[-66,-39]],[[11496,11810],[-67,-60],[-37,32],[29,45],[-19,48],[-64,-1],[-26,27]],[[11496,11810],[62,-75]],[[11558,11735],[-65,-37],[-31,-74]],[[12109,10907],[-26,103],[-72,34],[-9,85],[-69,65],[-29,56]],[[11904,11250],[70,93]],[[12060,11337],[134,-5],[33,71],[-21,95],[12,26]],[[11904,11250],[-152,37]],[[11603,11472],[119,38],[30,31]],[[11558,11735],[66,-62],[37,31],[52,-3]],[[11815,12041],[-5,89],[-30,7],[-25,70],[10,27],[-36,75]],[[11732,11762],[-53,38],[-33,91]],[[13295,12732],[26,-5],[42,-120]],[[11571,12210],[44,44],[-6,64],[33,17]],[[15963,13186],[-17,-53],[3,-68],[54,-85],[-7,-47],[-128,20],[-11,-40]],[[15857,12913],[-86,7],[-64,24],[-91,57]],[[15616,13001],[-20,50],[-38,-3],[-23,59]],[[15275,12506],[72,85]],[[15347,12591],[78,54],[-42,56],[64,20]],[[15447,12721],[10,-66],[57,-34]],[[15514,12621],[1,-69],[59,-69]],[[15574,12483],[-49,-69],[8,-31]],[[11065,11645],[27,-58],[-23,-117]],[[11069,11470],[-64,13],[-63,72]],[[10942,11555],[48,63],[75,27]],[[10294,11965],[48,50]],[[10342,12015],[84,-38]],[[10426,11977],[84,-99],[78,5],[20,-32]],[[10608,11851],[-24,-18]],[[10584,11833],[-65,-36],[-96,3],[-18,-18]],[[10684,11698],[11,-39]],[[10695,11659],[-33,-15]],[[10662,11644],[22,54]],[[10608,11851],[55,-14],[-52,119]],[[10611,11956],[122,1],[30,30],[5,75],[30,19]],[[10798,12081],[111,-20],[10,-67]],[[10919,11994],[-18,-89],[-95,-30],[72,-46]],[[10878,11829],[-35,-20],[6,-92],[-104,-7],[-64,27]],[[10681,11737],[-77,1]],[[10604,11738],[0,78],[-20,17]],[[11250,11894],[-74,45],[-21,-26]],[[11155,11913],[-39,26],[-46,-11]],[[11070,11928],[-102,35],[-49,31]],[[10798,12081],[61,31],[56,82],[7,63],[72,-28],[133,23]],[[11155,11913],[-32,-62],[14,-58]],[[11137,11793],[-16,-37],[-51,3],[-76,64],[-67,21]],[[10927,11844],[48,28],[77,16],[18,40]],[[11141,11398],[-72,72]],[[11065,11645],[69,-5],[39,114]],[[11173,11754],[71,25]],[[10411,11546],[68,34],[-5,30],[50,91],[80,37]],[[10681,11737],[3,-39]],[[10662,11644],[-29,-34],[8,-45],[-26,-42],[-95,-57],[-60,-72]],[[10460,11394],[-23,-1]],[[15514,12621],[17,49],[55,30],[59,-11],[30,-59],[42,45],[61,4],[51,-61],[39,-5]],[[15868,12613],[3,-71]],[[15871,12542],[-134,-5],[-63,-26],[-14,-43],[-86,15]],[[10426,11977],[30,10]],[[10456,11987],[62,-27],[48,37]],[[10566,11997],[45,-41]],[[10734,10392],[-12,138],[26,91],[-50,49],[5,39],[-44,102],[9,67],[-26,121]],[[10642,10999],[49,65],[-14,80],[61,14],[27,-35],[100,43],[65,-32],[-2,-55],[41,-57],[-26,-92],[15,-18],[17,-115],[-20,-32],[36,-61]],[[10991,10704],[-106,-66],[0,-44]],[[10398,10990],[110,110],[54,103],[37,2],[51,-48]],[[10650,11157],[22,-39],[-32,-58],[2,-61]],[[10991,10704],[53,69],[-7,60],[116,73],[30,66],[-51,74],[59,52],[-7,42],[28,51],[43,31]],[[10927,11844],[-49,-15]],[[10632,12280],[17,-73],[-10,-57],[-48,-55],[18,-35],[-43,-63]],[[10456,11987],[91,62],[-2,39],[-80,48],[-30,-6],[-91,44],[-34,-4],[-29,-51],[61,-104]],[[16323,12841],[-60,45],[-172,20],[-30,-29],[-90,-5]],[[15971,12872],[-84,8],[-30,33]],[[10844,12414],[90,26],[49,33],[144,4]],[[10942,11555],[-115,3]],[[10827,11558],[-37,12],[-39,50],[-56,39]],[[11137,11793],[36,-39]],[[10460,11394],[100,-68],[-6,-52],[93,20]],[[10647,11294],[20,-65],[-41,-29],[24,-43]],[[15182,13003],[67,-85],[48,-6],[64,-122],[65,-37]],[[15426,12753],[21,-32]],[[15347,12591],[-35,51],[-41,7],[-41,50],[-53,29],[-71,-2],[-68,-30]],[[10827,11558],[-68,-37],[24,-34],[-40,-154],[-72,-9],[-24,-30]],[[15971,12872],[-16,-21],[-72,0],[-40,-91],[32,-75],[-7,-72]],[[15426,12753],[26,37],[-54,55],[79,85],[59,-66],[52,11],[28,126]],[[15871,12542],[49,21],[30,-59],[-111,-102]],[[15266,13183],[-4,-68],[17,-48],[-98,-35]],[[13768,12471],[63,-90],[3,-101],[43,0]],[[13877,12280],[-82,-63],[-48,48],[-77,14]],[[13670,12279],[-51,62],[-12,59],[66,13],[15,-26],[64,50],[10,36]],[[14429,9364],[-47,130]],[[14382,9494],[93,29],[41,46],[0,44],[78,-1],[18,-69],[49,-30]],[[14661,9513],[-43,-26],[-27,-83],[10,-64],[-71,-27],[-57,-69]],[[14473,9244],[-44,120]],[[14377,6930],[-27,53],[9,78],[-108,139],[-36,93],[-126,111],[-81,23]],[[14008,7427],[25,28],[73,17],[25,39],[49,-13],[46,23],[70,-18],[111,69],[62,74],[31,10],[24,62]],[[14524,7718],[43,-1],[69,-32],[59,44]],[[13794,9098],[69,12],[55,-47],[60,2],[61,66]],[[14039,9131],[42,-16],[54,22],[53,67],[79,67],[62,83],[100,10]],[[14473,9244],[9,-41],[-16,-86]],[[14466,9117],[-22,-39],[-72,-38],[-80,17],[-84,-31],[-85,27],[-63,-2],[-51,-26],[26,-34],[-25,-107],[-106,-131],[-22,-107],[-34,-72]],[[13848,8574],[-47,-9],[-25,-34],[-66,-20]],[[15528,9722],[-100,-75],[-3,-20],[-101,-1],[-6,-36],[-47,-31],[-12,-38]],[[15259,9521],[-122,-39],[-146,84],[-23,47],[-42,32],[0,-218],[-53,-99],[-44,-27],[20,-35],[55,-19],[23,-102],[-12,-30]],[[14915,9115],[-132,29],[-65,105],[-55,7],[-64,64],[-126,-76]],[[14661,9513],[37,46],[47,125]],[[14745,9684],[118,-26],[18,24]],[[14466,9117],[36,11],[34,-47],[71,-11],[42,-47],[83,-11],[7,-31]],[[14739,8981],[-25,-68],[-45,-26],[-47,-77],[23,-87]],[[14645,8723],[-110,-56],[-68,-52],[-101,60],[-15,-14],[-104,-15],[-42,-57],[-168,0],[-83,-51]],[[13954,8538],[-36,-2],[-70,38]],[[14361,9766],[30,-34],[62,63],[62,4],[33,-32],[105,-28],[22,-32],[70,-23]],[[14382,9494],[-14,36]],[[13506,8006],[65,47],[69,-23],[47,29],[20,-49],[86,-118],[85,-50],[14,-43],[112,-34],[101,18],[76,-58],[73,26],[66,-22],[82,16],[85,37]],[[14487,7782],[37,-64]],[[14008,7427],[-71,-23],[-36,-39],[-78,5],[-55,-58]],[[13768,7312],[-105,-54],[-79,54],[-76,-40],[-83,-5],[-89,61]],[[14487,7782],[-6,151],[17,88],[-118,15],[-126,71],[-99,139],[-76,27],[-75,68],[-18,88],[-31,39],[-1,70]],[[14645,8723],[10,-191],[19,-65],[-72,-62],[-35,-7],[-25,-60],[97,-29],[98,-91],[23,-54],[-35,-51],[27,-41],[-4,-51],[55,-61],[-5,-27]],[[13968,7000],[-71,163],[-20,4],[-75,128],[-34,17]],[[13994,9766],[50,-5],[11,-38]],[[14055,9723],[66,-39],[68,41],[82,-45]],[[14260,9632],[-28,-7],[-106,-103],[21,-26],[-29,-59],[55,-28],[8,-35],[-37,-65],[-65,-38],[-40,-140]],[[14915,9115],[-9,-31]],[[14906,9084],[-3,-80],[-39,-21]],[[14864,8983],[-71,-18],[-54,16]],[[14218,9892],[-77,-22],[-6,-32],[-80,-115]],[[14864,8983],[9,-59],[91,-56],[74,3],[2,-43],[99,-42],[135,33],[67,88],[24,-5],[103,59],[103,-17],[77,117]],[[15648,9061],[115,9]],[[15259,9521],[-5,-71],[-116,-126],[-8,-124],[-34,-58],[-42,-1],[-53,-69],[-95,12]],[[15648,9061],[-9,95],[52,47],[-25,219]],[[13633,12695],[-3,34]],[[13630,12729],[33,33]],[[13663,12762],[-13,33],[19,183]],[[13669,12978],[-3,105],[-76,93]],[[13952,13029],[-9,-47],[30,-116]],[[13973,12866],[-50,-70]],[[13923,12796],[-40,-74],[-73,-40]],[[14795,11704],[10,60],[27,9],[64,94],[65,1]],[[14961,11868],[146,59],[24,-24],[71,5],[26,-40],[78,-22],[-71,-88]],[[14961,11868],[22,99],[-20,28]],[[15614,11908],[15,-49],[57,-11]],[[15686,11848],[0,-93],[-40,-70],[-14,-115],[39,-51]],[[15671,11519],[-47,-34],[-19,-72]],[[15686,11848],[87,55]],[[15773,11903],[52,-69],[74,-57]],[[15899,11777],[19,-45],[-45,-47],[33,-76],[-5,-40]],[[15901,11569],[-62,-19],[-84,19],[-84,-50]],[[15756,11166],[43,86],[40,10],[82,-54],[24,41],[70,-14],[22,18],[83,-26],[3,55],[36,33]],[[16159,11315],[53,-64],[78,13],[28,21]],[[15711,12013],[7,-36],[55,-74]],[[16283,11460],[-78,43],[-34,34],[8,63],[33,8],[0,73],[19,45],[46,14],[22,62]],[[16159,11315],[-22,93],[-128,82],[-81,8],[-27,71]],[[15899,11777],[74,81]],[[15973,11858],[32,17],[112,-51],[136,16]],[[15973,11858],[-31,39],[47,75],[-18,63]],[[14066,12914],[-93,-48]],[[13923,12796],[66,-61]],[[13989,12735],[18,-46],[-15,-41],[44,-41],[1,-41]],[[14037,12566],[0,-23],[-88,-72]],[[15080,10868],[-50,-75],[4,-61],[-18,-68]],[[15016,10664],[-40,-68],[13,-40],[-121,-17],[-18,42],[-68,88],[28,46],[-32,55],[-11,60]],[[14767,10830],[38,67],[-6,41],[26,67],[-11,79],[62,7]],[[14100,11014],[6,-73],[25,-32],[11,-85],[48,-101]],[[14190,10723],[-1,-36],[-70,-53],[4,-64],[-23,-32],[-80,-5]],[[14804,10334],[24,37],[64,3],[11,28],[177,-16],[38,53],[80,-43],[53,-60]],[[15251,10336],[-1,-29]],[[15250,10307],[-18,-32],[-66,-46],[-53,-8],[-90,24],[-41,-12],[-90,85],[-88,16]],[[14080,10435],[63,18],[87,4],[96,44]],[[14326,10501],[34,21],[59,-38],[104,-113],[-31,-56]],[[14492,10315],[-114,-44],[-23,-58]],[[14190,10723],[147,-146]],[[14337,10577],[-11,-76]],[[14492,10315],[40,-28],[6,-51],[123,-5]],[[14661,10231],[50,-6],[17,-59],[81,-8],[13,-27],[-39,-127]],[[14783,10004],[-132,-3],[-81,43],[-59,-95],[-17,-5]],[[14995,9764],[-73,37],[-9,164],[-130,39]],[[14661,10231],[58,87],[85,16]],[[15250,10307],[88,17],[33,21],[136,14],[78,44]],[[15585,10403],[69,-29],[35,7],[79,189]],[[15169,10915],[41,-17],[60,-118],[43,14],[62,-14],[-21,-100],[31,-33],[-13,-82],[16,-39],[-50,-41],[-12,-48],[22,-33],[72,39],[60,72],[29,105],[107,17],[-24,-45],[9,-111],[-16,-78]],[[15251,10336],[-11,64],[-36,11],[-20,47],[-168,206]],[[14238,12548],[-56,-28],[-21,-33],[-109,78]],[[14052,12565],[68,51],[78,30]],[[14767,10830],[-45,-27],[-43,29]],[[14679,10832],[-6,151],[-28,46],[-44,25],[-4,108],[-33,28]],[[14679,10832],[-26,29],[-70,28],[-71,-47]],[[14512,10842],[-44,48],[-99,3],[-81,69],[-31,5]],[[14257,10967],[-31,80]],[[14337,10577],[47,76],[62,48],[11,53],[55,88]],[[14190,10723],[90,73],[-23,171]],[[14737,11841],[-68,-3],[-38,34],[37,96],[61,74],[64,103]],[[14098,12809],[-63,-24],[-46,-50]],[[13669,12978],[-35,29],[-65,5],[-55,56]],[[12611,9815],[-58,-97]],[[12553,9718],[-127,51],[-65,-70],[-52,12],[-25,62],[-64,18],[-82,-17],[-36,-27],[-65,2],[-75,-20],[-17,64],[-43,19]],[[11434,9359],[53,-26],[32,18],[63,-37],[56,21],[29,41],[36,0],[62,59],[68,-83],[54,-19],[40,-46],[65,-19],[87,-88]],[[12086,9160],[-25,-55],[120,-146],[40,-90],[-3,-27]],[[12218,8842],[-86,-27],[-99,73],[-122,-7],[-77,-65],[-41,-95],[-90,-27],[-111,2],[-54,-66]],[[12345,8482],[-31,94]],[[12314,8576],[94,35],[37,70],[70,93],[47,6],[38,162],[1,71],[57,109],[-27,59],[-106,91],[8,137],[-61,42]],[[12472,9451],[-23,63],[63,96],[0,33],[41,75]],[[12218,8842],[7,-31],[-42,-166],[65,8],[25,-49],[41,-28]],[[12366,9389],[106,62]],[[14052,12565],[-15,1]],[[13566,9576],[-57,109],[-36,40]],[[13473,9725],[16,75],[-87,41],[-31,68]],[[13371,9909],[66,105],[37,116]],[[13895,10102],[8,-25],[-84,-51],[-75,0],[-3,-51],[-67,-52],[23,-63],[11,-169]],[[13371,9909],[-59,10],[-108,174],[-57,-25]],[[13147,10068],[3,60]],[[13150,10128],[96,48],[73,24]],[[12881,9778],[39,16]],[[12920,9794],[134,42]],[[13054,9836],[23,14],[105,-4],[-4,-53]],[[13178,9793],[19,-47],[-13,-128],[-123,-19],[-60,-50],[-65,-83],[-57,-6]],[[13150,10128],[-17,79]],[[13133,10207],[22,54],[-18,77]],[[12839,10080],[61,-6]],[[12900,10074],[37,-53],[-17,-227]],[[12849,10176],[-135,-21]],[[13473,9725],[-83,-19],[-62,28],[-44,42],[-106,17]],[[13054,9836],[-15,82],[10,54],[58,75],[40,21]],[[12900,10074],[10,29],[153,84],[70,20]],[[14223,12841],[122,29],[54,-10],[35,40],[35,-48]],[[14469,12852],[-58,-31],[-46,13],[-39,-39],[12,-48],[-54,-14]],[[14633,12793],[-65,-28],[-47,-91],[-146,-12],[-28,-18]],[[14469,12852],[36,24],[88,-6]],[[15240,12253],[-35,6],[-129,131],[4,31],[-81,51]],[[14999,12472],[91,52]],[[14138,13042],[59,16],[36,-32],[80,42]],[[14313,13068],[44,-68],[98,-39],[28,45],[64,21]],[[14718,12440],[102,125]],[[14820,12565],[40,-4],[53,-59]],[[14913,12502],[5,-84],[-41,-72]],[[14751,12657],[-63,-81],[-28,-109]],[[14999,12472],[-86,30]],[[14820,12565],[-14,21]],[[14313,13068],[58,58],[37,-8],[31,71]],[[17207,10669],[-22,7]],[[17185,10676],[-53,69],[27,98],[-76,-5],[-35,71],[-80,4],[-79,27],[-86,-14],[-13,49],[-51,23],[-80,-13],[-44,-55],[-12,-44],[-104,-41],[-84,-10],[-91,-94],[-78,-12],[-67,23]],[[16183,10099],[155,85],[10,51],[81,71],[28,51],[96,58],[32,38],[50,13],[47,64],[99,17],[66,44],[52,-40],[27,-64],[37,20],[99,-41],[60,78],[15,64],[48,68]],[[14158,12112],[11,-39],[-80,-68],[-20,-56],[-3,-89]],[[14066,11860],[-12,-32],[30,-65],[-4,-153],[16,-15]],[[14096,11595],[-24,-20]],[[14072,11575],[-38,44],[-54,23],[-30,49],[21,32],[-22,45],[-65,30],[-130,99]],[[13754,11897],[23,57],[-42,21],[36,70],[-23,26]],[[13648,12043],[-58,-62],[42,-126]],[[13632,11855],[-10,-34]],[[13509,11760],[-38,51],[-67,24],[-50,60],[-14,71],[14,25],[-53,113],[13,142]],[[13518,11708],[-49,-69],[-90,-6],[-76,-31],[-10,24]],[[14653,11647],[-44,-66],[-217,1],[-25,21],[-72,-8],[-27,-21]],[[14268,11574],[-38,23],[-42,-22],[-63,-1],[-29,21]],[[14066,11860],[65,15],[36,46],[90,-51],[80,44],[-1,77],[-22,8]],[[14359,11315],[-12,-30],[-54,0],[13,55]],[[14338,11097],[-9,109],[24,65],[60,13]],[[14289,11455],[-1,74],[-20,45]],[[13754,11897],[-122,-42]],[[14175,11039],[-58,91]],[[14117,11130],[-32,44],[-5,107],[-65,22],[-16,47]],[[13999,11350],[-18,91],[11,56],[80,78]],[[13753,11279],[41,44],[11,42],[47,19],[71,1],[19,-56],[57,21]],[[14117,11130],[-18,-27]],[[15005,13546],[1,64],[43,29],[71,-3]],[[15120,13636],[50,-26],[28,-112],[53,-36]],[[15251,13462],[-50,-81],[-53,-40],[-11,-38]],[[15494,13146],[-42,14],[-3,63],[-28,28],[16,50],[-47,64],[33,69],[65,48],[97,13]],[[15585,13495],[46,-67],[-3,-31],[58,-84]],[[14889,14000],[4,-50],[-28,-36],[-77,25],[-54,-52],[7,-97],[-53,-69]],[[15251,13462],[47,-25],[88,62],[80,13],[3,86]],[[15469,13598],[63,-3],[69,-33]],[[15601,13562],[-16,-67]],[[15120,13636],[9,62]],[[15129,13698],[112,-19],[36,24],[41,60],[83,36]],[[15401,13799],[12,-111],[56,-90]],[[15395,13844],[6,-45]],[[15129,13698],[-44,115],[24,58],[-19,43],[59,37],[44,-11],[39,32],[-1,74]],[[15953,13607],[-40,-26],[-53,8],[-23,-51],[-144,-6],[-33,51]],[[15660,13583],[14,67],[-79,13],[-60,59],[7,52],[64,84]],[[15660,13583],[-59,-21]],[[14453,13664],[27,-7],[66,-81],[36,-16]],[[14582,13560],[-8,-19]],[[14574,13541],[-48,-97]],[[14526,13444],[-63,18],[-75,-55],[5,-80]],[[14601,12851],[69,25],[49,40],[105,36]],[[14824,12952],[46,-21],[3,-38],[-28,-52],[45,-44],[61,-3],[44,-47]],[[14526,13444],[75,-71]],[[14608,13254],[-40,15],[-95,-21],[-59,23]],[[14655,13370],[10,59]],[[14665,13429],[51,-41]],[[14716,13388],[20,-47],[60,-28],[20,-68],[-40,-30]],[[14823,12959],[1,-7]],[[14840,12997],[56,-46],[129,53],[35,-45]],[[15148,13008],[-8,66]],[[14574,13541],[47,-10],[-2,-50],[46,-52]],[[14716,13388],[72,66],[66,11],[81,-64],[59,50]],[[14582,13560],[78,68],[14,86]],[[13373,12790],[117,-95],[53,42],[87,-8]],[[13606,12487],[-58,5],[-43,36],[-45,-17]],[[4681,8986],[45,37],[62,-52],[-87,-56]],[[4701,8915],[-20,71]],[[5144,8769],[-62,-68],[9,-19]],[[5091,8682],[-58,13],[-64,-10]],[[4969,8685],[-99,84],[45,68],[87,31],[0,74]],[[5002,8942],[47,-14],[83,26]],[[5139,8594],[-48,88]],[[5294,9644],[79,-93]],[[5373,9551],[-35,-30],[-25,-65]],[[4749,8797],[-50,-30],[-47,52]],[[4652,8819],[12,44],[46,33]],[[4710,8896],[88,69],[-31,61],[22,26],[82,8],[22,-43]],[[4893,9017],[-12,-91],[-29,-44],[-40,-4],[-63,-81]],[[4909,8603],[75,33],[-15,49]],[[5604,8977],[55,-28],[25,-58],[42,-24],[46,6]],[[4635,8562],[61,84],[84,80]],[[4780,8726],[-5,-38],[67,-73]],[[4629,8220],[28,-33]],[[5441,9412],[-32,92]],[[5409,9504],[89,93],[61,7]],[[5617,9364],[-47,-16]],[[4985,8956],[-62,55]],[[4923,9011],[-26,48],[26,43],[66,28],[20,-15],[11,-76],[-26,-22],[-9,-61]],[[5373,9551],[36,-47]],[[4780,8726],[-48,30],[17,41]],[[4893,9017],[30,-6]],[[4985,8956],[17,-14]],[[4469,8672],[66,83],[117,64]],[[4710,8896],[-9,19]],[[4681,8986],[-75,43]],[[4571,7044],[52,0]],[[4623,7044],[17,-54]],[[5069,7917],[82,-52],[-80,-121]],[[5071,7744],[-55,-37],[-64,-4]],[[4719,6922],[16,15],[2,87],[34,51],[51,19]],[[5654,7579],[-15,28],[-94,61],[-16,48],[-53,7],[9,73]],[[5485,7796],[129,34]],[[5614,7830],[71,-69],[78,-27]],[[5260,8465],[131,-73],[50,-52]],[[5441,8340],[-15,-34],[-78,-76],[-10,-42],[-41,0]],[[4582,7469],[40,-57],[-13,-70]],[[4609,7342],[3,-71],[37,-45],[16,-106],[-37,-35],[-5,-41]],[[5598,7559],[-19,49],[-184,120],[54,98]],[[5449,7826],[36,-30]],[[5449,7826],[-4,38]],[[5445,7864],[-4,96]],[[5441,7960],[101,41]],[[5542,8001],[72,-171]],[[5445,7864],[-75,-49],[-51,-121],[-73,-45]],[[5246,7649],[-36,-11],[-53,26],[2,50]],[[5159,7714],[1,26],[75,138],[52,-34],[27,37],[113,99]],[[5427,7980],[14,-20]],[[4663,7497],[27,-74],[-24,-40]],[[4666,7383],[-57,-41]],[[5224,8109],[43,-110],[60,-57],[82,54],[18,-16]],[[5159,7714],[-88,30]],[[4666,7383],[34,-52],[77,0],[-2,-65],[67,-37],[-43,-71],[33,-40]],[[5246,7649],[-3,-41],[-38,-61],[-19,-76],[-36,32],[-41,-59],[-53,-34],[28,-53],[-43,-44]],[[5001,7303],[-85,-7],[-22,87],[17,56],[-66,58],[8,60]],[[14853,14990],[-53,9],[-78,45],[-16,38]],[[14706,15082],[59,61],[51,-25],[43,-48],[56,52],[-48,13],[-31,54]],[[14836,15189],[21,-7],[81,76],[40,-72]],[[14978,15186],[-2,-39],[-41,-32],[88,-96]],[[15434,14982],[-46,-55]],[[15388,14927],[-103,18]],[[15285,14945],[-77,6]],[[5441,8340],[87,20]],[[5593,8218],[-79,-33],[-31,-35],[-9,-63],[46,-23],[22,-63]],[[5703,6667],[56,-5],[30,-36],[41,25]],[[5830,6651],[98,-51],[86,-11]],[[6014,6589],[-11,-29],[81,-50],[66,-24],[-9,-45],[44,-31],[-35,-56],[34,-18],[-4,-44],[62,-38]],[[6070,7646],[-13,3]],[[6057,7649],[-97,115],[-59,14],[-20,29]],[[6195,7396],[180,-16]],[[6375,7380],[-11,-26]],[[6364,7354],[-32,-13],[-21,-86],[-35,-53]],[[6276,7202],[-19,-21],[-56,25]],[[6276,7202],[17,-27],[86,36]],[[6379,7211],[21,-9]],[[6400,7202],[57,-46],[21,-39]],[[6478,7117],[-145,-102],[-115,-25],[-35,16],[-34,130]],[[6149,7136],[3,91]],[[6152,7227],[14,-3]],[[6149,7136],[-62,-14],[-72,-71]],[[6015,7051],[-76,79],[-58,-37],[15,-95],[-59,-53],[22,-30]],[[5859,6915],[-79,-55],[-27,43],[40,44],[-28,174],[-40,-17],[7,-63],[-26,-63],[-32,-9],[-13,-73]],[[5661,6896],[-51,74],[23,74],[45,93],[45,35],[107,50],[-2,118]],[[5828,7340],[-14,48]],[[5814,7388],[30,48],[81,-54],[77,9],[48,-33],[22,39]],[[6072,7397],[27,-16]],[[6099,7381],[-36,-32],[-8,-91],[97,-31]],[[6364,7354],[5,-1]],[[6369,7353],[10,-142]],[[6204,7625],[83,-3],[55,-43],[62,-9],[38,-33]],[[6442,7537],[0,-71],[-65,-58],[-2,-28]],[[14626,14325],[64,-63],[49,36],[21,-45],[78,-89],[-39,-21]],[[6369,7353],[82,-17],[11,-65],[-52,-37],[-10,-32]],[[5828,7340],[-16,-4]],[[5812,7336],[2,52]],[[6143,7401],[-44,-20]],[[6072,7397],[-20,13],[-56,102]],[[5996,7512],[66,26],[19,52],[43,10]],[[6014,6589],[102,183],[191,-105],[47,-40],[73,15],[-22,-51],[34,-15]],[[6439,6576],[8,-49]],[[5645,6891],[4,3]],[[5649,6894],[12,2]],[[5859,6915],[88,29],[50,42],[18,65]],[[6478,7117],[39,-83]],[[6517,7034],[-112,-49],[-57,-2],[-107,-33],[-90,-59],[-65,26],[-183,-73],[-40,-61],[-17,-60],[23,-30],[-39,-42]],[[6290,7801],[218,-137],[85,12]],[[6593,7676],[-5,-55],[-36,-26],[-63,7],[-47,-65]],[[7050,6810],[-101,46],[-63,51],[-50,6],[-52,-76],[-84,-7],[-7,134],[24,21],[26,144],[59,-22],[32,46],[167,136],[60,26],[36,36],[97,152],[-23,81],[-24,16]],[[15064,14980],[20,-28],[-12,-122],[28,-35]],[[15100,14795],[-53,-13],[-48,12]],[[14999,14794],[-49,61],[-123,60]],[[5714,7624],[65,11],[24,-43],[68,23],[78,-9],[108,43]],[[5996,7512],[-37,-12],[-100,28],[-39,-17],[-54,22],[-61,-2]],[[5705,7531],[-60,43]],[[6340,8043],[-30,-41],[13,-65],[-32,-52],[-1,-52]],[[5812,7336],[-55,11]],[[5757,7347],[5,62],[-46,32]],[[5716,7441],[11,32],[-22,58]],[[6817,7883],[-7,-41],[-62,-19],[-155,-147]],[[14485,14629],[31,-51]],[[14516,14578],[-122,3]],[[5716,7441],[-32,-44],[-206,-106],[-6,-38],[-70,-58],[41,-48]],[[5757,7347],[-48,-31],[-24,-45],[-78,-25],[-74,-49],[17,-69],[3,-127],[96,-107]],[[15285,14945],[-6,-49],[21,-65],[-19,-70],[-33,4]],[[15248,14765],[-112,31]],[[15136,14796],[-36,-1]],[[6517,7034],[14,-64],[-33,-48],[-45,-113],[28,-61],[66,-68],[-108,-104]],[[5681,8182],[31,18],[-4,63],[-46,80]],[[5662,8343],[66,19]],[[5728,8362],[36,-86],[117,-1],[69,36]],[[5950,8311],[-21,-101],[-48,-9],[-32,-40],[-60,11],[-50,-24],[-56,1]],[[6009,8714],[43,-2],[44,-84]],[[6096,8628],[-54,-33],[-20,-43]],[[6022,8552],[-61,-9]],[[5961,8543],[-32,37],[-59,-20],[-29,51]],[[5841,8611],[9,66],[30,14],[70,-13],[59,36]],[[6171,9084],[10,-69],[30,-74],[56,6],[27,-54]],[[6294,8893],[-42,-38]],[[6252,8855],[-81,-1],[-76,-40],[-92,-21]],[[6003,8793],[-25,52],[35,47]],[[6013,8892],[10,23],[74,40],[10,24],[-64,93]],[[5539,8331],[39,25],[45,-28],[39,15]],[[15542,14840],[-90,-9]],[[15452,14831],[-64,96]],[[6009,8714],[-5,74]],[[6004,8788],[-1,5]],[[6252,8855],[22,-37],[-12,-61]],[[6262,8757],[-50,-60],[2,-29]],[[6214,8668],[-82,-39],[-36,-1]],[[5728,8362],[39,56],[57,-3],[56,40]],[[5880,8455],[3,-51],[50,-52],[36,11]],[[5969,8363],[-3,-23]],[[5966,8340],[-16,-29]],[[15480,14315],[-61,-29]],[[15419,14286],[4,48],[-89,11],[-52,39],[-18,43],[27,36],[71,49]],[[15362,14512],[43,-6],[107,57],[19,-67]],[[15531,14496],[12,-77],[-20,-44],[-44,-21],[1,-39]],[[6553,8356],[22,-83],[-101,-4]],[[6474,8269],[47,77],[32,10]],[[6596,8680],[-53,-1],[-26,-70]],[[6517,8609],[-87,41]],[[6430,8650],[-51,73],[34,33],[-21,46]],[[6392,8802],[10,25]],[[6402,8827],[12,24],[160,44]],[[6294,8893],[25,26]],[[6319,8919],[60,-7],[23,-85]],[[6392,8802],[-45,-32],[-85,-13]],[[6319,8919],[32,40],[93,41]],[[14502,15571],[-12,25]],[[14490,15596],[24,78],[-36,35]],[[14478,15709],[95,18],[29,33],[75,-2]],[[14677,15758],[-9,-150],[-62,-63]],[[14606,15545],[-67,2],[-37,24]],[[15891,14429],[-92,-12],[-78,74],[-23,-22],[-54,30]],[[15644,14499],[24,37],[-36,26],[8,50],[137,34]],[[6013,8892],[-48,54],[-66,21]],[[6517,8609],[26,-13],[25,-69],[56,-32],[-27,-56]],[[6597,8439],[-72,16]],[[6525,8455],[-76,30],[-49,-3],[-50,-56],[-28,56]],[[6322,8482],[-21,41],[45,91]],[[6346,8614],[64,-12],[20,48]],[[6214,8668],[15,-43]],[[6229,8625],[-78,-73],[-12,-33]],[[6139,8519],[-117,33]],[[14999,14794],[-68,-59],[-78,-24],[-13,-34]],[[14840,14677],[-14,-1]],[[14826,14676],[-11,77],[-45,79]],[[6139,8519],[-53,-63],[-87,-33],[-30,-60]],[[5880,8455],[63,53],[18,35]],[[6640,8435],[-43,4]],[[6004,8788],[-75,6],[-81,-16],[-28,57],[-31,10]],[[6687,7922],[-79,89],[-37,99],[-50,54],[-67,46],[-2,39]],[[6452,8249],[22,20]],[[6553,8356],[-3,26]],[[6550,8382],[84,20],[38,-31]],[[5841,8611],[-6,-35],[-120,-32],[-24,-42],[-91,-45],[-65,-66]],[[6260,8085],[-21,42],[7,103]],[[6246,8230],[71,-11],[135,30]],[[6346,8614],[-83,-20],[-34,31]],[[14826,14676],[-55,-14],[-4,-31],[-61,-69],[-79,-22]],[[14627,14540],[-111,38]],[[6550,8382],[-25,73]],[[6322,8482],[-33,3],[-58,-119],[-42,31],[-16,-52]],[[6173,8345],[-57,-64],[-82,5],[-68,54]],[[6173,8345],[39,0],[50,-36],[-16,-79]],[[14627,14540],[-24,-61],[19,-24],[-10,-69]],[[15452,14831],[-114,-133],[-39,-12]],[[15299,14686],[-32,13],[-19,66]],[[6132,9603],[51,29]],[[6183,9632],[54,71],[29,-34]],[[6286,9560],[-143,-101]],[[6143,9459],[-7,15]],[[6143,9459],[42,-60],[78,8]],[[6263,9407],[54,17]],[[6317,9424],[14,-73],[48,-24],[46,-70],[97,12],[-4,73],[109,67],[35,-14]],[[6248,9134],[29,36],[-78,138],[-78,17],[-116,-38]],[[6395,9608],[79,-24],[41,-55]],[[6515,9529],[-88,-36],[-29,-34],[-81,-35]],[[6263,9407],[56,71],[-9,64]],[[6854,10301],[-25,9],[-50,-45],[-64,15],[-57,49]],[[6290,10003],[31,69],[114,98],[27,-5]],[[6462,10165],[84,-146],[27,-29]],[[6573,9990],[-2,-42]],[[6571,9948],[-94,-81],[-36,39]],[[6441,9906],[-11,47],[-70,-38]],[[15480,14315],[74,-55],[81,-92],[44,-24],[31,-69]],[[15710,14075],[-44,-42],[-30,18]],[[15636,14051],[-150,62],[-116,10],[-17,25]],[[15353,14148],[32,54],[44,9],[-10,75]],[[6747,9852],[-84,-10]],[[6663,9842],[6,68]],[[6669,9910],[31,25],[69,3]],[[6153,9710],[30,-78]],[[6702,9532],[-25,-62],[12,-64]],[[6515,9529],[82,4],[12,30]],[[6609,9563],[2,37],[59,42],[-50,36],[64,51]],[[6684,9729],[72,-12]],[[6702,10058],[-6,29],[-76,-4],[-37,99],[-70,68]],[[15136,14796],[-24,-80],[-35,-51],[-40,-18],[1,-58],[-33,-16],[-123,32],[-39,-22],[-3,94]],[[6239,9765],[8,-33],[-59,-14]],[[6441,9906],[-117,-91],[-27,8]],[[6571,9948],[73,-2],[25,-36]],[[6663,9842],[-11,-53]],[[6652,9789],[-69,-53],[-55,-79],[81,-94]],[[6476,10240],[48,-133],[66,-95],[-17,-22]],[[6462,10165],[-24,64]],[[6684,9729],[-32,60]],[[15545,13920],[16,51],[46,32],[29,48]],[[15710,14075],[-7,-73],[94,32],[76,38],[11,41]],[[7099,8478],[12,-4]],[[7111,8474],[47,-32],[66,-6]],[[7224,8436],[-49,-60],[15,-53],[-22,-32],[-95,47]],[[7073,8338],[22,47],[4,93]],[[6727,9392],[42,-73],[81,-20]],[[6850,9299],[-10,-75],[32,-23],[-19,-66],[19,-46],[-31,-24],[7,-46],[53,-57]],[[6901,8962],[-48,-26],[-79,-17],[-59,-63],[-90,-51]],[[6999,8333],[4,-3]],[[7003,8330],[-31,-83],[-36,-30]],[[6936,8217],[-76,117],[-112,26],[-11,18]],[[6737,8378],[1,67],[36,44],[35,-35],[31,23]],[[6840,8477],[43,-97],[55,-36],[61,-11]],[[7165,9456],[-46,3],[-67,-34],[-17,-42],[9,-53],[-36,-13],[-12,-47]],[[6996,9270],[-48,-27],[-76,27],[-22,29]],[[7374,8932],[-106,-55],[-28,59],[-61,58],[5,32],[-45,59],[61,77]],[[7200,9162],[26,-38],[53,28]],[[7019,7687],[56,130],[6,88],[40,59],[-34,107],[-27,28],[6,59],[-63,172]],[[6999,8333],[74,5]],[[7224,8436],[17,15]],[[14146,15009],[161,-16],[62,-35]],[[14369,14958],[22,-10]],[[14391,14948],[-37,-48],[15,-54],[-26,-60],[-61,6]],[[15299,14686],[-79,-43],[-20,-67],[42,-48],[69,-29],[51,13]],[[15353,14148],[-127,-72],[-63,10]],[[6815,8059],[73,14],[4,28],[51,63],[-7,53]],[[6840,8477],[10,17]],[[6850,8494],[31,33],[135,-9],[83,-40]],[[6694,8349],[43,29]],[[6628,8753],[114,41],[69,-32],[65,-109],[83,-4],[63,20],[43,-14],[13,-51],[44,-32],[-11,-98]],[[6850,8494],[-125,67]],[[6996,9270],[112,-34],[67,-59]],[[7175,9177],[-87,-86],[-24,-86],[-41,-39],[-60,-20],[-62,16]],[[7200,9162],[3,19]],[[7203,9181],[25,26],[1,82],[55,102],[53,46],[96,-4],[101,19]],[[7203,9181],[-28,-4]],[[4726,6396],[58,53]],[[4784,6449],[52,-16],[55,16],[-21,43],[24,52]],[[4923,6624],[46,-23],[-9,-143],[45,-60]],[[4784,6449],[-31,18],[18,99],[-32,34]],[[5014,6405],[41,113],[6,156],[25,52],[-17,51]],[[5118,6792],[87,-36],[68,13],[100,-74],[67,-30],[113,24],[24,-7],[48,-110],[25,0]],[[4848,6961],[60,75],[138,76]],[[5046,7112],[79,-44],[34,-40],[-74,-73],[40,-65]],[[5125,6890],[-32,-21]],[[5125,6890],[35,3],[71,122],[48,-15],[105,120]],[[5144,7230],[-49,-27],[-49,-91]],[[14391,14948],[74,-12]],[[14465,14936],[37,-43],[60,-14]],[[10076,12965],[-94,-67]],[[9982,12898],[-59,-25],[-32,29]],[[9891,12902],[-39,46],[-17,80]],[[10092,12672],[-46,125],[-64,101]],[[9637,12460],[1,98],[-25,69],[48,40],[-22,42],[74,40]],[[9713,12749],[24,14]],[[9737,12763],[107,-184]],[[9844,12579],[-63,-64]],[[10048,12622],[-78,15]],[[9970,12637],[-100,143]],[[9870,12780],[16,39],[-31,52],[36,31]],[[9737,12763],[81,47],[52,-30]],[[9970,12637],[-41,-31],[-85,-27]],[[9713,12749],[-10,26],[-78,42],[-33,40]],[[13304,13086],[4,38],[92,39],[94,-34],[20,-61]],[[13259,13326],[21,-10],[131,32]],[[13411,13348],[140,-56],[32,-60],[36,-12]],[[8764,13055],[25,-55],[61,26],[20,30],[60,-10]],[[8930,13046],[-6,-71],[57,-138],[9,-64],[-30,-142]],[[8927,12626],[-52,97],[30,16],[-20,52],[-54,52],[-91,27],[-27,30],[-78,-31],[-26,28]],[[9305,12696],[-35,112],[18,69],[30,28],[-18,165]],[[9300,13070],[122,-35],[56,-42],[36,21],[5,42],[37,27]],[[9556,13083],[27,-94],[39,-56],[54,20]],[[8670,12559],[-50,87],[27,11],[-47,75],[-158,-37],[-14,29]],[[9556,13083],[153,62]],[[9169,12647],[-50,170],[-99,248]],[[9020,13065],[66,14],[33,-14],[102,19],[79,-14]],[[8930,13046],[90,19]],[[13149,13825],[77,-52]],[[13226,13773],[43,-90]],[[13269,13683],[-3,-33]],[[13266,13650],[17,-47],[116,12]],[[13399,13615],[-40,-49]],[[13359,13566],[-87,-28],[-13,-26],[-4,-104],[-42,-16]],[[13226,13773],[13,48],[66,15]],[[13305,13836],[35,-46],[-71,-107]],[[7960,11355],[50,-120]],[[8010,11235],[-45,-10],[-96,-81]],[[7869,11144],[-24,28],[-7,139]],[[13581,13620],[-87,47]],[[13494,13667],[59,82]],[[8325,11743],[81,29]],[[8467,11638],[-160,-52],[-67,25]],[[8002,11396],[77,41],[35,-67],[-47,-33],[28,-54]],[[8095,11283],[-85,-48]],[[7871,11134],[-2,10]],[[8095,11283],[43,36],[45,7]],[[8183,11326],[30,-65]],[[13314,13874],[-9,-38]],[[8183,11326],[30,27],[74,10],[19,-62]],[[13475,13435],[-66,59],[-50,72]],[[13399,13615],[50,52],[45,0]],[[8453,14382],[-26,-110]],[[8427,14272],[-92,48]],[[9348,13827],[-48,11],[-35,35],[-27,65],[-69,40]],[[9169,13978],[21,40],[78,68],[2,39]],[[9270,14125],[35,10]],[[9305,14135],[13,-43],[63,-67]],[[9111,14266],[32,-95],[-20,-9]],[[9123,14162],[-61,-22],[-31,-34]],[[9259,14299],[1,-94],[76,-21],[-31,-49]],[[9270,14125],[-116,7],[-31,30]],[[9169,13978],[-35,-33],[-162,-44]],[[8427,14272],[79,-44]],[[8321,14079],[-42,82],[43,91],[-40,11]],[[8107,14167],[84,81]],[[13411,13348],[44,40],[20,47]],[[13266,13650],[47,11],[35,-26],[39,47],[68,30],[48,56]],[[10398,13463],[7,-2]],[[10405,13461],[-15,-70],[-36,-22],[-41,-97]],[[10851,13779],[-6,-5]],[[10710,13661],[66,-4],[19,-103],[76,-125],[22,-11]],[[10893,13418],[-17,-47]],[[10876,13371],[-71,20],[-60,-5],[-21,-26]],[[10724,13360],[-91,55],[-72,8],[-71,29]],[[10490,13452],[-14,18]],[[10476,13470],[57,31],[31,82]],[[10864,13329],[12,42]],[[10893,13418],[81,76]],[[10974,13494],[17,-37],[63,-27]],[[10733,13263],[-9,97]],[[14375,15717],[-29,45],[16,50],[50,-10],[35,-79],[31,-14]],[[14490,15596],[-40,17]],[[10405,13461],[71,9]],[[10490,13452],[47,-28],[-12,-56],[77,1],[7,-23]],[[10974,13494],[-12,121],[-76,68],[-37,57],[-30,9]],[[9175,11669],[-36,75]],[[9139,11744],[17,58],[51,64]],[[9207,11866],[67,-17]],[[8559,11675],[35,38],[-38,89],[-53,66],[-30,12],[-44,-75]],[[8592,11990],[34,-51],[37,22],[53,-3],[54,-38]],[[8770,11920],[-1,-126]],[[8769,11794],[-49,-30],[-37,14],[-18,-63]],[[9207,11866],[-18,63],[-52,47]],[[9137,11976],[9,12]],[[9151,12063],[-87,-14],[-27,19],[-41,-53],[-106,11]],[[8890,12026],[8,57]],[[8898,12083],[-3,95],[-67,-3],[-3,28]],[[8770,11920],[6,58]],[[8776,11978],[12,35],[60,-1],[6,42],[44,29]],[[8890,12026],[-7,-66],[35,-48]],[[8918,11912],[-32,-84]],[[8886,11828],[-70,-61]],[[8816,11767],[-47,27]],[[9139,11744],[-39,39]],[[9100,11783],[17,77],[-47,30]],[[9070,11890],[14,61],[53,25]],[[8816,11767],[0,-4]],[[8816,11763],[-62,-78]],[[8900,11766],[41,-23],[63,5],[22,-25]],[[9026,11723],[8,-51],[-60,-6]],[[8974,11666],[-100,46]],[[8874,11712],[26,54]],[[9139,11744],[-48,-7]],[[9091,11737],[-135,62],[2,69]],[[8958,11868],[44,29],[53,-12]],[[9055,11885],[10,-82],[35,-20]],[[9055,11885],[15,5]],[[8886,11828],[14,-62]],[[8874,11712],[-57,21]],[[8817,11733],[-1,30]],[[8776,11978],[-46,152]],[[8817,11733],[16,-154]],[[8918,11912],[40,-44]],[[9091,11737],[-65,-14]],[[8974,11666],[19,-62]],[[8721,12567],[-19,-48],[-93,-2],[76,-98]],[[8685,12419],[-39,-69]],[[8646,12350],[-77,77],[-51,1],[-68,34]],[[8268,11995],[-35,20],[-14,84]],[[8219,12099],[70,25],[-4,45]],[[8285,12169],[75,-30]],[[8360,12139],[13,-33],[65,-30]],[[7975,11821],[-37,2],[-103,205],[-54,57],[-31,76],[29,43],[79,32]],[[7858,12236],[87,-70],[41,-10],[35,-57],[-40,-36],[33,-79],[-16,-22],[15,-80],[64,-29]],[[8581,12139],[17,68],[-15,69],[36,14]],[[8619,12290],[6,-40],[49,-38],[23,-53],[-4,-45]],[[8854,12217],[-30,-3],[-14,71]],[[8810,12285],[64,54]],[[7873,12305],[-15,-69]],[[8859,12490],[-43,-18],[17,-83]],[[8833,12389],[-53,-14]],[[8780,12375],[-17,30],[-78,14]],[[8810,12285],[-27,35],[-3,55]],[[8833,12389],[40,-4]],[[8360,12139],[37,37],[90,48]],[[8487,12224],[-2,-95]],[[8219,12099],[-34,-11],[-62,23],[-42,188],[79,29]],[[8160,12328],[34,1],[24,-84],[67,-76]],[[8436,12441],[-20,-35],[12,-83],[59,-99]],[[8160,12328],[-66,65]],[[8619,12290],[39,8],[-12,52]],[[10238,12695],[-48,45]],[[10190,12740],[25,-5],[1,115],[28,40]],[[10244,12890],[58,-16],[104,21]],[[10406,12895],[1,-18]],[[10896,13333],[103,-114],[9,-102],[-36,-21]],[[10972,13096],[-110,-6]],[[10862,13090],[-1,67],[-31,49]],[[10681,13053],[-6,63],[23,69],[-34,61]],[[10664,13246],[3,7]],[[10862,13090],[-67,-50],[-24,10]],[[11183,13334],[-42,99],[4,25]],[[11008,13049],[-36,47]],[[11021,13320],[66,-1],[21,-43],[47,29]],[[10066,12984],[77,28]],[[10143,13012],[50,-85],[51,-37]],[[10218,13265],[3,-35],[-33,-60],[72,-76],[19,-48]],[[10279,13046],[-83,-45],[-53,11]],[[10279,13046],[28,1],[52,-72]],[[10359,12975],[47,-41]],[[10406,12934],[0,-39]],[[10406,12934],[48,36],[-20,77],[-37,63],[44,27],[-64,97],[9,27]],[[10664,13246],[-24,-33],[-12,-82],[-30,-49]],[[10235,13268],[66,-149],[56,-91],[2,-53]],[[14465,14936],[26,22],[13,98],[59,51]],[[14563,15107],[52,1]],[[14615,15108],[37,-124],[10,-68]],[[14662,14916],[0,-5]],[[8299,13479],[35,-58],[-16,-63]],[[8563,13272],[78,-7],[15,54],[37,49],[35,-3]],[[8728,13365],[71,-14],[46,15],[63,-33],[63,3]],[[8971,13336],[70,68],[75,17]],[[8783,13715],[20,-27]],[[8803,13688],[11,-61]],[[8814,13627],[-78,-29],[-16,-48]],[[8720,13550],[-62,31],[-153,23]],[[8720,13550],[50,-26]],[[8770,13524],[-47,-50]],[[8723,13474],[-35,-23],[-75,-2],[-75,-24],[-74,11]],[[8464,13436],[-22,67]],[[8451,13370],[13,66]],[[8723,13474],[5,-109]],[[8814,13627],[20,-71]],[[8834,13556],[-64,-32]],[[8890,13724],[-87,-36]],[[8834,13556],[41,-65],[24,-79],[70,-49]],[[8969,13363],[2,-27]],[[8969,13363],[26,42],[-77,172],[-59,16],[-45,34]],[[14666,15848],[-49,3]],[[14617,15851],[-108,60],[-38,59]],[[9022,15051],[-19,-54],[63,-92]],[[9185,14913],[44,103],[80,-7],[19,54]],[[8900,14100],[-23,-12],[-114,41],[-92,12]],[[8671,14141],[6,72],[27,76]],[[8704,14289],[9,-2]],[[8713,14287],[50,-35],[143,37],[20,20]],[[8436,14671],[-63,84]],[[8647,14766],[-39,-83]],[[8608,14683],[-46,-50],[-43,-1]],[[8924,14322],[5,120]],[[8929,14442],[25,44],[145,-50],[46,-35]],[[9145,14401],[18,-38]],[[8478,14530],[64,-10],[4,-73]],[[8546,14447],[-25,-61]],[[8801,14657],[-22,-14],[-108,-12]],[[8671,14631],[-63,52]],[[9135,14834],[-73,-18],[-24,-81],[-32,15]],[[8831,14575],[-42,-80]],[[8789,14495],[-67,50],[-51,86]],[[8547,14285],[55,37],[102,-33]],[[8671,14141],[-27,-52]],[[8999,14725],[9,-137],[43,-26],[87,-111],[7,-50]],[[8929,14442],[-14,70],[-48,21],[-7,34]],[[8789,14495],[8,-24]],[[8797,14471],[-97,-19],[-154,-5]],[[8713,14287],[32,73],[94,60],[10,44],[-52,7]],[[8644,14089],[-48,-17],[2,-88]],[[8598,13984],[-52,-31],[-110,-24],[-58,32]],[[8378,13961],[-67,101],[-46,-2]],[[8491,13693],[-21,17]],[[8470,13710],[-51,48],[55,30]],[[8474,13788],[142,114],[-6,48]],[[8610,13950],[47,-4],[63,-54]],[[8103,13998],[-91,-27]],[[8378,13961],[46,-76],[-15,-80]],[[8409,13805],[-142,29]],[[8267,13834],[-29,26],[18,80],[-23,54],[-86,19]],[[8934,13862],[-57,37],[3,64]],[[8720,13892],[49,67]],[[8132,13600],[-20,106]],[[8112,13706],[45,-4],[99,59],[11,73]],[[8409,13805],[65,-17]],[[8470,13710],[-129,-106],[-10,-44],[-39,-41],[15,-43]],[[8064,13714],[48,-8]],[[8610,13950],[-12,34]],[[14259,15182],[32,22],[45,-19],[67,20]],[[14403,15205],[55,-41],[55,-17]],[[14513,15147],[9,-30]],[[14522,15117],[-60,-16],[-59,-57],[2,-53],[-36,-33]],[[9922,14418],[-35,-11],[-69,-77]],[[9818,14330],[-53,8],[-62,97]],[[9703,14435],[-33,10],[-19,74],[29,38]],[[9680,14557],[20,50]],[[9616,14651],[-33,-17]],[[9583,14634],[-44,-49]],[[9539,14585],[-32,106],[-118,-32],[-33,12]],[[9553,13810],[0,91],[-70,79]],[[9483,13980],[33,30]],[[9516,14010],[112,127]],[[9628,14137],[77,-30]],[[9705,14107],[49,-59]],[[9754,14048],[-24,-57],[12,-60]],[[9247,14381],[40,45],[72,-10],[151,91],[19,53]],[[9529,14560],[21,-165]],[[9550,14395],[-60,-19],[-105,30],[-118,-117]],[[9703,14435],[-36,-46],[-89,26],[-28,-20]],[[9529,14560],[10,25]],[[9583,14634],[41,-88],[56,11]],[[9892,14264],[-18,9]],[[9874,14273],[-56,57]],[[9855,14056],[-101,-8]],[[9705,14107],[-19,65],[36,62],[63,16],[31,48],[58,-25]],[[9628,14137],[-60,30],[-83,-9]],[[9422,14047],[94,-37]],[[9483,13980],[-44,-28],[-91,-125]],[[10765,13947],[-59,0],[-73,59]],[[10633,14006],[43,68],[53,26]],[[10158,14231],[12,-10]],[[10170,14221],[23,-37],[17,-103],[-14,-25]],[[10196,14056],[-92,46]],[[10104,14102],[9,94],[45,35]],[[10288,13665],[-125,-40]],[[10163,13625],[27,66],[-7,52]],[[10183,13743],[29,21]],[[10212,13764],[26,-5]],[[10238,13759],[50,-94]],[[10487,14425],[4,-16],[-138,-57]],[[10353,14352],[-27,-37]],[[10326,14315],[-49,11],[-58,47]],[[10219,14373],[-26,87]],[[10193,14460],[22,52],[83,31],[12,62]],[[10231,13938],[-32,-30],[-59,11]],[[10140,13919],[1,65],[55,30]],[[10196,14014],[35,-76]],[[10170,14221],[127,15]],[[10297,14236],[18,-81],[34,-33],[56,-16],[16,-34]],[[10421,14072],[-36,-23],[9,-68]],[[10394,13981],[-69,-108],[-34,1]],[[10291,13874],[-39,36]],[[10252,13910],[-21,28]],[[10196,14014],[0,42]],[[10402,13610],[-114,55]],[[10238,13759],[77,-3],[27,58]],[[10342,13814],[49,-47],[24,-60]],[[10415,13707],[35,-52]],[[10542,14284],[-56,-49]],[[10486,14235],[-59,17],[-21,65],[-53,35]],[[9846,14210],[31,21],[79,-100],[-63,-122]],[[9893,14009],[-29,-7]],[[10122,13762],[61,-19]],[[10163,13625],[-50,-6]],[[10500,13892],[42,-75]],[[10542,13817],[27,-20]],[[10569,13797],[-81,-57],[-42,16]],[[10446,13756],[19,92],[35,44]],[[10193,14460],[-45,17],[-4,50]],[[10011,14384],[-7,-71]],[[10004,14313],[-77,-35]],[[10731,13710],[-40,59],[-93,-10],[-4,25]],[[10594,13784],[49,77],[-10,145]],[[10486,14235],[-31,-58],[24,-78]],[[10479,14099],[15,-63],[-35,-35]],[[10459,14001],[-38,71]],[[10297,14236],[29,79]],[[10158,14231],[-9,28]],[[10149,14259],[51,64],[19,50]],[[10633,14006],[-70,66],[-84,27]],[[10212,13764],[9,77],[31,69]],[[10291,13874],[51,-60]],[[10394,13981],[75,-81],[31,-8]],[[10446,13756],[-31,-49]],[[10004,14313],[68,-49],[77,-5]],[[10104,14102],[-54,-71]],[[10050,14031],[-64,44],[-65,-18],[-28,-48]],[[10459,14001],[21,-64],[52,-35],[25,-52],[-15,-33]],[[9902,13826],[42,10],[33,56],[60,46],[89,-2]],[[10126,13936],[14,-17]],[[10140,13919],[-11,-134]],[[10126,13936],[-5,25],[-71,70]],[[10594,13784],[-25,13]],[[9587,13765],[31,-27],[-16,-90]],[[9602,13648],[-65,21],[-49,-12],[-35,25],[-45,-13]],[[9408,13669],[9,29]],[[9857,13409],[-54,97],[-9,66]],[[9794,13572],[13,122]],[[9807,13694],[42,-25],[72,38]],[[9921,13707],[34,-50],[-6,-111],[-33,-33]],[[9916,13513],[2,-102]],[[9918,13411],[-61,-2]],[[9233,13675],[40,-66],[-24,-24]],[[9249,13585],[1,-31],[-51,-106],[-84,-10]],[[10181,13538],[-135,33],[-20,-44],[-110,-14]],[[9921,13707],[-8,99]],[[10005,13291],[-41,44],[16,32],[-62,44]],[[9807,13694],[-63,34]],[[9744,13728],[57,98],[-29,79],[-30,26]],[[9744,13728],[-96,-10],[-4,63]],[[9587,13328],[162,9],[90,73]],[[9839,13410],[18,-1]],[[9249,13585],[100,-45],[17,-75],[-3,-102]],[[9580,13474],[-84,-50]],[[9496,13424],[-5,84],[-24,31]],[[9467,13539],[40,-3],[81,37]],[[9588,13573],[15,-49],[-23,-50]],[[9794,13572],[-1,74],[-164,-11],[-9,-30]],[[9620,13605],[-18,43]],[[9839,13410],[-34,44],[-160,45],[-65,-25]],[[9588,13573],[32,32]],[[9408,13669],[5,-34],[47,-54],[7,-42]],[[9496,13424],[-49,-7],[-15,-46]],[[14617,15851],[-33,-21],[-158,7]],[[13839,16070],[4,-23],[-140,-36]],[[13998,15952],[-29,16],[-56,-19],[-15,38],[3,80]],[[14543,16556],[65,1]],[[14608,16557],[10,-64],[-31,-83]],[[14587,16410],[-49,-26],[-36,-45]],[[14869,16206],[67,-3]],[[14608,16557],[69,29],[64,-61],[40,-58],[-37,-93]],[[14744,16374],[-24,41],[-133,-5]],[[14744,16374],[-15,-39]],[[15105,15391],[-26,51]],[[15079,15442],[14,90]],[[14381,11970],[25,54],[48,46],[-65,89],[34,42],[3,64],[50,51],[-27,82]],[[14393,16679],[-55,32],[-140,-8],[-25,-13]],[[14048,16293],[51,-49],[121,-3],[8,-63],[39,-49],[-3,-50],[57,-32],[17,-63]],[[14338,15984],[-58,-54],[-77,-10],[1,-100],[-34,-30],[-70,-5]],[[13962,15120],[77,-32],[84,-78],[-32,-102],[10,-35]],[[14064,15197],[18,-76],[-41,-15],[-39,47]],[[14458,16374],[-126,-34],[-74,79],[-113,24],[-16,17]],[[14338,15984],[68,6],[27,-29]],[[15531,14496],[66,17],[47,-14]],[[14020,16415],[-125,67],[-75,31],[-12,43],[57,45]],[[13990,16353],[-41,-4],[-63,-40]],[[15373,16250],[-38,-100],[8,-85]],[[15657,15874],[-15,-30],[-64,-48]],[[15404,15852],[65,75],[110,16],[27,43],[-15,53],[50,18]],[[13514,16213],[46,-70],[-14,-29],[100,-129]],[[13557,15897],[-35,-34],[-64,72],[-57,-7],[-70,-32]],[[13331,15896],[-4,2]],[[13331,15896],[52,-74],[47,-26],[4,-77],[47,-57]],[[13663,12762],[-32,9],[-133,112],[-60,30]],[[14706,15082],[-55,31],[-36,-5]],[[14563,15107],[-41,10]],[[14513,15147],[55,-18],[12,57],[61,92],[37,-10],[57,-73],[91,1]],[[14826,15196],[10,-7]],[[18221,13600],[2,-159],[-21,-57]],[[18202,13384],[-74,-43]],[[18128,13341],[-77,30],[-43,-42],[-114,-41],[-17,-20]],[[17877,13268],[-118,70],[-26,27],[-76,15]],[[18129,12451],[32,103],[-50,24],[65,64],[-69,47],[5,102],[29,55],[-27,109],[-53,9],[-23,83],[21,126]],[[18059,13173],[8,97],[48,21],[13,50]],[[18202,13384],[2,-34],[74,-41],[12,-38],[71,-40]],[[17722,13044],[63,-31],[48,11]],[[17833,13024],[-21,-86],[-73,-51],[-101,-39]],[[17877,13268],[39,-136]],[[17916,13132],[-18,-45],[-66,-40],[1,-23]],[[17916,13132],[54,-4],[89,45]],[[7159,10572],[-80,-5]],[[7079,10567],[6,88],[23,24],[-69,65]],[[7039,10744],[41,17],[69,76],[66,13]],[[7215,10850],[68,-27],[25,-28]],[[7308,10795],[-40,-66],[12,-32],[-108,-76],[-13,-49]],[[7079,10567],[-60,-58]],[[7019,10509],[-53,19],[5,42],[-68,52],[-53,16],[-56,-50]],[[6794,10588],[-21,53],[31,24]],[[6804,10665],[37,4],[19,57],[141,1],[-1,40]],[[7000,10767],[39,-23]],[[18629,13416],[62,59],[39,-1],[78,58]],[[18808,13532],[52,-15],[41,24]],[[18901,13541],[15,-60]],[[18916,13481],[-104,-61],[-24,-62]],[[18916,13481],[62,37],[90,-30],[23,25],[114,25]],[[18959,13738],[-43,-82],[-15,-115]],[[18808,13532],[2,66],[-34,2],[-13,54]],[[7658,10796],[-108,-3],[-67,-64]],[[7483,10729],[-42,20],[-23,103]],[[7418,10852],[35,144],[88,91]],[[7541,11087],[70,19],[16,-24]],[[7627,11082],[-13,-38],[46,-38],[55,25],[27,-76]],[[7742,10955],[-33,-132],[-51,-27]],[[19292,14013],[-29,-66],[5,-86],[-26,-26]],[[19242,13835],[-47,42],[-58,-4],[-49,21],[-59,-16],[-39,27]],[[19480,13852],[-62,-15]],[[19307,13784],[-60,25]],[[19247,13809],[-5,26]],[[19305,14082],[107,33]],[[19450,14115],[-39,-25],[-45,-141],[42,-11],[73,-67]],[[19644,14065],[-9,-41]],[[19635,14024],[-33,32],[-106,-4],[3,53]],[[19247,13809],[-37,-31],[-95,12],[-103,-48]],[[19521,13911],[37,33],[66,18],[11,62]],[[6612,10304],[-42,47],[45,76]],[[6615,10427],[16,-18],[73,69]],[[6704,10478],[59,52],[31,58]],[[7019,10509],[18,-85],[-76,-28],[-12,-59],[111,-153]],[[7000,10767],[-2,40]],[[6998,10807],[49,115]],[[7047,10922],[16,39]],[[7063,10961],[38,72]],[[7101,11033],[62,86],[18,-3]],[[7181,11116],[38,-18],[69,56],[19,-49]],[[7307,11105],[-68,-112],[-30,-120],[6,-23]],[[7307,11105],[31,98]],[[7338,11203],[14,42]],[[7352,11245],[92,-21],[3,-89],[32,-27],[48,4]],[[7527,11112],[14,-25]],[[7418,10852],[-66,-11],[-44,-46]],[[7767,10505],[15,-98],[-36,-53],[82,-40],[41,-79]],[[7869,10235],[-64,13],[-48,69],[-76,60],[-41,8],[-78,-52],[-58,53],[-113,-17],[-53,5]],[[7159,10572],[23,-25],[71,-133],[-77,-54],[-22,-79],[10,-35]],[[14662,14916],[87,53],[64,7],[18,-25]],[[7983,10611],[-20,33],[-140,46],[-5,66],[-48,-2],[-112,42]],[[7742,10955],[75,-4],[44,18],[63,86],[-39,89]],[[19051,13167],[-152,-33],[-81,-35],[-64,3],[-63,-73]],[[18691,13029],[3,-72],[-35,-25],[-8,-54],[-130,-50]],[[18521,12828],[7,76],[-58,90],[43,68],[73,23],[13,82]],[[18599,13167],[21,30],[153,85],[42,-9]],[[18316,13148],[100,-26],[59,33],[41,-6],[83,18]],[[18521,12828],[-102,-16],[-181,-96],[-28,-69]],[[18463,12298],[114,57],[64,56],[84,15],[86,91],[111,39],[60,73],[-57,122],[-117,116],[-15,61],[-50,67],[-52,34]],[[7575,10483],[-72,61],[-15,53],[-39,3],[-12,88],[46,41]],[[18174,14477],[77,-59],[44,47]],[[18450,14350],[3,-45]],[[18447,14283],[-63,-8],[-28,-38]],[[18356,14237],[-77,11],[-84,84]],[[18051,13571],[-18,62],[37,32],[-11,80],[55,28]],[[18114,13773],[11,-60],[105,37],[114,64],[68,61],[-19,41],[25,44]],[[18418,13960],[100,66],[4,-37],[110,31]],[[18783,13921],[29,-45]],[[18418,13960],[-19,78]],[[18399,14038],[34,120],[-35,58],[-42,21]],[[9872,12398],[62,-63]],[[9934,12335],[-12,-56]],[[9922,12279],[-74,-91],[-48,-30]],[[9800,12158],[-55,-11],[-79,9],[-54,-17]],[[9612,12139],[-6,68]],[[9606,12207],[59,60],[24,62],[55,32]],[[9744,12361],[108,16],[20,21]],[[18739,14381],[-30,-39],[50,-61]],[[18759,14281],[-69,-41],[-6,-27]],[[19071,14255],[2,-63]],[[19073,14192],[-27,-93]],[[19046,14099],[-77,28],[-106,-7]],[[18863,14120],[46,54]],[[18909,14174],[61,106]],[[18759,14281],[18,-15],[81,21]],[[18858,14287],[20,-94],[31,-19]],[[18863,14120],[-28,-67],[-53,-48]],[[19223,14244],[-20,-63],[-70,-19],[-60,30]],[[19065,14016],[-19,83]],[[19324,14255],[16,-109]],[[18891,14344],[-33,-57]],[[10221,11555],[45,-30],[-27,-40],[2,-76]],[[10241,11409],[-69,-48],[-48,-10]],[[10124,11351],[-35,130],[-108,9]],[[9981,11490],[22,35],[84,-28],[64,2],[30,45],[40,11]],[[9606,12207],[-87,-17]],[[9519,12190],[-12,10]],[[9507,12200],[-33,46]],[[9474,12246],[34,52]],[[9508,12298],[81,70],[90,0]],[[9679,12368],[65,-7]],[[19980,13729],[-88,6],[-37,-84],[-30,-9]],[[19825,13642],[18,59]],[[19843,13701],[26,115],[-23,97]],[[19866,13920],[113,22]],[[19730,13711],[113,-10]],[[19825,13642],[20,-40]],[[19845,13602],[-53,-40],[28,-38]],[[19820,13524],[-71,-97]],[[19749,13427],[-48,56],[29,21],[-56,113],[-62,51]],[[19837,13321],[59,81],[-89,33],[13,89]],[[19845,13602],[39,-24],[-20,-45],[52,-24],[68,124]],[[19676,14112],[-30,-36]],[[9731,11686],[-35,29]],[[9696,11715],[-60,112]],[[9636,11827],[55,63],[94,73]],[[9785,11963],[45,17],[51,-21]],[[9881,11959],[-84,-31],[-28,-38],[55,-38]],[[9824,11852],[-33,-28],[-12,-70],[-48,-68]],[[10032,11837],[47,46]],[[10079,11883],[21,43],[41,22],[68,-25]],[[10209,11923],[-28,-78],[-56,-34]],[[10125,11811],[-45,-12],[-48,38]],[[19143,12833],[28,5]],[[19171,12838],[32,-55],[92,-74],[-31,-96],[68,-15],[67,10]],[[19463,13568],[24,-46],[63,-39],[64,-82],[36,-18],[18,-47]],[[19668,13336],[-72,-115],[-135,-63],[-32,-38],[13,-45],[-52,-68],[-47,9],[-52,75]],[[19291,13091],[92,14],[-32,78],[13,57],[-60,41],[-127,-56]],[[19291,13091],[-112,-3]],[[19526,13603],[48,-47],[78,-37],[2,-85],[67,-46]],[[19721,13388],[-53,-52]],[[19519,12819],[-88,26],[-14,-16],[-116,3],[-78,37],[-52,-31]],[[19721,13388],[28,39]],[[10366,11364],[-36,28],[-89,17]],[[10221,11555],[-17,45]],[[10204,11600],[51,-3]],[[10255,11597],[62,57],[-9,30],[76,12]],[[19184,11545],[-61,-18],[-61,8],[-65,-13],[-46,60],[-53,2],[-54,-61],[-47,8],[-186,-68]],[[18611,11463],[12,72],[64,18],[51,36],[82,16],[60,53],[59,12],[39,61],[57,218]],[[19035,11949],[12,50]],[[19047,11999],[64,-40]],[[18631,12236],[44,32],[139,-61],[45,-43],[23,-49],[207,-24],[-42,-92]],[[19035,11949],[-174,-10],[-87,-20],[-43,25],[-84,-19],[-92,27]],[[18611,11463],[-14,-50]],[[19256,11285],[-150,-50],[-81,21],[-87,57],[-65,6],[-38,-35],[-73,7],[-88,-44]],[[19292,10129],[32,89],[-77,46],[2,43],[-97,21],[-48,-62],[-35,6],[-84,-41],[-21,-31],[-53,-16],[-76,4],[-55,-18],[-17,-42],[2,-78],[-77,-11],[-67,48]],[[18621,10087],[-10,33]],[[18611,10120],[83,54],[112,11],[10,102],[57,23],[95,155],[44,47],[-24,22],[49,56],[31,-27],[51,61]],[[17650,10620],[-10,84],[-20,37],[44,32],[6,74],[70,41],[45,5],[114,40],[39,95],[45,39],[1,37],[45,20],[11,63],[54,21]],[[18094,11208],[35,-28],[105,23],[81,-48],[39,16],[63,-31],[70,-78],[37,31],[20,51]],[[18635,11012],[-58,-68],[-151,-33],[-20,-34],[-68,-50],[-43,-68],[-54,29],[-68,-14],[-82,-78],[-2,-42],[-32,-58],[19,-60],[-22,-77],[-62,-14],[-35,-52],[-92,9],[-64,-47]],[[18785,10876],[-62,-24],[5,-41],[100,-121],[130,10],[30,64],[55,-12]],[[19084,10679],[-122,-74],[-39,28],[-100,40],[-59,-15],[-62,-104],[-14,33],[-52,17],[-54,-118],[-25,-7],[-57,-87],[22,-103],[46,-37],[53,-85],[-10,-47]],[[18621,10087],[-50,-77],[-53,-15],[-14,-82],[-52,-35],[-60,27],[-29,-18],[-65,1],[-110,56],[-53,-13],[-60,-53],[-74,-10],[-63,-51],[-11,-30],[-98,-26]],[[18048,11295],[54,-44],[-8,-43]],[[17749,11558],[19,-62],[35,-18],[19,-70]],[[14403,15205],[107,9],[17,48],[118,50],[17,41],[-5,104]],[[14657,15457],[81,4],[38,-24]],[[14776,15437],[1,-91],[43,-11],[41,-41],[-33,-44],[-2,-54]],[[9881,11959],[-2,22],[143,-10],[10,-76],[47,-12]],[[10032,11837],[-94,20],[-4,-70],[-35,-31]],[[9899,11756],[-24,86],[-51,10]],[[17058,11456],[-25,78],[21,118]],[[17054,11652],[21,51],[16,111],[33,45]],[[16321,12078],[50,-22]],[[16371,12056],[92,-31],[19,14],[66,-45],[41,13],[63,-58]],[[16652,11949],[-63,-116],[-1,-48],[-82,-37]],[[17077,11966],[-26,-76],[-71,-38],[-100,172],[-75,39],[-46,-58],[-61,-7],[-46,-49]],[[16371,12056],[54,63],[67,35],[6,38]],[[16498,12192],[61,-2],[115,37],[33,-43],[-24,-40],[34,-50],[50,43],[73,5],[58,-79],[131,-1]],[[17054,11652],[-109,-65],[-4,-30],[-94,-96],[3,-55]],[[9981,11490],[-59,-85]],[[9922,11405],[-29,0]],[[9893,11405],[-23,29],[24,112],[-73,8],[-70,-102],[-62,19]],[[9689,11471],[54,151]],[[9743,11622],[56,14],[64,64],[36,56]],[[10125,11811],[30,-23],[-2,-65]],[[10153,11723],[-2,-80],[53,-43]],[[10255,11597],[-20,25],[18,85],[-100,16]],[[10209,11923],[66,58]],[[16015,12177],[0,33]],[[16015,12210],[92,29],[108,19]],[[16215,12258],[45,-47],[24,-52],[-9,-41]],[[16215,12258],[143,52]],[[16358,12310],[54,-67],[36,10],[50,-61]],[[15758,12341],[92,-24],[136,-67],[29,-40]],[[16358,12310],[17,34],[78,42],[-32,35],[89,84],[30,-12],[72,27],[76,-4]],[[9596,11273],[41,57]],[[9637,11330],[47,-4],[68,-40],[13,-55]],[[9765,11231],[-34,-76],[-77,12]],[[16859,12376],[40,18],[2,52],[86,36],[36,83],[-8,40],[155,15],[5,31],[101,-9]],[[17276,12642],[37,-44],[105,8],[54,18],[16,-27]],[[17488,12597],[2,-60],[-91,-39],[-89,-187],[-39,-29],[6,-38],[-38,-95],[39,-48],[-7,-43],[-53,-59],[-7,-68],[-26,-41],[-61,-31]],[[17690,11831],[24,1],[12,83],[28,-11],[55,39],[-22,51],[34,40],[36,89],[135,36],[107,47],[16,41],[44,5],[3,44],[46,57]],[[17488,12597],[87,-3]],[[10174,12165],[-5,-51],[-41,-10],[-33,34],[-3,47],[-87,65],[-37,-20],[-46,49]],[[9934,12335],[103,55],[85,19]],[[17276,12642],[11,12],[-45,115],[12,18]],[[17254,12787],[41,20],[60,67],[85,22],[22,28],[68,-1],[64,32]],[[16903,13057],[49,-2]],[[16952,13055],[81,-41],[25,-58],[33,-21]],[[17091,12935],[-6,-18],[-94,6],[-16,38],[-52,-6],[-20,102]],[[16952,13055],[-22,32],[-5,66],[44,95],[70,31],[-23,48],[28,56],[56,-38]],[[17100,13345],[49,-105],[7,-60],[50,-24],[-57,-66],[-47,-16],[46,-80],[-22,-58]],[[17126,12936],[-35,-1]],[[17254,12787],[-37,31],[10,62],[-74,18],[-27,38]],[[17100,13345],[39,70]],[[17139,13415],[21,35],[-19,39],[48,52],[50,-45],[43,46]],[[16570,12769],[81,96],[-13,79],[59,46],[44,90],[-17,17]],[[16724,13097],[41,-40],[-43,-26],[27,-41],[141,46],[13,21]],[[10124,11351],[-20,-13]],[[10104,11338],[-28,25],[-102,-1],[-52,43]],[[9900,11146],[30,141],[-13,44]],[[9917,11331],[-24,74]],[[10104,11338],[-32,-82],[-62,-96],[12,-24]],[[16883,13693],[-34,-55],[23,-35],[-60,-78]],[[16812,13525],[-93,-65],[-54,-10],[-16,-35],[-61,4],[-7,60],[-54,5],[-40,-21],[-19,-45],[-42,-28],[-60,12],[-48,39],[-12,84]],[[17139,13415],[-48,2],[-20,40],[-120,33],[5,45],[-72,-13],[-20,-58],[-64,10]],[[16800,13474],[12,51]],[[16800,13474],[-23,-62],[-3,-68],[24,-41],[-26,-30],[20,-47],[-70,-41],[-35,-43],[37,-45]],[[14719,15752],[-42,6]],[[9743,11622],[-12,64]],[[17236,13610],[-3,48],[67,11]],[[17300,13669],[65,-21],[81,-9],[28,-101],[108,-10],[61,-56]],[[17300,13669],[133,122],[18,32]],[[17451,13823],[55,-15],[30,-98],[67,-29],[21,13]],[[16799,13972],[19,24]],[[16818,13996],[30,-24],[42,25],[59,-66],[78,-43]],[[17027,13888],[-32,-62],[51,-1],[33,-61],[-15,-34],[55,-36],[-6,-51]],[[17027,13888],[115,8],[79,-81],[33,25],[4,60],[33,13]],[[17291,13913],[36,-10],[124,-80]],[[16818,13996],[29,43],[49,24]],[[16896,14063],[157,29],[52,23]],[[17105,14115],[30,-55],[61,-20],[79,1],[16,-128]],[[9890,12430],[-18,-32]],[[9679,12368],[59,77]],[[9800,12158],[-36,-23],[27,-61],[-6,-111]],[[9636,11827],[-42,89]],[[9594,11916],[15,93],[-56,6],[-7,32]],[[9546,12047],[66,92]],[[17672,13973],[-38,-17],[-134,9],[-54,85],[-15,77]],[[17431,14127],[64,37],[95,80]],[[17431,14127],[-60,-27],[-18,40],[-66,16],[-22,-88],[-51,27],[-72,2],[-36,24]],[[17106,14121],[62,57],[23,45],[-34,58]],[[17105,14115],[1,6]],[[16896,14063],[-45,60],[-44,27],[92,47],[61,11],[-11,56]],[[9917,11331],[-58,28],[-57,-34],[-6,-71],[-31,-23]],[[9637,11330],[-23,45],[75,96]],[[18064,13850],[-23,-22],[-104,4],[-72,116],[19,55]],[[17884,14003],[44,-17],[32,37],[63,-8],[0,-33],[67,-49],[14,-34],[-40,-49]],[[18114,13773],[-54,35],[4,42]],[[18064,13850],[62,-9],[14,95],[54,59],[61,34],[63,-19],[81,28]],[[17787,14023],[64,16],[33,-36]],[[14316,15309],[23,-71],[40,4],[34,116],[46,61],[-52,41],[46,45],[67,24],[-18,42]],[[14606,15545],[52,-59],[-1,-29]],[[19690,8263],[-86,-81],[-23,-104],[-112,-15],[-33,-133],[-47,-75]],[[19389,7855],[-65,-99],[-68,-52],[-42,4]],[[7241,9723],[17,42],[44,29],[3,77]],[[7305,9871],[87,21],[27,25],[72,-3],[64,51],[27,-8],[71,75]],[[7653,10032],[30,-2],[30,49],[161,28]],[[7874,10107],[86,32]],[[7960,10139],[56,-42],[24,104]],[[8040,10201],[54,14]],[[8094,10215],[13,-48],[70,-12],[12,-66],[67,30]],[[8256,10119],[16,-55],[-33,-14],[23,-103],[-5,-36],[61,-44]],[[8318,9867],[-45,-34],[-48,6],[-42,-62],[6,-25]],[[8189,9752],[-63,73],[-60,-39],[-54,4],[-124,49],[-5,-42],[-67,-84],[-76,74],[-105,16],[-41,-43],[-118,-21],[-41,-42]],[[7435,9697],[-33,45],[-80,-22],[-81,3]],[[19780,8225],[-15,-68],[4,-110],[-93,-54],[8,-75],[-35,-75],[-17,-69],[-47,-68],[-55,-9],[-12,40],[-47,10],[-59,57],[-23,51]],[[20252,7822],[-64,-16],[-88,-61]],[[20100,7745],[-48,65],[23,17],[8,85],[-34,27],[-57,166],[59,54]],[[8598,10202],[5,-151],[-40,3]],[[8563,10054],[-45,-21],[-50,11],[-11,44],[-65,-12]],[[8392,10076],[-43,-1],[-59,37]],[[8290,10112],[13,76],[-57,44],[-28,59],[3,45]],[[8221,10336],[117,26],[22,-39],[43,26]],[[8403,10349],[38,30],[47,5]],[[8488,10384],[56,-7]],[[7960,10139],[-17,50],[58,46]],[[8001,10235],[39,-34]],[[7821,10518],[8,-30],[64,5],[87,-49]],[[7980,10444],[42,-30]],[[8022,10414],[-38,-85],[17,-94]],[[7874,10107],[-25,30],[20,98]],[[20100,7745],[-60,-77],[-82,-20],[39,-57],[-39,-217],[-1,-135]],[[19957,7239],[-91,42],[-93,97],[-33,55],[-45,25],[-95,-5],[-18,-26],[-218,44],[-30,-8],[-176,-156]],[[19957,7239],[27,-49],[104,10],[15,-66],[161,-16],[43,-25],[1,-43],[-33,-72],[62,-61],[0,-83],[51,-72],[0,-57]],[[7557,9428],[-16,83],[-68,61],[-3,48],[-35,77]],[[8189,9752],[6,-7]],[[8195,9745],[-70,-95],[-42,-16],[0,-53]],[[8083,9581],[4,-54],[-22,-39],[-77,-37],[22,-72]],[[8010,9379],[-28,-52],[-159,80],[-81,-6],[14,-45],[-38,-25],[22,-54],[-35,-61]],[[7705,9216],[-78,39],[-41,-15]],[[22195,4254],[46,-38],[70,34],[26,106],[136,184],[-33,57],[76,10],[32,42],[63,33]],[[8318,9867],[61,40],[25,46]],[[8404,9953],[14,-32]],[[8418,9921],[51,-70]],[[8469,9851],[14,-33],[-16,-72]],[[8467,9746],[-21,19],[-54,-34],[-22,-63]],[[8370,9668],[-93,74]],[[8277,9742],[-82,3]],[[8010,9379],[-10,-68],[27,-37],[-58,-100]],[[7969,9174],[-8,-4]],[[7961,9170],[-76,-62],[-39,4],[-91,43]],[[7755,9155],[-50,61]],[[17575,8803],[-91,67],[-37,67],[7,59],[-21,78],[-72,113],[7,72],[-13,58]],[[17355,9317],[147,-147],[90,-67],[114,-66],[3,-78],[26,-53],[34,-18],[30,-119],[-25,-100]],[[17774,8669],[-108,11],[-40,71],[-51,52]],[[18046,8854],[-73,-79],[-78,-26],[-22,-41],[-99,-39]],[[17355,9317],[-47,11]],[[8563,10054],[-29,-60],[17,-28],[-42,-42],[-91,-3]],[[8404,9953],[14,90],[-26,33]],[[18123,8789],[-41,-19],[-11,-84],[-93,19],[-23,-32],[-64,-35],[-32,-42]],[[17859,8596],[-80,10]],[[17779,8606],[-5,63]],[[17779,8606],[-112,-55],[-73,-9],[21,120],[-35,66],[-5,75]],[[8905,10165],[43,-18],[28,-73],[-69,-47],[24,-57],[-48,-23]],[[8883,9947],[-27,11],[-50,-40],[-74,-11],[16,-42],[-80,1],[-52,42],[-40,-81],[-107,24]],[[7653,10032],[-32,49],[-103,-34],[-111,15],[-42,19],[-64,-22]],[[7301,10059],[-64,51],[-83,38]],[[16696,6972],[58,-49],[90,1],[77,38],[68,103],[9,49],[119,145],[1,29],[113,43],[23,52],[-21,90],[63,49],[-6,45],[36,68],[38,-11],[59,43],[7,82],[93,-13],[51,37],[123,-83],[32,-123]],[[17729,7567],[-11,-15],[51,-130],[-26,-95],[-47,-30],[80,-81],[-32,-36],[-26,-91],[21,-125],[-20,-60]],[[8022,10414],[44,-34],[138,-38]],[[8204,10342],[4,-25],[-77,-116],[-37,14]],[[16229,7353],[64,13]],[[16293,7366],[17,-17],[126,-17],[36,-28],[24,50],[101,80],[-25,43],[23,47],[-25,26],[-27,97],[13,81]],[[16556,7728],[109,-10],[180,78],[90,4],[79,117],[17,65],[82,81],[27,56],[71,69],[60,26],[57,52],[99,20]],[[17427,8286],[105,-10],[34,26],[70,-15],[2,-47],[-102,-162],[29,-42],[89,-54],[136,-207],[8,-49],[-26,-103],[-43,-56]],[[17859,8596],[36,-72],[-2,-55],[39,-115],[74,-39],[38,-69],[18,-72],[-65,-58],[13,-138],[-45,-95],[-43,-146],[-116,-144],[-77,-26]],[[17427,8286],[-38,33],[-22,57],[33,81],[-117,48],[-119,-30]],[[17164,8475],[-11,106],[-23,42],[-15,99],[-49,82],[-13,56],[12,50],[-66,97],[-14,44],[-82,97],[-16,49],[-55,79],[-17,59]],[[8204,10342],[17,-6]],[[8290,10112],[-34,7]],[[18901,8006],[-7,9]],[[18894,8015],[-23,33]],[[18871,8048],[-31,87]],[[18840,8135],[98,17]],[[18938,8152],[-26,-17],[-11,-129]],[[18701,8692],[23,-64],[46,-22],[-6,-60],[24,-43],[0,-56],[-44,-31]],[[18744,8416],[-13,70]],[[19142,7862],[-77,18],[-50,65],[-114,61]],[[18938,8152],[142,39],[6,37],[117,-5]],[[19169,7835],[-65,-19],[-60,-38],[-67,-16],[3,-41],[54,-47]],[[19034,7674],[-136,23]],[[18898,7697],[14,51]],[[18912,7748],[-57,117],[39,150]],[[18744,8416],[96,-281]],[[18871,8048],[-79,-32],[17,-64]],[[18809,7952],[-35,71],[-48,6],[-20,-57]],[[18706,7972],[-141,5],[-51,24],[-127,-36]],[[18634,7607],[-42,-28]],[[18592,7579],[-23,86],[12,59],[35,42]],[[18616,7766],[28,-110],[-10,-49]],[[18706,7972],[16,-20],[-19,-80]],[[18703,7872],[-192,67],[4,-35]],[[18515,7904],[-107,36],[-21,25]],[[18809,7952],[5,-33],[-49,-130],[-67,-21],[-21,-25]],[[18677,7743],[-5,44],[42,57],[-11,28]],[[19103,7499],[-57,111],[-12,64]],[[18912,7748],[-73,8],[-70,-51],[-7,-45]],[[18762,7660],[-81,-28]],[[18681,7632],[-4,111]],[[18515,7904],[56,-17],[45,-121]],[[18592,7579],[-104,-30],[-69,9],[-53,-52],[-53,-83]],[[18681,7632],[-47,-25]],[[18959,6137],[-6,36]],[[18971,6359],[67,-16],[49,-57],[89,-18],[69,-32],[26,11]],[[18419,6373],[11,52],[-29,55],[42,46],[-6,44],[39,102],[-51,43],[-22,69],[-63,90],[-61,39],[-90,-14],[-25,82],[-46,22]],[[18155,7103],[110,-37]],[[18265,7066],[226,-74],[79,-48],[104,-93],[-3,-110],[16,-19],[-43,-88]],[[18644,6634],[6,-31],[-27,-67],[-10,-90],[-24,-44],[-110,-77]],[[15098,15156],[-48,-37],[-33,68],[-39,-1]],[[14776,15437],[65,50],[64,9]],[[14905,15496],[-6,-37],[68,-62],[30,-2],[82,47]],[[19136,6968],[-43,-47],[-126,15],[-47,-39],[-48,37],[-48,-41],[-10,-67],[-48,-99],[-122,-93]],[[18265,7066],[33,32],[108,-20],[98,14],[57,76],[60,47],[70,25],[118,60]],[[18809,7300],[21,-15],[212,-53],[39,-65],[65,-32]],[[7230,11755],[20,-68],[79,-9]],[[7329,11678],[-67,-47],[6,-43]],[[7268,11588],[-36,-39],[-42,-11],[-11,-46]],[[7179,11492],[-93,-33],[5,-68],[-70,-47]],[[7021,11344],[-12,6]],[[7009,11350],[-32,56],[-61,3]],[[6814,11380],[-81,50]],[[6733,11430],[59,49],[153,65],[24,83],[-93,-27]],[[18898,7697],[-8,-57]],[[18890,7640],[-31,37],[-97,-17]],[[18809,7300],[21,134],[41,37],[-10,60],[17,30],[-23,61],[35,18]],[[6733,11430],[-3,6]],[[18248,8768],[55,21],[58,72],[47,-2],[50,55],[32,9]],[[18490,8923],[12,-17],[146,33],[15,-19]],[[18663,8920],[27,-133]],[[18690,8787],[-215,11],[7,-124],[42,-83],[-68,-34]],[[18660,8686],[30,101]],[[18663,8920],[47,62],[84,19],[48,46]],[[18842,9047],[96,-1],[142,55]],[[18842,9047],[-61,105],[56,43],[42,-18],[45,15],[-74,103],[37,25]],[[17902,9570],[-33,-67],[-12,-96],[193,-50],[38,2],[32,-41],[24,-78],[51,-62],[45,5],[66,-131],[-104,-52],[-14,-128],[7,-65]],[[18490,8923],[64,62],[-49,148],[-42,38],[-19,70],[2,66],[-72,95],[-67,16],[8,108]],[[22436,9061],[-11,-88],[50,-83],[-22,-38],[51,-28],[-24,-79],[28,-61],[114,-22],[10,-186],[-33,-37],[78,-117],[-3,-75],[-38,-69],[112,-119],[88,-20],[49,-54],[40,-81],[90,-13],[93,-50],[54,-3],[44,-27],[95,-21]],[[23301,7790],[12,-115],[66,-89],[-23,-38],[-97,-91],[-95,-2],[57,-134],[-29,-35]],[[24209,9997],[-42,-25],[-61,7],[-115,-80],[-55,-108],[-32,-153],[8,-91],[-23,-25],[-47,36],[-117,11],[-13,53],[-42,63],[-93,59]],[[23126,9310],[63,-4],[45,83],[-13,51],[54,17],[-13,70],[26,63],[-29,55],[1,137]],[[23455,9717],[-34,-38],[1,-55],[33,-40],[58,-22],[41,-66],[76,-77],[35,-103],[-9,-47],[19,-95],[41,-83],[157,-91],[-13,-72],[60,24],[92,-78],[18,-45],[68,-69],[-8,-46],[63,-80]],[[23998,7550],[-43,-37],[-149,-4],[-142,22],[-100,92],[-8,84],[-76,28],[-84,57],[-32,49],[-63,-51]],[[20538,6755],[40,-71],[37,-13],[6,-109],[-62,-94],[3,-54]],[[20562,6414],[-50,-80]],[[20512,6334],[-117,27]],[[21499,6541],[-170,90],[-6,52],[-99,14],[-69,-25],[-78,21],[-75,-59],[-85,-3],[-76,15],[17,56],[77,103],[20,78]],[[20955,6883],[54,12],[208,-30],[77,39],[67,-72],[32,-10],[-16,-76],[52,-39],[4,-30],[77,-107],[-11,-29]],[[21162,6069],[-34,-15]],[[21128,6054],[5,93],[-24,51],[19,42],[-22,54],[-101,38],[-38,63],[-39,-21],[-97,1],[-77,-26],[-52,19]],[[20702,6368],[4,70],[55,105],[86,43],[48,-39],[87,13],[73,-61],[88,-31],[85,-109]],[[21228,6359],[96,-117],[-35,-53],[-43,-31],[-84,-89]],[[21920,6131],[-52,-38],[-37,34],[-55,-11]],[[21776,6116],[24,43],[8,91],[-17,40],[15,64]],[[21806,6354],[84,62],[-12,26],[90,49],[35,-69],[-94,-61],[7,-131],[16,-22],[-12,-77]],[[21678,6860],[-74,-7],[-62,45],[3,54],[-58,4],[-28,42],[-90,17],[-49,61]],[[21320,7076],[41,-23],[43,40],[-15,70],[-63,143],[6,26]],[[21332,7332],[45,-43]],[[21377,7289],[17,-48],[38,-36],[19,-130],[74,-11],[51,-29],[25,-62],[42,7],[42,-80],[-7,-40]],[[21369,5328],[-11,86],[10,42],[-34,50],[5,120],[-45,72],[28,82],[24,15],[12,107],[-33,132],[-74,36],[-89,-1]],[[21228,6359],[66,37],[97,-33],[44,-43],[128,-61]],[[21563,6259],[124,-147],[89,4]],[[21920,6131],[143,-34],[49,12]],[[21126,7133],[66,-23],[84,-6],[44,-28]],[[21678,6860],[55,-13],[61,-41],[-2,-40],[39,-49],[1,-84],[-17,-64],[12,-45],[-36,-44],[24,-27],[-9,-99]],[[21563,6259],[54,150],[-41,77],[-77,55]],[[20959,5168],[-69,111],[-27,147],[18,32],[13,129]],[[20894,5587],[3,40],[104,94],[6,87],[29,50],[-17,58],[27,37]],[[21046,5953],[27,12],[55,89]],[[20702,6368],[-85,2],[-55,44]],[[21046,5953],[-150,121],[-58,58],[-32,56],[-160,-37],[-25,70],[-52,2],[-57,111]],[[22120,6653],[-30,43],[-159,28],[-26,33],[-80,24],[41,42],[33,67],[-4,89],[16,57],[-89,119],[-92,-14],[-44,65],[2,54],[-136,63],[-31,-42],[-45,-10],[-67,32],[-32,-14]],[[21332,7332],[-44,7],[-27,-48]],[[20702,5006],[-23,65],[-40,34],[-1,61],[-31,85],[-32,202],[-36,70],[-33,123],[-224,-21],[-30,-23],[-36,38],[-54,-14],[-57,45]],[[20401,5896],[21,-13],[88,54],[21,41],[163,-106],[68,-27],[14,-62],[38,-49],[-29,-28],[33,-79],[76,-40]],[[17139,4275],[-6,-14]],[[17133,4261],[-59,10],[-6,43],[-67,60],[-46,62],[-66,-2],[-23,38],[19,36],[-39,95],[-10,61]],[[16836,4664],[42,-67],[58,12],[57,-58],[34,-6],[14,54],[115,-45],[45,4]],[[17201,4558],[-39,-74]],[[17162,4484],[-57,-27],[9,-35],[-50,-34],[14,-35],[59,-42],[2,-36]],[[17162,4484],[26,-55],[-63,-29],[64,-56],[48,22],[43,-41]],[[17280,4325],[-34,-51],[-63,-8]],[[17183,4266],[-44,9]],[[16944,5784],[82,-1],[44,54],[35,73],[91,-41],[32,-123],[33,-37],[2,-72],[59,-69],[5,-98],[31,-85],[46,-28]],[[17404,5357],[-39,-108],[19,-90],[-35,-59],[-45,-41],[8,-59]],[[17312,5000],[-99,4],[-58,-11],[-35,47],[-105,-49],[-118,41],[-52,-5]],[[8033,11817],[9,-44],[61,-154]],[[8025,11572],[-63,51],[-47,-9],[-52,53]],[[17201,4558],[39,42],[3,56],[27,74],[-6,48],[23,40]],[[17287,4818],[38,-71],[6,-86],[22,-82],[-31,-53],[-1,-50],[-34,-50],[-7,-101]],[[7447,11303],[27,-37],[53,12]],[[7527,11278],[15,-116],[-15,-50]],[[7352,11245],[36,23]],[[17823,4409],[-66,-6],[-64,113],[-83,-12]],[[17610,4504],[-66,33],[-45,47],[-66,24]],[[17433,4608],[-34,8],[-79,206]],[[17320,4822],[4,114],[13,42]],[[17337,4978],[41,-39],[107,-18],[58,36],[40,47],[95,-87],[55,34],[37,-42],[61,-10],[50,38],[50,-6],[72,-141]],[[17953,5224],[-38,-6],[-48,-56],[-95,1],[-28,38],[-28,98],[-54,-12],[-197,80],[-61,-10]],[[17337,4978],[-25,22]],[[7770,11652],[-40,-97],[47,-53],[42,-1]],[[7819,11501],[-26,-68],[-57,-60]],[[7653,11346],[-22,5]],[[7631,11351],[-61,-8],[-27,58],[47,64],[-25,63],[-53,-45]],[[7512,11483],[-58,25]],[[17654,4097],[-9,122],[-50,70],[-77,21],[-21,31],[-158,70],[47,80],[10,52],[37,65]],[[17610,4504],[-57,-51],[-52,51],[-29,-31],[-11,-69],[172,1],[-7,-26],[81,-82],[96,-49]],[[17553,3930],[-103,94],[16,60],[-133,28],[-58,-2],[-100,142],[8,14]],[[17287,4818],[33,4]],[[17394,3627],[-32,136],[15,96],[-52,87],[-12,-202],[28,-75],[31,-39]],[[16934,3971],[95,2],[39,23],[0,43],[-63,4],[-3,60],[38,9],[54,57],[39,92]],[[16838,4737],[-2,-73]],[[7710,11128],[-31,-46],[-52,0]],[[7527,11278],[87,33],[17,40]],[[20810,8991],[65,199],[71,23],[24,-15],[75,10],[36,-30],[88,-26],[33,-40],[7,-46],[61,-156],[80,-32],[37,-35],[116,-41],[83,28],[-17,-86],[75,-53],[-19,-81],[21,-48],[-10,-41],[50,-95]],[[7500,11414],[12,69]],[[20257,9229],[39,47],[26,112],[79,16],[23,23],[-11,86],[-103,87],[22,66],[74,82]],[[20406,9748],[54,-24],[63,-4],[50,-79],[45,-33]],[[20618,9608],[-22,-70],[4,-49],[-31,-25],[27,-93],[19,-20],[3,-83]],[[19898,9724],[-27,32],[29,68],[-44,34],[0,78],[83,57],[87,-14],[51,31]],[[20077,10010],[64,-32]],[[20141,9978],[18,-107],[39,-29],[-9,-61],[-39,-25],[8,-71],[34,-141],[-11,-62],[-62,-94],[26,-41],[-32,-78]],[[20141,9978],[53,4],[83,-33]],[[20277,9949],[40,-114],[89,-87]],[[7683,11664],[-96,81]],[[7587,11745],[6,40],[-46,93]],[[20761,9174],[22,59],[38,30],[3,47],[43,87],[-37,47],[41,107],[-97,76],[-24,2]],[[20750,9629],[-2,82],[151,36],[78,58]],[[20977,9805],[53,-24],[53,29],[93,-18],[22,29]],[[20618,9608],[132,21]],[[20618,9608],[0,56],[25,67],[35,17],[21,51],[104,78],[150,-51],[24,-21]],[[20277,9949],[31,-24],[56,2],[55,94],[78,20],[55,33],[14,67],[39,82],[35,-18],[84,49],[151,7],[21,33],[-27,81],[57,33],[26,60]],[[7492,11583],[-76,104]],[[7416,11687],[59,25],[94,2],[18,31]],[[7819,11501],[103,37],[30,-43]],[[19149,8879],[36,-1],[61,91],[168,-4],[96,20]],[[19510,8985],[29,-96],[1,-113],[109,-263],[30,-19]],[[7416,11687],[-87,-9]],[[19718,8398],[90,22],[26,123],[35,25],[56,106],[-20,59],[-48,11],[-58,78],[12,57],[-16,59],[-84,48],[-96,8]],[[19615,8994],[13,79],[47,71],[-4,61],[-29,43]],[[19510,8985],[77,23],[28,-14]],[[19375,9518],[35,90],[46,51],[7,37]],[[19463,9696],[217,18]],[[19155,9904],[68,-3],[72,20],[31,-12],[68,21]],[[19394,9930],[13,-58],[62,-72],[-6,-104]],[[19394,9930],[35,175]],[[19429,10105],[29,-36],[6,-62],[80,-122],[77,-47],[70,43]],[[19638,10799],[27,-54],[39,-29],[7,-54],[58,-71],[-52,-13],[-79,53],[-61,14],[-39,34],[-64,3],[-3,69]],[[19329,10863],[-65,-145],[25,-47],[60,-23],[59,60],[23,-51],[-9,-40],[-88,-41],[-8,-28],[-107,-109]],[[19721,10453],[-43,35],[-124,-97],[-27,-57],[5,-51],[-74,-45]],[[19458,10238],[-7,36],[-60,77],[94,179],[-4,34],[65,26],[34,-7],[-18,-121],[61,59],[83,21],[22,26],[61,-2]],[[19458,10238],[43,-50],[-5,-85]],[[19496,10103],[-67,2]],[[19429,10105],[-18,58]],[[19755,10136],[-25,9],[-135,-35],[-99,-7]],[[19755,10136],[69,17],[52,-10],[70,59],[11,76],[27,13],[-12,59],[21,84],[30,57],[129,85],[89,-17],[157,18],[81,-11]],[[20479,10566],[-31,-60],[-47,-35],[-8,-61],[-34,-25],[-77,-6],[-21,-45],[-70,-68],[-110,-86],[-14,-49],[10,-121]],[[20479,10566],[33,35],[80,-5],[65,19],[39,48]],[[17164,8475],[-139,-112],[-175,-38],[-50,16],[-76,-27],[-94,-120]],[[16630,8194],[-31,52],[46,52],[-46,46],[-36,124],[-123,88],[-50,63],[-73,-14],[-67,54],[-18,106]],[[15999,8820],[-134,-91],[-2,-66],[-38,-65],[11,-99],[-37,-68],[76,-98],[27,-79],[7,-64],[-17,-78],[39,-81],[16,-191]],[[15947,7840],[-31,12],[-136,-19],[-46,-37],[-53,14],[-141,-129],[-126,10]],[[15414,7691],[-32,148],[-82,85],[-36,19],[-40,73],[57,132]],[[15112,6914],[4,52],[45,111],[30,12],[30,67],[46,52],[42,19],[15,87],[-23,50],[21,112],[35,78],[48,47],[9,90]],[[15947,7840],[19,-69],[-14,-79],[33,-40],[-18,-68],[45,-34],[146,21],[49,17],[43,-35],[43,-187]],[[16072,8701],[29,-140],[45,-21],[103,-176],[-15,-54],[44,-153],[29,-142],[-28,-21],[-8,-124],[66,-19],[55,-34],[63,19],[60,-57]],[[16515,7779],[41,-51]],[[16630,8194],[-103,-80],[6,-142],[21,-80],[-39,-113]],[[13991,12153],[-31,54],[4,36],[-87,37]],[[14930,15556],[-25,-60]],[[11196,13560],[51,50]],[[11247,13610],[44,-14],[142,82]],[[11433,13678],[182,18]],[[11666,13638],[-119,-8],[-22,-53],[-100,-1],[-17,-69]],[[11408,13507],[-37,-34],[45,-98],[-67,-24]],[[11349,13351],[-36,47],[-49,15],[-67,-50]],[[11433,13678],[-14,36],[0,118]],[[11681,13597],[-48,-30],[-105,-22],[91,-40],[-163,-63],[-48,65]],[[11413,13855],[-69,33],[-127,-1]],[[11135,13847],[0,-61],[43,-10],[41,-156],[28,-10]],[[11518,13034],[-11,36],[-70,67]],[[11437,13137],[0,46]],[[11437,13183],[56,59],[28,74],[149,104]],[[11292,12984],[54,4],[59,-34],[110,11]],[[11291,13157],[146,-20]],[[8541,11218],[8,25]],[[8549,11243],[60,-66],[79,-45]],[[8688,11132],[-47,-74]],[[11176,13311],[65,6],[65,-44],[65,4]],[[11371,13277],[66,-94]],[[11371,13277],[-22,74]],[[9271,11377],[-93,-91],[14,-47]],[[9192,11239],[-94,26]],[[9098,11265],[-1,28],[-92,37],[-12,27]],[[8993,11357],[30,77]],[[9023,11434],[106,29]],[[9129,11463],[29,-7]],[[12506,13869],[-22,4]],[[12484,13873],[1,56],[46,14]],[[12531,13943],[11,-41],[-36,-33]],[[12732,14260],[22,-58]],[[12754,14202],[-21,-119]],[[12733,14083],[-34,-47],[41,-38],[-22,-35]],[[12718,13963],[-50,21],[-35,-34],[-63,30]],[[12570,13980],[-52,95],[-6,73]],[[12512,14148],[-21,52],[6,44]],[[12497,14244],[19,40]],[[12516,14284],[133,1],[83,-25]],[[12929,13422],[-78,142],[0,22]],[[12851,13586],[6,20],[87,37],[32,-16]],[[12976,13627],[13,0]],[[12989,13627],[1,-53],[33,-118]],[[12989,13627],[23,-12]],[[12313,14174],[63,17],[38,-43],[73,-13],[25,13]],[[12570,13980],[-39,-37]],[[12484,13873],[-61,-28]],[[12423,13845],[-7,-3]],[[12732,14260],[75,38]],[[12807,14298],[9,1]],[[12816,14299],[83,-66]],[[12899,14233],[-145,-31]],[[12718,13963],[34,-30]],[[12752,13933],[-40,-49]],[[12712,13884],[-93,-86]],[[12619,13798],[-79,34],[-34,37]],[[12877,14473],[-61,-174]],[[12807,14298],[-61,48],[-69,106],[-40,32]],[[12324,14457],[64,-97]],[[12388,14360],[41,-88],[68,-28]],[[8548,10383],[-28,31],[10,45],[-56,-3],[-37,49],[76,77]],[[13002,13876],[-41,-41],[-15,-74],[-49,-20]],[[12897,13741],[-66,80]],[[12831,13821],[95,77]],[[12926,13898],[76,-22]],[[12717,13470],[58,49],[-10,46],[30,31]],[[12795,13596],[56,-10]],[[12337,13788],[0,-50],[56,-49],[12,-39]],[[12497,13658],[8,26],[-42,71],[3,76],[-43,14]],[[12619,13798],[-2,-93]],[[12617,13705],[-32,-24]],[[12733,14083],[10,-42],[85,14],[45,-94]],[[12873,13961],[53,-63]],[[12831,13821],[-72,86],[-7,26]],[[12712,13884],[-14,-54],[38,-47],[23,-84]],[[12759,13699],[-53,-14]],[[12706,13685],[-70,-27],[-19,47]],[[12897,13741],[-5,-10]],[[12892,13731],[-72,-8],[-61,-24]],[[12516,14284],[-29,30]],[[12487,14314],[79,205]],[[12566,14519],[13,9]],[[12795,13596],[-39,70],[-50,19]],[[12892,13731],[84,-104]],[[9122,10591],[-20,-16]],[[9102,10575],[-18,10],[-55,100],[-52,26],[-77,-28],[-28,-46],[-96,-45]],[[8776,10592],[1,40]],[[8777,10632],[-5,85],[23,20]],[[8795,10737],[59,-36],[53,70],[-28,27]],[[8879,10798],[-40,12]],[[8839,10810],[50,105],[16,8]],[[8905,10923],[12,18],[98,-61],[59,55],[17,65],[-10,47]],[[9081,11047],[49,-25]],[[9130,11022],[-11,-53],[44,-64],[-51,-115],[18,-17],[-32,-140],[24,-42]],[[13002,13876],[77,-18]],[[12388,14360],[49,23],[11,-45],[39,-24]],[[12314,14557],[171,5],[81,-43]],[[13030,14133],[-51,-16],[-41,-77],[-65,-79]],[[12899,14233],[119,18]],[[9098,11265],[-41,-15],[-6,-45],[46,-55]],[[9097,11150],[-31,-38],[9,-40],[-55,-29],[-64,10],[-96,-10],[-102,133],[-3,30]],[[8755,11206],[7,34],[47,32],[23,78]],[[8832,11350],[44,-8],[55,25],[62,-10]],[[8845,10226],[10,31]],[[8855,10257],[93,19],[-16,57]],[[8932,10333],[23,50],[50,16],[-1,81],[69,35],[29,60]],[[9122,10591],[15,-20]],[[9137,10571],[9,-43],[83,-70]],[[9229,10458],[109,-65],[-17,-18],[23,-106]],[[9344,10269],[8,-62]],[[9352,10207],[-39,-27],[-113,18]],[[13369,12794],[-61,81]],[[13308,12875],[-38,93]],[[13270,12968],[-8,51]],[[13262,13019],[22,-2]],[[13393,12999],[-16,-54],[33,-37]],[[13252,13206],[-132,-79],[-38,-8]],[[13082,13119],[-28,74],[-64,80]],[[12990,13273],[43,37]],[[13033,13310],[60,-43],[99,-5],[51,42]],[[12780,13135],[-45,77],[-54,54]],[[12681,13266],[27,29],[44,-29],[19,51],[57,16]],[[12828,13333],[75,-46],[22,15]],[[12925,13302],[29,-59],[-11,-94],[-64,-48],[64,-44]],[[12943,13057],[81,-35]],[[12601,12974],[-40,6],[-74,-35]],[[12487,12945],[-52,34]],[[12435,12979],[-13,22],[65,94]],[[12487,13095],[51,-37]],[[12312,13574],[55,-93],[46,-51],[53,-17],[16,-36]],[[12482,13377],[-74,-33]],[[12408,13344],[-83,38],[-20,50]],[[12955,13395],[-25,-94]],[[12930,13301],[-5,1]],[[12828,13333],[-22,18],[20,60]],[[12681,13266],[-45,19]],[[12636,13285],[48,38],[-18,58],[41,41]],[[12473,13609],[43,-30],[53,-82]],[[12569,13497],[-16,-117]],[[12553,13380],[-71,-3]],[[9489,10977],[-2,74],[49,33],[21,-69]],[[9557,11015],[-68,-38]],[[9130,11022],[56,66],[56,11],[73,-30],[28,-60],[59,7],[1,-51]],[[9403,10965],[71,-69]],[[9474,10896],[-31,-21],[-47,-90],[-61,-23],[0,-65],[-198,-126]],[[13114,12829],[77,-14],[35,36],[38,-7],[44,31]],[[12636,13285],[-83,95]],[[12569,13497],[78,95]],[[13108,12860],[28,22],[7,63],[81,2],[46,21]],[[13262,13019],[-106,-10],[-61,31],[-25,43]],[[13070,13083],[12,36]],[[13033,13310],[16,50],[-14,58]],[[12317,13006],[79,-7],[39,-20]],[[12487,12945],[0,-30],[74,-101]],[[12943,13057],[89,-9],[38,35]],[[9129,11463],[-26,79],[29,19]],[[12408,13344],[61,-85],[-28,-34],[30,-49],[-8,-55],[24,-26]],[[12990,13273],[-60,28]],[[11585,13784],[75,21]],[[11660,13805],[65,7]],[[11725,13812],[17,-22],[111,-2],[67,-50]],[[12068,13809],[0,35],[51,60],[87,22],[91,-6]],[[12297,13920],[-8,-105]],[[12297,13920],[47,37]],[[12344,13957],[48,-2],[54,38]],[[11506,14504],[28,-23],[12,-65],[72,-5]],[[11618,14411],[-79,-95],[17,-61]],[[11944,13941],[-16,141]],[[11928,14082],[11,37],[56,7],[42,52],[47,3],[16,49],[-87,23]],[[12013,14253],[18,14]],[[12031,14267],[143,56],[-11,19]],[[12163,14342],[90,50],[52,73]],[[12305,14465],[18,8]],[[12336,14351],[-46,-45],[-82,-6],[-11,-56],[-56,-60]],[[12141,14184],[-13,-7]],[[12128,14177],[-33,-33],[-87,-22],[-21,-28],[15,-60]],[[12002,14034],[-16,-101]],[[11618,14411],[55,12],[28,96]],[[11701,14519],[44,-32]],[[11745,14487],[22,-65],[-46,-144],[-58,-12],[-36,-56]],[[11658,14549],[43,-30]],[[12128,14177],[3,-47],[-41,-58],[22,-51]],[[12112,14021],[-65,38],[-45,-25]],[[9023,11434],[-2,68]],[[9021,11502],[-12,90]],[[12141,14184],[40,-2],[16,-41],[43,-15],[13,-44]],[[12253,14082],[12,-32]],[[12265,14050],[-25,-20],[-128,-9]],[[12265,14050],[-5,-59],[84,-34]],[[12177,14782],[-146,-52],[-4,-12]],[[11875,14620],[-17,-43],[21,-75]],[[11879,14502],[-20,-17],[-114,2]],[[12253,14082],[27,5]],[[8712,10928],[-76,-110]],[[8636,10818],[-87,-18]],[[12004,14677],[-52,-33],[86,-34],[30,4],[40,-57],[71,-2],[7,-38]],[[12186,14517],[-50,-12],[-43,-48]],[[12093,14457],[-54,-28]],[[12039,14429],[-3,56],[-42,7],[-93,-24],[-22,34]],[[12031,14267],[-67,32],[-16,33],[44,67],[47,30]],[[12093,14457],[70,-115]],[[12186,14517],[114,-11],[5,-41]],[[11595,14066],[55,-70],[35,-79],[-25,-112]],[[8905,10923],[-41,22],[-57,68]],[[8807,11013],[-119,119]],[[8688,11132],[67,74]],[[9097,11150],[15,-28]],[[9112,11122],[-34,-34],[3,-41]],[[11725,13812],[45,47],[22,73],[152,9]],[[11928,14082],[-15,34],[-109,28]],[[11804,14144],[-16,43],[48,67],[66,-29],[111,28]],[[11804,14144],[-60,-17],[-53,-43]],[[8915,11590],[18,-74],[88,-14]],[[8832,11350],[-26,52],[26,109],[-18,37]],[[12177,12361],[-6,17]],[[12171,12378],[-34,91],[-37,50],[0,99]],[[12100,12618],[65,18]],[[12100,12618],[-32,48],[-32,-1]],[[12036,12665],[-18,54],[12,38]],[[12030,12757],[55,48]],[[11982,12203],[19,86],[43,11],[14,-46],[-62,-56]],[[11998,12937],[-49,-37],[-26,-91],[-21,-23]],[[11902,12786],[-18,15]],[[11884,12801],[-11,42],[-50,48],[-57,9]],[[11766,12900],[-9,39]],[[11757,12939],[71,34],[16,75]],[[11754,12326],[6,37],[-67,134],[61,53],[-29,72]],[[11725,12622],[-3,18],[78,52]],[[11800,12692],[57,-31],[49,-74]],[[11906,12587],[38,-56]],[[11944,12531],[13,-34],[-68,-39],[11,-35],[-70,-10],[-5,-56]],[[11287,12935],[-42,-84]],[[11906,12587],[74,16],[39,25]],[[12019,12628],[20,-28],[-95,-69]],[[12030,12757],[-128,29]],[[12026,12344],[145,34]],[[8566,10537],[14,75],[73,12],[49,26],[75,-18]],[[8776,10592],[35,-52],[-22,-38],[96,-11],[-20,-48],[23,-36]],[[8888,10407],[-90,-69]],[[8798,10338],[-43,-53]],[[11884,12801],[-84,-109]],[[11725,12622],[-52,16]],[[11673,12638],[-61,82],[19,36],[-69,61]],[[11562,12817],[116,23],[88,60]],[[12019,12628],[17,37]],[[11385,12734],[41,54],[88,28]],[[11514,12816],[48,1]],[[11673,12638],[-107,-47],[-20,-49]],[[11497,12863],[17,-47]],[[11757,12939],[-45,1],[-14,44],[-41,21],[-4,54]],[[8719,10941],[45,58],[43,14]],[[8839,10810],[-36,-5],[-38,46],[-33,-51]],[[8732,10800],[-50,-7],[-46,25]],[[11097,14849],[0,-158]],[[11097,14691],[-54,-97]],[[11043,14594],[-54,37],[-25,-40]],[[10888,14627],[19,116],[-8,58]],[[10899,14801],[128,-19],[65,71]],[[11043,14594],[33,-55],[47,8],[51,-31],[13,41],[54,24]],[[11241,14581],[52,-61]],[[11293,14520],[-30,-123]],[[11263,14397],[-71,-88]],[[11192,14309],[-19,52],[-60,45],[-20,51],[-87,-3],[-71,28]],[[11711,14603],[-39,18],[-27,62],[22,51],[-18,96],[15,22]],[[11664,14852],[36,-30],[160,-36],[8,-86],[42,-52]],[[11192,14309],[-18,-59]],[[11174,14250],[-17,-56]],[[11157,14194],[-69,-13],[-13,38],[-51,42]],[[11024,14261],[-51,82],[-66,-8],[-73,24],[-55,-23]],[[11209,14098],[-32,29],[-20,67]],[[11174,14250],[47,-49],[63,6],[1,48],[88,-31],[63,11]],[[11436,14235],[28,-36],[-11,-69]],[[11460,14321],[-1,-46],[-37,-17]],[[11422,14258],[-63,60],[-9,38],[-87,41]],[[11293,14520],[24,7],[41,-56],[35,55],[99,-4],[13,34]],[[11505,14556],[67,11]],[[11301,14855],[173,-75],[-13,-122],[44,-102]],[[11241,14581],[-85,120],[-59,-10]],[[11578,14902],[62,-19],[24,-31]],[[10854,14814],[45,-13]],[[10955,13972],[47,0],[-4,47],[120,28],[58,-50],[38,19]],[[11913,15090],[-94,-70],[-85,62]],[[11422,14258],[14,-23]],[[8798,10338],[57,-81]],[[8932,10333],[-44,74]],[[8795,10737],[84,61]],[[11024,14261],[-49,-44],[2,-58],[-74,-58],[9,-21]],[[12153,13400],[59,12],[45,-22]],[[12257,13390],[-40,-57],[6,-39],[-53,-22]],[[12170,13272],[-91,19]],[[12079,13291],[21,67],[53,42]],[[11986,12967],[40,-52],[46,28]],[[12072,12943],[50,14],[13,-44]],[[12220,13694],[-31,21]],[[12189,13715],[-14,70]],[[12198,13154],[8,46],[-36,72]],[[12257,13390],[26,21]],[[12198,13154],[-133,-60],[-26,1]],[[12039,13095],[-69,125],[26,45],[-10,51]],[[11986,13316],[93,-25]],[[12286,12605],[51,26],[-15,213]],[[11940,13028],[68,50]],[[12008,13078],[60,-66],[4,-69]],[[12153,13400],[4,69],[-24,75]],[[12133,13544],[25,-13],[123,13]],[[12051,13813],[-42,5],[-36,-52],[-42,15]],[[11679,13542],[102,-14]],[[11781,13528],[13,-78],[-73,-79]],[[11986,13316],[-61,-3]],[[11925,13313],[48,64],[21,78],[-15,35],[-92,7],[-48,79]],[[11839,13576],[33,40]],[[11872,13616],[61,-41],[81,-21],[64,14],[-59,96]],[[12019,13664],[116,-38]],[[12135,13626],[-2,-82]],[[12212,12887],[66,8]],[[12189,13715],[-42,-39],[-12,-50]],[[12019,13664],[-49,42],[-73,-29]],[[11897,13677],[-8,43]],[[11872,13616],[25,61]],[[11839,13576],[-8,-15]],[[11831,13561],[-50,-33]],[[11831,13561],[63,-142],[-31,-32],[5,-51]],[[11868,13336],[-29,-31],[-9,-59],[-38,-2]],[[12008,13078],[31,17]],[[11868,13336],[57,-23]],[[9192,11239],[-5,-84],[-75,-33]],[[8795,10737],[-63,63]],[[12887,11848],[17,-12],[109,17],[2,-56],[25,-42],[-50,-132]],[[13170,12675],[-41,-59],[-120,-22],[-5,-25]],[[13004,12569],[-43,57],[9,56],[-73,14],[-31,60]],[[12866,12756],[30,51]],[[12896,12807],[71,39]],[[12967,12846],[42,-33],[74,-18]],[[12823,12061],[46,-19],[72,88],[48,5],[3,114],[55,-13],[28,17]],[[13075,12253],[26,-107],[38,-19]],[[13139,12127],[-5,-90],[-54,-41],[-152,10],[-36,-19],[-54,6]],[[12755,12296],[60,50],[71,-23],[65,25]],[[12951,12348],[16,-56],[92,26],[8,-42]],[[13067,12276],[8,-23]],[[12944,12962],[23,-116]],[[12896,12807],[-16,22]],[[12880,12829],[-18,56],[32,66]],[[12573,12752],[13,-25]],[[12586,12727],[82,-146]],[[12668,12581],[-6,-23]],[[12866,12756],[-69,-63],[-2,-41],[-46,-30]],[[12749,12622],[-81,-41]],[[12586,12727],[17,66],[56,26],[23,39],[-21,101],[-36,22]],[[12625,12981],[52,-1]],[[12677,12980],[58,-34],[34,-50],[-10,-65],[88,11],[33,-13]],[[12677,12980],[35,56],[70,37]],[[12749,12622],[16,-72],[33,-37],[-9,-63]],[[12789,12450],[-22,-37],[-60,-29]],[[13079,11511],[39,91],[3,54]],[[13121,11656],[85,-17],[1,-45],[86,32]],[[12789,12450],[51,-52],[47,24],[30,-61],[35,-4]],[[12952,12357],[-1,-9]],[[12608,12968],[17,13]],[[13259,12320],[-123,48],[-66,-63],[-3,-29]],[[12952,12357],[64,128],[-12,84]],[[13196,12138],[-57,-11]],[[13121,11656],[-11,86],[67,25],[13,59],[27,14]],[[9620,11057],[-33,-50],[-30,8]],[[9489,10977],[-52,42],[-34,-54]],[[10480,14453],[23,88],[37,15],[53,-14],[77,50]],[[10670,14592],[78,-78]],[[10748,14514],[-9,-14]],[[10739,14500],[-29,-41],[-87,-69],[-86,-47]],[[9715,14772],[57,-35]],[[9772,14737],[-23,-41],[6,-52],[-51,-33]],[[9916,14448],[43,85],[-20,73],[-59,27],[22,54]],[[9902,14687],[28,-13],[60,57],[79,-40]],[[10069,14691],[81,-10]],[[9962,14402],[-30,34]],[[10501,15089],[-47,-92],[-63,-25]],[[10391,14972],[-47,-130],[-55,-19],[-32,-34],[-2,-96]],[[10069,14691],[15,120],[57,25],[-5,69],[26,10],[-20,99],[35,13],[-3,50]],[[10731,14768],[-37,-38]],[[10694,14730],[-48,4],[-43,-28]],[[10603,14706],[-36,35],[-67,3]],[[10500,14744],[-6,85]],[[10494,14829],[64,27]],[[10817,14628],[-40,-46],[-29,-68]],[[10670,14592],[-48,55],[-19,59]],[[10694,14730],[64,-23],[36,-37]],[[9772,14737],[90,-58],[40,8]],[[10779,14336],[20,64],[-60,100]],[[10494,14829],[-6,52],[-97,91]],[[10500,14744],[-43,-84],[-71,-69]],[[10610,13072],[-8,-100],[-29,-19],[-9,-104]],[[10564,12849],[-94,67]],[[11040,12620],[-102,43],[38,51]],[[10976,12714],[68,41],[82,-17],[34,-24]],[[11160,12714],[-30,-45],[31,-129]],[[11161,12540],[-83,9]],[[10483,12319],[1,28]],[[10484,12347],[-18,102],[133,74]],[[10599,12523],[47,-25],[84,83]],[[10730,12581],[56,0]],[[11523,12495],[-54,23],[-115,77]],[[11354,12595],[-6,34],[63,62]],[[11197,12869],[-48,6],[-44,36]],[[11105,12911],[-26,76],[-31,42],[10,38]],[[11357,12424],[-32,97]],[[11325,12521],[29,74]],[[10245,12705],[139,-80]],[[10384,12625],[-28,-55],[57,-67],[-79,-51],[110,-118],[40,13]],[[10856,12953],[2,-60],[67,-45]],[[10925,12848],[-69,-71],[-56,7],[-69,-58],[-64,45]],[[10667,12771],[-57,65],[-46,13]],[[11127,12477],[75,24],[5,32]],[[11207,12533],[118,-12]],[[10667,12771],[-36,-21],[-86,0]],[[10545,12750],[-65,-69],[-96,-56]],[[11105,12911],[-56,1],[-89,-62]],[[10960,12850],[-35,-2]],[[11207,12533],[-46,7]],[[11160,12714],[24,-7],[35,63]],[[10599,12523],[13,27],[-68,68],[26,88],[-25,44]],[[10667,12771],[63,-190]],[[10976,12714],[20,43],[-36,93]],[[12815,14869],[9,-66],[-27,-13]],[[12797,14790],[-3,-9]],[[12794,14781],[-112,45]],[[12682,14826],[27,97]],[[12709,14923],[62,-6],[44,-48]],[[12815,14869],[50,11]],[[12865,14880],[61,-35]],[[12926,14845],[42,-64]],[[12968,14781],[-30,-23],[-82,6],[-59,26]],[[12164,14877],[51,28],[64,95],[-6,39],[83,30]],[[12356,15069],[54,-58]],[[12410,15011],[16,-56],[-19,-49],[-110,-31],[-26,-53],[29,-49]],[[12410,15011],[23,29],[97,-6]],[[12530,15034],[21,-17]],[[12551,15017],[-44,-84],[-3,-49],[-31,-88]],[[12480,14771],[152,76],[50,-21]],[[12794,14781],[-41,-40],[4,-40],[-56,-88],[-46,-33],[-4,-69],[-36,-14]],[[13304,14981],[11,-52]],[[13230,14677],[-40,84],[19,66],[-39,31]],[[13170,14858],[2,14]],[[13172,14872],[14,79],[-13,64]],[[12249,15344],[-40,-19],[-5,-103],[-83,-59]],[[12121,15163],[-23,55],[-48,39]],[[12356,15069],[-106,129]],[[12250,15198],[78,27],[25,-45],[51,31],[76,-16]],[[12480,15195],[49,-88]],[[12529,15107],[1,-73]],[[6542,11700],[-112,-65]],[[6430,11635],[-31,-14]],[[6399,11621],[-53,122],[-5,92],[-38,12],[-16,122],[-108,42],[-7,23]],[[6172,12034],[8,28]],[[6180,12062],[56,-12],[123,64],[45,2]],[[6404,12116],[75,-17]],[[6479,12099],[16,-27],[-4,-174],[51,-198]],[[12465,15327],[15,-132]],[[12250,15198],[14,76]],[[12121,15163],[-32,-92],[-56,-31],[-32,-54]],[[12968,14781],[4,-4]],[[12972,14777],[-15,-58],[-43,-45],[29,-42],[-53,-45],[25,-81]],[[13011,14970],[-17,-45]],[[12994,14925],[-68,-80]],[[12865,14880],[11,98]],[[12709,14923],[-5,52]],[[12704,14975],[59,72]],[[12612,15302],[13,-76],[-48,-50],[-33,3],[-15,-72]],[[13172,14872],[-76,8],[-102,45]],[[12972,14777],[44,-25]],[[13016,14752],[29,-59],[51,-11],[47,-41]],[[13016,14752],[37,50],[69,6],[48,50]],[[5784,11956],[109,-123],[67,-17],[13,-126],[83,-226]],[[6056,11464],[-122,-41]],[[5934,11423],[-68,209],[-92,174],[-40,44],[-2,81]],[[12551,15017],[117,10],[36,-52]],[[6399,11621],[-201,-95]],[[6198,11526],[-46,65],[45,17],[-26,45],[49,72],[-55,66],[-4,44],[-52,157],[7,19]],[[6116,12011],[56,23]],[[5834,12053],[34,-85],[82,29],[38,-14],[128,28]],[[6198,11526],[-142,-62]],[[5191,11677],[47,-137],[50,-88]],[[5288,11452],[-35,-44],[-93,-64],[-48,-13],[-82,25]],[[5885,12152],[28,-49],[59,-54],[58,-19],[90,37],[60,-5]],[[6446,12257],[6,42]],[[6452,12299],[23,-25]],[[6475,12274],[-29,-17]],[[6678,11465],[-113,-56],[-90,123]],[[6475,11532],[-45,103]],[[6542,11700],[71,47],[82,-7],[43,18]],[[6738,11758],[146,13],[56,-39],[68,-1]],[[7008,11731],[11,-15]],[[18708,14388],[-46,81],[31,92],[-60,27],[-8,43]],[[18625,14631],[30,63]],[[18121,14998],[29,6],[16,61],[65,-6]],[[18231,15059],[15,-35],[-33,-40]],[[18213,14984],[-92,14]],[[5288,11452],[14,-29],[-29,-54],[44,-128]],[[18296,14665],[-5,22],[-85,62]],[[18206,14749],[93,33],[82,7],[50,-14],[94,58]],[[18625,14631],[-43,5],[-49,-150],[-58,10],[-45,-76]],[[18213,14984],[11,-69]],[[18224,14915],[32,-53],[5,-52]],[[18261,14810],[-43,-12],[-17,-40]],[[18201,14758],[-52,18],[-13,76],[-54,-1],[-23,33]],[[18059,14884],[-7,49],[27,47],[1,54]],[[18080,15034],[41,-36]],[[7283,11805],[11,24],[73,35],[23,34],[-24,73],[10,69]],[[7376,12040],[-5,58]],[[7371,12098],[45,2]],[[7416,12100],[70,16]],[[18252,15147],[-29,-34],[8,-54]],[[18080,15034],[-11,13]],[[5559,11300],[13,191],[-54,51],[-122,197],[-76,75]],[[5561,11780],[59,-55],[49,5],[38,-65],[-6,-68],[23,-90],[125,-129]],[[18519,14863],[-80,1],[-59,-20],[-13,-35],[-98,16],[-8,-15]],[[18224,14915],[70,33],[29,50],[38,-1],[31,66]],[[6479,12099],[56,-1],[81,-28],[151,-85]],[[6767,11985],[-41,-48],[-9,-43],[23,-81],[-2,-55]],[[18206,14749],[-5,9]],[[18059,14884],[-59,39],[-104,36]],[[6459,12350],[-7,-51]],[[6446,12257],[7,-52],[-30,-21],[-19,-68]],[[17421,14383],[15,79],[92,65],[52,-53]],[[17580,14474],[29,-64],[51,-41],[62,71]],[[17722,14440],[60,-52],[40,46]],[[17822,14434],[42,-71],[-63,-47],[-2,-45],[-46,-19]],[[17818,14662],[-27,-16]],[[17791,14646],[-39,42],[-73,-8]],[[17679,14680],[-22,41],[-64,-8],[-55,13],[58,73],[-3,66]],[[17593,14865],[69,1],[53,28],[-10,36],[85,52]],[[5934,11423],[-71,-64]],[[17822,14434],[73,13],[50,-37],[50,4],[52,-37],[72,48]],[[7008,11731],[15,141],[73,43],[52,11],[16,42],[63,34],[149,38]],[[17580,14474],[2,88],[39,29],[-24,48]],[[17597,14639],[82,41]],[[17791,14646],[-46,-76],[11,-96],[-34,-34]],[[6767,11985],[52,52],[54,1]],[[6873,12038],[71,33],[57,2]],[[7001,12073],[127,0],[118,38]],[[7246,12111],[125,-13]],[[17357,14439],[-74,-17]],[[17283,14422],[-2,35],[71,73],[-42,11],[-14,100],[18,35]],[[17314,14676],[29,15],[51,-32]],[[17394,14659],[22,-40],[95,34],[86,-14]],[[17394,14659],[18,7],[-41,98],[9,68],[76,-4],[47,62],[33,18]],[[17536,14908],[57,-43]],[[15010,15558],[-13,63],[-33,21],[-18,135]],[[14946,15777],[11,38],[93,5],[38,44],[84,-20],[71,-48],[128,-22],[0,-29]],[[15371,15836],[22,-92]],[[14946,15777],[-30,78],[-39,-18],[-8,88],[-23,39]],[[16086,14472],[60,-78],[-70,-15],[-19,-68],[23,-66],[43,-49]],[[16123,14196],[-33,-33],[-9,-82],[-41,33],[-99,-17],[-46,12]],[[16136,14912],[-14,35],[-58,4],[-31,31],[-29,127]],[[16004,15109],[10,3]],[[16014,15112],[181,22]],[[16195,15134],[40,-103],[8,-70],[32,-42]],[[16275,14919],[-139,-7]],[[15587,14983],[13,37],[50,9]],[[15650,15029],[173,17],[181,63]],[[16136,14912],[31,-59],[-42,-29]],[[16125,14824],[-59,-48],[1,-77],[-18,-52],[-66,4]],[[15745,14654],[3,85],[62,22],[-41,105],[-45,36],[7,53],[-50,14],[-26,-46]],[[16470,14901],[-28,47],[-147,-38],[-20,9]],[[16195,15134],[183,3]],[[16378,15137],[183,-70]],[[16125,14824],[27,-62],[-17,-30]],[[16135,14732],[46,-75],[-47,-36],[11,-49],[-13,-61]],[[15401,15283],[34,-76]],[[15435,15207],[-34,-19],[-34,-91]],[[16135,14732],[55,2],[129,-44],[42,-35]],[[15435,15207],[122,80],[46,-10],[43,-37],[74,-29]],[[15720,15211],[-11,-108],[-60,0],[1,-74]],[[15720,15211],[105,-20],[17,21]],[[15842,15212],[82,-15],[59,-38]],[[15983,15159],[31,-47]],[[17979,15360],[-28,-58],[-23,-119],[-52,-76]],[[17876,15107],[-22,72],[-70,84],[-25,79]],[[17759,15342],[18,26],[71,-5],[42,47]],[[17646,15534],[-20,-30],[24,-57],[-42,-53]],[[17608,15394],[-8,26],[-69,56]],[[17531,15476],[34,54],[8,49]],[[17090,15747],[26,-12]],[[17116,15735],[-45,-120],[-32,-51],[-2,-55]],[[17037,15509],[-173,53]],[[16864,15562],[-17,20]],[[16847,15582],[30,57],[-19,86],[-66,52],[24,44]],[[17212,15730],[-96,5]],[[17949,15053],[-87,13],[14,41]],[[17002,15283],[84,123]],[[17086,15406],[37,76],[95,5]],[[17218,15487],[83,-88],[55,107],[44,-32],[0,-79]],[[17400,15395],[54,-77],[1,-48]],[[17531,15476],[-56,-6],[-31,-83],[-44,8]],[[17218,15487],[47,104],[59,44],[18,73]],[[17627,15162],[23,127],[-7,64]],[[17643,15353],[58,28],[58,-39]],[[17608,15394],[35,-41]],[[17086,15406],[-30,19],[-19,84]],[[16751,15407],[40,77],[53,22],[20,56]],[[16522,14752],[64,40],[114,19]],[[16700,14811],[-9,-64],[71,7],[86,32],[25,-75],[49,-48],[-20,-42],[17,-69]],[[16525,14321],[-9,60],[-38,28],[-38,-23]],[[16440,14386],[-22,40]],[[16418,14426],[54,45],[45,-20]],[[16517,14451],[19,-23],[143,-18],[8,-37]],[[16023,13861],[-34,-37]],[[16123,14196],[53,-46]],[[16176,14150],[21,-27],[-28,-85],[10,-56]],[[16179,13982],[-25,-42],[45,-52],[-35,-29],[-78,-5]],[[16691,14375],[25,23],[-32,97],[102,29],[62,-48],[-25,97]],[[16176,14150],[82,36]],[[16258,14186],[30,-34],[84,0]],[[16372,14152],[-16,-64],[-40,-8],[-12,-42],[30,-52]],[[16334,13986],[-81,-29],[-69,-4],[-5,29]],[[16334,13986],[52,-52],[45,-4],[25,-50]],[[16544,14512],[-27,-61]],[[16418,14426],[-93,91]],[[16614,14093],[-75,74],[-104,9],[-52,19],[-11,-43]],[[16258,14186],[16,73],[72,27],[38,46],[39,9],[17,45]],[[16666,13932],[-10,40],[38,60]],[[16981,14484],[36,10],[-17,70],[11,37],[56,27],[108,-32],[34,25],[16,59]],[[17225,14680],[19,-27],[70,23]],[[17283,14422],[-38,-56],[-41,-8],[-11,54],[-36,62],[-60,-8],[-39,45],[-77,-48]],[[16608,14063],[-16,-49],[17,-31],[-30,-50]],[[16634,15672],[-154,132]],[[15894,15579],[-2,-97],[23,-37]],[[15915,15445],[-9,-48],[-80,-41],[-70,25],[-48,46],[-85,15],[-36,58]],[[15587,15500],[66,15],[-16,84],[18,28]],[[15842,15212],[-5,45],[40,81],[63,36],[25,76],[-33,6]],[[15932,15456],[66,2],[10,41],[68,25]],[[16076,15524],[57,-53],[49,0],[67,-30]],[[16249,15441],[-39,-43],[-59,-28],[11,-64],[-87,-11],[-19,-99],[-73,-37]],[[16568,15383],[-123,-39],[-79,-51]],[[16366,15293],[-88,15],[5,83],[-34,50]],[[16076,15524],[61,37],[51,60]],[[16188,15621],[197,-12],[141,56],[105,-3]],[[15516,15519],[71,-19]],[[15915,15445],[17,11]],[[16184,15665],[4,-44]],[[16378,15137],[-12,156]],[[16679,15662],[57,-29],[6,-58],[105,7]],[[16561,15067],[108,-50],[60,-5]],[[16729,15012],[91,17],[53,-37]],[[16873,14992],[-35,-42]],[[16838,14950],[-11,-72],[-127,-67]],[[17560,15178],[-46,-2],[-41,-37],[19,-129],[30,-8]],[[17522,15002],[-13,-38],[-59,-35],[-22,42],[-37,-13],[1,-48],[-40,-29],[-60,22],[-64,-2]],[[17228,14901],[-21,33],[39,130],[-5,45]],[[17536,14908],[10,85],[-24,9]],[[6382,10411],[-70,106],[100,32],[-22,36]],[[6390,10585],[80,31],[65,78],[48,-26],[4,-50]],[[6587,10618],[2,-35],[-89,-83]],[[6500,10500],[-58,-89]],[[6442,10411],[-60,0]],[[17225,14680],[-22,48],[-39,10],[-7,64]],[[17157,14802],[31,101],[40,-2]],[[5964,10568],[35,33],[54,1]],[[6053,10602],[3,-21],[-78,-45],[-14,32]],[[6220,10672],[-49,-76],[-7,-57]],[[6164,10539],[-43,-50]],[[6121,10489],[35,79],[-46,57]],[[6110,10625],[27,55],[-13,49]],[[16838,14950],[99,-64],[220,-84]],[[6615,10427],[-62,90],[-53,-17]],[[6587,10618],[33,32]],[[6620,10650],[33,-113],[51,-59]],[[6110,10625],[-57,-23]],[[5964,10568],[-41,-43]],[[5923,10525],[-18,-19],[-77,11]],[[17058,15103],[-103,-11],[-82,-100]],[[16729,15012],[-7,61],[24,21],[16,108],[34,-3],[208,54]],[[5923,10525],[56,-26],[59,23],[58,-64]],[[6096,10458],[-39,-61],[26,-56],[-222,-153]],[[6096,10458],[25,31]],[[6164,10539],[49,-18],[58,61],[21,-24],[98,27]],[[6382,10411],[-23,-55],[4,-98],[29,-41]],[[6590,10843],[63,-108],[-15,-25]],[[6638,10710],[-18,-60]],[[6390,10585],[-7,57],[54,24],[-40,42]],[[6442,10411],[77,-110],[51,-19]],[[6638,10710],[106,15]],[[6744,10725],[60,-60]],[[17213,3689],[-27,-13],[-114,11],[-52,29],[-110,12],[-46,32],[-52,5],[-34,50],[-18,85],[-92,4],[12,88],[-28,78],[4,81],[100,50]],[[16756,4201],[-26,-82],[16,-62],[115,-71],[33,-38],[49,15]],[[16826,3333],[-5,48],[-61,73],[-88,1],[-36,-23]],[[16636,3432],[-46,-28],[-67,37],[-68,-24]],[[16455,3417],[-27,90],[-51,-1],[-26,54],[-39,-20],[-87,42],[-136,-8]],[[16089,3574],[-41,-7]],[[16799,4479],[3,-101],[-46,-177]],[[16089,3574],[37,-37],[88,4],[40,-68],[104,-122]],[[16358,3351],[-36,-25]],[[16358,3351],[97,66]],[[16636,3432],[100,-54],[0,-40],[50,-24]],[[15719,6308],[176,34],[-2,-76],[69,-57],[110,9],[40,67],[50,-3],[12,-38],[-18,-73],[-33,-60],[77,-15],[136,-54],[36,28],[45,-7]],[[16417,6063],[25,-41],[-11,-38],[34,-134],[-23,-41],[-41,-17],[-12,-101],[-71,0],[-92,51],[-26,-38],[-58,31],[-128,-119]],[[16671,6228],[-107,10],[-4,-52],[-115,-68],[-28,-55]],[[14866,6894],[-73,-25],[8,-39],[-104,-146],[-39,-104],[-24,-102],[-38,-24],[-15,-106],[-71,-56],[-32,-68]],[[14075,6231],[-19,32],[-22,125],[-61,91],[27,38],[-59,82],[-79,13]],[[16138,5393],[-28,-62],[-38,-26],[-6,-76]],[[16066,5229],[-75,32],[-37,55],[-33,-24],[-36,25]],[[15885,5317],[41,70]],[[15926,5387],[75,28],[33,82],[42,34]],[[14906,5966],[62,-7],[27,23],[64,-21],[82,34],[43,139],[36,50]],[[15220,6184],[44,-57],[43,-11],[56,-69],[-9,-44],[-85,-15],[-29,-49],[12,-50],[68,-48],[-66,-87],[-41,-15],[-32,79],[-44,28],[-56,1],[-38,34],[-166,37]],[[15220,6184],[-122,20],[-28,62]],[[15070,6266],[32,18],[3,163],[24,50],[76,2]],[[15926,5387],[-46,67],[-104,107],[-59,108],[-46,13],[10,-123],[-37,5],[-56,-72],[-19,-57],[48,-29],[-14,-53],[-66,-43]],[[15885,5317],[-131,-62],[-66,-54],[-76,-15]],[[16066,5229],[28,-41],[114,-63]],[[14792,6107],[19,32],[73,52],[20,43],[52,16],[39,37],[75,-21]],[[15054,3441],[35,45],[-68,26],[-97,-11],[-110,-62]],[[14814,3439],[4,11]],[[14818,3450],[103,59]],[[14921,3509],[289,43],[33,37]],[[15243,3589],[112,-137]],[[16088,4133],[-48,-176],[-26,0],[-118,-52],[-57,4],[-32,-78]],[[15807,3831],[-132,77]],[[15675,3908],[16,86],[-23,57],[-42,-4],[-166,66],[-93,-33]],[[15336,3930],[67,3],[94,-37],[23,-55]],[[15520,3841],[-2,-39],[58,-41]],[[15576,3761],[-77,-15],[-35,18],[-53,110],[-82,7]],[[14557,3241],[50,10],[77,202],[59,15],[75,-18]],[[14814,3439],[134,-122]],[[13880,14387],[-3,13]],[[13877,14400],[149,90],[34,35],[55,-28]],[[14115,14497],[-36,-102],[-70,-1],[-44,-38],[5,-48]],[[15697,3632],[4,61],[-79,69],[23,92]],[[15645,3854],[30,54]],[[15807,3831],[13,-108]],[[15820,3723],[-11,-40]],[[15809,3683],[-51,12],[-9,-77],[-51,-17]],[[15809,3683],[104,-23]],[[15913,3660],[96,5],[-28,-79],[40,-25]],[[16030,3525],[-4,-30],[-125,26],[-146,-30],[-20,-29]],[[15243,3589],[-91,113]],[[15152,3702],[-44,45],[30,35]],[[15325,3834],[-17,-124],[41,-56]],[[15349,3654],[40,-30]],[[14921,3509],[29,182]],[[14950,3691],[50,-3],[60,33],[92,-19]],[[15645,3854],[-72,10],[-53,-23]],[[15434,3630],[-18,60]],[[15416,3690],[-37,72],[-54,72]],[[15576,3761],[46,-30],[-7,-82]],[[15349,3654],[67,36]],[[14871,3906],[-25,-50],[-50,-22],[2,-61]],[[14798,3773],[-181,-18],[-110,-38],[-89,2],[-111,-24],[-50,51],[-33,-21],[-74,32],[-17,-80]],[[15889,2146],[-28,-4]],[[15861,2142],[-17,12]],[[15844,2154],[24,62]],[[15868,2216],[52,95],[62,-34]],[[15982,2277],[-25,-74]],[[15730,2345],[-12,14]],[[15718,2359],[35,96]],[[15753,2455],[92,61]],[[15845,2516],[-28,-29],[10,-156]],[[15827,2331],[-59,-26]],[[15768,2305],[-38,40]],[[16526,1785],[-67,11],[-100,-13]],[[16359,1783],[-39,-7]],[[16320,1776],[8,154]],[[16338,2099],[64,-40],[10,-49]],[[15485,2447],[33,-26]],[[15518,2421],[32,-116]],[[15550,2305],[-66,-21],[-121,55]],[[15983,2610],[5,69],[-44,91]],[[15944,2770],[72,-64],[59,-10]],[[16075,2696],[-32,-51],[2,-38]],[[16045,2607],[-62,3]],[[15643,2298],[36,-56],[-1,-74]],[[15678,2168],[-28,-9]],[[15650,2159],[-14,97],[-103,-14]],[[15533,2242],[21,51]],[[15554,2293],[23,69]],[[15577,2362],[65,-41],[1,-23]],[[15868,2216],[-39,25],[-72,7]],[[15757,2248],[11,57]],[[15827,2331],[102,35],[6,143],[-25,30]],[[15910,2539],[49,12]],[[15959,2551],[30,-73],[8,-57],[68,-14]],[[16065,2407],[7,-14]],[[16072,2393],[-54,-79],[-13,-40]],[[16005,2274],[-23,3]],[[16320,1776],[-60,-23]],[[15845,2516],[65,23]],[[16138,2496],[-71,-17]],[[16067,2479],[1,37]],[[16068,2516],[63,41]],[[16131,2557],[7,-61]],[[16276,2458],[-34,46]],[[16242,2504],[40,83],[-5,35]],[[16277,2622],[33,-9]],[[16310,2613],[2,-121],[-36,-34]],[[15725,1997],[-4,13]],[[15721,2010],[84,90]],[[15805,2100],[84,-63]],[[16242,2504],[-65,-10]],[[16177,2494],[14,117]],[[16191,2611],[42,27]],[[16233,2638],[44,-16]],[[15506,2140],[22,57]],[[15528,2197],[5,45]],[[15650,2159],[39,-58],[1,-58]],[[15690,2043],[-21,-43]],[[16131,2557],[2,30]],[[16133,2587],[58,24]],[[16177,2494],[-20,-22]],[[16157,2472],[-19,24]],[[15757,2248],[-3,-16]],[[15754,2232],[5,-55]],[[15759,2177],[-81,-9]],[[15643,2298],[87,-10],[0,57]],[[15518,2421],[32,36],[27,-95]],[[15554,2293],[-4,12]],[[16037,1970],[32,62]],[[16069,2032],[22,-60],[-54,-2]],[[16045,2607],[19,-65]],[[16064,2542],[-69,16]],[[15995,2558],[-12,52]],[[16075,2696],[32,-4]],[[16107,2692],[26,-105]],[[16068,2516],[-4,26]],[[16181,2190],[-38,-59],[60,-30],[23,13]],[[16227,2047],[-26,15],[-38,-65],[-22,-132],[-49,-91]],[[16092,1774],[-29,-25],[-42,17]],[[16021,1766],[-84,55]],[[15917,1954],[48,-51],[34,50],[38,17]],[[16069,2032],[9,39],[-31,31],[80,93]],[[16127,2195],[25,47],[-37,56],[-49,-66]],[[16066,2232],[-61,42]],[[16072,2393],[24,-14]],[[16096,2379],[36,-16]],[[16132,2363],[4,-57],[40,2],[12,66]],[[16188,2374],[23,56]],[[16211,2430],[65,28]],[[16310,2613],[41,157],[53,51],[65,20],[84,49],[-2,39],[45,14],[-4,43]],[[16592,2986],[10,57]],[[16602,3043],[47,103],[94,15]],[[16743,3161],[89,46],[20,-9]],[[15805,2100],[56,42]],[[15959,2551],[36,7]],[[16067,2479],[-2,-72]],[[16359,1783],[-26,-70]],[[16333,1713],[-23,-40]],[[16132,2363],[56,11]],[[16333,1713],[29,-40],[79,9],[20,-50],[79,14]],[[16117,1746],[-25,28]],[[15726,2970],[84,-14],[23,-47]],[[15833,2909],[-38,-3]],[[15795,2906],[-61,18]],[[16158,3010],[-27,-66]],[[16131,2944],[-18,-12],[-190,2],[-87,62],[15,32],[-60,69],[48,28],[115,-13],[55,28],[75,-77],[74,-53]],[[14450,13836],[-9,41],[-86,-4],[-43,39]],[[14312,13912],[24,51],[-6,55],[65,64],[41,-14]],[[14436,14068],[55,-60],[72,-103],[51,-21]],[[15570,2596],[143,-56],[17,-69],[23,-16]],[[15718,2359],[-67,77],[-110,61],[-10,21]],[[15341,2306],[42,-7],[7,-44],[138,-58]],[[16021,1766],[-33,-76],[19,-53],[62,34]],[[16066,2232],[13,-25]],[[16079,2207],[-34,1],[-63,-42]],[[15805,2100],[-46,77]],[[15754,2232],[34,-58],[56,-20]],[[16107,2692],[7,33]],[[16114,2725],[90,-31]],[[16204,2694],[29,-56]],[[16114,2725],[-13,45],[24,39]],[[16125,2809],[-26,75]],[[16099,2884],[43,-34],[9,-58],[53,-34],[0,-64]],[[16743,3161],[-31,22],[-116,4],[-3,75]],[[15690,2043],[31,-33]],[[16211,2430],[-79,-28]],[[16132,2402],[25,70]],[[16096,2379],[36,23]],[[16127,2195],[-48,12]],[[16602,3043],[-10,-57]],[[16099,2884],[32,60]],[[16158,3010],[71,8],[60,90],[93,68]],[[15944,2770],[-26,47],[-77,10],[-46,79]],[[15833,2909],[23,-52],[71,11],[75,-35],[123,-24]],[[16580,961],[-39,-56],[-73,-2]],[[16468,903],[-1,56],[-32,43]],[[16435,1002],[28,52]],[[16463,1054],[53,-7]],[[16516,1047],[17,-54],[47,-32]],[[16621,1644],[-30,-37],[-42,-8],[20,-70],[-34,-30],[37,-44],[12,-81],[103,48],[-5,-71],[-26,-18],[-16,-71],[-35,-33],[0,-46],[-68,-99]],[[16537,1084],[-21,-37]],[[16463,1054],[2,33],[-93,17]],[[16372,1104],[34,62],[104,92],[4,125],[21,38],[-35,116],[44,12],[-16,46]],[[16446,902],[22,1]],[[16580,961],[12,-10]],[[16592,951],[59,-11]],[[16435,1002],[-52,24],[-20,-26],[-57,48],[-13,36]],[[16293,1084],[34,25],[45,-5]],[[16172,965],[49,96],[-89,74]],[[16189,1544],[60,-130],[30,-106],[44,36],[-11,41],[21,91]],[[16293,1084],[-45,-54],[-27,-64]],[[16537,1084],[109,-28]],[[16646,1056],[-8,-49],[-46,-56]],[[16646,1056],[42,-14],[50,27]],[[16835,5027],[-125,20],[-52,-22],[-77,26],[-57,-30],[-30,-91],[-153,-53],[-55,138],[-35,29],[-43,81]],[[13877,14400],[-39,57],[23,53]],[[13861,14510],[37,61],[-42,31],[26,92],[-34,73]],[[13848,14767],[35,42],[31,-7],[-9,98]],[[13905,14900],[39,-6],[87,-78]],[[14031,14816],[5,-79],[62,-79],[-9,-50],[75,-51]],[[14164,14557],[-26,-72]],[[14138,14485],[-23,12]],[[13563,5036],[42,-27],[53,0],[80,-25],[66,6],[64,-23]],[[13868,4967],[28,-78],[-35,-153],[-96,-20]],[[13765,4716],[-106,4],[-46,-35],[-64,-18],[-77,9],[-37,31]],[[13765,4716],[84,-78],[33,34],[90,-78],[3,-46],[50,-41],[65,6]],[[14090,4513],[67,-32]],[[14157,4481],[-26,-72]],[[13868,4967],[91,53],[121,-89]],[[14080,4931],[-4,-51],[22,-74],[66,-97],[-17,-70],[-36,-34]],[[14111,4605],[-21,-92]],[[13961,6029],[-36,-56],[96,-92],[-34,-119],[25,-83],[97,-66],[-10,-47],[45,-22],[24,-52],[-10,-82],[89,-64],[88,28],[56,-57],[-46,-87],[-28,-78],[68,-88],[29,31],[86,26],[27,-94],[69,-66],[50,-17],[92,-68]],[[14738,4876],[-85,-8],[-25,-31],[-68,7],[-89,-25],[-78,-57],[33,-51],[-3,-42],[-126,-96],[-29,-52],[-51,-33],[-60,-7]],[[14111,4605],[96,23],[51,56],[-27,105],[11,36],[-40,29],[-52,76],[-70,1]],[[15230,4343],[-100,-47],[-35,9],[-51,-94],[21,-23],[-51,-57],[-18,-49],[88,-87],[-31,-33],[3,-57]],[[8665,11715],[-26,-27]],[[8639,11688],[-57,-52],[10,-65]],[[8592,11571],[0,-157],[-59,-130],[16,-41]],[[8068,10725],[90,-47]],[[8158,10678],[12,-26]],[[8170,10652],[-36,-61]],[[8134,10591],[-115,47]],[[7857,10566],[121,-9],[31,-67],[-29,-46]],[[8546,10846],[-56,-7],[-123,61],[-24,-1]],[[8343,10899],[-59,36],[-110,31]],[[8174,10966],[-7,25]],[[14950,3691],[-50,15],[-76,-7],[-26,74]],[[14399,13655],[-41,91],[-44,45],[-9,90],[-28,7]],[[14277,13888],[35,24]],[[8482,10654],[-82,19],[-9,-21]],[[8391,10652],[-111,21],[-29,-18]],[[8251,10655],[-44,-20],[-37,17]],[[8158,10678],[4,39],[61,-15],[37,14],[13,74]],[[8273,10790],[26,16],[7,76],[37,17]],[[8174,10966],[-67,-101]],[[5276,4880],[38,-12],[115,14],[14,-12],[84,44]],[[5527,4914],[1,-58],[36,-13],[165,77],[83,92]],[[5812,5012],[22,-31]],[[5834,4981],[-13,-49],[-66,-85],[-54,-25],[-54,-50],[-103,-36],[-44,-53],[-82,-35],[-46,-45]],[[6031,3871],[-7,82],[-47,30],[-28,83],[-36,-9],[-93,33],[-123,-61]],[[5485,4381],[86,31],[108,10],[91,53],[111,91],[84,-31]],[[5965,4535],[-36,-100],[27,-35],[101,25],[25,54],[106,43],[50,-7],[63,27],[7,83],[71,45],[84,26]],[[6463,4696],[30,16],[175,-7]],[[5033,5204],[88,-33]],[[5121,5171],[41,-66],[-2,-44]],[[5160,5061],[-77,-59]],[[5723,5360],[-26,-71],[-71,-42],[20,-67],[-2,-64],[-66,-68]],[[5578,5048],[-96,-34],[-25,-38],[-55,-5],[-34,104],[35,34],[-7,40],[-41,15]],[[5355,5164],[-10,72],[-32,9]],[[5313,5245],[53,26],[56,68],[124,50],[71,84],[95,53]],[[5712,5526],[15,-54],[-17,-54],[13,-58]],[[5965,4535],[61,26],[59,93],[91,77],[107,26],[114,-20],[36,-23]],[[6433,4714],[30,-18]],[[5527,4914],[39,76],[12,58]],[[5723,5360],[98,-8],[43,-49],[-15,-94],[-41,-101],[4,-96]],[[5209,5451],[50,-88]],[[5259,5363],[-46,-5],[-69,-64],[-3,-98]],[[5141,5196],[-20,-25]],[[5834,4981],[67,10],[164,-71],[62,19],[89,-49],[72,12],[17,74]],[[6305,4976],[76,-80],[45,-15],[23,-137],[-16,-30]],[[8273,10790],[-61,1],[-28,34],[-60,-9]],[[6390,5328],[-31,-26],[-63,7],[-106,-63],[-28,-59],[-19,-96],[58,-24],[14,-91],[90,0]],[[5712,5526],[153,147]],[[5259,5363],[-17,-109]],[[5242,5254],[-51,-11],[-50,-47]],[[5242,5254],[71,-9]],[[5355,5164],[-64,-85],[-131,-18]],[[2513,6551],[-110,68]],[[2403,6619],[15,77],[-70,41],[22,116],[-27,32]],[[2343,6885],[-11,24]],[[2332,6909],[40,30],[96,-76],[118,-23]],[[2586,6840],[-1,-43],[65,-22]],[[2650,6775],[-3,-87]],[[2647,6688],[-73,-26],[-5,-72]],[[2569,6590],[-56,-39]],[[2403,6619],[-46,10],[-32,47],[-70,-75]],[[2255,6601],[-108,79],[4,15]],[[2151,6695],[14,109]],[[2165,6804],[60,-14],[58,67],[60,28]],[[1853,6988],[40,15]],[[1893,7003],[165,-77],[47,-72],[60,-50]],[[2151,6695],[-41,22],[-24,-30],[-64,56],[-114,126],[26,39],[-76,9],[-36,35]],[[2255,6601],[-15,-7]],[[2240,6594],[-56,-48],[-5,-98]],[[2179,6448],[-47,-16]],[[2132,6432],[-117,39],[-81,100]],[[1934,6571],[-72,12],[-22,49],[-39,30],[-27,55],[-54,3],[-51,62]],[[2039,6231],[83,-65],[70,43]],[[2192,6209],[8,-94]],[[2192,6209],[124,-77],[16,-42]],[[2332,6090],[-50,-63]],[[1934,6571],[-34,-43],[41,-87],[119,-111]],[[2060,6330],[-34,-42]],[[1950,6289],[-32,28],[-105,1],[-15,-54]],[[1467,6541],[59,-35],[22,-96],[44,-87],[64,-37],[67,72]],[[1736,6204],[-21,-51],[-37,-9]],[[1678,6144],[-41,23],[-43,98],[-95,48],[-74,151],[12,23]],[[8488,10384],[-37,88],[-20,5],[-56,113],[16,62]],[[1678,6144],[-93,-32]],[[1585,6112],[-49,40],[-56,-13],[-74,25],[-67,123]],[[1339,6287],[-37,76],[56,71],[17,53]],[[1893,7003],[41,24],[233,6],[97,-31],[71,-60]],[[2335,6942],[-3,-33]],[[2240,6594],[46,-11],[7,-37],[77,-50],[51,-99]],[[2421,6397],[-57,-33],[29,-57],[-15,-35]],[[2378,6272],[-49,16],[-150,160]],[[2132,6432],[-26,-14],[-46,-88]],[[8221,10336],[-22,105],[-37,-3],[10,108]],[[8172,10546],[12,15]],[[8184,10561],[133,-65],[82,-94],[4,-53]],[[5696,9452],[27,53],[90,57],[54,61]],[[5941,9583],[39,-87],[68,8]],[[5968,9960],[-90,-68],[-105,-52],[29,-31],[-57,-34],[-50,48],[-50,-40],[-40,30],[-60,80]],[[8134,10591],[38,-45]],[[3301,7371],[55,21],[35,-31]],[[3391,7361],[36,-40]],[[3427,7321],[-11,-80]],[[3416,7241],[-37,-31]],[[3379,7210],[-55,57],[-60,-19]],[[3264,7248],[-10,37],[47,86]],[[3379,7210],[-23,-70],[-29,-33]],[[3327,7107],[-56,-17],[-50,10]],[[3221,7100],[12,46],[-29,46]],[[3204,7192],[60,56]],[[3401,7044],[-74,63]],[[3416,7241],[43,-45]],[[3459,7196],[-6,-117]],[[2870,7325],[-25,-65],[11,-85]],[[2856,7175],[-104,0]],[[2752,7175],[-21,-9]],[[2731,7166],[-34,93]],[[2697,7259],[-24,22],[27,89],[-35,45]],[[2665,7415],[27,22]],[[2692,7437],[33,-30],[90,-27],[22,11]],[[2837,7391],[33,-66]],[[2731,7166],[-28,-6],[-74,-60]],[[2629,7100],[-65,75]],[[2564,7175],[33,52]],[[2597,7227],[70,-6],[30,38]],[[2692,7437],[112,66]],[[2804,7503],[37,42],[65,11]],[[2906,7556],[4,-40],[-48,-58]],[[2862,7458],[-25,-67]],[[3064,7097],[-62,-44],[-57,5]],[[2945,7058],[-31,58],[14,44],[-72,15]],[[2870,7325],[63,-18],[12,47],[60,8]],[[2731,6844],[-60,-57]],[[2671,6787],[8,70],[-42,133]],[[2637,6990],[9,24]],[[2646,7014],[36,-61],[55,-1]],[[2737,6952],[-6,-108]],[[2597,7227],[-57,55],[13,54],[-83,58]],[[2470,7394],[91,119]],[[2561,7513],[69,-48],[35,-50]],[[3248,7491],[-3,-60],[56,-60]],[[3204,7192],[-12,16]],[[3192,7208],[-11,43],[-49,18],[-28,43]],[[3104,7312],[58,77],[10,36]],[[2862,7458],[58,-23],[47,13],[18,-48]],[[3047,7453],[6,92],[89,15],[11,20]],[[3153,7580],[92,22],[22,32]],[[2802,7663],[52,-53],[28,20],[70,-47],[-46,-27]],[[2804,7503],[-63,90],[16,41],[-6,47]],[[3082,6913],[-65,-16],[-34,26],[-58,-29]],[[2925,6894],[-12,72]],[[2913,6966],[-4,74],[36,18]],[[2752,7175],[42,-71],[31,-82]],[[2825,7022],[-46,-65],[-42,-5]],[[2646,7014],[-17,86]],[[2125,7385],[46,50],[152,-23],[97,25],[50,-43]],[[2564,7175],[-22,-26]],[[2542,7149],[-32,18],[-68,-46]],[[2442,7121],[-52,51],[-70,108],[-191,39],[-107,-19]],[[3104,7312],[-59,-39]],[[2542,7149],[-63,-50],[-32,9]],[[2447,7108],[-5,13]],[[3220,6829],[-26,83],[13,79]],[[3207,6991],[58,0],[85,-35]],[[2561,7513],[27,55]],[[2447,7108],[0,-34],[-49,-101],[-63,-31]],[[3153,7580],[-129,42]],[[3024,7622],[95,165],[92,-8]],[[2637,6990],[-52,-103],[1,-47]],[[3192,7208],[-57,-35],[-36,-73]],[[3221,7100],[-8,-79]],[[3213,7021],[-125,-2]],[[3024,7622],[-22,-13],[-90,64]],[[2913,6966],[-9,35],[-79,21]],[[13686,15037],[70,26]],[[13756,15063],[89,7],[-6,95]],[[13839,15165],[59,2],[47,-50],[50,44]],[[13995,15161],[19,-2]],[[14052,14835],[-3,-17]],[[14049,14818],[-18,-2]],[[13905,14900],[-54,5],[-33,61],[-63,52],[-87,15]],[[2671,6787],[-7,-10]],[[2664,6777],[-14,-2]],[[3207,6991],[6,30]],[[8184,10561],[37,-9],[4,61],[26,42]],[[3708,7234],[-18,-60]],[[3690,7174],[-44,11],[-22,-50]],[[3624,7135],[-78,60]],[[3546,7195],[1,109]],[[3547,7304],[14,25]],[[3561,7329],[100,-2],[30,13],[48,-28]],[[3739,7312],[-31,-78]],[[3976,7798],[49,79],[-2,83]],[[4023,7960],[50,32]],[[4073,7992],[53,-80]],[[4126,7912],[-11,-33]],[[4115,7879],[-33,-122],[-34,-47]],[[4048,7710],[-72,88]],[[3427,7321],[115,17],[5,-34]],[[3546,7195],[-87,1]],[[3793,7101],[-56,42],[7,55],[-36,36]],[[3739,7312],[21,45]],[[3760,7357],[74,-31],[43,-1]],[[3877,7325],[11,-57],[71,-43],[1,-33]],[[3960,7192],[-76,-3],[-44,-75]],[[3769,7059],[-63,-29]],[[3706,7030],[-43,46],[27,98]],[[3638,6937],[38,82],[30,11]],[[3561,7329],[30,105],[-37,24]],[[3554,7458],[30,58],[29,-14]],[[3613,7502],[85,12],[42,-31],[47,13]],[[3787,7496],[22,-11],[-58,-81],[9,-47]],[[8592,11571],[47,-70],[84,8],[21,63]],[[8744,11572],[70,-24]],[[3842,8025],[-26,-25],[-15,-73]],[[3801,7927],[-31,18],[-114,-21]],[[3624,7135],[-29,-64],[-38,-38]],[[4048,7710],[-10,-9]],[[4038,7701],[-39,-47]],[[3999,7654],[-64,-80],[-59,46],[-59,-50]],[[3817,7570],[-32,30],[49,126]],[[3834,7726],[20,74]],[[3854,7800],[85,42]],[[3939,7842],[37,-44]],[[3960,7192],[36,5],[56,-35]],[[4052,7162],[30,-66]],[[3372,7610],[42,-76],[-14,-57],[13,-64]],[[3413,7413],[-22,-52]],[[3893,8021],[86,-70]],[[3979,7951],[-40,-109]],[[3854,7800],[-64,55],[11,72]],[[3625,7840],[-10,-58]],[[3615,7782],[-33,-71]],[[3582,7711],[-134,-18],[-41,61]],[[3877,7325],[-7,53],[42,40],[105,-60]],[[4017,7358],[18,-73],[-5,-58],[22,-65]],[[3979,7951],[44,9]],[[3413,7413],[84,61],[57,-16]],[[3613,7502],[49,155],[-80,54]],[[3615,7782],[16,-61],[115,-62],[88,67]],[[3817,7570],[-30,-74]],[[8639,11688],[66,-11],[11,-66],[28,-39]],[[3999,7654],[24,-81],[47,-35]],[[4070,7538],[-22,-116],[-31,-64]],[[3934,8926],[-61,44],[-21,-17]],[[3852,8953],[-44,63],[9,62],[-29,28]],[[3788,9106],[26,33],[-38,162],[23,18]],[[3799,9319],[76,34],[28,-4]],[[3903,9349],[26,-45]],[[3929,9304],[2,-8]],[[3931,9296],[-59,-38],[17,-80],[31,-53],[-27,-33],[36,-49],[27,-73]],[[3381,9255],[-7,-32],[50,-71],[-27,-35],[30,-56]],[[3427,9061],[-60,-42]],[[3367,9019],[-64,-31],[-47,-99]],[[3256,8889],[-10,-3]],[[3246,8886],[-70,-19],[-53,63],[-7,61],[-54,35],[21,89],[51,56]],[[3134,9171],[76,6],[153,128]],[[3363,9305],[18,-50]],[[3852,8953],[-23,-24],[-83,28],[-35,-10]],[[3711,8947],[-21,64]],[[3690,9011],[-3,25]],[[3687,9036],[10,71],[91,-1]],[[4112,9110],[-54,90],[-34,23],[-21,68],[-74,13]],[[3903,9349],[67,31]],[[3970,9380],[93,20],[84,-77],[89,50]],[[4039,9019],[-97,211],[-11,66]],[[3711,8947],[-91,-40]],[[3620,8907],[-46,-6],[-46,31]],[[3528,8932],[29,32],[133,47]],[[3281,9343],[82,-38]],[[3134,9171],[-44,34]],[[3413,9412],[18,-53],[53,-37],[21,-46],[-38,-22],[42,-101],[63,-9]],[[3572,9144],[20,-92],[95,-16]],[[3528,8932],[-21,-2]],[[3507,8930],[-6,75]],[[3501,9005],[70,22],[-121,134],[-20,50],[-49,44]],[[3620,8907],[-4,-96],[-29,-61]],[[3587,8750],[-63,38],[-59,-9]],[[3465,8779],[-15,137]],[[3450,8916],[4,17]],[[3454,8933],[53,-3]],[[3501,9005],[-32,-4],[-42,60]],[[3678,8696],[-75,-7]],[[3603,8689],[-16,61]],[[3516,9433],[53,-159],[65,-79],[-62,-51]],[[3970,9380],[5,32],[131,34],[122,-31]],[[3454,8933],[-31,14],[-56,72]],[[3799,9319],[-70,38],[-84,116]],[[3246,8886],[-13,-57],[-41,-32],[-96,-10]],[[3096,8787],[-150,-37],[-180,-98],[-24,13],[-107,145],[-47,-64]],[[4035,6612],[47,-17]],[[4082,6595],[-51,-26],[-48,-62],[-8,-40],[-84,-59]],[[3891,6408],[-11,-4]],[[3880,6404],[-17,59],[105,109],[67,40]],[[3570,6957],[-76,-3],[-57,-72]],[[3422,6665],[-40,-1],[-23,-56]],[[3359,6608],[-62,42]],[[3297,6650],[10,103],[-17,29]],[[3290,6782],[48,73],[8,43]],[[3736,6673],[47,10],[29,101]],[[3812,6784],[43,16],[50,-51],[40,2],[-5,-57],[-56,-89],[-101,31],[-47,37]],[[3408,6717],[58,88]],[[3466,6805],[45,-98]],[[3643,6934],[-13,-53],[-52,-48]],[[3578,6833],[-33,26],[-60,3],[-19,-57]],[[3549,6702],[32,3]],[[3581,6705],[57,-94],[98,62]],[[3812,6784],[-22,126]],[[3581,6705],[-3,128]],[[3849,6327],[31,77]],[[3891,6408],[2,-84]],[[3490,6495],[-17,46],[-10,102]],[[4013,6642],[22,-30]],[[3424,6520],[-62,21],[-5,62]],[[3357,6603],[2,5]],[[14457,14458],[-6,-57],[-64,-4],[-81,-49]],[[14306,14348],[-73,31],[-41,-46],[-37,7]],[[14155,14340],[-17,145]],[[14164,14557],[32,13],[30,66]],[[14226,14636],[47,-35],[57,-6]],[[4487,6092],[-52,36]],[[4435,6128],[-69,56],[59,57],[74,-29]],[[4499,6212],[14,-62],[-26,-58]],[[4283,6851],[150,-56]],[[4433,6795],[80,-44]],[[4513,6751],[20,-39],[48,-26]],[[4581,6686],[-46,-49],[-79,-129],[-94,-48],[-38,-118]],[[4324,6342],[-81,44],[-92,88]],[[4151,6474],[117,103],[43,84],[-16,49],[29,36],[-73,49],[32,56]],[[4082,6595],[36,-59],[11,-92]],[[4129,6444],[-72,-158],[-23,-25]],[[4557,6132],[-58,80]],[[4435,6128],[9,-47]],[[4294,6184],[21,54],[9,104]],[[4581,6686],[51,-29],[-32,-110],[30,-39]],[[4487,6092],[3,-14]],[[8483,6415],[80,101]],[[8563,6516],[49,-31],[61,21],[76,-30],[73,-4],[10,-63],[45,-14],[105,-80],[58,-24]],[[9040,6291],[-46,-65],[36,-38],[2,-52],[-45,-29],[-64,-99],[-33,-20],[-9,-61],[-32,-71],[20,-32],[-35,-127],[-34,-72]],[[4120,6885],[134,9]],[[4254,6894],[29,-43]],[[4151,6474],[-22,-30]],[[10060,7051],[-37,-31],[-47,-4],[-63,-36],[-75,36],[-34,58],[-133,-59],[41,91],[-17,65],[-2,86],[-64,55],[48,32],[-125,74],[-34,34],[-80,-39],[-142,-4],[-31,62]],[[9265,7471],[72,57],[-69,121]],[[9268,7649],[65,10],[124,-62],[51,-6]],[[9508,7591],[2,-39],[61,-32],[141,-135],[67,-19]],[[3091,6782],[-5,9]],[[3086,6791],[-27,-10],[-115,26],[-14,-23]],[[2930,6784],[-18,4]],[[2912,6788],[13,106]],[[3132,6717],[-28,-22],[-44,-84]],[[3060,6611],[-55,27],[-25,-18]],[[2980,6620],[-4,65]],[[2976,6685],[48,20],[62,86]],[[2356,5972],[79,69],[2,35]],[[2437,6076],[99,-9],[18,25]],[[2554,6092],[42,-35]],[[3220,6811],[70,-29]],[[3297,6650],[-46,-33],[-34,16]],[[2569,6590],[20,-13],[72,26]],[[2661,6603],[47,-28]],[[2708,6575],[-6,-58]],[[2702,6517],[-105,-38]],[[2597,6479],[-84,72]],[[3398,6461],[-94,30],[30,91],[23,21]],[[2814,6387],[-12,-16]],[[2802,6371],[-21,37]],[[2781,6408],[10,60],[45,-3]],[[2836,6465],[-22,-78]],[[2664,6777],[37,-107]],[[2701,6670],[-30,-52]],[[2671,6618],[-24,70]],[[8563,6516],[-6,33],[73,141],[101,53],[18,31],[31,123],[56,65],[6,69],[45,38],[53,106],[9,63]],[[8949,7238],[38,21],[37,-38],[66,-15],[57,-39],[81,-91],[86,-49],[26,-71],[54,-47],[-2,-71]],[[9392,6838],[-243,-13],[-54,-123],[18,-88],[-5,-40],[51,-82],[-30,-122]],[[9129,6370],[-17,-43],[-72,-36]],[[2702,6517],[66,-58],[-4,-45]],[[2764,6414],[-70,-50]],[[2694,6364],[-52,4]],[[2642,6368],[-31,82]],[[2611,6450],[-14,29]],[[3166,6405],[-34,13],[-23,74]],[[3109,6492],[5,55]],[[3114,6547],[45,-22],[33,14]],[[3114,6547],[-40,21],[-14,43]],[[2731,6844],[64,-17],[87,-64],[30,25]],[[2930,6784],[42,-52],[4,-47]],[[2980,6620],[-17,-55],[19,-79]],[[2982,6486],[-134,-19],[-12,19]],[[2836,6486],[-36,49]],[[2800,6535],[-1,93],[-41,40],[-57,2]],[[3398,6461],[-150,-51],[-82,-5]],[[2437,6076],[-3,60]],[[2434,6136],[-2,24]],[[2432,6160],[24,64],[75,58]],[[2531,6282],[61,-14]],[[2592,6268],[22,-55],[-20,-62],[-40,-59]],[[2421,6397],[67,-21]],[[2488,6376],[-6,-25],[49,-69]],[[2432,6160],[-25,94],[-29,18]],[[3109,6492],[-80,-33],[-16,-22]],[[3013,6437],[-31,49]],[[2802,6371],[-34,-61],[54,-62]],[[2822,6248],[-31,-21],[-57,64]],[[2734,6291],[-40,73]],[[2764,6414],[17,-6]],[[9392,6838],[19,-119],[57,-64],[49,-100],[51,-47],[22,-48]],[[9590,6460],[-68,-48],[-52,6],[-31,32],[-47,-90],[-101,16],[-56,-31],[-106,25]],[[2734,6291],[-61,-51],[-40,8],[-38,41]],[[2595,6289],[9,54],[38,25]],[[2873,6307],[-59,80]],[[2836,6465],[0,21]],[[3013,6437],[-1,-21]],[[2671,6618],[-10,-15]],[[2592,6268],[3,21]],[[2822,6248],[9,-26]],[[9590,6460],[113,-33],[65,-3],[94,-81],[8,-61],[60,-25],[42,18],[53,-9]],[[2800,6535],[-92,40]],[[2488,6376],[68,26],[55,48]],[[9508,7591],[55,19],[7,34]],[[4115,7879],[60,-87],[-13,-106],[-38,-40],[-86,55]],[[4591,7894],[34,32],[78,-70]],[[4703,7856],[-80,-80],[-43,-5],[-61,40]],[[4557,7507],[13,77],[130,135]],[[4700,7719],[17,-55],[54,8],[40,-44],[-9,-27]],[[4923,7953],[-71,-107]],[[4852,7846],[-82,-62]],[[4770,7784],[-48,62]],[[4722,7846],[55,47],[-5,47],[48,10],[71,126]],[[4891,8076],[33,-24]],[[4924,8052],[17,-22],[-18,-77]],[[8949,7238],[44,101],[45,36]],[[9038,7375],[66,23],[65,89],[72,-29],[24,13]],[[5063,7939],[-26,18],[-79,-10]],[[4958,7947],[-35,6]],[[4924,8052],[27,37],[114,-54],[29,-48]],[[4356,7530],[30,-12]],[[4386,7518],[31,-20]],[[4417,7498],[-43,-125]],[[4374,7373],[-22,-16]],[[4352,7357],[-31,-5],[-25,-52]],[[4296,7300],[-65,0]],[[4913,7726],[-17,5],[-44,115]],[[4958,7947],[44,-109]],[[4700,7719],[70,65]],[[4898,8118],[-7,-42]],[[4722,7846],[-19,10]],[[4567,7939],[71,58],[28,56],[-50,33],[9,59]],[[4557,7507],[-50,-5]],[[4507,7502],[-40,74]],[[4467,7576],[-24,20],[-47,-27],[-10,-51]],[[4185,8060],[43,-41],[-5,-71],[-97,-36]],[[4073,7992],[3,51]],[[4467,7576],[-3,-55],[-47,-23]],[[4507,7502],[-13,-74]],[[4494,7428],[-36,-42],[-84,-13]],[[4386,7261],[-34,96]],[[4494,7428],[6,-97],[-48,-94]],[[4341,7566],[-78,37],[-118,14],[-75,-79]],[[14226,14636],[-87,135],[-90,47]],[[1191,5256],[69,34],[21,40]],[[1281,5330],[50,-73]],[[1331,5257],[-52,-1],[-67,-58]],[[1212,5198],[-21,58]],[[1283,6547],[-90,-144],[-132,-114],[100,-85],[96,-122],[71,-23],[50,-38],[82,-12],[27,-74],[5,-78]],[[1492,5857],[0,-18]],[[1492,5839],[-65,-17],[-1,46],[-61,-4],[-53,-65]],[[1312,5799],[-47,-12],[-49,90]],[[1216,5877],[0,51],[-73,29],[-24,-12],[-138,55],[-58,-22],[-65,18],[-1,64],[-76,22],[-18,65]],[[1339,6287],[-15,-33],[101,-127],[86,-54],[37,-69],[32,-24]],[[1580,5980],[-18,-85],[-42,-56]],[[1520,5839],[-28,18]],[[1580,5980],[92,-26]],[[1672,5954],[13,-30],[-25,-47],[27,-71]],[[1687,5806],[-83,-46]],[[1604,5760],[-34,-17],[-50,96]],[[1158,5213],[23,-33]],[[1282,5030],[-47,17]],[[1158,5050],[-66,37]],[[1191,5256],[-37,118]],[[1154,5374],[35,18]],[[1189,5392],[70,-10]],[[1259,5382],[8,-10]],[[1267,5372],[14,-42]],[[1143,5020],[-29,-62]],[[1114,4958],[-33,-35],[-37,29],[-54,3]],[[948,4874],[49,6],[26,-64]],[[1023,4816],[-14,-55]],[[1009,4761],[-11,-43]],[[1154,5374],[-11,9]],[[1143,5383],[-50,73],[-47,9],[-16,33],[-76,59],[-8,39],[-97,88],[-75,3]],[[774,5687],[34,81],[40,-40],[80,-32],[69,1],[53,-43],[43,-8],[65,-131],[76,-11]],[[1234,5504],[14,-44],[-59,-68]],[[10262,8406],[-17,-62],[-103,-61],[15,-119],[-37,-33],[68,-67]],[[10188,8064],[-82,-164],[-40,-41],[2,-39],[-71,4],[-26,-35]],[[1591,5612],[-21,-1]],[[1570,5611],[-62,4],[-60,44]],[[1448,5659],[9,55],[40,3],[47,36]],[[1544,5753],[52,-87],[-5,-54]],[[1114,4958],[47,-46],[43,-3]],[[1204,4909],[42,-63],[-36,-60]],[[1210,4786],[-73,-13],[-114,43]],[[1252,4757],[-42,29]],[[1204,4909],[8,12],[111,-42]],[[1331,5257],[66,9]],[[1397,5266],[68,-53]],[[1095,5319],[14,71],[34,-7]],[[1570,5611],[4,-31],[-46,-75],[-12,-46]],[[1516,5459],[-30,29],[-75,24],[-18,25]],[[1393,5537],[33,33],[-36,61]],[[1390,5631],[58,28]],[[1267,5372],[89,33],[37,-23]],[[1393,5382],[4,-116]],[[1492,5839],[39,-27],[13,-59]],[[1448,5659],[-25,40],[-74,39],[-37,61]],[[693,5806],[72,64],[93,-69],[144,-26],[149,-96],[62,-17]],[[1213,5662],[87,-114]],[[1300,5548],[-24,-37],[-42,-7]],[[774,5687],[-44,53],[-37,-28],[-50,31]],[[10722,7909],[88,-19],[56,-90]],[[10866,7800],[-81,-67],[-34,-57],[-81,-25],[-117,-95]],[[10553,7556],[-32,72]],[[10521,7628],[20,60],[-23,22],[-75,-7],[-45,-29]],[[10398,7674],[7,11]],[[10405,7685],[36,54],[9,48]],[[10450,7787],[-2,65]],[[1390,5631],[-75,79],[-58,18],[-60,64]],[[1197,5792],[-6,60],[25,25]],[[1741,5447],[-34,76],[-88,73]],[[1619,5596],[-15,164]],[[1687,5806],[32,-67],[26,-15]],[[1745,5724],[66,0],[33,-41]],[[1300,5548],[50,-38]],[[1350,5510],[4,-14]],[[1354,5496],[-95,-114]],[[1009,4761],[84,3],[31,-23],[98,-31]],[[1393,5382],[-12,52],[18,51],[-45,11]],[[1350,5510],[43,27]],[[1516,5459],[10,-39],[55,-46]],[[1591,5612],[28,-16]],[[1197,5792],[-34,-68],[46,-29],[4,-33]],[[11306,8625],[-34,-55],[1,-101],[44,-105],[92,-37]],[[11409,8327],[0,-40],[52,-17],[68,21]],[[11529,8291],[21,-4]],[[11550,8287],[-21,-174]],[[11529,8113],[-3,-44],[-46,-18],[-52,-55],[-64,4],[-10,-140],[100,0]],[[11454,7860],[4,-55],[-75,-56],[-140,-64],[-51,15],[-6,41],[-65,-5]],[[11121,7736],[13,71],[63,35],[2,52],[-29,75],[-70,102],[33,81],[-11,26]],[[11039,8358],[-17,76],[-69,20],[-60,-1],[75,73]],[[1585,6112],[-5,-132]],[[11529,8113],[78,-4],[65,36],[22,-44],[-27,-28],[3,-48],[-58,-80],[-89,-37],[-69,-48]],[[11712,8489],[20,-205],[-124,19],[-58,-16]],[[11529,8291],[-11,98],[45,110],[43,18]],[[2680,8518],[15,-52]],[[2695,8466],[71,-5],[31,-43],[44,32],[99,-54]],[[2940,8396],[-44,-124]],[[2896,8272],[-77,19]],[[2819,8291],[-75,89],[-54,36],[-39,1],[-40,40]],[[2883,8147],[28,2]],[[2911,8149],[51,-19],[95,14]],[[3057,8144],[23,-9],[-22,-151],[18,-16]],[[3076,7968],[-67,-16]],[[3009,7952],[-6,34],[-68,19],[-56,52]],[[2879,8057],[-18,38],[22,52]],[[2876,7692],[11,42],[48,-3],[2,70]],[[2937,7801],[72,151]],[[3076,7968],[77,-70]],[[3153,7898],[54,-13]],[[3207,7885],[3,-54]],[[2493,8346],[19,-87],[51,-25],[34,-82]],[[2597,8152],[-34,-43]],[[2563,8109],[-118,-40],[-59,33],[-31,57],[-33,20]],[[3096,8787],[11,-30],[59,-22],[50,-66]],[[3216,8669],[-3,-53],[-87,-87]],[[3126,8529],[-105,-30],[-23,-33],[-193,71],[-110,-71]],[[3207,7885],[9,28],[92,-24],[73,-64],[20,-56]],[[2937,7801],[-34,94],[-51,5]],[[2852,7900],[-26,44]],[[2826,7944],[22,75],[31,38]],[[2826,7944],[-73,-14]],[[2753,7930],[-75,192]],[[2678,8122],[116,16],[64,-14],[25,23]],[[3153,7898],[14,113],[50,61],[30,-12],[114,199]],[[3361,8259],[88,-46]],[[3449,8213],[-47,-82],[-6,-58],[51,-49],[76,-4],[4,-65],[55,14]],[[10116,7636],[43,18]],[[10159,7654],[51,-10]],[[10210,7644],[-73,-104]],[[10137,7540],[-112,-121],[-4,-22]],[[10021,7397],[-57,39],[-39,73],[18,51],[-18,45]],[[2896,8272],[54,-17]],[[2950,8255],[7,-23],[-46,-83]],[[2678,8122],[-45,9],[-33,-41]],[[2600,8090],[-37,19]],[[2597,8152],[61,52],[66,8],[64,-20],[31,99]],[[2745,7683],[-1,33],[60,-13],[-32,-31]],[[2858,7705],[-52,34],[-15,47],[13,74],[48,40]],[[2636,7609],[-22,107],[-88,46],[-44,-13],[-95,-57]],[[2348,7684],[35,111],[27,41],[79,7],[52,39]],[[2541,7882],[141,-12],[60,-83],[-15,-37],[-67,-71],[-8,-69]],[[2950,8255],[9,3]],[[2959,8258],[24,11],[107,-39]],[[3090,8230],[-33,-86]],[[3090,8230],[-4,39],[55,35],[63,-47],[47,33],[40,-63],[81,57]],[[3372,8284],[12,-8]],[[3384,8276],[-23,-17]],[[2753,7930],[-55,-18],[-82,10],[-47,-22]],[[2569,7900],[-66,72],[-75,-19]],[[2428,7953],[140,59],[48,59],[-16,19]],[[11038,6769],[49,96],[-5,60],[-79,11],[-106,-27],[-34,40],[-26,72],[-67,30],[10,61],[-43,57],[-26,87]],[[10711,7256],[58,29],[55,-4],[-17,127],[75,-14],[24,33],[36,118],[-97,27],[63,91],[28,13]],[[10936,7676],[72,36],[76,-9],[37,33]],[[11454,7860],[110,-69],[114,-5],[54,-54],[30,-59],[7,-106],[51,-8]],[[11820,7559],[-12,-89],[-81,-78],[55,-18],[46,-63],[-16,-93]],[[11812,7218],[-55,-110],[-93,-46],[81,-73],[-53,-50],[17,-111],[54,-92],[-37,-9],[-57,-54]],[[3126,8529],[47,-7],[21,-67]],[[3194,8455],[19,-33],[-31,-34]],[[3182,8388],[-39,-34],[-112,36],[-48,-19]],[[2983,8371],[-43,25]],[[2541,7882],[28,18]],[[2983,8371],[-24,-113]],[[3182,8388],[1,-49],[47,-20],[27,26],[115,-61]],[[2428,7953],[-77,-55],[-40,23],[-22,-34],[-54,1],[-102,-101]],[[2133,7787],[-74,12]],[[2059,7799],[-159,108]],[[2131,7666],[2,121]],[[3256,8889],[30,-43]],[[3286,8846],[41,-52],[-2,-36]],[[3325,8758],[7,-68],[-52,13],[-64,-34]],[[10553,7556],[12,-30],[-44,-73],[-24,-74]],[[10497,7379],[-121,101],[-45,109],[35,55]],[[10366,7644],[89,-28],[66,12]],[[10866,7800],[37,-13],[81,37],[-1,-87],[-56,-36]],[[10927,7701],[-114,-56],[-99,-137],[-67,-70],[44,-115],[9,-65]],[[10700,7258],[-115,17],[-27,-62],[-33,21]],[[10525,7234],[11,83],[-39,62]],[[4296,7300],[-36,-80],[21,-43],[-24,-65],[3,-91]],[[4560,6897],[-52,-129],[5,-17]],[[4433,6795],[59,41],[45,70]],[[10804,8661],[-35,-41],[6,-84],[-40,-102],[-49,-71],[-68,-66],[48,-36],[-28,-36]],[[10638,8225],[-51,-75],[-43,-30]],[[10544,8120],[-131,-60]],[[10413,8060],[-23,38],[-72,-16],[-63,-39],[-22,-39],[-45,60]],[[4933,6635],[-31,135],[10,55],[40,35]],[[4291,7037],[-4,-91]],[[4287,6946],[-33,-52]],[[4287,6946],[141,-18],[3,61],[54,-3],[38,-35],[36,23]],[[13848,14767],[-91,-12]],[[13727,15065],[29,-2]],[[11409,8327],[16,310]],[[10113,7641],[100,82],[0,31],[44,70],[49,13],[-8,49],[-34,28],[36,38]],[[10300,7952],[54,-93],[74,11]],[[10450,7787],[-142,5],[-35,-44],[-114,-94]],[[4503,8494],[48,-74]],[[4551,8420],[-3,-41],[-59,-74]],[[5245,9481],[-37,-63],[-45,-35]],[[5421,8893],[-58,50],[20,27],[-44,56],[39,93],[26,25],[-29,84]],[[5375,9228],[81,30]],[[5482,9274],[55,-7]],[[5375,9228],[-52,-15],[-56,-47]],[[5007,8322],[60,13]],[[5067,8335],[38,-69],[-18,-42]],[[5087,8224],[-78,24],[-57,-83],[-29,21]],[[10936,7676],[-9,25]],[[5314,8580],[-22,46],[-70,-47],[-28,45]],[[5182,8428],[13,19],[-48,83],[17,36]],[[4665,8548],[-45,-64],[-69,-64]],[[5215,8096],[12,37],[-140,91]],[[5067,8335],[24,24],[70,-9]],[[10067,7079],[79,12],[35,21],[26,94],[49,82]],[[10256,7288],[24,-10],[64,44],[27,-8],[119,29],[35,-109]],[[10700,7258],[11,-2]],[[5776,5814],[16,-26],[56,43],[52,-23],[41,35],[144,92],[62,25],[79,131],[-13,70],[-25,29]],[[6570,6562],[25,-79],[-56,-88],[20,-76],[82,-65],[1,-44],[-25,-97],[35,-83],[-7,-31],[83,-76],[-29,-109]],[[5767,5835],[-31,36]],[[10021,7397],[-85,-110]],[[2332,6090],[77,18],[25,28]],[[1912,5746],[-57,45]],[[1855,5791],[2,53]],[[1857,5844],[88,26],[14,-16]],[[1959,5854],[10,-17]],[[1855,5791],[-48,3]],[[1807,5794],[-22,19]],[[1785,5813],[0,36],[-83,122]],[[1702,5971],[45,81]],[[1747,6052],[110,-208]],[[1753,6170],[14,-44]],[[1767,6126],[5,-63],[-25,-11]],[[1702,5971],[-17,-8]],[[1685,5963],[-13,-9]],[[1745,5724],[62,70]],[[2053,6219],[-63,-105],[21,-88],[-19,-28]],[[1992,5998],[-33,12],[-32,77],[-49,39]],[[1878,6126],[-111,0]],[[1878,6126],[34,-78],[-3,-46],[54,-24]],[[1963,5978],[31,-93],[-35,-31]],[[1685,5963],[49,-111],[51,-39]],[[2113,6136],[-7,-73],[34,-64]],[[2140,5999],[-148,-1]],[[2139,5979],[1,20]],[[1963,5978],[29,20]],[[10366,7644],[32,30]],[[10405,7685],[-47,38],[-27,-23],[-121,-56]],[[1854,6993],[-51,17],[-97,79],[-3,28],[-87,107]],[[1998,7650],[-29,21],[90,128]],[[1424,7471],[119,-140],[80,3]],[[10613,8007],[-14,63],[-55,50]],[[10638,8225],[55,-16],[50,-89]],[[5525,6079],[-64,31],[1,70],[-56,-40],[-96,-2],[-66,-22],[5,-75],[-14,-45],[-120,-22],[-90,-53]],[[5025,5921],[-49,83],[-37,-7]],[[4939,5997],[125,72],[-24,118],[70,-2],[5,29],[105,-16],[131,77],[89,-4]],[[4741,6337],[49,-116],[3,-63],[-16,-49]],[[4777,6109],[2,-17]],[[4779,6092],[-145,-48]],[[4779,6092],[-17,-47]],[[4762,6045],[-27,-18]],[[5083,6356],[-53,-58],[-62,-110],[-91,-68]],[[4877,6120],[-72,-78],[-43,3]],[[4777,6109],[59,17],[42,115]],[[5558,5794],[-35,-41],[-56,7],[-237,-105]],[[4980,5851],[45,70]],[[4939,5997],[-34,2]],[[4905,5999],[-34,3],[-21,71],[27,47]],[[4844,5941],[61,58]],[[13861,14510],[-67,6],[-5,40],[-66,20]],[[10300,7952],[113,108]],[[3606,8031],[-19,13],[14,74],[-42,67]],[[3559,8185],[-4,78],[51,49],[-51,64]],[[3555,8376],[54,53],[60,-67],[-35,-21],[47,-107]],[[4039,8152],[-29,47]],[[4010,8199],[64,37],[120,-95]],[[3412,8562],[23,48]],[[3435,8610],[121,5],[14,-38]],[[3570,8577],[-93,-96],[-34,-17]],[[3443,8464],[-27,13],[-4,85]],[[3325,8758],[65,-10]],[[3390,8748],[43,-46]],[[3433,8702],[24,-61],[-22,-31]],[[3412,8562],[-57,-56]],[[3355,8506],[-90,32],[-34,-66],[-37,-17]],[[4468,8463],[-44,20],[-1,40],[-55,9]],[[4368,8532],[-7,3]],[[4361,8535],[54,106],[19,5]],[[4438,8384],[-35,-7],[-42,52]],[[4361,8429],[7,103]],[[4131,8783],[15,-54],[-95,-84]],[[4051,8645],[-64,-63]],[[3987,8582],[-65,-43],[-64,-17]],[[3858,8522],[-52,94]],[[10137,7540],[85,-17],[53,-57],[-16,-50],[24,-56],[-38,-43],[11,-29]],[[3960,8381],[19,39],[69,39],[30,49]],[[4078,8508],[64,33],[16,-36]],[[4158,8505],[-2,-61],[-63,-82],[-38,-84],[-47,-2],[-50,-40]],[[3958,8236],[-28,6]],[[3930,8242],[-35,53]],[[3895,8295],[20,-11],[45,97]],[[3987,8582],[62,-10]],[[4049,8572],[29,-64]],[[3960,8381],[-40,6],[11,67],[-66,54]],[[3865,8508],[-7,14]],[[4267,8210],[-22,44]],[[4245,8254],[33,72],[83,103]],[[4010,8199],[-52,37]],[[4158,8505],[56,-16]],[[4214,8489],[-11,-104],[28,-47],[14,-84]],[[3384,8276],[56,30]],[[3440,8306],[56,74]],[[3496,8380],[28,27]],[[3524,8407],[31,-31]],[[3559,8185],[-110,28]],[[3355,8506],[-14,-92],[99,-108]],[[4214,8489],[94,70]],[[4308,8559],[53,-24]],[[3678,8691],[-70,-55],[-32,-63]],[[3576,8573],[-6,4]],[[3433,8702],[72,13],[43,-28],[55,2]],[[3465,8779],[-75,-31]],[[3286,8846],[72,33],[24,39],[68,-2]],[[3851,8087],[64,106],[15,49]],[[3895,8295],[-50,-7]],[[3845,8288],[-88,89]],[[3757,8377],[-7,41],[52,64],[63,26]],[[4308,8559],[-25,41],[55,51],[26,-3],[74,102]],[[3443,8464],[53,-84]],[[3576,8573],[19,-60],[-89,-69],[18,-37]],[[3784,8180],[61,108]],[[3703,8273],[4,52],[50,52]],[[4051,8645],[-2,-73]],[[13702,12063],[-22,61]],[[13680,12124],[31,67],[-41,88]],[[10567,4699],[171,37],[6,-13],[192,105],[130,32]],[[11066,4860],[1,-26]],[[11067,4834],[-8,-18],[26,-105],[-10,-20],[66,-85],[37,-20],[39,-124]],[[11217,4462],[-67,-104],[-135,-49],[-55,49]],[[10433,4996],[41,-100],[63,-58],[9,-49],[-10,-89]],[[11122,4917],[-56,-57]],[[11602,4264],[-93,-47]],[[11509,4217],[-94,-18],[-47,24],[-38,48],[-79,60],[-34,131]],[[11067,4834],[119,26],[64,-13],[69,35],[-3,-52],[32,-38],[-91,-98],[104,18],[136,-40],[55,16],[54,41],[5,59]],[[11509,4217],[-20,-80],[56,-53],[19,-89],[59,-32],[7,-72],[43,-57],[-53,-23],[-70,-66],[-50,9],[-46,-22],[-13,-87],[-37,-66],[-61,-77],[-95,-77],[-2,-80],[16,-37],[-6,-63],[-45,-27],[-72,42]],[[9417,9851],[-54,-31],[-13,-32],[-53,-14],[-44,-43],[-76,11],[-46,-60],[-31,-10]],[[9100,9672],[-15,5]],[[9085,9677],[-13,25],[40,82],[-10,64],[-83,-25],[6,41]],[[9025,9864],[83,57],[21,51]],[[9129,9972],[20,-46],[56,-15],[57,18],[54,-37],[50,-3],[52,-27]],[[6831,4365],[25,-18],[94,10],[65,-172],[-15,-150],[90,-75],[83,-104]],[[7173,3856],[-82,-48]],[[7091,3808],[-51,33],[-28,-33],[-49,36],[11,68],[-52,30],[-71,145],[-89,84],[-63,18]],[[6699,4189],[55,82],[5,32],[72,62]],[[6462,4223],[48,9],[44,-76],[46,-3],[99,36]],[[7091,3808],[-95,-78]],[[7335,3944],[-57,-48]],[[7278,3896],[-52,91],[36,26],[-43,105],[-61,13],[-122,-109],[6,195],[-40,44],[-34,75],[-1,71],[-64,17],[-72,-59]],[[7173,3856],[105,40]],[[9085,9677],[-100,25]],[[8985,9702],[-32,58],[23,36]],[[8976,9796],[49,68]],[[6475,1261],[-86,282]],[[6389,1543],[58,40],[52,60],[41,115],[-4,42],[35,104],[106,21],[6,54],[93,45],[122,-80],[164,-17]],[[7062,1927],[16,-93],[72,-97],[113,-3],[8,-24],[-34,-104]],[[6389,1543],[-44,13],[-32,105],[43,54],[59,188],[-18,24],[30,96],[26,193],[-22,77],[72,89],[52,35],[176,-31]],[[6731,2386],[18,-31],[146,-38],[56,-141],[124,-64],[41,-139],[-54,-46]],[[6219,2725],[32,-63],[85,43],[105,23],[196,-59]],[[6637,2669],[30,-84],[54,-66],[10,-133]],[[6234,2949],[34,6],[101,-66],[123,-2],[19,-11],[102,9],[51,47],[24,-86],[-60,-116],[9,-61]],[[9502,9258],[-132,-29]],[[9370,9229],[-39,50],[28,87],[-9,28]],[[9350,9394],[19,61],[-10,37],[-53,-16],[-27,36],[8,110]],[[9287,9622],[23,-33],[91,-2],[89,45]],[[11855,5545],[4,50]],[[11859,5595],[25,89],[-59,67],[13,49],[-32,47],[-31,159],[28,29],[60,12],[57,56]],[[11920,6103],[16,-117],[34,-36],[2,-47],[54,-86],[-8,-53],[-30,-32]],[[11988,5732],[-42,-66],[14,-31],[-105,-90]],[[11888,5236],[29,28],[83,13],[45,73],[81,13],[195,95],[74,59],[27,45],[90,8]],[[12512,5570],[103,-19]],[[11625,5309],[71,54],[36,58]],[[11732,5421],[123,124]],[[11988,5732],[35,-32],[45,48],[78,-87],[34,-93],[105,56],[47,-10],[29,46],[57,21],[40,-65],[54,-46]],[[11558,5713],[39,-6],[58,24],[67,-26],[21,-46],[48,-42],[68,-22]],[[11732,5421],[-112,127],[-75,63],[-51,15]],[[12051,6465],[28,-56],[-52,-61],[-78,-69],[1,-54],[-31,-53],[1,-69]],[[8927,8791],[-2,98]],[[8925,8889],[78,20],[53,-16]],[[9056,8893],[-46,-107]],[[9129,9186],[-8,-4]],[[9121,9182],[-95,22],[-56,-21]],[[8970,9183],[31,71]],[[9001,9254],[52,67]],[[9053,9321],[87,-34],[-11,-101]],[[12715,5695],[33,72],[-22,37],[76,70],[-33,75],[-9,72],[56,57],[144,42],[16,26],[-47,43],[-39,91],[-55,-18],[-60,13],[-39,-13],[-29,55],[-63,-41],[-13,60],[-128,111],[-55,88]],[[9100,9672],[5,-51],[61,-23]],[[9166,9598],[-27,-59]],[[9139,9539],[-49,-17]],[[9090,9522],[-52,-4],[-53,-28],[-42,34],[-41,76],[-34,19]],[[8868,9619],[-6,15]],[[8862,9634],[47,44],[53,-43],[23,67]],[[7979,2168],[60,59],[-9,49],[-42,32],[-9,55],[20,165],[-47,46],[37,94],[3,105],[-46,46],[36,59],[-20,132],[-9,166]],[[7953,3176],[41,38],[62,25],[23,46],[11,75],[45,59],[45,1],[27,-84],[50,52],[82,-11],[91,31]],[[7612,3540],[20,-9],[95,104],[13,83],[26,56],[70,29],[50,-32]],[[7886,3771],[23,-69],[77,-91],[-40,-33],[-36,-99],[9,-101],[33,-40],[8,-52],[-7,-110]],[[7886,3771],[-4,12]],[[7882,3783],[84,-18],[112,35],[150,-71],[59,-38],[28,-85],[48,-49],[71,32]],[[7856,4046],[-6,-34],[34,-54],[-2,-175]],[[8970,9183],[-8,-89],[-37,-4]],[[8925,9090],[10,45],[-58,21],[-54,77]],[[8823,9233],[48,41],[42,-8]],[[8913,9266],[39,-22],[49,10]],[[13686,15115],[-42,92],[30,50]],[[13674,15257],[75,-54],[80,-17],[10,-21]],[[8925,8889],[-29,48],[-57,50]],[[8839,8987],[65,40]],[[8904,9027],[24,1],[52,-85],[107,-19],[-31,-31]],[[9121,4250],[28,55],[43,38],[23,73],[-11,124],[10,108],[74,22],[64,43]],[[9352,4713],[-34,-54],[-9,-90]],[[9309,4569],[-44,-25],[18,-157],[-41,-130],[-62,-77]],[[8987,5166],[181,135],[103,-8]],[[9271,5293],[-9,-70],[9,-89],[-11,-88],[-64,-50]],[[9196,4996],[-122,-87],[-57,16]],[[9017,4925],[4,33],[-48,163],[14,45]],[[9721,3896],[-112,8],[-149,63]],[[9460,3967],[30,82],[11,107],[-18,102],[19,82],[-46,140]],[[9456,4480],[-35,128],[27,38],[140,37],[25,-18],[54,-103]],[[8782,5357],[95,0],[-14,-42],[64,-69],[-64,-52],[53,-23],[13,-63],[58,58]],[[9017,4925],[-8,-20]],[[9009,4905],[-89,114],[-63,33],[-9,48],[-110,143]],[[8720,8792],[12,72],[-20,67]],[[8712,8931],[92,38]],[[8804,8969],[49,-176],[-3,-57]],[[9497,5170],[-63,-77],[16,-62],[-11,-61],[6,-116],[-29,-97]],[[9416,4757],[-64,-44]],[[8827,4491],[-39,147],[20,105]],[[8808,4743],[113,44],[87,14],[24,78],[-23,26]],[[9196,4996],[37,-62],[-46,-83],[110,-37],[59,86],[9,61],[-54,57],[8,69],[35,57],[-17,43],[6,61],[79,57],[23,37]],[[9390,5383],[-30,-59],[-89,-31]],[[9309,4569],[102,5],[45,-94]],[[9460,3967],[-43,-113],[-30,-212],[-5,-192],[-28,-95],[11,-43],[97,-108],[28,-67]],[[8757,5016],[55,-9],[47,-76],[-57,-122],[6,-66]],[[9672,4720],[-85,20],[-43,28],[-34,-14],[-94,3]],[[8913,9266],[-25,81],[-77,40],[-47,79],[43,50],[12,56],[49,47]],[[9090,9522],[29,-80]],[[9119,9442],[-93,-63],[27,-58]],[[9129,9972],[66,64]],[[5927,2456],[-6,69],[30,86],[60,42],[43,72],[-27,97],[-44,24]],[[5983,2846],[33,11],[21,71],[108,35]],[[6123,3876],[36,-32]],[[6159,3844],[15,-49],[35,-28],[113,-40],[105,-4]],[[6427,3723],[14,-42]],[[6441,3681],[101,-178]],[[6542,3503],[-85,-58],[-42,-68]],[[6415,3377],[-140,-128],[-141,-8],[-13,9],[-105,-32]],[[7143,3176],[-24,71],[-64,75],[-72,170],[-97,112],[78,47],[38,71]],[[6852,3689],[-99,-12]],[[6753,3677],[-37,49],[-147,-7],[-78,-46],[-50,8]],[[6427,3723],[-10,79],[-64,46],[-194,-4]],[[5862,2427],[-59,96],[-48,45],[-23,58],[-59,-10],[12,62],[-30,33],[3,77],[-56,52],[26,37],[-27,44]],[[5799,3110],[41,-12],[23,48],[145,-50],[-61,-88],[16,-38],[-32,-109],[52,-15]],[[6856,3242],[12,51],[-37,59],[-33,109],[-82,91]],[[6716,3552],[11,92],[26,33]],[[9281,8963],[-5,49],[-57,76],[-72,-13],[-26,107]],[[9129,9186],[58,-14],[68,36]],[[9255,9208],[17,-31],[80,16],[18,36]],[[6716,3552],[-57,-40],[-21,19],[-96,-28]],[[6460,3166],[36,87],[-81,124]],[[9139,9539],[59,-18],[-5,-46],[37,-41]],[[9230,9434],[-22,-36],[-44,7]],[[9164,9405],[-45,37]],[[4631,3712],[48,-23],[74,11],[56,-34],[56,83],[164,31],[11,-40],[-53,-58],[68,-22],[54,-61],[50,-100]],[[4882,4596],[84,-18],[64,43]],[[5030,4621],[-38,-162],[38,-22],[34,81],[-1,133]],[[5063,4651],[84,2],[51,40],[13,37],[-49,101]],[[5162,4831],[115,40]],[[5438,4511],[-26,-47],[30,-78],[-60,11],[-57,-22],[-28,73],[-46,-31],[-68,41],[-25,73],[-75,-65],[54,-37],[104,-96],[44,-4],[25,-64]],[[5310,4265],[-77,23],[-80,44],[-58,-50],[-73,58],[-53,2],[-65,-43],[-46,42]],[[4676,4106],[59,-22],[25,-40],[-55,-84],[54,-44],[27,-58]],[[4786,3858],[-29,-31],[-68,-22],[-27,49],[-61,-20]],[[5342,3905],[-42,34],[12,85],[-11,36],[18,81],[-36,21],[27,103]],[[5030,4621],[33,30]],[[5006,5128],[-108,-54],[-44,14],[-112,0],[-62,55]],[[4680,5143],[40,46],[41,7],[22,130],[41,65]],[[4680,5143],[-105,-35],[-30,-37],[-86,5],[-32,-19]],[[5338,3856],[-117,100],[-81,55],[-97,-12],[-159,0],[-55,-43],[-43,-98]],[[4668,4798],[140,35],[30,24],[67,7],[96,-19],[65,36],[96,-50]],[[8823,9233],[-105,2],[-72,-67]],[[8646,9168],[-46,103]],[[8600,9271],[-34,93]],[[8566,9364],[36,49],[41,22],[-6,60],[69,16],[-13,64],[9,46]],[[8702,9621],[160,13]],[[9164,9405],[19,-23],[-25,-56],[31,-45],[64,-29],[2,-44]],[[11549,4909],[-29,16],[-124,171],[-45,76],[77,57]],[[11428,5229],[46,-53],[13,-53],[63,-45],[37,-96],[-38,-73]],[[11066,5279],[47,-107],[24,-94],[60,-59]],[[11428,5229],[-16,31]],[[11412,5260],[76,35],[41,70],[45,-32],[49,33]],[[11346,5584],[56,-77],[-50,-98],[0,-49],[-28,-58],[43,-67]],[[11367,5235],[-48,-37],[-128,-155],[11,-8]],[[8904,9027],[21,63]],[[10201,4732],[-116,55],[-11,65],[146,140],[-4,27],[58,101]],[[10241,5431],[-84,-34],[-26,13]],[[10131,5410],[-43,26],[-161,165],[-3,76],[-62,35],[-64,97]],[[9927,4213],[31,91],[8,73],[55,103],[-51,191]],[[9970,4671],[73,-7],[-14,-37],[71,-48],[67,-13]],[[9670,4687],[69,11],[76,-13],[75,-33],[37,1]],[[9927,4653],[4,-69],[-76,-111],[-37,-34],[-48,-79],[-89,-47]],[[9627,5680],[62,-93],[-33,-65],[96,-52],[-15,-117],[54,-92],[69,26],[73,-37]],[[9933,5250],[-7,-64],[-23,-22]],[[9903,5164],[-60,35],[-114,-21]],[[9669,5715],[115,-101],[15,-56],[54,-73],[23,-8],[32,-90],[76,-87]],[[9984,5300],[-51,-50]],[[9672,4720],[60,31],[90,-14],[-17,73],[81,127],[2,184],[15,43]],[[9984,5300],[44,12]],[[10028,5312],[40,-109],[-12,-140],[-27,-74],[-54,-68],[-27,-81],[-23,-113],[7,-42]],[[9932,4685],[-5,-32]],[[9970,4671],[-38,14]],[[10028,5312],[18,34],[85,64]],[[14051,14136],[67,48],[41,8],[27,53]],[[14186,14245],[73,-53],[24,-53],[72,116]],[[14355,14255],[78,-57]],[[14433,14198],[3,-130]],[[14277,13888],[-20,0]],[[14257,13888],[-29,23],[-75,14],[-101,-2]],[[9287,9622],[-78,7],[-43,-31]],[[11367,5235],[45,25]],[[6078,3625],[-57,33],[-50,-66],[-107,-30],[-25,-29]],[[5839,3533],[-40,131],[25,56]],[[5824,3720],[0,37]],[[5923,3360],[-74,68],[-4,79]],[[5845,3507],[-6,26]],[[5348,3638],[70,-38],[85,-5],[82,58],[5,22],[90,29],[25,28],[100,1],[19,-13]],[[5845,3507],[-50,-22],[-106,-3]],[[5689,3482],[-85,-8],[-68,-23],[6,-68],[-12,-91],[-47,-71],[-70,-12],[-175,60],[-137,-22]],[[5058,2840],[53,-24],[57,48],[42,-8],[32,36],[93,32],[102,-22],[4,49],[48,45],[-37,41],[50,26],[84,-46],[18,-29],[-8,-68]],[[5663,4073],[-19,-14]],[[5644,4059],[-53,4],[-47,-37],[-65,14],[-91,-118]],[[5364,3643],[42,14],[74,64],[21,38],[58,40],[3,76],[44,30],[38,154]],[[5689,3482],[23,-48],[80,-52],[-19,-27],[79,-80],[-28,-68],[-48,-43],[42,-44]],[[8804,8969],[35,18]],[[8566,9364],[-21,108],[-75,104],[-41,33]],[[8429,9609],[132,17]],[[8561,9626],[27,-10],[81,25],[33,-20]],[[8514,3994],[-94,70],[-8,96],[-50,107],[-123,78]],[[8239,4345],[75,43],[82,24],[108,73]],[[8504,4485],[30,-42],[111,-59]],[[8645,4384],[62,-68],[-21,-174],[-129,-125],[-43,-23]],[[8504,4485],[58,44],[57,20],[37,-40]],[[8656,4509],[-42,-31],[-10,-64],[41,-30]],[[7928,4201],[38,37]],[[7966,4238],[87,49],[186,58]],[[8514,3994],[4,-37]],[[8278,5248],[-35,-78],[57,-71],[-32,-49]],[[8268,5050],[-63,-94],[-65,-59],[-23,-80],[27,-42],[-76,-77],[-13,-95],[10,-34],[-58,-39],[-20,-37],[-8,-86],[-20,-39]],[[7959,4368],[-25,58]],[[8268,5050],[3,-31],[-41,-59],[63,-92],[-44,-38],[-58,-1],[8,-74],[-27,-7],[-9,-96],[-37,-4],[42,-129],[46,9],[-11,-67]],[[8203,4461],[-158,-95]],[[8045,4366],[-44,-24],[-42,26]],[[8045,4366],[19,-31],[-98,-97]],[[8785,4479],[-65,-23],[-64,53]],[[8203,4461],[125,32],[115,82],[5,45]],[[8448,4620],[43,44],[125,8],[53,59]],[[8619,4875],[-29,-40],[-73,-11],[0,-49],[-102,-72],[33,-83]],[[8976,9796],[-137,70],[44,81]],[[7760,4403],[-110,-27]],[[7650,4376],[-18,109],[-49,39],[-44,115],[41,179],[-14,47],[21,137]],[[7813,4283],[-82,-15],[-59,-42],[-60,9]],[[7612,4235],[42,108],[-4,33]],[[7612,4235],[-60,-49],[-100,-42],[-60,17],[-60,-35]],[[7524,3809],[-31,93]],[[8561,9626],[-25,76],[-69,44]],[[9350,9394],[-49,-25],[-71,65]],[[10397,4715],[-11,-20]],[[10386,4695],[-195,0]],[[11028,3696],[-89,20],[-130,-57],[-29,19],[-111,-12],[-40,-48],[-43,-13],[57,-63],[-63,-35],[-99,-31],[-76,-139]],[[10405,3337],[-70,120],[-47,46],[14,87],[-9,57]],[[10293,3647],[17,54],[36,29],[22,63],[-1,131]],[[10367,3924],[27,1],[23,70]],[[10417,3995],[61,-70],[46,-10],[99,-75],[78,-2],[39,19]],[[10740,3857],[71,-3],[22,-31],[96,-21],[92,-56]],[[9938,4148],[134,-81],[46,35],[84,-97]],[[10202,4005],[-91,-92]],[[10111,3913],[-92,59],[-99,-13],[7,-55],[-140,-26],[-63,26]],[[10293,3647],[-107,39],[-123,168],[48,59]],[[10202,4005],[25,25]],[[10227,4030],[76,-46],[64,-60]],[[10575,4667],[-78,-22],[-111,50]],[[9678,3262],[101,-52],[81,27],[22,-50],[67,-2],[47,-34],[109,43],[91,1],[36,16],[30,-58],[55,-28]],[[10317,3125],[19,-37],[-14,-53],[-32,-31],[-10,-87]],[[10280,2917],[-86,-44],[-106,-39],[-61,12],[-64,72],[-41,-12],[-71,23],[-5,-38],[-116,-4],[-58,-31],[-75,-2],[-42,-22],[-42,-50],[-79,4],[-58,-18]],[[10559,2216],[-21,127],[-73,19],[-43,67]],[[10422,2429],[13,111],[47,75]],[[10482,2615],[11,40],[81,101],[56,-3],[48,120],[77,92],[-31,54],[33,50],[-37,57]],[[10720,3126],[178,24],[138,98],[80,28]],[[11071,3603],[-25,-26],[-83,-17],[-70,-87],[-145,-62],[-40,2],[-26,-55],[17,-111]],[[10699,3247],[-138,-44],[-88,58],[-73,-42]],[[10400,3219],[5,118]],[[10317,3125],[83,94]],[[10699,3247],[23,-73],[-2,-48]],[[10482,2615],[-108,37],[-57,98],[15,83],[-52,84]],[[10227,4030],[43,33],[69,102],[-16,44],[2,75],[-18,46]],[[10307,4330],[85,3]],[[10392,4333],[48,-97],[66,-8],[94,-86]],[[10600,4142],[1,-43],[-79,53],[-40,-37],[-115,45],[-6,-51],[56,-114]],[[10600,4142],[124,-83],[-23,-76],[19,-26],[-9,-76],[29,-24]],[[10422,2429],[-106,-46],[-59,-10],[-80,13],[-57,47],[-77,24],[-51,69],[-6,61],[-49,43],[-78,-28],[-56,37],[-85,20],[-45,36],[-100,-23],[-102,63],[-54,-31],[-41,64]],[[10231,4440],[51,-53],[25,-57]],[[10676,4535],[-89,-26],[-81,61],[-27,-44],[-76,1],[-1,-87]],[[10402,4440],[-36,117],[-139,-7],[-60,16]],[[10392,4333],[10,107]],[[14306,14348],[49,-93]],[[14186,14245],[-41,72],[10,23]],[[8429,9609],[-59,59]],[[8712,8931],[-19,45]],[[8693,8976],[-9,21]],[[8684,8997],[-38,171]],[[14433,14198],[53,38],[25,-54],[69,-5]],[[4949,9912],[-22,29],[-109,32],[-26,-39],[-70,-52]],[[4722,9882],[-83,11],[-21,-26],[-67,-10],[-72,50],[-17,39],[-111,-77],[-53,31]],[[4298,9900],[-64,70]],[[4234,9970],[69,-30],[89,66],[82,24],[47,34],[85,-10],[74,65]],[[4680,10119],[71,-93],[43,-8],[120,14],[52,-38]],[[4781,9805],[-41,27],[-18,50]],[[4234,9970],[7,60]],[[4241,10030],[160,100],[47,15],[-87,150],[80,46],[96,122]],[[4537,10463],[82,-41],[66,-79],[122,-78]],[[4807,10265],[9,-49]],[[4816,10216],[5,-45],[-97,-11],[-44,-41]],[[4816,10216],[35,1],[59,-45],[113,46],[46,1],[12,-53]],[[4136,9834],[-8,32],[170,34]],[[4925,11042],[-112,-150]],[[4813,10892],[-35,36],[-62,108],[-73,-53],[-17,-31],[-81,-27],[-56,-74]],[[4489,10851],[-7,70],[-68,42],[-43,107]],[[4371,11070],[18,43],[52,27],[56,58],[88,45],[1,72],[-64,67]],[[4371,11070],[-59,19],[-78,-4]],[[4813,10892],[-170,-299],[-106,-130]],[[4537,10463],[-77,37]],[[4460,10500],[-154,82]],[[4306,10582],[-7,24],[102,72],[52,65],[36,108]],[[3438,10277],[132,-72],[29,50],[160,98]],[[3759,10353],[22,-42],[6,-69],[56,-93]],[[3843,10149],[-21,-20]],[[3822,10129],[-132,-117],[-90,8],[-117,-64],[-107,-7],[-97,-27],[-76,2],[-62,-27],[-125,56],[-57,9],[-64,68],[-29,55]],[[4241,10030],[-36,-20],[-108,72]],[[4097,10082],[24,85],[110,84],[32,68],[87,70],[13,35],[97,76]],[[4017,10666],[67,-59]],[[4084,10607],[99,-46],[-123,-33],[-53,-61],[-248,-114]],[[4084,10607],[110,0],[75,-68]],[[4269,10539],[-61,-46],[-50,-60],[-32,-69],[-42,-50],[-78,-53],[-95,-38],[-68,-74]],[[4097,10082],[-120,84],[-29,2],[-93,-56],[-33,17]],[[4269,10539],[37,43]],[[5825,10545],[-38,57],[-50,-63],[-37,-17]],[[5700,10522],[-64,42],[34,106],[47,33]],[[5717,10703],[82,-5],[47,-66]],[[5717,10703],[-51,-5],[-47,25],[-21,45]],[[5598,10768],[3,2]],[[5601,10770],[47,72],[27,-58],[38,-18],[54,44],[-13,48],[51,48]],[[5805,10906],[111,-83],[-12,-25],[31,-94]],[[5601,10770],[-27,100]],[[5574,10870],[65,91],[77,159]],[[5716,11120],[26,2],[99,-92]],[[5841,11030],[-59,-103],[23,-21]],[[5486,10380],[-40,117],[-35,45],[41,37]],[[5452,10579],[76,-71],[15,-85]],[[5841,11030],[110,-91],[75,49],[36,-20]],[[5716,11120],[56,142]],[[5452,10579],[83,123],[63,66]],[[5700,10522],[-25,-50],[-51,8],[-14,-58]],[[5574,10870],[-71,-179],[-51,-71],[-77,-31],[-201,-161],[19,65],[-96,30],[-86,-42],[-20,-81],[-66,-60],[-63,-25],[-4,-43],[-51,-7]],[[2740,9342],[182,53]],[[2922,9395],[64,-99],[104,-91]],[[2788,9992],[78,-39],[8,-66],[67,-164],[13,-128],[28,-2],[34,-63],[-44,-40],[-50,-95]],[[14257,13888],[-21,-97],[-71,-6],[-13,-30]],[[410,3854],[96,-43],[56,-3]],[[562,3808],[-30,-84],[-21,-4]],[[511,3720],[-56,-5],[-59,35]],[[396,3750],[-5,70],[19,34]],[[1590,4368],[-51,-1],[-29,33],[-48,-8],[-54,53]],[[1514,4595],[44,-42]],[[196,3675],[95,-1]],[[291,3674],[6,0]],[[297,3674],[47,-107]],[[344,3567],[-77,-47]],[[267,3520],[-24,43],[-38,12],[-54,-54],[-42,20]],[[144,3394],[16,75],[107,51]],[[344,3567],[-15,-80],[101,-49]],[[430,3438],[-61,-56]],[[734,3471],[38,48]],[[772,3519],[38,10],[98,-58],[59,34],[23,-10]],[[990,3495],[41,-71]],[[887,4359],[115,89]],[[1002,4448],[11,-89],[78,-49]],[[1065,4234],[-61,43],[-69,-52]],[[935,4225],[-29,48]],[[562,3808],[27,6],[50,68]],[[639,3882],[30,6],[41,58],[36,-18],[-28,-118]],[[718,3810],[-12,-9]],[[706,3801],[9,-116],[-55,-10],[-18,-55],[65,-31]],[[707,3589],[-58,-41],[-8,-96]],[[641,3452],[-74,88],[-64,-66]],[[503,3474],[-20,99],[-40,54]],[[443,3627],[32,10],[36,83]],[[995,3608],[-37,62],[-32,94]],[[926,3764],[52,7],[91,-20],[2,-51]],[[1071,3700],[-81,-61],[5,-31]],[[443,3627],[-99,-60]],[[297,3674],[99,76]],[[1327,3922],[-80,-139]],[[1247,3783],[-31,-85]],[[1216,3698],[-51,17],[-21,45]],[[1144,3760],[43,114]],[[1187,3874],[24,38],[83,8]],[[1294,3920],[33,2]],[[1327,3922],[12,15]],[[1339,3937],[9,-81],[-40,-68],[-61,-5]],[[1392,4160],[5,-40],[49,-12],[42,48],[32,-16],[-7,-50],[39,-15]],[[1552,4075],[-49,-63]],[[1503,4012],[-44,-7],[-48,-58]],[[1411,3947],[16,63],[-9,54],[-41,14],[-48,49]],[[1329,4127],[63,33]],[[893,4651],[64,-46],[-4,-83],[59,-2],[-10,-72]],[[1369,4397],[59,-45],[44,-53]],[[1472,4299],[-35,-22],[-45,-77],[0,-40]],[[1329,4127],[-49,43]],[[1329,4127],[-75,-95],[9,-55],[31,-57]],[[1187,3874],[-19,87]],[[706,3801],[27,-87],[41,-64]],[[774,3650],[28,-28],[-35,-40]],[[767,3582],[-60,7]],[[1590,4368],[-28,-50]],[[1562,4318],[-56,7],[-34,-26]],[[6914,9779],[-9,-108]],[[6905,9671],[-41,-17],[-37,84]],[[6762,10032],[97,11]],[[6859,10043],[100,-13]],[[6959,10030],[-46,-61],[65,-97],[-28,-79]],[[6950,9793],[-35,15]],[[6817,9765],[-30,-5]],[[238,4026],[-27,-86]],[[211,3940],[-1,-45],[82,-157],[-1,-64]],[[1144,3760],[-24,3],[-22,-78],[-27,15]],[[926,3764],[2,76],[-82,-1],[-7,45]],[[839,3884],[-4,28],[45,85],[-7,44]],[[873,4041],[82,79]],[[955,4120],[95,-30]],[[1002,4448],[33,27],[120,-42]],[[935,4225],[37,-62],[-17,-43]],[[873,4041],[-29,23]],[[844,4064],[-56,103]],[[1834,4317],[-80,-89],[-56,-22],[-72,9]],[[1626,4215],[-17,74],[-47,29]],[[1815,4053],[-67,20],[-56,-22],[-71,2],[-48,27]],[[1573,4080],[-31,52],[9,32],[56,8],[19,43]],[[503,3474],[-36,8],[-37,-44]],[[995,3608],[-35,3],[-125,57]],[[835,3668],[4,65],[-45,105]],[[794,3838],[45,46]],[[1573,4080],[-21,-5]],[[7305,9871],[-12,88],[8,100]],[[844,4064],[-45,-39],[-87,35],[-21,-34],[-47,4],[-31,-26]],[[613,4004],[-86,95],[-39,19]],[[718,3810],[42,-10],[34,38]],[[835,3668],[-61,-18]],[[767,3582],[5,-63]],[[995,3608],[6,-2]],[[1001,3606],[-11,-111]],[[1562,3916],[16,74],[-75,22]],[[1448,3874],[-37,73]],[[211,3940],[71,1],[47,-77],[81,-10]],[[613,4004],[28,-29],[-2,-93]],[[1244,3686],[-28,12]],[[1339,3937],[72,10]],[[1001,3606],[43,3],[78,-74],[64,-12]],[[6848,10145],[31,-81],[-20,-21]],[[6905,9671],[3,-75],[-17,-23]],[[6950,9793],[-1,-99]],[[6949,9694],[8,-27],[80,-54],[-3,-87]],[[7241,9723],[-92,-6],[-93,12],[-58,-46],[-49,11]],[[6959,10030],[45,71],[1,64],[25,17]],[[13995,15161],[-89,92],[-32,81],[12,82],[-41,61]],[[6873,13452],[93,-18],[44,-28]],[[7010,13406],[-36,-54],[26,-39]],[[7000,13313],[-18,-37],[-53,-35],[-79,-130],[-89,20],[-90,51]],[[6921,13685],[74,2],[32,-28],[70,-20],[62,-106],[99,4]],[[7258,13537],[-42,-71],[3,-37]],[[7219,13429],[-103,6],[-106,-29]],[[7468,12829],[-10,7]],[[7458,12836],[-18,58],[32,37],[-16,63],[3,105]],[[7459,13099],[98,-20]],[[7557,13079],[18,-176],[-40,-74]],[[8124,13359],[25,-174],[-13,-58],[21,-73],[-61,-22],[-57,10],[-25,114],[33,78],[16,102],[35,44]],[[7557,13079],[-4,90]],[[7553,13169],[57,74]],[[7610,13243],[124,-53],[55,-157],[56,31]],[[7845,13064],[181,-56],[11,-153]],[[8003,13419],[-39,-37],[-20,-59],[-57,-89],[-84,3],[10,-116],[32,-57]],[[7610,13243],[20,52],[-25,20],[-8,113],[34,72]],[[7631,13500],[138,25],[113,-14]],[[7458,12836],[-194,28],[13,177],[-27,141]],[[7250,13182],[63,23],[18,29]],[[7331,13234],[64,-17],[52,-40]],[[7447,13177],[12,-78]],[[7258,13537],[45,26],[49,-15],[124,11],[114,-8]],[[7590,13551],[44,-4],[-3,-47]],[[7553,13169],[-30,40],[-76,-32]],[[7331,13234],[9,73],[-121,122]],[[7250,13182],[-72,-19],[-61,39],[-8,31],[-61,59],[-48,21]],[[7577,13718],[-45,13],[-167,7],[-25,31]],[[7340,13769],[34,16],[33,71]],[[8499,12969],[-118,44],[-39,67],[-63,-5],[-4,101],[88,48],[115,-54],[70,22]],[[7258,13537],[16,68]],[[7274,13605],[253,-12]],[[7527,13593],[51,-5],[12,-37]],[[7609,13715],[-17,-42],[-65,-80]],[[7274,13605],[3,68],[23,36],[4,58],[36,2]],[[6388,12784],[43,10],[23,58],[103,79],[92,-24],[35,-28],[-81,-98],[-15,-44]],[[5953,12302],[-11,37],[-116,24],[-48,62],[-60,-13],[-73,88],[28,88],[115,52]],[[5788,12640],[72,3]],[[5860,12643],[-19,-72],[104,-84],[28,-4],[31,-58],[-15,-72],[-30,-40]],[[5047,12015],[24,4],[92,-38],[29,24],[38,-50],[73,-34],[46,-66]],[[5618,11886],[-64,-1],[-27,21],[-2,62],[-25,58]],[[5500,12026],[46,28],[-4,44]],[[5542,12098],[79,6],[11,33],[74,-9]],[[5706,12128],[36,35],[29,-58],[38,-17],[18,-52]],[[5706,12128],[4,65],[42,52]],[[5752,12245],[72,-5],[44,-43],[34,-2]],[[5788,12640],[-24,59],[-118,-51],[-36,93],[43,28],[16,17],[27,-9]],[[5696,12777],[56,-12],[48,-34],[120,48],[75,-9]],[[5995,12770],[54,-25],[-75,-38],[-93,-73],[-21,9]],[[6074,12362],[-3,97],[63,82],[54,-16]],[[6188,12525],[60,-49],[90,43],[111,15]],[[6188,12525],[38,73],[-32,86]],[[6194,12684],[72,40],[54,-3]],[[5995,12770],[50,52],[33,71]],[[6171,12893],[27,-77],[-109,-60],[3,-61],[50,-51],[52,40]],[[5386,11871],[26,50],[-30,43],[118,62]],[[5542,12098],[-7,67],[-32,114],[-107,-5]],[[5396,12274],[67,50],[27,74],[64,14],[59,-58],[50,-7],[12,-34],[77,-68]],[[5335,12254],[61,20]],[[5685,12795],[11,-18]],[[6220,13718],[-35,163]],[[6185,13881],[96,75],[28,7]],[[6309,13963],[39,-50],[87,14],[35,-12]],[[6470,13915],[-14,-31],[16,-83]],[[6472,13801],[-13,-1],[-80,-119]],[[6379,13681],[-56,33],[-103,4]],[[6507,14332],[-103,-19],[-26,-54],[-41,-18]],[[6337,14241],[-46,48]],[[6291,14289],[-41,78]],[[5845,14387],[-54,-77],[-31,0],[-27,-103],[-65,-7],[-21,-62]],[[6612,14116],[-142,-33],[-116,0],[-68,48]],[[6286,14131],[25,88],[26,22]],[[5651,14097],[79,-36],[39,-49],[0,-49]],[[5769,13963],[21,-112]],[[5790,13851],[-15,-9]],[[5775,13842],[-115,-41],[-12,-38]],[[5648,13763],[-50,-24],[-69,0]],[[5529,13739],[-47,116],[-34,30]],[[6185,13881],[-59,4],[-31,36],[-76,6]],[[6019,13927],[-3,75]],[[6016,14002],[34,43],[-30,76]],[[6020,14121],[117,24],[71,-5],[72,-25]],[[6280,14115],[31,-13],[-2,-139]],[[6472,13801],[36,-5],[64,-44],[69,-141],[16,-100],[-56,-33]],[[6601,13478],[-59,-23]],[[6542,13455],[-61,84],[-16,48],[-58,45],[-28,49]],[[5769,13963],[40,-13],[56,30],[97,-10],[54,32]],[[6019,13927],[-41,-87],[-55,7],[-68,-39],[-65,43]],[[6542,13455],[-46,-29],[-82,4],[-80,-28],[-110,-2],[-64,-47],[-48,-5]],[[6112,13348],[56,94],[-31,102],[30,53],[-15,81]],[[6152,13678],[68,40]],[[5775,13842],[46,-91],[52,-2],[78,-145]],[[5951,13604],[-92,-20],[-55,-30]],[[5804,13554],[-92,154],[-64,55]],[[6286,14131],[-6,-16]],[[6020,14121],[-16,43],[41,32]],[[6045,14196],[109,32],[137,61]],[[5529,13739],[-61,-53],[-15,-46],[-38,-1]],[[5415,13639],[-83,-7],[-19,16]],[[5712,13375],[32,77],[5,72],[55,30]],[[5951,13604],[44,-1],[56,57],[101,18]],[[6112,13348],[-93,6],[-192,-19]],[[5415,13639],[-15,-116],[-34,-13],[-8,-64],[36,-66],[-49,-20],[-19,80],[-67,-6],[-127,24]],[[6045,14196],[5,115]],[[6567,13948],[-97,-33]],[[6601,13478],[49,-70],[47,13],[93,-42]],[[7910,12801],[10,-47],[-43,-22],[-6,-112]],[[7871,12620],[-75,-15],[-29,37]],[[7767,12642],[-15,74],[34,54]],[[7786,12770],[124,31]],[[7476,12530],[-5,-92]],[[7471,12438],[-42,8],[-119,-54]],[[7310,12392],[-91,-25]],[[7219,12367],[-41,65],[-12,79]],[[7166,12511],[6,28],[66,-9]],[[7238,12530],[238,0]],[[8230,12610],[-101,10],[-43,27],[20,96],[27,45],[40,-4],[37,57]],[[8296,12802],[24,-113]],[[7416,12100],[-26,84],[-23,130],[-46,11],[-11,67]],[[7471,12438],[36,-81],[132,92],[54,-22],[74,10]],[[7767,12437],[63,-45],[66,5]],[[8055,12485],[-18,99],[-48,30],[-11,116],[38,15],[26,66],[-17,42]],[[7767,12437],[6,83],[-22,53],[16,69]],[[7871,12620],[41,-83],[85,-79]],[[7910,12801],[14,39]],[[6878,12289],[14,129],[53,37]],[[6945,12455],[34,7]],[[6979,12462],[5,-213],[22,-110],[-9,-35]],[[6997,12104],[-23,9],[-55,163],[-41,13]],[[13466,12164],[-2,48],[-45,62],[1,54]],[[13420,12328],[79,-33],[33,-46],[33,8],[24,-48],[85,-64],[6,-21]],[[13636,15305],[38,-48]],[[7527,12827],[12,-46],[-12,-58],[184,24],[75,23]],[[7476,12530],[85,22],[-40,80],[-39,7],[-23,50],[-90,-9],[-62,8]],[[7238,12530],[3,92],[-81,37]],[[6873,12038],[-92,180],[-29,-8],[-36,44],[-108,21]],[[6608,12275],[68,-2],[95,33],[35,-4]],[[6806,12302],[72,-13]],[[6997,12104],[4,-31]],[[6557,12580],[116,-160],[99,27]],[[6772,12447],[-12,-48],[39,-45],[7,-52]],[[6608,12275],[-133,-1]],[[6772,12447],[93,54],[50,1],[30,-47]],[[7246,12111],[-2,190],[-25,66]],[[6782,12694],[-67,-24],[-150,-22]],[[7166,12511],[-128,-15],[-59,-34]],[[6116,11014],[114,52],[38,-48],[62,51]],[[6475,11532],[-166,-69],[-57,-40],[-35,-50],[-57,-32],[21,-61],[-23,-54],[21,-84],[-79,-76]],[[7161,11235],[-45,66]],[[7116,11301],[131,76]],[[7247,11377],[15,-42]],[[6874,11093],[26,40],[-1,54],[-39,53]],[[6860,11240],[45,-7],[62,34]],[[6967,11267],[57,-37],[50,18],[21,-31]],[[7179,11492],[38,-11],[26,-50],[42,-15]],[[7285,11416],[-38,-39]],[[7116,11301],[-43,-6],[-52,49]],[[6998,10807],[-27,-11],[-45,50],[-18,54]],[[6908,10900],[24,40]],[[6932,10940],[57,43],[58,-61]],[[6862,11009],[70,-69]],[[6908,10900],[-28,-25],[-77,-23]],[[6803,10852],[-27,-11],[-30,76],[35,46]],[[6594,10870],[42,-34],[22,-50],[51,-39]],[[6709,10747],[35,-22]],[[6921,11099],[37,-42],[46,-18],[59,-78]],[[7172,11239],[35,-10]],[[7207,11229],[-33,-24],[7,-89]],[[7101,11033],[-95,98]],[[6880,11355],[48,-50],[27,12]],[[6955,11317],[12,-50]],[[6860,11240],[-25,50]],[[6403,11225],[4,-50],[80,-93]],[[6742,11085],[61,-4],[58,-50]],[[7009,11350],[-54,-33]],[[7268,11588],[28,1],[107,-106]],[[7403,11483],[-71,-28]],[[7332,11455],[-47,-39]],[[7453,11483],[-50,0]],[[7486,11403],[-53,10],[-61,-30],[-40,72]],[[7338,11203],[-72,5],[-59,21]],[[6803,10852],[-94,-105]],[[8548,7870],[-158,-42],[22,-89],[1,-80],[26,-20],[10,-84],[-14,-72],[-61,-5],[-59,-88],[-113,-49],[-2,-83]],[[8200,7258],[-24,-35]],[[8176,7223],[-10,105],[-16,33],[10,66],[-21,39],[75,61],[-19,79],[-44,43],[17,64],[-24,95],[-39,77],[-45,27]],[[7997,7091],[52,36],[46,60],[81,36]],[[8200,7258],[110,20],[176,0],[50,67],[3,84],[30,82],[114,25],[25,67],[119,10],[104,50]],[[8931,7663],[27,-58],[-13,-83],[26,-24],[22,-75],[45,-48]],[[8976,7740],[-17,-72],[-28,-5]],[[8745,7829],[110,157],[45,-7]],[[8900,7979],[32,-38],[60,17],[45,-9],[27,32]],[[9268,7649],[-57,130],[-145,66]],[[8521,7927],[60,25],[25,115],[-31,73]],[[8575,8140],[44,42],[87,24]],[[8706,8206],[45,-112],[51,-48],[95,-16]],[[8897,8030],[3,-51]],[[11812,7218],[116,-93],[120,-123],[246,-15],[54,11],[20,48],[-36,65],[90,144],[30,114],[-86,121],[-63,58],[-19,40],[11,95],[27,23],[95,31],[38,-5],[109,138],[25,16],[160,-35],[4,-85],[90,-60],[4,-46],[62,-75],[75,-56],[34,-89],[49,-64]],[[12761,8487],[52,-6],[-35,-56],[-65,-63],[-128,-107],[-34,-63],[-73,-78],[-43,-25],[-93,-21],[-34,-38],[-55,37]],[[12253,8067],[-35,54],[-119,43],[-57,73],[-50,5],[-89,56],[-59,68]],[[11820,7559],[38,2],[72,-42],[123,217],[19,56],[64,8],[69,65],[1,69],[-24,23],[19,120]],[[12201,8077],[52,-10]],[[12201,8077],[-169,31],[5,-43],[-91,37],[-79,-4],[-51,-92],[-2,-57],[-134,-64],[168,-120],[19,-122],[-47,-84]],[[7987,7084],[-41,-102],[-56,27],[-26,66],[-92,-2],[-50,-22],[-52,-101],[-79,-59],[-29,-55],[-37,5],[-65,-134],[-50,0],[-32,-93],[-30,14],[-59,122],[-56,-45],[-27,-77],[-49,111]],[[7157,6739],[103,199]],[[7712,5203],[-43,89],[-4,49],[98,39],[-66,34],[-25,60],[45,61],[7,83],[-39,41],[-14,55]],[[7671,5714],[33,36],[38,134]],[[7742,5884],[34,13],[62,97],[52,2],[17,91],[-50,54],[139,58],[36,51],[129,-41],[130,64]],[[7742,5884],[-39,126],[-58,77],[-20,101],[-67,35]],[[7558,6223],[25,138],[30,23],[14,66],[82,103],[57,7],[93,-31],[33,-38],[91,23],[76,63]],[[7558,6223],[-90,7],[-31,63],[-5,107],[-33,90],[-45,4],[-58,45],[-72,3],[-64,63],[-21,89],[18,45]],[[13406,12360],[14,-32]],[[13548,14531],[-38,-34]],[[13340,14554],[-24,40]],[[13316,14594],[-86,83]],[[13315,14064],[-48,49],[-70,22],[-39,35],[21,89]],[[13179,14259],[59,3]],[[13238,14262],[61,-9],[27,-88]],[[13326,14165],[-11,-101]],[[13326,14165],[34,-11]],[[13360,14154],[173,-21],[18,-36]],[[13551,14097],[9,-22]],[[13560,14075],[-43,-46],[-81,-43]],[[13436,13986],[-114,13]],[[13322,13999],[-14,7]],[[13308,14006],[7,58]],[[13302,13876],[-4,87],[24,36]],[[13436,13986],[6,-40],[54,-46],[-39,-75]],[[6780,5616],[51,9],[86,53],[68,3],[27,36],[119,-29],[69,-1],[61,18]],[[7261,5705],[11,-54],[-16,-117],[56,-49],[-24,-99],[30,-37],[5,-66]],[[7261,5705],[-31,54],[55,133],[70,5],[147,-101],[60,15],[68,-87],[41,-10]],[[13235,13895],[-47,25],[-29,88]],[[13159,14008],[66,12],[83,-14]],[[7283,8640],[67,-13],[66,31],[10,-36],[88,88],[13,-65],[46,-27]],[[7573,8618],[19,-94],[-16,-56],[46,-9],[-48,-91],[88,-108],[-25,-35],[50,-31],[15,-64],[-16,-81],[62,10],[37,-46],[-39,-166],[46,-40],[57,19]],[[7573,8618],[27,-21],[63,28],[36,44],[106,37],[23,43],[-33,32],[17,42]],[[7812,8823],[46,-19],[-1,-63],[-26,-42],[35,-64],[96,-34]],[[7962,8601],[33,-147],[36,-50],[29,-83],[27,-27],[-3,-67]],[[8084,8227],[-47,-20],[-7,-74],[20,-23],[30,-151]],[[7755,9155],[-36,-74],[-52,-50],[104,-48],[51,-65],[-36,-66],[26,-29]],[[13626,14321],[-75,-224]],[[13360,14154],[38,62],[72,64]],[[8600,9271],[-63,-42],[-53,-71],[-23,-1]],[[8461,9157],[-80,31]],[[8381,9188],[-15,41],[-40,8],[-2,137],[-34,2]],[[8290,9376],[-61,148]],[[8229,9524],[44,82],[-28,36],[33,63],[-1,37]],[[8498,8727],[-33,-107]],[[8465,8620],[-17,-22]],[[8448,8598],[-27,-34],[-113,-3]],[[8684,8997],[-107,-32],[-46,32],[-11,48],[-54,68],[-5,44]],[[8465,8620],[44,-6],[32,-34],[97,12]],[[8638,8592],[-9,-130]],[[8629,8462],[-35,10],[-70,-63],[-17,21],[8,78],[-56,18]],[[8459,8526],[-11,72]],[[8704,8795],[-31,135],[20,46]],[[8255,9080],[74,23],[39,-5]],[[8368,9098],[41,-80]],[[8409,9018],[-64,-18],[-29,27],[-63,8]],[[8253,9035],[2,45]],[[13144,13997],[15,11]],[[8229,9524],[-54,50],[-92,7]],[[8495,8754],[-27,169],[-41,59]],[[8427,8982],[-18,36]],[[8368,9098],[13,90]],[[8348,8753],[-22,25],[-10,149],[96,31],[15,24]],[[7969,9174],[68,30],[22,39],[63,21],[64,49],[32,-37],[37,5]],[[8255,9281],[14,-130],[-25,-28]],[[8244,9123],[-58,-12],[-114,15],[-46,-12],[-19,40],[-46,16]],[[13687,14389],[-51,-35]],[[8084,8227],[79,-92]],[[8244,9123],[11,-43]],[[8253,9035],[-73,-67],[38,-56],[0,-45],[-234,-16],[19,-93],[-53,-36],[56,-70],[-44,-51]],[[8656,8656],[-18,-64]],[[8255,9281],[9,63],[26,32]],[[8798,8706],[42,-31],[27,-57]],[[8867,8618],[-47,-9],[-77,15],[-68,45]],[[8459,8526],[-19,-48],[-4,-79],[23,-39]],[[8459,8360],[-72,-77],[-99,-36],[-69,-57]],[[13860,14373],[-63,-85],[-37,-22]],[[13760,14266],[-26,46],[-50,34],[-58,-10]],[[13760,14266],[43,-129],[84,-22]],[[13887,14115],[-76,-43],[6,-50],[-58,-57]],[[13759,13965],[-33,18],[-4,52],[-41,-4],[-25,51],[-47,12],[-49,-19]],[[10349,9113],[-99,-87],[-82,-116],[-69,-44],[-30,-39]],[[10069,8827],[-51,-16],[-16,79],[-101,96],[-40,85],[0,69],[-33,39],[-13,80],[-59,47],[-67,22],[-19,76]],[[9670,9404],[83,12],[27,39],[6,89],[-45,93],[59,50]],[[9800,9687],[48,-27],[69,32],[84,-96]],[[10001,9596],[46,-44],[2,-78],[55,-58],[94,-9],[61,59]],[[10259,9466],[29,-45]],[[9800,9687],[-33,113],[-52,11],[41,60],[-53,82]],[[9703,9953],[35,-23],[110,96],[49,112],[51,70],[12,97]],[[9960,10305],[74,51],[46,-9]],[[10080,10347],[49,24],[222,-39]],[[10351,10332],[-13,-81],[-39,8],[-73,-48],[-33,-65],[-16,-202],[54,-58],[3,-55]],[[10234,9831],[-38,-48],[-72,-17],[-48,-35],[-1,-65],[-74,-70]],[[9441,10003],[70,-8],[91,135],[135,11],[84,41],[0,60],[20,42]],[[9841,10284],[119,21]],[[9703,9953],[-34,-100],[-42,-27],[-35,24],[-100,-7],[-75,-41]],[[9597,9456],[73,-52]],[[10069,8827],[39,-56]],[[9349,10129],[30,58],[-27,20]],[[9344,10269],[62,34],[-16,30],[165,-6],[24,-56],[43,2],[32,-40],[64,6],[7,40],[52,35]],[[9777,10314],[64,-30]],[[10259,9466],[-5,84],[35,40],[-23,45],[40,19],[9,109],[-57,13],[-24,55]],[[10351,10332],[32,49],[140,18]],[[13172,14611],[-20,-164],[-39,-114],[-28,-30],[13,-46]],[[13098,14257],[-27,-1]],[[14034,14155],[-61,-25],[-86,-15]],[[8706,8206],[4,59]],[[8710,8265],[79,-91],[28,31]],[[8817,8205],[22,-42],[100,-89]],[[8939,8074],[-42,-44]],[[8459,8360],[95,-41]],[[8554,8319],[8,-54],[-47,-23],[-11,-44]],[[8504,8198],[-62,-41],[-36,-52],[-42,-17],[-25,-44],[-47,-19],[26,-65]],[[9598,8947],[-75,-33],[63,-82],[-12,-29]],[[9574,8803],[-51,21],[-75,7],[-50,-62],[-85,-18]],[[9313,8751],[-77,53],[-33,-17],[-83,6]],[[10085,8400],[111,130],[-96,82],[-105,20],[-151,-32],[-73,22],[-71,-32],[-103,-18]],[[9597,8572],[-20,9]],[[9577,8581],[42,28],[19,66],[136,17],[29,49],[43,-2]],[[9313,8751],[-15,-30],[24,-88]],[[9322,8633],[-57,-15]],[[9265,8618],[-13,60],[-79,-22],[-48,10],[-98,77]],[[9027,8743],[-26,42]],[[9176,8337],[67,53],[113,9],[21,17]],[[9377,8416],[41,-3]],[[9418,8413],[21,-61],[48,-52]],[[9487,8300],[-62,10],[-68,-41]],[[9357,8269],[-46,7]],[[9311,8276],[-56,-3],[-46,56],[-33,8]],[[9027,8743],[-35,-30],[-44,22],[-12,-80],[36,-136],[-52,-31]],[[8920,8488],[-4,7]],[[8916,8495],[-49,123]],[[9097,8297],[54,12],[25,28]],[[9311,8276],[-38,-53],[-59,-36]],[[13316,14594],[-57,5],[33,-71],[-3,-236]],[[13274,14265],[-26,-1]],[[13248,14264],[-32,114],[11,123],[-24,101],[-24,13]],[[9322,8633],[74,-6]],[[9396,8627],[53,-131]],[[9449,8496],[-31,-83]],[[9377,8416],[-19,35],[-76,26]],[[9282,8477],[-77,96],[60,45]],[[10012,8320],[-62,55],[-55,72],[-29,0],[-127,67],[-17,-22],[-90,35],[-31,-12]],[[9601,8515],[-4,57]],[[9396,8627],[57,23],[48,-18]],[[9501,8632],[76,-51]],[[9577,8581],[-53,-9],[-10,-33],[-65,-43]],[[9357,8269],[8,-32],[-21,-91],[28,-99]],[[9501,8632],[73,171]],[[9574,8803],[37,17],[14,44],[72,-21],[38,30]],[[8504,8198],[71,-58]],[[9487,8300],[15,69],[29,30],[10,87],[60,29]],[[9282,8477],[-34,-31],[-51,10],[-222,-37],[-55,69]],[[13098,14257],[81,2]],[[8710,8265],[-40,-5],[-29,59],[-70,26]],[[8571,8345],[40,15],[20,67]],[[8631,8427],[70,-29],[105,46],[43,-21]],[[8849,8423],[-32,-218]],[[9058,7991],[-84,40],[-35,43]],[[8849,8423],[20,42],[47,30]],[[8554,8319],[17,26]],[[8631,8427],[-2,35]],[[13238,14262],[10,2]],[[9628,11058],[47,0],[25,-105],[98,-86]],[[9798,10867],[-60,-37],[-55,-63],[-69,32],[-24,-12]],[[9590,10787],[-72,13],[-9,51],[-35,45]],[[9952,10854],[-89,-2],[-65,15]],[[10172,10804],[23,123],[-24,96],[42,66]],[[10213,11089],[176,-26],[18,18]],[[10213,11089],[-182,27]],[[9229,10458],[48,31],[9,34],[114,-1],[61,60],[12,106],[56,39],[23,-81],[28,14]],[[9580,10660],[-19,-60],[11,-36],[-22,-68],[75,25]],[[9625,10521],[43,-136],[63,-14],[46,-57]],[[9590,10787],[-17,-56],[36,-15],[-29,-56]],[[10080,10347],[-36,164],[-24,35],[-48,-5],[-20,38],[-71,-12],[-43,-38],[-86,-45],[-127,37]],[[13759,13965],[5,-86]],[[8805,12559],[90,20],[33,-52],[-14,-52],[82,-3],[32,16]],[[9028,12488],[12,-46]],[[9040,12442],[-27,-28],[48,-65],[-37,-39],[0,-54]],[[9024,12256],[-48,-10]],[[9519,12190],[-49,-68]],[[9470,12122],[-80,53]],[[9390,12175],[49,60]],[[9439,12235],[68,-35]],[[9389,11683],[17,80],[117,69]],[[9523,11832],[49,-87],[-49,-77]],[[9523,11668],[-49,-35]],[[9474,11633],[-45,26]],[[8927,12626],[6,-40],[77,9],[18,-107]],[[9546,12047],[-65,-3]],[[9481,12044],[-11,78]],[[9481,12044],[-72,-79],[-15,-36]],[[9394,11929],[-56,4]],[[9338,11933],[-59,144]],[[9279,12077],[27,15]],[[9306,12092],[84,83]],[[9287,11748],[57,27],[-5,50],[74,16],[-19,88]],[[9594,11916],[-12,-63],[-59,-21]],[[9320,11674],[-37,42]],[[9155,12054],[124,23]],[[9338,11933],[-45,-33]],[[9508,12298],[-57,98],[-40,-21],[-37,29],[-44,84],[-63,74],[-48,-36],[-36,12]],[[9183,12538],[-33,60],[2,42]],[[9474,12246],[-79,65]],[[9395,12311],[-84,50],[-66,-18],[-51,72],[-53,38]],[[9141,12453],[-3,38],[45,47]],[[9593,11270],[-36,73],[8,82],[-39,25],[-34,66],[-57,51],[39,66]],[[9523,11668],[54,-54],[58,70],[61,31]],[[9141,12453],[-55,-20],[-46,9]],[[9439,12235],[-112,27]],[[9327,12262],[14,46],[54,3]],[[9306,12092],[-34,71]],[[9272,12163],[23,14],[-1,94]],[[9294,12271],[33,-9]],[[9024,12256],[69,8],[79,30],[122,-23]],[[9272,12163],[-83,-10],[-22,36],[-60,-38]],[[15913,3660],[-3,44],[-90,19]],[[15784,15672],[50,-3],[57,-30],[93,24],[79,45],[71,14],[87,63],[83,18],[24,45],[82,39],[74,90],[8,29],[-66,76],[-108,38],[-101,75],[-62,25],[-20,32],[23,51],[83,30],[77,-20],[137,-79],[38,8],[-32,60],[-53,8],[-36,61],[64,2],[48,-52],[164,-81],[179,-110],[132,-48],[53,-39],[92,-187],[36,-32],[12,-59],[71,-18],[38,29],[52,-23]],[[17445,15671],[-20,90],[58,-2],[96,-32],[56,26],[-39,83],[8,53],[-55,75],[-75,47],[-44,59],[9,61],[-40,177],[-78,44],[-58,75],[-66,-16],[-111,103],[-150,73],[-102,77],[-107,120],[-13,31],[-82,54],[35,108],[91,-25],[36,6],[40,-57],[138,-102],[60,-21],[98,-80],[46,-79],[34,-29],[79,-13],[152,-83],[61,-57],[83,-25],[94,-62],[6,-134],[60,-98],[149,-139],[58,-23],[69,-63],[116,-75],[75,-15],[135,-91],[40,-5],[70,40],[89,2],[28,22],[89,0],[74,-19],[72,-72],[104,-23],[51,-47],[44,22],[47,-20],[31,26],[87,-45],[77,-101],[7,-94],[52,-56],[8,-94],[119,-120],[46,-30],[73,-10],[116,29],[45,-26],[25,-91],[113,-57],[59,4],[24,-42],[59,-41],[129,8],[32,-46],[60,-18],[94,9],[58,-55],[38,-82],[44,-24],[66,-80],[4,-85],[-55,-48],[-185,34],[-42,-14],[-45,-54],[-27,-59],[-131,35],[-63,-59],[-51,36],[-10,34],[40,37],[-28,28],[-99,-100],[-34,3],[-43,-48],[-112,-8],[-82,57],[-20,60],[-40,-25]],[[16701,15970],[200,-78],[54,27],[-58,39],[-46,55],[-60,43],[-37,-10],[-33,43],[-43,-52],[23,-67]],[[16007,1546],[-29,-13],[-36,-72],[-37,21],[14,102],[-44,37],[-19,76],[32,101],[-16,74],[-81,3],[-10,35],[-54,15],[-25,42]],[[16582,2370],[34,31],[99,37],[60,-12],[188,11],[125,93],[49,-70],[-180,-154],[-58,-3],[-162,37],[-58,-9],[-87,-83]],[[15139,3093],[-101,-14],[-15,-70],[-79,-93],[58,-123],[-10,-61],[24,-66],[-7,-103],[75,-55],[27,-75],[0,-62],[-25,-63],[-71,-89],[-55,-8],[-62,-61],[-9,-70],[-78,-87],[-180,-132],[-11,-89],[-59,-19],[-88,-101],[-87,-46],[-32,-56],[38,-56],[-90,-7],[-32,-63],[-10,-64],[36,-56],[-17,-59],[39,-60],[36,-119],[47,-12],[34,-208],[-102,-78],[1,-91],[-58,-61],[-53,-10],[-56,25],[-36,-12],[-34,-121],[-3,-54],[40,-67],[52,11],[-4,-68],[29,-47],[45,-29],[31,-168],[25,-57],[-92,-19],[-29,119],[-72,70],[5,59],[-30,64],[-82,82],[-45,212],[34,72],[10,61],[45,-9],[62,61],[-2,77],[46,74],[-34,201],[-33,82],[-37,45],[-141,79],[-48,12],[-74,117],[-8,66],[-68,75],[-58,41],[30,52],[57,28],[13,30],[207,-93],[51,-75],[65,-49],[34,95],[48,70],[77,31],[157,134],[50,119],[90,134],[37,30],[59,127],[65,97],[138,46],[62,57],[-31,134],[9,50],[-36,62],[-5,60],[-42,140],[1,48],[30,78]],[[4592,5437],[-77,-5],[-208,89],[-89,7],[-99,28],[-218,-7],[-53,-21],[-170,13],[-96,-25],[-163,-9],[-236,-61],[-166,77],[-121,-53],[-52,-61],[1,-69],[-30,-52],[-237,-122],[-109,-8],[-123,-157],[-147,56],[-58,94],[-15,71],[-30,20],[-72,-3],[-22,17],[-124,-44],[-144,-134],[1,-36],[-108,-165],[7,-41],[-47,-105],[-44,-20]]],"transform":{"scale":[0.0001849578485405569,0.00012330553196854394],"translate":[5.956800664952974,45.72493187952228]},"objects":{"country":{"type":"GeometryCollection","geometries":[{"arcs":[[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269]],"type":"Polygon","id":"CH"}]},"cantons":{"type":"GeometryCollection","geometries":[{"arcs":[[246,247,248,249,250,251,252,253,270,271,272,273,274,275,276,277,278,279,255,256,257,258,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371]],"type":"Polygon","id":1},{"arcs":[[[372,373,374]],[[375,376,377,378]],[[379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,500,501,502,503,504,505,506,507,508,509,510,511,512,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,543,544,545,546,547,548,549,550,551,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567,568,569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,600],[605,606,607,608],[609,610,611]],[[601,602,603,604]]],"type":"MultiPolygon","id":2},{"arcs":[[-457,612,613,614,615,616,617,618,619,620,621,622,623,624,625,626,627,628,629,630,631,632,633,634,635,636,637,638,639,640,641,642,643,644,645,646,647,648,649,650,651,652,653,654,655,656,657,658,659,660,661,662,663,664,665,666,667,668,669,670,671,672,673,674,675,676,677,-479,-478,-477,-476,-475,-474,-473,-472,-471,-470,-469,-468,-467,-466,-465,-464,-463,-462,-461,-460,-459,-458]],"type":"Polygon","id":3},{"arcs":[[-490,678,679,680,681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,696,697,698,699,700,701,702,703,704,-491]],"type":"Polygon","id":4},{"arcs":[[-323,705,706,707,708,709,710,711,712,713,714,715,716,-693,-692,-691,-690,-689,-688,-687,717,-665,-664,-663,-662,-661,-660,-659,-658,-657,-656,-655,-654,718,719,720,721,722,723,724,725,726,727,-326,-325,-324]],"type":"Polygon","id":5},{"arcs":[[[-489,728,-681,-680,-679]],[[-480,-678,-677,-676,-675,-674,-673,729,730,731,732,733,734,735,-487,-486,-485,-484,-483,-482,-481]]],"type":"MultiPolygon","id":6},{"arcs":[[-488,-736,-735,-734,-733,-732,-731,-730,-672,-671,-670,-669,-668,-667,-666,-718,-686,-685,-684,-683,-682,-729]],"type":"Polygon","id":7},{"arcs":[[-694,-717,-716,-715,-714,-713,-712,736,737,738,739,740,741,742,743,744,745,746,747,748,749,750,751,-695]],"type":"Polygon","id":8},{"arcs":[[-327,-728,-727,-726,-725,-724,-723,-722,-721,-720,-719,-653,-652,-651,-650,752,753,754,755,756,-339,-338,-337,-336,-335,-334,-333,-332,-331,-330,-329,-328]],"type":"Polygon","id":9},{"arcs":[[[757,758,759,760]],[[761,762,763,764,765,766,767,768]],[[769,770,771,772,773,774,775,776,777,778,779,780,781,782,783,784,785,786,787]],[[-606,-609,-608,-607]],[[-373,788,789,790,791,792,-549,-548,-547,-546,-545,-544,-543,-542,-541,-540,-539,-538,-537,-536,-535,-534,-533,-532,-531,-530,-529,-528,-527,-526,-525,-524,-523,-522,-521,-520,-519,-518,-517,-516,793,794,795,796,797,798,799,800,801,802,803,804,805,806,807,808,809,810,811,812,813,814,815,816,817,818,819,820,821,822,823,824,825,826,827,828,829,830,831,832,833,834,835,836,837,838,839,840,841,842,843,844,845,846,847,848,849,850,851,852,853,854,855,856,857,858,859,860,861,862,863,864,865,-374],[-376,-379,-378,-377]]],"type":"MultiPolygon","id":10},{"arcs":[[[-610,-612,-611]],[[203,866,867,868,869]],[[-380,-605,-604,870,871,872,873,874,875,876,877,878,879,880,881,882,883,884,885,886,887,888,889,890,891,892,893,894,895,896,897,898,899,900,901,902,903,904,905,906,907,908,909,910,911,912,913,914,915,916,917,918,919,920,921,922,923,924,925,926,927,928,929,930,931,932,933,934,935,936,937,938,939,940,941,942,943,944,945,946,947,948,949,950,-454,-453,-452,-451,-450,-449,-448,-447,-446,-445,-444,-443,-442,-441,-440,-439,-438,-437,-436,-435,-434,-433,-432,-431,-430,-429,-428,-427,-426,-425,-424,-423,-422,-421,-420,-419,-418,-417,-416,-415,-414,-413,-412,-411,-410,-409,-408,-407,-406,-405,-404,-403,-402,-401,-400,-399,-398,-397,-396,-395,-394,-393,-392,-391,-390,-389,-388,-387,-386,-385,-384,-383,-382,-381]],[[205,206,207,208,209,951,952,953,954,955,956,957,958,959,960]]],"type":"MultiPolygon","id":11},{"arcs":[[215,216,217,218,961,962,963,964,965,966,967,968]],"type":"Polygon","id":12},{"arcs":[[[202,-870,969,970,971,972]],[[204,-961,-960,-959,-958,-957,-956,-955,-954,-953,-952,210,211,212,213,214,-969,-968,-967,-966,-965,-964,-963,-962,219,220,221,222,973,974,975,976,977,978,979,980,981,982,983,984,985,986,987,988,989,990,991,992,993,-927,-926,-925,-924,-923,-922,-921,-920,-919,-918,-917,-916,-915,-914,-913,-912,-911,-910,-909,-908,-907,-906,-905,-904,-903,-902,-901,-900,-899,-898,-897,-896,-895,-894,-893,-892,-891,-890,-889,-888,-887,-886,-885,-884,-883,-882,-881,-880,-879,994,995,-869,-868,-867]]],"type":"MultiPolygon","id":13},{"arcs":[[[254,-280,-279,-278,-277,-276,-275,-274,-273,-272,-271]],[[5,6,7,8,9,10,996,997,998,999,1000,1001]],[[0,1,2,3,1002,1003,1004,-284,-283,-282,-281,259,260,261,262,263,264,265,266,267,268,269]]],"type":"MultiPolygon","id":14},{"arcs":[[1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1019,1020,1021,1022,1023,1024,1025,1026,1027,1028,1029,1030,1031,1032,1033,1034,1035,1036,1037,1038,1039,1040,1041,1042,1043,1044,1045,1046,1047,1048,1049,1050,1051,1052,1053,1054,1055,1056,1057,1058,1059]],"type":"Polygon","id":15},{"arcs":[[[-1020,1060,1061,1062,1063,1064,1065,-1028,-1027,-1026,-1025,-1024,-1023,-1022,-1021]],[[-1013,1066,-1016,-1015,-1014]],[[-1006,1067,1068,-1009,-1008,-1007]]],"type":"MultiPolygon","id":16},{"arcs":[[15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,1069,1070,1071,1072,1073,1074,1075,1076,1077,1078,1079,1080,1081,-745,-744,-743,-742,-741,-740,-739,-738,-737,-711,-710,-709,-708,-707,-706,-322,-321,-320,-319,-318,-317,-316,-315,1082,1083,1084,1085,1086,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1099,1100,1101,1102,1103,1104,1105,1106,1107,1108,1109,1110,1111,1112,1113,1114,1115,1116,1117,1118],[-1010,-1069,-1068,-1060,-1059,-1058,-1057,-1056,-1055,-1054,-1053,-1052,-1051,-1050,-1049,-1048,-1047,-1046,-1045,-1044,-1043,-1042,-1041,-1040,-1039,-1038,-1037,-1036,-1035,-1034,-1033,-1032,-1031,-1030,-1029,-1066,-1065,-1064,-1063,-1062,-1061,-1019,-1018,-1017,-1067,-1012,-1011],[1119,1120,1121,1122]],"type":"Polygon","id":17},{"arcs":[[30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1135,1136,1137,1138,1139,1140,1141,-699,-698,-697,-696,-752,-751,-750,-749,-748,-747,-746,-1082,-1081,-1080,-1079,-1078,-1077,-1076,-1075,-1074,-1073,-1072,-1071,-1070]],"type":"Polygon","id":18},{"arcs":[[223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,-372,-371,-370,-369,-368,-367,-366,-365,-364,-363,-362,-361,-360,-359,-358,-357,-356,-355,-354,-353,-352,-351,-350,-349,-348,-347,-346,-345,-344,-343,-342,-341,-340,-757,-756,-755,-754,-753,-649,-648,-647,-646,-645,-644,-643,-642,-641,-640,-639,-638,-637,-636,-635,-634,-633,-632,-631,-630,-629,-628,-627,-626,-625,-624,-623,-622,-621,-620,-619,-618,-617,-616,-615,-614,-613,-456,-455,-951,-950,-949,-948,-947,-946,-945,-944,-943,-942,-941,-940,-939,-938,-937,-936,-935,-934,-933,-932,-931,-930,-929,-928,-994,-993,-992,-991,-990,-989,-988,-987,-986,-985,-984,-983,-982,-981,-980,-979,-978,-977,-976,-975,-974]],"type":"Polygon","id":19},{"arcs":[[[-1120,-1123,-1122,-1121]],[[4,-1002,-1001,-1000,-999,-998,-997,11,12,13,14,-1119,-1118,-1117,-1116,-1115,-1114,-1113,-1112,-1111,-1110,-1109,-1108,-1107,-1106,-1105,-1104,-1103,-1102,-1101,-1100,-1099,-1098,-1097,-1096,-1095,-1094,-1093,-1092,-1091,-1090,-1089,-1088,-1087,-1086,-1085,-1084,-1083,-314,-313,-312,-311,-310,-309,-308,-307,-306,-305,-304,-303,-302,-301,-300,-299,-298,-297,-296,-295,-294,-293,-292,-291,-290,-289,-288,-287,-286,1142,1143,-1003]]],"type":"MultiPolygon","id":20},{"arcs":[[73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,1144,-703,-702,-701,-700,-1142,-1141,-1140,-1139,-1138,-1137,-1136,-1135,-1134,-1133,-1132,-1131,-1130,-1129,-1128,-1127,-1126,-1125,-1124],[1145,1146,1147,1148]],"type":"Polygon","id":21},{"arcs":[[[132,1149,1150,1151,1152,1153,1154,134,1155,1156,1157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,1158,1159,1160,1161,1162,1163,1164,1165,1166,1167,-788,-787,-786,-785,-784,-783,-782,-781,-780,-779,-778,-777,-776,-775,-774,-773,-772,-771,1168,-857,-856,-855,-854,-853,-852,-851,-850,-849,-848,-847,-846,-845,-844,-843,-842,-841,-840,-839,-838,-837,-836,-835,-834,-833,-832,-831,-830,-829,-828,-827,-826,-825,-824,-823,-822,-821,-820,-819,-818,-817,-816,-815,-814,-813,-812,-811,-810,-809,-808,-807,-806,-805,-804,-803,-802,-801,-800,-799,-798,-797,-796,-795,-794,-515,-514,-513,-512,1169,1170,1171,1172,1173,1174,1175,1176,1177,1178,1179,1180,1181,1182,1183,1184,1185,1186,1187,1188,1189],[-758,-761,-760,-759],[-762,-769,-768,-767,-766,-765,-764,-763],[1191,1192,1193]],[[-375,-866,-865,-864,-863,-862,-861,-860,-859,1190,-550,-793,-792,-791,-790,-789]]],"type":"MultiPolygon","id":22},{"arcs":[[107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,-1190,-1189,-1188,-1187,-1186,-1185,-1184,-1183,-1182,-1181,-1180,-1179,-1178,-1177,-1176,-1175,-1174,-1173,-1172,-1171,-1170,-511,-510,-509,-508,-507,-506,-505,-504,-503,-502,-501,-500,-499,-498,-497,-496,-495,-494,-493,-492,-705,-704,-1145]],"type":"Polygon","id":23},{"arcs":[[175,176,177,178,179,180,181,182,1194,-568,-567,-566,-565,-564,-563,-562,-561,-560,-559,-558,-557,-556,-555,-554,-553,-552,-551,-1191,-858,-1169,-770,-1168,-1167,-1166,-1165,-1164,-1163,-1162,-1161,-1160,-1159]],"type":"Polygon","id":24},{"arcs":[[[135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,-1158,-1157,-1156]],[[-1192,-1194,-1193]],[[133,-1155,-1154,-1153,-1152,-1151,-1150]]],"type":"MultiPolygon","id":25},{"arcs":[[183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,-973,-972,-971,-970,-996,-995,-878,-877,-876,-875,-874,-873,-872,-871,-603,-602,-601,-600,-599,-598,-597,-596,-595,-594,-593,-592,-591,-590,-589,-588,-587,-586,-585,-584,-583,-582,-581,-580,-579,-578,-577,-576,-575,-574,-573,-572,-571,-570,-569,-1195]],"type":"Polygon","id":26}]}}};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.OfflineMapNode.TOPOJSON_MAPS = {
    "world": {
        features: Machinata.Reporting.Node.OfflineMapNode.TOPOJSON_110M_FEATURES,
        properties: Machinata.Reporting.Node.OfflineMapNode.TOPOJSON_110M_PROPERTIES,
        featuresFeature: "countries",
        featuresIdKey: "id",
        factToPropertiesKey: "currency",
        factFallbackCoordinates: [-27, -137.0],
        propertiesIdKey: "iso_n3",
        projectionWidth: 960,
        projectionHeight: 500,
        projectionZeroScale: 120,
        projectionType: "mercator",
        projectionScale: "auto",
        projectionZoom: 1.0,
        projectionCenter: [0, 45],
    },
    "switzerland": {
        features: Machinata.Reporting.Node.OfflineMapNode.TOPOJSON_SWISSTOPO_FEATURES,
        properties: Machinata.Reporting.Node.OfflineMapNode.TOPOJSON_SWISSTOPO_PROPERTIES,
        featuresFeature: "cantons",
        featuresIdKey: "id",
        factToPropertiesKey: "canton",
        factFallbackCoordinates: [45.85, 9.66],
        propertiesIdKey: "id",
        projectionWidth: 960,
        projectionHeight: 500,
        projectionZeroScale: 8000,
        projectionType: "mercator",
        projectionScale: "auto",
        projectionZoom: 1.0,
        projectionCenter: [8.062474, 46.842931],
    }
};