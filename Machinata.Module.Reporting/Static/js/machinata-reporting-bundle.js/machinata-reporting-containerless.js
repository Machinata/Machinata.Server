/// <summary>
/// Collection of tools specifically designed for operating the Reporting module
/// in a Containerless mode, where individual nodes are rendered very specifically to parts of
/// a page without the context of a report, thus "containerless".
///
/// The containerless APIs are designed to render a node in the most fast and efficient way possible.
/// This means that a single configuration for all the containerless nodes can be generated once,
/// and then the all logically grouped containerless nodes can be built and rendered.
///
/// Containerless nodes always fill the space given by their parent DOM element (their container).
/// 
/// Note: The Containerless API does not use the reporting data layer.
/// 
/// ## Containerless Example
/// ```
/// // Our node to test
/// var nodeJSON = {
///     "nodeType": "GaugeNode",
///     "theme": "magenta",
///     "width": 800,
///     "height": 600,
///     "theme": "magenta",
///     "status": {
///         "value": 1.6,
///         "title": { "resolved": "Moderate" },
///         "tooltip": { "resolved": "1.6 / 5" }
///     },
///     "segments": [
///         {
///             "active": true,
///             "title": { "resolved": "Low" }
///         },
///         {
///             "active": true,
///             "title": { "resolved": "Moderate" }
///         },
///         {
///             "active": false,
///             "title": { "resolved": "Medium" }
///         },
///         {
///             "active": false,
///             "title": { "resolved": "Enhanced" }
///         },
///         {
///             "active": false,
///             "title": { "resolved": "High" }
///         }
///     ]
/// };
/// 
/// // Create a configuration to your needs
/// // Note: you should only create a single configuration for all logically grouped containerless nodes
/// var reportingConfig = {
///     "brandingConfiguration": "acme", // Set the correct branding id so that the full brand configuration will be loaded
///     "iconsBundle": "/static/bundle/svg/csam-reporting-icons-bundle.svg", // Must be relative to the page (best to be absolute path, like most static resources)
///     "dataProvider": "Memory",
///     "catalogSource": null, // Containerless doesn't use a data source, each node is provided separately...
///     "dataSource": null, // Containerless doesn't use a data source, each node is provided separately...
///     "language": "de"
/// };
/// reportingConfig = Machinata.Reporting.Containerless.getContainerlessConfigForProfile("web", reportingConfig);
/// 
/// // Containerless nodes can be injected anywhere, this is up to the integrator.
/// var containerElem = $("#my-node-container");
/// 
/// // Call the Containerless buildNode helper function to initialize the node and all its UI
/// // Note: this will not actually redraw the node, you have full control over when you want to
/// // draw the nodes
/// var nodeElem = Machinata.Reporting.Containerless.buildNode(reportingConfig, nodeJSON, containerElem);
/// 
/// // Initial draw
/// // Whenever you want the node to redraw itself, you can call this method:
/// Machinata.Reporting.Containerless.redrawNode(nodeElem);
/// 
/// // Responsiveness: bind redraw on the window resize dummy method
/// // Do this however your ReactJS resizing works...
/// $(window).resize(function () {
///     Machinata.Reporting.Containerless.redrawNode(nodeElem);
/// });
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Containerless = {};


/// <summary>
/// If ```true``` is passed, verbose debug output is enabled.
/// </summary>
Machinata.Reporting.Containerless.enableVerboseOutput = function (enabled) {
    Machinata.Reporting.setDebugEnabled(enabled);
};


/// <summary>
/// Returns a merged (and loaded) configuration according to the specified ```profile```, with the 
/// config setting ```Containerless``` set to ```true```.
/// Any custom configuration must be bassed at this point to ensure that it loaded and applied properly.
/// </summary>
Machinata.Reporting.Containerless.getContainerlessConfigForProfile = function (profile, config) {
    // Init config, with containerless=true
    if (profile == null) profile = "web";
    if (config == null) {
        config = {};
    }
    config["containerless"] = true;

    // Create merged default version
    Machinata.Reporting.debug("Machinata.Reporting.Containerless.getContainerlessConfigForProfile: "+profile);
    config = Machinata.Reporting.mergeAndLoadConfigWithDefaults(config, profile); // here we merge in all the configs, including the profile
    return config;
};

/// <summary>
/// Builds a node using the given configuration and node JSON.
/// </summary>
/// <returns>nodeElem: a jQuery selector to the build node</returns>
Machinata.Reporting.Containerless.buildNode = function (config, nodeJSON, containerElem) {
    // Sanity
    if (config == null) throw new "Machinata.Reporting.Containerless.buildNode: config is null, it must be initialized via Machinata.Reporting.Containerless.getContainerlessConfigForProfile().";
    if (nodeJSON == null) throw new "Machinata.Reporting.Containerless.buildNode: nodeJSON is null, it be a valid drawable node.";
    if (containerElem == null) throw new "Machinata.Reporting.Containerless.buildNode: containerElem is null, it be a valid jquery selector.";
    if (containerElem.length == 0) throw new "Machinata.Reporting.Containerless.buildNode: containerElem is an empty selector.";
    if (containerElem.parent().length == 0) throw new "Machinata.Reporting.Containerless.buildNode: containerElem appears to not be in the DOM (it has no parent node).";

    // Create clones
    //nodeJSON = Machinata.Util.cloneJSON(nodeJSON); // no longer cloned for performance

    // Register the reporting css namespace to container
    containerElem.addClass("machinata-reporting-styling");

    // Create an instance to work with
    var instance = Machinata.Reporting._createEmptyInstance();
    instance.config = config;

    // Build the node and contents
    var nodeElem = Machinata.Reporting.buildContents(instance, config, nodeJSON, null, containerElem);
    nodeElem.addClass("container-fullsize");
    nodeElem.data("reporting-instance",instance);

    // Return the built node
    return nodeElem;
};

/// <summary>
/// Tells a node it should redraw itself. 
/// This is especially useful if the size of the container has changed.
/// </summary>
Machinata.Reporting.Containerless.redrawNode = function (nodeElem) {
    nodeElem.trigger(Machinata.Reporting.Events.REDRAW_NODE_SIGNAL);
};

/// <summary>
/// </summary>
Machinata.Reporting.Containerless.cleanupNode = function (nodeElem) {
    nodeElem.trigger(Machinata.Reporting.Events.CLEANUP_NODE_SIGNAL);
    nodeElem.trigger(Machinata.Reporting.Events.REMOVE_NODE_SIGNAL);
};