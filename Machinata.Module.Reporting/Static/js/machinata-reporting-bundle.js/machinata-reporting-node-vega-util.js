﻿/// <summary>
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.VegaNode.Util = {};


/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.createLegendForGroupedSeries = function (instance, config, json, seriesGroups, opts) {
    var legendMap = {};
    var legendData = [];
    for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
        var seriesGroup = json.seriesGroups[sgi];
        for (var si = 0; si < seriesGroup.series.length; si++) {
            var serie = seriesGroup.series[si];
            var serieKey = seriesGroup.id + "_" + serie.id;
            var serieTitle = Machinata.Reporting.Text.resolve(instance, serie.title);
            var serieSymbol = "square"; // fallback
            if (serie.chartType == "line") serieSymbol = "line";
            if (serie.chartType == "area") serieSymbol = "square";
            var legendTitle = "";
            if (json.legendLabelsShowSerie == true) legendTitle += (legendTitle == "" ? "" : ": ") + serieTitle;
            var legendDatum = {
                key: serieKey,
                title: serieTitle,
                symbol: serieSymbol,
                titleResolved: legendTitle
            };
            legendMap[serieKey] = legendDatum;
            if (opts != null && opts.reverseOrder == true) {
                legendData.splice(0, 0, legendDatum); // insert
            } else {
                legendData.push(legendDatum); // push
            }
        }
    } 
    json.legendData = legendData;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.createLegendForSeries = function (instance, config, json, series, opts) {
    var legendData = [];
    for (var si = 0; si < series.length; si++) {
        var serie = series[si];
        var serieKey = serie.id; 
        var serieTitle = Machinata.Reporting.Text.resolve(instance, serie.title);
        var legendTitle = "";
        if (json.legendLabelsShowSerie == true) legendTitle += (legendTitle == "" ? "" : ": ") + serieTitle;
        if (json.legendLabelsShowCategory == true) legendTitle += (legendTitle == "" ? "" : ": ") + serieTitle;
        if (json.legendLabelsShowValue == true) {
            var factValResolved = serie.facts[0].resolved;
            legendTitle += (legendTitle == "" ? "" : ": ") + factValResolved;
        }
        var legendDatum = {
            key: serieKey,
            title: serieTitle,
            titleResolved: legendTitle
        };
        legendData.push(legendDatum);
    }
    json.legendData = legendData;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.createLegendForCategoryFacts = function (instance, config, json, facts, opts) {
    var legendData = [];
    for (var i = 0; i < facts.length; i++) {
        var fact = facts[i];
        var factCategoryResolved = Machinata.Reporting.Text.resolve(instance, fact.category);
        var factValResolved = fact.resolved;
        var factKey = factCategoryResolved; //TODO
        var legendTitle = "";
        if (json.legendLabelsShowCategory == true) legendTitle += (legendTitle == "" ? "" : ": ") + factCategoryResolved;
        if (json.legendLabelsShowValue == true) legendTitle += (legendTitle == "" ? "" : ": ") + factValResolved;
        var legendDatum = {
            key: factKey,
            title: factCategoryResolved,
            titleResolved: legendTitle
        };
        legendData.push(legendDatum);
    }
    json.legendData = legendData;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.createLegendForSeriesFacts = function (instance, config, json, series, opts) {
    var legendMap = {};
    var legendData = [];
    for (var si = 0; si < series.length; si++) {
        var serie = series[si];
        for (var fi = 0; fi < serie.facts.length; fi++) {
            var fact = serie.facts[fi];
            var factCategoryResolved = Machinata.Reporting.Text.resolve(instance, fact.category);
            var factValResolved = fact.resolved;
            var factKey = factCategoryResolved; //TODO
            var legendTitle = "";
            if (json.legendLabelsShowCategory == true) legendTitle += (legendTitle == "" ? "" : ": ") +  factCategoryResolved;
            if (json.legendLabelsShowValue == true) legendTitle += (legendTitle == "" ? "" : ": ") +  factValResolved;
            if (legendMap[factKey] == null) {
                var legendDatum = {
                    key: factKey,
                    title: factCategoryResolved,
                    titleResolved: legendTitle
                };
                if (fact.symbolType != null) legendDatum.symbol = fact.symbolType;
                legendMap[factKey] = legendDatum;
                legendData.push(legendDatum);
            }
        }
    }
    json.legendData = legendData;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.createLegendForLegendData = function (instance, config, json, legendData, opts) {
    json.legendData = legendData;
};



/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.getNiceDomain = function (domain) {
    return d3.scaleLinear().domain([domain[0], domain[1]]).nice().domain();
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.getNiceStep = function (step) {
    return d3.scaleLinear().domain([0, step]).nice().domain()[1];
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.autoSnapSeriesAxis = function (instance, config, json, series, opts) {
    // Init
    Machinata.Reporting.debug("Machinata.Reporting.Node.VegaNode.Util.autoSnapSeriesAxis:");
    Machinata.Reporting.debug("  " + "node id: " + json.id);
    if (opts == null) opts = {};
    if (opts.axisKey == null) throw "The axisKey must be set (usually xAxis or yAxis) for Machinata.Reporting.Node.VegaNode.Util.autoSnapSeriesAxis!";
    if (opts.stackedSeries == null) opts.stackedSeries = false; // Note: when we say we want to stack series, we want to stack each serie ontop of each other whithin a category
    if (opts.tickCount == null) opts.tickCount = config.axisTargetTickCount; // Fallback

    // Get min/max
    var minVal = null; // Facts min/max values
    var maxVal = null; // Facts min/max values
    var serieMinTotalVal = null; // Series total min/max values
    var serieMaxTotalVal = null; // Series total min/max values
    var categoryMinTotalVal = null; // Categories total min/max values
    var categoryMaxTotalVal = null; // Categories total min/max values
    var categories = {};
    for (var si = 0; si < series.length; si++) {
        var serie = series[si];
        serie.totalValue = null; // tracks the total value of a serie
        if (serie[opts.axisKey] == null) serie[opts.axisKey] = {};
        for (var fi = 0; fi < serie.facts.length; fi++) {
            var fact = serie.facts[fi];
            // Set axis format?
            if (fact.format != null && serie[opts.axisKey].format == null) {
                Machinata.Reporting.debug("  " + "serie axis format was null, setting based on first fact format (node " + json.id + ")");
                console.warn("Machinata.Reporting.Node.VegaNode.Util.autoSnapSeriesAxis:" + " " +"serie axis format was null, setting based on first fact format (node "+json.id+")");
                serie[opts.axisKey].format = fact.format;
            }
            if (fact.formatType != null && serie[opts.axisKey].formatType == null) {
                Machinata.Reporting.debug(" " + "serie axis formatType was null, setting based on first fact format (node " + json.id + ")");
                console.warn("Machinata.Reporting.Node.VegaNode.Util.autoSnapSeriesAxis:" + " " + "serie axis formatType was null, setting based on first fact format (node " + json.id + ")");
                serie[opts.axisKey].formatType = fact.formatType;
            }
            if (fact.val != null) {
                // Validate 
                if (opts.stackedSeries == true && fact.val < 0) throw "Machinata.Reporting.Node.VegaNode.Util.autoSnapSeriesAxis: a stacked series cannot have negative values."
                // Track fact min/max
                if (fact.val < minVal || minVal == null) minVal = fact.val;
                if (fact.val > maxVal || maxVal == null) maxVal = fact.val;
                // Track series total
                if (serie.totalValue == null) serie.totalValue = 0;
                // Track categories total
                var category = categories[fact.category.resolved];
                if (category == null) {
                    category = { "key": fact.category.resolved,"totalValue": 0};
                    categories[category.key] = category;
                }
                // Track the stacking value (this makes drawing later very easy and cheap)
                fact.stackedValA = category.totalValue; // pre value
                category.totalValue += fact.val; 
                serie.totalValue += fact.val;
                fact.stackedValB = category.totalValue; // post value
            }
        }
        if (serie.totalValue != null && (serie.totalValue < serieMinTotalVal || serieMinTotalVal == null)) serieMinTotalVal = serie.totalValue;
        if (serie.totalValue != null && (serie.totalValue > serieMaxTotalVal || serieMaxTotalVal == null)) serieMaxTotalVal = serie.totalValue;
    }

    // Stacked? If so the min / max should reflect the series total min max
    if (opts.stackedSeries == true) {
        
        // Get the min/max over all categories
        Machinata.Util.each(categories, function (key,category) {
            if (category.totalValue != null && (category.totalValue < categoryMinTotalVal || categoryMinTotalVal == null)) categoryMinTotalVal = category.totalValue;
            if (category.totalValue != null && (category.totalValue > categoryMaxTotalVal || categoryMaxTotalVal == null)) categoryMaxTotalVal = category.totalValue;
        });
        Machinata.Reporting.debug("  " + "calc categoryMinTotalVal/categoryMaxTotalVal: " + categoryMinTotalVal + "/" + categoryMaxTotalVal);
        minVal = categoryMinTotalVal;
        maxVal = categoryMaxTotalVal;
    }

    // Debug out
    Machinata.Reporting.debug("  " + "calc min/max: " + minVal + "/" + maxVal);

    // Balance?
    if (opts.balance == true) {
        var minMaxAbsVal = Math.max(Math.abs(minVal), Math.abs(maxVal));
        if (minVal < 0 && maxVal > 0) {
            minVal = -minMaxAbsVal;
            maxVal = +minMaxAbsVal;
        } else {
            minVal = Math.min(0, minVal);
            maxVal = Math.max(0, maxVal);
        }
        Machinata.Reporting.debug("  " + "balanced min/max: " + minVal + "/" + maxVal);

    }

    // Snap to zero?
    // We do so if either both min/max are >0, or min/max < 0
    if (opts.snapToZero == true) {
        if (minVal >= 0 && maxVal >= 0) {
            minVal = 0;
        } else if (minVal < 0 && maxVal < 0) {
            maxVal = 0;
        }
        Machinata.Reporting.debug("  " + "zero-snap min/max: " + minVal + "/" + maxVal);
    }

    // Add margin
    if (opts.margin != null) {
        if (minVal < 0) minVal = minVal + (minVal * opts.margin);
        if (maxVal > 0) maxVal = maxVal + (maxVal * opts.margin);
        Machinata.Reporting.debug("  " + "margined min/max: " + minVal + "/" + maxVal);
    }

    // Use D3's nice algorithm to snap the domain to nice round values...
    if (opts.niceDomain == true) {
        // See https://github.com/d3/d3-scale/blob/master/src/nice.js
        // See https://github.com/d3/d3-scale/blob/v2.2.2/README.md#continuous_nice
        var d3NiceDomain = d3.scaleLinear().domain([minVal, maxVal]).nice().domain();
        minVal = d3NiceDomain[0];
        maxVal = d3NiceDomain[1];
        Machinata.Reporting.debug("  " + "niced min/max: " + minVal + "/" + maxVal);
    }


    // Set min max on axis's
    Machinata.Reporting.debug("  " + "new min/max: " + minVal + "/" + maxVal);
    for (var si = 0; si < series.length; si++) {
        var serie = series[si];
        if (serie[opts.axisKey] == null) serie[opts.axisKey] = {};
        // We only update the min/max if it has not already been explicitly set...
        if (serie[opts.axisKey].minValue == null) serie[opts.axisKey].minValue = minVal;
        if (serie[opts.axisKey].maxValue == null) serie[opts.axisKey].maxValue = maxVal;
        if (serie[opts.axisKey].formatType == null) serie[opts.axisKey].formatType = "number"; //fallback
    }

    // Set tick values, set formatting
    for (var si = 0; si < series.length; si++) {
        var serie = series[si];
        var axis = serie[opts.axisKey];
        if (axis.minValue != null && axis.maxValue != null) {
            if (axis.requestedMinValue == null) axis.requestedMinValue = axis.minValue;
            if (axis.requestedMaxValue == null) axis.requestedMaxValue = axis.maxValue;
            Machinata.Reporting.Node.VegaNode.Util.automaticallySetAxisTickValues(axis, opts.tickCount, opts.niceDomain, opts.extendToDomain);
            Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatAxis(axis);
        }
    }
    
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.autoSnapMultiSeriesAxis = function (instance, config, json, opts) {
    //TODO: @dankrusi: make this more generic
    //TODO: @dankrusi: refactor to options params

    // Init
    Machinata.Reporting.debug("Machinata.Reporting.Node.VegaNode.Util.autoSnapMultiSeriesAxis:");
    Machinata.Reporting.debug("  "+"node id: "+json.id);

    // Pre process
    // Here we track all the min/max, and track the absolute min/max across all series
    for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
        var seriesGroup = json.seriesGroups[sgi];
        if (seriesGroup.yAxis == null) seriesGroup.yAxis = {}; // Make sure the axis exists
        var minValue = null;
        var maxValue = null;
        for (var si = 0; si < seriesGroup.series.length; si++) {
            var serie = seriesGroup.series[si];
            for (var fi = 0; fi < serie.facts.length; fi++) {
                var fact = serie.facts[fi];
                if (fact.y != null && (fact.y < minValue || minValue == null)) minValue = fact.y;
                if (fact.y != null && (fact.y > maxValue || maxValue == null)) maxValue = fact.y;
            }
        }
        Machinata.Reporting.debug("  " + "orig " + seriesGroup.id + ": ");
        Machinata.Reporting.debug("  " + "  " + "min: " + seriesGroup.yAxis.minValue);
        Machinata.Reporting.debug("  " + "  " + "max: " + seriesGroup.yAxis.maxValue);
        Machinata.Reporting.debug("  " + "calc " + seriesGroup.id + ": ");
        Machinata.Reporting.debug("  " + "  " + "min: " + minValue);
        Machinata.Reporting.debug("  " + "  " + "max: " + maxValue);

        // Snap to zero?
        // We do so if either both min/max are >0, or min/max < 0
        if (json.autoSnapYAxisSnapToZero == true) {
            if (minValue >= 0 && maxValue >= 0) {
                minValue = 0;
            } else if (minValue < 0 && maxValue < 0) {
                maxValue = 0;
            }
            Machinata.Reporting.debug("  " + "snapped to zero " + seriesGroup.id + ": ");
            Machinata.Reporting.debug("  " + "  " + "min: " + minValue);
            Machinata.Reporting.debug("  " + "  " + "max: " + maxValue);
        }

        // Add margin
        if (json.autoSnapYAxisMargin != null) {
            if (minValue < 0) minValue = minValue * (1.0 + json.autoSnapYAxisMargin);
            if (maxValue > 0) maxValue = maxValue * (1.0 + json.autoSnapYAxisMargin);

            Machinata.Reporting.debug("  " + "margined " + seriesGroup.id + ": ");
            Machinata.Reporting.debug("  " + "  " + "margin: " + json.autoSnapYAxisMargin);
            Machinata.Reporting.debug("  " + "  " + "min: " + minValue);
            Machinata.Reporting.debug("  " + "  " + "max: " + maxValue);
        }

        // Use D3's nice algorithm to snap the domain to nice round values...
        if (json.autoSnapYAxisNiceDomain == true) {
            // See https://github.com/d3/d3-scale/blob/master/src/nice.js
            // See https://github.com/d3/d3-scale/blob/v2.2.2/README.md#continuous_nice
            var domain = Machinata.Reporting.Node.VegaNode.Util.getNiceDomain([minValue, maxValue]);
            minValue = domain[0];
            maxValue = domain[1];
            Machinata.Reporting.debug("  " + "niced " + seriesGroup.id + ": ");
            Machinata.Reporting.debug("  " + "  " + "min: " + minValue);
            Machinata.Reporting.debug("  " + "  " + "max: " + maxValue);
        }

        // Register, for now
        seriesGroup.yAxis.calcMinValue = minValue;
        seriesGroup.yAxis.calcMaxValue = maxValue;
        seriesGroup.yAxis.calcBandwidth = maxValue - minValue;

    }

    // Apply the strategy...
    if (json.autoSnapYAxisStategy == "proportional") {

        // Proportional strategy...

        // Track top/bottom min/max ratios (we use the upper value)
        var maxTopRatio = 0;
        var maxBottomRatio = 0;
        for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
            var seriesGroup = json.seriesGroups[sgi];
            if (seriesGroup.yAxis.preBandwidth != 0) {
                maxTopRatio = Math.max(maxTopRatio, seriesGroup.yAxis.calcMaxValue * (1) / seriesGroup.yAxis.calcBandwidth);
                maxBottomRatio = Math.min(maxBottomRatio, seriesGroup.yAxis.calcMinValue * (1) / seriesGroup.yAxis.calcBandwidth);
            }
        }
        // Apply each axis domain according to the max ratios
        for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
            var seriesGroup = json.seriesGroups[sgi];
            if (json.seriesGroups.length == 1) {
                // Exception if we only have one series group (we use the series group calculated value)
                seriesGroup.yAxis.strategyMinValue = seriesGroup.yAxis.calcMinValue;
                seriesGroup.yAxis.strategyMaxValue = seriesGroup.yAxis.calcMaxValue;
            } else if (seriesGroup.yAxis.calcBandwidth != 0) {
                seriesGroup.yAxis.strategyMinValue = maxBottomRatio * seriesGroup.yAxis.calcBandwidth;
                seriesGroup.yAxis.strategyMaxValue = maxTopRatio * seriesGroup.yAxis.calcBandwidth;
            }
        }

    } else if (json.autoSnapYAxisStategy == "none") {

        // No strategy...

        // Apply each axis domain according to the max ratios
        for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
            var seriesGroup = json.seriesGroups[sgi];
            seriesGroup.yAxis.strategyMinValue = minValue;
            seriesGroup.yAxis.strategyMaxValue = maxValue;
        }

    }else {

        throw "The auto snap strategy " + json.autoSnapYAxisStategy + " is not supported! Please check yout autoSnapYAxisStategy setting.";

    }

    // Check each group to see if a yaxis min/max is missing
    for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
        var seriesGroup = json.seriesGroups[sgi];

        // Don't apply a strategy min/max value if one has been manually set...
        if (seriesGroup.yAxis.minValue != null && seriesGroup.yAxis.maxValue != null) {
            seriesGroup.yAxis.requestedMinValue = seriesGroup.yAxis.minValue;
            seriesGroup.yAxis.requestedMaxValue = seriesGroup.yAxis.maxValue;
            continue;
        }

        // Set the axis value to those provided by the strategy
        seriesGroup.yAxis.minValue = seriesGroup.yAxis.strategyMinValue;
        seriesGroup.yAxis.maxValue = seriesGroup.yAxis.strategyMaxValue;
        seriesGroup.yAxis.requestedMinValue = seriesGroup.yAxis.strategyMinValue;
        seriesGroup.yAxis.requestedMaxValue = seriesGroup.yAxis.strategyMaxValue;
        Machinata.Reporting.debug("  " + "strategy " + seriesGroup.id + ": ");
        Machinata.Reporting.debug("  " + "  " + "min: " + seriesGroup.yAxis.strategyMinValue);
        Machinata.Reporting.debug("  " + "  " + "max: " + seriesGroup.yAxis.strategyMaxValue);
    }

    // Check each group to see if a insertYZeroRule is set but no axis crosses the zero mark?
    if (json.insertYZeroRule == true) {
        var aSeriesGroupCrossesZeroMark = false;
        for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
            var seriesGroup = json.seriesGroups[sgi];
            if (seriesGroup.yAxis.minValue <= 0 && seriesGroup.yAxis.maxValue >= 0) {
                aSeriesGroupCrossesZeroMark = true;
                break;
            }
        }
        if (aSeriesGroupCrossesZeroMark != true) {
            // Disable the zero mark
            json.insertYZeroRule = false;
        }
    }

    // Set tick values
    if (true) {

        // Reference: d3 ticks() source: https://github.com/d3/d3-array/blob/master/src/ticks.js
        Machinata.Reporting.debug("will set ticks manually:")
        var tickCount = json.autoSnapYAxisTickCount;
        if (tickCount == null) tickCount = config.axisTargetTickCount; //TODO: @dankrusi what about if we want responsive?
        var tries = 0;
        var maxTries = 10;
        var currentAxisBoundFitScaleFactor = 1.0;
        while (tries < maxTries) {
            tries++;
            Machinata.Reporting.debug("  "+"try", tries);
            var allAxisWithinRequestedBounds = true;
            for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
                var axis = json.seriesGroups[sgi].yAxis;
                if (sgi == 0) {
                    Machinata.Reporting.Node.VegaNode.Util.automaticallySetAxisTickValues(axis, tickCount, json.autoSnapYAxisNiceDomain);
                } else {
                    var refAxis = json.seriesGroups[0].yAxis;
                    var axisBandwidth = (axis.requestedMaxValue - axis.requestedMinValue);
                    var refAxisBandwidth = (refAxis.requestedMaxValue - refAxis.requestedMinValue);
                    var bandwidthFactor = (axisBandwidth / refAxisBandwidth) * currentAxisBoundFitScaleFactor;
                    //console.log("axisBandwidth", axisBandwidth);
                    //console.log("refAxisBandwidth", refAxisBandwidth);
                    //console.log("bandwidthFactor", bandwidthFactor);
                    axis.values = [];
                    for (var ti = 0; ti < refAxis.values.length; ti++) {
                        var refAxisTickValue = refAxis.values[ti];
                        var tickValue = refAxisTickValue * bandwidthFactor;
                        //console.log("tick", ti, refAxisTickValue, tickValue)
                        axis.values.push(tickValue);
                    }
                    Machinata.Reporting.debug("  " +"refAxis values",refAxis.values);
                    Machinata.Reporting.debug("  " +"axis values",axis.values);
                }
                // Register
                axis.minValue = axis.values[0];
                axis.maxValue = axis.values[axis.values.length - 1];
                // Check if within bounds
                if (axis.minValue > axis.requestedMinValue || axis.maxValue < axis.requestedMaxValue) {
                    allAxisWithinRequestedBounds = false;
                    Machinata.Reporting.debug("  " +"axis " + sgi + " does not fit bounds:");
                } else {
                    Machinata.Reporting.debug("  " +"axis " + sgi + " fits bounds:");

                }
                Machinata.Reporting.debug("  " + "requested", axis.requestedMinValue, axis.requestedMaxValue);
                Machinata.Reporting.debug("  " + "current", axis.minValue, axis.maxValue);
                
            }
            if (allAxisWithinRequestedBounds == false) {
                Machinata.Reporting.debug("  " +"not allAxisWithinRequestedBounds!");
                currentAxisBoundFitScaleFactor += 0.05;
            } else {
                Machinata.Reporting.debug("  " + "allAxisWithinRequestedBounds! Finishing...");
                break;
            }
        }

        
    }

    // Automatically set the axis format?
    if (true) {
        for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
            var axis = json.seriesGroups[sgi].yAxis;
            Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatAxis(axis);
        }
    }

};


/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.automaticallySetAxisTickValues = function (axis, tickCount, niceDomain, extendToDomain) {
    Machinata.Reporting.debug("  " + "automatically set axis tick values:");
    if (axis.tickCount != null) tickCount = axis.tickCount;
    var scale = d3.scaleLinear().domain([axis.minValue, axis.maxValue]);
    Machinata.Reporting.debug("  " + "raw scale", scale.domain());
    if (niceDomain == true) scale = scale.nice(tickCount);
    Machinata.Reporting.debug("  " + "niced scale", scale.domain());
    var ticks = scale.ticks(tickCount);
    Machinata.Reporting.debug("  " + "ticks", ticks);
    if (axis.requestedMinValue != null && axis.requestedMaxValue != null && extendToDomain != false) axis.values = Machinata.Reporting.Node.VegaNode.Util.extendTicksToDomain(ticks, axis.requestedMinValue, axis.requestedMaxValue);
    else axis.values = ticks;
    Machinata.Reporting.debug("  " + "values", axis.values);
    Machinata.Reporting.debug("  " + "ticks domain", axis.values[0], axis.values[axis.values.length - 1]);

    return axis.values;
};


/// <summary>
/// Automatically formats fact resolved values if the fact is between - 1 % and 1 % (very small) or greater than 
/// 99.5 % (very large, but not 100 %), or if equal to zero or one (0% or 100%),
/// we use the facts full formatting (```fact.format```), otherwise we use a standard full number percent format
/// that is simplified (ie 43% instead of 43.04%)...
/// Currently supported fact formats (```fact.format```):
///  - ```.2%```
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatEdgeNumbers = function (facts) {
    var fullNumberFormat = d3.format(".0%");
    for (var i = 0; i < facts.length; i++) {
        var fact = facts[i];
        //console.log(fact);
        if (fact.format != null && fact.format.endsWith("%")) {
            // If the fact is between -1% and 1% (very small) or greater than 99.5% (very large, but not 100%), 
            // we use the facts full formatting, otherwise we use a standard full number percent format
            // that is simplified (ie 43% instead of 43.04%)...
            if ((fact.val <= -0.01 || fact.val >= 0.01) && fact.val <= 0.995 && fact.val != 1) fact.resolved = fullNumberFormat(fact.val);
            else if (fact.val == 0 || fact.val == 1) fact.resolved = fullNumberFormat(fact.val);
        }
    }
};


/// <summary>
/// Helper function to preprocess access properties to make it faster and easier for Vega to work with...
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.preprocessAxisProperties = function (axis) {
    if (axis == null) return axis;

    // Convert ticks properties to raw values
    if (axis.ticks != null) {
        axis.values = []; // this is our standard vega values array
        axis.tickValues = []; // this is an additional array which controls if ticks are to be shown
        axis.labelValues = []; // this is an additional array which controls if labels are to be shown
        for (var i = 0; i < axis.ticks.length; i++) {
            var tick = axis.ticks[i];
            axis.values.push(tick.val);
            if (tick.showTick != false) axis.tickValues.push(tick.val);
            if (tick.showLabel != false) axis.labelValues.push(tick.val);
        }
    }

    return axis;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatAxis = function (axis) {
    // We only support number formats
    if (axis.formatType == "number" && axis.format != null && axis.values != null) {
        Machinata.Reporting.debug("Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatAxis:");
        if (axis.format.startsWith(".") && axis.format.endsWith("%")) {
            // Percent formatting ".5%"
            Machinata.Reporting.debug("  " + "automatically formatting percent axis...");
            var minValue = axis.values[0];
            var maxValue = axis.values[axis.values.length-1];
            var tickCount = axis.values.length;
            var stepWidth = (maxValue - minValue) / (tickCount-1);
            var decPoints = -Math.ceil(Math.log10(stepWidth)); // from bmpi https://banknotes.atlassian.net/secure/RapidBoard.jspa?rapidView=7&projectKey=NGR&modal=detail&selectedIssue=NGR-817&assignee=5cf767844ddcda0e7cb95b49
            if (isFinite(decPoints) == false) decPoints = 0; // make sure this never fails
            Machinata.Reporting.debug("  old format", axis.format);
            decPoints--; // TODO: @bmpi: for some reason this needs to be subracted one...?
            if (decPoints < 0) decPoints = 0; // make sure this never fails
            axis.format = "." + (decPoints) + "%";
            Machinata.Reporting.debug("  new format", axis.format);
            axis.formatSource = "Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatAxis";
        } else if (axis.format.startsWith(".") && axis.format.endsWith("f")) {
            // Decimal formatting ".f%"
            Machinata.Reporting.debug("  " + "automatically formatting decimal axis...");
            var minValue = axis.values[0];
            var maxValue = axis.values[axis.values.length - 1];
            var tickCount = axis.values.length;
            var stepWidth = (maxValue - minValue) / (tickCount - 1);
            var decPoints = -Math.ceil(Math.log10(stepWidth)); // from bmpi https://banknotes.atlassian.net/secure/RapidBoard.jspa?rapidView=7&projectKey=NGR&modal=detail&selectedIssue=NGR-817&assignee=5cf767844ddcda0e7cb95b49
            if (isFinite(decPoints) == false) decPoints = 0; // make sure this never fails
            Machinata.Reporting.debug("  old format", axis.format);
            decPoints++; // TODO: @bmpi: for some reason this needs to be added one...?
            if (decPoints < 0) decPoints = 0; // make sure this never fails
            axis.format = "." + (decPoints) + "f";
            Machinata.Reporting.debug("  new format", axis.format);
            axis.formatSource = "Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatAxis";
        } else {
            Machinata.Reporting.debug("  " + "unsupported automatic format:"+axis.format);
        }
    }
};


/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.extendTicksToDomain = function (ticks, requestedMin, requestedMax) {
    // Sanity
    if (ticks.length < 2) return ticks; // we need at least two ticks
    // Init
    var tries = 0;
    var maxTries = 4;
    while (tries < maxTries) {
        tries++;
        var minValue = ticks[0];
        var maxValue = ticks[ticks.length - 1];
        var step = ticks[1] - ticks[0]; // Note: this will have a floating point error on small numbers
        var stepNiced = Machinata.Reporting.Node.VegaNode.Util.getNiceStep(step);
        if (minValue > requestedMin) {
            if (tries == 1) Machinata.Reporting.debug("Automatically extending axis ticks:");
            Machinata.Reporting.debug(" adding to front:", step, stepNiced);
            ticks.unshift(minValue - stepNiced);
        } if (maxValue < requestedMax) {
            if (tries == 1) Machinata.Reporting.debug("Automatically extending axis ticks:");
            Machinata.Reporting.debug("  adding to back", step, stepNiced);
            ticks.push(maxValue + stepNiced);
        } else {
            return ticks;
        }
    }
    return ticks;
};


/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.applySettingsToVegaAxis = function (instance, config, json, axisDefinition) {
    // Ticks on bottom
    if (axisDefinition.orient == "bottom" && axisDefinition.ticks == null && config.axisBottomShowTicks == true) {
        axisDefinition.ticks = true;
        axisDefinition.tickSize = config.axisBottomTickSize;
    }
    // Offset on bottom (only if no ticks)
    else if (axisDefinition.orient == "bottom" && axisDefinition.offset == null && config.axisBottomOffset > 0) {
        axisDefinition.offset = config.axisBottomOffset;
    }
    // Angle on bottom
    if (axisDefinition.orient == "bottom" && json.xAxisLabelAngle != null && json.xAxisLabelAngle != 0) {
        // Figure out label overlap. Usually we want greedy, but if they are rotated enough we use false (off)
        var labelOverlap = "greedy"; // default
        if (json.xAxisLabelAngle != null && (json.xAxisLabelAngle >= 45 || json.xAxisLabelAngle <= -45)) {
            labelOverlap = "greedy";
            axisDefinition.labelBound = false;
        }
        if (json.xAxisLabelAngle != null && (json.xAxisLabelAngle == -90)) {
            axisDefinition.labelBaseline = "middle"; // Vertical text baseline of axis tick labels, overriding the default setting for the current axis orientation. One of alphabetic (default), top, middle, bottom, line-top, or line-bottom. The line-top and line-bottom values ≥ 5.10 operate similarly to top and bottom, but are calculated relative to the lineHeight rather than fontSize alone.
        }
        // Define axis
        axisDefinition.labelAngle = json.xAxisLabelAngle; // Angle in degrees of axis tick labels
        axisDefinition.labelAlign = "right"; // Horizontal text alignment of axis tick labels, overriding the default setting for the current axis orientation.
        axisDefinition.labelOverlap = labelOverlap; // The strategy to use for resolving overlap of axis labels. If false (the default), no overlap reduction is attempted. If set to true or "parity", a strategy of removing every other label is used (this works well for standard linear axes). If set to "greedy", a linear scan of the labels is performed, removing any label that overlaps with the last visible label (this often works better for log-scaled axes).
        axisDefinition.labelSeparation = 0; // The minimum separation that must be between label bounding boxes for them to be considered non-overlapping (default 0). This property is ignored if labelOverlap resolution is not enabled.
    } else if (axisDefinition.orient == "bottom") {
        axisDefinition.labelAngle = { "signal": "responsiveSize == 'tiny' ? 90 : 0" }; 
        axisDefinition.labelBaseline = { "signal": "responsiveSize == 'tiny' ? 'middle' : 'top'" }; 
        axisDefinition.labelAlign = { "signal": "responsiveSize == 'tiny' ? 'left' : 'center'" }; 
    }
    if (axisDefinition.labelOverlap == "none") axisDefinition.labelOverlap = false;
    // Apply tooltips
    if (json.xAxisShowTooltipsInLabel == true) {
        // Make sure encoding exists
        if (axisDefinition["encode"] == null) axisDefinition["encode"] = {};
        if (axisDefinition["encode"]["labels"] == null) axisDefinition["encode"]["labels"] = {};
        if (axisDefinition["encode"]["labels"]["enter"] == null) axisDefinition["encode"]["labels"]["enter"] = {};
        // Apply
        axisDefinition["encode"]["labels"]["interactive"] = true;
        axisDefinition["encode"]["labels"]["enter"]["tooltip"] = { "signal": "scale('xScaleTooltip',datum.value)" };
        
    }
    // Alternating label offset
    // EXPERIMENTAL!
    if (json.xAxisUseAlternatingLabelPositioning == true) {
        // Make sure encoding exists
        if (axisDefinition["encode"] == null) axisDefinition["encode"] = {};
        if (axisDefinition["encode"]["labels"] == null) axisDefinition["encode"]["labels"] = {};
        if (axisDefinition["encode"]["labels"]["update"] == null) axisDefinition["encode"]["labels"]["update"] = {};
        // Apply
        // This uses the scale called xScaleIndex to determine if even/odd, and then jumps the label around
        axisDefinition["encode"]["labels"]["update"]["dy"] = { "signal": "scale('xScaleIndex',datum.value) % 2 == 0 ? 0 : 20" };
        //axisDefinition["labelOverlap"] = "greedy";
        axisDefinition.labelAngle = 0; // don't allow
    }
    // Tick/Label custom values
    if (axisDefinition.tickValues != null) {
        // Make sure encoding exists
        if (axisDefinition["encode"] == null) axisDefinition["encode"] = {};
        if (axisDefinition["encode"]["ticks"] == null) axisDefinition["encode"]["ticks"] = {};
        if (axisDefinition["encode"]["ticks"]["enter"] == null) axisDefinition["encode"]["ticks"]["enter"] = {};
        // Apply
        var valuesArray = "(" + axisDefinition.tickValues.signal + ")"; // this is our signal which should return an array (or null if none) the values indicate which to actually show
        var labelValueSignal = "null"; // ignore/dont set
        if (axisDefinition["encode"]["ticks"]["enter"]["strokeWidth"] != null) {
            // Axis has custom strokeWidth signal, so we need to use that as our value
            labelValueSignal = axisDefinition["encode"]["ticks"]["enter"]["strokeWidth"].signal;
        }
        axisDefinition["encode"]["ticks"]["enter"]["strokeWidth"] = { "signal": "( " + valuesArray + "==null || " + valuesArray + ".length==0 || indexof(" + valuesArray + ",datum.value)!=-1) ? (" + labelValueSignal+") : (0)" };
    }
    if (axisDefinition.labelValues != null) {
        // Make sure encoding exists
        if (axisDefinition["encode"] == null) axisDefinition["encode"] = {};
        if (axisDefinition["encode"]["labels"] == null) axisDefinition["encode"]["labels"] = {};
        if (axisDefinition["encode"]["labels"]["update"] == null) axisDefinition["encode"]["labels"]["update"] = {};
        // Apply
        var valuesArray = "(" + axisDefinition.labelValues.signal + ")"; // this is our signal which should return an array (or null if none) the values indicate which to actually show
        var labelValueSignal = "datum.label";
        if (axisDefinition["encode"]["labels"]["update"]["text"] != null) {
            // Axis has custom text update, so we need to use that as our value
            labelValueSignal = axisDefinition["encode"]["labels"]["update"]["text"].signal;
        }
        axisDefinition["encode"]["labels"]["update"]["text"] = { "signal": "( " + valuesArray + "==null || " + valuesArray + ".length==0 || indexof(" + valuesArray + ",datum.value)!=-1) ? ("+labelValueSignal+") : ('')" };
    }
    return axisDefinition;
};


/// <summary>
/// </summary>
Machinata.Reporting.Node.VegaNode.Util.createStatusLabelSpec = function (instance, config, json, status, opts) {
    // Sanity
    if (status == null) return { "type": "group" };
    if (opts == null) opts = {};
    if (opts.x == null) opts.x = "width/2";
    if (opts.y == null) opts.y = "height/2";
    if (opts.maxWidth == null) opts.maxWidth = "width";
    if (opts.maxHeight == null) opts.maxHeight = "height";
    if (opts.safetyMargin == null) opts.safetyMargin = 10;
    if (opts.lineHeight == null) opts.lineHeight = 1.2;
    
    // Build texts
    var texts = [];
    var tooltip = Machinata.Reporting.Text.resolve(instance, status.tooltip);
    if (status.subTitle != null) {
        var text = {
            text: Machinata.Reporting.Text.resolve(instance, status.subTitle),
            size: config.labelSize,
            tooltip: tooltip
        };
        texts.push(text);
    }
    if (status.title != null) {
        var text = {
            text: Machinata.Reporting.Text.resolve(instance, status.title),
            size: config.subtitleSize,
            tooltip: tooltip
        };
        texts.push(text);
    }

    // Build text marks
    // First step is to split the lines into multiple texts
    var textsLineBreaked = [];
    var totalLineSize = 0;
    for (var i = 0; i < texts.length; i++) {
        var text = texts[i];
        var lines = Machinata.Reporting.Tools.lineBreakString(text.text, { returnArray: true });
        for (var l = 0; l < lines.length; l++) {
            var textLineBreaked = {
                text: lines[l],
                size: text.size,
                lineSize: text.size * opts.lineHeight,
                tooltip: tooltip,
            }
            totalLineSize += textLineBreaked.lineSize;
            textsLineBreaked.push(textLineBreaked);
        }
    }

    // Apply positioning to each text line
    var currentLineY = 0;
    for (var i = 0; i < textsLineBreaked.length; i++) {
        var text = textsLineBreaked[i];
        text.dy = (-totalLineSize / 2) + text.lineSize/2; // go up to top from center
        text.dy += currentLineY; // move down to current position
        currentLineY += text.lineSize;
    }

    // Now build the mark specs
    var textMarks = [];
    for (var i = 0; i < textsLineBreaked.length; i++) {
        var text = textsLineBreaked[i];
        var textMark = {
            "type": "text",
            "style": "text",
            "encode": {
                "enter": {
                    "fontWeight": { "value": "bold" },
                    "text": { "value": text.text },
                    "tooltip": { "value": text.tooltip }, 
                    "align": { "value": "center" },
                    "baseline": { "value": "middle" },
                },
                "update": {
                    "limit": { "signal": "maxWidthMargined" }, // limits the num chars
                    "fontSize": { "value": text.size }, 
                    "x": { "signal": "maxWidthMargined/2" },
                    "y": { "signal": "maxHeightMargined/2" },
                    "dy": { "value": text.dy },
                }
            }
        };
        textMarks.push(textMark);
    }

    // Build spec
    var spec = {
        // Text
        "type": "group",
        "encode": {
            "enter": {
                //"fill": { "signal": "'red'" }, // DEBUG ONLY
                //"opacity": { "signal": "0.5" }, // DEBUG ONLY
            },
            "update": {
                "xc": { "signal": opts.x },
                "yc": { "signal": opts.y },
                "width": { "signal": opts.maxWidth },
                "height": { "signal": opts.maxHeight },
            }
        },
        "signals": [
            {
                "name": "maxWidth",
                "update": opts.maxWidth,
            },
            {
                "name": "maxHeight",
                "update": opts.maxHeight,
            },
            {
                "name": "maxWidthMargined",
                "update": "(" + opts.maxWidth + ")-" + opts.safetyMargin * 2,
            },
            {
                "name": "maxHeightMargined",
                "update": "(" + opts.maxHeight + ")-" + opts.safetyMargin * 2,
            }
        ],
        "marks": [
            // Margin
            {
                "type": "group",
                "encode": {
                    "enter": {
                        //"fill": { "signal": "'blue'" }, // DEBUG ONLY
                    },
                    "update": {
                        "xc": { "signal": "maxWidth / 2" },
                        "yc": { "signal": "maxHeight / 2" },
                        "width": { "signal": "maxWidthMargined" },
                        "height": { "signal": "maxHeightMargined" },
                    }
                },
                "marks": textMarks
            },
            
        ]
    };
    return spec;
};