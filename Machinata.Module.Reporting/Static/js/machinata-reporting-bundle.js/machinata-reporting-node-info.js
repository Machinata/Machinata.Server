/// <summary>
/// A very simple node that displays a info message for testing and demonstration
/// purposes. A user on a production system should not see this.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.InfoNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.InfoNode.defaults = {};

/// <summary>
/// This node does not support a toolbar.
/// </summary>
Machinata.Reporting.Node.InfoNode.defaults.supportsToolbar = false;

/// <summary>
/// Defines the type of info. Can either be ```'info'``` or ```'warning'```.
/// If set to 'none' no icon is displayed.
/// </summary>
Machinata.Reporting.Node.InfoNode.defaults.infoType = "info";

/// <summary>
/// The info message to show (string or resolvable text).
/// Required
/// </summary>
Machinata.Reporting.Node.InfoNode.defaults.message = null;

/// <summary>
/// A title to display just before the message (string or resolvable text).
/// Optional
/// </summary>
Machinata.Reporting.Node.InfoNode.defaults.title = null;

/// <summary>
/// </summary>
Machinata.Reporting.Node.InfoNode.defaults.chrome = "solid";


Machinata.Reporting.Node.InfoNode.init = function (instance, config, json, nodeElem) {
    var bgElem = $("<div class='node-bg'></div>").appendTo(nodeElem);
    var messageElem = $("<div class='info-message'></div>").appendTo(bgElem);
    // Message/title
    var msg = json.message;
    if (json.message != null && json.message.resolved != null) msg = Machinata.Reporting.Text.resolve(instance, json.message);
    var title = json.title;
    if (json.title != null && json.title.resolved != null) title = Machinata.Reporting.Text.resolve(instance, json.title);
    if (msg != null) messageElem.text(msg);
    if (title != null) messageElem.prepend($("<span class='info-title'></span>").text(title));
    // Icon
    if (json.infoType == "warning") messageElem.prepend(Machinata.Reporting.buildIcon(instance, "alert"));
    else if (json.infoType == "info") messageElem.prepend(Machinata.Reporting.buildIcon(instance,"info"));

};
Machinata.Reporting.Node.InfoNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...
};








