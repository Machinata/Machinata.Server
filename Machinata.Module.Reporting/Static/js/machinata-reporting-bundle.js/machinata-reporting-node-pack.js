


/// <summary>
/// </summary>
/// <example>
/// See ```example-report-packs.json``` for complete example usages.
/// </example>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.LineColumnNode</inherits>
Machinata.Reporting.Node.PackNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.PackNode.defaults = {};

/// <summary>
/// 
/// </summary>
Machinata.Reporting.Node.PackNode.defaults.chrome = "light";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.PackNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.PackNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.PackNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_4x2, Machinata.Reporting.Layouts.BLOCK_2x2];

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.PackNode.defaults.supportsToolbar = true;

/// <summary>
/// Yes, insert a legend by default.
/// </summary>
Machinata.Reporting.Node.PackNode.defaults.insertLegend = false;


/// <summary>
/// Sets the number of columns to use for legends. By default we use ```2```.
/// </summary>
Machinata.Reporting.Node.PackNode.defaults.legendColumns = 3;

/// <summary>
/// Sets the correct legend data source for the automatic legend.
/// </summary>
Machinata.Reporting.Node.PackNode.defaults.legendDataSource = "factsLegend";

Machinata.Reporting.Node.PackNode.getVegaSpec = function (instance, config, json, nodeElem) {
    return {
        "data": [
          {
              "name": "facts",
              "values": null
          },
          {
              "name": "factsResolved",
              "source": "facts",
              "transform": [
                {
                    "type": "stratify",
                    "key": "id",
                    "parentKey": "parent"
                },
                {
                    "type": "pack",
                    "field": json.weightingKey,
                    "padding": 4,
                    "sort": { "field": json.weightingKey },
                    "size": [{ "signal": "width" }, { "signal": "height" }]
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "key",
                    "expr": "datum." + json.shadingKey
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "colorResolved",
                    "expr": "datum.depth == 0 ? scale('theme-gray-bg','gray3') : ( datum.depth == 1 ? scale('theme-gray-bg','gray1') :  scale('color',datum." + json.shadingKey+") )  "
                },
              ]
          },
          {
              "name": "factsPacked",
              "source": "factsResolved",
              "transform": [
                    
                    
              ]
          },
          {
              "name": "factsLegend",
              "source": "factsResolved",
              "transform": [
                    { "type": "filter", "expr": "datum.depth >= 2" },
                    {
                        "type": "aggregate",
                        "groupby": [json.shadingKey, "colorResolved", "key"],
                    }

              ]
          }
        ],
        "signals": [
            
        ],
        "scales": [
            {
                "name": "color",
                "type": "ordinal",
                "domain": { "data": "facts", "field": json.shadingKey },
                "range": { "scheme": json.theme + "-bg" }
            },
            {
                "name": "colorFill",
                "type": "quantize",
                "domain": [0,1],
                "range": { "scheme": "infographic-bg" },
                "reverse": true
            },
            {
                "name": "legendColor",
                "type": "ordinal",
                "domain": { "data": "factsLegend", "field": "key" },
                "range": { "data": "factsLegend", "field": "colorResolved" },
            },
            {
                "name": "grayColor",
                "type": "ordinal",
                "domain": { "data": "facts", "field": "id" },
                "range": { "scheme": "gray-bg" }
            },
            {
                "name": "infographicColor",
                "type": "ordinal",
                "domain": { "data": "facts", "field": "id" },
                "range": { "scheme": "infographic-bg" }
            }
        ],
        "marks": [
          {
              "type": "symbol",
              "from": { "data": "factsPacked" },
              "encode": {
                  "enter": {
                      "shape": { "value": "circle" },
                      "tooltip": { "signal": "datum.titleResolved + (datum." + json.weightingKey + " ? ': '+format(datum." + json.weightingKey + ",'.2s')+', '+datum.weight+', '+datum." + json.shadingKey + " : '')" }
                  },
                  "update": {
                      "x": { "field": "x" },
                      "y": { "field": "y" },
                      "size": { "signal": "4 * datum.r * datum.r" },
                      "stroke": { "value": "black" },
                      "strokeWidth": { "value": 0 },
                      "fill": { "signal": "datum.colorResolved" },
                  },
                  "hover": {
                      //"strokeWidth": { "value": 2 },
                  }
              }
          }
        ]
    };
};
Machinata.Reporting.Node.PackNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    spec["data"][0].values = json.facts;
};
Machinata.Reporting.Node.PackNode.init = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.PackNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);

    Machinata.Reporting.Node.VegaNode.debugOutputDataSet(nodeElem, 'factsLegend');
};
Machinata.Reporting.Node.PackNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};








