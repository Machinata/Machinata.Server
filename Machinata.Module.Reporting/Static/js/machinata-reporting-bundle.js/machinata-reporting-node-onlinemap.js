/// <summary>
/// 
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.OnlineMapNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.OnlineMapNode.defaults = {};

/// <summary>
/// This node supports a toolbar.
/// </summary>
Machinata.Reporting.Node.OnlineMapNode.defaults.supportsToolbar = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.OnlineMapNode.defaults.tileLayerAPIKey = null;

/// <summary>
/// </summary>
Machinata.Reporting.Node.OnlineMapNode.defaults.markersClustered = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.OnlineMapNode.defaults.markersJitter = null;

/// <summary>
/// </summary>
Machinata.Reporting.Node.OnlineMapNode.defaults.mapMinZoom = 3;

/// <summary>
/// </summary>
Machinata.Reporting.Node.OnlineMapNode.defaults.mapMaxZoom = null;



/// <summary>
/// </summary>
Machinata.Reporting.Node.OnlineMapNode.init = function (instance, config, json, nodeElem) {
    
    // Validate Core JS Maps
    if (Machinata.Maps == null) throw "Machinata.Reporting.Node.OnlineMapNode: Machinata.Maps has not be loaded. Please include the Machinata Core Maps JS library machinata-maps-bundle.js to have ";

    // Validate that a API key has been explicitly set in reporting config
    if (json.tileLayerAPIKey == null) throw "Machinata.Reporting.Node.OnlineMapNode: No Tile Layer API Key configured";

    // Create UI
    var mapElem = $("<div class='map'></div>").appendTo(nodeElem);
    nodeElem.data("map-elem", mapElem);

    // Force our api key
    Machinata.Maps.DEFAULT_MAPS_ACCESS_TOKEN = json.tileLayerAPIKey;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.OnlineMapNode.postBuild = function (instance, config, json, nodeElem) {

    // Create the map...
    var mapElem = nodeElem.data("map-elem");
    var mapOptions = {
        minZoom: json.mapMinZoom, // zoomed out
        //maxZoom: json.mapMaxZoom, // zoomed in
        doubleClickZoom: true,
        scrollWheelZoom: true
    };
    if (json.mapMaxZoom != null) mapOptions.maxZoom = json.mapMaxZoom;
    var options = {};
    options["providerOptions"] = mapOptions;
    var map = Machinata.Maps.createMap(mapElem, options);
    nodeElem.data("map", map);

    // Add facts as markers to map...
    var markers = [];
    for (var i = 0; i < json.facts.length; i++) {
        var fact = json.facts[i];
        // Sanity
        if (fact.address == null && fact["lat-lng"] == null) {
            console.warn("Machinata.Reporting.Node.OnlineMapNode: fact has not address or coordinate!",fact);
            continue;
        }
        // Unwrap link url
        if (fact.link != null) {
            fact["link"] = fact.link.url;
        }
        markers.push(fact);
    }
    Machinata.Maps.addMarkersToMap(map, markers, json.markersClustered, json.markersJitter);
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.OnlineMapNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...
};









