/// <summary>
/// A simple node for displaying a menu of items in a standard menubar.
/// 
/// ## Node Links and Drilldowns
/// When using node links and drilldowns, it is up to the report designer to ensure that a logical
/// and realistic link between a menubar item and the targetted node is made. 
/// For example, if a menubar links to a node which is not on the same page (ie chapter), this does not
/// make sense in many cases (especially if it is initially selected) and the resulting behavior is not defined.
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node</inherits>
Machinata.Reporting.Node.MenuBarNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.MenuBarNode.defaults = {};

/// <summary>
/// By default this chart is infographic-style ```light```.
/// </summary>
Machinata.Reporting.Node.MenuBarNode.defaults.chrome = "light";

/// <summary>
/// This node does not support headless rendering...
/// </summary>
Machinata.Reporting.Node.MenuBarNode.defaults.supportsHeadlessRendering = false;

/// <summary>
/// This node does not support being added to dashboards...
/// </summary>
Machinata.Reporting.Node.MenuBarNode.defaults.supportsDashboard = false;

/// <summary>
/// No support for toolbar.
/// </summary>
Machinata.Reporting.Node.MenuBarNode.defaults.supportsToolbar = false;

/// <summary>
/// A array of menu items to display. Each item is added to the menu bar in the order of
/// the array, and can have any standard link item bound to it.
/// Each item can have a title, tooltip (optional) and a link:
///
/// -```title```: resolvable string
/// -```tooltip```: resolvable string
/// -```selected```: boolean, if true makes this item initially selected. If the selected menu item link is a drilldown, this is automatically performed when the node is first rendered, however the drilldown won't perform a scroll..
/// -```link```: standard link object, see Machinata.Reporting.Tools.openLink for more details
/// </summary>
/// <example>
/// ```
/// [
///     {
///         "title": { "resolved": "All" },
///         "tooltip": { "resolved": "Show all document languages" },
///         "link": {
///             "nodeId": "SampleTableForDrilldown1",
///                 "drilldown": {
///                 "filters": [
///                 ]
///             }
///         }
///     },
///     {
///         "title": { "resolved": "EN" },
///         "selected": true,
///         "tooltip": { "resolved": "Show only English documents" },
///         "link": {
///             "nodeId": "SampleTableForDrilldown1",
///                 "drilldown": {
///                 "filters": [
///                     {
///                         "columnId": "lang",
///                         "value": "EN",
///                         "name": "EN"
///                     }
///                 ]
///             }
///         }
///     },
///     {
///         "title": { "resolved": "DE" },
///         "tooltip": { "resolved": "Show only German documents" },
///         "link": {
///             "nodeId": "SampleTableForDrilldown1",
///                 "drilldown": {
///                 "filters": [
///                     {
///                         "columnId": "lang",
///                         "value": "DE",
///                         "name": "DE"
///                     }
///                 ]
///             }
///         }
///     }
/// ]
/// ```
/// </example>
Machinata.Reporting.Node.MenuBarNode.defaults.menuItems = null;

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.MenuBarNode.init = function (instance, config, json, nodeElem) {
    // Init
    var useSoftTabMenuForMenuSelection = true;
    // Create menubar
    var menuBar = Machinata.Reporting.buildMenubar(instance, { softMenuBar: useSoftTabMenuForMenuSelection }).appendTo(nodeElem);
    nodeElem.data("menubar", menuBar);
    nodeElem.addClass("menu-selection");
    // Create items
    for (var i = 0; i < json.menuItems.length; i++) {
        var menuItem = json.menuItems[i];
        var buttonElem = Machinata.Reporting.addMenubarButton(instance, nodeElem, Machinata.Reporting.Text.resolve(instance, menuItem.title), function (clickedElem) {
            // Get menu item
            var clickedMenuItem = clickedElem.data("item");
            // Update UI
            nodeElem.find(".menu-item").removeClass("selected");
            clickedElem.addClass("selected");
            // Perform link open
            var clickedMenuItem = clickedElem.data("item");
            Machinata.Reporting.Tools.openLink(instance, clickedMenuItem.link);
        }, {
            softMenuBar: useSoftTabMenuForMenuSelection,
            tooltip: Machinata.Reporting.Text.resolve(instance, menuItem.tooltip)
        });
        if (menuItem.selected == true) {
            buttonElem.addClass("selected")
        }
        buttonElem.data("item", menuItem);
        buttonElem.addClass("menu-item");
    }
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.MenuBarNode.draw = function (instance, config, json, nodeElem) {
    // Gate
    if (json._state != null && json._state.firstDraw == false) return;
    json._state.firstDraw = false;

    // Do initial selection
    var selectedButton = nodeElem.find(".menu-item.selected");
    if (selectedButton.length == 1) {
        // Get the link object
        var selectedMenuItem = selectedButton.data("item");
        if (selectedMenuItem.link.drilldown != null) {
            var clonedMenuItemLink = Machinata.Util.cloneJSON(selectedMenuItem.link);
            // Patch drilldown since we don't want the scroll to happen
            clonedMenuItemLink.scrollPage = false;
            // Perform the open link
            Machinata.Reporting.Tools.openLink(instance, clonedMenuItemLink);
        }
    }
};





