


/// <summary>
/// 
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.ThermometerNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults = {};

/// <summary>
/// This is a infographic, thus ```light```.
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.chrome = "light";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_4x2, Machinata.Reporting.Layouts.BLOCK_4x4];

/// <summary>
/// Yes, we support toolbar.
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.supportsToolbar = true;

/// <summary>
/// No legends for this chart.
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.insertLegend = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.titleSize = 22;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.titleLineHeight = 1.2;


/// <summary>
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.labelSize = 12;


/// <summary>
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.valueSize = 22;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.valueLargeSize = 22;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.symbolSize = 18;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.symbolFontWeight = "bold";

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.strokeDash = "4,4";

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.titleMaxCharsPerLine = 16;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.graphicAlignment = "middle";

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.beginAndEndBucketsAlignLeft = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.bucketFactHeight = 60;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.bucketFactPaddingMultiplier = 1;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThermometerNode.defaults.chartBorderMultiplier = 0.5;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ThermometerNode.getVegaSpec = function (instance, config, json, nodeElem) {
    return {
        "data": [
            {
                "name": "labels",
                "values": null
            },
            {
                "name": "shades",
                "values": null
            },
            {
                "name": "labelsResolved",
                "source": "labels",
                "transform": [
                    {
                        "type": "formula",
                        "initonly": true,
                        "as": "titleResolved",
                        "expr": "datum.title != null ? datum.title.resolved : null"
                    },
                    {
                        "type": "formula",
                        "initonly": true,
                        "as": "colorBGResolved",
                        "expr": "scale('theme-bg', datum.colorShade)"
                    },
                    {
                        "type": "formula",
                        "initonly": true,
                        "as": "colorFGResolved",
                        "expr": "scale('theme-fg', datum.colorShade)"
                    },
                    {
                        "type": "formula",
                        "initonly": true,
                        "as": "subTitleResolved",
                        "expr": "datum.subTitle != null ? datum.subTitle.resolved : null"
                    },
                    {
                        "type": "formula",
                        "initonly": true,
                        "as": "hasTitle",
                        "expr": "datum.title != null ? true : false"
                    },
                    {
                        "type": "formula",
                        "initonly": true,
                        "as": "hasSubTitle",
                        "expr": "datum.subTitle != null ? true : false"
                    },
                    {
                        "type": "formula",
                        "initonly": true,
                        "as": "hasTitleOrSubTitle",
                        "expr": "(datum.title != null || datum.subTitle != null) ? true : false"
                    },
                    {
                        "type": "formula",
                        "initonly": true,
                        "as": "hasTitleAndSubTitle",
                        "expr": "(datum.title != null && datum.subTitle != null) ? true : false"
                    },

                ]
            }
        ],
        "signals": [
            { "name": "minThermometerWidth", "init": "100" },
            { "name": "thermometerWidth", "update": "round(min(width*0.25,minThermometerWidth))" },
            { "name": "thermometerHeight", "update": "height" },
            { "name": "temperatureMin", "init": "0" },
            { "name": "temperatureMax", "init": "4.5" },
            { "name": "tickSmallSize", "update": "round(thermometerWidth*1.0)" },
            { "name": "tickLargeSize", "update": "round(width/2 - thermometerWidth/2)" },
            { "name": "tickOutsideOffset", "init": "0.75" }, // controls how far outside of the thermometer the tick extends (in percent of the thermometer width)
            { "name": "tickInsideOffset", "init": "0.25" }, // controls how far inside into the thermometer the tick extends (in percent of the thermometer width)
            { "name": "strokeThinSize", "init": config.lineSize }, 
            { "name": "strokeStrongSize", "init": config.lineSize*2 }, 
            { "name": "titleOffsetDY", "init": "-config_labelSize * 0.1" },
        ],
        "marks": [
            {
                "type": "group",
                "encode": {
                    "enter": {
                    },
                    "update": {
                        "x": { "signal": "width/2 - thermometerWidth/2" },
                        "width": { "signal": "thermometerWidth" },
                        "y": { "signal": "0" },
                        "height": { "signal": "thermometerHeight" },
                    }
                },
                "scales": [
                    {
                        "name": "thermometerScale",
                        "type": "linear",
                        "zero": false,
                        "domain": { "signal": "[temperatureMax,temperatureMin]" },
                        "nice": true,
                        "round": true,
                        "range": { "signal": "[0,thermometerHeight]" }
                    }
                ],
                "marks": [
                    {
                        "type": "rect",
                        "name": "colorRect",
                        "from": { "data": "shades" },
                        "encode": {
                            "enter": {
                                "fill": { "signal": "scale('theme-bg', datum.colorShade)" },
                            },
                            "update": {
                                "x": { "signal": "0" },
                                "y": { "signal": "round(scale('thermometerScale',datum.startVal))" },
                                "x2": { "signal": "thermometerWidth" },
                                "y2": { "signal": "round(scale('thermometerScale',datum.endVal))" },
                            }
                        },
                    },
                    {
                        "type": "rect",
                        "name": "borderRect",
                        "encode": {
                            "enter": {
                                "fill": { "value": "transparent" },
                                "stroke": { "value": config.domainLineColor },
                                "strokeWidth": { "signal": "strokeStrongSize" },
                            },
                            "update": {
                                "x": { "signal": "0" },
                                "y": { "signal": "0" },
                                "x2": { "signal": "thermometerWidth" },
                                "y2": { "signal": "thermometerHeight" },
                            }
                        },
                    },
                    {
                        "type": "rule",
                        "name": "thermometerValueLine",
                        "encode": {
                            "enter": {
                                "stroke": { "value": config.domainLineColor },
                                "strokeWidth": { "signal": "strokeStrongSize*2" },
                            },
                            "update": {
                                "x": { "signal": "thermometerWidth*0.5" },
                                "x2": { "signal": "thermometerWidth*0.5" },
                                "y": { "signal": "scale('thermometerScale',0)" },
                                "y2": { "signal": "scale('thermometerScale'," + json.thermometerValue + ")" },
                            }
                        },
                    },
                    {
                        "type": "symbol",
                        "name": "thermometerValueBulb",
                        "encode": {
                            "enter": {
                                "fill": { "value": config.domainLineColor },
                            },
                            "update": {
                                "xc": { "signal": "thermometerWidth*0.5" },
                                "yc": { "signal": "scale('thermometerScale'," + json.thermometerValue + ")" },
                                "size": { "signal": "thermometerWidth*12" },
                            }
                        },
                    },
                    {
                        "type": "group",
                        "name": "labelItem",
                        "from": { "data": "labelsResolved" },
                        "encode": {
                            "enter": {
                            },
                            "update": {
                                "x": { "signal": "datum.orient == 'right' ? thermometerWidth : 0" },
                                "y": { "signal": "round(scale('thermometerScale',datum.val))" },
                            }
                        },
                        "signals": [
                            { "name": "tickSmallX1", "update": "parent.orient == 'right' ? (-tickSmallSize * tickInsideOffset) : (-tickSmallSize * tickOutsideOffset)" },
                            { "name": "tickSmallX2", "update": "parent.orient == 'right' ? (tickSmallSize * tickOutsideOffset) : (tickSmallSize * tickInsideOffset)" },
                            { "name": "tickSmallTextXC", "update": "parent.orient == 'right' ? (tickSmallSize * tickOutsideOffset) : (-tickSmallSize * tickOutsideOffset)" },
                            { "name": "tickLargeX1", "update": "parent.orient == 'right' ? (-tickSmallSize * tickInsideOffset) : (-tickLargeSize * tickOutsideOffset)" },
                            { "name": "tickLargeX2", "update": "parent.orient == 'right' ? (tickLargeSize * tickOutsideOffset) : (tickSmallSize * tickInsideOffset)" },
                            { "name": "tickLargeTextXC", "update": "parent.orient == 'right' ? (tickLargeSize * tickOutsideOffset) : (-tickLargeSize * tickOutsideOffset)" },
                       ],
                        "marks": [
                            {
                                "type": "rule",
                                "encode": {
                                    "enter": {
                                        "stroke": { "signal": "parent.hasTitleOrSubTitle ? '" + config.domainLineColor + "' : '" + config.domainLineColor + "'" },
                                        "strokeWidth": { "signal": "parent.hasTitleOrSubTitle ? strokeStrongSize : strokeThinSize" },
                                    },
                                    "update": {
                                        "x": { "signal": "parent.hasTitleOrSubTitle ? tickLargeX1 : tickSmallX1" },
                                        "x2": { "signal": "parent.hasTitleOrSubTitle ? tickLargeX2 : tickSmallX2" },
                                        "y": { "signal": "0" },
                                        "y2": { "signal": "0" }
                                    }
                                },
                            },
                            {
                                "type": "text",
                                "name": "textAxisLabel",
                                "encode": {
                                    "enter": {
                                        "fontSize": { "signal": "config_labelSize" },
                                        "fontWeight": { "signal": "parent.hasTitleOrSubTitle ? 'bold' : 'normal'" },
                                        "text": { "signal": "parent.resolved" },
                                        //"tooltip": { "signal": "parent" },
                                        "baseline": { "value": "bottom" },
                                        "align": { "signal": "parent.orient == 'right' ? 'right' : 'left'" },
                                        "dy": { "signal": "titleOffsetDY" },
                                    },
                                    "update": {
                                        "xc": { "signal": "tickSmallTextXC" },
                                        "yc": { "signal": "0" },
                                    }
                                },
                            },
                            {
                                "type": "text",
                                "name": "textTitle",
                                "encode": {
                                    "enter": {
                                        "fontSize": { "signal": "config_subtitleSize" },
                                        "text": { "signal": "parent.titleResolved" },
                                        "baseline": { "value": "bottom" },
                                        "align": { "signal": "parent.orient == 'right' ? 'right' : 'left'" },
                                        "dy": { "signal": "titleOffsetDY" },
                                    },
                                    "update": {
                                        "xc": { "signal": "tickLargeTextXC" },
                                        "yc": { "signal": "parent.hasTitleAndSubTitle ? -config_labelSize*1.2 : 0" },
                                    }
                                },
                            },
                            {
                                "type": "text",
                                "name": "textSubTitle",
                                "encode": {
                                    "enter": {
                                        "fontSize": { "signal": "config_labelSize" },
                                        "text": { "signal": "parent.subTitleResolved" },
                                        "baseline": { "value": "bottom" },
                                        "align": { "signal": "parent.orient == 'right' ? 'right' : 'left'" },
                                        "dy": { "signal": "titleOffsetDY" },
                                    },
                                    "update": {
                                        "xc": { "signal": "tickLargeTextXC" },
                                        "yc": { "signal": "0" },
                                    }
                                },
                            }
                        ]
                    }
                ],
            }
        ]
    };
};
Machinata.Reporting.Node.ThermometerNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    // Calculate end vals
    
    // Apply
    spec["data"][0].values = json.labels;
    spec["data"][1].values = json.shades;
};

Machinata.Reporting.Node.ThermometerNode.init = function (instance, config, json, nodeElem) {
    // Process data
    
    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.ThermometerNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.ThermometerNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};







