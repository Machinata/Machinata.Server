


/// <summary>
/// Abstract class for all nodes that need to draw a bunch of shapes on a graph
/// 
/// </summary>
/// <example>
/// </example>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.ShapeGraphNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ShapeGraphNode.defaults = {};

/// <summary>
/// By default charts are on solid chrome.
/// </summary>
Machinata.Reporting.Node.ShapeGraphNode.defaults.chrome = "solid";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.ShapeGraphNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.ShapeGraphNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.ShapeGraphNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_4x2];

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.ShapeGraphNode.defaults.supportsToolbar = true;

/// <summary>
/// Yes, insert a legend by default.
/// </summary>
Machinata.Reporting.Node.ShapeGraphNode.defaults.insertLegend = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ShapeGraphNode.defaults.showXAxisGrid = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ShapeGraphNode.defaults.showYAxisGrid = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ShapeGraphNode.getVegaSpec = function (instance, config, json, nodeElem) {
    
    return {
        "data": [
          {
              "name": "shapes",
              "values": null,
            },
            {
                "name": "allShapePoints",
                "source": "shapes",
                "transform": [
                    {
                        "type": "flatten",
                        "fields": [
                            "points"
                        ],
                        "as": [
                            "point"
                        ],
                    },
                ]
            },
        ],
        "signals": [
            {
                "name": "globalXScalePadding",
                "update": "0"
            },
        ],
        "scales": [
          {
              "name": "colorByKey",
              "type": "ordinal",
                "domain": { "data": "shapes", "field": "key" },
              "range": { "scheme": json.theme + "-bg" }
          },
          {
              "name": "color",
              "type": "ordinal",
              "domain": { "data": "shapes", "field": "key" },
              "range": { "data": "shapes", "field": "colorResolved" },
          },
            {
                "name": "xScale",
                "type": "time", //TODO @dankrusi should be configurable!
                "domain": {
                    "data": "allShapePoints",
                    "field": "point.x"
                },
                "domainMin": {
                    "signal": (json.xAxis != null && json.xAxis.minValue != null) ? json.xAxis.minValue  : "null",
                },
                "domainMax": {
                    "signal": (json.xAxis != null && json.xAxis.maxValue != null) ? json.xAxis.maxValue : "null",
                },
                "range": "width",
                "padding": config.padding, 
            },
            {
                "name": "yScale",
                "type": "linear",
                "domain": {
                    "data": "allShapePoints",
                    "field": "point.y"
                },
                "domainMin": {
                    "signal": (json.yAxis != null && json.yAxis.minValue != null) ? json.yAxis.minValue : "null",
                },
                "domainMax": {
                    "signal": (json.yAxis != null && json.yAxis.maxValue != null) ? json.yAxis.maxValue : "null",
                },
                "range": "height",
                "zero": false,
                "padding": { "signal": "height * 0.1" },
            },
        ],
        "marks": [
          {
              "name": "MASTER_GROUP",
              "type": "group",
              "encode": {
                  "update": {
                      "width": {
                          "signal": "width"
                      },
                      "height": {
                          "signal": "height"
                      }
                  }
                },
                "axes": [

                    Machinata.Reporting.Node.VegaNode.Util.applySettingsToVegaAxis(instance, config, json, {
                        "orient": "bottom",
                        "scale": "xScale",
                        "domain": false,
                        "format": {
                            "signal": (json.xAxis != null && json.xAxis.format != null) ? "\"" + json.xAxis.format + "\"" : "null",
                        },
                        "formatType": {
                            "signal": (json.xAxis != null && json.xAxis.formatType != null) ? "\"" + json.xAxis.formatType + "\"" : "null",
                        },
                        "values": {
                            "signal": (json.xAxis != null && json.xAxis.values != null) ? json.xAxis.values : "null",
                        },
                        "tickValues": {
                            "signal": (json.xAxis != null && json.xAxis.tickValues != null) ? json.xAxis.tickValues : "null",
                        },
                        "labelValues": {
                            "signal": (json.xAxis != null && json.xAxis.labelValues != null) ? json.xAxis.labelValues : "null",
                        },
                        "grid": json.showXAxisGrid,
                        //"labelBound": (json.globalXAxisLabelBound == true ? (config.padding / 2) : false), // Indicates if labels should be hidden if they exceed the axis range. If false (the default) no bounds overlap analysis is performed. If true, labels will be hidden if they exceed the axis range by more than 1 pixel. If this property is a number, it specifies the pixel tolerance: the maximum amount by which a label bounding box may exceed the axis range.
                        //"tickCount": { "signal": "data('series')[0].xAxis.tickCount" },
                    }),

                    Machinata.Reporting.Node.VegaNode.Util.applySettingsToVegaAxis(instance, config, json, {
                        "orient": "left",
                        "scale": "yScale",
                        //"tickSize": 0,
                        //"tickRound": true,
                        //"tickCount": yaxisTickCount,
                        "format": {
                            "signal": (json.yAxis != null && json.yAxis.format != null) ? "\"" + json.yAxis.format + "\"" : "null",
                        },
                        "formatType": {
                            "signal": (json.yAxis != null && json.yAxis.formatType != null) ? "\"" + json.yAxis.formatType + "\"" : "null",
                        },
                        "values": {
                            "signal": (json.yAxis != null && json.yAxis.values != null) ? json.yAxis.values : "null",
                        },
                        "tickValues": {
                            "signal": (json.yAxis != null && json.yAxis.tickValues != null) ? json.yAxis.tickValues : "null",
                        },
                        "labelValues": {
                            "signal": (json.yAxis != null && json.yAxis.labelValues != null) ? json.yAxis.labelValues : "null",
                        },
                        "grid": json.showYAxisGrid,
                        "domain": false,
                    }),
                ],
                // Here we insert all the shape marks using our helper method...
                "marks": Machinata.Reporting.Node.ShapeGraphNode.createShapeSpecsForShapes(json._state.shapes) // dynamically creates the spec as needed for each shape
          }
        ]
    };
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ShapeGraphNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    // Insert shapes
    spec["data"][0].values = json._state.shapes;
};


/// <summary>
/// </summary>
Machinata.Reporting.Node.ShapeGraphNode.createShapeDataForPoints = function (instance, json, points, opts) {
    // Init options
    if (opts == null) opts = {};
    if (opts.type == null) opts.type = "line";
    // Pre-process points
    if (points != null) {
        for (var i = 0; i < points.length; i++) {
            var p = points[i];
            // Special handling for path (SVG path string)
            if (opts.type == "path") {
                if (opts.curve == null) opts.curve = "linear";
                p.i = i;
                if (i == 0) {
                    // First
                    p.first = true;
                    p.pathOp = 'M'; // move
                } else if (i == points.length - 1) {
                    // Last
                    p.last = true;
                    if (opts.curve == "bezier") {
                        p.pathOp = 'Q'; // quadratic B�ziers
                    } else {
                        p.pathOp = 'L'; // line
                    }
                } else {
                    // Middle
                    if (opts.curve == "bezier") {
                        p.pathOp = 'Q'; // quadratic B�ziers
                    } else {
                        p.pathOp = 'L'; // line
                    }
                }
            }
        }
    }
    // Resolve colors
    var strokeResolved = null;
    if (opts.strokeColor != null) {
        strokeResolved = opts.strokeColor;
    } else if (opts.strokeColorShade != null) {
        strokeResolved = Machinata.Reporting.getThemeBGColorByShade(instance.config, json.theme, opts.strokeColorShade);
    }
    var fillResolved = null;
    // Return struct
    if (opts.fillColor != null) {
        fillResolved = opts.fillColor;
    } else if (opts.fillColorShade != null) {
        fillResolved = Machinata.Reporting.getThemeBGColorByShade(instance.config, json.theme, opts.fillColorShade);
    }
    return {
        type: opts.type,
        points: points,
        opacity: opts.opacity,
        stroke: strokeResolved,
        strokeDash: opts.strokeDash,
        strokeOpacity: opts.strokeOpacity,
        strokeWidth: opts.strokeWidth || instance.config.graphLineSize,
        fill: fillResolved,
        fillOpacity: opts.fillOpacity,
        interpolate: opts.interpolate,
        tooltip: opts.tooltip,
        legend: opts.legend,
    };
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ShapeGraphNode.createShapeSpecsForShapes = function (shapes, opts) {
    if (opts == null) opts = {};

    return [
        {
            // Facet Shapes
            "type": "group",
            "from": {
                "facet": {
                    "name": "shape",
                    "data": "shapes",
                    "groupby": "key"
                }
            },
            // Create data filters based on shape types
            "data": [
                {
                    "name": "lineShape",
                    "source": "shape",
                    "transform": [
                        { "type": "filter", "expr": "datum.type == 'line'" }
                    ]
                },
                {
                    "name": "pathShape",
                    "source": "shape",
                    "transform": [
                        { "type": "filter", "expr": "datum.type == 'path'" }
                    ]
                }
            ],
            "marks": [
                {
                    /////////////////////////////////////////////////////////////////////////////////////
                    // Line
                    "type": "group",
                    "from": { "data": "lineShape" },
                    "encode": {
                        "enter": {
                            //"tooltip": { "signal": "'asfd'" }, // DEBUG ONLY
                        },
                    },
                    "marks": [
                        {
                            "type": "group",
                            "data": [
                                {
                                    "name": "linePoints",
                                    "source": "shape",
                                    "transform": [
                                        {
                                            "type": "flatten",
                                            "fields": ["points"],
                                            "as": ["point"]
                                        },
                                        {
                                            "type": "formula",
                                            "initonly": true,
                                            "as": "points",
                                            "expr": "null"
                                        }
                                    ]
                                }
                            ],
                            "marks": [
                                {
                                    "type": "line",
                                    "from": { "data": "linePoints" },
                                    "encode": {
                                        "enter": {
                                            "interpolate": { "signal": "datum.interpolate" },
                                            "opacity": { "signal": "datum.opacity" },
                                            "fill": { "signal": "datum.fill" },
                                            "fillOpacity": { "signal": "datum.fillOpacity" },
                                            "stroke": { "signal": "datum.stroke" },
                                            "strokeDash": { "signal": "datum.strokeDash" },
                                            "strokeOpacity": { "signal": "datum.strokeOpacity" },
                                            "strokeWidth": { "signal": "datum.strokeWidth" },
                                            //"tooltip": { "signal": "'test'" }, // note tooltips don't work on lines...
                                        },
                                        "update": {
                                            "x": { "signal": "scale('xScale',datum.point.x)" },
                                            "y": { "signal": "scale('yScale',datum.point.y)" },
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    /////////////////////////////////////////////////////////////////////////////////////
                    // Path
                    // See https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths
                    // See https://vega.github.io/vega/docs/marks/path/
                    "type": "group",
                    "from": { "data": "pathShape" },
                    "marks": [
                        {
                            "type": "group",
                            "data": [
                                {
                                    "name": "pathPoints",
                                    "source": "shape",
                                    "transform": [
                                        {
                                            "type": "flatten",
                                            "fields": ["points"],
                                            "as": ["point"]
                                        },
                                        {
                                            "type": "formula",
                                            "initonly": true,
                                            "as": "points",
                                            "expr": "null"
                                        },
                                        {
                                            // Build a SVG path segment
                                            "type": "formula",
                                            "initonly": false,
                                            "as": "pathSeg",
                                            "expr": "datum.point.pathOp + ' ' + scale('xScale',datum.point.x) + ' ' + scale('yScale',datum.point.y)"
                                        },
                                    ]
                                },
                            ],
                            "signals": [
                                {
                                    "name": "svgPath",
                                    "update": "join(pluck(data('pathPoints'),'pathSeg'),' ') + ' Z'" //TODO: always close?
                                }
                            ],
                            "marks": [
                                {
                                    // Path
                                    "type": "path",
                                    "encode": {
                                        "enter": {
                                            "path": { "signal": "svgPath" },
                                            "stroke": { "signal": "'green'" },
                                            "fill": { "signal": "'red'" },
                                            "strokeWidth": { "value": 4 },
                                        },
                                        "update": {
                                            "x": { "signal": "0" },
                                            "y": { "signal": "0" },
                                        }
                                    }
                                },
                                {
                                    // Path DEBUG
                                    "type": "rect",
                                    "encode": {
                                        "enter": {
                                            "stroke": { "signal": "'green'" },
                                            "fill": { "signal": "'red'" },
                                            "strokeWidth": { "value": 2 },
                                            "tooltip": { "signal": "svgPath" }, // DEBUG ONLY
                                        },
                                        "update": {
                                            "x": { "signal": "0" },
                                            "y": { "signal": "0" },
                                            "width": { "signal": "40" },
                                            "height": { "signal": "40" },
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ];

};


/// <summary>
/// </summary>
Machinata.Reporting.Node.ShapeGraphNode.init = function (instance, config, json, nodeElem, shapes) {

    // Init
    var legendData = [];

    // Preprocess
    {
        // Preprocess axis
        Machinata.Reporting.Node.VegaNode.Util.preprocessAxisProperties(json.xAxis);
        Machinata.Reporting.Node.VegaNode.Util.preprocessAxisProperties(json.yAxis);
    }

    // Process shapes
    Machinata.Util.each(shapes, function (i, shape) {
        // Assign ids
        if (shape.key == null) shape.key = "shape" + i;
        // Legend
        if (shape.legend != null) {
            if (shape.legend.key == null) shape.legend.key = shape.key;
            legendData.push(shape.legend);
        }
    });
    json._state.shapes = shapes;

    // Generate legend data
    Machinata.Reporting.Node.VegaNode.Util.createLegendForLegendData(instance, config, json, legendData);


    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ShapeGraphNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
    Machinata.Reporting.Node.VegaNode.debugOutputDataSet(nodeElem, "allShapePoints");
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ShapeGraphNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ShapeGraphNode.exportExcelFormat = function (instance, config, json, nodeElem, format, filename) {
    //var workbook = Machinata.Reporting.Export.createExcelWorkbook(instance, config, json);
    //workbook = Machinata.Reporting.Export.createExcelDataForSeriesGroupsChart(instance, config, json, workbook, format);
    //Machinata.Reporting.Export.saveExcelWorkbook(instance, config, json, workbook, filename);
    //return true;
};








