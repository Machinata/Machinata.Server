/// <summary>
/// Config namespace. 
/// These attributes define the default behaviour of the reporting package. 
/// To change them, use a custom config object when calling init(), or provide a report data file
/// with the root ```config``` object.
/// 
/// ## Defaults
/// Each config has a predefined default value. Furthermore, each profile may have their own custom defaults (see Machinata.Reporting.Config.profile).
///
/// ## Priority (lowest to highest)
///  - Machinata.Reporting.Config default values
///  - Profile default values
///  - init() config values
///  - Report JSON ```config``` default values
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Config = {};

/// <summary>
/// Defines the source of the configuration. Each product should make sure to define their own source, as this
/// allows quick test to ensure the custom config is being loaded correctly.
/// </summary>
Machinata.Reporting.Config.configSource = "DEFAULT";

/// <summary>
/// Defines the target medium. 
/// ## Known Mediums
///  - ```screen```
///  - ```print```
/// </summary>
Machinata.Reporting.Config.medium = "screen";

/// <summary>
/// Defines the default medium content size (full-width) for the profile (in pixels).
/// For example, on medium ```web```, this should be set to the width of the container in which the report
/// is loaded. On medium ```print```, this should be set to the main content area width of the page.
/// This configuration is used to give nodes a context of how large they are in relation to the content area.
/// </summary>
Machinata.Reporting.Config.mediumContentWidthSize = 1128;

/// <summary>
/// Which user settings provider to use. Changing this setting will
/// load a different implementation of the data provider.
/// Currently supported:
///   - ```WebStorage```: uses the browsers built in web storage
/// See Machinata.Reporting.Data.UserSettings for more information.
/// </summary>
Machinata.Reporting.Config.userSettingsProvider = "WebStorage";

/// <summary>
/// Which data provider to use. Changing this setting will
/// load a different implementation of the data provider.
/// Currently supported:
///   - ```HTTP```
///   - ```JSON```
/// See Machinata.Reporting.Data.Providers for more information.
/// </summary>
Machinata.Reporting.Config.dataProvider = "HTTP";

/// <summary>
/// Defines the data source (report JSON) to use. The dataProvider will know
/// how to handle this value.
/// You can override this setting using the query parameter ```data```.
/// </summary>
Machinata.Reporting.Config.dataSource = "report.json";

/// <summary>
/// Defines the catalog source (catalog JSON) to use. The dataProvider will know
/// how to handle this value.
/// You can override this setting using the query parameter ```catalog```.
/// </summary>
Machinata.Reporting.Config.catalogSource = "catalog.json";

/// <summary>
/// If ```true```, caching will of any data via the dataSource will be prevented.
/// You can enable this feature automatically using the query string parameter ```?prevent-caching=true```.
/// </summary>
Machinata.Reporting.Config.preventDataCaching = false;

/// <summary>
/// Defines the type of profile display mode and interface to use.
/// You can override this setting using the query parameter ```profile```.
/// Can be:
///  - ```web```
///  - ```fullscreen```
///  - ```webprint```
///  - ```print```
/// </summary>
Machinata.Reporting.Config.profile = "web";

/// <summary>
/// Defines the language to use for the report content. This especially affects all 
/// ```Machinata.Reporting.Text.resolve``` usages.
/// </summary>
Machinata.Reporting.Config.contentLanguage = "en";

/// <summary>
/// Defines the language to use for the reporting interface. This especially affects all 
/// ```Machinata.Reporting.Text.translate``` usages.
/// </summary>
Machinata.Reporting.Config.interfaceLanguage = "en";

/// <summary>
/// If ```true```, hints that this reporting instance is running headless.
/// </summary>
Machinata.Reporting.Config.headless = false;

/// <summary>
/// The toc level for which to build logical screens at.
/// </summary>
/// <deprecated>
/// This is no currently unsupported. Use screen = true on each node to define a screen
/// </deprecated>
Machinata.Reporting.Config.screenLevel = 3;

/// <summary>
/// </summary>
Machinata.Reporting.Config.chapterLevel = 1;

/// <summary>
/// Defines which chapters in the report to display.
/// ## Auto
/// If set to ```auto```, then the chapters is automatically derived from the query string, 
/// and if no chapter is given, the configuration ```initialChapter``` is used.
/// ## *
/// If set to ```*```, then all the chapters are displayed.
/// </summary>
Machinata.Reporting.Config.chapters = "auto";

/// <summary>
/// </summary>
Machinata.Reporting.Config.initialChapter = null;

/// <summary>
/// If set, defines which logical screens to show.
///
/// If set to ```auto``` (the default value), allows
/// query parameters and other behaviour to control the screens automatically.
///
/// If set to ```printcomposer```, the screens are automatically populated using the data
/// from the print composer for the currently loaded report.
///
/// If set to an array of integers, only those screen numbers are shown in the report.
///
/// Note: when a report is built, section titles are automatically included regardless whether they
/// are part of the screens or not. This ensures a consistent content navigation tree for the user.
/// </summary>
Machinata.Reporting.Config.screens = "auto";

/// <summary>
/// If true, the report contents will be built and shown.
/// </summary>
Machinata.Reporting.Config.buildContents = true;

/// <summary>
/// </summary>
Machinata.Reporting.Config.headerSelector = "#header";

/// <summary>
/// </summary>
Machinata.Reporting.Config.navigationSelector = "#navigation";

/// <summary>
/// </summary>
Machinata.Reporting.Config.contentSelector = ".machinata-reporting-report";

/// <summary>
/// </summary>
Machinata.Reporting.Config.chapterSelector = ".machinata-reporting-chapters";

/// <summary>
/// </summary>
Machinata.Reporting.Config.catalogSelector = ".machinata-reporting-catalog";

/// <summary>
/// </summary>
Machinata.Reporting.Config.printComposerSelector = ".machinata-printcomposer";

/// <summary>
/// </summary>
Machinata.Reporting.Config.catalogItemMandateTemplateSelector = ".machinata-reporting-catalog-mandate-template";

/// <summary>
/// </summary>
Machinata.Reporting.Config.catalogItemReportTemplateSelector = ".machinata-reporting-catalog-report-template";

/// <summary>
/// </summary>
Machinata.Reporting.Config.catalogDateFormat = "%Y-%m-%d";

/// <summary>
/// </summary>
Machinata.Reporting.Config.catalogDateDisplayFormat = "%d.%m.%Y";

/// <summary>
/// If not null, the expanded catalog view that lists all reports is limited to this number.
/// </summary>
Machinata.Reporting.Config.catalogMaxReportsToShow = 30;

/// <summary>
/// </summary>
Machinata.Reporting.Config.navigationMaxSiblings = 8;

/// <summary>
/// </summary>
Machinata.Reporting.Config.automaticallyAddPageTools = true;

/// <summary>
/// </summary>
Machinata.Reporting.Config.automaticallyGenerateScreenNumbers = false;

/// <summary>
/// </summary>
Machinata.Reporting.Config.automaticallyFoldChaptersIntoPages = true;

/// <summary>
/// </summary>
Machinata.Reporting.Config.automaticallyFoldChaptersIntoAccordion = false;

/// <summary>
/// </summary>
Machinata.Reporting.Config.automaticallyInsertCatalogNavigation = true;

/// <summary>
/// </summary>
Machinata.Reporting.Config.automaticallyInsertChapterNavigation = true;

/// <summary>
/// </summary>
Machinata.Reporting.Config.automaticallyBuildCatalogUI = true;

/// <summary>
/// </summary>
Machinata.Reporting.Config.loadReport = true;

/// <summary>
/// </summary>
Machinata.Reporting.Config.loadCatalog = true;

/// <summary>
/// The report URL. If this is not set, the URL is automatically derived from the browser window.
/// </summary>
Machinata.Reporting.Config.url = null;

/// <summary>
/// </summary>
Machinata.Reporting.Config.reportURL = null;

/// <summary>
/// </summary>
Machinata.Reporting.Config.catalogURL = null;

/// <summary>
/// The report fullscreen URL. ?screen=N is automatically appended if used.
/// </summary>
Machinata.Reporting.Config.fullscreenURL = null;

/// <summary>
/// A resolved report name (string). Is read out from the report JSON file if not defined.
/// TODO: @bmpi: this should be explicitly present in every report JSON file.
/// </summary>
Machinata.Reporting.Config.reportName = null;

/// <summary>
/// A resolved report title (string). Is read out from the report JSON file if not defined, or if present, from the
/// catalog JSON if not defined.
/// TODO: @bmpi: this should be explicitly present in every report JSON file.
/// </summary>
Machinata.Reporting.Config.reportTitle = null;

/// <summary>
/// A resolved report title (string). This can be, for example, the reporting date.
/// TODO: @bmpi: this should be explicitly present in every report JSON file.
/// </summary>
Machinata.Reporting.Config.reportSubtitle = null;

/// <summary>
/// A resolved chapter title (string). Is automatically set if a single chapter has been selected and not defined.
/// This is a helper variable so that it's easy to access this information at any time.
/// </summary>
Machinata.Reporting.Config.reportChapter = null;

/// <summary>
/// A resolved report disclaimer text (string). Is automatically loaded through a tag, if present and not defined.
/// See Machinata.Reporting.Config.tagDisclaimer
/// </summary>
Machinata.Reporting.Config.reportDisclaimer = null;

/// <summary>
/// A resolved report legal entity text (string). Is automatically loaded through a tag, if present and not defined.
/// See Machinata.Reporting.Config.tagLegalEntity
/// </summary>
Machinata.Reporting.Config.reportLegalEntity = null;

/// <summary>
/// The standard padding dimension, in pixels.
/// </summary>
Machinata.Reporting.Config.padding = 8;

/// <summary>
/// The default font size in pixels. This is the default size used the element doesn't specifiy a more specific.
/// </summary>
Machinata.Reporting.Config.textSize = 12;

/// <summary>
/// The default line height factor to use.
/// </summary>
Machinata.Reporting.Config.lineHeight = 1.2;

/// <summary>
/// The font size in pixels for charts. This is the default size used the element doesn't specifiy a more specific.
/// </summary>
Machinata.Reporting.Config.chartTextSize = 14;

/// <summary>
/// Defines a global padding for charts. Typically this should be 0.
/// Can also be a object in the format of {"left": 5, "top": 5, "right": 5, "bottom": 5}
/// </summary>
Machinata.Reporting.Config.chartPadding = 0;

/// <summary>
/// </summary>
Machinata.Reporting.Config.chartBackgroundColorSolidChrome = "gray";

/// <summary>
/// </summary>
Machinata.Reporting.Config.chartBackgroundColorDarkChrome = "black";

/// <summary>
/// </summary>
Machinata.Reporting.Config.chartBackgroundColorLightChrome = "white";

/// <summary>
/// </summary>
Machinata.Reporting.Config.chartTextColorSolidChrome = "black";

/// <summary>
/// </summary>
Machinata.Reporting.Config.chartTextColorDarkChrome = "white";

/// <summary>
/// </summary>
Machinata.Reporting.Config.chartTextColorLightChrome = "black";

/// <summary>
/// The default font to use for charts.
/// </summary>
Machinata.Reporting.Config.textFont = "sans-serif";

/// <summary>
/// An approximate font-size to character width ratio used for very fast (and innaccurate) text string measurements.
/// See ```Machinata.Reporting.Tools.measureStringApproximately()```
/// </summary>
Machinata.Reporting.Config.fontSizeToCharacterWidthRatio = 0.65;

/// <summary>
/// The font size in pixels for chart labels.
/// </summary>
Machinata.Reporting.Config.labelSize = 12;

/// <summary>
/// The font name for chart labels.
/// </summary>
Machinata.Reporting.Config.labelFont = "sans-serif";

/// <summary>
/// The font size in pixels for chart axis labels.
/// </summary>
Machinata.Reporting.Config.axisSize = 12;

/// <summary>
/// The font name for chart axis labels.
/// </summary>
Machinata.Reporting.Config.axisFont = "sans-serif";

/// <summary>
/// </summary>
Machinata.Reporting.Config.axisLabelFontWeight = "normal";

/// <summary>
/// The strategy to use for resolving overlap of axis labels. 
/// If```false```, no overlap reduction is attempted.If set to```true``` or```parity```, a strategy of removing every other label is used(this works well for standard linear axes).If set to```greedy```, a linear scan of the labels is performed, removing any label that overlaps with the last visible label(this often works better for log - scaled axes).
/// By default ```parity```
/// </summary>
Machinata.Reporting.Config.axisLabelOverlap = "parity";

/// <summary>
/// The minimum separation that must be between label bounding boxes for them to be considered non-overlapping.
/// By default ```Machinata.Reporting.Config.padding / 2```
/// </summary>
Machinata.Reporting.Config.axisLabelSeparation = Machinata.Reporting.Config.padding / 2;


/// <summary>
/// The padding in pixels between labels and ticks.
/// By default ```Machinata.Reporting.Config.padding / 2```
/// </summary>
Machinata.Reporting.Config.axisLabelPadding = Machinata.Reporting.Config.padding / 2;

/// <summary>
/// The maximum allowed length in pixels of axis tick labels.
/// By default ```null``` (off)
/// </summary>
Machinata.Reporting.Config.axisLabelLimit = null;

/// <summary>
/// This defines a target number of ticks
/// for all value-axis's (typically the vertical axis). 
/// It is not gauranteed that axis's have exactly said number of ticks.
/// The closest human-friendly tick step and tick count will be used.
/// By default ```10```
/// </summary>
Machinata.Reporting.Config.axisTargetTickCount = 11;

/// <summary>
/// If ```true```, buttom axis's will show ticks next to labels.
/// By default ```false```
/// </summary>
Machinata.Reporting.Config.axisBottomShowTicks = true;

/// <summary>
/// If ```axisBottomShowTicks``` is enabled, defines the buttom axis's tick size.
/// By default ```20```
/// </summary>
Machinata.Reporting.Config.axisBottomTickSize = 20;

/// <summary>
/// If set, buttom axis's will have a slight offset (in pixels).
/// Only applies if ```axisBottomShowTicks``` is false.
/// By default ```2```
/// </summary>
Machinata.Reporting.Config.axisBottomOffset = 2;

/// <summary>
/// The font size in pixels for chart legends.
/// </summary>
Machinata.Reporting.Config.legendSize = 12;

/// <summary>
/// The font name for chart legends.
/// </summary>
Machinata.Reporting.Config.legendFont = "sans-serif";

/// <summary>
/// The size of the legend symbols, in pixels.
/// </summary>
Machinata.Reporting.Config.legendSymbolSize = 14;

/// <summary>
/// The stroke width of the legend symbols, in pixels.
/// Only lines have strokes.
/// </summary>
Machinata.Reporting.Config.legendSymbolStroke = 2;

/// <summary>
/// The legend box padding, in pixels.
/// </summary>
Machinata.Reporting.Config.legendPadding = 0;

/// <summary>
/// The horizontal padding between legend columns, in pixels.
/// </summary>
Machinata.Reporting.Config.legendColumnPadding = 12;

/// <summary>
/// The vertical padding between legend rows, in pixels.
/// </summary>
Machinata.Reporting.Config.legendRowPadding = 8;

/// <summary>
/// The maximum width a legend label can have, in pixels.
/// </summary>
Machinata.Reporting.Config.legendLabelMaxWidth = 300;

/// <summary>
/// The label offset in relation to it's symbol, in pixels.
/// </summary>
Machinata.Reporting.Config.legendLabelOffsetY = 0;

/// <summary>
/// For nodes that support legends, this property defines the position of the node legend.
/// Supported positions:
///  - ```left-top```: Place the legend to the top-left of the chart, in the top corner.
///  - ```right-top```: Place the legend to the right of the chart, in the top corner.
///  - ```top-left```: Place the legend above the top of the chart, on the left side.
///  - ```bottom-left```: Place the legend below the bottom of the chart, on the left side.
/// By default ```bottom-left``.
/// </summary>
Machinata.Reporting.Config.legendPosition = "bottom-left";

/// <summary>
/// Defines the type of symbol shape to use for the given symbol.
/// Symbols within chart data must be defined as one of the following:
///  - ```dot```
///  - ```diamond```
///  - ```bar```
///  - ```line```
/// Configuration value can be any symbol shape defined by vega (see https://vega.github.io/vega/docs/marks/symbol/).
/// By default ```circle``.
/// </summary>
Machinata.Reporting.Config.legendSymbolForDot = "circle";

/// <summary>
/// Defines the type of symbol shape to use for the given symbol.
/// Symbols within chart data must be defined as one of the following:
///  - ```dot```
///  - ```diamond```
///  - ```bar```
///  - ```line```
/// Configuration value can be any symbol shape defined by vega (see https://vega.github.io/vega/docs/marks/symbol/).
/// By default ```diamond``.
/// </summary>
Machinata.Reporting.Config.legendSymbolForDiamond = "diamond";

/// <summary>
/// Defines the type of symbol shape to use for the given symbol.
/// Symbols within chart data must be defined as one of the following:
///  - ```dot```
///  - ```diamond```
///  - ```bar```
///  - ```line```
/// Configuration value can be any symbol shape defined by vega (see https://vega.github.io/vega/docs/marks/symbol/).
/// By default ```square``.
/// </summary>
Machinata.Reporting.Config.legendSymbolForBar = "square";

/// <summary>
/// Defines the type of symbol shape to use for the given symbol.
/// Symbols within chart data must be defined as one of the following:
///  - ```dot```
///  - ```diamond```
///  - ```bar```
///  - ```line```
/// Configuration value can be any symbol shape defined by vega (see https://vega.github.io/vega/docs/marks/symbol/).
/// By default ```stroke``.
/// </summary>
Machinata.Reporting.Config.legendSymbolForLine = "stroke";

/// <summary>
/// Defines the type of symbol shape to use for a unknown symbol.
/// Symbols within chart data must be defined as one of the following:
///  - ```dot```
///  - ```diamond```
///  - ```bar```
///  - ```line```
/// Configuration value can be any symbol shape defined by vega (see https://vega.github.io/vega/docs/marks/symbol/).
/// By default ```square``.
/// </summary>
Machinata.Reporting.Config.legendSymbolForUnknown = "square";

/// <summary>
/// Defines the legend offset (spacing) from the chart elements or graph.
/// By default ```18``.
/// </summary>
Machinata.Reporting.Config.legendOffset = 18;

/// <summary>
/// The font size in pixels for chart titles. Should be same as h1 size.
/// </summary>
Machinata.Reporting.Config.titleSize = 60;

/// <summary>
/// The font size in pixels for chart subtitles.
/// </summary>
Machinata.Reporting.Config.subtitleSize = 18;

/// <summary>
/// The font name chart subtitles.
/// </summary>
Machinata.Reporting.Config.subtitleFont = "sans-serif";

/// <summary>
/// The font name for chart numbers.
/// </summary>
Machinata.Reporting.Config.numberFont = "sans-serif";

/// <summary>
/// The grid color for charts (RGB/Hex).
/// </summary>
Machinata.Reporting.Config.gridColor = "#7c7c7b"; //gray5

/// <summary>
/// The solid color (object fill color) for charts (RGB/Hex).
/// </summary>
Machinata.Reporting.Config.solidColor = "#f1f2f2"; //gray1

/// <summary>
/// </summary>
Machinata.Reporting.Config.darkColor = "black";

/// <summary>
/// </summary>
Machinata.Reporting.Config.pageColor = "white";

/// <summary>
/// The general stroke thickness (in pixel) for a line.
/// </summary>
Machinata.Reporting.Config.lineSize = 1;

/// <summary>
/// The grid stroke thickness (in pixel) for a grid line.
/// </summary>
Machinata.Reporting.Config.gridLineSize = 1;

/// <summary>
/// The stroke thickness (in pixel) for a graph line (ie a line-chart line).
/// </summary>
Machinata.Reporting.Config.graphLineSize = 4;

/// <summary>
/// The width and height size (bounding box) for scatterplot symbols, in pixels, for a graph symbol.
/// </summary>
Machinata.Reporting.Config.graphSymbolSize = 16;

/// <summary>
/// The general stroke thickness (in pixel) for a domain (axis, zero) line.
/// </summary>
Machinata.Reporting.Config.domainLineSize = 2;

/// <summary>
/// The general color for a domain (axis, zero) line.
/// </summary>
Machinata.Reporting.Config.domainLineColor = "black";

/// <summary>
/// Defines which color shades represent negative numbers and positive numbers.
/// Array must contain exactly 2 values, the first being the negative shade and the second
/// being the positive shade name.
/// </summary>
/// <example>
/// ```
/// ['dark', 'bright']
///    ^         ^
///  negative    | 
///            positive
/// ```
/// </example>
Machinata.Reporting.Config.themeNegativePositiveShadeNames = ['dark', 'bright'];

/// <summary>
/// Similar to themeNegativePositiveShadeNames, but an alternative neutral color scheme for negative and positive.
/// </summary>
/// <example>
/// ```
/// ['dark', 'bright']
///    ^         ^
///  negative    | 
///            positive
/// ```
/// </example>
Machinata.Reporting.Config.themeNegativePositiveGrayShadeNames = ['gray4', 'gray3'];




/// <summary>
/// Defines the tag name to use to automatically extract the report disclaimer from a content node 
/// (such as a ParagraphNode).
/// Note: The node with this tag is excluded from the report.
/// </summary>
Machinata.Reporting.Config.tagDisclaimer = "Report_Disclaimer";

/// <summary>
/// Defines the tag name to use to automatically extract the report legal entity from a content node 
/// (such as a ParagraphNode).
/// Note: The node with this tag is excluded from the report.
/// </summary>
Machinata.Reporting.Config.tagLegalEntity = "Report_LegalEntity";

/// <summary>
/// Defines the tag name to use to automatically mark a node as a screen.
/// This is an alternate to setting the node's ```screen``` to ```true```.
/// </summary>
Machinata.Reporting.Config.tagScreen = "Screen";

/// <summary>
/// Defines the tag name to use for an alternative to setting a nodes chrome to ```dark```.
/// </summary>
Machinata.Reporting.Config.tagDarkChrome = "DarkChrome";

/// <summary>
/// Defines the tag name to use for an alternative to setting a nodes chrome to ```light```.
/// </summary>
Machinata.Reporting.Config.tagLightChrome = "LightChrome";

/// <summary>
/// Defines the tag name to use for an alternative to setting a nodes chrome to ```solid```.
/// </summary>
Machinata.Reporting.Config.tagSolidChrome = "SolidChrome";

/// <summary>
/// Defines the tag to use to allow a node to only appear on the asserted profile.
/// </summary>
Machinata.Reporting.Config.tagWebPrintOnly = "WebPrintOnly";

/// <summary>
/// Defines the tag to use to allow a node to only appear on the asserted profile.
/// </summary>
Machinata.Reporting.Config.tagWebOnly = "WebOnly";

/// <summary>
/// Defines the tag to use to allow a node to only appear on the asserted profile.
/// </summary>
Machinata.Reporting.Config.tagPrintOnly = "PrintOnly";

/// <summary>
/// Defines the tag to use to allow a node to only appear on the asserted profile.
/// </summary>
Machinata.Reporting.Config.tagFullscreenOnly = "FullscreenOnly";

/// <summary>
/// If defined, sets the default locale definition object for number formatting. 
/// This configuration is used for Vega and D3 number formatting, the formatting framework used. 
/// See https://github.com/d3/d3-format#formatDefaultLocale
/// See https://github.com/d3/d3-format/tree/master/locale
/// </summary>
/// <example>
/// ```
/// {
///     "decimal": ".",
///     "thousands": ",",
///     "grouping": [3],
///     "currency": ["$", ""]
/// }
/// ```
/// </example>
Machinata.Reporting.Config.numberFormatLocaleDefinition = null;

/// <summary>
/// If defined, sets the default locale definition object for time formatting. 
/// This configuration is used for Vega and D3 time formatting, the formatting framework used. 
/// See https://github.com/d3/d3-time-format#timeFormatDefaultLocale
/// See https://github.com/d3/d3-time-format/tree/master/locale
/// </summary>
/// <example>
/// ```
/// {
///     "dateTime": "%x, %X",
///     "date": "%-m/%-d/%Y",
///     "time": "%-I:%M:%S %p",
///     "periods": ["AM", "PM"],
///     "days": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
///     "shortDays": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
///     "months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
///     "shortMonths": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
/// }
/// ```
/// </example>
Machinata.Reporting.Config.timeFormatLocaleDefinition = null;

/// <summary>
/// If ```true```, table sorting on the column headers will be default allow a reset on a third click.
/// </summary>
Machinata.Reporting.Config.allowTableSortResets = true;


/// <summary>
/// If ```true```, calibration marks will be automatically added to all charts.
/// </summary>
Machinata.Reporting.Config.calibrationMarks = false;

/// <summary>
/// Defines the units to show on the calibration marks. Can be either ```px```, ```mm``` or ```in```.
/// </summary>
Machinata.Reporting.Config.calibrationUnits = "px";

/// <summary>
/// Defines the logical layouting DPI for the current medium. This means that this value should match
/// the drawing points DPI of the underlying drawing device or platform. For example, on an iPhone the
/// this value would be ```96``` (1x resolution), even though the devices display has a much higher
/// resolution. This is because the OS's drawing APIs are all aligned on points, regardless of the display
/// resolution.
/// In most circumstances this value will always be ```96```.
/// </summary>
Machinata.Reporting.Config.layoutingDPI = 96;

/// <summary>
/// If ```true```, drawing routines should try to round a full pixel before calling a drawing call. This
/// can be important if drawing on screen devices with low pixel density.
/// </summary>
Machinata.Reporting.Config.roundToNearestFullPixel = true;

/// <summary>
/// If ```true```, the report subtitle will be inserted as a text page-tool using the addPageTool handler.
/// </summary>
Machinata.Reporting.Config.insertReportSubtitleAsPageTool = true;

/// <summary>
/// Defines the max width for vertical bars in charts (in pixels).
/// </summary>
Machinata.Reporting.Config.verticalBarMaxSize = 22;

/// <summary>
/// Defines the max width for horizontal bars in charts (in pixels).
/// </summary>
Machinata.Reporting.Config.horizontalBarMaxSize = 22;

/// <summary>
/// Defines the default text truncation ellipsis.
/// Usually always ```...```
/// </summary>
Machinata.Reporting.Config.textTruncationEllipsis = "...";

/// <summary>
/// Defines the default text trailing info symbol, typically used to expose a underlying additional information.
/// Can be any valid chart font character or text, such as ```" \\uD83D\\uDEC8"``` (i) or ```" \\u24D8"``` (i).
/// To turn off this feature, you can set the symbol to ```''``` (empty string).
/// By default ```''``` (empty string)
/// </summary>
/// <deprecated/>
Machinata.Reporting.Config.textInfoSymbol = '';

/// <summary>
/// Defines the window title to set, if not null.
/// Uses ```Machinata.Reporting.Tools.createTitleString```, so you can use all the variables
/// aviable there.
/// </summary>
Machinata.Reporting.Config.reportWindowTitle = null;

/// <summary>
/// </summary>
Machinata.Reporting.Config.catalogWindowTitle = null;

/// <summary>
/// Defines the path separator to use when concatenating paths with files.
/// By default this is ```/```.
/// </summary>
Machinata.Reporting.Config.pathSeparator = "/";

/// <summary>
/// Defines the assets path root folder.
/// This is used to determine full or absolute paths to certain resources needed, specifically configuration values that 
/// support the path variables in them.
/// The typical layout for a assets folder for a reporting configuration will have the following structure:
/// - ```/[your-assets-path]/```
///   - ```css/```: CSS bundles needed specific to the configuration/customer
///   - ```fonts/```: font files needed specific to the configuration/customer
///   - ```images/```: image files needed specific to the configuration/customer
///   - ```js/```: frontend JS needed specific to the configuration/customer
///   - ```svg/```: SVG icon bundles needed specific to the configuration/customer
/// </summary>
Machinata.Reporting.Config.assetsPath = "/";

/// <summary>
/// Defines the location of the icons bundle. Must be absolute path, or relative to the document loading the report.
/// Typically this is defined by theme or customer configuration, and doesn't need to be set.
/// Supported variables:
///  - ```{assets-path}```
/// </summary>
/// <example>
/// ```{assets-path}/svg/csam-reporting-icons-bundle.svg```
/// </example>
Machinata.Reporting.Config.iconsBundle = null;

/// <summary>
/// </summary>
Machinata.Reporting.Config.iconNameToBundleIdMapping = {
    "alert": "error",
    "info": "info",
    "close": "delete",
    "trend-up": "up",
    "trend-down": "down-arrow",
    "fullscreen": "full-screen",
    "print": "print",
    "check": "checkmark",
    "copy": "copy-to-clipboard",
    "list": "bulleted-list",
    "filter": "filter",
    "search": "search",
    "report": "pie-chart-report",
    "export": "download",
    "download": "download",
    "arrow-left": "left",
    "arrow-right": "right",
    "configure": "tune",
    "cross": "delete",
    "glossary": "info",
    "calendar": "calendar",
    "chart": "pie-chart",
    "share": "connect",
    "expand": "forward",
    "collapse": "expand-arrow",
    "chevron-up": "collapse-arrow",
    "chevron-down": "expand-arrow",
    "chevron-left": "back",
    "chevron-right": "forward",
    "ask-question": "ask-question",
    "add-to-dashboard": "template",
    "add-to-presentation": "presentation",
    "debug": "maintenance",
    "more": "more",
    "studio": "wallpaper",
    "star": "star",
};

/// <summary>
/// </summary>
Machinata.Reporting.Config.injectThemeCSSIntoPage = true;


/// <summary>
/// </summary>
Machinata.Reporting.Config.askAQuestionViaEmailDefaultEmail = "support@nerves.ch";
Machinata.Reporting.Config.askAQuestionViaEmailDefaultName = "Nerves Support";
Machinata.Reporting.Config.askAQuestionViaEmailDefaultSubject = "Question regarding {report.title}, {report.subtitle}, {node.title}, {node.subtitle}";
Machinata.Reporting.Config.askAQuestionViaEmailDefaultBody = "Dear {to.name}, I have a question regarding the following:\n\nReport: {report.title}, {report.subtitle}\nURL: {report.url}\n\nSection: {node.title}, {node.subtitle}\nURL: {node.url}";

/// <summary>
/// Defines a branding configuration to load when first initializing a profiled configuration.
/// You can register branding configurations �sing ```Machinata.Reporting.Branding.registerBrandingConfiguration```
/// </summary>
Machinata.Reporting.Config.brandingConfiguration = null;

/// <summary>
/// If set, defines the report layout minimum width.
/// Typically this is done by the container (the report takes the full width), or CSS.
/// </summary>
Machinata.Reporting.Config.layoutMinWidth = null;

/// <summary>
/// If set, defines the report layout maximum width.
/// Typically this is done by the container (the report takes the full width), or CSS.
/// </summary>
Machinata.Reporting.Config.layoutMaxWidth = null;

/// <summary>
/// If true, reports will have their layout min/max dimensions automatically set
/// via the configurations ```layoutMinWidth``` and  ```layoutMaxWidth```.
/// </summary>
Machinata.Reporting.Config.automaticallySetLayoutMinMaxDimensions = false;

/// <summary>
/// If true, the current report layout name (ie tablet/mobile/desktop) will automatically be set on the 
/// body of the page.
/// </summary>
Machinata.Reporting.Config.automaticallySetReportLayoutClassOnBody = false;

/// <summary>
/// If true, the current page layout name (ie tablet/mobile/desktop) will automatically be set on the 
/// body of the page.
/// </summary>
Machinata.Reporting.Config.automaticallySetPageLayoutClassOnBody = false;

/// <summary>
/// If true, a browser print dialog will automatically be opened on init of a report.
/// Requires the profile to be set to Machinata.Reporting.Profiles.PROFILE_WEBPRINT_KEY
/// </summary>
Machinata.Reporting.Config.automaticallyOpenBrowserPrintDialog = false;

/// <summary>
/// If true, additional support for IE11 will be injected into the page.
/// </summary>
Machinata.Reporting.Config.compatibilityIE11Support = true;

/// <summary>
/// Defines the minimum width of a node before it begins to compress (this is derived from two-column layout node at the lax report size (layoutMaxWidth))
/// </summary>
Machinata.Reporting.Config.nodeSizeMinWidthUncompressed = 580;



/// <summary>
/// Defines the size factor which can be used for title sizes on mobile layouts.
/// </summary>
Machinata.Reporting.Config.mobileTitleSizeFactor = 0.6;

/// <summary>
/// Defines the size factor which can be used for subtitle sizes on mobile layouts.
/// </summary>
Machinata.Reporting.Config.mobileSubtitleSizeFactor = 0.8;

/// <summary>
/// Defines the size factor which can be used for label sizes on mobile layouts.
/// </summary>
Machinata.Reporting.Config.mobileLabelSizeFactor = 0.9;

/// <summary>
/// If ```true```, CSP compliance is achieved via various methods such as using a custom vega expression parser compatible with 
/// all vega customizations in the VegaNode namespace.
/// Setting this mode to true can degrade performance and will have an impact on render times for charts.
/// By default ```false```.
/// 
/// Note that to ensure full CSP compliance for the entire Machinata some core features should be disabled by setting
/// appropriate Machinata flags before the document is ready:
/// ```
/// // Disable some core features
/// Machinata.DISABLE_CORE_RESPONSIVE = true;
/// Machinata.DISABLE_CORE_UI = true;
/// ```
/// </summary>
Machinata.Reporting.Config.cspCompliance = false;

/// <summary>
/// Defines the default dialog size for info nodes.
/// By default ```520```.
/// </summary>
Machinata.Reporting.Config.infoDialogDefaultDesiredWidth = 520;

/// <summary>
/// Defines the wide dialog size for info nodes when wideDialog is set to true.
/// By default ```520```.
/// </summary>
Machinata.Reporting.Config.infoDialogWideDesiredWidth = 900;

/// <summary>
/// Defines the amount of delay to enforce to allow the browser to update the loading
/// state UI, in milliseconds.
/// Since browsers dont have an official API to enforce a specific UI update before doing
/// some heavy lifting on the main thread, a timeout is used with a delay. Typically at least 25+ ms
/// is needed for the browser to do a UI pending update.
/// </summary>
Machinata.Reporting.Config.loadingUITimeoutDelay = 50;

/// <summary>
/// If true, certain configuration parameters can be controlled via the query string.
/// Known configuration parameters are:
///  - ```profile```: Machinata.Reporting.Config.profile
///  - ```prevent-caching```: Machinata.Reporting.Config.preventDataCaching
///  - ```focus```: instance.focusKey
///  - ```screen```: Machinata.Reporting.Config.screens
///  - ```catalog```: Machinata.Reporting.Config.catalogSource
///  - ```data```: Machinata.Reporting.Config.dataSource
///  - ```chapter```: Machinata.Reporting.Config.chapters
///  - ```print-dialog```: Machinata.Reporting.Config.automaticallyOpenBrowserPrintDialog
/// Other flags when this feature is enabled:
///  - ```stats```: show report loading times stats 
///  - ```debug```: enable debug output
/// When using the catalog UI, the following parameters are supported:
///  - ```report```: if set defines the current report id, used to automatically select a report from a catalog
///  - ```reports```: if true shows all the reports 
///  - ```mandate```: if set, defines a mandate filter 
/// </summary>
Machinata.Reporting.Config.allowConfigurationParametersViaQueryString = false;

/// <summary>
/// If true, indicates that the current report is part of a headless testing scenario.
/// Some features will be automatically disabled to better support automated tests.
/// </summary>
Machinata.Reporting.Config.automatedTesting = false;

/// <summary>
/// If true, SVG resources (such as images) will be automatically inlined on export
/// or headless rendering. This is useful to ensure that SVG assets produced by the reporting
/// are self-contained.
/// For currently supported inlined SVG resources see ```Machinata.Reporting.SVG.inlineSVGResourcesInSVG```
/// By default ```true```.
/// </summary>
Machinata.Reporting.Config.automaticallyInlineSVGResourcesOnExport = true;

/// <summary>
/// If true, unsupported characters such as soft-hyphens are removed/replaced on export.
/// The following replacements are undertaken:
///  - Machinata.SOFT_HYPHEN: replaced with '' (remaining soft-hyphens can safely be removed if they were never used, if they were used to line-break/word-break then the soft-hyphen has already been converted to a fixed hyphen)
/// By default ```true```.
/// </summary>
Machinata.Reporting.Config.automaticallyReplaceSVGUnsupportedCharactersOnExport = true;

/// <summary>
/// Defines the default encoding to use for file and IO operations.
/// </summary>
Machinata.Reporting.Config.defaultFileEncoding = "utf8";