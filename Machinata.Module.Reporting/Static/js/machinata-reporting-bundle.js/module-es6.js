
/// <summary>
/// Provides ES6 module loading support
/// </summary>
if (typeof Machinata === "undefined") var Machinata = ((typeof global !== 'undefined') ? global : window).MACHINATA;

Machinata.Reporting = {};
export default Machinata.Reporting;
