/// <summary>
/// Utilities to export Charts and Tables to XLSX and CSV format.
///
/// Supported chart types:
/// - DonutNode
/// - PieNode
/// - HBarNode
/// - VBarNode
/// - SymboledVBarNode
/// - CoreSatelliteNode
/// - MultiGaugeNode
/// - OfflineMapNode
/// - VerticalTableNode
/// - PivotTableNode
/// - LineColumnNode
/// - ScatterPlotNode
/// - StackedAreaNode
/// Behaviour if Value type is set (CategoryFact, Tables.Fact, etc):
/// if the facts have a ValueType defined the ```fact.value``` will be exported as a number, text or date using the defined excel format and multiplied by the factor(if defined).
/// Behaviour if no ValueType set:
/// if the ValueType is null/undefined the ```fact.value``` is exported without any formatting.
///
///
/// Note:
/// Excel only supports 15 significant figures, see: https://en.wikipedia.org/wiki/Numeric_precision_in_Microsoft_Excel
/// 
/// Current mappings:
/// supported value types and their mappings and factor 
/// Note: thousands separators , will be changed to ’ in excel (based on localization settings of excel?)
///
/// | valueType | value               | Excel format      | Factor   | Excel Type |
/// | --------- | ------------------- | -----------       | -------  | -----------|
/// | text      | This is sample text |                   |          | text       |
/// | date	    | 31/03/2019	      | dd.mm.yyyy        |          | date       |
/// | year	    | 31/03/2019     	  | yyyy              |          | date       |
/// | month     | 31/03/2019	      | mm                |          | date       |
/// | percent   | 0.02305             | 0.00%             |          | number     |
/// | p-abs	    | 0.02305	          | 0.00	          | 100      | number     |
/// | p-rel	    | 0.00412	          | 0.00	          | 10000    | number     |
/// | bp	    | 0.00412	          | 0.0               | 10000    | number     |
/// | amount	| 1234567.89	      | #,##0.00          | 1        | number     |
/// | count	    | 1234567.89	      | 0                 | 1        | number     |
/// | mv	    | 1234567.89	      | #,##0	          | 1        | number     |
/// | mv_t	    | 1234567.89	      | #,##0	          | 0.001    | number     |
/// | mv_m	    | 1234567.89	      | #,##0	          | 0.000001 | number     |
/// | mv_m2d	| 1234567.89	      | #,##0.00          | 0.000001 | number     |
/// | pl	    | 1234567.89	      | #,##0.00          | 1        | number     |
/// | fxrate	| 1.0345665	          | #,##0.0000        | 1        | number     |
///
/// ```
/// var workbook = Machinata.Reporting.Export.createExcelWorkbook(instance, config, json);
/// workbook = Machinata.Reporting.Export.createExcelDataForCategoryChart(instance, config, json, workbook, format);
/// Machinata.Reporting.Export.saveExcelWorkbook(instance, config, json, workbook, filename);
/// ```
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Export = {};


/// <summary>
/// Creates a workbook, can be used for e.g. xlsx or csv exports
/// Sets the title from the reports mainTitle
/// </summary>
Machinata.Reporting.Export.createExcelWorkbook = function (instance, config, json) {
    // create a new blank workbook
    var workbook = XLSX.utils.book_new();

    if (!workbook.Props) workbook.Props = {};
    workbook.Props.Title = json.mainTitle.resolved;

    return workbook;

};

/// <summary>
/// helper method: Adds a text/string value to a specified cell (address)
/// </summary>
Machinata.Reporting.Export.addStringCell = function (sheet, address, value) {
    sheet[address] = { v: value, t: "s" };
};

/// <summary>
/// helper method: Adds a number value to a specified cell (address)
/// </summary>
Machinata.Reporting.Export.addNumberCell = function (sheet, address, value, formatString, format) {
    
    if (formatString != null && formatString != undefined && format != "csv") {
        sheet[address] = { v: value, t: "n", z: formatString };
    }
    else {
        sheet[address] = { v: value, t: "n" };
    }
};

/// <summary>
/// helper methods: Adds a date value to a specified cell (address)
/// </summary>
Machinata.Reporting.Export.addDateCell = function (sheet, address, value, formatString, format) {

    if (format != "csv") {
        sheet[address] = { v: value, t: "d", z: formatString };
    }
    else {
        sheet[address] = { v: value, t: "d" };
    }
};


/// <summary>
/// helper method: Adds a value without defining a type (let excel decide)
/// </summary>
Machinata.Reporting.Export.addGenericCell = function (sheet, address, value, format) {
    if (format == "csv") {
        // We have to assume t == "s" for csv otherwise it exports empty values
        sheet[address] = { v: value, t: "s" };
    } else {
        sheet[address] = { v: value };
    }
};



/// <summary>
/// Adds a value to specific cell in the sheet
/// each ValueType has to be mapped to a number format supported by excel 
/// The 'resolved' values cannot be used because there is no 'known' way the set the value and a custom display value other than using the number format string
/// https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_api_src_model_values_.valuetype.html#date
/// https://customformats.com/
/// </summary>
Machinata.Reporting.Export.addTableCell = function (sheet, address, dataValue, valueType, format) {
    
    if (valueType == "text" || valueType == "string") {
        Machinata.Reporting.Export.addStringCell(sheet, address, dataValue);
    }
    else if (valueType == "date") {
        Machinata.Reporting.Export.addDateCell(sheet, address, dataValue, "dd.mm.yyyy", format);
    }
    else if (valueType == "year") {
        Machinata.Reporting.Export.addDateCell(sheet, address, dataValue, "yyyy", format);
    }
    else if (valueType == "month") {
        Machinata.Reporting.Export.addDateCell(sheet, address, dataValue, "mm", format);
    }
    else if (valueType == "percent") {
        Machinata.Reporting.Export.addNumberCell(sheet, address, dataValue, "0.00%", format);
    }
    else if (valueType == "p-abs") {
        Machinata.Reporting.Export.addNumberCell(sheet, address, dataValue * 100, "0.00", format);
    }
    else if (valueType == "p-rel") {
        Machinata.Reporting.Export.addNumberCell(sheet, address, dataValue * 10000, "0.00", format);
    }
    else if (valueType == "bp") {
        Machinata.Reporting.Export.addNumberCell(sheet, address, dataValue * 10000, "0.0", format);
    }
    else if (valueType == "amount") {
        Machinata.Reporting.Export.addNumberCell(sheet, address, dataValue, "#,##0.00", format);
    }
    else if (valueType == "count") {
        Machinata.Reporting.Export.addNumberCell(sheet, address, dataValue, "0", format);
    }
    else if ( valueType == "mv" ) {
        Machinata.Reporting.Export.addNumberCell(sheet, address, dataValue, "#,##0", format);
    }
    else if (valueType == "mv_t") {
        Machinata.Reporting.Export.addNumberCell(sheet, address, dataValue * 0.001, "#,##0", format);
    }
    else if (valueType == "mv_m") {
        Machinata.Reporting.Export.addNumberCell(sheet, address, dataValue * 0.000001, "#,##0", format);
    }
    else if (valueType == "mv_m2d") {
        Machinata.Reporting.Export.addNumberCell(sheet, address, dataValue * 0.000001, "#,##0.00", format);
    }
    else if (valueType == "pl") {
        Machinata.Reporting.Export.addNumberCell(sheet, address, dataValue, "#,##0.00", format);
    }
    else if (valueType == "fxrate") {
        Machinata.Reporting.Export.addNumberCell(sheet, address, dataValue, "#,##0.0000", format);
    }
    else if (valueType == "percentMin2Max8") {
        Machinata.Reporting.Export.addNumberCell(sheet, address, dataValue, "0.00000000%", format);
    }
    else if (valueType == null) {

        // No ValueType Set -> therefore we only take the raw value and save it to the specified cell
        Machinata.Reporting.Export.addGenericCell(sheet, address, dataValue, format);
    }
    else {
        // Unknown ValueType
        Machinata.Reporting.Export.addGenericCell(sheet, address, dataValue,  format);
        console.warn("The ValueType '" + valueType + "' is not supported by Machinata.Reporting.Export.addTableCell(). Please use a supported ValueType or update Machinata.Reporting.Export.addTableCell() to include support for this ValueType.");
    }
      

};


/// <summary>
/// Helper method to create a sheetname from a given title
/// Uses default "Sheet" if empty
/// Shortens the title to 31 characterss because of excel restriction
/// </summary>
Machinata.Reporting.Export.createExcelSheetName = function (title){

    if (title == null || title == "") {
        return "Sheet";
    }

    title = title.substring(0, 31);
    return title;
}

/// <summary>
/// Helper method for Machinata.Reporting.Export.createExcelDataForCategoryChart
/// </summary>
Machinata.Reporting.Export.addCategoryFactsToCells = function (sheet,  facts, currentRow, format) {
  
    for (var f = 0; f < facts.length; f++) {
        var fact = facts[f];

        var factCatAddr = { c: 0, r: currentRow };
        var factCatAddrRef = XLSX.utils.encode_cell(factCatAddr);

        var factAddr = { c: 1, r: currentRow };
        var factAddrRef = XLSX.utils.encode_cell(factAddr);

        // Symbol type
        var label = fact.category.resolved;
        if (fact.symbolType != null && fact.symbolType != "") {
            label += " (" + fact.symbolType + ")";
        }

        Machinata.Reporting.Export.addStringCell(sheet, factCatAddrRef, label)

        var dataValue = fact.val;
        var valueType = fact.valueType;

        Machinata.Reporting.Export.addTableCell(sheet, factAddrRef, dataValue, valueType, format);

        currentRow++;
    }

    return currentRow;

};


/// <summary>
/// Adds a sheet to the workbook with the values from CategoryFact type charts.
/// Currently tested with Pie and Donut
/// </summary>
Machinata.Reporting.Export.createExcelDataForCategoryChart = function (instance, config, json, workbook,format) {
    var row = 0; // keep track of current row

    var title = Machinata.Reporting.Export.createExcelSheetName(json.mainTitle.resolved);

    var sheet = {};
    for (var s = 0; s < json.series.length; s++) {

        var addr = { c: 1, r: row };
        var addRef = XLSX.utils.encode_cell(addr);

        sheet[addRef] = { v: json.series[s].title.resolved , t:"s" };
        row++;

        // Facts
        var facts = json.series[s].facts;

        row = Machinata.Reporting.Export.addCategoryFactsToCells(sheet, facts, row, format);
    }

    // Define range to be exported
    sheet['!ref'] = "A1:B" + row;

    // Add worksheet to workbook 
    XLSX.utils.book_append_sheet(workbook, sheet, title);

    return workbook;

};


/// <summary>
/// Adds a sheet to the workbook with the values from SeriesGroupsCharts type charts.
/// LineColumnNode
/// </summary>
Machinata.Reporting.Export.createExcelDataForSeriesGroupsChart = function (instance, config, json, workbook, format) {


    var title = Machinata.Reporting.Export.createExcelSheetName(json.mainTitle.resolved);
    var xAxisIsTime = json.globalXAxisScaleType == "time";

    var data = [];

    var sheet = {};

    var seriesNames = [];

    var maxColumn = 0;

    var hasZAxis = false;

    // Consolidate into same date -> data

    for (var g = 0; g < json.seriesGroups.length; g++) {
        var group = json.seriesGroups[g];

        var series = group.series;

        for (var s = 0; s < series.length; s++) {
            var serie = series[s];

            var serieName = serie.title.resolved;
            if (serieName == null || serieName == "") {
                serieName = serie.id;
            }

            seriesNames.push({ id: serie.id, name: serieName } );

            // Facts
            var facts = serie.facts;

            for (var f = 0; f < facts.length; f++) {
                var fact = facts[f];


                if (fact.x != null) {
                    var key = fact.x;

                    var entry = data.find(function (elem) { return elem[0] == key });

                    if (entry == null) {
                        entry = [];
                        entry.push(key);
                        entry[1] = [];
                        data.push(entry);
                    }

                    if (fact.z != null) {
                        hasZAxis = true;
                    }

                    entry[1].push({ serie: serie.id, y: fact.y, x: fact.x, z: fact.z, serieTitle: serie.title.resolved });
                }
            }
        }

    }

    // Sort by date 
    data.sort(function (a, b) {
        return a[0] - b[0];
    });

    // x Column name
    {
        var xColumnName = json.globalXAxisScaleType;
        var xColumnNameAddress = { c: 0, r: 0 };
        var xColumnNameAddressRef = XLSX.utils.encode_cell(xColumnNameAddress);
        Machinata.Reporting.Export.addStringCell(sheet, xColumnNameAddressRef, xColumnName);
    }

    var row = 0; // keep track of current row

    for (var d = 0; d < data.length; d++) {

        var column = 1;
       
        var key = data[d][0];
        var entry = data[d];
        var factxAddr = { c: 0, r: row + 1 };
        var factxAddrRef = XLSX.utils.encode_cell(factxAddr);
        var xValue = key;

        if (xAxisIsTime == true) {
            Machinata.Reporting.Export.addTableCell(sheet, factxAddrRef, xValue, "date", format);
        }
        else {
            // Unknown data type or format
            Machinata.Reporting.Export.addNumberCell(sheet, factxAddrRef, xValue, null, format);
        }

        for (var s = 0; s < seriesNames.length; s++) {
            var serieName = seriesNames[s].name;
            var serieId = seriesNames[s].id;

         
            if (row == 0) {
                var titleAddress = { c: column, r: row };
                var titleAddressRef = XLSX.utils.encode_cell(titleAddress);

                Machinata.Reporting.Export.addStringCell(sheet, titleAddressRef, serieName);
            }

            



            var value = entry[1].find(function (elem) {
                return elem.serie == serieId;
            });


            if (value != null) {

                // Y
                if (value.y != null) {
                    var factyAddr = { c: column, r: row + 1 };
                    var factyAddrRef = XLSX.utils.encode_cell(factyAddr);

                    // Unknown data type or format
                    Machinata.Reporting.Export.addNumberCell(sheet, factyAddrRef, value.y, null, format);
                }

                // Z
                if (value.z != null) {
                    var factzAddr = { c: column + 1, r: row + 1 };
                    var factzAddrRef = XLSX.utils.encode_cell(factzAddr);

                    // Unknown data type or format
                    Machinata.Reporting.Export.addNumberCell(sheet, factzAddrRef, value.z, null, format);
                }
            }

            column++;

            if (hasZAxis == true) {
                // Additional columns
                column++;
            }

            if (column > maxColumn) {
                maxColumn = column;
            }


        }

        row++;
    }

    // Define range to be exported
    var maxColRef = XLSX.utils.encode_cell({ c: maxColumn, r: (row ) });
    sheet['!ref'] = "A1:" + maxColRef;


    // Add worksheet to workbook 
    XLSX.utils.book_append_sheet(workbook, sheet, title);

    return workbook;

};


/// <summary>
///  Expanded non consolidated x-axis adds a sheet to the workbook with the values from seriesgroupscharts type charts.
/// currently not used
/// </summary>
Machinata.Reporting.Export.createExcelDataForSeriesGroupsChartExpanded = function (instance, config, json, workbook, format) {
    var row = 0; // keep track of current row

    var title = Machinata.Reporting.Export.createExcelSheetName(json.mainTitle.resolved);
    var xAxisIsTime = json.globalXAxisScaleType == "time";

    var sheet = {};

    for (var g = 0; g < json.seriesGroups.length; g++) {
        var group = json.seriesGroups[g];

        var groupName = group.id;

        var series = group.series;

        var gLabeladdr = { c: 0, r: row };
        var gLabeladdRef = XLSX.utils.encode_cell(gLabeladdr);

        sheet[gLabeladdRef] = { v: groupName, t: "s" };
        row++;
        https://ngr.vcaire.ch/typedoc/ngr-core/doc/classes/_ngr_core_src_reporting_charts_.graphfact.html

        for (var s = 0; s < series.length; s++) {
            var serie = series[s];
            var addr = { c: 1, r: row };
            var addRef = XLSX.utils.encode_cell(addr);

            sheet[addRef] = { v: serie.title.resolved, t: "s" };
            row++;

            // Facts
            var facts = serie.facts;


            for (var f = 0; f < facts.length; f++) {
                var fact = facts[f];

                var factXAddr = { c: 1, r: row };
                var factXAddrRef = XLSX.utils.encode_cell(factXAddr);

                var factYAddr = { c: 2, r: row };
                var factYAddrRef = XLSX.utils.encode_cell(factYAddr);

                // only facts with value
                if (fact.y != null) {
                    // var dataValue = fact.x + " : " + fact.y;
                    var valueType = fact.valueType;

                    if (valueType == null) {
                        valueType = "p-rel";
                        // TODO ASK MARCUS WHICH DATATYPE STYLE?
                    }

                    //Machinata.Reporting.debug(fact);

                    Machinata.Reporting.Export.addTableCell(sheet, factYAddrRef, fact.y, valueType, format);

                    if (xAxisIsTime == true) {

                        var date = fact.x;
                        Machinata.Reporting.Export.addTableCell(sheet, factXAddrRef, date, "date", format);
                    }
                    else {
                        Machinata.Reporting.Export.addTableCell(sheet, factXAddrRef, fact.x, valueType, format);
                    }

                    row++;
                }


            }
        }

    }

    // Define range to be exported
    sheet['!ref'] = "A1:D" + row;



    // Add worksheet to workbook 
    XLSX.utils.book_append_sheet(workbook, sheet, title);

    return workbook;

};





/// <summary>
/// For MultiGaugeNode, OfflineMapNode
/// </summary>
Machinata.Reporting.Export.createExcelDataForCategoryFacts = function (instance, config, json, workbook, format) {
   
    var row = 0; // keep track of current row

    var title = Machinata.Reporting.Export.createExcelSheetName(json.mainTitle.resolved);

    var sheet = {};

    // Facts
    var facts = json.facts;

    for (var f = 0; f < facts.length; f++) {
        var fact = facts[f];

        var factCatAddr = { c: 0, r: row };
        var factCatAddrRef = XLSX.utils.encode_cell(factCatAddr);

        var factAddr = { c: 1, r: row };
        var factAddrRef = XLSX.utils.encode_cell(factAddr);

        Machinata.Reporting.Export.addStringCell(sheet, factCatAddrRef, fact.category.resolved)

        Machinata.Reporting.Export.addTableCell(sheet, factAddrRef, fact.val, fact.valueType, format);
        row++;
    }


    // Define range to be exported
    sheet['!ref'] = "A1:B" + row;

    // Add worksheet to workbook 
    XLSX.utils.book_append_sheet(workbook, sheet, title);

    return workbook;

};


/// <summary>
/// Adds a sheet to the workbook with the values from a html table
/// Use intermediate format tables generated by Machinata.Reporting.Node.VerticalTableNode.buildTable
/// </summary>
Machinata.Reporting.Export.createExcelDataForTable = function (instance, config, json, workbook, table, format) {

    var title = Machinata.Reporting.Export.createExcelSheetName(json.mainTitle.resolved);

    // excel sheet which holds the data
    var sheet = {};

    // Colspan/Merges of cells
    var merges = [];

    var rowElems = table.find("thead tr, tbody tr");

    var columnsMax = 0;
    var row = 0; // keep track of current row

    // Go through each row
    rowElems.each(function (index) {

        var rowElem = $(this);
        var columnElems = rowElem.find("th, td");
        var column = 0;
        var colspanOffset = 0;

        // Go through each colum
        columnElems.each(function (indexC) {

            var cell = $(this);

            var addr = { c: column + colspanOffset, r: row };
            var addrRef = XLSX.utils.encode_cell(addr);

            var colspan = parseInt(cell.attr("colspan"));
          
            // Colspan?
            if (colspan > 1) {

                //https://github.com/SheetJS/sheetjs/blob/3542d62fffc155dd505a23230ba182c4402a0e2c/dist/xlsx.js#L19509

                // Merges: an array of ranges which are merged into one
                var merge = { s: { r: row, c: column + colspanOffset }, e: { r: row , c: colspanOffset + column + colspan - 1 } };
                merges.push(merge);

                // how far we are off the actual column number because of colspans
                colspanOffset += (colspan - 1);
            }
                       
            // Header
            if (cell.is("th")) {
                Machinata.Reporting.Export.addStringCell(sheet, addrRef, cell.text());
            }

            // Body
            else if (cell.is("td")) {

                var dataValue = cell.attr("data-value")
                var dataType = cell.attr("data-type")
                var dataText = cell.attr("data-text");

               // Machinata.debug(dataValue, dataType, dataText);

                // We have a data value
                if (dataValue != null && dataType != undefined) {
                    Machinata.Reporting.Export.addTableCell(sheet, addrRef, dataValue, dataType, format);
                }
                else if (dataText != null && dataType != undefined) {
                    Machinata.Reporting.Export.addTableCell(sheet, addrRef, dataText, dataType, format);
                }
                else if (dataText != null) {
                    // Labels: just add a string cell
                    Machinata.Reporting.Export.addStringCell(sheet, addrRef, dataText);
                }
            }

            // Column count
            column++;

            // Max width
            if (columnsMax < column + colspanOffset) {
                columnsMax = column + colspanOffset;
            }

        });

        row++;
    });

    var maxColRef = XLSX.utils.encode_cell({ c: columnsMax, r: (row-1) });

    // Define range to be exported
    sheet['!ref'] = "A1:" + maxColRef;

    // Merges
    if (merges.length) sheet["!merges"] = merges;

    // Add worksheet to workbook 
    XLSX.utils.book_append_sheet(workbook, sheet, title);

    return workbook;

};

/// <summary>
/// Saves the workbook to a file and forces browser to download
/// automatically exports XLSX or CSV depending on the filename extension
/// </summary>
Machinata.Reporting.Export.saveExcelWorkbook = function (instance, config, json, workbook, filename) {
    XLSX.writeFile(workbook, filename);
};

