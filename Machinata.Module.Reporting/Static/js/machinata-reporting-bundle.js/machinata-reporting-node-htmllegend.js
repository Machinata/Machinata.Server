/// <summary>
/// EXPERIMENTAL: NOT FOR PRODUCTION USE
/// DEPRECATED
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.HTMLLegendNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.HTMLLegendNode.defaults = {
    chrome: "light",
    supportsToolbar: false,
    addStandardTools: false
};


Machinata.Reporting.Node.HTMLLegendNode.init = function (instance, config, json, nodeElem) {
    var containerElem = $("<div class='node-bg'></div>").appendTo(nodeElem);
    // Legend
    var legendElem = Machinata.Reporting.Node.createHTMLLegendElem(instance, config, json, json.legendData).appendTo(containerElem);
};
Machinata.Reporting.Node.HTMLLegendNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here
};
Machinata.Reporting.Node.HTMLLegendNode.resize = function (instance, config, json, nodeElem) {
    // Nothing to do here
};








