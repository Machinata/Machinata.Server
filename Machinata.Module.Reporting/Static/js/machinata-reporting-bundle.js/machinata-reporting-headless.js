/// <summary>
/// Collection of tools specifically designed for operating the Reporting module
/// in a headless mode, such as server-side on NodeJS.
/// 
/// ## Headless Example on NodeJS
/// ```
/// // We need some global includes, since the Machinata NodeJS bundles purposefully don't include these
/// global.vega = require('vega');
/// global.fs = require('fs');
/// 
/// // Include the Machinata libraries
/// // Make sure these libraries have been exported/built using the 'nodejs' profile
/// require("./lib/machinata-core-bundle-en-debug.js");
/// require("./lib/machinata-reporting-bundle-en-debug.js");
/// 
/// // Include the reporting configuration
/// require("./lib/machinata-YOUR_REPORTING_CONFIG-bundle-en-debug.js");
/// 
/// // Show our build
/// console.log("Machinata Build: ");
/// console.log(Machinata.buildInfo());
/// 
/// // Our node to test
/// var gaugeJSON = {
///     "nodeType": "GaugeNode",
///     "theme": "magenta",
///     "width": 800,
///     "height": 600,
///     "theme": "magenta",
///     "status": {
///         "value": 1.6,
///         "title": { "resolved": "Moderate" },
///         "tooltip": { "resolved": "1.6 / 5" }
///     },
///     "segments": [
///         {
///             "active": true,
///             "title": { "resolved": "Low" }
///         },
///         {
///             "active": true,
///             "title": { "resolved": "Moderate" }
///         },
///         {
///             "active": false,
///             "title": { "resolved": "Medium" }
///         },
///         {
///             "active": false,
///             "title": { "resolved": "Enhanced" }
///         },
///         {
///             "active": false,
///             "title": { "resolved": "High" }
///         }
///     ]
/// };
/// 
/// // Create profiled config for `print`.
/// // Using Machinata.Reporting.Headless.getHeadlessConfigForProfile
/// // Note: this only needs to be called once per report
/// const config = Machinata.Reporting.Headless.getHeadlessConfigForProfile("print");
/// 
/// // Show our config
/// console.log("Machinata.Reporting Config: ");
/// console.log(config);
/// 
/// TODO: Update when API is finalized...
/// ```
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Headless = {};


/// <summary>
/// If ```true``` is passed, verbose debug output is enabled.
/// </summary>
Machinata.Reporting.Headless.enableVerboseOutput = function (enabled) {
    Machinata.Reporting.setDebugEnabled(enabled);
};

Machinata.Reporting.Headless.VerboseErrorsEnabled = false;

/// <summary>
/// If ```true``` is passed, VERY verbose error logging is enabled.
/// </summary>
Machinata.Reporting.Headless.enableVerboseErrors = function (enabled) {
    Machinata.Reporting.Headless.VerboseErrorsEnabled = enabled;
};

/// <summary>
/// Returns a merged (and loaded) configuration according to the specified ```profile```, with the 
/// config setting ```headless``` set to ```true```.
/// Any custom configuration must be bassed at this point to ensure that it loaded and applied properly.
/// </summary>
Machinata.Reporting.Headless.getHeadlessConfigForProfile = function (profile, config) {
    // Init config, with headless=true
    if (config == null) {
        config = {};
    }
    config["headless"] = true;

    // Create merged default version
    Machinata.Reporting.debug("Machinata.Reporting.Headless.getHeadlessConfigForProfile: "+profile);
    config = Machinata.Reporting.mergeAndLoadConfigWithDefaults(config, profile); // here we merge in all the configs, including the profile
    return config;
};

/// <summary>
/// Returns a full Vega spec for a ```VegaNode```, including the spec data integrated.
/// The returned Vega specs can be used to generate a chart using a VegaView.
/// Note: the Vega spec is unparsed.
/// Note: you can only call this method on node's that implement VegaNode.
/// </summary>
Machinata.Reporting.Headless.getVegaSpecForNode = function (config, nodeJSON, instance) {
    if (Machinata.Reporting.isDebugEnabled()) {
        Machinata.Reporting.debug("Machinata.Reporting.Headless.getVegaSpecForNode:");
        Machinata.Reporting.debug("Raw node (node " + nodeJSON.id + "):");
        Machinata.Reporting.debug(JSON.stringify(nodeJSON));
    }

    // Reset the state
    nodeJSON._state = {};

    // Get a merged version of the node
    Machinata.Reporting.Node.mergeJSONWithDefaults(config, nodeJSON);
    if (Machinata.Reporting.isDebugEnabled()) {
        Machinata.Reporting.debug("Merged node (node " + nodeJSON.id + "):");
        Machinata.Reporting.debug(JSON.stringify(nodeJSON));
    }

    // Create an instance to work with
    if (instance == null) {
        instance = Machinata.Reporting._createEmptyInstance();
        instance.config = config;
    }

    // Get the node implementation and call the init
    var impl = Machinata.Reporting.Node[nodeJSON.nodeType];
    impl.init(instance, config, nodeJSON, null);
    //TODO: validate this is a vega node?

    if (Machinata.Reporting.isDebugEnabled()) {
        Machinata.Reporting.debug("Initialized node (node " + nodeJSON.id + "):");
        Machinata.Reporting.debug(JSON.stringify(nodeJSON));
    }

    // Get spec
    var spec = null;
    try{
        spec = Machinata.Reporting.Node.VegaNode.compileVegaSpec(instance, config, nodeJSON);
    } catch (error) {
        if (Machinata.Reporting.Headless.VerboseErrorsEnabled == true) {
            console.error("Machinata.Reporting.Headless.getVegaSpecForNode: Could not compile node spec for node JSON:");
            console.error(JSON.stringify(nodeJSON));
        }
        throw error;
    }
    if (Machinata.Reporting.isDebugEnabled()) {
        Machinata.Reporting.debug("Spec (node " + nodeJSON.id + "):");
        Machinata.Reporting.debug(JSON.stringify(spec));
    }
    return spec;
};



/// <summary>
/// Writes a VegaNode's SVG chart output to the filepath specified.
/// You can specify the width and height of the SVG through the node's 
/// ```width``` and ```height``` property (pixels).
/// This method returns the Vega View ```toSVG()``` promise.
/// Note: you can only call this method on node's that implement VegaNode.
/// </summary>
Machinata.Reporting.Headless.writeVegaNodeSVGToFile = function (config, nodeJSON, filepath) {
    // Get the svg generation promise
    var svgPromise = Machinata.Reporting.Headless.getVegaNodeSVGAsPromise(config, nodeJSON);

    // Generate a static SVG image (promise)
    return svgPromise.then(function (svgStr) {
        fs.writeFile(filepath, svgStr, function (err) {
            if (err) throw err;
        });
    });
};



/// <summary>
/// Returns a a VegaView (https://vega.github.io/vega/docs/api/view/) for a VegaNode's generated specs.
/// You can specify the width and height of the SVG through the node's 
/// ```width``` and ```height``` property (pixels).
/// Note: you can only call this method on node's that implement VegaNode.
/// </summary>
Machinata.Reporting.Headless.getVegaNodeView = function (config, nodeJSON, instance) {
    // Get spec
    var spec = Machinata.Reporting.Headless.getVegaSpecForNode(config, nodeJSON, instance);

    // Get output dimensions
    var widthPixels = nodeJSON["width"];
    if (isNaN(widthPixels)) throw "Machinata.Reporting.Headless.getVegaNodeView: node width (nodeJSON.width) is NaN";
    if (widthPixels == null) widthPixels = 600; // default fallback
    var heightPixels = nodeJSON["height"];
    if (isNaN(heightPixels)) throw "Machinata.Reporting.Headless.getVegaNodeView: node height (nodeJSON.height) is NaN";
    if (heightPixels == null) heightPixels = 600; // default fallback

    // Parse the spec...
    var parsedSpec = null;
    var parserOptions = {};
    if (config.cspCompliance == true) parserOptions.ast = true;
    try {
        parsedSpec = vega.parse(spec, null, parserOptions);
    } catch (error) {
        if (Machinata.Reporting.Headless.VerboseErrorsEnabled == true) {
            console.error("Machinata.Reporting.Headless.getVegaNodeView: Could not parse node spec:");
            console.error(JSON.stringify(spec));
            console.error("For node JSON:");
            console.error(JSON.stringify(nodeJSON));
        }
        throw error;
    }
    Machinata.Reporting.debug("Parsed spec (node " + nodeJSON.id + "):");
    Machinata.Reporting.debug(JSON.stringify(spec));

    // Create view
    try {
        var vegaView = new vega.View(parsedSpec, {
            expr: config.cspCompliance == true ? vega.expressionInterpreter : null, // https://vega.github.io/vega/usage/interpreter/
            renderer: 'svg',
            container: null,
            responsive: false,
            hover: false,
        });
    } catch (error) {
        if (Machinata.Reporting.Headless.VerboseErrorsEnabled == true) {
            console.error("Machinata.Reporting.Headless.getVegaNodeView: Could not create vega view with parsed spec:");
            console.error(JSON.stringify(parsedSpec));
            console.error("For node JSON:");
            console.error(JSON.stringify(nodeJSON));
        }
        throw error;
    }
    vegaView.width(widthPixels);
    vegaView.height(heightPixels);

    // Return
    return vegaView;
};

/// <summary>
/// Returns a promise for a Chart Node's SVG chart output.
/// You can specify the width and height of the SVG through the node's 
/// ```width``` and ```height``` property (pixels).
/// If Machinata.Reporting.Config.automaticallyInlineSVGResourcesOnExport is enabled,
/// then the NodeJS packages ```jsdom``` and ```jquery``` must be installed.
/// </summary>
Machinata.Reporting.Headless.getChartNodeSVGAsPromise = function (config, nodeJSON) {
    // Get SVG (promise)
    //TODO: Create new mechanism for detecting vega implementation and switch on this...
    var vegaView = Machinata.Reporting.Headless.getVegaNodeView(config, nodeJSON);

    return new Promise((resolve, reject) => {
        try {
            vegaView.toSVG().then(function (svgString) {
                // Remove unsupported characters
                if (config.automaticallyReplaceSVGUnsupportedCharactersOnExport == true) {
                    svgString = Machinata.Reporting.SVG.replaceUnsupportedCharacters(config, svgString);
                }
                // Inline all images?
                if (config.automaticallyInlineSVGResourcesOnExport == true) {
                    // Require jQuery
                    try {
                        Machinata.Reporting.Headless.setGlobalJQueryReference(config);
                    } catch (errSetGlobalJQuery) {
                        throw "Machinata.Reporting.Headless.getChartNodeSVGAsPromise: inlining SVG resources on export (Machinata.Reporting.Config.automaticallyInlineSVGResourcesOnExport = true) requires the packages jsdom and jquery to be installed. Please run 'npm install jsdom jquery' first. " + errSetGlobalJQuery
                    }
                    try {
                        Machinata.Reporting.SVG.inlineSVGResourcesInSVG(config, svgString).then(function (svgString2) {
                            resolve(svgString2);
                        }).catch(function (err) {
                            reject(err);
                        });
                    } catch (errInlineSVG) {
                        reject(errInlineSVG);
                    }
                } else {
                    resolve(svgString);
                }
            });
        } catch (errorToSVG) {
            reject(errorToSVG);
        }
    });
};

/// <summary>
/// Goes through a report and renderes all nodes (which support headless rendering).
/// </summary>
/// <returns>
/// A array of JSON objects in the format: 
/// ```
/// {
///     nodeType: node type (string),
///     id: node id (string),
///     svgData: svg XML data (string)
/// }
/// ```
/// </returns>
Machinata.Reporting.Headless.getAllChartNodesSVGsForReportAsPromise = async function (config, reportJSON, nodeIdFilter) {
    var results = [];
    var rootNode = reportJSON["body"];

    // Create an instance to work with
    var instance = Machinata.Reporting._createEmptyInstance();
    instance.config = config;

    // Build report index
    Machinata.Reporting.buildIndex(instance, config, rootNode, null);

    // Helper function to visit all nodes recursively
    async function parseChildren(parentJSON) {
        // Sanity
        if (parentJSON == null || parentJSON.children == null) return;
        // Get a merged version of the node
        for (var i = 0; i < parentJSON.children.length; i++) {
            var childNodeJSON = parentJSON.children[i];
            if (childNodeJSON.enabled == false) continue;
            Machinata.Reporting.Node.mergeJSONWithDefaults(config, childNodeJSON);
            if (childNodeJSON.supportsHeadlessRendering == true && (nodeIdFilter == null || nodeIdFilter == childNodeJSON.id)) {
                // Compute size if not explicitly set
                if (childNodeJSON.width == null || childNodeJSON.height == null) {
                    if (childNodeJSON.supportedDashboardSizes != null && childNodeJSON.supportedDashboardSizes.length > 0) {
                        var dashboardSize = childNodeJSON.supportedDashboardSizes[0];
                        var unitSizeMM = 42;
                        if (dashboardSize == Machinata.Reporting.Layouts.BLOCK_2x2) {
                            childNodeJSON.width = Math.ceil(Machinata.Reporting.Tools.convertMillimetersToPixels(config, unitSizeMM * 2));
                            childNodeJSON.height = Math.ceil(Machinata.Reporting.Tools.convertMillimetersToPixels(config, unitSizeMM * 2));
                        } else if (dashboardSize == Machinata.Reporting.Layouts.BLOCK_2x1) {
                            childNodeJSON.width = Math.ceil(Machinata.Reporting.Tools.convertMillimetersToPixels(config, unitSizeMM * 2));
                            childNodeJSON.height = Math.ceil(Machinata.Reporting.Tools.convertMillimetersToPixels(config, unitSizeMM * 1));
                        } else if (dashboardSize == Machinata.Reporting.Layouts.BLOCK_1x1) {
                            childNodeJSON.width = Math.ceil(Machinata.Reporting.Tools.convertMillimetersToPixels(config, unitSizeMM * 1));
                            childNodeJSON.height = Math.ceil(Machinata.Reporting.Tools.convertMillimetersToPixels(config, unitSizeMM * 1));
                        } else if (dashboardSize == Machinata.Reporting.Layouts.BLOCK_4x2) {
                            childNodeJSON.width = Math.ceil(Machinata.Reporting.Tools.convertMillimetersToPixels(config, unitSizeMM * 4));
                            childNodeJSON.height = Math.ceil(Machinata.Reporting.Tools.convertMillimetersToPixels(config, unitSizeMM * 2));
                        }
                    } 
                }
                // Fix node if some things are empty
                if (childNodeJSON.width == null) childNodeJSON.width = 600;
                if (childNodeJSON.height == null) childNodeJSON.height = 600;
                // Create view and get svg
                var vegaView = Machinata.Reporting.Headless.getVegaNodeView(config, childNodeJSON, instance);
                await vegaView.toSVG().then(function (svg) {
                    results.push({
                        nodeType: childNodeJSON.nodeType,
                        id: childNodeJSON.id,
                        svgData: svg,
                        mainTitleResolved: Machinata.Reporting.Text.resolve(instance,childNodeJSON.mainTitle),
                        subTitleResolved: Machinata.Reporting.Text.resolve(instance,childNodeJSON.subTitle)
                    });
                });

            }
            // Recurse
            await parseChildren(childNodeJSON);
        }
    }
    await parseChildren(rootNode);

    return results;
};

/// <summary>
/// Returns a promise for a Chart Node's Canvas chart output.
/// You can specify the width and height of the SVG through the node's 
/// ```width``` and ```height``` property (pixels).
/// </summary>
Machinata.Reporting.Headless.getChartNodeCanvasAsPromise = function (config, nodeJSON) {
    // Get PNG (promise)
    //TODO: Create new mechanism for detecting vega implementation and switch on this...
    var vegaView = Machinata.Reporting.Headless.getVegaNodeView(config, nodeJSON);
    return vegaView.toCanvas();
};


/// <summary>
/// </summary>
/// <deprecated/>
Machinata.Reporting.Headless.getVegaNodeSVGAsPromise = function (config, nodeJSON) {
    console.warn("***Machinata.Reporting.Headless.getVegaNodeSVGAsPromise is deprecated, please use Machinata.Reporting.Headless.getChartNodeSVGAsPromise instead!");
    throw "Machinata.Reporting.Headless.getVegaNodeSVGAsPromise is deprecated, please use Machinata.Reporting.Headless.getChartNodeSVGAsPromise instead!";
};


/// <summary>
/// Loads jQuery in a headless (browserless) environment through the use of
/// jsdom, which is the standard NodeJS DOM emulator.
/// Note: to use this method the NodeJS packages ```jsdom``` and ```jquery``` must be installed.
/// See https://github.com/jsdom/jsdom for more information on headless DOM emulation.
/// </summary>
Machinata.Reporting.Headless.setGlobalJQueryReference = function (config) {
    if (global.$ == null && global.jQuery != null) {
        // Just set jQuery $ shortcut global...
        global.$ = global.jQuery;
    } else if (global.jQuery == null || global.$ == null) {
        // Load jQuery with jsdom
        // Import jsdom
        const jsdom = require('jsdom');
        // Create a window with the document object
        const dom = new jsdom.JSDOM("");
        // Import jquery and supply it with the new dom
        const jQuery = require('jquery')(dom.window);
        // Set globals
        global.jQuery = jQuery;
        global.$ = global.jQuery;
    }
};
