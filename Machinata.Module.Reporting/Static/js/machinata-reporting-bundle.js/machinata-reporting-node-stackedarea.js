


/// <summary>
/// ## Axis's
/// Scaling Engine: ```Machinata.Reporting.Node.VegaNode.Util.autoSnapMultiSeriesAxis```
/// </summary>
/// <example>
/// See ```example-report-stackedarea.json``` for complete example usages.
/// </example>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.StackedAreaNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.StackedAreaNode.defaults = {};

/// <summary>
/// By default charts are on solid chrome.
/// </summary>
Machinata.Reporting.Node.StackedAreaNode.defaults.chrome = "solid";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.StackedAreaNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.StackedAreaNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.StackedAreaNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_4x2];

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.StackedAreaNode.defaults.supportsToolbar = true;

/// <summary>
/// Yes, insert a legend by default.
/// </summary>
Machinata.Reporting.Node.StackedAreaNode.defaults.insertLegend = true;

/// <summary>
/// Sets the number of columns to use for legends. By default we use ```2```.
/// </summary>
Machinata.Reporting.Node.StackedAreaNode.defaults.legendColumns = 2;

/// <summary>
/// If ```true```, includes the serie title in the legend label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.StackedAreaNode.defaults.legendLabelsShowSerie = true;

/// <summary>
/// Makes sense for stacked areas - this way the legend optically matches the stack...
/// </summary>
Machinata.Reporting.Node.StackedAreaNode.defaults.legendAutomaticallyReversedForVerticalLegends = true;

/// <summary>
/// Set the global x-axis scale type. Can be any vega supported scale type as long as the data matches.
/// Note: For time-scales, you should ensure that the fact x-values are in Unix Epoch Format (milliseconds).
/// </summary>
Machinata.Reporting.Node.StackedAreaNode.defaults.xAxisScaleType = "time";

/// <summary>
/// If ```true```, labels will automatically be removed if they overlap the boundries of the axis by ```config.padding/2```.
/// By default ```true```,
/// </summary>
Machinata.Reporting.Node.StackedAreaNode.defaults.xAxisLabelBound = true;


/// <summary>
/// If set, defines the angle in degrees of axis labels. Typically this would be set to -45.
/// By default ```null``` (off),
/// </summary>
Machinata.Reporting.Node.StackedAreaNode.defaults.xAxisLabelAngle = null;



/// <summary>
/// </summary>
Machinata.Reporting.Node.StackedAreaNode.getVegaSpec = function (instance, config, json, nodeElem) {
    var emptyGroup = {
        "type": "group",
        /*"_comment": "HELPER GROUP FOR INSERTING EMPTY MARKS AT RUNTIME...",*/
    };
    return {
        "data": [
          {
              "name": "seriesGroups",
              "values": null,
          },
          {
              "name": "allSeries",
              "source": "seriesGroups",
              "transform": [
                {
                    "type": "flatten",
                    "fields": [
                      "series"
                    ],
                    "as": [
                      "serie"
                    ]
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "key",
                    "expr": "datum.id + '_' + datum.serie.id"
                }
              ]
          },
          {
              "name": "allSeriesResolved",
              "source": "allSeries",
              "transform": [
                { "type": "formula", "initonly": true, "as": "colorResolved", "expr": "(datum.serie.colorShade ? scale('theme-bg',datum.serie.colorShade) : scale('colorByKey',datum.key))" },
                  { "type": "formula", "initonly": true, "as": "textColorResolved", "expr": "(datum.serie.colorShade ? scale('theme-fg',datum.serie.colorShade) : scale('colorByKeyText',datum.key))" },
                { "type": "formula", "initonly": true, "as": "titleResolved", "expr": "datum.serie.title.resolved" },
                { "type": "formula", "initonly": true, "as": "type", "expr": "'area'" }, // hardcoded
                { "type": "formula", "initonly": true, "as": "xAxisTransform", "expr": "datum.xAxis.transform" },
              ]
          },
            {
                "name": "allFlattened",
                "source": "allSeriesResolved",
                "transform": [
                  {
                      "type": "flatten",
                      "fields": [
                        "serie.facts"
                      ],
                      "as": [
                        "fact"
                      ]
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "factY",
                      "expr": "datum.fact.y"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "factX",
                      "expr": "datum.fact.x"
                  }
                ]
            },
            
        ],
        "signals": [
            {
                "name": "configLegendSymbolSize",
                "init": config.legendSymbolSize
            },
            {
                "name": "configPadding",
                "init": config.padding
            },
            {
                "name": "configLegendRowHeightWithPadding",
                "init": config.legendSymbolSize + config.legendSymbolStroke + config.legendRowPadding 
            }
        ],
        "scales": [
            {
                "name": "colorByKey",
                "type": "ordinal",
                "domain": { "data": "allSeries", "field": "key" },
                "range": { "scheme": json.theme + "-bg" }
            },
            {
                "name": "colorByKeyText",
                "type": "ordinal",
                "domain": { "data": "allSeries", "field": "key" },
                "range": { "scheme": json.theme + "-fg" }
            },
            {
                "name": "color",
                "type": "ordinal",
                "domain": { "data": "allSeriesResolved", "field": "key" },
                "range": { "data": "allSeriesResolved", "field": "colorResolved" },
            },
            {
                "name": "textColor",
                "type": "ordinal",
                "domain": { "data": "allSeriesResolved", "field": "key" },
                "range": { "data": "allSeriesResolved", "field": "textColorResolved" },
            },
          {
              "name": "debugColor",
              "type": "ordinal",
              "domain": {
                  "data": "allSeriesResolved",
                  "field": "key"
              },
              "range": {
                  "scheme": "category20"
              }
          },
          {
              // Provide the proper color for the legend
              "name": "legendColor",
              "type": "ordinal",
              "domain": { "data": "allSeriesResolved", "field": "key" },
              "range": { "data": "allSeriesResolved", "field": "colorResolved" },
          },
            {
                "name": "tooltipTitle",
                "type": "ordinal",
                "domain": { "data": "allSeriesResolved", "field": "key" },
                "range": { "data": "allSeriesResolved", "field": "titleResolved" }
            },
            {
                // This is a workaround/hack for Vega using a fixed scale
                "name": "xAxisMinValue",
                "type": "ordinal",
                "domain": ["xAxisMinValue"],
                "range": { "data": "seriesGroups", "field": "xAxis.minValue" }
            },
            {
                // This is a workaround/hack for Vega using a fixed scale
                "name": "xAxisMaxValue",
                "type": "ordinal",
                "domain": ["xAxisMaxValue"],
                "range": { "data": "seriesGroups", "field": "xAxis.maxValue" }
            },
            {
                "name": "xScale",
                /*"_comment": "This x-scale is used by everyone. Thus the chart groups are 'tight' with each other.",*/
                "type": json.xAxisScaleType,
                "domain": {
                    "data": "allFlattened",
                    "field": "factX"
                },
                "domainMin": {
                    "signal": "scale('xAxisMinValue','xAxisMinValue')",
                },
                "domainMax": {
                    "signal": "scale('xAxisMaxValue','xAxisMaxValue')",
                },
                "range": "width",
                "padding": { "signal": (config.padding * 4) },
                //"padding": 30, // debugging only
            }
        ],
        "marks": [
          {
              /*"_comment": "Explodes the seriesGroups into individual group marks",*/
              "name": "groupGroups",
              "type": "group",
              "encode": {
                  "update": {
                      "width": {
                          "signal": "width"
                      },
                      "height": {
                          "signal": "height"
                      }
                  }
              },
              "from": {
                  "facet": {
                      "data": "seriesGroups",
                      "name": "facetGroups",
                      "groupby": "id"
                  }
              },
              "data": [],
              "marks": [
                {
                    /*"_comment": "Here we have a single group with all its series. At this stage we do an axis and scale",*/
                    "name": "groupSeries",
                    "type": "group",
                    "encode": {
                        "update": {
                            "width": {
                                "signal": "width"
                            },
                            "height": {
                                "signal": "height"
                            }
                        }
                    },
                    "data": [
                      {
                          "name": "series",
                          "source": "facetGroups",
                          "transform": [
                            {
                                "type": "flatten",
                                "fields": [
                                  "series"
                                ],
                                "as": [
                                  "serie"
                                ]
                            },
                            {
                                "type": "formula",
                                "initonly": true,
                                "as": "key",
                                "expr": "datum.id + '_' + datum.serie.id"
                              },
                              {
                                  "type": "formula",
                                  "initonly": true,
                                  "as": "titleResolved",
                                  "expr": "datum.serie.titleResolved"
                              },
                            {
                                "type": "formula",
                                "initonly": true,
                                "as": "chartType",
                                "expr": "datum.serie.chartType"
                            }
                          ]
                      },
                      {
                          "name": "flattened",
                          "source": "series",
                          "transform": [
                            {
                                "type": "flatten",
                                "fields": [
                                  "serie.facts"
                                ],
                                "as": [
                                  "fact"
                                ]
                            },
                            {
                                "type": "formula",
                                "initonly": true,
                                "as": "factKey",
                                "expr": "datum.key"
                            },
                            {
                                "type": "formula",
                                "initonly": true,
                                "as": "factY",
                                "expr": "datum.fact.y"
                              },
                              {
                                  "type": "formula",
                                  "initonly": true,
                                  "as": "factX",
                                  "expr": "datum.fact.x"
                              },
                              {
                                  "type": "formula",
                                  "initonly": true,
                                  "as": "factStackY0",
                                  "expr": "datum.fact.stackY0"
                              },
                              {
                                  "type": "formula",
                                  "initonly": true,
                                  "as": "factStackY1",
                                  "expr": "datum.fact.stackY1"
                              },
                              {
                                  "type": "identifier",
                                  "as": "sortId"
                              },
                              {
                                  "type": "collect",
                                  "sort": {
                                      "field": "sortId",
                                      "order": ["ascending"] // Stacks are sorted ascending to mimick Excel behaviour
                                  }
                              },
                              /*{
                                  // >>> this is the stacking
                                  "type": "stack",
                                  "groupby": ["factX"],
                                  "field": "factY"
                              }*/
                          ]
                      }
                    ],
                    "scales": [
                        {
                            // This is a workaround/hack for Vega using a fixed scale
                            "name": "xAxisFormat",
                            "type": "ordinal",
                            "domain": ["xAxisFormat"],
                            "range": { "data": "series", "field": "xAxis.format" }
                        },
                        {
                            // This is a workaround/hack for Vega using a fixed scale
                            "name": "xAxisFormatType",
                            "type": "ordinal",
                            "domain": ["xAxisFormatType"],
                            "range": { "data": "series", "field": "xAxis.formatType" }
                        },
                        {
                            // This is a workaround/hack for Vega using a fixed scale
                            "name": "yAxisMinValue",
                            "type": "ordinal",
                            "domain": ["yAxisMinValue"],
                            "range": { "data": "series", "field": "yAxis.minValue" }
                        },
                        {
                            // This is a workaround/hack for Vega using a fixed scale
                            "name": "yAxisMaxValue",
                            "type": "ordinal",
                            "domain": ["yAxisMaxValue"],
                            "range": { "data": "series", "field": "yAxis.maxValue" }
                        },
                        {
                          "name": "yScale",
                          "type": "linear",
                          "domain": {
                              "data": "flattened",
                              "field": "factStackY1"
                          },
                          "domainMin": {
                              "signal": "scale('yAxisMinValue','yAxisMinValue')",//"scale('yAxisMinValue','yAxisMinValue')",
                          },
                          "domainMax": {
                              "signal": "scale('yAxisMaxValue','yAxisMaxValue')",
                          },
                          "padding": config.yAxisLabelPadding,
                          //"padding": 30, // testing only
                          "range": "height",
                          "round": true, // setting round to false will cause a strange decoupled yaxis where the zero round is floating off
                          "zero": true,
                          "nice": true // vega does nice
                          //"nice": { "signal": "scale('yAxisTickCount','yAxisTickCount')" }
                          //"nice": true // setting nice to true will cause the axis domain to snap to a value, leading to a side affect that two axis' no longer align, setting it to false will cause the top/bottom of the bars/lines to hang over the edge of the yaxis
                      },
                        {
                            // This is a workaround/hack for Vega using a fixed scale
                            "name": "yAxisOrient",
                            "type": "ordinal",
                            "domain": ["yAxisOrient"],
                            "range": { "data": "series", "field": "yAxis.orient" }
                        },
                        {
                            // This is a workaround/hack for Vega using a fixed scale
                            "name": "yAxisFormat",
                            "type": "ordinal",
                            "domain": ["yAxisFormat"],
                            "range": { "data": "series", "field": "yAxis.format" }
                        },
                        {
                            // This is a workaround/hack for Vega using a fixed scale
                            "name": "yAxisFormatType",
                            "type": "ordinal",
                            "domain": ["yAxisFormatType"],
                            "range": { "data": "series", "field": "yAxis.formatType" }
                        }
                    ],
                    "axes": [

                        Machinata.Reporting.Node.VegaNode.Util.applySettingsToVegaAxis(instance,config,json,{
                            "orient": "bottom",
                            "scale": "xScale",
                            "domain": false,
                            "format": { "signal": "if(scale('xAxisFormat','xAxisFormat') == null, ' ', scale('xAxisFormat','xAxisFormat'))" }, // If the format is null, use empty string ' ', otherwise 
                            "formatType": { "signal": "scale('xAxisFormatType','xAxisFormatType')" },
                            "values": { "signal": "data('series')[0].xAxis.values" },
                            "tickValues": { "signal": "data('series')[0].xAxis.tickValues" },
                            "labelValues": { "signal": "data('series')[0].xAxis.labelValues" },
                            "grid": false,
                            "labelBound": (json.xAxisLabelBound == true ? (config.padding/2) : false), // Indicates if labels should be hidden if they exceed the axis range. If false (the default) no bounds overlap analysis is performed. If true, labels will be hidden if they exceed the axis range by more than 1 pixel. If this property is a number, it specifies the pixel tolerance: the maximum amount by which a label bounding box may exceed the axis range.
                            "tickCount": { "signal": "data('series')[0].xAxis.tickCount" }
                        }),
                      
                      {
                          "orient": "left",
                          "scale": "yScale",
                          "tickSize": 0,
                          "tickRound": true,
                          "format": { "signal": "scale('yAxisFormat','yAxisFormat')" },
                          "formatType": { "signal": "scale('yAxisFormatType','yAxisFormatType')" },
                          "zindex": 1,
                          "grid": true,
                          "domain": false,
                          "values": { "signal": "data('series')[0].yAxis.values" },
                          "tickValues": { "signal": "data('series')[0].yAxis.tickValues" },
                          "labelValues": { "signal": "data('series')[0].yAxis.labelValues" },
                      }
                    ],
                    "marks": [
                      {
                          /*"_comment": "AREAS MASTER GROUPS",*/
                          "type": "group",
                          "clip": false,
                          "zindex": 3,
                          "from": {
                              "facet": {
                                  "data": "flattened",
                                  "name": "facet",
                                  "groupby": "factKey"
                              }
                            },
                          "marks": [
                            {
                                "type": "area",
                                "from": {
                                    "data": "facet"
                                },
                                "encode": {
                                    "enter": {
                                        "interpolate": { "value": "linear" },
                                        "x": {
                                            "scale": "xScale",
                                            "field": "factX",
                                        },
                                        "y": {
                                            "scale": "yScale",
                                            "field": "factStackY0"
                                        },
                                        "y2": {
                                            "scale": "yScale",
                                            "field": "factStackY1"
                                        },
                                        "fill": {
                                            "scale": "color",
                                            "field": "key"
                                        },
                                        "defined": {
                                            "signal": "datum.factY != null ? true : false"
                                        }
                                    },
                                }
                            },
                            {
                                "type": "rect",
                                /*"_comment": "These are transparent rects for the tooltip",*/
                                "from": {
                                    "data": "facet"
                                },
                                "encode": {
                                    "enter": {
                                        "fill": {
                                            "value": "transparent" // should be transparent
                                        },
                                    },
                                    "update": {
                                        "width": {
                                            "signal": "width / data('facet').length"
                                        },
                                        "xc": {
                                            "scale": "xScale",
                                            "field": "factX",
                                        },
                                        "y": {
                                            "scale": "yScale",
                                            "field": "factStackY0"
                                        },
                                        "y2": {
                                            "scale": "yScale",
                                            "field": "factStackY1"
                                        },
                                        "tooltip": [
                                            {
                                                // Show tooltip only if we have a value
                                                "test": "datum.factY == null",
                                                "value": null
                                            },
                                            { "signal": "scale('tooltipTitle',datum.factKey) + ': ' + format(datum.factY,scale('yAxisFormat','yAxisFormat'))" }
                                        ]
                                    }
                                }
                              },
                              /*{
                                  "type": "text",
                                  "_comment": "TEXT LABELS ON CHART FOR SERIES (AREAS)",
                                  "zindex": 4,
                                  "encode": {
                                      "enter": {
                                          "fill": {
                                              "signal": "scale('textColor',data('facet')[0].key)"
                                              // DEBUGGING
                                              //"signal": "'red'"
                                          },
                                          "text": {
                                              "signal": "scale('tooltipTitle',data('facet')[0].key)"
                                              // DEBUGGING
                                              //"signal": "data('facet')[0].key"
                                          },
                                          "baseline": {
                                              "value": "middle"
                                          },
                                          "align": {
                                              "value": "right"
                                          },
                                          "dx": {
                                              "value": -config.padding
                                          },
                                      },
                                      "update": {
                                          "x": {
                                              "signal": "scale('xScale',data('facet')[0].factX)",
                                          },
                                          "y": {
                                              "signal": "scale('yScale', data('facet')[0].y1 - (data('facet')[0].y1-data('facet')[0].y0)/2 )",
                                          },
                                          "tooltip": [
                                              
                                              { "value": null }
                                          ]
                                      }
                                  }
                              },*/
                              /*{
                                  "type": "text",
                                  "_comment": "TEXT LABELS NEXT TO CHART FOR SERIES (AREAS)",
                                  "zindex": 4,
                                  "encode": {
                                      "enter": {
                                          
                                          "text": {
                                              "signal": "scale('tooltipTitle',data('facet')[0].key)"
                                              // DEBUGGING
                                              //"signal": "data('facet')[0].key"
                                          },
                                          "baseline": {
                                              "value": "middle"
                                          },
                                          "align": {
                                              "value": "left"
                                          },
                                          "dx": {
                                              "value": config.padding*5
                                          },
                                      },
                                      "update": {
                                          "x": {
                                              "signal": "scale('xScale',data('facet')[0].factX)",
                                          },
                                          "y": {
                                              "signal": "scale('yScale', data('facet')[0].y1 - (data('facet')[0].y1-data('facet')[0].y0)/2 )",
                                          },
                                          "tooltip": [

                                              { "value": null }
                                          ]
                                      }
                                  }
                              }*/
                              /*{
                                  "type": "rule",
                                  "_comment": "CONNECTING LINES FOR SERIES (AREAS)",
                                  "zindex": 4,
                                  "encode": {
                                      "enter": {

                                          "tooltip": {
                                              "signal": "scale('tooltipTitle',data('facet')[0].key)"
                                              // DEBUGGING
                                              //"signal": "data('facet')[0].key"
                                          },
                                          "stroke": {
                                              "signal": "scale('color',data('facet')[0].key)"
                                          },
                                          "strokeWidth": {
                                              "value": config.lineSize
                                          },
                                      },
                                      "update": {
                                          "x": {
                                              "signal": "scale('xScale',data('facet')[0].factX) + configPadding",
                                          },
                                          "y": {
                                              "signal": "scale('yScale', data('facet')[0].y1 - (data('facet')[0].y1-data('facet')[0].y0)/2 )",
                                          },
                                          "x2": {
                                              "signal": "width + configPadding*2",
                                          },
                                          "y2": {
                                              "signal": "(data('facet')[0].serie.serieIndex * configLegendRowHeightWithPadding) + configLegendSymbolSize/2",
                                          },
                                          "tooltip": {
                                              // DEBUGGING
                                              "signal": "data('facet')[0].serie.serieIndex "
                                          },
                                      }
                                  }
                              },*/
                              json.connectSeriesToLegend != true ? emptyGroup : {
                                  "type": "group",
                                  /*"_comment": "CONNECTING LINES FOR SERIES (AREAS)",*/
                                  "zindex": 4,
                                  "encode": {
                                      "enter": {
                                      },
                                      "update": {
                                          "x": {
                                              "signal": "round( scale('xScale',data('facet')[0].factX) + configPadding )",
                                          },
                                          "width": {
                                              "signal": "configPadding*4",
                                          },
                                          "height": {
                                              "signal": "height",
                                          },
                                          "y": {
                                              "signal": "0",
                                          },
                                          "tooltip": {
                                              // DEBUGGING
                                              //"signal": "data('facet')[0].serie.serieIndex "
                                          },
                                      }
                                  },
                                  "signals": [
                                      {
                                          "name": "connectorWidth",
                                          "update": "connectorNumTotal * 6",
                                      },
                                      {
                                          "name": "connectorOffset",
                                          "update": "round( ((connectorWidth) / connectorNumTotal) * connectorIndex )",
                                      },
                                      {
                                          "name": "connectorIndex",
                                          "update": "data('facet')[0].serie.serieIndex",
                                      },
                                      {
                                          "name": "connectorNumTotal",
                                          "update": "data('allSeries').length",
                                      },
                                      {
                                          "name": "connectorYStart",
                                          "update": "round( scale('yScale', data('facet')[0].factStackY1 - (data('facet')[0].factStackY1-data('facet')[0].factStackY0)/2 ) )",
                                      },
                                      {
                                          "name": "connectorYEnd",
                                          "update": "round( (data('facet')[0].serie.serieIndex * configLegendRowHeightWithPadding) + configLegendSymbolSize/2 )",
                                      }
                                  ],
                                  "marks": [
                                      {
                                          "type": "rule",
                                          "encode": {
                                              "enter": {
                                                  "stroke": {
                                                      //"signal": "scale('color',data('facet')[0].key)"
                                                      "value": config.gridColor
                                                  },
                                                  "strokeWidth": {
                                                      "value": config.lineSize
                                                  },
                                              },
                                              "update": {
                                                  "x": {
                                                      "signal": "0",
                                                  },
                                                  "x2": {
                                                      "signal": "connectorOffset",
                                                  },
                                                  "y": {
                                                      "signal": "connectorYStart",
                                                  },
                                                  "y2": {
                                                      "signal": "connectorYStart",
                                                  },
                                              }
                                          }
                                      },
                                      {
                                          "type": "rule",
                                          "encode": {
                                              "enter": {
                                                  "stroke": {
                                                      //"signal": "scale('color',data('facet')[0].key)"
                                                      "value": config.gridColor
                                                  },
                                                  "strokeWidth": {
                                                      "value": config.lineSize
                                                  },
                                              },
                                              "update": {
                                                  "x": {
                                                      "signal": "connectorOffset",
                                                  },
                                                  "x2": {
                                                      "signal": "connectorOffset",
                                                  },
                                                  "y": {
                                                      "signal": "connectorYStart",
                                                  },
                                                  "y2": {
                                                      "signal": "connectorYEnd",
                                                  },
                                              }
                                          }
                                      },
                                      {
                                          "type": "rule",
                                          "encode": {
                                              "enter": {
                                                  "stroke": {
                                                      //"signal": "scale('color',data('facet')[0].key)"
                                                      "value": config.gridColor
                                                  },
                                                  "strokeWidth": {
                                                      "value": config.lineSize
                                                  },
                                              },
                                              "update": {
                                                  "x": {
                                                      "signal": "connectorOffset",
                                                  },
                                                  "x2": {
                                                      "signal": "connectorWidth + configPadding",
                                                  },
                                                  "y": {
                                                      "signal": "connectorYEnd",
                                                  },
                                                  "y2": {
                                                      "signal": "connectorYEnd",
                                                  },
                                              }
                                          }
                                      }
                                  ]
                              }
                          ]
                      }
                    ]
                }
              ]
          }
        ]
    };
};
Machinata.Reporting.Node.StackedAreaNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    // Insert series
    spec["data"][0].values = json.seriesGroups;
};




Machinata.Reporting.Node.StackedAreaNode.init = function (instance, config, json, nodeElem) {

    // Init
    var seriesCount = 0;
    var legendReverseOrder = false;
    for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
        var seriesGroup = json.seriesGroups[sgi];
        seriesGroup.index = sgi;
        for (var si = 0; si < seriesGroup.series.length; si++) {
            var serie = seriesGroup.series[si];
            serie.serieIndex = seriesCount;
            seriesCount++;
        }
    }


    // Pre-process
    for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
        var seriesGroup = json.seriesGroups[sgi];
        for (var si = 0; si < seriesGroup.series.length; si++) {
            var serie = seriesGroup.series[si];
            // Preprocess axis
            Machinata.Reporting.Node.VegaNode.Util.preprocessAxisProperties(serie.xAxis);
            Machinata.Reporting.Node.VegaNode.Util.preprocessAxisProperties(serie.yAxis);
        }
    }

    // Automatically add some helper classes for sizing...
    if (nodeElem != null) {
        if (seriesCount > 0 && seriesCount <= 5) {
            nodeElem.addClass("series-count-range-0-5");
        } else if (seriesCount > 5 && seriesCount <= 10) {
            nodeElem.addClass("series-count-range-5-10");
        } else if (seriesCount > 10 && seriesCount <= 15) {
            nodeElem.addClass("series-count-range-10-15");
        } else if (seriesCount > 15 && seriesCount <= 20) {
            nodeElem.addClass("series-count-range-15-20");
        } else if (seriesCount > 20 && seriesCount <= 30) {
            nodeElem.addClass("series-count-range-20-30");
        }
        nodeElem.addClass("series-count-" + seriesCount);
    }

    // Do our own stacking...
    for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
        var seriesGroup = json.seriesGroups[sgi];
        var xStackTable = {};
        for (var si = 0; si < seriesGroup.series.length; si++) {
            var serie = seriesGroup.series[si];
            for (var fi = 0; fi < serie.facts.length; fi++) {
                var fact = serie.facts[fi];
                // Init empty (on first entry on x)
                if (xStackTable[fact.x] == null) {
                    if (si == 0) xStackTable[fact.x] = 0;
                    else console.warn("Machinata.Reporting.Node.StackedAreaNode.init: series stack doesnt overlap! This means that the series have different x values, which is not supported.");
                }
                // Append
                fact.stackY0 = xStackTable[fact.x];
                fact.stackY1 = fact.stackY0 + fact.y;
                if (isNaN(fact.stackY0)) console.warn("fact " + si + "." + fi +" stackY0 is NaN");
                if (isNaN(fact.stackY1)) console.warn("fact " + si + "." + fi +" stackY1 is NaN");
                xStackTable[fact.x] = fact.stackY1;
            }
        }
    }

    // Generate legend data
    Machinata.Reporting.Node.VegaNode.Util.createLegendForGroupedSeries(instance, config, json, json.seriesGroups, {});

    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.StackedAreaNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.StackedAreaNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};


/// <summary>
/// </summary>
Machinata.Reporting.Node.StackedAreaNode.exportExcelFormat = function (instance, config, json, nodeElem, format, filename) {
    var workbook = Machinata.Reporting.Export.createExcelWorkbook(instance, config, json);
    workbook = Machinata.Reporting.Export.createExcelDataForSeriesGroupsChart(instance, config, json, workbook, format);
    Machinata.Reporting.Export.saveExcelWorkbook(instance, config, json, workbook, filename);
    return true;
};






