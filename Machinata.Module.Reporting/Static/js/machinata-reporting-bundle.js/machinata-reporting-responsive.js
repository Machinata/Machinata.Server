

/// <summary>
/// Tools and extensions that make reports fully responsive.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Responsive = {};

/// <summary>
/// </summary>
Machinata.Reporting.Responsive.LAYOUT_NAME_DESKTOP = "desktop";

/// <summary>
/// </summary>
Machinata.Reporting.Responsive.LAYOUT_NAME_TABLET = "tablet";

/// <summary>
/// </summary>
Machinata.Reporting.Responsive.LAYOUT_NAME_MOBILE = "mobile";

/// <summary>
/// </summary>
Machinata.Reporting.Responsive.LAYOUT_WIDTH_MOBILE = 450;

/// <summary>
/// </summary>
Machinata.Reporting.Responsive.LAYOUT_WIDTH_TABLET = 800;

/// <summary>
/// Provides the handler used by Core JS Responsive to get the actual report layout (we don't use the page layout for the nodes).
/// </summary>
Machinata.Reporting.Responsive.layoutHandlerForCoreResponsive = function (elem, definition) {
    return elem.closest(".machinata-reporting-report").data("layout"); //TODO: what if someone is using a different selector?
};

/// <summary>
/// </summary>
Machinata.Reporting.Responsive.getReportLayout = function (instance) {
    return instance._currentLayout;
    //if (instance._previousLayout == null) Machinata.Reporting.Responsive.updateReportLayout(instance,"Machinata.Reporting.Responsive.getReportLayout"); // make sure this has been called at least once before
    //return instance._previousLayout;
};

/// <summary>
/// </summary>
Machinata.Reporting.Responsive.getReportLayoutForSize = function (instance, size) {
    var layout = Machinata.Reporting.Responsive.LAYOUT_NAME_DESKTOP; //fallback
    if (size < Machinata.Reporting.Responsive.LAYOUT_WIDTH_MOBILE) layout = Machinata.Reporting.Responsive.LAYOUT_NAME_MOBILE;
    else if (size < Machinata.Reporting.Responsive.LAYOUT_WIDTH_TABLET) layout = Machinata.Reporting.Responsive.LAYOUT_NAME_TABLET;
    return layout;
};

/// <summary>
/// </summary>
Machinata.Reporting.Responsive.updateForContainerSize = function (instance) {
    Machinata.Reporting.Responsive.updateReportLayout(instance);
    instance.elems.content.find(".machinata-reporting-node").trigger(Machinata.Reporting.Events.REDRAW_NODE_SIGNAL);
};

/// <summary>
/// </summary>
Machinata.Reporting.Responsive.updateReportLayout = function (instance) {

    if (instance.config.automaticallySetLayoutMinMaxDimensions == true) {
        instance.elems.content.css("min-width", instance.config.layoutMinWidth);
        instance.elems.content.css("max-width", instance.config.layoutMaxWidth);
        instance.elems.content.css("margin-left", "auto");
        instance.elems.content.css("margin-right", "auto");
    }

    // Init
    var reportWidth = instance.elems.content.width();
    var reportLayout = Machinata.Reporting.Responsive.getReportLayoutForSize(instance, reportWidth);

    // Are we changing?
    var layoutIsChanging = instance._currentLayout != reportLayout;
    if (layoutIsChanging == true) {
        // Debug
        Machinata.Reporting.debug("Machinata.Reporting.Responsive.updateReportLayout:");
        Machinata.Reporting.debug("  width:" + reportWidth);
        Machinata.Reporting.debug("  layout:" + instance._currentLayout + " > " + reportLayout);

        // Update the layout attributes
        instance._currentLayout = reportLayout;
        instance.elems.content.data("layout", reportLayout);

        // Update the layout classes
        instance.elems.content.addClass("layout-" + reportLayout);
        if (reportLayout != Machinata.Reporting.Responsive.LAYOUT_NAME_DESKTOP) instance.elems.content.removeClass("layout-" + Machinata.Reporting.Responsive.LAYOUT_NAME_DESKTOP);
        if (reportLayout != Machinata.Reporting.Responsive.LAYOUT_NAME_TABLET) instance.elems.content.removeClass("layout-" + Machinata.Reporting.Responsive.LAYOUT_NAME_TABLET);
        if (reportLayout != Machinata.Reporting.Responsive.LAYOUT_NAME_MOBILE) instance.elems.content.removeClass("layout-" + Machinata.Reporting.Responsive.LAYOUT_NAME_MOBILE);
        if (instance.config.automaticallySetReportLayoutClassOnBody == true) {
            $("body").addClass("machinata-reporting-layout-" + reportLayout);
            if (reportLayout != Machinata.Reporting.Responsive.LAYOUT_NAME_DESKTOP) $("body").removeClass("machinata-reporting-layout-" + Machinata.Reporting.Responsive.LAYOUT_NAME_DESKTOP);
            if (reportLayout != Machinata.Reporting.Responsive.LAYOUT_NAME_TABLET) $("body").removeClass("machinata-reporting-layout-" + Machinata.Reporting.Responsive.LAYOUT_NAME_TABLET);
            if (reportLayout != Machinata.Reporting.Responsive.LAYOUT_NAME_MOBILE) $("body").removeClass("machinata-reporting-layout-" + Machinata.Reporting.Responsive.LAYOUT_NAME_MOBILE);
        }
        
    }

    if (instance.config.automaticallySetPageLayoutClassOnBody == true) {
        var pageWidth = $("body").width();
        var pageLayout = Machinata.Reporting.Responsive.getReportLayoutForSize(instance, pageWidth);
        $("body").addClass("machinata-page-layout-" + pageLayout);
        if (pageLayout != Machinata.Reporting.Responsive.LAYOUT_NAME_DESKTOP) $("body").removeClass("machinata-page-layout-" + Machinata.Reporting.Responsive.LAYOUT_NAME_DESKTOP);
        if (pageLayout != Machinata.Reporting.Responsive.LAYOUT_NAME_TABLET) $("body").removeClass("machinata-page-layout-" + Machinata.Reporting.Responsive.LAYOUT_NAME_TABLET);
        if (pageLayout != Machinata.Reporting.Responsive.LAYOUT_NAME_MOBILE) $("body").removeClass("machinata-page-layout-" + Machinata.Reporting.Responsive.LAYOUT_NAME_MOBILE);
    }

};