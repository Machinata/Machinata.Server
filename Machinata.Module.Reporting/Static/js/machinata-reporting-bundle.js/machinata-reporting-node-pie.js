


/// <summary>
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.DonutNode</inherits>
Machinata.Reporting.Node.PieNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.PieNode.defaults = {};

/// <summary>
/// Donuts and pies have a ```solid``` chrome.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.chrome = "solid";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_1x1];

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.supportsToolbar = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.insertLegend = true;

/// <summary>
/// Defines the inner radius in decimal percent.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.innerRadius = 0.0;

/// <summary>
/// Defines the outer radius in decimal percent.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.outerRadius = 1.0;

/// <summary>
/// If ```true```, displays labels inside the slices with a slide angle larger than ```sliceLablesMinimumDegrees```.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.sliceLabels = true;

/// <summary>
/// If ```true```, includes the slice value in the label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.sliceLabelsShowValue = true;

/// <summary>
/// If ```true```, includes the slice category in the label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.sliceLabelsShowCategory = false;

/// <summary>
/// If ```true```, includes the slice value in the legend label.
/// By default ```false```.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.legendLabelsShowValue = false;


/// <summary>
/// If ```true```, includes the slice category in the label.
/// If this feature is used, care must be taken to ensure that the category size/length matches
/// the area given by the pie/donut slices.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.legendLabelsShowCategory = true;

/// <summary>
/// If ```sliceLabels``` is enabled, defines the minimum slice angle required for the label to display.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.sliceLabelsMinimumDegrees = 36;

/// <summary>
/// If ```sliceLabels``` is enabled, defines the offset from the outer edge in pixels.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.sliceLabelsOffsetPixels = 4;

/// <summary>
/// If ```sliceLabels``` is enabled, defines the font size in pixels for the labels.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.sliceLabelsSize = Machinata.Reporting.Config.chartTextSize;

/// <summary>
/// If ```sliceLabels``` is enabled, defines the maximum width a label can have (in pixels).
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.sliceLabelsLengthLimit = 160;

/// <summary>
/// If ```sliceLabels``` is enabled, defines whether slice labels should be placed on the inside or outside of the slices.
/// 
/// The following options are supported:
///  - ```inside-corner```: labels are placed on the inside corner of the slices, with the text rotated accordingly.
///  - ```inside-center```: labels are placed on the inside center of the slices.
///  - ```outside-center```: labels are placed on the outside of the slices, centered along the edge of the slice with the text aligned right or left depending on the quadrant. EXPERIMENTAL - DO NOT USE IN PRODUCTION.
/// 
/// By default ```inside-corner```.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.sliceLabelsPosition = "inside-corner";

/// <summary>
/// If set, sorts the facts automatically.
/// Value can either be ```descending``` or ```ascending```.
/// If no value is set, then the order of the data is used.
/// Note: For donuts/pies it is recommending to always use descending fact sort.
/// Note: Sorting the facts does not affect the legend order.
/// By default ```null```.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.sortFacts = null;

/// <summary>
/// If enabled, if the fact is between -1% and 1% (very small) or greater than 99.5% (very large, but not 100%),
/// we use the facts full formatting (```fact.format```), otherwise we use a standard full number percent format
/// that is simplified (ie 43% instead of 43.04%)...
/// For this setting to work, the fact format specified (```fact.format```) must be of type percent (ie '.2%').
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.PieNode.defaults.automaticallyFormatEdgeNumbers = true;


Machinata.Reporting.Node.PieNode.getVegaSpec = function (instance, config, json, nodeElem) {
    // Call parent
    return Machinata.Reporting.Node["DonutNode"].getVegaSpec(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.PieNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    // Call parent
    Machinata.Reporting.Node["DonutNode"].applyVegaData(instance, config, json, nodeElem, spec);
};
Machinata.Reporting.Node.PieNode.init = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["DonutNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.PieNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["DonutNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.PieNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["DonutNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};



Machinata.Reporting.Node.PieNode.exportExcelFormat = function (instance, config, json, nodeElem, format, filename) {

    var workbook = Machinata.Reporting.Export.createExcelWorkbook(instance, config, json);
    workbook = Machinata.Reporting.Export.createExcelDataForCategoryChart(instance, config, json, workbook,format);
    Machinata.Reporting.Export.saveExcelWorkbook(instance, config, json, workbook, filename);
    return true;
};



