
/// <summary>
/// A very simple and efficient node that allows the integration of external content (a 'portal').
/// 
/// ## Handler Integration
/// Typically content should be integrated on-the-fly using the following handlers:
///  - ```Machinata.Reporting.Handler.portalNodeReady```
///  - ```Machinata.Reporting.Handler.portalNodeCleanup```
/// 
/// Additionally for the following handlers may prove usefull:
///  - ```Machinata.Reporting.Handler.portalNodeResize```
///  - ```Machinata.Reporting.Handler.portalNodeDraw```
///
/// Using this method, the integrator will get a call to ```Machinata.Reporting.Handler.portalNodeReady```
/// when the the PortalNode is ready and placed in the DOM. From there the element can be manipulated
/// and appended freely.
/// ```portalElementId````can be used to keep track of what type of content should be inserted (or alternatively additional
/// properties on the node JSON).
///
/// ## Static Content Integration
/// An alternate method, is to provide a id-selector via ```portalExistingContentElementId```.
/// If this element can be found in the DOM, then it will be cloned (deeply, including event handlers) and appended
/// to the node. The existing content element can be hidden (using the ```display:none``` style),
/// and will automatically the unhidden when added to the report node.
///
/// ## CSS Stack
/// An integrator must be aware that the portal will be exposed to the ```machinata-reporting-styling``` CSS class.
/// This means that generic HTML elements (such as a H1 or P) will inherit a series of styles.
/// It is best practice to use the existing styles and tools that the reporting CSS stack provideds, if possible, to ensure
/// a consistent look and feel for the report.
///
/// ## Design Implications and Challenges
/// Some thought and care must be given to ensure that the integration of foreign content
/// within a report doesn't interfere with the report user experience, and that the overall
/// look-and-feel of the report is not compromised.
/// Furthermore, attention should be put on advanced features, such as fullscreen and web-printing.
/// In these cases, the portal content may need to adapt to ensure the functionality of the features.
///
/// </summary>
/// <example>
/// ## Handler Integration: Report JSON
/// ```
/// ...
/// {
///     "nodeType": "PortalNode",
///     "portalElementId": "portal-test-1",
///     "portalElementClass": "my-own-css-stack"
/// }
/// ...
/// ```
/// ## Handler Integration: Handlers
/// ```
/// Machinata.Reporting.Handler.portalNodeReady = function (instance, nodeJSON, nodeElem, portalElem) {
///     console.log("Machinata.Reporting.Handler.portalNodeReady", nodeJSON.portalElementId);
///     // Make a decision on what portal content to inject...
///     if (nodeJSON.portalElementId == "portal-test-1") {
///         console.log("Will create content for ", nodeJSON.portalElementId, "...");
///         portalElem.append($("<h1/>").text("This is external content that was created on-the-fly via PortalNode and Machinata.Reporting.Handler.portalNodeReady."));
///     }
/// };
/// Machinata.Reporting.Handler.portalNodeResize = function (instance, nodeJSON, nodeElem, portalElem) {
///     console.log("Machinata.Reporting.Handler.portalNodeResize", nodeJSON.portalElementId);
/// };
/// Machinata.Reporting.Handler.portalNodeDraw = function (instance, nodeJSON, nodeElem, portalElem) {
///     console.log("Machinata.Reporting.Handler.portalNodeDraw", nodeJSON.portalElementId);
/// };
/// Machinata.Reporting.Handler.portalNodeCleanup = function (instance, nodeJSON, nodeElem, portalElem) {
///     console.log("Machinata.Reporting.Handler.portalNodeCleanup", nodeJSON.portalElementId);
/// };
/// ```
/// </example>
/// <type>class</type>
Machinata.Reporting.Node.PortalNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.PortalNode.defaults = {};

/// <summary>
/// By default PortalNode have a ```light``` chrome.
/// </summary>
Machinata.Reporting.Node.PortalNode.defaults.chrome = "light";

/// <summary>
/// This node does not support being added to dashboards...
/// </summary>
Machinata.Reporting.Node.PortalNode.defaults.supportsDashboard = false;

/// <summary>
/// Typically not needed by this node.
/// </summary>
Machinata.Reporting.Node.PortalNode.defaults.addStandardTools = false;

/// <summary>
/// Not supported by this node.
/// </summary>
Machinata.Reporting.Node.PortalNode.defaults.addCopyToClipboardTool = false;

/// <summary>
/// Defines the element ID to add to the portal DOM element (if any).
/// Typically the exact type of content or node requested as a portal
/// can be derived from this id ```portalElementId```
/// </summary>
Machinata.Reporting.Node.PortalNode.defaults.portalElementId = null;

/// <summary>
/// Defines the element CSS class to add to the portal DOM element (if any).
/// </summary>
Machinata.Reporting.Node.PortalNode.defaults.portalElementClass = null;

/// <summary>
/// If defined, the content of the given existing DOM element (selected by its Id), will
/// be cloned and injected in the place of the portal. This can be useful for static sites
/// that wish to inject content into a report.
/// </summary>
/// <example>
/// ```<div id='my-portal'>Portal content...</div>```
/// ...
/// ```
/// {
///    "nodeType": "PortalNode",
///    "portalExistingContentElementId": "my-portal",
/// }
/// ```
/// </example>
Machinata.Reporting.Node.PortalNode.defaults.portalExistingContentElementId = null;

/// <summary>
/// </summary>
Machinata.Reporting.Node.PortalNode.init = function (instance, config, json, nodeElem) {

    // Init
    var portalElement = $("<div></div>");
    if (json.portalExistingContentElementId != null) {
        // See if this portal element already exists (by id)
        // If so, we use that
        var portalExistingContentElem = $("#" + json.portalExistingContentElementId);
        if (portalElement.length == 1) {
            portalElement = portalExistingContentElem.clone(true);
            portalElement.attr("id", ""); // make sure the original id is removed
            portalExistingContentElem.hide();
            portalElement.show();
        }
    }
    // Add classes/id
    portalElement.attr("id", json.portalElementId);
    if (json.portalElementClass != null) portalElement.addClass(json.portalElementClass);
    portalElement.addClass("machinata-reporting-portal");
    // Add to node DOM
    nodeElem.append(portalElement);
    // Register in state
    json._state.portalElement = portalElement;

};

/// <summary>
/// </summary>
Machinata.Reporting.Node.PortalNode.postBuild = function (instance, config, json, nodeElem) {
    if (Machinata.Reporting.Handler.portalNodeReady != null) {
        Machinata.Reporting.Handler.portalNodeReady(instance, json, nodeElem, json._state.portalElement);
    }
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.PortalNode.draw = function (instance, config, json, nodeElem) {
    if (Machinata.Reporting.Handler.portalNodeDraw != null) {
        Machinata.Reporting.Handler.portalNodeDraw(instance, json, nodeElem, json._state.portalElement);
    }
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.PortalNode.resize = function (instance, config, json, nodeElem) {
    if (Machinata.Reporting.Handler.portalNodeResize != null) {
        Machinata.Reporting.Handler.portalNodeResize(instance, json, nodeElem, json._state.portalElement);
    }
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.PortalNode.cleanup = function (instance, config, json, nodeElem) {
    if (Machinata.Reporting.Handler.portalNodeCleanup != null) {
        Machinata.Reporting.Handler.portalNodeCleanup(instance, json, nodeElem, json._state.portalElement);
    }
};








