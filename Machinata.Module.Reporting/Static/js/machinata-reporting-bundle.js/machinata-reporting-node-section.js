


/// <summary>
/// Inserts a basic title element (h1,h2,h3...).
/// These SectionNodes serve as a way to logically split a report into 
/// different chapters and sections, and can be used to build an overview or table-of-contents.
///
/// ## Title Level
/// To support different types of readers and print-modes, the SectionNode
/// will automatically create the appropriate hN-element based on it's ```titleLevel``` 
/// int setting or the style name. If the style name for example contains ```h1```, a html node
/// ```<h1/>``` will be inserted.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.SectionNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.SectionNode.defaults = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.SectionNode.defaults.supportsToolbar = false;

/// <summary>
/// If ```true```, the SectionNode is ignored if it has no ```style``` or the ```style``` is ```null```.
/// </summary>
Machinata.Reporting.Node.SectionNode.defaults.ignoreIfStyleIsNull = false;

/// <summary>
/// If set, defines the level of the h1,h2,h3... html element.
/// </summary>
Machinata.Reporting.Node.SectionNode.defaults.titleLevel = null;

/// <summary>
/// Accordion: Beta
/// </summary>
Machinata.Reporting.Node.SectionNode.defaults.accordion = false;

/// <summary>
/// Accordion: Beta
/// </summary>
Machinata.Reporting.Node.SectionNode.defaults.accordionCloseOthersWhenOpened = false;

/// <summary>
/// Accordion: Beta
/// </summary>
Machinata.Reporting.Node.SectionNode.defaults.accordionInitiallyClosed = false;

/// <summary>
/// If not null, defines the background shade to use for the title, creating a solid-colored section header.
/// </summary>
Machinata.Reporting.Node.SectionNode.defaults.titleBackgroundShade = null;

/// <summary>
/// Accordion: Beta
/// </summary>
Machinata.Reporting.Node.SectionNode.getAccordionChildNodes = function (instance, config, json, nodeElem) {
    return nodeElem.find("> .machinata-reporting-node");
};
/// <summary>
/// Accordion: Beta
/// </summary>
Machinata.Reporting.Node.SectionNode.getAccordionSiblings = function (instance, config, json, nodeElem) {
    return nodeElem.parent().find("> .machinata-reporting-node.type-SectionNode.option-accordion").not(nodeElem);
};
/// <summary>
/// Accordion: Beta
/// </summary>
Machinata.Reporting.Node.SectionNode.openAccordion = function (instance, config, json, nodeElem, allowAnimations) {
    Machinata.Reporting.Node.SectionNode.getAccordionChildNodes(instance, config, json, nodeElem).show();
    nodeElem.addClass("option-accordion-opened");
    nodeElem.removeClass("option-accordion-closed");
    console.log("openAccordion");
};
/// <summary>
/// Accordion: Beta
/// </summary>
Machinata.Reporting.Node.SectionNode.closeAccordion = function (instance, config, json, nodeElem, allowAnimations) {
    Machinata.Reporting.Node.SectionNode.getAccordionChildNodes(instance, config, json, nodeElem).hide();
    nodeElem.addClass("option-accordion-closed");
    nodeElem.removeClass("option-accordion-opened");
    console.log("closeAccordion");
};
/// <summary>
/// Accordion: Beta
/// </summary>
Machinata.Reporting.Node.SectionNode.toggleAccordion = function (instance, config, json, nodeElem, allowAnimations) {
    if (nodeElem.hasClass("option-accordion-opened")) {
        Machinata.Reporting.Node.SectionNode.closeAccordion(instance, config, json, nodeElem, allowAnimations);
        return false;
    } else {
        Machinata.Reporting.Node.SectionNode.openAccordion(instance, config, json, nodeElem, allowAnimations);
        return true;
    }
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.SectionNode.init = function (instance, config, json, nodeElem) {

    // Init
    var styleName = null;
    if (json.styles != null && json.styles.length > 0) styleName = json.styles[0];

    // Ignore?
    if (json.ignoreIfStyleIsNull == true && styleName == null) return;


    // Derive the title level (h1,h2,h3...) from the style
    if (styleName != null) {
        if (styleName.toLowerCase().contains("h1")) json.titleLevel = 1;
        else if (styleName.toLowerCase().contains("h2")) json.titleLevel = 2;
        else if (styleName.toLowerCase().contains("h3")) json.titleLevel = 3;
        else if (styleName.toLowerCase().contains("h4")) json.titleLevel = 4;
        else if (styleName.toLowerCase().contains("h5")) json.titleLevel = 5;
        else if (styleName.toLowerCase().contains("h6")) json.titleLevel = 6;
    }
    if (json.titleLevel == null) json.titleLevel = 1; // fallback

    // Title text
    var titleText = Machinata.Reporting.Text.resolve(instance, json.mainTitle);
    var titleElem = null;

    // Create H html element (only if we have content)
    if (titleText != null && titleText != "") {
        titleElem = $("<h" + json.titleLevel + "></h" + json.titleLevel + ">");
        titleElem.addClass("section-title");
        titleElem.text(titleText);
        titleElem.attr("id", "toc_" + Machinata.Reporting.uid()); // for automatic TOC systems
        if (styleName != null) titleElem.addClass(styleName);
        nodeElem.append(titleElem);
    } else {
        // Leave blank
    }

    // Accordion?
    if (config.automaticallyFoldChaptersIntoAccordion == true && json.chapter == true) {
        json.accordion = true;
    }
    if (json.accordion == true && titleElem != null) {
        nodeElem.addClass("option-accordion");

        // State icon
        var stateIcon = Machinata.Reporting.buildIcon(instance, "chevron-down");
        stateIcon.addClass("accordion-state");
        stateIcon.addClass("fullscreen-hidden");
        titleElem.append(stateIcon);

        // Interaction
        titleElem.click(function () {
            // Toggle self
            var didOpen = Machinata.Reporting.Node.SectionNode.toggleAccordion(instance, config, json, nodeElem, true);
            if (didOpen == true) {
                if (json.accordionCloseOthersWhenOpened == true) {
                    var siblings = Machinata.Reporting.Node.SectionNode.getAccordionSiblings(instance, config, json, nodeElem);
                    siblings.each(function () {
                        Machinata.Reporting.Node.SectionNode.closeAccordion(instance, config, json, $(this), true);
                    });
                }
            }
        });

    }

    // Background shade?
    if (json.styles != null && json.styles.length > 0) {
        for (var i = 0; i < json.styles.length; i++) {
            if (json.styles[i] == "W_Section_ColoredTitleBackground") {
                json.titleBackgroundShade = "mid";
            }
        }
    }
    if (json.titleBackgroundShade != null && titleElem != null) {
        var bgColor = Machinata.Reporting.getThemeBGColorByShade(config, json.theme, json.titleBackgroundShade);
        var fgColor = Machinata.Reporting.getThemeFGColorByShade(config, json.theme, json.titleBackgroundShade);
        titleElem.css("background-color", bgColor);
        titleElem.css("color", fgColor);
        titleElem.find("svg").css("fill", fgColor);
        nodeElem.addClass("style-W_TitlePadding");
    }
    

};

/// <summary>
/// </summary>
Machinata.Reporting.Node.SectionNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.SectionNode.postBuild = function (instance, config, json, nodeElem) {
    // Accordion
    if (json.accordion == true) {

        // Initial state
        if (json.accordionInitiallyClosed == true) {
            Machinata.Reporting.Node.SectionNode.closeAccordion(instance, config, json, nodeElem, false);
        } else {
            Machinata.Reporting.Node.SectionNode.openAccordion(instance, config, json, nodeElem, false);
        }
    }
};








