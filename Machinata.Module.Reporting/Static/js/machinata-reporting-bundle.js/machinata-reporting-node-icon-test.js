/// <summary>
/// A very simple node that displays all the required icons for the reporting system.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.IconTestNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.IconTestNode.defaults = {};

/// <summary>
/// This node does not support a toolbar.
/// </summary>
Machinata.Reporting.Node.IconTestNode.defaults.chrome = "solid";


Machinata.Reporting.Node.IconTestNode.init = function (instance, config, json, nodeElem) {

    var containerElem = $("<div class='node-bg'></div>");
    nodeElem.append(containerElem);

    function addIcon(name) {
        var iconElem = Machinata.Reporting.buildIcon(instance,name);
        iconElem.css("border", "1px solid black");
        iconElem.css("margin", "1rem");
        iconElem.attr("title",name);
        containerElem.append(iconElem);
    }
    addIcon("arrow-left");
    addIcon("arrow-right");
    addIcon("chevron-left");
    addIcon("chevron-right");
    addIcon("chevron-up");
    addIcon("chevron-down");
    addIcon("alert");
    addIcon("info");
    addIcon("trend-up");
    addIcon("trend-down");
    addIcon("fullscreen");
    addIcon("print");
    addIcon("check");
    addIcon("copy");
    addIcon("filter");
    addIcon("export");
    addIcon("download");
    addIcon("report");
    addIcon("configure");
    addIcon("cross");
    addIcon("glossary");
    addIcon("calendar");
    addIcon("chart");
    addIcon("share");
    addIcon("expand");
    addIcon("collapse");
    addIcon("list");
    addIcon("ask-question");
    addIcon("add-to-dashboard");
    addIcon("add-to-presentation");
    addIcon("debug");
    addIcon("more");
    addIcon("studio");
    addIcon("star");
};
Machinata.Reporting.Node.IconTestNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...
};








