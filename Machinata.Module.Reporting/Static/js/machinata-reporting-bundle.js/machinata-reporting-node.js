
/// <summary>
/// Provides a namespace for all node implementaions and common routines for building
/// and managine nodes and their tools.
/// 
/// Nodes are automatically created by the reporting layout, however you can
/// create node independently using the Machinata.Reporting.Node.build method.
///
/// ## JSON Data
/// Each node is driven by its JSON data-object. This extends (or overwrites) the node's implementation defaults.
/// 
/// ### Timestamps
/// In general, timestamps are always given as a Unix Epoch Milliseconds long.
///
/// ### Translatable (Resolvable) Texts
/// Texts which may be translated or parsed for arguments can be used instead of baked int strings.
/// It is highly recommend to always provide reports JSON with fully resolved text.
/// See ```Machinata.Reporting.Text.resolve``` for more details.
/// 
/// ## Barebones Implementation Example
/// A barebones node implementation looks like:
///
/// ```
/// Machinata.Reporting.Node.Example = {}; // Define new Node
/// Machinata.Reporting.Node.Example.defaults = {}; // Define any defaults your node needs
/// Machinata.Reporting.Node.Example.init = function (config, json, nodeElem) {}; // Your init routine, called only once on build
/// Machinata.Reporting.Node.Example.draw = function (config, json, nodeElem) {}, // Your draw routine, called when needed
/// ```
///
/// See the example implementation for more details.
///
/// ## Responsive and Redrawing
/// All nodes must be responsive, thus the init and draw methods are seperated. Whenever the report view
/// changes, your ```draw()``` method will be automatically called.
///
/// You can force a redraw of any node by calling the event ```machinata-reporting-redraw-node```:
/// 
/// ```
/// nodeElem.trigger("machinata-reporting-redraw-node");
/// ```
/// 
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Node = {};



/// <summary>
/// Node Defaults. 
/// Provides all the global node defaults that every node will inherit in its JSON.
/// Each node can extend or alter these defaults with its own defaults implementation.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Node.Defaults = {};

/// <summary>
/// If true, the node supports headless rendering without a DOM (browser) context.
/// Typically, all chart type nodes support headless rendering.
/// </summary>
Machinata.Reporting.Node.Defaults.supportsHeadlessRendering = false;

/// <summary>
/// If true, the node supports a toolbar and will automatically build a toolbar to accompany the node.
/// </summary>
Machinata.Reporting.Node.Defaults.supportsToolbar = true;

/// <summary>
/// If true, the node supports being added to a dashboard and the appropriate tool will be added to the nodes toolbar.
/// </summary>
Machinata.Reporting.Node.Defaults.supportsDashboard = false;

/// <summary>
/// Every node which supports the dashboard must return an array of strings which indicate which layout sizes for a dashboard the node supports, in order or preferered size.
/// Although every node is fully responsive, there are some design and practical limits which make sense for a node and its data.
///
/// See the layouts examples for more information on the grid layout standard sizes.
///
/// The default supported sizes are:
///  - ```Block1x1```: small square
///  - ```Block2x2```: large square
///  - ```Block2x1```: large square half horizontal
///  - ```Block4x2```: large full width
/// 
/// Atypical (not recommended) sizes, but technically possible:
///  - ```Block1x2```: large square half vertical
///  - ```Block4x1```: small full width
///  - ```Block4x4```: super large square
///
/// When possible, use the constants defined at ```Machinata.Reporting.Layouts.BLOCK_*```
///
/// The block sizes are easy to compute into hard values. For example, if your layout uses a 4-column grid layout (the standard),
/// then you can compute the size of any block using (not accounting for borders, padding, margins...):
///  - ```(containerWidth/4) * blockSize```
/// This means that a size of ```Block2x1``` will yield:
///  - width ```2```: ```(containerWidth/4) * 2 => (1200/4) * 2 = 600```
///  - height ```1```: ```(containerWidth/4) * 1 => (1200/4) * 1 = 300```
/// On mobile, the grid layout is usually halfed:
///  - width ```2```: ```(containerWidth/2) * 2 => (375/2) * 2 = 375```
///  - height ```1```: ```(containerWidth/2) * 1 => (375/2) * 1 = 187.5 = 187```
/// Note that you should always round fractions to avoid browsers from anti-aliasing off of the pixel grid (will cause bluriness in lines).
///
/// See also the ```nodeCurrentPixelSize``` in ```Machinata.Reporting.Node.compileNodeInfos()```.
/// See also the ```nodeCurrentBlockSize``` in ```Machinata.Reporting.Node.compileNodeInfos()```.
/// </summary>
/// <example>
/// The following example supports two sizes, with "Block2x1" the preffered size.
/// ```[Machinata.Reporting.Layouts.BLOCK_2x1,Machinata.Reporting.Layouts.BLOCK_4x2]```
/// </example>
Machinata.Reporting.Node.Defaults.supportedDashboardSizes = null;

/// <summary>
/// If true, the node has standard tools added to the main toolbar (supportsToolbar must be true as well).
/// </summary>
Machinata.Reporting.Node.Defaults.addStandardTools = true;

/// <summary>
/// When true, the node will automatically build all it's children. You can set this to false if you want to process your children by yourself.
/// </summary>
Machinata.Reporting.Node.Defaults.processChildren = true;

/// <summary>
/// Automatically add these classes (space seperated string) to the node element.
/// </summary>
Machinata.Reporting.Node.Defaults.additionalCSSClasses = null;

/// <summary>
/// If set, defines the main title for the node. 
/// Resolvable text.
/// Note: Setting this will add a node toolbar automatically, unless ```supportsToolbar```
/// has explicitly been set to ```false```.
/// </summary>
Machinata.Reporting.Node.Defaults.mainTitle = null;

/// <summary>
/// If set, defines the sub title for the node. 
/// Resolvable text.
/// Note: Without a ```mainTitle```, this is typically disregarded.
/// </summary>
Machinata.Reporting.Node.Defaults.subTitle = null;

/// <summary>
/// If set, defines the footer for the node. 
/// Resolvable text.
/// A node footer is automatically added to the bottom of the node.
/// </summary>
Machinata.Reporting.Node.Defaults.footer = null;

/// <summary>
/// If set to ```true```, this node (and all it's children) are marked by the index builder as a single 
/// screen. This is important if you want to use fullscreen and print composer functionalities.
///
/// ## User Experience
/// For the UX, screens should always span a single layout row, and typically will include just a single 
/// Node with toolbar (like a chart, paragraph, image). It is recommended to keep screens on a single over-arching
/// node depth. A screen should mostly fit onto a Full-HD layout (browser in fullscreen), however some cases (like a long table node) will not fit
/// on a screen all at once causing a scroll functionality.
/// </summary>
Machinata.Reporting.Node.Defaults.screen = null;

/// <summary>
/// If defined, an automatic info icon is attached to the node's main toolbar. Pressing the info button
/// will invoke a popup dialog (unless there is a custom implementation).
///
/// ## Info JSON
/// The info object can have the following properties:
///  - ```tooltip```: resolvable JSON text which is used the mouse-hover tooltip or accessibility
///  - ```title```: resolvable JSON text which is used for the dialog box title
///  - ```text```: resolvable JSON text which is used for the dialog box text
///  - ```wideDialog```: optional, a boolean indicating to show a wide dialog box for the info (by default false). Note that on mobile devices this usually has no effect.
/// </summary>
/// <example>
/// ```
/// "info": {
///     "tooltip": {
///        "resolved": "About SRRI risk profile"
///     },
///     "title": {
///         "resolved": "SRRI risk profile"
///     },
///     "text": {
///         "resolved": "The risk profile corresponds to the Synthetic Risk and Reward Indicator (SRRI). The SRRI is a measure of the overall risk and reward profile of a fund. Its calculation is based on the CESR 10-673 guidelines. Funds are categorised on a scale from 1 to 7, with 1 representing lowest risk and 7 representing highest risk. The fund�s position on this scale is a reasonably accurate reflection of the risk inherent in the fund under past market conditions. It does not reflect the risk inherent in any future circumstances or events that differ from what the fund has experienced in the past. Changes in the fund�s alignment within the boundaries set by its investment policy may also impact the fund�s risk."
///     }
/// }
/// ```
/// </example>
Machinata.Reporting.Node.Defaults.info = null;


/// <summary>
/// An optional string[] array that can define various tags (labels, markers) for a node.
/// 
/// Some tags are used to automatically extract information for a report, such as a disclaimer or legal entity.
/// Depending on the configuration, these nodes are excluded from the report index, since their content is used
/// in an overarching way (for example, the disclaimer is used across all chapters, not just at the end of the report).
/// 
/// Every tag is exposed to the CSS stack via ```tag-YOURTAG```.
/// 
/// ## Known Tags
///  - ```Report_Disclaimer```
///  - ```Report_LegalEntity```
///  - ```Screen```
///  - ```PageBreak```
///  - ```WebOnly```
///  - ```WebPrintOnly```
///  - ```PrintOnly```
///  - ```FullscreenOnly```
///  - ```DarkChrome```
///  - ```LightChrome```
///  - ```SolidChrome```
/// </summary>
/// <example>
/// ```
/// "tags": [ "Report_Disclaimer" ]
/// ```
/// </example>
Machinata.Reporting.Node.Defaults.tags = null;


/// <summary>
/// If defined, the node (and it's children) will accept take this theme to be used for graphics
/// and UI's such as charts. This property trickles down to the node's children until a child 
/// re-defines the theme.
///
/// ```theme``` defines color pallets and color scales for node contents, while ```chrome``` defines
/// the nodes user interface (typically the UI that encloses the nodes content). Thus a ```theme``` and a ```chrome``` can be combined.
/// 
/// Typically a theme is set for each chapter, though in some places a chapter might have mixed
/// themes - such as a overview.
///
/// Almost all nodes require a theme context (meaning either they set a theme or the theme is inherited from a parent node). Only NoOp Nodes do not need to have a theme (specifically the root node).
///
/// ## Registered Themes
/// Themes used by nodes should always be registered with ```Machinata.Reporting.registerTheme```,
/// ensuring that charting libraries have proper access to them.
/// 
/// ## Standard Themes
/// Each branding or project will have their own themes, however the following standard themes should exist:
///  - ```gray```: a standard gray color (or neutral color)
///  - ```infographic```: Used by charting frameworks, the solid color for infographics
/// </summary>
Machinata.Reporting.Node.Defaults.theme = null;


/// <summary>
/// If defined, the node will apply this specific style to the node. It should be avoided to use styles
/// within the node building runtime. Styles should generally only be used by the CSS stack.
///
/// The style is exposed to the CSS stack via ```style-YOURSTYLE```.
/// 
/// Note: it is not possible to define more than one style.
/// Note: the style ```NoStyle``` is reserved.
/// </summary>
Machinata.Reporting.Node.Defaults.style = null;


/// <summary>
/// If defined, the node will apply this specific style to the node. It should be avoided to use styles
/// within the node building runtime. Styles should generally only be used by the CSS stack.
///
/// The style is exposed to the CSS stack via ```style-YOURSTYLE```.
/// 
/// Note: it is not possible to define more than one style.
/// Note: the style ```NoStyle``` is reserved.
/// </summary>
Machinata.Reporting.Node.Defaults.styles = null;

/// <summary>
/// If defined, the node will apply this specific chrome-style to the nodes user interface.
///
/// ```theme``` defines color pallets and color scales for node contents, while ```chrome``` defines
/// the nodes user interface (typically the UI that encloses the nodes content). Thus a ```theme``` and a ```chrome``` can be combined.
/// 
/// Each node type typically defines it's own standard chrome it wants to use. For example, charts will define ```solid``` for their chrome,
/// while paragraphs will define ```light```.
///
/// The chrome is exposed to the CSS stack via ```chrome-YOURCHROME```.
///
/// ## Standard Chrome Styles
/// The following chrome styles are standardized, however each branding or project can provide their own chrome styles:
///  - ```solid```: a solid chrome with clear border and defined area. By default most Nodes will use this.
///  - ```dark```: Inverted chrome, or dark chrome. Used to make a node stand out.
///  - ```light```: a light and minimal interface, used especially for typography or infographics.
///
/// ## Using Tags
/// Instead of setting the chrome property explicitly, you can use the following tags instead:
///  - SolidChrome: ```solid```
///  - DarkChrome: ```dark```
///  - LightChrome: ```light```
/// </summary>
Machinata.Reporting.Node.Defaults.chrome = null;


/// <summary>
/// If defined, an automatic link (arrow) icon is attached to the node's main toolbar. Pressing the link button
/// will invoke either a drilldown to a specifc node or an external URL.
///
/// See Machinata.Reporting.Tools.openLink for more details.
/// </summary>
/// <example>
/// ### Example with node focus
/// ```
/// "link": {
///     "tooltip": {
///        "resolved": "About SRRI risk profile"
///     },
///     "nodeId": "rep-20190710-152228-nodexyz"
/// }
/// ```
/// ### Example with chapter change
/// ```
/// "link": {
///     "tooltip": {
///        "resolved": "About SRRI risk profile"
///     },
///     "chapterId": "rep-20190710-chapterxyz"
/// }
/// ```
/// ### Example with external link
/// ```
/// "link": {
///     "tooltip": {
///        "resolved": "Go to Nerves.ch"
///     },
///     "url": "https://nerves.ch",
///     "target": "_blank"
/// }
/// ```
/// ### Example with drilldown for table
/// ```
/// "link": {
///     "tooltip": {
///        "resolved": "Drilldown on AAA >10 years"
///     },
///     "nodeId": "rep-20190710-152228-nodexyz"
///     "drilldown": {
///         "filters": [
///             { 
///                 "columnId": "rating",
///                 "value": "AAA"
///             } ,
///             { 
///                 "columnId": "maturityBucket",
///                 "value": ">10 years"
///             } 
///         ]
///     }
/// }
/// ```
/// </example>
Machinata.Reporting.Node.Defaults.link = null;

/// <summary>
/// If ```true```, the export functionality (if any) is disabled and non-accessible.
/// </summary>
Machinata.Reporting.Node.Defaults.disableExport = null;










/// <summary>
/// Merges in the defaults from all sources (Machinata.Reporting.Node.Defaults, impl.defaults, profile defaults)
/// Note: this will modify your incomming ```json```.
/// </summary>
Machinata.Reporting.Node.mergeJSONWithDefaults = function (config,json) {
    // Create defaults and merge into json
    // The priority is as following (lowest least priority)
    //      1. Machinata.Reporting.Node.Defaults (universal node defaults)
    //      2. impl.defaults (node implementation defaults)
    //      2. profile defaults (defaults from profile.nodeDefaults)
    //      4. json (specific node json)
    // See https://codepen.io/dankrusi/pen/vqbKXm?editors=0011 for a jquery PoC
    // See https://api.jquery.com/jquery.extend/

    // Old method
    /*var defaults = Machinata.Reporting.Node.Defaults; // Global node defaults
    if (impl != null) defaults = $.extend(null, defaults, impl.defaults); // Implementation defaults
    json = $.extend(json, defaults, json);*/

    // New method

    // Get impl defaults
    var impl = Machinata.Reporting.Node[json.nodeType];
    var implementationDefaults = null;
    if (impl != null) implementationDefaults = impl.defaults;

    // Get profile defaults (Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["HBarNode"]);
    // We do this very safely since not everyone is providing profile node detaults...
    var profileDefaults = null;
    if (Machinata.Reporting.Profiles.Configs != null) {
        if (Machinata.Reporting.Profiles.Configs[config.profile] != null) {
            if (Machinata.Reporting.Profiles.Configs[config.profile]["nodeDefaults"] != null) {
                if (Machinata.Reporting.Profiles.Configs[config.profile]["nodeDefaults"][json.nodeType] != null) {
                    profileDefaults = Machinata.Reporting.Profiles.Configs[config.profile]["nodeDefaults"][json.nodeType];
                }
            }
        }
    }

    // Create merged json
    var jsonNew = {};
    Machinata.Util.extend(jsonNew, Machinata.Reporting.Node.Defaults); // 1
    if (implementationDefaults != null) Machinata.Util.extend(jsonNew, implementationDefaults); // 2
    if (profileDefaults != null) Machinata.Util.extend(jsonNew, profileDefaults); // 3

    Machinata.Util.extend(jsonNew, json); // 4
    Machinata.Util.extend(json, jsonNew); // Swap out: note this seems odd, but we must do this to ensure the correct priority as we want it. This final extend ensures that the object json has the correct new priority applied inline. This is important to ensure that future upstream methods see the same json
};

/// <summary>
/// Builds a node using the given config and node json.
/// The build method will automatically load the node implementation and call it's build sub-routines:
///  - ```preBuild```
///  - ```init```
///  - ```postBuild```
/// If a ```containerElem``` is given, then the node element is automatically appended to the container.
/// </summary>
Machinata.Reporting.Node.build = function (instance, config, json, containerElem) {

    // Load implememtnation
    var impl = Machinata.Reporting.Node[json.nodeType];


    // Create defaults and merge into json
    Machinata.Reporting.Node.mergeJSONWithDefaults(config, json);

    // Reset the state
    json._state = {};

    // Convert deprecated style to new format
    if (json.style != null) {
        if (json.styles == null) json.styles = [];
        json.styles.push(json.style);
    }

    // Create main DOM elements
    var nodeElem = $("<div class='machinata-reporting-node machinata-reporting-themable'></div>");
    nodeElem.attr("id", json.id);
    nodeElem.addClass("level-" + json.level);
    nodeElem.addClass("screen-" + json.screenNumber);
    nodeElem.addClass("type-" + json.nodeType);
    if (json.additionalCSSClasses != null) nodeElem.addClass(json.additionalCSSClasses);
    nodeElem.attr("data-node-id", json.id);
    nodeElem.attr("data-screen", json.screenNumber);
    nodeElem.attr("data-level", json.level);
    nodeElem.attr("data-type", json.nodeType);
    nodeElem.attr("data-chrome", json.chrome);
    nodeElem.data("json", json);
    // Register theme as css style
    if (json.theme != null) nodeElem.addClass("theme-" + json.theme);
    // Register layout as css style
    if (json.layout != null) nodeElem.addClass("layout-" + json.layout);
    // Register styles as css styles
    /*if (json.style != null) nodeElem.addClass("style-" + json.style);
    else nodeElem.addClass("style-NoStyle");*/
    if (json.styles == null || json.styles.length == 0) {
        nodeElem.addClass("style-NoStyle");
    } else {
        for (var i = 0; i < json.styles.length; i++) {
            nodeElem.addClass("style-" + json.styles[i]);
        }
    }
    // Register chrome as css style
    if (json.chrome != null) nodeElem.addClass("chrome-" + json.chrome);
    // Register tags as css tags
    if (json.tags != null) {
        for (var i = 0; i < json.tags.length; i++) {
            nodeElem.addClass("tag-" + json.tags[i]);
        }
    }

    // Toolbar
    if (json.supportsToolbar == true && impl != null) {
        // Validate we have the bare-minimum for a toolbar
        var title = Machinata.Reporting.Text.resolve(instance,json.mainTitle);
        var subtitle = Machinata.Reporting.Text.resolve(instance,json.subTitle);
        if (title != null) {

            // Create toolbar
            var toolbarElem = Machinata.Reporting.buildToolbar(instance, title, subtitle); // Note: sectional icons have been deprecated
            nodeElem.append(toolbarElem);
            nodeElem.addClass("option-toolbar");
            nodeElem.data("toolbar", toolbarElem);

            // Link
            if (json.link != null) {
                var tooltip = Machinata.Reporting.Text.resolve(instance,json.link.tooltip);
                Machinata.Reporting.addTool(instance, nodeElem, tooltip, "arrow-right", function () {
                    Machinata.Reporting.Tools.openLink(instance,json.link);
                });
            }

            // Standard tools
            if (json.addStandardTools != false) {

                // Fullscreen button (only if we have screens)
                if (instance.screens != null && instance.screens.length > 0) {
                    if (json.screenNumber != null) {
                        Machinata.Reporting.addTool(instance, nodeElem, Machinata.Reporting.Text.translate(instance, "reporting.nodes.fullscreen"), "fullscreen", function () {
                            Machinata.Reporting.Tools.toggleFullscreenForNode(instance, nodeElem);
                        });
                    } else {
                        console.warn("Machinata.Reporting.Node.build: a visible node does not have a screen assigned to it, even though screens are present in the report.", json);
                    }
                }
                // Print button (only if we have screens)
                if (instance.screens != null && instance.screens.length > 0) {
                    if (Machinata.Reporting.Handler.printNode != null) {
                        if (json.screenNumber != null) {
                            Machinata.Reporting.addTool(instance, nodeElem, Machinata.Reporting.Text.translate(instance, "reporting.nodes.print"), "print", function () {
                                var infos = Machinata.Reporting.Node.compileNodeInfos(instance, config, json);
                                Machinata.Reporting.Handler.printNode(instance, infos, json);
                            });
                        } else {
                            console.warn("Machinata.Reporting.Node.build: a visible node does not have a screen assigned to it, even though screens are present in the report.", json);
                        }
                    }
                }
                // Ask a question
                if (Machinata.Reporting.Handler.askAQuestion != null) {
                    Machinata.Reporting.addTool(instance, nodeElem, Machinata.Reporting.Text.translate(instance, "reporting.nodes.ask-question"), "ask-question", function () {
                        var infos = Machinata.Reporting.Node.compileNodeInfos(instance, config, json);
                        Machinata.Reporting.Handler.askAQuestion(instance, infos, json);
                        Machinata.Reporting.Tools.trackNodeEvent(instance, config, json, Machinata.Reporting.TRACKING_ACTION_NODE_ASKAQUESTION); // instance, config, nodeJSON, action, val
                    });
                }
                // Add to dashboard
                if (Machinata.Reporting.Handler.addToDashboard != null && json.supportsDashboard == true) {
                    Machinata.Reporting.addTool(instance, nodeElem, Machinata.Reporting.Text.translate(instance, "reporting.nodes.add-to-dashboard"), "add-to-dashboard", function () {
                        var infos = Machinata.Reporting.Node.compileNodeInfos(instance, config, json);
                        Machinata.Reporting.Handler.addToDashboard(instance, infos, json);
                        Machinata.Reporting.Tools.trackNodeEvent(instance, config, json, Machinata.Reporting.TRACKING_ACTION_NODE_ADDTODASHBOARD); // instance, config, nodeJSON, action, val
                    });
                }
                // Add to presentation
                if (Machinata.Reporting.Handler.addToPresentation != null) {
                    Machinata.Reporting.addTool(instance, nodeElem, Machinata.Reporting.Text.translate(instance, "reporting.nodes.add-to-presentation"), "add-to-presentation", function () {
                        var infos = Machinata.Reporting.Node.compileNodeInfos(instance, config, json);
                        Machinata.Reporting.Handler.addToPresentation(instance, infos, json);
                        Machinata.Reporting.Tools.trackNodeEvent(instance, config, json, Machinata.Reporting.TRACKING_ACTION_NODE_ADDTOPRESENTATION); // instance, config, nodeJSON, action, val
                    });
                }
                // Share node
                if (Machinata.Reporting.Handler.shareNode != null) {
                    Machinata.Reporting.addTool(instance, nodeElem, Machinata.Reporting.Text.translate(instance, "reporting.nodes.share"), "share", function () {
                        var infos = Machinata.Reporting.Node.compileNodeInfos(instance, config, json);
                        Machinata.Reporting.Handler.shareNode(instance, infos, json);
                        Machinata.Reporting.Tools.trackNodeEvent(instance, config, json, Machinata.Reporting.TRACKING_ACTION_NODE_SHARE); // instance, config, nodeJSON, action, val
                    });
                }
            }

            // Info?
            if (json.info != null) {
                var tooltip = Machinata.Reporting.Text.resolve(instance,json.info.tooltip);
                Machinata.Reporting.addTool(instance, nodeElem, tooltip, "info", function () {
                    Machinata.Reporting.Tools.showInfoDialog(instance, config, json.info);
                    Machinata.Reporting.Tools.trackNodeEvent(instance, config, json, Machinata.Reporting.TRACKING_ACTION_NODE_SHOWINFO); // instance, config, nodeJSON, action, val
                });
            }

            // Debug?
            if (Machinata.Reporting.isDebugEnabled() == true) {
                Machinata.Reporting.addTool(instance, nodeElem, Machinata.Reporting.Text.translate(instance, "reporting.nodes.debug"), "debug", function () {
                    Machinata.Reporting.Tools.debugMenuForNode(instance, config, json, nodeElem);
                });
            }
        }
    }


    // Init node
    if (impl != null) impl.init(instance, config, json, nodeElem);

    // Bind redraw events
    var NODE_EVENT_PROPOGATION_DEBUGGING = false;
    {
        // Bind redraw self
        nodeElem.on(Machinata.Reporting.Events.REDRAW_NODE_SIGNAL, function (event) {
            // Stop event from bubbling up, we handle it here and for every node...
            event.stopPropagation();

            Machinata.Reporting.setLoading(instance, true);

            // Rebuild context
            var eventNodeElem = $(this);
            var eventNodeId = eventNodeElem.attr("id");
            var eventNodeJSON = Machinata.Reporting.Node.getJSONByID(instance,eventNodeId);
            var eventNodeImpl = Machinata.Reporting.Node[eventNodeJSON.nodeType];
            if (NODE_EVENT_PROPOGATION_DEBUGGING == true) Machinata.Reporting.debug(Machinata.Reporting.Events.REDRAW_NODE_SIGNAL, eventNodeId, "(" + instance.uid + ")");

            // Resize
            Machinata.Reporting.Node.resize(instance, config, eventNodeJSON, eventNodeElem);

            // Call draw, if we have a implementation
            if (eventNodeImpl != null && eventNodeElem != null && eventNodeElem.is(":visible")) {
                // Debugging
                if (NODE_EVENT_PROPOGATION_DEBUGGING == true) console.log("REDRAW CMD " + eventNodeJSON.id + " " + eventNodeJSON.nodeType);
                // Implementation Resize
                if (eventNodeImpl.resize) eventNodeImpl.resize(instance, config, eventNodeJSON, eventNodeElem);
                // Implementation Redraw
                eventNodeImpl.draw(instance, config, eventNodeJSON, eventNodeElem);
            }

            Machinata.Reporting.setLoading(instance, false);
        });

        // Bind redraw self + children
        nodeElem.on(Machinata.Reporting.Events.REDRAW_NODE_AND_CHILDREN_SIGNAL, function (event) {

            // Stop event from bubbling up, we handle it here and for every node...
            // In this case, we actually want the reverse - we want to bubble down, not up...
            event.stopPropagation();

            Machinata.Reporting.setLoading(instance, true);

            // Rebuild context
            var eventNodeElem = $(this);
            var eventNodeId = eventNodeElem.attr("id");
            if (NODE_EVENT_PROPOGATION_DEBUGGING == true) Machinata.Reporting.debug(Machinata.Reporting.Events.REDRAW_NODE_AND_CHILDREN_SIGNAL, eventNodeId,"(" + instance.uid + ")");
            var eventNodeJSON = Machinata.Reporting.Node.getJSONByID(instance,eventNodeId);
            var eventNodeImpl = Machinata.Reporting.Node[eventNodeJSON.nodeType];

            // Resize
            Machinata.Reporting.Node.resize(instance, config, eventNodeJSON, eventNodeElem);

            // Redraw ourselves
            if (NODE_EVENT_PROPOGATION_DEBUGGING == true) console.log("REDRAW EVT   " + eventNodeJSON.id + " " + eventNodeJSON.nodeType);
            if (eventNodeImpl != null && eventNodeElem != null && eventNodeElem.is(":visible")) {
                // Debugging
                if (NODE_EVENT_PROPOGATION_DEBUGGING == true) console.log("REDRAW CMD " + eventNodeJSON.id + " " + eventNodeJSON.nodeType);
                // Implementation Resize
                if (eventNodeImpl.resize) eventNodeImpl.resize(instance, config, eventNodeJSON, eventNodeElem);
                // Implementation Draw
                eventNodeImpl.draw(instance, config, eventNodeJSON, eventNodeElem);
            }

            // Pass event to children
            if (eventNodeElem != null) {
                // We search for nodes dynamically (instead of node.children) since some nodes
                // dynamically create nodes on-the-fly that are untracked in the index
                // This is actually also faster, oddly
                eventNodeElem.find(".machinata-reporting-node").each(function () {
                    $(this).trigger(Machinata.Reporting.Events.REDRAW_NODE_SIGNAL);
                });
            }

            Machinata.Reporting.setLoading(instance, false);
        });

        // Bind cleanup self
        nodeElem.on(Machinata.Reporting.Events.CLEANUP_NODE_SIGNAL, function (event) {
            // Stop event from bubbling up, we handle it here and for every node...
            event.stopPropagation();

            // Rebuild context
            var eventNodeElem = $(this);
            var eventNodeId = eventNodeElem.attr("id");
            var eventNodeJSON = Machinata.Reporting.Node.getJSONByID(instance, eventNodeId);
            var eventNodeImpl = Machinata.Reporting.Node[eventNodeJSON.nodeType];
            if (NODE_EVENT_PROPOGATION_DEBUGGING == true) Machinata.Reporting.debug(Machinata.Reporting.Events.CLEANUP_NODE_SIGNAL, eventNodeId, "(" + instance.uid + ")");

            // Call draw, if we have a implementation
            if (eventNodeImpl != null && eventNodeElem != null) {
                // Implementation Cleanup
                if (eventNodeImpl.cleanup) eventNodeImpl.cleanup(instance, config, eventNodeJSON, eventNodeElem);
                // Handler
                if (Machinata.Reporting.Handler.cleanupNode != null) Machinata.Reporting.Handler.cleanupNode(instance, eventNodeJSON, eventNodeElem);
                // Note: actual remove from DOM is done later by REMOVE_NODE_SIGNAL
            }
        });

        // Bind remove self
        nodeElem.on(Machinata.Reporting.Events.REMOVE_NODE_SIGNAL, function (event) {
            // Stop event from bubbling up, we handle it here and for every node...
            event.stopPropagation();

            // Rebuild context
            var eventNodeElem = $(this);
            
            if (eventNodeElem != null) {
                // Actual remove from DOM
                eventNodeElem.remove();
            }
        });
    }

    // Register with responsive framework
    // Note: This is now handled at a later stage


    // Footer
    if (json.footer != null) {
        var text = Machinata.Reporting.Text.resolve(instance, json.footer);
        var nodeFooterElem = $("<div/>");
        nodeFooterElem.addClass("node-footer");
        if (text == "") {
            nodeFooterElem.html("&#160;"); // empty white space
        } else {
            nodeFooterElem.text(text);
        }
        nodeElem.append(nodeFooterElem);
        nodeElem.addClass("option-footer");
    }

    // Clearer
    //$("<div class='clear'></div>").appendTo(sectionElem);

    // Register in lookup tables
    instance.idToNodeElementLookupTable[json.id] = nodeElem;

    // Append to dom
    //TODO: can this be done later?
    nodeElem.appendTo(containerElem);

    // Initial draw
    // Note: this has been deprecated in favor of a single redraw at the end of reporting node build
    //if (impl != null) impl.draw(config, json, nodeElem);

    return nodeElem;
};


/// <summary>
/// Helper function for compiling some common useful infos for a specific node.
/// </summary>
/// <example>
/// ```
/// {
///     nodeId: "rep-20220524-170423-sample_OnlineReport-1-1_19",
///     nodeDefinitionId: "ov_top5_return",
///     nodeScreen: 22,
///     nodeSubtitle: "Commentary, 31.03.2019",
///     nodeTitle: "Market",
///     nodeURL: "https://domain.com/report/xyz/date/?focus=8392390294&chapter=chapter_overview",
///     nodeSupportedDashboardSizes: ["Block1x1","Block2x1"],
///     nodeCurrentPixelSize: {width: 580, height: 580}
///     nodeCurrentBlockSize: "Block4x1"
///     reportSubtitle: "31.12.2019",
///     reportTitle: "Investment Grade Global II",
///     reportURL: "https://domain.com/report/xyz/date/"
/// }
/// ```
/// </example>
Machinata.Reporting.Node.compileNodeInfos = function (instance, config, json) {
    // Infos based on generic node
    var infos = {};
    infos.nodeURL = json.url;
    infos.nodeId = json.id;
    infos.nodeDefinintionId = json.definitionId; // can be null
    infos.nodeScreen = json.screenNumber;
    infos.nodeTitle = Machinata.Reporting.Text.resolve(instance,json.mainTitle);
    infos.nodeSubtitle = Machinata.Reporting.Text.resolve(instance,json.subTitle);
    infos.nodeSupportedDashboardSizes = json.supportedDashboardSizes;
    infos.nodeCurrentPixelSize = Machinata.Reporting.Node.getSize(instance, config, json, Machinata.Reporting.Node.getElementByID(instance, json.id));
    infos.nodeCurrentBlockSize = Machinata.Reporting.Node.getBlockSize(instance, config, json, Machinata.Reporting.Node.getElementByID(instance, json.id));
    if (instance.reportData != null) infos.reportTitle = Machinata.Reporting.Text.resolve(instance, instance.reportData.reportTitle)
    if (instance.reportData != null) infos.reportSubtitle = Machinata.Reporting.Text.resolve(instance, instance.reportData.reportSubtitle)
    if (config != null) infos.reportURL = config.url;

    // Infos based on implementation
    // Not needed...
    //var impl = Machinata.Reporting.Node[json.nodeType];
    //if (impl != null) {
    //    
    //}

    return infos;
};



/// <summary>
/// Registers the export tool with the given options. 
/// Any export option will then call the export implementation method.
/// options: json key value.
/// </summary>
Machinata.Reporting.Node.registerExportOptions = function (instance, config, json, nodeElem, opts) {
    if (json.disableExport == true) return;

    Machinata.Reporting.addTool(instance, nodeElem, Machinata.Reporting.Text.translate(instance, "reporting.nodes.export"), "export", function () {


        Machinata.optionsDialog(Machinata.Reporting.Text.translate(instance, "reporting.nodes.export"), Machinata.Reporting.Text.translate(instance, "reporting.nodes.export.what-format"), opts)
            .okay(function (format) {
                Machinata.Reporting.setLoading(instance, true);

                // Get implementation
                var impl = Machinata.Reporting.Node[json.nodeType];

                // Create file title
                var title = Machinata.Reporting.Text.resolve(instance,json.mainTitle);
                var subtitle = Machinata.Reporting.Text.resolve(instance,json.subTitle);

                var filename = config.reportTitle;
                if (title != null) filename += " " + title;
                if (subtitle != null) filename += " " + subtitle;
                if (config.type != null) filename += " " + config.type;
                filename += "." + format;
                filename = filename.replaceAll(" ", "_");
                filename = filename.replaceAll("&", "-");
                filename = filename.replaceAll("/", "-");
                filename = filename.replaceAll(",", "");
                filename = filename.replaceAll("\\", "-");
                // Call implementation export
                setTimeout(function () {
                    var ret = impl.exportFormat(instance, config, json, nodeElem, format, filename);
                    Machinata.Reporting.setLoading(instance, false);
                    if (ret == false) {
                        Machinata.messageDialog(Machinata.Reporting.Text.translate(instance, "reporting.nodes.export"), Machinata.Reporting.Text.translate(instance, "reporting.nodes.export.format-no-available")).show()
                    } else {
                        var additionalInfos = { format: format, filename: filename };
                        Machinata.Reporting.Tools.trackNodeEvent(instance, config, json, Machinata.Reporting.TRACKING_ACTION_NODE_EXPORT_SUCCESS, format, additionalInfos); // instance, config, nodeJSON, action, val, additionalInfos
                    }
                }, config.loadingUITimeoutDelay);
                
            })
            .show();
    });
};

/// <summary>
/// Destroyes a node by its id and removes it from the DOM, calling all appropriate handlers and cleanup routines (if any).
/// </summary>
Machinata.Reporting.Node.destoryByID = function (id) {
    // Find node
    var nodeElem = $("#" + id);

    // Call node-specific cleanup
    // ...nothing yet todo here

    // Remove
    nodeElem.remove();
};


/// <summary>
/// Gets a node JSON by its id.
/// </summary>
Machinata.Reporting.Node.getJSONByID = function (instance, id) {
    return instance.idToNodeJSONLookupTable[id];
};

/// <summary>
/// Gets a node element by its id.
/// </summary>
Machinata.Reporting.Node.getElementByID = function (instance, id) {
    return instance.idToNodeElementLookupTable[id];
};



/// <summary>
/// Gets a node's size. This is computed either via the actual DOM node element, or if existing, the pre-configured width/height in the node JSON.
/// The caller is responsible for calling this method at the correct time (ie when the DOM element is properly hooked into the document).
/// </summary>
Machinata.Reporting.Node.getSize = function (instance, config, json, nodeElem) {

    // Try via node element
    if (nodeElem != null && nodeElem.length == 1) return { width: nodeElem.width(), height: nodeElem.height() };

    // Try via the pre-configured size
    if (json.width != null && json.height != null) return { width: json.width, height: json.height() };

    // Return empty size
    return { width: null, height: null };
    //throw "Machinata.Reporting.Node.getSize doesnt work unless the node is being called via draw()";
    
};


/// <summary>
/// Gets a node's block size (or nearest matching block size). This is computed using the current
/// report width and calculating backwards the logical block size based on a 4-column grid for desktop and
/// 2-column grid for mobile.
/// Note: Block sizes reflect the actual block size, and may differ from a officially supported block size.
/// This especially applies to nodes that don't use a block layout.
/// </summary>
Machinata.Reporting.Node.getBlockSize = function (instance, config, json, nodeElem) {
    // Sanity
    if (nodeElem == null || nodeElem.length == 0) return null;

    var gridColumns = 4;
    var size = Machinata.Reporting.Node.getSize(instance, config, json, nodeElem);
    var reportWidth = nodeElem.closest(".machinata-reporting-report").width();
    if (reportWidth < Machinata.Reporting.Responsive.LAYOUT_WIDTH_MOBILE) gridColumns = 2; //TODO: refactor this out
    var gridColumnWidth = reportWidth / gridColumns;
    var nodeBlockWidth = size.width / gridColumnWidth;
    var nodeBlockHeight = size.height / gridColumnWidth;

    var blockSize = "Block" + Math.round(nodeBlockWidth) + "x" + Math.round(nodeBlockHeight);
    return blockSize;

};

/// <summary>
/// </summary>
Machinata.Reporting.Node.resize = function (instance, config, json, nodeElem) {
    
    var layout = Machinata.Reporting.Responsive.getReportLayout(instance);

    if (json.responsiveSize != null) {
        var responsiveDefinitionForLayout = null;
        if (json.responsiveSize[layout] != null) responsiveDefinitionForLayout = json.responsiveSize[layout];
        else responsiveDefinitionForLayout = json.responsiveSize["default"];
        if (responsiveDefinitionForLayout != null) {
            var containerElem = nodeElem.parent();
            if (responsiveDefinitionForLayout.width != null) {
                var newSize = Machinata.Reporting.Layouts.calculateSizeForBlockOrColumn(instance, config, responsiveDefinitionForLayout.width, containerElem.width(), containerElem.height());
                nodeElem.css("width",newSize.width+"px"); // you cannot use the logical .width() since this takes box model into account
            }
            if (responsiveDefinitionForLayout.height) {
                var newSize = Machinata.Reporting.Layouts.calculateSizeForBlockOrColumn(instance, config, responsiveDefinitionForLayout.height, containerElem.width(), containerElem.height());
                nodeElem.css("height", newSize.height + "px"); // you cannot use the logical .width() since this takes box model into account
            }
            if (responsiveDefinitionForLayout.size) {
                var newSize = Machinata.Reporting.Layouts.calculateSizeForBlockOrColumn(instance, config, responsiveDefinitionForLayout.size, containerElem.width(), containerElem.height());
                nodeElem.css("width", newSize.width + "px"); // you cannot use the logical .width() since this takes box model into account
                nodeElem.css("height", newSize.height + "px"); // you cannot use the logical .width() since this takes box model into account
            }
        } else {
            console.warn("Machinata.Reporting.Node.resize: node responsiveSize was set but no layout could be resolved for "+layout+". You should set a default.");
        }
    }


};

/// <summary>
/// NOT YET SUPPORTED
/// </summary>
Machinata.Reporting.Node.widthInPercentOfContainer = function (config, json, nodeElem) {
    throw "This doesnt work unless the node is being called via draw()";
    //TODO
};



/// <summary>
/// NOT YET SUPPORTED
/// </summary>
Machinata.Reporting.Node.redrawChildren = function (config, json, nodeElem) {
    throw "NOT SUPPORTED";
    //TODO
};


/// <summary>
/// </summary>
/// <example>
/// ## Legend Data
/// ```
/// [
///    {
///       "title": {"resolved":"Aligned"},
///       "symbol": "square",
///       "colorShade": "positive-bg"
///    }
/// ]
/// ```
/// </example>
Machinata.Reporting.Node.createHTMLLegendElem = function (instance, config, nodeJSON, legendData, opts) {
    // Init
    if (opts == null) opts = {};

    // Legend
    var legendElem = $("<div class='legend'/>");
    for (var i = 0; i < legendData.length; i++) {
        var legendItem = legendData[i];
        var itemElem = $("<div class='legend-item'><div class='legend-symbol'></div><div class='legend-title'></div></div>").appendTo(legendElem);
        var color = legendItem.colorShade != null ?
            Machinata.Reporting.getThemeBGColorByShade(config, nodeJSON.theme || "default", legendItem.colorShade)
             :
            Machinata.Reporting.getThemeBGColorByIndex(nodeJSON.theme || "default", i);
        itemElem.find(".legend-title").text(Machinata.Reporting.Text.resolve(instance, legendItem.title));
        itemElem.find(".legend-symbol")
            .css("background-color", color)
            .addClass("symbol-" + legendItem.symbol)
            .addClass("shade-" + legendItem.colorShade);
    }
    return legendElem;
};