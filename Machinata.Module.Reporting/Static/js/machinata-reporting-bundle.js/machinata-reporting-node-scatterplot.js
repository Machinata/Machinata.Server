


/// <summary>
/// A graph-based scatterplot chart.
///
/// This node used the LineColumnNode underlying engine to display the data and drive the axes, thus
/// it shares the same data structure. 
/// 
/// Each serie must have the ```chartType``` of ```scatterplot```. 
/// 
/// Each fact may define the optional ```z``` property, which adjusts the fact symbol's size through a multiplier between
/// ```1``` and ```10```.
/// </summary>
/// <example>
/// See ```example-report-scatterplot.json``` for complete example usages.
/// </example>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.LineColumnNode</inherits>
Machinata.Reporting.Node.ScatterPlotNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults = {};

/// <summary>
/// By default charts are on solid chrome.
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.chrome = "solid";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_4x2];

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.supportsToolbar = true;

/// <summary>
/// Yes, insert a legend by default.
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.insertLegend = true;

/// <summary>
/// If ```true```, includes the serie title in the legend label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.legendLabelsShowSerie = true;

/// <summary>
/// Mark legend as interactive.
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.legendIsInteractive = Machinata.Reporting.Node.LineColumnNode.defaults.legendIsInteractive;

/// <summary>
/// Sets the number of columns to use for legends. By default we use ```2```.
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.legendColumns = 2;

/// <summary>
/// Sets the correct legend data source for the automatic legend.
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.legendDataSource = Machinata.Reporting.Node.LineColumnNode.defaults.legendDataSource;

/// <summary>
/// If true, a seperate x-zero rule is displayed.
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.insertXZeroRule = true;

/// <summary>
/// If true, a seperate y-zero rule is displayed.
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.insertYZeroRule = true;

/// <summary>
/// If defined, adds the given number of pixels to the y-scale.
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.yScalePadding = 20;

/// <summary>
/// If true, the x-axis grid is shown.
/// Default is ```true```.
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.showXAxisGrid = true;

/// <summary>
/// If true, the x-axis title is shown.
/// Default is ```true```.
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.showXAxisTitle = true;

/// <summary>
/// If true, the y-axis title is shown.
/// Default is ```true```.
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.showYAxisTitle = true;

/// <summary>
/// Set the global x-axis scale type. Can be any vega supported scale type as long as the data matches.
/// Note: For time-scales, you should ensure that the fact x-values are in Unix Epoch Format (milliseconds).
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.globalXAxisScaleType = "linear";

/// <summary>
/// If ```true```, labels will automatically be removed if they overlap the boundries of the axis by ```config.padding/2```.
/// By default ```true```,
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.globalXAxisLabelBound = true;

/// <summary>
/// For scatterplots, the symbol type.
/// Can be:
///  - ```circle```
///  - ```square```
///  - ```triangle```
///  - ```diamond```
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.scatterPlotSymbolType = "circle";

/// <summary>
/// The width and height size (bounding box) for scatterplot symbols, in pixels.
/// This value will be multiplied by the fact z value (if any).
/// By default this is ```Machinata.Reporting.Config.graphSymbolSize```.
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.scatterPlotSymbolSize = Machinata.Reporting.Config.graphSymbolSize;



/// <summary>
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.defaults.barMaxSize = 0;


Machinata.Reporting.Node.ScatterPlotNode.getVegaSpec = function (instance, config, json, nodeElem) {
    return Machinata.Reporting.Node.LineColumnNode.getVegaSpec(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.ScatterPlotNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    Machinata.Reporting.Node.LineColumnNode.applyVegaData(instance, config, json, nodeElem, spec);
};
Machinata.Reporting.Node.ScatterPlotNode.init = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["LineColumnNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.ScatterPlotNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["LineColumnNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.ScatterPlotNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["LineColumnNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};


/// <summary>
/// </summary>
Machinata.Reporting.Node.ScatterPlotNode.exportExcelFormat = function (instance, config, json, nodeElem, format, filename) {
    var workbook = Machinata.Reporting.Export.createExcelWorkbook(instance, config, json);
    workbook = Machinata.Reporting.Export.createExcelDataForSeriesGroupsChart(instance, config, json, workbook, format);
    Machinata.Reporting.Export.saveExcelWorkbook(instance, config, json, workbook, filename);
    return true;
};








