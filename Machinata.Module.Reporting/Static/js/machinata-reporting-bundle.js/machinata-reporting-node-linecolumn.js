


/// <summary>
/// ## Axis's
/// Scaling Engine: ```Machinata.Reporting.Node.VegaNode.Util.autoSnapMultiSeriesAxis```
/// </summary>
/// <example>
/// See ```example-report-linecolumns.json``` for complete example usages.
/// </example>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.LineColumnNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults = {};

/// <summary>
/// By default charts are on solid chrome.
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.chrome = "solid";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_4x2];

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.supportsToolbar = true;

/// <summary>
/// Yes, insert a legend by default.
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.insertLegend = true;

/// <summary>
/// If ```true```, includes the serie title in the legend label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.legendLabelsShowSerie = true;

/// <summary>
/// Mark legend as interactive.
/// Deprecated.
/// </summary>
/// <deprecated/>
Machinata.Reporting.Node.LineColumnNode.defaults.legendIsInteractive = false;

/// <summary>
/// Sets the number of columns to use for legends. By default we use ```2```.
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.legendColumns = 2;

/// <summary>
/// Defines the amount of padding (or spacing) for bar-groups (in percent relative to bandwidth).
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.barBandPadding = 0.2;

/// <summary>
/// If true, a seperate x-zero rule is displayed.
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.insertXZeroRule = false;

/// <summary>
/// If true, the x-axis grid is shown.
/// Default is ```false```.
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.showXAxisGrid = false;

/// <summary>
/// If true, the x-axis title is shown.
/// Default is ```false```.
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.showXAxisTitle = false;

/// <summary>
/// If true, the y-axis title is shown.
/// Default is ```false```.
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.showYAxisTitle = false;

/// <summary>
/// If true, a seperate y-zero rule at 0 is displayed.
/// Note that if this is enabled, it will force the axis domain to cover the zero-mark.
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.insertYZeroRule = true;

/// <summary>
/// Set the global x-axis scale type. Can be any vega supported scale type as long as the data matches.
/// Note: For time-scales, you should ensure that the fact x-values are in Unix Epoch Format (milliseconds).
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.globalXAxisScaleType = "time";

/// <summary>
/// If ```true```, labels will automatically be removed if they overlap the boundries of the axis by ```config.padding/2```.
/// By default ```true```,
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.globalXAxisLabelBound = true;

/// <summary>
/// When ```true```, the Y-Axis domains are automatically adjusted through different mechanisms
/// to make the axis' as pretty and nice as possible, especially when using multiple seriesGroups with
/// different axis domains.
///
/// With charts with multiple seriesGroups, each successive group's axis is always snapped to the first groups
/// domain through a multiple of 0.5.
///
/// Note: If a series-group contains a yAxis definitions with a minValue
/// and maxValue, then the autoSnap is not performed on this series. 
/// It is not recommended to mix manually-set axis domains with autoSnapped domains.
///
/// This feature spans across all Y-Axis (ie all series groups).
///
/// By default ```true```.
///  
/// See also:
///  - Machinata.Reporting.Node.LineColumnNode.defaults.autoSnapYAxisStategy
///  - Machinata.Reporting.Node.LineColumnNode.defaults.autoSnapYAxisMargin
///  - Machinata.Reporting.Node.LineColumnNode.defaults.autoSnapYAxisNiceDomain
///  - Machinata.Reporting.Node.LineColumnNode.defaults.autoSnapYAxisSnapToZero
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.autoSnapYAxis = true;

/// <summary>
/// When ```autoSnapYAxis``` is ```true```, defines the margin to add to the axis domain
/// in decimal percent.
/// By default ```0.05```, or 5%.
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.autoSnapYAxisMargin = 0.05;

/// <summary>
/// When ```autoSnapYAxis``` is ```true```, rounds the axis' domain to nice numbers.
/// Uses D3's domain scale ```nice()```.
/// By default ```true```
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.autoSnapYAxisNiceDomain = true;

/// <summary>
/// When ```autoSnapYAxis``` is ```true```, snaps the axis' domain min value to zero. This only happens
/// if all values are either ```> 0``` or all values are ```< 0```.
/// By default ```true``` 
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.autoSnapYAxisSnapToZero = true;

/// <summary>
/// Defines the strategy to use when adjusting the series groups y-axis automatically.
/// By default ```proportional```.
///
/// Valid strategies:
///  - ```proportional```: scales the axis domains proportionally, keeping a common domain ratio between all series
///  - ```none```: no additional strategy is applied (other than the nicing and margin)
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.autoSnapYAxisStategy = "proportional";

/// <summary>
/// If set and ```autoSnapYAxis``` is ```true```, this defines a target number of ticks
/// for all series y-axis. It is not gauranteed that axis's have exactly said number of ticks.
/// The closest human-friendly tick step and tick count will be used.
/// If unset, this configuration will use the global axis config value ```Machinata.Reporting.Config.axisTargetTickCount```
///
/// By default ```null```.
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.autoSnapYAxisTickCount = null;

/// <summary>
/// For scatterplots, the symbol type.
/// Can be:
///  - ```circle```
///  - ```square```
///  - ```triangle```
///  - ```diamond```
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.scatterPlotSymbolType = "circle";

/// <summary>
/// The width and height size (bounding box) for scatterplot symbols, in pixels.
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.scatterPlotSymbolSize = 16;


/// <summary>
/// Defines the maximum width of a bar in pixels. If there is not enough space, then the bar may be
/// smaller.
/// By default ```Machinata.Reporting.Config.verticalBarMaxSize```
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.barMaxSize = Machinata.Reporting.Config.verticalBarMaxSize;


/// <summary>
/// If true, tooltips for facts will include the x-axis value.
/// By default ```true```
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.tooltipsIncludeXAxisValue = true;

/// <summary>
/// If set, tooltips for facts will use this format for the x-axis value.
/// This is useful if you want to display the x-axis value in a different format than the x-axis format, for example of you use quarters Q1,Q2,etc for the x-axis labels, but the data is daily.
/// By default ```null``` and will fallback to the x-axis format.
/// </summary>
Machinata.Reporting.Node.LineColumnNode.defaults.tooltipsIncludeXAxisFormat = null;


Machinata.Reporting.Node.LineColumnNode.getVegaSpec = function (instance, config, json, nodeElem) {
    let factTooltipFormat = "'[title]' + scale('tooltipTitle',datum.factKey) + '[/title][items][item]' + format(datum.factY,scale('yAxisFormat','yAxisFormat')) + '[/item][/items]'";
    if (json.tooltipsIncludeXAxisValue == true) {
        let valueFormatType = "format";
        let valueFormat = "scale('xAxisFormat','xAxisFormat')";
        if (json.globalXAxisScaleType == "time") valueFormatType = "timeFormat";
        if (json.tooltipsIncludeXAxisFormat != null) valueFormat = json.tooltipsIncludeXAxisFormat;
        factTooltipFormat = "'[title]' + scale('tooltipTitle',datum.factKey) + '[/title][items][item][key]' + " + valueFormatType + "(datum.factX," + valueFormat +") + '[/key][val]' + format(datum.factY,scale('yAxisFormat','yAxisFormat')) + '[/val][/item][/items]'";
        
    }
    return {
        "data": [
          {
              "name": "seriesGroups",
              "values": null,
          },
          {
              "name": "allSeries",
              "source": "seriesGroups",
              "transform": [
                {
                    "type": "flatten",
                    "fields": [
                      "series"
                    ],
                    "as": [
                      "serie"
                    ]
                },
                {
                    "type": "formula",
                    "initonly": true,
                    "as": "key",
                    "expr": "datum.id + '_' + datum.serie.id"
                }
              ]
          },
          {
              "name": "allSeriesResolved",
              "source": "allSeries",
              "transform": [
                { "type": "formula", "initonly": true, "as": "colorResolved", "expr": "(datum.serie.colorShade ? scale('theme-bg',datum.serie.colorShade) : scale('colorByKey',datum.key))" },
                { "type": "formula", "initonly": true, "as": "titleResolved", "expr": "datum.serie.title.resolved" },
                { "type": "formula", "initonly": true, "as": "type", "expr": "datum.serie.chartType" },
                { "type": "formula", "initonly": true, "as": "xAxisTransform", "expr": "datum.xAxis.transform" },
                { "type": "formula", "initonly": true, "as": "xAxisTitleResolved", "expr": "datum.xAxis.title != null ? datum.xAxis.title.resolved : null" },
                { "type": "formula", "initonly": true, "as": "yAxisTitleResolved", "expr": "datum.yAxis.title != null ? datum.yAxis.title.resolved : null" },
              ]
          },
            {
                "name": "allFlattened",
                "source": "allSeriesResolved",
                "transform": [
                  {
                      "type": "flatten",
                      "fields": [
                        "serie.facts"
                      ],
                      "as": [
                        "fact"
                      ]
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "factY",
                      "expr": "datum.fact.y"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "factX",
                      "expr": "datum.fact.x"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "factZ",
                      "expr": "datum.fact.z"
                  }
                ]
            },
            {
                "name": "allFlattenedBars",
                "source": "allFlattened",
                "transform": [
                  {
                      "type": "filter",
                      "expr": "datum.type == 'column'"
                  }
                ]
            },
            {
                "name": "xZeroData",
                /*"_comment": "Dummy data set to drive displaying the x-zero rule",*/
                "values": [0],
                "transform": [
                  {
                      "type": "filter",
                      "expr": "" + json.insertXZeroRule + " == true"
                  }
                ]
            },
            {
                "name": "yZeroData",
                /*"_comment": "Dummy data set to drive displaying the y-zero rule",*/
                "values": [0],
                "transform": [
                  {
                      "type": "filter",
                      "expr": "" + json.insertYZeroRule + " == true"
                  }
                ]
            },
        ],
        "signals": [
            {
                "name": "globalXScalePadding",
                /*"_comment": "This is a pre-computed value for the global x scale padding left and right to ensure there is room for the bars on the edge. If there are no bars, then we just use the standard padding",*/
                "update": "if(   data('allFlattenedBars').length > 0,    width / data('allFlattenedBars').length , " + (config.padding*3)+")"
            },
        ],
        "scales": [
          {
              "name": "colorByKey",
              "type": "ordinal",
              "domain": { "data": "allSeries", "field": "key" },
              "range": { "scheme": json.theme + "-bg" }
          },
          {
              "name": "color",
              "type": "ordinal",
              "domain": { "data": "allSeriesResolved", "field": "key" },
              "range": { "data": "allSeriesResolved", "field": "colorResolved" },
          },
          {
              "name": "debugColor",
              "type": "ordinal",
              "domain": {
                  "data": "allSeriesResolved",
                  "field": "key"
              },
              "range": {
                  "scheme": "category20"
              }
          },
          {
              // Provide the proper color for the legend
              "name": "legendColor",
              "type": "ordinal",
              "domain": { "data": "allSeriesResolved", "field": "key" },
              "range": { "data": "allSeriesResolved", "field": "colorResolved" },
          },
            {
                "name": "tooltipTitle",
                "type": "ordinal",
                "domain": { "data": "allSeriesResolved", "field": "key" },
                "range": { "data": "allSeriesResolved", "field": "titleResolved" }
            },
            {
                // This is a workaround/hack for Vega using a fixed scale
                "name": "xAxisMinValue",
                "type": "ordinal",
                "domain": ["xAxisMinValue"],
                "range": { "data": "seriesGroups", "field": "xAxis.minValue" }
            },
            {
                // This is a workaround/hack for Vega using a fixed scale
                "name": "xAxisMaxValue",
                "type": "ordinal",
                "domain": ["xAxisMaxValue"],
                "range": { "data": "seriesGroups", "field": "xAxis.maxValue" }
            },
            {
                "name": "xScaleGlobal",
                /*"_comment": "This x-scale is used by everyone. Thus the chart groups are 'tight' with each other.",*/
                "type": json.globalXAxisScaleType,
                "domain": {
                    "data": "allFlattened",
                    "field": "factX"
                },
                "domainMin": {
                    "signal": "scale('xAxisMinValue','xAxisMinValue')",
                },
                "domainMax": {
                    "signal": "scale('xAxisMaxValue','xAxisMaxValue')",
                },
                "range": "width",
                "padding": { "signal": "globalXScalePadding" },
                //"padding": 30, // debugging only
            },
            {
                "name": "zScaleGlobal",
                "type": "linear",
                "domain": {
                    "data": "allFlattened",
                    "field": "factZ"
                },
                "range": [1,10],
            }
        ],
        "marks": [
          {
              /*"_comment": "Explodes the seriesGroups into individual group marks",*/
              "name": "groupGroups",
              "type": "group",
              "encode": {
                  "update": {
                      "width": {
                          "signal": "width"
                      },
                      "height": {
                          "signal": "height"
                      }
                  }
              },
              "from": {
                  "facet": {
                      "data": "seriesGroups",
                      "name": "facetGroups",
                      "groupby": "id"
                  }
              },
              "data": [],
              "marks": [
                {
                    /*"_comment": "Here we have a single group with all its series. At this stage we do an axis and scale",*/
                    "name": "groupSeries",
                    "type": "group",
                    "encode": {
                        "update": {
                            "width": {
                                "signal": "width"
                            },
                            "height": {
                                "signal": "height"
                            }
                        }
                    },
                    "data": [
                      {
                          "name": "series",
                          "source": "facetGroups",
                          "transform": [
                            {
                                "type": "flatten",
                                "fields": [
                                  "series"
                                ],
                                "as": [
                                  "serie"
                                ]
                            },
                            {
                                "type": "formula",
                                "initonly": true,
                                "as": "key",
                                "expr": "datum.id + '_' + datum.serie.id"
                            },
                            {
                                "type": "formula",
                                "initonly": true,
                                "as": "titleResolved",
                                "expr": "datum.serie.titleResolved"
                            },
                            {
                                "type": "formula",
                                "initonly": true,
                                "as": "chartType",
                                "expr": "datum.serie.chartType"
                            }
                          ]
                      },
                      {
                          "name": "flattened",
                          "source": "series",
                          "transform": [
                            {
                                "type": "flatten",
                                "fields": [
                                  "serie.facts"
                                ],
                                "as": [
                                  "fact"
                                ]
                              },
                              {
                                  "type": "formula",
                                  "initonly": true,
                                  "as": "factKey",
                                  "expr": "datum.key"
                              },
                            {
                                "type": "formula",
                                "initonly": true,
                                "as": "factY",
                                "expr": "datum.fact.y"
                            },
                            {
                                "type": "formula",
                                "initonly": true,
                                "as": "factX",
                                "expr": "datum.fact.x"
                              },
                              {
                                  "type": "formula",
                                  "initonly": true,
                                  "as": "factTitle",
                                  "expr": "(datum.fact && datum.fact.title) ? datum.fact.title.resolved : null"
                              },
                          ]
                      },
                      {
                          "name": "bars",
                          "source": "flattened",
                          "transform": [
                            {
                                "type": "filter",
                                  "expr": "datum.chartType == 'column'" // make sure we filter out null values as well
                            }
                          ]
                      },
                      {
                          "name": "lines",
                          "source": "flattened",
                          "transform": [
                            {
                                "type": "filter",
                                  "expr": "datum.chartType == 'line'" // note: lines should include null values, since we need to make sure we connect the dots with disjoint gaps (defined=false)
                            },
                            {
                                "type": "identifier", 
                                "as": "sortId"
                            },
                            {
                                "type": "collect",
                                "sort": {
                                    "field": "sortId",
                                    "order": ["descending"] // Lines are sorted descending (we want the first to be on top)
                                }
                            }
                          ]
                      },
                      {
                          "name": "scatterplots",
                          "source": "flattened",
                          "transform": [
                            {
                                "type": "filter",
                                "expr": "datum.chartType == 'scatterplot'"
                            }
                          ]
                      }
                    ],
                    "scales": [
                        {
                            // This is a workaround/hack for Vega using a fixed scale
                            "name": "xAxisFormat",
                            "type": "ordinal",
                            "domain": ["xAxisFormat"],
                            "range": { "data": "series", "field": "xAxis.format" }
                        },
                        {
                            // This is a workaround/hack for Vega using a fixed scale
                            "name": "xAxisFormatType",
                            "type": "ordinal",
                            "domain": ["xAxisFormatType"],
                            "range": { "data": "series", "field": "xAxis.formatType" }
                        },
                      {
                          "name": "xScaleBar",
                          "type": "band",
                          "domain": {
                              "data": "bars",
                              "field": "factX"
                          },
                          "range": "width",
                          "padding": json.barBandPadding
                      },
                        {
                            // This is a workaround/hack for Vega using a fixed scale
                            "name": "yAxisMinValue",
                            "type": "ordinal",
                            "domain": ["yAxisMinValue"],
                            "range": { "data": "series", "field": "yAxis.minValue" }
                        },
                        {
                            // This is a workaround/hack for Vega using a fixed scale
                            "name": "yAxisMaxValue",
                            "type": "ordinal",
                            "domain": ["yAxisMaxValue"],
                            "range": { "data": "series", "field": "yAxis.maxValue" }
                        },
                        {
                          "name": "yScale",
                          "type": "linear",
                          "domain": {
                              "data": "flattened",
                              "field": "factY"
                          },
                          "domainMin": {
                              "signal": "scale('yAxisMinValue','yAxisMinValue')",//"scale('yAxisMinValue','yAxisMinValue')",
                          },
                          "domainMax": {
                              "signal": "scale('yAxisMaxValue','yAxisMaxValue')",
                          },
                          "padding": config.yAxisLabelPadding,
                          //"padding": 30, // testing only
                          "range": "height",
                          "round": true, // setting round to false will cause a strange decoupled yaxis where the zero round is floating off
                            "zero": json._yAxisZero, // Boolean flag indicating if the scale domain should include zero. The default value is true for linear, sqrt and pow, and false otherwise.
                          "nice": json._yAxisNice // was true
                          //"nice": { "signal": "scale('yAxisTickCount','yAxisTickCount')" }
                          //"nice": true // setting nice to true will cause the axis domain to snap to a value, leading to a side affect that two axis' no longer align, setting it to false will cause the top/bottom of the bars/lines to hang over the edge of the yaxis
                      },
                        {
                            // This is a workaround/hack for Vega using a fixed scale
                            "name": "yAxisOrient",
                            "type": "ordinal",
                            "domain": ["yAxisOrient"],
                            "range": { "data": "series", "field": "yAxis.orient" }
                        },
                        {
                            // This is a workaround/hack for Vega using a fixed scale
                            "name": "yAxisFormat",
                            "type": "ordinal",
                            "domain": ["yAxisFormat"],
                            "range": { "data": "series", "field": "yAxis.format" }
                        },
                        {
                            // This is a workaround/hack for Vega using a fixed scale
                            "name": "yAxisFormatType",
                            "type": "ordinal",
                            "domain": ["yAxisFormatType"],
                            "range": { "data": "series", "field": "yAxis.formatType" }
                        },
                        {
                            // This is a workaround/hack for Vega using a fixed scale
                            "name": "seriesIndex",
                            "type": "ordinal",
                            "domain": ["seriesIndex"],
                            "range": { "data": "series", "field": "index" }
                        },
                        {
                            // This is a workaround/hack for Vega using a fixed scale
                            "name": "seriesGroupIndex",
                            "type": "ordinal",
                            "domain": ["seriesGroupIndex"],
                            "range": { "data": "series", "field": "groupIndex" }
                        }
                    ],
                    "axes": [

                        Machinata.Reporting.Node.VegaNode.Util.applySettingsToVegaAxis(instance, config, json,{
                            "orient": "bottom",
                            "scale": "xScaleGlobal",
                            "domain": false,
                            //"tickWidth": { "signal": "scale('seriesIndex','seriesIndex')" },
                            "format": { "signal": "if(scale('xAxisFormat','xAxisFormat') == null, ' ', scale('xAxisFormat','xAxisFormat'))" }, // If the format is null, use empty string ' ', otherwise 
                            "formatType": { "signal": "scale('xAxisFormatType','xAxisFormatType')" },
                            "values": { "signal": "scale('seriesIndex','seriesIndex') != 0 ? [] : data('series')[0].xAxis.values" }, // set values to [] if the series index is not 0, this ensures that values are set to nothing if not the first series, and thus preventing the axis from displaying multiple times
                            "tickValues": { "signal": "scale('seriesIndex','seriesIndex') != 0 ? [] : data('series')[0].xAxis.tickValues" }, // tickValues controls which tick lines to show (if set), set values to [] if the series index is not 0, this ensures that values are set to nothing if not the first series, and thus preventing the axis from displaying multiple times
                            "labelValues": { "signal": "scale('seriesIndex','seriesIndex') != 0 ? [] : data('series')[0].xAxis.labelValues" }, // tickValues controls which labels to show (if set), set values to [] if the series index is not 0, this ensures that values are set to nothing if not the first series, and thus preventing the axis from displaying multiple times
                            "title": (json.showXAxisTitle == true ? { "signal": "data('series')[0].xAxis.title != null ? data('series')[0].xAxis.title.resolved : null" } : null), // Vega can't not show a title other than if we set it to null hard, thus we doe this before compiling the spec (with an extra runtime check for safety)
                            "grid": json.showXAxisGrid,
                            "labelBound": (json.globalXAxisLabelBound == true ? (config.padding/2) : false), // Indicates if labels should be hidden if they exceed the axis range. If false (the default) no bounds overlap analysis is performed. If true, labels will be hidden if they exceed the axis range by more than 1 pixel. If this property is a number, it specifies the pixel tolerance: the maximum amount by which a label bounding box may exceed the axis range.
                            "tickCount": { "signal": "data('series')[0].xAxis.tickCount" },
                            //"translate": { "signal": "random()*20" }, // testing only
                            //"grid": true, // testing only
                            //"domain": true,  // testing only
                            //"labelFlush": true,  // testing only // Indicates if labels at the beginning or end of the axis should be aligned flush with the scale range. If a number, indicates a pixel distance threshold: labels with anchor coordinates within the threshold distance for an axis end-point will be flush-adjusted. If true, a default threshold of 1 pixel is used. Flush alignment for a horizontal axis will left-align labels near the beginning of the axis and right-align labels near the end. For vertical axes, bottom and top text baselines will be applied instead.
                            //"labelBound": false,  // testing only // Indicates if labels should be hidden if they exceed the axis range. If false (the default) no bounds overlap analysis is performed. If true, labels will be hidden if they exceed the axis range by more than 1 pixel. If this property is a number, it specifies the pixel tolerance: the maximum amount by which a label bounding box may exceed the axis range.
                        }),
                        // Unfortunately orient is not dynamic, thus we always render the left/right orient but just hide some specific settings...
                        Machinata.Reporting.Node.VegaNode.Util.applySettingsToVegaAxis(instance, config, json,{
                          "orient": "left",
                          "scale": "yScale",
                          "tickSize": 0,
                          "tickRound": true,
                          "format": { "signal": "scale('yAxisFormat','yAxisFormat')" },
                          "formatType": { "signal": "scale('yAxisFormatType','yAxisFormatType')" },
                          "title": (json.showYAxisTitle == true ? { "signal": "data('series')[0].yAxis.title != null ? data('series')[0].yAxis.title.resolved : null" } : null), // Vega can't not show a title other than if we set it to null hard, thus we doe this before compiling the spec (with an extra runtime check for safety)
                          "zindex": 1,
                          "grid": true, // always enabled but hidden if not first series
                          "domain": false,
                          "values": { "signal": "data('series')[0].yAxis.values" },
                            "tickValues": { "signal": "data('series')[0].yAxis.tickValues" }, // tickValues controls which tick lines to show (if set)
                            "labelValues": { "signal": "data('series')[0].yAxis.labelValues" }, // tickValues controls which labels to show (if set)
                            "encode": {
                              "labels": {
                                  "update": {
                                      "text": {
                                          "signal": "scale('yAxisOrient','yAxisOrient') == 'left' ? datum.label : ' '"
                                      }
                                  }
                              },
                              "grid": {
                                  "update": {
                                      "opacity": {
                                          "signal": "(scale('yAxisOrient','yAxisOrient') == 'left') && (scale('seriesGroupIndex','seriesGroupIndex') == 0) ? 1 : 0"
                                      }
                                  }
                              }
                          }
                      }),
                        Machinata.Reporting.Node.VegaNode.Util.applySettingsToVegaAxis(instance, config, json,{
                          "orient": "right",
                          "scale": "yScale",
                          "tickSize": 0,
                          "tickRound": true,
                          "format": { "signal": "scale('yAxisFormat','yAxisFormat')" },
                          "formatType": { "signal": "scale('yAxisFormatType','yAxisFormatType')" },
                          "title": (json.showYAxisTitle == true ? { "signal": "(scale('yAxisOrient','yAxisOrient') == 'right' && data('series')[0].yAxis.title != null) ? data('series')[0].yAxis.title.resolved : null" } : null), // Vega can't not show a title other than if we set it to null hard, thus we doe this before compiling the spec (with an extra runtime check for safety)
                          "zindex": 1,
                            "grid": true, // always enabled but hidden if not first series
                          "domain": false,
                          "values": { "signal": "data('series')[0].yAxis.values" },
                          "encode": {
                              "labels": {
                                  "update": {
                                      "text": {
                                          "signal": "scale('yAxisOrient','yAxisOrient') == 'right' ? datum.label : ' '"
                                      }
                                  }
                              },
                              "grid": {
                                  "update": {
                                      "opacity": {
                                          "signal": "(scale('yAxisOrient','yAxisOrient') == 'right') && (scale('seriesGroupIndex','seriesGroupIndex') == 0) ? 1 : 0"
                                      }
                                  }
                              }
                          }
                        }),
                    ],
                    "marks": [
                      {
                          /*"_comment": "BARS MASTER GROUPS - hold on to your seat: its about to get complicated: first we do a transfrom on our xScaleGlobal on factX to move to the proper position. This is offset to snap to the left edge, not the center of tick. Then we facet the bars by factX. At the stage we are grouped in on a region that the bars could theoretically fill entirely for their factX (ie left edge 'Jan' to right edge 'Jan'). Now we create a band scale for the bars to test if the calculated bar width is larger than maxBarSize. We save the bar width to barWidthAdjusted that limits the width. From this we create another band scale that takes this into account and has it's range to a custom width where the width is not the entire 'Jan' area, but rather the barWidthAdjusted*numberOfBars. Using this scale we draw the bars, each offset by the different of the totalBarWidthAdjusted (the actual combined width of the bars) and the entire width ('Jan' left edge to right edge)",*/
                          "type": "group",
                          "clip": true,
                          "zindex": 1,
                          "from": {
                              "facet": {
                                  "data": "bars",
                                  "name": "facetBars",
                                  "groupby": "factX"
                              }
                          },
                          "encode": {
                              "update": {
                                  "x": {
                                      "scale": "xScaleGlobal",
                                      "field": "factX",
                                      "offset": {
                                          "scale": "xScaleBar",
                                          "band": 1,
                                          "mult": -0.5
                                      }
                                  }
                              }
                          },
                            "signals": [
                                {
                                    "name": "width",
                                    /*"_comment": "This is the entire width space of the bars for this x value",*/
                                    "update": "bandwidth('xScaleBar')"
                                },
                                {
                                    "name": "barWidthRaw",
                                    /*"_comment": "This is the calculated width of an individual bar, if it where to be unbound",*/
                                    "update": "bandwidth('barPosRawScale')"
                                },
                                {
                                    "name": "barWidthAdjusted",
                                    /*"_comment": "This is the calculated width of an individual bar set to a limit of barMaxSize",*/
                                    "update": "min( barWidthRaw, " + json.barMaxSize + " )"
                                },
                                {
                                    "name": "barCount",
                                    /*"_comment": "The actual count of bars", //BUG: what if a single facet does not have the entire set of bars?*/
                                    "update": "data('facetBars').length"
                                },
                                {
                                    "name": "totalBarWidthAdjusted",
                                    /*"_comment": "This is the total width of our adjusted bars. We need this for the actual scale, barPosAdjustedScale",*/
                                    "update": "barWidthAdjusted * barCount"
                                }
                          ],
                          "scales": [
                            {
                                "name": "barPosRawScale",
                                  "type": "band",
                                "range": "width",
                                "domain": {
                                    "data": "facetBars",
                                    "field": "factKey"
                                }
                              }, {
                                  "name": "barPosAdjustedScale",
                                  "type": "band",
                                  "align": 0.5,
                                  "range": { "signal": "[0,totalBarWidthAdjusted]" },
                                  "domain": {
                                      "data": "facetBars",
                                      "field": "factKey"
                                  }
                              }
                          ],
                          "marks": [
                            {
                                "name": "bars",
                                "from": {
                                    "data": "facetBars"
                                },
                                "type": "rect",
                                "encode": {
                                    "enter": {
                                        "fill": { "signal": "scale('color',datum.key)" },
                                        "opacity": [
                                            {
                                                // Show only if we have a value
                                                "test": "datum.factY == null",
                                                "value": 0
                                            },
                                            { "value": null }, // show
                                        ],
                                        "tooltip": { "signal": factTooltipFormat },
                                    },
                                    "update": {
                                        "x": { "signal": "scale('barPosAdjustedScale',datum.factKey) + (width-totalBarWidthAdjusted)/2" }, // we add the difference in size to center the entire group of bars again
                                        "width": { "signal": "bandwidth('barPosAdjustedScale')" },
                                        "y": {
                                            "scale": "yScale",
                                            "field": "factY"
                                        },
                                        "y2": {
                                            "scale": "yScale",
                                            "value": 0
                                        }
                                    }
                                }
                            }
                          ]
                      },
                      {
                          /*"_comment": "LINES MASTER GROUPS",*/
                          "type": "group",
                          "clip": true,
                          "zindex": 3,
                          "from": {
                              "facet": {
                                  "data": "lines",
                                  "name": "facetLines",
                                  "groupby": "factKey"
                              }
                          },
                          "marks": [
                            {
                                "type": "line",
                                "from": {
                                    "data": "facetLines"
                                },
                                "encode": {
                                    "enter": {
                                        "stroke": {
                                            "scale": "color",
                                            "field": "key"
                                        },
                                        "strokeWidth": {
                                            "value": config.graphLineSize
                                        },
                                        "defined": {
                                            "signal": "datum.factY != null ? true : false" //dankrusi: probably no longer needed since null facts are filtered out
                                        }
                                    },
                                    "update": {
                                        "x": {
                                            "scale": "xScaleGlobal",
                                            "field": "factX",
                                        },
                                        "y": {
                                            "scale": "yScale",
                                            "field": "factY"
                                        },
                                    }
                                }
                            },
                            {
                                "type": "rect",
                                /*"_comment": "These are transparent rects for the tooltip",*/
                                "from": {
                                    "data": "facetLines"
                                },
                                "encode": {
                                    "enter": {
                                        "width": {
                                            "value": 10,
                                        },
                                        "height": {
                                            "value": 10,
                                        },
                                        "fill": {
                                            "value": "transparent"
                                        },
                                        "tooltip": [
                                            {
                                                // Show tooltip only if we have a value
                                                "test": "datum.factY == null",
                                                "value": null
                                            },
                                            { "signal": "'[title]' + scale('tooltipTitle',datum.factKey) + '[/title][items][item]' + format(datum.factY,scale('yAxisFormat','yAxisFormat')) + '[/item][/items]'" },
                                        ]
                                    },
                                    "update": {
                                        "xc": {
                                            "scale": "xScaleGlobal",
                                            "field": "factX",
                                            //"offset": {
                                            //    "scale": "xScaleBar",
                                            //    "band": 1,
                                            //    "mult": 0
                                            //}
                                        },
                                        "yc": {
                                            "scale": "yScale",
                                            "field": "factY"
                                        },
                                    }
                                }
                            }
                          ]
                      },
                      {
                          /*"_comment": "Y-ZERO RULE",*/
                          "from": {
                              "data": "yZeroData"
                          },
                          "name": "yzero",
                          "zindex": 4,
                          "type": "rule",
                          "encode": {
                              "enter": {
                                  "stroke": { "value": "black" },
                                  "strokeWidth": { "value": config.domainLineSize }
                              },
                              "update": {
                                  "x": { "value": 0 },
                                  "x2": { "signal": "width" },
                                  "y": { "signal": "scale('yScale',0)" }
                              }
                          }
                      },
                      {
                          /*"_comment": "X-ZERO RULE",*/
                          "from": {
                              "data": "xZeroData"
                          },
                          "name": "xzero",
                          "zindex": 4,
                          "type": "rule",
                          "encode": {
                              "enter": {
                                  "stroke": { "value": "black" },
                                  "strokeWidth": { "value": config.domainLineSize }
                              },
                              "update": {
                                  "y": { "value": 0 },
                                  "y2": { "signal": "height" },
                                  "x": { "signal": "scale('xScaleGlobal',0)" }
                              }
                          }
                      },
                      {
                          /*"_comment": "SCATTERPLOT MASTER GROUPS",*/
                          "type": "group",
                          "clip": false,
                          "zindex": 5,
                          "from": {
                              "facet": {
                                  "data": "scatterplots",
                                  "name": "facetScatterplots",
                                  "groupby": "factKey"
                              }
                          },
                          "marks": [
                            {
                                "type": "symbol",
                                "from": {
                                    "data": "facetScatterplots"
                                },
                                "encode": {
                                    "enter": {
                                        "fill": {
                                            "scale": "color",
                                            "field": "key"
                                        },
                                        "tooltip": { "signal": "'[title]' + scale('tooltipTitle',datum.factKey) + (datum.factTitle ? ': '+datum.factTitle : '') + '[/title][items]' + '[item][key]' +  datum.xAxis.title.resolved   + '[/key][val]' +   format(datum.factX,scale('xAxisFormat','xAxisFormat')) + '[/val][/item]' + '[item][key]' +  datum.yAxis.title.resolved   + '[/key][val]' +   format(datum.factY,scale('yAxisFormat','yAxisFormat')) + '[/val][/item]'  + '[/items]'"}
                                    },
                                    "update": {
                                        "shape": { "value": json.scatterPlotSymbolType },
                                        "size": { "signal": (json.scatterPlotSymbolSize * json.scatterPlotSymbolSize) + " * (datum.fact.z ? scale('zScaleGlobal',datum.fact.z) : 1)" },
                                        "xc": {
                                            "scale": "xScaleGlobal",
                                            "field": "factX"
                                        },
                                        "yc": {
                                            "scale": "yScale",
                                            "field": "factY"
                                        }
                                    }
                                }
                            }
                          ]
                      }
                    ]
                }
              ]
          }
        ]
    };
};
Machinata.Reporting.Node.LineColumnNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    // Insert series
    spec["data"][0].values = json.seriesGroups;
};




Machinata.Reporting.Node.LineColumnNode.init = function (instance, config, json, nodeElem) {

    // Init
    json._yAxisNice = true; // As a fallback we let vega do its nice...
    json._yAxisZero = json.insertYZeroRule; // axis needs zero if we want to show the rule...


    // Generate legend data
    Machinata.Reporting.Node.VegaNode.Util.createLegendForGroupedSeries(instance, config, json, json.seriesGroups);

    // Autosnap? We only do this if there are multiple series groups...
    if (json.autoSnapYAxis == true) {
        json._yAxisNice = false;
        json._yAxisZero = json.autoSnapYAxisSnapToZero;
        Machinata.Reporting.Node.VegaNode.Util.autoSnapMultiSeriesAxis(instance, config, json);
    }

    // Pre-process
    for (var sgi = 0; sgi < json.seriesGroups.length; sgi++) {
        var seriesGroup = json.seriesGroups[sgi];
        // Apply index
        seriesGroup.index = sgi;
        seriesGroup.groupIndex = sgi;
        // Preprocess axis
        Machinata.Reporting.Node.VegaNode.Util.preprocessAxisProperties(seriesGroup.xAxis);
        Machinata.Reporting.Node.VegaNode.Util.preprocessAxisProperties(seriesGroup.yAxis);
    }

    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.LineColumnNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.LineColumnNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.LineColumnNode.exportExcelFormat = function (instance, config, json, nodeElem, format, filename) {
    var workbook = Machinata.Reporting.Export.createExcelWorkbook(instance, config, json);
    workbook = Machinata.Reporting.Export.createExcelDataForSeriesGroupsChart(instance, config, json, workbook, format);
    Machinata.Reporting.Export.saveExcelWorkbook(instance, config, json, workbook, filename);
    return true;
};








