/// <summary>
/// A user interface for dynamically manipulating and exporting nodes.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Studio = {};


/// <summary>
/// </summary>
Machinata.Reporting.Studio.init = function (containerElem) {
    var instance = {
        elems: {
            ui: null,
            tools: null,
            toolsMenubar: null,
            toolsCode: null,
            toolsSettings: null,
            toolsExport: null,
            preview: null,
            previewNode: null,
        },
        nodeReportingConfig: null,
        nodeReportingInstance: null,
        nodeJSON: null,
        nodeProfile: "web",
        nodeSize: "responsive",
        studioReportingInstance: null,
    };
    {
        // Create a reporting instance to work with
        instance.studioReportingInstance = Machinata.Reporting._createEmptyInstance();
        instance.studioReportingInstance.config = Machinata.Reporting.mergeAndLoadConfigWithDefaults({}, "web");
    }
    instance.containerElem = containerElem;
    instance.elems.ui = $("<div class='machinata-reporting-studio'/>");

    // Initial config
    Machinata.Reporting.Studio.updateConfig(instance); //TODO

    // Build UI elems
    instance.elems.tools = $("<div class='tools'/>").appendTo(instance.elems.ui);
    instance.elems.toolsSettings = $("<div class='settings tool-panel option-scroll'></div>").appendTo(instance.elems.tools);
    instance.elems.toolsCode = $("<div class='code tool-panel'><textarea></textarea></div>").appendTo(instance.elems.tools);
    instance.elems.toolsExport = $("<div class='export tool-panel'></div>").appendTo(instance.elems.tools);
    instance.elems.preview = $("<div class='preview'/>").appendTo(instance.elems.ui);
    instance.elems.previewSizer = $("<div class='sizer'/>").appendTo(instance.elems.preview);
    if (instance.containerElem != null) instance.containerElem.append(instance.elems.ui);

    // Build tools toggle menubar
    instance.elems.toolsMenubar = $("<div class='menubar machinata-reporting-menubar'><div class='menu-items'></div></div>").appendTo(instance.elems.tools);
    instance.elems.toolsMenubar.addClass("option-hard");
    instance.elems.toolsMenubar.addClass("option-wide");
    instance.elems.toolsMenubar.find(".menu-items").append($("<a class='action-settings'>" + "Settings"+"</a>"));
    instance.elems.toolsMenubar.find(".menu-items").append($("<a class='action-code'>" + "Code"+"</a>"));
    instance.elems.toolsMenubar.find(".menu-items").append($("<a class='action-export'>" + "Export/Save"+"</a>"));

    // Build settings UI
    {
        // General
        var groupElem = $("<div class='group'/>").appendTo(instance.elems.toolsSettings);
        groupElem.append($("<h4/>").text("General"));
        instance.elems.toolsSettingsToolbarCheckbox = Machinata.Reporting.buildCheckbox(instance.studioReportingInstance, "Toolbar", false).appendTo(groupElem);
    }
    {
        // Chrome
        var groupElem = $("<div class='group'/>").appendTo(instance.elems.toolsSettings);
        groupElem.append($("<h4/>").text("Chrome"));
        instance.elems.toolsSettingsChromeLightCheckbox = Machinata.Reporting.buildCheckbox(instance.studioReportingInstance, "Light", false, "radio", "chrome", "light").appendTo(groupElem);
        instance.elems.toolsSettingsChromeSolidCheckbox = Machinata.Reporting.buildCheckbox(instance.studioReportingInstance, "Solid", false, "radio", "chrome", "solid").appendTo(groupElem);
        instance.elems.toolsSettingsChromeDarkCheckbox = Machinata.Reporting.buildCheckbox(instance.studioReportingInstance, "Dark", false, "radio", "chrome", "dark").appendTo(groupElem);
    }
    {
        // Profile
        var groupElem = $("<div class='group'/>").appendTo(instance.elems.toolsSettings);
        groupElem.append($("<h4/>").text("Profile"));
        instance.elems.toolsSettingsProfileWebCheckbox = Machinata.Reporting.buildCheckbox(instance.studioReportingInstance, "Web", false, "radio", "profile", "web").appendTo(groupElem);
        instance.elems.toolsSettingsProfilePrintCheckbox = Machinata.Reporting.buildCheckbox(instance.studioReportingInstance, "Print", false, "radio", "profile", "print").appendTo(groupElem);
    }
    {
        // Theme
        var groupElem = $("<div class='group'/>").appendTo(instance.elems.toolsSettings);
        groupElem.append($("<h4/>").text("Theme"));
        Machinata.Util.each(Machinata.Reporting._themesNew, function (key, val) {
            if (Machinata.String.startsWith(key, "text-on-")) return; // continue
            Machinata.Reporting.buildCheckbox(instance.studioReportingInstance, key, false, "radio", "theme", key).appendTo(groupElem);
        });
    }
    {
        // Size
        var groupElem = $("<div class='group size'/>").appendTo(instance.elems.toolsSettings);
        groupElem.append($("<h4/>").text("Size"));
        instance.elems.toolsSettingsSizeResponsiveCheckbox = Machinata.Reporting.buildCheckbox(instance.studioReportingInstance, "Responsive", false, "radio", "size", "responsive").appendTo(groupElem);
        instance.elems.toolsSettingsSizeCustomCheckbox = Machinata.Reporting.buildCheckbox(instance.studioReportingInstance, "Custom", false, "radio", "size", "custom").appendTo(groupElem);
        var customSizeGroupElem = $("<div class='custom-size'/>").appendTo(groupElem);
        instance.elems.toolsSettingsSizeCustomWidthInput = $("<input type='number' value='400'/>").appendTo(customSizeGroupElem);
        $("<span class='x'>x</span>").appendTo(customSizeGroupElem);
        instance.elems.toolsSettingsSizeCustomHeightInput = $("<input type='number' value='300'/>").appendTo(customSizeGroupElem);
        $("<span class='units'>px</span>").appendTo(customSizeGroupElem);
    }

    // Export UI
    instance.elems.toolsExportJSONButton = $("<button class='ui-button'/>").text("Save JSON").appendTo(instance.elems.toolsExport);
    instance.elems.toolsExport.append("<div/>");
    instance.elems.toolsExportSVGButton = $("<button class='ui-button'/>").text("Save SVG").appendTo(instance.elems.toolsExport);
    instance.elems.toolsExport.append("<div/>");
    instance.elems.toolsExportPNGButton = $("<button class='ui-button'/>").text("Save PNG").appendTo(instance.elems.toolsExport);

    // Helper functions
    function showSettings() {
        // Sync to settings UI
        if (instance.nodeJSON != null) {
            instance.elems.toolsSettingsToolbarCheckbox.find("input").prop("checked", instance.nodeJSON.supportsToolbar == true);
            instance.elems.toolsSettingsChromeLightCheckbox.find("input").prop("checked", instance.nodeJSON.chrome == "light");
            instance.elems.toolsSettingsChromeSolidCheckbox.find("input").prop("checked", instance.nodeJSON.chrome == "solid");
            instance.elems.toolsSettingsChromeDarkCheckbox.find("input").prop("checked", instance.nodeJSON.chrome == "dark");
            instance.elems.toolsSettings.find("input[name='theme'][value='" + instance.nodeJSON.theme + "']").prop("checked", true);
        }
        instance.elems.toolsSettingsProfileWebCheckbox.find("input").prop("checked", instance.nodeProfile == "web");
        instance.elems.toolsSettingsProfilePrintCheckbox.find("input").prop("checked", instance.nodeProfile == "print");
        instance.elems.toolsSettingsSizeResponsiveCheckbox.find("input").prop("checked", instance.nodeSize == "responsive");
        instance.elems.toolsSettingsSizeCustomCheckbox.find("input").prop("checked", instance.nodeSize == "custom");
        
        // Update UI
        instance.elems.toolsSettings.show();
        instance.elems.toolsCode.hide();
        instance.elems.toolsExport.hide();
        instance.elems.toolsMenubar.find(".action-settings").addClass("selected");
        instance.elems.toolsMenubar.find(".action-code").removeClass("selected");
        instance.elems.toolsMenubar.find(".action-export").removeClass("selected");
    }
    function showCode() {

        // Sync code
        var codeToShow = Machinata.Reporting.Tools.stringifyReportJSONWithoutCircularReferences(instance.nodeJSON);
        instance.elems.toolsCode.find("textarea").val(codeToShow);

        // Update UI
        instance.elems.toolsSettings.hide();
        instance.elems.toolsCode.show();
        instance.elems.toolsExport.hide();
        instance.elems.toolsMenubar.find(".action-settings").removeClass("selected");
        instance.elems.toolsMenubar.find(".action-code").addClass("selected");
        instance.elems.toolsMenubar.find(".action-export").removeClass("selected");
    }
    function showExport() {
        // Update UI
        instance.elems.toolsSettings.hide();
        instance.elems.toolsCode.hide();
        instance.elems.toolsExport.show();
        instance.elems.toolsMenubar.find(".action-settings").removeClass("selected");
        instance.elems.toolsMenubar.find(".action-code").removeClass("selected");
        instance.elems.toolsMenubar.find(".action-export").addClass("selected");
    }
    function updatePreview() {
        Machinata.Reporting.Studio.updatePreview(instance);
    }
    function updateConfig() {
        Machinata.Reporting.Studio.updateConfig(instance);
    }

    // Bind events
    instance.elems.toolsExportJSONButton.click(function () {
        Machinata.Data.exportDataAsBlob(btoa(JSON.stringify(instance.nodeJSON, null, 2)), "text/json", instance.nodeJSON.id + ".studio.json");
    });
    instance.elems.toolsExportSVGButton.click(function () {
        var impl = Machinata.Reporting.Node[instance.nodeJSON.nodeType];
        if (impl != null && impl.exportFormat != null) {
            impl.exportFormat(instance.nodeReportingInstance, instance.nodeReportingConfig, instance.elems.previewNode.data("json"), instance.elems.previewNode, "svg", instance.nodeJSON.id + ".studio.svg");
        } else {
            alert("This node type doesn't support this export format!");
        }
    });
    instance.elems.toolsExportPNGButton.click(function () {
        var impl = Machinata.Reporting.Node[instance.nodeJSON.nodeType];
        if (impl != null && impl.exportFormat != null) {
            impl.exportFormat(instance.nodeReportingInstance, instance.nodeReportingConfig, instance.elems.previewNode.data("json"), instance.elems.previewNode, "png", instance.nodeJSON.id + ".studio.png");
        } else {
            alert("This node type doesn't support this export format!");
        }
    });
    instance.elems.toolsMenubar.find(".action-settings").click(function () {
        showSettings();
    });
    instance.elems.toolsMenubar.find(".action-code").click(function () {
        showCode();
    });
    instance.elems.toolsMenubar.find(".action-export").click(function () {
        showExport();
    });
    $(window).resize(function () {
        if (instance.elems.previewNode != null) {
            Machinata.Reporting.Containerless.redrawNode(instance.elems.previewNode);
        }
    });
    instance.elems.toolsSettingsToolbarCheckbox.find("input").on("change", function () {
        if (instance.nodeJSON != null) {
            instance.nodeJSON.supportsToolbar = $(this).prop("checked");
            updatePreview();
        }
    });
    instance.elems.toolsSettings.find("input[name='chrome']").on("change", function () {
        if (instance.nodeJSON != null) {
            instance.nodeJSON.chrome = $(this).val();
            updatePreview();
        }
    });
    instance.elems.toolsSettings.find("input[name='theme']").on("change", function () {
        if (instance.nodeJSON != null) {
            instance.nodeJSON.theme = $(this).val();
            updatePreview();
        }
    });
    instance.elems.toolsSettings.find("input[name='size']").on("change", function () {
        instance.nodeSize = $(this).val();
        updatePreview();
    });
    instance.elems.toolsSettingsSizeCustomWidthInput.on("change", function () {
        updatePreview();
    });
    instance.elems.toolsSettingsSizeCustomHeightInput.on("change", function () {
        updatePreview();
    });
    instance.elems.toolsSettings.find("input[name='profile']").on("change", function () {
        instance.nodeProfile = $(this).val();
        if (instance.nodeProfile == "print") {
            instance.elems.toolsSettings.find(".group.size .units").text("mm");
        } else {
            instance.elems.toolsSettings.find(".group.size .units").text("px");
        }
        updateConfig();
        updatePreview();
    });
    instance.elems.toolsCode.find("textarea").on("keyup", function () {
        if (instance.codeChangeTimeout != null) clearTimeout(instance.codeChangeTimeout);
        instance.codeChangeTimeout = setTimeout(function () {
            instance.codeChangeTimeout = null;

            // Load JSON
            var rawJSONText = instance.elems.toolsCode.find("textarea").val();
            var newJSON = null;
            try {
                newJSON = JSON.parse(rawJSONText);
            } catch (e) {
                instance.elems.toolsCode.attr("title",e);
            }
            if (newJSON != null) {
                instance.elems.toolsCode.removeClass("has-error");
                instance.elems.toolsCode.attr("title", null);
                instance.nodeJSON = newJSON;
                updatePreview();
            } else {
                instance.elems.toolsCode.addClass("has-error");
            }
        }, 500);
    });

    // Bind helper functions
    instance.showSettings = function () {
        showSettings();
    };

    // Initial state
    showSettings();

    return instance;
};


/// <summary>
/// </summary>
Machinata.Reporting.Studio.initForNode = function (containerElem, nodeJSON) {
    var instance = Machinata.Reporting.Studio.init(containerElem);
    Machinata.Reporting.Studio.setNodeJSON(instance, nodeJSON);
    instance.showSettings(); // needed for resync...
    return instance;
};


/// <summary>
/// </summary>
Machinata.Reporting.Studio.setNodeJSON = function (instance, nodeJSON) {
    var filteredNodeJSON = JSON.parse(Machinata.Reporting.Tools.stringifyReportJSONWithoutCircularReferences(nodeJSON));
    filteredNodeJSON.level = null;
    filteredNodeJSON.enabled = true;
    filteredNodeJSON.exclude = false;
    filteredNodeJSON.responsiveSize = null;

    instance.nodeJSON = filteredNodeJSON;

    
    Machinata.Reporting.Studio.updatePreview(instance);
};

/// <summary>
/// </summary>
Machinata.Reporting.Studio.updateConfig = function (instance) {
    var configJSON = {}; //TODO
    instance.nodeReportingConfig = Machinata.Reporting.Containerless.getContainerlessConfigForProfile(instance.nodeProfile, configJSON);
};

/// <summary>
/// </summary>
Machinata.Reporting.Studio.updatePreview = function (instance) {

    // Destroy old
    if (instance.elems.previewNode != null) {
        Machinata.Reporting.Containerless.cleanupNode(instance.elems.previewNode);
        instance.elems.previewNode = null;
    }

    // Update size
    if (instance.nodeSize == "responsive") {
        instance.elems.preview.css("overflow", "hidden");
        instance.elems.previewSizer.css("width", "100%");
        instance.elems.previewSizer.css("height", "100%");
    } else {
        var wpx = parseInt(instance.elems.toolsSettingsSizeCustomWidthInput.val());
        var hpx = parseInt(instance.elems.toolsSettingsSizeCustomHeightInput.val());
        if (instance.nodeProfile == "print") {
            // Convert mm to px
            wpx = Machinata.Reporting.Tools.convertMillimetersToPixels(instance.nodeReportingConfig, wpx);
            hpx = Machinata.Reporting.Tools.convertMillimetersToPixels(instance.nodeReportingConfig, hpx);
        }
        instance.elems.preview.css("overflow", "auto");
        instance.elems.previewSizer.css("width", wpx+"px");
        instance.elems.previewSizer.css("height", hpx + "px");
    }

    // Get JSON

    instance.elems.previewNode = Machinata.Reporting.Containerless.buildNode(instance.nodeReportingConfig, instance.nodeJSON, instance.elems.previewSizer);
    instance.nodeReportingInstance = instance.elems.previewNode.data("reporting-instance");
    Machinata.Reporting.Containerless.redrawNode(instance.elems.previewNode);

};
