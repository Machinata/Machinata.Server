/// <summary>
/// A table node using HTML elements.
/// Supports filters, sticky headers, sorting and automatic grouping and merging of headers.
///
/// ## Cell Styles
/// Individual cells or entire rows can be styles using the ```styles``` property. 
/// Row groups, rows, and row facts may have a ```styles``` property set with one or more of the styles,
/// and are automatically merged according to their priority:
///  -Row Fact:     priority 1 (highest)
///  -Row:          priority 2
///  -Row Group:    priority 3 (lowest)
/// The following styles are supported:
///  -```textColor```: string, must be either ```red``` or  ```green```
///  -```faded```: boolean, if ```true``` the cell is faded in opacity
///  -```highlighted```: boolean, if ```true``` the cell is highlighted (usually bold)
/// Note: only text cells are supported (chart cells are not supported)
/// 
/// ## Sorting Rows
/// By default all tables can be sorted by clicking on the column headers. If a table has nested rows, then
/// only the first level is sorted. Row dimension groups always stay in place, regardless of the sort. This
/// means that all sorting happens within each row dimension group.
///
/// ## Filterting Rows
/// You can include a simple row search & filter tool by setting ```allowRowFiltering``` to ```true```.
/// Note that totals are not updated when filtering, as well as other cells that might lose 
/// meaning if neighboring rows are hidden due to filters. Thus one must consider the type of data to enable this feature on.
///
/// Filters can contain some minimal semantic:
///  - exact match: ```'text'```
///  - not: ```!'text'```
///  - less than: ```<100``` (also ```<=```)
///  - greater than: ```>100``` (also ```<=```)
///  - and: ```>100 and <102```
///  - or: ```'USD' or 'EUR'```
///  - range: ```10 - 30```
/// 
/// As soon as a filter contains logical semantic (such as or, and, !), texts must be escaped. This allows
/// for the best of both worlds: a lazy input such as "hello" with does a partial match search, and
/// the powerful mode with semantics for exact match filtering such as ("'hello'").
///
/// You can set pre-defined filters using the ```filters``` property.
///
/// ## Nested Rows
/// Rows can be nested by including an additional array of dimension elements as ```childRows``` for each row 
/// dimension element. For nested rows, especially when ```nestedRowsAreInteractive``` is ```true```, each row
/// can be preconfigured as expanded or collapsed through it's ```expanded``` property.
///
/// ## Linking Rows and Cells
/// Any cell (fact) can be linked by setting the ```link````property on the fact. 
/// Additionaly, an entire row can be linked by setting the row dimension element ```link````property.
/// Furthermore, one can also link only the row label (the first cell in the row) by setting the row dimension element ```labelLink````property.
/// A column header can be linked by setting the column dimension element ```labelLink````property. When linking column headers, one must ensure that other functionality such as ```allowRowSorting``` and ```allowRowFiltering``` are disabled. In general, it is not suggested to use column header links.
/// 
/// All links follow the standard linking API (see Machinata.Reporting.Node.Defaults.link).
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.VerticalTableNode = {};


/// <summary>
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults = {};

/// <summary>
/// By default, tables have a ```light``` chrome.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.chrome = "light";

/// <summary>
/// This node does not support being added to dashboards...
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.supportsDashboard = false;

/// <summary>
/// Defines the minimum number of rows that the table must have before the header
/// automatically becomes sticky.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.stickyHeadersMinRows = 5;

/// <summary>
/// If enabled, the user may configure the table columns through a basic configuration UI.
/// If you would like a column to be initially hidden, you can set the property ```display``` to
/// ```false``` on a column.
/// You can use this setting together with column ```importance``` and the node setting ```maxColumnImportanceForDisplay``` to
/// automatically set a grouped set of columns that are initially displayed to the user.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.allowColumnConfiguration = false;

/// <summary>
/// If enabled, the user may filter the table row. Row filtering should only be enabled
/// on large tables where searching for cells makes sense. Tables with calculated totals 
/// and row filtering may provide a confusing (and inaccurate) view. You cannot row filter
/// nested tables.
/// By default this is ```false```.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.allowRowFiltering = false;

/// <summary>
/// If enabled, the row filters are shown for all the columns of the table.
/// If ```allowRowFiltering``` is enabled, ```allowRowFiltering``` will show the filters when the user uses the
/// corresponding toolbar tool.
/// By default this is ```false```.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.showRowFiltering = false;

/// <summary>
/// If enabled, the user may sort the table rows using the column headers. 
/// If any header cell has a link, this feature is automatically disabled.
/// By default this is ```true```.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.allowRowSorting = true;

/// <summary>
/// If not null, header groups are only displayed if they meet this minimum count.
/// By default this is ```2``` (meaning that a single group is not shown).
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.headerGroupsMinimumRequired = 2;

/// <summary>
/// If true, header groups are not shown if they have no labels. This takes into account empty-string group labels as well 
/// (meaning that if all header groups have empty string labels, they will not be shown).
/// By default this is ```true```.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.headerGroupsHideIfNoLabels = true;

/// <summary>
/// If true, row group headers are not shown if they have no labels. This takes into account empty-string group labels as well 
/// By default this is ```true```.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.rowGroupHeadersHideIfNoLables = true;

/// <summary>
/// If true, header groups are spaced using DOM table cells.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.insertHeaderSpacerCells = false;

/// <summary>
/// If an integer, each column or column group is automatically evaluated to and it's ```display``` value is automatically
/// set based on if it has a lower or equal ```importance``` than ```maxColumnImportanceForDisplay```.
/// If ```null```, this property has no effect.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.maxColumnImportanceForDisplay = null;

/// <summary>
/// If ```true```, nested rows are able to be collapsed and expanded by the user.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.nestedRowsAreInteractive = true;

/// <summary>
/// If ```true```, grouped rows with header facts are able to be collapsed and expanded by the user.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.groupedRowsAreInteractive = true;

/// <summary>
/// If ```true```, the user can easily copy the table to clipboard.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.addCopyToClipboardTool = true;

/// <summary>
/// If ```true```, line-breaks (\n) will be rendered in the table data (cells).
/// This is achieved using the CSS stack and ```white-space: pre-line```
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.insertLineBreaksInCells = true;

/// <summary>
/// If set, a drilldown binding from this table (source table) to a other table (target table) is automatically created for all table cells.
/// This is especially useful if creating pivot-like tables, where one pivoted table data links to
/// the full data set.
///
/// To get the automatic system to work, one should set the following properties for 
/// ```automaticTableDrilldown```:
///  - ```tableNodeId```: the node id of the target table to which to drilldown to
///  - ```tableColumnIdForRow```: the target column id which is mapped from the source table
///  - ```tableColumnIdForColumn```: the target column id which is mapped from the source table
///  - ```tableColumnIdForTotal```: the source column id which represents the total column (needed to change the drilldown behaviour to on total columns)
///  - ```tableRowIdForTotal```: the source row id which represents the total row (needed to change the drilldown behaviour to on total rows)
/// 
/// Of course, the ```automaticTableDrilldown``` data must match exactly the ids of the 
/// tables which are to be bound together.
///
/// Internally, this feature will generate links on the various facts, column headers, or row labels using
/// the ```link.drilldown``` feature (see Machinata.Reporting.Node.Defaults.link).
///
/// Note: one can also individually bind rows via ```automaticTableDrilldown``` on a row element.

/// For more information on table filters, see ```Machinata.Reporting.Node.VerticalTableNode.defaults.filters```.
/// </summary>
/// <example>
/// ## Automtic drilldown:
/// ```
/// "automaticTableDrilldown": {
///     "tableNodeId": "tableForDrilldown1",
///     "tableColumnIdForRow": "Type",
///     "tableColumnIdForColumn": "Currency",
///     "tableColumnIdForTotal": "Total",
///     "tableRowIdForTotal": "Total"
/// }
/// ```
/// ## Custom Drilldown from table cell (fact) using filter value:
/// ```
/// "link": {
///     "nodeId": "tableForDrilldown1",
///         "drilldown": {
///         "filters": [
///             {
///                 "columnId": "Currency",
///                 "value": "EUR"
///             },
///             {
///                 "columnId": "Type",
///                 "expression": "!'BUY'"
///             }
///         ]
///     },
///     "tooltip": { "resolved": "Custom drilldown on BUY EUR" }
/// }
/// ```
/// ## Custom Drilldown from table cell (fact) using isAny/notAny:
/// ```
/// "link": {
///     "nodeId": "tableForDrilldown1",
///         "drilldown": {
///         "filters": [
///             {
///                 "columnId": "Currency",
///                 "isAny": ["EUR"]
///             },
///             {
///                 "columnId": "Type",
///                 "notAny": ["BUY"]
///             }
///         ]
///     },
///     "tooltip": { "resolved": "Custom drilldown on BUY EUR" }
/// }
/// ```
/// </example>
Machinata.Reporting.Node.VerticalTableNode.defaults.automaticTableDrilldown = null;


/// <summary>
/// If set, one can define a predefined set of filters to apply to the table.
/// Typically this is used along with ```allowRowFiltering``` and ```showRowFiltering```, however these
/// do not need to be enabled to apply the filtering.
///
/// The filters are provided as an array of filters, where each filter can have the following properties:
///  - ```columnId```: the id of the column to which the filter applies to (string) (required)
///  - ```name```: the name of the filter for a named filter. If this is provided then the filter will be visualized as a single (removable) tag in the filter input field. (string) (optional)
///
/// Each filter must have one of the following type of filtering properties (only one):
///  - ```value```: the string value to be filtered for. Values are automatically escaped (string) 
///  - ```expression```: a filter expression to filter for. For more details on expression see below. (string)
///  - ```isAny```: an array of strings to filter for using OR. This will automatically create the appropriate expression. (string array)
///  - ```notAny```: an array of strings to filter against using !AND. This will automatically create the appropriate expression. (string array)
///
/// ## Multiple Filters Per Column
/// When using multiple filters for a single column (ie multiple filters have the same columnId),
/// the filters are combined as AND. Adding multiple filters for a single column is only recommended via named filters,
/// as expression or value filters cannot be properly displayed to the user.
/// 
/// ## Expressions
/// Expressions in filters allow for some simple yet powerful syntax within filters. The expression language
/// has been purposefully crafted with the end-user in mind, which can use expressions from the table filters UI.
/// The expression parser behind the filters is provided by ```Machinata.Parser.parseTextFilter```.
/// 
/// The following filter expressions are supported:
///  - exact match: ```"keyword"```,```'keyword'```
///  - not: ```!"keyword"```, ```!100```
///  - less than: ```< 100```, ```<100```, ```<= 100```
///  - greater than: ```> 100```, ```>100```, ```>= 100```
///  - and: ```> 100 and < 102```
///  - or: ```'EUR' or 'USD'```
///  - range: ```10 - 30```
///
/// String literals are provided by the ```'``` or ```"``` character, and can be escaped through ```\'``` or ```\"``` characters.
/// The util method Machinata.Parser.escapeString can be used to escape a string.
///
/// Numbers are represented through the regex ```/^-?\d+\.?\d*$/``` and table cell content is normalized to basic types
/// using ```Machinata.Parser.normalizeStringInputToBasicType```, where some characters (```'``` and ```,```) are stripped before testing for number.
///
/// Note: when using expressions you must use the ```expression``` property.
/// 
/// </summary>
/// <example>
/// ```
/// ## Simple filters:
/// ```
/// "filters": [
///     {
///         "columnId": "Currency",
///         "value": "EUR"
///     },
///     {
///         "columnId": "Type",
///         "expression": "!'BUY'"
///     }
/// ]
/// ```
/// ## Filters using isAny/notAny:
/// ```
/// "filters": [
///     {
///         "columnId": "Currency",
///         "isAny": ["EUR"]
///     },
///     {
///         "columnId": "Type",
///         "notAny": ["BUY"]
///     }
/// ]
/// ```
/// ## Named filters:
/// ```
/// "filters": [
///     {
///         "columnId": "Currency",
///         "isAny": ["EUR","CHF","USD","JPY"],
///         "name": "Others"
///     },
///     {
///         "columnId": "Type",
///         "expression": "'Other' or 'Cash and Cash Equivalents' or 'FX'",
///         "name": "Others"
///     }
/// ]
/// ```
/// </example>
Machinata.Reporting.Node.VerticalTableNode.defaults.filters = null;

/// <summary>
/// If ```true```, table data is lazy loaded where the data is only loaded when the node's draw
/// routine is called. This is especially usefull for hidden tables (such as those behind a toggle) 
/// which are very large.
/// This applies for tables with more than```minimumRowsForLargeTables``` rows.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.automaticallyLazyLoadLargeTables = true;

/// <summary>
/// Defines the minimum number of rows for a table to be considered large.
/// This is used for features such as ```automaticallyLazyLoadLargeTables```
/// and ```automaticallyCollapseLargeTables```
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.minimumRowsForLargeTables = 1000;

/// <summary>
/// Defines the maximum number of rows to display for large tables when
/// ```automaticallyCollapseLargeTables``` is enabled.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.maximumRowsForCollapsedLargeTables = 20;

/// <summary>
/// If ```true```, table data is automatically truncated to ```maximumRowsForCollapsedLargeTables```
/// for large tables with more than ```minimumRowsForLargeTables``` rows.
/// This feature will automatically set the ```maximumNumberOfRowsToLoad``` setting.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.automaticallyCollapseLargeTables = true;

/// <summary>
/// If ```true```, when a table is collapsed (ie not fully loaded), then a option to load
/// all the data will be shown ontop of the loaded rows.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.collapsedTableShowLoaderOnTop = true;

/// <summary>
/// If ```true```, when a table is collapsed (ie not fully loaded), then a option to load
/// all the data will be shown beneath of the loaded rows.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.collapsedTableShowLoaderOnBottom = true;

/// <summary>
/// If not null, specifies the maximum number of rows to show
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.maximumNumberOfRowsToLoad = null;

/// <summary>
/// If true, header groups will have a vertical border between groups
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.showHeaderGroupBorders = true;

/// <summary>
/// If true, the last column will always be right-aligned. 
/// Can also be enabled with the style ```W_TableLastColumnNotRightAligned```.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.defaults.lastColumnRightAligned = false;

/// <summary>
/// Common helper routing for building a HTML table with the given table JSON data.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.buildTable = function (instance, config, json, nodeElem, opts) {
    
    // Options
    if (opts == null) opts = {};
    if (opts.interactive == null) opts.interactive = true; 
    if (opts.includeFormattingInformation == null) opts.includeFormattingInformation = false;

    // Stats
    if (json._state == null) json._state = {};
    json._state.buildTimestampStart = new Date();

    // Create table elem
    var tableElem = $("<table class='machinata-reporting-table'></table>");
    var tableHeadElem = $("<thead class='table-head'></thead>").appendTo(tableElem);

    // Init
    var allGroupLabelsEmpty = true;
    var columnIdToColumnMap = {}; // contains a map from column ids to their column defintions
    var columnIdToGroupMap = {}; // contains a map from column ids to their groups
    var groupNumber = 0;
    var dataRequiresStatsCompilation = false;
    var didAddRowLabel = false; // set to true if any row contains a label
    var numColumns = 0;

    // Process styles
    if (json.styles != null) {
        for (var i = 0; i < json.styles.length; i++) {
            if (json.styles[i] == "W_TableLastColumnRightAligned") json.lastColumnRightAligned = true;
        }
    }

    // Last column right-aligned?
    if (json.lastColumnRightAligned == true) {
        tableElem.addClass("option-last-column-right-aligned");
    }

    // Line breaks?
    if (json.insertLineBreaksInCells == true) {
        tableElem.addClass("option-white-space-as-newlines");
    }

    // Table cell styles helper method
    function applyCellStyles(elem, firstPriorityStyles, secondPriorityStyles, thirdPriorityStyles) {
        // Optimization
        if (firstPriorityStyles == null && secondPriorityStyles == null && thirdPriorityStyles == null) return;
        // Defaults
        var styles = {
            "faded": false,
            "highlighted": false,
            "textColor": null,
        }
        if (thirdPriorityStyles != null) {
            if (thirdPriorityStyles.faded != null) styles.faded = thirdPriorityStyles.faded;
            if (thirdPriorityStyles.highlighted != null) styles.highlighted = thirdPriorityStyles.highlighted;
            if (thirdPriorityStyles.textColor != null) styles.textColor = thirdPriorityStyles.textColor;
        }
        if (secondPriorityStyles != null) {
            if (secondPriorityStyles.faded != null) styles.faded = secondPriorityStyles.faded;
            if (secondPriorityStyles.highlighted != null) styles.highlighted = secondPriorityStyles.highlighted;
            if (secondPriorityStyles.textColor != null) styles.textColor = secondPriorityStyles.textColor;
        }
        if (firstPriorityStyles != null) {
            if (firstPriorityStyles.faded != null) styles.faded = firstPriorityStyles.faded;
            if (firstPriorityStyles.highlighted != null) styles.highlighted = firstPriorityStyles.highlighted;
            if (firstPriorityStyles.textColor != null) styles.textColor = firstPriorityStyles.textColor;
        }
        // Apply
        if (styles.faded == true) elem.addClass("style-faded");
        if (styles.highlighted == true) elem.addClass("style-highlighted");
        if (styles.textColor == "red") elem.addClass("style-red-text");
        else if (styles.textColor == "green") elem.addClass("style-green-text");
        else if (styles.textColor != null) console.warn("VerticalTableNode fact style color " + styles.color + " is not supported!");
    }

    // Preflight for column selection
    $.each(json.columnDimension.dimensionGroups, function (index, groupJSON) {
        // Validate importance
        if (json.maxColumnImportanceForDisplay != null && groupJSON.importance != null && groupJSON.importance > json.maxColumnImportanceForDisplay) {
            if (groupJSON.display == null) groupJSON.display = false;
        }
        $.each(groupJSON.dimensionElements, function (index, columnJSON) {
            // Validate importance
            if (json.maxColumnImportanceForDisplay != null && columnJSON.importance != null && columnJSON.importance > json.maxColumnImportanceForDisplay) {
                if (columnJSON.display == null) columnJSON.display = false;
            }
        });
    });

    // Header groups
    var headerGroupsElem = $("<tr class='header groups'></tr>");

    // Placeholder for labels
    var headerGroupsLabelElem = $("<th class='label' data-col-id='LABEL'></th>").appendTo(headerGroupsElem);
    headerGroupsLabelElem.addClass("is-empty");
    $.each(json.columnDimension.dimensionGroups, function (index, groupJSON) {

        // Don't display?
        if (groupJSON.display == false) {
            return;
        }

        groupNumber++;

        // Spacer
        if (groupNumber > 1 && json.insertHeaderSpacerCells == true) {
            $("<th class='spacer'>&#160;</th>").appendTo(headerGroupsElem);
        }
        // Create th elem
        var headerElem = $("<th></th>").appendTo(headerGroupsElem);
        headerElem.attr("colspan", groupJSON.dimensionElements.length);
        headerElem.attr("data-grp-id", groupJSON.id);
        headerElem.addClass("group-type-" + groupJSON.groupType);
        if (json.showHeaderGroupBorders == true && index != json.columnDimension.dimensionGroups.length-1) headerElem.addClass("group-border-right");
        if (groupJSON.valueType != null) headerElem.addClass("type-" + groupJSON.valueType);
        var groupText = Machinata.Reporting.Text.resolve(instance,groupJSON.name);
        if (groupText != null && groupText != "") allGroupLabelsEmpty = false;
        headerElem.text(groupText);
        headerElem.addClass("importance-" + groupJSON.importance);
    });
    // Add the header groups?
    // We only add header groups if the minimum number is reached and if they all have a label
    var addHeaderGroups = true;
    if (json.headerGroupsMinimumRequired != null && json.columnDimension.dimensionGroups.length < json.headerGroupsMinimumRequired) {
        addHeaderGroups = false;
    }
    if (json.headerGroupsHideIfNoLabels == true && allGroupLabelsEmpty == true) {
        addHeaderGroups = false;
    }
    if(addHeaderGroups == true) headerGroupsElem.appendTo(tableHeadElem);

    // Headers
    var headerRowElem = $("<tr class='header columns'></tr>").appendTo(tableHeadElem);
    var rowTemplateElem = $("<tr class='table-row'></tr>");
    groupNumber = 0;

    // Placeholder for labels
    var headerLabelElem = $("<th class='label' data-col-id='LABEL'></th>").appendTo(headerRowElem);//if (json.rowHeaderName != null) headerLabelElem.text(Machinata.Reporting.Text.resolve(instance,json.rowHeaderName));
    if (json.rowHeaderName != null) {
        headerLabelElem.text(Machinata.Reporting.Text.resolve(instance, json.rowHeaderName));
    } else {
        headerLabelElem.addClass("is-empty");
    }
    $("<td class='label' data-col-id='LABEL'></th>").appendTo(rowTemplateElem);
    $.each(json.columnDimension.dimensionGroups, function (indexGroup, groupJSON) {

        // Don't display?
        if (groupJSON.display == false) {
            return;
        }

        groupNumber++;

        // Spacer
        if (groupNumber > 1 && json.insertHeaderSpacerCells == true) {
            $("<th class='spacer'>&#160;</th>").appendTo(headerRowElem);
            $("<td class='spacer'>&#160;</th>").appendTo(rowTemplateElem);
        }
        // Columns
        $.each(groupJSON.dimensionElements, function (index, columnJSON) {
            numColumns++;

            // Don't display?
            if (columnJSON.display == false) {
                return;
            }

            // Init
            var headerTitle = "";
            if (columnJSON.columnLabel != null) headerTitle = Machinata.Reporting.Text.resolve(instance, columnJSON.columnLabel);
            else headerTitle = Machinata.Reporting.Text.resolve(instance, columnJSON.name);

            // Register in map
            columnIdToColumnMap[columnJSON.id] = columnJSON;
            columnIdToGroupMap[columnJSON.id] = groupJSON;

            // Automatic table drilldown for column header?
            var automaticTableDrilldown = columnJSON.automaticTableDrilldown || json.automaticTableDrilldown;
            if (opts.interactive == true && automaticTableDrilldown != null && columnJSON.labelLink == null) {
                var cellFilters = [];
                var drilldownColumnID = columnJSON.id;
                var drilldownColumnJSON = columnIdToColumnMap[columnJSON.id];
                var drilldownColumnValue = Machinata.Reporting.Text.resolve(instance, drilldownColumnJSON.name);
                var drilldownTooltipValue = "";
                if (drilldownColumnID != automaticTableDrilldown.tableColumnIdForTotal) {
                    cellFilters.push({
                        "columnId": automaticTableDrilldown.tableColumnIdForColumn,
                        "isAny": [drilldownColumnValue],
                        "name": drilldownColumnValue
                    });
                    drilldownTooltipValue += " " + drilldownColumnValue;
                }
                columnJSON.labelLink = {
                    "nodeId": automaticTableDrilldown.tableNodeId,
                    "tooltip": { "resolved": Machinata.Reporting.Text.translate(instance, "reporting.tables.drilldown-on") + drilldownTooltipValue },
                    "drilldown": { "filters": cellFilters },
                };
                if (cellFilters.length == 0) columnJSON.labelLink.tooltip.resolved = Machinata.Reporting.Text.translate(instance, "reporting.tables.drilldown-on-total");
            }

            // Create th elem
            var headerElem = $("<th></th>").appendTo(headerRowElem);
            headerElem.addClass("column");
            headerElem.addClass("importance-" + columnJSON.importance);
            headerElem.addClass("type-" + columnJSON.valueType);
            headerElem.addClass("group-type-" + groupJSON.groupType);
            headerElem.attr("data-col-id", columnJSON.id);
            headerElem.attr("data-filter-placeholder", Machinata.Reporting.Text.resolve(instance, columnJSON.filterPlaceholder));
            headerElem.text(headerTitle);
            if (index == groupJSON.dimensionElements.length - 1) {
                headerElem.addClass("last-in-group");
                if (addHeaderGroups && json.showHeaderGroupBorders == true && indexGroup != json.columnDimension.dimensionGroups.length - 1) headerElem.addClass("group-border-right");
            }

            // Label link?
            if (opts.interactive == true && columnJSON.labelLink != null) {
                headerElem.addClass("has-link");
                headerElem.attr("title", Machinata.Reporting.Text.resolve(instance, columnJSON.labelLink.tooltip));
                headerElem.click(function () {
                    if (columnJSON.labelLink.drilldown != null) {
                        tableElem.find(".selected-drilldown").removeClass("selected-drilldown");
                        headerElem.addClass("selected-drilldown");
                    }
                    Machinata.Reporting.Tools.openLink(instance, columnJSON.labelLink);
                });
                // Register so that we can later disable sorting
                json._state.headerLabelHasLink = true;
            }

            // Create td elem (for template)
            var tdElem = $("<td class='data'></td>").appendTo(rowTemplateElem);
            tdElem.attr("data-col-id", columnJSON.id);
            tdElem.addClass("type-" + columnJSON.valueType);
            tdElem.attr("data-type", columnJSON.valueType);

            // Special handling for grouped columns (such as bar charts)
            if (groupJSON.groupType == "bars") {
                var headerElemtext = "";
                if (columnJSON.columnLabel != null) headerElemtext = Machinata.Reporting.Text.resolve(instance, columnJSON.columnLabel);
                headerElem.text(headerElemtext);
                if (index == 0) {
                    headerElem.attr("colspan", groupJSON.dimensionElements.length);
                    tdElem.attr("colspan", groupJSON.dimensionElements.length);
                    headerElem.attr("data-th-grp-id", groupJSON.id);
                    tdElem.attr("data-td-grp-id", groupJSON.id);
                    dataRequiresStatsCompilation = true;
                } else {
                    headerElem.remove();
                    tdElem.remove();
                }
            }
        });

    });

    // Stats compilation?
    var tableStatsMinValue = {};
    var tableStatsMaxValue = {};
    if (dataRequiresStatsCompilation == true) {
        $.each(json.rowDimension.dimensionGroups, function (groupIndex, groupJSON) {
            $.each(groupJSON.dimensionElements, function (rowIndex, rowJSON) {
                // ROW
                $.each(rowJSON.facts, function (index, factJSON) {
                    // COLUMN
                    if (tableStatsMinValue[factJSON.col] == null || factJSON.val < tableStatsMinValue[factJSON.col]) {
                        tableStatsMinValue[factJSON.col] = factJSON.val;
                    }
                    if (tableStatsMaxValue[factJSON.col] == null || factJSON.val > tableStatsMaxValue[factJSON.col]) {
                        tableStatsMaxValue[factJSON.col] = factJSON.val;
                    }
                    var columnGroupJSON = columnIdToGroupMap[factJSON.col];
                    if (columnGroupJSON != null) {
                        if (tableStatsMinValue[columnGroupJSON.id] == null || factJSON.val < tableStatsMinValue[columnGroupJSON.id]) {
                            tableStatsMinValue[columnGroupJSON.id] = factJSON.val;
                        }
                        if (tableStatsMaxValue[columnGroupJSON.id] == null || factJSON.val > tableStatsMaxValue[columnGroupJSON.id]) {
                            tableStatsMaxValue[columnGroupJSON.id] = factJSON.val;
                        }
                    }
                });
            });
        });
    }
    tableElem.addClass("num-columns-" + numColumns);
    if (numColumns == 1) tableElem.addClass("option-single-column");
    else tableElem.addClass("option-multi-column");

    // Row groups
    groupNumber = 0;
    json._state.totalLoadedRows = 0;
    var maxRowColspan = 0;
    $.each(json.rowDimension.dimensionGroups, function (groupIndex, groupJSON) {

        // Register and create new tbody group
        groupNumber++;
        var tbodyElem = $("<tbody class='table-body'></tbody>").appendTo(tableElem);
        tbodyElem.addClass("importance-" + groupJSON.importance);
        tbodyElem.addClass("group-" + groupNumber);

        // Initial group header
        var groupName = Machinata.Reporting.Text.resolve(instance,groupJSON.name);
        var headerGroupRowJSON = null;
        var headerGroupRowElem = null;
        var headerGroupRowNeedsChildrenCollapsed = false;
        if ((groupName != null && groupName != "") || json.rowGroupHeadersHideIfNoLables == false) {
            // Create header row
            var rowElem = createRow(json._state.totalLoadedRows, groupJSON, groupJSON, 0, null, null);
            rowElem.addClass("group-header");
            rowElem.addClass("static"); // for tablesorter widget
            headerGroupRowElem = rowElem;
            // Extract label
            var labelElem = rowElem.find("[data-col-id='LABEL']");
            if (labelElem.length == 0) {
                labelElem = rowElem.find("td").first(); // if no label, fallback to first cell
            } else {
                didAddRowLabel = true;
            }
            // Calculate colspan
            var colSpan = 1;
            var tdi = 0;
            var tdElems = rowElem.find("td");
            while (tdi < tdElems.length) {
                tdi++; // we start at 1 (skipping the label, which we want to colspan)
                var tdElem = tdElems.eq(tdi);
                if (tdElem.hasClass("has-definition")) {
                    break;
                } else {
                    tdElem.addClass("mark-for-deletion");
                    colSpan++;
                }
            }
            rowElem.find("td.mark-for-deletion").remove();
            if (colSpan > 1) {
                labelElem.attr("colspan", colSpan);
                labelElem.addClass("is-spanning");
            }
            if (colSpan > maxRowColspan) maxRowColspan = colSpan;
            if (json.groupedRowsAreInteractive == true && groupJSON.dimensionElements.length > 0) {
                // Register class
                rowElem.addClass("has-child-rows");
                tableElem.addClass("has-nested-rows");
                // Make clickable
                if (opts.interactive == true) {
                    if (groupJSON.expanded == false) {
                        rowElem.addClass("collapsed");
                        headerGroupRowNeedsChildrenCollapsed = true;
                    } else {
                        rowElem.addClass("expanded");
                    }
                    rowElem.find("td").first().prepend(Machinata.Reporting.buildIcon(instance,"expand")).prepend(Machinata.Reporting.buildIcon(instance,"collapse"));
                    rowElem.addClass("toggable");
                    rowElem.click(function () {
                        Machinata.Reporting.Node.VerticalTableNode._toggleRowInternal(tableElem, $(this));
                    });
                    
                }
            }
            // Register
            headerGroupRowJSON = groupJSON;
        }
        tableElem.addClass("max-row-colspan-" + maxRowColspan);
        if (maxRowColspan > 1) {
            tableElem.addClass("has-row-with-colspan");
        }

        // Row create helper function
        function createRow(rowIndex, rowJSON, rowGroupJSON, rowIndent, parentRowJSON, parentRowElem) {
            // ROW
            var rowElem = rowTemplateElem.clone().appendTo(tbodyElem); // Note: clone is faster than generating html
            rowElem.attr("data-row-id", rowJSON.id);
            rowElem.addClass("importance-" + rowJSON.importance);
            var label = Machinata.Reporting.Text.resolve(instance, rowJSON.name);
            // Bookkeeping
            json._state.totalLoadedRows++;

            // Automatic table drilldown for entire row?
            var automaticTableDrilldown = rowJSON.automaticTableDrilldown || json.automaticTableDrilldown;
            if (opts.interactive == true && automaticTableDrilldown != null && rowJSON.labelLink == null) {
                var cellFilters = [];
                var drilldownRowValue = label;
                var drilldownRowId = rowJSON.id;
                var drilldownTooltipValue = "";
                if (drilldownRowId != automaticTableDrilldown.tableRowIdForTotal) {
                    cellFilters.push({
                        "columnId": automaticTableDrilldown.tableColumnIdForRow,
                        "isAny": [drilldownRowValue],
                        "name": drilldownRowValue
                    });
                    drilldownTooltipValue += " " + drilldownRowValue;
                }
                rowJSON.labelLink = {
                    "nodeId": automaticTableDrilldown.tableNodeId,
                    "tooltip": { "resolved": Machinata.Reporting.Text.translate(instance, "reporting.tables.drilldown-on") + drilldownTooltipValue },
                    "drilldown": { "filters": cellFilters },
                };
                if (cellFilters.length == 0) rowJSON.labelLink.tooltip.resolved = Machinata.Reporting.Text.translate(instance, "reporting.tables.drilldown-on-total");
            }
            
            // Parent or header?
            if (parentRowJSON != null && parentRowElem != null) {
                // Has a parent row
                rowElem.addClass("is-child-row");
                rowElem.addClass("tablesorter-childRow"); // for tablesorter widget
                rowElem.attr("data-parent-row-id", parentRowJSON.id);
                json.allowRowFiltering = false; // do not allow row filtering!
            } else if (json.groupedRowsAreInteractive == true && headerGroupRowJSON != null) {
                // Has header group
                rowElem.attr("data-parent-row-id", headerGroupRowJSON.id);
                if (groupJSON.expanded == false) {
                    rowElem.addClass("hidden");
                }
            }

            //TODO: 
            //if (rowJSON["class"] != null) rowElem.addClass("class-" + rowJSON["class"]);
            //if (rowJSON["class"] == "total") rowElem.addClass("static").attr("data-row-index", "100%");

            // Row label
            if (label != null && label != "") {
                didAddRowLabel = true;
                var labelElem = rowElem.find("[data-col-id='LABEL']"); //TODO: this is probably a performance bottleneck
                labelElem.text(label);
                labelElem.attr("data-text", rowJSON.sort || label);
                labelElem.attr("data-sort", rowJSON.sort || label);

                // Label link
                if (opts.interactive == true && rowJSON.labelLink != null) {
                    labelElem.addClass("has-link");
                    labelElem.attr("title", Machinata.Reporting.Text.resolve(instance, rowJSON.labelLink.tooltip));
                    labelElem.click(function () {
                        if (rowJSON.labelLink.drilldown != null) {
                            tableElem.find(".selected-drilldown").removeClass("selected-drilldown");
                            labelElem.addClass("selected-drilldown");
                        }
                        Machinata.Reporting.Tools.openLink(instance, rowJSON.labelLink);
                    });
                }

                // Apply cell styles
                applyCellStyles(labelElem, rowJSON.styles, rowGroupJSON != null? rowGroupJSON.styles : null);
            } else {
                var labelElem = rowElem.find("[data-col-id='LABEL']"); //TODO: this is probably a performance bottleneck
                labelElem.addClass("is-empty");
            }

            // Indent?
            if (rowIndent == null) rowIndent = 0;
            if (rowJSON.indent != null) rowIndent += rowJSON.indent;
            if (rowIndent > 0) {
                rowElem.addClass("indent-" + rowIndent);
            }

            

            // Link?
            if (opts.interactive == true && rowJSON.link != null) {
                rowElem.addClass("has-link");
                rowElem.attr("title", Machinata.Reporting.Text.resolve(instance,rowJSON.link.tooltip));
                rowElem.click(function () {
                    if (rowJSON.link.drilldown != null) {
                        tableElem.find(".selected-drilldown").removeClass("selected-drilldown");
                        rowElem.addClass("selected-drilldown");
                    }
                    Machinata.Reporting.Tools.openLink(instance,rowJSON.link);
                });
            }

            // Row data columnms (facts)
            $.each(rowJSON.facts || rowJSON.headerFacts, function (index, factJSON) {

                // COLUMN

                // Automatic table drilldown for cell?
                if (opts.interactive == true && automaticTableDrilldown != null && factJSON.link == null) {
                    var cellFilters = [];
                    var drilldownRowValue = label;
                    var drilldownRowId = rowJSON.id;
                    var drilldownColumnID = factJSON.col;
                    var drilldownColumnJSON = columnIdToColumnMap[factJSON.col];
                    var drilldownColumnValue = Machinata.Reporting.Text.resolve(instance,drilldownColumnJSON.name);
                    var drilldownTooltipValue = "";
                    if (drilldownRowId != automaticTableDrilldown.tableRowIdForTotal) {
                        cellFilters.push({
                            "columnId": automaticTableDrilldown.tableColumnIdForRow,
                            "isAny": [drilldownRowValue],
                            "name": drilldownRowValue
                        });
                        drilldownTooltipValue += " " + drilldownRowValue;
                    }
                    if (drilldownColumnID != automaticTableDrilldown.tableColumnIdForTotal) {
                        cellFilters.push({
                            "columnId": automaticTableDrilldown.tableColumnIdForColumn,
                            "isAny": [drilldownColumnValue],
                            "name": drilldownColumnValue
                        });
                        drilldownTooltipValue += " " + drilldownColumnValue;
                    }
                    factJSON.link = {
                        "nodeId": automaticTableDrilldown.tableNodeId,
                        "tooltip": { "resolved": Machinata.Reporting.Text.translate(instance, "reporting.tables.drilldown-on") + drilldownTooltipValue },
                        "drilldown": { "filters": cellFilters },
                    };
                    if (cellFilters.length == 0) factJSON.link.tooltip.resolved = Machinata.Reporting.Text.translate(instance, "reporting.tables.drilldown-on-total");
                }

                // Find matching column
                // Normally, each fact has it's own column, but some are virtually combined into a single column as a group (such as bar charts)
                var dataElem = null;
                var columnGroupJSON = columnIdToGroupMap[factJSON.col]; // gets the column group defintion, if any
                if (columnGroupJSON != null && columnGroupJSON.groupType == "bars") {
                    dataElem = rowElem.find("[data-td-grp-id='" + columnGroupJSON.id + "']");

                    // Chart cell

                    if (columnGroupJSON.groupType == "bars") {
                        // Lookup theme
                        var columnName = factJSON.col;
                        var theme = Machinata.Reporting.getTheme(json.theme);
                        var shade = theme.colorNames[0]; //fallback
                        var columnNameResolved = columnName;
                        $.each(columnGroupJSON.dimensionElements, function (index, columnJSON) {
                            if (columnJSON.id == factJSON.col) {
                                if (columnJSON.colorShade != null) {
                                    shade = columnJSON.colorShade;
                                } else {
                                    // Cycle all shades
                                    shade = theme.colorNames[index % theme.colorNames.length];
                                }
                                if (columnJSON.name != null) {
                                    columnNameResolved = Machinata.Reporting.Text.resolve(instance, columnJSON.name);
                                }
                            }
                        });
                        // Get the relative value according to min max
                        var min = tableStatsMinValue[columnGroupJSON.id];
                        var max = tableStatsMaxValue[columnGroupJSON.id];
                        // Balance min/max
                        if (min < 0 && max > 0) {
                            if (Math.abs(min) < Math.abs(max)) min = max * -1;
                            else if (Math.abs(min) > Math.abs(max)) max = min * -1;
                        }
                        var val = factJSON.val;
                        var pPos = 0;
                        var pNeg = 0;
                        if (val > 0) pPos = val / max;
                        if (val < 0) pNeg = val / min;
                        var barsElem = $("<div class='balanced-bars'></div>");
                        var tooltip = columnNameResolved + ": " + factJSON.resolved; //todo col
                       
                        // Positive bar
                        if (max >= 0) {
                            var barPositiveElem = Machinata.UI.microBar(pPos, tooltip).addClass("positive");
                            barPositiveElem.find(".bar").addClass("theme-bg-" + shade);
                            barsElem.append(barPositiveElem);
                            barsElem.addClass("has-positive");
                        }
                        // Negative bar
                        if (min < 0) {
                            var barNegativeElem = Machinata.UI.microBar(pNeg, tooltip).addClass("negative");
                            barNegativeElem.find(".bar").addClass("theme-bg-" + shade);
                            barsElem.append(barNegativeElem);
                            barsElem.addClass("has-negative");
                        }

                        dataElem.append(barsElem);
                    }
                } else {

                    // Text cell

                    // Find matching fact column and apply data
                    dataElem = rowElem.find("[data-col-id='" + factJSON.col + "']"); //TODO: this is probably a performance bottleneck
                    dataElem.text(factJSON.resolved);
                    if (opts.includeFormattingInformation == true) {
                        // Add raw value attribute
                        if (factJSON.val != null) {
                            dataElem.attr("data-value", factJSON.val);
                        } else {
                            dataElem.attr("data-value", factJSON.resolved);
                        }
                        // Add format attribute
                        //TODO: not yet integrated (see valueType instead)
                        //dataElem.attr("data-format", factJSON.val);
                    }
                    dataElem.attr("data-text", factJSON.sort || factJSON.resolved);
                    dataElem.attr("data-sort", factJSON.sort || factJSON.val);

                    // Overwrite type?
                    if (factJSON.valueType != null) {
                        dataElem.addClass("type-" + factJSON.valueType);
                        dataElem.attr("data-type", factJSON.valueType);
                    }

                    // Tooltip?
                    if (factJSON.tooltip != null) {
                        dataElem.attr("title", Machinata.Reporting.Text.resolve(instance,factJSON.tooltip));
                    }

                }

                // Mark as having explicit fact defintion
                dataElem.addClass("has-definition");

                // Link?
                if (opts.interactive == true && factJSON.link != null) {
                    dataElem.addClass("has-link");
                    dataElem.attr("title", Machinata.Reporting.Text.resolve(instance,factJSON.link.tooltip));
                    dataElem.click(function () {
                        if (factJSON.link.drilldown != null) {
                            tableElem.find(".selected-drilldown").removeClass("selected-drilldown");
                            dataElem.addClass("selected-drilldown");
                        }
                        Machinata.Reporting.Tools.openLink(instance,factJSON.link);
                    });
                }

                // Add alert?
                if (factJSON.alert != null) {
                    dataElem.addClass("has-icon");
                    var icon = factJSON.alert.icon;
                    if (icon == null) icon = "alert"; // fallback
                    var iconElem = Machinata.Reporting.buildIcon(instance, icon)
                        .addClass("alert")
                        .attr("title", Machinata.Reporting.Text.resolve(instance, factJSON.alert.hint || factJSON.alert.tooltip));
                    dataElem.prepend(iconElem);
                }

                // Add info?
                if (factJSON.info != null) {
                    dataElem.addClass("has-icon");
                    var icon = factJSON.info.icon;
                    if (icon == null) icon = "info"; // fallback
                    var iconElem = Machinata.Reporting.buildIcon(instance, icon)
                        .addClass("info")
                        .click(function () {
                            Machinata.Reporting.Tools.showInfoDialog(instance, config, factJSON.info);
                        })
                        .attr("title", Machinata.Reporting.Text.resolve(instance, factJSON.info.tooltip));
                    dataElem.append(iconElem);
                }

                // Add icon?
                if (factJSON.icon != null) {
                    dataElem.addClass("has-icon");
                    if (factJSON.icon == "trend") {
                        if (factJSON.val > 0) {
                            dataElem.addClass("style-green-text");
                            dataElem.append(Machinata.Reporting.buildIcon(instance,"trend-up"));
                        } else {
                            dataElem.addClass("style-red-text");
                            dataElem.append(Machinata.Reporting.buildIcon(instance,"trend-down"));
                        }
                    } else {
                        //dataElem.append("<span class='icon " + Machinata.Reporting.icon(factJSON.icon) + "'></span>");
                        dataElem.append(Machinata.Reporting.buildIcon(instance,factJSON.icon));
                    }
                }

                // Add tag?
                if (factJSON.tag != null) {
                    dataElem.addClass("has-tag");
                    var tagElem = $("<span class='tag'></span>");
                    var tagTitle = Machinata.Reporting.Text.resolve(instance, factJSON.tag.title);
                    tagElem.text(tagTitle);
                    if (factJSON.tag.colorShade != null) {
                        tagElem.css("background-color", Machinata.Reporting.getThemeBGColorByShade(config, json.theme, factJSON.tag.colorShade));
                        tagElem.css("color", Machinata.Reporting.getThemeFGColorByShade(config, json.theme, factJSON.tag.colorShade));
                    }
                    if (factJSON.tag.icon != null) {
                        var iconElem = Machinata.Reporting.buildIcon(instance, factJSON.tag.icon);
                        if (factJSON.tag.colorShade != null) {
                            iconElem.css("fill", Machinata.Reporting.getThemeFGColorByShade(config, json.theme, factJSON.tag.colorShade));
                        }
                        tagElem.append(iconElem);
                    }
                    if (factJSON.tag.hint != null) {
                        tagElem.attr("title", Machinata.Reporting.Text.resolve(instance, factJSON.tag.hint));
                    }
                    dataElem.append(tagElem);
                    // Make sure to register in data-text (search/filter/export need this)
                    if (dataElem.attr("data-text") == null) {
                        dataElem.attr("data-text", tagTitle);
                    } else {
                        dataElem.attr("data-text", dataElem.attr("data-text")+" "+tagTitle);
                    }
                }

                // Add color? (deprecated: backwards compatibility)
                if (factJSON.color != null) {
                    if (factJSON.styles == null) factJSON.styles = {};
                    factJSON.styles.textColor = factJSON.color;
                }

                // Apply cell styles
                applyCellStyles(dataElem, factJSON.styles, rowJSON.styles, rowGroupJSON != null ? rowGroupJSON.styles : null);
            });

            // Nested rows?!
            if (rowJSON.childRows != null) {
                if (rowJSON.childRows.length > 0) {
                    // Register class
                    rowElem.addClass("has-child-rows");
                    tableElem.addClass("has-nested-rows");
                    // Make clickable
                    if (opts.interactive == true && json.nestedRowsAreInteractive == true) {
                        rowElem.addClass("toggable");
                        rowElem.find("td").first().prepend(Machinata.Reporting.buildIcon(instance,"expand")).prepend(Machinata.Reporting.buildIcon(instance,"collapse"));
                        rowElem.click(function () {
                            Machinata.Reporting.Node.VerticalTableNode._toggleRowInternal(tableElem, $(this));
                        });
                    }
                }
                $.each(rowJSON.childRows, function (childRowIndex, childRowJSON) {
                    createRow(childRowIndex, childRowJSON, rowGroupJSON, rowIndent + 1, rowJSON, rowElem); //TODO: what do we do about the index?
                });

                Machinata.Reporting.Node.VerticalTableNode._toggleRowInternal(tableElem, rowElem, rowJSON.expanded != false);
            }

            
            return rowElem;
        }

        // Process each row
        var stopProcessingGroups = false;
        json._state.notAllRowsAreLoaded = null; // reset
        $.each(groupJSON.dimensionElements, function (rowIndex, rowJSON) {
            // Make sure we don't go over the max num rows
            if (opts.alwaysIncludeAllRows != true && json._state.maximumNumberOfRowsToLoad != null && json._state.totalLoadedRows >= json._state.maximumNumberOfRowsToLoad) {
                // Create a final row to allow user to expand
                var expandAllRow = $("<tr class='table-row has-link expand-all-rows static'><td class='has-link data' colspan='100'></td></tr>");
                expandAllRow.find("td").text(Machinata.Reporting.Text.translate(instance, "reporting.tables.show-all-rows").replace("{numRows}", json._state.totalUnnestedRows));
                expandAllRow.find("td").attr("data-expand-text", Machinata.Reporting.Text.translate(instance, "reporting.tables.show-all-rows").replace("{numRows}", json._state.totalUnnestedRows));
                expandAllRow.find("td").attr("data-loading-text",Machinata.Reporting.Text.translate(instance, "reporting.tables.loading-all-rows").replace("{numRows}", json._state.totalUnnestedRows));
                expandAllRow.find("td").click(function () {
                    Machinata.Reporting.Node.VerticalTableNode.setLoading(instance, nodeElem, true);
                    setTimeout(function () {
                        json._state.maximumNumberOfRowsToLoad = null;
                        Machinata.Reporting.Node.VerticalTableNode.rebuild(instance, config, json, nodeElem, json._state.containerElem)
                    }, config.loadingUITimeoutDelay);
                });
                if (json.collapsedTableShowLoaderOnBottom == true) expandAllRow.appendTo(tbodyElem);
                if (json.collapsedTableShowLoaderOnTop == true) expandAllRow.clone(true).prependTo(tbodyElem);
                json._state.notAllRowsAreLoaded = true;
                stopProcessingGroups = true; // stop looping groups
                return false; // stop looping rows
            }

            // Create row
            var rowElem = createRow(rowIndex, rowJSON, groupJSON);

            // Last in group
            if (rowIndex == groupJSON.dimensionElements.length - 1 && groupIndex != json.rowDimension.dimensionGroups.length - 1) {
                rowElem.addClass("last-in-group");
            }
            
        });

        if (headerGroupRowNeedsChildrenCollapsed == true) {
            Machinata.Reporting.Node.VerticalTableNode._toggleRowInternal(tableElem, headerGroupRowElem, false);
        }

        if (stopProcessingGroups == true) return false;
    });

    // Post-processing

    // Did we never add a label?
    if (didAddRowLabel == false) {
        tableElem.find("td.label,th.label").remove();
    }

    // Show row filtering
    if (json.showRowFiltering == true) {
        tableElem.addClass("option-filter");
    }

    // Stats
    json._state.buildTimestampEnd = new Date();
    json._state.buildTime = json._state.buildTimestampEnd - json._state.buildTimestampStart;
    //console.log("table build time: "+json._state.buildTime+"ms");
    return tableElem;
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.Reporting.Node.VerticalTableNode._toggleRowInternal = function (tableElem, rowElem, show) {
    
    // Helper method for recursion...
    function _toggleAllChildrenRows(rowElem, show) {
        var selector = "[data-parent-row-id='" + rowElem.attr("data-row-id") + "']";
        var childRowElems = tableElem.find(selector);
        if (show == true) {
            childRowElems.removeClass("hidden");
        } else {
            childRowElems.addClass("hidden");
        }
        // Recurse
        childRowElems.each(function () {
            if (show == true && $(this).hasClass("collapsed")) return;
            _toggleAllChildrenRows($(this), show);
        });
    }

    // Parent state
    if (show == null) show = !rowElem.hasClass("expanded");
    if (show == true) {
        rowElem.addClass("expanded");
        rowElem.removeClass("collapsed");
    } else {
        rowElem.removeClass("expanded");
        rowElem.addClass("collapsed");
    }

    // Initial toggle
    _toggleAllChildrenRows(rowElem, show);
};



/// <summary>
/// Toggles a set of table columns or groups on the table.
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.toggleColumn = function (instance, config, json, nodeElem, colId, grpId, show) {
    if (show == null) show = true; //TODO: @dan

    // Init selection
    var tableElem = nodeElem.data("table");
    var elemsCols = null;
    var elemsGrps;
    if (colId != null) elemsCols =tableElem.find("[data-col-id='" + colId + "']");
    if (grpId != null) elemsGrps = tableElem.find("[data-grp-id='" + grpId + "']");

    // Toggle
    if (show == true) {
        if (elemsCols != null) elemsCols.show();
        if (elemsGrps != null) elemsGrps.show();
    } else {
        if (elemsCols != null) elemsCols.hide();
        if (elemsGrps != null) elemsGrps.hide();
    }
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.rebuild = function (instance, config, json, nodeElem, containerElem) {
    if (containerElem == null) containerElem = nodeElem;


    // Already exists?
    if (nodeElem.data("table") != null) {
        nodeElem.data("table").remove();
    }

    // Create table elem
    var tableElem = this.buildTable(instance, config, json, nodeElem, {
        interactive: true,
        fastLoad: true
    });
    tableElem.data("node", nodeElem); // bind the nodeElem to the tableElem
    containerElem.append(tableElem);
    nodeElem.data("table", tableElem); // bind the tableElem to the nodeElem
    containerElem.data("table", tableElem); // bind the tableElem to the containerElem

    // Make sortable and filterabile
    // See https://mottie.github.io/tablesorter/docs/ for more documentation
    // Filter plugin: https://mottie.github.io/tablesorter/docs/example-node-filter.html
    // Static row plugin: https://mottie.github.io/tablesorter/docs/example-node-static-row.html
    if (json.allowRowSorting == true) {
        // Make sure we are not using json.automaticTableDrilldown and that no header has a link
        if (json.automaticTableDrilldown == null && json._state.headerLabelHasLink != true) {
            Machinata.Reporting.Tools.makeTableSortable(instance, tableElem);
        }
    }

    // Make filterable
    if (json.allowRowFiltering == true || json.filters != null) {
        Machinata.Reporting.Tools.makeTableFilterable(instance, tableElem, json.filters); // pass in the original filters, this saves us from having to refilter later...
        // Sometimes the table was rebuilt after the user enabled filters, if so we show them
        if (nodeElem.find(".machinata-reporting-toolbar .tool-filter").hasClass("active")) {
            Machinata.Reporting.Node.VerticalTableNode.showTableFilters(instance, nodeElem);
        }

    }

    // Make sticky, but only if it makes sense
    if (json.stickyHeadersMinRows != null && json._state.totalLoadedRows > json.stickyHeadersMinRows && config.profile != "webprint") {
        // https://github.com/jmosbech/StickyTableHeaders
        tableElem.stickyTableHeaders({
            cacheHeaderHeight: true,
            fixedOffset: $('#header')
        });
        tableElem.on("enabledStickiness.stickyTableHeaders", function () {
            $(this).addClass("header-is-sticky");
        });
        tableElem.on("disabledStickiness.stickyTableHeaders", function () {
            $(this).removeClass("header-is-sticky");
        });
        // If we have a sidescroller, we must update the sticky table headers to match the scroll position
        if (tableElem.parent().hasClass("side-scroller")) {
            tableElem.parent()[0].addEventListener("scroll", function () {
                tableElem.trigger('resize.stickyTableHeaders');
            },false);
        }
    }

    Machinata.Reporting.Node.VerticalTableNode.setLoading(instance, nodeElem, false);

    return tableElem;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.showTableFilters = function (instance, nodeElem) {
    nodeElem.data("table").addClass("option-filter");
    nodeElem.find(".machinata-reporting-toolbar .tool-filter").addClass("active");
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.hideTableFilters = function (instance, nodeElem) {
    nodeElem.data("table").removeClass("option-filter");
    nodeElem.find(".machinata-reporting-toolbar .tool-filter").removeClass("active");
    Machinata.Filter.Tables.resetFilters(nodeElem.data("table"));
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.setLoading = function (instance, nodeElem, loading) {
    Machinata.Reporting.setLoading(instance, loading);
    if (nodeElem.data("table") != null) {
        var loadingRowCell = nodeElem.data("table").find("tr.expand-all-rows td");
        if (loading == true) {
            loadingRowCell.text(loadingRowCell.attr("data-loading-text"));
        } else {
            loadingRowCell.text(loadingRowCell.attr("data-expand-text"));
        }
    }
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.makeSureAllDataIsLoaded = function (instance, config, nodeJSON, nodeElem, callback) {
    // Have not all rows been laoded? If so, make sure first...
    if (nodeJSON._state.notAllRowsAreLoaded == true) {
        Machinata.Reporting.Node.VerticalTableNode.setLoading(instance, nodeElem, true);
        setTimeout(function () { // make sure browser can update loading ui
            nodeJSON._state.maximumNumberOfRowsToLoad = null;
            Machinata.Reporting.Node.VerticalTableNode.rebuild(instance, config, nodeJSON, nodeElem, nodeJSON._state.containerElem);
            if (callback) callback(instance, config, nodeJSON, nodeElem, true);
        }, config.loadingUITimeoutDelay);
    } else {
        if (callback) callback(instance, config, nodeJSON, nodeElem, false);
    }
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.init = function (instance, config, json, nodeElem) {
    var self = this;

    // Get row count (non nested)
    json._state.totalUnnestedRows = 0;
    $.each(json.rowDimension.dimensionGroups, function (groupIndex, groupJSON) {
        json._state.totalUnnestedRows += groupJSON.dimensionElements.length;
    });

    // Build container for table (side scroller)
    json._state.containerElem = $("<div class='side-scroller node-bg'></div>").appendTo(nodeElem);

    // Determin lazy load feature
    json._state.lazyLoadTableData = false;
    if (json.automaticallyLazyLoadLargeTables == true && json._state.totalUnnestedRows >= json.minimumRowsForLargeTables) {
        json._state.lazyLoadTableData = true;
    }

    // Determine how many rows to load
    json._state.maximumNumberOfRowsToLoad = json.maximumNumberOfRowsToLoad;
    if (json.automaticallyCollapseLargeTables == true && json._state.totalUnnestedRows >= json.minimumRowsForLargeTables && json.maximumNumberOfRowsToLoad == null) {
        json._state.maximumNumberOfRowsToLoad = json.maximumRowsForCollapsedLargeTables;
    }

    // Initial rebuild
    if (json._state.lazyLoadTableData != true) {
        Machinata.Reporting.Node.VerticalTableNode.rebuild(instance, config, json, nodeElem, json._state.containerElem);
    }
    

    // Export
    Machinata.Reporting.Node.registerExportOptions(instance, config, json, nodeElem, {
        "xlsx": "Excel",
        "csv": "CSV",
    });


    // Copy
    if (json.addCopyToClipboardTool == true) {
        Machinata.Reporting.addTool(instance, nodeElem, Machinata.Reporting.Text.translate(instance, "reporting.nodes.copy-to-clipboard"), "copy", function () {
            // Create a raw copy of table
            //TODO: this doesnt include the filters sorts - do we want that?
            //TODO: LibreOffice doesnt copy table nicely
            var tempTableElem = self.buildTable(instance, config, json, nodeElem, {
                interactive: false
            });
            Machinata.Reporting.Tools.copyElementToClipboard(tempTableElem, Machinata.Reporting.Text.translate(instance, 'reporting.nodes.copied'), Machinata.Reporting.Text.translate(instance, 'reporting.nodes.copied.table'), true);
            Machinata.Reporting.Tools.trackNodeEvent(instance, config, json, Machinata.Reporting.TRACKING_ACTION_NODE_COPYTOCLIPBOARD); // instance, config, nodeJSON, action, val
        });
    }

    // Filter
    if (json.allowRowFiltering == true) {
        // Add tool
        nodeElem.addClass("option-allow-row-filtering");
        var filterToolElem = Machinata.Reporting.addTool(instance, nodeElem, Machinata.Reporting.Text.translate(instance, "reporting.nodes.search-filter"), "filter", function (toolElem) {
            if (nodeElem.data("table").hasClass("option-filter")) {
                Machinata.Reporting.Node.VerticalTableNode.hideTableFilters(instance, nodeElem);
            } else {
                // Have not all rows been laoded? If so, make sure first...
                if (json._state.notAllRowsAreLoaded == true) {
                    Machinata.Reporting.Node.VerticalTableNode.setLoading(instance, nodeElem, true);
                    setTimeout(function () { // make sure browser can update loading ui
                        json._state.maximumNumberOfRowsToLoad = null;
                        Machinata.Reporting.Node.VerticalTableNode.rebuild(instance, config, json, nodeElem, json._state.containerElem);
                        Machinata.Reporting.Node.VerticalTableNode.showTableFilters(instance, nodeElem);
                    }, config.loadingUITimeoutDelay);
                } else {
                    Machinata.Reporting.Node.VerticalTableNode.showTableFilters(instance, nodeElem);
                }
            }
        }, "fullscreen-supported");

        // Do we have preset filters?
        if (json.filters != null) {
            // Toggle toolbar element
            //nodeElem.data("table").addClass("option-filter");
            //filterToolElem.addClass("active");
            Machinata.Reporting.Node.VerticalTableNode.showTableFilters(instance, nodeElem);
        }
    }

    // Column selection
    if (json.allowColumnConfiguration == true) {
        nodeElem.addClass("option-allow-column-configuration");
        Machinata.Reporting.addTool(instance, nodeElem, Machinata.Reporting.Text.translate(instance, "reporting.nodes.configure"), "configure", function () {
            // Create a UI for each column
            var diag = Machinata.messageDialog(Machinata.Reporting.Text.translate(instance, 'reporting.nodes.configure'), '');
            diag.elem.append($("<p></p>").text(Machinata.Reporting.Text.translate(instance, 'reporting.tables.select-columns')));
            var checkboxesElem = $("<div></div>");
            var checkboxesCount = 0;
            $.each(json.columnDimension.dimensionGroups, function (index, groupJSON) {
                $.each(groupJSON.dimensionElements, function (index, columnJSON) {
                    var checkboxUIElem = Machinata.Reporting.buildCheckbox(instance, Machinata.Reporting.Text.resolve(instance, columnJSON.name), columnJSON.display != false);
                    checkboxUIElem.find("input")
                        .attr("data-grp-id", groupJSON.id)
                        .attr("data-col-id", columnJSON.id)
                        .on("change", function () {
                            //TODO: limit max checked columns?
                            var checked = $(this).is(':checked');
                            columnJSON.display = checked;
                        });
                    /*var checkboxId = Machinata.Reporting.uid();
                    var checkboxUIElem = $('<div class="machinata-reporting-checkbox"><input type="checkbox"/><label></label></div>');
                    checkboxUIElem.find("label").append(Machinata.Reporting.buildIcon(instance,"check"));
                    checkboxUIElem.find("label")
                        .attr("for", checkboxId)
                        .append(Machinata.Reporting.Text.resolve(instance,columnJSON.name));
                    if(columnJSON.display != false) checkboxUIElem.find("input").attr("checked", "checked")
                    checkboxUIElem.find("input")
                        .attr("id", checkboxId)
                        .attr("data-grp-id", groupJSON.id)
                        .attr("data-col-id", columnJSON.id)
                        .on("change", function () {
                            //TODO: limit max checked columns?
                            var checked = $(this).is(':checked');
                            columnJSON.display = checked;
                        });*/
                    checkboxesElem.append(checkboxUIElem);
                    checkboxesCount++;
                });
            });
            if (checkboxesCount > 8) {
                checkboxesElem.css("column-count", "2");
                checkboxesElem.css("column-gap", config.padding+"px");
            }
            diag.elem.append(checkboxesElem);
            diag.cancelButton();
            diag.okayButton();
            diag.okay(function () {
                // User wants an update - call rebuild
                Machinata.Reporting.Node.VerticalTableNode.setLoading(instance, nodeElem, true);
                setTimeout(function () {
                    json._state.maximumNumberOfRowsToLoad = null; // make sure we load everything in case the table was truncated...
                    Machinata.Reporting.Node.VerticalTableNode.rebuild(instance, config, json, nodeElem, json._state.containerElem);
                }, config.loadingUITimeoutDelay);
                
            });
            diag.show();
        });
    }


    // Do we have preset highlight?
    if (config.highlight != null) {
        config.highlight = config.highlight.toLowerCase();
        nodeElem.data("table").find("td").each(function () {
            var dataElem = $(this);
            var dataElemSearch = dataElem.text().toLowerCase();
            if (dataElemSearch.indexOf(config.highlight) != -1) {
                dataElem.addClass("selected-highlight");
            }
        });
    }

    // Do we have preset sorts?
    //TODO: Support this on the data side as well
    if (json.sorts != null) {
        // Compile filter array (needed by tablesorter)
        var sortArray = []; // For example, [[1,0], [2,0]]
        // Find matching value in config
        for (sortKey in json.sorts) {
            // Register 
            var sortDir = 0;
            if (json.sorts[sortKey] == "asc") sortDir = 0;
            if (json.sorts[sortKey] == "desc") sortDir = 1;
            // The value contains an array of instructions for per-column sorting and direction in the format: 
            // [[columnIndex, sortDirection], ... ] where columnIndex is a zero-based index for your columns left-to-right and sortDirection is 0 for Ascending and 1 for Descending. A valid argument that sorts ascending first by column 1 and then column 2 looks like: [[0,0],[1,0]].
            // Note: the above documentation is misleading: if you have groups, then each group offsets the column by one.
            // Instead of using the column header index, we just go look it up
            var columnHeaderElem = nodeElem.data("table").find("th[data-name='" + sortKey + "']");
            sortArray.push([parseInt(columnHeaderElem.attr("data-column")), sortDir]);
        }
        // Apply sort
        nodeElem.data("table").trigger('sorton', [sortArray]); // From documentation: $( 'table' ).trigger( 'sorton', [ [[0,0],[2,0]] ] );
    }


    // Events
    nodeElem.on(Machinata.Reporting.Events.REBUILD_TABLE_SIGNAL, function () {
        Machinata.Reporting.Node.VerticalTableNode.rebuild(instance, config, json, nodeElem, json._state.containerElem);
    });

};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    var self = this;
    if (format == "xlsx" || format =="csv") {
        var tempTableElem = self.buildTable(instance, config, json, nodeElem, {
            interactive: false,
            includeFormattingInformation: true,
            alwaysIncludeAllRows: true,
        });
    
        var workbook = Machinata.Reporting.Export.createExcelWorkbook(instance, config, json);
        workbook = Machinata.Reporting.Export.createExcelDataForTable(instance, config, json, workbook, tempTableElem,format);
        Machinata.Reporting.Export.saveExcelWorkbook(instance, config, json, workbook, filename);
        return true;
    }
    return false;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.VerticalTableNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...

    // First draw (build?)
    var firstDraw = json._state.firstDraw;
    if (firstDraw == null) firstDraw = true;
    json._state.firstDraw = false; // mark as false for next

    if (json._state.lazyLoadTableData == true && firstDraw == true) {
        Machinata.Reporting.Node.VerticalTableNode.rebuild(instance, config, json, nodeElem, json._state.containerElem);
    }
};









