


/// <summary>
/// A very simple node that displays a debug message when ```Machinata.DebugEnabled``` is ```true```.
/// To enable this mode, use ```debug=true``` in the query string for any report URL.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.DebugNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.DebugNode.defaults = {};

Machinata.Reporting.Node.DebugNode.defaults.supportsToolbar = false;

/// <summary>
/// The debug message to show (string).
/// </summary>
Machinata.Reporting.Node.DebugNode.defaults.message = false;

Machinata.Reporting.Node.DebugNode.init = function (instance, config, json, nodeElem) {

    if (Machinata.Reporting.isDebugEnabled()) {
        var errorElem = Machinata.UI.createDebugPanel(json.message, {}).addClass("option-warning");
        nodeElem.append(errorElem);
    }

};
Machinata.Reporting.Node.DebugNode.draw = function (instance, config, json, nodeElem) {
    // Nothing to do here...
};








