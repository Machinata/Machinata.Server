


/// <summary>
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.MultiGaugeNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.MultiGaugeNode.defaults = {};

/// <summary>
/// ```solid``` chrome by default.
/// </summary>
Machinata.Reporting.Node.MultiGaugeNode.defaults.chrome = "solid";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.MultiGaugeNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.MultiGaugeNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.MultiGaugeNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_4x2, Machinata.Reporting.Layouts.BLOCK_2x2];

/// <summary>
/// We do support a toolbar.
/// </summary>
Machinata.Reporting.Node.MultiGaugeNode.defaults.supportsToolbar = true;

/// <summary>
/// Legends are not necessary by default.
/// </summary>
Machinata.Reporting.Node.MultiGaugeNode.defaults.insertLegend = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.MultiGaugeNode.defaults.innerRadius = 0.5;

/// <summary>
/// </summary>
Machinata.Reporting.Node.MultiGaugeNode.defaults.outerRadius = 1.0;

/// <summary>
/// </summary>
Machinata.Reporting.Node.MultiGaugeNode.defaults.arcPadAngle = 2;

/// <summary>
/// </summary>
Machinata.Reporting.Node.MultiGaugeNode.defaults.layoutPadding = 18;


/// <summary>
/// The stroke width of the status arc line and it's arrow head, in pixels.
/// </summary>
Machinata.Reporting.Node.MultiGaugeNode.defaults.statusArcLineSize = 2; // was 4


/// <summary>
/// If set, a predifined maximum value for all the facts can be used (no fact should exceed this value).
/// If unset (null), the maximum value is automatically determined based on the facts.
/// </summary>
Machinata.Reporting.Node.MultiGaugeNode.defaults.maximumStatusValue = null;


/// <summary>
/// If true, a one-sided single arc will be used (either fully positive or fully negative) if the facts
/// are either all positive or all negative. This allows for multi-gauges to use their space more effectively in
/// these cases.
/// </summary>
Machinata.Reporting.Node.MultiGaugeNode.defaults.automaticallyUseSingleArc = true;



Machinata.Reporting.Node.MultiGaugeNode.getVegaSpec = function (instance, config, json, nodeElem) {
    var arcSliceActive = 'gray4';
    var arcSliceInactive = 'gray2';
    var labelColor = "black";
    if (json.chrome == "dark") {
        arcSliceActive = 'gray4';
        arcSliceInactive = 'gray5';
        labelColor = "white";
    }
    var allFactsArePositive = true;
    var allFactsAreNegative = true;
    for (var i = 0; i < json.facts.length; i++) {
        if (json.facts[i].val < 0) allFactsArePositive = false;
        if (json.facts[i].val > 0) allFactsAreNegative = false;
    }
    var arcIsOneSided = json.automaticallyUseSingleArc && (allFactsArePositive || allFactsAreNegative);
    

    return {
        "data": [
          {
              "name": "facts",
              "values": null,
              "transform": [
                {
                    "type": "formula",
                    "as": "valAbs",
                    "expr": "abs(datum.val)"
                },
              ]
          },
          {
              "name": "factsResolved",
              "source": "facts",
              "transform": [
                {
                    "type": "identifier",
                    "as": "index",
                },
                {
                    "type": "formula",
                    "as": "categoryResolved",
                    "expr": "datum.category.resolved"
                },
                {
                    "type": "formula",
                    "as": "valAdjusted",
                    // This is either:
                    // the value adjusted to 0.0-1.0 based on the valScale scale (ie the largest fact value is 1.0)
                    // or 
                    // based on the maximumStatusValue value (factValue/maximumStatusValue), limited to max 1.0
                    "expr": json.maximumStatusValue == null ? "datum.val > 0 ? +max(scale('valScale',datum.valAbs),0.1) : -max(scale('valScale',datum.valAbs),0.1)" : "datum.val > 0 ? +min(datum.val/maximumStatusValue,1.0) : max(datum.val/maximumStatusValue,-1.0)"
                  },
                  {
                      "type": "formula",
                      "as": "valAdjusted",
                      "expr": "datum.val == 0 ? 0 : datum.valAdjusted"
                  },
                  {
                      "type": "formula",
                      "as": "valAdjustedForArrowHead",
                      "expr": "datum.valAdjusted > 0 ? max(datum.valAdjusted - _arcArrowHeadOffsetMultiplier,0) : min(datum.valAdjusted + _arcArrowHeadOffsetMultiplier,0)"
                  }
              ]
          },
          {
              "name": "factsGridded",
              "source": "factsResolved",
              "transform": [
                {
                    "type": "formula",
                    "as": "gridColumn",
                    "expr": "(datum.index-1) % columns"
                },
                {
                    "type": "formula",
                    "as": "gridRow",
                    "expr": "floor( (datum.index-1) / columns)"
                }
              ]
          },
          //{
          //    "name": "layoutInfo",
          //    "source": "factsGridded",
          //    "transform": [
          //      {
          //          "type": "aggregate",
          //          "fields": ["gridColumn", "gridRow"],
          //          "ops": ["max", "max"],
          //          "as": ["columns", "rows"]
          //      }
          //    ]
          //},
        ],
        "scales": [
          {
              "name": "numFactsToColumnsScale",
              "type": "linear",
              "domain": [0,1,2,3,4,5,6,7,8,9,10],
              "range":  [0,1,2,3,4,3,3,4,4,3, 5]
          },
          {
              "name": "valScale",
              "type": "linear",
              "domain": { "data": "facts", "field": "valAbs" },
              "range": [0,1]
          }
        ],
        //NOTE: The vega layout is buggy in responsive mode (it doesnt re-fill space once shrunk)
        //"layout": {
        //    "columns": 4,
        //    "padding": 0
        //},
        "signals": [
            {
                "name": "maximumStatusValue",
                "init": json.maximumStatusValue
            },
            {
                "name": "numFacts",
                "init": "length(data('facts'))"
            },
            {
                // Column calculator, uses provided value if given
                "name": "_columns",
                "init": json.gaugeColumns || "numFacts > 10 ? 4 : scale('numFactsToColumnsScale',numFacts)"
            },
            {
                // Rows calculator, uses provided value if given
                "name": "_rows",
                "init": json.gaugeRows || "numFacts / _columns"
            },
            {
                // Actual columns to use, on aspect ratios less than 1 (portrait) fixed 2 columns
                "name": "columns",
                "update": "aspectRatio > 1.0 ? _columns : 2"
            },
            {
                // Actual rows to use, on aspect ratios less than 1 (portrait) fixed 2 columns
                "name": "rows",
                "update": "aspectRatio > 1.0 ? _rows : ceil(numFacts / 2.0)"
            },
            {
                "name": "itemWidth",
                "update": "width / columns"
            },
            {
                "name": "itemHeight",
                "update": "height / rows"
            },
            {
                "name": "_layoutPadding",
                "init": "" + json.layoutPadding + ""
            },
            {
                "name": "layoutPadding",
                "update": "responsiveLayout == 'mobile'||'tablet' ? _layoutPadding/2 : _layoutPadding"
            },
            {
                "name": "subtitleSize",
                "update": "min(itemWidth,itemHeight) < 210 ? (" + config.subtitleSize + " * " + config.mobileSubtitleSizeFactor + ") : " + config.subtitleSize
            },
            {
                "name": "labelSize",
                "update": "min(itemWidth,itemHeight) < 210 ? (" + config.labelSize + " * " + config.mobileLabelSizeFactor + ") : " + config.labelSize
            },
            {
                "name": "_arcIsOneSided",
                "init": arcIsOneSided
            },
            {
                "name": "_arcPadAngle",
                "init": "_arcIsOneSided ? 0 : " + json.arcPadAngle
            },
            {
                "name": "_arcArrowHeadOffsetMultiplier",
                "init": "_arcIsOneSided ? 0.005 : 0.012"
            },
            {
                "name": "_arcTotalBandwidthInRadians",
                "init": "_arcIsOneSided ? PI*2 : PI"
            },
        ],

        "marks": [
          {
              "type": "group",
              "from": { "data": "factsGridded" },
              "signals": [
                  { "name": "outerWidth", "update": "itemWidth" },
                  { "name": "outerHeight", "update": "itemHeight" },
                  { "name": "itemX", "update": "itemWidth" },
                  { "name": "itemY", "update": "itemHeight" },
                  { "name": "data", "init": "parent" }
              ],
                "encode": {
                    "enter": {
                        "clip": { "value": true }
                    },
                  "update": {
                        "width": { "signal": "itemWidth" },
                        "height": { "signal": "itemHeight" },
                        "x": { "signal": "itemWidth * datum.gridColumn" },
                        "y": { "signal": "itemHeight * datum.gridRow" }
                    }
              },
              "marks": [
                  {
                      "type": "group",
                      "encode": {
                          "update": {
                              "x": { "signal": "layoutPadding" },
                              "y": { "signal": "layoutPadding" },
                              "width": { "signal": "outerWidth - layoutPadding*2" },
                              "height": { "signal": "outerHeight - layoutPadding*2" },
                              "clip": { "value": true }
                          }
                      },
                      "signals": [
                          { "name": "innerWidth", "update": "outerWidth - layoutPadding*2" },
                          { "name": "innerHeight", "update": "outerHeight - layoutPadding*2" },
                          { "name": "innerSize", "update": "min(innerWidth,innerHeight)" }
                      ],
                      "marks": [
                          arcIsOneSided ? {"type":"group"}:{
                              "type": "arc",
                              /*"_comment": "BACKGROUND ARC LEFT",*/
                              "encode": {
                                  "enter": {
                                      //"fill": { "scale": "grayColorByShade", "value": "gray3" }
                                      "fill": { "signal": "data.valAdjusted < 0 ? scale('theme-gray-bg','" + arcSliceActive + "') : scale('theme-gray-bg','" + arcSliceInactive+"')" }
                                  },
                                  "update": {
                                      "x": { "signal": "innerWidth / 2" },
                                      "y": { "signal": "innerHeight / 2" },
                                      "startAngle": { "signal": "0" },
                                      "endAngle": { "signal": "-_arcTotalBandwidthInRadians" },
                                      "padAngle": { "signal": "(PI/180) * _arcPadAngle" },
                                      "innerRadius": { "signal": "(innerSize / 2)*" + json.innerRadius },
                                      "outerRadius": { "signal": "(innerSize / 2)*" + json.outerRadius }
                                  }
                              }
                          },
                          arcIsOneSided ? { "type": "group" } : {
                                "type": "arc",
                                /*"_comment": "BACKGROUND ARC RIGHT",*/
                                "encode": {
                                    "enter": {
                                        //"fill": { "scale": "grayColorByShade", "value": "gray3" }
                                        "fill": { "signal": "data.valAdjusted > 0 ? scale('theme-gray-bg','" + arcSliceActive + "') : scale('theme-gray-bg','" + arcSliceInactive+"')" }
                                    },
                                    "update": {
                                        "x": { "signal": "innerWidth / 2" },
                                        "y": { "signal": "innerHeight / 2" },
                                        "startAngle": { "signal": "0" },
                                        "endAngle": { "signal": "_arcTotalBandwidthInRadians" },
                                        "padAngle": { "signal": "(PI/180) * _arcPadAngle"  },
                                        "innerRadius": { "signal": "(innerSize / 2)*" + json.innerRadius },
                                        "outerRadius": { "signal": "(innerSize / 2)*" + json.outerRadius },
                                    }
                                }
                          },
                          !arcIsOneSided ? { "type": "group" } : {
                              "type": "arc",
                              /*"_comment": "BACKGROUND ARC FULL",*/
                              "encode": {
                                  "enter": {
                                      "fill": { "signal": "scale('theme-gray-bg','" + arcSliceActive + "')" }
                                  },
                                  "update": {
                                      "x": { "signal": "innerWidth / 2" },
                                      "y": { "signal": "innerHeight / 2" },
                                      "startAngle": { "signal": "0" },
                                      "endAngle": { "signal": "_arcTotalBandwidthInRadians" },
                                      "innerRadius": { "signal": "(innerSize / 2)*" + json.innerRadius },
                                      "outerRadius": { "signal": "(innerSize / 2)*" + json.outerRadius },
                                  }
                              }
                          },
                            {
                                "type": "arc",
                                /*"_comment": "VALUE ARC",*/
                                "encode": {
                                    "enter": {
                                        "fill": { "signal": "data.valAdjusted > 0 ? scale('theme-bg','bright') : scale('theme-bg','bright')" } // always bright
                                    },
                                    "update": {
                                        "x": { "signal": "innerWidth / 2" },
                                        "y": { "signal": "innerHeight / 2" },
                                        "startAngle": { "signal": "0" },
                                        "endAngle": { "signal": "_arcTotalBandwidthInRadians * data.valAdjusted" },
                                        "padAngle": { "signal": "(PI/180) * _arcPadAngle" },
                                        "innerRadius": { "signal": "(innerSize / 2)*" + (json.innerRadius + 0.1) },
                                        "outerRadius": { "signal": "(innerSize / 2)*" + (json.outerRadius - 0.1) }
                                    }
                                }
                            },
                            {
                                "type": "arc",
                                /*"_comment": "VALUE LINE",*/
                                "encode": {
                                    "enter": {
                                        "fill": { "value": "white" } //TODO
                                    },
                                    "update": {
                                        "x": { "signal": "innerWidth / 2" },
                                        "y": { "signal": "innerHeight / 2" },
                                        "startAngle": { "signal": "0" },
                                        "endAngle": { "signal": "_arcTotalBandwidthInRadians * data.valAdjusted * 0.99" },
                                        "padAngle": { "signal": "(PI/180) * _arcPadAngle" },
                                        "innerRadius": { "signal": "(innerSize / 2)*" + ((json.innerRadius + json.outerRadius) / 2) + " - " + (json.statusArcLineSize / 2) },
                                        "outerRadius": { "signal": "(innerSize / 2)*" + ((json.innerRadius + json.outerRadius) / 2) + " + " + (json.statusArcLineSize / 2) }
                                    }
                                }
                            },
                            {
                                "type": "rect",
                                /*"_comment": "VALUE FOOT",*/
                                "encode": {
                                    "enter": {
                                        "fill": { "value": "white" } //TODO
                                    },
                                    "update": {
                                        "x": { "signal": "(innerWidth/2)-" + (json.statusArcLineSize * 2/2) },
                                        "y": { "signal": "(innerHeight/2) - (innerSize/2)*" + json.outerRadius },
                                        "width": { "value": (json.statusArcLineSize*2) },
                                        "height": { "signal": "(innerSize/2) * " + (json.outerRadius - json.innerRadius) },
                                        
                                    }
                                }
                            },
                            {
                                "type": "symbol",
                                /*"_comment": "ARROW HEAD",*/
                                "encode": {
                                    "enter": {
                                        "opacity": { "signal": "data.valAdjusted == 0 ? 0 : 1" },
                                        "stroke": { "value": "white" },
                                        //"strokeWidth": { "value": 4 },
                                        "strokeWidth": { "value": (json.statusArcLineSize) },
                                        "shape": { "value": "M-1,1L0,0L-1,-1" },
                                        "size": { "signal": "(innerSize / 2)*" + ((json.innerRadius + json.outerRadius) / 2) + " *4.0" }, // at normal res should be about 1000
                                    },
                                    "update": {
                                        "angle": { "signal": "((data.valAdjusted) > 0 ? (data.valAdjusted * _arcTotalBandwidthInRadians) : (data.valAdjusted * _arcTotalBandwidthInRadians)+PI) * (180/PI)" },
                                        "x": { "signal": "innerWidth/2  + sin(_arcTotalBandwidthInRadians * data.valAdjustedForArrowHead)*  (innerSize / 2)*" + ((json.innerRadius + json.outerRadius) / 2) + "" },
                                        "y": { "signal": "innerHeight/2 - cos(_arcTotalBandwidthInRadians * data.valAdjustedForArrowHead)*  (innerSize / 2)*" + ((json.innerRadius + json.outerRadius) / 2) + "" },
                                    }
                                }
                            },
                            {
                                "type": "text",
                                /*"_comment": "CATEGORY LABEL",*/
                                "encode": {
                                    "enter": {
                                        "align": { "value": "center" },
                                        "baseline": { "value": "bottom" },
                                        "text": { "signal": "data.categoryResolved" },
                                        "fill": { "value": labelColor }
                                    },
                                    "update": {
                                        "fontSize": { "signal": "labelSize" },
                                        "dy": { "signal": "-labelSize/3" },
                                        "x": { "signal": "innerWidth/2" },
                                        "y": { "signal": "innerHeight/2" },
                                    }
                                }
                            },
                            {
                                "type": "text",
                                /*"_comment": "VAL LABEL",*/
                                "encode": {
                                    "enter": {
                                        "dy": { "signal": "0" },
                                        "align": { "value": "center" },
                                        "baseline": { "value": "top" },
                                        "text": { "signal": "data.resolved" },
                                        "fill": { "value": labelColor }
                                    },
                                    "update": {
                                        "fontSize": { "signal": "subtitleSize" },
                                        "x": { "signal": "innerWidth/2" },
                                        "y": { "signal": "innerHeight/2" }
                                    }
                                }
                            }
                      ]
                  },
                
                
              ]
          }
        ]
    };
};
Machinata.Reporting.Node.MultiGaugeNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    spec["data"][0].values = json.facts;
};
Machinata.Reporting.Node.MultiGaugeNode.init = function (instance, config, json, nodeElem) {
    // Call parent
    if (nodeElem != null) nodeElem.addClass("numfacts-"+json.facts.length);
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.MultiGaugeNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.MultiGaugeNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.MultiGaugeNode.exportExcelFormat = function (instance, config, json, nodeElem, format, filename) {

    var workbook = Machinata.Reporting.Export.createExcelWorkbook(instance, config, json);
    workbook = Machinata.Reporting.Export.createExcelDataForCategoryFacts(instance, config, json, workbook,format);
    Machinata.Reporting.Export.saveExcelWorkbook(instance, config, json, workbook, filename);
    return true;
};






