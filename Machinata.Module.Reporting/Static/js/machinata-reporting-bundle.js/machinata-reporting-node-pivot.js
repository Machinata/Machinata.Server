


/// <summary>
/// A simple interactive pivot for VerticalTableNode.
/// This node is compatible with any VerticalTableNode, 
/// and only requires a few additional properties (```pivotData```, ```pivotAggregation```)  to work.
/// </summary>
/// <type>class</type>
Machinata.Reporting.Node.PivotTableNode = {};


/// <summary>
/// </summary>
Machinata.Reporting.Node.PivotTableNode.defaults = {};

/// <summary>
/// This node does not support being added to dashboards...
/// </summary>
Machinata.Reporting.Node.PivotTableNode.defaults.supportsDashboard = false;

/// <summary>
/// Defines the default table column (id) to use for the pivot operation rows.
/// </summary>
Machinata.Reporting.Node.PivotTableNode.defaults.pivotRow = null;

/// <summary>
/// Defines the default table column (id) to use for the pivot operation columns.
/// </summary>
Machinata.Reporting.Node.PivotTableNode.defaults.pivotColumn = null;

/// <summary>
/// Required. Defines the table column (id) to use for the pivot operation data.
/// </summary>
Machinata.Reporting.Node.PivotTableNode.defaults.pivotData = null;

/// <summary>
/// Required. Defines the pivot operation aggregation type.
/// Possible values are:
///  - ```percent```: calculates a pivot based on total sum, displayed in percent
/// </summary>
Machinata.Reporting.Node.PivotTableNode.defaults.pivotAggregation = null;

/// <summary>
/// Required. The D3 format to use for the pivot calculations.
/// </summary>
Machinata.Reporting.Node.PivotTableNode.defaults.pivotAggregationFormat = null;

/// <summary>
/// If ```true```, then a pivot calculation can be done on the same column. The data can be argued to be
/// meaningless, however there can be UX advantages to allowing this.
/// data. By default ```false```.
/// </summary>
Machinata.Reporting.Node.PivotTableNode.defaults.allowPivotOnSameColumn = false;

/// <summary>
/// If ```true```, then each pivot result fact can be drilled down on, exposing the underlying
/// data. By default ```true```.
/// </summary>
Machinata.Reporting.Node.PivotTableNode.defaults.allowPivotResultsDrilldown = true;

/// <summary>
/// If ```true```, the drilldown results table will expose the filters to the user, allow the user
/// to further explore the drilldown.
/// By default ```false```.
/// </summary>
Machinata.Reporting.Node.PivotTableNode.defaults.showPivotResultsDrilldownFilters = false;


/// <summary>
/// Defines the chart type to use for charts that have all positive values.
/// Default is ```DonutNode```.
/// </summary>
Machinata.Reporting.Node.PivotTableNode.defaults.chartTypePositiveValues = "DonutNode";

/// <summary>
/// Defines the chart type to use for charts that have negative values.
/// Default is ```VBarNode```.
/// </summary>
Machinata.Reporting.Node.PivotTableNode.defaults.chartTypeNegativeValues = "HBarNode";

/// <summary>
/// When ```true```, the chart legends are automaticall balanced so that they will have
/// the same count of rows, thus taking up the same amount of space.
/// </summary>
Machinata.Reporting.Node.PivotTableNode.defaults.chartBalancedLegends = true;

/// <summary>
/// Defines the number of columns for the chart legends.
/// Default is ```2```.
/// </summary>
Machinata.Reporting.Node.PivotTableNode.defaults.chartLegendColumns = 2;

/// <summary>
/// Pivots are by default ```light``` chrome.
/// </summary>
Machinata.Reporting.Node.PivotTableNode.defaults.chrome = "light";

/// <summary>
/// Rebuilds the pivot data and charts using the currently set ```pivotRow``` and ```pivotColumn```.
/// </summary>
Machinata.Reporting.Node.PivotTableNode.rebuild = function (instance, config, json, nodeElem) {
    
    // Debug
    Machinata.Reporting.debug("PivotTableNode.rebuild()");
    Machinata.Reporting.debug("  pivotRow: " + json.pivotRow);
    Machinata.Reporting.debug("  pivotColumn: " + json.pivotColumn);
    Machinata.Reporting.debug("  pivotData: " + json.pivotData);
    Machinata.Reporting.debug("  pivotAggregation: " + json.pivotAggregation);
    Machinata.Reporting.debug("  pivotFormat: " + json.pivotFormat);

    // Init
    var pivotElem = nodeElem.data("pivot-elem");
    pivotElem.empty();
    var formatterAggregation = d3.format(json.pivotAggregationFormat);


    // Create table data for pivotted data...
    var pivotData = {};
    pivotData._state = {};
    pivotData.columnDimension = {};
    pivotData.columnDimension.dimensionGroups = [{
        "id": json.columnDimension.dimensionGroups[0].id+"-pivot",
        "importance": 2,
        "dimensionElements": []
    }];
    pivotData.rowDimension = {};
    pivotData.rowDimension.dimensionGroups = [{
        "id": json.rowDimension.dimensionGroups[0].id + "-pivot",
        "importance": 2,
        "dimensionElements": []
    }];
    pivotData.allowRowFiltering = true;

    // Chart data
    //TODO: refactor chart title extractions....
    var chart1Title = json.pivotRow; //fallback
    $.each(json.columnDimension.dimensionGroups, function (index, dimensionGroupJSON) {
        $.each(dimensionGroupJSON.dimensionElements, function (index, dimensionElementJSON) {
            if (dimensionElementJSON.id == json.pivotRow) {
                var resolvedTitle = Machinata.Reporting.Text.resolve(instance,dimensionElementJSON.name);
                if (resolvedTitle != null) chart1Title = resolvedTitle;
            }
        });
    });
    var chart1Node = {
        "nodeType": json.chartTypePositiveValues,
        "theme": json.theme,
        "chrome": json.chrome,
        "sortFacts": "descending",
        "legendColumns": json.chartLegendColumns, 
        "series": [{
            "id": "s0",
            "title": { "resolved": chart1Title }, 
            "facts": []
        }]
    };
    var chart2Title = json.pivotColumn; // fallback
    $.each(json.columnDimension.dimensionGroups, function (index, dimensionGroupJSON) {
        $.each(dimensionGroupJSON.dimensionElements, function (index, dimensionElementJSON) {
            if (dimensionElementJSON.id == json.pivotColumn) {
                var resolvedTitle = Machinata.Reporting.Text.resolve(instance,dimensionElementJSON.name);
                if (resolvedTitle != null) chart2Title = resolvedTitle;
            }
        });
    });
    var chart2Node = {
        "nodeType": json.chartTypePositiveValues,
        "theme": json.theme,
        "chrome": json.chrome,
        "sortFacts": "descending",
        "legendColumns": json.chartLegendColumns,
        "series": [{
            "id": "s0",
            "title": { "resolved": chart2Title }, 
            "facts": []
        }]
    };
    var chartsLayoutNode = {
        "nodeType": "LayoutNode",
        "style": "CS_W_Two_Columns", //TODO
        "slotChildren": {
            "{left}": 0,
            "{right}": 1
        },
        "children": []
    };
    chartsLayoutNode.children.push(chart1Node);
    chartsLayoutNode.children.push(chart2Node);

    // Pivot table columns (based on pivotColumn) (label is not needed)
    {
        // Find all matching pivotColumn facts
        $.each(json.rowDimension.dimensionGroups, function (index, groupJSON) {
            $.each(groupJSON.dimensionElements, function (index, rowJSON) {
                $.each(rowJSON.facts, function (index, factJSON) {
                    // Match?
                    if (factJSON.col == json.pivotColumn) {
                        var factId = factJSON.resolved; //TODO
                        // New? We only add new discovieres
                        var exists = false;
                        $.each(pivotData.columnDimension.dimensionGroups[0].dimensionElements, function (index, columnJSON) {
                            if (columnJSON.id == factId) {
                                exists = true;
                                return;
                            }
                        });
                        if (exists == true) return;
                        var newColumnJSON = {
                            "id": factId,
                            "name": {
                                "resolved": factJSON.resolved
                            },
                            "sort": factJSON.val,
                            "importance": 2,
                            "valueType": "p-abs"
                        };
                        pivotData.columnDimension.dimensionGroups[0].dimensionElements.push(newColumnJSON);
                    }
                });
            });
        });
        //TODO: SORT!
        // Total column
        var totalColumnJSON = {
            "id": "total-col",
            "name": {
                "resolved": Machinata.Reporting.Text.translate(instance, "reporting.total-row")
            },
            "importance": 1,
            "valueType": "p-abs"
        };
        pivotData.columnDimension.dimensionGroups[0].dimensionElements.push(totalColumnJSON);
    }

    // Pivot table rows (based on pivotRow)
    {
        // Find all matching pivotRow facts
        $.each(json.rowDimension.dimensionGroups, function (index, groupJSON) {
            $.each(groupJSON.dimensionElements, function (index, rowJSON) {
                $.each(rowJSON.facts, function (index, factJSON) {
                    // Match?
                    if (factJSON.col == json.pivotRow) {
                        var factId = factJSON.resolved; //TODO
                        // New? We only add new discovieres
                        var exists = false;
                        $.each(pivotData.rowDimension.dimensionGroups[0].dimensionElements, function (index, columnJSON) {
                            if (columnJSON.id == factId) {
                                exists = true;
                                return;
                            }
                        });
                        if (exists == true) return;
                        // Create row definition
                        var newRowJSON = {
                            "id": factId,
                            "name": {
                                "resolved": factJSON.resolved
                            },
                            "importance": 2,
                            "facts": []
                        };
                        pivotData.rowDimension.dimensionGroups[0].dimensionElements.push(newRowJSON);
                    }
                });
            });
        });
        //TODO: SORT!
        // Total row

    }

    // Compile pivot aggregate values
    var totalRowData = {}; // contains the total for each column
    // Lets go through each cell in our pivot...
    $.each(pivotData.rowDimension.dimensionGroups[0].dimensionElements, function (index, privotRowJSON) {
        var rowTotal = 0;
        $.each(pivotData.columnDimension.dimensionGroups[0].dimensionElements, function (index, pivotColumnJSON) {
            //console.log(pivotColumnJSON.id + " x " + privotRowJSON.id);
            var sum = 0; // set to null to leave blank
            var hasValue = false;
            // Lets go through the original data to get total
            $.each(json.rowDimension.dimensionGroups, function (index, dataGroupJSON) {
                $.each(dataGroupJSON.dimensionElements, function (index, dataRowJSON) {
                    // Does the element match?
                    var val = null;
                    var rowMatches = false;
                    var columnMatches = false;
                    $.each(dataRowJSON.facts, function (index, dataFactJSON) {
                        if (dataFactJSON.col == json.pivotData) val = dataFactJSON.val;
                        if (dataFactJSON.col == json.pivotColumn && dataFactJSON.resolved == pivotColumnJSON.name.resolved) rowMatches = true;
                        if (dataFactJSON.col == json.pivotRow && dataFactJSON.resolved == privotRowJSON.name.resolved) columnMatches = true;
                    });
                    //console.log("rowMatches="+rowMatches+", columnMatches="+columnMatches+", val="+val);
                    if (rowMatches == true && columnMatches == true && val != null) {
                        if (sum == null) sum = 0;
                        sum += val;
                        hasValue = true;
                        //console.log("match! new sum = "+sum);
                    }
                });
            });
            // Create fact
            if (pivotColumnJSON.id == "total-col") sum = rowTotal;
            var newPivotFact = {
                "val": sum,
                "resolved": sum,
                "col": pivotColumnJSON.id
            };
            // Fact drilldown
            if (json.allowPivotResultsDrilldown == true) {
                var drilldown = {};
                drilldown.filters = [];
                if (pivotColumnJSON.id != "total-col") {
                    drilldown.filters.push({
                        "columnId": json.pivotColumn,
                        "isAny": [ Machinata.Reporting.Text.resolve(instance, pivotColumnJSON.name) ]
                    });
                }
                drilldown.filters.push({
                    "columnId": json.pivotRow,
                    "isAny": [ Machinata.Reporting.Text.resolve(instance, privotRowJSON.name) ]
                });
                if (hasValue == true || pivotColumnJSON.id == "total-col") {
                    newPivotFact.link = {
                        "tooltip": {
                            "resolved": Machinata.Reporting.Text.translate(instance, "reporting.tables.drilldown-on") + Machinata.Reporting.Text.resolve(instance,pivotColumnJSON.name) + " " + Machinata.Reporting.Text.resolve(instance,privotRowJSON.name)
                        },
                        "elementId": json.id + "_DATA",
                        "drilldown": drilldown
                    };
                }
            }
            privotRowJSON.facts.push(newPivotFact);
            if (pivotColumnJSON.id == "total-col") privotRowJSON.totalFact = newPivotFact; // shortcut for later
            if (sum != null && rowTotal == null) rowTotal = 0;
            if (sum != null) rowTotal += sum;
            // Register in totals row
            if (totalRowData[pivotColumnJSON.id] == null && sum != null) totalRowData[pivotColumnJSON.id] = 0;
            if (sum != null) totalRowData[pivotColumnJSON.id] += sum;
        });
    });
    // Add totals row
    var totalSum = null;
    {
        // Create row json 
        var newRowJSON = {
            "id": "total-row",
            "name": {
                "resolved": "Total" //TODO
            },
            "importance": 1,
            "facts": []
        };
        pivotData.rowDimension.dimensionGroups[0].dimensionElements.push(newRowJSON);
        // Add each total column cell
        $.each(totalRowData, function (key, val) {
            // Totals fact
            var newPivotFact = {
                "val": val,
                "resolved": val,
                "col": key
            };
            if (key == "total-col") totalSum = val;
            // Fact drilldown
            if (json.allowPivotResultsDrilldown == true) {
                var tooltip = Machinata.Reporting.Text.translate(instance, "reporting.tables.drilldown-on-total");
                var drilldown = {};
                drilldown.filters = [];
                if (key != "total-col") {
                    drilldown.filters.push({
                        "columnId": json.pivotColumn,
                        "value": key
                    });
                    tooltip += " " + key;
                }
                newPivotFact.link = {
                    "tooltip": {
                        "resolved": tooltip
                    },
                    "elementId": json.id + "_DATA",
                    "drilldown": drilldown
                };
            }
            newRowJSON.facts.push(newPivotFact);
        });
    }
    //console.log("pivot total = " + totalSum);


    // Make percent?
    if (json.pivotAggregation == "percent") {
        // Go through each row and update
        $.each(pivotData.rowDimension.dimensionGroups, function (index, pivotGroupJSON) {
            $.each(pivotGroupJSON.dimensionElements, function (index, pivotRowJSON) {
                $.each(pivotRowJSON.facts, function (index, pivotFactJSON) {
                    //pivotFactJSON.tooltip = {"resolved":pivotFactJSON.val};
                    pivotFactJSON.val = pivotFactJSON.val / totalSum;
                    pivotFactJSON.resolved = formatterAggregation(pivotFactJSON.val);
                });
            });
        });
    }

    // Create chart1/chart2 data facts
    var allValuesPositive = true;
    $.each(pivotData.rowDimension.dimensionGroups[0].dimensionElements, function (index, pivotRowJSON) {
        if (pivotRowJSON.id == "total-row") {
            // This is our chart 2
            $.each(pivotRowJSON.facts, function (index, factJSON) {
                if (factJSON.col == "total-col") return;
                if (factJSON.val < 0) allValuesPositive = false;
                chart2Node.series[0].facts.push(
                    {
                        "val": factJSON.val,
                        "resolved": factJSON.resolved,
                        "category": { "resolved": factJSON.col }
                    }
                );
            });
        } else if (pivotRowJSON.totalFact != null) {
            // This our chart 1
            if (pivotRowJSON.totalFact.val < 0) allValuesPositive = false;
            chart1Node.series[0].facts.push(
                {
                    "val": pivotRowJSON.totalFact.val,
                    "resolved": pivotRowJSON.totalFact.resolved,
                    "category": { "resolved": Machinata.Reporting.Text.resolve(instance,pivotRowJSON.name) }
                }
            );
        }
    });

    // Change chart type based on whether there exist negative values
    if (allValuesPositive != true) {
        chart1Node.nodeType = json.chartTypeNegativeValues;
        chart2Node.nodeType = json.chartTypeNegativeValues;
    }

    // Balance the chart legends?
    if (json.chartBalancedLegends == true && chart1Node != null && chart2Node != null) {
        // Here we fix the legend rows to the max of both charts, taking into account the columns configured.
        var maxFacts = Math.max(chart1Node.series[0].facts.length, chart2Node.series[0].facts.length);
        chart1Node.legendMinRows = Math.ceil(maxFacts / (chart1Node.legendColumns || 1));
        chart2Node.legendMinRows = Math.ceil(maxFacts / (chart2Node.legendColumns || 1));
    }

    // Create charts
    var chartsElem = $("<div class='charts'></div>");
    pivotElem.append(chartsElem);
    {
        var chartsNodeElem = Machinata.Reporting.buildContents(instance, config, chartsLayoutNode, json, chartsElem);
        //Machinata.Responsive.detectNewResponsiveElements(); // deprecated
        //Machinata.Responsive.updateResponsiveElements(); // deprecated
        nodeElem.data("charts-elem", chartsElem);
        chartsElem.find(".machinata-reporting-node").trigger("machinata-reporting-redraw-node");
    }

    // Create table elem with pivoted data
    var tablePivotElem = Machinata.Reporting.Node.VerticalTableNode.buildTable(instance, config, pivotData, nodeElem, {
        interactive: true
    });
    tablePivotElem.attr("id", json.id + "_PIVOT");
    tablePivotElem.addClass("pivot-table");
    pivotElem.append(tablePivotElem);

    


};

/// <summary>
/// </summary>
Machinata.Reporting.Node.PivotTableNode.init = function (instance, config, json, nodeElem) {


    // Create dimension selector
    var selectWrapperElem = $('<div class="machinata-reporting-select-wrapper"><select class="machinata-reporting-select"></select></div>');
    selectWrapperElem.append(Machinata.Reporting.buildIcon(instance,"chevron-down"));
    {
        var selectElem = selectWrapperElem.find("select");
        $.each(json.columnDimension.dimensionGroups, function (index, groupJSON) {
            $.each(groupJSON.dimensionElements, function (index, columnJSON) {
                // Exclude the pivot data row (doesnt make sense)
                if (columnJSON.id == json.pivotData) return;
                // Create option
                var optElem = $("<option></option>");
                optElem.text(Machinata.Reporting.Text.resolve(instance,columnJSON.name));
                optElem.attr("value", columnJSON.id);
                optElem.attr("data-column-id", columnJSON.id);
                selectElem.append(optElem);
                //TODO: Set default pivot selections
            });
        });
    }

    // Create configuration UI
    var configurationElem = $("<div class='node-config machinata-reporting-config'></div>");
    var pivotRowSelectElem = selectWrapperElem.clone().appendTo(configurationElem);
    pivotRowSelectElem.find("option[value='" + json.pivotRow + "']").attr("selected", "selected");
    pivotRowSelectElem.on("change", function () {
        // Validate the change
        var newRow = pivotRowSelectElem.find(":selected").attr("value");
        if (newRow == json.pivotColumn && json.allowPivotOnSameColumn != true) {
            // Revert back
            pivotRowSelectElem.find("select").val(json.pivotRow);
            return;
        }
        json.pivotRow = newRow;
        Machinata.Reporting.Node.PivotTableNode.rebuild(instance, config, json, nodeElem);
    });
    nodeElem.data("pivot-row-select", pivotRowSelectElem);
    var crossElem = Machinata.Reporting.buildIcon(instance,"cross").addClass("machinata-reporting-cross");
    configurationElem.append(crossElem);
    var pivotColumnSelectElem = selectWrapperElem.clone().appendTo(configurationElem);
    pivotColumnSelectElem.find("option[value='" + json.pivotColumn + "']").attr("selected", "selected");
    pivotColumnSelectElem.on("change", function () {
        // Validate the change
        var newColumn = pivotColumnSelectElem.find(":selected").attr("value");
        if (newColumn == json.pivotRow && json.allowPivotOnSameColumn != true) {
            // Revert back
            pivotColumnSelectElem.find("select").val(json.pivotColumn);
            return;
        }
        json.pivotColumn = newColumn
        Machinata.Reporting.Node.PivotTableNode.rebuild(instance, config, json, nodeElem);
    });
    nodeElem.data("pivot-column-select", pivotColumnSelectElem);
    configurationElem.append("<div class='clear'></div>");
    nodeElem.append(configurationElem);

    // Pivot results container
    var pivotElem = $("<div class='pivot-results'></div>").appendTo(nodeElem);
    nodeElem.data("pivot-elem", pivotElem);


    // Create table elem with drilldown data
    if (json.allowPivotResultsDrilldown == true) {

        // Create table data for drilldown (just a copy of the driving data)...
        var drilldownData = {};
        drilldownData._state = {};
        drilldownData.id = json.id + "_DATA";
        drilldownData.columnDimension = json.columnDimension;
        drilldownData.rowDimension = json.rowDimension;
        if (json.showPivotResultsDrilldownFilters == true) {
            drilldownData.showRowFiltering = true;
        }

        // Table
        var tableDrilldownElem = Machinata.Reporting.Node.VerticalTableNode.buildTable(instance, config, drilldownData, nodeElem, {
            interactive: true
        });
        Machinata.Reporting.Tools.makeTableSortable(instance, tableDrilldownElem);
        Machinata.Reporting.Tools.makeTableFilterable(instance, tableDrilldownElem);
        tableDrilldownElem.hide();
        tableDrilldownElem.attr("id", drilldownData.id);
        tableDrilldownElem.addClass("drilldown-table");
        nodeElem.append(tableDrilldownElem);
    }

    // Do rebuild of pivot data
    Machinata.Reporting.Node.PivotTableNode.rebuild(instance, config, json, nodeElem);

    // Export
    Machinata.Reporting.Node.registerExportOptions(instance, config, json, nodeElem, {
        "xlsx": "Excel",
        "csv": "CSV",
    });

};

/// <summary>
/// </summary>
Machinata.Reporting.Node.PivotTableNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {

    Machinata.Reporting.debug("Machinata.Reporting.Node.PivotTableNode.exportFormat", instance, config, json, nodeElem, format, filename);

    if (format == "xlsx" || format == "csv") {
      
        var tempTableElem = Machinata.Reporting.Node.VerticalTableNode.buildTable(instance, config, json, nodeElem, {
            interactive: false,
            includeFormattingInformation: true,
        });

        var workbook = Machinata.Reporting.Export.createExcelWorkbook(instance, config, json);
        workbook = Machinata.Reporting.Export.createExcelDataForTable(instance, config, json, workbook, tempTableElem, format);
        Machinata.Reporting.Export.saveExcelWorkbook(instance, config, json, workbook, filename);
        return true;
    }
   
    return false;
};

/// <summary>
/// </summary>
Machinata.Reporting.Node.PivotTableNode.draw = function (instance, config, json, nodeElem) {
    
};









