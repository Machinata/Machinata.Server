


/// <summary>
/// BETA
///
/// ### Bucket Titles
/// The bucket titles are automatically line-breaked, 
/// but you can also define your own using the \n character. A bucket title should never
/// be more than two lines.
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.AllocationTreeNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.AllocationTreeNode.defaults = {};

/// <summary>
/// This is a infographic, thus ```light```.
/// </summary>
Machinata.Reporting.Node.AllocationTreeNode.defaults.chrome = "light";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.AllocationTreeNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.AllocationTreeNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.AllocationTreeNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_4x2, Machinata.Reporting.Layouts.BLOCK_4x4];

/// <summary>
/// Yes, we support toolbar.
/// </summary>
Machinata.Reporting.Node.AllocationTreeNode.defaults.supportsToolbar = true;

/// <summary>
/// No legends for this chart.
/// </summary>
Machinata.Reporting.Node.AllocationTreeNode.defaults.insertLegend = false;


/// <summary>
/// </summary>
Machinata.Reporting.Node.AllocationTreeNode.defaults.titleMaxCharsPerLine = 20;

/// <summary>
/// </summary>
Machinata.Reporting.Node.AllocationTreeNode.defaults.titleFontWeight = "bold";


/// <summary>
/// </summary>
Machinata.Reporting.Node.AllocationTreeNode.defaults.subTitleMaxCharsPerLine = 20;


/// <summary>
/// Spacing between tree nodes, in paddings.
/// </summary>
Machinata.Reporting.Node.AllocationTreeNode.defaults.horizontalSpacing = 6;

/// <summary>
/// </summary>
Machinata.Reporting.Node.AllocationTreeNode.getVegaSpec = function (instance, config, json, nodeElem) {
    var DEBUG_ENABLED = Machinata.Reporting.isDebugEnabled();
    if (DEBUG_ENABLED) console.log(json._state.factsFlattened);
    return {
        "data": [
            {
                "name": "facts",
                "values": json._state.factsFlattened
            },
            {
                "name": "factsResolved",
                "source": "facts",
                "transform": [
                    
                ]
            },
            
        ],
        "signals": [
            
            {
                "name": "maxTreeRows",
                "init": json._state.maxTreeRows
            },
            {
                "name": "maxTreeColumns",
                "init": json._state.maxTreeColumns
            },
            {
                "name": "columnSpacing",
                "init": "config_padding * " + json.horizontalSpacing
            },
            {
                "name": "factWidth",
                "update": "(width - (columnSpacing * (maxTreeColumns-1))) / maxTreeColumns"
            },
            {
                "name": "factHeight",
                "update": "(config_chartTextSize * 2) + (config_chartTextSize * 2) + (2 * config_chartTextSize) * config_lineHeight" // 2 lines title, 2 lines subtitle, 2 line spacing
            },
            {
                "name": "factTitleSize",
                "init": "config_chartTextSize"
            },
            {
                "name": "factSubtitleSize",
                "init": "config_chartTextSize"
            },
            {
                "name": "factConnectionLineSize",
                "init": config.lineSize
            },
            {
                "name": "treeWidth",
                "update": "width - factWidth/2 - factWidth/2"
            },
            {
                "name": "treeHeight",
                "update": "height - factHeight/2 - factHeight/2"
            },
        ],
        "scales": [
            
            
        ],
        "marks": [
            {
                "clip": true, // makes sure that when we scale it doesnt push open the chart size
                "type": "group",
                "data": [
                    {
                        "name": "tree",
                        "source": "factsResolved",
                        "transform": [
                            {
                                "type": "stratify",
                                "key": "treeNodeId",
                                "parentKey": "treeParentId"
                            },
                            {
                                // https://vega.github.io/vega/docs/transforms/tree/
                                "type": "tree",
                                "method": "tidy",
                                "size": [{ "signal": "1" }, { "signal": "1" }], // our tree is logically sized from 0..1
                                "separation": true,
                                "as": ["y", "x", "depth", "children"]
                            }
                        ]
                    }, 
                    {
                        "name": "treelinks",
                        "source": "tree",
                        "transform": [
                            { "type": "treelinks" },
                        ]
                    },
                ],
                "scales": [
                    {
                        // A special scale to map the tree Y to fill the entire Y space (since vega doesnt do this on Y automatically)
                        "name": "treeYFit",
                        "type": "linear",
                        "zero": false,
                        "domain": { "data": "tree", "field": "y" },
                        "range": [0, 1],
                    },
                    {
                        "name": "treeDepthBGColor",
                        "type": "ordinal",
                        "domain": { "data": "tree", "field": "depth" },
                        "range": { "scheme": json.theme + "-bg" }
                    },
                    {
                        "name": "treeDepthFGColor",
                        "type": "ordinal",
                        "domain": { "data": "tree", "field": "depth" },
                        "range": { "scheme": json.theme + "-fg" }
                    },
                ],
                "encode": {
                    "update": {
                        "x": { "signal": "width/2 - treeWidth/2" },
                        "y": { "signal": "height/2 - treeHeight/2" },
                        "width": { "signal": "treeWidth" },
                        "height": { "signal": "treeHeight" },
                        "stroke": DEBUG_ENABLED ? { "signal": "'red'" } : null
                    }
                },
                "signals": [
                ],
                "marks": [
                    /*{
                        "type": "path",
                        "from": { "data": "linkpath" },
                        "encode": {
                            "update": {
                                "path": { "field": "path" },
                                "stroke": { "value": "#ccc" }
                            }
                        }
                    },*/
                    {
                        // Orthogonal lines to connect the nodes
                        "type": "group",
                        "from": { "data": "treelinks" },
                        "scales": [
                        ],
                        "signals": [
                            {
                                "name": "factX",
                                "update": "(parent.target.x * treeWidth)"
                            },
                            {
                                "name": "factY",
                                "update": "(scale('treeYFit',parent.target.y) * treeHeight)"
                            },
                            {
                                "name": "factTreeLeafNumber",
                                "update": "parent.target.treeLeafNumber"
                            },
                            {
                                "name": "factNumLeafs",
                                "update": "parent.target.treeNumLeafs"
                            },
                            {
                                "name": "parentX",
                                "update": "(parent.source.x * treeWidth)"
                            },
                            {
                                "name": "parentYConnectionOffsetInPercent",
                                "update": "factNumLeafs == 1 ? (0) : (((factTreeLeafNumber-1)/(factNumLeafs-1))-0.5)"
                            },
                            {
                                "name": "parentYConnectionOffsetInPixels",
                                "update": "parentYConnectionOffsetInPercent * (factHeight/3)"
                            },
                            {
                                "name": "parentY",
                                "update": "(scale('treeYFit',parent.source.y) * treeHeight) + parentYConnectionOffsetInPixels"
                            },
                            //                                  xxxxxxxxxxxxxxx     
                            //                         B        x             x  
                            //                          xxxxxxxxx A           x  
                            //                          x       x             x  
                            //                          x       xxxxxxxxxxxxxxx 
                            //                          x                       
                            //      PARENT              x                       
                            // xxxxxxxxxxxxxxx          x
                            // x             x          x                   
                            // x           D xxxxxxxxxxxx C                                
                            // x             x                                 
                            // xxxxxxxxxxxxxxx                                    
                            {
                                "name": "pathToParentAX",
                                "update": "factX - factWidth/2",
                            },
                            {
                                "name": "pathToParentAY",
                                "update": "factY",
                            },
                            {
                                "name": "pathToParentDX",
                                "update": "parentX + factWidth/2",
                            },
                            {
                                "name": "pathToParentDY",
                                "update": "parentY",
                            },
                            {
                                "name": "pathToParentBX",
                                "update": "(pathToParentAX + pathToParentDX)/2",
                            },
                            {
                                "name": "pathToParentBY",
                                "update": "pathToParentAY",
                            },
                            {
                                "name": "pathToParentCX",
                                "update": "(pathToParentAX + pathToParentDX)/2",
                            },
                            {
                                "name": "pathToParentCY",
                                "update": "pathToParentDY",
                            },

                        ],
                        "marks": [
                            {
                                "type": "rule",
                                "encode": {
                                    "enter": {
                                        "strokeWidth": { "signal": "factConnectionLineSize" },
                                    },
                                    "update": {
                                        "x": { "signal": "pathToParentAX" },
                                        "y": { "signal": "pathToParentAY" },
                                        "x2": { "signal": "pathToParentBX" },
                                        "y2": { "signal": "pathToParentBY" },
                                    }
                                }
                            },
                            {
                                "type": "rule",
                                "encode": {
                                    "enter": {
                                        "strokeWidth": { "signal": "factConnectionLineSize" },
                                    },
                                    "update": {
                                        "x": { "signal": "pathToParentBX" },
                                        "y": { "signal": "pathToParentBY" },
                                        "x2": { "signal": "pathToParentCX" },
                                        "y2": { "signal": "pathToParentCY" },
                                    }
                                }
                            },
                            {
                                "type": "rule",
                                "encode": {
                                    "enter": {
                                        "strokeWidth": { "signal": "factConnectionLineSize" },
                                    },
                                    "update": {
                                        "x": { "signal": "pathToParentCX" },
                                        "y": { "signal": "pathToParentCY" },
                                        "x2": { "signal": "pathToParentDX" },
                                        "y2": { "signal": "pathToParentDY" },
                                    }
                                }
                            },
                        ],
                    },
                    {
                        // Fact
                        "type": "group",
                        "from": { "data": "tree" },
                        "encode": {
                            "enter": {
                                "fill": { "signal": "datum.colorShade != null ? scale('theme-bg',datum.colorShade) : scale('treeDepthBGColor',datum.depth)" },
                            },
                            "update": {
                                "width": { "signal": "factWidth" },
                                "height": { "signal": "factHeight" },
                                "xc": { "signal": "datum.x * treeWidth" },
                                "yc": { "signal": "scale('treeYFit',datum.y) * treeHeight" },
                            }
                        },
                        "marks": [
                            {
                                // Fact title
                                "type": "text",
                                "encode": {
                                    "enter": {
                                        "text": { "signal": "parent.titleResolved" },
                                        "fill": { "signal": "parent.colorShade != null ? scale('theme-fg',parent.colorShade) : scale('treeDepthFGColor',parent.depth)" },
                                        "fontSize": { "signal": "factTitleSize" },
                                        "fontWeight": { "value": json.titleFontWeight },
                                        "align": { "value": "center" },
                                        "baseline": { "signal": "parent.subTitleSet ? 'bottom' : 'middle'" },
                                        "dy": { "signal": "parent.subTitleSet ? -(factTitleSize*1.0*config_lineHeight ) : -(factTitleSize*(parent.titleResolved.length-1)*config_lineHeight/2)" },
                                    },
                                    "update": {
                                        "x": { "signal": "factWidth / 2" },
                                        "y": { "signal": "factHeight / 2" },
                                    }
                                }
                            },
                            {
                                // Fact subtitle
                                "type": "text",
                                "encode": {
                                    "enter": {
                                        "text": { "signal": "parent.subTitleResolved" },
                                        "fill": { "signal": "parent.colorShade != null ? scale('theme-fg',parent.colorShade) : scale('treeDepthFGColor',parent.depth)" },
                                        "fontSize": { "signal": "factSubtitleSize" },
                                        "align": { "value": "center" },
                                        "baseline": { "signal": "parent.titleSet ? 'top' : 'middle'" },
                                        "dy": { "signal": "parent.titleSet ? (factSubtitleSize*1.0*config_lineHeight) : 0" },
                                    },
                                    "update": {
                                        "x": { "signal": "factWidth / 2" },
                                        "y": { "signal": "factHeight / 2" },
                                    }
                                }
                            }
                        ]
                    },
                    DEBUG_ENABLED ? {
                        // DEBUG
                        "type": "symbol",
                        "from": { "data": "tree" },
                        "encode": {
                            "enter": {
                                "tooltip": { "signal": "datum" },
                                "size": { "value": 40 },
                                "fill": { "signal": "'red'" }
                            },
                            "update": {
                                "x": { "signal": "datum.x * treeWidth" },
                                "y": { "signal": "scale('treeYFit',datum.y) * treeHeight" },
                            }
                        }
                    } : { "type":"group"},
                ]
            }
          
        ]
    };
};
Machinata.Reporting.Node.AllocationTreeNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    
};

Machinata.Reporting.Node.AllocationTreeNode.init = function (instance, config, json, nodeElem) {

    // Flatten tree
    var factsFlattened = [];
    json._state.maxTreeColumns = 0;
    json._state.maxTreeRows = 0;
    var numLeafsForColumn = {};
    var nodeId = 0;
    function processFacts(facts, depth, parent) {
        for (var i = 0; i < facts.length; i++) {
            var fact = facts[i];
            // Line break
            fact.titleResolved = Machinata.Reporting.Tools.lineBreakString(Machinata.Reporting.Text.resolve(instance, fact.title), {
                maxCharsPerLine: json.titleMaxCharsPerLine,
                maxLines: 2,
                wordSeparator: " ",
                ellipsis: config.textTruncationEllipsis,
                appendEllipsisOnLastLine: true,
                returnArray: true
            });
            fact.subTitleResolved = Machinata.Reporting.Tools.lineBreakString(Machinata.Reporting.Text.resolve(instance, fact.subTitle), {
                maxCharsPerLine: json.subTitleMaxCharsPerLine,
                maxLines: 2,
                wordSeparator: " ",
                ellipsis: config.textTruncationEllipsis,
                appendEllipsisOnLastLine: true,
                returnArray: true
            });
            fact.titleSet = !(fact.titleResolved.length == 1 && fact.titleResolved[0] == '');
            fact.subTitleSet = !(fact.subTitleResolved.length == 1 && fact.subTitleResolved[0] == '');
            // Apply tree properties
            fact.treeColumn = depth;
            fact.treeRow = i;
            fact.treeLeafNumber = i + 1;
            fact.treeNumLeafs = facts.length;
            if (fact.treeNodeId == null) fact.treeNodeId = nodeId++;
            if (parent != null) fact.treeParentId = parent.treeNodeId;
            factsFlattened.push(fact);
            // Bookkeeping
            if (numLeafsForColumn[fact.treeColumn] == null) numLeafsForColumn[fact.treeColumn] = 0;
            numLeafsForColumn[fact.treeColumn] = numLeafsForColumn[fact.treeColumn] + 1;
            // Recurse children
            if (fact.subFacts != null) processFacts(fact.subFacts, depth + 1, fact);
        }
    }
    processFacts(json.facts, 0, null);
    // Apply final values
    for (var i = 0; i < factsFlattened.length; i++) {
        var fact = factsFlattened[i];
        fact.treeNumLeafsForDepth = numLeafsForColumn[fact.treeColumn];
        if (fact.treeNumLeafsForDepth > json._state.maxTreeRows) json._state.maxTreeRows = fact.treeNumLeafsForDepth;
        if (fact.treeColumn + 1 > json._state.maxTreeColumns) json._state.maxTreeColumns = fact.treeColumn + 1;
    }
    // Apply to state
    json._state.factsFlattened = factsFlattened;
    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.AllocationTreeNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.AllocationTreeNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};







