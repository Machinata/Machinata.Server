


/// <summary>
/// </summary>
/// <example>
/// See ```example-report-packs.json``` for complete example usages.
/// </example>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.SunburstNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults = {};

/// <summary>
/// 
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.chrome = "light";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_4x2, Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1];

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.supportsToolbar = true;

/// <summary>
/// Yes, insert a legend by default.
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.insertLegend = true;

/// <summary>
/// If ```true```, includes the serie value in the legend label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.legendLabelsShowSerie = true;

/// <summary>
/// Sets the correct legend data source for the automatic legend.
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.legendDataSource = "factsLegend";

/// <summary>
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.sliceLabelsShowValue = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.sliceLabelsShowCategory = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.SunburstNode.defaults.sliceLabelsShowPercentage = true;

Machinata.Reporting.Node.SunburstNode.getVegaSpec = function (instance, config, json, nodeElem) {
    // Create flat version of all facts
    var facts = [];
    var currentId = 0;
    var valFormat = null;
    // Create root fact
    var rootFact = {
        _id: currentId++,
        _parent: null,
        _val: 0
    };
    facts.push(rootFact);
    // Create all series facts...
    for (var s = 0; s < json.series.length; s++) {
        var serie = json.series[s];
        var serieFact = {
            serieId: serie.id,
            colorShade: serie.colorShade,
            color: serie.color,
            title: serie.title,
            _id: currentId++,
            _parent: rootFact._id,
            _val: 0
        };
        facts.push(serieFact);
        for (var f = 0; f < serie.facts.length; f++) {
            var fact = serie.facts[f];
            fact._id = currentId++;
            fact._parent = serieFact._id;
            fact._val = fact.val;
            fact.serieId = serieFact.serieId;
            if (fact.colorShade == null) fact.colorShade = serieFact.colorShade;
            fact.color = serieFact.color;
            facts.push(fact);
            if (fact.format != null && valFormat == null) valFormat = fact.format;
            // Bookkeeping on series values
            serieFact._val += fact.val;
            rootFact._val += fact.val;
        }
    }
    // Get formats
    valFormat = valFormat || ".0f";
    var valFormatter = d3.format(valFormat);
    var percentFormatter = d3.format(".0%"); //TODO: how can the user configure this?
    // Resolve facts
    for (var f = 0; f < facts.length; f++) {
        var fact = facts[f];
        if (fact._parent == null) continue;
        // Resolve title
        if (fact.title != null) fact._titleResolved = fact.title.resolved;
        else if (fact.category != null) fact._titleResolved = fact.category.resolved;
        // Calculate percentage
        fact._valPercent = fact._val / rootFact._val;
        // Resolve tooltip
        fact._tooltipResolved = fact._titleResolved + ": " + valFormatter(fact._val) + ", " + percentFormatter(fact._valPercent);
        // Resolve label
        fact._labelResolved = [];
        if (json.sliceLabelsShowCategory == true && fact._titleResolved == "") {
            // In this case the user wants to show the category but some series/facts may
            // be blank: so we hide those labels...
            fact._showLabel = false;
        } else {
            fact._showLabel = true;
            if (json.sliceLabelsShowCategory == true) fact._labelResolved.push(fact._titleResolved);
            if (json.sliceLabelsShowValue == true && json.sliceLabelsShowPercentage == true) fact._labelResolved.push(valFormatter(fact._val) + ", " + percentFormatter(fact._valPercent));
            else if (json.sliceLabelsShowValue == true) fact._labelResolved.push(valFormatter(fact._val));
            else if (json.sliceLabelsShowPercentage == true) fact._labelResolved.push(percentFormatter(fact._valPercent));
        }
    }
    // Status?
    var statusTitle = null;
    var statusVal = null;
    if (json.status != null) {
        if (json.status.title != null) statusTitle = Machinata.Reporting.Text.resolve(instance, json.status.title);
        if (json.status.value != null) statusVal = json.status.value;
    } 
    return {
        "data": [
            {
                "name": "facts",
                "values": facts
            },
          {
              "name": "factsPartitioned",
              "source": "facts",
              "transform": [
                {
                      "type": "stratify",
                      "key": "_id",
                      "parentKey": "_parent"
                },
                {
                    "type": "partition",
                    "field": "val",
                    "round": false,
                    "sort": { "field": "val"},
                    "size": [{ "signal": "2 * PI" }, { "signal": "arcSize" }],
                    "as": ["a0", "r0", "a1", "r1", "depth", "children"]
                }
              ]
            },
            {
                "name": "factsPartitionedDepth1",
                "source": "factsPartitioned",
                "transform": [
                    { "type": "filter", "expr": "datum.depth == 1" },
                ]
            },
          {
              "name": "factsResolved",
              "source": "factsPartitioned",
              "transform": [
                  { "type": "filter", "expr": "datum.depth > 0" }, // exclude root
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "colorResolved",
                      "expr": "datum.color != null ? datum.color : ( datum.colorShade != null ? scale('theme-bg',datum.colorShade) : scale('color',datum.serieId) )"
                  },
                  {
                      "type": "formula",
                      "initonly": true,
                      "as": "textColorResolved",
                      "expr": "datum.colorShade != null ? scale('theme-fg',datum.colorShade) : scale('textColor',datum.serieId)"
                  },
              ]
            },
          {
              "name": "factsLegend",
              "source": "factsResolved",
              "transform": [
                    { "type": "filter", "expr": "datum.depth == 1" },
              ]
          }
        ],
        "signals": [
            {
                "name": "arcSize",
                "update": "round( (min(width,height) / 2)*0.96 )"
            },
            {
                "name": "valFormat",
                "init": "'" + valFormat + "'"
            },
            {
                "name": "statusVal",
                "init": statusVal
            },
        ],
        "scales": [
            {
                "name": "color",
                "type": "ordinal",
                "domain": { "data": "factsPartitionedDepth1", "field": "serieId" },
                "range": { "scheme": json.theme + "-bg" }
            },
            {
                "name": "textColor",
                "type": "ordinal",
                "domain": { "data": "factsPartitionedDepth1", "field": "serieId" },
                "range": { "scheme": json.theme + "-fg" }
            },
            {
                "name": "legendColor",
                "type": "ordinal",
                "domain": { "data": "factsLegend", "field": "serieId" },
                "range": { "data": "factsLegend", "field": "colorResolved" },
            },
        ],
        "marks": [
            {
                "type": "arc",
                "from": { "data": "factsResolved" },
                "encode": {
                    "enter": {
                        "fill": { "signal": "datum.colorResolved" },
                        //"opacity": { "signal": "datum.depth == 0 ? 0 : datum.depth == 1 ? 1.0 : 1/datum.depth" }
                        "tooltip": { "signal": "datum._tooltipResolved" },
                        "startAngle": { "field": "a0" },
                        "endAngle": { "field": "a1" },
                        "innerRadius": { "field": "r0" },
                        "outerRadius": { "field": "r1" },
                        "stroke": { "value": "white" },
                        "strokeWidth": { "value": config.domainLineSize },
                    },
                    "update": {
                        "x": { "signal": "width / 2" },
                        "y": { "signal": "height / 2" },
                        //"zindex": { "value": 0 },
                    }
                }
            },
            {
                // Text background stroke to ensure the real thing is visible...
                "name": "markSliceLabelsInsideCenterTextBGOutline",
                "type": "text",
                "from": {
                    "data": "factsResolved"
                },
                "encode": {
                    "enter": {
                        "text": {
                            "signal": "datum._labelResolved"
                        },
                        "fill": { "signal": "datum.colorResolved" },
                        "stroke": { "signal": "datum.colorResolved" },
                        "strokeWidth": { "value": 3 },
                        //"limit": { "value": json.sliceLabelsLengthLimit },
                        "fontSize": { "signal": "config_chartTextSize" },
                        "align": { "signal": "'center'" },
                        "dy": { "signal": "-(length(datum._labelResolved) * config_chartTextSize)/2 " }, // center lines vertically
                        "baseline": { "signal": "'middle'" },
                        "tooltip": { "signal": "datum._tooltipResolved" }
                    },
                    "update": {
                        "x": {
                            "signal": "width / 2"
                        },
                        "y": {
                            "signal": "height / 2"
                        },
                        "radius": {
                            "signal": "(datum.r1+datum.r0)/2"
                        },
                        "theta": {
                            "signal": "(datum.a1+datum.a0)/2"
                        }
                    }
                }
            },
            {
                "name": "markSliceLabelsInsideCenterText",
                "type": "text",
                "from": {
                    "data": "factsResolved"
                },
                "encode": {
                    "enter": {
                        "text": {
                            "signal": "datum._labelResolved"
                        },
                        "fill": { "signal": "datum.textColorResolved" },
                        //"limit": { "value": json.sliceLabelsLengthLimit },
                        "fontSize": { "signal": "config_chartTextSize" },
                        "align": { "signal": "'center'" },
                        "dy": { "signal": "-(length(datum._labelResolved) * config_chartTextSize)/2 " }, // center lines vertically
                        "baseline": { "signal": "'middle'" },
                        "tooltip": { "signal": "datum._tooltipResolved" }
                    },
                    "update": {
                        "x": {
                            "signal": "width / 2"
                        },
                        "y": {
                            "signal": "height / 2"
                        },
                        "radius": {
                            "signal": "(datum.r1+datum.r0)/2"
                        },
                        "theta": {
                            "signal": "(datum.a1+datum.a0)/2"
                        }
                    }
                }
            },
            {
                "type": "text",
                /*"_comment": "STATUS TITLE LABEL",*/
                "encode": {
                    "enter": {
                        "align": { "value": "center" },
                        "baseline": { "value": "bottom" },
                        "text": { "value": statusTitle },
                        //"fill": { "value": "red" }
                    },
                    "update": {
                        "fontSize": { "signal": "config_labelSize" },
                        "dy": { "signal": "-config_labelSize/3" },
                        "x": { "signal": "width/2" },
                        "y": { "signal": "height/2" },
                    }
                }
            },
            {
                "type": "text",
                /*"_comment": "STATUS VAL LABEL",*/
                "encode": {
                    "enter": {
                        "dy": { "signal": "0" },
                        "align": { "value": "center" },
                        "baseline": { "value": "top" },
                        "text": { "signal": "statusVal != null ? format(statusVal,valFormat) : null" },
                        //"fill": { "value": "red" }
                    },
                    "update": {
                        "fontSize": { "signal": "config_subtitleSize" },
                        "x": { "signal": "width/2" },
                        "y": { "signal": "height/2" }
                    }
                }
            }
        ]
    };
};
Machinata.Reporting.Node.SunburstNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    //spec["data"][0].values = json.series;
};
Machinata.Reporting.Node.SunburstNode.init = function (instance, config, json, nodeElem) {
    // Create legend
    Machinata.Reporting.Node.VegaNode.Util.createLegendForSeries(instance, config, json, json.series);

    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.SunburstNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);

    Machinata.Reporting.Node.VegaNode.debugOutputDataSet(nodeElem, 'factsLegend');
    Machinata.Reporting.Node.VegaNode.debugOutputDataSet(nodeElem, 'factsResolved');
};
Machinata.Reporting.Node.SunburstNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};








