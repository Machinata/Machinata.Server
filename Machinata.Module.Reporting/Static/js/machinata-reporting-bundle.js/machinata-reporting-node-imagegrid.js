


/// <summary>
/// 
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.GraphicalCompositionNode</inherits>
Machinata.Reporting.Node.ImageGridNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.ImageGridNode.defaults = {};

/// <summary>
/// By default charts are on solid chrome.
/// </summary>
Machinata.Reporting.Node.ImageGridNode.defaults.chrome = "light";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.ImageGridNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.ImageGridNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.ImageGridNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_4x2];

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.ImageGridNode.defaults.supportsToolbar = true;

/// <summary>
/// No legend
/// </summary>
Machinata.Reporting.Node.ImageGridNode.defaults.insertLegend = false;

/// <summary>
/// </summary>
Machinata.Reporting.Node.ImageGridNode.defaults.aspectRatio = null;

/// <summary>
/// Defines the amount (in pixels) of spacing between the grid items.
/// </summary>
Machinata.Reporting.Node.ImageGridNode.defaults.gridSpacing = 8;

/// <summary>
/// If defined, a additional padding is added around the image from the background color fill.
/// This is useful if one is inserting icons for example, but want the icons to fill less of the grid space.
/// </summary>
Machinata.Reporting.Node.ImageGridNode.defaults.imagePadding = null;


/// <summary>
/// </summary>
Machinata.Reporting.Node.ImageGridNode.getVegaSpec = function (instance, config, json, nodeElem) {
    // Call parent
    return Machinata.Reporting.Node["GraphicalCompositionNode"].getVegaSpec(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.ImageGridNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    // Call parent
    Machinata.Reporting.Node["GraphicalCompositionNode"].applyVegaData(instance, config, json, nodeElem, spec);
};

Machinata.Reporting.Node.ImageGridNode.init = function (instance, config, json, nodeElem) {
    // Create graphics
    json._state.graphics = [];
    json.autosize = "stretch";
    // Do layout
    var currentRow = 1;
    var currentColumn = 0;
    var numRows = 0;
    var numColumns = 0;
    for (var i = 0; i < json.images.length; i++) {
        var image = json.images[i];
        currentColumn++;
        if (json.maxColumns != null && currentColumn > json.maxColumns) {
            currentColumn = 1;
            currentRow++;
        }
        image.column = currentColumn;
        image.row = currentRow;
        if (currentColumn > numColumns) numColumns = currentColumn;
        if (currentRow > numRows) numRows = currentRow;
    }
    // Helper function
    function replaceVars(signal, image) {
        var imagePadding = 0;
        if (image.imagePadding != null) imagePadding = image.imagePadding;
        if (json.imagePadding != null) imagePadding = json.imagePadding;
        signal = Machinata.String.replaceAll(signal, "[width]","([width-without-padding] / [numColumns])");
        signal = Machinata.String.replaceAll(signal, "[height]", "([height-without-padding] / [numRows])");
        signal = Machinata.String.replaceAll(signal, "[width-without-padding]", "(width - (([numColumns]-1)*[grid-spacing]))");
        signal = Machinata.String.replaceAll(signal, "[height-without-padding]", "(height - (([numRows]-1)*[grid-spacing]))");
        signal = Machinata.String.replaceAll(signal, "[row]",image.row);
        signal = Machinata.String.replaceAll(signal, "[column]", image.column);
        signal = Machinata.String.replaceAll(signal, "[numRows]", numRows);
        signal = Machinata.String.replaceAll(signal, "[numColumns]", numColumns);
        signal = Machinata.String.replaceAll(signal, "[grid-spacing]", json.gridSpacing);
        signal = Machinata.String.replaceAll(signal, "[image-padding]", imagePadding);
        return signal;
    }
    for (var i = 0; i < json.images.length; i++) {
        var image = json.images[i];
        // Create graphic items
        if (image.background != null) {
            var graphicBG = {
                "type": "rect",
                "x": replaceVars("([width]+[grid-spacing]) * ([column]-1)", image),
                "y": replaceVars("([height]+[grid-spacing]) * ([row]-1)", image),
                "width": replaceVars("[width]", image),
                "height": replaceVars("[height]", image),
                "color": image.background.color,
                "colorShade": image.background.colorShade
            };
            json._state.graphics.push(graphicBG);
        }
        var graphicImage = {
            "type": "image",
            "align": "center",
            "baseline": "middle",
            "x": replaceVars("([width]+[grid-spacing]) * ([column]-1) + ([width]/2)", image),
            "y": replaceVars("([height]+[grid-spacing]) * ([row]-1) + ([height]/2)", image),
            "width": replaceVars("[width]-[image-padding]*2", image),
            "height": replaceVars("[height]-[image-padding]*2", image),
            "image": image.image
        };
        json._state.graphics.push(graphicImage);
    }
    // Call parent
    Machinata.Reporting.Node["GraphicalCompositionNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.ImageGridNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["GraphicalCompositionNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.ImageGridNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["GraphicalCompositionNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};







