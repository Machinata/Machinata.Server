

/// <summary>
/// Handler namespace.
/// The handlers let you configure and bind a custom user interface
/// for different reporting functionality. Typically, these handlers
/// handle things happening outside of the report.
/// For example: a fullscreen button located at the top right of the page.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Handler = {};  

/// <summary>
/// A handler called when a specific profile is loaded.
/// This allows you to prepare a page specifically for the profile, if needed.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.setupForProfile = function(profile) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.setupForProfile = null;

/// <summary>
/// A handler for adding a page tool that has to do with the report being displayed.
/// For example, such tools may include report attachments, date change, print composer, etc....
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.addPageTool = function(instance, title, icon, action, tooltip, addSeparator) { 
///     // instance >> reporting instance
///     // title >> string, tool title
///     // icon >> string, icon id
///     // action >> click hander, called when tool is pressed - you must bind this handler to a press event of the tool
///     // tooltip >> string, tool tooltip
///     // addSeparator >> bool, true if a separator should be added
/// }
/// ```
/// </example>
Machinata.Reporting.Handler.addPageTool = null;

/// <summary>
/// A handler called when a user would like to change the reporting date.
/// You can use the default supplied version: 
/// ```Machinata.Reporting.Tools.showReportDates```
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.changeReportingDate = function(instance, catalogJSON, currentMandateJSON, currentReportJSON) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.changeReportingDate = null;

/// <summary>
/// A handler that is called whenever the user would like to change chapter. This allows you to customize the behaviour of a chapter
/// a chapter change, depending on the integration.
///
/// You can use the default supplied version: 
///  - ```Machinata.Reporting.Tools.changeChapterViaURLUpdate```
///  - ```Machinata.Reporting.Tools.changeChapterViaInlineUpdate```
///
/// Note: If you use a custom integration, you must make sure that absolute direct URL links are avialable on all nodes. These links are used
/// for various features, such as a Ask-A-Question and Sharing.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.changeChapter = function(instance, chapterId) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.changeChapter = null; 


/// <summary>
/// A handler that is called whenever the user would like to change chapter, and directly focus on a node. This allows you to customize the behaviour of a chapter
/// a chapter change, depending on the integration.
///
/// For custom integrations, you can use a combination of default supplied tools: 
///  - ```Machinata.Reporting.Tools.changeChapterViaInlineUpdate```
///  - ```Machinata.Reporting.Tools.drilldownOnNode```
///
/// Note: If you use a custom integration, you must make sure that absolute direct URL links are avialable on all nodes. These links are used
/// for various features, such as a Ask-A-Question and Sharing.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.changeChapterAndFocusOnNode = function(instance, chapterId, nodeId) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.changeChapterAndFocusOnNode = null; 

/// <summary>
/// A handler for when the user wishes to ask a question regarding a specifc node or section.
///
/// A summary of useful properties is supplied in the handler ```infosJSON``` parameter (see ```Machinata.Reporting.Node.compileNodeInfos```), 
/// and the entire node is passed via the ```nodeJSON``` parameter.
/// 
/// You can use the default supplied version: 
///  - ```Machinata.Reporting.Tools.askAQuestionViaEmail```
///
/// Note: If you use a custom integration, you must make sure that absolute URL links are avialable on all nodes. These links are used
/// for various features, such as a Ask-A-Question and Sharing.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.askAQuestion = function(instance,infosJSON,nodeJSON) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.askAQuestion = null;

/// <summary>
/// A handler for when the user wishes to add a specific node to their dashboard.
///
/// A summary of useful properties is supplied in the handler ```infosJSON``` parameter (see ```Machinata.Reporting.Node.compileNodeInfos```), 
/// and the entire node is passed via the ```nodeJSON``` parameter.
/// 
/// In the ```infosJSON``` of special interest are the properties ```nodeSupportedDashboardSizes``` (see ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```),
/// ```nodeCurrentPixelSize``` (see ```Machinata.Reporting.Node.compileNodeInfos```), and
/// ```nodeCurrentBlockSize``` (see ```Machinata.Reporting.Node.compileNodeInfos```).
/// 
/// Here it is best practice to add the node to the dashboard as one of the supported dashboard sizes that best matches the current block size.
/// This ensures that the user has the closest representation of the node on the dashboard as it was on the report when the user originally requested to add
/// it to the dashboard.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.addToDashboard = function(instance,infosJSON,nodeJSON) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.addToDashboard = null;

/// <summary>
/// A handler for when the user wishes to add a specific node to a presentation.
/// Typically, this will be the entire screen for the current node.
///
/// A summary of useful properties is supplied in the handler ```infosJSON``` parameter (see ```Machinata.Reporting.Node.compileNodeInfos```), 
/// and the entire node is passed via the ```nodeJSON``` parameter.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.addToPresentation = function(instance,infosJSON,nodeJSON) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.addToPresentation = null;

/// <summary>
/// A handler for sharing a specific node and it's URL.
///
/// A summary of useful properties is supplied in the handler ```infosJSON``` parameter (see ```Machinata.Reporting.Node.compileNodeInfos```), 
/// and the entire node is passed via the ```nodeJSON``` parameter.
/// 
/// You can use the default supplied version: 
///  - ```Machinata.Reporting.Tools.shareNodeByCopyingLink```
///
/// Note: If you use a custom integration, you must make sure that absolute URL links are avialable on all nodes. These links are used
/// for various features, such as a Ask-A-Question and Sharing.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.shareNode = function(instance,infosJSON,nodeJSON) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.shareNode = null;


/// <summary>
/// A handler for printing a specific report.
/// Typically, such a handler should display a list of common
/// printing options, such as:
///  - Print entire report
///  - Print current chapter
///  - View print composer
///
/// The options displayed depend on which of the following corresponding handlers are implemented:
///  - Machinata.Reporting.Handler.openPrintPageForChapter
///  - Machinata.Reporting.Handler.openPrintPageForReport
///  - Machinata.Reporting.Handler.openPrintComposer
///
/// A summary of useful properties is supplied in the handler ```infosJSON``` parameter (see ```Machinata.Reporting.Node.compileNodeInfos```), 
/// and the entire node is passed via the ```nodeJSON``` parameter.
/// 
/// You can use the default supplied version which automatically builds the UI and correct user options:
///  - ```Machinata.Reporting.Tools.printNode```
///
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.printNode = function(instance,infosJSON,nodeJSON) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.printReport = Machinata.Reporting.Tools.printReport;

/// <summary>
/// A handler for printing a specific node. 
/// Typically, such a handler should display a list of common
/// printing options, such as:
///  - Print entire report
///  - Print current chapter
///  - Print this section
///  - Add to print composer
///  - View print composer
///
/// The options displayed depend on which of the following corresponding handlers are implemented:
///  - Machinata.Reporting.Handler.openPrintPageForSection
///  - Machinata.Reporting.Handler.openPrintPageForChapter
///  - Machinata.Reporting.Handler.openPrintPageForReport
///  - Machinata.Reporting.Handler.openPrintComposer
/// 
/// You can use the default supplied version which automatically builds the UI and correct user options: 
///  - ```Machinata.Reporting.Tools.printReport```
///
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.printNode = function(instance, infosJSON, nodeJSON) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.printNode = Machinata.Reporting.Tools.printNode;

/// <summary>
/// A handler for tracking node and report events. 
/// 
/// The following categoryes are supported:
///  - ```Machinata.Reporting.TRACKING_CATEGORY_NODE```
///  - ```Machinata.Reporting.TRACKING_CATEGORY_REPORT```
/// 
/// The following events are currently supported:
///  - ```Machinata.Reporting.TRACKING_ACTION_NODE_EXPORT_SUCCESS```: this happens when a user successfully exports a node
///  - ```Machinata.Reporting.TRACKING_ACTION_NODE_SHARE```: this happens when a user shares a node (as soon as the button is pressed)
///  - ```Machinata.Reporting.TRACKING_ACTION_NODE_ASKAQUESTION```: this happens when a user presses the ask-a-question button
///  - ```Machinata.Reporting.TRACKING_ACTION_NODE_ADDTODASHBOARD```: this happens when a user adds a node to a dashboard (as soon as the button is pressed)
///  - ```Machinata.Reporting.TRACKING_ACTION_NODE_ADDTOPRESENTATION```: this happens when a user adds a node to a presentation (as soon as the button is pressed)
///  - ```Machinata.Reporting.TRACKING_ACTION_NODE_SHOWINFO```: this happens when a user pressed the info button for a node
///  - ```Machinata.Reporting.TRACKING_ACTION_NODE_COPYTOCLIPBOARD```: this happens when a user copies the node to clipboard (as soon as the button is pressed)
///
/// This handler is useful if certian events wish to be tracked externally for reporting/analysis uses.
/// Note that the reporting framework does not evaluate any browser privacy settings or agreements - 
/// this must be handled by the frontend before attaching this handler.
///
/// A tracking event will always specify a category and an action, but may also provide a value optionally.
/// In addition, the node JSON and the node infos JSON (see ```Machinata.Reporting.Node.compileNodeInfos```) is provided as well.
///
/// Some tracking actions may also provide a ```additionalInfos``` parameter, which varies depending on the action.
///
/// This handler is compatible with standardized event tracking frameworks such as Google Analytics with
/// the concept of event category, action, value.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.trackEvent = function(instance, infosJSON, nodeJSON, category, action, val, additionalInfos) { ... }
/// ```
/// When a user exports a node, the handler will be called with the following parameters:
///  - ```category```: ```Machinata.Reporting.TRACKING_CATEGORY_NODE``` (```node```)
///  - ```action```: ```Machinata.Reporting.TRACKING_ACTION_NODE_EXPORT_SUCCESS``` (```node_export_success```)
///  - ```val```: the export format
///  - ```additionalInfos```: a JSON object with the export file format and the export filename (for example  {format: 'csv', filename: 'LineColumnData.csv'})
/// </example>
Machinata.Reporting.Handler.trackEvent = null;

/// <summary>
/// A handler for printing a specific section (screen) via the nodeInfos screen number.
/// 
/// Typically such a handler will display a report on blank page with the profile set to 'webprint' (and appropriate chapter/screen settings).
/// The config setting ```Machinata.Reporting.Config.automaticallySetLayoutMinMaxDimensions``` 
/// can be used to ensure a fine - tuned dimension that helps chrome / firefox / safari / edge figure out the proper scaling in its print engine.
/// The config setting ```Machinata.Reporting.Config.automaticallyOpenBrowserPrintDialog```
/// can be used to automatically open the browsers print dialog after the report is rendered.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.openPrintPageForSection = function(instance,nodeInfos) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.openPrintPageForSection = null;

/// <summary>
/// A handler for printing a specific chapter via the chapterId (nodeId of the chapter).
/// 
/// Typically such a handler will display a report on blank page with the profile set to 'webprint' (and appropriate chapter/screen settings).
/// The config setting ```Machinata.Reporting.Config.automaticallySetLayoutMinMaxDimensions``` 
/// can be used to ensure a fine - tuned dimension that helps chrome / firefox / safari / edge figure out the proper scaling in its print engine.
/// The config setting ```Machinata.Reporting.Config.automaticallyOpenBrowserPrintDialog```
/// can be used to automatically open the browsers print dialog after the report is rendered.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.openPrintPageForChapter = function(instance,chapterId) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.openPrintPageForChapter = null;

/// <summary>
/// A handler for printing an entire report via a reportId. The reportId will always match the currently loaded report of the instance.
/// 
/// Typically such a handler will display a report on blank page with the profile set to 'webprint' (and appropriate chapter/screen settings).
/// The config setting ```Machinata.Reporting.Config.automaticallySetLayoutMinMaxDimensions``` 
/// can be used to ensure a fine - tuned dimension that helps chrome / firefox / safari / edge figure out the proper scaling in its print engine.
/// The config setting ```Machinata.Reporting.Config.automaticallyOpenBrowserPrintDialog```
/// can be used to automatically open the browsers print dialog after the report is rendered.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.openPrintPageForReport = function(instance,reportId) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.openPrintPageForReport = null;


/// <summary>
/// A handler for displaying the print composer and it's data to the user.
/// 
/// See ```Machinata.Reporting.PrintComposer``` for more details.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.openPrintPageForReport = function(instance,reportId) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.openPrintComposer = null;

/// <summary>
/// A handler that can change the default icon ids for icon keys. This allows one
/// to use a custom icon set (or icon library) with differing icon ids than the default
/// shipped version.
/// You must support the mapping as defined by dictionary ```Machinata.Reporting.Config.iconNameToBundleIdMapping```.
/// By default this handler is setup to use the current configuration ```iconNameToBundleIdMapping```.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.getIconByKey = function(instance,key) {
///    // return string
/// }
/// ```
/// </example>
Machinata.Reporting.Handler.getIconByKey = function (instance,key) {
    return instance.config.iconNameToBundleIdMapping[key];
};

/// <summary>
/// </summary>
Machinata.Reporting.Handler.addNavigationItem = null;

/// <summary>
/// </summary>
Machinata.Reporting.Handler.addNavigationSibling = null;

/// <summary>
/// A handler to allow customization of chapter navigation.
/// This is usefull if you want to provide a custom way for users to navigation chapters.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.addChapterNavigation = function(instance, infosJSON, chapterNodeJSON) {
///    // add your own UI using the infos and chapter JSON
/// }
/// ```
/// </example>
Machinata.Reporting.Handler.addChapterNavigation = null; 

/// <summary>
/// A handler to allow customization of table UI/UX. Typically the default supplied handler should be used.
/// You can use the default supplied version: 
///  - ```Machinata.Reporting.Tools.bindTableUI```
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.bindTableUI = function(instance,elements) {
///    // elements is a jquery selector of tables
/// }
/// ```
/// </example>
Machinata.Reporting.Handler.bindTableUI = null;

/// <summary>
/// A handler to allow customization of tooltips. Typically the default supplied handler should be used.
/// You can use the default supplied version: 
///  - ```Machinata.Reporting.Tools.bindTooltipsUI```
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.bindTooltipsUI = function(instance,elements) {
///    // elements is a jquery selector for elements (and their children) for which tooltips should be registered
/// }
/// ```
/// </example>
Machinata.Reporting.Handler.bindTooltipsUI = null;

/// <summary>
/// A handler called when a node is cleaned up.
/// This is called just after the node implementation cleanup routine runs, and just before
/// the actual DOM content is removed.
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.cleanupNode = function(instance,nodeJSON,nodeElem) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.cleanupNode = null;

/// <summary>
/// A handler called when a PortalNode is ready. This means that the node as a element in the
/// DOM is and is available for manipulation/appending.
/// 
/// The source reporting node is available as the ```nodeJSON``` parameter, and the element in the DOM
/// is provided by ```portalElem``` as a JQuery selector. Typically the exact type of content or node requested as a portal
/// can be derived from ```nodeJSON.portalElementId```, however any additional properties or parameters of the node can
/// be used, as long as they don't conflict with standard reporting node parameters...
///
/// To get a DOM element, juse use ```portalElem[0]```: returns the first (and only) DOM element
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.portalNodeReady = function(instance,nodeJSON,nodeElem,portalElem) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.portalNodeReady = null;

/// <summary>
/// A handler called when a PortalNode has been resized.
/// Reporting nodes are always first resized, then redrawn - meaning that ```portalNodeResize``` will be called just before ```portalNodeDraw```.
/// This allows for small (yet meaninful) optimizations that can differ between a DOM layout resize and a redraw of content.
/// 
/// The source reporting node is available as the ```nodeJSON``` parameter, and the element in the DOM
/// is provided by ```portalElem``` as a JQuery selector. Typically the exact type of content or node requested as a portal
/// can be derived from ```nodeJSON.portalElementId```, however any additional properties or parameters of the node can
/// be used, as long as they don't conflict with standard reporting node parameters...
///
/// To get a DOM element, juse use ```portalElem[0]```: returns the first (and only) DOM element
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.portalNodeResize = function(instance,nodeJSON,nodeElem,portalElem) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.portalNodeResize = null;

/// <summary>
/// A handler called when a PortalNode has been requested for drawing. Can be usefull
/// if a draw of the portal contents should be defferred or only done when needed.
/// Reporting nodes are always first resized, then redrawn - meaning that ```portalNodeResize``` will be called just before ```portalNodeDraw```.
/// This allows for small (yet meaninful) optimizations that can differ between a DOM layout resize and a redraw of content.
///
/// The source reporting node is available as the ```nodeJSON``` parameter, and the element in the DOM
/// is provided by ```portalElem``` as a JQuery selector. Typically the exact type of content or node requested as a portal
/// can be derived from ```nodeJSON.portalElementId```, however any additional properties or parameters of the node can
/// be used, as long as they don't conflict with standard reporting node parameters...
///
/// To get a DOM element, juse use ```portalElem[0]```: returns the first (and only) DOM element
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.portalNodeDraw = function(instance,nodeJSON,nodeElem,portalElem) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.portalNodeDraw = null;

/// <summary>
/// A handler called when a PortalNode will be cleaned up (and later removed). 
/// This allows an integration to perform their own cleanup.
///
/// Note that the cleanup operation happens without any guarantee of order or execution wait, and by the time
/// the cleanup signal reaches the handler the DOM element may already be flushed. Thus the task
/// of the handler is to cleanup any resources not explicitly bound by the DOM element.
/// 
/// The source reporting node is available as the ```nodeJSON``` parameter, and the element in the DOM
/// is provided by ```portalElem``` as a JQuery selector. Typically the exact type of content or node requested as a portal
/// can be derived from ```nodeJSON.portalElementId```, however any additional properties or parameters of the node can
/// be used, as long as they don't conflict with standard reporting node parameters...
///
/// To get a DOM element, juse use ```portalElem[0]```: returns the first (and only) DOM element
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.portalNodeCleanup = function(instance,nodeJSON,nodeElem,portalElem) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.portalNodeCleanup = null;

/// <summary>
/// A handler called when the report loading state changes. This can be used for when
/// the report is loading, or when individual user operations take place.
/// The parameter ```isLoading``` signals if the loading state should be shown (true) or hidden (false).
/// Note: for user actions originating from the integrator (ie not from Machinata itself), the loading UI
/// must be first updated on the UI thread by the integrator. Machinata will not pause the its working
/// thread to allow for loading state updates, unless the user action originated from within Machinata.
/// For example, if the integrator has a button to change the chapter, it is the integrators responsibility
/// to update the loading state UI (and ensure that the browser has time to update) before
/// calling Machinata routines. If a user action which originates from Machinata causes a loading change, then
/// Machinata will ensure that the appropriate thread pause is invoked to allow for any UI updates.
/// See also ```Machinata.Reporting.Config.loadingUITimeoutDelay```
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Handler.showLoading = function(instance,isLoading) { ... }
/// ```
/// </example>
Machinata.Reporting.Handler.showLoading = null;
