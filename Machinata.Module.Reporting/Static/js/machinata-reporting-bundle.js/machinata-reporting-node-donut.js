


/// <summary>
/// </summary>
/// <type>class</type>
/// <inherits>Machinata.Reporting.Node.VegaNode</inherits>
Machinata.Reporting.Node.DonutNode = {};

/// <summary>
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults = {};

/// <summary>
/// Donuts and pies have a ```solid``` chrome.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.chrome = "solid";

/// <summary>
/// This node supports headless rendering...
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.supportsHeadlessRendering = true;

/// <summary>
/// This node supports being added to dashboards...
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.supportsDashboard = true;

/// <summary>
/// Defines which layout sizes are supported on the dashboard.
/// See ```Machinata.Reporting.Node.Defaults.supportedDashboardSizes```
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.supportedDashboardSizes = [Machinata.Reporting.Layouts.BLOCK_2x2, Machinata.Reporting.Layouts.BLOCK_2x1, Machinata.Reporting.Layouts.BLOCK_1x1];

/// <summary>
/// Yes, we support the toolbar.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.supportsToolbar = true;

/// <summary>
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.insertLegend = true;

/// <summary>
/// Defines the inner radius in decimal percent.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.innerRadius = 0.5;

/// <summary>
/// Defines the outer radius in decimal percent.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.outerRadius = 1.0;

/// <summary>
/// If ```true```, displays labels inside the slices with a slide angle larger than ```sliceLablesMinimumDegrees```.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.sliceLabels = true;

/// <summary>
/// If ```true```, includes the slice value in the label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.sliceLabelsShowValue = true;

/// <summary>
/// If ```true```, includes the slice category in the label.
/// If this feature is used, care must be taken to ensure that the category size/length matches
/// the area given by the pie/donut slices.
/// By default ```false```.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.sliceLabelsShowCategory = false;

/// <summary>
/// If ```true```, includes the slice value in the legend label.
/// By default ```false```.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.legendLabelsShowValue = false;

/// <summary>
/// If ```true```, includes the slice category in the legend label.
/// By default ```true```.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.legendLabelsShowCategory = true;

/// <summary>
/// If ```sliceLabels``` is enabled, defines the minimum slice angle required for the label to display.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.sliceLabelsMinimumDegrees = 36;

/// <summary>
/// If ```sliceLabels``` is enabled, defines the offset from the outer edge in pixels.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.sliceLabelsOffsetPixels = 4;

/// <summary>
/// If ```sliceLabels``` is enabled, defines the font size in pixels for the labels.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.sliceLabelsSize = Machinata.Reporting.Config.chartTextSize;

/// <summary>
/// If ```sliceLabels``` is enabled, defines the maximum width a label can have (in pixels).
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.sliceLabelsLengthLimit = 160;

/// <summary>
/// If ```sliceLabels``` is enabled, defines whether slice labels should be placed on the inside or outside of the slices.
/// 
/// The following options are supported:
///  - ```inside-corner```: labels are placed on the inside corner of the slices, with the text rotated accordingly.
///  - ```inside-center```: labels are placed on the inside center of the slices.
///  - ```outside-center```: labels are placed on the outside of the slices, centered along the edge of the slice with the text aligned right or left depending on the quadrant. EXPERIMENTAL - DO NOT USE IN PRODUCTION.
/// 
/// By default ```inside-corner```.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.sliceLabelsPosition = "inside-corner";

/// <summary>
/// If set, sorts the facts automatically.
/// Value can either be ```descending``` or ```ascending```.
/// If no value is set, then the order of the data is used.
/// Note: For donuts/pies it is recommending to always use descending fact sort.
/// Note: Sorting the facts does not affect the legend order.
/// By default ```null```.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.sortFacts = null;

/// <summary>
/// If enabled, if the fact is between -1% and 1% (very small) or greater than 99.5% (very large, but not 100%),
/// or if equal to zero or one (0% or 100%),
/// we use the facts full formatting (```fact.format```), otherwise we use a standard full number percent format
/// that is simplified (ie 43% instead of 43.04%)...
/// For this setting to work, the fact format specified (```fact.format```) must be of type percent (ie '.2%').
/// By default ```true```.
/// See Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatEdgeNumbers for more details.
/// </summary>
Machinata.Reporting.Node.DonutNode.defaults.automaticallyFormatEdgeNumbers = true;


Machinata.Reporting.Node.DonutNode.getVegaSpec = function (instance, config, json, nodeElem) {



    // Create the sort (collection) transform. If no sort is given, we just provide an empty transform
    var collectTransform = {"type": "collect"};
    if (json.sortFacts != false && json.sortFacts != null) {
        collectTransform["sort"] = {
            "field": "fact.val",
            "order": [json.sortFacts]
        };
    }
    // Create slice label template
    var sliceLabelTemplate = "''";
    if (json.sliceLabels == true && json.sliceLabelsShowCategory == true) sliceLabelTemplate += " + datum.fact.category.resolved";
    if (json.sliceLabels == true && json.sliceLabelsShowCategory == true && json.sliceLabelsShowValue == true) sliceLabelTemplate += " + ': '";
    if (json.sliceLabels == true && json.sliceLabelsShowValue == true) sliceLabelTemplate += " + datum.valResolved";

    // Status
    var status = {
        title: null,
        titleNumChars: 0,
        tooltip: null,
    };
    if (json.status != null) {
        if (json.status.title != null) {
            status.titleNumChars = json.status.title.resolved.length;
            status.title = Machinata.Reporting.Tools.lineBreakString(Machinata.Reporting.Text.resolve(instance, json.status.title), { returnArray: true });
        }
        if (json.status.tooltip != null) status.tooltip = Machinata.Reporting.Text.resolve(instance, json.status.tooltip);
    }
    return {
        "data": [
          {
              "name": "series",
              "values": null
          },
          {
              "name": "seriesResolved",
              "source": "series",
              "transform": [
                  {
                      "type": "flatten",
                      "fields": ["facts"],
                      "as": ["fact"]
                  }
              ]
          },
          {
              "name": "facts",
              "source": "seriesResolved",
              "transform": [
                  { "type": "formula", "initonly": true, "as": "key", "expr": "datum.fact.category.resolved" },
                  { "type": "formula", "initonly": true, "as": "valResolved", "expr": "datum.fact.resolved" },
                  { "type": "formula", "initonly": true, "as": "tooltipResolved", "expr": "datum.fact.category.resolved + ': ' + datum.valResolved" },
                  { "type": "formula", "initonly": true, "as": "sliceLabelResolved", "expr": sliceLabelTemplate },
                  collectTransform,
                {
                    "type": "pie",
                    "field": "fact.val",
                    "startAngle": 0,
                    "endAngle": 6.29,
                    "sort": false
                  },
                  { "type": "formula", "initonly": true, "as": "midAngle", "expr": "(datum.startAngle + datum.endAngle) / 2" },
                  { "type": "formula", "initonly": true, "as": "hideSliceLabel", "expr": "((datum['endAngle'] - datum['startAngle'])*(180/PI) < " + json.sliceLabelsMinimumDegrees + ")" },
              ]
            },
            {
                "name": "factsResolved",
                "source": "facts",
                "transform": [
                    {
                        "type": "formula",
                        "initonly": true,
                        "as": "colorResolved",
                        "expr": "datum.fact.colorShade != null ? scale('theme-bg',datum.fact.colorShade) : scale('colorAuto',datum.fact.category.resolved)"
                    },
                    {
                        "type": "formula",
                        "initonly": true,
                        "as": "textColorResolved",
                        "expr": "datum.fact.colorShade != null ? scale('theme-fg',datum.fact.colorShade) : scale('textColorAuto',datum.fact.category.resolved)"
                    },
                ]
            },
            // Dynamically set the data source for inside labels depending on settings
            json.sliceLabelsPosition == "inside-corner" ? {
                "name": "factsForInsideCornerLabels",
                "source": "factsResolved"
            } : {
                    "name": "factsForInsideCornerLabels",
                "values": [] // no data
            },
            json.sliceLabelsPosition == "outside-center" ? {
                "name": "factsForOutsideCenterLabels",
                "source": "factsResolved"
            } : {
                "name": "factsForOutsideCenterLabels",
                "values": [] // no data
            },
            json.sliceLabelsPosition == "inside-center" ? {
                "name": "factsForInsideCenterLabels",
                "source": "factsResolved"
            } : {
                    "name": "factsForInsideCenterLabels",
                "values": [] // no data
            }
        ],
        "signals": [
            {
                "name": "drawZeroLine",
                "init": "data('factsResolved')[0].colorResolved == data('factsResolved')[length(data('factsResolved'))-1].colorResolved",
            },
            {
                "name": "innerRadius",
                "update": "(min(drawingWidth,drawingHeight) / 2)*" + json.innerRadius,
                
            },
            {
                "name": "outerRadius",
                "update": "(min(drawingWidth,drawingHeight) / 2)*" + json.outerRadius,
            },
            {
                "name": "chartTextSize",
                "update": json.chartTextSize,
            },
        ],
        "scales": [
          {
              "name": "colorAuto",
              "type": "ordinal",
              "domain": { "data": "facts", "field": "key" }, // we cant do this on factsResolved because of cyclic dependency...
              "range": { "scheme": json.theme + "-bg" }
            },
          {
              "name": "textColorAuto",
              "type": "ordinal",
              "domain": { "data": "facts", "field": "key" }, // we cant do this on factsResolved because of cyclic dependency...
              "range": { "scheme": json.theme + "-fg" }
          },
          {
              "name": "legendColor",
              "type": "ordinal",
              //"domain": { "data": "factsResolved", "field": "key" },
              //"range": { "scheme": json.theme + "-bg" }
              "domain": { "data": "factsResolved", "field": "key" },
              "range": { "data": "factsResolved", "field": "colorResolved" }, // BUG: if two keys have same color, the ordinal scale ignores it

          },
        ],

        "marks": [
            {
                "name": "masterGroupForSizing",
                "type": "group",
                "clip": true,
                "encode": {
                    "update": {
                        //"fill": { "signal": "'red'" },
                        "width": { "signal": "drawingWidth" },
                        "x2": { "signal": "drawingX2" },
                        "height": { "signal": "drawingHeight" },
                    }
                },
                "marks": [
                    {
                        "name": "markSlices",
                        "type": "arc",
                        "from": { "data": "factsResolved" },
                        "encode": {
                            "enter": {
                                "fill": { "field": "colorResolved" },
                            },
                            "update": {
                                "x": { "signal": "drawingWidth / 2" },
                                "y": { "signal": "drawingHeight / 2" },
                                "startAngle": { "field": "startAngle" },
                                "endAngle": { "field": "endAngle" },
                                "tooltip": { "field": "tooltipResolved" },
                                "padAngle": { "value": 0 },
                                "innerRadius": { "signal": "innerRadius" },
                                "outerRadius": { "signal": "outerRadius" },
                                "cornerRadius": { "value": 0 }
                            }
                        }
                    },
                    {
                        "name": "markSliceLabelsInsideText",
                        "type": "text",
                        "from": {
                            "data": "factsForInsideCornerLabels"
                        },
                        "encode": {
                            "enter": {
                                "text": {
                                    // Only show the label if it meets the requirements of a min angle
                                    "signal": "if(" + json.sliceLabels + " == false || datum['hideSliceLabel'], null, datum.sliceLabelResolved)"
                                },
                                "fill": { "field": "textColorResolved" },

                                "fontSize": { "value": json.sliceLabelsSize },
                                "limit": { "value": json.sliceLabelsLengthLimit },
                                "align": {
                                    // This depends on whether the label is on the left right side
                                    "signal": "if(datum['endAngle']*(180/PI) < 180, 'right', 'left')"
                                },
                                "baseline": {
                                    // This depends on whether the label is on the left right side
                                    "signal": "if(datum['endAngle']*(180/PI) < 180, 'bottom', 'top')"
                                },
                                "dx": {
                                    // This depends on whether the label is on the left right side
                                    "signal": "if(datum['endAngle']*(180/PI) < 180, -" + json.sliceLabelsOffsetPixels + ", " + json.sliceLabelsOffsetPixels + ")"
                                },
                                "dy": {
                                    // This depends on whether the label is on the left right side
                                    "signal": "if(datum['endAngle']*(180/PI) < 180, -" + json.sliceLabelsOffsetPixels + ", " + json.sliceLabelsOffsetPixels + ")"
                                },
                            },
                            "update": {
                                "tooltip": { "field": "tooltipResolved" },
                                "x": {
                                    "signal": "drawingWidth / 2"
                                },
                                "y": {
                                    "signal": "drawingHeight / 2"
                                },
                                "radius": {
                                    "signal": "(min(drawingWidth,drawingHeight) / 2)*1"
                                },
                                "theta": {
                                    "signal": "datum['endAngle']"
                                },
                                "angle": {
                                    // The angle is the endAngle value, but we need to add 90 degrees depending on whether its on the left/right side
                                    "signal": "if(datum['endAngle']*(180/PI) < 180, (datum['endAngle']*(180/PI))-90, (datum['endAngle']*(180/PI))+90)"
                                }
                            }
                        }
                    },
                    {
                        "name": "markSliceLabelsOutsideText",
                        "type": "text",
                        "from": {
                            "data": "factsForOutsideCenterLabels"
                        },
                        "encode": {
                            "enter": {
                                "text": {
                                    // Only show the label if it meets the requirements of a min angle
                                    "signal": "if(" + json.sliceLabels + " == false || datum['hideSliceLabel'], null, datum.sliceLabelResolved)"
                                },
                                //"fill": { "scale": "sliceTextColor", "field": "key" },
                                "limit": { "value": json.sliceLabelsLengthLimit },
                                "fontSize": { "value": json.sliceLabelsSize },
                                "align": {
                                    // This depends on whether the label is on the left right side
                                    "signal": "if(datum['midAngle']*(180/PI) > 180, 'right', 'left')"
                                },
                                "baseline": {
                                    // This depends on whether the label is on the top or bottom side
                                    "signal": "if(datum['midAngle']*(180/PI) < 90 || datum['midAngle']*(180/PI) > 360-90, 'bottom', 'top')"
                                },
                            },
                            "update": {
                                "tooltip": { "field": "tooltipResolved" },
                                "x": {
                                    "signal": "drawingWidth / 2"
                                },
                                "y": {
                                    "signal": "drawingHeight / 2"
                                },
                                "radius": {
                                    "signal": "(outerRadius)*1 + " + json.sliceLabelsOffsetPixels * 2
                                },
                                "theta": {
                                    "signal": "datum['midAngle']"
                                },
                                "angle": {
                                    // The angle is the endAngle value, but we need to add 90 degrees depending on whether its on the left/right side
                                    "signal": "0"
                                    //"signal": "if(datum['endAngle']*(180/PI) < 180, (datum['endAngle']*(180/PI))-90, (datum['endAngle']*(180/PI))+90)"
                                }
                            }
                        }
                    },
                    {
                        "name": "markSliceLabelsInsideCenterText",
                        "type": "text",
                        "from": {
                            "data": "factsForInsideCenterLabels"
                        },
                        "encode": {
                            "enter": {
                                "text": {
                                    // Only show the label if it meets the requirements of a min angle
                                    "signal": "if(" + json.sliceLabels + " == false || datum['hideSliceLabel'], null, datum.sliceLabelResolved)"
                                },
                                "fill": { "field": "textColorResolved" },
                                "limit": { "value": json.sliceLabelsLengthLimit },
                                "fontSize": { "value": json.sliceLabelsSize },
                                "align": {"signal": "'center'"},
                                "baseline": {"signal": "'middle'"},
                            },
                            "update": {
                                "tooltip": { "field": "tooltipResolved" },
                                "x": {
                                    "signal": "drawingWidth / 2"
                                },
                                "y": {
                                    "signal": "drawingHeight / 2"
                                },
                                "radius": {
                                    "signal": "(outerRadius+innerRadius)/2"
                                },
                                "theta": {
                                    "signal": "datum['midAngle']"
                                },
                                "angle": {
                                    // The angle is the endAngle value, but we need to add 90 degrees depending on whether its on the left/right side
                                    "signal": "0"
                                    //"signal": "if(datum['endAngle']*(180/PI) < 180, (datum['endAngle']*(180/PI))-90, (datum['endAngle']*(180/PI))+90)"
                                }
                            }
                        }
                    },
                    // Starting line
                    {
                        "type": "rect",
                        "encode": {
                            "enter": {
                                "fill": {
                                    "value": (json.chrome == "dark" ? config.darkColor : (json.chrome == "solid" ? config.solidColor : config.whiteColor))
                                },
                                "opacity": {
                                    "signal": "drawZeroLine == true ? 1 : 0"
                                }
                            },
                            "update": {

                                "x": {
                                    "signal": "drawingWidth / 2"
                                },
                                "y": {
                                    "signal": "0"
                                },
                                "width": {
                                    "signal": config.domainLineSize
                                },
                                "height": {
                                    "signal": "(outerRadius - innerRadius) + 0" //  + config.domainLineSize
                                },

                            }
                        }
                    },
                    Machinata.Reporting.Node.VegaNode.Util.createStatusLabelSpec(instance, config, json, json.status, {
                        maxWidth: "innerRadius*2",
                        maxHeight: "innerRadius*2",
                        safetyMargin: config.padding,
                    })
                ]
            },
          
        ]
    }; 
};

Machinata.Reporting.Node.DonutNode.applyVegaData = function (instance, config, json, nodeElem, spec) {
    spec["data"][0].values = json.series;
};


Machinata.Reporting.Node.DonutNode.init = function (instance, config, json, nodeElem) {
    // Create legend
    Machinata.Reporting.Node.VegaNode.Util.createLegendForSeriesFacts(instance, config, json, json.series);

    // Normalize facts to rounded percentages, but only if fact is very edge of boundaries...
    if (json.automaticallyFormatEdgeNumbers == true) {
        for (var si = 0; si < json.series.length; si++) {
            Machinata.Reporting.Node.VegaNode.Util.automaticallyFormatEdgeNumbers(json.series[si].facts);
        }
    }

    // Call parent
    Machinata.Reporting.Node["VegaNode"].init(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.DonutNode.draw = function (instance, config, json, nodeElem) {
    // Call parent
    Machinata.Reporting.Node["VegaNode"].draw(instance, config, json, nodeElem);
};
Machinata.Reporting.Node.DonutNode.exportFormat = function (instance, config, json, nodeElem, format, filename) {
    // Call parent
    return Machinata.Reporting.Node["VegaNode"].exportFormat(instance, config, json, nodeElem, format, filename);
};

/// <summary>
/// 
/// </summary>
Machinata.Reporting.Node.DonutNode.exportExcelFormat = function (instance, config, json, nodeElem, format, filename) {
  
    var workbook = Machinata.Reporting.Export.createExcelWorkbook(instance, config, json);
    workbook = Machinata.Reporting.Export.createExcelDataForCategoryChart(instance, config, json, workbook, format);
    Machinata.Reporting.Export.saveExcelWorkbook(instance, config, json, workbook, filename);
    return true;
};








