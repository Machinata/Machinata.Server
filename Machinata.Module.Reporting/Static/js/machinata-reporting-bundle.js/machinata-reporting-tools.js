/// <summary>
/// Collection of tools that supplement a report.
/// </summary>
/// <type>namespace</type>
Machinata.Reporting.Tools = {};

/// <summary>
/// Converts the given value to pixels using ```Machinata.Reporting.Config.layoutingDPI```. 
/// If the input value is a number, then pixels is assumed and the input number is returned.
/// If the input is a string and ends with "mm", then the number is converted to pixels and returned.
/// If the input is a string and ends with "in", then the number is converted to pixels and returned.
/// </summary>
Machinata.Reporting.Tools.convertStringOrNumberToPixels = function (config, input) {
    // If number, assume pixels
    if (typeof input == 'number') return input;
    // mm?
    if (typeof input == 'string' && Machinata.String.endsWith(input, "mm")) return Machinata.Reporting.Tools.convertMillimetersToPixels(config,parseFloat(input.replace("mm", "")));
    // in?
    if (typeof input == 'string' && Machinata.String.endsWith(input, "in")) return Machinata.Reporting.Tools.convertInchesToPixels(config, parseFloat(input.replace("in", "")));
    // Fallback
    return input;
};

/// <summary>
/// Converts the given value to pixels using ```Machinata.Reporting.Config.layoutingDPI```.
/// </summary>
Machinata.Reporting.Tools.convertMillimetersToPixels = function (config,millimeters) {
    var inches = millimeters * (1.0 / 25.4);
    return Machinata.Reporting.Tools.convertInchesToPixels(config,inches);
};

/// <summary>
/// Converts the given value to pixels using ```Machinata.Reporting.Config.layoutingDPI```.
/// </summary>
Machinata.Reporting.Tools.convertInchesToPixels = function (config,inches) {
    if (config == null) throw ("Machinata.Reporting.Tools.convertInchesToPixels: config is null");
    if (config.layoutingDPI == null) throw ("Machinata.Reporting.Tools.convertInchesToPixels: config.layoutingDPI is null");
    var dpi = config.layoutingDPI;
    var px = inches * dpi;
    return px;
};

/// <summary>
/// Toggles fullscreen for a node by opening a fullscreen report for the nodes screen number.
/// </summary>
Machinata.Reporting.Tools.toggleFullscreenForNode = function (instance,nodeElem) {
    // Find screen number
    var screenNumber = nodeElem.attr("data-screen");
    if ($(document).fullScreen() == false) {
        if (screenNumber == "") screenNumber = null;
        if (screenNumber != null) screenNumber = parseInt(screenNumber);
        Machinata.Reporting.Tools.openFullscreenReport(instance,screenNumber);
    } else {
        Machinata.Reporting.Tools.exitFullscreenLayout(instance);
    }
};

/// <summary>
/// Opens a report in fullscreen using a fullscreen HTML node and an iframe.
/// A toolbar is automatically created for the report.
/// </summary>
Machinata.Reporting.Tools.openFullscreenReport = function (instance,screenNumber, title, subtitle) {
    //Machinata.Reporting.Tools.openFullscreenReportViaIframe(instance,screenNumber, title, subtitle);
    Machinata.Reporting.Tools.openFullscreenReportViaInlineElement(instance, screenNumber, title, subtitle);
};

/// <summary>
/// Opens a report in fullscreen using a fullscreen HTML node and an iframe.
/// A toolbar is automatically created for the report.
/// </summary>
Machinata.Reporting.Tools.openFullscreenReportViaInlineElement = function (instance, screenNumber, title, subtitle) {


    // Fallback title to report tile
    if (title == null) title = instance.config.reportTitle;

    // Find screen number
    if (screenNumber == null) screenNumber = 0;

    // Create fullscreen UI
    var fullscreenContainer = $("<div class='machinata-reporting-fullscreen-container machinata-reporting-styling'></div>");
    instance.elems.fullscreen = fullscreenContainer;

    // Read progress
    var reportContainer = $("<div class='machinata-reporting-report'></div>");
    var reportContainerId = Machinata.UI.autoUIDElem(reportContainer);
    fullscreenContainer.append(reportContainer);

    // Read progress
    var fullscreenReadProgress = $("<div class='machinata-reporting-readprogress'><div class='progress'></div></div>");
    fullscreenContainer.append(fullscreenReadProgress);

    // Toolbar
    var fullscreenToolbar = Machinata.Reporting.buildToolbar(instance, title, "");
    fullscreenToolbar.addClass("machinata-reporting-fullscreen-toolbar");
    Machinata.Reporting.addToolToToolbar(instance, fullscreenContainer, fullscreenToolbar, Machinata.Reporting.Text.translate(instance, "reporting.exit-fullscreen"), "close", function () {
        Machinata.Reporting.Tools.exitFullscreenLayout(instance);
    });
    fullscreenToolbar.addClass("machinata-reporting-fullscreen-toolbar");
    fullscreenToolbar.find(".title,.subtitle").css("user-select","none");
    fullscreenToolbar.find(".subtitle").addClass("machinata-reporting-attribute-currentscreen");
    fullscreenContainer.append(fullscreenToolbar);

    // Controls
    {
        var controlElem = $('<div class="machinata-reporting-screen-controls prev"></div>');
        controlElem.append(Machinata.Reporting.buildIcon(instance, "chevron-left"));
        controlElem.appendTo(fullscreenContainer);
    }
    {
        var controlElem = $('<div class="machinata-reporting-screen-controls next"></div>');
        controlElem.append(Machinata.Reporting.buildIcon(instance, "chevron-right"));
        controlElem.appendTo(fullscreenContainer);
    }

    // Register in body
    $("body").append(fullscreenContainer);

    // Init a fullscreen version of the report
    // This is a new clone from the initConfig of the calling report
    var fullScreenConfig = Machinata.Util.extend({}, instance.initConfig, {
        profile: "fullscreen",
        screens: [screenNumber],
        chapters: "*",
        contentSelector: "#" + reportContainerId
    });
    var fullscreenInstance = Machinata.Reporting.init(fullScreenConfig, function (instance, config) {
        // Success...
        // Initial UI update
        Machinata.Reporting.Tools._updateFullscreenUI(instance);
    });
    fullscreenContainer.data("report-instance", fullscreenInstance);

    // Bind some tools
    fullscreenContainer.find(".machinata-reporting-screen-controls.next").click(function () {
        Machinata.Reporting.Tools.nextScreen(fullscreenInstance);
    });
    fullscreenContainer.find(".machinata-reporting-screen-controls.prev").click(function () {
        Machinata.Reporting.Tools.prevScreen(fullscreenInstance);
    });

    // Bind some events
    $(document).bind("fullscreenchange", function () {
        if ($(document).fullScreen() == false) {
            Machinata.Reporting.Tools.exitFullscreenLayout(instance);
        }
    });

    // Enter fullscreen
    fullscreenContainer.fullScreen(true);

};

/// <summary>
/// Opens a report in fullscreen using a fullscreen HTML node and an iframe.
/// A toolbar is automatically created for the report.
/// </summary>
/// <deprecated/>
/// <hidden/>
Machinata.Reporting.Tools.openFullscreenReportViaIframe = function (instance,screenNumber, title, subtitle) {
    throw "Machinata.Reporting.Tools.openFullscreenReportViaIframe is no longer supported"

    /*
    // Fallback title to report tile
    if (title == null) title = instance.config.reportTitle;

    // Find screen number
    if (screenNumber == null) screenNumber = 0;

    // Create fullscreen UI
    var fullscreenContainer = $("<div class='machinata-reporting-fullscreen-container'><iframe></iframe></div>");
    var screenURL = instance.config.url;
    screenURL = Machinata.updateQueryString("screen", screenNumber, screenURL);
    screenURL = Machinata.updateQueryString("profile", "fullscreen", screenURL);
    screenURL = Machinata.updateQueryString("chapter", "*", screenURL);
    fullscreenContainer.find("iframe").attr("src", screenURL);

    // Read progress
    var fullscreenReadProgress = $("<div class='machinata-reporting-readprogress'><div class='progress'></div></div>");
    fullscreenContainer.append(fullscreenReadProgress);

    // Toolbar
    var fullscreenToolbar = Machinata.Reporting.buildToolbar(instance, title, "");
    var totalScreens = instance.totalScreens;
    if (totalScreens == 0) totalScreens = 1;
    fullscreenContainer.find("iframe").on("load", function () {
        var newURL = this.contentWindow.location.href;
        var newScreen = Machinata.queryParameterFromURL("screen", newURL);
        var currentScreen = parseInt(newScreen);
        currentScreen++;

        fullscreenToolbar.find(".subtitle").text(currentScreen + " / " + totalScreens);
        var progress = (currentScreen / totalScreens) * 100.0;
        fullscreenReadProgress.find(".progress").css("width", progress + "%");
    });
    Machinata.Reporting.addToolToToolbar(instance, fullscreenContainer, fullscreenToolbar, Machinata.Reporting.Text.translate(instance, "reporting.exit-fullscreen"), "close", function () {
        Machinata.Reporting.Tools.exitFullscreenLayout();
    });
    fullscreenToolbar.addClass("machinata-reporting-fullscreen-toolbar");
    fullscreenToolbar.find(".subtitle").text((screenNumber + 1) + " / " + totalScreens);
    fullscreenContainer.append(fullscreenToolbar);
    $("body").append(fullscreenContainer);

    // Enter fullscreen
    fullscreenContainer.fullScreen(true);

    // Set focus
    setTimeout(function () {
        fullscreenContainer.find("iframe").focus();
    }, 1000);*/
};


/// <summary>
/// Forces a exit of fullscreen mode
/// </summary>
Machinata.Reporting.Tools.exitFullscreenLayout = function (instance) {
    Machinata.Reporting.debug("Machinata.Reporting.Tools.exitFullscreenLayout");
    if (instance != null && instance.elems.fullscreen != null) {
        var fullscreenContainer = instance.elems.fullscreen;

        // Cleanup report
        var fullscreenInstance = fullscreenContainer.data("report-instance");
        if (fullscreenInstance != null) {
            Machinata.Reporting.cleanup(fullscreenInstance);
        }

        // Close container fullscreen and remove
        fullscreenContainer.fullScreen(false);
        fullscreenContainer.remove();
        instance.elems.fullscreen = null;
    }
};

/// <summary>
/// Internal method for updating the fullscreen UI (when the report is opened in fullscreen)
/// </summary>
/// <hidden/>
Machinata.Reporting.Tools._updateFullscreenUI = function (instance) {
    // Init
    var fullscreenContainer = instance.elems.content.closest(".machinata-reporting-fullscreen-container");
    var totalScreens = instance.totalScreens;
    if (totalScreens == 0) totalScreens = 1;
    var progress = ((instance.currentScreen+1) / totalScreens) * 100.0;
    fullscreenContainer.find(".machinata-reporting-readprogress .progress").css("width", progress + "%");

    // Update screen number UI
    fullscreenContainer.find(".machinata-reporting-attribute-currentscreen")
        .text((instance.currentScreen + 1) + " / " + totalScreens);

    // Update next/prev buttons
    if (instance.currentScreen >= instance.totalScreens - 1) {
        fullscreenContainer.find(".machinata-reporting-screen-controls.next").hide();
    } else {
        fullscreenContainer.find(".machinata-reporting-screen-controls.next").show();
    }
    if (instance.currentScreen <= 0) {
        fullscreenContainer.find(".machinata-reporting-screen-controls.prev").hide();
    } else {
        fullscreenContainer.find(".machinata-reporting-screen-controls.prev").show();
    }
};

/// <summary>
/// Changes the report to the given screen number.
/// </summary>
Machinata.Reporting.Tools.changeScreen = function (instance, screenNumber) {
    Machinata.Reporting.debug("Machinata.Reporting.Tools.changeScreen:", screenNumber);

    if (instance.screenNumber >= instance.totalScreens - 1) throw "Machinata.Reporting.Tools.changeScreen: screen " + screenNumber + " is not valid"; // sanity
    if (instance.screenNumber <= 0) throw "Machinata.Reporting.Tools.changeScreen: screen " + screenNumber + " is not valid"; // sanity

    // Update screen state to reflect the new screen number
    instance.currentScreen = screenNumber;
    instance.screensToShow = [screenNumber];
    
    // Update actual report to reflect the current screen state
    Machinata.Reporting.Tools._updateFullscreenUI(instance);
    Machinata.Reporting.buildReport(instance, instance.config);
}

/// <summary>
/// Moves to the next screen.
/// This is only supported in fullscreen mode.
/// </summary>
Machinata.Reporting.Tools.nextScreen = function (instance) {
    if (instance.currentScreen >= instance.totalScreens - 1) return; // sanity
    Machinata.Reporting.Tools.changeScreen(instance, instance.currentScreen + 1);
};

/// <summary>
/// Moves to the previous screen.
/// This is only supported in fullscreen mode.
/// </summary>
Machinata.Reporting.Tools.prevScreen = function (instance) {
    if (instance.currentScreen <= 0) return; // sanity
    Machinata.Reporting.Tools.changeScreen(instance, instance.currentScreen - 1);
};


/// <summary>
/// Scrolls to an element, automatically taking into account the header.
/// The header reference can be provided via ```Machinata.Reporting.Config.headerSelector```.
/// If an element is already on screen and on the top half of the viewport, the scroll is not performed.
/// </summary>
Machinata.Reporting.Tools.scrollTo = function (instance, elem, duration) {
    if (duration == null) duration = 0;
    // Compile options for scrollto
    var opts = {};
    if (instance.elems.header != null && instance.elems.header.length > 0) {
        opts.offset = { top: instance.elems.header.height() * -1.5, left: 0 };
    }
    // Validate that we even need to do a scroll, if the elem is close to the destination, then
    // we don't do the scroll, as it just annoys the user.
    opts.ignoreIfTargetIsCloseToDestination = true;
    // Do scroll
    Machinata.UI.scrollTo(elem, duration, opts);
};

/// <summary>
/// Scrolls to top of report, automatically taking into account the header.
/// See for more details ```Machinata.Reporting.Tools.scrollTo```.
/// </summary>
Machinata.Reporting.Tools.scrollToTop = function (instance, duration) {
    Machinata.Reporting.Tools.scrollTo(instance, instance.elems.content, duration);
};



/// <summary>
/// Opens/focuses/drilldowns using a link JSON object.
/// 
/// When linking to nodes within the same report, the browser will automatically scroll to the node (if it is already on the page).
/// If the given node happens to be a chapter, instead of a scroll to that node the chapter will be automatically selected.
///
/// ## Link JSON
/// The link object can have the following properties:
///  - ```tooltip```: resolvable JSON text which is used the mouse-hover tooltip or accessibility
///  - ```nodeId```: the id of a node within the same report to link to (string)
///  - ```chapterId```: the id of a chapter within the same report to link to (string)
///  - ```url```: fully qualified URL to an external page or report (string)
///  - ```target```: if defined, will open a new browser window (string)
///  - ```drilldown```: Drilldown on node information, for example invoking a table to focus on a very specific row (JSON object)
///
/// When performing drilldowns on tables, filters can be specified for the table at the same time in the drilldown
/// ```filters``` property. For more information on table filters, see ```Machinata.Reporting.Node.VerticalTableNode.defaults.filters```.
/// </summary>
/// <example>
/// ### Example with node focus
/// ```
/// "link": {
///     "tooltip": {
///        "resolved": "About SRRI risk profile"
///     },
///     "nodeId": "rep-20190710-152228-nodexyz"
/// }
/// ```
/// ### Example with chapter change
/// ```
/// "link": {
///     "tooltip": {
///        "resolved": "About SRRI risk profile"
///     },
///     "chapterId": "rep-20190710-chapterxyz"
/// }
/// ```
/// ### Example with external link
/// ```
/// "link": {
///     "tooltip": {
///        "resolved": "Go to Nerves.ch"
///     },
///     "url": "https://nerves.ch",
///     "target": "_blank"
/// }
/// ```
/// ### Example with drilldown for table with defined filters
/// Note: using this method filter strings are automatically escaped as needed.
/// ```
/// "link": {
///     "tooltip": {
///        "resolved": "Drilldown on rating AAA or BBB or A'A, but not maturity bucket A or B or C"
///     },
///     "nodeId": "rep-20190710-152228-nodexyz"
///     "drilldown": {
///         "filters": [
///             { 
///                 "columnId": "rating",
///                 "isAny": ["AAA","BBB","A'A"] // will generate the filter value "'AAA' or 'BBB' or 'A\'A'"
///             } ,
///             { 
///                 "columnId": "maturityBucket",
///                 "notAny": ["A","B","C"] // will generate the filter value "!'A' and !'B' and !'C'"
///             } 
///         ]
///     }
/// }
/// ```
/// ### Example with drilldown for table with custom filters
/// ```
/// "link": {
///     "tooltip": {
///        "resolved": "Drilldown on AAA >10 years"
///     },
///     "nodeId": "rep-20190710-152228-nodexyz"
///     "drilldown": {
///         "filters": [
///             { 
///                 "columnId": "rating",
///                 "expression": "'AAA' and !'BBB'"
///             },
///             { 
///                 "columnId": "maturityBucket",
///                 "expression": ">10 and <15"
///             },
///             { 
///                 "columnId": "type",
///                 "value": "Security"
///             } 
///         ]
///     }
/// }
/// ```
/// </example>
Machinata.Reporting.Tools.openLink = function (instance,linkJSON) {
    if (linkJSON == null) return;

    // Which mode?
    if (linkJSON.url != null) {
        // URL
        if (linkJSON.target != null) {
            window.open(linkJSON.url, linkJSON.target);
        } else {
            window.location = linkJSON.url;
        }
    } else if (linkJSON.elementId != null) {
        // HTML element drilldown
        Machinata.Reporting.Tools.drilldownOnElement(instance,linkJSON);
    } else if (linkJSON.nodeId != null) {
        // Node drilldown
        Machinata.Reporting.Tools.drilldownOnNode(instance,linkJSON);
    } else if (linkJSON.chapterId != null) {
        // Chapter
        Machinata.Reporting.Tools.changeChapter(instance,linkJSON.chapterId);
    }

};


/// <summary>
/// Changes to a ```chapterId``` in the current report via the default handler.
/// If no handler is provided, then a URL update is performed.
/// </summary>
Machinata.Reporting.Tools.changeChapter = function (instance, chapterId) {
    if (Machinata.Reporting.Handler.changeChapter != null) {
        // Handler provided
        Machinata.Reporting.Handler.changeChapter(instance, chapterId);
    } else {
        // URL update
        Machinata.Reporting.Tools.changeChapterViaURLUpdate(instance, chapterId); // default
    }
};

/// <summary>
/// Changes to a ```chapterId``` in the current report via the default handler.
/// If no handler is provided, then a URL update is performed.
/// </summary>
Machinata.Reporting.Tools.changeChapterAndFocusOnNode = function (instance, chapterId, nodeId) {
    if (Machinata.Reporting.Handler.changeChapterAndFocusOnNode != null) {
        // Handler provided
        Machinata.Reporting.Handler.changeChapterAndFocusOnNode(instance, chapterId, nodeId);
    } else {
        // URL Update
        var nodeJSON = Machinata.Reporting.Node.getJSONByID(instance, nodeId);
        Machinata.Reporting.Tools.openLink(instance, { url: nodeJSON.url });
    }
};

/// <summary>
/// Changes to a ```chapterId``` in the current report.
/// </summary>
Machinata.Reporting.Tools.changeChapterViaURLUpdate = function (instance,chapterId) {
    // Find matching chapter go to that url
    Machinata.Util.each(instance.chapters, function (index, chapterJSON) {
        if (chapterJSON.id == chapterId) {
            window.location = chapterJSON.chapterURL;
            return;
        }
    });
};

/// <summary>
/// Changes to a ```chapterId``` in the current report.
/// </summary>
Machinata.Reporting.Tools.changeChapterViaInlineUpdate = function (instance, chapterId, onSuccess) {
    // Update loading UI
    Machinata.Reporting.setLoading(instance, true, true); // locked loading...
    //setTimeout(function () {
        // Just rebuild the report set to the new chapter
        instance.config.chapters = [chapterId];
        instance.config.initialChapter = chapterId;
        Machinata.Reporting.buildReport(instance, instance.config, onSuccess);
        // Update loading UI
        Machinata.Reporting.setLoading(instance, false, true); // unlock loading...
    //}, (instance != null && instance.config != null) ? instance.config.loadingUITimeoutDelay : 50);
};

/// <summary>
/// 
/// </summary>
Machinata.Reporting.Tools.showReportDates = function (instance, catalog, mandate, report, defaultDate, title) {
    if (title == null) title = Machinata.Reporting.Text.translate(instance, 'reporting.change-date');
    var diag = Machinata.messageDialog(title, '');
    var datePickerElem = $("<div class='ui-datepicker'></div>");
    if (report != null && defaultDate == null) defaultDate = report.date;
    datePickerElem.datepicker({
        defaultDate: defaultDate,
        prevText: "",
        nextText: "",
        onSelect: function (dateText, inst) {
            var date = datePickerElem.datepicker("getDate");
            var report = mandate.dateToReportMap[date];
            Machinata.goToPage(report.urlResolved);
        },
        beforeShowDay: function (date) {
            // See https://api.jqueryui.com/datepicker/#option-beforeShowDay
            // Init
            var selectable = false;
            var classes = "";
            var tooltip = Machinata.Reporting.Text.translate(instance,"reporting.no-data-for-this-date"); 
            // Data available?
            if (mandate.dateToReportMap[date] != null) {
                selectable = true;
                //tooltip = mandate.dateToReportMap[date].dateResolved; //BUG: jquery ui bug
                tooltip = null;
            }
            return [selectable, classes, tooltip];
        }
    });
    diag.elem.append(datePickerElem);
    diag.cancelButton();
    diag.show();
};

/// <summary>
/// A universal routine to make any table sortable.
/// </summary>
Machinata.Reporting.Tools.makeTableSortable = function (instance, tableElem) {
    var allowReset = true;
    if (instance.config != null) allowReset = instance.config.allowTableSortResets;
    // See https://mottie.github.io/tablesorter/docs/example-widget-filter.html#filter-placeholder for more options
    tableElem.tablesorter({
        sortReset: allowReset, // third click on the header will reset column to default - unsorted
        widthFixed: false, // was true
        widgets: ['staticRow'],
        widgetOptions: {
            
        }
    });

    //TODO: original this also invoked the filters... check all callers to see if this needs to be called sepeartaly
};

/// <summary>
/// A universal routine to make any table filterable.
/// </summary>
Machinata.Reporting.Tools.makeTableFilterable = function (instance, tableElem, filters) {
    // Init filter options
    var opts = {};
    opts.filters = filters; // optional, can be null
    opts.noResultsText = "";
    opts.buildFilterInputUI = function (tableElem, opts, textInputElem, filterHeaderColumnElem) {
        // No longer needed: tooltips are more annoying than useful
        //textInputElem.attr("data-tooltip-empty", Machinata.Reporting.Text.translate(instance, "reporting.nodes.search-filter") + " " + textInputElem.attr("data-column-title"));
    };
    opts.buildNamedFilterUI = function (tableElem, opts, namedFilterElem, filter) {
        var iconElem = Machinata.Reporting.buildIcon(instance, "cross");
        iconElem.addClass("action-remove-filter");
        iconElem.attr("title", Machinata.Reporting.Text.translate(instance, "reporting.nodes.remove-filter"));
        namedFilterElem.append(iconElem);
    };
    opts.buildNoResultsUI = function (tableElem, opts, noResultsRow) {
        // Create our own UI
        var noResultsUI = $('<div>' + Machinata.Reporting.Text.translate(instance, "reporting.filter-no-results") + ' <a class="reset-action">' + Machinata.Reporting.Text.translate(instance, "reporting.filter-no-results.reset") + '</a><br/><br/><a class="info-action">' + Machinata.Reporting.Text.translate(instance, "reporting.filter-no-results.filter-info") + '</a>' + '</div>');
        // Bind actions
        noResultsUI.find("a.info-action").click(function () {
            var dialog = Machinata.messageDialog(
                Machinata.Reporting.Text.translate(instance, "reporting.filter-no-results.filter-info"),
                Machinata.Reporting.Text.translate(instance, "reporting.filter-no-results.filter-info.message")
            );
            var listElem = $("<ul style='margin-left:2em;margin-bottom:2em;'></ul>");
            listElem.append($("<li/>").text(Machinata.Reporting.Text.translate(instance, "reporting.filter-no-results.filter-info.tip-1")));
            listElem.append($("<li/>").text(Machinata.Reporting.Text.translate(instance, "reporting.filter-no-results.filter-info.tip-2")));
            listElem.append($("<li/>").text(Machinata.Reporting.Text.translate(instance, "reporting.filter-no-results.filter-info.tip-3")));
            listElem.append($("<li/>").text(Machinata.Reporting.Text.translate(instance, "reporting.filter-no-results.filter-info.tip-4")));
            listElem.append($("<li/>").text(Machinata.Reporting.Text.translate(instance, "reporting.filter-no-results.filter-info.tip-5")));
            listElem.append($("<li/>").text(Machinata.Reporting.Text.translate(instance, "reporting.filter-no-results.filter-info.tip-6")));
            listElem.append($("<li/>").text(Machinata.Reporting.Text.translate(instance, "reporting.filter-no-results.filter-info.tip-7")));
            //listElem.append($("<li/>").text(Machinata.Reporting.Text.translate(instance, "reporting.filter-no-results.filter-info.tip-8")));
            dialog.elem.append(listElem);
            dialog.show();
        });
        // Add to no results row
        noResultsRow.find("td").append(noResultsUI);
    };
    // Make filterable
    Machinata.Filter.Tables.makeTableFilterable(tableElem, opts);


    //TODO: to port from old code:
    /*
    // Mark as disabled (where needed)
    tableElem.find(".tablesorter-filter-row .tablesorter-filter.disabled")
        .parent()
        .addClass("disabled");
    */
}

/// <summary>
/// Performs a drilldown operation on a table. Usually this is not called directly, but rather
/// indirectly using a drilldown on a node/element using a link JSON.
/// </summary>
Machinata.Reporting.Tools.drilldownOnTable = function (instance, tableElem, drilldownJSON) {
    // Sanity
    if (drilldownJSON.filters == null) {
        drilldownJSON.filters = [];
    }
    var nodeElem = tableElem.data("node");
    var nodeJSON = null;
    if (nodeElem != null) nodeJSON = nodeElem.data("json");

    // Do all the columns for the filter exist?
    var columnsNotExistingButWillBeFiltered = [];
    Machinata.Util.each(drilldownJSON.filters, function (index, filter) {
        // Does the column exist?
        var columnHeaderElem = tableElem.find("tr.header.columns th.column[data-col-id='" + filter.columnId +"']");
        if (columnHeaderElem.length == 0) {
            columnsNotExistingButWillBeFiltered.push(filter.columnId);
        }
    });
    if (columnsNotExistingButWillBeFiltered.length > 0) {
        // Manipulate and rebuild the table?
        if (nodeElem != null) {
            if (nodeJSON != null && nodeJSON.columnDimension != null && nodeJSON.columnDimension.dimensionGroups != null) {
                // Go through each column and see if it matches on in columnsNotExistingButWillBeFiltered
                // If so - set it to display = true
                Machinata.Util.each(nodeJSON.columnDimension.dimensionGroups, function (index, dimensionGroup) {
                    Machinata.Util.each(dimensionGroup.dimensionElements, function (index, dimensionElement) {
                        Machinata.Util.each(columnsNotExistingButWillBeFiltered, function (index, columnNotExistingButWillBeFiltered) {
                            if (columnNotExistingButWillBeFiltered == dimensionElement.id) {
                                dimensionElement.display = true; // force display
                                return false; // break each
                            }
                        });
                    });
                });
                // Alter the node json column dimensions to make sure they are shown
                nodeElem.trigger(Machinata.Reporting.Events.REBUILD_TABLE_SIGNAL);
                tableElem = nodeElem.data("table"); // get handle on new table
            } else {
                console.warn("Machinata.Reporting.Tools.drilldownOnTable(): some columns for the filter do not exist: " + JSON.stringify(columnsNotExistingButWillBeFiltered) + " (could not get node json for the table node)");
            }
        } else {
            console.warn("Machinata.Reporting.Tools.drilldownOnTable(): some columns for the filter do not exist: " + JSON.stringify(columnsNotExistingButWillBeFiltered) + " (could not get a node for the table)");
        }
    }

    // Apply filter
    var filters = drilldownJSON.filters;
    if (filters == null || filters.length == 0) {
        Machinata.Filter.Tables.resetFilters(tableElem);
    } else {
        // Do we have a node json?
        if (nodeJSON != null) {
            Machinata.Reporting.Node.VerticalTableNode.makeSureAllDataIsLoaded(instance, instance.config, nodeJSON, nodeElem, function () {
                // Data loaded, now set the filters
                //TODO: would be nicer if this happened already in the load
                Machinata.Filter.Tables.setFilters(nodeElem.data("table"), filters); // note table is now new...
            })
        } else {
            // No node json, just set filter
            Machinata.Filter.Tables.setFilters(tableElem, filters);
        }
    }
    
    // Show
    tableElem.show();

};

/// <summary>
/// Scrolls to a html element, automatically taking into account the header
/// </summary>
Machinata.Reporting.Tools.drilldownOnElement = function (instance,linkJSON) {
    var targetElement = null;
    if (linkJSON.elementId != null) targetElement = $("#" + linkJSON.elementId);

    if (linkJSON.drilldown != null) {
        Machinata.Reporting.Tools.drilldownOnTable(instance, targetElement, linkJSON.drilldown);
    }

    Machinata.Reporting.Tools.scrollTo(instance, targetElement, 1200);
};

/// <summary>
/// Scrolls to a node element, automatically taking into account the header
/// </summary>
Machinata.Reporting.Tools.drilldownOnNode = function (instance,linkJSON) {

    var targetNodeElem = null;
    if (linkJSON.nodeId != null) targetNodeElem = $("#" + linkJSON.nodeId);

    if (targetNodeElem == null || targetNodeElem.length == 0) {
        // Node is not on page, get its link
        //console.log(linkJSON);
        //console.log(instance.idToNodeJSONLookupTable);
        var nodeJSON = Machinata.Reporting.Node.getJSONByID(instance,linkJSON.nodeId);
        if (nodeJSON == null) {
            console.warn("Machinata.Reporting.Tools.drilldownOnNode: could not find a matching node (" + linkJSON.nodeId+") for this report!");
            return;
        }
        // Is this node actually a chapter?
        if (nodeJSON.chapter == true) {
            // Just change chapter - node is a chapter
            Machinata.Reporting.Tools.changeChapter(instance,linkJSON.nodeId);
        } else {
            // Node is not on page and not a chapter - we need to switch to the chapter and then focus...
            Machinata.Reporting.Tools.changeChapterAndFocusOnNode(instance, nodeJSON.chapterId, linkJSON.nodeId);
        }
    } else {
        // Node is on page, just scroll to it...
        // Do we have drilldown information for the node? If so we perform the drilldown operation...
        if (linkJSON.drilldown != null) {
            var tableElem = targetNodeElem.find("table");
            if (tableElem.length == 1) {
                // Drilldown
                Machinata.Reporting.Tools.drilldownOnTable(instance, tableElem, linkJSON.drilldown);
                // Show filters (if allowed)
                if (targetNodeElem.hasClass("option-allow-row-filtering")) {
                    Machinata.Reporting.Node.VerticalTableNode.showTableFilters(instance, targetNodeElem);
                }
            }
        }
        // Is the node hidden behind something, like a toggle node?
        var didNeedToReveal = Machinata.Reporting.Tools.revealNodeIfHidden(instance, linkJSON.nodeId);
        if (didNeedToReveal == true) {
            // Scroll to elem
            //setTimeout(function () {
            if (linkJSON.scrollPage != false) Machinata.Reporting.Tools.scrollTo(instance, targetNodeElem, 1200);
            //}, 10);
        } else {
            // Scroll to elem
            if (linkJSON.scrollPage != false) Machinata.Reporting.Tools.scrollTo(instance, targetNodeElem, 1200);
        }
    }
};


/// <summary>
/// </summary>
Machinata.Reporting.Tools.revealNodeIfHidden = function (instance, nodeId) {
    var didNeedToReveal = false;
    // Helper function
    function revealToggleForNodeElem(currentNodeElem, depth) {
        if (depth == null) depth = 1;
        if (depth > 10) {
            console.warn("Machinata.Reporting.Tools.revealNodeIfHidden: aborting reveal up node tree, too many nested toggles (more than 10).");
            return;
        }
        // Check if parent is toggle?
        //var parentToggleElem = currentNodeElem.parent().parent();
        var parentToggleElem = currentNodeElem.parent().closest(".machinata-reporting-node.type-ToggleNode");
        if (parentToggleElem.length == 1) {
            var toggleElem = parentToggleElem;
            //("DEBUG: parent is toggle (id=" + toggleElem.attr("data-node-id")+" depth=" + depth + ")");
            // Get the current tab id
            var currentToggledTabId = toggleElem.attr("data-current-tab");
            //alert("DEBUG: current tab id = " + currentToggledTabId);
            // Get the needed tab (this is the closest toggle-node-child)
            var toggleTabNodeElem = currentNodeElem.closest(".toggle-node-child");
            var toggleTabId = toggleTabNodeElem.attr("data-node-id");
            //alert("DEBUG: toggle tab id = " + toggleTabId);
            // Are we currently toggled?
            if (toggleTabId != currentToggledTabId) {
                // Node is not toggled - toggle to it!
                //alert("DEBUG: Node is not toggled, will toggle " + toggleTabId);
                didNeedToReveal = true;
                Machinata.Reporting.Tools.showToggleNodeTab(instance, toggleElem, toggleTabId);
            }
            // Recurse up
            revealToggleForNodeElem(parentToggleElem, depth + 1);
        } else {
            //alert("DEBUG: no more toggles found");
        }
    }
    // Begin recursion
    var targetNodeElem = $("#" + nodeId);
    if (targetNodeElem.length != 0) {
        // Process ToggleNodes, ensuring that the tree path to the node has the toggles opened
        revealToggleForNodeElem(targetNodeElem);
    }
    return didNeedToReveal;
};


/// <summary>
/// </summary>
Machinata.Reporting.Tools.showToggleNodeTab = function (instance, toggleElem, tabId) {
    // Send signal to node elem (must be a toggle)
    toggleElem.trigger(Machinata.Reporting.Events.SHOW_TAB_SIGNAL, [tabId]);
};

/// <summary>
/// Opens the print composer built-in UI.
/// </summary>
Machinata.Reporting.Tools.openPrintComposer = function (instance) {
    Machinata.Reporting.PrintComposer.UI.open(instance);
}


/// <summary>
/// Opens the browser print dialog
/// </summary>
Machinata.Reporting.Tools.openBrowserPrintDialog = function (instance) {
    window.print();
}

/// <summary>
/// 
/// </summary>
/// <deprecated/>
Machinata.Reporting.Tools.openPrintPage = function (instance, nodeInfosOrChapter) {
    throw "Machinata.Reporting.Tools.openPrintPage is deprecated!";
}

/// <summary>
/// 
/// </summary>
Machinata.Reporting.Tools.openPrintPageForSection = function (instance,nodeInfos) {
    var screenNumber = nodeInfos.nodeScreen;
    var printURL = instance.config.url;
    printURL = Machinata.updateQueryString("screen", screenNumber, printURL);
    printURL = Machinata.updateQueryString("profile", "webprint", printURL);
    printURL = Machinata.updateQueryString("chapter", "*", printURL);
    printURL = Machinata.updateQueryString("print-dialog", "true", printURL);
    Machinata.openPage(printURL);
};

/// <summary>
/// 
/// </summary>
Machinata.Reporting.Tools.openPrintPageForChapter = function (instance, chapter) {
    var printURL = instance.config.url;
    printURL = Machinata.updateQueryString("screen", "", printURL);
    printURL = Machinata.updateQueryString("profile", "webprint", printURL);
    printURL = Machinata.updateQueryString("chapter", chapter, printURL);
    printURL = Machinata.updateQueryString("print-dialog", "true", printURL);
    Machinata.openPage(printURL);
};

/// <summary>
/// 
/// </summary>
Machinata.Reporting.Tools.openPrintPageForReport = function (instance, reportId) {
    var printURL = instance.config.url;
    printURL = Machinata.updateQueryString("screen", "", printURL);
    printURL = Machinata.updateQueryString("profile", "webprint", printURL);
    printURL = Machinata.updateQueryString("chapter", "*", printURL);
    printURL = Machinata.updateQueryString("print-dialog", "true", printURL);
    Machinata.openPage(printURL);
};


/// <summary>
/// For the given report, shows a menu of various printing options.
/// Typically such options include the following:
///  - Print entire report
///  - Print current chapter
///  - View print composer
/// </summary>
Machinata.Reporting.Tools.printReport = function (instance) {
    
    var opts = {};
    if (Machinata.Reporting.Handler.openPrintPageForReport != null) {
        opts["printreport"] = Machinata.Reporting.Text.translate(instance, "reporting.printing.option-report");
    }
    if (Machinata.Reporting.Handler.openPrintPageForChapter != null && instance.chapters != null && instance.chapters.length > 1 && instance.config.automaticallyFoldChaptersIntoPages == true) {
        opts["printchapter"] = Machinata.Reporting.Text.translate(instance, "reporting.printing.option-chapter");
    }
    if (Machinata.Reporting.Handler.openPrintComposer != null && Machinata.Reporting.PrintComposer.hasData(instance) == true) {
        opts["viewcomposer"] = Machinata.Reporting.Text.translate(instance, "reporting.printing.option-view-composer");
    }
    if (Object.keys(opts).length == 0) console.warn("Machinata.Reporting.Tools.printReport: no handlers have been registered for printing. See Machinata.Reporting.Handler.printReport for more information.");
    Machinata.optionsDialog(Machinata.Reporting.Text.translate(instance, "reporting.printing.print-report.title"), Machinata.Reporting.Text.translate(instance, "reporting.printing.print-report.message"), opts)
        .okay(function (id, val) {
            if (id == "printchapter") {
                Machinata.Reporting.Handler.openPrintPageForChapter(instance,instance.config.chapters[0]);
            } else if (id == "viewcomposer") {
                Machinata.Reporting.Handler.openPrintComposer(instance);
            } else  {
                Machinata.Reporting.Handler.openPrintPageForReport(instance, instance.reportData == null ? null : instance.reportData.reportId);
            }
        })
        .show();
};

/// <summary>
/// For the given node, shows a menu of various printing options.
/// Typically such options include the following:
///  - Print entire report
///  - Print current chapter
///  - Print this section
///  - Add to print composer
///  - View print composer
/// </summary>
Machinata.Reporting.Tools.printNode = function (instance, infosJSON, nodeJSON) {

    var opts = {};
    // Report
    if (Machinata.Reporting.Handler.openPrintPageForReport != null) {
        opts["printreport"] = Machinata.Reporting.Text.translate(instance, "reporting.printing.option-report");
    }
    // Chapter
    if (Machinata.Reporting.Handler.openPrintPageForChapter != null && instance.chapters != null && instance.chapters.length > 1) {
        opts["printchapter"] = Machinata.Reporting.Text.translate(instance, "reporting.printing.option-chapter");
    }
    // Section
    if (Machinata.Reporting.Handler.openPrintPageForSection != null) {
        opts["printsection"] = Machinata.Reporting.Text.translate(instance, "reporting.printing.option-section");
    }
    // Add to composer
    if (Machinata.Reporting.Handler.openPrintComposer != null) {
        opts["addtocomposer"] = Machinata.Reporting.Text.translate(instance, "reporting.printing.option-add-composer");
    }
    // View composer
    if (Machinata.Reporting.Handler.openPrintComposer != null && Machinata.Reporting.PrintComposer.hasData(instance) == true) {
        opts["viewcomposer"] = Machinata.Reporting.Text.translate(instance, "reporting.printing.option-view-composer");
    }
    if (Object.keys(opts).length == 0) console.warn("Machinata.Reporting.Tools.printReport: no handlers have been registered for printing. See Machinata.Reporting.Handler.printNode for more information.");
    // Show dialog with all options
    Machinata.optionsDialog(Machinata.Reporting.Text.translate(instance, "reporting.printing.print-node.title"), Machinata.Reporting.Text.translate(instance, "reporting.printing.print.node.message"), opts)
        .okay(function (id, val) {
            if (id == "addtocomposer") {
                Machinata.Reporting.PrintComposer.addScreen(instance, infosJSON);
            } else if (id == "printsection") {
                Machinata.Reporting.Handler.openPrintPageForSection(instance, infosJSON);
            } else if (id == "printchapter") {
                Machinata.Reporting.Handler.openPrintPageForChapter(instance,nodeJSON.chapterId);
            } else if (id == "viewcomposer") {
                Machinata.Reporting.Handler.openPrintComposer(instance);
            } else {
                Machinata.Reporting.Handler.openPrintPageForReport(instance, instance.reportData == null ? null : instance.reportData.reportId);
            }
        })
        .show();
};


/// <summary>
/// </summary>
Machinata.Reporting.Tools.copyElementToClipboard = function (tempElem, title, message, useTemporaryDom) {
    if (useTemporaryDom == true) {
        Machinata.UI.copyElementToClipboardInTemporaryDOM(tempElem);
    } else {
        Machinata.UI.copyElementToClipboard(tempElem);
    }
    setTimeout(function () {
        Machinata.messageDialog(title, message).show();
    }, 100);
};


/// <summary>
/// </summary>
Machinata.Reporting.Tools.generateThemeCode = function (config, type) {
    var ret = "";
    ret += "// Machinata.Reporting.Tools.generateThemeCode(\"" + type + "\")\n";
    ret += "//   type: " + type + "\n";

    Machinata.Util.each(Machinata.Reporting._themesNew, function (name, theme) {
        ret += "\n";
        for (var i = 0; i < theme.colorsBG.length; i++) {
            var shade = theme.colorNames[i];
            var color = theme.colorsBG[i];
            var textColor = theme.colorsFG[i];
            if (color == null) continue;
            ret += "/* Theme: " + theme.name + ", Shade: " + shade + " */\n";
            ret += ".machinata-reporting-themable.theme-" + theme.name + " .theme-fill-" + shade + " { fill: " + color + "; }  \n";
            ret += ".machinata-reporting-themable.theme-" + theme.name + " .theme-stroke-" + shade + " { stroke: " + color + "; }  \n";
            ret += ".machinata-reporting-themable.theme-" + theme.name + " .theme-bg-" + shade + " { background-color: " + color + "; }  \n";
            ret += ".machinata-reporting-themable.theme-" + theme.name + " .theme-color-" + shade + " { color: " + color + "; }  \n";
            if (textColor != null) ret += ".machinata-reporting-themable.theme-" + theme.name + " .theme-text-" + shade + " { color: " + textColor + "; }  \n";

        }
    });
    return ret;
};

/// <summary>
/// </summary>
Machinata.Reporting.Tools.getWordsFromString = function (text, opts) {
    // Init
    if (opts == null) opts = {};
    if (opts.wordSeparator == null) opts.wordSeparator = " ";
    var splitVal = opts.wordSeparator;
    var splitChar = splitVal[0];
    var ret = [];
    var nextVal = '';
    var i = 0;
    // Parse text, searching for any word separator...
    while (i < text.length) {
        if (text[i] === Machinata.SOFT_HYPHEN) {
            ret.push({ word: nextVal, separator: Machinata.SOFT_HYPHEN });
            nextVal = '';
            i = i + 1;
        } if (text[i] === Machinata.NEW_LINE) {
            ret.push({ word: nextVal, separator: Machinata.NEW_LINE });
            nextVal = '';
            i = i + 1;
        } else if (text[i] === splitChar) {
            ret.push({ word: nextVal, separator: splitVal });
            nextVal = '';
            i = i + 1;
        } else {
            nextVal += text[i];
            i = i + 1;
        }
    }
    ret.push({ word: nextVal, separator: null }); // terminate
    // Shift all separators to next word
    for (var w = ret.length-1; w >= 1; w--) {
            ret[w].separator = ret[w - 1].separator;
    }
    if (ret.length > 0) ret[0].separator = ''; // empty, first word never has a separator
    return ret;
};

/// <summary>
/// </summary>
Machinata.Reporting.Tools.lineBreakString = function (text, opts) {
    // Debug
    var DEBUG_LINE_BREAK = false;
    // Init
    if (opts == null) opts = {};
    if (opts.appendEllipsisOnLastLine == null) opts.appendEllipsisOnLastLine = true;
    if (opts.wordSeparator == null) opts.wordSeparator = " ";
    if (opts.lineSeparator == null) opts.lineSeparator = Machinata.NEW_LINE;
    if (opts.ellipsis == null) opts.ellipsis = Machinata.Reporting.Config.textTruncationEllipsis;
    if (opts.returnArray == null) opts.returnArray = false;
    var lines = [];
    var currentLine = null;
    // Sanity
    if (text == null || text == "") {
        if (opts.returnArray == true) return [""];
        else return "";
    }
    if (DEBUG_LINE_BREAK == true) alert("text: "+text);
    // Loop words
    var words = Machinata.Reporting.Tools.getWordsFromString(text, { wordSeparator: opts.wordSeparator });
    if (DEBUG_LINE_BREAK == true) console.log(words);
    var currentWordSeperator = null;
    for (var w = 0; w < words.length; w++) {
        // Init
        var word = words[w].word;
        currentWordSeperator = words[w].separator;
        if (DEBUG_LINE_BREAK == true) console.log("word=", word, "sep=", currentWordSeperator?currentWordSeperator.charCodeAt(0):null, "reason=", words[w].reason,);
        // Basically if wl is >0, then we moving to a new forced line break
        // Thus we will want to commit the currentLine and then continue building a new line
        if (currentWordSeperator == Machinata.NEW_LINE) {
            // Soft-hyphen?
            if (currentWordSeperator == Machinata.SOFT_HYPHEN) currentLine += Machinata.HYPHEN;
            // Push current
            if (currentLine != null) lines.push(currentLine);
            // New line
            currentLine = "";
        }
        var newCurrentLine = null;
        // Create a new line
        if (currentLine == null) newCurrentLine = word;
        else newCurrentLine = currentLine + currentWordSeperator + word;
        if (DEBUG_LINE_BREAK == true) alert("newCurrentLine: " + newCurrentLine);
        if (newCurrentLine.length > opts.maxCharsPerLine) {
            // Soft-hyphen?
            if (currentWordSeperator == Machinata.SOFT_HYPHEN) currentLine += Machinata.HYPHEN;
            // Push current
            if (DEBUG_LINE_BREAK == true) alert("pushing: " + currentLine);
            if (currentLine != null) lines.push(currentLine);
            // New line with current word
            currentLine = word;
        } else {
            // Accept, continue...
            currentLine = newCurrentLine;
        }
    }
    // Soft-hyphen?
    //if (currentWordSeperator == Machinata.SOFT_HYPHEN) currentLine += Machinata.HYPHEN;
    // Push final line
    if (DEBUG_LINE_BREAK == true) alert("pushing final: " + currentLine);
    lines.push(currentLine);
    // Rebuild
    var ret = null;
    for (var l = 0; l < lines.length; l++) {
        // Init
        var line = lines[l];
        // Clean out line of any special characters
        if (line != null) line = line.replace(Machinata.SOFT_HYPHEN,""); 
        if (line != null) line = line.replace(Machinata.NEW_LINE,""); 
        // Validate
        if (opts.maxLines != null && l >= (opts.maxLines)) {
            if (opts.returnArray == true) {
                if (opts.appendEllipsisOnLastLine == true) {
                    ret[ret.length - 1] = ret[ret.length - 1] + opts.ellipsis;
                } else {
                    ret.push(opts.ellipsis);
                }
            } else {
                if (opts.appendEllipsisOnLastLine == true) {
                    ret = ret + opts.ellipsis;
                } else {
                    ret = ret + opts.lineSeparator + opts.ellipsis;
                }
            }
            break;
        }
        // Append
        if (opts.returnArray == true) {
            if (ret == null) ret = [];
            //var linesInLine = line.split(opts.lineSeparator); // this additional split will allow the input to force a custom set line break
            //for (var l = 0; l < linesInLine.length; l++) {
            //    ret.push(linesInLine[l]);
            //}
            ret.push(line);
        } else {
            if (ret == null) ret = line;
            else ret = ret + opts.lineSeparator + line;
        }
        
    }
    //console.log(ret);
    return ret;
};


/// <summary>
/// Creates a title using the givens string with variables, using the avaiable data.
/// You can use the following variables:
/// - ```{mandate.id}```
/// - ```{mandate.title}```
/// - ```{report.id}```
/// - ```{report.title}```
/// - ```{report.subtitle}```
/// - ```{report.name}```
/// - ```{report.date-raw}```
/// - ```{report.date-resolved}```
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Tools.createTitleString(instance, config, "ACME AG Report {report.portfolio-id} {report.date-raw} {report.title}");
/// ```
/// </example>
Machinata.Reporting.Tools.createTitleString = function (instance, config, text) {
    // Grab template variables from various sources
    if (instance.catalogCurrentMandate != null) {
        text = text.replaceAll("{mandate.id}", instance.catalogCurrentMandate.id);
        text = text.replaceAll("{mandate.title}", instance.catalogCurrentMandate.title);
    }
    if (instance.catalogCurrentReport != null) {
        text = text.replaceAll("{report.id}", instance.catalogCurrentReport.id);
        text = text.replaceAll("{report.date-raw}", instance.catalogCurrentReport.dateRaw);
        text = text.replaceAll("{report.date-resolved}", instance.catalogCurrentReport.dateResolved);
        if (instance.catalogCurrentReport.parameters !=  null) text = text.replaceAll("{report.portfolio-id}", instance.catalogCurrentReport.parameters.portfolioId);
    }
    text = text.replaceAll("{report.title}", config.reportTitle);
    text = text.replaceAll("{report.subtitle}", config.reportSubtitle);
    text = text.replaceAll("{report.name}", config.reportName);
    // Cleanup
    text = text.replaceAll("{mandate.id}", "");
    text = text.replaceAll("{mandate.title}", "");
    text = text.replaceAll("{report.id}", "");
    text = text.replaceAll("{report.title}", "");
    text = text.replaceAll("{report.subtitle}", "");
    text = text.replaceAll("{report.name}", "");
    text = text.replaceAll("{report.date-resolved}", "");
    text = text.replaceAll("{report.date-raw}", "");
    text = text.replaceAll("{report.portfolio-id}", "");
    //TODO: trim out missing variables separators...
    text = text.replace(/  +/g, ' ');
    text = text.replace(/ - - +/g, ' ');
    return text;
};

/// <summary>
/// </summary>
Machinata.Reporting.Tools.measureStringApproximately = function (config, text, fontSize, fontName) {
    // Init
    if (text == null) return 0;
    if (fontSize == null) fontSize = config.textSize; // fallback
    // Do approximate calculation using config.fontSizeToCharacterWidthRatio
    var approxLen = text.length * fontSize * config.fontSizeToCharacterWidthRatio;
    return approxLen;
};

/// <summary>
/// </summary>
Machinata.Reporting.Tools.convertPixelstoCharactersApproximately = function (config, pixels, fontSize, fontName) {
    // Init
    if (fontSize == null) fontSize = config.textSize; // fallback
    // Do approximate calculation using config.fontSizeToCharacterWidthRatio
    var approxChars = pixels / (fontSize * config.fontSizeToCharacterWidthRatio);
    return approxChars;
};

/// <summary>
/// </summary>
Machinata.Reporting.Tools.bindTableUI = function (instance,elements) {
    // Table filters/sorting
    elements.filter(".machinata-reporting-table.option-sort").each(function () {
        Machinata.Reporting.Tools.makeTableSortable(instance, tableElem);
    });
    elements.filter(".machinata-reporting-table.option-filter").each(function () {
        var tableElem = $(this);
        Machinata.Reporting.Tools.makeTableFilterable(instance, tableElem);
    });

    // Sticky table headers
    elements.filter(".machinata-reporting-table.option-auto-sticky").each(function () {
        var tableElem = $(this);
        if (tableElem.find(".table-row").length > 5) {
            tableElem.stickyTableHeaders({
                cacheHeaderHeight: true,
                fixedOffset: $('#header')
            });
            tableElem.on("enabledStickiness.stickyTableHeaders", function () {
                $(this).addClass("header-is-sticky");
            });
            tableElem.on("disabledStickiness.stickyTableHeaders", function () {
                $(this).removeClass("header-is-sticky");
            });
        }
    });

    // Jump to top
    elements.find(".table-header .icon-arrow.up").click(function () {
        var tableElem = $(this).closest(".ui-table");
        Machinata.UI.scrollTo(tableElem, 600, { offset: -60 });
    });

    // Clickable rows
    elements.find("tr.option-link").click(function () {
        var url = $(this).attr("href");
        if (url == null || url == "") return;
        Machinata.goToPage(url);
    });
};

/// <summary>
/// Binds a custom tooltips handler to the given elements (a JQuery selector).
/// </summary>
/// <example>
/// ```
/// Machinata.Reporting.Tools.bindTooltipsUI(instance, $("body"))
/// ```
/// </example>
Machinata.Reporting.Tools.bindTooltipsUI = function (instance, elements) {
    elements.each(function () {
        var elem = $(this);
        // Destroy if already?
        if (elem.hasClass("ui-tooltips-bound")) {
            elem.tooltip("destroy");
        }
        // Bind
        elem.tooltip({
            classes: {
                "ui-tooltip": "machinata-reporting-tooltip"
            },
            track: true,
            show: {
                delay: 0,
                effect: "none"
            },
            hide: {
                delay: 0,
                effect: "none"
            },
            position: {
                my: "center bottom-40",
                at: "center top",
                using: function (position, feedback) {
                    $(this).css(position);
                    $("<div>").addClass("arrow").addClass(feedback.vertical).appendTo(this);
                }
            },
            open: function (e, ui) {
                if (document.fullscreenElement) {
                    ui.tooltip.appendTo($(document.fullscreenElement));
                }
            }
        });
        elem.addClass("ui-tooltips-bound");
    });
};



/// <summary>
/// </summary>
Machinata.Reporting.Tools.askAQuestionViaEmail = function (instance, infosJSON, nodeJSON) {
    var toEmail = instance.config.askAQuestionViaEmailDefaultEmail;
    var toName = instance.config.askAQuestionViaEmailDefaultName;
    var subject = instance.config.askAQuestionViaEmailDefaultSubject;
    var body = instance.config.askAQuestionViaEmailDefaultBody;
    function replaceVariables(str) {
        str = str.replace("{report.title}", infosJSON.reportTitle);
        str = str.replace("{report.subtitle}", infosJSON.reportSubtitle);
        str = str.replace("{report.url}", infosJSON.reportURL);
        str = str.replace("{node.title}", infosJSON.nodeTitle);
        str = str.replace("{node.subtitle}", infosJSON.nodeSubtitle);
        str = str.replace("{node.url}", infosJSON.nodeURL);
        str = str.replace("{to.email}", toEmail);
        str = str.replace("{to.name}", toName);
        return str;
    }
    subject = replaceVariables(subject);
    body = replaceVariables(body);
    window.open("mailto:" + toEmail + "?subject=" + encodeURIComponent(subject) + "&body=" + encodeURIComponent(body));
};



/// <summary>
/// </summary>
Machinata.Reporting.Tools.shareNodeByCopyingLink = function (instance, infosJSON, nodeJSON) {
    
    var diag = Machinata.messageDialog(Machinata.Reporting.Text.translate(instance, "reporting.nodes.share"), Machinata.Reporting.Text.translate(instance, "reporting.nodes.share.description"));
    diag.elem.append($("<p class='node-link' style='font-size:0.8em;word-break:break-all;'></p>").text(infosJSON.nodeURL));
    diag.button(Machinata.Reporting.Text.translate(instance, "reporting.nodes.share.copy"), "copy", function () {
        Machinata.Reporting.Tools.copyElementToClipboard(diag.elem.find(".node-link"), Machinata.Reporting.Text.translate(instance, 'reporting.nodes.share.copied'), Machinata.Reporting.Text.translate(instance, 'reporting.nodes.share.copied.text'));
        diag.close();
    },false,false);
    diag.okayButton();
    diag.show();

};


/// <summary>
/// </summary>
Machinata.Reporting.Tools.stringifyReportJSONWithoutCircularReferences = function (json) {
    var seen = new WeakSet();
    function getCircularReplacer(key, value) {
        if (key == "dataSource") return;
        if (key == "initConfig") return;
        if (key == "parent") return;
        if (key == "_domElem") return;
        if (key == "_state") return;
        if (typeof value === "object" && value !== null) {
            if (seen.has(value)) {
                return;
            }
            seen.add(value);
        }
        return value;
    }
    return JSON.stringify(json, getCircularReplacer, 2);
};


/// <summary>
/// </summary>
Machinata.Reporting.Tools.getCalibrationJSONForFont = function (config, font) {

    // Relevant string measurement code in Vega:
    // https://github.com/vega/vega/blob/master/packages/vega-scenegraph/src/util/canvas/context.js
    // https://github.com/vega/vega/blob/master/packages/vega-canvas/index.js
    // https://github.com/vega/vega/tree/master/packages/vega-scenegraph/src/util
    // https://github.com/vega/vega/blob/master/packages/vega-scenegraph/src/util/text.js#L133
    // See also
    // https://github.com/vega/vega/issues/2396

    console.log("Machinata.Reporting.Tools.getCalibrationJSONForFont:");

    var fontString = "9px " + font;
    var measureString = "abcdefghijklmnopqstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()";
    //measureString = "14,000,000";
    console.log("  "+"fontString: "+fontString);
    console.log("  " + "measureString: " + measureString);

    try {

        var canvas = Machinata.Imaging.createDrawingCanvas(1, 1);

        
        console.log(canvas);
        var context = canvas.getContext('2d');
        console.log(context);
        context.font = fontString;
        var measure = context.measureText(measureString);
        console.log("  " + "measured dims are:", measure);




    } catch (error) {
        console.error(error);
    }
};

/// <summary>
/// 
/// </summary>
Machinata.Reporting.Tools.showInfoDialog = function (instance, config, infoJSON) {
    // Create and show dialog
    var title = Machinata.Reporting.Text.resolve(instance, infoJSON.title);
    var text = Machinata.Reporting.Text.resolve(instance, infoJSON.text);
    var diag = Machinata.messageDialog(title, text);
    diag.setDesiredWidth(config.infoDialogDefaultDesiredWidth);
    if (infoJSON.wideDialog == true) {
        diag.setDesiredWidth(config.infoDialogWideDesiredWidth);
    }
    diag.setScrollable();
    diag.show();
    // Add some additional functionality...
    // Fullscreen images
    var zoomableImages = diag.elem.find("img");
    zoomableImages.css("cursor", "zoom-in");
    zoomableImages.click(function () {
        Machinata.Reporting.Tools.showImageDialog(instance, config, $(this).attr("src"));
    });
};



/// <summary>
/// 
/// </summary>
Machinata.Reporting.Tools.showImageDialog = function (instance, config, imgSrc) {
    var diag = Machinata.fullwindowDialog("");
    diag.elem.find(".dialog-contents").css("cursor","zoom-out");
    diag.elem.find(".dialog-contents").css("background-size","contain");
    diag.elem.find(".dialog-contents").css("background-repeat","no-repeat");
    diag.elem.find(".dialog-contents").css("background-position","center center");
    diag.elem.find(".dialog-contents").css("background-image", "url(" + imgSrc + ")");
    diag.elem.find(".dialog-contents").click(function () {
        diag.close();
    });
    diag.show();
};


/// <summary>
/// 
/// </summary>
Machinata.Reporting.Tools.openNodeInStudio = function (instance, config, nodeJSON) {
    var diag = Machinata.fullwindowDialog("Node Studio", []);
    diag.button("Close", "close", null, true);
    diag.show();
    //alert(diag.elem.find(".ui-dialog-content").length);
    Machinata.Reporting.Studio.initForNode(diag.elem, nodeJSON);
};


/// <summary>
/// 
/// </summary>
Machinata.Reporting.Tools.debugMenuForNode = function (instance, config, nodeJSON, nodeElem) {
    var opts = {
        "studio": "Open in studio",
        "view-node-json": "View Node JSON",
        "view-runtime-json": "View Runtime JSON",
        "download-json": "Download Runtime JSON",
        "view-infos": "View Infos JSON",
        "download-infos": "Download Infos JSON",
    };
    var diag = Machinata.optionsDialog(Machinata.Reporting.Text.translate(instance, "reporting.nodes.debug"), Machinata.Reporting.Text.translate(instance, "reporting.nodes.debug.options"), opts);
    diag.okay(function (opt) {
        // Get implementation
        var impl = Machinata.Reporting.Node[nodeJSON.nodeType];
        var JSON_VIEW_ELEM_SOURCE = "<div style='font-size:0.8em; overflow-y:auto; max-height:600px; font-family:monospace; white-space:pre;'/>";
        if (opt == "studio") {
            var originalJSON = instance.idToNodeOriginalJSONLookupTable[nodeJSON.id];
            Machinata.Reporting.Tools.openNodeInStudio(instance, config, originalJSON);
        }
        if (opt == "view-infos") {
            var infos = Machinata.Reporting.Node.compileNodeInfos(instance, config, nodeJSON);
            var data = Machinata.Reporting.Tools.stringifyReportJSONWithoutCircularReferences(infos);
            console.log(data);
            var viewDiag = Machinata.messageDialog("Infos JSON", "");
            viewDiag.elem.find("p").append($(JSON_VIEW_ELEM_SOURCE).text(data));
            viewDiag.show();
        }
        if (opt == "download-infos") {
            var infos = Machinata.Reporting.Node.compileNodeInfos(instance, config, nodeJSON);
            var data = Machinata.Reporting.Tools.stringifyReportJSONWithoutCircularReferences(infos);
            console.log(data);
            Machinata.Data.exportDataAsBlob(btoa(data), "text/json", nodeJSON.id + ".infos.json");
        }
        if (opt == "view-runtime-json") {
            var data = Machinata.Reporting.Tools.stringifyReportJSONWithoutCircularReferences(nodeJSON);
            console.log(data);
            var viewDiag = Machinata.messageDialog("Runtime JSON", "");
            viewDiag.elem.find("p").append($(JSON_VIEW_ELEM_SOURCE).text(data));
            viewDiag.show();
        }
        if (opt == "view-node-json") {
            var originalJSON = instance.idToNodeOriginalJSONLookupTable[nodeJSON.id];
            var data = Machinata.Reporting.Tools.stringifyReportJSONWithoutCircularReferences(originalJSON);
            console.log(data);
            var viewDiag = Machinata.messageDialog("Node JSON", "");
            viewDiag.elem.find("p").append($(JSON_VIEW_ELEM_SOURCE).text(data));
            viewDiag.show();
        }
        if (opt == "download-json") {
            var originalJSON = instance.idToNodeOriginalJSONLookupTable[nodeJSON.id];
            var data = Machinata.Reporting.Tools.stringifyReportJSONWithoutCircularReferences(nodeJSON);
            Machinata.Data.exportDataAsBlob(btoa(data), "text/json", nodeJSON.id+".runtime.json");
        }
    });
    diag.show();
};



/// <summary>
/// 
/// </summary>
Machinata.Reporting.Tools.applyMachinataBundleThemeToDefaultConfiguration = function (themeJSON) {
    // Init
    var themeJSONKeys = Object.keys(themeJSON);

    // Web / default (from theme)
    Machinata.Reporting.Config.assetsPath = themeJSON["assets-path"];
    Machinata.Reporting.Config.iconsBundle = themeJSON["icons-bundle"];

    Machinata.Reporting.Config.padding = themeJSON["padding"];

    Machinata.Reporting.Config.lineSize = themeJSON["line-size"];
    Machinata.Reporting.Config.graphLineSize = themeJSON["graph-line-size"];
    Machinata.Reporting.Config.graphSymbolSize = themeJSON["graph-symbol-size"];
    Machinata.Reporting.Config.domainLineSize = themeJSON["domain-line-size"];
    Machinata.Reporting.Config.verticalBarMaxSize = themeJSON["vertical-bar-max-size"];
    Machinata.Reporting.Config.horizontalBarMaxSize = themeJSON["horizontal-bar-max-size"];
    Machinata.Reporting.Config.textTruncationEllipsis = themeJSON["text-truncation-ellipsis"];

    Machinata.Reporting.Config.textFont = themeJSON["text-font"];
    Machinata.Reporting.Config.labelFont = themeJSON["label-font"];
    Machinata.Reporting.Config.axisFont = themeJSON["axis-font"];
    Machinata.Reporting.Config.headlineFont = themeJSON["headline-font"];
    Machinata.Reporting.Config.legendFont = themeJSON["legend-font"];
    Machinata.Reporting.Config.subtitleFont = themeJSON["subtitle-font"];
    Machinata.Reporting.Config.numberFont = themeJSON["number-font"];

    Machinata.Reporting.Config.textSize = themeJSON["text-size"];
    Machinata.Reporting.Config.lineHeight = themeJSON["line-height"];
    Machinata.Reporting.Config.subtitleSize = themeJSON["subtitle-size"];
    Machinata.Reporting.Config.chartTextSize = themeJSON["chart-text-size"];
    Machinata.Reporting.Config.labelSize = themeJSON["label-size"];
    Machinata.Reporting.Config.axisSize = themeJSON["axis-size"];
    Machinata.Reporting.Config.titleSize = themeJSON["h1-size"]; // same as h1
    Machinata.Reporting.Config.h1Size = themeJSON["h1-size"];
    Machinata.Reporting.Config.h2Size = themeJSON["h2-size"];
    Machinata.Reporting.Config.h3Size = themeJSON["h3-size"];
    Machinata.Reporting.Config.h4Size = themeJSON["h4-size"];

    Machinata.Reporting.Config.legendSize = themeJSON["legend-size"];
    Machinata.Reporting.Config.legendPadding = themeJSON["legend-padding"];
    Machinata.Reporting.Config.legendSymbolSize = themeJSON["legend-symbol-size"];
    Machinata.Reporting.Config.legendSymbolStroke = themeJSON["legend-symbol-stroke"];
    Machinata.Reporting.Config.legendColumnPadding = themeJSON["legend-column-padding"];
    Machinata.Reporting.Config.legendRowPadding = themeJSON["legend-row-padding"];
    Machinata.Reporting.Config.legendOffset = themeJSON["legend-offset"];
    Machinata.Reporting.Config.legendLabelOffsetY = themeJSON["legend-label-offset-y"];

    Machinata.Reporting.Config.gridColor = themeJSON["grid-color"];
    Machinata.Reporting.Config.solidColor = themeJSON["solid-color"];
    Machinata.Reporting.Config.darkColor = themeJSON["dark-color"];
    Machinata.Reporting.Config.pageColor = themeJSON["page-color"];
    Machinata.Reporting.Config.titleColor = themeJSON["title-color"];
    Machinata.Reporting.Config.highlightColor = themeJSON["highlight-color"];
    Machinata.Reporting.Config.domainLineColor = themeJSON["domain-line-color"];

    Machinata.Reporting.Config.chartBackgroundColorSolidChrome = themeJSON["chrome-solid-bg-color"];
    Machinata.Reporting.Config.chartBackgroundColorDarkChrome = themeJSON["chrome-dark-bg-color"];
    Machinata.Reporting.Config.chartBackgroundColorLightChrome = themeJSON["chrome-light-bg-color"];

    Machinata.Reporting.Config.mediumContentWidthSize = themeJSON["medium-content-width-size"];
    Machinata.Reporting.Config.fontSizeToCharacterWidthRatio = themeJSON["font-size-to-character-width-ratio"];

    Machinata.Reporting.Config.axisBottomShowTicks = themeJSON["axis-bottom-show-ticks"];
    Machinata.Reporting.Config.axisBottomTickSize = themeJSON["axis-bottom-tick-size"];

    Machinata.Reporting.Config.layoutMinWidth = themeJSON["reporting-layout-min-width"];
    Machinata.Reporting.Config.layoutMaxWidth = themeJSON["reporting-layout-max-width"];
    Machinata.Reporting.Config.nodeSizeMinWidthUncompressed = themeJSON["node-size-min-width-uncompressed"];

    Machinata.Reporting.Config.mobileTitleSizeFactor = themeJSON["mobile-title-size-factor"];
    Machinata.Reporting.Config.mobileSubtitleSizeFactor = themeJSON["mobile-subtitle-size-factor"];
    Machinata.Reporting.Config.mobileLabelSizeFactor = themeJSON["mobile-label-size-factor"];

    // Webprint profile
    Machinata.Reporting.Profiles.Configs["webprint"].layoutMinWidth = themeJSON["reporting-layout-min-width-webprint"];
    Machinata.Reporting.Profiles.Configs["webprint"].layoutMaxWidth = themeJSON["reporting-layout-max-width-webprint"];


    // Print profile (from theme)

    Machinata.Reporting.Profiles.Configs["print"].textFont = themeJSON["text-font-print"];
    Machinata.Reporting.Profiles.Configs["print"].labelFont = themeJSON["label-font-print"];
    Machinata.Reporting.Profiles.Configs["print"].axisFont = themeJSON["axis-font-print"];
    Machinata.Reporting.Profiles.Configs["print"].headlineFont = themeJSON["headline-font-print"];
    Machinata.Reporting.Profiles.Configs["print"].legendFont = themeJSON["legend-font-print"];
    Machinata.Reporting.Profiles.Configs["print"].subtitleFont = themeJSON["subtitle-font-print"];
    Machinata.Reporting.Profiles.Configs["print"].numberFont = themeJSON["number-font-print"];

    Machinata.Reporting.Profiles.Configs["print"].textSize = themeJSON["text-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].lineHeight = themeJSON["line-height-print"];
    Machinata.Reporting.Profiles.Configs["print"].subtitleSize = themeJSON["subtitle-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].chartTextSize = themeJSON["chart-text-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].labelSize = themeJSON["label-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].axisSize = themeJSON["axis-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].titleSize = themeJSON["h1-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].h1Size = themeJSON["h1-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].h2Size = themeJSON["h2-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].h3Size = themeJSON["h3-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].h4Size = themeJSON["h4-size-print"];

    Machinata.Reporting.Profiles.Configs["print"].padding = themeJSON["padding-print"];
    Machinata.Reporting.Profiles.Configs["print"].fontSizeToCharacterWidthRatio = themeJSON["font-size-to-character-width-ratio-print"];
    Machinata.Reporting.Profiles.Configs["print"].lineSize = themeJSON["line-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].gridLineSize = themeJSON["grid-line-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].graphLineSize = themeJSON["graph-line-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].domainLineSize = themeJSON["domain-line-size-print"];
    

    Machinata.Reporting.Profiles.Configs["print"].axisLabelSeparation = themeJSON["padding-print"];
    Machinata.Reporting.Profiles.Configs["print"].axisLabelPadding = themeJSON["padding-print"] / 2;
    Machinata.Reporting.Profiles.Configs["print"].axisTargetTickCount = 6;
    Machinata.Reporting.Profiles.Configs["print"].verticalBarMaxSize = themeJSON["vertical-bar-max-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].horizontalBarMaxSize = themeJSON["horizontal-bar-max-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].graphSymbolSize = themeJSON["graph-symbol-size-print"];

    Machinata.Reporting.Profiles.Configs["print"].legendSize = themeJSON["legend-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].legendFont = themeJSON["legend-font-print"];
    Machinata.Reporting.Profiles.Configs["print"].legendPadding = themeJSON["legend-padding-print"];
    Machinata.Reporting.Profiles.Configs["print"].legendSymbolSize = themeJSON["legend-symbol-size-print"];
    Machinata.Reporting.Profiles.Configs["print"].legendSymbolStroke = themeJSON["legend-symbol-stroke-print"];
    Machinata.Reporting.Profiles.Configs["print"].legendColumnPadding = themeJSON["legend-column-padding-print"];
    Machinata.Reporting.Profiles.Configs["print"].legendRowPadding = themeJSON["legend-row-padding-print"];
    Machinata.Reporting.Profiles.Configs["print"].legendOffset = themeJSON["legend-offset-print"];
    Machinata.Reporting.Profiles.Configs["print"].legendLabelOffsetY = themeJSON["legend-label-offset-y-print"];

    Machinata.Reporting.Profiles.Configs["print"].gridColor = themeJSON["grid-color-print"];
    Machinata.Reporting.Profiles.Configs["print"].solidColor = themeJSON["solid-color-print"];
    Machinata.Reporting.Profiles.Configs["print"].darkColor = themeJSON["dark-color-print"];
    Machinata.Reporting.Profiles.Configs["print"].pageColor = themeJSON["page-color-print"];
    Machinata.Reporting.Profiles.Configs["print"].titleColor = themeJSON["title-color-print"];
    Machinata.Reporting.Profiles.Configs["print"].highlightColor = themeJSON["highlight-color-print"];
    Machinata.Reporting.Profiles.Configs["print"].domainLineColor = themeJSON["domain-line-color-print"];

    Machinata.Reporting.Profiles.Configs["print"].axisBottomShowTicks = themeJSON["axis-bottom-show-ticks-print"];
    Machinata.Reporting.Profiles.Configs["print"].axisBottomTickSize = themeJSON["axis-bottom-tick-size-print"];

    // Print (generic customizations)
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["LineColumnNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["LineColumnNode"].barMaxSize = themeJSON["vertical-bar-max-size-print"];
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["ScatterPlotNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["ScatterPlotNode"]["scatterPlotSymbolSize"] = themeJSON["graph-symbol-size-print"];
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["CoreSatelliteNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["CoreSatelliteNode"].planetLabelLimitLines = 4;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["CoreSatelliteNode"].planetLabelLimitChars = 8;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["CoreSatelliteNode"].planetLabelAddSpaceBeforeValue = false;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["CoreSatelliteNode"].satelliteLabelLimitLines = 3;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["CoreSatelliteNode"].satelliteLabelLimitChars = 20;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["DonutNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["DonutNode"]["sliceLabelsSize"] = themeJSON["legend-size-print"];
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["DonutNode"]["legendPositionPortrait"] = "bottom-left";
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["DonutNode"]["legendPositionLandscape"] = "left-top";
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["PieNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["PieNode"]["sliceLabelsSize"] = themeJSON["legend-size-print"];
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["PieNode"]["legendPositionPortrait"] = "bottom-left";
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["PieNode"]["legendPositionLandscape"] = "left-top";
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["GaugeNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["GaugeNode"]["statusArcLineSize"] = 3;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["MultiGaugeNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["MultiGaugeNode"]["statusArcLineSize"] = 3;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["VBarNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["VBarNode"].barMaxSize = themeJSON["vertical-bar-max-size-print"];
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["VBarNode"].barLabelsSize = Machinata.Reporting.Profiles.Configs["print"].chartTextSize;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["HBarNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["HBarNode"].barMaxSize = themeJSON["horizontal-bar-max-size-print"];
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["HBarNode"].fillChartSpace = false;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["HBarNode"].autoSnapXAxisTickCount = 3;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["HBarNode"].barLabelsSize = Machinata.Reporting.Profiles.Configs["print"].chartTextSize;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["SymboledVBarNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["SymboledVBarNode"].barMaxSize = themeJSON["vertical-bar-max-size-print"] * 2;
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["SymboledVBarNode"].symbolFactSize = themeJSON["graph-symbol-size-print"];
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["OfflineMapNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["RadarNode"] = {};
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["RadarNode"].gridColorShade = "gray4";
    Machinata.Reporting.Profiles.Configs["print"]["nodeDefaults"]["RadarNode"].radarValueLabelSize = 0.8;

    // Register default theme shade names
    function registerShades(name, shades) {
        if (shades == null || shades == "") console.warn("Machinata.Reporting.Tools.applyMachinataBundleThemeToDefaultConfiguration: The shades '" + name + "' do not have a valid series of shade names (null)!");
        try {
            Machinata.Reporting.Config[name] = shades.split(",");
        } catch (e) {
            console.warn("Machinata.Reporting.Tools.applyMachinataBundleThemeToDefaultConfiguration: The shades '" + name + "' do not have a valid series of shade names (" + shades + "): ", e);
        }
    }

    // Register default chart themes
    function registerTheme(name, colorsFG, colorsBG, colorNames) {
        if (colorsFG == null || colorsFG == "") console.warn("Machinata.Reporting.Tools.applyMachinataBundleThemeToDefaultConfiguration: The theme '" + name + "' does not have a valid series of fg colors (null)!");
        if (colorsBG == null || colorsBG == "") console.warn("Machinata.Reporting.Tools.applyMachinataBundleThemeToDefaultConfiguration: The theme '" + name + "' does not have a valid series of bg colors (null)!");
        try {
            colorsFG = colorsFG.split(",");
        } catch (e) {
            console.warn("Machinata.Reporting.Tools.applyMachinataBundleThemeToDefaultConfiguration: The theme '" + name + "' does not have a valid series of colors fg (" + colorsFG + "): ", e);
        }
        try {
            colorsBG = colorsBG.split(",");
        } catch (e) {
            console.warn("Machinata.Reporting.Tools.applyMachinataBundleThemeToDefaultConfiguration: The theme '" + name + "' does not have a valid series of colors bg (" + colorsBG + "): ", e);
        }
        if (colorNames != null) {
            try {
                colorNames = colorNames.split(",");
            } catch (e) {
                console.warn("Machinata.Reporting.Tools.applyMachinataBundleThemeToDefaultConfiguration: The theme '" + name + "' does not have a valid series of colors shades (" + colorNames + "): ", e);
            }
        }
        Machinata.Reporting.registerTheme(name, colorsFG, colorsBG, colorNames);
    }
    for (var i = 0; i < themeJSONKeys.length; i++) {
        var key = themeJSONKeys[i];
        if (Machinata.String.startsWith(key, "theme-") && Machinata.String.endsWith(key, "-bg")) {
            var themeName = key.replace("theme-", "").replace("-bg", "")
            if (themeJSON[key] != null && themeJSON[key] != "") {
                registerTheme(
                    themeName,
                    themeJSON["theme-" + themeName + "-bg"],
                    themeJSON["theme-" + themeName + "-fg"],
                    themeJSON["theme-" + themeName + "-shades"]
                );
            }
        }
    }
}



/// <summary>
/// Returns a JSON object with various performance statistics which may prove useful for profiling.
/// This method can be called at anytime, however it only gives a proper image of the performance when called after
/// the report has fully loaded.
/// All times are in millisecond (ms).
/// The returned object looks like the following:
/// ```
/// {
///     "timers": [
///         {
///             "timer": "report-init",
///             "start": 0,
///             "end": 8,
///             "time": 8,
///             "summary": "report-init: 0ms...8ms (8ms)"
///         },
///         {
///             "timer": "catalog-download",
///             "start": 8,
///             "end": 9,
///             "time": 1,
///             "summary": "catalog-download: 8ms...9ms (1ms)"
///         },
///         {
///             "timer": "report-download",
///             "start": 9,
///             "end": 45,
///             "time": 36,
///             "summary": "report-download: 9ms...45ms (36ms)"
///         },
///         {
///             "timer": "report-preinit",
///             "start": 45,
///             "end": 45,
///             "time": 0,
///             "summary": "report-preinit: 45ms...45ms (0ms)"
///         },
///         {
///             "timer": "report-buildindex",
///             "start": 45,
///             "end": 46,
///             "time": 1,
///             "summary": "report-buildindex: 45ms...46ms (1ms)"
///         },
///         {
///             "timer": "report-buildcontents",
///             "start": 48,
///             "end": 130,
///             "time": 82,
///             "summary": "report-buildcontents: 48ms...130ms (82ms)"
///         },
///         {
///             "timer": "report-firstdraw",
///             "start": 94,
///             "end": 130,
///             "time": 36,
///             "summary": "report-firstdraw: 94ms...130ms (36ms)"
///         },
///         {
///             "timer": "report-postinit",
///             "start": 130,
///             "end": 130,
///             "time": 0,
///             "summary": "report-postinit: 130ms...130ms (0ms)"
///         },
///         {
///             "timer": "total",
///             "start": 0,
///             "end": 164,
///             "time": 164,
///             "summary": "total: 0ms...164ms (164ms)"
///         }
///     ]
/// }
/// ```
/// </summary>
Machinata.Reporting.Tools.getPerformanceStats = function () {
    // Init
    var data = {
        timers: []
    };
    var totalEllapsed = 0;
    Machinata.Debugging.compileTimerStats();
    // Loop all timers and build overview
    $.each(Machinata.Debugging._timers, function (key, timer) {
        data.timers.push({
            timer: key,
            start: timer.rstart,
            end: timer.rend,
            time: timer.ellapsed,
            summary: key+": "+ timer.rstart + "ms..." + timer.rend + "ms (" + timer.ellapsed+"ms)"
        });
        totalEllapsed += timer.ellapsed;
    });
    data.timers.push({
        timer: "total",
        start: 0,
        end: totalEllapsed,
        time: totalEllapsed,
        summary: "total: 0ms..." + totalEllapsed + "ms (" + totalEllapsed + "ms)"
    });
    // Return
    return data;
};

/// <summary>
/// 
/// </summary>
Machinata.Reporting.Tools.trackNodeEvent = function (instance, config, nodeJSON, action, val, additionalInfos) {
    if (Machinata.Reporting.Handler.trackEvent != null) {
        setTimeout(function () {
            var infosJSON = Machinata.Reporting.Node.compileNodeInfos(instance, config, nodeJSON);
            Machinata.Reporting.Handler.trackEvent(instance, infosJSON, nodeJSON, Machinata.Reporting.TRACKING_CATEGORY_NODE, action, val, additionalInfos);
        }, 10); 
    }
};