

/* ======== Machinata UI-Kit Reporting  ======== */


/// <summary>
/// This package is part of the Machinata Core JS library.
/// See https://nerves.ch/documentation/machinata-core-bundle.js
/// </summary>
/// <namespace></namespace>
/// <name>Machinata</name>
/// <type>namespace</type>
if (typeof Machinata === "undefined") var Machinata = ((typeof global !== 'undefined') ? global : window).MACHINATA;

/// <summary>
/// This package is part of the Machinata Core JS UIKit library.
/// </summary>
/// <namespace>Machinata</namespace>
/// <name>Machinata.UIKit</name>
/// <type>namespace</type>
if (Machinata.UIKit == null) Machinata.UIKit = {};

/// <summary>
/// </summary>
/// <namespace>Machinata.UIKit</namespace>
/// <name>Machinata.UIKit.Reporting</name>
/// <type>namespace</type>
Machinata.UIKit.Reporting = {};

/// <summary>
/// Registers the uikit branding configuration with the reporting system...
/// </summary>
/// <hidden/>
Machinata.Reporting.Branding.registerBrandingConfiguration("uikit", function () {

    // Apply the Machinata theme to the configuration
    // The Machinata themes are available to all bundle contexts and thus the main bulk of the styling configuration
    // comes from these bundle themes.
    Machinata.Reporting.Tools.applyMachinataBundleThemeToDefaultConfiguration({theme.json});

    // Set the system maps api key
    if (Machinata.Reporting.Node.OnlineMapNode != null) Machinata.Reporting.Node.OnlineMapNode.defaults.tileLayerAPIKey = "{config.maps-access-token}";

    // Apply our custom configuration values that are not included in the theme
    Machinata.Reporting.Config.configSource = "machinata-uikit-reporting.js";

});

/// <summary>
/// Helper method to get the regular configuration (used for full reports)
/// </summary>
Machinata.UIKit.Reporting.getRegularConfiguration = function () {
    var reportingConfig = {
        "dataProvider": "Memory",
        "brandingConfiguration": "uikit",
        "numberFormatLocaleDefinition": {
            "thousands": "'",
            "decimal": ".",
            "grouping": [3],
            "currency": ["$", ""]
        },
    };
    return reportingConfig;
};

/// <summary>
/// Helper method to get the containerless configuration (used for single charts)
/// </summary>
Machinata.UIKit.Reporting.getContainerlessConfiguration = function () {
    if (Machinata.UIKit.Reporting._currentContainerlessConfiguration == null) {
        var reportingConfig = {
            "dataProvider": "Memory",
            "brandingConfiguration": "uikit"
        };
        Machinata.UIKit.Reporting._currentContainerlessConfiguration = Machinata.Reporting.Containerless.getContainerlessConfigForProfile("web", reportingConfig);
    }
    return Machinata.UIKit.Reporting._currentContainerlessConfiguration;
};

/// <summary>
/// Changes the chart date range (start and end). If no start/end is provided then the user is asked via a dialog.
/// </summary>
Machinata.UIKit.Reporting.changeChartDates = function (chartElem, start, end) {
    if (chartElem == null) throw "You must pass a jQuery element to changeChartDates";
    if (chartElem.length != 1) throw "You must pass a single jQuery element to changeChartDates";

    if (start == null || end == null) {
        // Ask user...

        // See if we can get a existing start/end
        var currentDateString = null;
        var existingStart = Machinata.queryParameterFromURL("start", chartElem.attr("data-api-call"));
        var existingEnd = Machinata.queryParameterFromURL("end", chartElem.attr("data-api-call"));
        if (existingStart != null && existingStart != "" && existingEnd != null && existingEnd != "") {
            currentDateString = existingStart + " - " + existingEnd;
        }
        // Show dialog
       // var diag = Machinata.dateDialog(Machinata.Reporting.Text.translate(instance, 'reporting.change-date'), null, null, currentDateString, null, null, true);
        var diag = Machinata.dateDialog("{text.change-date}", null, null, currentDateString, null, null, true);
        diag.input(function (val,id) {
            if (id == "clear") {
                Machinata.UIKit.Reporting._changeChartDates(chartElem, "", "");
            } else {
                var strStart = Machinata.formattedDateFromDate(val.start);
                var strEnd = Machinata.formattedDateFromDate(val.end);
                Machinata.UIKit.Reporting._changeChartDates(chartElem, strStart, strEnd);
            }
        });
        diag.show();
    } else {
        // Just update
        Machinata.UIKit.Reporting._changeChartDates(chartElem, start, end);
    }
};

/// <summary>
/// </summary>
/// <hidden/>
Machinata.UIKit.Reporting._changeChartDates = function (chartElem, start, end) {
    // Alter api call
    var newAPICall = chartElem.attr("data-api-call");
    newAPICall = Machinata.updateQueryString("start", start, newAPICall);
    newAPICall = Machinata.updateQueryString("end", end, newAPICall);

    // Rebuild
    Machinata.UIKit.Reporting.buildChartViaAPICall(chartElem, newAPICall);
};

/// <summary>
/// Updates the chart using its api-call
/// </summary>
Machinata.UIKit.Reporting.updateChart = function (chartElem) {

    // api call
    var newAPICall = chartElem.attr("data-api-call");

    // Rebuild
    Machinata.UIKit.Reporting.buildChartViaAPICall(chartElem, newAPICall);
};

/// <summary>
/// </summary>
/// <deprecated/>
Machinata.UIKit.Reporting.UpdateChart = Machinata.UIKit.Reporting.updateChart; // backwards compatibility

/// <summary>
/// </summary>
Machinata.UIKit.Reporting.buildChartViaAPICall = function (chartElem, apiCall) {
    // Init
    if (apiCall == null) apiCall = chartElem.attr("data-api-call");
    else chartElem.attr("data-api-call", apiCall);


    // Create and send api call
    chartElem.addClass("loading");
    var call = Machinata.apiCall(apiCall);
    call.success(function (message) {
        // Build with the contents of the message
        chartElem.removeClass("loading");
        Machinata.UIKit.Reporting.buildChart(chartElem, message.data);
    });
    call.genericError();
    call.send();
};

/// <summary>
/// </summary>
Machinata.UIKit.Reporting.buildReportViaAPICall = function (reportElem, apiCall) {
    // Init
    if (apiCall == null) apiCall = reportElem.attr("data-api-call");
    else reportElem.attr("data-api-call", apiCall);


    // Create and send api call
    reportElem.addClass("loading");
    var call = Machinata.apiCall(apiCall);
    call.success(function (message) {
        // Build with the contents of the message
        reportElem.removeClass("loading");
        Machinata.UIKit.Reporting.buildReport(reportElem, message.data);
    });
    call.genericError();
    call.send();
};

/// <summary>
/// </summary>
Machinata.UIKit.Reporting.buildChart = function (chartElem, nodeJSON) {

    // Already exists?
    var existingNodeElem = chartElem.data("reporting-node-elem");
    if (existingNodeElem != null) {
        Machinata.Reporting.Containerless.cleanupNode(existingNodeElem);
        existingNodeElem.remove();
    }

    // Manipule node json
    nodeJSON.supportsToolbar = false;
    if (nodeJSON.chrome == null) nodeJSON.chrome = "light"; // make sure the default implemntation chrome is not used

    // Call the Containerless buildNode helper function to initialize the node and all its UI
    // Note: this will not actually redraw the node, you have full control over when you want to
    // draw the nodes
    var newNodeElem = Machinata.Reporting.Containerless.buildNode(Machinata.UIKit.Reporting.getContainerlessConfiguration(), nodeJSON, chartElem);
    chartElem.data("reporting-node-elem", newNodeElem);

    // Initial draw
    // Whenever you want the node to redraw itself, you can call this method:
    Machinata.Reporting.Containerless.redrawNode(newNodeElem);


};

/// <summary>
/// </summary>
Machinata.UIKit.Reporting.buildReport = function (reportElem, reportJSON) {
    // Create config
    var reportingConfig = Machinata.UIKit.Reporting.getRegularConfiguration();
    reportingConfig.dataSource = reportJSON;
    reportingConfig.contentSelector = "#" + reportElem.attr("id");
    reportingConfig.chapters = "*";
    if (Machinata.queryParameter("print") == "true") {
        reportingConfig.profile = Machinata.Reporting.Profiles.PROFILE_WEBPRINT_KEY;
    }
    // Init report
    var reportingInstance = Machinata.Reporting.init(reportingConfig, function (instance, config) {
        // Report loaded successfully callback
    });
};

/// <summary>
/// </summary>
Machinata.ready(function () {

    // Responsiveness: bind redraw on the window resize
    $(window).resize(function () {
        $(".ui-reporting-chart .machinata-reporting-node").each(function () {
            Machinata.Reporting.Containerless.redrawNode($(this));
        });
    });

    // Find all reporting nodes that want to automatically be populated using api calls...
    $(".ui-reporting-chart").each(function () {
        // Init
        var chartElem = $(this);
        var chartID = chartElem.attr("id");
        if (String.isEmpty(chartID)) {
            chartID = "reporting_chart_" + Machinata.guid();
            chartElem.attr("id", chartID);
        }
        // Build chart
        Machinata.UIKit.Reporting.buildChartViaAPICall(chartElem);
    });

    // Find all reporting reports that want to automatically be populated using api calls...
    $(".ui-reporting-report").each(function () {
        // Init
        var reportElem = $(this);
        var reportID = reportElem.attr("id");
        if (String.isEmpty(reportID)) {
            reportID = "reporting_report_" + Machinata.guid();
            reportElem.attr("id", reportID);
        }
        // Build report
        Machinata.UIKit.Reporting.buildReportViaAPICall(reportElem);
    });
});

