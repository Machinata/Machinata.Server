using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Module.Finance.Model;
using System.Web;
using Machinata.Core.Exceptions;
using System.Security.Cryptography;
using System.Globalization;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Stripe;
using Machinata.Core.Localization;

namespace Machinata.Module.Finance.Payment.Providers.CreditCardProviders {
    public class StripePaymentProvider : CreditCardPaymentProvider {
        public override string Name
        {
            get
            {
                return "Stripe";
            }
        }

        /// <summary>
        /// Checks amount and signage from datatrans, creates new Payment
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        /// <exception cref="BackendException">payment-error;Missing information in request</exception>
        /// <exception cref="PaymentException">
        /// Payment wasn't authorized by the provider:  + errorMessage + , detail:  + errorDetail
        /// or
        /// Wrong signature
        /// See:
        /// https://github.com/segphault/stripe-checkout-aspnet/blob/master/Controllers/HomeController.cs
        /// </exception>
        public override PaymentResult ConfirmPayment(ModelContext db, HttpContext context) {

            PaymentResult result = new PaymentResult();
            try {

                StripeConfiguration.SetApiKey(Config.PaymentProvider.Stripe.SecretKey);

                // Token is created using Checkout or Elements!
                // Get the payment token submitted by the form:
                var token = context.Request["stripeToken"];
                var currency = context.Request["currency"];
                var invoiceId = context.Request["refId"];
                var customerId = context.Request["customerId"];

                result.Invoice = db.Invoices().GetByPublicId(invoiceId);

                // Charge the user's card:
                var metaData = new Dictionary<string, string>();
                metaData["customerId"] = customerId;
                metaData["refId"] = invoiceId;

                var charges = new StripeChargeService();
                charges.ExpandBalanceTransaction = true;

                var charge = charges.Create(new StripeChargeCreateOptions {
                    Amount = result.Invoice.TotalCustomer.ToCents(),
                    Currency = currency,
                    Description = customerId + ": " + result.Invoice.PublicId,
                    Metadata = metaData,
                    SourceTokenOrExistingSourceId = token,

                }, new StripeRequestOptions { });

                // Fee
                var fee = new Price(0);
                if (charge.BalanceTransaction != null) {
                    fee.Currency = charge.BalanceTransaction.Currency;
                    fee.Value = charge.BalanceTransaction.Fee / 100.0m;
                }

                Model.Payment.CreateFullPaymentForInvoice(charge.Id, result.Invoice, this.Name, fee, customerId);

                result.Success = true;

            } catch (Exception e) {
                // Extract error codes
                var stripeException = e as StripeException;
                string code = "internal-error";
                string message = "Internal Error";
                var internalError = true;
                if (stripeException != null) {
                    code = stripeException.StripeError.Code;
                    message = stripeException.StripeError.Message;
                    internalError = false;
                }
                else
                {
                    try
                    {
                        Core.EmailLogger.SendMessageToAdminEmail("Internal Payment error", "Machinata.Module.Finance", context, null, e);
                    }
                    catch
                    {

                    }
                }
                result.ErrorCode = code;
                result.ErrorMessage = message;
                result.Exception = e;
                result.IsInternalError = internalError;
               
            }


            return result;

        }



        public override string GetPaymentLink(Invoice invoice) {
            return null;
        }

        public override string GetPaymentEmbedCode(Invoice invoice, string language) {
            invoice.Include(nameof(Invoice.BillingAddress));
            string locale = language;
            string action = "/payment/confirm/stripe/init?refId=" + invoice.PublicId;
            // See https://stripe.com/docs/checkout
            string code = @"<form action='{action}' class='stripe-payment-form' method='POST'>
                                <script src='https://checkout.stripe.com/checkout.js' class='stripe-button'
                                        data-key='{key}'
                                        data-amount='{amount}'
                                        data-name='{name}'
                                        data-description='{description}'
                                        data-image=''
                                        data-locale='{locale}'
                                        data-zip-code='false'
                                        data-currency='{currency}'
                                        data-email='{email}'>
                                 </script>
                                 <input name='refId' value='{order}' hidden='hidden'/>
                                 <input name='customerId' value='{email}' hidden='hidden'/>
                                 <input name='currency' value='{currency}' hidden='hidden'/>
                             </form>";
            code = code.Replace("{action}", action);
            code = code.Replace("{key}", Config.PaymentProvider.Stripe.PublicKey);
            code = code.Replace("{amount}", invoice.TotalCustomer.ToCents().ToString());
            code = code.Replace("{name}", Core.Config.ProjectName);
            code = code.Replace("{description}", "");
            code = code.Replace("{locale}", locale);
            code = code.Replace("{order}", invoice.PublicId);
            code = code.Replace("{email}", invoice.BillingAddress.Email);
            code = code.Replace("{currency}", invoice.TotalCustomer.Currency);
            return code;
        }

        public override string GetExternalTransactionLink(Model.Payment payment) {
            if (payment != null) {
                //payment.PaymentProviderTransactionId
                if (Core.Config.IsTestEnvironment) {
                    return $"https://dashboard.stripe.com/test/payments/{payment.PaymentProviderTransactionId}";
                } else {
                    return $"https://dashboard.stripe.com/payments/{payment.PaymentProviderTransactionId}";
                }
            }
            return null;
        }


        public override bool CanRefundLive
        {
            get
            {
                return true;
                //return false;
            }
        }

        public override void Refund(Invoice invoice, Model.Payment payment, User actingUser) {

            // Access
            StripeConfiguration.SetApiKey(Config.PaymentProvider.Stripe.SecretKey);

            // Options
            var refundOptions = new StripeRefundCreateOptions() {
                // Amount = Rappen, if empty the whole charge will be refunded
                Reason = StripeRefundReasons.RequestedByCustomer
            };

            // Service
            var refundService = new StripeRefundService();

            // Refund
            StripeRefund refund = refundService.Create(payment.PaymentProviderTransactionId, refundOptions);

            // Payment
            var refundPayment = new Model.Payment();
            refundPayment.PaymentProvider = payment.PaymentProvider;
            refundPayment.Amount = -payment.Amount;
            refundPayment.Paid = DateTime.UtcNow;
            refundPayment.PaymentProviderTransactionId = refund.ChargeId;
            refundPayment.PaymentProviderPayerId = payment.PaymentProviderPayerId;
            refundPayment.ProviderTransactionFee = new Price(0, payment.Amount.Currency); // todo should 

            invoice.AddRefundPayment(refundPayment, actingUser);
            
            // refund_application_fee connect only optional, default is false

            //Boolean indicating whether the application fee should be refunded when refunding this charge.If a full charge refund is given, the full application fee will be refunded.Otherwise, the application fee will be refunded in an amount proportional to the amount of the charge refunded.

            //An application fee can be refunded only by the application that created the charge. 
        }
    }
}
