
Machinata.Finance = {};

Machinata.Finance.createGroupDialog = function (invoiceId, successPathPrefix) {
    var options = ['{text.project}', '{text.contract}', '{text.discount}'];
    Machinata.optionsDialog('{text.add-group}', '{text.finance.choose-category}', options)
        .okay(function (result) {
            Machinata.apiCall("/api/admin/finance/invoices/invoice/" + invoiceId + "/groups/create", { category: options[result] })
                .success(function (response) {
                    if (successPathPrefix != null && successPathPrefix != "") {
                        Machinata.goToPage(successPathPrefix + response.data.group["public-id"] + "/edit");
                    }
                    else {
                        Machinata.reload();
                    }
                })
                .send();
        })
        .show();
}

Machinata.Finance.createDiscountDialog = function (invoiceId, successPathPrefix) {
    Machinata.numberDialog('{text.add-discount}', '{text.discount}', 0, "{text.amount.en=Amount}")

        .input(function (val, id, diag, button) {
            if (id == "okay") {
                Machinata.apiCall("/api/admin/finance/invoices/invoice/" + invoiceId + "/groups/create-discount", { amount: val })
                    .success(function (response) {
                        if (successPathPrefix != null && successPathPrefix != "") {
                            Machinata.goToPage(successPathPrefix + response.data.group["public-id"] + "/edit");
                        }
                        else {
                            Machinata.reload();
                        }


                    })
                    .send();
            }
        })
        .show();
}