using Machinata.Core.Model;
using Machinata.Core.TaskManager;
using Machinata.Module.Finance.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Finance.Tasks {
    public class InvoiceCreateHashTask : Core.TaskManager.Task {
        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            return ScheduledTaskConfig.GetDefault();
        }

        public override void Process() {

            var invoices = this.DB.Invoices().Where(i => i.Hash == null);
            Log($"Generating hashes for {invoices.Count()}");

            foreach (var invoice in invoices) {
             
                Log("Invoice:" + invoice.SerialId + "------------------------");
                try {
                    invoice.Hash = invoice.GetNewHash();
                    Log(invoice.Hash);
                }
                catch(Exception e) {
                    Log($"Error:\n {e.Message}" + e.StackTrace);
                }
            }

            // Save
            if (this.TestMode == false) {
                this.DB.SaveChanges();
            }

        }

        
    }
}
