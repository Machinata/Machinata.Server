using Machinata.Core.Builder;
using Machinata.Core.Messaging;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Finance;
using Machinata.Module.Finance.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machinata.Module.Finace.Tasks {

    public class DueInvoicesTask : Core.TaskManager.MonthlyTask {

        public const string USER_SETTINGS_KEY_SEND_DUE_INVOICES_EMAILS = "FinanceSendDueInvoicesEmail";
      //  public const int DAY_OF_MONTH_TO_SEND_EMAIL = 1;
             
        public override void Process() {

            var minDate = DateTime.UtcNow.AddYears(-20);
            var maxDate = DateTime.UtcNow.AddDays(-0);
           
            var now = DateTime.UtcNow;
         
            var users = this.DB.Users().Where(pu => pu.Enabled == true && pu.CreationSource == User.CreationSources.Admin).ToList();
            users = users.Where(u => u.Settings.Bool(USER_SETTINGS_KEY_SEND_DUE_INVOICES_EMAILS, false)).ToList();

            Log($"Found {users.Count()} contacts to send emails to...");

            var invoices = this.DB.Invoices()
                .Include(nameof(Invoice.Business))
                .Where(p => p.DueDate != null)
                .Where(p => p.InvoiceSent !=  null)
                .Where(p=>p.Status == Invoice.InvoiceStatus.Sent)
                .Where(p => p.DueDate < now);

            Log($"Found {invoices.Count()} due invoices...\n\n");


            

            foreach (var invoice in invoices.OrderBy(i=>i.Business.Name)) {
                Log($"{invoice.Business.Name}: {invoice.SerialId} - {invoice.Title}", false);
            }

            TaskLog taskLog = null;

            // Day to send?
            taskLog = GetTaskLog(now);

            var isDayToSend = now.Day == Config.FinanceDueInvoicesEmailDayToSend;

            // Send Email?
            if (TestMode == false && invoices.Any() && taskLog == null && isDayToSend) {
                // Contacts
                var emailList = users.Select(u => new EmailContact() { Name = u.Name, Address = u.Email });
                Log("Sending emails to:", false);

                // Email
                var form = new FormBuilder(Forms.EMPTY);
                form.Include(nameof(Invoice.BusinessName));
                form.Include(nameof(Invoice.Title));
                form.Include(nameof(Invoice.SerialId));
                form.Include(nameof(Invoice.TotalCustomer));
                form.Include(nameof(Invoice.DueDate));


                var email = new EmailTemplate(this.DB, "due-invoices");
                email.Subject = "Due Invoices " + now.ToString("MMMM yyyy");
                email.InsertEntityList(
                    variableName: "invoices",
                    entities: invoices,
                    form: form,
                    link: "{public-url}/admin/finance/invoices/invoice/{entity.public-id}"
                    );

                // Send
                foreach (var emailContact in emailList) {
                    Log($"{emailContact.Name}: {emailContact.Address}", false);
                    email.SendEmail(emailContact, MailingUnsubscription.Categories.Finance, "unbilled-costs");
                }

                // Mark as sent
                this.SetMonthlyTaskDone(now);

            } else {

                Log($"Not sending email. Day={now.Day}, Sent={taskLog?.Created}, TestMode={this.TestMode}", false);
            }

            if (this.TestMode == false) {
                this.DB.SaveChanges();
            }

            Log("\n\n", false);

        }

       
        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = false;
            config.Install = true;
            config.Interval = "5h";
            return config;
        }


    }


   




}
