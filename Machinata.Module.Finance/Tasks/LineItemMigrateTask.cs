using Machinata.Core.Model;
using Machinata.Core.TaskManager;
using Machinata.Module.Finance.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Finance.Tasks {
    public class LineItemMigrateTask : Core.TaskManager.Task {
        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            return ScheduledTaskConfig.GetDefault();
        }

        public override void Process() {

            var invoices = this.DB.Invoices()
                .Include(nameof(Invoice.LineItemGroups) + "." +(nameof(LineItemGroup.LineItems)));

            foreach (var invoice in invoices) {
                Log("");
                Log("Invoice:" + invoice.SerialId + "------------------------");
                try {
                    foreach (var lineItemGroup in invoice.LineItemGroups) {
                        Log("LineItemGroup:" + lineItemGroup.Title);

                        foreach (var lineItem in lineItemGroup.LineItems) {
                            Log("Migrating LineItem:");

                           
                            if (lineItem.CustomerVATRate != null) {
                                lineItem.CustomerVAT = lineItem.CustomerItemPrice * lineItem.CustomerVATRate;
                                Log("New CustomerVAT: " + lineItem.CustomerVAT);
                                lineItem.CustomerTotal = lineItem.CustomerSubtotal + lineItem.CustomerVAT * lineItem.Quantity;
                                Log("New CustomerTotal: " + lineItem.CustomerTotal);
                            } else {
                                // Shipping and discount line itmes
                                lineItem.CustomerVAT = new Price(0, invoice.Currency);
                                Log("New CustomerVAT: " + lineItem.CustomerVAT);
                                lineItem.CustomerTotal = lineItem.CustomerSubtotal;
                                Log("New CustomerTotal: " + lineItem.CustomerTotal);
                            }
                        }

                    }


                    // Only if invoices are incorrect ONLY ACTIVATE IF YOU WANT TO TOUCH THE INVOICES
                    var vat = invoice.LineItemGroups.Sum(g => g.CustomerVAT).Value;
                    if (invoice.VATCustomer.Value != vat) {
                        Log($"We have to update invoice because vat rounding is incorrect:{invoice.VATCustomer.Value} != {vat}");
                        invoice.UpdateInvoice();
                    }
                    CheckInvoiceValues(invoice);

                  
                    
                }
                catch(Exception e) {
                    Log($"Error:\n {e.Message}" + e.StackTrace);
                }
            }

            // Save
            if (this.TestMode == false) {
                this.DB.SaveChanges();
            }

        }

        private static void CheckInvoiceValues(Invoice invoice) {
            if (invoice.LineItemGroups.Any() == false) {
                return;
            }
            var subTotal = invoice.LineItemGroups.Sum(g => g.CustomerSubtotal).Value;
            if (invoice.SubtotalCustomer.Value != subTotal) {
                throw new Exception($"SubtotalCustomer incorrect! expected: {invoice.SubtotalCustomer.Value} new sum: {subTotal}");
            }
            var vat = invoice.LineItemGroups.Sum(g => g.CustomerVAT).Value;
            if (invoice.VATCustomer.Value != vat) {
                throw new Exception($"VATCustomer incorrect! expected: {invoice.VATCustomer.Value} new sum: {vat}");
            }
            var total = invoice.LineItemGroups.Sum(g => g.CustomerTotal).Value;
            if (total != invoice.TotalCustomer.Value.Value) {
                throw new Exception($"TotalCustomer incorrect! expected: {invoice.TotalCustomer.Value} new sum: {total}" );
            }

            if (invoice.TotalCustomer.Value - invoice.VATCustomer.Value != invoice.SubtotalCustomer.Value) {
                throw new Exception($"Invoice totals incorrect!: {invoice.TotalCustomer.Value} - {invoice.VATCustomer.Value} != {invoice.SubtotalCustomer}");
            }

        }
    }
}
