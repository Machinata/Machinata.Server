using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Module.Finance.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Finance.Extensions {
    public static class PdfHandlerExtensions {


        public static void InsertInvoicePdfVariables(this PDFPageTemplateHandler handler, Invoice entity) {

            // Refunded
            if (entity.Status == Invoice.InvoiceStatus.Refunded) {
                throw new BackendException("invoice-invalid", "This invoice in invalid because it was refunded.");
            }

            // Currency not from context but invoice
            handler.Currency = entity.Currency;

            entity.LoadFirstLevelNavigationReferences();
            entity.LoadFirstLevelObjectReferences();


            // QR Code or QR Bill
            if (Config.FinancePDFUseQRBill == true) {
                handler.Template.InsertTemplate("qr-code", "invoice.qr-bill");
            } else {
                handler.Template.InsertTemplate("qr-code", "invoice.scan-code");
            }

            // Summary
            var invoiceSummaryForm = new FormBuilder(Forms.Admin.INVOICE);
            if (entity.ShowTimeRanges == true) {
                invoiceSummaryForm.Include(nameof(LineItemGroup.TimeRange));
            }
            handler.Template.InsertInvoiceSummary("invoice.summary", entity, invoiceSummaryForm);

            // Line Items
            if (entity.ShowLineItems == true) {
                var form = new FormBuilder(Forms.Admin.PDF);

                if (string.IsNullOrWhiteSpace(Config.FinanceInvoiceCustomPDFLineItemForm) == false) {
                    form = new FormBuilder(Forms.Admin.PDFv2);
                }

                handler.Template.InsertInvoiceLineItems("invoice.line-items", entity, form);
            } else {
                handler.Template.InsertVariable("invoice.line-items", string.Empty);
            }

            // Category Overview
            if (entity.ShowCategoryOverview == true) {
                handler.Template.InsertCategoryOverview("invoice.category-overview", entity, new FormBuilder(Forms.Admin.PDF));
            } else {
                handler.Template.InsertVariable("invoice.category-overview", string.Empty);
            }




            // Total
            var totalForm = new FormBuilder(Forms.Admin.PDF);
            if (entity.ShowTimeRanges == true) {
                totalForm.Include(nameof(LineItemGroup.TimeRange));
            }
            handler.Template.InsertInvoiceTotal("invoice.total", entity, totalForm);


            // Content
            handler.Template.InsertContent(
                variableName: "invoice.cms-content",
                cmsPath: "/Misc/Invoice/PDF Text",
                throw404IfNotExists: false
            );
            if (entity.Description != null) {
                handler.Template.InsertContent(
                    variableName: "invoice.description-content",
                    node: entity.Description
                );
            } else {
                handler.Template.InsertVariable("invoice.description-content", "");
            }
            handler.Template.InsertInvoicePaymentDetails("invoice.payment-details", entity);

            // Title, needs break or not?
            if (!string.IsNullOrWhiteSpace(entity.Title)) {
                handler.Template.Data.Replace("{line-break}", "<br/>");
            } else {
                handler.Template.Data.Replace("{line-break}", string.Empty);
            }

            // Insert entity properties 
            handler.Template.InsertVariables("invoice", entity);
            handler.Template.InsertVariableXMLSafeWithLineBreaks("invoice.billing-address-breaked", GetBillingAddressBreaked(entity));
            handler.Template.InsertVariableXMLSafeWithLineBreaks("invoice.sender-address-breaked", entity.SenderAddress.ToString().Replace(", ", "\n"));
            handler.Template.InsertVariableXMLSafeWithLineBreaks("invoice.sender-address.city", entity.SenderAddress.City);



            // Set footer
            handler.FooterLeft = "{text.invoice} " + entity.SerialId;

            // Set filename
            handler.SetFileName(entity.GetPDFFileName());
        }


        /// <summary>
        /// Copies the address and replaces the billing name if availble
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private static string GetBillingAddressBreaked(Invoice entity) {

            var address = new Address();
         
            if (entity.BillingAddress == null) {
                throw new BackendException("no-billing-address","No BillingAddress defined");
            }
            entity.BillingAddress.CopyValuesTo(address, new FormBuilder(Forms.Admin.VIEW));
            if (string.IsNullOrEmpty(entity.BillingName) == false) {
                address.Name = entity.BillingName;
            }
            return address.ToString().Replace(", ", "\n");
        }
    }
}
