using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Module.Finance.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Machinata.Module.Finance.Model.Invoice;

namespace Machinata.Module.Finance.Extensions {
    public static class InvoiceExtensions {

        /// <summary>
        /// Is double past due date 2*(DueDate - SentDate)
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns></returns>
        public static bool IsOverdue(this Invoice invoice) {
            if (invoice.Status == InvoiceStatus.Sent && invoice.DueDate.HasValue && invoice.InvoiceSent.HasValue) {
                var dueDays = invoice.DueDate.Value - invoice.InvoiceSent.Value;
                var daysDue = DateTime.UtcNow - invoice.DueDate.Value;
                if (daysDue > dueDays + dueDays) {
                    return true;
                }
            }
            return false;
        }

        public static bool IsDueAndNotOverdue(this Invoice invoice) {
            if (invoice.Status == InvoiceStatus.Sent && invoice.DueDate.HasValue) {
             
                if (invoice.DueDate.Value < DateTime.UtcNow && !IsOverdue(invoice) ) {
                    return true;
                }
            }
            return false;
        }

    }
}
