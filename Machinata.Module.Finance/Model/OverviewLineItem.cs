using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Machinata.Module.Finance.Model {

    /// <summary>
    /// Helper to generate overview tables on the fly
    /// </summary>
    /// <seealso cref="Machinata.Core.Model.ModelObject" />
    public partial class OverviewLineItem : ModelObject {
        

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public OverviewLineItem() {
            
        }

        #endregion


        #region Public Data Store Properties //////////////////////////////////////////////////////

       
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EMAIL)]
        public string LineItem { get; set; }
             
               
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EMAIL)]
        public Price CustomerTotal { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [Decimal("0.##")]
        public decimal Quantity { get; set; }



        #endregion


    }

    public class GroupedLineItems {
        public string Category { get; set; }
        public IEnumerable<IGrouping<string, LineItem>> SubCategories { get; set; }
    }

}
