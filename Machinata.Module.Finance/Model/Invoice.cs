using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using Machinata.Core.Messaging.Interfaces;
using Machinata.Module.Finance.Payment;
using System.ComponentModel.DataAnnotations;
using Machinata.Core.Exceptions;
using System.Text;
using Machinata.Module.Finance.Extensions;

namespace Machinata.Module.Finance.Model {
       


    [Serializable()]
    [ModelClass]
    public partial class Invoice : ModelObject, IMessageSender, IInfoStatusModelObject {

        public enum InvoiceStatus : short {
            Created = 10,
            Sent = 20,
            Paid = 100,


            Refunded = 1100
        }

        public enum DueStatuses : short {
            None = 0,
            Payable = 10,
            Due = 20
          
        }

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Events ////////////////////////////////////////////////////////////////////////////

        // Invoice Status Change Event/Delegate
        public delegate void StatusChangedDelegate(InvoiceStatus status, Invoice invoice);
        public static event StatusChangedDelegate StatusChanged;

        // Invoice Deleted Event/Delegate
        public delegate void InvoiceDeletedDelegate(Invoice invoice);
        public static event InvoiceDeletedDelegate InvoiceDeleted;

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Invoice() {
            this.Payments = new List<Payment>();
            this.LineItemGroups = new List<LineItemGroup>();
            this.InvoiceHistories = new List<InvoiceHistory>();

            this.Hash = this.GetNewHash();

            // Initialize empty
            this.ExternalCosts = new Price();
        }

        #endregion

        #region Constants //////////////////////////////////////////////////////////////////////////

        public const string InvoiceShowLineItemsSettingsKey = "InvoiceShowLineItems";
        public const string InvoiceShowTimeRangesSettingsKey = "InvoiceShowTimeRanges";
        public const string InvoiceShowCategoryOverviewSettingsKey = "InvoiceShowCategoryOverview";

        #endregion


        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public string SerialId { get; set; }
        
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        public InvoiceStatus Status { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public DateTime InvoiceCreated { get { return Created; } }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.API.VIEW)]
        public DateTime? InvoiceSent { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.API.VIEW)]
        public DateTime? DueDate { get; set; }

        [FormBuilder(Forms.Admin.EDIT)]
        [NotMapped]
        public InvoiceStatus SetStatus { get; set; }

        [FormBuilder()]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public Price SubtotalCustomer { get; set; }
        
        [FormBuilder()]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public Price VATCustomer { get; set; }
        
        [FormBuilder()]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [FormBuilder(Forms.Frontend.VIEW)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public Price TotalCustomer { get; set; }

       

        [FormBuilder()]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public PaymentMethod PaymentMethod { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        [FormBuilder(Forms.API.VIEW)]
        [MaxLength(3)]
        public string Currency { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public ContentNode Description { get; set; }

        /// <summary>
        /// Internal notes
        /// </summary>
        /// <value>
        /// The notes.
        /// </value>
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public ContentNode Notes { get; set; }

        /// <summary>
        /// To override billing address name
        /// </summary>
        /// <value>
        /// The name of the billing.
        /// </value>
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        public string BillingName { get; set; }

        [Column]
        [DataType("addressbook")]
        public string BillingEmails { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        public string Title { get; set; }

        [Column]
        [Index(IsUnique = true)]
        [MaxLength(64)]
        [FormBuilder()]
        public string Hash { get; set; }

        [Column]
        public bool ShowLineItems { get; set; } = true;

        [Column]
        public bool ShowTimeRanges { get; set; }

        [Column]
        public bool ShowCategoryOverview { get; set; }

        [FormBuilder()]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.API.VIEW)]
        [Column]
        public Price ExternalCosts { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public bool BilledExternally { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.API.VIEW)]
        public DateTime? PaidDate { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        [FormBuilder]
        [NotMapped]
        public string BusinessName {
            get {
                return this.Business?.Name;
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        [NotMapped]
        public Price PaidAmount
        {
            get
            {
                if (Context != null) {
                    Include(nameof(Payments));
                }
                var pastPayments = this.Payments.Sum(p => p.Amount.Value);
               
                return new Price(pastPayments, this.Currency);
            }
        }
        [FormBuilder(Forms.Admin.VIEW)]
        [NotMapped]
        public Price MissingAmount
        {
            get
            {
                return TotalCustomer - PaidAmount;
            }
        }



   

        /// <summary>
        /*
0200
1
CH2231989000007611146
S
Krankenkasse fit&munter
Am Wasser
1
3000
Bern
CH







121.00
CHF
S
Sarah Beispiel
Mustergasse
1
3600
Thun
CH
QRR
000003701588132583136809972
Monatsprämie Juli 2020
EPD
//S1/10/10201409/11/200630/20/140.000-53/30/102673831/31/200630/32/7.7/33/7.7:9.30/40/0:30
eBill/B/sarah.beispiel @einfach-zahlen.ch*/
        ///      https://www.paymentstandards.ch/dam/downloads/ig-qr-bill-de.pdf
        ///      
        /// 
        /// </summary>

        [FormBuilder()]
        [NotMapped]
        public string QRBillCode {
            get {
                var ownerBusiness = this.Context.OwnerBusiness(false);
                ownerBusiness.Include(nameof(Business.DefaultBillingAddress));

                this.Include(nameof(this.BillingAddress));

                var code = new StringBuilder();
                code.AppendLine("SPC");
                code.AppendLine("0200");
                code.AppendLine("1");
                code.AppendLine(ownerBusiness?.Settings[Business.SETTING_KEY_IBAN]?.ToString().Replace(" ",""));
                code.AppendLine("K");

                var billingName = ownerBusiness?.DefaultBillingAddress?.Company;
                if (string.IsNullOrEmpty(billingName) == true) {
                    billingName = ownerBusiness?.DefaultBillingAddress?.Company;
                }

                code.AppendLine(billingName);
                //code.AppendLine(ownerBusiness?.DefaultBillingAddress?.Address1);
                //code.AppendLine(ownerBusiness?.DefaultBillingAddress?.Address2);
                //code.AppendLine(ownerBusiness?.DefaultBillingAddress?.ZIP);
                //code.AppendLine(ownerBusiness?.DefaultBillingAddress?.City);
                //code.AppendLine(ownerBusiness?.DefaultBillingAddress?.CountryCode?.ToUpper());
                code.AppendLine(ownerBusiness?.DefaultBillingAddress?.Address1);
                code.AppendLine(ownerBusiness?.DefaultBillingAddress?.ZipAndCity);
                code.AppendLine("");
                code.AppendLine("");
                code.AppendLine(ownerBusiness?.DefaultBillingAddress?.CountryCode?.ToUpper());

                code.AppendLine("");
                code.AppendLine("");
                code.AppendLine("");
                code.AppendLine("");
                code.AppendLine("");
                code.AppendLine("");
                code.AppendLine("");
                code.AppendLine(this.TotalCustomer.Value?.ToString("0.00"));
                code.AppendLine(this.TotalCustomer.Currency?.ToUpper());
                code.AppendLine("K");

                var payername = this.BillingAddress?.Company;
                if (string.IsNullOrEmpty(payername) == true) {
                    payername = this.BillingAddress?.Name;
                }

                code.AppendLine(payername);
                //code.AppendLine(this.BillingAddress?.Address1); 
                //code.AppendLine(this.BillingAddress?.Address2);
                //code.AppendLine(this.BillingAddress?.ZIP);
                //code.AppendLine(this.BillingAddress?.City);
                //code.AppendLine(this.BillingAddress?.CountryCode?.ToUpper());
                code.AppendLine(this.BillingAddress?.Address1);
                code.AppendLine(this.BillingAddress?.ZipAndCity);
                code.AppendLine("");
                code.AppendLine("");
                code.AppendLine(this.BillingAddress?.CountryCode?.ToUpper());

                code.AppendLine("NON"); // referenztyp
                code.AppendLine(""); // referenz
                code.AppendLine("Referenznummer: " + this.SerialId); // umstrukturierte meldung
                code.AppendLine("EPD"); // trailer
                code.AppendLine(""); //
                code.AppendLine("");
                var result = code.ToString();

                return result;

            }
        }


        [FormBuilder()]
        [NotMapped]
        public string QRBillCodeURISafe {
            get
            {
                return System.Web.HttpUtility.UrlEncode(this.QRBillCode);
            }
        }

        [FormBuilder()]
        [NotMapped]
        public IEnumerable<string> Warnings {
            get {

                var warnings = new List<string>();
                if (this.DueDate== null) {
                    warnings.Add("invoice.warning.no-due-date");
                }
                if (this.BilledExternally == true) {
                    warnings.Add("invoice.warning.billed-externally");
                }
                return warnings;
            }
        }

        [FormBuilder()]
        [NotMapped]
        public string WarningsText {
            get {

                if (this.Warnings.Any()) {
                    return "(" + string.Join(", ", this.Warnings.Select(w =>"{text." +  w + ".short}")) + ")";
                }
                return null;
            }
        }


        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        public Business Business { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        public User User { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        public Address BillingAddress { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        public Business Sender { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        public Address SenderAddress { get; set; }
        
        [Column]
        public ICollection<LineItemGroup> LineItemGroups { get; set; }

        [Column]
        public ICollection<Payment> Payments { get; set; }

        [Column]
        public ICollection<InvoiceHistory> InvoiceHistories { get; set; }


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public string GetPDFFileName() {
            var name = $"{Core.Config.ProjectID}_Invoice_{this.SerialId}";
            if (!string.IsNullOrEmpty(this.Business?.Name)) {
                name += "_" + this.Business.Name;
            }
            if (!string.IsNullOrEmpty(this.Title)) {
                name += "_" + this.Title;
            }
            return name + ".pdf";
        }

        public List<decimal> GetAllVATRates() {
            var ret = new List<decimal>();
            foreach(var li in this.LineItemGroups) {
                foreach (var v in li.GetAllVATRates()) { 
                    if (!ret.Contains(v)) ret.Add(v);
                }
            }
            return ret;
        }

       

        public List<LineItemTotal> TotalAsLineItems() {
            var ret = new List<LineItemTotal>();
            var vats = this.GetAllVATRates();

            // e.g. Groups
            foreach(var group in this.LineItemGroups) {
                var t = new LineItemTotal();
                t.LineItem = group.Title;
                t.CustomerVAT = group.CustomerVAT.Clone();
                t.TimeRange = group.TimeRange.Clone();

                // Don't show minus
                var dontShowMinusSubtotal = false;
                if (dontShowMinusSubtotal && group.CustomerSubtotal.HasValue && group.CustomerSubtotal.Value < 0) {
                    t.CustomerSubtotal = new Price(0,group.CustomerSubtotal.Currency);
                }
                else {
                    t.CustomerSubtotal = group.CustomerSubtotal.Clone();
                }
                
                t.CustomerTotal = group.CustomerTotal.Clone();
                ret.Add(t);
            }
            // Show Subtotal ?
            if (Config.FinanceInvoiceTotalGroupShowSubtotalIfMultiple == true && this.LineItemGroups.Count > 1){
                var t = new LineItemTotal();
                t.StyleClass = "italic";
                t.LineItem = "{text.subtotal-excl-vat}";
                t.CustomerSubtotal = this.SubtotalCustomer.Clone();
                ret.Add(t);
            }

            // Sum in multiple-vat-rates
            if (Config.FinanceInvoiceTotalGroupShowMultipleVatRateLineItems == false) {
                var t = new LineItemTotal();
                t.LineItem = "{text.vat}";
                t.StyleClass = "italic";
                if (vats.Count == 1) {
                    t.LineItem += " " + (vats.First().ToString(Core.Config.PercentFormat));
                } else if (vats.Count > 1) {
                    t.LineItem += " ({text.multiple-vat-rates}";
                    t.LineItem += ")";
                }
                t.CustomerVAT = this.VATCustomer.Clone();
                ret.Add(t);
            } else {
                var vatRateGroups = this.LineItemGroups.SelectMany(lig => lig.LineItems).GroupBy(li => li.CustomerVATRate);

                foreach (var vatRateGroup in vatRateGroups) {
                    var t = new LineItemTotal();
                    t.LineItem = "{text.vat}";
                    t.StyleClass = "italic";
                    t.LineItem += " " + (vatRateGroup.Key?.ToString(Core.Config.PercentFormat));
                    t.CustomerVAT = vatRateGroup.Sum(g => g.CustomerVATTotalSpecial);
                    t.CustomerSubtotal = vatRateGroup.Sum(g => g.CustomerSubtotal);
                //    t.CustomerTotal = vatRateGroup.Sum(g => g.CustomerTotal); this does not make sense
                    ret.Add(t);
                }
            }



            {
                var t = new LineItemTotal();
                t.StyleClass = "total";
                t.LineItem = "{text.total-incl-vat}";
                t.CustomerTotal = this.TotalCustomer.Clone();
                ret.Add(t);
            }
            return ret;
        }

        public static IQueryable<Invoice> GetInvoicesToSend(ModelContext db) {
            var businesses = db.PayAfterBusinesses().Select(b=>b.Id);
            return db.Invoices().Include("Business").Where(i => i.Status == InvoiceStatus.Created && businesses.Contains(i.Business.Id));
        }

        public static void SendInvoices(IEnumerable<Invoice> invoices, User user) {
        
            foreach(var invoice in invoices) {

                // Load required data
                invoice.LoadFirstLevelNavigationReferences();
                invoice.LoadFirstLevelObjectReferences();

                // Sending allowed?
                invoice.CheckSendingAllowed();

                foreach (var lineItemGroup in invoice.LineItemGroups) {
                    lineItemGroup.Include(nameof(lineItemGroup.LineItems));
                }

                // DB
                var db = invoice.Context;

                // Get the PDF data
                var data = GetPdfData(invoice, user);

                // Attach to email
                EmailAttachment attachement = new EmailAttachment();
                attachement.Content = new System.IO.MemoryStream(data);
                attachement.Name = invoice.GetPDFFileName();
                EmailTemplate emailTemplate = CreateInvoiceEmail(invoice, db);

                // Send
                emailTemplate.SendBusinessEmail(invoice.Business, invoice.BillingAddress.Email, MailingUnsubscription.Categories.Finance, "invoice", new List<EmailAttachment>() { attachement });

                // Update status
                invoice.Status = InvoiceStatus.Sent;
                invoice.InvoiceSent = DateTime.UtcNow;
                if (!invoice.DueDate.HasValue) {
                    invoice.DueDate = invoice.InvoiceSent.Value.AddDays(Config.FinanceInvoiceDueDateDays);
                }

                // Tracking
                invoice.AddHistory("{text.invoice-history-sent}: " + invoice.SerialId, user);

            }
        }

        /// <summary>
        /// Throws an exception if the BilledExternally flag is set
        /// those invoices are not supposed to be sent
        /// </summary>
        public void CheckSendingAllowed() {
            if (this.BilledExternally == true) {
                throw new BackendException("sending-forbidden", $"The Invoice {this.SerialId} has been marked as 'Externally billed' and is therefore not supported for sending via email");
            }
        }


        /// <summary>
        /// This sends a notification for an invoice
        /// Not a summary in the email.
        /// Just some basic infos and a message 
        /// and the invoice as a pdf attachement
        /// </summary>
        /// <param name="invoice"></param>
        /// <param name="user"></param>
        /// <param name="toAddresses"></param>
        /// <param name="ccAddresses"></param>
        /// <param name="additionalMessage"></param>
        /// <param name="language"></param>
        public static void SendInvoiceNotification(Invoice invoice, User user, string toAddresses, string ccAddresses, string additionalMessage, string language, string subject) {
            // DB
            var db = invoice.Context;

            // Get the PDF data
            var data = GetPdfData(invoice, user);

            // Attach to email
            EmailAttachment attachement = new EmailAttachment();
            attachement.Content = new System.IO.MemoryStream(data);
            attachement.Name = invoice.GetPDFFileName();

            EmailTemplate emailTemplate = CreateInvoiceNotificationEmail(invoice, additionalMessage, language, db);
            emailTemplate.Subject = subject;

            emailTemplate.InsertVariables("entity", invoice);

            // Contacts
            var toList = MessageCenter.ParseContactsList(toAddresses);
            var ccList = MessageCenter.ParseContactsList(ccAddresses);

            // Validate Contacts
            if (toList.Any() == false) {
                throw new BackendException("invoice-error", "Not valid email address defined to send the email to");
            }

            // Send 
            emailTemplate.Send(toList, ccList, null,MailingUnsubscription.Categories.Finance,"Invoice", new List<EmailAttachment>() { attachement },ignoreUnsubscribe: true);

            // Update status
            invoice.Status = InvoiceStatus.Sent;
            invoice.InvoiceSent = DateTime.UtcNow;
            if (!invoice.DueDate.HasValue) {
                invoice.DueDate = invoice.InvoiceSent.Value.AddDays(Config.FinanceInvoiceDueDateDays);
            }

            // Tracking
            invoice.AddHistory("{text.invoice-history-sent}: " + invoice.SerialId, user);

        }

        private static EmailTemplate CreateInvoiceNotificationEmail(Invoice invoice, string additionalMessage, string language, ModelContext db) {

            EmailTemplate emailTemplate = new EmailTemplate(db, "invoice-notification", ".htm", language);

            // Additional Message
            if (string.IsNullOrEmpty(additionalMessage) == false) {
                //var messageTemplate = emailTemplate.LoadTemplate("default/invoice-notification.message");

                var messageTemplate = new EmailTemplate(db, "invoice-notification.message");
                messageTemplate.InsertVariableXMLSafeWithLineBreaks("message-content", additionalMessage);
                emailTemplate.InsertTemplate("message", messageTemplate);
            } else {
                emailTemplate.InsertVariable("message", "");
            }


            // Company
            emailTemplate.InsertVariable("company", db.OwnerBusiness().Name);

            // Email Content from CMS
            emailTemplate.InsertContent(
                variableName: "cms-content",
                cmsPath: "/General/Invoice/EmailText",
                throw404IfNotExists: false, db: db
            );

            emailTemplate.InsertVariable("contact", invoice.BillingName);

          
            return emailTemplate;
        }

        /// <summary>
        /// Creates and fills an invoice email template
        /// </summary>
        /// <param name="invoice">The invoice.</param>
        /// <param name="db">The database.</param>
        /// <returns></returns>
        private static EmailTemplate CreateInvoiceEmail(Invoice invoice, ModelContext db, string templateName = "invoice") {
                   
            var emailTemplate = new EmailTemplate(db, templateName);

            invoice.LoadLineItems();

            // Summary and line items
            emailTemplate.InsertInvoiceSummary("invoice.overview", invoice, new FormBuilder(Forms.Admin.EMAIL));
            if (invoice.ShowLineItems == true) {
                emailTemplate.InsertInvoiceLineItems("invoice.line-items", invoice, new FormBuilder(Forms.Admin.EMAIL));
            } else {
                emailTemplate.InsertVariable("invoice.line-items", string.Empty);
            }
            emailTemplate.InsertInvoiceTotal("invoice.total", invoice, new FormBuilder(Forms.Admin.EMAIL));

            // Email Content from CMS
            emailTemplate.InsertContent(
                variableName: "cms-content",
                cmsPath: "/General/Invoice/EmailText",
                throw404IfNotExists: false, db: db
            );

            // Email Intro content from CMS
            // {contact} can be used as a placeholder
            emailTemplate.InsertContent(
               variableName: "email-intro",
               cmsPath: "/General/Invoice/EmailIntro",
               throw404IfNotExists: false, db: db
           );

            // Email Outro content from CMS
            // {contact} can be used as a placeholder
            emailTemplate.InsertContent(
               variableName: "email-outro",
               cmsPath: "/General/Invoice/EmailOutro",
               throw404IfNotExists: false, db: db
           );

            // Payment details
            emailTemplate.InsertInvoicePaymentDetails("payment-details", invoice);

            // Addresses
            emailTemplate.InsertAddress("billing-address", "{text.order.billing-address}", invoice.BillingAddress);
            emailTemplate.InsertAddress("invoice.sender-address-breaked", "{text.order.billing-address}", invoice.SenderAddress);
            emailTemplate.InsertAddress("invoice.sender-address-breaked", "{text.order.billing-address}", invoice.SenderAddress);


            // Insert entity properties 
            emailTemplate.InsertVariables("entity", invoice);
            emailTemplate.InsertVariables("invoice", invoice);

            // Download link
            emailTemplate.InsertVariable(
              "invoices.path", Config.InvoicePublicPath
            );

            // Subject
            var invoiceText = Core.Localization.Text.GetTranslatedTextByIdForLanguage("invoice", Core.Config.LocalizationDefaultLanguage, Config.MonthlyInvoiceSubject);
            emailTemplate.Subject = $"{invoiceText}: {invoice.SerialId}" ;

            // Replace {contact} with the name of the BillingAddress
            emailTemplate.InsertVariable("contact", invoice.BillingAddress.Name);
            return emailTemplate;
        }

        /// <summary>
        /// Loads the all its LineItemGroups and its items
        /// </summary>
        public void LoadLineItems() {
            this.Include(nameof(Invoice.LineItemGroups));
            this.LineItemGroups.ToList().ForEach(lig => lig.Include(nameof(lig.LineItems)));
        }

        public void UpdateInvoice() {
            this.SubtotalCustomer = this.LineItemGroups.Sum(g => g.CustomerSubtotal);
            this.VATCustomer = this.LineItemGroups.Sum(g => g.CustomerVAT);
            this.TotalCustomer = this.LineItemGroups.Sum(g => g.CustomerTotal);
        }

        /// <summary>
        /// Changes the status
        /// If the status of the invoice is set to paid update all its containing orders (statuses)
        /// </summary>
        /// <param name="value">The value.</param>
        public void ChangeStatus(InvoiceStatus value, User user = null) {
            
            // Inform subscribers
            StatusChanged?.Invoke(value, this);

            // Set status
            Status = value;

            // Tracking
            this.AddHistory("{text.invoice-history-status-changed}", user);
        }

        public void Delete() {
            // Tell Order to unlink the invoice 
            Invoice.InvoiceDeleted?.Invoke(this);
            this.Context.DeleteEntity(this);
        }

        public static IQueryable<Invoice> GetInvoicesForBusiness(ModelContext db, Business business) {
            var paymentPointOfTime = business.Settings[Business.SETTING_KEY_PAYMENT].ToString();
            return db.Invoices().
                Include(nameof(Invoice.Payments))
                .Include("LineItemGroups")
                .Include("LineItemGroups.LineItems")
                .Where(i => i.Business.Id == business.Id
                            &&
                            (
                                (paymentPointOfTime == Business.SETTING_VALUE_PAYMENT_AFTER && i.Status >= Invoice.InvoiceStatus.Sent)
                             || (paymentPointOfTime == Business.SETTING_VALUE_PAYMENT_BEFORE)

                             )
                       );
        }

        public static byte[] GetPdfData(Invoice invoice, User user) {
            var data = Core.Handler.PDFPageTemplateHandler.GeneratePDFDateForRoute("/pdf/finance/invoice/" + invoice.PublicId, user);
            return data;
        }

        /// <summary>
        /// Adds a payment. Changes status to paid if sufficient amount. informs orders of status change
        /// </summary>
        /// <param name="payment">The payment.</param>
        public void AddPayment(Payment payment, DateTime? paidDate = null, User user = null) {
          
            // we only set the invoice and all its orders to paid if the amount is >= Invoice Total
            if (this.PaidAmount + payment.Amount >= this.TotalCustomer) {
                this.PaidDate = paidDate ?? DateTime.UtcNow;
                this.ChangeStatus(InvoiceStatus.Paid, user);
            }
  			this.Payments.Add(payment);
           
        }

        // TODO MERGE WITH PREVIOUS
        public void AddPaymentWithSave(ModelContext db, Payment payment ,DateTime? paidDate = null) {
            this.Payments.Add(payment);
            db.SaveChanges();
            // we only set the invoice and all its orders to paid if the amount is >= Invoice Total
            if (this.PaidAmount + payment.Amount >= this.TotalCustomer) {
                this.PaidDate = paidDate ?? DateTime.UtcNow;
                this.ChangeStatus(InvoiceStatus.Paid);
               
            }
            db.SaveChanges();
        }

        public void AddRefundPayment(Payment refundPayment, User actingUser) {
            this.Payments.Add(refundPayment);
            // Change status to Refunded
            if (this.PaidAmount.Value == 0) {
                this.ChangeStatus(Invoice.InvoiceStatus.Refunded, actingUser);
            }
        }



        public void AddHistory(string description, User user, InvoiceStatus? status = null) {
            var history = new InvoiceHistory();
            history.Description = description;
            history.Created = DateTime.UtcNow;
            history.User = user?.Username;
            if (!status.HasValue) {
                history.Status = this.Status;
            }
            else {
                history.Status = this.Status;
            }
            this.InvoiceHistories.Add(history);
        }

        public EmailTemplate GetTestTemplate(ModelContext db) {
            var invoice = db.Invoices().OrderByDescending(i => i.Id).FirstOrDefault();
            if (invoice != null) {
                // Load required data
                invoice.LoadFirstLevelNavigationReferences();
                invoice.LoadFirstLevelObjectReferences();
                return CreateInvoiceEmail(invoice, db);
            }
            return null;
        }

        public IEnumerable<Tuple<string, string>> GetSubCategories(MailingUnsubscription.Categories category) {
            var categories = new List<Tuple<string, string>>();
            if (category == MailingUnsubscription.Categories.Finance) {
                // Dont allow unsubscribe for invoices
                //categories.Add(new Tuple<string, string>(Core.Util.String.CreateShortURLForName("invoice"), "Invoice"));
            }
            return categories;
        }

        /// <summary>
        /// Get all LineItems from all LineItemGroups grouped by their SubCategory
        /// HINT: SubCategory, because Category is in all Invoices the same: ProjectCost, OrderItem
        /// </summary>
        /// <returns></returns>
        //public IEnumerable<IGrouping<string, LineItem>> GetItemsSubCategoryGrouped() {
        //    var allLineItems = this.LineItemGroups.SelectMany(g => g.LineItems);
        //    var subCategories = allLineItems.Where(i =>i.Category != null && i.SubCategory != null && i.CustomerTotal != null && i.CustomerTotal.Value != null).GroupBy(li => li.SubCategory);
        //    return subCategories;
        //}

        public IEnumerable<GroupedLineItems> GetItemCategoryGrouped(bool  ignoreEmptyCategories = true) {
            var allLineItems = this.LineItemGroups.SelectMany(g => g.LineItems);
            var categories = allLineItems.Where(i => i.CustomerTotal != null && i.CustomerTotal.Value != null).GroupBy(li => li.Category);
            var categoryGroups = new List<GroupedLineItems>();

            foreach(var category in categories) {
                if (ignoreEmptyCategories == true && string.IsNullOrEmpty(category.Key) == true) {
                    continue;
                }
                var groupedItem = new GroupedLineItems();
                groupedItem.Category = category.Key;
                groupedItem.SubCategories = category.GroupBy(li => li.SubCategory);
                categoryGroups.Add(groupedItem);
            }

            return categoryGroups;
        }


        public bool CheckContainsGroup(string title, string category, bool throwException) {
            if (string.IsNullOrWhiteSpace(title)) {
                throw new Exception("CheckContainsGroup: empty title");
            }
            if (string.IsNullOrWhiteSpace(category)) {
                throw new Exception("CheckContainsGroup: empty category");
            }
            var sameGroups = this.LineItemGroups.Where(li => li.Category == category && li.Title == title);
            if (sameGroups.Any() == true && throwException) {
                throw new BackendException("already-containing-group", $"This Invoice already contains {title}/{category}".InjectTextVariables(Core.Config.LocalizationAdminLanguage));
            }

            return sameGroups.Any();

        }


        /// <summary>
        /// Loads the flags like ShowLineItems etc from the business settings
        /// </summary>
        /// <param name="business"></param>
        public void LoadBusinessSettings(Business business) {
            this.ShowLineItems = business.Settings.Bool(InvoiceShowLineItemsSettingsKey, true); // true is the invoices default value in this case
            this.ShowTimeRanges = business.Settings.Bool(InvoiceShowTimeRangesSettingsKey, false);
            this.ShowCategoryOverview = business.Settings.Bool(InvoiceShowCategoryOverviewSettingsKey, false);
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override int TypeNumber { get { return 30; } }


        public string InfoStatusColor {
            get {
                if (this.IsOverdue()) {
                    return "error";
                }
                if (this.IsDueAndNotOverdue()) {
                    return "warning";
                }
                return null;
            }
        }



        public string InfoStatusText {
            get {
                if (this.IsOverdue()) {
                    return "{text.overdue}";
                } else if (this.IsDueAndNotOverdue()) {
                    return "{text.due}";
                }
                return null;
            }
        }

        public string InfoStatusTooltip {

            get {
                if (this.IsOverdue()) {
                    return "{text.invoice.is-overdue.tooltip}";
                } else if (this.IsDueAndNotOverdue()) {
                    return "{text.invoice.is-due.tooltip}";
                }
                return null;
            }
        }

        public override Core.Cards.CardBuilder VisualCard() {
            // Init card
            var card = new Core.Cards.CardBuilder(this)
                .Title(this.SerialId)
                .Subtitle(this.Business?.Name)
                .Sub("{text.invoice.due-date}"+" "+this.DueDate?.ToDefaultTimezoneDateString())
                .Sub(this.Status)
                .Icon("mail")
                .Wide();
            // Price
            card.Tag(this.TotalCustomer.ToString());
            // Blink?
            if (this.DueDate < DateTime.UtcNow) card.BlinkSub();
            // Breadcrumb
            var crumbs = new List<InvoiceStatus>() { InvoiceStatus.Created,InvoiceStatus.Sent,InvoiceStatus.Paid };
            card.Breadcrumb(crumbs, this.Status);
            return card;
        }

        public override void Validate() {
            base.Validate();

            //TODO: @micha @dan

            

            //Include(nameof(Orders));
            //if (Orders.Select(o => o.PaymentMethod).Distinct().Count() > 1) {
            //    throw new BackendException("mixed-orders-error", "This invoice has orders with different payment methods.");
            //}
            //if (Orders.Select(o => o.PaymentPointOfTime).Distinct().Count() > 1) {
            //    throw new BackendException("mixed-orders-error", "This invoice has orders with different payment point of time.");
            //}

            //todo: more advanced with Order.Isbefore(status) etc
            //if (Status == InvoiceStatus.Created) {
            //    if (Orders.Any(o => o.Status == Order.StatusType.Paid)) {
            //        throw new BackendException("orders-status-error", "This invoice has orders with wrong statuses.");
            //    }
            //}

        }



        #endregion


        #region Private Methods ///////////////////////////////////////////////////////////////////


        internal string GetNewHash() {
            return Core.Encryption.DefaultHasher.HashString(this.Created.Ticks + "_" + Guid.NewGuid().ToString());
        }
        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextInvoiceExtensions {
        [ModelSet]
        public static DbSet<Invoice> Invoices(this Core.Model.ModelContext context) {
            return context.Set<Invoice>();
        }

        public static Invoice GetByHash(this IQueryable<Invoice> query, string hash, bool throwExceptions = true) {
            var ret = query.Where(e => e.Hash == hash).SingleOrDefault();
            if (ret == null && throwExceptions == true) throw new Backend404Exception("invoice-not-found", $"The entity {typeof(Invoice).Name} with ID {hash} could not be found.");
            return ret;
        }
    }

    #endregion

}