using System;
using System.Data.Entity;
using System.Linq;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Machinata.Module.Finance.Model {

    [Serializable()]
    [ModelClass]
    public partial class LineItemGroup : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public LineItemGroup() {
            this.LineItems = new List<LineItem>();
            this.TimeRange = new DateRange();
        }

        #endregion
        

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
          
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [FormBuilder(Forms.Admin.INVOICE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public string Title { get; set; }
        
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [FormBuilder(Forms.Admin.INVOICE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public string Description { get; set; }
        
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.INVOICE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public string Category { get; set; }

        [Column]
        //[FormBuilder(Forms.Admin.VIEW)]
        //[FormBuilder(Forms.Admin.CREATE)]
        //[FormBuilder(Forms.Admin.EDIT)]
        //[FormBuilder(Forms.Admin.LISTING)]
        [DataType(DataType.Date)]
        public DateRange TimeRange { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [NotMapped]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EMAIL)]
        public Price CustomerSubtotal {
            get {
                if (this.LineItems != null && this.LineItems.Count > 0) {
                    return this.LineItems.Sum(li => li.CustomerSubtotal);
                }
                else {
                    return new Price(0);
                }
            }
        }
        
        [NotMapped]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [FormBuilder(Forms.Admin.LISTING)]
        public Price CustomerVAT {
            get {
                // return this.LineItems.Sum(li => li.CustomerVATTotalDerived);
                return this.CustomerTotal - this.CustomerSubtotal;
            }
        }

        [NotMapped]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [FormBuilder(Forms.Admin.INVOICE)]
        [Decimal("0.##")]
        public decimal QuantityTotal
        {
            get
            {
                return this.LineItems.Sum(li => li.Quantity);
            }
        }

        [NotMapped]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [FormBuilder(Forms.Admin.INVOICE)]
        public Price CustomerTotal {
            get {
                if (this.LineItems != null && this.LineItems.Count > 0) {
                    return this.LineItems.Sum(li => li.CustomerTotal);
                } else {
                    return new Price(0);
                }
            }
        }



        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        public ICollection<LineItem> LineItems { get; set; }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////
        
        public List<decimal> GetAllVATRates() {
            var ret = new List<decimal>();
            foreach(var li in this.LineItems) {
                if(li.CustomerVATRate != null && !ret.Contains(li.CustomerVATRate.Value)) {
                    ret.Add(li.CustomerVATRate.Value);
                }
            }
            return ret;
        }

        public LineItem AddItem(string description, Price price, Price internalPrice, decimal? vatRate, int quantity = 1) {
            var lineItem = new Finance.Model.LineItem();
            lineItem.CustomerItemPrice = price.Clone();
            lineItem.InternalItemPrice = internalPrice.Clone();
            lineItem.Quantity = quantity;
            lineItem.Description = description;
            lineItem.CustomerVATRate = vatRate;
            if(vatRate == null) {
                lineItem.CustomerTotal = price * quantity;
                lineItem.CustomerVAT = new Price(0, price.Currency);
            } else {
                lineItem.CustomerVAT = (price * quantity) * vatRate;
                lineItem.CustomerTotal = price * quantity + lineItem.CustomerVAT;
            }
            this.LineItems.Add(lineItem);
            return lineItem;
        }

        public LineItem AddDiscount(Price discount, string description = null) {
            var lineItem = new Finance.Model.LineItem();
            lineItem.CustomerItemPrice = discount.Clone();
            lineItem.InternalItemPrice = new Price(0,discount.Currency);
            lineItem.Quantity = 1;
            if (description == null) {
                lineItem.Description = "{text.discount}";
            }else {
                lineItem.Description = description;
            }
            lineItem.CustomerTotal = discount.Clone();
            lineItem.CustomerVAT = new Price(0, discount.Currency);
            lineItem.CustomerVATRate = null;
            this.LineItems.Add(lineItem);
            return lineItem;
        }


        /// <summary>
        /// Adds a discount resulting in the CustomerTotal == discount and a default VAT 
        /// </summary>
        /// <param name="discount"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public LineItem AddTotalDiscount(Price discount, string description = null) {
            var lineItem = new Finance.Model.LineItem();
            lineItem.CustomerTotal = discount.Clone();
            lineItem.InternalItemPrice = new Price(0, discount.Currency);
            lineItem.Quantity = 1;
            if (description == null) {
                lineItem.Description = "{text.discount}";
            } else {
                lineItem.Description = description;
            }
            lineItem.CustomerItemPrice = new Price( lineItem.CustomerTotal.Value / (1m + (decimal)Config.FinanceDefaultVATRate));
            lineItem.CustomerVAT = lineItem.CustomerTotal - lineItem.CustomerItemPrice;
            lineItem.CustomerVATRate =(decimal) Config.FinanceDefaultVATRate;
            this.LineItems.Add(lineItem);
            return lineItem;
        }

        public LineItem AddShipping(Price shipping) {
            var lineItem = new Finance.Model.LineItem();
            lineItem.CustomerItemPrice = shipping.Clone();
            lineItem.InternalItemPrice = new Price(0, shipping.Currency);
            lineItem.Quantity = 1;
            lineItem.Description = "{text.shipping}";
            lineItem.CustomerVATRate = null;
            lineItem.CustomerTotal = shipping.Clone();
            lineItem.CustomerVAT = new Price(0, shipping.Currency);
            this.LineItems.Add(lineItem);
            return lineItem;
        }

        public LineItem AddVAT(Price vatRetail) {
            var lineItem = new Finance.Model.LineItem();
            lineItem.CustomerItemPrice = vatRetail.Clone();
            lineItem.InternalItemPrice = new Price(0, vatRetail.Currency);
            lineItem.Quantity = 1;
            lineItem.Description = "{text.vat}";
            lineItem.CustomerVATRate = null;
            lineItem.CustomerTotal = vatRetail.Clone();
            lineItem.CustomerVAT = new Price(0, vatRetail.Currency);
            this.LineItems.Add(lineItem);
            return lineItem;

        }



            #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////


        #endregion


        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

        }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextLineItemGroupExtensions {
        [ModelSet]
        public static DbSet<LineItemGroup> LineItemGroups(this Core.Model.ModelContext context) {
            return context.Set<LineItemGroup>();
        }
    }

    #endregion

}
