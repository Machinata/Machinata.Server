using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.ComponentModel.DataAnnotations;

namespace Machinata.Module.Finance.Model {

    [Serializable()]
    [ModelClass]
    public partial class LineItem : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Events ////////////////////////////////////////////////////////////////////////////

   
        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public LineItem() {
            
            this.InternalItemPrice = new Price(0);
            this.CustomerItemPrice = new Price(0);
            this.CustomerVAT = new Price(0);
            this.CustomerTotal = new Price(0);
            this.CustomerVATRate = null;
            this.TimeRange = new DateRange();
        }

        #endregion
        
        #region Public Data Store Properties //////////////////////////////////////////////////////
        
          
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.PDFv2)]
        [FormBuilder(Forms.Admin.LISTEDIT)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [Column]
        public string Description { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [Column]
        public string Notes { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [Column]
        public string Category { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public string SubCategory { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.PDFv2)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.LISTEDIT)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [Decimal("0.##")]
        [Column]
        public decimal Quantity { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        public Price InternalItemPrice { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.PDFv2)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [FormBuilder(Forms.Admin.LISTEDIT)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public Price CustomerItemPrice { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [FormBuilder(Forms.Admin.LISTEDIT)]
        [FormBuilder(Forms.Frontend.LISTING)]
        [Percent]
        public decimal? CustomerVATRate { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [FormBuilder(Forms.Admin.LISTING)]
        //[FormBuilder(Forms.Frontend.LISTING)]
        public Price CustomerVAT { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.EMAIL)]
        //[FormBuilder(Forms.Admin.PDFv2)]
        public Price CustomerTotal { get; set; }



        [Column]
        //[FormBuilder(Forms.Admin.VIEW)]
        //[FormBuilder(Forms.Admin.CREATE)]
        //[FormBuilder(Forms.Admin.EDIT)]
        //[FormBuilder(Forms.Admin.LISTING)]
        [DataType(DataType.DateTime)]
        public DateRange TimeRange { get; set; }

        #endregion


        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        /// <summary>
        /// Derived: CustomerItemPrice * Quantity
        /// </summary>
        /// <value>
        /// The customer subtotal. (Total exkl. MWST)
        /// </value>
        [NotMapped]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.PDFv2)]
        [FormBuilder(Forms.Admin.TOTAL)]
        public Price CustomerSubtotal {
            get {
                return CustomerItemPrice * Quantity;
            }
        }

        /// <summary>
        /// CustomerVAT per item
        /// </summary>
        [NotMapped]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.API.VIEW)]
        [FormBuilder(Forms.Admin.EMAIL)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.TOTAL)]
        public Price CustomerVATTotal {
            get {

                if (this.CustomerVATRate == null) return new Price(0, CustomerSubtotal.Currency);
                return this.CustomerItemPrice * CustomerVATRate;
            }
        }

        /// <summary>
        /// CustomerVAT total
        /// </summary>
        [NotMapped]
        [FormBuilder(Forms.Admin.PDFv2)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public Price CustomerVATTotalSpecial {
            get {
                // if (this.CustomerVATRate == null) return new Price(0, CustomerSubtotal.Currency);
                return this.CustomerTotal - this.CustomerSubtotal;
            }
        }


        [NotMapped]
        [FormBuilder(Forms.Admin.PDFv2)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Frontend.LISTING)]
        public Price CustomerTotalSpecial {
            get {
                return this.CustomerTotal;
            }
        }








        #endregion


        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////










        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Updates CustomerVAT and CustomerTotal 
        /// </summary>
        internal void Update() {

            this.CustomerVAT = (this.CustomerItemPrice * this.Quantity) * this.CustomerVATRate;
            this.CustomerTotal = (this.CustomerItemPrice * this.Quantity) + this.CustomerVAT;
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////


        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextLineItemExtensions {
        [ModelSet]
        public static DbSet<LineItem> LineItems(this Core.Model.ModelContext context) {
            return context.Set<LineItem>();
        }
    }

    #endregion

}
