﻿using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Module.Admin.Dashboard;
using Machinata.Module.Finance.Model;
using Machinata.Module.Reporting.Logic;
using Machinata.Module.Reporting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Finance.Dashboard {
    class FinanceDashboard {

        [DashboardLoader]
        public static void LoadDashboard(DashboardLoader loader) {

            var section = new SectionNode();
            section.SetTitle("Finance");
            section.Style = "W_H2";
            //  section.Info = new NodeInfo();
            var burnRateMonthly = Finance.Config.FinanceMonthlyBurnRate;
            var burnRateQuarterly = burnRateMonthly.HasValue ? burnRateMonthly * 3 : null;

            // Info
            {
                var infoText = $"Revenue = the total amount of income from all invoices | Gross Profit = revenue minus all external costs | Net Profit = gross profit minus burn rate | Monthly Burn Rate = {burnRateMonthly} {Core.Config.CurrencyDefault}";
                var info = new InfoNode();
                info.SetTitle("Info");
                info.Message = infoText;
                section.AddChild(info);
            }

            // Only take invoice with default currencies
            // TODO convert foreign currencies or do grouped graphs
            var invoices = loader.DB.Invoices()
                .Where(i => i.InvoiceSent.HasValue)
                .Where(i => i.Currency == Core.Config.CurrencyDefault)
                .OrderByDescending(i => i.Id).ToList().AsQueryable();

            if (invoices.Any () == true) {
                var oldestInvoice = invoices.Where(i=>i.InvoiceSent> DateTime.MinValue).Min(i => i.InvoiceSent).Value;

                var currency = Core.Config.CurrencyDefault;
                var newestInvoice = invoices.Max(i => i.InvoiceSent).Value;
                var previewDate = newestInvoice.ToLocalTime().EndOfYear().ToUniversalTime();
               
                // Quarters
                {
                    var toggle = new ToggleNode();


                    // Revenue over Quarters
                    {
                        var node = CostsOverTimeReports.GetQuarterlyCosts(invoices, "Revenue", i => i.InvoiceSent, c => c.SubtotalCustomer.Value, unit: currency, accumulate: false, endDate: previewDate);

                        // External Costs
                        {
                            var externalCostsSerie = CostsOverTimeReports.CreateQuarterlyCostsSerie(invoices.Where(i => i.ExternalCosts.HasValue), "External Costs", i => i.InvoiceSent, c => c.ExternalCosts.Value, accumulate: false, endDate: previewDate);
                            externalCostsSerie.ChartType = "column";
                            node.SeriesGroups.First().Series.Add(externalCostsSerie);
                        }
                        // Burn rate
                        if (burnRateQuarterly != null) {
                            var burnRate = CostsOverTimeReports.CreateBurnRateLine("Burn Rate", oldestInvoice.StartOfQuarter(), newestInvoice.EndOfQuarter(), new Price(burnRateQuarterly));
                            node.SeriesGroups.First().Series.Add(burnRate);
                        }
                        toggle.AddChild(node);
                    }

                    // Profit Gross over Quarters
                    {
                        var node = CostsOverTimeReports.GetQuarterlyCosts(invoices, "Gross Profit", i => i.InvoiceSent, c => c.SubtotalCustomer.Value - (c.ExternalCosts.HasValue ? c.ExternalCosts.Value : 0), unit: currency, accumulate: false, endDate: previewDate);

                        // Burn rate
                        if (burnRateQuarterly != null) {
                            var burnRate = CostsOverTimeReports.CreateBurnRateLine("Burn Rate", oldestInvoice.StartOfQuarter(), newestInvoice.EndOfQuarter(), new Price(burnRateQuarterly));
                            node.SeriesGroups.First().Series.Add(burnRate);
                        }

                        toggle.AddChild(node);
                    }

                    // Net Profit over Quarters
                    {

                        if (burnRateQuarterly.HasValue == true) {
                            var node = CostsOverTimeReports.GetQuarterlyCosts(invoices, "Net Profit", i => i.InvoiceSent, c => c.SubtotalCustomer.Value - (c.ExternalCosts.HasValue ? c.ExternalCosts.Value : 0), unit: currency, accumulate: false, null, burnRateQuarterly);
                            toggle.AddChild(node);
                        }

                        
                    }

                    section.AddChild(toggle);


                }


                // Months
                {
                    var toggle = new ToggleNode();

                    // Revenue over last 12 Months
                    {
                        var node = RevenueLastMonths(invoices, currency, null , burnRateMonthly);

                        node.SetTitle("Revenue");
                        toggle.AddChild(node);
                    }

                    // Profit Gross over months
                    {

                        var node = CostsOverTimeReports.GetMonthlyCosts(invoices, "Gross Profit", i => i.InvoiceSent, c => c.SubtotalCustomer.Value - (c.ExternalCosts.HasValue ? c.ExternalCosts.Value : 0), unit: currency, accumulate: false);
                        toggle.AddChild(node);

                        // Burn rate
                        if (burnRateMonthly != null) {
                            var burnRate = CostsOverTimeReports.CreateBurnRateLine("Burn Rate", oldestInvoice.StartOfMonth(), newestInvoice.EndOfMonth(), new Price(burnRateMonthly));
                            node.SeriesGroups.First().Series.Add(burnRate);
                        }
                    }

                    // Net Profit over months
                    {

                        if (burnRateMonthly.HasValue == true) {
                          //  var node = CostsOverTimeReports.GetQuarterlyCosts(invoices, "Gross Profit", i => i.InvoiceSent, c => c.SubtotalCustomer.Value - (c.ExternalCosts.HasValue ? c.ExternalCosts.Value : 0), unit: currency, accumulate: false, endDate: previewDate);
                            var node = CostsOverTimeReports.GetMonthlyCosts(invoices, "Net Profit", i => i.InvoiceSent, c => c.SubtotalCustomer.Value - (c.ExternalCosts.HasValue ? c.ExternalCosts.Value : 0), unit: currency,  accumulate: false, burnRateMonthly);
                            toggle.AddChild(node);
                        }
                    }

                    section.AddChild(toggle);


                }
            }

           



            loader.AddItem(section, "/admin/finance");
        }

        public static LineColumnNode RevenueLastMonths(IQueryable<Invoice> invoices, string currency, int? months = 12,  decimal? burnRate = null) {


            if (months != null) {
                var minDate = DateTime.UtcNow.AddMonths(-months.Value);
                invoices = invoices.Where(i => i.InvoiceSent > minDate);
            }

           
            var node = CostsOverTimeReports.GetMonthlyCosts(invoices, "Revenue", i => i.InvoiceSent, c => c.SubtotalCustomer.Value, unit: currency, accumulate: false);

            // External Costs
            {
                var externalCostsSerie = CostsOverTimeReports.CreateMonthlyCostsSerie(invoices.Where(i => i.ExternalCosts.HasValue), "External Costs", i => i.InvoiceSent, c => c.ExternalCosts.Value, accumulate: false);
                node.SeriesGroups.First().AddColumnSerie(externalCostsSerie);
            }

            // Burn rate
            if (burnRate != null && invoices.Any(i=>i.InvoiceSent.HasValue)) {
                var minDate = invoices.Min(i => i.InvoiceSent).Value.StartOfMonth();
                var maxDate = invoices.Max(i => i.InvoiceSent).Value.EndOfMonth();
                var burnRateLine = CostsOverTimeReports.CreateBurnRateLine("Burn Rate", minDate, maxDate, new Price(burnRate));
                node.SeriesGroups.First().Series.Add(burnRateLine);
            }




            return node;
        }
    }
}
