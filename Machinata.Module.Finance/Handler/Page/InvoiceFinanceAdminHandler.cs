
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Finance.Model;
using Machinata.Module.Finance.Payment;

namespace Machinata.Module.Admin.Handler {


    public class InvoiceFinanceAdminHandler : AdminPageTemplateHandler {
        public const string CookiesNameAddressesCC = "InoiceSendEmailsCCAdresses";
        public const string CookiesNameAdditionalMessage = "InoiceSendEmailsAdditionalMessage";

        private class FinanceBalanceOverview : ModelObject {
            [FormBuilder]
            [FormBuilder(Forms.Admin.VIEW)]
            public Price PayedRevenueIncludingVAT { get; set; }

            [FormBuilder]
            [FormBuilder(Forms.Admin.VIEW)]
            public Price PendingRevenueIncludingVAT { get; set; }

            [FormBuilder]
            [FormBuilder(Forms.Admin.VIEW)]
            public Price ExternalCosts { get; set; }

            [FormBuilder]
            [FormBuilder(Forms.Admin.VIEW)]
            public Price TotalIncludingVAT { get; set; }

            [FormBuilder]
            [FormBuilder(Forms.Admin.VIEW)]
            public Price TotalExcludingVAT { get; set; }

            [FormBuilder]
            [FormBuilder(Forms.Admin.VIEW)]
            public Price TotalExcludingVATAndExternalCosts { get; set; }
        }

        [RequestHandler("/admin/finance/invoices")]
        public void Invoices() {

            var invoices = this.DB.Invoices()
                .Include(nameof(Invoice.Business))
                .AsQueryable();


            var year = this.Params.Int("year", null);
            var quarter = this.Params.Int("quarter", null);
            var business = this.Params.String("business", null);
            var status = this.Params.String("status", null);
            var statuses = Core.Util.EnumHelper.GetEnumValues<Invoice.InvoiceStatus>(typeof(Invoice.InvoiceStatus));
            var dueStatuses = Core.Util.EnumHelper.GetEnumValues<Invoice.DueStatuses>(typeof(Invoice.DueStatuses));
            var due = this.Params.Enum<Invoice.DueStatuses?>("due", null);

            Invoice.InvoiceStatus? statusEnum = null;
            if (string.IsNullOrEmpty(status) == false) {
                statusEnum = statuses.FirstOrDefault(s => s.ToString() == status);
            }

            var availableBusiness = this.DB.Invoices()
              .Include(nameof(Invoice.Business))
              .AsQueryable().Where(i => i.Business != null).Select(i => i.Business).DistinctBy(b => b.Id).OrderBy(b => b.Name);

            invoices = FilterByYear(invoices, year);

            IQueryable<Invoice> invoicesWithoutQuarterFilter = invoices.AsQueryable();

            invoices = FilterByQuarter(invoices, quarter);

            IQueryable<Invoice> invoicesWithoutStatusFilter = invoices.AsQueryable();

            invoices = FilterByStatus(invoices, statusEnum);

            IQueryable<Invoice> invoicesWithoutDueFilter = invoices.AsQueryable();

            invoices = FilterByDue(invoices, due);

            IQueryable<Invoice> invoicesWithoutBusinessFilter = invoices.AsQueryable();

            invoices = FilterByBusiness(invoices,  business);

            // Listing
            var entities = invoices.OrderByDescending(e => e.Created);
            var entitiesFiltered = this.Template.Filter(entities, this, new List<string> { nameof(Invoice.SerialId), nameof(Invoice.Title), nameof(Invoice.BillingName) }).ToList(); //TODO: is it faster to do a tolist here, since we use the data several times...?
            var entitiesPaginated = this.Template.Paginate(entitiesFiltered.AsQueryable(), this);
            this.Template.InsertEntityList(
                "entity-list",
                entitiesPaginated,
                new FormBuilder(Forms.Admin.LISTING).Include(nameof(Invoice.ExternalCosts)),
                "/admin/finance/invoices/invoice/{entity.public-id}"
                
            );

            // Balance Overview
            {
                var balanceOverview = new FinanceBalanceOverview();
                
                balanceOverview.PayedRevenueIncludingVAT = entitiesFiltered
                    .Where(e => e.Status == Invoice.InvoiceStatus.Paid)
                    .SumIgnoringEmptyValues(e => e.TotalCustomer);
                if (balanceOverview.PayedRevenueIncludingVAT == null) balanceOverview.PayedRevenueIncludingVAT = new Price(0);
                
                balanceOverview.PendingRevenueIncludingVAT = entitiesFiltered
                    .Where(e => e.Status == Invoice.InvoiceStatus.Sent)
                    .SumIgnoringEmptyValues(e => e.TotalCustomer);
                if (balanceOverview.PendingRevenueIncludingVAT == null) balanceOverview.PendingRevenueIncludingVAT = new Price(0);

                balanceOverview.ExternalCosts = entitiesFiltered
                    .Where(e => e.Status == Invoice.InvoiceStatus.Sent || e.Status == Invoice.InvoiceStatus.Paid || e.Status == Invoice.InvoiceStatus.Refunded)
                    .SumIgnoringEmptyValues(e => e.ExternalCosts);
                if (balanceOverview.ExternalCosts == null) balanceOverview.ExternalCosts = new Price(0);

                balanceOverview.TotalIncludingVAT = entitiesFiltered
                    .Where(e => e.Status == Invoice.InvoiceStatus.Sent || e.Status == Invoice.InvoiceStatus.Paid)
                    .SumIgnoringEmptyValues(e => e.TotalCustomer);
                if (balanceOverview.TotalIncludingVAT == null) balanceOverview.TotalIncludingVAT = new Price(0);

                balanceOverview.TotalExcludingVAT = entitiesFiltered
                    .Where(e => e.Status == Invoice.InvoiceStatus.Sent || e.Status == Invoice.InvoiceStatus.Paid)
                    .SumIgnoringEmptyValues(e => e.SubtotalCustomer);
                if (balanceOverview.TotalExcludingVAT == null) balanceOverview.TotalExcludingVAT = new Price(0);

                balanceOverview.TotalExcludingVATAndExternalCosts = balanceOverview.TotalExcludingVAT - balanceOverview.ExternalCosts;
                if (balanceOverview.TotalExcludingVATAndExternalCosts == null) balanceOverview.TotalExcludingVATAndExternalCosts = new Price(0);

                this.Template.InsertPropertyList("balance-overview", balanceOverview);
            }

            // Quarter Filters
            {
                //var allQuarters = invoices.ToList().Select(i => Time.ConvertToDefaultTimezone(i.Created).QuarterNumber());
                var allQuarters = invoicesWithoutQuarterFilter.ToList().Select(i => Time.ConvertToDefaultTimezone(i.Created).QuarterNumber());
                var templates = new List<PageTemplate>();
                for (int q = 1; q < 5; q++) {
                    var template = this.Template.LoadTemplate("invoices.filters.quarter");
                    template.InsertVariable("selected", quarter != null && q == quarter.Value ? "selected" : string.Empty);
                    template.InsertVariable("disabled", allQuarters.Contains(q) == false ? "disabled" : string.Empty);
                    template.InsertVariable("quarter", q);
                    templates.Add(template);
                }
                this.Template.InsertTemplates("quarters", templates);
            }


            // Status Filters
            {
               // var allStatuses = invoicesWithoutStatusFilter.ToList().Select(i => i.Status);
                var templates = new List<PageTemplate>();
                foreach (var s in statuses) {
                    var template = this.Template.LoadTemplate("invoices.filters.status");
                    template.InsertVariable("selected", s == statusEnum ? "selected" : string.Empty);
                    template.InsertVariable("disabled", statuses.Contains(s) == false ? "disabled" : string.Empty);
                    template.InsertVariable("status", s);
                    templates.Add(template);
                }
                this.Template.InsertTemplates("statuses", templates);
            }

            // Due Filters
            {
                var templates = new List<PageTemplate>();
                foreach (var s in dueStatuses.Where(d => d != Invoice.DueStatuses.None)) {
                    var template = this.Template.LoadTemplate("invoices.filters.due");
                    template.InsertVariable("selected", s == due ? "selected" : string.Empty);
                    template.InsertVariable("disabled", statusEnum != Invoice.InvoiceStatus.Sent && statusEnum != null  ? "disabled" : string.Empty);
                    template.InsertVariable("due", s);
                    templates.Add(template);
                }
                this.Template.InsertTemplates("due-statuses", templates);
            }


            // Businesses filter
            {
                var templates = new List<PageTemplate>();
                var businesses = invoicesWithoutBusinessFilter.Where(i => i.Business != null).Select(i => i.Business).DistinctBy(b => b.Id);
                foreach (var b in availableBusiness) {
                    var template = this.Template.LoadTemplate("invoices.filters.business");
                    template.InsertVariable("selected", business != null && business == b.PublicId ? "selected" : string.Empty);
                    template.InsertVariable("disabled", businesses.Contains(b) == false ? "disabled" : string.Empty);
                    template.InsertVariables("business", b);
                    templates.Add(template);
                }
                this.Template.InsertTemplates("businesses", templates);

            }

            //  Filters
            this.Template.InsertVariable("year", year.HasValue ? year.Value.ToString() : "{text.all}");
            this.Template.InsertVariable("quarter", quarter);
            this.Template.InsertVariable("status", statusEnum);
            this.Template.InsertVariable("business", business);
            this.Template.InsertVariable("due", due);
            this.Template.InsertVariable("quarter-all-selected", quarter.HasValue == false ? "selected" : string.Empty);
            this.Template.InsertVariable("year-all-selected", year.HasValue == false ? "selected" : string.Empty);
            this.Template.InsertVariable("status-all-selected", statusEnum == null ? "selected" : string.Empty);
            this.Template.InsertVariable("status-due-all-selected", due ==  null ? "selected" : string.Empty);
            this.Template.InsertVariable("business-all-selected", string.IsNullOrWhiteSpace(business) == true ? "selected" : string.Empty);


            // Tool
            this.Template.InsertVariable("enable-invoice-creation", Finance.Config.FinanceAdminInvoiceCreation == true ? "true" : "false");

            // Navigation
            this.Navigation.Add("finance", "{text.finance}");
            this.Navigation.Add("invoices", "{text.invoices}");

        }

        public static IQueryable<Invoice> FilterByBusiness(IQueryable<Invoice> invoices,string business) {

            // Filter: business
            if (string.IsNullOrWhiteSpace(business) == false) {
                int id = Core.Model.ModelObject.GetIdForPublicId(business);
                invoices = invoices.Where(i => i.Business != null && i.Business.Id == id);
            }

            return invoices;
        }

        public static IQueryable<Invoice> FilterByStatus(IQueryable<Invoice> invoices, Invoice.InvoiceStatus? status) {

            // Filter: status
            if (status != null) {
                invoices = invoices.Where(i => i.Status == status);
            }

            return invoices;
        }

        public static IQueryable<Invoice> FilterByDue(IQueryable<Invoice> invoices, Invoice.DueStatuses? status) {

            var now = DateTime.UtcNow;
            // Filter: status
            if (status != null) {
                if (status == Invoice.DueStatuses.Payable) {
                    invoices = invoices.Where(i => i.Status == Invoice.InvoiceStatus.Sent && i.DueDate > now );
                } else if (status == Invoice.DueStatuses.Due) {
                    invoices = invoices.Where(i => i.Status == Invoice.InvoiceStatus.Sent && i.DueDate < now);
                } else {
                    throw new Exception("filter type not supported" + status);
                }
            }

            return invoices;
        }

        public static IQueryable<Invoice> FilterByQuarter(IQueryable<Invoice> invoices, int? quarter) {
            // Filter: quarter 
            if (quarter != null) {
                invoices = invoices.ToList().Where(i => Time.ConvertToDefaultTimezone(i.Created).QuarterNumber() == quarter).AsQueryable();
            }

            return invoices;
        }

        public static IQueryable<Invoice> FilterByYear(IQueryable<Invoice> invoices, int? year) {
            // Filter: year
            if (year != null) {

                var startLocal = new DateTime(year.Value, 1, 1);
                var endLocal = startLocal.EndOfYear();

                var startUTC = startLocal.ToUniversalTime();
                var endUTC = endLocal.ToUniversalTime();


                invoices = invoices.Where(i => i.Created >= startUTC && i.Created <= endUTC);
            }

            return invoices;
        }

        [RequestHandler("/admin/finance/invoices/invoice/{publicId}")]
        public void InvoiceView(string publicId) {
            // Load entity
            var entity = DB.Invoices()
                .Include("LineItemGroups")
                .Include("LineItemGroups.LineItems")
                .Include(nameof(Invoice.InvoiceHistories))
                .Include(nameof(Invoice.Payments))
                .Include(nameof(Invoice.Business))
                .Include(nameof(Invoice.Description))
                .Include(nameof(Invoice.BillingAddress))
                .Include(nameof(Invoice.Notes))
                .GetByPublicId(publicId);

            // Categories
            this.Template.InsertVariable("invoice.show-category-overview", entity.ShowCategoryOverview ? "true" : "false");
            //   Overview
            if (entity.ShowCategoryOverview == true) {
                var form = new FormBuilder(Forms.Admin.LISTING);
                this.Template.InsertCategoryOverview("invoice.category-overview-items", entity, form);
            }

            this.Template.InsertVariable("invoice.has-categories", entity.GetItemCategoryGrouped().Any() ? "true" : "false");



            // Insert entity properties and cards
            this.Template.InsertVariables("invoice", entity);

            var invoiceForm = new FormBuilder(Forms.Admin.VIEW);
            UpdateFormFeatures(entity, invoiceForm);
            this.Template.InsertPropertyList("invoice", entity, invoiceForm);

            // Payments
            this.Template.InsertEntityList("invoice-payments", entity.Payments.AsQueryable(), new FormBuilder(Forms.Admin.LISTING), "/admin/finance/invoices/invoice/" + publicId + "/payment/{entity.public-id}");
            this.Template.InsertVariable("invoice.allow-refund", entity.Status == Invoice.InvoiceStatus.Paid);
            this.Template.InsertVariable("invoice.allow-payment", entity.Status == Invoice.InvoiceStatus.Sent);
            this.Template.InsertVariable("invoice.missing-amount-number", entity.MissingAmount?.Value?.ToString("#.##"));

            // Summary & Line Items
            var groupItemsForm = new FormBuilder(Forms.Admin.INVOICE);
            if (Finance.Config.FinanceAdminInvoiceCreation == true) {
                groupItemsForm.Include(nameof(LineItemGroup.TimeRange));
            }
            this.Template.InsertInvoiceSummary("invoice.summary", entity, groupItemsForm);

            var lineItemForm = new FormBuilder(Forms.Admin.LISTING);
            if (Finance.Config.FinanceAdminInvoiceCreation == true) {
                lineItemForm.Include(nameof(LineItem.TimeRange));
            }
            this.Template.InsertInvoiceLineItems("invoice.line-items", entity, lineItemForm);

            // History
            this.Template.InsertEntityList(
                variableName: "invoice-histories",
                entities: entity.InvoiceHistories.OrderByDescending(ih => ih.Id).AsQueryable(),
                form: new FormBuilder(Forms.Admin.LISTING));

            // Misc
            this.Template.InsertVariable("finance.invoice-due-date-days", Module.Finance.Config.FinanceInvoiceDueDateDays);
            // Tools
            // this.Template.InsertVariable("invoice.qr-bill-code-urisafe", Core.Util.HTTP.UrlPathEncode(entity.QRBillCode));
            this.Template.InsertVariable("invoice.qr-bill-code-urisafe", entity.QRBillCodeURISafe);
            this.Template.InsertVariable("invoice.qr-bill-class", Finance.Config.FinancePDFUseQRBill ? "" : "hidden-content");
            this.Template.InsertVariable("invoice.scan-code-class", Finance.Config.FinancePDFUseQRBill == false ? "" : "hidden-content");
            this.Template.InsertVariable("enable-invoice-creation", Finance.Config.FinanceAdminInvoiceCreation == true && entity.Status < Invoice.InvoiceStatus.Sent ? "true" : "false");
            this.Template.InsertVariable("enable-invoice-sending", Finance.Config.FinanceAdminInvoiceCreation == true ? "true" : "false");
            this.Template.InsertVariable("invoice.has-flat-fee", entity.CheckContainsGroup("{text.flat-fee}", "{text.flat-fee}",false) == true ? "true" : "false");
            this.Template.InsertVariable("invoice.can-mark-as-paid",entity.Status == Invoice.InvoiceStatus.Sent ? "true" : "false");
            this.Template.InsertVariable("invoice.can-mark-as-sent", entity.Status == Invoice.InvoiceStatus.Created ? "true" : "false");
            //this.Template.InsertVariable("invoice.is-editable", entity.Status == Invoice.InvoiceStatus.Created ? "true" : "false");
            this.Template.InsertVariable("invoice.is-editable", "true"); // requested by accounting responsible




            // Warnings
            {
                var templates = new List<PageTemplate>();
                if (entity.Warnings.Any() == true) {
                    foreach (var warning in entity.Warnings) {
                        var template = this.Template.LoadTemplate("invoice.warning");
                        template.InsertVariable("warning.text", warning);
                        templates.Add(template);
                    }
                }
                this.Template.InsertTemplates("warnings", templates);
            }

            // Navigation
            Navigation.Add("finance", "{text.finance}");
            Navigation.Add("invoices", "{text.invoices}");
            this.AddInvoiceNavigation(entity);
        }

        private void AddInvoiceNavigation(Invoice entity) {
            var title = "{text.invoice} " + entity.SerialId;
            var customePageTitle = title;
            if (string.IsNullOrWhiteSpace(entity.Title) == false) {
                customePageTitle += " - " + entity.Title;
            }
            Navigation.Add($"invoice/{entity.PublicId}", title, customePageTitle);
        }

       

        /// <summary>
        /// Include ShowCategories, ShowLineItems, ShowTimeRanges flag if FinanceAdminInvoiceCreation is enabled Power Mode
        /// </summary>
        /// <param name="invoice"></param>
        /// <param name="invoiceForm"></param>
        public static void UpdateFormFeatures(Invoice invoice, FormBuilder invoiceForm) {
            if (Finance.Config.FinanceAdminInvoiceCreation == true) {
                invoiceForm.Include(nameof(invoice.ShowCategoryOverview));
                invoiceForm.Include(nameof(invoice.ShowLineItems));
                invoiceForm.Include(nameof(invoice.ShowTimeRanges));
                invoiceForm.Include(nameof(invoice.BillingEmails));
            }
        }

        [RequestHandler("/admin/finance/invoices/invoice/{publicId}/details")]
        public void InvoiceDetails(string publicId) {
            // Load entity
            var entity = DB.Invoices().Include("LineItemGroups").Include("LineItemGroups.LineItems").GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            entity.LoadFirstLevelObjectReferences();

            // Summary & Line Items
            var groupItemsForm = new FormBuilder(Forms.Admin.INVOICE);
            if (Finance.Config.FinanceAdminInvoiceCreation == true) {
                groupItemsForm.Include(nameof(LineItemGroup.TimeRange));
            }
            this.Template.InsertInvoiceSummary("invoice.summary", entity, groupItemsForm);

            var lineItemForm = new FormBuilder(Forms.Admin.LISTING);
            if (Finance.Config.FinanceAdminInvoiceCreation == true) {
                lineItemForm.Include(nameof(LineItem.TimeRange));
            }
            this.Template.InsertInvoiceLineItems("invoice.line-items", entity, lineItemForm);


            // Navigation
            Navigation.Add("finance", "{text.finance}");
            Navigation.Add("invoices", "{text.invoices}");
            Navigation.Add($"invoice/{entity.PublicId}", "{text.invoice} " + entity.SerialId);
            Navigation.Add("details", "{text.details}");
        }



        [RequestHandler("/admin/finance/invoices/send")]
        public void InvoicesSend() {
            this.RequireWriteARN();

            // Open Invoices
            var invoices = Invoice.GetInvoicesToSend(DB).ToList();
            this.Template.InsertSelectionList(
                variableName: "invoices",
                entities: invoices.AsQueryable(),
                selectedEntities: invoices,
                form: new FormBuilder(Forms.Admin.LISTING),
                selectionAPICall: null
            );

            // Navigation
            this.Navigation.Add("finance", "{text.finance}");
            this.Navigation.Add("invoices", "{text.invoices}");
            this.Navigation.Add("send", "{text.send}");

        }

        [RequestHandler("/admin/finance/invoices/invoice/{publicId}/edit")]
        public void InvoiceEdit(string publicId) {
            this.RequireWriteARN();

            var invoice = DB.Invoices().GetByPublicId(publicId);
            invoice.LoadFirstLevelNavigationReferences();
            invoice.LoadFirstLevelObjectReferences();
            invoice.SetStatus = invoice.Status;

            var form = new FormBuilder(Forms.Admin.EDIT);
            UpdateFormFeatures(invoice, form);

            this.Template.InsertForm(
               variableName: "form",
               entity: invoice,
               form: form,
               apiCall: "/api/admin/finance/invoice/" + invoice.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

            this.Template.InsertVariables("entity", invoice);

            // Navigation
            this.Navigation.Add("finance", "{text.finance}");
            this.Navigation.Add("invoices", "{text.invoices}");
            this.Navigation.Add($"invoice/{invoice.PublicId}", invoice.SerialId);
            this.Navigation.Add($"edit", "{text.edit}");
        }

        [RequestHandler("/admin/finance/invoices/invoice/{invoiceId}/payment/{publicId}")]
        public void InvoicePaymentView(string invoiceId, string publicId) {
            var invoice = DB.Invoices().GetByPublicId(invoiceId);
            var payment = DB.Payments().GetByPublicId(publicId);

            payment.LoadFirstLevelNavigationReferences();
            payment.LoadFirstLevelObjectReferences();

            if (!invoice.Payments.Contains(payment)) {
                throw new BackendException("wrong-invoice", "This invoice does not contain the given payment.");
            }

            Template.InsertPropertyList("entity", payment, new FormBuilder(Forms.Admin.VIEW));

            Template.InsertVariable("entity.has-external-link", (payment.PaymentProviderInstance.GetExternalTransactionLink(payment) != null));
            Template.InsertVariable("entity.is-live-refundable", (payment.IsLiveRefundable && invoice.Status == Invoice.InvoiceStatus.Paid).ToString().ToLowerInvariant());
            Template.InsertVariable("entity.is-manually-refundable", (!payment.IsLiveRefundable && invoice.Status == Invoice.InvoiceStatus.Paid).ToString().ToLowerInvariant());
            Template.InsertVariables("entity", payment);
            Template.InsertVariables("invoice", invoice);


            // Navigation
            this.Navigation.Add("finance", "{text.finance}");
            this.Navigation.Add("invoices", "{text.invoices}");
            this.Navigation.Add($"invoice/{invoice.PublicId}", invoice.SerialId);
            this.Navigation.Add($"payment/{payment.PublicId}", payment.PublicId, "{text.payment}: " + payment.PaymentProvider);
        }

        [RequestHandler("/admin/finance/invoices/invoice/{invoiceId}/payment/{publicId}/edit")]
        public void InvoicePaymentEdit(string invoiceId, string publicId) {
            this.RequireWriteARN();

            var payment = DB.Payments().GetByPublicId(publicId);
            var invoice = DB.Invoices().GetByPublicId(invoiceId);
            payment.LoadFirstLevelNavigationReferences();
            payment.LoadFirstLevelObjectReferences();

            this.Template.InsertForm(
               variableName: "form",
               entity: payment,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: "/api/admin/finance/invoice/" + invoice.PublicId + "/payments/" + payment.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

            Template.InsertVariables("entity", payment);


            // Navigation
            this.Navigation.Add("finance", "{text.finance}");
            this.Navigation.Add("invoices", "{text.invoices}");
            this.Navigation.Add($"invoice/{invoice.PublicId}", invoice.SerialId);
            this.Navigation.Add($"payment/{payment.PublicId}", payment.PublicId, "{text.payment}: " + payment.PublicId);
            this.Navigation.Add($"edit", "{text.edit}");
        }

        [RequestHandler("/admin/finance/invoices/invoice/{publicId}/add-payment")]
        public void InvoiceAddPayment(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Invoices().GetByPublicId(publicId);

            this.Template.InsertForm(
                variableName: "form",
                entity: new Payment() { Amount = entity.MissingAmount,
                    PaymentProvider = Finance.Payment.PaymentMethod.Invoice.ToString(),
                    Paid = DateTime.UtcNow },
                form: new FormBuilder(Forms.Admin.CREATE),
                apiCall: "/api/admin/finance/invoice/" + publicId + "/add-payment",
                onSuccess: "/admin/finance/invoices/invoice/" + publicId
             );

            // Navigation
            this.Navigation.Add("finance", "{text.finance}");
            this.Navigation.Add("invoices", "{text.invoices}");
            this.Navigation.Add($"invoice/{entity.PublicId}", entity.SerialId);
            this.Navigation.Add("add-payment", "{text.add-payment}");
        }


        [RequestHandler("/admin/finance/invoices/create")]
        public void InvoiceCreate() {
            this.RequireWriteARN();

            this.RequireInvoiceEditEnabled();

            var invoice = new Invoice();

            var form = new FormBuilder(Forms.Admin.CREATE);
            var businesses = this.DB.CustomerBusinesses().OrderBy(b=>b.Name);

            ModelObject selected = null;
            form.Exclude(nameof(Invoice.Business));
            form.DropdownList(nameof(Invoice.Business), businesses, selected, true);

            this.Template.InsertForm(
               variableName: "form",
               entity: invoice,
               form: form,
               apiCall: "/api/admin/finance/invoices/create",
               onSuccess: "/admin/finance/invoices/invoice/{entity.public-id}"
            );


            // Navigation
            this.Navigation.Add("finance", "{text.finance}");
            this.Navigation.Add("invoices", "{text.invoices}");
            this.Navigation.Add("create");
        }

        private void RequireInvoiceEditEnabled() {
            if (Finance.Config.FinanceAdminInvoiceCreation == false) {
                throw new BackendException("feature-disable", "This feature is not enabled");
            }
        }

        [RequestHandler("/admin/finance/invoices/invoice/{publicId}/line-items")]
        public void InvoiceGroups(string publicId) {

            this.RequireInvoiceEditEnabled();
            var invoice = this.DB.Invoices()
              .Include(nameof(Invoice.LineItemGroups) + "." + nameof(LineItemGroup.LineItems))
              .GetByPublicId(publicId);

            this.Template.InsertEntityList(
                variableName: "entities",
                entities: invoice.LineItemGroups.AsQueryable(),
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "{page.navigation.current-path}/group/{entity.public-id}",
                total: new FormBuilder(Forms.Admin.TOTAL));

            this.Template.InsertVariables("invoice", invoice);
          //  this.Template.InsertInvoiceSummary("invoice.summary", invoice);

            this.Navigation.Add("finance", "{text.finance}");
            this.Navigation.Add("invoices", "{text.invoices}");
            this.Navigation.Add($"invoice/{invoice.PublicId}", "{text.invoice} " + invoice.SerialId);
            this.Navigation.Add($"line-items");

        }

        [RequestHandler("/admin/finance/invoices/invoice/{publicId}/line-items/group/{groupId}")]
        public void InvoiceGroup(string publicId, string groupId) {

            this.RequireInvoiceEditEnabled();
            // Group
            var invoice = this.DB.Invoices()
              .Include(nameof(Invoice.LineItemGroups) + "." + nameof(LineItemGroup.LineItems))
              .GetByPublicId(publicId);
            var group = invoice.LineItemGroups.AsQueryable().GetByPublicId(groupId);
            this.Template.InsertPropertyList(
                variableName: "form",
                entity: group,
                form: new FormBuilder(Forms.Admin.VIEW));
            this.Template.InsertVariables("entity", group);

            // LineItems
           var changed = false; ;
            if (group.LineItems.Count == 0) {
                group.AddItem("Item 1", new Price(0), new Price(0), (decimal)Finance.Config.FinanceDefaultVATRate);
                changed = true;
            }

            if (changed == true) {
                this.DB.SaveChanges();
            }
            var form = new FormBuilder(Forms.Admin.LISTEDIT);
            this.Template.InsertEditableEntityList(
                variableName: "group.line-items",
                editAPICall: $"/api/admin/finance/invoice/{invoice.PublicId}/group/{group.PublicId}/edit-line-items",
                editAPIOnSuccess: "{page.navigation.current-path}",
                entities: group.LineItems.AsQueryable(),
                form: form
                );


            // Vars
            this.Template.InsertVariables("invoice", invoice);

            // Nav
            this.Navigation.Add("finance", "{text.finance}");
            this.Navigation.Add("invoices", "{text.invoices}");
            this.Navigation.Add($"invoice/{invoice.PublicId}", "{text.invoice} " + invoice.SerialId);
            this.Navigation.Add($"line-items");
            this.Navigation.Add($"group/" + groupId, group.Title);

        }


        [RequestHandler("/admin/finance/invoices/invoice/{publicId}/line-items/group/{groupId}/edit")]
        public void InvoiceGroupEdit(string publicId, string groupId) {
            this.RequireInvoiceEditEnabled();
            var invoice = this.DB.Invoices()
              .Include(nameof(Invoice.LineItemGroups) + "." + nameof(LineItemGroup.LineItems))
              .GetByPublicId(publicId);

            var group = invoice.LineItemGroups.AsQueryable().GetByPublicId(groupId);

            this.Template.InsertForm(
                variableName: "form",
                entity: group,
                form: new FormBuilder(Forms.Admin.EDIT),
                  apiCall: "/api/admin/finance/invoices/groups/" + group.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}");

            this.Navigation.Add("finance", "{text.finance}");
            this.Navigation.Add("invoices", "{text.invoices}");
            this.Navigation.Add($"invoice/{invoice.PublicId}", "{text.invoice} " + invoice.SerialId);
            this.Navigation.Add($"line-items");
            this.Navigation.Add($"group/" + groupId, group.Title);
            this.Navigation.Add($"edit");

        }


        [RequestHandler("/admin/finance/invoices/invoice/{publicId}/line-items/create-group")]
        public void InvoiceGroupCreate(string publicId) {
            this.RequireInvoiceEditEnabled();
            var invoice = this.DB.Invoices()
              .Include(nameof(Invoice.LineItemGroups) + "." + nameof(LineItemGroup.LineItems))
              .GetByPublicId(publicId);

            var group = new LineItemGroup();

            this.Template.InsertForm(
                variableName: "form",
                entity: group,
                form: new FormBuilder(Forms.Admin.EDIT),
                  apiCall: $"/api/admin/finance/invoices/invoice/{invoice.PublicId}/groups/create",
               onSuccess: "{page.navigation.prev-path}/group/{line-item-group.public-id}");

            this.Navigation.Add("finance", "{text.finance}");
            this.Navigation.Add("invoices", "{text.invoices}");
            this.Navigation.Add($"invoice/{invoice.PublicId}", "{text.invoice} " + invoice.SerialId);
            this.Navigation.Add($"line-items");
            this.Navigation.Add($"create-group");

        }


        /// <summary>
        /// To send one invoice
        /// </summary>
        /// <param name="publicId"></param>
        [RequestHandler("/admin/finance/invoices/invoice/{publicId}/send")]
        public void InvoiceSend(string publicId) {
            this.RequireWriteARN();

            var invoice = DB.Invoices().GetByPublicId(publicId);
            invoice.LoadFirstLevelNavigationReferences();
            invoice.LoadFirstLevelObjectReferences();


            var destinationEmails = invoice.BillingEmails;
           
            var destinationCCEmails = this.Context.Request.Cookies[CookiesNameAddressesCC]?.Value;
            var additionalMessage = this.Context.Request.Cookies[CookiesNameAdditionalMessage]?.Value;


            var dueDate = DateTime.UtcNow.AddDays(this.Params.Int("due-days", Finance.Config.FinanceInvoiceDueDateDays));


            var form = new FormBuilder(Forms.EMPTY);
            
      
            form.Custom("Destination Emails", "destination-emails", "addressbook", destinationEmails, true,readOnly: false, autoComplete: null, tooltip: "{text.invoice.send.destination-emails.tooltip}");
            form.Custom("CC Emails", "destination-cc-emails", "addressbook", destinationCCEmails, false, false,"off", tooltip: "{text.invoice.send.destination-cc-emails.tooltip}");

            if (invoice.DueDate.HasValue == false) {
                form.Custom("Due Date", "due-date", "date", dueDate, true, readOnly: false, autoComplete: null, "{text.invoice.send.due-date.tooltip}");
            }

            // Fix new lines form cookie
            if(additionalMessage != null) {
                additionalMessage = additionalMessage.Replace("%0a", "\n");
            }

            form.Custom("Additional Message", "additional-message", "multilinetext", additionalMessage, false, readOnly: false, autoComplete: null, tooltip: "{text.invoice.send.additional-message.tooltip}");
            form.Button("Send", "Send");

            this.Template.InsertForm(variableName: "form", 
                 form: form,
                 apiCall: "/api/admin/finance/invoice/" +invoice.PublicId+ "/send",
                 onSuccess:"{page.navigation.prev-path}");

         

            this.Template.InsertVariables("entity", invoice);

            // Navigation
            this.Navigation.Add("finance", "{text.finance}");
            this.Navigation.Add("invoices", "{text.invoices}");
            this.Navigation.Add($"invoice/{invoice.PublicId}", invoice.SerialId);
            this.Navigation.Add($"send", "{text.send-via-email}");
        }


    }
}
