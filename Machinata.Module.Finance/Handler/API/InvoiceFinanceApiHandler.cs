
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Module.Finance.Model;
using Machinata.Module.Finance.Payment;
using System.Data.Entity;
using Machinata.Module.Admin.View;
using Machinata.Module.Admin.Handler;
using System.Web;

namespace Machinata.Module.Finance.Handler {


    public class InvoiceFinanceApiHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Module.Finance.Model.Invoice> {
        

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("finance");
        }

        #endregion

        [RequestHandler("/api/admin/finance/invoice/{publicId}/edit")]
        public void InvoiceEdit(string publicId) {
            this.RequireWriteARN();

            // Make changes and save
         

            var entity = this.DB.Invoices()
                .Include(nameof(Invoice.Notes))
                .Include(nameof(Invoice.Description))
                .GetByPublicId(publicId);

            var form = new FormBuilder(Forms.Admin.EDIT);
            InvoiceFinanceAdminHandler.UpdateFormFeatures(entity, form);

            entity.SetStatus = entity.Status;
            var oldValues = entity.GetValues(form);
            entity.Populate(this,form);
            entity.Validate();
            var newValues = entity.GetValues(form);
            var diff = ModelObject.GetObjectValueDiffSummary(oldValues, newValues);

            // Tracking
            if (!string.IsNullOrEmpty(diff)) {
                entity.AddHistory("{text.invoice-history-invoice-edited}: " + entity.SerialId + ": " + diff, this.User);
            }

            // Status
            if (entity.Status != entity.SetStatus) {
                entity.ChangeStatus(entity.SetStatus, this.User);
            }

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                Invoice = new {
                    PublicId = entity.PublicId
                }
            });
        }
        


        [RequestHandler("/api/admin/finance/invoices/send")]
        public void InvoiceSend() {
            this.RequireWriteARN();


            // Init
            var selecteableInvoices = Invoice.GetInvoicesToSend(DB).ToList();
            var invoiceIds = Params.StringArray("selected", new string[] { });
            var selectedInvoices = DB.Invoices().Include("Business").GetByPublicIds(invoiceIds).ToList();

            // Sanity
            if (!selectedInvoices.All(oo => selecteableInvoices.Contains(oo))) {
                throw new BackendException("invoices-wrong-status", "Not all invoices are ready for sending");
            }

            // Send
            Invoice.SendInvoices(selectedInvoices,this.User);

           

            // Commit to DB
            this.DB.SaveChanges();
            
            // Return
            SendAPIMessage("invoices-sent-success");
        }

        [RequestHandler("/api/admin/finance/invoice/{publicId}/add-payment")]
        public void InvoiceAddPayment(string publicId) {
            
            // Get
            var entity = DB.Invoices().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();

            // Payment
            var payment = new Model.Payment();
            payment.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            payment.Validate();
            entity.AddPayment(payment);

            // Tracking
            entity.AddHistory("{text.invoice-history-payment-added}: " + payment.ToString(), this.User);

            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("add-payment-success", new {
                Payment = new {
                    PublicId = payment.PublicId
                }
            });
        }


        [RequestHandler("/api/admin/finance/invoice/{publicId}/add-payment-costs-date")]
        public void InvoiceAddPaymentWithCostsDate(string publicId) {

            // Get
            var entity = DB.Invoices().GetByPublicId(publicId);

            // Amount
            var amount = this.Params.Decimal("amount",0);

            // Date
            var date = this.Params.DateTimeGlobalConfigFormat("date", DateTime.UtcNow);

            // Cap to Date without time
            date = date.ToDefaultTimezone().Date.ToUniversalTime();


            // Payment
            var payment = new Model.Payment();
            payment.Amount = new Price(amount, entity.Currency);
            payment.PaymentProvider = "Invoice";
            payment.Paid = date;
            payment.ProviderTransactionFee = new Price(0, entity.Currency);

            // Validate
            payment.Validate();

            entity.AddPayment(payment, date, this.User);

            // Tracking
            entity.AddHistory("{text.invoice-history-payment-added}: " + payment.ToString(), this.User);

            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("add-payment-success", new {
                Payment = new {
                    PublicId = payment.PublicId
                }
            });
        }



        [RequestHandler("/api/admin/finance/invoice/{invoiceId}/payments/{publicId}/delete")]
        public void DeletePayment(string invoiceId, string publicId) {

            // Get
            var invoice = this.DB.Invoices().Include(nameof(Invoice.Payments)).GetByPublicId(invoiceId);
            var payment = invoice.Payments.SingleOrDefault(p => p.PublicId == publicId);

            // Remove
            this.DB.Payments().Remove(payment);

            // Tracking
            invoice.AddHistory("{text.invoice-history-payment-deleted}: " + payment.ToString(), this.User);

            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("delete-payment-success");
        }



        /// <summary>
        /// Invoices the delete.
        /// </summary>
        /// <param name="publicId">The public identifier.</param>
        [RequestHandler("/api/admin/finance/invoice/{publicId}/delete")]
        public void InvoiceDelete(string publicId) {
            
            // Get
            var entity = this.DB.Invoices().GetByPublicId(publicId);

            // Delete
            entity.Delete();

            // Save
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success");
        }



        /// <summary>
        /// Rebuilds the invoice as a flat rate, while keeping all the line items.
        /// </summary>
        [RequestHandler("/api/admin/finance/invoice/{publicId}/mark-as-sent")]
        public void MarkAsSent(string publicId) {
            
            // Get
            var entity = this.DB.Invoices().GetByPublicId(publicId);

            // Update
            entity.InvoiceSent = DateTime.UtcNow;
            entity.DueDate = entity.InvoiceSent.Value.AddDays(this.Params.Int("due-days", Config.FinanceInvoiceDueDateDays));
            entity.ChangeStatus(Invoice.InvoiceStatus.Sent);
            

            // Tracking
            entity.AddHistory("{text.invoice-history-marked-as-sent}: " + "Due Date: " + entity.DueDate.Value.ToDefaultTimezoneDateString(), this.User);

            // Save
            this.DB.SaveChanges();
            
            // Return
            SendAPIMessage("success");

        }

        /// <summary>
        /// Rebuilds the invoice as a flat rate, while keeping all the line items.
        /// </summary>
        [RequestHandler("/api/admin/finance/invoice/{publicId}/set-flat-fee")]
        public void InvoiceSetFlatFee(string publicId) {
            
            // Get
            var entity = this.DB.Invoices()
                .Include(nameof(Invoice.LineItemGroups))
                .Include(nameof(Invoice.LineItemGroups)+"."+nameof(LineItemGroup.LineItems))
                .GetByPublicId(publicId);
            var vats = entity.GetAllVATRates();

            // Get flat fee
            decimal flatFeeDecimal = this.Params.Decimal("flat-fee", -1m);
            if (flatFeeDecimal == -1m) throw new BackendException("invalid-flat-fee","The flat fee is not valid. Please enter a number.");
            Price flatFee = new Price(flatFeeDecimal, entity.Currency);

            // Calculate discount - this will equalize the invoice to zero
            var discountForFlatFee = entity.SubtotalCustomer.Clone();

            // Calculate the vat rate
            decimal? vatRate = null;
            if(vats.Count == 0) {
                vatRate = null;
            } else if (vats.Count == 1) {
                vatRate = vats.First();
            } else {
                throw new BackendException("invalid-vat-rate","Could not determine the VAT rate to use since there are multiple VATs.");
            }

            // Check if already has flat fee
            entity.CheckContainsGroup("{text.flat-fee}", "{text.flat-fee}", true);
                

            // Calculate the internal price
            Price internalFlatFee = flatFee.Clone(); //TODO: Get the internal fee using the average ratio of the total invoice

            // Create new line item groups
            var discountGroup = new LineItemGroup();
            discountGroup.Category = "{text.discount}";
            discountGroup.Title = "{text.discount}";
            discountGroup.AddItem("{text.discount} "+discountForFlatFee.Clone(), -discountForFlatFee.Clone(), discountForFlatFee.Clone(), vatRate, 1);
            entity.LineItemGroups.Add(discountGroup);

            var flatFeeGroup = new LineItemGroup();
            flatFeeGroup.Category = "{text.flat-fee}";
            flatFeeGroup.Title = "{text.flat-fee}";
            flatFeeGroup.AddItem("{text.flat-fee} "+flatFee, flatFee, internalFlatFee, vatRate, 1);
            entity.LineItemGroups.Add(flatFeeGroup);

            entity.UpdateInvoice();
            entity.AddHistory("Set Flat Fee to " + flatFee, this.User);

            // Save
            this.DB.SaveChanges();
            
            // Return
            SendAPIMessage("success");
        }

        [RequestHandler("/api/admin/finance/invoice/{publicId}/refund/{paymentId}")]
        public void InvoiceRefund(string publicId, string paymentId) {

            // Grab entities
            var invoice = DB.Invoices().Include(nameof(Invoice.Payments)).GetByPublicId(publicId);
            var paymentToRefund = invoice.Payments.AsQueryable().GetByPublicId(paymentId);
            invoice.LoadFirstLevelNavigationReferences();

            // Add payment
            var refundPayment = new Model.Payment();
            refundPayment.PaymentProvider = paymentToRefund.PaymentProvider;
            refundPayment.Amount = -paymentToRefund.Amount;
            refundPayment.Paid = DateTime.UtcNow;
            refundPayment.PaymentProviderTransactionId = this.Params.String("transaction-id", Guid.NewGuid().ToString());
            refundPayment.PaymentProviderPayerId = paymentToRefund.PaymentProviderPayerId;
            refundPayment.ProviderTransactionFee = new Price(0, paymentToRefund.Amount.Currency); 
            
            invoice.AddRefundPayment(refundPayment, this.User);


            // Tracking
            invoice.AddHistory("{text.invoice-history-refunded}: " + refundPayment.ToString(), this.User);

            //Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("refund-success", new {
                Payment = new {
                    PublicId = refundPayment.PublicId
                }
            });
        }


        [RequestHandler("/api/admin/finance/invoice/{publicId}/refund-live/{paymentId}")]
        public void InvoiceRefundLive(string publicId, string paymentId) {

            var entity = DB.Invoices().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            var payment = entity.Payments.AsQueryable().GetByPublicId(paymentId);
            var providerName = payment.PaymentProvider;
            var provider = PaymentCenter.GetPaymentProvider(providerName);

            if (!provider.CanRefundLive) {
                throw new BackendException("refund-error", "Payment provider doenst not support live refunding");
            } else {
                provider.Refund(entity, payment, this.User);
            }

            // Tracking
            entity.AddHistory("{text.invoice-history-refunded}: " + payment.ToString(), this.User);

            //Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("refund-success", new {
                Payment = new {
                    PublicId = payment.PublicId
                }
            });
        }

        [RequestHandler("/api/admin/finance/invoice/{invoiceId}/payments/{publicId}/edit")]
        public void EditPayment(string invoiceId, string publicId) {
            this.RequireWriteARN();

            // Make changes and save
            var invoice = this.DB.Invoices().GetByPublicId(invoiceId);

            var entity = this.DB.Payments().GetByPublicId(publicId);

            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();

            // Tracking
            invoice.AddHistory("{text.invoice-history-payment-edited}: " + entity.ToString(), this.User);

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new {
                Invoice = new {
                    PublicId = entity.PublicId
                }
            });
        }


        [RequestHandler("/api/admin/finance/invoices/create")]
        public void Create() {
         
            var form = new FormBuilder(Forms.Admin.CREATE);

            var entity = new Invoice();
            entity.SubtotalCustomer = new Price(0);
            entity.VATCustomer = new Price(0);
            entity.TotalCustomer = new Price(0);
            entity.PaymentMethod = PaymentMethod.Invoice;
            entity.Status = Invoice.InvoiceStatus.Created;
            entity.Sender = this.DB.OwnerBusiness();

            entity.Sender.Include(nameof(Business.DefaultBillingAddress));
            entity.SenderAddress = entity.Sender.DefaultBillingAddress;

            entity.Populate(this, form);
            // Post Populate
            entity.Validate();


            // Add a first group
            var group = new LineItemGroup();
           
            group.Title = entity.Title;
            group.Description = entity.Title;
            entity.LineItemGroups.Add(group);

            // Save
            this.DB.Invoices().Add(entity);

            if (entity.Business!= null) {
                entity.Business.Include(nameof(entity.Business.DefaultBillingAddress));
                entity.BillingAddress = entity.Business.DefaultBillingAddress;
                entity.LoadBusinessSettings(entity.Business);
            }



            this.DB.SaveChanges();

            this.SendAPIMessage("create-success",new {
                Entity = new {
                    PublicId = entity.PublicId
                }
            });
        }


        [RequestHandler("/api/admin/finance/invoice/{publicId}/group/{groupId}/edit-line-items")]
        public void EditLineItems(string publicId, string groupId) {

            this.RequireWriteARN();
            if (Finance.Config.FinanceAdminInvoiceCreation == false) {
                new BackendException("not-supported", "This is not supported");
            }

            var invoice = this.DB.Invoices()
              .Include(nameof(Invoice.LineItemGroups) + "." + nameof(LineItemGroup.LineItems))
               .GetByPublicId(publicId);

            var lineItemGroup = invoice.LineItemGroups.AsQueryable().GetByPublicId(groupId);

            var newEntities = this.PopulateListEdit(lineItemGroup.LineItems.AsQueryable(), createNewLineItem, null, enableEntityDeletion: true);

            // Add to Group
            foreach (var newEntity in newEntities) {
                lineItemGroup.LineItems.Add(newEntity);
                this.DB.LineItems().Add(newEntity);
            }

            // Update Items
            foreach (var lineItem in lineItemGroup.LineItems) {
                lineItem.Update();
            }

            // Update Invoice
            invoice.UpdateInvoice();


            // Tracking
            invoice.AddHistory("{text.invoice-history-line-items-edited}", this.User);

            // Save
            this.DB.SaveChanges();

            // Return
            this.SendAPIMessage("edit-success", new { Invoice = new { invoice.PublicId } });
        }

        private LineItem createNewLineItem(string id ) {
            return new LineItem();
        }



        /// <summary>
        /// Sends an email to the defined recipeients with an invoice as a pdf 
        /// </summary>
        [RequestHandler("/api/admin/finance/invoice/{publicId}/send")]
        public void Send(string publicId) {

            // Get
            var entity = this.DB.Invoices().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            entity.LoadFirstLevelObjectReferences();

            // Allowed to send?
            entity.CheckSendingAllowed();


            var addresses = this.Params.String("destination-emails");
            var ccAddresses = this.Params.String("destination-cc-emails");
            var additionalMessage = this.Params.String("additional-message");
            var dueDate = this.Params.DateTime("due-date",DateTime.MinValue);


            var log = new StringBuilder();
            log.Append("to: " + addresses);
            log.Append(", cc: " + ccAddresses);
            log.Append(", message: " + additionalMessage);
            log.Append(", dueDate: " + dueDate);


            if (entity.Status < Invoice.InvoiceStatus.Sent) {

                // Update
                entity.InvoiceSent = DateTime.UtcNow;


                if (dueDate == DateTime.MinValue) {
                    entity.InvoiceSent.Value.AddDays(Config.FinanceInvoiceDueDateDays);
                } else { 
                    entity.DueDate = dueDate;
                }
                entity.ChangeStatus(Invoice.InvoiceStatus.Sent);

                // Save
                this.DB.SaveChanges();
            }

          




            // Set cookies
            AddCookie(InvoiceFinanceAdminHandler.CookiesNameAddressesCC, ccAddresses);
            AddCookie(InvoiceFinanceAdminHandler.CookiesNameAdditionalMessage, additionalMessage);

            // Subject
            var subject = Core.Localization.Text.GetTranslatedTextByIdForLanguage("invoice.notification-email.subject", Core.Config.LocalizationDefaultLanguage, "Invoice");
            subject += ": " + entity.SerialId;
            if (string.IsNullOrEmpty(entity.Title) == false) {
                subject += " - " + entity.Title;
            }

            // Send
            Invoice.SendInvoiceNotification(entity, this.User, addresses, ccAddresses, additionalMessage, Core.Config.LocalizationDefaultLanguage, subject);

            // Tracking
            entity.AddHistory("{text.invoice-history-email-sent}: "  + log, this.User);


            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("success");

        }

        private void AddCookie(string key, string value) {
            var cookie = new HttpCookie(key);
            cookie.Value = value;
          //  cookie.Expires = DateTime.UtcNow.AddDays(30);

        
            this.Context.Response.Cookies.Add(cookie);
        }
    }
}
