
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Module.Finance.Model;
using Machinata.Module.Finance.Payment;
using System.Data.Entity;
using Machinata.Module.Admin.View;
using Machinata.Module.Admin.Handler;
using System.Web;
using Newtonsoft.Json.Linq;
using Machinata.Module.Reporting.Model;

namespace Machinata.Module.Finance.Handler {


    public class FinanceReportAPIHandler : AdminAPIHandler {
        

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("finance");
        }

        #endregion

        [RequestHandler("/api/admin/finance/report/revenue")]
        public void Revenue() {

            var currency = Core.Config.CurrencyDefault;
            var year = this.Params.Int("year", null);
            var quarter = this.Params.Int("quarter", null);
            var business = this.Params.String("business", null);
            var status = this.Params.String("status", null);
            var statuses = Core.Util.EnumHelper.GetEnumValues<Invoice.InvoiceStatus>(typeof(Invoice.InvoiceStatus));
            var due = this.Params.Enum<Invoice.DueStatuses?>("due", null);
            Invoice.InvoiceStatus? statusEnum = null;
            if (string.IsNullOrEmpty(status) == false) {
                statusEnum = statuses.FirstOrDefault(s => s.ToString() == status);
            }


            var invoices = this.DB.Invoices()
                .Include(nameof(Invoice.Business))
                .AsQueryable();

            invoices = InvoiceFinanceAdminHandler.FilterByYear(invoices, year);

            invoices = InvoiceFinanceAdminHandler.FilterByQuarter(invoices, quarter);

            invoices = InvoiceFinanceAdminHandler.FilterByStatus(invoices, statusEnum);

            invoices = InvoiceFinanceAdminHandler.FilterByDue(invoices, due);

            invoices = InvoiceFinanceAdminHandler.FilterByBusiness(invoices, business);

            invoices = invoices.ToList().AsQueryable();

            int? maxMonths = 12;
            if (year != null || quarter != null) {
                maxMonths = null;
            }

            var node = Dashboard.FinanceDashboard.RevenueLastMonths(invoices, currency, maxMonths);

            this.SendAPIMessage("node-data", JObject.FromObject(node));
        }


        [RequestHandler("/api/admin/finance/report/business-revenues")]
        public void RevenueByBusiness() {
                    
            // Timerange
            var startDate = this.Params.DateTimeGlobalConfigFormat("start", DateTime.MinValue);
            var endDate = this.Params.DateTimeGlobalConfigFormat("end", DateTime.MinValue);

            if (startDate == DateTime.MinValue) {
                startDate = DateTime.UtcNow.ToDefaultTimezone().StartOfYear().ToUniversalTime();
            }

            if (endDate == DateTime.MinValue) {
                endDate = DateTime.UtcNow.ToDefaultTimezone().ToUniversalTime();
            }

            // Report -> HNT: DONT SEND REPORT BECAUSE DATE FILTER UI TOOL IS BETTER SUPPORTED WITH JUST CHART NODES
            //var report = new Report();
            //var section = new SectionNode();
            //report.AddToBody(section);

            var node = new DonutNode();
            node.SetTitle("Businesses");
            node.SetSubTitle("by Revenue");
            node.LegendLabelsShowValue = true;
            node.Theme = "default";

            int potsize = 7;

            // Sent invoices grouped by businesses
            // in time range
            var invoicesByBusiness = this.DB.Invoices()
                .Where(o => o.InvoiceSent != null)
                .Where(i=>i.InvoiceSent >= startDate && i.InvoiceSent < endDate)
                .GroupBy(i=>i.Business)
                .ToList()
                .Where(b => b.Sum(o => o.SubtotalCustomer?.Value.Value) > 0)
                .OrderByDescending(b => b.Sum(o => o.SubtotalCustomer.Value.Value));

            if (invoicesByBusiness.Any()) {

                if (invoicesByBusiness.Count() < potsize) {
                    potsize = invoicesByBusiness.Count();
                }

                foreach (var b in invoicesByBusiness.Take(potsize)) {
                    var costs = b.Sum(o => o.SubtotalCustomer.Value.Value);
                    node.AddSliceFormatWithThousandsNoDecimal(costs, b.Key.Name);
                }

                // Rest
                {
                    var rest = invoicesByBusiness.Skip(potsize);
                    var costs = rest.SelectMany(r => r).Sum(o => o.SubtotalCustomer.Value.Value);
                    node.AddSliceFormatWithThousandsNoDecimal(costs, "Others");
                }
            } else {
                node.AddSliceFormatWithThousandsNoDecimal(1, "No Data");
                node.Status = new ChartStatus("","No data","No Data in timerange");
            }

            //report.AddToBody(node);

            this.SendAPIMessage("node-data", JObject.FromObject(node));
        }


        [RequestHandler("/api/admin/finance/report/business/{publicId}/revenues")]
        public void RevenueForBusinessOverTime(string publicId) {
            var currency = Core.Config.CurrencyDefault;
            var business = this.DB.Businesses().GetByPublicId(publicId);

            var invoices = this.DB.Invoices()
                .Include(nameof(Invoice.Business))
                .AsQueryable();


            invoices = InvoiceFinanceAdminHandler.FilterByBusiness(invoices, business.PublicId);

            invoices = invoices.ToList().AsQueryable();

            var node = Dashboard.FinanceDashboard.RevenueLastMonths(invoices, currency, months: null);
            node.SetTitle("Revenue");
            node.SetSubTitle("by Month");

            this.SendAPIMessage("node-data", JObject.FromObject(node));
        }


        [RequestHandler("/api/admin/finance/report/invoice/{publicId}/categories")]
        public void DonutInvoiceCategories(string publicId) {


            var invoice = this.DB.Invoices()
                .Include(nameof(Invoice.LineItemGroups) + "." + nameof(LineItemGroup.LineItems))
                .GetByPublicId(publicId);


            var node = new DonutNode();
            node.Theme = "default";
            node.SetTitle("Project Costs");

            var subCategories = invoice.GetItemCategoryGrouped().FirstOrDefault();
            if (subCategories != null) {
                foreach (var subCategory in subCategories.SubCategories) {
                    node.AddSliceFormatWithThousandsNoDecimal((int)subCategory.Sum(c => c.CustomerSubtotal.Value), subCategory.Key);

                }
            }

            this.SendAPIMessage("node-data", JObject.FromObject(node));
        }





    }
}
