using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using System.Text;
using Machinata.Core.Model;
using Machinata.Module.Finance.Model;

namespace Machinata.Module.Finance.Handler {
    public class ItemsAdminAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<LineItemGroup> {

     


        [RequestHandler("/api/admin/finance/invoices/invoice/{invoiceId}/groups/create")]
        public void Create(string invoiceId) {

            var category = this.Params.String("category");
            var group = CRUDCreate("group");
            group.Category = category;

            var invoice = this.DB.Invoices()
            .Include(nameof(Invoice.LineItemGroups) + "." + nameof(LineItemGroup.LineItems))
            .GetByPublicId(invoiceId);

            invoice.LineItemGroups.Add(group);

            invoice.UpdateInvoice();

            this.DB.SaveChanges();


        }


        [RequestHandler("/api/admin/finance/invoices/invoice/{invoiceId}/groups/create-discount")]
        public void CreateDiscount(string invoiceId) {
            var amount = this.Params.Decimal("amount", 0);


            var group = CRUDCreate("group");
            group.Category = "{text.discount}";
            group.Title = "{text.discount}";
            var price = new Price(-amount);
            var item=  group.AddTotalDiscount(price);
            var invoice = this.DB.Invoices()
            .Include(nameof(Invoice.LineItemGroups) + "." + nameof(LineItemGroup.LineItems))
            .GetByPublicId(invoiceId);

            invoice.LineItemGroups.Add(group);

            invoice.UpdateInvoice();

            this.DB.SaveChanges();


        }


        [RequestHandler("/api/admin/finance/invoices/{invoiceId}/groups/{publicId}/delete")]
        public void Delete(string invoiceId, string publicId) {
            var group = CRUDDelete(publicId);

            var invoice = this.DB.Invoices()
                    .Include(nameof(Invoice.LineItemGroups) + "." + nameof(LineItemGroup.LineItems))
                    .GetByPublicId(invoiceId);

            invoice.UpdateInvoice();

            this.DB.SaveChanges();

        }

        [RequestHandler("/api/admin/finance/invoices/groups/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }



       

    }
}