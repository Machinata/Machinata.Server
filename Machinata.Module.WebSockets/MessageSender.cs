﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using WebSocketSharp;
using WebSocketSharp.Net;
using WebSocketSharp.Server;

namespace Machinata.Module.WebSockets {
    using Machinata.Core.Exceptions;
    using Newtonsoft.Json.Linq;
    using WebSocketSharp;
    using WebSocketSharp.Server;


    /// <summary>
    /// Websocket Behaviour which just broadcasts all received messages to all connected clients
    /// </summary>
    public class MessageSender : WebSocketBehavior {



        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();


        #endregion

        private string _usernameFilter;
        private Action<string> _messageReceivedCallback = null;

        public MessageSender() {
          
        }

        public MessageSender(string usernameFilter) {
            _usernameFilter = usernameFilter;
        }


        public MessageSender(Action<string> messageReceived) {
            _messageReceivedCallback = messageReceived;
        }

        public void SetMessageReceived(Action<string> messageReceived) {
            _messageReceivedCallback = messageReceived;
        }


        protected override void OnMessage(MessageEventArgs e) {
            // base.OnOpen();

            //if (this.Context?.User?.Identity?.Name != _usernameFilter && _usernameFilter != null) {
            //    throw new BackendException("access-denied", "Access denied for user", null, 403);

            //    this.Context.WebSocket.Close();
            //}
            if (_usernameFilter == null)
            {
                this.Sessions.Broadcast(e.Data);
            }
            else
            {
                foreach (var session in this.Sessions.Sessions)
                {
                    if (session.Context.User.Identity.Name == _usernameFilter && _usernameFilter != null)
                    {
                        this.Sessions.SendTo(e.Data, session.ID);
                    }
                }
            }

          

            if (_messageReceivedCallback != null) {

                try {
                    _messageReceivedCallback(e.Data);
                }
                catch (Exception exception) {
                    _logger.Error(exception, "OnMessage: exception from callback recepient");
                }
            }
        }


    }
}


namespace Machinata.Module.WebSockets {

    public class MessageWebsocketServer : IDisposable {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();


        #endregion


        private WebSocketServer _server;

        private string _url;

        private List<string> _paths = new List<string>();
      

        public void Dispose() {
            if (_server != null) {
                _server.Stop();
            }
        }
         

       /// <summary>
       /// Starts a websocket on the given ur
       /// </summary>
       /// <param name="url"></param>
       /// <param name="pathsUsernames">dictionary of path and allowed user to listen on</param>
       /// <param name="secure">Secure WSS server</param>
       /// <param name="GetPasswordForUsername"></param>
       /// <param name="certificate">SSL Certificate</param>
        public bool Start(string url, Dictionary<string,string> pathsUsernames, bool secure , Func<string, string> GetPasswordForUsername, X509Certificate2 certificate, Func<Dictionary<string,string>,string, bool> CookieValidation) {

            // Stop if already running
            Stop();

            _logger.Info("Starting Websocket at: " + url);
            _server = new WebSocketServer(url);
            _url = url;
            _paths = new List<string>();
            //_pathsUsernames = pathsUsernames;

            // Secure SSL Websocket?
            if (secure == true) {
            
                _server.SslConfiguration.ServerCertificate = certificate;

                // Use only tsl1.2 -> otherwise browser will complain
                _server.SslConfiguration.EnabledSslProtocols = System.Security.Authentication.SslProtocols.Tls12;
            }

            // Register all paths
            foreach (var pathsUsername in pathsUsernames) {
#pragma warning disable CS0618 // Type or member is obsolete

                _server.AddWebSocketService<MessageSender>(pathsUsername.Key, () => new MessageSender()
                {
                    CookiesValidator = (req, res) =>
                    {
                        // Check the cookies in 'req', and set the cookies to send to
                        // the client with 'res' if necessary.
                        //foreach (Cookie cookie in req) {
                        //    cookie.Expired = true;
                        //    res.Add(cookie);
                        //}
                        //return false; // If valid.
                      
                        var cookies = new Dictionary<string, string>();

                        foreach (Cookie cookie in req) {
                            cookies.Add(cookie.Name, cookie.Value);
                        }

                        return CookieValidation(cookies, pathsUsername.Value);

                       
                    }
                });
#pragma warning restore CS0618 // Type or member is obsolete

                _paths.Add(pathsUsername.Key);
            }
                _server.Start();

            _logger.Info("Websocket listening=" + _server.IsListening);
            _logger.Info("Websocket secure=" + _server.IsSecure);


            return _server.IsListening;
        }

        public void RemoveAllServices() {
            foreach (var path in _paths) {
                _server.RemoveWebSocketService(path);
            }
        }

        public void RemoveService(string path) {
            _server.RemoveWebSocketService(path);
        }

        public void AddService(string path, Action<string> messageReceived = null ) {
            _server.AddWebSocketService<MessageSender>(path, delegate(MessageSender s) { 
            
                if (messageReceived != null) {
                    s.SetMessageReceived(messageReceived);
                }
            
            });
            _paths.Add(path);
        }

        //private Action<MessageSender> CreateMessageSender(Action<string> messageReceived) {
        //    if (messageReceived == null) {
        //        return delegate (MessageSender s) {

        //        };
        //    } else {
        //        return delegate (MessageSender s) {
        //            s = new MessageSender(messageReceived);
        //        };
        //    }
        //}


        /// <summary>
        /// Starts a websocket on the given url at 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="pathsUsernames">dictionary of path and allowed user to listen on</param>
        /// <param name="secure">Secure WSS server</param>
        /// <param name="GetPasswordForUsername"></param>
        /// <param name="certificate">SSL Certificate</param>
        public bool Start(string url, bool secure, X509Certificate2 certificate, IEnumerable<string> paths, Action<string> messageReceived = null) {

            // Stop if already running
            Stop();

            _logger.Info("Starting Websocket at: " + url);
            _server = new WebSocketServer(url);
        
            _url = url;

            // Secure SSL Websocket?
            if (secure == true) {

                _server.SslConfiguration.ServerCertificate = certificate;

                // Use only tsl1.2 -> otherwise browser will complain
                _server.SslConfiguration.EnabledSslProtocols = System.Security.Authentication.SslProtocols.Tls12;
            }

            foreach (var path in paths) {
                AddService(path, messageReceived);
            }

            _server.Start();

            _logger.Info("Websocket listening=" + _server.IsListening);
            _logger.Info("Websocket secure=" + _server.IsSecure);


            return _server.IsListening;
        }


        /// <summary>
        /// Get the credentions for a username (Id.Name)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private static NetworkCredential FindCredentials(IIdentity id, Func<string,string> GetPasswordForUsername) {
            var name = id.Name;
            var username = id.Name;
            string password = GetPasswordForUsername(username);
            return new NetworkCredential(username, password);

        }


        /// <summary>
        /// Server send via client websocket to server
        /// </summary>
        /// <param name="path"></param>
        /// <param name="message"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public void SendMessage(string path, string message, string username, string password, bool useSecureConnection) {
            using (var ws = new WebSocket(_url + path)) {

                if (useSecureConnection) {
                    ws.SslConfiguration.EnabledSslProtocols = System.Security.Authentication.SslProtocols.Tls12;
                }
                ws.SetCredentials(username, password, true);

                //TODO refactor
                ws.SetCookie(new Cookie()
                {
                    Name = "ServerAuthCookie",
                    Value = password,
                    Expires = DateTime.UtcNow.AddDays(30)
                });
              

                ws.OnError += ErrorClient;
                ws.Connect();
                ws.Send(message);
            }
        }


        private void ErrorClient(object sender, ErrorEventArgs e) {
            var errorLog = Core.EmailLogger.CompileFullErrorReport(null, null, e.Exception);
            _logger.Error(e.Exception, "Websocket Send error: " + errorLog);
        }

       

        /// <summary>
        /// Stop the Socket
        /// </summary>
        public void Stop() {
            _server?.Stop();
            


        }
    }
}