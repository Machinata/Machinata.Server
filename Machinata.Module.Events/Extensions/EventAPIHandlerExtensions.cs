using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Module.Events.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Events.Extensions {
    public static class EventAPIHandlerExtensions {

        public static EventGuest AddGuestFromRequest(this APIHandler handler, Event eventEntity, bool allowAutoCheckin) {

            var checkin = handler.Params.Bool("checkin", false);
            var firstname = handler.Params.String("firstname");
            var lastname = handler.Params.String("lastname");
            if (firstname == null) {
                firstname = handler.Params.String("first-name");
            }
            if (lastname == null) {
                lastname = handler.Params.String("last-name");
            }

            var entourage = handler.Params.Int("entourage", 0);
            var company = handler.Params.String("company");
            var email = handler.Params.String("email", null);
            var source = handler.Params.String("source", null);

            if (string.IsNullOrWhiteSpace(firstname) && string.IsNullOrWhiteSpace(lastname)) {
                throw new BackendException("invalid-name", "Neither first nor last name provided.");
            }

            // Guest
            EventGuest guest = EventGuest.CreateGuest(firstname, lastname, entourage, company, email, source);

            // Checkin          
            if (checkin && allowAutoCheckin) {
                guest.Checkin();
            }

            // Add to event
            eventEntity.Guests.Add(guest);
            return guest;
        }

   
    }
}
