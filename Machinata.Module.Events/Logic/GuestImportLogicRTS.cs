using Machinata.Core.Model;
using Machinata.Module.Events.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Events.Logic {


    /// <summary>
    /// This was used for the JBC Fred Segal Event
    /// </summary>
    /// <seealso cref="Machinata.Module.Events.Logic.GuestImportLogic" />
    public class GuestImportLogicRTS : GuestImportLogic {

        public override void ImportRow(EventGuest guest, ModelContext db, Dictionary<string, string> data, int rowNumber) {
            guest.Address = new Address();

            string companyKey = "Address_Company";
            string address1Key = "Address_Address1";
            string address1NumberKey = "Address_Address1_Nr";
            string zipKey = "Address_Zip";
            string cityKey = "Address_City";

            // Check all Address header fields are resent
            if (rowNumber == 1) {
                CheckKey(data, companyKey);
                CheckKey(data, address1Key);
                CheckKey(data, address1NumberKey);
                CheckKey(data, zipKey);
                CheckKey(data, cityKey);
            }

            guest.Address.Name = guest.Name;
            guest.Address.Company = GetValue(data, companyKey);
            guest.Company = guest.Address.Company;
            guest.Address.Address1 = GetValue(data, address1Key);

            if (!string.IsNullOrEmpty(GetValue(data, address1NumberKey))) {
                guest.Address.Address1 = guest.Address.Address1 + " " + GetValue(data, address1NumberKey);
            }
            guest.Address.ZIP = GetValue(data, zipKey);
            guest.Address.City = GetValue(data, cityKey);

            // Accepted
            var accepted = GetValue(data, "Accepted");
            if (string.IsNullOrWhiteSpace(accepted)) {
                guest.Accepted = null;
            } else if (accepted.Trim() == "0") {
                guest.Accepted = false;
            } else if (accepted.Trim() == "1") {
                guest.Accepted = true;
            } else {
                throw new Exception($"Column Accepted Error: {accepted} is unknown");
            }
        }


        private static string GetValue(Dictionary<string, string> data, string key) {
            return data.ContainsKey(key) ? data[key] : string.Empty;
        }

        private static void CheckKey(Dictionary<string, string> data, string key) {
            if (!data.ContainsKey(key)) {
                throw new Exception($"XLSX doesn't contain the '{key}' column");
            }

        }

        public override string Name
        {
            get
            {
                return "RTS: Fred Segal";
            }
        }


    }
}

   
