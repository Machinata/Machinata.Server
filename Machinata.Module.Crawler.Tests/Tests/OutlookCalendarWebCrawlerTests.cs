using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Crawler.Tests {


    
    [TestClass]
    public class OutlookCalendarWebCrawlerTests {
        
        
        public const string CALENDAR_URL = "https://owa.burriag.ch/owa/calendar/3bf3a59c9a514dcfad31bf60f9335354@burri.world/c0d7c4de17b84b5b97d3103353203e1c5239749711070084347/calendar.html";


        [TestMethod]
        public void Crawler_OutlookCalendar_GetCalendar() {
            // This just tests a basic crawl
            var crawler = new Machinata.Module.Crawler.OutlookCalendarWebCrawler();
            var entries = crawler.GetCalendarEntries(CALENDAR_URL); // today
        }
        

    }
}
