using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Admin.View;
using Machinata.Module.Projects.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Machinata.Module.Projects.Model.ProjectCost;

namespace Machinata.Module.Projects.View {
    public static class ProjectsForms {

        public const string PROJECT_PROPOSALS_VIEW_FORM = "/proposals/view";
        public const string PROJECT_PROPOSALS_EDIT_FORM = "/proposals/edit";
        public const string PROJECT_PROPOSALS_DUPLICATE_FORM = "/proposals/duplicate";
        public const string PROJECT_PROPOSALS_LIST_EDIT_FORM = "/proposals/list-edit";
        public const string PROJECT_PROPOSALS_PDF_FORM = "/proposals/pdf";

        private static List<KeyValuePair<string, string>> _getFormOptionsForSubProjectSelection(Project project) {
            var opts = new List<KeyValuePair<string, string>>();
            var rootProject = project.GetRootProject();
            var subprojects = rootProject.GetAllSubProjects(true, true);
            foreach (var subproj in subprojects) {
                var tabs = "";
                var levelRelative = subproj.Level - rootProject.Level;
                for (var i = 0; i < levelRelative; i++) tabs += "\u00A0\u00A0";
                opts.Add(new KeyValuePair<string, string>(tabs + subproj.Name, subproj.PublicId));
            }
            return opts;
        }

        public static FormBuilder GetProjectCostFormBuilder(Project project, string projectCostDefault, DateTime defaultDate) {
            var form = new FormBuilder(Forms.Admin.EMPTY);
            form.DropdownList(
                "projectcosttype",
                "projectcosttype",
                    new List<KeyValuePair<string, string>>() { new KeyValuePair<string, string>("Internal", "Internal"), new KeyValuePair<string, string>("External", "External") },
                    projectCostDefault, required: true);
            form.Include(nameof(ProjectCost.Units));
            form.Include(nameof(ProjectCost.CostPerUnit));
            form.Include(nameof(ProjectCost.CostPerUnitInternal));
            form.Include(nameof(ProjectCost.Description));
            form.Include(nameof(ProjectCost.Billable));
            form.Include(nameof(ProjectCost.Note));
            form.Include(nameof(ProjectCost.ProjectCostType));
            // Subproject selection
            {
                var opts = _getFormOptionsForSubProjectSelection(project);
                form.DropdownList("Subproject", "project", opts, project.PublicId, true, null);
            }
            form.Custom("date", "date", "date", value: defaultDate, required: true, readOnly: false);
            return form;
        }

        public static FormBuilder GetTimesheetForm(ProjectCost cost, Project project, IEnumerable<ProjectWorkType> workTypes, IEnumerable<User> projectUsers, User defaultUser, DateTime date, decimal units, ProjectWorkType worktype) {
            var form = new FormBuilder(Forms.Admin.EMPTY);
            form.DropdownList("user", projectUsers, projectUsers.FirstOrDefault(pu => pu.Id == defaultUser?.Id));
            form.DropdownList("worktype", workTypes, worktype);
            form.Include(nameof(ProjectCost.Description));
            // Subproject selection
            {
                var opts = _getFormOptionsForSubProjectSelection(project);
                form.DropdownList("Subproject", "project", opts, project.PublicId, true, null);
            }
            form.Include(nameof(ProjectCost.User));
            form.Include(nameof(ProjectCost.Note));
            form.Include(nameof(ProjectCost.Billable));
            form.Include(nameof(ProjectCost.WorkType));
            //form.Include(nameof(ProjectCost.Overtime));
            form.Custom("date", "date", "datetime", date, true, false);
            form.Hidden("projectcosttype", ProjectCostTypes.Timesheet.ToString());
            form.Custom("units", "units", "decimal", units, true, false);
            form.Custom("overtime", "overtime", "bool", cost.Overtime, true, false);
            
            return form;
        }


        public static void PopulateProjectCost(ProjectCost projectCost, Machinata.Core.Handler.Handler handler ) {
            projectCost.Populate(handler,
                new FormBuilder(Forms.Admin.EMPTY)
                .Include(nameof(ProjectCost.Description))
                .Include(nameof(ProjectCost.User))
                .Include(nameof(ProjectCost.Units))
                .Include(nameof(ProjectCost.Billable))
                .Include(nameof(ProjectCost.Note))
                .Include(nameof(ProjectCost.ProjectCostType))
                .Include(nameof(ProjectCost.CostPerUnit))
                .Include(nameof(ProjectCost.CostPerUnitInternal))
                );
            // Subproject update
            {
                var projectId = handler.Params.String("project", null);
                var project = handler.DB.Set<Project>().GetByPublicId(projectId);
                if (!string.IsNullOrEmpty(projectId)) {
                    projectCost.Project = project;
                }
            }
            var startDate = handler.Params.DateTime("date", Core.Util.Time.GetDefaultTimezoneToday());
            projectCost.TimeRange = new DateRange(startDate, startDate);
            if (!projectCost.CostPerUnitInternal.HasValue) {
                projectCost.CostPerUnitInternal = projectCost.CostPerUnit.Clone();
            }
            

            projectCost.Validate();
        }

      

        public static FormBuilder GetProposalEditForm(ProjectCostProposal project, IEnumerable<ContentNode> appendicesNodes) {
            var form = new FormBuilder(ProjectsForms.PROJECT_PROPOSALS_EDIT_FORM);
            if (project.IsRootProject == false) {
                form.Exclude(nameof(Project.Business));
                form.Exclude(nameof(Project.Archived));

                // Subprojects have no date but TimeRange
                form.Exclude(nameof(project.Date));
                form.Exclude(nameof(project.DocumentTitle));
                form.Include(nameof(project.TimeRange));
            } else {
                var values = new List<KeyValuePair<string, string>>();
                foreach(var node in appendicesNodes) {
                    values.Add(new KeyValuePair<string, string>(node.Name,node.PublicId));
                }
                form.CheckboxList(
                    name: "appendices",
                    formName: "appendices",
                    options: values,
                    value: project.AppendixNodeIds,// hint: concated list
                    required: false
                    );


                // Billing Address
                if (project.Business != null) {
                    Address selectedAddress = null;
                    var addresses = project.Business.ActiveAddresses.ToList();
                    if (project.BillingAddress != null) {
                        selectedAddress = addresses.FirstOrDefault(a => a.Hash == project.BillingAddress.Hash); // is the current address still available in the business adreses?
                        if (selectedAddress == null) {
                            // if not available anymore we add the current Billing Address of the project to the selection
                            addresses.Add(project.BillingAddress);
                            selectedAddress = project.BillingAddress;
                        }
                    }
                    var addressesDict = addresses.Select(k => new KeyValuePair<string,string>(k.Name,k.Hash)).ToList();
                    form.DropdownList(nameof(project.BillingAddress), nameof(project.BillingAddress), addressesDict, value: selectedAddress?.Hash, required:true);
                    //form.DropdownListEntities(nameof(project.BillingAddress), addresses, selected: selectedAddress);
                }


            }
            form.Exclude(nameof(Project.Budget));
            return form;
        }
        public static FormBuilder GetProposalViewForm(ProjectCostProposal proposal) {
            var form = new FormBuilder(ProjectsForms.PROJECT_PROPOSALS_VIEW_FORM);


            // Sub projects
            if (proposal.IsRootProject == false) {
                form.Exclude(nameof(proposal.Date));
                form.Include(nameof(proposal.TimeRange));
                form.Exclude(nameof(proposal.DocumentTitle));
                form.Exclude(nameof(proposal.AppendicesTitles));
            }

            return form;
        }


        public static void PopulateTimesheet(ProjectCost projectCost, Machinata.Core.Handler.Handler handler) {
            projectCost.Populate(handler,
                new FormBuilder(Forms.Admin.EMPTY)
                .Include(nameof(ProjectCost.Description))
                .Include(nameof(ProjectCost.User))
                .Include(nameof(ProjectCost.Units))
                .Include(nameof(ProjectCost.Note))
             //   .Include(nameof(ProjectCost.Overtime))
                .Include(nameof(ProjectCost.Billable))
                .Include(nameof(ProjectCost.WorkType)));

            if ( projectCost.WorkType == null) {
                throw new BackendException("timesheet-error", "No worktype selected");
            }

            // Subproject update
            {
                var projectId = handler.Params.String("project", null);
                var project = handler.DB.Set<Project>().GetByPublicId(projectId);
                if (!string.IsNullOrEmpty(projectId)) {
                    projectCost.Project = project;
                }
            }
            // Hint: we used to interpret the date as the end date 14.06.2023
            var startDate = handler.Params.DateTime("date", Core.Util.Time.GetDefaultTimezoneToday());
           


            var isOvertime = handler.Params.Bool("overtime",false);
            if (isOvertime == true) {
                projectCost.OvertimeFactor = Config.ProjectsOvertimeFactor;
            } else {
                projectCost.OvertimeFactor = ProjectCost.OVERTIME_FACTOR_DEFAULT;
            }

            // Recalculate 
            projectCost.RecalculateCostsPerUnit();
             
            projectCost.Duration = TimeSpan.FromTicks((long)(projectCost.WorkType.GetUnitDuration().Ticks * projectCost.Units));

            var endDate = startDate + projectCost.Duration;
            projectCost.TimeRange = new DateRange(startDate, endDate);
            projectCost.ProjectCostType = ProjectCost.ProjectCostTypes.Timesheet;
            if (!projectCost.CostPerUnitInternal.HasValue) {
                projectCost.CostPerUnitInternal = projectCost.CostPerUnit.Clone();
            }


            projectCost.Validate();
        }


    }
}

