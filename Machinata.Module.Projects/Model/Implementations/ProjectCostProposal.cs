﻿using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Module.Projects.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Projects.Model {

    [Serializable()]
    [NotMapped]
    public class ProjectCostProposal : Model.Project {

        public enum Units {
            None,
            Hosting,
            Daily,
            Monthly,
            Units,
            Hourly

        }

        public enum Statuses : short{
            Created = 0,
            Pending = 10,
            Accepted = 20,
            Declined = 100,
            Archived = 1000,
        }

      
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_VIEW_FORM)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_EDIT_FORM)]
        public Units Unit {
            get {
                //return MapPropertyToJSONStore<ChallengeCategories>(nameof(Category)).Get();
                return MapPropertyToJSONStore<Units>(nameof(Unit), nameof(ImplementationJSON)).Get();
            }
            set {
                MapPropertyToJSONStore<Units>(nameof(Unit), nameof(ImplementationJSON)).Set(value);
            }
        }

        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_VIEW_FORM)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_EDIT_FORM)]
        public Price DefaultCostsPerUnit {  
            get {
                //return MapPropertyToJSONStore<ChallengeCategories>(nameof(Category)).Get();
                return MapPropertyToJSONStore<Price>(nameof(DefaultCostsPerUnit), nameof(ImplementationJSON)).Get(new Price(0));
            }
            set {
                MapPropertyToJSONStore<Price>(nameof(DefaultCostsPerUnit), nameof(ImplementationJSON)).Set(value);
            }
        }

        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_VIEW_FORM)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_EDIT_FORM)]
        public string DocumentTitle {
            get {
                //return MapPropertyToJSONStore<ChallengeCategories>(nameof(Category)).Get();
                return MapPropertyToJSONStore<string>(nameof(DocumentTitle), nameof(ImplementationJSON)).Get("Budget Proposal");
            }
            set {
                MapPropertyToJSONStore<string>(nameof(DocumentTitle), nameof(ImplementationJSON)).Set(value);
            }
        }

        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_VIEW_FORM)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_EDIT_FORM)]
        [FormBuilder(Forms.Admin.LISTING)]
        public Statuses Status {
            get {
                return MapPropertyToJSONStore<Statuses>(nameof(Status), nameof(ImplementationJSON)).Get();
            }
            set {
                MapPropertyToJSONStore<Statuses>(nameof(Status), nameof(ImplementationJSON)).Set(value);
            }
        }

        //[FormBuilder(ProjectsForms.PROJECT_PROPOSALS_VIEW_FORM)]
        //[FormBuilder(ProjectsForms.PROJECT_PROPOSALS_EDIT_FORM)]
        public Address BillingAddress {
            get {
                //return MapPropertyToJSONStore<ChallengeCategories>(nameof(Category)).Get();
                return MapPropertyToJSONStore<Address>(nameof(Address), nameof(ImplementationJSON)).Get();
            }
            set {
                MapPropertyToJSONStore<Address>(nameof(Address), nameof(ImplementationJSON)).Set(value);
            }
        }


        /// <summary>
        /// This saves a coma seperated value
        /// </summary>
        public string AppendixNodeIds {
            get {
                return MapPropertyToJSONStore<string>(nameof(AppendixNodeIds), nameof(ImplementationJSON)).Get();
            }
            set {
                MapPropertyToJSONStore<string>(nameof(AppendixNodeIds), nameof(ImplementationJSON)).Set(value);
            }
        }

        /// <summary>
        /// Sort
        /// </summary>
        public int Sort {
            get {
                return MapPropertyToJSONStore<int>(nameof(Sort), nameof(ImplementationJSON)).Get();
            }
            set {
                MapPropertyToJSONStore<int>(nameof(Sort), nameof(ImplementationJSON)).Set(value);
            }
        }

        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_VIEW_FORM)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_EDIT_FORM)]
        public DateTime? Date {
            get {
               if (this.TimeRange?.Start != null) {
                    return this.TimeRange.Start;
                }
                return this.Created;
            }
            set {
                this.TimeRange.Start = value;
                this.TimeRange.End = value;
            }
        }

        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_VIEW_FORM)]
        public string AppendicesTitles {
            get {
                var contentNodes = this.Context.ContentNodes().GetPagesAtPath(Project.COST_PROPOSAL_APPENDIX_PATH);
                contentNodes = contentNodes.Where(cn => this.AppendixNodeIdsList.Contains(cn.PublicId));
                return string.Join(", ", contentNodes.Select(cn => cn.Name));
            }
           
        }


        public string GetUnitName() {
            if (this.Unit == Units.None) {
                return string.Empty;
            }
            if (this.Unit == Units.Daily) {
                return "Days";
            }
            if (this.Unit == Units.Monthly) {
                return "Months";
            }
            if (this.Unit == Units.Hosting) {
                return "Months";
            }
            if (this.Unit == Units.Hourly) {
                return "Hours";
            }
            if (this.Unit == Units.Units) {
                return "Units";
            }
            return this.Unit.ToString();
        }

        public static string GetValueForProperty(ProjectCost cost, System.Reflection.PropertyInfo property, FormBuilder form, string language) {
            var value = property.GetValue(cost);
            var formProp = new EntityFormBuilderProperty(cost, property, form);
            if (property.Name == nameof(ProjectCost.Billable)) {
                if ((bool)value == true) {
                    return string.Empty;
                } else {
                    return "optional";
                }
                // Optional costs format:  ({costs}) ie “(CHF 100.00)”
                //  - and Units
            } else if ((property.Name == nameof(ProjectCost.Costs) 
                     || property.Name == nameof(ProjectCost.Units)) 
                && cost.Billable == false) {
                return "(" +  formProp.GetFormattedPropertyValue(language) + ")";
            }
            else {
                return formProp.GetFormattedPropertyValue(language);
            }
        }

        public IEnumerable<string> AppendixNodeIdsList {
            get {
                if (string.IsNullOrWhiteSpace(this.AppendixNodeIds) == true) {
                    return new List<string>();
                }
                var splits = this.AppendixNodeIds.Split(',');
                return splits.ToList();
            }
        }

     


    }

    //[NotMapped]
    //public class ProjectCosteEstimationEntity : ModelObject {
    //    [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_PDF_FORM)]
    //    public string Block { get; set; }
    //    [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_PDF_FORM)]
    //    public decimal Units { get; set; } = 0;
    //    [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_PDF_FORM)]
    //    public Price Costs { get; set; } = new Price(0);
    //}
}
