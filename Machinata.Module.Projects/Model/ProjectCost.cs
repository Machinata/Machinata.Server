using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using Machinata.Module.Finance.Model;
using System.Collections.Generic;
using Machinata.Module.Projects.View;

namespace Machinata.Module.Projects.Model {

    [Serializable()]
    [ModelClass]
    public partial class ProjectCost : ProjectDeletable {
        public const int OVERTIME_FACTOR_DEFAULT = 1;

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////

        public enum ProjectCostTypes : short {
            Timesheet = 10,
            External = 20,
            Internal = 30,
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public ProjectCost() {
            this.CostPerUnit = new Price();
            this.CostPerUnitInternal = new Price();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTEDIT)]
        [Column]
        public ProjectCostTypes ProjectCostType { get; set; }


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.Admin.LISTEDIT)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_PDF_FORM)]
        [Column]
        public string Description { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTEDIT)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_PDF_FORM)]
        [Column]
        public bool Billable { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.Admin.LISTEDIT)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_PDF_FORM)]
        [Decimal("0.##")]
        [Column]
        public decimal Units { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [FormBuilder(Forms.Admin.LISTEDIT)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_PDF_FORM)]
        [Column]
        public Price CostPerUnit { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        public Price CostPerUnitInternal { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [Column]
        public string Note { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EXPORT)]
        [DataType(DataType.DateTime)] 
        public DateRange TimeRange { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.EXPORT)]
        public TimeSpan Duration { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        public DateTime? Billed { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.PDF)]
        public User User { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.PDF)]
        [Decimal("0.##")]
        public decimal OvertimeFactor { get; set; } = OVERTIME_FACTOR_DEFAULT;

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        public int Sort { get; set; } 


        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        public Project Project { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.VIEW)]
        public ProjectWorkType WorkType { get; set; }

        [Column]
        public Finance.Model.Invoice Invoice { get; set; }

        [Column]
        public Finance.Model.LineItem LineItem { get; set; }

        [Column]
        public Core.Model.Business Business { get; set; }

        #endregion


        #region Derived Properties //////////////////////////////////////////////////////        

        [NotMapped]
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_PDF_FORM)]
        public Price Costs
        {
            get
            {
                if (this.CostPerUnit.HasValue) {
                    return this.CostPerUnit * this.Units;
                } else return new Price(0);

            }
        }

        [NotMapped]
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Admin.VIEW)]
        public Price CostsInternal
        {
            get
            {
                if (this.CostPerUnitInternal.HasValue) {
                    return this.CostPerUnitInternal * this.Units;
                } else return new Price(0);

            }
        }

        [NotMapped]
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EXPORT)]
        public string ProjectName
        {
            get { return this.Project?.Name; }
        }


        [NotMapped]
        [FormBuilder]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EXPORT)]
        public string BusinessName {
            get { return this.Business?.Name; }
        }

        [NotMapped]
        [FormBuilder]
        [FormBuilder(Forms.System.LISTING)]
        public string ProjectId
        {
            get { return this.Project?.PublicId; }
        }

        [NotMapped]
        [FormBuilder]
        [FormBuilder(Forms.System.LISTING)]
        public string ProjectPath
        {
            get { return this.Project?.Path; }
        }

        [FormBuilder]
        [FormBuilder(Forms.System.LISTING)]
        [NotMapped]
        public string URL
        {
            get
            {
                if (this.Project == null) {
                    return null;
                } else if (this.Project.ProjectType == Project.ProjectTypes.Project) {
                    return $"/admin/projects/project/cost/{this.ProjectPath}/{this.PublicId}";
                }
                else if (this.Project.ProjectType == Project.ProjectTypes.CostProposal || this.Project.ProjectType == Project.ProjectTypes.CostEstimationTemplate) {
                    return $"/admin/projects/cost-estimations/cost-estimation/cost/{this.ProjectPath}/{this.PublicId}";
                }
                throw new NotImplementedException(this.Project?.ProjectType + " url for this type of project cost not implemented");
            }
        }

        [FormBuilder]
        [FormBuilder(Forms.System.LISTING)]
        [NotMapped]
        public string EditURL {
            get {
                if (this.Project == null) {
                    return null;
                } else if (this.Project.ProjectType == Project.ProjectTypes.Project) {
                    return $"/admin/projects/project/cost-edit/{this.ProjectPath}/{this.PublicId}";
                } 
                else if (this.Project.ProjectType == Project.ProjectTypes.CostProposal) {
                    return null; // $"/admin/projects/project/cost-edit/{this.ProjectPath}/{this.PublicId}";
                }
                throw new NotImplementedException(this.Project?.ProjectType + " url for this type of project cost not implemented");
            }
        }

        [FormBuilder]
        public string Title
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.Description)) {
                    return this.ToString();
                }
                return this.Description;
            }
        }


        [Column]
        [FormBuilder()]
        [CustomFormatAttribute(@"dd\.hh\:mm")]
        public TimeSpan DurationShort
        {
            get
            {
                return this.Duration;
            }
        }


        [NotMapped]
        [FormBuilder]
        [FormBuilder(Forms.Admin.PDF)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [CustomFormat("{0:#,0.00}")]
        public Price CostsDefaultCurrency
        {
            get
            {
                return this.Costs.ConvertToCurrency(Core.Config.CurrencyDefault);
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public bool Overtime {
            get {

                return this.OvertimeFactor != OVERTIME_FACTOR_DEFAULT; 
            }
        }


        [NotMapped]
        [FormBuilder()]
        public bool IsExternal  {
            get { return this.ProjectCostType == ProjectCostTypes.External; }
        }


        [NotMapped]
        [FormBuilder()]
        public bool IsBilled {
            get { return this.Billed != null; }
        }

        [NotMapped]
        public string TempID { get; set; } // for edit line items proposal needed;

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Finishes/closes a time tracking cost
        /// </summary>
        public void Finish() {
            this.TimeRange.End = DateTime.UtcNow;

            UpdateDurationAndUnits();

            var minDurationFeature = false; // 13.11.2023: we dont do this anymore if we have a minimum tracking time
            if (minDurationFeature == true) {
                // Set billable false if we have timesheet costs smaller than one minute
                // so they wont end up in invoices
                var minDurationForBillable = new TimeSpan(0, 1, 0);
                if (this.ProjectCostType == ProjectCostTypes.Timesheet && this.Duration < minDurationForBillable) {
                    this.Billable = false;
                }
            }

            // But we might wanna have minimum time if above one minutes and round it up
            var minDurationToRoundUpTo = new TimeSpan(0, Config.ProjectsTimesheetCostMinimumRoundUpMinutes.HasValue ? Config.ProjectsTimesheetCostMinimumRoundUpMinutes.Value : 0, 0);
            if (this.Billable == true && minDurationToRoundUpTo.Ticks > 0 && this.Duration < minDurationToRoundUpTo) {
                var minimumEndDate = this.TimeRange.Start.Value + minDurationToRoundUpTo;
                this.TimeRange.End = minimumEndDate;
                UpdateDurationAndUnits(); // reset duration and units
            }


            RecalculateCostsPerUnit();
        }

        private void UpdateDurationAndUnits() {
            this.Duration = this.TimeRange.End.Value - this.TimeRange.Start.Value;
            this.Units = ((decimal)this.Duration.Ticks / (decimal)this.WorkType.GetUnitDuration().Ticks);
        }

       

        public void RecalculateCostsPerUnit() {

            // Check worktype
            if (this.WorkType == null) {
                throw new Exception("No Worktype is defined");
            }

            this.CostPerUnit = this.WorkType.CostPerUnit.Clone() * this.OvertimeFactor;
            this.CostPerUnitInternal = this.WorkType.CostPerUnitInternal.Clone() * this.OvertimeFactor;
        }






        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override Core.Cards.CardBuilder VisualCard() {
            var title = this.ProjectCostType.ToString();
            if (this.Description != null) title += ": " + Core.Util.String.CreateSummarizedText(this.Description, 15, true, true);
            return new Core.Cards.CardBuilder(this)
                .Title(title)
                .Subtitle($"{"project".TextVar()}: {this.Project?.Name}")
                .Tag(new Price(this.Units * this.CostPerUnit.Value,this.CostPerUnit.Currency).ToString());
            
    }

        public static IQueryable<ProjectCost> GetCostsForInvoice(Invoice invoice) {
            var allCosts = invoice.Context.ProjectCosts()
                .Include(nameof(ProjectCost.Invoice))
                .Include(nameof(ProjectCost.Business));
            return allCosts.Where(c => c.Invoice.Id == invoice.Id);
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextProjectCostExtensions {
        [ModelSet]
        public static DbSet<ProjectCost> ProjectCosts(this Core.Model.ModelContext context) {
            return context.Set<ProjectCost>();
        }

        [ModelSet]
        public static IQueryable<ProjectCost> ProjectOpenTimesheetCosts(this Core.Model.ModelContext context, User user, params string [] includePaths) {
            var query = context.Set<ProjectCost>()
                .Include(nameof(ProjectCost.User))
                .Include(nameof(ProjectCost.Project))
                .Include(nameof(ProjectCost.WorkType));
            foreach(var path in includePaths) {
                query.Include(path);
            }
            return query.Where(c => c.User.Id == user.Id && c.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet && c.TimeRange.End == null && c.TimeRange.Start != null); ;
        }

    }

    #endregion

}
