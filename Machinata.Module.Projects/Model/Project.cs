using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Text;
using Machinata.Core.Cards;
using Machinata.Module.Projects.Logic;
using Machinata.Module.Finance.Model;
using Machinata.Core.Lifecycle;
using Machinata.Module.Projects.View;
using static Machinata.Module.Projects.Model.ProjectCostProposal;

namespace Machinata.Module.Projects.Model {

    [Serializable()]
    [ModelClass]
    public partial class Project : ProjectDeletable, IPublishedModelObject {

        public const string COST_PROPOSAL_APPENDIX_PATH = "/Misc/Projects/Proposals/Appendices";
        public const string COST_PROPOSAL_NO_BUSINESS_SHORT_URL = "no-business";
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////

        public enum ProjectTypes : short {
            Project = 10,
            CostProposal = 20,
            CostEstimationTemplate = 30,
            Special = 100
        }

        public enum ProjectReportingTypes : short {
            Off = 0,
            Weekly = 10,
            Monthly = 20,
            Daily = 30
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Project() {
            this.TimeRange = new DateRange();
            this.Tasks = new List<ProjectTask>();
            this.WorkTypes = new List<ProjectWorkType>();
            this.Costs = new List<ProjectCost>();
            this.Events = new List<ProjectEvent>();
            this.Budget = new Price();
            this.Invoices = new List<Invoice>();
            this.Notifications = new Properties();
           

        }

      
        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder(Forms.API.LISTING)]
        //[FormBuilder(Forms.Admin.VIEW)]
        //[FormBuilder(Forms.Admin.CREATE)]
        //[FormBuilder(Forms.Admin.EDIT)]
        //[FormBuilder(Forms.Admin.LISTING)]
        //[FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public ProjectTypes ProjectType { get; set; }
        
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
      
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTEDIT)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_LIST_EDIT_FORM)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_PDF_FORM)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_VIEW_FORM)]
        [Column]
        public string Name { get; set; }
        
        [FormBuilder(Forms.API.LISTING)]
        [Column]
        public string ShortURL { get; set; }
        
        [FormBuilder(Forms.API.LISTING)]
        [Column]
        public string ShortURLPath { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
       // [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_VIEW_FORM)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        //[FormBuilder(ProjectsForms.PROJECT_PROPOSALS_EDIT_FORM)]
        //[FormBuilder(ProjectsForms.PROJECT_PROPOSALS_DUPLICATE_FORM)]
        [DataType(DataType.Date)] 
        public DateRange TimeRange { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_VIEW_FORM)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_EDIT_FORM)]
        [FormBuilder(Forms.Admin.EDIT)]
        [CascadeDelete]
        [Column]
        public ContentNode Description { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_VIEW_FORM)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_EDIT_FORM)]
        [FormBuilder(Forms.Admin.EDIT)]
        [CascadeDelete]
        [Column]
        public ContentNode Notes { get; set; }

        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_VIEW_FORM)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_EDIT_FORM)]
        [ContentTypeAttribute("color")]
        [Column]
        public string Color { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool Timeline { get; set; } = true;

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool Sticky { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool CostsOnlyOnSubprojects { get; set; } = true;


        [Column]
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool Completed { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_VIEW_FORM)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_EDIT_FORM)]
        public bool Archived { get; set; }
        
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_VIEW_FORM)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_EDIT_FORM)]
        [FormBuilder(Forms.Admin.LISTEDIT)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_LIST_EDIT_FORM)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_PDF_FORM)]
        [Column]
        public Price Budget { get; set; }

        [Column]
        public Properties Notifications { get; set; }
        
        [Column]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder()]
        [Required]
        public ProjectReportingTypes AutomaticReporting { get; set; }

        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [Column]
        [DataType("addressbook")]
        public string NotificationEmails { get; set; }

        [Column]
        [DefaultPropertyMappingJSONStore]
        public string ImplementationJSON { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        [ForeignKey("ParentId")]
        public Project Parent { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(ProjectsForms.PROJECT_PROPOSALS_EDIT_FORM)]
        [ForeignKey("Business_Id")]
        public Business Business { get; set; }
        
        [Column]
        public int? ParentId { get; set; }

        [Column]
        public int? Business_Id { get; set; }

        [Column]
        [InverseProperty("Parent")]
        public ICollection<Project> Children { get; set; }
        
        [Column]
        public ICollection<ProjectEvent> Events { get; set; }
        
        [Column]
        public ICollection<ProjectTask> Tasks { get; set; }
        
        [Column]
        public ICollection<ProjectWorkType> WorkTypes { get; set; }
        
        [Column]
        public ICollection<ProjectCost> Costs { get; set; }
        
        [Column]
        public ICollection<ProjectUser> Users { get; set; }

        [Column]
        public ICollection<Invoice> Invoices { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder(Forms.API.LISTING)]
        [NotMapped]
        public DateTime? Start {
            get {
                return this.TimeRange.Start;
            }
        }
        
        [FormBuilder(Forms.API.LISTING)]
        [NotMapped]
        public DateTime? End {
            get {
                return this.TimeRange.End;
            }
        }
        
        [NotMapped]
        public bool Published {
            get {
                return this.Archived == false;
            }
        }
        
        [NotMapped]
        [FormBuilder]
        public bool IsRootProject {
            get {
                return this.ParentId == null;
            }
        }

        [FormBuilder(Forms.System.LISTING)]
        [NotMapped]
        public string Path {
            get {
                return $"{this.PublicId}/{this.ShortURLPath}";
            }
        }
        
        /// <summary>
        /// Admin url
        /// </summary>
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.System.LISTING)] 
        [NotMapped]
        public string URL {
            get {

                if (this.ProjectType == ProjectTypes.Project) {
                    return $"/admin/projects/project/{this.Path}";
                }
                if (this.ProjectType == ProjectTypes.CostProposal || this.ProjectType == ProjectTypes.CostEstimationTemplate) {
                    return $"/admin/projects/cost-estimations/cost-estimation/{this.Path}";
                }
                throw new NotImplementedException("URL for project type not supported");
            }
        }

        [NotMapped]
        [FormBuilder]
        public int Level
        {
            get
            {
                // return this.ShortURLPath.Count(s => s == '/') - 1;
                int level = 0;
                Project project = GetParentProject(this);
                while (project != null) {
                    project = GetParentProject(project);
                    level++;
                }
                return level;
            }
        }

        public bool IsOverdue
        {
            get
            {
                return !this.Archived && !this.Completed && this.TimeRange.HasValue() && this.TimeRange.End.HasValue && this.TimeRange.End.Value < DateTime.UtcNow;
            }
        }

        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.System.LISTING)]
        [NotMapped]
        public string BusinessFormalName {
            get {
                return this.Business?.FormalName;
            }
        }


        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.System.LISTING)]
        [NotMapped]
        public string ReportURL {
            get {
                return $"/admin/projects/project/report/{this.Path}";
            }
        }

        [NotMapped]
        public string ShortDescription {
            get {
                
                var description = (this).GetDescription(includeDescription: false);
                    
                if (description == null) {
                    return null;
                }
                var shortDescription = Core.Util.String.CreateSummarizedText(description, 200, true, true, true);
                return shortDescription;
               
            }
        }

        #endregion

        /// <summary>
        /// Hidden feature in EntityListInsert
        /// </summary>
        [NotMapped]
        public string StyleClass { get; set; }


        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public Project LoadImplementation() {
            return this.ModelObjectTypeImplementer<Project>(nameof(this.ProjectType)).Load();
        }


        public void SaveImplementation(Project impl) {
            ModelObjectTypeImplementer<Project>(nameof(this.ProjectType)).Save(impl);
        }

        public ProjectBudget CalculateBudget(ProjectBudget.BudgetTypes type) {
          //  if (this.Budget == null || this.Budget.HasValue == false) return null;

            var budget = new ProjectBudget();
            IList<ProjectCost> costs = null;

            if (type == ProjectBudget.BudgetTypes.External) {
                costs =  this.GetAllCosts(hideDeleted: true).Where(c=>c.Billable).ToList();
              
                budget.TotalCosts = costs.Sum(c => c.Costs);
                var timesheetCosts = costs.Where(c => c.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet);
                budget.TotalTimesheetCosts = timesheetCosts.Sum(c => c.Costs);


            }
            else if (type == ProjectBudget.BudgetTypes.Internal) {
                costs = this.GetAllCosts(hideDeleted: true).ToList();
                budget.TotalCosts = costs.Sum(c => c.CostsInternal);
                var timesheetCosts = costs.Where(c => c.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet);
                budget.TotalTimesheetCosts = timesheetCosts.Sum(c => c.Costs);
            }

            budget.TotalBudget = this.Budget;
         
            if (budget.TotalCosts == null || budget.TotalCosts.HasValue == false) {
                budget.TotalCosts = new Price(0); 
            }

            if (budget.TotalTimesheetCosts == null || budget.TotalTimesheetCosts.HasValue == false) {
                budget.TotalTimesheetCosts = new Price(0);
            }

            budget.TotalHours = (decimal)costs.Where(c => c.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet).Sum(c => c.Duration.TotalHours);

            if (this.Budget != null && this.Budget.HasValue) {
                budget.RemainingBudget = budget.TotalBudget - budget.TotalCosts;

                //Estimated Budged
                if (budget.TotalHours > 0 && budget.TotalCosts != null && budget.TotalCosts.Value > 0) {
                    var hourlyAverage = budget.TotalCosts.Value / budget.TotalHours;
                    budget.EstimatedRemainingHours = System.Math.Round(budget.RemainingBudget.Value.Value / hourlyAverage.Value);
                    budget.EstimatedBudgetedHours = System.Math.Round(budget.TotalBudget.Value.Value / hourlyAverage.Value);
                }

                budget.OverBudget = null;
                if (budget.TotalCosts > budget.TotalBudget) budget.OverBudget = budget.TotalCosts - budget.TotalBudget;
            }

           
        

         

            return budget;
        }

        public void ChangeName(ModelContext db,string newName, bool updateAllSubProjectsAlso = true, bool checkSiblingName = true, bool updateBusiness = false) {
            // Update name
            this.Name = newName;
                  
            // Check name available
            if (checkSiblingName ) {    
                var siblings = this.GetSiblings(db).ToList();
                var siblingsSameName = siblings.Where(c => c.Name == newName);
                if (siblingsSameName.Any()) {
                    throw new BackendException("name-taken", "Name is already taken by a sibling: " + newName);
                }
            }

            // Update ShortURLs
            if (this.IsRootProject) {
                if(this.Business == null && this.Context != null) this.Include(nameof(Project.Business));
                var businesShortURL = this.Business?.ShortURL;
                if (businesShortURL == null) { businesShortURL = COST_PROPOSAL_NO_BUSINESS_SHORT_URL; } // if a proposal without a business
                this.ShortURL = businesShortURL + "/" + Core.Util.String.CreateShortURLForCamelCased(newName);
                this.ShortURLPath = this.ShortURL;
            } else {
                this.ShortURL = Core.Util.String.CreateShortURLForCamelCased(newName);
                this.ShortURLPath = this.Parent.ShortURLPath + "/" + this.ShortURL;
            }
            // Update all children
            if (updateAllSubProjectsAlso) {
                this.Include(nameof(Project.Children));
                foreach (var subproject in this.Children.Where(c => c.Deleted == false)) {
                    if (updateBusiness == true) {
                        subproject.Business_Id = this.Business_Id;
                    }
                    subproject.ChangeName(db, subproject.Name,true,false, updateBusiness: updateBusiness);
                }
            }
        }

        /// <summary>
        /// Gets the siblings of a project.
        /// If root project -> all root projects of same business
        /// </summary>
        /// <value>
        /// The siblings.
        /// </value>
        public IEnumerable<Project> GetSiblings(ModelContext db) {
            if (this.IsRootProject) {
                var business = GetProjectBusiness();
                if (business != null) { // empty busines should be allow in proposals
                    return db.Projects().Where(p => p.ParentId == null && p.Business.Id == business.Id && p.Id != this.Id && p.ProjectType == this.ProjectType && p.Deleted == false);
                }
                // We assume all root projects without business are siblings:
                return db.Projects().Where(p => p.ParentId == null && p.Business == null && p.Id != this.Id && p.ProjectType == this.ProjectType && p.Deleted == false);
            }
            if (this.Context != null) {
                this.Include(nameof(Project.Parent));
            }
            this.Parent.Include(nameof(Project.Children));
            return this.Parent.Children.Where(p=>p.Id != this.Id && p.Deleted == false);
        }



        /// <summary>
        /// With recursion
        /// </summary>
        /// <param name="includeSelf"></param>
        /// <param name="includePaths"></param>
        /// <returns></returns>
        public List<Project> GetAllSubProjects(bool includeSelf, bool hideDeleted, params string[] includePaths) {

            //TODO: @micha: this is probably what is causing the projects to run slowly no? Couldn't we just select all based on path for most cases, that would be blazingly fast. For some special cases like cleanup or renames etc we could use the recursion method...
            //                                                                       V
            var allProjects = GetAllProjectsOfBusiness(hideDeleted, includePaths).ToList();
            // For example:
            // var subprojects = GetAllProjectsOfBusiness(hideDeleted, includePaths).Where(p => p.Path.Includes(this.Path+"/"));

            var self = allProjects.FirstOrDefault(p => p.Id == this.Id);
            var projects = self.GetAllSubProjectRecursion(hideDeleted, allProjects, includeSelf);
           
            return projects;
          
        }

        private List<Project> GetAllSubProjectRecursion(bool hideDeleted ,IEnumerable<Project> allProjects, bool includeSelf = true) {
            var projects = new List<Project>();

            var self = allProjects.FirstOrDefault(p => p.Id == this.Id);
            if (self == null) {
                throw new Exception("Project has wrong Business: " + this.ShortURLPath);
            }
            if (includeSelf == true) {
                projects.Add(self);
            }
            if (self.Children != null) {
                var children = this.Children.AsQueryable();
                if (hideDeleted == true) {
                    children = children.Where(c => c.Deleted == false);
                }
                foreach (var child in children) {
                    projects.AddRange(child.GetAllSubProjectRecursion(hideDeleted, allProjects, true));
                }
            }
            return projects;
        }


        private IQueryable<Project> GetAllProjectsOfBusiness(bool hideDeleted, params string[] includePaths) {

            var projects = this.Context.Projects().Include(nameof(Project.Children)).AsQueryable();
            if (includePaths != null) {
                foreach (var includePath in includePaths) {
                    projects.Include(includePath);
                }
            }

            projects = projects.Where(p => p.Business_Id == this.Business_Id);

            if (hideDeleted == true) {
                projects = projects.Where(p => p.Deleted == false);
            }

            return projects;
        }

        /// <summary>
        /// Gets all costs of this and all sub projects
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProjectCost> GetAllCosts(bool hideDeleted, params string[] costsIncludePaths) {

            var includePahts = new List<string>() { nameof(Project.Costs) };
            if (costsIncludePaths != null) {
                foreach (var costIncludePath in costsIncludePaths) {
                    includePahts.Add(nameof(Project.Costs) + "." + costIncludePath);
                }
            }


            var projects = GetAllSubProjects(includeSelf: true, hideDeleted: hideDeleted, includePahts.ToArray());

            var costs = projects.SelectMany(p => p.Costs);

            if (hideDeleted == true) {
                costs = costs.Where(c => c.Deleted == false);
            }
            return costs;
        }


        // Get all costs faster?
        public IQueryable<ProjectCost> GetAllCostsOfProject(ModelContext db, bool hideDeleted) {
            var costsOfProject = this.GetAllCosts(hideDeleted).Select(c => c.Id);
            var costs = db.ProjectCosts()
                .Include(nameof(ProjectCost.User))
                .Include(nameof(ProjectCost.WorkType))
                .Where(e => costsOfProject.Contains(e.Id))
                .ToList()
                .AsQueryable();
           
            if (hideDeleted == true) {
                costs = costs.Where(c => c.Deleted == false);
            }

            return costs;
        }

        /// <summary>
        /// Gets all ubilled costs of this and all sub projects
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProjectCost> GetUnbilledProjectCosts(bool hideDeleted) {
            // Open Orders
            return this.GetAllCosts(hideDeleted: hideDeleted).Where(c => c.Billed.HasValue == false && c.Billable );
        }



        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        
        //public IEnumerable<ProjectCost> GetCostsForProjectsWithSubprojects(bool hideDeleted, bool onlyDirect) {
        //    // Open Orders
        //    var allCosts =  this.GetAllCosts(hideDeleted: hideDeleted);

        //    var costsForProjectsWithNoSubprojects = allCosts.Where(c=>c.Project.GetAllSubProjects(false, hideDeleted).Count() > 0);

        //    return costsForProjectsWithNoSubprojects;
        //}


        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>

        public bool HasDirectCostsAndSubprojects() {
            var directCosts = this.Costs.Where(c => c.Deleted == false);

            if (directCosts.Any() == false) {
                return false;
            }
                    

            if (this.Children.Any(c=>c.Deleted == false)) {
                return true;
            }

            return false;
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Project> GetAndLoadDirectSubrojects() {
            // Open Orders
            this.Include(nameof(this.Children));
            var children = this.Children.Where(c => c.Deleted == false);

            //var costsForProjectsWithNoSubprojects = allCosts.Where(c => c.Project.GetAllSubProjects(false, hideDeleted).Count() > 0);

            return children;
        }

        /// <summary>
        /// Gets all events of this and all sub projects
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProjectEvent> GetAllEvents(bool hideDeleted) {
         
            var projects = GetAllSubProjects(includeSelf: true, hideDeleted: hideDeleted, nameof(Project.Events) + "." + nameof(ProjectEvent.Description));
            var events =  projects.SelectMany(p => p.Events);

            if (hideDeleted == true) {
                events = events.Where(e => e.Deleted == false);
            }

            return events;
        }

        /// <summary>
        /// Gets all tasks of this and all sub projects
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProjectTask> GetAllTasks(bool hideDeleted) {
           
            var projects = GetAllSubProjects(includeSelf: true, hideDeleted: hideDeleted
                , nameof(Project.Tasks) + "." + nameof(ProjectTask.User)
                , nameof(Project.Tasks) + "." + nameof(ProjectTask.Description));
            var tasks =  projects.SelectMany(p => p.Tasks);

            if (hideDeleted == true) {
                tasks = tasks.Where(t => t.Deleted == false);
            }

            return tasks;
        }

        /// <summary>
        /// Gets the work types of the projects root project
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProjectWorkType> GetWorkTypes() {
            Project root = this;
            if (!this.IsRootProject) {
                root = this.GetRootProject();
            }
            root.Include(nameof(Project.WorkTypes));
            return root.WorkTypes;
        }
                
        public Project GetRootProject() {
            if (this.IsRootProject) return this;

            if (this.Parent == null) this.Include(nameof(Project.Parent));
            return this.Parent.GetRootProject();
        }

        public static Project GetParentProject(Project project) {
            if (project.IsRootProject) return null;
            if (project.Parent == null) project.Include(nameof(Project.Parent));
            return project.Parent;
        }

        /// <summary>
        /// Gets all projectusers of its root project
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProjectUser> GetProjectUsers(bool includeUser = false) {
            var rootProject = this.GetRootProject();
            rootProject.Include(nameof(Project.Users));
            if (includeUser) {
                foreach (var projectUser in rootProject.Users) {
                    projectUser.Include(nameof(projectUser.User));
                }
            }
            return rootProject.Users;
        }

        public IEnumerable<ProjectUserFlat> GetProjectUsersAndNotificationEmails(bool includeUser = false) {
            var users = GetProjectUsersFlatFromUsers();

            var additional = GetProjectUsersFlatFromString(this.NotificationEmails);

            var combined =  users.Concat(additional);
            return combined.DistinctBy(u => u.Email);
        }

        private IEnumerable<ProjectUserFlat> GetProjectUsersFlatFromUsers() {
            var users =  GetProjectUsers(true).ToList();
            foreach(var user in users) {
                var userFlat = new ProjectUserFlat();
                userFlat.Name = user.User.Name;
                userFlat.Email = user.User.Email ;
                userFlat.Enabled = user.User.Enabled;
                userFlat.User = user.User;
                yield return userFlat;
            }
        }

        public static  IEnumerable<ProjectUserFlat> GetProjectUsersFlatFromString(string users) {
            if (string.IsNullOrWhiteSpace(users) == false) {
                var addresses = users.Split(new char[]{ ','}, StringSplitOptions.RemoveEmptyEntries);
                foreach(var address in addresses) {
                    var cleaned = address.Trim();

                    var user = new ProjectUserFlat();
                    user.Name = cleaned;
                    user.Email = cleaned;
                    user.Enabled = true;
                    yield return user;
                }
            }
        }

        public Business GetProjectBusiness() {
            var rootProject = this.GetRootProject();
            if (this.Context != null) {
                rootProject.Include(nameof(Project.Business));
            }
            return rootProject.Business;
        }


        /// <summary>
        /// Gets all Invoices of this and all sub projects
        /// </summary>
        /// <returns></returns>
        //public IEnumerable<Invoice> GetAllInvoices() {
        //    var projects = GetAllSubProjects(true, nameof(Project.Costs));
        //    var costs = projects.SelectMany(p => p.Costs).ToList();
        //    costs.ForEach(p => p.Include(nameof(ProjectCost.Invoice)));
        //    return costs.Where(c => c.Invoice != null).Select(c => c.Invoice).Distinct();
        //}


        /// <summary>
        /// Gets all Invoices of this and all sub projects,
        /// HINT: the entities are already loaded at return
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Invoice> GetAllInvoicesFast() {
            //var projects = GetAllSubProjects(includeSelf: true, hideDeleted: true, nameof(Project.Costs));
            //var costs = projects.SelectMany(p => p.Costs).ToList();
            //var costIds = costs.Select(c => c.Id);
            //var costsWithInvoice =  this.Context.ProjectCosts().Include(nameof(ProjectCost.Invoice)).Where(c => costIds.Contains(c.Id));

            IEnumerable<Invoice> byCosts;
            {
                var projects = GetAllSubProjects(true, true, nameof(Project.Costs)).ToList();
                var costs = projects.SelectMany(p => p.Costs).ToList();
                var costIds = costs.Select(c => c.Id);
                var costsWithInvoice = this.Context.ProjectCosts()
                    .Include(nameof(ProjectCost.Invoice))
                    .Where(c => costIds.Contains(c.Id));
                // old way
                byCosts = costsWithInvoice.Where(c => c.Invoice != null).Select(c => c.Invoice);
            }

            IEnumerable<Invoice> byProjects;
            {
                // Hint: using one GetAllSubProjects(...) for both does not work
                var projects = GetAllSubProjects(true, true, nameof(Project.Invoices)).ToList();
                //new: we have a direct reference to the invoice (there might be empty mnaual invoices which are based on project)
                byProjects = projects.SelectMany(p => p.Invoices);
            }

            var all = (byCosts.Union(byProjects)).DistinctBy(i => i.Id);

            return all;
        }


        public void AddNewProjectUser(User user) {
            var newUser = new ProjectUser();
            newUser.User = user;
            newUser.Flags = ProjectUser.ProjectUserFlags.Subscribed;
            this.Users.Add(newUser);
        }


        // Subscribe to Invoice Status changes
        [OnApplicationStartup]
        public static void OnApplicationStartup() {
            Business.BusinessNameChangedEvent += Project.HandleBusinessNameChanged;
        }

        public static void HandleBusinessNameChanged(Business business, string oldName) {
            var projects = business.Context.Projects()
                .Include(nameof(Project.Business))
                .Where(p => p.ParentId == null && p.Business_Id == business.Id).ToList();

            foreach (var project in projects) {
                project.ChangeName(business.Context, project.Name);
            }
        }

        /// <summary>
        /// Set the color of the project if business has one
        /// </summary>
        public void SetColorFromBusiness() {
            if (this.Business?.Settings.Keys.Contains(Config.BUSINESS_COLOR_PROPERTY_KEY) == true) {
                this.Color = this.Business.Settings[Config.BUSINESS_COLOR_PROPERTY_KEY]?.ToString();
            }
        }

        public static ProjectColor GetColor(Business business) {
            if (business.Settings.Keys.Contains(Config.BUSINESS_COLOR_PROPERTY_KEY) && !string.IsNullOrEmpty(business.Settings[Config.BUSINESS_COLOR_PROPERTY_KEY]?.ToString())) {
                return  new ProjectColor() { Color = business.Settings[Config.BUSINESS_COLOR_PROPERTY_KEY]?.ToString() };
            }
            return new ProjectColor();
        }

        public string GetCostsPDFFileName() {
            var name = $"{Core.Config.ProjectID}_Costs_{this.Name}";
            if (!string.IsNullOrEmpty(this.Business?.Name)) {
                name += "_" + this.Business.Name;
            }
            return name + ".pdf";
        }

        public string GetProjectProposalPDFFileName(string documentTitle) {
            var name = $"{Core.Config.ProjectID}";
            if (!string.IsNullOrEmpty(documentTitle)) {
                name += "_" + documentTitle;
            } else {
                name += "_CostProposal";
            }
            name += "_" + this.Name;

            if (!string.IsNullOrEmpty(this.Business?.Name)) {
                name += "_" + this.Business.Name;
            }
            
            return name + ".pdf";
        }

        /// <summary>
        /// Gets the path from root to its own to show in Invoices etc recursevley
        /// Slow!
        /// </summary>
        /// <returns></returns>
        public string GetFullyQualifiedName(string separator = " / ") {
            var list = new List<Project>();
            var path = GetPath(this, list);
            return string.Join(separator, list.Select(l=>l.Name));

        }

        public IEnumerable<Project> GetPath(Project project, List<Project> projects) {
            this.Include(nameof(project.Parent));

            projects.Insert(0,project);
            if (project.Parent != null) {
                return GetPath(project.Parent, projects);
            }
            return projects;

        }

        /// <summary>
        /// Gets the timerange (localtime) string for the report depending on ReportCycle
        /// </summary>
        /// <returns></returns>
        public string GetCurrentTimeRangeString(DateTime utcNow) {
            DateRange dateRange = GetCurrentTimeRangeForReporting(utcNow);

            return dateRange.ToDateString();


        }

        public DateRange GetCurrentTimeRangeForReporting(DateTime utcNow) {
            var now = utcNow.ToDefaultTimezone();
            DateTime start;
            DateTime end;
            if (this.AutomaticReporting == ProjectReportingTypes.Monthly) {
                start = now.StartOfMonth();
                end = now.EndOfMonth().Date;
            } else if (this.AutomaticReporting == ProjectReportingTypes.Weekly) {
                start = now.StartOfWeek();
                end = start.EndOfWeek().Date;
            } else if (this.AutomaticReporting == ProjectReportingTypes.Daily) {
                start = now.StartOfDay();
                end = start.EndOfDay().Date;
            } else {
                throw new Exception($"AutomaticReporting={this.AutomaticReporting} is not supported");
            }

            var dateRange = new DateRange(start, end);
            return dateRange;
        }


        /// <summary>
        /// Get the richtext description for the project
        /// HINT: Description.Children.Children has to be loaded
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public string GetProjectescriptionHtml(string language) {
            var trans = this.Description?.GetExactTranslationForLanguageOrDefault(language);
            string description = null;
            if (trans != null) {
                var htmlNodes = trans.ChildrenForType(ContentNode.NODE_TYPE_HTML);
                description = string.Join("\n",htmlNodes.OrderBy(h => h.Sort).Select(hn => hn.Value));
            }
            description = description?.Replace("&nbsp;", string.Empty); // remove spaces from cms editor

            return description;
        }


        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        //public override void Populate(IPopulateProvider populateProvider, FormBuilder form) {
        //    base.Populate(populateProvider, form);

        //    // Currently always Project Type
        //    //this.ProjectType = ProjectTypes.Project;

        //}

        public override CardBuilder VisualCard() {

            // Init card
            this.Include(nameof(Business));
            var card = new Core.Cards.CardBuilder(this)
                .Title(this.Name)
                .Subtitle(this.Business?.Name)
                .Sub(this.ProjectType)
                .Sub(this.TimeRange?.ToString(Core.Config.DateFormat))
                .Icon("star-full-outline")
                .Wide();
            if (this.IsOverdue) {
                card.BlinkSub();
            }

            card.Link("/admin/projects/project/" + this.Path);
            
            return card;
        }

        public IList<Finance.Model.LineItemGroup> GenerateLineItemGroups(IEnumerable<ProjectCost> costs, Invoice invoice) {
            this.LoadFirstLevelNavigationReferences();
            this.LoadFirstLevelObjectReferences();
            var billedDate = DateTime.UtcNow;
            var groups = new List<Finance.Model.LineItemGroup>();
            {
                // Order group
                var group = new Finance.Model.LineItemGroup();
               
                // Default Timerange
                group.TimeRange = this.TimeRange.Clone();

                // Try Timerange from first cost to last cost
                if (costs != null && costs.Any(c => c.TimeRange?.Start != null) && costs.Any(c => c.TimeRange?.End != null)) {
                    var min = costs.Where(c => c.TimeRange?.Start != null).Min(c => c.TimeRange.Start.Value);
                    var max = costs.Where(c => c.TimeRange?.End != null).Max(c => c.TimeRange.End.Value);
                    group.TimeRange = new DateRange(min, max);
                }

                //group.Title = "{text.project} " + this.Name;
                group.Title = "{text.project} " + this.GetFullyQualifiedName();
                group.Description = this.GetDescription();
                group.Category = "{text.project}";

                // Items
                foreach (var cost in costs.OrderBy(c => c.TimeRange?.Start ?? c.Created)) {
                    cost.LoadFirstLevelNavigationReferences();
                    cost.LoadFirstLevelObjectReferences();
                    var lineItem = new Finance.Model.LineItem();




                    // General
                    lineItem.CustomerItemPrice = cost.CostPerUnit.Clone();
                    lineItem.InternalItemPrice = cost.CostPerUnitInternal.Clone();
                    lineItem.Category = "{text.project-cost}";
                    lineItem.SubCategory = cost.WorkType?.Name;

                    if (string.IsNullOrEmpty(lineItem.SubCategory)) {
                        if (cost.ProjectCostType == ProjectCost.ProjectCostTypes.Internal) {
                            lineItem.SubCategory = "{text.invoice.project-cost-types.internal}";
                        }
                        if (cost.ProjectCostType == ProjectCost.ProjectCostTypes.External) {
                            lineItem.SubCategory = "{text.invoice.project-cost-types.external}";
                        }
                    }

                    lineItem.Quantity = cost.Units;
                    lineItem.CustomerVATRate = (decimal?)(Config.ProjectsDefaultVatRate / 100);
                    lineItem.TimeRange = cost.TimeRange?.Clone();

                    // Set totals baked in
                    lineItem.CustomerVAT = lineItem.CustomerItemPrice * lineItem.CustomerVATRate;
                    lineItem.CustomerTotal = lineItem.CustomerSubtotal + lineItem.CustomerVAT * lineItem.Quantity;

                    // Description
                    var description = Core.Config.GetStringSetting("ProjectsInvoiceGenerationLineItemText"+ cost.ProjectCostType.ToString());
                    description = description.Replace("cost.description".TemplateVar(), cost.Description);
                    if (cost.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet) {
                        description = description.Replace("cost.work-type.name".TemplateVar(), cost.WorkType.Name);
                        description = description.Replace("cost.work-type.type".TemplateVar(), cost.WorkType.WorkUnit.ToString());
                        description = description.Replace("cost.duration".TemplateVar(), cost.Duration.TotalHours.ToString("0.00h"));
                        description = description.Replace("cost.user.name".TemplateVar(), cost.User?.Name);
                    }
                    description = description.Replace("cost.note".TemplateVar(), cost.Note);
                    description = description.ReplacePrefix(", ", "");
                    description = description.ReplaceSuffix(", ", "");
                    description = description.Trim();

                    // Description: overtime suffix
                    if (cost.Overtime == true) {
                        description += ", {text.projects.overtime} " + cost.OvertimeFactor.ToString("0.##x");
                    }


                    lineItem.Description = description;

                    cost.LineItem = lineItem;
                    cost.Invoice = invoice;
                    cost.Billed = billedDate;
                    group.LineItems.Add(lineItem);
                }
                               
                groups.Add(group);
            }
            return groups;
        }

        /// <summary>
        /// Gets the description of the projects.
        /// </summary>
        /// <returns></returns>
        public string GetDescription(bool includeDescription = true) {
            if (includeDescription == true) {
                this.Include(nameof(this.Description));
            }
            if (this.Description != null && !string.IsNullOrEmpty(this.Description.ToString())) {
                return this.Description.ToString();
            }
            //If none available tries with ancestors
            //if (this.ParentId != null) {
            //    this.Include(nameof(this.Parent));
            //    return this.Parent.GetDescription();
            //}
            return null;
        }

        public static IEnumerable<ProjectBudgetView> GetProjectBudgetsView(Project project, IList<ProjectBudgetView> budgets = null) {

            if (budgets == null) { budgets = new List<ProjectBudgetView>(); }

            var budget = project.CalculateBudget(ProjectBudget.BudgetTypes.External);
            var pbv = new ProjectBudgetView();
            pbv.Name = project.Name;
            project.Include(nameof(Project.Description));
            pbv.Description = project.Description?.Summary;
            pbv.Budget = budget.TotalBudget?.Clone();
            pbv.Remaining = budget.RemainingBudget?.Clone();
            pbv.Used = budget.BudgetUsed;
            pbv.Level = project.Level;
            pbv.Project = project;

            budgets.Add(pbv);
            foreach(var childProject in project.Children.Where(c => c.Deleted == false)) {
                GetProjectBudgetsView(childProject, budgets);
                   
            }

            return budgets;

            
          
        }

        public static IEnumerable<ProjectCostsView> GetProjectCostsView(Project project, IQueryable<ProjectCost> allCostsForTimeRange, IList<ProjectCostsView> costsPerSubproject = null) {

            if (costsPerSubproject == null) { costsPerSubproject = new List<ProjectCostsView>(); }

            var pcv = new ProjectCostsView();
            pcv.Name = project.Name;
            pcv.Description = project.Description?.Summary;
         
            pcv.Level = project.Level;
            pcv.Project = project;

            var costsSubproject = allCostsForTimeRange.Where(c => c.Project.Id == project.Id && c.Costs.HasValue == true);
            pcv.Costs = costsSubproject.Sum(c => c.Costs);

            costsPerSubproject.Add(pcv);
            foreach (var childProject in project.Children.Where(c => c.Deleted == false)) {
                GetProjectCostsView(childProject, allCostsForTimeRange, costsPerSubproject);

            }

            return costsPerSubproject;


        }

        public bool CheckAllowBookingCostsOnProject(bool throwException ) {

           

            // New: from dan

            // Check mode
            if(this.GetRootProject().CostsOnlyOnSubprojects == true) {
                // CostsOnlyOnSubprojects ON
                // Ensure that this is the deepest leaf in the project/subproject tree
                var children = this.GetAndLoadDirectSubrojects();
                if (children.Any() == true) {
                    if (throwException == true) {
                        throw new BackendException("costs-only-on-subprojects", "This project only allows to book costs on subprojects (must be the deepest subproject in the project tree)!");
                    }
                    return false;
                } 
                // Ensure that we are not the root of the project
                //TODO: @micha
                // If all checks pass, then booking is allowed
                return true;
            } else {
                // CostsOnlyOnSubprojects OFF
                // Always allow to book, irregardless of where it is
                return true;
            }

            // Old: from micha
            /*
            if (this.CostsOnlyOnSubprojects == false) {
                return true;
            }

            // new: always forbid if flag is set
            //var children = this.GetAndLoadDirectSubrojects();
            //if (children.Any() == true) {
            //    if (throwException == true) {
            //        //throw new BackendException("costs-only-on-subprojects", "This project only allows to book costs on subprojects with no subprojects");
            //        throw new BackendException("costs-only-on-subprojects", "This project only allows to book costs on subprojects with no subprojects");
            //    }
            //    return false;
            //}

            if (throwException == true) {
                throw new BackendException("costs-only-on-subprojects", "This project only allows to book costs on subprojects");
            }
            return false;
            */
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextProjectExtensions {
        [ModelSet]
        public static DbSet<Project> Projects(this Core.Model.ModelContext context) {
            return context.Set<Project>();
        }

        /// <summary>
        /// Gets only the 'normal' Project Types = Project
        /// No Estimations, Cost Proposals etc
        /// </summary>
        /// <param name="projects"></param>
        /// <returns></returns>
        public static IQueryable<Project> OnlyStandardProjects(this IQueryable<Projects.Model.Project> projects) {
            projects = projects.Where(p => p.ProjectType == Project.ProjectTypes.Project);
            return projects;
        }

        public static IQueryable<Project> OnlyProposals(this IQueryable<Projects.Model.Project> projects) {
            projects = projects.Where(p => p.ProjectType == Project.ProjectTypes.CostProposal);
            return projects;
        }

        public static Project GetByShortUrlPath(this IQueryable<Projects.Model.Project> query, string shortUrlPath, bool throwExceptions = true){
            var ret = query.Where(e => e.ShortURLPath == shortUrlPath).SingleOrDefault();
            if (ret == null && throwExceptions == true) throw new Backend404Exception("project-not-found", $"The project with path {shortUrlPath} could not be found.");
            return ret;
        }

        public static Project GetByPath(this IQueryable<Projects.Model.Project> query, string path, bool throwExceptions = true) {
            var publicId = path?.Split('/').FirstOrDefault();
            return query.GetByPublicId(publicId);
        }

        public static Project GetRootByPath(this IQueryable<Projects.Model.Project> query, string path, bool throwExceptions = true) {
            var rootPath = string.Join("/", path.Split('/').Skip(1).Take(2));
            return query.GetByShortUrlPath(rootPath);
        }
    }

    #endregion


    public class ProjectColor : ModelObject {

        [FormBuilder(Forms.Admin.VIEW)]
        public string Color { get; set; }
    }
}


