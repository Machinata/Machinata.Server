using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Text;
using Machinata.Core.Cards;
using Machinata.Module.Projects.Logic;
using Machinata.Module.Finance.Model;

namespace Machinata.Module.Projects.Model {

    [Serializable()]
    [ModelClass]
    public partial class Absence : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////

        public enum AbsenceTypes : short {
            Vacation = 10,
            Sickness = 20,
            Unpaid = 30,
            Compensation = 40
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Absence() {
            this.TimeRange = new DateRange();
        }

        #endregion
        
        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Required]
        [Column]
        public AbsenceTypes AbsenceType { get; set; }
        
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Title { get; set; }
        
           
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [CascadeDelete]
        [Column]
        public string Description { get; set; }

        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        public DateRange TimeRange { get; set; }
        
        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public double? Days { get; set; }
        
        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        //[FormBuilder(Forms.Admin.CREATE)]
        //[FormBuilder(Forms.Admin.VIEW)]
        //[FormBuilder(Forms.Admin.LISTING)]
        //[FormBuilder(Forms.Admin.SELECTION)]
        //[FormBuilder(Forms.Admin.EDIT)]
        public Business Business { get; set; } // later


        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Required]
        public User User { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
        
        [FormBuilder(Forms.Admin.LISTING)]
        [NotMapped]
        [DataType(DataType.Date)]
        public DateTime? Start {
            get {
                return this.TimeRange.Start;
            }
        }

        [FormBuilder(Forms.Admin.LISTING)]
        [NotMapped]
        [DataType(DataType.Date)]
        public DateTime? End {
            get {
                return this.TimeRange.End;
            }
        }
        
        [FormBuilder]
        [NotMapped]
        public string URL {
            get {
                return "/admin/projects/absences/absence/" + this.PublicId;
            }
        }
        
        [NotMapped]
        [FormBuilder]
        public string FullTitle {
            get {
                return $"{this.User?.Name} {this.Title}";
            }
        }

        [NotMapped]
        [FormBuilder(Forms.Admin.TOTAL)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder]
        public double DaysForTotal
        {
            get
            {
                return !this.Days.HasValue ? 0 : this.Days.Value;
            }
        }



        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public double? CalculateNumberOfDays() {
            if (this.TimeRange.HasValue()) {
                // Times must be local, but we save in db in UTC
                var start = this.Start.Value.ToDefaultTimezone();
                var end = this.End.Value.EndOfDay().ToDefaultTimezone();
                return Core.Util.Time.GetWesternWorkingDays(start, end);
            }
            return null;
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override void Populate(IPopulateProvider populateProvider, FormBuilder form) {
            base.Populate(populateProvider, form);

            if (this.Days == null) this.Days = this.CalculateNumberOfDays();
        }

        public override CardBuilder VisualCard() {

            // Init card
            this.Include(nameof(Business));
            var card = new Core.Cards.CardBuilder(this)
                .Title(this.Title)
                .Subtitle(this.User?.Name)
                .Sub(this.TimeRange.ToString())
                .Icon("plane-outline")
                .Wide();
            return card;
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextAbsenceExtensions {
        [ModelSet]
        public static DbSet<Absence> Absences(this Core.Model.ModelContext context) {
            return context.Set<Absence>();
        }
                
       
    }

    #endregion

}
