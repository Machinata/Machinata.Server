using System;
using System.ComponentModel.DataAnnotations.Schema;
using Machinata.Core.Model;

namespace Machinata.Module.Projects.Model {

    public abstract class ProjectDeletable : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////



        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////



        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [Column]
        public bool Deleted { get; set; }

        [Column]
        public DateTime? DeletedDate { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

       
        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////
               


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Sets the Deleted Flag and Date
        /// </summary>
        /// <param name="deleted"></param>
        public void SetDeleted(bool deleted = true) {
            if (deleted == true) {
                this.Deleted = true;
                this.DeletedDate = DateTime.UtcNow;
            } else {
                this.Deleted = false;
                this.DeletedDate = null;
            }
        }

      

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////



        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

   

    #endregion

}
