﻿using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Module.Admin.Dashboard;
using Machinata.Module.Projects.Model;
using Machinata.Module.Reporting.Logic;
using Machinata.Module.Reporting.Model;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Projects.Dashboard {
    class ProjectsDashboard {

        [DashboardLoader]
        public static void LoadDashboard(DashboardLoader loader) {

            var section = new SectionNode();
            section.SetTitle("Projects");
            section.Style = "W_H2";
            section.RandomID();

            var currency = Core.Config.CurrencyDefault;
            var hours = "Hours";


            // 12 weeks back
            var minDate = DateTime.UtcNow.AddDays(-12 * 7);
           

            var costs = loader.DB.ProjectCosts().Where(c => c.Created > minDate).ToList().OrderByDescending(c => c.Id);



            var billed = costs.Where(c => c.Billed.HasValue == true).ToList().AsQueryable();

            // Info
            {
                var infoText = "Costs = Total amount of costs from all projects | Billed = Costs which have been billed via invoice, Hours = Timesheet hours of all projects";
                var info = new InfoNode();
                info.SetTitle("Info");
                info.Message = infoText;
                section.AddChild(info);
            }

            // Costs per week
            {
                var node = Reporting.Logic.CostsOverTimeReports.GetWeeklyCosts(costs.ToList().AsQueryable(), "Costs", o => o.Created, o => o.Costs?.Value, unit: currency);
                {
                    // Billed
                    var serie = Reporting.Logic.CostsOverTimeReports.CreateWeeklyCategorySerie(billed, "Billed", o => o.Created, o => o.Costs?.Value);
                    node.Series.Add(serie);
                }

                section.AddChild(node);

            }

            // Hours per week
            {

                var timesheets = costs.Where(c => c.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet).ToList().AsQueryable();
                var node = Reporting.Logic.CostsOverTimeReports.GetWeeklyCosts(timesheets, hours, o => o.Created, o => o.Units, hours);
                {
                    // Billed
                    var serie = Reporting.Logic.CostsOverTimeReports.CreateWeeklyCategorySerie(timesheets.Where(t => t.Billed.HasValue == true), "Billed", o => o.Created, o => o.Units);
                    node.Series.Add(serie);
                }
                section.AddChild(node);
            }

            loader.AddItem(section, "/admin/projects");


            section.AddChild(CreateProposalSection(loader.DB, currency));

            //// Proposals
            //var proposalSection = 
            //loader.AddItem(proposalSection, "/admin/projects");



        }

        public static SectionNode CreateProposalSection(ModelContext db, string currency) {
            var section = new SectionNode();
          
            section.SetTitle("Proposals");
            section.AddStyle("CS_H3");
            var minDate12MonthsAgo = DateTime.UtcNow.AddMonths(-12);
            var minDate3MonthsAgo = DateTime.UtcNow.AddMonths(-3);
            var proposals = db.Projects().Where(p => p.ParentId == null).OnlyProposals().ToList().Select(p => p.LoadImplementation() as ProjectCostProposal);
            var last12Months = proposals
                 .Where(p => p.Budget != null && p.Budget.Value != null)
                 .Where(p => p.Date >= minDate12MonthsAgo)
                 .AsQueryable();

            var pendingLast12 = last12Months.Where(p => p.Status == ProjectCostProposal.Statuses.Created || p.Status == ProjectCostProposal.Statuses.Pending);
            var acceptedLast12 = last12Months.Where(p => p.Status == ProjectCostProposal.Statuses.Accepted);
            var declinedLast12 = last12Months.Where(p => p.Status == ProjectCostProposal.Statuses.Declined);


            var last3Months = proposals
                 .Where(p => p.Budget != null && p.Budget.Value != null)
                 .Where(p => p.Date >= minDate3MonthsAgo)
                 .AsQueryable();


            var pendingLast3 = last3Months.Where(p => p.Status == ProjectCostProposal.Statuses.Created || p.Status == ProjectCostProposal.Statuses.Pending);
            var acceptedLast3 = last3Months.Where(p => p.Status == ProjectCostProposal.Statuses.Accepted);
            var declinedLast3 = last3Months.Where(p => p.Status == ProjectCostProposal.Statuses.Declined);

            // Donut status
            {
                var node = new DonutNode();
                node.Screen = true;
                node.Theme = "default";
                node.SetTitle("Proposals");
                node.SetSubTitle("by Status");
                var groups = proposals.GroupBy(p => p.Status);
                foreach (var group in groups) {
                    node.AddSliceNumber(group.Count(), group.Key.ToString());
                }
                node.LegendLabelsShowValue = true;
                section.AddChild(node);

            }

            // Last 12 MOonths
            {


                var node = new VBarNode();
                node.Screen = true;
                node.SetTitle("Proposals");
                node.SetSubTitle("by Month" + CostsOverTimeReports.GetUnitSuffix(currency));
                node.DefaultTheme();
             
                // var group = node.AddSerieGroup();
                //group.XAxisMonths();
                //group.YAxisNumberThousands();


                {

                    var serie = CostsOverTimeReports.CreateMonthlyCostsCatogorieSerie(pendingLast12, "Pending", p => p.Date, c => c.Budget.Value.Value, accumulate: false);
                    serie.YAxisNumberThoundsandsNoDecimal();
                    node.Series.Add(serie);
                    //group.AddColumnSerie(serie);
                }
                {

                    var serie = CostsOverTimeReports.CreateMonthlyCostsCatogorieSerie(acceptedLast12, "Accepted", p => p.Date, c => c.Budget.Value.Value, accumulate: false);
                    //var group = node.AddSerieGroup();
                    //group.XAxisMonths();
                    // group.YAxisNumberThousands();
                    // group.AddColumnSerie(serie);
                    node.Series.Add(serie);
                }
                {

                    var serie = CostsOverTimeReports.CreateMonthlyCostsCatogorieSerie(declinedLast12, "Declined", p => p.Date, c => c.Budget.Value.Value, accumulate: false);
                    //var group = node.AddSerieGroup();
                    //group.XAxisMonths();
                    //group.YAxisNumberThousands();
                    node.Series.Add(serie);
                    //group.AddColumnSerie(serie);
                }

                section.AddChild(node);

            }

            // Metrics
            {

                var layoutNode = new LayoutNode();
                layoutNode.Screen = true;
                layoutNode.TwoColumns();

                section.AddChild(layoutNode);
                {
                    var node = new MetricsNode();
                    //node.Growing();
                    node.Theme = "default";
                    node.SetTitle("Proposals");
                    node.SetSubTitle("last 12 Month" + CostsOverTimeReports.GetUnitSuffix(currency));
                    {
                        var group = node.AddGroup();
                        group.CreateItem()
                            .SetLabel("Pending")
                            .SetValue(pendingLast12.Sum(p => p.Budget.Value).Value.ToString(Reporting.Config.NumberThousandsFormatNet, Reporting.Config.FormattingCulture))
                            .SetSubValue(pendingLast12.Count().ToString());
                    }
                    {
                        var group = node.AddGroup();
                        group.CreateItem()
                            .SetLabel("Accepted")
                            .SetValue(acceptedLast12.Sum(p => p.Budget.Value).Value.ToString(Reporting.Config.NumberThousandsFormatNet, Reporting.Config.FormattingCulture))
                            .SetSubValue(acceptedLast12.Count().ToString());
                    }
                    {
                        var group = node.AddGroup();
                        group.CreateItem()
                            .SetLabel("Denied")
                            .SetValue(declinedLast12.Sum(p => p.Budget.Value).Value.ToString(Reporting.Config.NumberThousandsFormatNet, Reporting.Config.FormattingCulture))
                            .SetSubValue(declinedLast12.Count().ToString());
                    }
                    layoutNode.AddLeft(node);
                }

                {
                    var node = new MetricsNode();
                    //node.Growing();
                    node.Theme = "default";
                    node.RandomID();
                    node.SetTitle("Proposals");
                    node.SetSubTitle("last 3 Month" + CostsOverTimeReports.GetUnitSuffix(currency));

                    {
                        var group = node.AddGroup();
                        group.CreateItem()
                            .SetLabel("Pending")
                            .SetValue(pendingLast3.Sum(p => p.Budget.Value).Value.ToString(Reporting.Config.NumberThousandsFormatNet, Reporting.Config.FormattingCulture))
                            .SetSubValue(pendingLast3.Count().ToString());
                    }
                    {
                        var group = node.AddGroup();
                        group.CreateItem()
                            .SetLabel("Accepted")
                            .SetValue(acceptedLast3.Sum(p => p.Budget.Value).Value.ToString(Reporting.Config.NumberThousandsFormatNet, Reporting.Config.FormattingCulture))
                            .SetSubValue(acceptedLast3.Count().ToString());
                    }
                    {
                        var group = node.AddGroup();
                        group.CreateItem()
                            .SetLabel("Denied")
                            .SetValue(declinedLast3.Sum(p => p.Budget.Value).Value.ToString(Reporting.Config.NumberThousandsFormatNet, Reporting.Config.FormattingCulture))
                            .SetSubValue(declinedLast3.Count().ToString());
                    }
                    layoutNode.AddRight(node);
                }
            }

            return section;

        }
    }
}
