using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Module.Projects.Model;
using Machinata.Module.Projects.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Projects.Logic {
    public static class PageTemplateExtensions {

        /// <summary>
        /// Inserts a list of project costs
        /// Filters based on the request filters: budgets, billed, users, costtypes, worktypes
        /// Autohides columns based on the filters
        /// </summary>
        public static void InsertFilteredProjectCosts(this Core.Handler.Handler handler, PageTemplate template, Project project, bool insertListing, FormBuilder form = null, FormBuilder formTotal = null, string link = null) {

            if (form == null) {
                form = new FormBuilder(Forms.Admin.LISTING)
                    .Include(nameof(ProjectCost.Created))
                    .Include(nameof(ProjectCost.WorkType));
            }
            if (formTotal == null) {
                formTotal = new FormBuilder(Forms.Admin.TOTAL);
            }

            var costsOfProject = project.GetAllCosts(hideDeleted: true).Select(c => c.Id);
            var costsUnfiltered = handler.DB.ProjectCosts()
                .Include(nameof(ProjectCost.User))
                .Include(nameof(ProjectCost.WorkType))
                .Where(c => costsOfProject.Contains(c.Id))
                .ToList()
                .AsQueryable();

            var costs = costsUnfiltered;
            var billable = handler.Params.BoolNullable("billable", null);
            var budgetsFilter = handler.Params.String("budgets", null);
            var billedFilter = handler.Params.BoolNullable("billed", null);
            var userFilter = handler.Params.StringArray("users", new string[] { }, ',', StringSplitOptions.RemoveEmptyEntries);
            var workTypeFilter = handler.Params.StringArray("worktypes", new string[] { }, ',', StringSplitOptions.RemoveEmptyEntries);
            var costTypeFilter = handler.Params.String("costtype", null);
            var timerangeFilter = handler.Params.String("timerange", "all");
            if (timerangeFilter == "") {
                timerangeFilter = "all";
            }
            var timeRangeFilterType = "all";


            // Billable
            if (billable != null) {
                costs = costs.Where(c => c.Billable == billable);
            }


            IEnumerable<User> filteredUsers = new List<User>();
            IEnumerable<ProjectWorkType> filteredWorkTypes = new List<ProjectWorkType>();


            // Budgets
            if (budgetsFilter == "internal") {
                form.Exclude(nameof(ProjectCost.CostPerUnit));
                form.Exclude(nameof(ProjectCost.Costs));
                form.Include(nameof(ProjectCost.CostPerUnitInternal));
            } else if (budgetsFilter == "external") {
                form.Exclude(nameof(ProjectCost.CostPerUnitInternal));
                form.Exclude(nameof(ProjectCost.CostsInternal));
            }

            // Billed?
            if (billedFilter == true) {
                costs = costs.FilterBilled(billedFilter);
            } else if (billedFilter == false) {
                costs = costs.FilterBilled(billedFilter);
                form.Exclude(nameof(ProjectCost.Billed));
            }

            // Users
            if (userFilter.Any()) {
                costs = costs.FilterUsers(userFilter, ref filteredUsers);
                form.Exclude(nameof(ProjectCost.User));
            }

            // Worktypes
            if (workTypeFilter.Any()) {
                costs = costs.Where(c => c.WorkType != null && workTypeFilter.Contains(c.WorkType.PublicId));
                filteredWorkTypes = costs.Select(c => c.WorkType);
                form.Exclude(nameof(ProjectCost.WorkType));
            }

            // Costtype
            if (!string.IsNullOrEmpty(costTypeFilter)) {
                var filter = Core.Util.EnumHelper.ParseEnum<ProjectCost.ProjectCostTypes>(costTypeFilter);
                costs = costs.Where(c => c.ProjectCostType == filter);
                form.Exclude(nameof(ProjectCost.ProjectCostType));
            }

            // Datefilter
            DateRange dateRange = null;
            costs = FilterDateRange(costs, timerangeFilter, ref timeRangeFilterType, ref dateRange);

            DateTime? minDate = null;
            DateTime? maxDate = null;
            if (costs.Any()) {
                minDate = costs.Where(p => p.TimeRange != null && p.TimeRange.Start != null).Min(p => p.TimeRange.Start);
                maxDate = costs.Where(p => p.TimeRange != null && p.TimeRange.End != null).Max(p => p.TimeRange.End);
            }

            // Listing
            if (insertListing == true) {
                template.InsertEntityList(
                    entities: costs,
                    variableName: "entities",
                        form: form,
                        link: link,
                        total: formTotal
                );
            }


            // Filter
            template.InsertTemplates("user-filters", costsUnfiltered.Where(c => c.User != null).Select(c => c.User).Distinct(), "costs.filter", InsertFilterVariables);
            template.InsertTemplates("worktype-filters", costsUnfiltered.Where(c => c.WorkType != null).Select(c => c.WorkType).Distinct(), "costs.filter", InsertFilterVariables);

            // Query Param
            template.InsertVariable("query-string", handler.Context.Request.QueryString.ToString());

            // Vars
            template.InsertVariables("project", project);
            template.InsertVariable("budgets", string.IsNullOrEmpty(budgetsFilter) ? "all" : budgetsFilter);
            template.InsertVariable("internal-budget", (budgetsFilter == "internal" || string.IsNullOrEmpty(budgetsFilter)) ? "true" : "false");
            template.InsertVariable("external-budget", (budgetsFilter == "external" || string.IsNullOrEmpty(budgetsFilter)) ? "true" : "false");
            
            template.InsertVariable("min-date", Core.Util.Time.ToDefaultTimezoneDateString(minDate));
            template.InsertVariable("max-date", Core.Util.Time.ToDefaultTimezoneDateString(maxDate));
            template.InsertVariable("billed", billedFilter.HasValue ? billedFilter.Value.ToString().ToLowerInvariant() : "all");
            template.InsertVariable("billed-raw", billedFilter.HasValue ? billedFilter.Value.ToString().ToLowerInvariant() : "");
            template.InsertVariable("costtype", !string.IsNullOrEmpty(costTypeFilter) ? costTypeFilter : "all");
            template.InsertVariable("costtype-raw", !string.IsNullOrEmpty(costTypeFilter) ? costTypeFilter : "");
            template.InsertVariable("user", filteredUsers.Any() ? filteredUsers.FirstOrDefault().PublicId : "all");
            template.InsertVariable("user-raw", filteredUsers.Any() ? filteredUsers.FirstOrDefault().PublicId : "");
            template.InsertVariable("worktype", filteredWorkTypes.Any() ? filteredWorkTypes.FirstOrDefault().PublicId : "all");
            template.InsertVariable("worktype-raw", filteredWorkTypes.Any() ? filteredWorkTypes.FirstOrDefault().PublicId : "");
            template.InsertVariable("timerange", timerangeFilter);
            template.InsertVariable("timerange-type", timeRangeFilterType);

           

        }

        public static IQueryable<ProjectCost> FilterDateRange(this IQueryable<ProjectCost> costs, string timerangeFilter, ref string timeRangeFilterType, ref DateRange dateRange) {
            // Timerange filter
            if (string.IsNullOrEmpty(timerangeFilter) == false && timerangeFilter.ToLowerInvariant() != "all") {


                DateTime start;
                DateTime end;

                const string DateFormat = "yyyy.MM.dd";
                const string MonthYearFormat = "yyyy.MM";

                // Daterange
                if (timerangeFilter.Contains("-")) {

                    var startString = timerangeFilter.Split('-').First().Trim();
                    var endString = timerangeFilter.Split('-').Last().Trim();

                    // Date
                    start = DateTime.ParseExact(startString, DateFormat, System.Globalization.CultureInfo.InvariantCulture).ToUniversalTime();
                    end = Core.Util.Time.EndOfDay(DateTime.ParseExact(endString, DateFormat, System.Globalization.CultureInfo.InvariantCulture)).ToUniversalTime();

                    timeRangeFilterType = "timerange";


                } else {

                    // Month
                    var startLocaltime = DateTime.ParseExact(timerangeFilter, MonthYearFormat, System.Globalization.CultureInfo.InvariantCulture);
                    var endLocalTime = Core.Util.Time.EndOfMonth(startLocaltime);

                    start = startLocaltime.ToUniversalTime();
                    end = endLocalTime.ToUniversalTime();

                    timeRangeFilterType = "month";

                }

                dateRange = new DateRange(start, end);
                return FilterDateRange(costs, start, end);
            }

            return costs;
        }

        public static IQueryable<ProjectCost> FilterDateRange(this IQueryable<ProjectCost> costs, DateTime start, DateTime end) {
            return costs.Where(c => c.TimeRange != null && c.TimeRange.Start >= start && c.TimeRange.End <= end);
        }

        public static IQueryable<ProjectCost> FilterBilled(this IQueryable<ProjectCost> costs, bool? billedFilter) {

            // Billed?
            if (billedFilter == true) {
                costs = costs.Where(c => c.Billed != null);
            } else if (billedFilter == false) {
                costs = costs.Where(c => c.Billed == null);
            }

            return costs;
        }

        public static IQueryable<ProjectCost> FilterUsers(this IQueryable<ProjectCost> costs, IEnumerable<string> userFilter, ref IEnumerable<User> filteredUsers) {

            if (userFilter.Any()) {
                costs = costs.Where(c => c.User != null && userFilter.Contains(c.User.PublicId));
                filteredUsers = costs.Select(w => w.User);
            }

            return costs;
        }

        public static void InsertFilterVariables(ProjectWorkType workType, PageTemplate template) {
            template.InsertVariable("filter.name", "worktypes");
            template.InsertVariable("filter.title", workType.Name);
            template.InsertVariable("filter.id", workType.PublicId);
        }

        public static void InsertFilterVariables(User user, PageTemplate template) {
            template.InsertVariable("filter.name", "users");
            template.InsertVariable("filter.title", user.Name);
            template.InsertVariable("filter.id", user.PublicId);    
        }

        public static void InsertFilterVariablesSelection(Project project, PageTemplate template) {
            template.InsertVariable("filter.name", "projects");
            template.InsertVariable("filter.title", project.Name);
            template.InsertVariable("filter.id", project.PublicId);
        }

   

        public static void InsertProjectProposalsSubprojects(this PageTemplateHandler handler,string variableName, Project project) {

            // Form to get the value of the properties via refleciton
            var valueForm = new FormBuilder(Forms.Admin.EMPTY)
                .Include(nameof(ProjectCost.Description))
                .Include(nameof(ProjectCost.Units))
                .Include(nameof(ProjectCost.CostPerUnit))
                .Include(nameof(ProjectCost.WorkType))
                .Include(nameof(ProjectCost.DurationShort))
                .Include(nameof(ProjectCost.CostsDefaultCurrency));


            var totalForm = new FormBuilder(Forms.Admin.EMPTY)
                .Include(nameof(ProjectCost.Description))
                .Include(nameof(ProjectCost.Units))
                .Include(nameof(ProjectCost.Costs));

         
            var sampleCost = new ProjectCost();
            var properties = sampleCost.GetPropertiesForForm(new FormBuilder(ProjectsForms.PROJECT_PROPOSALS_PDF_FORM));
            var propertiesTotal = sampleCost.GetPropertiesForForm(totalForm);


            // Sub projects
            var groupTemplates = new List<PageTemplate>();
            var subProjectsImpls = project.Children.Select(c => c.LoadImplementation() as ProjectCostProposal).OrderBy(p=>p.Sort);
            foreach (var subProjectimpl in subProjectsImpls) {
                var groupTemplate = handler.Template.LoadTemplate("proposal.group");

                // Load description children translation
                // Does not work in the original project query
                if (subProjectimpl.Description!= null) {
                    subProjectimpl.Description.IncludeContent();
                }

                // Header
                {
                    var groupHeaderTemplate = groupTemplate.LoadTemplate("proposal.group.header");
                    var groupHeaderTemplateProperties = new List<PageTemplate>();
                    foreach (var property in properties) {
                        var groupHeaderTemplateItem = groupHeaderTemplate.LoadTemplate("proposal.group.header.property");
                        groupHeaderTemplateItem.InsertVariable("entity.property.type", property.PropertyType?.Name);

                        // Special name for units
                        {
                            if (property.Name == nameof(ProjectCost.Units)) {
                                groupHeaderTemplateItem.InsertVariable("entity.property.title", subProjectimpl.GetUnitName());
                            } else if (property.Name == nameof(ProjectCost.Billable)) {
                                groupHeaderTemplateItem.InsertVariable("entity.property.title", "");
                            } else {
                                var formProperty = new EntityFormBuilderProperty(subProjectimpl, property, valueForm);
                                groupHeaderTemplateItem.InsertVariable("entity.property.title", formProperty.GetPropertyTitle(handler.Language));
                            }

                        }

                        groupHeaderTemplateProperties.Add(groupHeaderTemplateItem);
                    }
                    groupHeaderTemplate.InsertTemplates("entity.properties", groupHeaderTemplateProperties);
                    groupTemplate.InsertTemplate("group.header", groupHeaderTemplate);
                }

                // Items
                {
                    //var groupHeaderTemplate = groupTemplate.LoadTemplate("proposal.group.header");
                    var groupTemplateItems = new List<PageTemplate>();
                    foreach (var cost in subProjectimpl.Costs.OrderBy(c=>c.Sort)) {
                        var groupTemplateItem = groupTemplate.LoadTemplate("proposal.group.item");
                        var groupTemplateItemProperties = new List<PageTemplate>();
                        var isOptionalCost = cost.Billable == false;
                        var classes = "";
                        classes += isOptionalCost ? "optional" : string.Empty;

                        // Costs
                        foreach (var property in properties) {
                            var groupTemplateItemProperty = groupTemplate.LoadTemplate("proposal.group.item.property");
                            groupTemplateItemProperty.InsertVariable("entity.property.type", property.PropertyType?.Name);
                            groupTemplateItemProperty.InsertVariable("entity.property.title", property.Name);
                            groupTemplateItemProperty.InsertVariable("entity.property.name", property.Name);
                            groupTemplateItemProperty.InsertVariable("entity.property.value", ProjectCostProposal.GetValueForProperty(cost, property, valueForm, handler.Language));
                            groupTemplateItemProperties.Add(groupTemplateItemProperty);
                        }


                        groupTemplateItem.InsertVariable("entity.classes", classes);
                        groupTemplateItem.InsertTemplates("entity.properties", groupTemplateItemProperties);
                        groupTemplateItems.Add(groupTemplateItem);
                    }

                    // Total
                    {
                     
                        var totalUnits = subProjectimpl.Costs.Where(c => c.Billable == true).Sum(c=> c.Units);
                        var groupTemplateItemProperties = new List<PageTemplate>();
                        var groupTemplateItem = groupTemplate.LoadTemplate("proposal.group.item");
                        var totalCost = new ProjectCost();
                        totalCost.Units = totalUnits;
                        totalCost.Description = "{text.subtotal}";
                        totalCost.Billable = true;
                        if (totalUnits != 0 && subProjectimpl.Budget?.Value.HasValue == true) {
                            totalCost.CostPerUnit = new Price(subProjectimpl.Budget?.Value / totalUnits);
                        }
                        // Costs
                        foreach (var property in properties) {
                            var groupTemplateItemProperty = groupTemplate.LoadTemplate("proposal.group.item.property");
                            groupTemplateItemProperty.InsertVariable("entity.property.type", property.PropertyType?.Name);
                            groupTemplateItemProperty.InsertVariable("entity.property.title", property.Name);
                            groupTemplateItemProperty.InsertVariable("entity.property.name", property.Name);

                            // Total?
                            // if (property.Name == nameof(ProjectCost.Units) || property.Name == nameof(ProjectCost.Costs)) {
                            if (propertiesTotal.Contains(property)) {
                                groupTemplateItemProperty.InsertVariable("entity.property.value", ProjectCostProposal.GetValueForProperty(totalCost, property, valueForm, handler.Language));
                            } else {
                                // Empty if not in total form
                                groupTemplateItemProperty.InsertVariable("entity.property.value", string.Empty);
                            }
                            groupTemplateItemProperties.Add(groupTemplateItemProperty);
                        }
                        // Subtotal class
                        groupTemplateItem.InsertVariable("entity.classes", "total");
                        groupTemplateItem.InsertTemplates("entity.properties", groupTemplateItemProperties);
                        groupTemplateItems.Add(groupTemplateItem);
                    }

                    groupTemplate.InsertTemplates("group.items", groupTemplateItems);
                }

                string description = subProjectimpl.GetProjectescriptionHtml(handler.Language);
                groupTemplate.InsertVariableUnsafe("group.description", description);

                // Vars
                groupTemplate.InsertVariables("group", subProjectimpl);
                groupTemplates.Add(groupTemplate);

            }
            handler.Template.InsertTemplates(variableName, groupTemplates);
        }


        public static void InserProjectProposalsTotalGroup(this PageTemplateHandler handler,string variableName, Project project,  FormBuilder form) {
            // Subprojects total overview

            var overviewItems = project.Children
                .Select(c => c.LoadImplementation() as ProjectCostProposal)
                .OrderBy(p => p.Sort)
                .ToList();
            var totalItem = new ProjectCostProposal();
            totalItem.Name = "{text.total}";
            totalItem.Budget = new Price(0);
            totalItem.ProjectType = project.ProjectType;
            totalItem.StyleClass = "total";
            var itemsWithBudget = overviewItems.Where(i => i.Budget != null && i.Budget.HasValue);
            if (itemsWithBudget.Any() == true) {
                totalItem.Budget = itemsWithBudget.Sum(i => i.Budget);
            }
            overviewItems.Add(totalItem);
            handler.Template.InsertEntityList(
                variableName: variableName,
                entities: overviewItems.AsQueryable(),
               form: form);
            //total: new FormBuilder(Forms.EMPTY).Include(nameof(Project.Budget)));

        }




       





    }
}
