using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Module.Projects.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Projects.Logic {
    public class TimetrackingLogic {

        public static ProjectCost Start(ModelContext db, User user, Project project, ProjectWorkType worktype, string description, bool billable) {
            
            // stop any existing tasks
            TimetrackingLogic.Stop(db, user);

            // validate that work type and project are compatible
            if (!project.GetWorkTypes().Contains(worktype)) {
                throw new BackendException("start-error", $"This project doesn't contain the worktype {worktype}");
            }

            // create new project cost using work type info
            var cost = new ProjectCost();
            cost.Project = project;
            cost.WorkType = worktype;
            cost.TimeRange = new DateRange(DateTime.UtcNow, null);
            cost.Business = project.Business;
            cost.User = user;
            cost.CostPerUnit = worktype.CostPerUnit;
            cost.CostPerUnitInternal = worktype.CostPerUnitInternal;
            cost.ProjectCostType = ProjectCost.ProjectCostTypes.Timesheet;
            cost.Billable = true;
            cost.Description = description;
            db.ProjectCosts().Add(cost);

            // Add to project users if missing
            if (!project.GetProjectUsers(true).Select(u=>u.User).Contains(user)) {
                var root = project.GetRootProject();
                root.AddNewProjectUser(user);
            }

            return cost;
        }

        public static ProjectCost Stop(ModelContext db, User user) {

            // stop any ProjectCost with ProjectCostType == Timesheet and TimeRange.End == null
            var openCosts = db.ProjectOpenTimesheetCosts(user);

            // prune very short durations using config
            //var minDuration = new TimeSpan(0, 0, Config.ProjectsPruneCostsMinSeconds);

            //if (false) {
            //    var costsToDelete = db.ProjectCosts().Include(nameof(ProjectCost.User)).Where(c => c.TimeRange.Start.HasValue && c.TimeRange.End.HasValue && c.Duration < minDuration).ToList();
            //    db.ProjectCosts().RemoveRange(costsToDelete);
            //}
            
            

          


            //TODO: validate that we have a open cost
            var openCost = openCosts.OrderByDescending(c => c.Id).FirstOrDefault();
            
            foreach (var cost in openCosts) {
                cost.Finish();
            }
            
            return openCost;
        }
    }
}
