﻿using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Module.Projects.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Projects.Logic {

    public static class AccessRights {

        public static bool CheckAdminRightsOrProjectMembership(this Core.Handler.Handler handler, Project project, User user, bool throwException) {
            Project rootProject = project;

            if (user == null) {
                if (throwException) {
                    throw new BackendException("authorization-required", $"You must be logged in with a admin account or have project membership to access this page");
                }
                return false;
            }

            if (project == null) {
                throw new Exception("CheckAdminRightsOrProjectMembership(): No project passed");
            }

            if (project.IsRootProject == false) {
                rootProject = project.GetRootProject();
            }
            var arnValid = handler.ValidateARN(handler.RequiredRequestARN, throwExceptions: false);
            if (arnValid) {
                var isInAdminGroup = user.AccessGroups.FirstOrDefault(ag => ag.Name == AccessGroup.GROUP_ADMIN_READWRITE) != null;
                if (isInAdminGroup == false) {

                    if (rootProject.Users == null) {
                        //throw new Exception("Project.Users not loaded");
                        rootProject.Include(nameof(rootProject.Users));
                        foreach (var projectUser in rootProject.Users) {
                            projectUser.Include(nameof(projectUser.User));
                        }
                    }

                    if (rootProject.Users.Select(u => u.User).Contains(user) == false) {
                        if (throwException == true) {
                            throw new BackendException("authorization-required", $"You must be logged in with a admin account or have project membership to access this page");
                        }
                        return false;
                    }
                }
            }

            return true;

        }

        public static bool CheckIsInAdminGroup(this Core.Handler.Handler handler, User user, bool throwExceptions) {
            var isInAdminGroup = user.AccessGroups.FirstOrDefault(ag => ag.Name == AccessGroup.GROUP_ADMIN_READWRITE) != null;

            if (isInAdminGroup == false) {

                if (throwExceptions == true) {
                    throw new BackendException("authorization-required", $"You must be logged in with a admin account to access this page");
                }
            }

            return isInAdminGroup;

        }
    }
}
