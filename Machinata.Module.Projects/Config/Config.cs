using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Projects {
    public class Config {

        public readonly static int ProjectsPruneCostsMinSeconds = Core.Config.GetIntSetting("ProjectsPruneCostsMinSeconds");
        public readonly static double ProjectsDefaultVatRate = Core.Config.GetDoubleSetting("ProjectsDefaultVatRate");
        public readonly static string ProjectsDefaultWorkTypeGroup = Core.Config.GetStringSetting("ProjectsDefaultWorkTypeGroup");
        public readonly static bool ProjectsFinanceIntegrationEnabled = Core.Config.GetBoolSetting("ProjectsFinanceIntegrationEnabled");

        public readonly static int? ProjectsTimesheetCostMinimumRoundUpMinutes = Core.Config.GetNullableIntSetting("ProjectsTimesheetCostMinimumRoundUpMinutes");


        /// <summary>
        /// Factor for timesheet costs with the flag OverTime enabled.
        /// This is only used when an invoice is generated:   lineItem.CustomerItemPrice = cost.CostPerUnit.Clone() * ProjectsOvertimeFactor;
        /// </summary>
        public readonly static decimal ProjectsOvertimeFactor = Core.Config.Dynamic.Decimal("OvertimeFactor", false) ?? 1m;

        public const string BUSINESS_COLOR_PROPERTY_KEY = "Business_Color";


        /// <summary>
        /// Day of month to send the unbilled costs email
        /// </summary>
        public readonly static int ProjectsUnbilldedCostsEmailDayToSend = Core.Config.GetIntSetting("ProjectsUnbilldedCostsEmailDayToSend", 1);
    }
}
