
Machinata.Projects = {};


//Gets the public id of costs from a ui entity table Element
Machinata.Projects.getSelectedCostIds = function (tableElem) {
    if (tableElem == null) {
        tableElem = $(".ui-table.entity-list");
    }
    var costs = tableElem.find("tr:not(.deselected)[data-public-id]");
    var costIds = [];
    costs.each(function (index) {
        Machinata.debug($(this));
        var id = $(this).attr("data-public-id");
        costIds.push(id);
    });
    return costIds;

};



Machinata.Projects.moveProjectCostDialog = function (costIds, onSuccess) {
    var diag = Machinata.entityListingDialogForAPICall("{text.move}", null, "/api/admin/projects/list", {
        entitiesKey: "projects",
        entityIdKey: "public-id",
        selectedEntityId: "{project.public-id}",
        search: true,
    });
    diag.wide();
    diag.input(function (entity) {

        var url = "/api/admin/projects/costs/move?project=" + entity["public-id"] + "&costs=" + costIds.join(",");

        Machinata.apiCall(url)
            .success(function (message) {
                if (onSuccess == null) {
                    Machinata.goToPage("/admin/projects/project/edit-costs/" + message.data.project.path);
                } else {
                    onSuccess(message);
                }
            })
            .error(function (error) {

                if (error.data.code == "worktype-error") {
                    Machinata.yesNoDialog('{text.costs-move-error=Costs Move Error}', error.data.message)
                        .okay(function () {
                            Machinata.apiCall(url + "&force=true")
                                .success(function (message) {
                                    if (onSuccess == null) {
                                        Machinata.reload();
                                    } else {
                                        onSuccess(message);
                                    }
                                }).send();
                        }).show();
                }
                else {
                    Machinata.apiError(error.data.code, error.data.message, error);
                }

            })
            .send();

    });

};


Machinata.Projects.changeProjectCostWorktypesAPICall = function (worktypeId, billable, costIds) {
    if (costIds == null) {
        costIds = Machinata.Projects.getSelectedCostIds();
    }
    var url = "/api/admin/projects/costs/change-worktype?worktype=" + worktypeId + "&billable=" + billable + "&costs=" + costIds.join(",");
    Machinata.apiCall(url)
        .success(function (response) { Machinata.reload(); })
        .send();

};

Machinata.Projects.changeProjectCostUsersAPICall = function (worktypeId) {
    var costIds = Machinata.Projects.getSelectedCostIds();
    var url = "/api/admin/projects/costs/change-user?user=" + worktypeId +  "&costs=" + costIds.join(",");
    Machinata.apiCall(url)
        .success(function (response) { Machinata.reload(); })
        .send();

};


Machinata.Projects.changeProjectCostWorktypesDialog = function (worktypes, costIds) {
    if (worktypes == null || Object.keys(worktypes).length === 0) {
        Machinata.genericError("no-worktypes", "No worktypes available for this project");
        return;
    }

    Machinata.optionsDialog('{text.costs-change-worktype.title}', '{text.costs-change-worktype.message}', worktypes)
        .okay(function (result) {
            Machinata.optionsDialog('{text.costs-change-worktype.billable.title}', '{text.costs-change-worktype.billable.message}', { true: 'Billable', false: 'Not Billable', null: 'Leave as is' })
                .okay(function (resultBillable) {

                    Machinata.Projects.changeProjectCostWorktypesAPICall(result, resultBillable, costIds);
                }).
                cancel(function () {


                })
                .show(); return;
        })


        .show()

};



Machinata.Projects.changeProjectCostUsersDialog = function (users) {
    Machinata.debug("users", users);
    if (users == null || Object.keys(users).length === 0) {
        Machinata.genericError("no-users", "No users available for this project");
        return;
    }

    Machinata.optionsDialog('{text.costs-change-user.title}', '{text.costs-change-user.message}', users)
        .okay(function (result) {

            Machinata.Projects.changeProjectCostUsersAPICall(result);
        })
        .show();
};


Machinata.Projects.changeProjectCostBilledDialog = function () {
  
    //Machinata.yesNoDialog('{text.costs-change-billed.title}', '{text.costs-change-billed.message}')
    var diag = Machinata.dateDialog("{text.costs-change-billed.message}", null, null, null, null, null, false);
    diag.input(function (val, id) {

        if (id != "clear") {

            // .okay(function () {
            var costIds = Machinata.Projects.getSelectedCostIds();
            var dateString = Machinata.formattedDate(val, Machinata.DEFAULT_DATE_FORMAT_JAVASCRIPT_UI)
            var url = "/api/admin/projects/costs/change-billed?billed=" + dateString + "&costs=" + costIds.join(",");
            Machinata.apiCall(url)
                .success(function (response) { Machinata.reload(); })
                .send();
        }


        }).show();
};


Machinata.Projects.deleteProjectCostsDialog = function () {
  
   var diag= Machinata.confirmDialog('{text.costs-delete.title}', '{text.costs-delete.message}')
   // var diag = Machinata.dateDialog("{text.costs-delete.message}", null, null, null, null, null, false);
    diag.okay(function (val, id) {

        

            // .okay(function () {
            var costIds = Machinata.Projects.getSelectedCostIds();
            var url = "/api/admin/projects/costs/delete?costs=" + costIds.join(",");
            Machinata.apiCall(url)
                .success(function (response) { Machinata.reload(); })
                .send();
       


        }).show();
};