
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Projects.Model;
using Machinata.Module.Finance.Model;
using Machinata.Module.Projects.Logic;

namespace Machinata.Module.Projects.Handler {


    public class ProjectInvoicesAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("projects");
        }

        #endregion

        [RequestHandler("/admin/projects/invoices")]
        public void Default() {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            IQueryable<Invoice> entities = GetInvoices();
            entities = this.Template.Paginate(entities, this);

            this.Template.InsertEntityList(
                entities: entities,
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING).Include(nameof(ProjectEvent.ProjectPath)),
                link: "/admin/finance/invoices/invoice/{entity.public-id}"
                );


            this.Navigation.Add("projects");
            this.Navigation.Add("invoices");
        }

        private IQueryable<Invoice> GetInvoices() {
            var byCosts = this.DB.ProjectCosts()
                .Include(nameof(ProjectCost.Invoice))
                .Where(pc => pc.Invoice != null)
                .Select(pc => pc.Invoice);

            // New: we have a direct link to the invoices
            var byProject =  this.DB.Projects()
                .Include(nameof(Project.Invoices))
                .SelectMany(pc => pc.Invoices);

            var all = byCosts.Concat(byProject).Distinct();

            return all;
        }




    }
}
