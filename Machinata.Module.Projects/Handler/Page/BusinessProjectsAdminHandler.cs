
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using System.Collections;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Projects.Model;
using Machinata.Module.Projects.Logic;

namespace Machinata.Module.Projects.Handler {


    public class BusinessProjectsAdminHandler : AdminPageTemplateHandler {


        [RequestHandler("/admin/projects/businesses")]
        public void Businesses() {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            var entities = this.DB
                .ActiveBusinesses(includeAdresses: true)
                .OrderByDescending(e => e.Created);
            entities = this.Template.Paginate(entities, this, nameof(Business.Name), "asc");
            this.Template.InsertEntityList("entity-list", entities, new FormBuilder(Forms.Admin.LISTING), "/admin/projects/businesses/business/{entity.public-id}");

            // Navigation
            this.Navigation.Add("projects", "{text.projects}");
            this.Navigation.Add("businesses", "{text.businesses}");
        }


        [RequestHandler("/admin/projects/businesses/business/{publicId}")]
        public void BusinessView(string publicId) {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            var entity = DB.Businesses().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertCard("entity.card", entity.VisualCard());
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW), true, true);

            // Projects
            var rootProjects = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Where(p => p.Business.Id == entity.Id && p.ParentId == null && p.ProjectType == Project.ProjectTypes.Project)
                .Where(p => p.Deleted == false)
                ;
            this.Template.InsertEntityList(
            variableName: "projects",
            entities: rootProjects,
            form: new FormBuilder(Forms.Admin.LISTING),
            link: "{entity.url}");


            // Color
            var color = Project.GetColor(entity);
            this.Template.InsertPropertyList("business.project-color", color);
            this.Template.InsertVariable("business.project-color.color", color?.Color);


            // Navigation
            this.Navigation.Add("projects", "{text.projects}");
            this.Navigation.Add($"businesses/business/{publicId}", entity.Name);
        }



        [RequestHandler("/admin/projects/businesses/business/{publicId}/trash")]
        public void BusinessTrash(string publicId) {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);


            var entity = DB.Businesses().GetByPublicId(publicId);
            entity.LoadFirstLevelNavigationReferences();
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertCard("entity.card", entity.VisualCard());
            this.Template.InsertPropertyList("entity", entity, new FormBuilder(Forms.Admin.VIEW), true, true);

            // Projects
            {
                var projects = this.DB.Projects()
                    .Include(nameof(Project.Business))
                    .Where(p => p.Business.Id == entity.Id && p.ProjectType == Project.ProjectTypes.Project)
                    .Where(p => p.Deleted == true)
                    ;
                this.Template.InsertEntityList(
                variableName: "projects",
                entities: projects,
                form: new FormBuilder(Forms.Admin.LISTING)
                .Include(nameof(Project.ShortURLPath))
                .Include(nameof(Project.IsRootProject))
                .Exclude(nameof(Project.Business)),
                link: "{page.navigation.current-path}/project/{entity.public-id}");
            }



            {
                // Costs
                var costs = this.DB.ProjectCosts()
                    .Include(nameof(ProjectCost.Business))
                    .Include(nameof(ProjectCost.Project))
                    .Where(p => p.Business.Id == entity.Id)
                    .Where(p => p.Deleted == true);

                this.Template.InsertEntityList(
                 variableName: "costs",
                 entities: costs,
                 form: new FormBuilder(Forms.Admin.LISTING),
                 link: "{page.navigation.current-path}/cost/{entity.public-id}"
                 );

            }

            {
                // Events
                var events = this.DB.ProjectEvents()
                  .Include(nameof(ProjectEvent.Project))
                  .Where(e => e.Project.Business.Id == entity.Id)
                  .Where(e => e.Deleted == true);

                this.Template.InsertEntityList(
                   variableName: "events",
                   entities: events,
                   form: new FormBuilder(Forms.Admin.LISTING),
                   link: "{page.navigation.current-path}/event/{entity.public-id}"
                   );
            }

            {
                // Tasks
                var tasks = this.DB.ProjectTasks()
                  .Include(nameof(ProjectTask.Project))
                  .Where(t => t.Project.Business.Id == entity.Id)
                  .Where(t => t.Deleted == true);

                this.Template.InsertEntityList(
                    variableName: "tasks",
                    entities: tasks,
                    form: new FormBuilder(Forms.Admin.LISTING),
                    link: "{page.navigation.current-path}/task/{entity.public-id}"
                    );
            }


            // Navigation
            this.Navigation.Add("projects", "{text.projects}");
            this.Navigation.Add($"businesses/business/{publicId}", entity.Name);
            this.Navigation.Add($"trash");
        }


        [RequestHandler("/admin/projects/businesses/business/{businessId}/trash/project/{projectId}")]
        public void BusinessTrashProject(string businessId, string projectId) {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            var entity = this.DB.Projects()
                .Include(nameof(Project.Business))
                .GetByPublicId(projectId);

            // Form
            this.Template.InsertPropertyList(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.VIEW)
                );

            // Vars
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("projects", "{text.projects}");
            this.Navigation.Add($"businesses/business/{businessId}", entity.Business.Name);
            this.Navigation.Add($"trash");
            this.Navigation.Add($"project/" + projectId, entity.Name);
        }

        [RequestHandler("/admin/projects/businesses/business/{businessId}/trash/cost/{costId}")]
        public void BusinessTrashCost(string businessId, string costId) {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            var entity = this.DB.ProjectCosts()
                .Include(nameof(ProjectCost.Business))
                 .Include(nameof(ProjectCost.Project))
                .GetByPublicId(costId);

            // Form
            this.Template.InsertPropertyList(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.VIEW)
                );

            // Vars
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("projects", "{text.projects}");
            this.Navigation.Add($"businesses/business/{businessId}", entity.Project.Business.Name);
            this.Navigation.Add($"trash");
            this.Navigation.Add($"cost/" + costId, entity.Title);
        }

        [RequestHandler("/admin/projects/businesses/business/{businessId}/trash/task/{taskId}")]
        public void BusinessTrashTask(string businessId, string taskId) {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            var entity = this.DB.ProjectTasks()
                .Include(nameof(ProjectTask.Project) + "." + nameof(Project.Business))
                .GetByPublicId(taskId);

            // Form
            this.Template.InsertPropertyList(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.VIEW)
                );

            // Vars
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("projects", "{text.projects}");
            this.Navigation.Add($"businesses/business/{businessId}", entity.Project.Business.Name);
            this.Navigation.Add($"trash");
            this.Navigation.Add($"task/" + taskId, entity.Title);
        }

        [RequestHandler("/admin/projects/businesses/business/{businessId}/trash/event/{eventId}")]
        public void BusinessTrashEvent(string businessId, string eventId) {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            var entity = this.DB.ProjectTasks()
                .Include(nameof(ProjectEvent.Project) + "." + nameof(Project.Business))
                .GetByPublicId(eventId);
          
            // Form
            this.Template.InsertPropertyList(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.VIEW)
                );

            // Vars
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("projects", "{text.projects}");
            this.Navigation.Add($"businesses/business/{businessId}", entity.Project.Business.Name);
            this.Navigation.Add($"trash");
            this.Navigation.Add($"event/" + eventId, entity.Title);
        }


    }
}
