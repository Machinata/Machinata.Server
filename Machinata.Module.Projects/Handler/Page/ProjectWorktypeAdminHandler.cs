
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Projects.Model;
using Machinata.Module.Projects.Logic;

namespace Machinata.Module.Projects.Handler {


    public class ProjectWorktypeAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("projects");
        }

        #endregion

        [RequestHandler("/admin/projects/worktypes")]
        public void Default() {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            var entities = this.DB.ProjectWorkTypes().AsQueryable();
            entities = this.Template.Paginate(entities, this, nameof(ProjectWorkType.Group),"asc");

            this.Template.InsertEntityList(
                entities: entities,
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "{page.navigation.current-path}/worktype/{entity.public-id}"
                );
            Navigation.Add("projects");
            Navigation.Add("worktypes");
        }

    

        [RequestHandler("/admin/projects/worktypes/create")]
        public void Create() {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            var entity = new ProjectWorkType();

            // Form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.CREATE),
                apiCall : "/api/admin/projects/worktypes/create",
                onSuccess: "/admin/projects/worktypes/worktype/{project-work-type.public-id}"
                );

            // Navigation
            Navigation.Add("projects");
            Navigation.Add("worktypes");
            Navigation.Add("create");
        }

        [RequestHandler("/admin/projects/worktypes/worktype/{publicId}")]
        public void View(string publicId) {


            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);


            // Entity
            var entity = this.DB.ProjectWorkTypes().GetByPublicId(publicId);

            // Props
            this.Template.InsertPropertyList(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.VIEW)
                );

            // Vars
            this.Template.InsertVariables("entity", entity);

            // Nav
            Navigation.Add("projects");
            Navigation.Add("worktypes");
            Navigation.Add("worktype/" + entity.PublicId, entity.Name);

        }

      

        [RequestHandler("/admin/projects/worktypes/worktype/{publicId}/edit")]
        public void Edit(string publicId) {


            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);


            // Menu items

            var entity = this.DB.ProjectWorkTypes()
          
            .GetByPublicId(publicId);

            // Form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/projects/worktype/{entity.PublicId}/edit",
                onSuccess: $"/admin/projects/worktypes/worktype/{entity.PublicId}"
                );

            Navigation.Add("projects");
            Navigation.Add("worktypes");
            Navigation.Add("worktype/" + entity.PublicId, entity.Name);
            Navigation.Add("edit");
        }
        
    }
}
