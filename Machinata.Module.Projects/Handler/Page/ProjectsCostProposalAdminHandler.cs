
using System;
using System.Collections.Generic;
using System.Linq;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Module.Projects.Model;
using Machinata.Module.Projects.Logic;
using Machinata.Module.Projects.View;
using Machinata.Module.Projects.Tasks;
using Machinata.Core.Templates;

namespace Machinata.Module.Projects.Handler {


    public class ProjectsCostProposalAdminHandler : AdminPageTemplateHandler {

      
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("projects");
        }

        #endregion

        public override void DefaultNavigation() {
            base.DefaultNavigation();
            this.Navigation.Add("projects", "{text.projects}");
            this.Navigation.Add("proposals", "{text.proposals}");
        }




        [RequestHandler("/admin/projects/proposals")]
        public void ProposalsList() {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            // Filter params
            var statusStrings = this.Params.AllParamsAndValues().Where(k => k.Key.StartsWith("status-"));
            var statusEnums = new List<ProjectCostProposal.Statuses>();
            foreach(var statusString in statusStrings) {
                if (string.IsNullOrWhiteSpace(statusString.Value) == false) {
                    var statusEnum = Core.Util.EnumHelper.ParseEnum<ProjectCostProposal.Statuses>(statusString.Value);
                    statusEnums.Add(statusEnum);
                }
            }

            // Proposals
            var projects = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Include(nameof(Project.Description))
                .Where(p => p.ParentId == null
                         && p.ProjectType == Project.ProjectTypes.CostProposal
                         && p.Deleted == false
                         && p.Archived == false);

            projects = this.Template.Filter(projects, this, new string[] { nameof(ProjectCostProposal.Name), nameof(ProjectCostProposal.ImplementationJSON) });

            var proposals = projects.ToList().Select(p => p.LoadImplementation() as ProjectCostProposal).AsQueryable();

         

            // Status Filters
            {
                var availableStatuses = proposals.Select(e => e.Status).Distinct().ToList();
                var statuses = Core.Util.EnumHelper.GetEnumValues<ProjectCostProposal.Statuses>(typeof(ProjectCostProposal.Statuses));
                foreach(var statusEnum in statusEnums) { 
                    proposals = proposals.Where(p => p.Status == statusEnum);
                }

                this.Template.InsertFilters("filter-statuses", statusEnums.Select(s => s.ToString()), statuses.Select(s => s.ToString()), availableStatuses.Select(s => s.ToString()), "status", postfixIndexEachParamter: true);
            }

            proposals = PaginateAndList(proposals);


        }

       

        private IQueryable<T> PaginateAndList<T>(IQueryable<T> proposals) where T: Project {
          
            proposals = this.Template.Paginate(proposals, this, null);
          
            var form = new FormBuilder(Forms.Admin.LISTING);
            form.Include(nameof(Project.Created));
            form.Include(nameof(Project.ShortDescription));
            form.Include(nameof(Project.Budget));

            this.Template.InsertEntityList(
                entities: proposals.AsQueryable(),
                variableName: "entities",
                form: form,
                link: "/admin/projects/proposals/proposal/{entity.public-id}"
                );
            return proposals;
        }

        [RequestHandler("/admin/projects/proposals/all")]
        public void ProposalsListAll() {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            var projects = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Include(nameof(Project.Description))
                .Where(p => p.ParentId == null
                         && p.ProjectType == Project.ProjectTypes.CostProposal
                         && p.Deleted == false
                         )
                .OrderBy(p => p.Business.Name);
            projects = this.Template.Filter(projects, this, new string[] { nameof(ProjectCostProposal.Name), nameof(ProjectCostProposal.ImplementationJSON) });

            var proposals = projects.ToList().Select(p => p.LoadImplementation() as ProjectCostProposal);

            PaginateAndList(proposals.AsQueryable());

            // Navigation
            Navigation.Add("all");
        }

        [RequestHandler("/admin/projects/proposals/create")]
        public void ProposalCreate() {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            this.RequireWriteARN();

            var entity = new Project();
            entity.ProjectType = Project.ProjectTypes.CostProposal;

            var businessId = this.Params.String("business");

            var businesses = this.DB.ActiveBusinesses().Where(b => b.IsOwner == false);

            Business selected = null;
            if (string.IsNullOrEmpty(businessId)== false) {
                selected = businesses.GetByPublicId(businessId, false);
            }

            var form = new FormBuilder(Forms.Admin.CREATE)
               .Exclude(nameof(Project.Business))
               .Exclude(nameof(Project.ProjectType))
               .Exclude(nameof(Project.TimeRange));

            form.DropdownListEntities("Business", businesses, selected, required: false, null);

            // Form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: form,

                apiCall: "/api/admin/projects/proposals/create",
                onSuccess: "/admin/projects/proposals/proposal/{project.public-id}/edit"
                );


            // Navigation
            this.Navigation.Add("create");
        }
                          
        [RequestHandler("/admin/projects/proposals/proposal/{publicId}")]
        public void ProjectView(string publicId) {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);


            // Entity
            var entity = this.DB.Projects()
                .Include(nameof(Project.Description))
                .Include(nameof(Project.Notes))
                .Include(nameof(Project.Business))
                .Include(nameof(Project.Invoices))
                .Include(nameof(Project.Children) + "." + nameof(Project.Costs))
                .Include(nameof(Project.Children) + "." + nameof(Project.Description))
                .Where(p => p.ProjectType == Project.ProjectTypes.CostProposal)
                .GetByPublicId(publicId);

            var impl = entity.LoadImplementation();


            // Props
            var form = new FormBuilder(ProjectsForms.PROJECT_PROPOSALS_VIEW_FORM)
                .Include(nameof(Project.Business));
            this.Template.InsertPropertyList(
                entity: impl,
                variableName: "form",
                form: form,
                showCard: false
                );



            // Sub Projs x Preview
            this.InsertProjectProposalsSubprojects("groups", entity);

            // Total
            this.InserProjectProposalsTotalGroup("groups-overview", entity, new FormBuilder(ProjectsForms.PROJECT_PROPOSALS_PDF_FORM));

            // Groups / Subprojects
            {
                var subprojects = entity.Children.ToList().Select(e => e.LoadImplementation() as ProjectCostProposal);
                var groupsListingForm = new FormBuilder(Forms.EMPTY)
                    .Include(nameof(Project.Name))
                    .Include(nameof(Project.Budget));
                this.Template.InsertSortableList(
                    variableName: "entities",
                    entities: subprojects.OrderBy(s=>s.Sort).AsQueryable(),
                    form: groupsListingForm,
                    link: "/admin/projects/proposals/proposal/" + entity.PublicId + "/groups/group/{entity.public-id}",
                    sortingAPICall: $"/api/admin/projects/proposals/proposal/{entity.PublicId}/subprojects/sort");

            }
            IEnumerable<ContentNode> appendicesNodes = GetAppendicesNodes(this.DB);
            var appendicesJObject = appendicesNodes.ToDictionary(k => k.ShortURL, v => v.Name);

            // Vars
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertVariable("proposal-appendices-cms-path", Project.COST_PROPOSAL_APPENDIX_PATH);


            this.Template.InsertJSON("proposal-appendices", appendicesJObject, lowerDashed: false);

            this.Template.InsertVariable("entity.subprojects-show-class", entity.Children.Any(p => p.Budget.HasValue) ? "" : "hidden-content");

            // Navigation
            // AddProjectNavigation(this.Navigation, entity);

            this.Navigation.Add("proposal/" + publicId, entity.Name);

        }

        public static IEnumerable<ContentNode> GetAppendicesNodes(ModelContext db) {
            // Appendices
            return db.ContentNodes().GetPagesAtPath(Project.COST_PROPOSAL_APPENDIX_PATH);
        }
 

        [RequestHandler("/admin/projects/proposals/proposal/{publicId}/groups/group/{groupId}")]
        public void GroupsGroup(string publicId, string groupId) {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);


            // Group
            var parent = this.DB.Projects()
                 .Include(nameof(Project.Description)/* + "." + nameof(ContentNode.Children)*/) // EF bug
                 .Include(nameof(Project.Notes))
                 .Include(nameof(Project.Business))
                 .Include(nameof(Project.Invoices))
                 .Include(nameof(Project.Children) + "." + nameof(Project.Costs))
                 //.Include(nameof(Project.Children) + "." + nameof(Project.Description) + "." + nameof(ContentNode.Children)) NOT WORKING
                 //.Include(nameof(Project.Children) + "." + nameof(Project.Notes) + "." + nameof(ContentNode.Children)) NOT WORKING
                 .Where(p => p.ProjectType == Project.ProjectTypes.CostProposal)
                 .GetByPublicId(publicId);
            var group = parent.Children.AsQueryable().GetByPublicId(groupId);
            group.LoadFirstLevelObjectReferences();
            group.LoadFirstLevelNavigationReferences();

            var groupImpl = group.LoadImplementation() as ProjectCostProposal;

            // Properties
            this.Template.InsertPropertyList(
                variableName: "form",
                entity: groupImpl,
                form: ProjectsForms.GetProposalViewForm(groupImpl),
                showCard: false
                );


            // Auto add first line item
            var changed = false; ;
            if (group.Costs.Count == 0) {

                var cost = new ProjectCost();
                group.Costs.Add(cost);
                cost.TimeRange = new DateRange();
                cost.Description = "Cost 1";
                cost.Project = group;
                cost.CostPerUnit = groupImpl.DefaultCostsPerUnit.Clone();
                cost.Units = 1;
                cost.Business = group.Business;
                cost.Billable = true;
                cost.ProjectCostType = ProjectCost.ProjectCostTypes.Timesheet;
                // Save later? 
                changed = true;
            }

            // Save if changed
            if (changed == true) {
                this.DB.SaveChanges();
            }

            var form = new FormBuilder(Forms.Admin.LISTEDIT);
            this.Template.InsertEditableEntityList(
                variableName: "group.line-items",
                editAPICall: $"/api/admin/projects/proposals/proposal/{parent.PublicId}/group/{group.PublicId}/edit-line-items",
                editAPIOnSuccess: "{page.navigation.current-path}",
                entities: group.Costs.OrderBy(c => c.Sort).AsQueryable(),
                form: form
                //  sortingAPICall: $"/api/admin/projects/proposals/proposal/{parent.PublicId}/subproject/{group.PublicId}/costs/sort" --> deactivated! we save on "save" button
                );


            // Vars
            this.Template.InsertVariables("entity", group);
            this.Template.InsertVariables("impl", groupImpl);
            this.Template.InsertVariable("unit-name", groupImpl.GetUnitName());
            this.Template.InsertVariables("parent", parent);

            // Navigation
            this.Navigation.Add("proposal/" + publicId, parent.Name);
            this.Navigation.Add($"groups/group/" + groupId, group.Name);

        }

       


        [RequestHandler("/admin/projects/proposals/proposal/{publicId}/edit")]
        public void ProjectEdit(string publicId) {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);
            this.RequireWriteARN();
            // Menu items
            var entity = this.DB.Projects()
            .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
            .Include(nameof(Project.WorkTypes))
            .Include(nameof(Project.Parent))
            .Include(nameof(Project.Business))
            .Where(e => e.ProjectType == Project.ProjectTypes.CostProposal)
            .GetByPublicId(publicId);
            var impl = entity.LoadImplementation() as ProjectCostProposal;
            var appendicesNodes = GetAppendicesNodes(this.DB);
            FormBuilder form = ProjectsForms.GetProposalEditForm(impl, appendicesNodes);


            // Form
            this.Template.InsertForm(
                entity: impl,
                variableName: "form",
                form: form,
                apiCall: $"/api/admin/projects/proposals/proposal/{entity.PublicId}/edit",
                onSuccess: $"/admin/projects/proposals/proposal/{entity.PublicId}"
                );



            // Users
            this.Template.InsertSelectionList(
                variableName: "users",
                entities: this.DB.Users().Active(),
                selectedEntities: entity.Users.Select(u => u.User),
                form: new FormBuilder(Forms.Admin.SELECTION),
                selectionAPICall: $"/api/admin/projects/proposals/proposal/{entity.PublicId}/" + "toggle-user/{entity.public-id}");

            //// Worktypes
            //this.Template.InsertSelectionList(
            //    variableName: "worktypes",
            //    entities: this.DB.ProjectWorkTypes().OrderBy(w => w.Group).ThenBy(w => w.CostPerUnit).ThenBy(w => w.Name),
            //    selectedEntities: entity.WorkTypes,
            //    form: new FormBuilder(Forms.Admin.SELECTION),
            //    selectionAPICall: $"/api/admin/projects/proposals/proposal/{entity.PublicId}/" + "toggle-worktype/{entity.public-id}");

            // Vars
            this.Template.InsertVariable("entity.is-root", entity.IsRootProject);

            // Navigation
            this.Navigation.Add("proposal/" + publicId, entity.Name);
            this.Navigation.Add("edit");
        }

      




        #region Edit Sub Projects /////////////////////////////////////////////////////////////////


        [RequestHandler("/admin/projects/proposals/proposal/{publicId}/groups/group/{groupId}/edit")]
        public void GroupsGroupEdit(string publicId, string groupId) {
            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);
            this.RequireWriteARN();
            var entity = this.DB.Projects()
             //.Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
             .Include(nameof(Project.Business))
             .Include(nameof(Project.Children) + "." + nameof(Project.Costs))
             .Where(e => e.ProjectType == Project.ProjectTypes.CostProposal)
             .GetByPublicId(publicId);

            var group = entity.Children.AsQueryable().GetByPublicId(groupId);
            var groupImpl = group.LoadImplementation() as ProjectCostProposal;
            var appendicesNodes = ProjectsCostProposalAdminHandler.GetAppendicesNodes(this.DB);

         

            FormBuilder form = ProjectsForms.GetProposalEditForm(groupImpl, appendicesNodes);
            // Form
            this.Template.InsertForm(
                form: form,
                entity: groupImpl,
                variableName: "form",
                apiCall: $"/api/admin/projects/proposals/proposal/{group.PublicId}/edit",
                onSuccess: "{page.navigation.prev-path}"
                );




            // Vars
            this.Template.InsertVariables("entity", entity);

            // Nav
            this.Navigation.Add($"proposal/{entity.PublicId}", "{text.proposal} " + entity.Name);
            //this.Navigation.Add($"groups");
            // this.Navigation.Add($"group/" + groupId, group.Name);
            this.Navigation.Add($"groups/group/" + groupId, group.Name);
            this.Navigation.Add($"edit");

        }

      

        #endregion

      


    }
}
