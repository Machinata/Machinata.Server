
using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;

namespace Machinata.Module.Projects.Handler {


    public class ProjectsTimelineAdminHandler : AdminPageTemplateHandler {
        
        [RequestHandler("/admin/projects/timeline")]
        public void Default() {
            // Navigation
            Navigation.Add("projects", "{text.projects}");
            Navigation.Add("timeline", "{text.timeline}");
        }

        [RequestHandler("/admin/projects/timeline/fullscreen")]
        public void Fullscreen() {
         
        }

    }
}
