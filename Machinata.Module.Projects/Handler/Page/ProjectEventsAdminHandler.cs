
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Projects.Model;
using Machinata.Module.Admin.View;
using Machinata.Module.Projects.View;
using Machinata.Module.Projects.Logic;

namespace Machinata.Module.Projects.Handler {


    public class ProjectEventsAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("projects");
        }

        #endregion

        [RequestHandler("/admin/projects/events")]
        public void Default() {

            var entities = this.DB.ProjectEvents()
                .Include(nameof(ProjectEvent.Project) + "." + nameof(Project.Business))
                .Include(nameof(ProjectEvent.Description))
                .AsQueryable();

            // Rights
            if (this.CheckIsInAdminGroup(this.User, throwExceptions: false) == false) {
                entities = entities.ToList().Where(e => e.Project != null && this.CheckAdminRightsOrProjectMembership(e.Project.GetRootProject(), this.User, false) == true).AsQueryable();
            } 

            // Weekly Events
            //this.Template.InsertWeeklyEvents("weekly-events",entities, DateTime.UtcNow);

            // All events
            var paged = this.Template.Paginate(entities, this, nameof(ProjectEvent.Date));
            this.Template.InsertEntityList(
                entities: paged,
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/projects/project/event/{entity.project-path}/{entity.public-id}"
                );


            this.Navigation.Add("projects");
            this.Navigation.Add("events");
        }

       
        }
        
    
}
