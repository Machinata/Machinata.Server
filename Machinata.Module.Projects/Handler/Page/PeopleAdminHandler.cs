
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Projects.Model;
using Machinata.Module.Admin.View;
using Machinata.Module.Projects.View;
using Machinata.Core.Reporting;
using Machinata.Module.Projects.Logic;

namespace Machinata.Module.Projects.Handler {


    public class PeopleAdminHandler : AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("projects");
        }

        #endregion

        [RequestHandler("/admin/projects/people")]
        public void Default() {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            // Users
            var users = this.DB.Users().OrderBy(u => u.Name);

            this.Template.InsertEntityList(
                entities: users,
                variableName: "users",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/projects/people/user/{entity.public-id}"
                );

            this.Navigation.Add("projects");
            this.Navigation.Add("people");
        }




        [RequestHandler("/admin/projects/people/user/{publicId}")]
        public void UserView(string publicId) {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            var user = this.DB.Users().GetByPublicId(publicId);
            var entities = this.DB.ProjectCosts()
                 .Include(nameof(ProjectCost.Project))
                 .Include(nameof(ProjectCost.User))
                  .Include(nameof(ProjectCost.Business))
                 .Where(pc => pc.User.Id == user.Id && pc.Deleted == false);


            // All absences
            var paged = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList(
                entities: paged,
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/projects/project/cost/{entity.project-path}/" + "{entity.public-id}"
                );

            this.Navigation.Add("projects");
            this.Navigation.Add("people");
            this.Navigation.Add("user/" + publicId, user.Username);
        }

        [RequestHandler("/admin/projects/people/hours")]
        public void Hours() {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);


            this.Navigation.Add("projects");
            this.Navigation.Add("people");
            this.Navigation.Add("hours");
        }

        [RequestHandler("/admin/projects/people/hours/export", null, null, Verbs.Get, ContentType.Binary)]
        public void HoursExport() {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            // Sheet name
            var monthName = this.Params.String("start");

            // Start and End of month
            var startLocaltime = this.Params.DateTimeFormat("start", "yyyy.MM", Core.Util.Time.StartOfMonth(DateTime.UtcNow.ToDefaultTimezone()));
            var endLocalTime = startLocaltime.EndOfMonth();

            // UTC
            var start = startLocaltime.ToUniversalTime();
            var end = endLocalTime.ToUniversalTime();

            // Owner
         
            var excludeOwnerBusiness = this.Params.Bool("excludeowner", true);

            // Data
            var result = ProjectUserHoursUnit.CreateDailyHoursReport(this.DB, start, end, excludeOwnerBusiness, monthName);
        
            // To XLSX
            var content = ProjectUserHoursUnit.ExportXLSX(result, monthName);

            var fileName = $"People_Hours_{monthName}";

            if (excludeOwnerBusiness) {
                fileName += "_ClientsOnly";
            }else {
                fileName += "_AllHours";
            }

            // Send
            this.SendBinary(content, $"{fileName}.xlsx");
        }



        [RequestHandler("/admin/projects/people/user/{publicId}/hours/export", null, null, Verbs.Get, ContentType.Binary)]
        public void UserHoursExport(string publicId) {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            // user
            var user = this.DB.Users().GetByPublicId(publicId);

            // Start and End of month
            var startLocaltime = this.Params.DateTimeFormat("start", "yyyy", Core.Util.Time.StartOfYear(DateTime.UtcNow.ToDefaultTimezone()));
            var endLocalTime = startLocaltime.EndOfYear();

            // UTC
            var start = startLocaltime.ToUniversalTime();
            var end = endLocalTime.ToUniversalTime();

            // by user
            var entities = this.DB.ProjectCosts()
                 .Include(nameof(ProjectCost.Project))
                 .Include(nameof(ProjectCost.User))
                 .Include(nameof(ProjectCost.Business))
                 .Where(pc => pc.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet)
                 .Where(pc => pc.User.Id == user.Id && pc.Deleted == false).ToList();

            // by date range
            entities = entities.Where(e => e.TimeRange != null && e.TimeRange.Start >= start && e.TimeRange.End < end).ToList();

            // to xlsx
            var content =  ExportService.ExportXLSX(entities: entities, form: new FormBuilder(Forms.Admin.EXPORT));

            // Sheet name
            var yearName = this.Params.String("start");

            var fileName = $"{user.Name}_Hours_{yearName}";


            // Send
            this.SendBinary(content, $"{fileName}.xlsx");
        }






    }



}

