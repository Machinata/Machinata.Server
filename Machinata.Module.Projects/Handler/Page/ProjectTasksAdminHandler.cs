
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Projects.Model;
using Machinata.Module.Admin.View;
using Machinata.Module.Projects.Logic;

namespace Machinata.Module.Projects.Handler {


    public class ProjectTasksAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("projects");
        }

        #endregion

        [RequestHandler("/admin/projects/tasks")]
        public void Default() {
          
            
            var user = this.DB.Users().GetByUsername(this.Params.String("user"), false);

            var startWeek = Core.Util.Time.StartOfDefaultTimezoneWeekInUtc(DateTime.UtcNow);
            var endWeek = startWeek.EndOfWeek();
            var weeklyTasks = this.DB.ProjectTasks()
                .Include(nameof(ProjectTask.Description))
                .Include(nameof(ProjectEvent.Project) + "." + nameof(Project.Business))
                .Include(nameof(ProjectTask.User))
                .Where(pt => pt.Deadline != null && (pt.Deadline >= startWeek));
            DateTime endRange = endWeek;
            if (weeklyTasks.Any()) {
                var weekendOflastTask = Time.StartOfDefaultTimezoneQuarterInUtc(weeklyTasks.Max(w => w.Deadline.Value)).EndOfWeek();
                if (weekendOflastTask > endRange) {
                    endRange = weekendOflastTask;
                }
            }
           
            if (user != null) {
                weeklyTasks = weeklyTasks.GetByUser(this.DB, user);
            }

            // user
            this.Template.InsertVariable("chart-user", user != null ? user.Username : "all");

            //// Weekly
            //this.Template.InsertScheduleTable(
            //    entities: weeklyTasks,
            //    start: startWeek,
            //    end: endRange,
            //    variableName: "weekly-tasks",
            //    templateName: "schedule-table",
            //    dateSelector: (e) => (e as ProjectTask).Deadline.Value,
            //    getTemplateForDay: getProjectTaskDayTemplate,
            //    selectedSchedule: new DateRange(startWeek,endWeek),
            //    startOfWeek: DayOfWeek.Monday,
            //    endOfWeekToShow: DayOfWeek.Sunday
            //    );


            // All Open tasks
            var opentasks = this.DB.ProjectTasks()

                .Include(nameof(ProjectTask.Project))
                .Include(nameof(ProjectTask.Project) + "." + nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .Where(pt => pt.Completed == false);

            // Filter by user
            if (user != null) {
                opentasks = opentasks.GetByUser(this.DB, user);
            }

            // Filter members
            if (this.CheckIsInAdminGroup(this.User, throwExceptions: false) == false) {
                opentasks = opentasks.ToList().Where(t => t.Project != null && this.CheckAdminRightsOrProjectMembership(t.Project.GetRootProject(), this.User, false) == true).AsQueryable();
            }

            // Pagination
            var paged = this.Template.Paginate(opentasks, this); 

            // Entity List
            this.Template.InsertEntityList(
                entities: paged,
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/projects/project/task/{entity.project-path}/{entity.public-id}"
                );


            this.Navigation.Add("projects");
            this.Navigation.Add("tasks");
        }

        private static PageTemplate getProjectTaskDayTemplate(IEnumerable<ModelObject> tasks, DateTime deadline, PageTemplate template) {
            var dayTemplate = template.LoadTemplate("task-schedule.week.day");
            dayTemplate.InsertTemplates("tasks", tasks, "task-schedule.week.day.task");
            return dayTemplate;
        }
    }
}
