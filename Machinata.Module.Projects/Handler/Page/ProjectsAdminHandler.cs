
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Projects.Model;
using Machinata.Module.Projects.Logic;
using Machinata.Module.Admin.View;
using Machinata.Module.Projects.View;
using Machinata.Module.Projects.Tasks;
using NLog.Filters;

namespace Machinata.Module.Projects.Handler {


    public class ProjectsAdminHandler : AdminPageTemplateHandler {

      
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("projects");
        }

        #endregion


        #region Menu

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "project",
                Path = "/admin/projects",
                Title = "{text.projects}",
                Sort = "500"
            });
        }

        #endregion


        #region Virtual Methods
        public override void InsertAdditionalVariables() {
            base.InsertAdditionalVariables();
            this.Template.InsertVariable("projects.finance-enabled", Config.ProjectsFinanceIntegrationEnabled ? "finance-enabled" : "");
        }
        #endregion

        [RequestHandler("/admin/projects")]
        public void Default() {
            // Menu items
            var menuItems = PageTemplate.Cache.FindAll(this.Template.Package, "admin/projects/menu/menu.item.", this.TemplateExtension);
            this.Template.InsertTemplates("projects.menu-items", menuItems);

            // Events
            var startWeek = Core.Util.Time.StartOfDefaultTimezoneWeekInUtc(DateTime.UtcNow);
            var end = startWeek.EndOfWeek().AddDays(7);
            var events = this.DB.ProjectEvents()
                .Include(nameof(ProjectEvent.Project) + "." +  nameof(Project.Business))
                .Include(nameof(ProjectEvent.Description) )
                .Where(pe => pe.Date != null && pe.Date.Value > startWeek && pe.Date.Value <= end);
            this.Template.InsertWeeklyEvents("weekly-events", events, startWeek, end);

       

            // Navigation
            Navigation.Add("projects", "{text.projects}");
        }

        [RequestHandler("/admin/projects/list")]
        public void ProjectList() {

            var projects = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .Where(p => p.ParentId == null && p.ProjectType == Project.ProjectTypes.Project)
                .OrderBy(p => p.Business.Name);
            // Rights
            var projectsList = projects.ToList().Where(p => this.CheckAdminRightsOrProjectMembership(p, this.User, throwException: false)).AsQueryable();

            projectsList = this.Template.Filter(projectsList, this, nameof(Project.Name));
            projectsList = this.Template.Paginate(projectsList, this, null);

            

            this.Template.InsertEntityList(
                entities: projectsList,
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING),
                link:"/admin/projects/project/{entity.path}"
                );
         
            // Navigation
            Navigation.Add("projects", "{text.projects}");
            Navigation.Add("list");

        }

        [RequestHandler("/admin/projects/create")]
        public void ProjectCreate() {
            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);
            this.RequireWriteARN();

            var entity = new Project();
            entity.ProjectType = Project.ProjectTypes.Project;

            var businessId = this.Params.String("business");

            var businesses = this.DB.ActiveBusinesses();/*.Where(b => b.IsOwner == false);*/

            Business selected = null;
            if (string.IsNullOrEmpty(businessId)== false) {
                selected = businesses.GetByPublicId(businessId, false);
            }

            var form = new FormBuilder(Forms.Admin.CREATE)
               .Exclude(nameof(Project.Business))
                .Hidden("projecttype", Project.ProjectTypes.CostProposal.ToString());

            form.DropdownListEntities("Business", businesses, selected, true, null);

            // Form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: form,

                apiCall: "/api/admin/projects/create",
                onSuccess: "/admin/projects/project/edit/{project.path}"
                );


            // Navigation
            Navigation.Add("projects", "{text.projects}");
            Navigation.Add("create");
        }

        [RequestHandler("/admin/projects/project/{path}")]
        public void ProjectView(string path) {

           
            // Entity
            var entity = this.DB.Projects()
                .Include(nameof(Project.Description))
                .Include(nameof(Project.Notes))
                .Include(nameof(Project.Business))
                .Include(nameof(Project.Invoices))
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .Where(p=>p.Deleted == false)
                .GetByPath(path);

            // Rights
            this.CheckAdminRightsOrProjectMembership(entity, this.User, throwException: true);

            // Form
            var form = new FormBuilder(Forms.Admin.VIEW);
            if (entity.IsRootProject == false) {
                form.Exclude(nameof(Project.CostsOnlyOnSubprojects));
            }

            // Properties
            this.Template.InsertPropertyList(
                entity: entity,
                variableName: "form",
                form: form
                );

            // Tasks
            this.Template.InsertEntityList(
              entities: entity.GetAllTasks(hideDeleted: true).Where(t => !t.Completed).AsQueryable(),
              variableName: "tasks",
              form: new FormBuilder(Forms.Admin.LISTING),
              link: "/admin/projects/project/task/{entity.project-path}/" + "{entity.public-id}"
              );

            // Events
            this.Template.InsertEntityList(
              entities: entity.GetAllEvents(hideDeleted: true).Where(e => e.Date != null && e.Date > DateTime.UtcNow).AsQueryable(),
              variableName: "events",
              form: new FormBuilder(Forms.Admin.LISTING),
              link: "/admin/projects/project/event/{entity.project-path}/" + "{entity.public-id}"
              );

            // Sub Projs
            if (this.Template.HasVariable("subprojects")) { 
                this.Template.InsertTemplates(
                  entities: entity.Children.Where(c => c.Deleted == false),
                  variableName: "subprojects",
                  templateName: "project.subproject"
                  );
            }

            // Budget
           // if(entity.Budget.HasValue || true)
            {
                // External
                {
                    var externalTemplate = this.Template.LoadTemplate("project.budget");
                    ProjectBudget externalBudget = entity.CalculateBudget(ProjectBudget.BudgetTypes.External);
                    externalTemplate.InsertPropertyList("budget.form", externalBudget, new FormBuilder(Forms.Admin.VIEW));
                    externalTemplate.InsertVariable("type", ProjectBudget.BudgetTypes.External);
                    this.Template.InsertTemplate("budget.external", externalTemplate);
                    this.Template.InsertVariables("entity.external-budget", externalBudget);
                }

                // Internal
                {
                    var internalTemplate = this.Template.LoadTemplate("project.budget");
                    ProjectBudget internalBudget = entity.CalculateBudget(ProjectBudget.BudgetTypes.Internal);
                    internalTemplate.InsertPropertyList("budget.form", internalBudget, new FormBuilder(Forms.Admin.VIEW));
                    internalTemplate.InsertVariable("type", ProjectBudget.BudgetTypes.Internal);
                    this.Template.InsertTemplate("budget.internal", internalTemplate);
                }


            }
            
            //else {
            //    this.Template.InsertVariable("budget.external", "");
            //    this.Template.InsertVariable("budget.internal", "");
            //    this.Template.InsertVariable("project.has-external-budget", "false");
            //}

            // Users
            /*
            // Note: this also makes sure user model object for the costs are preloaded
            this.Template.InsertEntityList(
              entities: entity.Users.AsQueryable(),
              variableName: "users",
              form: new FormBuilder(Forms.Admin.LISTING),
              link: "/admin/projects/users/user/{entity.public-id}"
            );*/


            // Costs
            this.Template.InsertEntityList(
              entities: entity.GetAllCosts(hideDeleted: true ,nameof(ProjectCost.User)).OrderByDescending(c=>c.TimeRange.Start).Take(20).AsQueryable(), // Note: this query alone will not include all the users, but since we have them in the context they will automically be fully loaded
              variableName: "costs",
              form: new FormBuilder(Forms.Admin.LISTING).Include(nameof(ProjectCost.Created)),
              link: "/admin/projects/project/cost/{entity.project-path}/" + "{entity.public-id}",
              total: null
            );

            // Invoices
            this.Template.InsertEntityList(
             //entities: entity.GetAllInvoicesFast().Where(o => o.Status < Finance.Model.Invoice.InvoiceStatus.Paid).OrderByDescending(i => i.Created).AsQueryable(),
             entities: entity.GetAllInvoicesFast().OrderByDescending(i => i.Created).AsQueryable(),
             variableName: "invoices",
             form: new FormBuilder(Forms.Admin.LISTING),
             link: "/admin/projects/project/invoice/" + entity.Path + "/" + "{entity.public-id}"
           );


            // warning wrong booked costs
            {
                var hasDirectCostsAndSubprojects = entity.HasDirectCostsAndSubprojects();
                this.Template.InsertVariableBool("entity.show-sub-projects-costs-warning", hasDirectCostsAndSubprojects);
            }

            // Vars
            this.Template.InsertVariables("entity", entity);

            this.Template.InsertVariable("entity.subprojects-show-class", entity.Children.Where(c => c.Deleted == false).Any(p=>p.Budget.HasValue) ? "":"hidden-content");

            // Navigation
            AddProjectNavigation(this.Navigation, entity);

        }


        [RequestHandler("/admin/projects/project/{path}/budgets")]
        public void ProjectBudgets(string path) {
            ProjectView(path);
            this.Navigation.Add("budgets", "{text.budgets}");
        }

        public static void AddProjectNavigation(NavigationBuilder navigationBuilder, Project project) {
            navigationBuilder.Add("projects");
            var p = project.GetRootProject();
            p.Include(nameof(Project.Business));
            var business = p.Business;
            navigationBuilder.Add("/admin/projects/businesses/business/" + business.PublicId, business.Name);
            AddProjectAncestorNavigation(navigationBuilder, project);
        }

        public static void AddProjectAncestorNavigation(NavigationBuilder navigationBuilder, Project project) {
            if (project.ParentId != null) {
                project.Include(nameof(Project.Parent));
                AddProjectAncestorNavigation(navigationBuilder, project.Parent);
            }
            navigationBuilder.Add("/admin/projects/project/" + project.Path, project.Name);
        }


        [RequestHandler("/admin/projects/project/edit/{path}")]
        public void ProjectEdit(string path) {

            // Rights
            this.RequireWriteARN();
            // Menu items
            var entity = this.DB.Projects()
            .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
            .Include(nameof(Project.WorkTypes))
            .Include(nameof(Project.Parent))
            .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
            .OnlyStandardProjects()
            .GetByPath(path);

            // Rights
            this.CheckAdminRightsOrProjectMembership(entity.GetRootProject(), this.User, throwException: true);
            FormBuilder form = GetProjectEditForm(entity);

            // Note: deprecated since it has a bug
            //if (!entity.IsRootProject) {
            //    form = form.Exclude(nameof(Project.Business));
            //}


            // Form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: form,
                apiCall: $"/api/admin/projects/project/{entity.PublicId}/edit",
                onSuccess: "/admin/projects/project/{path}"
                );

            // Users
            this.Template.InsertSelectionList(
                variableName: "users",
                entities: this.DB.Users().Active(),
                selectedEntities: entity.Users.Select(u => u.User),
                form: new FormBuilder(Forms.Admin.SELECTION),
                selectionAPICall: $"/api/admin/projects/project/{entity.PublicId}/" + "toggle-user/{entity.public-id}");

            // Worktypes
            this.Template.InsertSelectionList(
                variableName: "worktypes",
                entities: this.DB.ProjectWorkTypes().OrderBy(w => w.Group).ThenBy(w => w.CostPerUnit).ThenBy(w => w.Name),
                selectedEntities: entity.WorkTypes,
                form: new FormBuilder(Forms.Admin.SELECTION),
                selectionAPICall: $"/api/admin/projects/project/{entity.PublicId}/" + "toggle-worktype/{entity.public-id}");

            // Vars
            this.Template.InsertVariable("entity.is-root", entity.IsRootProject);

            // Navigation
            AddProjectNavigation(this.Navigation, entity);
            Navigation.Add("edit");
        }

        public static FormBuilder GetProjectEditForm(Project project) {

            var form = new FormBuilder(Forms.Admin.EDIT);
            if (project.IsRootProject == false) {
                form.Exclude(nameof(project.Business));
                form.Exclude(nameof(project.CostsOnlyOnSubprojects));
            }
            return form;
        }




        #region Task //////////////////////////////////////////////////////////////////////////////////////////////

        [RequestHandler("/admin/projects/project/create-task/{path}")]
        public void ProjectCreateTask(string path) {

            // Rights
            this.RequireWriteARN();

            var project = this.DB.Projects()
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .GetByPath(path);

            // Rights
            this.CheckAdminRightsOrProjectMembership(project, this.User, throwException: true);

            var entity = new ProjectTask();
            var projectUsers = project.GetProjectUsers(true).AsQueryable();
            var defaultUser = projectUsers.FirstOrDefault(p => p.User == this.User);

            // Vars
            this.Template.InsertVariables("project", project);

            // Form
            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.CREATE).DropdownList("user", projectUsers, defaultUser),
                apiCall: $"/api/admin/projects/project/{project.PublicId}/create-task",
                onSuccess: $"/admin/projects/project/task/{project.Path}/"+"{task.public-id}"
                );

            // Navigation
            AddProjectNavigation(this.Navigation, project);
            Navigation.Add("create-task", "{text.create-task}");
        }

        [RequestHandler("/admin/projects/project/task/{path}/{taskId}")]
        public void Task(string path, string taskId) {

         
         
            var project = this.DB.Projects()
            .Include(nameof(Project.Tasks) + "." + nameof(ProjectTask.User) + "." + nameof(ProjectUser.User))
            .Include(nameof(Project.Tasks) + "." + nameof(ProjectTask.Description))
            .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
            .GetByPath(path);

            // Rights
            this.CheckAdminRightsOrProjectMembership(project, this.User, throwException: true);

            var entity = project.Tasks.Where(t => t.Deleted == false).AsQueryable().GetByPublicId(taskId);
        
            // Form
            this.Template.InsertPropertyList(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.VIEW)
                );

            this.Template.InsertVariables("entity", entity);
            this.Template.InsertVariables("project", project);

            // Navigation
            AddProjectNavigation(this.Navigation, project);
            Navigation.Add("/admin/projects/project/tasks/" + project.Path, "tasks");
            this.Navigation.Add(entity.URL, entity.Title);
        }

        [RequestHandler("/admin/projects/project/task-edit/{path}/{taskId}")]
        public void TaskEdit(string path, string taskId) {

            this.RequireWriteARN();
            var project = this.DB.Projects()
                .Include(nameof(Project.Tasks) + "." + nameof(ProjectTask.User))
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .GetByPath(path);

            // Rights
            this.CheckAdminRightsOrProjectMembership(project, this.User, throwException: true);

            var task = project.Tasks.Where(t => t.Deleted == false).AsQueryable().GetByPublicId(taskId);
            var projectUsers = project.GetProjectUsers(true).AsQueryable();

            // Form
            this.Template.InsertForm(
                entity: task,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.EDIT).DropdownList("user", projectUsers, task.User),
                apiCall: $"/api/admin/projects/task/{taskId}/edit",
                onSuccess: "/admin/projects/project/task/{project.path}/{task.public-id}"
                );

                
            // Navigation
            AddProjectNavigation(this.Navigation, project);
            Navigation.Add("/admin/projects/project/tasks/" + project.Path, "tasks");
            this.Navigation.Add(task.URL, "{text.task}: " + task.Title);
            Navigation.Add("edit");
        }

        [RequestHandler("/admin/projects/project/tasks/{path}")]
        public void ProjectTasks(string path) {
            this.RequireWriteARN();
            var project = this.DB.Projects()
                 .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .GetByPath(path);
            // Rights
            this.CheckAdminRightsOrProjectMembership(project, this.User, throwException: true);
            var entities = project.GetAllTasks(hideDeleted: true).AsQueryable();
            var tasks = Template.Paginate(entities, this, nameof(ProjectTask.Deadline));

            var form = new FormBuilder(Forms.Admin.LISTING);
            form.Include(nameof(ProjectTask.Note));

            // Form
            this.Template.InsertEntityList(
                entities: tasks.AsQueryable(),
                variableName: "entities",
                form: form ,
                link: "{entity.url}"
                );

            this.Template.InsertVariables("project", project);

            // Navigation
            AddProjectNavigation(this.Navigation, project);
            Navigation.Add("tasks");
        }

        #endregion


        #region Event /////////////////////////////////////////////////////////////////////////////////////
        [RequestHandler("/admin/projects/project/event/{path}/{eventId}")]
        public void Event(string path, string eventId) {
            var project = this.DB.Projects()
                .Include(nameof(Project.Events))
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .GetByPath(path);
            var entity = project.Events.Where(e => e.Deleted == false).AsQueryable().GetByPublicId(eventId);
            entity.LoadFirstLevelObjectReferences();


            // Rights
            this.CheckAdminRightsOrProjectMembership(project, this.User, throwException: true);

            // Form
            this.Template.InsertPropertyList(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.VIEW)
                );

            this.Template.InsertVariables("entity", entity);
            this.Template.InsertVariables("project", project);

            // Navigation
            AddProjectNavigation(this.Navigation, project);
            this.Navigation.Add(entity.URL, "{text.event}: " + entity.Title);
        }

        [RequestHandler("/admin/projects/project/event-edit/{path}/{eventId}")]
        public void EventEdit(string path, string eventId) {
            this.RequireWriteARN();
            // Menu items
            var project = this.DB.Projects()
                .Include(nameof(Project.Events))
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .GetByPath(path);
            var projectEvent = project.Events.Where(e => e.Deleted == false).AsQueryable().GetByPublicId(eventId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(project, this.User, throwException: true);

            // Form
            this.Template.InsertForm(
                entity: projectEvent,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/projects/event/{eventId}/edit",
                onSuccess: "/admin/projects/project/event/{project.path}/{event.public-id}"
                );

            // Navigation
            AddProjectNavigation(this.Navigation, project);
            this.Navigation.Add(projectEvent.URL, "{text.event}: " + projectEvent.Title);
            Navigation.Add("edit");
        }


        [RequestHandler("/admin/projects/project/create-event/{path}")]
        public void ProjectCreateEvent(string path) {
            this.RequireWriteARN();
            
            // Project
            var project = this.DB.Projects()
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .GetByPath(path);

            // Rights
            this.CheckAdminRightsOrProjectMembership(project, this.User, throwException: true);

            var entity = new ProjectEvent();

            // Form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.CREATE),
                apiCall: $"/api/admin/projects/project/{project.PublicId}/create-event",
                onSuccess: "/admin/projects/project/event/{project.path}/{event.public-id}"
                );

            // Navigation
            AddProjectNavigation(this.Navigation, project);
            Navigation.Add("create-event", "{text.create-event}");
        }

        [RequestHandler("/admin/projects/project/events/{path}")]
        public void ProjectEvents(string path) {

            this.RequireWriteARN();

            var project = this.DB.Projects()
                 .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .GetByPath(path);

            // Rights
            this.CheckAdminRightsOrProjectMembership(project, this.User, throwException: true);

            var events = Template.Paginate(project.GetAllEvents(hideDeleted: true).AsQueryable(), this,nameof(ProjectEvent.Date) );


            // List
            this.Template.InsertEntityList(
                entities: events.AsQueryable(),
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "{entity.url}"
                );

            // Navigation
            AddProjectNavigation(this.Navigation, project);
            Navigation.Add("events");
        }

        #endregion


        #region Cost /////////////////////////////////////////////////////////////////////////////////////
        [RequestHandler("/admin/projects/project/cost/{path}/{costId}")]
        public void Cost(string path, string costId) {
            var project = this.DB.Projects()
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .Include(nameof(Project.Costs))
                .OnlyStandardProjects()
                .GetByPath(path);

            // Rights
            this.CheckAdminRightsOrProjectMembership(project, this.User, throwException: true);

            var entity = project.Costs.Where(c=>c.Deleted == false).AsQueryable().GetByPublicId(costId);
            entity.Include(nameof(ProjectCost.User));
            entity.Include(nameof(ProjectCost.Invoice));


            var form = new FormBuilder(Forms.Admin.VIEW);
            var debug = this.Params.Bool("debug", false);
            if (debug == true) {
                form.Include(nameof(ProjectCost.OvertimeFactor));
            }

            // Form
            this.Template.InsertPropertyList(
                entity: entity,
                variableName: "form",
                form: form,
                loadFirstLevelReferences: true
            );

            // Vars
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertVariables("project", project);
            if (entity.Invoice != null) {
                this.Template.InsertVariables("invoice", entity.Invoice);
            } 
            
            this.Template.InsertVariable("invoice.available", entity.Invoice != null);

            // Navigation
            AddProjectNavigation(this.Navigation, project);
            this.Navigation.Add(entity.URL, "{text.project-cost}: " + entity.Description);
        }

        [RequestHandler("/admin/projects/project/cost-edit/{path}/{costId}")]
        public void CostEdit(string path, string costId) {
            this.RequireWriteARN();

           
            var project = this.DB.Projects()
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .Include(nameof(Project.Costs)+ "." + nameof(ProjectCost.WorkType))
                .OnlyStandardProjects()
                .GetByPath(path);

            // Rights
            this.CheckAdminRightsOrProjectMembership(project, this.User, throwException: true);

            var projectCost = project.Costs.Where(c=> c.Deleted == false).AsQueryable().GetByPublicId(costId);

            var workTypes = project.GetWorkTypes();
            var projectUsers = project.GetProjectUsers(true).Select(u => u.User).ToList();
            var date = projectCost.TimeRange.Start.Value;
            
            var projectCostForm = ProjectsForms.GetProjectCostFormBuilder(project, projectCost.ProjectCostType.ToString(),date);
            var timeSheetForm = ProjectsForms.GetTimesheetForm(projectCost, project, workTypes, projectUsers, projectCost.User, date,projectCost.Units, projectCost.WorkType);
            var form = projectCost.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet 
                ? timeSheetForm
                : projectCostForm;

            // Form
            this.Template.InsertForm(
                entity: projectCost,
                variableName: "form",
                form: form ,
                apiCall: $"/api/admin/projects/cost/{costId}/edit" + (projectCost.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet? "-timesheet-cost": "-project-cost"),
                onSuccess: "/admin/projects/project/cost/{project.path}/{cost.public-id}"
            );

            // Vars
            this.Template.InsertVariables("cost", projectCost);

            // Navigation
            AddProjectNavigation(this.Navigation, project);
            this.Navigation.Add(projectCost.URL, "{text.project-cost}: " + projectCost.Description);
            Navigation.Add("edit");
        }


      
        [RequestHandler("/admin/projects/project/add-timesheet-cost/{path}")]
        public void ProjectAddTimesheetCost(string path) {
            this.RequireWriteARN();
            var project = this.DB.Projects()
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .Include(nameof(Project.WorkTypes))
                .OnlyStandardProjects()
                .GetByPath(path);
            var projectWorkTypes = project.GetWorkTypes();
            // Rights
            this.CheckAdminRightsOrProjectMembership(project, this.User, throwException: true);

            var entity = new ProjectCost();
            entity.Billable = true;
            var projectUsers = project.GetProjectUsers(true).Select(pu=>pu.User);
            var defaultUser = projectUsers.SingleOrDefault(u => u.Id == this.User.Id);
            //DateTime defaultDate = Core.Util.Time.GetDefaultTimezonesToday();
            DateTime defaultDate = DateTime.UtcNow;

            // Form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: ProjectsForms.GetTimesheetForm(entity, project, projectWorkTypes, projectUsers, defaultUser, defaultDate, 0, projectWorkTypes.FirstOrDefault()),
                apiCall: $"/api/admin/projects/project/{project.PublicId}/add-timesheet-cost",
                onSuccess: "/admin/projects/project/cost/{project.path}/{cost.public-id}"
            );

            // Navigation
            AddProjectNavigation(this.Navigation, project);
            Navigation.Add("add-timesheet-cost", "{text.add-timesheet-entry}");
        }


        [RequestHandler("/admin/projects/project/add-project-cost/{path}")]
        public void ProjectAddProjectCost(string path) {
            this.RequireWriteARN();
            var project = this.DB.Projects()
                .Include(nameof(Project.WorkTypes))
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                 .OnlyStandardProjects()
                .GetByPath(path);

            // Rights
            this.CheckAdminRightsOrProjectMembership(project, this.User, throwException: true);

            var entity = new ProjectCost();
            entity.Units = 1;
            entity.Billable = true;
            entity.Project = project;
           
            DateTime defaultDate = DateTime.UtcNow;
            var projectUsers = project.GetProjectUsers();
            var workTypes = project.GetWorkTypes();
            var defaultUser = projectUsers.SingleOrDefault(u => u.Id == this.User.Id);

            // Vars
            this.Template.InsertVariables("cost", entity);

            // Form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: ProjectsForms.GetProjectCostFormBuilder(project, "Internal", defaultDate),
                apiCall: $"/api/admin/projects/project/{project.PublicId}/add-project-cost",
                onSuccess: "/admin/projects/project/cost/{project.path}/{cost.public-id}"
            );

           

            // Navigation
            AddProjectNavigation(this.Navigation, project);
            Navigation.Add("add-project-cost");
        }

      

        [RequestHandler("/admin/projects/project/generate-invoice/{path}")]
        public void GenerateInvoice(string path) {
            this.RequireWriteARN();

            var dateFormat = "yyyy.MM.dd";
            var timerangeFilter = this.Params.String("timerange", null);
            var currentDateRangeFilter = "";
            
            var projectFilter = this.Params.StringArrayFromQueryString("projects", null);

            var project = this.DB.Projects().GetByPath(path);
            var rootProject = this.DB.Projects()
                .Include(nameof(Project.Users))
                .Include(nameof(Project.Users)+"."+nameof(ProjectCost.User))
                .OnlyStandardProjects()
                .GetRootByPath(path);

            // Rights
            this.CheckAdminRightsOrProjectMembership(rootProject, this.User, throwException: true);

            // Costs
            var unbilledCosts = project.GetUnbilledProjectCosts(hideDeleted: true);
            var filteredCosts = unbilledCosts;

            // Filter Projects
            if (projectFilter != null && projectFilter.Any() == true) {
                var projectIds = projectFilter.Select(pf=> Core.Ids.Obfuscator.Default.UnobfuscateId(pf));
                filteredCosts = filteredCosts.Where(c => c.Project != null &&  projectIds.Contains( c.Project.Id));
            }

            // Filter Date
            if (string.IsNullOrWhiteSpace(timerangeFilter) == false) {
                var startString = timerangeFilter.Split('-').First().Trim();
                var endString = timerangeFilter.Split('-').Last().Trim();
                var start = DateTime.ParseExact(startString, dateFormat, System.Globalization.CultureInfo.InvariantCulture).ToUniversalTime();
                var end = Core.Util.Time.EndOfDay(DateTime.ParseExact(endString, dateFormat, System.Globalization.CultureInfo.InvariantCulture)).ToUniversalTime();
                currentDateRangeFilter = start.ToString(dateFormat) + " - " + end.ToString(dateFormat);

                filteredCosts = filteredCosts.Where(c => c.TimeRange != null && c.TimeRange.Start >= start && c.TimeRange.End <= end);
            }
            this.Template.InsertVariable("currentDateRangeFilter", currentDateRangeFilter);
            this.Template.InsertVariable("filtered-project", projectFilter?.Any() == true ? string.Join(",",projectFilter): "all");
            this.Template.InsertVariable("filtered-timerange", string.IsNullOrWhiteSpace(currentDateRangeFilter) == false ? "timerange" : "all");

            this.Template.InsertTemplates("projects-filters", unbilledCosts.Where(c => c.Project != null).Select(c => c.Project).Distinct(), "generate-invoice.filter", PageTemplateExtensions.InsertFilterVariablesSelection);


            IEnumerable<ProjectCost> costsNoInvoice = filteredCosts.OrderBy(c => c.TimeRange?.Start ?? c.Created).ThenBy(c=>c.Created);

            this.Template.InsertSelectionList(
                variableName: "no-invoices-costs",
                entities: costsNoInvoice.AsQueryable(),
                selectedEntities: costsNoInvoice,
                form: new FormBuilder(Forms.Admin.LISTING),
                selectionAPICall: null
            );

            // Vars
            this.Template.InsertVariables("project", project);

            // Navigation
            AddProjectNavigation(this.Navigation, project);
            Navigation.Add("create-invoice", "{text.generate-invoice}");
        }

        [RequestHandler("/admin/projects/project/costs/{path}")]
        public void Costs(string path) {
            // Init
            var rootProject = this.DB.Projects()
                .Include(nameof(Project.Users))
                .Include(nameof(Project.Users)+"."+nameof(ProjectCost.User))
                .OnlyStandardProjects()
                .GetRootByPath(path);

            // Rights
            this.CheckAdminRightsOrProjectMembership(rootProject, this.User, throwException: true);

            var project = this.DB.Projects()
                .Include(nameof(Project.Costs))
                .GetByPath(path);

            // Common filtered data
            var link = "/admin/projects/project/cost/{entity.project-path}/{entity.public-id}";
          
            this.InsertFilteredProjectCosts(this.Template, project, true, null, null, link); 

            // Navigation
            AddProjectNavigation(this.Navigation, project);
            this.Navigation.Add("costs/" + project.PublicId, "{text.project-costs}");
        }

        [RequestHandler("/admin/projects/project/report/{path}", AccessPolicy.PUBLIC_ARN)]
        public void Report(string path) {
            // Init
            var project = this.DB.Projects()
               .Include(nameof(Project.Costs))
               .OnlyStandardProjects()
               .GetByPath(path);

            var reportAPICall = Core.Config.PublicURL + "/api/admin/projects/project/"+ project.PublicId+ "/report?" + this.Context.Request.QueryString;

            if (this.User == null || this.User.HasMatchingARN("/admin/projects") == false) {
                var url = this.Context.Request.Url.ToString();
                Core.Util.URL.ValidateURLWithAccessCodeForURL(url, true);
               
                reportAPICall = Core.Util.URL.RemoveQueryParameter(reportAPICall, "access-code");
                reportAPICall = Core.Util.URL.GetURLWithAccessCodeForURL(reportAPICall);

            }


            this.Template.InsertVariable("report-api-call", reportAPICall);

            // Common filtered data
            var link = "/admin/projects/project/cost/{entity.project-path}/{entity.public-id}";

            // Costs
            this.InsertFilteredProjectCosts(this.Template, project, true, null, null, link);

            // User
            this.Template.InsertVariableBool("has-user", this.User != null);

            // Shareable link
            var shareLink = project.GetReportURL(this.Params.String("timerange"), this.Params.String("billed"), this.Params.String("users"));
            this.Template.InsertVariable("share-link", shareLink);

            // Navigation
            AddProjectNavigation(this.Navigation, project);
            this.Navigation.Add("report/" + project.PublicId, "{text.project-report} " + this.Params.String("timerange", ""));
        }


       

        [RequestHandler("/admin/projects/project/edit-costs/{path}")]
        public void EditCosts(string path) {
            // Init
            var project = this.DB.Projects()
                 .Include(nameof(Project.Costs) +"." + nameof(ProjectCost.WorkType))
                 .Include(nameof(Project.Costs) + "." + nameof(ProjectCost.User))
                 .Include(nameof(Project.Costs) + "." + nameof(ProjectCost.Project))
                 .OnlyStandardProjects()
                 .GetByPath(path);

            // Rights
            this.CheckAdminRightsOrProjectMembership(project, this.User, throwException: true);

            // Worktypes
            var worktypes = project.GetWorkTypes();
            var users = project.GetProjectUsers(includeUser: true).Select(u=>u.User);
            this.Template.InsertJSON("worktypes", worktypes.ToDictionary(w =>w.PublicId, z =>z.Name), false);
            this.Template.InsertJSON("users", users.ToDictionary(w => w.PublicId, z => z.Name), false);
            // this.Template.InsertJSON("worktype-names", worktypes.Select(z => z.Name), false);



            // Form
            var form = new FormBuilder(Forms.Admin.LISTEDIT);
            form.Include(nameof(ProjectCost.CostPerUnit));
            form.Include(nameof(ProjectCost.WorkType));
            form.Include(nameof(ProjectCost.Billable));
            form.Include(nameof(ProjectCost.Billed));
            form.Include(nameof(ProjectCost.Costs));
            form.Include(nameof(ProjectCost.User));
            form.Include(nameof(ProjectCost.ProjectName));
            form.Include(nameof(ProjectCost.CostsInternal));


            var costs = project.GetAllCosts(hideDeleted: true);

            // Costs
            this.Template.InsertSelectionList(
              variableName: "entities",
              entities: costs.Where(c=>c.Billed == null).AsQueryable(),
              form: form,
              selectedEntities:new List<ModelObject>()
              );


            // Wrong Assigned Worktypes
            var costsWrongWorktypes = costs.Where(c => worktypes.Contains(c.WorkType) == false);
            this.Template.InsertSelectionList(
              variableName: "wrong-worktypes-entities",
              entities: costsWrongWorktypes.Where(c => c.Billed == null).AsQueryable(),
              form: form,
              selectedEntities: new List<ModelObject>()
              );
            this.Template.InsertVariable("wrong-worktypes-entities.count", costsWrongWorktypes.Count());


            // Navigation
            AddProjectNavigation(this.Navigation, project);
            this.Navigation.Add("edit-costs/" + project.PublicId, "{text.edit-project-costs}");
        }

        [RequestHandler("/admin/projects/project/hours/{path}")]
        public void Hours(string path) {
            // Init
            var rootProject = this.DB.Projects()
                .Include(nameof(Project.Users))
                .Include(nameof(Project.Users)+"."+nameof(ProjectCost.User))
                .OnlyStandardProjects()
                .GetRootByPath(path);

            // Rights
            this.CheckAdminRightsOrProjectMembership(rootProject, this.User, throwException: true);

            var project = this.DB.Projects()
                .Include(nameof(Project.Costs))
                .GetByPath(path);

            // Common filtered data
            var link = "/admin/projects/project/cost/{entity.project-path}/{entity.public-id}";
            this.InsertFilteredProjectCosts(this.Template, project, true, null, null, link);


            // Navigation
            AddProjectNavigation(this.Navigation, project);
            this.Navigation.Add("hours/" + project.PublicId, "{text.project-hours}");
        }

        #endregion


        #region Invoice ///////////////////////////////////////////////////////////////////////////
        [RequestHandler("/admin/projects/project/invoice/{path}/{invoiceId}")]
        public void Invoice(string path, string invoiceId) {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            var project = this.DB.Projects()
                .OnlyStandardProjects()
                .GetByPath(path);
            var entity = project.GetAllInvoicesFast().AsQueryable().GetByPublicId(invoiceId);

            var allCosts = ProjectCost.GetCostsForInvoice(entity);

            var entites = this.Template.Paginate(allCosts, this);

            // Costs
            this.Template.InsertEntityList(
                entities: entites,
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "{entity.url}"
                );


            // Properties
            this.Template.InsertPropertyList(
                entity: entity,
                variableName: "entity",
                form: new FormBuilder(Forms.Admin.VIEW)
                );

            // Vars
            this.Template.InsertVariables("entity", entity);
                

            // Navigation
            AddProjectNavigation(this.Navigation, project);
            this.Navigation.Add("invoice", "{text.invoice}: " + entity.SerialId);
        }

        [RequestHandler("/admin/projects/project/invoices/{path}")]
        public void ProjectInvoices(string path) {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);
            this.RequireWriteARN();
            var project = this.DB.Projects().OnlyStandardProjects().GetByPath(path);
            var invoices = Template.Paginate(project.GetAllInvoicesFast().AsQueryable(), this, nameof(Finance.Model.Invoice.DueDate));

            // List
            this.Template.InsertEntityList(
                entities: invoices.AsQueryable(),
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/projects/project/invoice/" + project.Path + "/" + "{entity.public-id}"
                );

            // Navigation
            AddProjectNavigation(this.Navigation, project);
            Navigation.Add("invoices");
        }

        #endregion


    }
}
