
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Projects.Model;
using Machinata.Module.Admin.View;
using Machinata.Module.Projects.View;
using Machinata.Module.Projects.Logic;

namespace Machinata.Module.Projects.Handler {

 
    public class AbencesAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("projects");
        }

        #endregion

        [RequestHandler("/admin/projects/absences")]
        public void Default() {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            var entities = this.DB.Absences()
            .Include(nameof(Absence.User))
            .AsQueryable();
                    
            // All absences
            var paged = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList(
                entities: paged,
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/projects/absences/absence/{entity.public-id}"
                );
                
            // Users
            var users = entities.Where(e => e.User != null).Select(e => e.User).Distinct().OrderBy(u=>u.Name);

            this.Template.InsertEntityList(
                entities: users,
                variableName: "users",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/projects/absences/user/{entity.public-id}"
                );

            this.Navigation.Add("projects");
            this.Navigation.Add("absences");
        }


        [RequestHandler("/admin/projects/absences/create")]
        public void Create() {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            var entity = new Absence();
            entity.User = this.User;

            // Form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.CREATE),
                apiCall: "/api/admin/projects/absences/create",
                onSuccess: "/admin/projects/absences/absence/{absence.public-id}"
                );

            // Navigation
            this.Navigation.Add("projects");
            this.Navigation.Add("absences");
            Navigation.Add("create");
        }

        [RequestHandler("/admin/projects/absences/absence/{publicId}")]
        public void View(string publicId) {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            // Entity
            var entity = this.DB.Absences().Include(nameof(Absence.User)).GetByPublicId(publicId);

            // Props
            this.Template.InsertPropertyList(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.VIEW)
                );

            // Vars
            this.Template.InsertVariables("entity", entity);

            // Nav
            this.Navigation.Add("projects");
            this.Navigation.Add("absences");
            this.Navigation.Add("absence/" + entity.PublicId, entity.FullTitle);

        }
                
        [RequestHandler("/admin/projects/absences/absence/{publicId}/edit")]
        public void Edit(string publicId) {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            // Entity

            var entity = this.DB.Absences()
            .GetByPublicId(publicId);

            // Form
            this.Template.InsertForm(
                entity: entity,
                variableName: "form",
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/projects/absence/{entity.PublicId}/edit",
                onSuccess: $"/admin/projects/absences/absence/{entity.PublicId}"
                );

            Navigation.Add("projects");
            Navigation.Add("absences");
            Navigation.Add("absence/" + entity.PublicId, entity.FullTitle);
            Navigation.Add("edit");
        }

        [RequestHandler("/admin/projects/absences/user/{publicId}")]
        public void UserAbsences(string publicId) {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            var user = this.DB.Users().GetByPublicId(publicId);
            var entities = this.DB.Absences()
            .Include(nameof(Absence.User)).Where(a => a.User.Id == user.Id);


            // All absences
            var paged = this.Template.Paginate(entities, this);
            this.Template.InsertEntityList(
                entities: paged,
                variableName: "entities",
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/projects/absences/absence/{entity.public-id}",
                total: new FormBuilder(Forms.Admin.TOTAL)
                );

            this.Navigation.Add("projects");
            this.Navigation.Add("absences");
            this.Navigation.Add("user/" + publicId ,user.Username );
        }



    }


}
