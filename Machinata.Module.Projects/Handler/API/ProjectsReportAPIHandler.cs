using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using System.Linq;
using System;
using Machinata.Module.Projects.Model;
using System.Collections.Generic;
using Machinata.Module.Reporting.Model;
using Newtonsoft.Json.Linq;
using Machinata.Module.Projects.Logic;
using Machinata.Module.Reporting.Logic;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;

namespace Machinata.Module.Projects.Handler {

    public class ProjectsReportAPIHandler : Module.Admin.Handler.AdminAPIHandler {

        public string CurrencySuffix {
           get {
                return ", in " + Core.Config.CurrencyDefault;
            }
        }

        [RequestHandler("/api/admin/projects/project/{publicId}/report", AccessPolicy.PUBLIC_ARN)]
        public void ProjectReport(string publicId) {

            var currency = Core.Config.CurrencyDefault;

            var accessValidated = false;

            // Check access code
            if (this.User == null || this.User.HasMatchingARN("/admin/projects") == false) {
                var url = this.Context.Request.Url.ToString();
                accessValidated = Core.Util.URL.ValidateURLWithAccessCodeForURL(url, true);
            }

            // Params
            var timerange = this.Params.String("timerange", null);
            var billedFilter = this.Params.BoolNullable("billed", null);
            var userFilter = this.Params.StringArray("users", new string[] { }, ',', StringSplitOptions.RemoveEmptyEntries);

            string filterType = string.Empty;
            DateRange dateRange = new DateRange();

            IEnumerable<User> filteredUsers = null;


            // Project
            var project = this.DB.Projects()
               .Include(nameof(Project.Costs))
               .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
               .GetByPublicId(publicId);

            // Rights
            if (accessValidated == false) {
                this.CheckAdminRightsOrProjectMembership(project, this.User, throwException: true);
            }

            // All costs
            IQueryable<ProjectCost> costs = project.GetAllCostsOfProject(this.DB, hideDeleted: true).Where(c=>c.Billable == true);

            // Billed
            costs = costs.FilterBilled(billedFilter);

            // Users
            costs = costs.FilterUsers(userFilter, ref filteredUsers);

            // Date
            IQueryable<ProjectCost> costsForTimerange = costs.FilterDateRange(timerange, ref filterType, ref dateRange);
            IQueryable<ProjectCost> totalCostsForTimerange = costsForTimerange;
            if(!string.IsNullOrEmpty(timerange) && dateRange != null) {
                totalCostsForTimerange = costs.FilterDateRange(new DateTime(0), dateRange.End.Value);
            }


            // Report
            var report = new Report();

            // Note: No costs?
            {
                if (costsForTimerange.Any() == false) {
                    var infoNode = new InfoNode();
                    infoNode.Message = "For this timerange and filters there are no incurred costs.";
                    report.Body.AddChild(infoNode);
                }
            }

            // Bugets
            {
                var chapter = report.NewChapter("Budget");
                chapter.TitleLevel = 3;

                // Subprojects Detail Table
                {
                    var node = GetSubprojectsDetailTable(project);
                    chapter.AddChild(node);
                }


                // Budgets
                {
                    var layoutNode = new LayoutNode();
                    layoutNode.Style = "CS_W_Two_Columns";
                    layoutNode.AddChildToSlot(GetBudgetPie(project), "{left}");
                    layoutNode.AddChildToSlot(GetBudgetsBars(project), "{right}");
                    chapter.AddChild(layoutNode);
                }
            }

            // Costs for period
            if(!string.IsNullOrEmpty(timerange)) {
                var chapter = report.NewChapter("Costs for "+timerange);
                chapter.TitleLevel = 3;

                var layoutNode = new LayoutNode();
                layoutNode.Style = "CS_W_Two_Columns";
                chapter.AddChild(layoutNode);

                // Worktypes pie
                {
                    var node = GetWorktypesPie(costsForTimerange);
                    node.RemoveZeroValueFacts();
                    layoutNode.AddChildToSlot(node, "{left}");
                }

                // Project pie
                {
                    var node = GetSubprojectsPie(project, costsForTimerange);
                    node.RemoveZeroValueFacts();
                    layoutNode.AddChildToSlot(node, "{right}");
                }
                layoutNode.AutoBalanceChildrenLegends();

            }

            // Total costs
            {

                var chapter = report.NewChapter("Total Costs");
                chapter.TitleLevel = 3;

                // Weekly costs
                {
                    var toggleNode = new ToggleNode();
                    toggleNode.AddChild(CostsOverTimeReports.GetWeeklyCosts(totalCostsForTimerange, "Costs", c => c.TimeRange?.Start, c => c.Costs?.Value, unit: currency, accumulate: true));
                    toggleNode.AddChild(CostsOverTimeReports.GetWeeklyCosts(totalCostsForTimerange, "Costs", c => c.TimeRange?.Start, c => c.Costs?.Value, unit: currency, accumulate: false));

                    chapter.AddChild(toggleNode);
                }

                // Monthly costs
                {
                    var toggleNode = new ToggleNode();
                    toggleNode.AddChild(CostsOverTimeReports.GetMonthlyCosts(totalCostsForTimerange, "Costs", c => c.TimeRange?.Start, c => c.Costs.Value, unit: currency, accumulate: true));
                    toggleNode.AddChild(CostsOverTimeReports.GetMonthlyCosts(totalCostsForTimerange, "Costs", c => c.TimeRange?.Start, c => c.Costs.Value, unit: currency, accumulate: false));
                    chapter.AddChild(toggleNode);
                }

                // Worktypes/subprojects
                {
                    var layoutNode = new LayoutNode();
                    layoutNode.Style = "CS_W_Two_Columns";
                    chapter.AddChild(layoutNode);

                    // Worktypes pie
                    {
                        var node = GetWorktypesPie(totalCostsForTimerange);
                        node.RemoveZeroValueFacts();
                        layoutNode.AddChildToSlot(node, "{left}");
                    }

                    // Project pie
                    {
                        var node = GetSubprojectsPie(project, totalCostsForTimerange);
                        node.RemoveZeroValueFacts();
                        layoutNode.AddChildToSlot(node, "{right}");
                    }
                    layoutNode.AutoBalanceChildrenLegends();
                }
            }

            
           


            this.SendAPIMessage("node-data", JObject.FromObject(report));
        }

       

        private Node GetSubprojectsDetailTable(Project project) {
         
            var table = new VerticalTableNode();
            table.SetTitle("Overview");
            table.SetSubTitle("by subprojects" + CurrencySuffix);
           

            var columns = new string[] { "Budget Planned", "Budget Actual", "Budget Used"};
            var colGroup = table.AddColumns(
                null,
                "g1",
                1,
                columns,
                new ValueTypes[] { ValueTypes.Amount, ValueTypes.Amount, ValueTypes.Amount });

            var group = table.AddRowGroup(project.Name, project.PublicId, 2);
            group.Expanded = true;

            AddDirectCostsFacts(project, group,null);

            // Child projects
            foreach (var p in project.Children.Where(c=>c.Deleted == false)) {
                var row = group.AddRow(p.Name);
                row.Id = row.Name.Resolved.ToDashedLower();
                row.Expanded = true;

                AddDirectCostsFacts(p, null,row);

                AddBudgetFacts(p, row);
                AddChildRowsRecursevely(row, p);
            }

            // Overall budget   
            var budget = project.CalculateBudget(ProjectBudget.BudgetTypes.External);

            // Header facts
            {
                {
                    var fact = new TableFact();
                    fact.Val = budget.TotalBudget?.Value;
                    if (budget.TotalBudget?.HasValue == true) {
                        fact.Resolved = Number.FormatWithThousandsNoDecimal(budget.TotalBudget.Value.Value);
                    }
                    group.AddHeaderFact(columns[0], colGroup, fact, false);
                }
                {
                    var fact = new TableFact();
                    fact.Val = budget.TotalCosts?.Value;
                    if (budget.TotalCosts?.HasValue == true) {
                        //fact.Resolved = fact.Val?.ToString();

                        fact.Resolved = Number.FormatWithThousandsNoDecimal(budget.TotalCosts.Value.Value);
                    }
                    
                    group.AddHeaderFact(columns[1], colGroup, fact, false);
                }
                {
                    var fact = new TableFact();
                  
                    if (budget.BudgetUsed.HasValue == true) {
                        //fact.Resolved = fact.Val?.ToString();
                        fact.Val = budget.BudgetUsed.Value;
                        fact.Resolved = budget.PercentUsed;
                    }

                    group.AddHeaderFact(columns[2], colGroup, fact, false);
                }
            }
           
            return table;


        }

        private static void AddBudgetFacts(Project p, DimensionElement row) {
            var budget = p.CalculateBudget(ProjectBudget.BudgetTypes.External);

            string alertHint = null;
            string alertIcon = null;
            string alertColor = "green";
            if (budget != null && budget.IsOverBudget) {
                alertHint = "over budget";
                alertIcon = "alert";
                alertColor = "red";
            }

            var fact1 = row.AddRowValue(budget.TotalBudget?.Value, budget.TotalBudget.HasValue ? Number.FormatWithThousandsNoDecimal(budget.TotalBudget.Value.Value): string.Empty);
            var fact2 = row.AddRowTagFact(budget.TotalCosts.HasValue ? Number.FormatWithThousandsNoDecimal(budget.TotalCosts.Value.Value) : string.Empty, null, alertIcon, alertColor, budget.TotalCosts?.Value);
            var fact3 = row.AddRowValue(budget.PercentUsed);

            fact1.Id = p.Id + nameof(fact1);
            fact2.Id = p.Id + nameof(fact2);
            fact3.Id = p.Id + nameof(fact3);
        }


        private static void AddChildRowsRecursevely(DimensionElement row, Project p) {

           

            foreach (var child in p.Children.Where(c => c.Deleted == false)) {
                var childRows = row.AddChildRow(child.PublicId, child.Name, "Label");
                childRows.Expanded = true;
                AddDirectCostsFacts(child, null, childRows);
                AddBudgetFacts(child, childRows);
                AddChildRowsRecursevely(childRows, child);
            }

        
        }



        private static void AddDirectCostsFacts(Project p, DimensionGroup group, DimensionElement parentRow) {

            return;

            // DOES this really make sense?

            if (p.Children.Any() == false) {
                return;
            }

            var costs = p.Costs.Where(c => c.Deleted == false && c.Billable == true && c.Costs!= null && c.Costs.Value != null);
            if (costs.Any() == false) { return; }
            var sum = costs.Sum(c => c.Costs);

            if (sum.Value == 0) {
                return;
            }

            DimensionElement row = null;

            if (group != null) {
                row = group.AddRow("(" + p.Name + ")");
            }
            else if (parentRow!= null) {
                row = parentRow.AddChildRow("childrowcosts" + p.Id, "(" + p.Name + ")");
            } else {
                throw new Exception("No row or group passed ");
            }

            row.Id = p.PublicId + nameof(row);
            row.Expanded = true;
            var costsFormatted = Number.FormatWithThousandsNoDecimal(sum.Value.Value);
            var fact1 = row.AddRowValue("");
            var fact2 = row.AddRowValue(costsFormatted);
            fact2.Id = p.Id + nameof(fact2);
        }

       

        private DonutNode GetWorktypesPie(IQueryable<ProjectCost> filteredCosts) {
            var chart = new DonutNode();
            chart.Theme = "default";
            chart.SortFacts = "descending";
            chart.SetTitle("Worktypes");
            chart.SetSubTitle("by costs" + CurrencySuffix);
            chart.LegendLabelsShowValue = true;
            IQueryable<ProjectCost> costs = filteredCosts.Where(c => c.WorkType != null);
            var grouped = costs.GroupBy(c => c.WorkType.Name);
            foreach (var group in grouped) {
                var costsGroup = group.Sum(g => g.Costs.Value.Value);
                chart.AddSliceFormatWithThousandsNoDecimal(costsGroup, group.Key);
            }
            if (chart.Series.Any()) {
                chart.Status = new ChartStatus(Number.FormatWithThousandsNoDecimal(chart.Series.First().Facts.Sum(f => f.Value).Value).ToString(), "Total");
            }
            return chart;
        }

        private DonutNode GetSubprojectsPie(Project project, IQueryable<ProjectCost> costs) {
            var chart = new DonutNode();
            chart.Theme = "default";
            chart.SortFacts = "descending";
            chart.SetTitle("Projects");
            chart.SetSubTitle("by costs"  + CurrencySuffix);
            chart.LegendLabelsShowValue = true;

            // OLD: Direct children and self
            //var projects = new List<Project>();
            //projects.AddRange(project.Children);
            //projects.Add(project);

            // New: All children and self
            var projects = project.GetAllSubProjects(true, true);
           

            foreach (var p in projects) {

                var groupCosts = costs.Where(c => c.Project == p);
                var costsGroup = groupCosts.Sum(g => g.Costs.Value.Value);
                chart.AddSliceFormatWithThousandsNoDecimal(costsGroup, p.Name);
            }
            if (chart.Series.Any()) {
                chart.Status = new ChartStatus(Number.FormatWithThousandsNoDecimal(chart.Series.First().Facts.Sum(f => f.Value).Value).ToString(), "Total");
            }
            return chart;
        }

        

       

        private Node GetBudgetsBars(Project project) {
            var subprojects = project.GetAllSubProjects(true, true);
            var bar = new VBarNode();
            bar.SetTitle("Sub-Project Budgets");
            bar.SetSubTitle("costs vs budget" + CurrencySuffix);
            var serieCosts = bar.AddSerie("Costs");
            //serieCosts.XAxis.Format
            serieCosts.YAxis = new Axis();
            serieCosts.YAxis.Format = Machinata.Module.Reporting.Config.NumberThousandsFormatD3;
            var serieBudget = bar.AddSerie("Budget");
            foreach (var subproject in subprojects) {
               
                var budget = subproject.CalculateBudget(ProjectBudget.BudgetTypes.External);
                if (budget != null && budget.TotalBudget?.Value > 0) {

                    serieCosts.AddFactFormatWithThousandsNoDecimal(budget.TotalCosts.Value.Value, subproject.Name);
                    if (budget.RemainingBudget?.HasValue == true) {
                        // serieBudget.AddFactFormatWithThousandsNoDecimal(budget.RemainingBudget.Value.Value, subproject.Name);
                        serieBudget.AddFactFormatWithThousandsNoDecimal(budget.TotalBudget.Value.Value, subproject.Name);
                    }
                }

                

            }
            return bar;

        }


        /// <summary>
        /// Budget vs remaining
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        private Node GetBudgetPie(Project project, ProjectBudget.BudgetTypes type = ProjectBudget.BudgetTypes.External) {
            var node = new GaugeNode();
            node.Theme = "default";
            node.SetTitle("Budget");

            node.SetSubTitle("costs vs budget, in %");
            var budget = project.CalculateBudget(type);

          
            // We have a budget
            if (budget?.BudgetUsed.HasValue == true) {
                node.AddSegment("Budget", true, null, 1);
                node.Facts.Add(new CategoryFact() { Tooltip = "Costs", Value = budget.BudgetUsed.Value, Label = "Costs" });
                node.SetStatus(budget.BudgetUsed.Value.ToString("#0.##%"), budget.BudgetUsed.Value);
            }


            // We have no budget
            if (project.Budget?.Value == null) {
                node.AddSegment("Budget", true, null, 1);
                node.SetStatus("No budget", 0);
            }
            return node;
        }

        [RequestHandler("/api/admin/projects/report/project/budget/{publicId}")]
        public void DonutProjectsProjectBudget(string publicId) {
            var entity = this.DB.Projects()
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .GetByPublicId(publicId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(entity, this.User, throwException: true);

            var type = this.Params.Enum<ProjectBudget.BudgetTypes>("type", ProjectBudget.BudgetTypes.External);


            var node = GetBudgetPie(entity, type);
            node.RandomID();
            node.Style = "default";
            SendAPIMessage("chart-data", JObject.FromObject(node));
        }
    }

    

}
