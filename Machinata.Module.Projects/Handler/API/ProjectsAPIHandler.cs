using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Charts;
using Machinata.Core.Builder;
using System.Linq;
using System;
using Machinata.Module.Projects.Model;
using Machinata.Core.Exceptions;
using Machinata.Module.Finance.Model;
using Machinata.Module.Projects.View;
using System.Collections.Generic;
using Machinata.Module.Admin.View;
using Machinata.Core.Messaging;
using Machinata.Module.Projects.Logic;
using Machinata.Module.Finance.Payment;

namespace Machinata.Module.Projects.Handler {
    
        

    public class ProjectsAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Project> {

        #region Handler Policies


        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return new List<AccessPolicy>() {
                new AccessPolicy() { Name = $"Team", ARN = AccessPolicy.GetDefaultRootAccessARN("team") } // indicates a user is part of the team
                //AccessPolicy.GetDefaultAdminPolicies("projects/timetracking").FirstOrDefault() // not needed, since /projects/* covers most cases
            };
        }

        #endregion

        [RequestHandler("/api/admin/projects/create")]
        public void Create() {

            // Rights: Only real admins
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);


            var project = new Project();
            project.ProjectType = Project.ProjectTypes.Project;
            project.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            project.ChangeName(this.DB, project.Name, false);
            project.Validate();

            // Color from Business
            project.SetColorFromBusiness();

            if (project.IsRootProject && project.Color == null) {
                project.Color = Core.Charts.ChartColors.D3_CATEGORY_10.Random();
            }

            // Default users
            project.Users = new List<ProjectUser>();
            foreach (var user in this.DB.Users().Active().ToList()) {
                // Does this user have the team ARN?
                var hasMatchingARN = user.HasMatchingARN("/team", false);
                // Register user
                if(hasMatchingARN) project.AddNewProjectUser(user);
            }

            // Default worktypes
            var worktypes = this.DB.ProjectWorkTypes().Where(wt => wt.Group == Config.ProjectsDefaultWorkTypeGroup);
            foreach (var worktype in worktypes) {
                project.WorkTypes.Add(worktype);
            }

            this.DB.Projects().Add(project);
            this.DB.SaveChanges();
            this.SendAPIMessage("create-success", new { Project = new { PublicId = project.PublicId, Path = project.Path } });

        }

      

        [RequestHandler("/api/admin/projects/create-sub/{parentId}")]
        public void CreateSub(string parentId) {
          
             // Parent
            var parent = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Include(nameof(Project.Children))
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .GetByPublicId(parentId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(parent.GetRootProject(), this.User, throwException: true);

            // Name
            var name = this.Params.String("name");

            // Project
            var project = new Project();
            project.Name = name ;
            project.Parent = parent;
            project.ParentId = parent.Id;
            project.Business = parent.Business;
            project.ProjectType = parent.ProjectType;
            project.ChangeName(this.DB, project.Name, false);
            project.SetColorFromBusiness();

            if (parent.TimeRange.HasValue()) {
                project.TimeRange = parent.TimeRange.Clone();
            }

            // Save
            this.DB.Projects().Add(project);
            this.DB.SaveChanges();

            // Send
            this.SendAPIMessage("create-success", new { Project = new { PublicId = project.PublicId, Path = project.Path } });
        }

       

        [RequestHandler("/api/admin/projects/project/{publicId}/delete")]
        public void Delete(string publicId) {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            var project = this.DB.Projects()            
                .GetByPublicId(publicId);

       
            SetDeletedRecursively(project, true);

            // Save
            this.DB.SaveChanges();

            this.SendAPIMessage("delete-success");

        }

        public void SetDeletedRecursively(Project project, bool deleted) {
            project.SetDeleted(deleted);

            project.Include(nameof(project.Children));
            foreach (var child in project.Children) {
                SetDeletedRecursively(child, deleted);
            }

        }

        [RequestHandler("/api/admin/projects/project/{publicId}/restore")]
        public void Restore(string publicId) {
            var project = this.DB.Projects()
               .Include(nameof(Project.Parent))
               .Include(nameof(Project.Business))
                .GetByPublicId(publicId);


            project.SetDeleted(false);

            // Check name still available
            if (project.GetSiblings(this.DB).ToList().Any(c => c.Name == project.Name)) {
                throw new BackendException("name-taken", "Name is already taken by a sibling: " + project.Name);
            }

            // Check parent is not deleted
            if (project.Parent != null) {
                if (project.Parent.Deleted == true) {
                    throw new BackendException("parent-deleted", "The original parent of the sub project has been deleted, please restore the parent project first: " + project.Parent.Name);
                }
            }


            // Save
            this.DB.SaveChanges();

            this.SendAPIMessage("restore-success");

        }




        [RequestHandler("/api/admin/projects/project/{publicId}/rename")]
        public void Rename(string publicId) {
            var project = this.DB.Projects()
                .GetByPublicId(publicId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(project.GetRootProject(), this.User, throwException: true);

            project.ChangeName(this.DB, this.Params.String("name"));
            this.DB.SaveChanges();

            this.SendAPIMessage("rename-success", new { Project = new { Path = project.Path, url = project.URL } });
        }

        [RequestHandler("/api/admin/projects/project/{publicId}/edit")]
        public void Edit(string publicId) {

            var entity = this.DB.Set<Project>()
                .Include(nameof(Project.Description))
                .Include(nameof(Project.Business))
                .GetByPublicId(publicId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(entity.GetRootProject(), this.User, throwException: true);

            // Subprojects in case we have to update them
            var subProjects = entity.GetAllSubProjects(includeSelf: false, hideDeleted: true, nameof(Project.Business)).ToList();

            // Update
            var oldBuisinessId = entity.Business?.Id;
            entity.Populate(this, ProjectsAdminHandler.GetProjectEditForm(entity));
            entity.Validate();
            var newBusinessId = entity.Business?.Id;

            // Did we change business?
            if(oldBuisinessId != newBusinessId) {
                entity.ChangeName(this.DB, entity.Name); // updates names of subprojects, but not the business relations
            
                // update business for subprojects
                foreach (var subProject in subProjects) {
                    subProject.Business = entity.Business;
                }
            }

            
          

            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("edit-success", new {
                PublicId = entity.PublicId,
                Path = entity.Path,
                URL = entity.URL
            });

        }

     
        [RequestHandler("/api/admin/projects/project/{publicId}/toggle-user/{userId}")]
        public void ToggleUser(string publicId,string userId) {



            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            var project = this.DB.Projects()
               .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
               .GetByPublicId(publicId);


           
            var user = this.DB.Users().GetByPublicId(userId);
            var enable = this.Params.Bool("value", false);

            var projectUser = project.Users.SingleOrDefault(u => u.User.Id == user.Id);

            if (projectUser != null && !enable ) {
                this.DB.ProjectUsers().Remove(projectUser);
            }
            else if (enable) {
                project.AddNewProjectUser(user);
            }

            this.DB.SaveChanges();
            this.SendAPIMessage("toggle-success");
        }

       

        [RequestHandler("/api/admin/projects/project/{publicId}/toggle-worktype/{worktypeId}")]
        public void ToggleWorkType(string publicId, string worktypeId) {

            // Rights: Only real admins
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);


            var project = this.DB.Projects()
               .Include(nameof(Project.WorkTypes))
               .GetByPublicId(publicId);

            var worktype = this.DB.ProjectWorkTypes().GetByPublicId(worktypeId);
            var enable = this.Params.Bool("value", false);

            if (!enable) {
               project.WorkTypes.Remove(worktype);
            } else  {
           
                project.WorkTypes.Add(worktype);
            }

            this.DB.SaveChanges();
            this.SendAPIMessage("toggle-success");
        }

        #region Task ///////////////////////////////////////////////////////////////////////

        [RequestHandler("/api/admin/projects/project/{publicId}/create-task")]
        public void CreateTask(string publicId) {
            var project = this.DB.Set<Project>()
                .Include(nameof(Project.Tasks))
                .Include(nameof(Project.Users))
                .GetByPublicId(publicId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(project.GetRootProject(), this.User, throwException: true);


            var task = new ProjectTask();
            task.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            task.Validate();
            project.Tasks.Add(task);

            // Users
            var users = project.GetProjectUsers();
            var user = users.AsQueryable().GetByPublicId(this.Params.String("user"));

            task.User = user;
        
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("create-success", new { Task = new { PublicId = task.PublicId} , Project = new { Path = project.Path} });
        }


        [RequestHandler("/api/admin/projects/task/{publicId}/edit")]
        public void TaskEdit(string publicId) {


            var entity = this.DB.Set<ProjectTask>()
            .Include(nameof(ProjectTask.Project))
            .GetByPublicId(publicId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(entity.Project.GetRootProject(), this.User, throwException: true);



            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();
            this.DB.SaveChanges();
            // Return
            SendAPIMessage("edit-success", new { Task = new { PublicId = entity.PublicId }, Project = new { Path = entity.Project.Path } });

        }

      

        [RequestHandler("/api/admin/projects/task/{publicId}/delete")]
        public void TaskDelete(string publicId) {
            var entity = this.DB.Set<ProjectTask>()
                .Include(nameof(ProjectTask.Project))
                .GetByPublicId(publicId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(entity.Project.GetRootProject(), this.User, throwException: true);


            // Flag
            entity.SetDeleted(true);

            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("delete-success");

        }

        [RequestHandler("/api/admin/projects/task/{publicId}/restore")]
        public void TaskRestore(string publicId) {
            var entity = this.DB.Set<ProjectTask>()
                .Include(nameof(ProjectTask.Project))
                .GetByPublicId(publicId);


          


            // Check project is not deleted
            if (entity.Project == null || entity.Project.Deleted == true) {
                throw new BackendException("project-deleted", "The original project of the project task has been deleted, please restore the project first: " + entity.Project.Name);
            }

            // Rights
            this.CheckAdminRightsOrProjectMembership(entity.Project.GetRootProject(), this.User, throwException: true);

            // Flag
            entity.SetDeleted(false);

            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("delete-success");

        }

        #endregion


        #region Event ///////////////////////////////////////////////////////////////////////

        [RequestHandler("/api/admin/projects/project/{publicId}/create-event")]
        public void CreateEvent(string publicId) {
            var project = this.DB.Set<Project>()
                .Include(nameof(Project.Events))
                .GetByPublicId(publicId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(project.GetRootProject(), this.User, throwException: true);


            var projectEvent = new ProjectEvent();
            projectEvent.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            projectEvent.Validate();
            project.Events.Add(projectEvent);

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("create-success", new { Event = new { PublicId = projectEvent.PublicId }, Project = new { Path = project.Path } });
        }


        [RequestHandler("/api/admin/projects/event/{publicId}/edit")]
        public void EventEdit(string publicId) {
            var entity = this.DB.Set<ProjectEvent>()
                .Include(nameof(ProjectEvent.Project))
                .GetByPublicId(publicId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(entity.Project.GetRootProject(), this.User, throwException: true);

            entity.Populate(this, new FormBuilder(Forms.Admin.EDIT));
            entity.Validate();
            this.DB.SaveChanges();
            // Return
            this.SendAPIMessage("edit-success", new { Event = new { PublicId = entity.PublicId }, Project = new { Path = entity.Project.Path } });

        }

       

        [RequestHandler("/api/admin/projects/event/{publicId}/delete")]
        public void EventDelete(string publicId) {

            // Entity
            var entity = this.DB.Set<ProjectEvent>()
                .Include(nameof(ProjectEvent.Project))
                .GetByPublicId(publicId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(entity.Project.GetRootProject(), this.User, throwException: true);

            // Flag
            entity.SetDeleted(true);

            // Save
            this.DB.SaveChanges();
         
            // Return
            SendAPIMessage("delete-success");

        }

        [RequestHandler("/api/admin/projects/event/{publicId}/restore")]
        public void EventRestore(string publicId) {

            var entity = this.DB.Set<ProjectEvent>()
               .Include(nameof(ProjectEvent.Project))
               .GetByPublicId(publicId);

            // Check project is not deleted
            if (entity.Project == null || entity.Project.Deleted == true) {
                throw new BackendException("project-deleted", "The original project of the project task has been deleted, please restore the project first: " + entity.Project.Name);
            }

            // Rights
            this.CheckAdminRightsOrProjectMembership(entity.Project.GetRootProject(), this.User, throwException: true);

            // Flag
            entity.SetDeleted(false);

            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("delete-success");

        }

        #endregion


        #region Costs ///////////////////////////////////////////////////////////////////////

      

        [RequestHandler("/api/admin/projects/project/{publicId}/add-timesheet-cost")]
        public void AddTimesheetCost(string publicId) {
            // Get the project we want to save to (by default its the endpoint url param publicId, but it can also be provided)
            var projectId = this.Params.String("project", publicId);
            var project = this.DB.Set<Project>()
                .Include(nameof(Project.Costs))
                .Include(nameof(Project.Business))
                .GetByPublicId(projectId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(project.GetRootProject(), this.User, throwException: true);


            // Allow based on subprojects?
            project.CheckAllowBookingCostsOnProject(throwException: true);


            var projectCost = new ProjectCost();
            projectCost.Business = project.Business;
            ProjectsForms.PopulateTimesheet(projectCost, this);
            project.Costs.Add(projectCost);

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("create-success", new { Cost = new { PublicId = projectCost.PublicId }, Project = new { Path = project.Path } });
        }

        

        [RequestHandler("/api/admin/projects/project/{publicId}/add-project-cost")]
        public void AddProjectCost(string publicId) {
            // Get the project we want to save to (by default its the endpoint url param publicId, but it can also be provided)
            var projectId = this.Params.String("project", publicId);
            var project = this.DB.Set<Project>()
                .Include(nameof(Project.Costs))
                .Include(nameof(Project.Business))
                .GetByPublicId(projectId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(project.GetRootProject(), this.User, throwException: true);

            // Allow based on subprojects?
            project.CheckAllowBookingCostsOnProject(throwException: true);


            var projectCost = new ProjectCost();
            projectCost.Business = project.Business;
            ProjectsForms.PopulateProjectCost(projectCost, this);
            project.Costs.Add(projectCost);

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("create-success", new { Cost = new { PublicId = projectCost.PublicId }, Project = new { Path = project.Path } });
        }

        [RequestHandler("/api/admin/projects/cost/{publicId}/edit-project-cost")]
        public void EditProjectCost(string publicId) {
            var entity = this.DB.Set<ProjectCost>()
               .Include(nameof(ProjectCost.Project))
               .Include(nameof(ProjectCost.User))
               .Include(nameof(ProjectCost.WorkType))
           .GetByPublicId(publicId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(entity.Project.GetRootProject(), this.User, throwException: true);


            ProjectsForms.PopulateProjectCost(entity, this);
          
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("edit-success", new { Cost = new { PublicId = entity.PublicId }, Project = new { Path = entity.Project.Path } });
        }


        [RequestHandler("/api/admin/projects/{publicId}/edit-project-costs")]
        public void EditProjectCosts(string publicId) {
            var project = this.DB.Projects()
               .Include(nameof(Project.Costs))
               .GetByPublicId(publicId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(project.GetRootProject(), this.User, throwException: true);



            var newEntities =  this.PopulateListEdit(project.Costs.Where(c=> c.Deleted = false).AsQueryable(), createNewCost, null , enableEntityDeletion : true);



            // Adding new entities to project
            foreach(var newEntity in newEntities) {
                newEntity.Project = project;
                this.DB.ProjectCosts().Add(newEntity);
            }

            // Save
            this.DB.SaveChanges();

            // Return
            this.SendAPIMessage("edit-success", new { Project = new { Path = project.Path } });
        }

        private ProjectCost createNewCost(string id) {
           return  new ProjectCost();
        }


       

        [RequestHandler("/api/admin/projects/cost/{publicId}/edit-timesheet-cost")]
        public void EditTimesheetCost(string publicId) {
            var entity = this.DB.Set<ProjectCost>()
                .Include(nameof(ProjectCost.Project))
                .Include(nameof(ProjectCost.User))
                .Include(nameof(ProjectCost.WorkType))
            .GetByPublicId(publicId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(entity.Project.GetRootProject(), this.User, throwException: true);


            ProjectsForms.PopulateTimesheet(entity, this);

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("edit-success", new { Cost = new { PublicId = entity.PublicId }, Project = new { Path = entity.Project.Path } });
        }


        [RequestHandler("/api/admin/projects/costs/change-user")]
        public void CostsChangeUser() {

            var publicIds = this.Params.StringArrayFromQueryString("costs", new string[] { }, ',');
            var userId = this.Params.String("user");

            if (publicIds == null || publicIds.Any() == false) {
                throw new BackendException("change-user-error", "No costs selected");
            }

            var entities = this.DB.Set<ProjectCost>()
                 .Include(nameof(ProjectCost.Project))
                 .Include(nameof(ProjectCost.User))
                 .Include(nameof(ProjectCost.Business))
                 .GetByPublicIds(publicIds);
            var rootProjects = entities.ToList().Select(e => e.Project.GetRootProject());

            if (rootProjects.DistinctBy(p => p.PublicId).Count() > 1) {
                throw new BackendException("change-user-error", "Only changing user of one project is supported");
            }

            var destination = rootProjects.First();


            // Rights
            this.CheckAdminRightsOrProjectMembership(destination.GetRootProject(), this.User, throwException: true);

            var users = destination.GetProjectUsers(includeUser: true).Select(pu=>pu.User);
            var user = users.AsQueryable().GetByPublicId(userId);

            foreach (var entity in entities) {
                entity.User = user;
            }


            this.DB.SaveChanges();

            // Return
            this.SendAPIMessage("move-success", new { Costs = entities.ToList().Select(e => new { PublicId = e.PublicId, Path = e.URL }), Project = new { Path = destination.Path } });
        }

        [RequestHandler("/api/admin/projects/costs/change-billed")]
        public void CostsChangeBilled() {

            var publicIds = this.Params.StringArrayFromQueryString("costs", new string[] { }, ',');
            var billed = this.Params.DateTimeGlobalConfigFormat("billed", DateTime.MinValue);
            var billedUTC = Core.Util.Time.ConvertToUTCTimezone(billed);

            if (publicIds == null || publicIds.Any() == false) {
                throw new BackendException("change-billed-error", "No costs selected");
            }

            if (billed == DateTime.MinValue) {
                throw new BackendException("change-billed-error", "No valid date selected");
            }

            var entities = this.DB.Set<ProjectCost>()
                 .Include(nameof(ProjectCost.Project))
                 .Include(nameof(ProjectCost.User))
                 .Include(nameof(ProjectCost.Business))
                 .GetByPublicIds(publicIds).ToList();
         
            foreach (var entity in entities) {
                // Rights
                this.CheckAdminRightsOrProjectMembership(entity.Project.GetRootProject(), this.User, throwException: true);
                // Billed time
                entity.Billed = billedUTC;
            }

            this.DB.SaveChanges();

            // Return
            this.SendAPIMessage("change-billed-success", new { Costs = entities.Select(e => new { PublicId = e.PublicId, Path = e.URL }) });
        }

        [RequestHandler("/api/admin/projects/costs/change-worktype")]
        public void CostsChangeWorktype() {

            var publicIds = this.Params.StringArrayFromQueryString("costs", new string[] { }, ',');
            var worktypeId = this.Params.String("worktype");
            var billable = this.Params.BoolNullable("billable", null);

            if (publicIds == null || publicIds.Any() == false) {
                throw new BackendException("change-worktype-error", "No costs selected");
            }
             
            var entities = this.DB.Set<ProjectCost>()
                 .Include(nameof(ProjectCost.Project))
                 .Include(nameof(ProjectCost.WorkType))
                 .Include(nameof(ProjectCost.Business))
                 .GetByPublicIds(publicIds);

            var rootProjects = entities.ToList().Select(e => e.Project.GetRootProject());

            if (rootProjects.DistinctBy(p => p.PublicId).Count() >1) {
                throw new BackendException("change-worktype-error", "Only changing worktypes of the same root project is supported");
            }

            var destination = rootProjects.First();

            // Rights
            this.CheckAdminRightsOrProjectMembership(destination.GetRootProject(), this.User, throwException: true);

            var workTypes = destination.GetWorkTypes();
            var workType = workTypes.AsQueryable().GetByPublicId(worktypeId);

            foreach (var entity in entities) {
                ChangeWorkType(billable, workType, entity);
            }


            this.DB.SaveChanges();

            // Return
            SendAPIMessage("move-success", new { Costs = entities.ToList().Select(e => new { PublicId = e.PublicId, Path = e.URL }), Project = new { Path = destination.Path } });
        }

        private static void ChangeWorkType(bool? billable, ProjectWorkType workType, ProjectCost entity) {
            if (entity.ProjectCostType != ProjectCost.ProjectCostTypes.Timesheet) {
                throw new BackendException("change-worktype-error", "Only changing worktype of timesheets is supported.");
            }

            // Checke billed
            if (entity.Billed != null) {
                throw new BackendException("change-worktype-error", ($"Cost {entity.URL} cannot be moved because it's billed already."));
            }

            entity.WorkType = workType;
            entity.CostPerUnit = workType.CostPerUnit.Clone();
            entity.CostPerUnitInternal = workType.CostPerUnitInternal.Clone();

            if (!entity.CostPerUnitInternal.HasValue) {
                entity.CostPerUnitInternal = entity.CostPerUnit.Clone();
            }
            if (billable.HasValue == true) {
                entity.Billable = billable.Value;
            }
        }

        [RequestHandler("/api/admin/projects/costs/move")]
        public void CostsMove() {

            var publicIds = this.Params.StringArrayFromQueryString("costs", new string[] { }, ',');
            var forceWorktypes = this.Params.Bool("force", false);

            if (publicIds == null || publicIds.Any() == false) {
                throw new BackendException("move-error", "No costs selected");
            }

            var entities = this.DB.Set<ProjectCost>()
                 .Include(nameof(ProjectCost.Project))
                 .Include(nameof(ProjectCost.WorkType))
                 .Include(nameof(ProjectCost.Business))
                 .GetByPublicIds(publicIds);

            var destination = this.DB.Projects()
                 .Include(nameof(Project.Business))
                 .GetByPublicId(this.Params.String("project"));


            // Rights
            if (this.CheckIsInAdminGroup(this.User, throwExceptions: false) == false) {
                // Destination
                this.CheckAdminRightsOrProjectMembership(destination.GetRootProject(), this.User, throwException: true);
                // Source
                entities = entities.ToList().Where(e => e.Project != null && this.CheckAdminRightsOrProjectMembership(e.Project.GetRootProject(), this.User, false) == true).AsQueryable();
            }
          

            MoveCosts(forceWorktypes, entities, destination);

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("move-success", new { Costs = entities.ToList().Select(e => new { PublicId = e.PublicId, Path = e.URL }), Project = new { Path = destination.Path } });
        }


        private static void MoveCosts(bool forceWorktypes, IQueryable<ProjectCost> entities, Project destination) {
            var workTypes = destination.GetWorkTypes();
            var workTypesOrigin = entities.ToList().Where(e=>e.WorkType != null).Select(e => e.WorkType).DistinctBy(e => e.PublicId);

            foreach (var entity in entities) {

                // Checke billed
                if (entity.Billed != null) {
                    throw new BackendException("billed-error", ($"Cost {entity.URL} cannot be moved because it's billed already."));
                }

                // Check worktype
                if (entity.WorkType != null && forceWorktypes == false && workTypes.Contains(entity.WorkType) == false) {

                    //var message = $"Cost '{entity.URL}' cannot be moved because worktype '{entity.WorkType}' does not match the destination projects worktypes: '{string.Join(", ", workTypes.Select(w => w.Name))}";

                    var destWorktypes = string.Join(", ", workTypes.Select(w => w.Name));
                    var origWorktypes = string.Join(", ", workTypesOrigin.Select(w => w.Name));

                    var message = Core.Localization.Text.GetTranslatedTextByIdForLanguage("move-costs-worktype-error");
                    message = string.Format(message, origWorktypes, destWorktypes);
                    throw new BackendException("worktype-error", message);
                }

                entity.Project = destination;
                entity.Business = destination.Business;
            }
        }


        [RequestHandler("/api/admin/projects/cost/{publicId}/delete")]
        public void CostDelete(string publicId) {
            var entity = this.DB.Set<ProjectCost>()

                .Include(nameof(ProjectCost.Project))
                .GetByPublicId(publicId);


            // Rights
            this.CheckAdminRightsOrProjectMembership(entity.Project.GetRootProject(), this.User, throwException: true);

            entity.SetDeleted(true);

            this.DB.SaveChanges();
            // Return
            SendAPIMessage("delete-success");

        }


        [RequestHandler("/api/admin/projects/costs/delete")]
        public void CostsDelete() {

            var publicIds = this.Params.StringArrayFromQueryString("costs", new string[] { }, ',');
          

            if (publicIds == null || publicIds.Any() == false) {
                throw new BackendException("delete-error", "No costs selected");
            }

            var entities = this.DB.Set<ProjectCost>()
                 .Include(nameof(ProjectCost.Project))
                 .Include(nameof(ProjectCost.WorkType))
                 .Include(nameof(ProjectCost.Business))
                 .GetByPublicIds(publicIds);

       


            // Rights
            if (this.CheckIsInAdminGroup(this.User, throwExceptions: false) == false) {
                // Source
                entities = entities.ToList().Where(e => e.Project != null && this.CheckAdminRightsOrProjectMembership(e.Project.GetRootProject(), this.User, false) == true).AsQueryable();
            }

            // Set deleted
            foreach(var entity in entities.ToList()) {
                entity.SetDeleted();
            }
           

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("move-success");
        }

        [RequestHandler("/api/admin/projects/cost/{publicId}/restore")]
        public void CostRestore(string publicId) {
            var entity = this.DB.Set<ProjectCost>()
                .Include(nameof(ProjectCost.Project))
                .GetByPublicId(publicId);



            // Check project is not deleted
            if (entity.Project == null || entity.Project.Deleted == true) {
                throw new BackendException("project-deleted", "The original project of the project cost has been deleted, please restore the project first: " + entity.Project.Name);
            }

            // Rights
            this.CheckAdminRightsOrProjectMembership(entity.Project.GetRootProject(), this.User, throwException: true);


            // Flag
            entity.SetDeleted(false);


            // Save
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("delete-success");

        }

        #endregion


        #region Invoice
        [RequestHandler("/api/admin/projects/project/{publicId}/generate-invoices")]
        public void BusinessesGenerateInvoices(string publicId) {
            this.RequireWriteARN();
            // Rights: Only real admins
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);


            var entity = this.DB.Projects()
                .Include(nameof(Project.Parent))
                .Include(nameof(Project.Invoices))
                .GetByPublicId(publicId);
            var title = this.Params.String("invoice-name", entity.Name);
            var business = entity.GetProjectBusiness();
            var address = business.GetBillingAddressForBusiness();

            var unbilledCosts = entity.GetUnbilledProjectCosts(hideDeleted: true);
            var costIds = Params.StringValuesAllowEmtpy("selected", null);
            if (costIds == null) throw new BackendException("nothing-selected", "Nothing was selected.");
            if (string.IsNullOrWhiteSpace(title)== true) throw new BackendException("empty-title", "The title for the invoice cannot be empty.");
            var costs = DB.ProjectCosts().GetByPublicIds(costIds);

            var sender = this.DB.OwnerBusiness();

            if (sender == null) {
                throw new BackendException("owner-business", "No IsOwner business has been defined yet.");
            }

            // check all costs are unbilled
            if (!costs.Select(c=>c.Id).ToList().All(oo => unbilledCosts.Select(uc=>uc.Id).ToList().Contains(oo))) {
                throw new BackendException("deleted-costs", "Not all orders are ready for invoice creation.");
            }

            // Create all the invoices and save
            var invoice = CreateInvoice(entity, sender, address, costs, Finance.Payment.PaymentMethod.Invoice, this.User, Core.Config.CurrencyDefault, title, business);

            // Save reference to Invoice
            entity.Invoices.Add(invoice);

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("invoice-generated-success", new { Invoice = new { PublicId = invoice.PublicId} });
        }

        /// <summary>
        /// Generates a manual empty invoice with a reference to the project
        /// </summary>
        /// <param name="publicId"></param>
        [RequestHandler("/api/admin/projects/project/{publicId}/generate-manual-invoice")]
        public void GenerateManualInvoices(string publicId) {

            // Rights: Only real admins
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            this.RequireWriteARN();

            var entity = this.DB.Projects()
                .Include(nameof(Project.Parent))
                .Include(nameof(Project.Invoices))
                .GetByPublicId(publicId);
            var business = entity.GetProjectBusiness();
            var address = business.GetBillingAddressForBusiness();

         
            var sender = this.DB.OwnerBusiness();

            if (sender == null) {
                throw new BackendException("owner-business", "No IsOwner business has been defined yet.");
            }

            var costs = new List<ProjectCost>();
            var title = entity.Name;
         
            // Create all the invoices and save
            var invoice = InitializeInvoice(entity, sender, address, Finance.Payment.PaymentMethod.Invoice, this.User, Core.Config.CurrencyDefault, business, title);

            //this.DB.Invoices().Add(invoice);

            entity.Invoices.Add(invoice);

            this.DB.SaveChanges();

            // Return
            this.SendAPIMessage("invoice-generated-success", new { Invoice = new { PublicId = invoice.PublicId } });
        }

        public static Invoice CreateInvoice(
         
            Project parentProject,
            Business sender,
            Address billingAddress,
            IQueryable<ProjectCost> costs,
            Finance.Payment.PaymentMethod method,
            User user,
            string currency,
            string name,
            Business business = null
            
            ) {
            try {
                // Create the invoice and setup details
                Invoice invoice = InitializeInvoice(parentProject, sender, billingAddress, method, user, currency, business, name);

                var grouped = costs.GroupBy(c => c.Project).Select(c => new { Key = c.Key, Value = c.AsEnumerable() });

                // Add each project as a line item group
                foreach (var projectGroup in grouped.ToList()) {
                    var project = projectGroup.Key;
                    var lineItemsGroups = project.GenerateLineItemGroups(projectGroup.Value, invoice);
                    foreach (var lineItemGroup in lineItemsGroups) {
                        invoice.LineItemGroups.Add(lineItemGroup);
                    }
                }

                // External Costs
                {
                    var externalCosts = costs.ToList().Where(c => c.ProjectCostType == ProjectCost.ProjectCostTypes.External && c.Costs != null && c.Costs.HasValue == true && c.LineItem != null && c.LineItem.CustomerTotal != null && c.LineItem.CustomerTotal.HasValue);
                    if (externalCosts != null && externalCosts.Any() == true) {
                        invoice.ExternalCosts = externalCosts.Sum(ec => ec.LineItem.CustomerSubtotal);

                    }

                }

                // Update totals
                invoice.UpdateInvoice();

                return invoice;

            } catch (Exception e) {

                _logger.Error(e.StackTrace);
              
                throw new BackendException("invoice-error", $"Could not create invoice for {parentProject.Name}",e);
            }
        }

        private static Invoice InitializeInvoice(Project parentProject, Business sender, Address billingAddress, PaymentMethod method, User user, string currency, Business business, string title) {
            var invoice = new Invoice();
            invoice.User = user;
            invoice.Business = business;
            invoice.BillingAddress = billingAddress;
            invoice.BillingName = billingAddress?.Name;
            invoice.BillingEmails = billingAddress?.Email;
            invoice.Sender = sender;
            invoice.SenderAddress = sender.GetBillingAddressForBusiness();
            invoice.Status = Invoice.InvoiceStatus.Created;
            invoice.PaymentMethod = method;

            // Prices /Currencies
            invoice.SubtotalCustomer = new Price(0, currency);
            invoice.TotalCustomer = new Price(0, currency);
            invoice.VATCustomer = new Price(0, currency);
            invoice.Currency = currency;

            invoice.LoadBusinessSettings(business);

            // Title
            //invoice.Title = parentProject.Name;
            invoice.Title = title;
            return invoice;
        }

        [RequestHandler("/api/admin/projects/invoice/{publicId}/rollback")]
        public void InvoiceRollback(string publicId) {

            // Rights: Only real admins
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            this.RequireWriteARN();

            var entity = this.DB.Invoices()
                .Include(nameof(Invoice.LineItemGroups) + "." + (nameof(LineItemGroup.LineItems)))
                .GetByPublicId(publicId);

            var costs = ProjectCost.GetCostsForInvoice(entity);

            // Costs Vars
            foreach (var cost in costs) {

                cost.Invoice = null;
                cost.LineItem = null;
                cost.Billed = null;
            }

            // Line Item Groups
            foreach ( var group in entity.LineItemGroups.ToList()) {

                // Line Items
                foreach (var item in group.LineItems.ToList()) {
                    this.DB.DeleteEntity(item);
                }

                this.DB.DeleteEntity(group);
            }

            // Invoice
            this.DB.DeleteEntity(entity);

       

            this.DB.SaveChanges();

            // Return
            this.SendAPIMessage("invoice-rollback-success", new { Invoice = new { PublicId = entity.PublicId } });
        }

        #endregion


        [RequestHandler("/api/admin/projects/business/{publicId}/set-color")]
        public void SetBusinessColor(string publicId) {

            // Rights: Only real admins
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);


            var business = this.DB.ActiveBusinesses().GetByPublicId(publicId);
            var color = this.Params.String("color", null);

            //if (string.IsNullOrWhiteSpace(color)) { 
            //    throw new BackendException("no-color", "No color value.");
            //}

            business.Settings[Config.BUSINESS_COLOR_PROPERTY_KEY] = color;

            this.DB.SaveChanges();

            this.SendAPIMessage("success");

        }


        

        [RequestHandler("/api/admin/projects/list")]
        public void List() {

            var allProjects = this.DB.Projects()
               .Include(nameof(Project.Business))
               .Where(p => !p.Archived)
               .Where(p => !p.Deleted)
               .Where(p => !p.Completed)
               .OnlyStandardProjects()
               .OrderBy(p => p.Business.Name)
               .ThenBy(p => p.Name).ToList();


            // Rights
            if (this.CheckIsInAdminGroup(this.User, throwExceptions: false) == false) {
                allProjects = allProjects.Where(e => this.CheckAdminRightsOrProjectMembership(e.GetRootProject(), this.User, false) == true).ToList();
            }

            var form = new FormBuilder(Forms.EMPTY)
                .Include(nameof(Project.PublicId))
                .Include(nameof(Project.ShortURLPath))
                .Include(nameof(Project.BusinessFormalName))
                .Include(nameof(Project.Name));
              

            var result = allProjects.Select(c => c.GetJSON(form));

            // manually here otherwise enums will be integers
            var rawJson = Core.JSON.Serialize(new { Projects = result }, true, true, true);

            // Send raw to 
            this.SendRawAPIMessage("success", rawJson, false);


        }


        [RequestHandler("/api/admin/projects/{publicId}/mark-as-completed-archived")]
        public void MarkAsCompletedAndArchived(string publicId) {

            var project = this.DB.Projects()
                .GetByPublicId(publicId);


            // Rights
            this.CheckAdminRightsOrProjectMembership(project.GetRootProject(), this.User, throwException: true);


            project.Archived = true;
            project.Completed = true;
      
            // Save
            this.DB.SaveChanges();

            this.SendAPIMessage("delete-success");

        }
    }
}
