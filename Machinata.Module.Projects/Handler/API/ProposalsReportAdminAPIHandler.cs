using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Charts;
using Machinata.Core.Builder;
using System.Linq;
using System;
using Machinata.Module.Projects.Model;
using System.Collections.Generic;
using Machinata.Module.Projects.Logic;
using Newtonsoft.Json.Linq;
using Machinata.Module.Reporting.Model;
using Machinata.Module.Projects.Dashboard;

namespace Machinata.Module.Projects.Handler {

    public class ProposalsReportAdminAPIHandler : Module.Admin.Handler.AdminAPIHandler {

        public override void SetupForUser(User user) {
            base.SetupForUser(user);
            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);
        }

        [RequestHandler("/api/admin/project/proposals/overview")]
        public void RevenueForBusinessOverTime() {

            var report = new Report(); 
           
            var sectionNode = ProjectsDashboard.CreateProposalSection(this.DB, Core.Config.CurrencyDefault);
            report.AddToBody(sectionNode); //needs too much space
            this.SendAPIMessage("node-data", JObject.FromObject(report));
        }


    }
}

        

