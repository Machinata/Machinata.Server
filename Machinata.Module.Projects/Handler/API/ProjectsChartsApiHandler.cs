using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Charts;
using Machinata.Core.Builder;
using System.Linq;
using System;
using Machinata.Module.Projects.Model;
using System.Collections.Generic;
using Machinata.Module.Projects.Logic;

namespace Machinata.Module.Projects.Handler {

    public class ProjectsChartsAPIHandler : Module.Admin.Handler.AdminAPIHandler {

        #region Project Timelines 

        private TimelineChartDataTrack _createProjectTrack(TimelineChartData chart, Project project, Project rootProject, bool loadSubprojects) {
            // Create track
            var track = new TimelineChartDataTrack();
            if (project.IsRootProject) {
                track.Title = project.Business.Name + " " + project.Name;
            } else {
                track.Title = new string('↳', project.Level) + " " + project.Name;//TODO
            }
            track.Name = track.Title;
            track.Group = project.ShortURLPath;
            track.Start = project.TimeRange.Start.Value;
            track.End = project.TimeRange.End.Value;
            track.Link = project.URL;
            track.Id = project.PublicId;
            track.Level = project.Level;
            // Color
            if (rootProject != null) {
                track.Color = rootProject.Color;
            } else {
                track.Color = project.Color;
            }
            if (project.Level > 0 && track.Color != null) {
                var numberOfColorSteps = 4;
                track.Color = Core.Util.Colors.FadedHTMLCodeForHTMLCode(track.Color, project.Level, numberOfColorSteps);
            }
            // Add events

            {
                // OLD
                //if (project.Events == null) {
                //    project.Include(nameof(Project.Events));
                //}
                //var events = project.Events.Where(e => e.Date.HasValue);
            }

            var events = project.GetAllEvents(hideDeleted: true).Where(e => e.Date.HasValue);

            foreach (var projectEvent in events) {
                var evt = new TimelineChartDataEvent();
                evt.Title = projectEvent.Title;
                evt.Date = projectEvent.Date.Value;
                evt.Link = projectEvent.URL;
                track.Events.Add(evt);
            }
            chart.Tracks.Add(track);
            // Subprojects?
            if (loadSubprojects) {
                project.Include(nameof(Project.Children));
                var subprojects = project.Children.Where(p => p.Deleted == false && p.Archived == false && p.TimeRange.Start.HasValue && p.TimeRange.End.HasValue);
                foreach (var subproject in subprojects) {
                    var subtrack = _createProjectTrack(chart, subproject, rootProject, loadSubprojects);
                    subtrack.ParentId = project.PublicId;
                }
            }
            return track;
        }


        private TimelineChartDataTrack _createAbsenceTrack(TimelineChartData chart, Absence absence, bool mergeIntoSingleTrack) {

            // Only if we have daterange
            if (absence.Start == null || absence.End == null) {
                return null;
            }

            // Init
            var chartId = "ABS_" + absence.PublicId;
            var track = new TimelineChartDataTrack();

            // Setup a track for this absence
            track.Name = absence.FullTitle;
            if (mergeIntoSingleTrack) {
                track.Title = "Absences";
                track.Group = "absences";
            } else {
                track.Title = absence.FullTitle;
                track.Group = chartId;
            }
            track.Start = absence.TimeRange.Start.Value;
            track.End = absence.TimeRange.End.Value;
            track.Link = absence.URL;
            track.Id = chartId;

            // Add an event
            track.Events.Add(new TimelineChartDataEvent() {
                Date = track.Start,
                Title = absence.FullTitle,
                Link = absence.URL,
                Id = chartId + "_EVT"
            });

            chart.Tracks.Insert(0, track);

            return track;
        }


        private TimelineChartDataTrack _createHolidayTrack(TimelineChartData chart) {
            // Init
            var chartId = "HOL";
            var track = new TimelineChartDataTrack();
            var start = DateTime.Now;
            var end = DateTime.Now;
            if (chart.Tracks != null && chart.Tracks.Count > 0) start = chart.Tracks.Min(e => e.Start);
            if (chart.Tracks != null && chart.Tracks.Count > 0) end = chart.Tracks.Max(e => e.End);
            start = new DateTime(start.Year, 1, 1);
            end = new DateTime(end.Year, 12, 31);
            track.Start = start;
            track.End = end;
            track.Color = "transparent";
            track.Title = "Holidays";
            track.Group = "holidays";
            track.Id = chartId;

            var holidays = Core.Util.Time.GetPublicHolidays(start, end);
            foreach (var holiday in holidays) {
                // Setup a track for this absence
                track.Name = holiday.LocalName;
                // Add an event
                track.Events.Add(new TimelineChartDataEvent() {
                    Date = holiday.Date,
                    Title = holiday.LocalName,
                    Id = chartId + "_TIT"
                });

            }

            chart.Tracks.Insert(0, track);
            return track;
        }


        private TimelineChartDataTrack _createEventsTrack(TimelineChartData chart) {
            // Init
            var chartId = "EVT";
            var track = new TimelineChartDataTrack();
            var start = chart.Tracks.Min(e => e.Start);
            var end = chart.Tracks.Max(e => e.End);
            start = new DateTime(start.Year, 1, 1);
            end = new DateTime(end.Year, 12, 31);
            track.Name = "Events";
            track.Title = "Events";
            track.Group = "events";
            track.Id = chartId;
            track.Start = start;
            track.End = end;
            track.Color = "transparent";
            foreach (var existingTrack in chart.Tracks) {
                foreach (var existingEvent in existingTrack.Events) {
                    track.Events.Add(existingEvent);
                }
            }
            chart.Tracks.Insert(0, track);
            return track;
        }

        [RequestHandler("/api/admin/projects/chart/timeline/projects")]
        public void TimelineChartProjects() {

            // Init
            var data = new TimelineChartData();
            bool loadEvents = this.Params.Bool("events", false);
            bool loadAbsences = this.Params.Bool("absences", false);
            bool loadSubprojects = this.Params.Bool("subprojects", false);
            bool loadHolidays = this.Params.Bool("holidays", false);
            bool ignoreTimeline = this.Params.Bool("ignoreTimeline", false); // for subprojects which will not be shown
            var projectType = this.Params.Enum<Nullable<Project.ProjectTypes>>("type", null);

            // Projects
            var projects = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Include(nameof(Project.Events))
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .Where(p=>p.ProjectType == Project.ProjectTypes.Project)
                .Where(p=>p.Deleted == false);

            
            


            // Project Type filter?
            if (projectType.HasValue == true) {
                projects = projects.Where(p => p.ProjectType == projectType);
            }

            // Apply filters
            projects = projects.Where(p => p.Archived == false && p.TimeRange.Start.HasValue && p.TimeRange.End.HasValue);
            if (ignoreTimeline == false) {
                projects = projects.Where(p => p.Timeline == true);
            }

            if (this.Params.String("business-id") != null) {
                var business = this.DB.Businesses().GetByPublicId(this.Params.String("business-id"));
                projects = projects.Where(p => p.Business.Id == business.Id);
            }

            // Subprojects
            if (!string.IsNullOrEmpty(this.Params.String("public-id"))) {
                // Get specific project
                var id = Core.Ids.Obfuscator.Default.UnobfuscateId(this.Params.String("public-id"));
                projects = projects.Where(p => p.Id == id);
            } else {
                // Get all root projects
                projects = projects.Where(p => p.ParentId == null);
            }

            // Order
            //projects = projects.OrderBy(p => p.Business.Name).ThenBy(p => p.Name);
            projects = projects.OrderByDescending(p => p.Sticky).ThenBy(p => p.Business.Name).ThenBy(p => p.Name);

            // Execute query and compile
            var projectsList = projects.ToList();

            // Check Project Rights
            if (this.CheckIsInAdminGroup(this.User, throwExceptions: false) == false){
                projectsList = projectsList.Where(p => this.CheckAdminRightsOrProjectMembership(p, this.User, throwException: false)).ToList();
                var isInAdminGroup = this.CheckIsInAdminGroup(this.User, throwExceptions: false);
                loadAbsences = loadAbsences && isInAdminGroup;
                loadEvents = loadEvents && isInAdminGroup;
                loadHolidays = loadHolidays && isInAdminGroup;

            }



            foreach (var project in projectsList) {
                _createProjectTrack(data, project, project, loadSubprojects);
            }

            // Insert events
            if (loadEvents) {
                _createEventsTrack(data);
            }

            // Insert holidays
            if (loadHolidays) {
                _createHolidayTrack(data);
            }

            // Absences
            if (loadAbsences) {
                // Get specific absence
                var absences = this.DB.Absences().Include(nameof(Absence.User));
                foreach (var absence in absences) {
                    _createAbsenceTrack(data, absence, true);
                }
            }

            // Auto-prune data
            data.Prune();
            data.ConvertToDefaultTimezone();

            this.SendAPIMessage("chart-data", data);
        }

        [RequestHandler("/api/admin/projects/chart/timeline/absences")]
        public void Absences() {

            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);

            // Init
            var data = new TimelineChartData();

            var minDate = DateTime.UtcNow.AddDays(-365);

            // Absences
            var absences = this.DB.Absences()
                .Include(nameof(Absence.User))
                .Where(e => e.TimeRange.End > minDate)
                .OrderBy(e => e.TimeRange.Start);
            foreach (var absence in absences) {
                _createAbsenceTrack(data, absence, false);
            }

            // Insert holidays
            bool loadHolidays = this.Params.Bool("holidays", false);
            if (loadHolidays) {
                _createHolidayTrack(data);
            }

            // Auto-prune data
            data.Prune();
            data.ConvertToDefaultTimezone();

            this.SendAPIMessage("chart-data", data);
        }

        #endregion

        #region Project Budgets

        [RequestHandler("/api/admin/projects/chart/donut/project/budget/{publicId}")]
        public void DonutProjectsProjectBudget(string publicId) {
            var entity = this.DB.Projects()
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .GetByPublicId(publicId);
            var type = this.Params.Enum<ProjectBudget.BudgetTypes>("type", ProjectBudget.BudgetTypes.External);
            var budget = entity.CalculateBudget(type);

            // Rights
            this.CheckAdminRightsOrProjectMembership(entity, this.User, throwException: true);

            var data = new DonutChartData();

            if (entity.Budget?.HasValue == true) {
                data.Title = entity.Budget.ToString();
            } else {
                data.Title = "No Budget";
            }
           

            data.Items.Add(new DonutChartDataItem() { Name = "Costs", Title = "Costs", Value = (int)budget.TotalCosts.Value });
            if (budget.RemainingBudget?.HasValue == true) {
                data.Items.Add(new DonutChartDataItem() { Name = "Remaining", Title = "Remaining", Value = (int)budget.RemainingBudget.Value });
            }


      
            data.UpdatePercentages();
            SendAPIMessage("chart-data", data);
        }

        #endregion

        #region Project Costs
             

        private IQueryable<ProjectCost> ApplyFilters(IQueryable<ProjectCost> costs) {
            // Apply filters
            {
                // Init
                var billedFilter = this.Params.BoolNullable("billed", null);
                var userFilter = this.Params.StringArray("users", new string[] { }, ',', StringSplitOptions.RemoveEmptyEntries);
                var workTypeFilter = this.Params.StringArray("worktypes", new string[] { }, ',', StringSplitOptions.RemoveEmptyEntries);
                var costTypeFilter = this.Params.String("costtype", null);
                var timerangeFilter = this.Params.String("timerange", null);
                IEnumerable<User> filteredUsers = new List<User>();
                IEnumerable<ProjectWorkType> filteredWorkTypes = new List<ProjectWorkType>();

                // Billed?
                if (billedFilter == true) {
                    costs = costs.Where(c => c.Billed != null);
                } else if (billedFilter == false) {
                    costs = costs.Where(c => c.Billed == null);
                }

                // Users
                if (userFilter.Any()) {
                    costs = costs.Where(c => c.User != null && userFilter.Contains(c.User.PublicId));
                    filteredUsers = costs.Select(w => w.User);
                }

                // Worktypes
                if (workTypeFilter.Any()) {
                    costs = costs.Where(c => c.WorkType != null && workTypeFilter.Contains(c.WorkType.PublicId));
                    filteredWorkTypes = costs.Select(c => c.WorkType);
                }

                // Costtype
                if (!string.IsNullOrEmpty(costTypeFilter)) {
                    var filter = Core.Util.EnumHelper.ParseEnum<ProjectCost.ProjectCostTypes>(costTypeFilter);
                    costs = costs.Where(c => c.ProjectCostType == filter);
                }

                // Timerange
                string filterType = "All";
                DateRange dateRange = null;
                costs = costs.FilterDateRange(timerangeFilter, ref filterType, ref dateRange);
            }

            return costs;
        }

        #endregion


     


        [RequestHandler("/api/admin/projects/chart/donut/project/budgets/{publicId}")]
        public void DonutProjectCostsBudgets(string publicId) {
            var entity = this.DB.Projects()
                .Include(nameof(Project.Costs) + "." + nameof(ProjectCost.WorkType))
                .Include(nameof(Project.Costs) + "." + nameof(ProjectCost.User))
                .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
                .GetByPublicId(publicId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(entity, this.User, throwException: true);

            var externalBudget = this.Params.Bool("external-budget", true);
            var budgetType = externalBudget ? ProjectBudget.BudgetTypes.External : ProjectBudget.BudgetTypes.Internal;

            // var projects = entity.GetAllSubProjects(includeSelf: false).Where(p => p.Budget != null && p.Budget.HasValue == true);
            var projects = entity.GetAllSubProjects(includeSelf: false, hideDeleted: true);

            //// Take self if no children
            //if  (projects.Any () == false) {
            //    projects = new List<Project>() { entity };
            //}
            var data = new DonutChartData();
            data.Title = "Budgets";


            foreach (var project in projects) {
                var budget = project.Budget;
                var title = project.Name;
                var amount = budget?.Value != null ? (int)budget.Value.Value : 0;
                data.Items.Add(new DonutChartDataItem() { Name = title, Title = title, Value = amount });
            }

            data.UpdatePercentages();

            this.SendAPIMessage("chart-data", data);
        }

     
    }
}

        

