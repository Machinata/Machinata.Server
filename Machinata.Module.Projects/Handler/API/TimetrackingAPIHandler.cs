using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Charts;
using Machinata.Core.Builder;
using System.Linq;
using System;
using Machinata.Module.Projects.Model;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Projects.Handler {

    public class TimeTrackingAPIHandler : Module.Admin.Handler.AdminAPIHandler {

        public const string VIRTUAL_BUSINESS_RECENT_PROJECTS_ID =  "recent-project";
        

        private object _getStatusMessage(Project project, Business business, ProjectWorkType worktype, ProjectCost cost) {
            return new {
                ProjectId = project?.PublicId,
                ProjectShortURL = project?.ShortURL,
                ProjectShortURLPath = project?.ShortURLPath,
                BusinessId = business?.PublicId,
                WorkTypeId = worktype?.PublicId,
                CostStart = cost?.TimeRange.Start,
                CostId = cost?.PublicId,
                CostPath = cost?.URL,
                EditCostPath = cost?.EditURL,
                Description = cost?.Description,
                Note = cost?.Note,
                ServerTime = DateTime.UtcNow, // Used for syncronization,
                ProjectAdminLink = project?.URL,
                RootProjectAdminLink = project?.GetRootProject().URL,
                ProjectBudget = project?.CalculateBudget(ProjectBudget.BudgetTypes.External)?.GetJSON(new FormBuilder(Forms.Admin.VIEW)),
                WorkTypeAdminLink = worktype?.URL,
                UserLink = "/admin/projects/people/user/" + cost?.User?.PublicId
            };
        }

        [RequestHandler("/api/admin/projects/timetracking/status")]
        public void Status() {

            // UI needs:
            // current business id
            // current project full path
            // current cost timestart

            // last open cost
            var cost = this.DB.ProjectOpenTimesheetCosts(
                this.User, 
                nameof(ProjectCost.Project), 
                nameof(ProjectCost.Business), 
                nameof(ProjectCost.WorkType))
                .OrderByDescending(c => c.Id)
                .FirstOrDefault();

            if (cost != null) {
                this.SendAPIMessage("timetracking-status", _getStatusMessage(cost.Project, cost.Business, cost.WorkType, cost));
            } else {
                this.SendAPIMessage("timetracking-status", _getStatusMessage(null, null, null, null));
            }

            
        }

        [RequestHandler("/api/admin/projects/timetracking/start")]
        public void Start() {

            var project = this.DB.Projects()
                .Include(nameof(Project.WorkTypes))
                .Include(nameof(Project.Business))
                .GetByPublicId(this.Params.String("project-id"));
            var worktype = this.DB.ProjectWorkTypes().GetByPublicId(this.Params.String("work-type-id"));
            var description = this.Params.String("description");
            var billable = this.Params.Bool("billable", true);
            var business = project.Business;

            //TODO: shall we check that the user is part of the project? Like in /api/admin/projects/timetracking/businesses
            // and /api/admin/projects/timetracking/projects
            var cost = Logic.TimetrackingLogic.Start(this.DB, this.User, project, worktype, description, billable);
            this.DB.SaveChanges();

            this.SendAPIMessage("timetracking-status", _getStatusMessage(project, business, worktype, cost));
        }

        [RequestHandler("/api/admin/projects/timetracking/stop")]
        public void Stop() {

            var cost = Logic.TimetrackingLogic.Stop(this.DB, this.User);
            this.DB.SaveChanges();

            object status = null;

            // Only if we have an open cost
            if (cost != null) {
                var project = cost.Project;
                var business = cost.Business;
                var worktype = cost.WorkType;

                status = _getStatusMessage(project, business, worktype, cost);

            }

            this.SendAPIMessage("timetracking-status", status);
        }

        
        [RequestHandler("/api/admin/projects/timetracking/businesses")]
        public void ListBusinesses() {
            // Get projects based on query
            var businesses = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Include(nameof(Project.WorkTypes))
                .Include(nameof(Project.Users))
                .Where(p => p.Archived == false && p.ProjectType == Project.ProjectTypes.Project && p.ParentId == null)
                .Where(p => p.Users.Any(pu => pu.User.Id == this.User.Id)) // Only include those that the user is
                .Where(p=> p.ProjectType == Project.ProjectTypes.Project)
                .Select(p => p.Business)
                .Distinct()
                .OrderBy(b=>b.Name);
            // Re-compile as JSON
            var businessesJSON = new List<JObject>();
            foreach(var business in businesses) {
                businessesJSON.Add(business.GetJSON(new FormBuilder(Forms.API.LISTING)));
            }

            // Recent Projects as virtual businesses
            {
                
                var recent = new Business();
                recent.Name = "Recent";
                var recentJSON = recent.GetJSON(new FormBuilder(Forms.API.LISTING));
                recentJSON["public-id"] = VIRTUAL_BUSINESS_RECENT_PROJECTS_ID;
                businessesJSON.Add(recentJSON);
            }

            SendAPIMessage("timetracking-businesses",new { Businesses = businessesJSON} );
        }

        [RequestHandler("/api/admin/projects/timetracking/projects")]
        public void ListProjects() {
            // Get projects based on query
            var parentId = this.Params.String("parent-id");
            var projects = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Include(nameof(Project.WorkTypes))
                .Include(nameof(Project.Users))
                .Where(p => p.Archived == false && p.Completed == false && p.ProjectType == Project.ProjectTypes.Project && p.Deleted == false);
            if(!string.IsNullOrEmpty(this.Params.String("parent-id"))) {
                var id = Core.Ids.Obfuscator.Default.UnobfuscateId(this.Params.String("parent-id"));
                projects = projects.Where(p => p.ParentId == id);
            } else {
                projects = projects.Where(p => p.ParentId == null);
            }
            if (this.Params.String("business-id") == VIRTUAL_BUSINESS_RECENT_PROJECTS_ID) {

                // last timesheet costs -> take their root project
                var hasParent = string.IsNullOrEmpty(this.Params.String("parent-id")) == false;

                if (hasParent == false) {
                    var recentProjectCosts = this.DB.ProjectCosts()
                  .Include(nameof(ProjectCost.Project))
                  .Include(nameof(ProjectCost.User))
                  .Include(nameof(ProjectCost.Business))
                  .Where(pc => pc.User.Id == this.User.Id && pc.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet)
                  .OrderByDescending(c => c.Created).Take(100).ToList();

                    IEnumerable<Project> recentProjects = null;

                    recentProjects = recentProjectCosts.Where(c=> c.Project != null).Select(c => c.Project.GetRootProject());

                    var recentProjectIds = recentProjects.Select(p => p.Id).ToList().Distinct().Take(5);
                    projects = projects.Where(p => recentProjectIds.Contains(p.Id)).AsQueryable();

                    // Rename if root project
                    foreach (var project in projects) {
                        project.Name = project.Business.Name + " / " + project.Name;
                    }

                    projects = projects.OrderBy(p => p.Name);
                }


            } else if (!string.IsNullOrEmpty(this.Params.String("business-id"))) {
                var id = Core.Ids.Obfuscator.Default.UnobfuscateId(this.Params.String("business-id"));
                projects = projects.Where(p => p.Business.Id == id);
            }
            // Get work types
            // For root or for subprojects
            var worktypesJSON = new List<JObject>();
            if (string.IsNullOrEmpty(this.Params.String("parent-id")) == false) {
                var parentProject = this.DB.Projects().Include(nameof(Project.Parent)).OnlyStandardProjects().GetByPublicId(this.Params.String("parent-id"));
                if (parentProject.CheckAllowBookingCostsOnProject(throwException: false) == true) { // are we allowed to book here?
                    var rootProject = parentProject.GetRootProject();
                    rootProject.Include(nameof(Project.WorkTypes));
                    var worktypes = rootProject.WorkTypes;
                    foreach (var worktype in worktypes) {
                        var worktypeJSON = worktype.GetJSON(new FormBuilder(Forms.API.LISTING));
                        worktypesJSON.Add(worktypeJSON);
                    }
                }
            }
          
            // Re-compile as JSON
            var projectsJSON = new List<JObject>();
            foreach(var project in projects.OrderBy(p=>p.Name)) {
                // Filter out some types of projects
                if (project.IsRootProject && project.WorkTypes.Count() == 0) continue;
                // Make sure use belongs to project
                if (project.IsRootProject && !project.Users.Any(p => p.User == this.User)) continue;
                // Get JSON format (we dont want to serialize the whole thing...)
                var projectJSON = project.GetJSON(new FormBuilder(Forms.API.LISTING));
                projectsJSON.Add(projectJSON);
            }
            SendAPIMessage("timetracking-projects",new { Projects = projectsJSON, WorkTypes = worktypesJSON} );
        }

        
        [RequestHandler("/api/admin/projects/timetracking/description/update")]
        public void DescriptionUpdate() {
            // Get projects based on query
            var costId = this.Params.String("cost-id");
            var desc = this.Params.String("description");
            var cost = this.DB.ProjectCosts().GetByPublicId(costId);
            // Validate
            if (cost.User != this.User) throw new BackendException("invalid-user","Cannot change the cost description of another user.");
            if (string.IsNullOrEmpty(desc)) desc = null;
            // Update
            cost.Description = desc;
            this.DB.SaveChanges();
            SendAPIMessage("success");
        }
    }
}
