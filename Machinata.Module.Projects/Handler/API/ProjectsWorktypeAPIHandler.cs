using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Charts;
using Machinata.Core.Builder;
using System.Linq;
using System;
using Machinata.Module.Projects.Model;
using Machinata.Module.Projects.Logic;

namespace Machinata.Module.Projects.Handler {

    public class ProjectsWorktypeAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<ProjectWorkType> {

        public override void SetupForUser(User user) {
            base.SetupForUser(user);
            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);
        }


        [RequestHandler("/api/admin/projects/worktypes/create")]
        public void Create() {
           
            base.CRUDCreate();
        }

        [RequestHandler("/api/admin/projects/worktype/{publicId}/edit")]
        public void Edit(string publicId) {
         
            base.CRUDEdit(publicId);
        }

        [RequestHandler("/api/admin/projects/worktype/{publicId}/delete")]
        public void Delete(string publicId) {
          
            base.CRUDDelete(publicId);
        }



    }
}
