using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Charts;
using Machinata.Core.Builder;
using System.Linq;
using System;
using Machinata.Module.Projects.Model;
using Machinata.Core.Exceptions;
using Machinata.Module.Finance.Model;
using Machinata.Module.Projects.View;
using System.Collections.Generic;
using Machinata.Module.Admin.View;
using Machinata.Core.Messaging;
using Machinata.Module.Finance.Payment;
using Machinata.Module.Projects.Logic;

namespace Machinata.Module.Projects.Handler {
    
        

    public class ProjectsCostProposalAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Project> {

        #region Handler Policies


        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("projects");
        }


        #endregion

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion


        public override void SetupForUser(User user) {
            base.SetupForUser(user);

            // This handler ist not for non admins 
            // Rights
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);
        }

        [RequestHandler("/api/admin/projects/proposals/create")]
        public void Create() {
            var project = new Project();
            project.Populate(this, new FormBuilder(Forms.Admin.CREATE));
            project.ChangeName(this.DB, project.Name, false);
            project.Validate();

            // Color from Business
            project.SetColorFromBusiness();

            if (project.IsRootProject && project.Color == null) {
                project.Color = Core.Charts.ChartColors.D3_CATEGORY_10.Random();
            }

            project.ProjectType = Project.ProjectTypes.CostProposal;
            var impl = project.LoadImplementation() as ProjectCostProposal;

            //// Default users
            //project.Users = new List<ProjectUser>();
            //foreach (var user in this.DB.Users().Active().ToList()) {
            //    // Does this user have the team ARN?
            //    var hasMatchingARN = user.HasMatchingARN("/team", false);
            //    // Register user
            //    if(hasMatchingARN) project.AddNewProjectUser(user);
            //}

            // Address from billing Address
            if (project.Business != null) {
                var billingAddress = project.Business.GetBillingAddressForBusiness();
                if (billingAddress != null) {
                    impl.BillingAddress = Address.Duplicate(billingAddress);
                }
            }

          

            project.SaveImplementation(impl);



            // Default worktypes
            var worktypes = this.DB.ProjectWorkTypes().Where(wt => wt.Group == Config.ProjectsDefaultWorkTypeGroup);
            foreach (var worktype in worktypes) {
                project.WorkTypes.Add(worktype);
            }

            this.DB.Projects().Add(project);
            this.DB.SaveChanges();
            this.SendAPIMessage("create-success", new { Project = new { PublicId = project.PublicId, Path = project.Path } });

        }



        [RequestHandler("/api/admin/projects/proposals/proposal/create-sub/{parentId}")]
        public void CreateSub(string parentId) {

            // Parent
            var parent = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Include(nameof(Project.Children))
                .Where(p => p.ProjectType == Project.ProjectTypes.CostProposal)
                .GetByPublicId(parentId);
            var parentImpl = parent.LoadImplementation() as ProjectCostProposal;
            var childrenImpls = parent.Children.Select(p => p.LoadImplementation() as ProjectCostProposal);

            // Name
            var name = this.Params.String("name");

            // Project
            Project project = CreateSubproject(parent, name);

           


            // Get unit from parents
            var projectImpl = project.LoadImplementation() as ProjectCostProposal;
            projectImpl.Unit = parentImpl.Unit;
            projectImpl.DefaultCostsPerUnit = parentImpl.DefaultCostsPerUnit.Clone();
            // Sort
            if (childrenImpls.Any() == true) {
                projectImpl.Sort = childrenImpls.Max(ci => ci.Sort) + 1;
            }


            // Save
            project.SaveImplementation(projectImpl);



            if (parent.TimeRange.HasValue()) {
                project.TimeRange = parent.TimeRange.Clone();
            }

            this.DB.Projects().Add(project);
            this.DB.SaveChanges();

            // Send
            this.SendAPIMessage("create-success", new { Project = new { PublicId = project.PublicId } });
        }

        private Project CreateSubproject(Project parent, string name) {
            var project = new Project();
            project.Name = name;
            project.Parent = parent;
            project.ParentId = parent.Id;
            project.Business = parent.Business;
            project.ProjectType = parent.ProjectType;
            project.ChangeName(this.DB, project.Name, false);
            project.SetColorFromBusiness();
            return project;
        }

        [RequestHandler("/api/admin/projects/proposals/proposal/{publicId}/group/{groupId}/edit-line-items")]
        public void EditLineItems(string publicId, string groupId) {

            this.RequireWriteARN();
            var entity = this.DB.Projects()
             .Include(nameof(Project.Children) + "." + nameof(Project.Costs))
             .Include(nameof(Project.Business))
             .Include(nameof(Project.Costs))
             .Where(e => e.ProjectType == Project.ProjectTypes.CostProposal)
             .GetByPublicId(publicId);

            var group = entity.Children.AsQueryable().GetByPublicId(groupId);
            var listEditForm = new FormBuilder(Forms.Admin.LISTEDIT);

            try {
                var newEntities = this.PopulateListEdit(group.Costs.AsQueryable(), createNewCost, form: listEditForm, enableEntityDeletion: true);

                // Update the sort
            
                // Add to Group
                foreach (var newEntity in newEntities) {
                    newEntity.Business = entity.Business;
                    group.Costs.Add(newEntity);
                    this.DB.ProjectCosts().Add(newEntity);
                }

               

            } catch (Exception e) {
                throw new BackendException("update-items-error", "Failed to update the costs: " + e.Message);
            }


            // Update Sort
            var ids = GetPopulateListEditIds(this);
            int sort = 0;
            var publicIdLength = "ouGtXA".Length;
            var guidLength = "1aea2ee2bcf0466abac9423549c973fa".Length;
            foreach (var id in ids) {
                ProjectCost cost = null;
                if (id.Length == publicIdLength) {
                    cost = group.Costs.AsQueryable().GetByPublicId(id);
                } else if (id.Length == guidLength) {
                    // Grab from the TempId we saved before
                    cost = group.Costs.FirstOrDefault(c => c.TempID == id);
                }

                if (cost != null) {
                    cost.Sort = sort++;
                }
            }


            UpdateBudgets(this.DB, entity);

            // Save
            this.DB.SaveChanges();

            

            // Return
            this.SendAPIMessage("edit-success", new { Project = new { entity.PublicId } });
        }

        private static IEnumerable<string> GetPopulateListEditIds(Core.Handler.Handler handler) {
            return handler.Params.AllValues().Keys.Where(k => k.Contains(":")).Select(k => k.GetUntilOrEmpty(":")).Distinct();
        }

        private void UpdateBudgets(ModelContext dB, Project entity) {
            if (entity.ParentId != null) {
                throw new Exception("UpdateBudgets() is only supported with root projects");
            }
            var subProjects = entity.GetAllSubProjects(false, true, nameof(Project.Costs)).OrderByDescending(p => p.Level);

            if (subProjects.Any(p => p.ParentId != entity.Id)) {
                throw new Exception("UpdateBudgets() is only supported with one level of sub projects");
            }

            foreach (var subproject in subProjects) {
                var costs = subproject.Costs.Where(c => c.Costs != null && c.Billable == true).Sum(c => c.Costs);
                if (costs != null) {
                    subproject.Budget = costs.Clone();
                } else {
                    subproject.Budget = new Price(0);
                }

                var timeSheets = subproject.Costs.Where(c => c.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet && c.Costs != null);
                foreach(var timesheet in timeSheets) {

                }
            }

            entity.Budget = subProjects.Sum(s => s.Budget);

        }

        private ProjectCost createNewCost(string id) {
            var cost =  new ProjectCost();
            cost.TimeRange = new DateRange();
            cost.TempID = id;
            return cost;
        }

        [RequestHandler("/api/admin/projects/proposals/proposal/{publicId}/delete")]
        public void Delete(string publicId) {
            var project = this.DB.Projects()
                .Include(nameof(Project.Children))
                .Include(nameof(Project.Costs))
                .Include(nameof(Project.Description))
                .Include(nameof(Project.Tasks))
                .Include(nameof(Project.Events))
                .Include(nameof(Project.Users))
                .Include(nameof(Project.Parent))
                .GetByPublicId(publicId);

            // Deny deleting cascading project hierarchies
            var subprojects = project.GetAllSubProjects(false, true);
            //if (subprojects.Any()) {
            //    var names = subprojects.Select(p => p.Name);
            //    throw new BackendException("delete-error", "Please delete sub projects before: " + string.Join(", ", names));
            //}

            // Delete Costs Tasks etc and same for children
            DeleteProposalRecursevely(project);


            // Update Budget parent?
            if (project.Parent != null) {
                UpdateBudgets(this.DB, project.Parent);
            }


            // Project
            this.DB.Projects().Remove(project);

            // Save
            this.DB.SaveChanges();

            this.SendAPIMessage("delete-success");

        }

        private void DeleteProposalRecursevely(Project project) {

            // Costs
            project.Include(nameof(project.Costs));
            foreach (var cost in project.Costs.ToList()) {
                project.Costs.Remove(cost);
                this.DB.ProjectCosts().Remove(cost);
            }

            // Tasks
            project.Include(nameof(project.Tasks));
            foreach (var task in project.Tasks.ToList()) {
                project.Tasks.Remove(task);
                this.DB.ProjectTasks().Remove(task);
            }

            // Events
            project.Include(nameof(project.Events));
            foreach (var projEvent in project.Events.ToList()) {
                project.Events.Remove(projEvent);
                this.DB.ProjectEvents().Remove(projEvent);
            }

            // Users
            project.Include(nameof(project.Users));
            foreach (var user in project.Users.ToList()) {
                project.Users.Remove(user);
                this.DB.ProjectUsers().Remove(user);
            }

            // Recusrion
            project.Include(nameof(project.Children));
            foreach (var child in project.Children.ToList()) {
                DeleteProposalRecursevely(child);
            }
        }

        [RequestHandler("/api/admin/projects/proposals/proposal/{publicId}/rename")]
        public void Rename(string publicId) {
            var project = this.DB.Projects()
                .GetByPublicId(publicId);
            project.ChangeName(this.DB, this.Params.String("name"));
            this.DB.SaveChanges();

            this.SendAPIMessage("rename-success", new { Project = new { Path = project.Path, url = project.URL, PublicId = project.PublicId } });
        }

        [RequestHandler("/api/admin/projects/proposals/proposal/{publicId}/edit")]
        public void Edit(string publicId) {

            var entity = this.DB.Set<Project>()
                .Include(nameof(Project.Description))
                .Include(nameof(Project.Business))
                .GetByPublicId(publicId);

            var addresses = entity.Business?.ActiveAddresses?.ToList();

            var appendicesNodes = ProjectsCostProposalAdminHandler.GetAppendicesNodes(this.DB);

            var appendicesShortURLS = this.Params.StringValues("appendices_opt", new string[] { });
            var appendicesShortURLSConcat = string.Join(",", appendicesShortURLS);
            var impl = entity.LoadImplementation() as ProjectCostProposal;
            var form = ProjectsForms.GetProposalEditForm(impl, appendicesNodes);

          
            // Update
            var oldBuisinessId = impl.Business?.Id;
            impl.Populate(this, form);

            //Appendices
            if (impl.IsRootProject == true) {
                impl.AppendixNodeIds = appendicesShortURLSConcat;
            }

            // Address
            if (impl.IsRootProject == true) {
                var addressHash = this.Params.String(nameof(impl.BillingAddress));
                if (addresses != null && string.IsNullOrEmpty(addressHash) == false) {
                    var address = addresses.FirstOrDefault(a=>a.Hash== addressHash);
                    if (address!= null) {
                        // The user has selected another address from the ActiveAddresses not the copy in project.BillingAddress
                        impl.BillingAddress = Address.Duplicate(address);
                    }
                }
                else if (string.IsNullOrEmpty(addressHash) == true){
                    impl.BillingAddress = null;
                }
            }

            impl.Validate();
            var newBusinessId = impl.Business?.Id;

            // Save
            entity.SaveImplementation(impl);

            // Did we change business?
            if (oldBuisinessId != newBusinessId) {
                entity.ChangeName(this.DB, entity.Name, updateAllSubProjectsAlso: true, checkSiblingName: true, updateBusiness: true);
            }

          
            this.DB.SaveChanges();

            // Return
            SendAPIMessage("edit-success", new {
                PublicId = entity.PublicId,
                Path = entity.Path,
                URL = entity.URL
            });

        }

        [RequestHandler("/api/admin/projects/proposals/templates/list")]
        public void ListTemplates() {

            var currentProjectId = this.Params.String("projectId", null);

            var templates = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Where(p => p.ProjectType == Project.ProjectTypes.CostProposal)
                .Where(p => p.ParentId != null)
               
                .Where(p =>p.Name != null && p.Name.ToLower().Contains("template")).ToList();

          
            var result = templates.Select(t => new { PublicId = t.PublicId, Business = t.Business?.Name, Name = t.Name });
            //templates.First()
            this.SendAPIMessage("success", new { templates = result });
        }

        [RequestHandler("/api/admin/projects/proposals/proposal/{publicId}/create-project")]
        public void CreateProject(string publicId) {
            var project = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Include(nameof(Project.Costs) + "." +  nameof(ProjectCost.WorkType))
                .Where(p=>p.ProjectType == Project.ProjectTypes.CostProposal)
                .GetByPublicId(publicId);

            if (project.Business == null) {
                throw new BackendException("no-business", "Please select a business for this proposal to create a project");
            }

            var generateSubprojectsFromCosts = this.Params.Bool("generateSubprojectsFromCosts", false);
            var name = this.Params.String("name");
                    
            Project newProject = null;
            using (var dbContextTransaction = this.DB.Database.BeginTransaction()) {
              
                newProject = Duplicate(name, project, checkSiblingName: true, projectType: Project.ProjectTypes.Project);
                
                foreach (var subProject in newProject.Children) {
                    subProject.ProjectType = Project.ProjectTypes.Project;
                    if (generateSubprojectsFromCosts) {
                        foreach (var cost in subProject.Costs) {
                            var subSubProject = CreateSubproject(subProject, cost.Description);
                            subSubProject.Budget = cost.Costs.Clone();
                            subSubProject.Timeline = true;
                            subSubProject.TimeRange = subProject.TimeRange.Clone();
                            this.DB.Projects().Add(subSubProject);
                        }
                    }

                    // Timerange?
                    if (subProject.TimeRange?.HasValue() == false && project.TimeRange?.HasValue() == true) {
                        subProject.TimeRange = project.TimeRange.Clone();
                    }

                    // Delete costs
                    foreach (var cost in subProject.Costs.ToList()) {
                        subProject.Costs.Remove(cost);
                        this.DB.DeleteEntity(cost);
                    }
                   
                }

                // TODO CHECK SEIBLINGS
                // Check name available
                var siblings = newProject.GetSiblings(this.DB).Where(c=>c.ProjectType == newProject.ProjectType).ToList();
                if (siblings.Any(c => c.Name == name )) {
                    var siblingsSame = siblings.Where(s => s.Name == name).ToList();
                    throw new BackendException("name-taken", "Name is already taken by a sibling: " + name);
                }

                // Save
                this.DB.SaveChanges();
                dbContextTransaction.Commit();
            }

          

            // Result
            this.SendAPIMessage("create-project-success", new { Project = new { Path = newProject.Path, url = newProject.URL, PublicId = newProject.PublicId } });
        }

        /// <summary>
        /// Duplicates a root project
        /// </summary>
        /// <param name="publicId"></param>
        [RequestHandler("/api/admin/projects/proposals/proposal/{publicId}/duplicate-project")]
        public void DuplicateProject(string publicId) {

            var name = this.Params.String("name", null);
            var project = this.DB.Projects()
                .Include(nameof(Project.Costs))
                .Include(nameof(Project.Parent))
                .Include(nameof(Project.Business))
                .Where(p => p.ProjectType == Project.ProjectTypes.CostProposal)
                .GetByPublicId(publicId);
            Project newProject = null;
            using (var dbContextTransaction = this.DB.Database.BeginTransaction()) {
                newProject = Duplicate(name, project, checkSiblingName: true);
               
              
                dbContextTransaction.Commit();
            }

            // Result
            this.SendAPIMessage("create-project-success", new { Project = new { Path = newProject.Path, url = newProject.URL, PublicId = newProject.PublicId } });
        }


        /// <summary>
        /// Duplicates a subproject
        /// </summary>
        /// <param name="publicId"></param>
        /// <param name="templateId"></param>
        [RequestHandler("/api/admin/projects/proposals/proposal/{publicId}/duplicate-project/{templateId}")]
        public void DuplicateSubproject(string publicId, string templateId) {

            var saveAsTemplate = this.Params.Bool("saveAsTemplate", false);
           

            var project = this.DB.Projects()
                .Include(nameof(Project.Costs))
                .Include(nameof(Project.Costs) + "." + nameof(ProjectCost.WorkType))
                .Include(nameof(Project.Children))
                .Include(nameof(Project.Business))
                .Where(p => p.ProjectType == Project.ProjectTypes.CostProposal)
                .GetByPublicId(publicId);

            var subproject = this.DB.Projects()
                 .Include(nameof(Project.Costs))
                 .Include(nameof(Project.Costs) + "." + nameof(ProjectCost.WorkType))
                 .Include(nameof(Project.Children))
                 .Include(nameof(Project.Business))
                 .GetByPublicId(templateId);

            var name = this.Params.String("name", subproject.Name);

            var destinationParent = project; // if not "Templates"- Project


            Project newSubProject = null;
            using (var dbContextTransaction = this.DB.Database.BeginTransaction()) {
                
                // Parent project
                if (saveAsTemplate == false) {
                    newSubProject = DuplicateRecursive(this.DB, subproject, project.Business);
                    newSubProject.Parent = project;
                    newSubProject.ParentId = project.Id;
                    project.Children.Add(newSubProject);
                    
                } else {

                    // Templates project
                    var templateParent = this.DB.Projects()
                      .Include(nameof(Project.Costs))
                      .Include(nameof(Project.Costs) + "." + nameof(ProjectCost.WorkType))
                      .Include(nameof(Project.Children))
                      .Include(nameof(Project.Business))
                      .Where(p => p.ProjectType == Project.ProjectTypes.CostProposal)
                      .FirstOrDefault(p => p.Name == "Templates");

                    if (templateParent == null) { throw new BackendException("missing-templates-project", "Please create a cost proposal named 'Templates' to use this"); }
                    newSubProject = DuplicateRecursive(this.DB, subproject, templateParent.Business);
                   
                   
                    newSubProject.Parent = templateParent;
                    newSubProject.ParentId = templateParent.Id;
                    templateParent.Children.Add(newSubProject);
                    destinationParent = templateParent;
                }
               
                // Save
                this.DB.SaveChanges(); // to check the siblings
              
                // Set name
                newSubProject.ChangeName(this.DB, name, updateAllSubProjectsAlso: true, checkSiblingName: true, updateBusiness: false);

                // Update budgets
                UpdateBudgets(this.DB, destinationParent);

                // Save
                this.DB.SaveChanges();

                // Commit aka all saves in one
                dbContextTransaction.Commit();
            }

            // Result
            this.SendAPIMessage("create-project-success", new { Project = new { Path = newSubProject.Path, url = newSubProject.URL, PublicId = newSubProject.PublicId } });
        }


        [RequestHandler("/api/admin/projects/proposals/proposal/{publicId}/toggle-user/{userId}")]
        public void ToggleUser(string publicId, string userId) {

            var project = this.DB.Projects()
               .Include(nameof(Project.Users) + "." + nameof(ProjectUser.User))
               .GetByPublicId(publicId);

            var user = this.DB.Users().GetByPublicId(userId);
            var enable = this.Params.Bool("value", false);

            var projectUser = project.Users.SingleOrDefault(u => u.User.Id == user.Id);

            if (projectUser != null && !enable) {
                this.DB.ProjectUsers().Remove(projectUser);
            } else if (enable) {
                project.AddNewProjectUser(user);
            }

            this.DB.SaveChanges();
            this.SendAPIMessage("toggle-success");
        }

        [RequestHandler("/api/admin/projects/proposals/proposal/{publicId}/subprojects/sort")]
        public void SortSubprojects(string publicId) {

            this.RequireWriteARN();

            var project = this.DB.Projects()
                 .Include(nameof(Project.Children))
                 .GetByPublicId(publicId)
                 ;


            var orderIds = this.Params.StringArray("order", new string[] { });
            int order = 0;

            // Set the order/sort
            foreach (var id in orderIds) {
                var subproject = project.Children.AsQueryable().GetByPublicId(id);
                var impl = subproject.LoadImplementation() as ProjectCostProposal;
                impl.Sort = order;
                subproject.SaveImplementation(impl);
                order++;
            }

            // Save to DB...
            this.DB.SaveChanges();
            this.SendAPIMessage("success");

        }

        [RequestHandler("/api/admin/projects/proposals/proposal/{parentId}/subproject/{publicId}/costs/sort")]
        public void SortSubprojectCosts(string parentId, string publicId) {

            this.RequireWriteARN();

            var parent = this.DB.Projects()
                 .Include(nameof(Project.Children) + "." + nameof(Project.Costs))
                 .GetByPublicId(parentId)
                 ;


            var project = parent.Children.AsQueryable().GetByPublicId(publicId);

            var orderIds = this.Params.StringArray("order", new string[] { });
            int order = 0;

            // Set the order/sort
            foreach (var id in orderIds) {
                var cost = project.Costs.AsQueryable().GetByPublicId(id);
                cost.Sort = order;
                order++;
            }

            // Save to DB...
            this.DB.SaveChanges();
            this.SendAPIMessage("success");

        }

        /// <summary>
        /// Duplicates the project
        /// HINT: does sevaral db.SaveChanges and should be wrapped into a transaction
        /// </summary>
        /// <param name="name"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        private Project Duplicate(string name, Project project, bool checkSiblingName, Project.ProjectTypes? projectType = null) {
            var newProject = DuplicateRecursive(this.DB, project, project.Business);
            if (projectType != null) {
                newProject.ProjectType = projectType.Value;
            }
            newProject.ChangeName(this.DB, name, updateAllSubProjectsAlso: true, checkSiblingName: checkSiblingName);
            this.DB.SaveChanges();
            return newProject;
        }


        /// <summary>
        /// Duplicates the project and the subproject and the costs but not the events and tasks
        /// This should be wrapped into a transaction because of the several db.saves 
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        private static Project DuplicateRecursive(ModelContext db, Project project, Business business) {

            if (project.ProjectType != Project.ProjectTypes.CostProposal) {
                throw new Exception("DuplicateRecursive not supported for " + project.ProjectType);
            }

            // Load children
            // project.Include(nameof(project.Children));
            project.LoadFirstLevelNavigationReferences();
            project.LoadFirstLevelObjectReferences();
            var projectImpl = project.LoadImplementation() as ProjectCostProposal;

            var newProject = new Project();
            newProject.Children = new List<Project>();

            // Project
            //project.CopyValuesTo(newProject, new FormBuilder(), skipEmpty: false, ignoreType: false);
            newProject.ProjectType = Project.ProjectTypes.CostProposal;
            newProject.Name = project.Name;
            newProject.TimeRange = project.TimeRange?.Clone();
            newProject.Color = project.Color;
            newProject.Budget = project.Budget?.Clone();

            {
                var newProjectImpl = newProject.LoadImplementation() as ProjectCostProposal;
                newProjectImpl.Unit = projectImpl.Unit;
                newProjectImpl.DefaultCostsPerUnit = projectImpl.DefaultCostsPerUnit.Clone();
                newProjectImpl.AppendixNodeIds = projectImpl.AppendixNodeIds;
                newProjectImpl.DocumentTitle = projectImpl.DocumentTitle;
                newProjectImpl.Sort = projectImpl.Sort;
                if (projectImpl.BillingAddress != null) {
                    newProjectImpl.BillingAddress = Address.Duplicate(projectImpl.BillingAddress);
                }
                newProject.SaveImplementation(newProjectImpl);
            }
            newProject.ParentId = project.ParentId;

            db.Projects().Add(newProject);
            db.SaveChanges(); // for id

            // Description Nodes
            if (project.Description != null) {
                var path = ContentNode.GetNodePathForEntityProperty(newProject, nameof(newProject.Description));
                newProject.Description = project.Description.Duplicate(db, path);
            }

            // Notes Nodes
            if (project.Notes != null) {
                var path = ContentNode.GetNodePathForEntityProperty(newProject, nameof(newProject.Notes));
                newProject.Notes = project.Notes.Duplicate(db, path);
            }



            // Business
            if (business != null){
                newProject.Business_Id = business.Id;
            }

            // Costs
            {
                foreach (var cost in project.Costs.ToList()) {
                    var newCost = new ProjectCost();
                    //cost.CopyValuesTo(newCost, new FormBuilder(), skipEmpty: false, ignoreType: false);
                    newCost.Created = DateTime.UtcNow;
                    newCost.Billable = cost.Billable;
                    newCost.ProjectCostType = cost.ProjectCostType;
                    newCost.Billed = cost.Billed;
                    newCost.Units = cost.Units;
                    newCost.CostPerUnit = cost.CostPerUnit?.Clone();
                    newCost.CostPerUnitInternal = cost.CostPerUnitInternal?.Clone();
                    newCost.TimeRange = cost.TimeRange?.Clone();
                    newCost.Description = cost.Description;
                    newCost.Note = cost.Note;
                    newCost.Duration = cost.Duration;
                    newCost.User = cost.User;
                    newCost.WorkType = cost.WorkType;
                    newCost.OvertimeFactor = cost.OvertimeFactor;
                    newCost.Sort = cost.Sort;
                    newCost.Business = business;
                    newProject.Costs.Add(newCost);
                }
            }

            // Subprojects
            {
                foreach (var subproject in project.Children.ToList()) {
                   var newSubProject = DuplicateRecursive(db, subproject, business);
                    newProject.Children.Add(newSubProject);
                }
            }


            return newProject;
        }
    }
}
