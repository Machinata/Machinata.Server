using System.Linq;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Module.Projects.Model;
using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using System;
using Machinata.Module.Projects.Logic;
using System.Collections.Generic;
using Machinata.Module.Projects.View;
using System.Text;

namespace Machinata.Module.Projects.Handler {


    public class ProjectsPDFHandler : PDFPageTemplateHandler {


        [RequestHandler("/pdf/projects/project/{publicId}/costs", "/admin/projects/*")]
        public void Costs(string publicId) {

            var project = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Include(nameof(Project.WorkTypes))
                .GetByPublicId(publicId);

            // Rights
            this.CheckAdminRightsOrProjectMembership(project.GetRootProject(), this.User, throwException: true);


            var billingAddress = project.Business.GetBillingAddressForBusiness();
            var ownerBusinessAddress = this.DB.OwnerBusiness().GetBillingAddressForBusiness();


            var form = new FormBuilder(Forms.Admin.EMPTY)
                .Include(nameof(ProjectCost.Description))
                .Include(nameof(ProjectCost.Units))
                .Include(nameof(ProjectCost.CostPerUnit))
                .Include(nameof(ProjectCost.WorkType))
                .Include(nameof(ProjectCost.DurationShort))
                .Include(nameof(ProjectCost.CostsDefaultCurrency));


            var formTotal = new FormBuilder(Forms.Admin.EMPTY)
                .Include(nameof(ProjectCost.Units))
                .Include(nameof(ProjectCost.DurationShort))
                .Include(nameof(ProjectCost.CostsDefaultCurrency));


            this.InsertFilteredProjectCosts(this.Template, project, true, form, formTotal);


            this.Template.Data.Replace("{line-break}", "<br/>");

            this.Template.InsertVariables("project", project);
            this.Template.InsertVariables("project.business", project.Business);
          //  this.Template.InsertVariable("date", DateTime.UtcNow.ToDefaultTimezoneDateTimeString());
            this.Template.InsertVariableXMLSafeWithLineBreaks("business.billing-address-breaked", billingAddress.ToString().Replace(", ", "\n"));
            this.Template.InsertVariableXMLSafeWithLineBreaks("invoice.sender-address.city", ownerBusinessAddress.City);
           
           
            this.Template.InsertVariable("default-currency", Core.Config.CurrencyDefault);




            // Set footer
            this.FooterLeft = "{text.costs} " + project.Name;

            // Set filename
            this.SetFileName(project.GetCostsPDFFileName());

        }


        [RequestHandler("/pdf/projects/proposals/proposal/{publicId}", "/admin/projects/*")]
        public void Proposal(string publicId) {

            // Rights: Only real admins
            this.CheckIsInAdminGroup(this.User, throwExceptions: true);


            var project = this.DB.Projects()
                .Include(nameof(Project.Description) + "." + nameof(ContentNode.Children) + "." + nameof(ContentNode.Children))
                .Include(nameof(Project.Business))
                .Include(nameof(Project.Children) + "." + nameof(Project.Costs))
                .Include(nameof(Project.Children) + "." + nameof(Project.Description)) // EF Bug: + "." + nameof(ContentNode.Children)+ "." + nameof(ContentNode.Children)
                                                                                       //.Include(nameof(Project.Users) + "." + nameof(ProjectUser.User)) --> not working EF bug
                .GetByPublicId(publicId);

            var projectImpl = project.LoadImplementation() as ProjectCostProposal;


            var users = this.DB.ProjectUsers()
                 .Include(nameof(ProjectUser.Project))
                .Include(nameof(ProjectUser.User))
                .Where(p => p.Project != null && p.Project.Id == project.Id)
                .Where(p => p.User != null && p.User.Enabled == true);




            // Billding Address
            //var billingAddress = project.Business?.GetBillingAddressForBusiness();
            var billingAddress = projectImpl.BillingAddress;
            if (billingAddress == null) {
                billingAddress = project.Business?.GetBillingAddressForBusiness();
            }


            // Sender Address
            var ownerBusinessAddress = this.DB.OwnerBusiness().GetBillingAddressForBusiness();

            // Horizontal layout
            this.PageWidth = Core.Config.PDFPageHeight;
            this.PageHeight = Core.Config.PDFPageWidth;

            // Form Total
            var formTotal = new FormBuilder(Forms.Admin.EMPTY)
                .Include(nameof(ProjectCost.Units))
                .Include(nameof(ProjectCost.DurationShort))
                .Include(nameof(ProjectCost.CostsDefaultCurrency));


            // Description
            {
                string description = project.GetProjectescriptionHtml(this.Language);
                this.Template.InsertVariableUnsafe("project.description-html", description);
            }

            // Subprojects
            this.InsertProjectProposalsSubprojects("groups", project);

            // Total/Overview
            this.InserProjectProposalsTotalGroup("groups-overview",project, new FormBuilder(ProjectsForms.PROJECT_PROPOSALS_PDF_FORM));

            // Linebreaks
            this.Template.Data.Replace("{line-break}", "<br/>");

            this.Template.InsertVariables("project", project);
            if (project.Business != null) {
                this.Template.InsertVariables("project.business", project.Business);
            } else {
                // what happens here?
            }

            // Date
            this.Template.InsertVariable("date", projectImpl.Date?.ToHumanReadableDateString());

            // Business ?
            this.Template.InsertVariableXMLSafeWithLineBreaks("business.billing-address-breaked", billingAddress?.ToString().Replace(", ", "\n"));
            this.Template.InsertVariableXMLSafeWithLineBreaks("invoice.sender-address.city", ownerBusinessAddress.City);
            this.Template.InsertVariables("invoice.sender-address", ownerBusinessAddress);

            this.Template.InsertVariable("default-currency", Core.Config.CurrencyDefault);

            // Title
            if (string.IsNullOrWhiteSpace(projectImpl.DocumentTitle) == false) {
                this.Template.InsertVariableXMLSafeWithLineBreaks("project.document-title-with-line-break", projectImpl.DocumentTitle + "\n");
                // Set footer
                this.FooterLeft = projectImpl.DocumentTitle + " " + project.Name;
            } else {
                this.Template.InsertVariableEmpty("project.document-title-with-line-break");
                // Set footer
                this.FooterLeft = project.Name;
            }

            // Appendix? 
            if (projectImpl.AppendixNodeIdsList.Any() == true) {
                var templates = new List<PageTemplate>();
                var contentNodes = this.DB.ContentNodes().GetPagesAtPath(Project.COST_PROPOSAL_APPENDIX_PATH).OrderBy(cn => cn.Sort).ToList();

                foreach (var contentNode in contentNodes) {

                    if (projectImpl.AppendixNodeIdsList.Contains(contentNode.PublicId)) {
                        var appendixTemplate = this.Template.LoadTemplate("proposal.appendix");
                        // Appendixes parent path + appendix short url

                        // CMS tailer
                        appendixTemplate.InsertContent(
                          variableName: "appendix",
                          cmsPath: contentNode.Path,
                          throw404IfNotExists: false,
                          language: this.Language
                        );
                        templates.Add(appendixTemplate);
                    }
                }


                this.Template.InsertTemplates("project.appendices", templates);
            } else {
                this.Template.InsertVariableEmpty("project.appendices");
            }



            // Sender: Address, users
            if (users.Any() == true) {
                var usersString = string.Join(", ", users.Select(u => u.User.Name)) + "\n";
                this.Template.InsertVariableXMLSafeWithLineBreaks("project.user-senders", usersString);
            } else {
                this.Template.InsertVariableEmpty("project.user-senders");

            }




            // Set filename
            this.SetFileName(project.GetProjectProposalPDFFileName(projectImpl.DocumentTitle));

        }

      


    }
}
