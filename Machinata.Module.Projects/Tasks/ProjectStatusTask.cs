using Machinata.Core.Messaging;
using Machinata.Core.Messaging.Providers;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Module.Finance.Model;
using Machinata.Module.Projects.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Projects.Tasks {

    public class ProjectStatusTask : Core.TaskManager.Task {


        public const string EMAIL_OVER_BUDGET_SENT_PREFIX = "EmailOverBudgetSent-";
      
        public static string GetNotificationEmailId(string id) {
            return ProjectStatusTask.EMAIL_OVER_BUDGET_SENT_PREFIX + id;
        }

        public override void Process() {
            
            ProcessProjects(0.75m, 0.9m);
            ProcessProjects(0.9m, 1m);
            ProcessProjects(1m, 1.1m);
            ProcessProjects(1.10m, 1.20m);
            ProcessProjects(1.2m, 1.3m);
            ProcessProjects(1.3m, 1.4m);
            ProcessProjects(1.4m, 1.5m);
            ProcessProjects(1.5m, decimal.MaxValue);

         

            // Save if not TestMode
            if (this.TestMode == false) {
                DB.SaveChanges();
            }
        }

        private void ProcessProjects(decimal minConsumption, decimal maxConsumption) {
            var projects = Projects.Logic.ProjectReporting.GetProjectsWithBudgetConsumption(this.DB, minConsumption, maxConsumption);
            Log($"Found {projects.Count()} over {minConsumption} Budget consumption");

            // E.g. "90" or "100"
            string thresholdId =((int)(minConsumption * 100m)).ToString();

            foreach (var project in projects) {

                if (project.OverBudgetSent(thresholdId) == false) {

                    var budget = project.CalculateBudget(ProjectBudget.BudgetTypes.External);
                    Log($"Sending notifications for project {project.Name}: {project.PublicId} - {project.Path}: {budget.BudgetUsed} ");

                    // Notification
                    project.SendNotificationEmail(this.DB, thresholdId, this, budget);
                    
                    // Set email sent
                    project.SetOverBudgetSent(thresholdId);


                } else {
                    Log($"Notification for {project.Business.Name} {project.Name} already sent");
                }
            }

            Log($"-----------------------------------------------------------------");
            Log($"");
        }

        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = false;
            config.Install = false;
            config.Interval = "1h";
            return config;
        }
    }

    public static class ProjectExtensions {


        public static bool OverBudgetSent(this Project project, string id) {
            var val = project.Notifications?[ProjectStatusTask.GetNotificationEmailId(id)]?.ToString();
            if (val != null && val.ToLowerInvariant() == "true") {
                return true;
            }
            return false;
        }

        public static void SetOverBudgetSent(this Project project, string id) {
            project.Notifications[ProjectStatusTask.GetNotificationEmailId(id)] = true;
        }
        public static void SendNotificationEmail(this Project project, ModelContext db, string id, ProjectStatusTask task, ProjectBudget budget) {

            var users = project.GetProjectUsersAndNotificationEmails().Where(pu=>pu.Enabled == true);

            // cap users on Test system
            if (Core.Config.IsTestEnvironment == true) {
                users = users.Reverse().Take(3);
            }

            foreach (var user in users) {
                task.Log($"Sending notification to {user.Name}, {user.Email}: ");

                var subject = $"Project Budget Warning for {project.Business.Name}: {project.Name}";


                var template = new EmailTemplate(db, "budget-warning");
                template.InsertVariables("project", project);
                template.InsertVariables("project.business", project.Business);
                template.InsertVariables("budget", budget);
                if (user.User != null) {
                    template.InsertVariables("user", user.User);
                }
                template.Subject = subject;


                if (task.TestMode == false) {

                    var contact = new EmailContact();
                    contact.Address = user.Email;
                    contact.Name = user.Name;

                    var subscribeCheck = false;

                    // Before: with unsub check
                    if (subscribeCheck == true) {
                        template.SendEmail(contact, MailingUnsubscription.Categories.Projects, "warnings");
                    } else {
                        // Dont check unsubscribe since we regurarly get bounces from mailgun
                        template.SendWithoutUnsubscribeCheck(
                           to: new List<EmailContact>() { contact },
                           cc: null,
                           bcc: null,
                           attachements: null
                           );
                    }

                }
            }
        }



    }
}
