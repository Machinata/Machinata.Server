using Machinata.Core.Builder;
using Machinata.Core.Messaging;
using Machinata.Core.Messaging.Providers;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Module.Finance.Model;
using Machinata.Module.Projects.Logic;
using Machinata.Module.Projects.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Projects.Tasks {

    public class ProjectReportingTask : Core.TaskManager.Task {



        public override void Process() {


            ProcessProjects();


            // Save if not TestMode
            if (this.TestMode == false) {
                DB.SaveChanges();
            }
        }

        private void ProcessProjects() {
            var projects = this.DB.Projects()
                .Include(nameof(Project.Business))
                .Where(p => p.Deleted == false)
                .Where(p => p.Completed == false)
                .OnlyStandardProjects()
                .Where(p => p.Archived == false)
                .Where(p => p.AutomaticReporting > Project.ProjectReportingTypes.Off).ToList();

            Log($"Found {projects.Count()} for reporting");
            var localNow = Core.Util.Time.ToDefaultTimezone(DateTime.UtcNow);
            foreach (var project in projects) {
              

                if (project.AutomaticReporting == Project.ProjectReportingTypes.Weekly && project.WeeklyReportSent() == false && (Core.Util.Time.IsPastTimeOfWeekday(localNow, DayOfWeek.Sunday, new TimeSpan(17, 0, 0)) )) {
                    Log("Sending weekly report for " + GetProjectDescription(project));
                    project.SendReportEmail(this.DB, this);
                    project.SetWeeklyReportSent(localNow);
                } else if (project.AutomaticReporting == Project.ProjectReportingTypes.Monthly && project.MonthlyReportSent() == false && (Core.Util.Time.IsEndOfMonth(localNow, new TimeSpan(10, 0, 0)) )) {
                    Log("Sending monthly report for " + GetProjectDescription(project));
                    project.SendReportEmail(this.DB, this);
                    project.SetMonthlyReportSent(localNow);
                } else if (project.AutomaticReporting == Project.ProjectReportingTypes.Daily && project.DailyReportSent() == false && (Core.Util.Time.IsEndOfDay(localNow, new TimeSpan(12, 0, 0)) )) {
                    Log("Sending daily report for " + GetProjectDescription(project));
                    project.SendReportEmail(this.DB, this);
                    project.SetDailyReportSent(localNow);
                } else {
                    Log("Not time to send a report for " + GetProjectDescription(project));
                }


                Log($"-----------------------------------------------------------------");
                Log($"");
            }
        }

        private static string GetProjectDescription(Project project) {
            return project.Business?.Name + " " +  project.ShortURLPath + ", Reporting: " + project.AutomaticReporting + ", Sent infos: " + project.Notifications;
        }

        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = false;
            config.Install = false;
            config.Interval = "1h";
            return config;
        }
    }

    public static class ProjectReportingExtensions {


        public static bool WeeklyReportSent(this Project project) {
            var localNow = Core.Util.Time.ToDefaultTimezone(DateTime.UtcNow);
            string key = GetWeeklyKey();
            var val = project.Notifications?[key]?.ToString();
            if (val != null && val.ToLowerInvariant() == GetWeeklyValue(localNow)) {
                return true;
            }
            return false;
        }

        private static string GetWeeklyKey() {
            return "WeeklyReport";
        }

        private static string GetWeeklyValue(DateTime localNow) {
            CultureInfo myCI = CultureInfo.InvariantCulture;
            Calendar myCal = myCI.Calendar;

            var week = myCal.GetWeekOfYear(localNow, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
            return week.ToString();
        }




        private static string GetDailyKey() {
            return "DailyReport";
        }

        private static string GetDailyValue(DateTime localNow) {
            return localNow.ToString("yyyy.MM.dd");
        }


        public static bool MonthlyReportSent(this Project project) {
            var localNow = Core.Util.Time.ToDefaultTimezone(DateTime.UtcNow);
            string key = GetMonthlykey();
            var val = project.Notifications?[key]?.ToString();
            if (val != null && val.ToLowerInvariant() == GetMonthlyValue(localNow)) {
                return true;
            }
            return false;
        }

        public static bool DailyReportSent(this Project project) {
            var localNow = Core.Util.Time.ToDefaultTimezone(DateTime.UtcNow);
            string key = GetDailyKey();
            var val = project.Notifications?[key]?.ToString();
            if (val != null && val.ToLowerInvariant() == GetDailyValue(localNow)) {
                return true;
            }
            return false;
        }


        private static string GetMonthlykey() {
            return "MonthlyReport";
        }

        private static string GetMonthlyValue(DateTime localNow) {
            return Core.Util.Time.ToMonthString(localNow);
        }

        public static void SetWeeklyReportSent(this Project project, DateTime localTime) {
            project.Notifications[GetWeeklyKey()] = GetWeeklyValue(localTime);
        }

        public static void SetDailyReportSent(this Project project, DateTime localTime) {
            project.Notifications[GetDailyKey()] = GetDailyValue(localTime);
        }

        public static void SetMonthlyReportSent(this Project project, DateTime localTime) {
            project.Notifications[GetMonthlykey()] = GetMonthlyValue(localTime);
        }

        public static void SendReportEmail(this Project project, ModelContext db, Core.TaskManager.Task task) {

            var users = project.GetProjectUsersAndNotificationEmails().Where(pu=>pu.Enabled == true);

            var utcNow = DateTime.UtcNow;

            // cap users on Test system
            if (Core.Config.IsTestEnvironment == true) {
                users = users.Reverse().Take(1);
            }

            foreach (var user in users) {
                task.Log($"Sending {project.AutomaticReporting} report to {user.Name}, {user.Email}: ");

                var subject = $"{project.AutomaticReporting} Project Report for {project.Business.Name}: {project.Name}";

                // Sample public url with access-key
                https://nerves.icasa.office.nerves.ch/admin/projects/project/report/sMWgLA/barry-callebaut/f-c-design-guidelines?timerange=2021-2022&access-key=[ENCRYPTED%20HASH]

                // DateRange in URL
                var dateRange = project.GetCurrentTimeRangeString(utcNow);
                string reportURL = GetReportURL(project, dateRange);

                var template = new EmailTemplate(db, "project-report");
                template.InsertVariable("timerange", dateRange);
                template.InsertVariables("project", project);
                template.InsertVariables("project.business", project.Business);
                template.InsertVariable("report-url", reportURL);

                // User
                if (user.User != null) {
                    template.InsertVariables("user", user.User);
                }

                // Subject
                template.Subject = subject;


                // Budget overview
                {
                    var budgets = Project.GetProjectBudgetsView(project);
                    template.InsertEntityList(variableName: "budgets",
                        entities: budgets.ToList(),
                        form: new FormBuilder(Forms.Frontend.LISTING),
                        total: new Core.Builder.FormBuilder(Forms.Frontend.TOTAL));
                }

                // Costs per subproject overview of timerange
                {
                    var timeRange = project.GetCurrentTimeRangeForReporting(utcNow);

                    var allCosts = project.GetAllCosts(hideDeleted: true).AsQueryable();
                    var costs = allCosts.FilterDateRange(timeRange.Start.Value, timeRange.End.Value);

                    var budgets = Project.GetProjectCostsView(project, costs);
                    template.InsertEntityList(variableName: "costs",
                        entities: budgets.ToList(),
                        form: new FormBuilder(Forms.Frontend.LISTING),
                        total: new Core.Builder.FormBuilder(Forms.Frontend.TOTAL));
                }



                // Sending email
                {

                    var contact = new EmailContact();
                    if (task.TestMode == false) {
                        contact.Address = user.Email;
                        //contact.Address = "micha@nerves.ch";
                    } else {
                        contact.Address = "micha@nerves.ch";
                    }


                    contact.Name = user.Name;

                    // Send with Unsubsribe check
                    //template.SendEmail(contact, MailingUnsubscription.Categories.Projects, "reports");

                    // 
                    template.SendWithoutUnsubscribeCheck(
                        to: new List<EmailContact>() { contact },
                        cc: null,
                        bcc: null,
                        attachements: null

                        );

                }
            }
        }

        public static string GetReportURL(this Project project, string dateRange, string billed = null, string user = null) {
            var reportURL = Core.Config.PublicURL + project.ReportURL;
            reportURL = Core.Util.URL.SetQueryParameter(reportURL, "timerange", dateRange);
            if (billed != null) {
                reportURL = Core.Util.URL.SetQueryParameter(reportURL, "billed", billed.ToString());
            }
            if (user != null) {
                reportURL = Core.Util.URL.SetQueryParameter(reportURL, "users", user.ToString());
            }

            // Access code in URL
            reportURL = Core.Util.URL.GetURLWithAccessCodeForURL(reportURL);
            return reportURL;
        }
    }
}
