using Machinata.Core.Builder;
using Machinata.Core.Messaging;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Projects.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machinata.Module.Projects.Tasks {

    public class UnbilledCostsTask : Core.TaskManager.MonthlyTask {

        public const string USER_SETTINGS_KEY_SEND_UNBILLED_COSTS_EMAILS = "ProjectsSendUnbilledCostsEmail";
       // public const int DAY_OF_MONTH_TO_SEND_EMAIL = 1;
             
        public override void Process() {

            var minDate = DateTime.UtcNow.AddYears(-20);
            var maxDate = DateTime.UtcNow.AddDays(-0);
           
            var now = DateTime.UtcNow;
            var projectCosts = new Dictionary<Project, List<ProjectCost>>();

            var users = this.DB.Users().Where(pu => pu.Enabled == true && pu.CreationSource == User.CreationSources.Admin).ToList();
            users = users.Where(u => u.Settings.Bool(USER_SETTINGS_KEY_SEND_UNBILLED_COSTS_EMAILS, false)).ToList();

            Log($"Found {users.Count()} contacts to send emails to...");

            var rootProjs = this.DB.Projects()
                .Include(nameof(ProjectCost.Business))
                .Where(p => p.Archived == false)
                .Where(p => p.Deleted == false)
                .Where(p => p.Completed == false)
                .OnlyStandardProjects()
                .Where(p => p.ParentId == null && p.Business.IsOwner == false).ToList().OrderBy(p => p.BusinessFormalName);

            Log($"Found {rootProjs.Count()} projects to check the costs...\n\n");


            var result = new List<UnbilledCostsProject>();

            foreach (var rootProj in rootProjs) {
                CollectUnbilledCost(maxDate, minDate, result, rootProj, 0);
            }

            TaskLog taskLog = null;

            // Day to send?
            taskLog = GetTaskLog(now);

            var isDayToSend = now.Day == Config.ProjectsUnbilldedCostsEmailDayToSend;

            // Send Email?
            if (TestMode == false && result.Any() && taskLog == null && isDayToSend) {
                // Contacts
                var emailList = users.Select(u => new EmailContact() { Name = u.Name, Address = u.Email });
                Log("Sending emails to:", false);

                // Email
                var email = new EmailTemplate(this.DB, "unbilled-costs");
                email.Subject = "Unbilled Costs " + now.ToString("MMMM yyyy");
                email.InsertEntityList(
                    variableName: "costs",
                    entities: result,
                    form: new FormBuilder(Forms.Admin.LISTING),
                    link: "{public-url}{entity.url}"
                    );

                // Send
                foreach (var emailContact in emailList) {
                    Log($"{emailContact.Name}: {emailContact.Address}", false);
                    email.SendEmail(emailContact, MailingUnsubscription.Categories.Finance, "unbilled-costs");
                }

                // Mark as sent
                this.SetMonthlyTaskDone(now);

            } else {

                Log($"Not sending email. Day={now.Day}, Sent={taskLog?.Created}, TestMode={this.TestMode}", false);
            }

            if (this.TestMode == false) {
                this.DB.SaveChanges();
            }

            Log("\n\n", false);

        }

        private void CollectUnbilledCost(DateTime maxDate, DateTime minDate, List<UnbilledCostsProject> output, Project project, int level) {
            var costs = project.GetAllCostsOfProject(this.DB, true).

            Where(c => c.Costs != null &&
                       c.Billable == true &&
                       c.Billed == null &&
                       c.Created < maxDate &&
                       c.Created > minDate &&
                       c.Costs.HasValue == true
                       && c.Deleted == false
                       );

            if (costs.Any() == true) {

                var unbilledProject = new UnbilledCostsProject();
                unbilledProject.Project = project;
                unbilledProject.Costs = costs.Select(c => c.Costs.Value.Value).Sum();
                unbilledProject.Hours = costs.Where(c => c.ProjectCostType == ProjectCost.ProjectCostTypes.Timesheet)
                    .Select(c => c.Units).Sum();

                Log(unbilledProject.ToString(), false);

                if (unbilledProject.Costs > 0) {
                    output.Add(unbilledProject);
                }

            }


        }


        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = false;
            config.Install = true;
            config.Interval = "5h";
            return config;
        }


    }


    public class UnbilledCostsProject : ModelObject {

        [FormBuilder(Forms.System.LISTING)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Business { get { return this.Project.BusinessFormalName; } }

        [FormBuilder(Forms.System.LISTING)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string ProjectName { get { return this.Project.Name; } }

        // [FormBuilder(Forms.Admin.LISTING)]
        public Project Project;

        [FormBuilder(Forms.System.LISTING)]
        [FormBuilder(Forms.Admin.LISTING)]
        public decimal Costs { get; set; }

        [FormBuilder(Forms.System.LISTING)]
        [FormBuilder(Forms.Admin.LISTING)]
        public decimal Hours { get; set; }

        [FormBuilder(Forms.System.LISTING)]
        public string URL { get { return this.Project.URL; } }



        public override string ToString() {
            var result = new StringBuilder();
            result.AppendLine("Business: " + this.Business);
            result.AppendLine("Project: " + this.Project.Name);
            result.AppendLine("Costs: " + this.Costs);
            result.AppendLine("Hours: " + this.Hours);
            result.AppendLine("Completed: " + this.Project.Completed);
            result.AppendLine("Archived: " + this.Project.Archived);
            result.AppendLine("Deleted: " + this.Project.Deleted);
            return result.ToString();

        }
    }





}
