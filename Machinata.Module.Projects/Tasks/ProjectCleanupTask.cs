using Machinata.Core.Model;
using Machinata.Module.Projects.Model;
using System;
using System.Linq;

namespace Machinata.Module.Projects.Tasks {

    public class ProjectCleanupTask : Core.TaskManager.Task {
        public override void Process() {

            // Remove old entities from trash
            CleanupProjectTrash(this.DB.Projects().AsQueryable().OnlyStandardProjects());
            CleanupProjectTrash(this.DB.ProjectCosts());
            CleanupProjectTrash(this.DB.ProjectEvents());
            CleanupProjectTrash(this.DB.ProjectTasks());
        }

        

        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = true;
            config.Install = true;
            config.Interval = "1d";
            return config;
        }

        private void CleanupProjectTrash(IQueryable<ProjectDeletable> entities) {

             var maxAge = DateTime.UtcNow.AddDays(-14);

            // Grab pages under /Trash
            var trashedEntities = entities.Where(e => e.Deleted == true && e.DeletedDate < maxAge).ToList();

            int deletedItems = 0;

            foreach (var entitiy in trashedEntities) {

                // Log
                this.Log($"\t{entitiy.TypeName} {entitiy.PublicId}/ {entitiy.Id}  deleted {entitiy.DeletedDate}) older than {maxAge}, will delete...");

                // Delete
                this.DB.DeleteEntity(entitiy); // hint: this will also delete children via OnDelete

                // Progress
                deletedItems++;

                // Save
                if (this.TestMode == false) {
                    this.DB.SaveChanges();
                }
            }

            Log($"Deleted {deletedItems} entities.");
        }


    }

   



}
