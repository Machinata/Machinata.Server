using Machinata.Core.Builder;
using Machinata.Core.Messaging;
using Machinata.Core.Messaging.Providers;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.Finance.Model;
using Machinata.Module.Projects.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Projects.Tasks {

    public abstract class MonthlyTask : Core.TaskManager.Task {

        public TaskLog GetTaskLog(DateTime dateTime) {
            var id = GetTaskId(dateTime);
            var taskLog = this.DB.TaskLogs().FirstOrDefault(t => t.TaskType == this.Type && t.TaskId == id);
            return taskLog;
        }

        public string GetTaskId(DateTime dateTime) {
            return dateTime.ToString("yyyy-MM");
        }

        public void SetMonthlyTaskDone(DateTime dateTime) {
            var id = GetTaskId(dateTime);
            var taskLog = new TaskLog();
            taskLog.TaskId = id;
            taskLog.TaskType = this.Type;
            this.DB.TaskLogs().Add(taskLog);
        }

     
    }





}
