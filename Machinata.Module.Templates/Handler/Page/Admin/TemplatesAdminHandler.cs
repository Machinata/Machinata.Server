
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core;
using Machinata.Module.Templates.Model;
using Machinata.Core.Exceptions;

namespace Machinata.Module.Admin.Handler {


    public class TemplatesAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("templates");
        }

        #endregion

        #region Menu 

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "template",
                Path = "/admin/templates",
                Title = "{text.templates}",
                Sort = "500"
            });
        }

        #endregion

        [RequestHandler("/admin/templates")]
        public void Default() {
            
            // Listing
            var entities = this.Template.Paginate(
                this.DB.Templates().Include(nameof(Templates.Model.Template.Business)).Where(e => e.Enabled == true), 
                this
            );
            this.Template.InsertEntityList(
                variableName: "entities",
                entities: entities,
                form: new Core.Builder.FormBuilder(Forms.Admin.LISTING),
                link: "admin/templates/template/{entity.public-id}"
            );

            // Navigation
            this.Navigation.Add("templates");


        }

        
        [RequestHandler("/admin/templates/all")]
        public void All() {
            
            // Listing
            var entities = this.Template.Paginate(
                this.DB.Templates().Include(nameof(Templates.Model.Template.Business)), 
                this
            );
            this.Template.InsertEntityList(
                variableName: "entities",
                entities: entities,
                form: new Core.Builder.FormBuilder(Forms.Admin.LISTING),
                link: "admin/templates/template/{entity.public-id}"
            );

            // Navigation
            this.Navigation.Add("templates");
            this.Navigation.Add("all");


        }

        [RequestHandler("/admin/templates/create")]
        public void Create() {
            this.RequireWriteARN();
            
            
            var form = new FormBuilder(Forms.Admin.CREATE);
            form.SelectionListForTypes(
                name:"TemplateType", 
                formName: "templatetype", 
                value: null, 
                types: Templates.Logic.TemplateImplementation.GetImplementations(),
                getTitle: delegate (Type t) { return t.Name.ReplaceSuffix("TemplateImplementation", ""); }
            );

            var entity = new Template();
            entity.User = this.User;
            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: form,
               apiCall: "/api/admin/templates/template/create",
               onSuccess: "/admin/templates/template/{entity.public-id}"
            );

            // Navigation
            this.Navigation.Add("templates");
            this.Navigation.Add("create");
        }


        [RequestHandler("/admin/templates/template/{publicId}")]
        public void View(string publicId) {
            this.RequireWriteARN();

            // Init
            var entity = DB.Templates()
                .Include(nameof(Templates.Model.Template.Business))
                .Include(nameof(Templates.Model.Template.User))
                .GetByPublicId(publicId);
            var impl = entity.GetImplementation();


            
                 
            // Variables
            this.Template.InsertPropertyList(
               variableName: "entity",
               entity: entity,
               form: new FormBuilder(Forms.Admin.VIEW)
            );
            this.Template.InsertVariables("entity", entity);
            string generateParams = "";
            if (entity.TemplateVariables != null) {
                foreach (var variable in entity.TemplateVariables.Split(',')) {
                    var segs = variable.Split('=');
                    string defaultValue = null;
                    if (segs.Length > 1) defaultValue = segs[1];
                    var variableName = segs[0].ToLower();
                    if(defaultValue != null) {
                        if (generateParams != "") generateParams += "&";
                        generateParams += $"{variableName}={Core.Util.HTTP.UrlPathEncode(defaultValue)}";
                    }
                }
            }
            this.Template.InsertVariable("entity.generate-parameters", generateParams);

            // Navigation
            this.Navigation.Add("templates");
            this.Navigation.Add($"template/{publicId}", entity.Title);

        }

        
        [RequestHandler("/admin/templates/template/{publicId}/generate", AccessPolicy.PUBLIC_ARN)]
        public void Generate(string publicId) {
         

            // Init
            var entity = DB.Templates()
                .Include(nameof(Templates.Model.Template.Business))
                .Include(nameof(Templates.Model.Template.User))
                .GetByPublicId(publicId);
            var impl = entity.GetImplementation();

            if (impl == null) {
                throw new BackendException("no-template", "Please define a template type prior to generating.");
            }

            // Verify authentication
            if(this.Params.String("guid") != entity.GUID) {
                this.RequireARN(AccessPolicy.GetDefaultAdminAccessARN("templates"));
            }

            // Change to output type
            this.Template.ChangeTemplate("generate." + impl.GetOutputType().ToString().ToLower());
            
            // Variables
            this.Template.InsertPropertyList(
               variableName: "entity",
               entity: entity,
               form: new FormBuilder(Forms.Admin.VIEW)
            );
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertCard("entity.visual-card", entity);

            // Configure form
            var configureForm = new FormBuilder();
            configureForm.Method = "get";
            if(this.Params.String("guid") != null) configureForm.Hidden("guid", this.Params.String("guid"));
            if (entity.TemplateVariables != null) {
                foreach (var variable in entity.TemplateVariables?.Split(',')) {
                    var segs = variable.Split('=');
                    string defaultValue = null;
                    if (segs.Length > 1) defaultValue = segs[1];
                    var variableName = segs[0].ToLower();
                    var variableValue = this.Params.String(variableName);
                    configureForm.Custom(segs[0], variableName, "text", variableValue);
                }
            }
            this.Template.InsertForm(
               variableName: "configure-form",
               entity: null,
               form: configureForm,
               apiCall: null,
               onSuccess: null
            );

            // Generated link
            var generatedLink = "/admin/templates/generated/" + entity.GUID + "?" + this.Context.Request.QueryString;
            if(impl.GetOutputType() == Templates.Logic.TemplateImplementation.TemplateOutputType.PDF) {
                generatedLink = "/pdf/admin/templates/generated/" + entity.GUID + "?" + this.Context.Request.QueryString;
            }
            this.Template.InsertVariableUnsafe("generated-link", generatedLink);

            // Instructions
            var instructionsTemplate = this.Template.LoadTemplate("instructions." + entity.TemplateType.ReplaceSuffix("TemplateImplementation", "").ToLower());
            this.Template.InsertTemplate("generated-instructions", instructionsTemplate);

            // Navigation
            this.Navigation.Add("templates");
            this.Navigation.Add($"template/{publicId}", entity.Title);
            this.Navigation.Add("generate","{text.generate-asset}");

        }

        
        
        
        [RequestHandler("/admin/templates/template/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Templates()
                .Include(nameof(Templates.Model.Template.Business))
                .Include(nameof(Templates.Model.Template.User))
                .GetByPublicId(publicId);

            var impl = entity.GetImplementation();
            if(impl != null) impl.LoadDefaultSettingsAndMerge();
            
            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: "/api/admin/templates/template/" + entity.PublicId + "/edit",
               onSuccess: "/admin/templates/template/" + entity.PublicId
            );

            // Variables
            Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("templates");
            this.Navigation.Add($"template/{publicId}", entity.Title);
            this.Navigation.Add($"edit");
        }

        
        

    }
}
