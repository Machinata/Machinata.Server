

/// <summary>
/// A standard infobox with a text and remove tool.
/// </summary>
Machinata.Dashboard.Widgets.Types["infobox"] = {
    init: function (dashboard, elem, config) {
        // Tools
        if (config.settings["URL"] != null) Machinata.Dashboard.Widgets.addLinkTool(elem, config.settings["URL"], config.settings["Icon"], config.settings["Hint"]);
        elem.addClass("option-contents-padding");
        elem.addClass("type-meta");
        if (config.settings["Align"] == "top") elem.addClass("option-contents-aligntop");
        // Init
        var bottom = $("<div class='bottom'></div>");
        //var bottomFader = $("<div class='ui-bottom-fader'></div>").appendTo(bottom);
        // Content
        if (!String.isNullOrEmpty(config.settings["Title"])) elem.data("contents").append($("<h3 class='title'></h3>").text(config.settings["Title"]));
        if (!String.isNullOrEmpty(config.settings["Subtitle"])) bottom.append($("<p class='subtitle'></h3>").text(config.settings["Subtitle"]));
        // Table
        var metaTable = $("<div class='table'></div>").appendTo(bottom);
        var itemsConfig = config.settings["Items"];
        for (var i = 0; i < itemsConfig.length; i++) {
            var itemConfig = itemsConfig[i];
            var item = $("<div class='row'></div>");
            if (itemConfig["Title"] != null) {
                item.append($("<td colspan='2' class='title'></td>").text(itemConfig["Title"]));
            } else if (itemConfig["Group"] != null) {
                item.append($("<td colspan='2' class='group'></td>").text(itemConfig["Group"]));
            } else {
                if (itemConfig["Key"] == null) {
                    item.append($("<td colspan='2' class='value left'></td>").text(itemConfig["Value"]));
                } else {
                    var key = itemConfig["Key"];
                    var val = itemConfig["Value"];
                    if (key == "" && val == "") {
                        key = "\xa0";
                        val = "\xa0";
                        item.addClass("empty");
                    }
                    item.append($("<div class='key'></div>").text(key));
                    item.append($("<div class='value'></div>").text(val));
                    if (itemConfig["Date"] != null) item.find(".value").prepend($("<span class='date'></span>").text(itemConfig["Date"]));
                }
            }
            item.appendTo(metaTable);
        }
        elem.data("contents").append(bottom);
    }
};


