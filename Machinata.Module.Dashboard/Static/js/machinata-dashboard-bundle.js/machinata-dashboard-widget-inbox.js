

/// <summary>
/// Inbox widget with message items.
/// </summary>
Machinata.Dashboard.Widgets.Types["inbox"] = {
    init: function (dashboard, elem, config) {
        // Tools
        Machinata.Dashboard.Widgets.addLinkTool(elem, config.settings["URL"], null, config.settings["Hint"]);
        if (config.settings["Subscribable"] != null) {
            Machinata.Dashboard.Widgets.addTool(elem, "Follow", "icon-csam-fn-small-follow", function () {
                Machinata.yesNoDialog("Follow", "Would you like to follow " + config.settings["Subscribable"] + "?")
                    .okay(function () {
                        Machinata.messageDialog("Follow", "Thanks for following " + config.settings["Subscribable"] + "! You'll get the latest updates in your inbox.").show();
                    })
                    .show();
            });
        }
        // Content
        elem.addClass("type-listing");
        elem.addClass("option-bottom-fader");
        var listing = $("<div class='ui-inbox'></div>");
        var itemsConfig = config.settings["Items"];
        for (var i = 0; i < itemsConfig.length; i++) {
            // Init
            var itemConfig = itemsConfig[i];
            var summarizedText = Machinata.String.createSummarizedText(itemConfig["Text"], 50);
            var dateMonth = itemConfig["DateMonth"];
            var dateDay = itemConfig["DateDay"];
            if ((dateMonth == null || dateMonth == "") && itemConfig["Date"] != null) {
                var date = Machinata.dateFromString(itemConfig["Date"], "dd.mm.yy"); //TODO format??
                dateMonth = Machinata.Time.getShortMonth(date);
                dateDay = Machinata.formattedDate(date, 'dd');
            }
            // Build elements
            var itemElem = $("<a class='ui-inbox-item'></a>");
            itemElem.attr("href", itemConfig["URL"]);
            var numberElem = $("<div class='icon text-icon'></div>").text(itemConfig["Number"]);
            var iconElem = $("<div class='icon'></div>").addClass(itemConfig["Icon"]);
            var dateElem = $("<div class='date'></div>").attr("data-date", itemConfig["Date"]);
            dateElem.append($("<div class='month'></div>").text(dateMonth));
            dateElem.append($("<div class='day ui-number'></div>").text(dateDay));
            var textElem = $("<div class='text'></div>");
            textElem.append($("<div class='subject'></div>").text(itemConfig["Title"]));
            textElem.append($("<div class='message'></div>").text(summarizedText));
            if (itemConfig["Number"] != null) itemElem.append(numberElem);
            if (itemConfig["Icon"] != null) itemElem.append(iconElem);
            if (dateMonth != null) itemElem.append(dateElem);
            itemElem.append(textElem);
            itemElem.append($("<div class='clear'></div>"));
            itemElem.appendTo(listing);
        }
        listing.appendTo(elem.data("contents"));
    }
};
