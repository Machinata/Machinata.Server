

/// <summary>
/// A mandate overview.
/// TODO: refactor this out to CSAM namespace.
/// </summary>
Machinata.Dashboard.Widgets.Types["mandate"] = {
    init: function (dashboard, elem, config) {
        // Tools
        Machinata.Dashboard.Widgets.addLinkTool(elem, config.settings["URL"], null, config.settings["Hint"]);
        elem.addClass("type-metrics");
        elem.addClass("option-grid-topborder");
        // Init
        var topElem = $("<div class='top'></div>");
        var bottomElem = $("<div class='bottom'></div>");
        // Content
        if (!String.isNullOrEmpty(config.settings["Title"])) topElem.append($("<h3 class='title'></h3>").text(config.settings["Title"]));
        if (!String.isNullOrEmpty(config.settings["Mandate"])) topElem.append($("<h2 class='subtitle ui-currency bb-currency' data-format='autoscale'></h2>").text(config.settings["Mandate"]["TotalAssets"]));
        if (!String.isNullOrEmpty(config.settings["Mandate"])) topElem.append($("<p class=''></p>").text("Total assets, "+config.settings["Mandate"]["Currency"]));
        //if (!String.isNullOrEmpty(config.settings["Mandate"])) topElem.append($("<p class=''></p>").text(config.settings["Mandate"]["Type"]));
        // Grid
        var metaGrid = Machinata.Dashboard.Widgets.Types["metrics"].buildItemsGrid(dashboard, elem, config, 2);
        // Layout
        bottomElem.append(metaGrid);
        elem.data("contents").append(topElem);
        elem.data("contents").append(bottomElem);
    },
    supportedSizes: function (config) {
        return "large-square,small-tall";
    },
};

