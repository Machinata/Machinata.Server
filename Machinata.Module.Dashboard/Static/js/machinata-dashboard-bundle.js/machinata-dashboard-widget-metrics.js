


/// <summary>
/// A metrics widget with a grid of key values.
/// </summary>
Machinata.Dashboard.Widgets.Types["metrics"] = {
    init: function (dashboard, elem, config) {
        // Init

        // Tools

        // Grid
        var metaGrid = Machinata.Dashboard.Widgets.Types["metrics"].buildItemsGrid(dashboard, elem, config, 3);
        elem.data("contents").append(metaGrid);
    },
    buildItemsGrid: function (dashboard, elem, config, numColumns, numRows) {
        if (config.settings["SmallValues"] == true) elem.addClass("option-small-values");
        var metaGrid = $("<div class='items'></div>");
        if (numColumns != null) metaGrid.addClass("option-columns-" + numColumns);
        if (numRows != null) metaGrid.addClass("option-rows-" + numRows);
        var itemsConfig = config.settings["Items"];
        var row = null;
        for (var i = 0; i < itemsConfig.length; i++) {
            if (i % numColumns == 0) {
                if (row != null) metaGrid.append(row);
                row = $("<div class='group'></div>");
            }
            var itemConfig = itemsConfig[i];
            var item = $("<div class='item'></div>");
            item.addClass("mod1-" + (i % 1));
            item.addClass("mod2-" + (i % 2));
            item.addClass("mod3-" + (i % 3));
            item.addClass("mod4-" + (i % 4));
            item.append($("<div class='title'></div>").text(itemConfig["Title"]));
            item.append($("<div class='value ui-number'></div>").text(itemConfig["Value"]));
            if (itemConfig["ValueSymbol"] != null) item.find(".value").append($("<span class='ui-symbol'>" + itemConfig["ValueSymbol"] + "</span>"));
            var subvalue = $("<div class='subvalue'></div>").text(itemConfig["SubValue"]);
            var diffvalue = $("<div class='deltavalue'></div>").text(itemConfig["DeltaValue"]);
            if (itemConfig["Alert"] == true) item.append($("<div class='alert'></div>"));
            if (itemConfig["Alert"] == true) item.append($("<div class='alert-icon icon-csam-fn-small-alert1'></div>"));
            if (!String.isNullOrEmpty(itemConfig["DeltaIcon"])) diffvalue.append($("<div class='icon'></div>").addClass(itemConfig["DeltaIcon"]));
            if (!String.isNullOrEmpty(itemConfig["DeltaColor"])) diffvalue.addClass("option-" + itemConfig["DeltaColor"]);
            subvalue.append(diffvalue);
            if (itemConfig["SubValue"] != null) item.append(subvalue);
            row.append(item);
        }
        metaGrid.append(row);
        return metaGrid;
    }
};