
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Courses.Model;

namespace Machinata.Module.Finance.Handler {


    public class CoursesAdminHandler : AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("courses");
        }

        #endregion

        #region Menu Item
               
        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "course",
                Path = "/admin/courses",
                Title = "{text.courses}",
                Sort = "500"
            });
        }

        #endregion

        [RequestHandler("/admin/courses")]
        public void Default() {
            // Menu items
            var menuItems = PageTemplate.Cache.FindAll(this.Template.Package, "admin/courses/menu/menu.item.", this.TemplateExtension);
            this.Template.InsertTemplates("courses.menu-items", menuItems);
            var courses = this.DB.Courses()
                .Include(nameof(Module.Courses.Model.Course.Title))
                .Include(nameof(Module.Courses.Model.Course.Type))
                .Where(c => !c.Archived);
            var coursesPaged = this.Template.Paginate(courses, this);

            this.Template.InsertEntityList(
                    variableName: "courses",
                    entities: coursesPaged.ToList(),
                    link: "/admin/courses/course/{entity.public-id}");


            // Upcoming Classes
            var upcomingClasses = this.DB.Classes()
                .Include(nameof(Module.Courses.Model.Class.Title))
                .ToList() // todo doesnt scale
                .Where(c => c.TimeRange.Start >= DateTime.UtcNow)
                .OrderBy(c => c.TimeRange.Start).Take(5);
            this.Template.InsertCards("upcoming-classes", upcomingClasses.AsQueryable());

            // Navigation
            this.Navigation.Add("courses");

        }
        
        [RequestHandler("/admin/courses/list")]
        public void CoursesList() {
            // Courses
            var courses = this.DB.Courses()
                .Include(nameof(Module.Courses.Model.Course.Type))
                .Include(nameof(Courses.Model.Course.Title));
            var coursesPaged = this.Template.Paginate(courses, this);
            this.Template.InsertEntityList(
                    variableName: "courses",
                    entities: coursesPaged.ToList(),
                    link: "/admin/courses/course/{entity.public-id}");

            // Navigation
            this.Navigation.Add("courses");
            this.Navigation.Add("list");


        }

        [RequestHandler("/admin/courses/course/{publicId}")]
        public void Course(string publicId) {

            // Course
            var course = this.DB.Courses()
                .Include(nameof(Courses.Model.Course.Classes) + "." + nameof(Courses.Model.Class.Title))
                .Include(nameof(Courses.Model.Course.Description))
                .Include(nameof(Courses.Model.Course.Title))
                .GetByPublicId(publicId);

            // Variables
            this.Template.InsertPropertyList(
                    variableName: "entity",
                    entity: course,
                    form: new FormBuilder(Forms.Admin.VIEW),
                    loadFirstLevelReferences: true
                    );
            this.Template.InsertVariables("entity", course);
            
            // Title
            var title = course.GetTitle(Core.Config.LocalizationAdminLanguage);
            this.Template.InsertVariable("entity.course-title", title);

            // Classes
            // Date sort in entit list not working
            // var classes = this.Template.Paginate(course.Classes.ToList().AsQueryable(), this, nameof(Courses.Model.Class.Start));
            this.Template.InsertEntityList(
                  variableName: "classes",
                  entities: course.Classes.ToList().OrderBy(c=>c.Start).AsQueryable(),
                  link: "{page.navigation.current-path}/class/{entity.public-id}"
                  );

            // Navigation
            this.Navigation.Add("courses");
            this.Navigation.Add("course/" + publicId, "{text.course}" + ": " + course.GetTitle(this.Language));

        }

        [RequestHandler("/admin/courses/create")]
        public void Create() {
            this.RequireWriteARN();

            // Course
            var course = new Course();
         
            // Form
            this.Template.InsertForm(
                 form: new FormBuilder(Forms.Admin.CREATE),
                  variableName: "form",
                  entity: course,
                  apiCall: "/api/admin/courses/create",
                  onSuccess: "/admin/courses/course/{course.public-id}"
                  );

            this.Navigation.Add("courses");
            this.Navigation.Add("create");

        }

        [RequestHandler("/admin/courses/course/{publicId}/class/{classId}")]
        public void Class(string publicId, string classId) {

            // Course
            var course = this.DB.Courses()
                .Include(nameof(Courses.Model.Course.Classes) + "." + nameof(Courses.Model.Class.Title))
                .Include(nameof(Courses.Model.Course.Description))
                .Include(nameof(Courses.Model.Course.Title))
                .GetByPublicId(publicId);

            // Class
            var classObject = course.Classes.AsQueryable().GetByPublicId(classId);


            // Variables
            this.Template.InsertPropertyList(
                    variableName: "entity",
                    entity: classObject,
                    form: new FormBuilder(Forms.Admin.VIEW)
                    );
            this.Template.InsertVariables("entity", classObject);

            // Title
            var title = classObject.GetTitle(Core.Config.LocalizationAdminLanguage);
            this.Template.InsertVariable("entity.class-title", title);

            // Navigation
            this.Navigation.Add("courses");
            this.Navigation.Add("course/" + publicId, "{text.course}" + ": " + course.GetTitle(this.Language));
            this.Navigation.Add("class/" + classId, "{text.class}" + ": " + classObject.GetTitle(this.Language));


        }

        [RequestHandler("/admin/courses/course/{publicId}/add-class")]
        public void CreateClass(string publicId) {
            // Course
            var course  = this.DB.Courses()
                .Include(nameof(Courses.Model.Course.Classes) + "." + nameof(Courses.Model.Class.Title))
                .Include(nameof(Courses.Model.Course.Description))
                .Include(nameof(Courses.Model.Course.Title))
            .GetByPublicId(publicId);

            var entity = new Class();

            // Form
            this.Template.InsertForm(
                 form: new FormBuilder(Forms.Admin.CREATE),
                  variableName: "form",
                  entity: entity,
                  apiCall: "/api/admin/courses/course/" + publicId + "/add-class",
                  onSuccess: "{page.navigation.prev-path}/class/{class.public-id}"
            );

            this.Navigation.Add("courses");
            this.Navigation.Add("course/" + publicId, "{text.course}" + ": " + course.GetTitle(this.Language));
            this.Navigation.Add("add-class");

        }


        [RequestHandler("/admin/courses/course/{publicId}/edit")]
        public void CourseEdit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Courses()
                .Include(nameof(Courses.Model.Course.Classes) + "." + nameof(Courses.Model.Class.Title))
                .Include(nameof(Courses.Model.Course.Description))
                .Include(nameof(Courses.Model.Course.Title))
                .GetByPublicId(publicId);

            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: "/api/admin/courses/course/" + publicId + "/edit",
                onSuccess: "{page.navigation.prev-path}");

            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("courses");
            this.Navigation.Add("course/" + entity.PublicId, "{text.course}" + ": " + entity.GetTitle(this.Language));
            this.Navigation.Add("edit");

        }

        [RequestHandler("/admin/courses/course/{courseId}/class/{publicId}/edit")]
        public void ClassEdit(string courseId, string publicId) {
            this.RequireWriteARN();

            // Course
            var course = this.DB.Courses()
                .Include(nameof(Courses.Model.Course.Classes))
                .GetByPublicId(courseId);

            // Class
            var classObject = course.Classes.AsQueryable().GetByPublicId(publicId);

            this.Template.InsertForm(
                variableName: "form",
                entity: classObject,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: "/api/admin/courses/class/" + publicId + "/edit",
                onSuccess: "{page.navigation.prev-path}");

            this.Template.InsertVariables("entity", classObject);

            // Navigation
            this.Navigation.Add("courses");
            this.Navigation.Add("course/" + course.PublicId, "{text.course}" + ": " + course.GetTitle(this.Language));
            this.Navigation.Add("class/" + publicId, "{text.class}" + ": " + classObject.GetTitle(this.Language));
            this.Navigation.Add("edit");

        }


        [RequestHandler("/admin/courses/timeline")]
        public void Timeline() {

            // Navigation
            this.Navigation.Add("courses");
            this.Navigation.Add("timeline");
          

        }





    }
}
