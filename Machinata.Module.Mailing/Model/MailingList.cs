using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Module.Mailing.Data;
using System.Web;
using Machinata.Core.Util;
using System.Linq.Expressions;
using Machinata.Module.Mailing.ContactProvider;

namespace Machinata.Module.Mailing.Model {

    [Serializable()]
    [ModelClass]
    public partial class MailingList : ModelObject {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();


        #endregion

        #region Constants /////////////////////////////////////////////////////////////////////////



        public enum Types : short {
            Contact = 0,
            Users = 10,
            Businesses = 20,

            // MailingContactProvider:
            Custom = 30
        }

        public enum ImportAction : short {
            Add = 0,
            Reset = 10,
            Update = 20
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public MailingList() {
            this.Contacts = new List<MailingContact>();
            this.Properties = new Properties();
            this.ContactProviderSettings = new Properties();
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Required]
        [MinLength(3)]
        [MaxLength(100)]
        public string Name { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Required]
        public Types Type { get; set; }

        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Frontend.VIEW)]
        public Properties Properties { get; set; }


        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        public bool SendNotification { get; set; }

        /// <summary>
        /// Visibility to the frontend
        /// </summary>
        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public bool Public { get; set; }


        [Column]
        [FormBuilder(Forms.Frontend.JSON)]
        [MaxLength(256)]
        public string ContactProviderType { get; set; }

        [Column]
        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public Properties ContactProviderSettings { get; set; }

        #endregion

        #region Public Navigation Properties /////////////////////////////////////////////////////        
        /// <summary>
        /// Contacts: don't read directly use GetContacts()
        /// </summary>
        /// <value>
        /// The contacts.
        /// </value>
        [Column]
        public ICollection<MailingContact> Contacts { get; set; }

        [Column]
        public ICollection<Mailing> Mailings { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        public IEnumerable<MailingContact> GetContacts(ModelContext db) {
            IEnumerable<MailingContact> contacts = null;
            if (this.Type == Types.Contact) {
                contacts = db.MailingContacts().Include(nameof(MailingContact.MailingLists)).Where(c => c.MailingLists.Select(l => l.Id).Contains(this.Id));
            } else if (this.Type == Types.Businesses) {
                contacts = MailingContact.GetContactsForBusinesses(GetActiveCustomerBusinesses(db).ToList().AsQueryable());
            } else if (this.Type == Types.Users) {
                contacts = MailingContact.GetContactsForUsers(GetActiveCustomerBusinesses(db).SelectMany(b => b.Users.Where(u=>u.Enabled == true)));
            } else if (this.Type == Types.Custom) {
                if (this.ContactProvider != null) {
                    contacts = this.ContactProvider.GetContacts(db);
                }else {
                    contacts = new List<MailingContact>().AsQueryable();
                }
            } else {
                throw new BackendException("mailing-list-error", $"Not implemented for {this.Type}");
            }
            return contacts; // show all ! even duplicates c.Where(c => c.Email != null).Distinct(new EmailEqualityComparer());
        }

        public MailingContactProvider ContactProvider
        {
            get
            {
                if (this.Type == Types.Custom && string.IsNullOrEmpty(this.ContactProviderType) == false) {
                    var provider = MailingContactProvider.GetByName(this.ContactProviderType);
                    provider.List = this;
                    return provider;                    
                } else {
                    return null;
                }
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        public string CustomName
        {
            get
            {
                if (this.ContactProvider != null) {
                    return MailingContactProvider.GetName(this.ContactProvider.GetType());
                }
                return null;
            }
        }

        //public int GetContactsCount(ModelContext db) {
        //    IEnumerable<MailingContact> contacts = null;
        //    if (this.Type == Types.Contact) {
        //        this.Include(nameof(this.Contacts));
        //        return this.Contacts.Count();
        //    } else if (this.Type == Types.Businesses) {
        //       return MailingContact.GetContactsForBusinesses(GetActiveCustomerBusinesses(db)).Count();
        //    } else if (this.Type == Types.Users) {
        //        contacts = MailingContact.GetContactsForUsers(GetActiveCustomerBusinesses(db).SelectMany(b => b.Users));
        //    } else {
        //        throw new BackendException("mailing-list-error", $"Not implemented for {this.Type}");
        //    }
        //    return -1;
        //}

        private static IQueryable<Business> GetActiveCustomerBusinesses(ModelContext db) {
            //var ownerBusiness = db.OwnerBusiness(throwException);
            return db.ActiveBusinesses().Where(b => b.IsOwner == false);
        }

        /// <summary>
        /// Creates an new MailingContact and adds it to the list
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="company">The company.</param>
        /// <param name="address">The address.</param>
        /// <param name="email">The email.</param>
        /// <param name="name">The name.</param>
        /// <param name="language">The language.</param>
        public MailingContact AddNewContact(ModelContext db, string company, string address, string email, string name, string language) {
            var contact = new MailingContact();
            contact.Source = MailingUnsubscription.Targets.Email;
            contact.Language = language;
            contact.Email = email;
            contact.Name = name;
            contact.Properties[MailingContact.PROPERTIES_KEY_COMPANY] = company;
            contact.Properties[MailingContact.PROPERTIES_KEY_ADDRESS] = address;
            db.MailingContacts().Add(contact);
            contact.MailingLists.Add(this);
            return contact;
        }

        /// <summary>
        /// Searches for existing MailingContact, creates new if needed, and adds it to the list
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="company">The company.</param>
        /// <param name="address">The address.</param>
        /// <param name="email">The email.</param>
        /// <param name="name">The name.</param>
        /// <param name="language">The language.</param>
      
        /// <exception cref="BackendException">subscribe-error;This email is already subscribed.</exception>
        public void SubscribeToList(ModelContext db, string company, string address, string email, string name, string language) {
            var contact = MailingContact.FindContact(db, email);
            if (contact == null) {
                contact = this.AddNewContact(db, company, address, email, name, language);
            } else {
                contact.Include(nameof(MailingContact.MailingLists));
                if (contact.MailingLists.Contains(this)) {
                    throw new BackendException("subscribe-error", "This email is already subscribed.");
                }
                contact.MailingLists.Add(this);
            }

            // Send Notification
            if (this.SendNotification) {
                var message = new StringBuilder();
                message.AppendLine("Email: " + email);
                message.AppendLine("Name: " + name);
                message.AppendLine("Company: " + company);
                message.AppendLine("Language: " + language);
                MessageCenter.SendMessageToAdminEmail($"New Subscription to Mailing List {this.Name}", message.ToString(), "Machinata.Module.Mailing");
            }

        }

        /// <summary>
        /// Adds the contact to the list. If list already contains the email, the existing contact will be updated
        /// </summary>
        /// <param name="db">The database.</param>
        /// <param name="contact">The contact.</param>
        public void SubscribeToList(ModelContext db, MailingContact contact) {

            var existingContact = this.Contacts.FirstOrDefault(c => c.EmailHash == contact.EmailHash);

            if (existingContact != null) {
                contact.CopyValuesTo(existingContact, new FormBuilder(Forms.Admin.EDIT), true);
            } else {
                contact.MailingLists.Add(this);
            }
            // Send Notification
            if (this.SendNotification) {
                MessageCenter.SendMessageToAdminEmail($"New Subscription to Mailing List {this.Name}", contact.GetObjectValueSummary(new FormBuilder(Forms.Admin.VIEW)), "Machinata.Module.Mailing");
            }
           

        }


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Override Methods ///////////////////////////////////////////////////////////

        public override string ToString() {
            if (!string.IsNullOrEmpty(this.Name)) return this.Name;
            else return base.ToString();
        }
          

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public static MailingList GetDefaultMailingList(ModelContext db) {
            return db.MailingLists().SingleOrDefault(ml => ml.Name == "Newsletter");
        }

        public static MailingContact AddToDefaultMailingList(ModelContext db, string name, string email, string business,string address, string title) {
            var list = GetDefaultMailingList(db);

            var contact = new MailingContact();
            contact.Email = email;
            contact.Name = name;
            contact.Properties[MailingContact.PROPERTIES_KEY_COMPANY] = business;
            contact.Properties[MailingContact.PROPERTIES_KEY_ADDRESS] = address;
            contact.Properties[MailingContact.PROPERTIES_KEY_TITLE] = title;

            list.Contacts.Add(contact);

            return contact;

        }

        /// <summary>
        /// Imports the contacts. Gets called by reflection
        /// </summary>
        /// <typeparam name="M"></typeparam>
        /// <param name="importAction">The import action.</param>
        /// <param name="allowFormulas">if set to <c>true</c> [allow formulas].</param>
        /// <param name="form">The form.</param>
        /// <param name="fileData">The file data.</param>
        /// <returns></returns>
        public StringBuilder ImportContacts<M>(MailingList.ImportAction importAction, bool allowFormulas, FormBuilder form, byte [] fileData) where M: MailingContactFormat {
            var log = new StringBuilder();
            var row = 1;
            var duplicates = 0;
         
            if (importAction == ImportAction.Reset) {
                foreach (var contact in this.Contacts.ToList()) {
                    this.Contacts.Remove(contact);
                }
            }
            IEnumerable<M> mailingContacts = null;
            // Import Basic
            mailingContacts = Core.Reporting.ExportService.ImportXLSX<M>(
                db: this.Context,
                content: fileData,
                form: form,
                importRow: null,
                log: log,
                allowFormulas: allowFormulas);

            try {
                // Add imported Contacts to the list
                foreach (var contactFormat in mailingContacts) {

                    var contact = contactFormat.ConvertToMailingContact();
                    contact.Validate();
                    
                    // if Action: Update
                    if (importAction == ImportAction.Update) {

                        // We have the same contact already in list (TODO: what is same, what is the exact action)
                        var sameContact = this.Contacts.FirstOrDefault(c => c.Email == contact.Email && c.Name == contact.Name);
                        if (sameContact != null) {
                            // Log Duplicates Count

                            log.AppendLine($"-------------------------------------------");
                            log.AppendLine($"Overriding duplicated contact:");
                            log.AppendLine($"old:");
                            log.AppendLine($"{sameContact.GetObjectValueSummary(form)}");
                            log.AppendLine($"new:");
                            log.AppendLine($"{contact.GetObjectValueSummary(form)}");
                            log.AppendLine($"-------------------------------------------");
                            contact.CopyValuesTo(sameContact, form);
                            duplicates++;
                        } else {
                            this.Contacts.Add(contact);
                        }

                    } else if (importAction == ImportAction.Add || importAction == ImportAction.Reset) {
                        this.Contacts.Add(contact);
                    }

                    // Book keeping
                    row++;
                }
            } catch (Exception e) {
                throw new BackendException("import-error" , $"Row: {row}", e);
            }

            // Log Duplicates Count
            log.AppendLine($"Duplicates added {duplicates}");

            return log;

           
        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override void Validate() {
            base.Validate();

        
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextMailingListExtenions {
        [ModelSet]
        public static DbSet<MailingList> MailingLists(this Core.Model.ModelContext context) {
            return context.Set<MailingList>();
        }
    }

    public static class IQueryableMailingListExtensions {
        public static MailingList DefaultList(this DbSet<MailingList> query, bool throwException) {

            if (string.IsNullOrEmpty(Machinata.Module.Mailing.Config.DefaultSubscriptionMailingList)) {
                if (throwException) {
                    throw new BackendException("mailing-list-error", "No default mailing list defined");
                }
            }
            var list = query.GetByPublicId(Machinata.Module.Mailing.Config.DefaultSubscriptionMailingList, throwException);
            return list;
        }

        public static IQueryable<MailingList> Public<T>(this IQueryable<MailingList> query) {
            return query.Where(e => e.Public == true);
        }




    }

    #endregion



}
