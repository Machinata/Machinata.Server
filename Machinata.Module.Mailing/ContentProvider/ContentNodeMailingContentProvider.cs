using Machinata.Core.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinata.Core.Model;
using Machinata.Module.Mailing.Model;

namespace Machinata.Module.Mailing.ContentProvider {
    public class ContentNodeMailingContentProvider : MailingContentProvider {

        public override bool AutogenerateContent
        {
            get
            {
                return true;
            }
        }

        public virtual bool IsLayoutedContent() {
            return false;
        }

        public override string GetContent(ContentType contentType) {
            //var template = this.ParentTemplate.LoadTemplate("mailing.content-node"); v1

            // var template = new EmailTemplate(this.DB ,"mailing.content-node"); v2

            var parentEmailTemplate = this.ParentTemplate as EmailTemplate;

            var template = new EmailTemplate("mailing.content-node", this.DB, this.ParentTemplate.Language, this.ParentTemplate.Package, parentEmailTemplate.EmailCSSBundle());

            // Email Content from CMS (Snapshot)

            ContentNode content = GetContentNode(contentType);
            if (IsLayoutedContent()) {
                template.InsertLayoutedContent(
                    variableName: "content",
                    cmsPath: content.Path
                );
            } else {
                template.InsertContent(
                    variableName: "content",
                    node: content
                );
            }
            return template.Data.ToString();
        }

      

        public override void Save() {
            string snapshotPath = GetSnapshotPath();
            DeleteOldSnapshot(snapshotPath);
            this.DB.SaveChanges();

            if (this.Mailing.Content != null) {
                var newSnapShot = this.Mailing.Content.Duplicate(this.DB, snapshotPath, false);
                this.Mailing.ContentSnapshot = newSnapShot;
            }
        }

    }
}
