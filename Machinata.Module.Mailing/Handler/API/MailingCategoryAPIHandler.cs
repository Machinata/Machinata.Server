
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;


namespace Machinata.Module.Mailing.Handler {


    public class MailingCategoryAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Mailing.Model.MailingCategory> {
        
        [RequestHandler("/api/admin/mailing/category/{publicId}/edit")]
        public void Edit(string publicId) {
            this.CRUDEdit(publicId);
        }

        [RequestHandler("/api/admin/mailing/categories/create")]
        public void Create() {
            this.CRUDCreate();
        }

        [RequestHandler("/api/admin/mailing/category/{publicId}/delete")]
        public void Delete(string publicId) {
            this.CRUDDelete(publicId);
        }

    }
}
