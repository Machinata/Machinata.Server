using Machinata.Core.Builder;
using Machinata.Core.Handler;
using Machinata.Core.Messaging.Providers;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Module.Mailing.Model;
using System.Linq;
using Machinata.Core.Templates;
using System;
using Machinata.Module.Mailing.ContentProvider;
using Machinata.Module.Mailing.Tasks;
using System.Linq.Expressions;
using System.Text;

namespace Machinata.Module.Mailing.Handler.Page {

    public class MailingsAdminPageHandler : Admin.Handler.AdminPageTemplateHandler {

        #region Menu Item

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "new-post",
                Path = "/admin/mailing",
                Title = "{text.mailing}",
                Sort = "500"
            });
        }

        #endregion

        [RequestHandler("/admin/mailing/mailings")]
        public void Mailings() {
            var entities = this.DB.Mailings()
                .Include(nameof(Module.Mailing.Model.Mailing.Category))
                .ToList();
            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: entities ,
                form: new FormBuilder(Forms.Admin.LISTING).Include(nameof(Model.Mailing.Created)),
                link: "/admin/mailing/mailings/mailing/{entity.public-id}"
                
                );
          
            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("mailings");
        }

        [RequestHandler("/admin/mailing/mailings/create")]
        public void CreateMailing() {
            
            this.Template.InsertForm(
                variableName: "form",
                entity:new Mailing.Model.Mailing(),
                form: new FormBuilder(Forms.Admin.CREATE),
                apiCall: "/api/admin/mailing/mailings/create",
                onSuccess: "/admin/mailing/mailings/mailing/{mailing.public-id}/edit"
                );

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("mailings");
            Navigation.Add("create");
        }

        [RequestHandler("/admin/mailing/mailings/mailing/{publicId}")]
        public void Mailing(string publicId) {

            // Init
            var entity = this.DB.Mailings()
                .Include(nameof(Model.Mailing.Lists))
                .Include(nameof(Model.Mailing.Content))
                .Include(nameof(Model.Mailing.ContentSnapshot))
                .GetByPublicId(publicId);

            // Content Preperation (no preview needed anymore)
            entity.PrepareContent(this.DB);

            // Status
            var statusMessage = "Ready to send";
            try {
                entity.CheckReadyToSend(this.DB, true, false);
            }catch(Exception e) {
                statusMessage = e.Message;
            }
            this.Template.InsertVariable("status-message", statusMessage);

            // Property list
            var viewForm = new FormBuilder(Forms.Admin.VIEW);
            if (entity.HasDefaultContentProvider) {
                viewForm = viewForm.Exclude(nameof(entity.ContentProvider));
            }
            viewForm.Custom("SendStatus", "SendStatus", "text", statusMessage);

            this.Template.InsertPropertyList(
                variableName: "entity",
                entity: entity,
                form: viewForm,
                loadFirstLevelReferences: true
             );

            // Lists
            this.Template.InsertEntityList(
               variableName: "lists",
               entities: entity.Lists.AsQueryable(),
               link: "/admin/mailing/lists/list/{entity.public-id}");

            // Card
            this.Template.InsertCard("card", entity.VisualCard());

            // Ready/Tools
            var readyToSend = entity.CheckReadyToSend(this.DB, false, false);
            this.Template.InsertVariable("ready-to-send", readyToSend);
            

            // Pausable
            this.Template.InsertVariable("is-pauseable", entity.IsPauseable(this.DB));

            // Resumeable
            this.Template.InsertVariable("is-resumeable", entity.IsResumeable(this.DB));

            // Failed
            this.Template.InsertVariable("is-failed", entity.IsFailed(this.DB));

            // Show generate content
            bool autogenerateContent = entity.GetContentProvider().AutogenerateContent;
            this.Template.InsertVariable("show-generate-content", autogenerateContent == false && entity.Status == TaskLog.BatchTaskDisplayStatuses.Created);

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Email current user
            this.Template.InsertVariable("current-user.email", this.User.Email);

            var contactsCount = entity.GetContacts(this.DB).Count();

            this.Template.InsertVariable("entity.contacts-count", contactsCount);


            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("mailings");
            Navigation.Add("mailing/" + entity.PublicId, entity.ToString());
            
        }

     
        [RequestHandler("/admin/mailing/mailings/mailing/{publicId}/edit")]
        public void MailingEdit(string publicId) {

            var entity = this.DB.Mailings().GetByPublicId(publicId);

            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/mailing/mailing/{publicId}/edit",
                onSuccess: $"/admin/mailing/mailings/mailing/{publicId}"
            );

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("mailings");
            Navigation.Add("mailing/" + entity.PublicId, entity.ToString());
            Navigation.Add("edit");

        }

        [RequestHandler("/admin/mailing/mailings/mailing/{publicId}/validate")]
        public void Validate(string publicId) {

            var entity = this.DB.Mailings().GetByPublicId(publicId);
            var log = new StringBuilder();
            // Validate contacts all data
            foreach (var contact in entity.GetContacts(this.DB)) {
                var template = entity.CreateMailingTemplate(this, contact);
                template.Compile();
                var emptyVars = template.DiscoverVariables();
                if (emptyVars.Count() > 0) {
                    log.AppendLine(contact.Name + ": " + string.Join(",", emptyVars));
                }
            }

            this.Template.InsertVariable("form", log);

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("mailings");
            Navigation.Add("mailing/" + entity.PublicId, entity.ToString());
            Navigation.Add("validate");

        }

        [RequestHandler("/admin/mailing/mailings/mailing/{publicId}/edit-lists")]
        public void EditContacts(string publicId) {

            var entity = this.DB.Mailings().Include(nameof(Model.Mailing.Lists)).GetByPublicId(publicId);

            // MailingContacts
            this.Template.InsertSelectionList(
                variableName: "entity-list",
                entities: this.DB.MailingLists(),
                selectedEntities: entity.Lists.AsQueryable(),
                form: new FormBuilder(Forms.Admin.SELECTION),
                selectionAPICall: $"/api/admin/mailing/mailing/{publicId}/toggle-list/" + "{entity.public-id}"

                );

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("mailings");
            Navigation.Add("mailing/" + entity.PublicId, entity.ToString());
            Navigation.Add("lists");

        }


        [RequestHandler("/admin/mailing/mailings/mailing/{publicId}/preview")]
        public void Preview(string publicId) {
            
            // Mailing
            var entity = this.DB.Mailings()
                .Include(nameof(Model.Mailing.Lists))
                .Include(nameof(Model.Mailing.Content))
                .GetByPublicId(publicId);

            // Properties
            this.Template.InsertPropertyList(
                  variableName: "entity",
                  entity: entity,
                  form: new FormBuilder("preview"),
                  showCard: false
               );

            // Variables
            this.Template.InsertVariables("entity", entity);

            // Navigation
            Navigation.Add("mailing");
            Navigation.Add("mailings");
            Navigation.Add("mailing/" + entity.PublicId, entity.ToString());
            Navigation.Add("preview");

        }

        [RequestHandler("/admin/mailing/mailings/mailing/{publicId}/preview-content")]
        public void PreviewContent(string publicId) {

            // Snapshot content if Autogenerate activated
            lock (_previewContentLock) {
                // Mailing
                var entity = this.DB.Mailings()
                .Include(nameof(Model.Mailing.Lists))
                .Include(nameof(Model.Mailing.Content))
                .GetByPublicId(publicId);

                // Preview Contact -> Try first one oder mockup
                var contact = entity.GetContacts(this.DB).FirstOrDefault();
                if (contact == null) {
                    contact = MailingContact.CreateTest();
                }

                entity.PrepareContent(this.DB);
         
                // Content
                var template = entity.CreateMailingTemplate(this, contact);
                template.Compile();
                this.Template.InsertVariableXMLDecoded("preview", template.Content);
            }

        }

        private static object _previewContentLock = new object();

        [RequestHandler("/admin/mailing/mailings/mailing/{publicId}/processed")]
        public void ProcessedContacts(string publicId) {

            var entity = this.DB.Mailings()
                .GetByPublicId(publicId);

            var allContacts = entity.GetContacts(this.DB).ToList();

            var task = MailingTask.CreateMailingTask(this.DB);
            var processedItems =  task.GetProcessedTypedItems(entity.PublicId, allContacts);
            var count = processedItems.Count();

            var pagedItems = this.Template.Paginate(processedItems.AsQueryable(), this);
            this.Template.InsertEntityList(
                variableName: "entities",
                entities: pagedItems,
                form: new FormBuilder(Forms.Admin.LISTING),
                link: null
            );

            // Vars
            this.Template.InsertVariable("count", count);
            
            // Navigation
            this.Navigation.Add("mailing");
            this.Navigation.Add("mailings");
            this.Navigation.Add("mailing/" + entity.PublicId, entity.ToString());
            this.Navigation.Add("processed");

        }

        [RequestHandler("/admin/mailing/mailings/mailing/{publicId}/failed")]
        public void FailedContacts(string publicId) {

            var entity = this.DB.Mailings()
                .GetByPublicId(publicId);

            var allContacts = entity.GetContacts(this.DB).ToList();

            var task = MailingTask.CreateMailingTask(this.DB);
            var failedItems = task.GetFailedTypedItems(entity.PublicId, allContacts);
            var count = failedItems.Count();

            var pagedItems = this.Template.Paginate(failedItems.AsQueryable(), this);
            this.Template.InsertEntityList(
                variableName: "entities",
                entities: pagedItems,
                form: new FormBuilder(Forms.Admin.LISTING),
                link: null
            );

            // Vars
            this.Template.InsertVariable("count", count);
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("mailing");
            this.Navigation.Add("mailings");
            this.Navigation.Add("mailing/" + entity.PublicId, entity.ToString());
            this.Navigation.Add("failed");

        }

        [RequestHandler("/admin/mailing/mailings/mailing/{publicId}/unsubscribed")]
        public void Unsubscribed(string publicId) {

            var entity = this.DB.Mailings()
                 .Include(nameof(Model.Mailing.Lists))
                 .Include(nameof(Model.Mailing.Category))
                 .GetByPublicId(publicId);


            // Unsubscribed Contacts
            var unsubscriptions = UnsubscribeService.GetEmailUnsubscriptions(this.DB, MailingUnsubscription.Categories.Mailing, entity.Category.UnsubscribeId).Select(u => u.Identifier).ToList();
            var unsubscribedContacts = entity.GetContacts(this.DB).Where(a => unsubscriptions.Contains(a.EmailHash));
            var pagedItems = this.Template.Paginate(unsubscribedContacts.AsQueryable(), this);
            this.Template.InsertEntityList(
                variableName: "entities",
                entities: pagedItems,
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/mailing/contacts/contact/{entity.public-id}"
                );

            // Navigation
            this.Navigation.Add("mailing");
            this.Navigation.Add("mailings");
            this.Navigation.Add("mailing/" + entity.PublicId, entity.ToString());
            this.Navigation.Add("unsubscribed", "{text.unsubscribed-contacts}");

        }

        /// <summary>
        /// Contacts which will be messaging to
        /// </summary>
        /// <param name="publicId"></param>
        [RequestHandler("/admin/mailing/mailings/mailing/{publicId}/contacts")]
        public void Contacts(string publicId) {

            var entity = this.DB.Mailings()
                 .Include(nameof(Model.Mailing.Lists))
                 .Include(nameof(Model.Mailing.Category))
                 .GetByPublicId(publicId);


            // Unsubscribed Contacts
            var contacts = entity.GetContacts(this.DB);
            var pagedItems = this.Template.Paginate(contacts.AsQueryable(), this);
            this.Template.InsertEntityList(
                variableName: "entities",
                entities: pagedItems,
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/mailing/contacts/contact/{entity.public-id}"
                );

            // Navigation
            this.Navigation.Add("mailing");
            this.Navigation.Add("mailings");
            this.Navigation.Add("mailing/" + entity.PublicId, entity.ToString());
            this.Navigation.Add("contacts");

        }


    }
}
