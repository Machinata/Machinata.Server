using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Mailing
{
    public class Config
    {

        public readonly static string DefaultSubscriptionMailingList = Core.Config.Dynamic.String("DefaultSubscriptionMailingList", false);

        public static bool MailingSubscribeCategoriesEnabled = Core.Config.GetBoolSetting("MailingSubscribeCategoriesEnabled");

        public static int MailingTaskBatchSize = Core.Config.GetIntSetting("MailingTaskBatchSize");

        // Optional: Only used to override the Mailing Email Package
        public readonly static string MailingEmailPackage = Core.Config.GetStringSetting("MailingEmailPackage", null);
        // Optional: Only used to override the Mailing CSS Bundle
        public readonly static string MailingEmailCSSBundle = Core.Config.GetStringSetting("MailingEmailCSSBundle", null);

         


    }
}
