using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Machinata.Core.Handler {


    public class ScreenshotStaticHandler : Core.Handler.Handler {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        #endregion

        public static string CachePath = Core.Config.CachePath + Core.Config.PathSep + "static";
        public static string CachePathScreenshots = CachePath + "-screenshots";
        public static TimeSpan DefaultCacheTime = new TimeSpan(30, 0, 0, 0); // 30 days

        [RequestHandler("/static/headless/screenshot", Core.Model.AccessPolicy.PUBLIC_ARN, null, Verbs.Get, ContentType.StaticFile)]
        public void Screenshot() {
            // Mark as static content type
            this.ContentType = ContentType.StaticFile;

            // Init
            string url = this.Params.String("url");
            bool download = this.Params.Bool("download", false);
            string width = this.Params.String("width", "1280");
            string height = this.Params.String("height", "720");
            string format = this.Params.String("format", "png");
            string awaitSelector = this.Params.String("await-selector", null);
            bool fullpage = this.Params.Bool("full-page", false);
            string urlHash = Core.Encryption.DefaultHasher.HashString(url);
            string awaitSelectorHash = awaitSelector != null ? Core.Encryption.DefaultHasher.HashString(awaitSelector) : "";
            string cachePath = CachePathScreenshots + Core.Config.PathSep + urlHash + "_" + width + "_" + height+ "_"+ awaitSelectorHash + (fullpage?"_fullpage":"") + "." + format;
            string size = this.Params.String("size", null);
            if(size == "A4") {
                width = "210"; //mm
                height = "297"; //mm
            }

            // Apply some secret variables to the final url?
            url = url.Replace("{headless-auth-secret}", Machinata.Core.Config.AuthSecretForHeadlessAccess);

            // Cache?
            bool forceCreate = Core.Config.GetBoolSetting("StaticHeadlessScreenshotCacheEnabled", true) == false;
            Core.Caching.FileCacheGenerator.CreateIfNeeded(cachePath, () => {
                // Get the screenshot
                Module.Headless.NervesScreenshot.GetScreenshot(url, format, cachePath, width, height, awaitSelector, fullpage);
            }, forceCreate);

            // Send as file or download from cache...
            if (download == true) {
                string filename = this.Params.String("filename", "Screenshot"+"."+format);
                SendFile(cachePath, DefaultCacheTime, filename);
            } else {
                SendFile(cachePath, DefaultCacheTime);
            }

        }


    }
}
