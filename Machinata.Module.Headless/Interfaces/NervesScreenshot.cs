﻿using Machinata.Core.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Machinata.Module.Headless {

    public class NervesScreenshot {

        public static string GetScreenshot(string url, string format, string path, object width, object height, string awaitSelector, bool fullpage) {

            var apiKey = Core.Config.GetStringSetting("NervesScreenshotApiAccessKey");
            var endpoint = Core.Config.GetStringSetting("NervesScreenshotEndpoint");
            var downloadURL = $"{endpoint}?key={apiKey}&format={format}&width={width}&height={height}&url={HttpUtility.UrlEncode(url)}&await-selector={HttpUtility.UrlEncode(awaitSelector)}&fullpage={fullpage.ToString().ToLower()}";

           
            var wc = new WebClient();
            wc.DownloadFile(downloadURL, path);

            return path;
        }


    }
}
