﻿using Machinata.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.Headless {
    public class NodeJS {

        public static string NodeExecutable = "node.exe";
        public static string NPMExecutable = "npm.cmd";
        public static string NodeVersion = "node-v14.15.1-win-x64";
        public static string CachePath = Core.Config.CachePath + Core.Config.PathSep + "nodejs-bin";
        public static string NPMCachePath = Core.Config.CachePath + Core.Config.PathSep + "npm-cache";
        public static string WorkspacesPath = Core.Config.CachePath + Core.Config.PathSep + "nodejs-workspace";

        private static void _cleanupOldWorkspaces() {
            try {
                var dir = new DirectoryInfo(WorkspacesPath);
                foreach (var d in dir.GetDirectories()) {
                    if (!d.Name.StartsWith(Core.Config.BuildID + "_")) {
                        d.Delete(true);
                    }
                }
            } catch (Exception e) { 
                // There are a multitude of reasons why this might fail (dir doesnt exist, locked files), typically we dont care
            }
        }

        public static string CreateWorkspace(string workspaceName, IEnumerable<string> packageDependencies) {
            // Init
            _cleanupOldWorkspaces();
            var workspacePath = WorkspacesPath + Core.Config.PathSep + Core.Config.BuildID+"_"+workspaceName;

            // Create directory
            if (!Directory.Exists(workspacePath)) {
                Directory.CreateDirectory(workspacePath);
            }

            // Skip packages if package-lock already exists (means we already did it...)
            if (File.Exists(workspacePath+Core.Config.PathSep+ "package-lock.json")) {
                return workspacePath; // already exists
            }

            // Run package dependencies
            if (packageDependencies != null) {
                foreach (var package in packageDependencies) {
                    NPMCommand("install " + package, workspacePath);
                }
            }

            return workspacePath;
        }

        public static string ExecuteFileInWorkspace(string scriptPath, string arguments) {
            return ExecuteFileInWorkspace(scriptPath, arguments, new FileInfo(scriptPath).Name, null);
        }

        public static string ExecuteFileInWorkspace(string scriptPath, string arguments, IEnumerable<string> packageDependencies) {
            return ExecuteFileInWorkspace(scriptPath, arguments, new FileInfo(scriptPath).Name, packageDependencies);
        }

        public static string ExecuteFileInWorkspace(string scriptPath, string arguments, string workspaceName) {
            return ExecuteFileInWorkspace(scriptPath, arguments, workspaceName, null);
        }

        public static string ExecuteFileInWorkspace(string scriptPath, string arguments, string workspaceName, IEnumerable<string> packageDependencies) {
            // Init
            var workspacePath = CreateWorkspace(workspaceName, packageDependencies);
            var scriptFile = new FileInfo(scriptPath).Name;
            var workspaceScriptFile = workspacePath + Core.Config.PathSep + scriptFile;


            // Copy script to project
            if (!File.Exists(workspaceScriptFile)) {
                // Install script we need
                // Only install the script if the rest was successful
                File.Copy(scriptPath, workspaceScriptFile);
            }



            return ExecuteFile(workspaceScriptFile, arguments);
        }
        
        public static string ExecuteFile(string scriptPath, string arguments = null) {

            Process proc = new Process();
            proc.StartInfo = new ProcessStartInfo(GetNodePath(), arguments == null ? scriptPath : scriptPath + " "+arguments);
            proc.StartInfo.WorkingDirectory = new FileInfo(scriptPath).Directory.FullName;


            return _runProcess(proc,"NodeJS");
        }


        public static string ExecuteScript(string script) {


            var arguments = new List<string>();
            arguments.Add("-e");
            arguments.Add("\"" + script.Replace("\"", "\\\"") + "\"");

            Process proc = new Process();
            proc.StartInfo = new ProcessStartInfo(GetNodePath(), string.Join(" ", arguments));

            

            return _runProcess(proc,"NodeJS");
        }


        public static string GetNodeDir() {
            return CachePath + Core.Config.PathSep + NodeVersion;
        }
        public static string GetNodePath() {
            return GetNodeDir() + Core.Config.PathSep + NodeExecutable;
        }
        public static string GetNPMPath() {
            return GetNodeDir() + Core.Config.PathSep + NPMExecutable;
        }


        public static string NPMCommand(string command, string workspacePath) {
            var npmCLIScriptPath = GetNodeDir() + Core.Config.PathSep + "node_modules\\npm\\bin\\npm-cli.js";

            Process proc = new Process();
            proc.StartInfo = new ProcessStartInfo(GetNPMPath(), command);
            proc.StartInfo.WorkingDirectory = new DirectoryInfo(workspacePath).FullName;
            
            return _runProcess(proc,"NPM");
        }


        public static string _runProcess(Process proc, string name) {
            // See https://stackoverflow.com/questions/4291912/process-start-how-to-get-the-output about getting both error and std output...

            _prepareNodeEnvironment();

            var output = new StringBuilder();
            var errors = new StringBuilder();

            proc.StartInfo.EnvironmentVariables["PATH"] = GetNodeDir() + ";" + proc.StartInfo.EnvironmentVariables["PATH"];
            proc.StartInfo.EnvironmentVariables["npm_config_cache"] = NPMCachePath;

            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.RedirectStandardError = true;
            proc.StartInfo.RedirectStandardOutput = true;

            proc.ErrorDataReceived += (s, e) => errors.AppendLine(e.Data); // only one can be async

            proc.Start();

            proc.BeginErrorReadLine();

            output.AppendLine(proc.StandardOutput.ReadToEnd());

            proc.WaitForExit();

         
            // Status?
            if (proc.ExitCode != 0) {
                throw new BackendException("nodejs-error",name+" threw an error: " + errors.ToString());
            }

            return output.ToString()?.Trim();
        }



        private static object _prepareNodeEnvironmentLock = new object();

        private static void _prepareNodeEnvironment() {
            lock (_prepareNodeEnvironmentLock) {
                // See if node is installed
                if (!System.IO.File.Exists(GetNodePath())) {
                    // No node, we must get our package and unzip it...
                    var zipPath = Core.Config.BinPath + Core.Config.PathSep + "libs" + Core.Config.PathSep + "nodejs" + Core.Config.PathSep + NodeVersion + ".zip";
                    var dest = GetNodeDir();
                    // Delete if the dest exists
                    if(Directory.Exists(dest)) {
                        Directory.Delete(dest);
                    }
                    // Unzip bundle contents to parent dir
                    var destParent = Directory.GetParent(dest).FullName;
                    System.IO.Compression.ZipFile.ExtractToDirectory(zipPath, destParent);
                    // Test for node.exe
                    if (!System.IO.File.Exists(GetNodePath())) {
                        throw new Exception("Could not prepare Node environment: expected "+GetNodePath() +" after unzipping bundle contents but could not find it!");
                    }
                    // All good
                }
            }
        }

    }
}
