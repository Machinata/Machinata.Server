using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Machinata.Core.Util;
using Machinata.Core.Builder;
using Machinata.Core.Model;
using Machinata.Core.Reflection;
using Machinata.Core.Templates;
using Machinata.Module.ThreeD.Model;

namespace Machinata.Module.ThreeD.Templates {

    /// <summary>
    /// Extensions for PageTemplate Variables Inserts Helper Methods
    /// </summary>
    public static class PageTemplateSceneInserts {


        public static void _insertEntityRecursively(PageTemplate template, string variableName, IEnumerable<ThreeD.Model.ThreeDEntity> allEntities, ThreeD.Model.ThreeDEntity parentEntity, int level = 0) {

            var children = allEntities.Where(e => e.ParentId == parentEntity?.Id).OrderBy(e => e.Sort);
            
            var childrenTemplates = new List<PageTemplate>();
            foreach(var entity in children) {
                // Enabled?
                if (entity.Enabled == false) continue;

                // Special template for type?
                var typeExtension = entity.Type.ToString().ToLowerInvariant();
                PageTemplate entityTemplate = null;
                try {
                    entityTemplate = template.LoadTemplate("scene.entity." + typeExtension);

                } catch {
                    entityTemplate = template.LoadTemplate("scene.entity.default");
                }

                var opacity = entity.Opacity;

                var visible = entity.Visible;

                var material = entity.Material;

                var color = entity.Color;

                var classes = entity.Classes;
                if (classes == null) classes = "";
                if (entity.Selectable == true) classes += " selectable";
                classes = classes.Trim();

                // https://aframe.io/docs/1.0.0/core/mixins.html
                string mixins = "";
                if (template.Handler != null && template.Handler.Params.String("highlight-entity") == entity.EntityId) {
                    mixins += "mixin-highlight-entity" + " ";
                    color = null;
                    material = null;
                }

                if (template.Handler != null && template.Handler.Params.String("preview-entity") != null) {
                    if(template.Handler.Params.String("preview-entity") == entity.EntityId) {
                        opacity = "1.0";
                    } else {
                        opacity = "0.0";
                    }
                }
                
                var entityStandardProperties = new List<string>();
                var entityBasicProperties = new List<string>();
                {
                    entityStandardProperties.Add("entity-public-id=\"{entity.public-id}\"");
                    entityStandardProperties.Add("entity-id=\"{entity.entity-id}\"");
                    if (!string.IsNullOrEmpty(entity.Title)) entityStandardProperties.Add("entity-title=\"{entity.title}\"");
                    if (!string.IsNullOrEmpty(entity.Script)) {
                        var scriptAttrSave = entity.Script;
                        scriptAttrSave = scriptAttrSave.Replace("\"", "'");
                        scriptAttrSave = scriptAttrSave.Replace("\n", " ");
                        entityStandardProperties.Add("entity-script=\"" + scriptAttrSave + "\"");
                    }
                    entityStandardProperties.Add("entity-level=\"" + level + "\"");
                    if(!string.IsNullOrEmpty(entity.MappingIds)) entityStandardProperties.Add("entity-mapping-ids=\"" + entity.MappingIds + "\"");
                    if(classes != "") entityStandardProperties.Add("class=\""+classes+"\"");
                    if (visible == false) entityStandardProperties.Add("visible=\"false\"");
                    if (entity.Selectable == true) entityStandardProperties.Add("selectable=\"true\"");
                    if (entity.LookAt == "billboard" || entity.LookAt == "camera") entityStandardProperties.Add("billboard");
                    else if (entity.LookAt != null) entityStandardProperties.Add("look-at=\"src:"+entity.LookAt+"\"");
                    if (mixins != "") entityStandardProperties.Add("mixin=\""+mixins+"\"");
                }
                {
                    entityBasicProperties.Add("entity-public-id=\"{entity.public-id}\"");
                    entityBasicProperties.Add("entity-id=\"{entity.entity-id}\"");
                    if(!string.IsNullOrEmpty(entity.Title)) entityBasicProperties.Add("entity-title=\"{entity.title}\"");
                    entityBasicProperties.Add("entity-level=\"" + level + "\"");
                    if(!string.IsNullOrEmpty(entity.MappingIds)) entityBasicProperties.Add("entity-mapping-ids=\"" + entity.MappingIds + "\"");
                    if(classes != "") entityBasicProperties.Add("class=\""+classes+"\"");
                    if (visible == false) entityBasicProperties.Add("visible=\"false\"");
                    if (entity.Selectable == true) entityBasicProperties.Add("selectable=\"true\"");
                    if (entity.LookAt == "billboard" || entity.LookAt == "camera") entityBasicProperties.Add("billboard");
                    else if (entity.LookAt != null) entityBasicProperties.Add("look-at=\"src:"+entity.LookAt+"\"");
                    if (mixins != "") entityBasicProperties.Add("mixin=\""+mixins+"\"");
                }
                //entityStandardProperties.Add("shadow=\"receive: true; cast: true;\"");
                if(material != null) entityStandardProperties.Add("material=\""+ material.GetPropString()  + "\"");
                if(!string.IsNullOrEmpty(color)) entityStandardProperties.Add("color=\""+color+"\"");
                if(!string.IsNullOrEmpty(opacity)) entityStandardProperties.Add("opacity=\""+opacity+"\"");
                if(!string.IsNullOrEmpty(entity.Rotation)) entityStandardProperties.Add("rotation=\""+entity.Rotation+"\"");
                if(!string.IsNullOrEmpty(entity.Position)) entityStandardProperties.Add("position=\""+entity.Position+"\"");
                if(!string.IsNullOrEmpty(entity.Scale)) entityStandardProperties.Add("scale=\""+entity.Scale+"\"");
                entityTemplate.InsertVariableUnsafe("entity.standard-properties", string.Join(" ",entityStandardProperties));
                entityTemplate.InsertVariableUnsafe("entity.basic-properties", string.Join(" ",entityBasicProperties));

                // Asset
                //TODO?


                entityTemplate.InsertVariables("entity", entity);
              

                if (entity.Asset != null) {
                    entityTemplate.InsertVariables("entity.asset", entity.Asset);
                }

                _insertEntityRecursively(entityTemplate, "entity.children", allEntities, entity, level+1);

                childrenTemplates.Add(entityTemplate);
            }
            
            template.InsertTemplates(variableName, childrenTemplates);
            

        }

        public static void InsertScene(this PageTemplate template, string variableName, ThreeD.Model.ThreeDScene scene) {
            // Scene
            PageTemplate sceneTemplate = LoadSceneTemplate(template.Handler, scene);

            // Insert main scene template
            template.InsertTemplate(variableName, sceneTemplate);
        }

        public static PageTemplate LoadSceneTemplate(Core.Handler.Handler handler, ThreeDScene scene) {
            var sceneTemplate = new PageTemplate("Machinata.Module.ThreeD", "scene");
            sceneTemplate.Handler = handler;
            sceneTemplate.Language = handler.Language;
            sceneTemplate.InsertVariables("scene", scene);

            // Cameras have to be before assets for chrome bug
            //https://github.com/aframevr/aframe/issues/3921
            //Yeah I reproduced this as well on my own project which also uses the template component. A workaround is to move the camera element up higher on the page(above assets for example) -->

            // Entities which are root cameras
             _insertEntityRecursively(sceneTemplate, "cameras", scene.Entities.Where(e=>e.Type == ThreeDEntity.EntityTypes.Camera && e.ParentId == null), null);

            // Assets
            var assetTemplates = new List<PageTemplate>();
            foreach (var asset in scene.Assets) {
                var typeExtension = asset.Type.ToString().ToLowerInvariant();
                PageTemplate assetTemplate = null;
                try {
                    assetTemplate = sceneTemplate.LoadTemplate("scene.asset." + typeExtension);
                } catch {
                    assetTemplate = sceneTemplate.LoadTemplate("scene.asset.default");
                }
                assetTemplate.InsertVariables("entity", asset);
                assetTemplates.Add(assetTemplate);
            }
            sceneTemplate.InsertTemplates("assets", assetTemplates);

            // Entities, which are not root cameras
            
            _insertEntityRecursively(sceneTemplate, "entities", scene.Entities.Where(e => (e.Type != ThreeDEntity.EntityTypes.Camera) || (e.Type == ThreeDEntity.EntityTypes.Camera && e.ParentId != null)), null);
            return sceneTemplate;
        }
    }
}
