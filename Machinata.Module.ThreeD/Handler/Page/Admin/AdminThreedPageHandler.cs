using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Templates;

using Machinata.Core.Reporting;
using Machinata.Module.ThreeD.Model;
using Machinata.Module.ThreeD.Templates;
using Machinata.Core.Exceptions;
using Newtonsoft.Json.Linq;
using System.Text;

namespace Machinata.Module.Threed.Handler {

    public class AdminScenePageHandler : Module.Admin.Handler.AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("threed");
        }

        #endregion

        #region Menu

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "3d-model",
                Path = "/admin/threed",
                Title = "{text.threed}",
                Sort = "500"
            });
        }

        #endregion

        #region Scene ///////////////////////////////////////////////////////////////////

        [RequestHandler("/admin/threed")]
        public void Default() {

          
            // Categories
            var categories = this.DB.ThreeDScenes().DistinctBy(s => s.Category).ToList().Select(s => s.CategoryLink);
            this.Template.InsertTemplatesForObjects("categories", categories, "default.category-tile", delegate (string c, PageTemplate t) {
                t.InsertVariable("category", c);
                t.InsertVariable("category-uri-safe", c);
            });

          

            // Scenes
            var entities = this.Template.Paginate(this.DB.ThreeDScenes(), this);
            this.Template.InsertEntityList(
                variableName: "entities",
                entities: entities,
                form: new Core.Builder.FormBuilder(Forms.Admin.LISTING)
                        .Include(nameof(ThreeDScene.Category)),
                link: "{page.navigation.current-path}/categories/{entity.category-link}/scenes/scene/{entity.public-id}"
            );

            // Navigation
            this.Navigation.Add("threed");
        }

      
        [RequestHandler("/admin/threed/categories/{category}")]
        public void Category(string category) {

         
            var entities = this.Template.Paginate(this.DB.ThreeDScenes().ToList().Where(s => s.CategoryLink == category).AsQueryable(), this);
            this.Template.InsertEntityList(
                variableName: "entities",
                entities: entities,
                form: new Core.Builder.FormBuilder(Forms.Admin.LISTING),
                link: "{page.navigation.current-path}/scenes/scene/{entity.public-id}"
            );

            // Navigation
            this.Navigation.Add("threed");
            this.Navigation.Add("categories/"+category, "{text.category}: " + category);
        }

        
        [RequestHandler("/admin/threed/scenes/create")]
        public void Create() {
            this.RequireWriteARN();

            var entity = new ThreeDScene();
        
            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.CREATE),
               apiCall: "/api/admin/threed/scenes/create",
               onSuccess: "/admin/threed/categories/{scene.category-link}/scenes/scene/{scene.public-id}"
            );

            // Navigation
            this.Navigation.Add("threed");
            this.Navigation.Add("/scenes/create","create");
        }

        [RequestHandler("/admin/threed/categories/{category}/scenes/scene/{publicId}/edit")]
        public void Edit(string category, string publicId) {
            this.RequireWriteARN();

            var entity = DB.ThreeDScenes()
            
                .GetByPublicId(publicId);

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: "/api/admin/threed/scenes/scene/" + entity.PublicId + "/edit",
               onSuccess: "{page.navigation.prev-path}"
            );

            // Variables
            Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("threed");
            this.Navigation.Add("categories/" + entity.CategoryLink, "{text.category}: " + entity.CategoryLink);
            this.Navigation.Add($"scenes/scene/{publicId}", entity.Title);
            this.Navigation.Add($"edit");
        }

                

        [RequestHandler("/admin/threed/categories/{category}/scenes/scene/{publicId}/preview")]
        public void Preview(string category, string publicId) {


            var entity = DB.ThreeDScenes()
                .Include(nameof(ThreeDScene.Assets))
                .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Asset))
                .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Material))
                .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Children))
                .GetByPublicId(publicId);
            
            // Variables
            this.Template.InsertScene("scene", entity);
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("threed");
            this.Navigation.Add("categories/" + entity.CategoryLink, "{text.category}: " + entity.CategoryLink);
            this.Navigation.Add($"scenes/scene/{publicId}", entity.Title);
            this.Navigation.Add($"preview");
        }



        [RequestHandler("/admin/threed/categories/{category}/scenes/scene/{publicId}")]
        public void Scene(string category, string publicId) {
            var entity = DB.ThreeDScenes()
                    .Include(nameof(ThreeDScene.Materials) + "." + nameof(ThreeDMaterial.Source))
                    .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Children))
                    .Include(nameof(ThreeDScene.Assets))
                    .GetByPublicId(publicId);

            // Scene
            this.Template.InsertPropertyList(
                variableName: "entity",
                entity: entity,
                form: new FormBuilder(Forms.Admin.VIEW).Include(nameof(ThreeDScene.PublicId))
            );
            
            // Assets
            this.Template.InsertEntityList(
                variableName: "assets",
                entities: entity.Assets.AsQueryable(),
                link: "{page.navigation.current-path}/asset/{entity.public-id}"
            );

            // Materials
            this.Template.InsertEntityList(
                variableName: "materials",
                entities: entity.Materials.AsQueryable(),
                link: "{page.navigation.current-path}/material/{entity.public-id}"
            );


            // Entities
            this.Template.InsertEntityList(
                    variableName: "entity-list",
                    entities: entity.Entities.AsQueryable(),
                    link: "{page.navigation.current-path}/entity/{entity.path}"
            );
            _insertEntityTiles("entity-tiles", entity.Entities.AsQueryable().Where(e => e.IsRoot == true));


            // Variables
            this.Template.InsertVariables("entity", entity);


            // Navigation
            this.Navigation.Add("threed");
            this.Navigation.Add("categories/" + entity.CategoryLink, "{text.category}: " + entity.CategoryLink);
            this.Navigation.Add($"scenes/scene/{publicId}", entity.Title);
           

        }

        private void _insertEntityTiles(string variableName, IEnumerable<ThreeDEntity> entities) {
            this.Template.InsertTemplates(
                variableName: variableName,
                entities: entities.OrderBy(e => e.Sort),
                templateName: "default.entity-tile",
                forEachEntity: delegate (ThreeDEntity e, PageTemplate t) {
                    t.InsertVariable("entity.title-or-id", e.Title != null ? e.Title : e.EntityId);
                    //t.InsertVariable("entity.link", "{page.navigation.current-path}/entity/"+e.Path);
                    t.InsertVariable("entity.link", $"/admin/threed/categories/{e.Scene.CategoryLink}/scenes/scene/{e.Scene.PublicId}/entity/" + e.Path);
                    // $"/admin/threed/categories/{entity.Scene.CategoryLink}/scenes/scene/{entity.Scene.PublicId}/entity/" + "{entity.path}"
                    t.InsertVariable("entity.published", (!e.Enabled || !e.Visible) ? "false" : "true");
                }
            );
        }


        [RequestHandler("/admin/threed/categories/{category}/scenes/scene/{publicId}/export", "/admin/threed/*", null, Verbs.Get, ContentType.StaticFile)]
        public void Export(string category, string publicId) {
                      
            var entity = DB.ThreeDScenes()
                  .Include(nameof(ThreeDScene.Materials) + "." + nameof(ThreeDMaterial.Source))
                  .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Children))
                  .Include(nameof(ThreeDScene.Assets))

                  .GetByPublicId(publicId);

            // Scene
            var exportForm = new FormBuilder(Forms.Frontend.JSON);
            var result = entity.GetJSON(exportForm);

            result["assets"] = Core.JSON.FromObjects(entity.Assets.Select(a => a.GetJObject(exportForm)));
            result["materials"] = Core.JSON.FromObjects(entity.Materials.Select(m=>m.GetJObject(exportForm)));
            result["entities"] = Core.JSON.FromObjects(entity.Entities.Select(e=>e.GetJObject(exportForm)));

            var rawJson = Core.JSON.Serialize(result, true, true, true);
            //  this.SendJSONAsFile(result, entity.Title + ".json");
            var binaries = Encoding.UTF8.GetBytes(rawJson);
            this.SendBinary(binaries, entity.Title + ".json");
            // Navigation
            this.Navigation.Add("threed");
            this.Navigation.Add("categories/" + entity.CategoryLink, "{text.category}: " + entity.CategoryLink);
            this.Navigation.Add($"scenes/scene/{publicId}", entity.Title);


        }

        #endregion

        #region Asset ////////////////////////////////////////////////////////////////

        [RequestHandler("/admin/threed/categories/{category}/scenes/scene/{sceneId}/asset/{assetId}")]
        public void Asset(string category, string sceneId, string assetId) {
            this.RequireWriteARN();

            var scene = DB.ThreeDScenes()
                 .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Asset))
                 .Include(nameof(ThreeDScene.Assets)).GetByPublicId(sceneId);



            var asset = scene.Assets.AsQueryable().GetByPublicId(assetId);

            var entites = scene.Entities.Where(e => e.Asset != null &&  e.Asset.Id == asset.Id);

            // Variables
            this.Template.InsertVariables("entity", asset);
            this.Template.InsertPropertyList("entity", asset);


            // Entities
            this.Template.InsertEntityList(
             variableName: "entities",
             entities: entites.AsQueryable(),
             link: "{page.navigation.prev-path}/entity/{entity.path}"
             );


            // Navigation
            this.Navigation.Add("threed");
            this.Navigation.Add("categories/" + scene.CategoryLink, "{text.category}: " + scene.CategoryLink);
            this.Navigation.Add($"scenes/scene/{sceneId}", scene.Title);
            this.Navigation.Add("asset/" + assetId , "{text.asset}: " + asset.AssetId);

        }


        [RequestHandler("/admin/threed/categories/{category}/scenes/scene/{sceneId}/create-asset")]
        public void AssetCreate(string category, string sceneId) {
            this.RequireWriteARN();

            var scene = DB.ThreeDScenes()
                  .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Children))
                  .Include(nameof(ThreeDScene.Assets)).GetByPublicId(sceneId);

            var entity = new ThreeDAsset();


            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.CREATE),
               apiCall: $"/api/admin/threed/scenes/scene/{sceneId}/create-asset",
               onSuccess: $"/admin/threed/categories/{scene.CategoryLink}/scenes/scene/{sceneId}" + "/asset/{asset.public-id}"
            );

            // Variables
            Template.InsertVariables("entity", entity);


            // Navigation
            this.Navigation.Add("threed");
            this.Navigation.Add("categories/" + scene.CategoryLink, "{text.category}: " + scene.CategoryLink);
            this.Navigation.Add($"scenes/scene/{sceneId}", scene.Title);
            this.Navigation.Add("create-asset", "{text.create-asset}");

        }


        [RequestHandler("/admin/threed/categories/{category}/scenes/scene/{sceneId}/asset/{assetId}/edit")]
        public void AssetEdit(string category, string sceneId, string assetId) {
            this.RequireWriteARN();

            var scene = DB.ThreeDScenes()
                .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Children))
                .Include(nameof(ThreeDScene.Assets)).GetByPublicId(sceneId);

            var entity = scene.Assets.AsQueryable().GetByPublicId(assetId);


            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.EDIT),
               apiCall: $"/api/admin/threed/assets/asset/{assetId}" + "/edit",
               onSuccess: $"/admin/threed/categories/{scene.CategoryLink}/scenes/scene/{sceneId}/asset/" + assetId
            );

            // Variables
            Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("threed");
            this.Navigation.Add("categories/" + scene.CategoryLink, "{text.category}: " + scene.CategoryLink);
            this.Navigation.Add($"scenes/scene/{sceneId}", scene.Title);
            this.Navigation.Add("asset/" + assetId, "{text.asset}: " + entity.AssetId);
            this.Navigation.Add("edit");

        }


        #endregion

        #region Entity ////////////////////////////////////////////////////////////////



        [RequestHandler("/admin/threed/categories/{category}/scenes/scene/{sceneId}/entity/{entityPath}")]
        public void Entity(string category, string sceneId, string entityPath) {
            this.RequireWriteARN();

            var scene = DB.ThreeDScenes()
                  .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Children))
                  .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Asset))
                  .Include(nameof(ThreeDScene.Assets))
                  .Include(nameof(ThreeDScene.Materials))
                  .GetByPublicId(sceneId);

            var entity = scene.Entities.FirstOrDefault(e => e.Path == entityPath);


            // Variables
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity);
            this.Template.InsertVariables("scene", entity.Scene);

            // Direct Children
            this.Template.InsertEntityList(
                 variableName: "children",
                 entities: entity.Children.AsQueryable().OrderBy(e => e.Sort),
                 link: $"/admin/threed/categories/{entity.Scene.CategoryLink}/scenes/scene/{entity.Scene.PublicId}/entity/" + "{entity.path}" 
             );
            _insertEntityTiles("children-tiles", entity.Children.AsQueryable());



            // Navigation
            this.Navigation.Add("threed");
            this.Navigation.Add("categories/" + scene.CategoryLink, "{text.category}: " + scene.CategoryLink);
            this.Navigation.Add($"scenes/scene/{sceneId}", scene.Title);
            this.AddEntityNav(entity);

        }

        private void AddEntityNav(ThreeDEntity entity) {
            foreach (var navItem in entity.NavigationList) {
                this.Navigation.Add($"/admin/threed/categories/{entity.Scene.CategoryLink}/scenes/scene/{entity.Scene.PublicId}/entity/" + navItem.Path, navItem.EntityId);
            }
            
        }

        [RequestHandler("/admin/threed/categories/{category}/scenes/scene/{sceneId}/create-entity")]
        public void EntityCreate(string category, string sceneId) {
            EntityCreate(category, sceneId, null);
        }
 

        [RequestHandler("/admin/threed/categories/{category}/scenes/scene/{sceneId}/create-entity/{entityPath}")]
        public void EntityCreate(string category, string sceneId, string entityPath) {
            this.RequireWriteARN();

            var scene = DB.ThreeDScenes()
                  .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Children))
                  .Include(nameof(ThreeDScene.Assets))
                  .Include(nameof(ThreeDScene.Materials))
                  .GetByPublicId(sceneId);

            var parent = scene.Entities.SingleOrDefault(e => e.Path == entityPath);

            var entity = new ThreeDEntity();
            if(this.Params.String("type") == "camera") {
                entity.Type = ThreeDEntity.EntityTypes.Camera;
                entity.EntityId = "camera";
            } else if (this.Params.String("type") == "sky") {
                entity.Type = ThreeDEntity.EntityTypes.Sky;
                entity.EntityId = "sky";
                entity.Color = "blue";
            }
            
            // Variables
            Template.InsertVariables("entity", entity);

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.CREATE),
               apiCall: $"/api/admin/threed/scenes/scene/{scene.PublicId}/create-entity" + (parent != null ? "?parent="+parent.PublicId: ""),
               onSuccess: $"/admin/threed/categories/" + scene.CategoryLink+ $"/scenes/scene/{scene.PublicId}/entity/" + "{entity.path}"
            );


            // Navigation
            this.Navigation.Add("threed");
            this.Navigation.Add("categories/" + scene.CategoryLink, "{text.category}: " + scene.CategoryLink);
            this.Navigation.Add($"scenes/scene/{sceneId}", scene.Title);
            if(parent != null) this.AddEntityNav(parent);
            this.Navigation.Add("create-entity","{text.create-entity}");

        }
        

        [RequestHandler("/admin/threed/categories/{category}/scenes/scene/{sceneId}/entity/{entityPath}/preview")]
        public void EntityPreview(string category, string sceneId, string entityPath) {
            EntityPreviewHighlight(category, sceneId, entityPath);
            this.Navigation.Add("preview");
        }

        [RequestHandler("/admin/threed/categories/{category}/scenes/scene/{sceneId}/entity/{entityPath}/highlight")]
        public void EntityHighlight(string category, string sceneId, string entityPath) {
            EntityPreviewHighlight(category, sceneId, entityPath);
            this.Navigation.Add("highlight");
        }
        
        public void EntityPreviewHighlight(string category, string sceneId, string entityPath) {

            var scene = DB.ThreeDScenes()
                   .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Children))
                   .Include(nameof(ThreeDScene.Assets))
                   .Include(nameof(ThreeDScene.Materials))
                   .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Asset))
                   .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Material))
                   .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Children))
                   .GetByPublicId(sceneId);

            var entity = scene.Entities.FirstOrDefault(e => e.Path == entityPath);
            
            // Variables
            this.Template.InsertScene("scene", scene);
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("threed");
            this.Navigation.Add("categories/" + scene.CategoryLink, "{text.category}: " + scene.CategoryLink);
            this.Navigation.Add($"scenes/scene/{sceneId}", scene.Title);
            this.AddEntityNav(entity);
        }

        [RequestHandler("/admin/threed/categories/{category}/scenes/scene/{sceneId}/entity/{entityPath}/edit")]
        public void EntityEdit(string category, string sceneId, string entityPath) {
            this.RequireWriteARN();


            var scene = DB.ThreeDScenes()
                   .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Children))
                   .Include(nameof(ThreeDScene.Assets))
                   .Include(nameof(ThreeDScene.Materials))
                   .GetByPublicId(sceneId);

            
            // Entity
            var entity = scene.Entities.FirstOrDefault(e => e.Path == entityPath);

            // Variables
            Template.InsertVariables("entity", entity);

            // Form Custom
            var form =  new FormBuilder(Forms.Admin.EDIT);
            form.DropdownList("{text.asset}", scene.Assets.Where(a => a.AssetId!= null).AsQueryable(), entity.Asset, false);
            form.DropdownList("{text.material}", scene.Materials.AsQueryable(), entity.Material, false);

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: form,
               apiCall: $"/api/admin/threed/entities/entity/{entity.PublicId}" + "/edit",
               onSuccess: $"/admin/threed/categories/" + scene.CategoryLink + $"/scenes/scene/{sceneId}/entity/" + "{entity.path}"
            );


            // Navigation
            this.Navigation.Add("threed");
            this.Navigation.Add("categories/" + scene.CategoryLink, "{text.category}: " + scene.CategoryLink);
            this.Navigation.Add($"scenes/scene/{sceneId}", scene.Title);
            this.AddEntityNav(entity);
            this.Navigation.Add("edit");

        }


        [RequestHandler("/admin/threed/categories/{category}/scenes/scene/{sceneId}/entity/{entityPath}/upload-meshes")]
        public void EntityUploadMeshes(string category, string sceneId, string entityPath) {
            this.RequireWriteARN();

            // Scene
            var scene = DB.ThreeDScenes()
                   .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Children))
                   .Include(nameof(ThreeDScene.Assets))
                   .Include(nameof(ThreeDScene.Materials))
                   .GetByPublicId(sceneId);


            // Entity
            var entity = scene.Entities.FirstOrDefault(e => e.Path == entityPath);

            // Variables
            Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("threed");
            this.Navigation.Add("categories/" + scene.CategoryLink, "{text.category}: " + scene.CategoryLink);
            this.Navigation.Add($"scenes/scene/{sceneId}", scene.Title);
            this.AddEntityNav(entity);
            this.Navigation.Add("upload-meshes");

        }


        #endregion

        #region Material ////////////////////////////////////////////////////////////////

        [RequestHandler("/admin/threed/categories/{category}/scenes/scene/{sceneId}/material/{materialId}")]
        public void Material(string category, string sceneId, string materialId) {

            var scene = DB.ThreeDScenes()
                 .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Material))
                 .Include(nameof(ThreeDScene.Materials) + "." + nameof(ThreeDMaterial.Source))
                 .GetByPublicId(sceneId);


            var material = scene.Materials.AsQueryable().GetByPublicId(materialId);

            var entites = scene.Entities.Where(e => e.Material != null && e.Material.Id == material.Id);

            // Variables
            this.Template.InsertVariables("entity", material);
            this.Template.InsertPropertyList("entity", material);


            // Entities
            this.Template.InsertEntityList(
             variableName: "entities",
             entities: entites.AsQueryable(),
             link: "{page.navigation.prev-path}/entity/{entity.path}"
             );


            // Navigation
            this.Navigation.Add("threed");
            this.Navigation.Add("categories/" + scene.CategoryLink, "{text.category}: " + scene.CategoryLink);
            this.Navigation.Add($"scenes/scene/{sceneId}", scene.Title);
            this.Navigation.Add("material/" + materialId, "{text.Material}: " + material.MaterialId);

        }


        [RequestHandler("/admin/threed/categories/{category}/scenes/scene/{sceneId}/create-material")]
        public void MaterialCreate(string category, string sceneId) {
            this.RequireWriteARN();

            var scene = DB.ThreeDScenes()
                  .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Children))
                  .Include(nameof(ThreeDScene.Materials)).GetByPublicId(sceneId);

            var entity = new ThreeDMaterial();

            // Form
            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: new FormBuilder(Forms.Admin.CREATE),
               apiCall: $"/api/admin/threed/scenes/scene/{sceneId}/create-material",
               onSuccess: $"/admin/threed/categories/" + scene.CategoryLink + $"/scenes/scene/{sceneId}" + "/material/{material.public-id}"
            );

            // Variables
            Template.InsertVariables("entity", entity);


            // Navigation
            this.Navigation.Add("threed");
            this.Navigation.Add("categories/" + scene.CategoryLink, "{text.category}: " + scene.CategoryLink);
            this.Navigation.Add($"scenes/scene/{sceneId}", scene.Title);
            this.Navigation.Add("create-material", "{text.create-material}");

        }


        [RequestHandler("/admin/threed/categories/{category}/scenes/scene/{sceneId}/material/{materialId}/edit")]
        public void MaterialEdit(string category, string sceneId, string materialId) {
            this.RequireWriteARN();

            var scene = DB.ThreeDScenes()
                .Include(nameof(ThreeDScene.Entities) + "." + nameof(ThreeDEntity.Children))
                .Include(nameof(ThreeDScene.Materials))
                .Include(nameof(ThreeDScene.Assets))
                .GetByPublicId(sceneId);

            var entity = scene.Materials.AsQueryable().GetByPublicId(materialId);

            var form = new FormBuilder(Forms.Admin.EDIT);
            form.DropdownList("{text.source}", scene.Assets.Where(a=>a.AssetId != null).AsQueryable(), entity.Source, false);

            this.Template.InsertForm(
               variableName: "form",
               entity: entity,
               form: form,
               apiCall: $"/api/admin/threed/materials/material/{materialId}" + "/edit",
               onSuccess: $"/admin/threed/categories/" + scene.CategoryLink + $"/scenes/scene/{sceneId}/material/" + materialId
            );

            // Variables
            Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("threed");
            this.Navigation.Add("categories/" + scene.CategoryLink, "{text.category}: " + scene.CategoryLink);
            this.Navigation.Add($"scenes/scene/{sceneId}", scene.Title);
            this.Navigation.Add("material/" + materialId, "{text.material}: " + entity.MaterialId);
            this.Navigation.Add("edit");

        }


        #endregion

    }


}