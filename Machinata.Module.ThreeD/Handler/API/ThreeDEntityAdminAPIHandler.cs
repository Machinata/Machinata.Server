using Machinata.Core.Handler;
using Machinata.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using System.Text;
using Machinata.Core.Model;
using Machinata.Module.ThreeD.Model;
using System.IO;

namespace Machinata.Module.ThreeD.Handler.API {
    public class ThreeDEntityAdminAPIHandler : Module.Admin.Handler.CRUDAdminAPIHandler<Module.ThreeD.Model.ThreeDEntity> {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultRootPolicies("threed");
        }

        #endregion


        [RequestHandler("/api/admin/threed/entities/entity/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }

        [RequestHandler("/api/admin/threed/entities/entity/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();
                     
            var entity = this.DB.Set<ThreeDEntity>()
                .Include(nameof(ThreeDEntity.Asset))
                .Include(nameof(ThreeDEntity.Material))
                .GetByPublicId(publicId);

            var form = new FormBuilder(Forms.Admin.EDIT)
                .Include(nameof(ThreeDEntity.Asset))
                .Include(nameof(ThreeDEntity.Material));

            entity.Populate(this, form);
            entity.UpdateProperties();

            // Post Populate
            this.EditPopulate(entity);
            entity.Validate();

            this.DB.SaveChanges();

            // Return
            SendAPIMessage("edit-success", new { Entity = new { entity.PublicId, entity.Path } });
        }

        protected override void EditPopulate(ThreeDEntity entity) {

            entity.Include(nameof(entity.Scene));
            entity.Scene.Include(nameof(entity.Scene.Entities));

            entity.Scene.CheckUniqueId(entity);
        }


        [RequestHandler("/api/admin/threed/entities/entity/{publicId}/select-asset/{toggleId}")]
        public void ToggleCourse(string publicId, string toggleId) {
            this.CRUDToggleSingleSelection<ThreeDAsset>(publicId, toggleId, nameof(ThreeDEntity.Asset));
        }


        [RequestHandler("/api/admin/threed/entities/entity/{publicId}/upload-meshes")]
        public void UploadMeshes(string publicId) {
            this.RequireWriteARN();

            var parent = this.DB.Set<ThreeDEntity>()
                .Include(nameof(ThreeDEntity.Scene))
                .Include(nameof(ThreeDEntity.Scene) + "." + (nameof(ThreeDScene.Assets)))
                .Include(nameof(ThreeDEntity.Scene) + "." + (nameof(ThreeDScene.Entities)))
                .Include(nameof(ThreeDEntity.Asset))
                .Include(nameof(ThreeDEntity.Material))
                .GetByPublicId(publicId);

            var scene = parent.Scene;

            var nFiles = this.Context.Request.Files.Count;


            for (int i = 0; i < nFiles; i++) {

                // Upload File
                var requestFile = this.Request.File(i);
                var contentFile = Core.Data.DataCenter.UploadFile(requestFile, this.User, "ThreeDAsset");
                this.DB.ContentFiles().Add(contentFile);
                this.DB.SaveChanges();

                var file = requestFile.FileName;

                // Asset ----------------------------------------------------------------------
                var fileName = Path.GetFileNameWithoutExtension(file);
                var fileExtension = Path.GetExtension(file);


                var asset = new ThreeDAsset();
                asset.Type = ThreeDAsset.AssetTypes.Mesh;
                asset.File = contentFile.ContentURL;
                asset.AssetId = file;
                asset.AssetId = Core.Util.String.CreateShortURLForName(asset.AssetId, true, true);


                // Add to DB
                this.DB.ThreeDAssets().Add(asset);

                // Check Id
                if (scene.HasUniqueEntityId(asset) == false) {
                    this.DB.SaveChanges();
                    asset.AssetId = asset.PublicId;
                }

                asset.Scene = scene;
                scene.Assets.Add(asset);

                // Entity ---------------------------------------------------------------------
                var entity = new ThreeDEntity();
                entity.Visible = true;
                entity.Enabled = true;
                entity.Type = ThreeDEntity.EntityTypes.Mesh;
                entity.Asset = asset;
                
               
                

                var newFileName = fileName.ToString();
                var classes = GetIdStringFromFilename(ref newFileName, '[', ']');
                var mappingIds = GetIdStringFromFilename(ref newFileName, '(', ')');

                entity.Classes = classes;
                entity.MappingIds = mappingIds;


                entity.EntityId = newFileName;
                entity.EntityId = Core.Util.String.CreateShortURLForName(entity.EntityId, true, true);

                entity.Title = newFileName.ToSentence();


                // Update Asset Name
                if (newFileName != fileName && asset.AssetId != asset.PublicId) {
                    asset.AssetId = newFileName + fileExtension;
                    asset.AssetId = Core.Util.String.CreateShortURLForName(asset.AssetId, true, true);

                    // Check Id
                    if (scene.HasUniqueEntityId(asset) == false) {
                        this.DB.SaveChanges();
                        asset.AssetId = asset.PublicId;
                    }
                }


                this.DB.ThreeDEntities().Add(entity);

                if (scene.HasUniqueEntityId(entity) == false) {
                    this.DB.SaveChanges();
                    entity.EntityId = entity.PublicId;
                }

                entity.UpdateProperties();

                entity.Scene = scene;
                scene.Entities.Add(entity);
                entity.Parent = parent;
            }


            this.DB.SaveChanges();

            // Return
            SendAPIMessage("upload-meshes-success", new { Entity = new { parent.PublicId, parent.Path } });
        }

        /// <summary>
        /// �Filename xyz [{classes}] ({mapping-ids})
        /// Retrievs the string between an opening character and closing character
        /// Updates the filename by removeing the found string
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string GetIdStringFromFilename(ref string fileName, char startChar, char endChar) {
            var opening = fileName.Count(c=> c == startChar);
            var closing = fileName.Count(c => c == endChar);

            if (opening == 0) {
                return null;
            }
            if (opening != closing) {
                throw new BackendException("upload-error", "Different count of opening and closing brackets is not supported: " + fileName);
            }
            if (opening > 1 || closing > 1) {
                throw new BackendException("upload-error", "Multiple opening and closing brackets is not supported: " + fileName);
            }

            var start = fileName.IndexOf(startChar);
            var end = fileName.IndexOf(endChar);

            if (start > end) {
                throw new BackendException("upload-error", "Opening bracket before closing bracket is not supported: " + fileName);
            }

           

            var substring = fileName.Substring(start + 1, end - start-1);
            fileName = fileName.Remove(start, end - start + 1);
            fileName = fileName.Trim();

            return substring;
           
        }

    }
}