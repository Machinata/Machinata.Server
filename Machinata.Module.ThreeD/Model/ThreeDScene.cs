using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core.Builder;
using Machinata.Core.Model;
using Newtonsoft.Json.Linq;
using Machinata.Core.Exceptions;
using Machinata.Core.Cards;
using System.IO;
using Newtonsoft.Json;

namespace Machinata.Module.ThreeD.Model {
    
    [Serializable()]
    [ModelClass] 
    [Table("ThreeDScenes")]
    public partial class ThreeDScene : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion


        #region Enums
       
        public enum CameraTypes : short {
            Orbital = 1000,
            Walking = 2000
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public ThreeDScene() {
          
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
        
        [Column]
        [Required]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [FormBuilder(Forms.Frontend.JSON)]
        public string Title { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        [Column]
        [Required]
        [MinLength(3)]
        [MaxLength(128)]
        public string ShortURL { get; set; }

        [FormBuilder]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Frontend.JSON)]
        [Column]
        [MinLength(3)]
        [MaxLength(128)]
        public string Category { get; set; }


        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////

        [Column]
        public ICollection<ThreeDMaterial> Materials { get; set; }

        
        [Column]
        public ICollection<ThreeDAsset> Assets { get; set; }


        [Column]
        public ICollection<ThreeDEntity> Entities { get; set; }



        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        public ThreeDEntity GetRoot() {
            throw new NotImplementedException("which one is the root");
             return this.Entities.First(e => e.ParentId == null);
        }

        public void CheckUniqueId(ThreeDEntity entity) {
            if (HasUniqueEntityId(entity) == false) {
                throw new BackendException("same-id", $"EntityId '{entity.EntityId}' has already been taken for the scene. Please choose another one!");
            }
        }

        public void CheckUniqueId(ThreeDAsset asset) {
            if (HasUniqueEntityId(asset) == false) {
                throw new BackendException("same-id", $"AssetId '{asset.AssetId}' has already been taken for the scene. Please choose another one!");
            }
        }

        public void CheckUniqueId(ThreeDMaterial material) {
            if (HasUniqueMaterialId(material) == false) {
                throw new BackendException("same-id", $"MaterialId '{material.MaterialId}' has already been taken for the scene. Please choose another one!");
            }
        }

        public bool HasUniqueEntityId(ThreeDEntity entity) {
            var sameIds = this.Entities.Where(e => e.EntityId == entity.EntityId && e.PublicId != entity.PublicId);
            return sameIds.Any() == false;
        }

        public bool HasUniqueEntityId(ThreeDAsset asset) {
            var sameIds = this.Assets.Where(e => e.AssetId == asset.AssetId && e.PublicId != asset.PublicId);
            return sameIds.Any() == false;
        }

        public bool HasUniqueMaterialId(ThreeDMaterial material) {
            var sameIds = this.Materials.Where(e => e.MaterialId == material.MaterialId && e.PublicId != material.PublicId);
            return sameIds.Any() == false;
        }

        [FormBuilder(Forms.System.LISTING)]
        public string CategoryLink {
            get {
                if (this.Category != null) {
                    return this.Category;
                }
                return "No Category";
            }
        }

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        [OnModelCreating]
        private static void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder) {


            //modelBuilder.Entity<ThreeDScene>()
            //.HasOptional(f => f.GetRoot())
            //.WithOptionalPrincipal(s => s.Scene);
        }


        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public static ThreeDScene ImportFromJSON(ModelContext dB, JObject json, string sceneName, FormBuilder form, User user) {

            var scene = new ThreeDScene();
            scene.FromJSON(json, form);


            //var test = ThreeDAsset.ImportFromJSON(dB, json["assets"].First, form, user);


            var assets = json["assets"].Select(j=>ThreeDAsset.ImportFromJSON(dB, j, form,user)).ToList();
            var materials = json["materials"].Select(j => ThreeDMaterial.ImportFromJSON(dB,j , form,assets)).ToList();
            var entities = json["entities"].Select(j => ThreeDEntity.ImportFromJSON(dB,j, form,assets,materials)).ToList();

            scene.Title = sceneName;
            scene.ShortURL = Core.Util.String.CreateShortURLForName(sceneName);

            scene.Assets = assets;
            scene.Materials = materials;
            scene.Entities = entities;
        

           

        	// Set parents
            foreach(var entity in entities.ToList()) {
                var parent = entities.FirstOrDefault(a => a.EntityId == entity.ParentEntityId);
                entity.Parent = parent;
            }

            return scene;




        }

        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public override CardBuilder VisualCard() {
            this.Include(nameof(ThreeDScene.Entities));
          
            var card = new Core.Cards.CardBuilder(this)
                  .Title(this.Title)
                  .Subtitle(this.ShortURL)
                  .Icon("image-outline");
            return card;
        }

        public override void OnDelete(ModelContext db) {
            base.OnDelete(db);

            // Delete Entities
            db.Set<ThreeDEntity>().RemoveRange(this.Entities.ToList());

            // Delete Assets
            db.Set<ThreeDAsset>().RemoveRange(this.Assets.ToList());

            // Delete Materials
            db.Set<ThreeDMaterial>().RemoveRange(this.Materials.ToList());
        }


        public override void Populate(IPopulateProvider populateProvider, FormBuilder form) {
            base.Populate(populateProvider, form);

            // Make safe category
            if (this.Category != null) {
                this.Category = Core.Util.String.CreateShortURLForName(this.Category, false, false);
            }
        }

        #endregion

    }

   

    #region Extensions ////////////////////////////////////////////////////////////////////////////
    
    public static class ModelContextThreeDSceneExtenions {
        public static DbSet<ThreeDScene> ThreeDScenes(this Core.Model.ModelContext context) {
            return context.Set<ThreeDScene>();
        }
    }

    #endregion
}
