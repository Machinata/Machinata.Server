using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Text;
using Machinata.Core.Cards;
using Machinata.Module.Finance.Model;
using Machinata.Module.Finance.Invoices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.ComponentModel;
using System.CodeDom;

namespace Machinata.Module.Infrastructure.Model {

    [Serializable()]
    [ModelClass]
    [Table("InfrastructureServices")]
    public partial class Service : ModelObject, IPublishedModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////
        public enum ServiceCategory : short {
            Unknown = 0,
            Office = 10,
            IT = 20,
            Software = 30,
            Assets = 40,
            WebService = 50,
            Special = 100
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Service() {
           
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Title { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EDIT)]
        [DataType(DataType.MultilineText)]
        [Column]
        public string Description { get; set; }


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public ServiceCategory Category { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public string Link { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public string Company { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public bool Active { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [Column]
        public Price MonthlyCost { get; set; } = new Price();

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.TOTAL)]
        [Column]
        public Price YearlyCost { get; set; } = new Price();



        public bool Published => this.Active;


        public string ActiveFilterValue =>  this.Active? "Active" : "Not Active";

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////  


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        public Business Business { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        public Price YearlyCostsSafe => this.YearlyCost ?? new Price(0);

        public Price MonthlyCostsSafe => this.MonthlyCost ?? new Price(0);

        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        //[OnModelCreating]
        //public static void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder) {
        //    //modelBuilder.Entity<Service>().Property(nameof(Service.Business)).IsOptional();
        //    modelBuilder.Entity<Service>().HasOptional(s => s.Business);



        //}
        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////




        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        //        [OnModelCreating]
        //        private static void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder) {
        //            //modelBuilder.Entity<Business>()
        //            //   .HasRequired(b => b.AddressRef).WithOptional(a => a.BusinessRef);

        //        }

        //public override CardBuilder VisualCard() {

        //    // Init card

        //    var card = new Core.Cards.CardBuilder(this)
        //        .Title(this.Title)
        //        .Subtitle(this.Interval)
        //        .Icon("document-text")
        //        .Tag(this.ExternalPrice?.Value?.ToString())
        //        .Wide();
        //    return card;
        //}



        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextServicesExtensions {
        [ModelSet]
        public static DbSet<Service> Services(this Core.Model.ModelContext context) {
            return context.Set<Service>();
        }
       
    }

    #endregion

}
