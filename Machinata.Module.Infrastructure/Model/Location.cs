using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Text;
using Machinata.Core.Cards;
using System.Collections.ObjectModel;

namespace Machinata.Module.Infrastructure.Model {

    [Serializable()]
    [ModelClass]
    [Table("InfrastructureLocations")]
    public partial class Location : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////


        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Location() {
           
        }

        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Required]
        [Column]
        public string LocationName { get; set; }


        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        


        [Column]
        public Address Address { get; set; }

        [Column]
        public ICollection<Device> Devices { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////




        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////
       
        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////



        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////


        public override CardBuilder VisualCard() {

            // Init card
          
            var card = new Core.Cards.CardBuilder(this)
                .Title(this.LocationName)
                .Icon("world-outline")
                .Wide();

            return card;
        }


        public override string ToString() {
            return this.LocationName;
        }

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextLocationsExtensions {

        [ModelSet]
        public static DbSet<Location> Locations(this Core.Model.ModelContext context) {
            return context.Set<Location>();
        }
       
    }

    #endregion

}
