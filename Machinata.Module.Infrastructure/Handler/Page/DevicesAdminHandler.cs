
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Infrastructure.Model;
using Machinata.Module.Infrastructure.Extensions;

namespace Machinata.Module.Infrastructure.Handler {


    public class DevicesAdminHandler : AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("infrastructure");
        }

        #endregion

        [RequestHandler("/admin/infrastructure")]
        public void Default() {
            // Menu items
            var menuItems = PageTemplate.Cache.FindAll(this.Template.Package, "admin/infrastructure/menu/menu.item.", this.TemplateExtension);
            this.Template.InsertTemplates("infrastructure.menu-items", menuItems);

            // Navigation
            this.Navigation.Add("infrastructure");

        }
        
        [RequestHandler("/admin/infrastructure/devices")]
        public void Devices() {

            var entities = this.DB.Devices()
                .Include(nameof(Infrastructure.Model.Device.Business));

            // Servers
            this.Template.InsertEntityList(
                    variableName: "servers",
                    entities: entities.Where(d=>d.Category == Infrastructure.Model.Device.DeviceCategories.Server),
                    link: "{page.navigation.current-path}/device/{entity.public-id}");

            // Workstations
            this.Template.InsertEntityList(
                    variableName: "workstations",
                    entities: entities.Where(d => d.Category == Infrastructure.Model.Device.DeviceCategories.Workstation),
                    link: "{page.navigation.current-path}/device/{entity.public-id}"
                    );

            // Devices
            this.Template.InsertEntityList(
                    variableName: "devices",
                    entities: entities.Where(d => d.Category == Infrastructure.Model.Device.DeviceCategories.Device),
                    link: "{page.navigation.current-path}/device/{entity.public-id}");

            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("devices");


        }

        [RequestHandler("/admin/infrastructure/devices/device/{publicId}")]
        public void Device(string publicId) {

            // Course
            var entity = this.DB.Devices()
                .Include(nameof(Infrastructure.Model.Device.Contracts))
                .Include(nameof(Infrastructure.Model.Device.Accounts) + "." + nameof(Infrastructure.Model.Account.Business))
                .GetByPublicId(publicId);

            // Variables
            this.Template.InsertPropertyList(
                    variableName: "entity",
                    entity: entity,
                    form: new FormBuilder(Forms.Admin.VIEW),
                    loadFirstLevelReferences: true
                    );
            this.Template.InsertVariables("entity", entity);


            // Accounts
            this.Template.InsertEntityList(
               variableName: "accounts",
               entities: entity.Accounts.AsQueryable(),
               link: "{page.navigation.current-path}/account/{entity.public-id}"
               );

            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("devices");
            this.Navigation.Add("device/" + publicId, "{text.device}" + ": " + entity.Name);

        }

        [RequestHandler("/admin/infrastructure/devices/create")]
        public void Create() {
            this.RequireWriteARN();

            // Course
            var entity = new Device();
            entity.Category = Infrastructure.Model.Device.DeviceCategories.Server;
            entity.Status = Infrastructure.Model.Device.DeviceStatuses.Offline;

            // Form
            this.Template.InsertForm(
                 form: new FormBuilder(Forms.Admin.CREATE),
                  variableName: "form",
                  entity: entity,
                  apiCall: "/api/admin/infrastructure/devices/create",
                  onSuccess: "/admin/infrastructure/devices/device/{device.public-id}"
                  );

            this.Navigation.Add("infrastructure");
            this.Navigation.Add("devices");
            this.Navigation.Add("create");

        }

       

        [RequestHandler("/admin/infrastructure/devices/device/{publicId}/edit")]
        public void DeviceEdit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Devices()
                .Include(nameof(Model.Device.Accounts))
                .GetByPublicId(publicId);

            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: "/api/admin/infrastructure/device/" + publicId + "/edit",
                onSuccess: "{page.navigation.prev-path}");

            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("devices");
            this.Navigation.Add("device/" + publicId, "{text.device}" + ": " + entity.Name);
            this.Navigation.Add("edit");

        }


        [RequestHandler("/admin/infrastructure/devices/device/{publicId}/add-account")]
        public void AddAccount(string publicId) {
            this.RequireWriteARN();

            // Entity
            var entity = DB.Devices()
                .Include(nameof(Model.Device.Accounts))
                .GetByPublicId(publicId);

            // Form
            this.Template.InsertForm(
                variableName: "form",
                entity: new Account(),
                form: new FormBuilder(Forms.Admin.CREATE).Exclude(nameof(Model.Account.Device)),
                apiCall: "/api/admin/infrastructure/device/" + publicId + "/add-account",
                onSuccess: "{page.navigation.prev-path}");

            // Vars
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("devices");
            this.Navigation.Add("device/" + publicId, "{text.device}" + ": " + entity.Name);
            this.Navigation.Add("add-account");

        }


        [RequestHandler("/admin/infrastructure/devices/device/{publicId}/account/{accountId}")]
        public void Account(string publicId, string accountId) {
            this.RequireWriteARN();

            var device = DB.Devices()
                .Include(nameof(Model.Device.Accounts))
                .GetByPublicId(publicId);

            var account = device.Accounts.AsQueryable().GetByPublicId(accountId);

            this.Template.InsertVariables("account", account);
            this.Template.InsertVariables("device", device);

            var form = new FormBuilder(Forms.Admin.VIEW).Exclude(nameof(Model.Account.Device));
            form.MakeSensitiveAdjustments(account, this.User);

            this.Template.InsertPropertyList(
                variableName: "entity",
                entity: account,
                form: form
                );


            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("devices");
            this.Navigation.Add("device/" + publicId, "{text.device}" + ": " + device.Name);
            this.Navigation.Add("account/" + accountId, "{text.account}" + ": " + account.Name);

        }

        
        [RequestHandler("/admin/infrastructure/devices/device/{publicId}/account/{accountId}/edit")]
        public void AccountEdit(string publicId, string accountId) {
            this.RequireWriteARN();

            var entity = DB.Devices()
                .Include(nameof(Model.Device.Accounts))
                .GetByPublicId(publicId);

            var account = entity.Accounts.AsQueryable().GetByPublicId(accountId);

            var form = new FormBuilder(Forms.Admin.EDIT).Exclude(nameof(Model.Account.Device));
            form.MakeSensitiveAdjustments(account, this.User);

            this.Template.InsertForm(
                variableName: "form",
                entity: account,
                form: form,
                apiCall: $"/api/admin/infrastructure/device/{publicId}/account/{accountId}/edit",
                onSuccess: "{page.navigation.prev-path}"
                );

            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("devices");
            this.Navigation.Add("device/" + publicId, "{text.device}" + ": " + entity.Name);
            this.Navigation.Add("account/" + accountId, "{text.account}" + ": " + account.Name);
            this.Navigation.Add("edit");

        }



    }
}
