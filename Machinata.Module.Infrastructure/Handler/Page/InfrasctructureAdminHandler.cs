
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Infrastructure.Model;
using Machinata.Module.Infrastructure.Extensions;

namespace Machinata.Module.Finance.Handler {


    public class InfrasctructureAdminHandler : AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("infrastructure");
        }

        #endregion

        #region Menu Item

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "monitor",
                Path = "/admin/infrastructure",
                Title = "{text.infrastructure}",
                Sort = "500"
            });
        }

        #endregion


        [RequestHandler("/admin/infrastructure/accounts")]
        public void List() {

            // Business from parameter filter
            Business business = null;
            var businessId = this.Params.String("business");
            if (string.IsNullOrEmpty(businessId) == false) {
                business = this.DB.Businesses().GetByPublicId(businessId);
            }

            // Businesses with accounts for filter list
            var businesses = this.DB.Accounts()
                .Include(nameof(Account.Business))
                .Include(nameof(Account.Device))
                .Where(a => a.Business != null)
                .Select(a => a.Business)
                .Distinct()
                .OrderBy(b => b.Name);

            // Insert business filters
            this.Template.InsertTemplates(
                variableName: "filter.businesses",
                entities: businesses.ToList(),
                templateName: "accounts.business.filter");

            // Accounts
            var accounts = this.DB.Accounts()
                .Include(nameof(Account.Business))
                .Include(nameof(Account.Device))
                .AsQueryable();

            // Filter by business
            if (business != null) {
                accounts = accounts.Where(a => a.Business.Id == business.Id);
            }

            // Accounts paging and filtering
            accounts = this.Template.Filter(accounts, this,nameof(Account.Name)).AsQueryable();
            accounts = this.Template.Paginate(accounts, this);

            // List
            var form = new FormBuilder(Forms.Admin.LISTING)
                .Include(nameof(Account.Device));
            if (this.User.HasSensitiveRights()) {
                form.Include(nameof(Account.IsSensitive));
            }

            this.Template.InsertEntityList(
                    variableName: "entities",
                    entities: accounts,
                    link: "{page.navigation.current-path}/account/{entity.public-id}",
                    form: form);

            // Variables
            this.Template.InsertVariable("filter.business", business == null ? "all" : business.PublicId);

            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("accounts");
        }

        [RequestHandler("/admin/infrastructure/accounts/account/{publicId}")]
        public void View(string publicId) {

            // Course
            var entity = this.DB.Accounts()
                .GetByPublicId(publicId);

            var form = new FormBuilder(Forms.Admin.VIEW);
            form.MakeSensitiveAdjustments(entity, this.User);

            // Variables
            this.Template.InsertPropertyList(
                    variableName: "entity",
                    entity: entity,
                    form: form,
                    loadFirstLevelReferences: true
                    );
            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("accounts");
            this.Navigation.Add("account/" + publicId, "{text.account}" + ": " + entity.Name);

        }

        [RequestHandler("/admin/infrastructure/accounts/create")]
        public void Create() {
            this.RequireWriteARN();

            // Entity
            var entity = new Account();
            entity.AccountCategory = Account.AccountCategories.Login;

            // Form
            this.Template.InsertForm(
                 form: new FormBuilder(Forms.Admin.CREATE),
                  variableName: "form",
                  entity: entity,
                  apiCall: "/api/admin/infrastructure/accounts/create",
                  onSuccess: "/admin/infrastructure/accounts/account/{account.public-id}"
                  );

            this.Navigation.Add("infrastructure");
            this.Navigation.Add("accounts");
            this.Navigation.Add("create");

        }

       

        [RequestHandler("/admin/infrastructure/accounts/account/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Accounts()
                .GetByPublicId(publicId);


            var form = new FormBuilder(Forms.Admin.EDIT);
            form.MakeSensitiveAdjustments(entity, this.User);

            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: form,
                apiCall: "/api/admin/infrastructure/account/" + publicId + "/edit",
                onSuccess: "{page.navigation.prev-path}");

            // Variables
            this.Template.InsertVariables("entity", entity);


            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("accounts");
            this.Navigation.Add("account/" + publicId, "{text.account}" + ": " + entity.Name);
            this.Navigation.Add("edit");

        }
      

    }
}
