
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;
using Machinata.Module.Infrastructure.Model;

namespace Machinata.Module.Finance.Handler {


    public class ServicesAdminHandler : AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("infrastructure");
        }

        #endregion

     
        [RequestHandler("/admin/infrastructure/services")]
        public void Services() {

            var entities = this.DB.Services()
                .Include(nameof(Service.Business)).ToList()
                .AsQueryable();

            entities = FilterEntities(entities, this, this.Template);


            // Report API Call
            {
                var reportAPICall = Core.Config.PublicURL + "/api/admin/infrastructure/services/report?" + this.Context.Request.QueryString;
                this.Template.InsertVariable("report-api-call", reportAPICall);
            }

            // Paging and filtering
            var filterProps = new List<string>() { nameof(Service.Title), nameof(Service.Description), nameof(Service.Link), nameof(Service.Company) };
            entities = this.Template.Filter(entities, this, /*nameof(Service.Title)*/filterProps);
            entities = this.Template.Paginate(entities, this);





            var totalForm = new FormBuilder(Forms.Admin.TOTAL);

            // List
            this.Template.InsertEntityList(
                    variableName: "entities",
                    entities: entities,
                    link: "{page.navigation.current-path}/service/{entity.public-id}",
                    total: totalForm
                   );


            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("services");
        }

        public static IQueryable<Service> FilterEntities(IQueryable<Service> entities, Core.Handler.Handler handler, PageTemplate template) {
            // Active Filters
            {
                var activeFilters = handler.Params.StringArrayFromQueryString("active", (new List<string>() { }).ToArray());
                var availableFilterValues = entities.Select(e => e.ActiveFilterValue).Distinct().ToList();
                var allFilterValues = new List<string>() { "Active", "Not Active" };
                foreach (var filterValue in activeFilters) {
                    //var categoryFilter = (Service.ServiceCategory)Enum.Parse(typeof(Service.ServiceCategory), filterValue);
                    entities = entities.Where(p => p.ActiveFilterValue == filterValue);
                }

               template?.InsertFilters("filter-active", activeFilters.Select(s => s.ToString()), allFilterValues.Select(s => s.ToString()), allFilterValues.Select(s => s.ToString()), "active", postfixIndexEachParamter: false);
            }


            // Category Filters
            {
                var categoryFilters = handler.Params.StringArrayFromQueryString("categories", (new List<string>() { }).ToArray());
                var availableFilterValues = entities.Select(e => e.Category).Distinct().ToList();
                var allFilterValues = Core.Util.EnumHelper.GetEnumValues<Service.ServiceCategory>(typeof(Service.ServiceCategory));
                foreach (var filterValue in categoryFilters) {
                    var categoryFilter = (Service.ServiceCategory)Enum.Parse(typeof(Service.ServiceCategory), filterValue);
                    entities = entities.Where(p => p.Category == categoryFilter);
                }

                template?.InsertFilters("filter-categories", categoryFilters.Select(s => s.ToString()), allFilterValues.Select(s => s.ToString()), availableFilterValues.Select(s => s.ToString()), "categories", postfixIndexEachParamter: false);
            }

            return entities;
        }

        [RequestHandler("/admin/infrastructure/services/service/{publicId}")]
        public void Services(string publicId) {

            // Entity
            var entity = this.DB.Services()
               .Include(nameof(Service.Business))
                .GetByPublicId(publicId);

            // Variables
            this.Template.InsertPropertyList(
                    variableName: "entity",
                    entity: entity,
                    form: new FormBuilder(Forms.Admin.VIEW),
                    loadFirstLevelReferences: true
                    );
            this.Template.InsertVariables("entity", entity);

          
            
            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("services");
            this.Navigation.Add("service/" + publicId, "{text.service}" + ": " + entity.Title);

        }

        [RequestHandler("/admin/infrastructure/services/create")]
        public void Create() {
            this.RequireWriteARN();

            // Course
            var entity = new Service();
            //entity.YearlyCost = new Price(0);
            //entity.MonthlyCost = new Price(0);

            // Form
            this.Template.InsertForm(
                 form: new FormBuilder(Forms.Admin.CREATE),
                  variableName: "form",
                  entity: entity,
                  apiCall: "/api/admin/infrastructure/services/create",
                  onSuccess: "/admin/infrastructure/services/service/{service.public-id}"
                  );

            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("services");
            this.Navigation.Add("create");

        }

        [RequestHandler("/admin/infrastructure/services/service/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Services()
                 .Include(nameof(Service.Business))
                .GetByPublicId(publicId);

            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: "/api/admin/infrastructure/services/" + publicId + "/edit",
                onSuccess: "{page.navigation.prev-path}");

            this.Template.InsertVariables("entity", entity);

            // Navigation
            this.Navigation.Add("infrastructure");
            this.Navigation.Add("services");
            this.Navigation.Add("service/" + publicId, "{text.service}" + ": " + entity.Title);
            this.Navigation.Add("edit");

        }


       

    }
}
