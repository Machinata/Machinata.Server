
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Core;
using Machinata.Core.Handler;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Module.Infrastructure.Model;
using Machinata.Module.Finance.Model;
using Machinata.Module.Reporting.Logic;
using Machinata.Module.Reporting.Model;
using Newtonsoft.Json.Linq;
using Machinata.Module.Finance.Handler;
using static System.Collections.Specialized.BitVector32;

namespace Machinata.Module.Admin.Handler {


    public class ServicesAPIHandler : CRUDAdminAPIHandler<Service> {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("infrastructure");
        }

        #endregion

        protected override void CreatePopulate(Service entity) {
            AutoSetMonthlyYearlyIfPossible(entity);
        }

        protected override void EditPopulate(Service entity) {
            AutoSetMonthlyYearlyIfPossible(entity);
        }
        

        private static void AutoSetMonthlyYearlyIfPossible(Service entity) {

            // calculate from yearly
            if (entity.MonthlyCost.HasValue == false) {
                if (entity.YearlyCost.HasValue) {
                    entity.MonthlyCost = entity.YearlyCost / 12;
                }
            }
           
            // calculate from monthly
            if (entity.YearlyCost.HasValue == false) {
                if (entity.MonthlyCost.HasValue) {
                    entity.YearlyCost = entity.MonthlyCost * 12;
                }
            }
            // set both to 0 if neither is set  

            if (entity.MonthlyCost.HasValue == false) {
                entity.MonthlyCost = new Price(0);
             
            }

            if (entity.YearlyCost.HasValue == false) {
                entity.YearlyCost = new Price(0);
            }


        }

        [RequestHandler("/api/admin/infrastructure/services/create")]
        public void Create() {
            CRUDCreate();
        }

        [RequestHandler("/api/admin/infrastructure/services/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);
        }
        
        [RequestHandler("/api/admin/infrastructure/services/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId);
        }



        [RequestHandler("/api/admin/infrastructure/services/report", AccessPolicy.PUBLIC_ARN)]
        public void ProjectReport() {

            var currency = Core.Config.CurrencyDefault;

            var entities = this.DB.Services()
                  .Include(nameof(Service.Business)).ToList()
                  .AsQueryable();

            entities = ServicesAdminHandler.FilterEntities(entities, this, null);
            entities = entities.ToList().AsQueryable();

            var yearlyCosts = entities.Where(e => e.YearlyCost.HasValue).ToList();
            var monthlyCosts = entities.Where(e => e.MonthlyCost.HasValue).ToList();

            var yearlyCostsSum = yearlyCosts.Any() ? new Price(yearlyCosts.Sum(e => e.YearlyCost.Value.Value)) : new Price(0);
            var monthlyCostsSum = monthlyCosts.Any()? new Price(monthlyCosts.Sum(e => e.MonthlyCost.Value.Value)) : new Price(0);
            var activeYearlyCostsSum = yearlyCosts.Any() ? new Price(yearlyCosts.Where(c=>c.Active).Sum(e => e.YearlyCost.Value.Value)) : new Price(0);
            var activeMonthlyCostsSum = monthlyCosts.Any() ? new Price(monthlyCosts.Where(c => c.Active).Sum(e => e.MonthlyCost.Value.Value)) : new Price(0);

            const int donutSlices = 7;

            // Report
            var report = new Report();
            var chapter = report.NewChapter("");


            // Metrics
            {

                var layoutNode = new LayoutNode();
                layoutNode.Screen = true;
                layoutNode.TwoColumns();

                chapter.AddChild(layoutNode);
                {
                    var node = new MetricsNode();
                    //node.Growing();
                    node.Theme = "default";
                    node.SetTitle("Total Costs");
                   // node.SetSubTitle("yearly" + CostsOverTimeReports.GetUnitSuffix(currency));
                    {
                        var group = node.AddGroup();
                        group.CreateItem()
                            .SetLabel("Active Services")
                            .SetValue(activeYearlyCostsSum.ToString())
                            .SetSubValue("yearly".ToString());
                    }
                    {
                        var group = node.AddGroup();
                        group.CreateItem()
                            .SetLabel("Active Services")
                            .SetValue(activeMonthlyCostsSum.ToString())
                        .SetSubValue("monthly".ToString());
                    }

                    layoutNode.AddLeft(node);
                }

               if (false) {
                    var node = new MetricsNode();
                    //node.Growing();
                    node.Theme = "default";
                    node.RandomID();
                    node.SetTitle("Proposals");
                    node.SetSubTitle("last 3 Month" + CostsOverTimeReports.GetUnitSuffix(currency));

                    {
                        var group = node.AddGroup();
                        //group.CreateItem()
                        //    .SetLabel("Pending")
                        //    .SetValue(pendingLast3.Sum(p => p.Budget.Value).Value.ToString(Reporting.Config.NumberThousandsFormatNet, Reporting.Config.FormattingCulture))
                        //    .SetSubValue(pendingLast3.Count().ToString());
                    }

                    layoutNode.AddRight(node);
                }
            }

                // Monthly Costs
                {
                var layoutNode = new LayoutNode();
                layoutNode.Style = "CS_W_Two_Columns";
                chapter.AddChild(layoutNode);

                // Left
                {
                    var toggle = new ToggleNode();
                    {
                        var node = new DonutNode();
                        node.ToggleTitle = "Yearly";
                        node.MainTitle = "Yearly Costs";
                        node.SubTitle = "by Category, in %";
                        node.InsertGroupedEntitiesAsPercentageSlices(entities,g =>g.Category.ToString(),g=>g.YearlyCostsSafe.Value.Value ,yearlyCostsSum.Value.Value, donutSlices, x=>Reporting.Config.GetNumberThousandsCurrency(x));
                        toggle.AddChild(node);
                        node.RemoveZeroValueFacts();
                    }


                    {
                        var node = new DonutNode();
                        node.ToggleTitle = "Monthly";
                        node.MainTitle = "Monthly Costs";
                        node.SubTitle = "by Category, in %";
                        node.InsertGroupedEntitiesAsPercentageSlices(entities, g => g.Category.ToString(), g => g.MonthlyCostsSafe.Value.Value, yearlyCostsSum.Value.Value, donutSlices, x => Reporting.Config.GetNumberThousandsCurrency(x));
                        toggle.AddChild(node);
                        node.RemoveZeroValueFacts();
                    }

                    layoutNode.AddChildToSlot(toggle, "{left}");
                }

                // Right
                {
                    var toggle = new ToggleNode();

                    // Yearly pie
                    {
                        var node = new DonutNode();
                        node.ToggleTitle = "Yearly";
                        node.MainTitle = "Yearly Costs";
                        node.SubTitle = "by Company, in %";
                        node.InsertGroupedEntitiesAsPercentageSlices(entities, g => g.Company?.ToString(), g => g.YearlyCostsSafe.Value.Value, yearlyCostsSum.Value.Value, donutSlices, x => Reporting.Config.GetNumberThousandsCurrency(x));
                        toggle.AddChild(node);
                        node.RemoveZeroValueFacts();
                        
                        //layoutNode.AddChildToSlot(node, "{right}");
                    }

                    // Monthly pie
                    {
                        var node = new DonutNode();
                        node.ToggleTitle = "Monthly";
                        node.MainTitle = "Monthly Costs";
                        node.SubTitle = "by Company, in %";
                        node.InsertGroupedEntitiesAsPercentageSlices(entities, g => g.Company?.ToString(), g => g.MonthlyCost.Value.Value, yearlyCostsSum.Value.Value, donutSlices, x => Reporting.Config.GetNumberThousandsCurrency(x));
                        toggle.AddChild(node);
                        node.RemoveZeroValueFacts();
                    }

                    layoutNode.AddChildToSlot(toggle, "{right}");
                }

               
                layoutNode.AutoBalanceChildrenLegends();
            }

           

                this.SendAPIMessage("node-data", JObject.FromObject(report));
        }

        
    }
}
