using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Charts;
using Machinata.Core.Builder;
using System.Linq;
using System;
using Machinata.Module.Infrastructure.Model;
using System.Collections.Generic;
using Machinata.Module.Reporting.Model;
using Newtonsoft.Json.Linq;
using Machinata.Module.Reporting.Extensions;

namespace Machinata.Module.Infrastructure.Handler {
    
    public class InfrastructureReportingApiHandler : Module.Admin.Handler.AdminAPIHandler {

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("infrastructure");
        }

        [RequestHandler("/api/admin/infrastructure/reporting/donut/devices/category")]
        public void DonutDevicesCategories() {
            
            var node = new DonutNode();
            node.DefaultTheme();
            node.SetTitle("Devices by Category");
            var devices = this.DB.Devices().Where(d=>d.Enabled);
            var cats = devices.GroupBy(t => t.Category).Select(g => new { Category = g.Key, Count = g.Count() });
            var serie = node.AddSerie("Categories");
            foreach (var cat in cats) {
                //data.Items.Add(new DonutChartDataItem() { Name = cat.Category.ToString(), Title = cat.Category.ToString(), Value = cat.Count });
                serie.AddFactAbsolute(cat.Count, cat.Category.ToString());

            }
            this.SendReportingAPIMessage(node);
        }

        [RequestHandler("/api/admin/infrastructure/reporting/donut/devices/status")]
        public void DonutDevicesStatuses() {

            var node = new DonutNode();
            node.DefaultTheme();
            node.SetTitle( "Devices by Status");
            var devices = this.DB.Devices().Where(d => d.Enabled);
            var cats = devices.GroupBy(t => t.Status).Select(g => new { Status = g.Key, Count = g.Count() });
            var serie = node.AddSerie("Statuses");
            foreach (var cat in cats) {
                //data.Items.Add(new DonutChartDataItem() { Name = cat.Status.ToString(), Title = cat.Status.ToString(), Value = cat.Count });
                serie.AddFactAbsolute(cat.Count, cat.Status.ToString());
            }
            this.SendReportingAPIMessage(node);
        }


        [RequestHandler("/api/admin/infrastructure/reporting/map/locations")]
        public void MapShopConfirmed() {
            var devices = this.DB.Devices()
                .Include(nameof(Model.Device.Location) + "." + nameof(Location.Address))
                .Where(d => d.Location != null);


            var node = new OnlineMapNode();

            foreach (var device in devices) {
                var address = device.Location.Address.FormatAsString(",", "Name");
                address = Core.Localization.TextParser.ReplaceTextVariablesForData("Machinata.Core", address, "en");//TODO
                //var dp = new MapChartDataMarker() {
                //    Name = device.Name + "@" + device.Location.LocationName,
                //    Address = address,
                //    Link = "/admin/infrastructure/devices/device/" + device.PublicId
                //};
                //if (false) data.Heat.Add(dp);
                //else data.Markers.Add(dp);
                node.AddGeoFact(address, device.Name + "@" + device.Location.LocationName, "/admin/infrastructure/devices/device/" + device.PublicId);

            }

            this.SendReportingAPIMessage(node);
        }

        #region Infrastructure Locations->Devices->Accounts Tree


        [RequestHandler("/api/admin/infrastructure/chart/tree/locations")]
        public void TreeLocations() {

            // Locations
            var locations = this.DB.Locations().Include(nameof(Location.Devices));

            // Create the root element
            var treeRoot = new Core.Charts.TreeChartDataNode() {
                Name = "Locations",
                ChildrenCall = $"/api/admin/infrastructure/chart/tree/locations"
            };

            // Add all children
            foreach (var location in locations) {
                var treeChild = new Core.Charts.TreeChartDataNode() {
                    Name = location.LocationName,
                    ChildrenCall = $"/api/admin/infrastructure/chart/tree/location/" + location.PublicId 
                };
                treeRoot.Children.Add(treeChild);
            }

            SendAPIMessage("tree-data", treeRoot);
        }

        [RequestHandler("/api/admin/infrastructure/chart/tree/location/{publicid}")]
        public void TreeLocation(string publicid) {

            // Locations
            var location = this.DB.Locations().Include(nameof(Location.Devices)).GetByPublicId(publicid);

            // Create the root element
            var treeRoot = new Core.Charts.TreeChartDataNode() {
                Name = location.LocationName,
                ChildrenCall = $"/api/admin/infrastructure/chart/tree/location/" + location.PublicId
            };

            // Add all children
            foreach (var category in Core.Util.EnumHelper.GetEnumValues<Device.DeviceCategories>(typeof(Device.DeviceCategories))) {
                var treeChild = new Core.Charts.TreeChartDataNode() {
                    Name = category.ToString(),
                    ChildrenCall = $"/api/admin/infrastructure/chart/tree/location/"  +location.PublicId + "/category/" + category.ToString()
                };
                treeRoot.Children.Add(treeChild);
            }

            SendAPIMessage("tree-data", treeRoot);
        }

        [RequestHandler("/api/admin/infrastructure/chart/tree/device/{publicid}")]
        public void Device(string publicid) {
            var isAccount = this.Params.Bool("account", false);

            // Locations
            var device = this.DB.Devices()
                .Include(nameof(Model.Device.Accounts)).GetByPublicId(publicid);

            // Create the root element
            var treeRoot = new Core.Charts.TreeChartDataNode() {
                Name = device.Name,
                ChildrenCall = $"/api/admin/infrastructure/chart/tree/device/" + device.PublicId
            };

            if (!isAccount) {
                // Add all children
                foreach (var account in device.Accounts) {
                    var treeChild = new Core.Charts.TreeChartDataNode() {
                        Name = account.Name,
                        ChildrenCall = $"api/admin/infrastructure/chart/tree/device/{publicid}?account=true"
                    };
                    treeRoot.Children.Add(treeChild);
                }
            }
            SendAPIMessage("tree-data", treeRoot);
        }

        [RequestHandler("/api/admin/infrastructure/chart/tree/location/{publicid}/category/{category}")]
        public void TreeLocationCategory(string publicid, string category) {

            // Locations
            var location = this.DB.Locations().Include(nameof(Location.Devices)).GetByPublicId(publicid);
            var categoryParsed = EnumHelper.ParseEnum<Device.DeviceCategories>(category);

            // Create the root element
            var treeRoot = new Core.Charts.TreeChartDataNode() {
                Name = location.LocationName,
                ChildrenCall = $"/api/admin/infrastructure/chart/tree/location/" + location.PublicId + "/" + category
            };

            // Add all children
            foreach (var device in location.Devices.Where(d=>d.Category == categoryParsed)) {
                var treeChild = new Core.Charts.TreeChartDataNode() {
                    Name = device.Name,
                    ChildrenCall = $"/api/admin/infrastructure/chart/tree/device/" + device.PublicId
                };
                treeRoot.Children.Add(treeChild);
            }

            SendAPIMessage("tree-data", treeRoot);
        }

        #endregion

    }
}
