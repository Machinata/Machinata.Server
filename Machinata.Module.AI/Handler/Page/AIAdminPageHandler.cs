
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Core.Templates;
using Machinata.Core.Exceptions;


namespace Machinata.Module.AI.Handler {


    public class AIAdminPageHandler : AdminPageTemplateHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("ai");
        }

        #endregion

        #region Menu 

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "artificial-intelligence",
                Path = "/admin/ai",
                Title = "{text.ai}",
                Sort = "500"
            });
        }

        #endregion


        [RequestHandler("/admin/ai")]
        public void Default() {


            var menuItems = PageTemplate.Cache.FindAll(this.Template.Package, "admin/ai/menu/menu.item.", this.TemplateExtension);
            this.Template.InsertTemplates("ai.menu-items", menuItems);

            // Navigation
            this.Navigation.Add("ai");
        }

        [RequestHandler("/admin/ai/text-generation")]
        public void TextGeneration() {

            // Navigation
            this.Navigation.Add("ai");
            this.Navigation.Add("text-generation");
        }

        [RequestHandler("/admin/ai/image-generation")]
        public void ImageGeneration() {

            // Navigation
            this.Navigation.Add("ai");
            this.Navigation.Add("image-generation");
        }


    }
}
