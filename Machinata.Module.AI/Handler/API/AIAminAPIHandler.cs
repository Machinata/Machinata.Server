﻿using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Module.Admin.Handler;
using Machinata.Module.Services.NLP.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.AI.Handler.API {
    public class AIAminAPIHandler : AdminAPIHandler {

        private NLPProvider _getProvider() {
            return Services.NLP.NLPService.Default;
        }

        [RequestHandler("/api/admin/ai/text-completion", AccessPolicy.PUBLIC_ARN)]
        public void TextCompletion() {
            var provider = _getProvider();
            var model = this.Params.String("model", provider.GetDefaultModel());
            var prompt = this.Params.String("prompt");
            var temperature = this.Params.Double("temperature", provider.GetDefaultTemperature());
            var maxTokens = this.Params.Int("max-tokens", provider.GetDefaultMaxTokens());

            var ret = provider.GetTextCompletion(prompt, model, temperature, maxTokens);

            SendAPIMessage("nlp-text-completion", ret);
        }

        [RequestHandler("/api/admin/ai/image-creation", AccessPolicy.PUBLIC_ARN)]
        public void CreateImage() {
            var provider = _getProvider();
            var prompt = this.Params.String("prompt");
            var size = this.Params.String("size", null);
            var number = this.Params.Int("number", 1);

            var ret = provider.CreateImage(prompt, number, size);

            SendAPIMessage("nlp-image-creation", ret);
        }

        [RequestHandler("/api/admin/ai/text-moderation", AccessPolicy.PUBLIC_ARN)]
        public void CreateModeration() {
            var provider = _getProvider();
            var input = this.Params.String("input");
            var ret = provider.CreateModeration(input);
            SendAPIMessage("nlp-text-moderation", ret);
        }

    }
}
