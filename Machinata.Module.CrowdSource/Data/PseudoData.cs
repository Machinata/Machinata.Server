﻿using Machinata.Core.Exceptions;
using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Module.CrowdSource.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.CrowdSource.Data {
    public class PseudoData {

        public static void CreateVotes(Project project, int count) {

            CheckTestSystem();
            var r = new System.Random();

            var min = r.Next(-100, -1);
            var max = r.Next(1, 100);
            for (int i = 0; i < count; i++) {

                var vote = new Vote();


                var uncapped = r.Next(min, max);
                vote.Value = uncapped >= 0 ? 1 : -1;
                vote.Changed = DateTime.UtcNow.AddHours(uncapped);
                vote.UserID = Guid.NewGuid().ToString();

                project.Votes.Add(vote);

            }

        }



        public static IList<Project> CreateProjects(ModelContext db, int count) {

            CheckTestSystem();

            var projects = new List<Project>();

            var categories = db.Categories().AsQueryable();
            var tags = db.Tags().AsQueryable();

            if (categories.Count() == 0) {
                throw new Exception("No Categories defined");
            }

            if (tags.Count() == 0) {
                throw new Exception("No Tags defined");
            }

            if (tags.Count() == 0) {
                throw new Exception("No Tags defined");
            }

            if (string.IsNullOrWhiteSpace(GetRandomImage(db))== true) {
                throw new Exception("No Tags defined");
            }
           

            var r = new System.Random();

            var min = r.Next(-100, -1);
            var max = r.Next(1, 100);
            for (int i = 0; i < count; i++) {

                var project = new Project();

                project.Context = db;
                // project.Status = Core.Util.EnumHelper.GetEnumValues<Project.ProjectStatuses>(typeof(Project.ProjectStatuses)).OrderBy(e => Guid.NewGuid()).FirstOrDefault();

                project.Status = Project.ProjectStatuses.Voting;
                project.Name = $"{Core.Util.PlaceholderText.RandomWord()} {Core.Util.PlaceholderText.RandomWord()} {Core.Util.PlaceholderText.RandomWord()}".ToTitleCase();
                project.ShortURL = Core.Util.String.CreateShortURLForName(project.Name);

                project.TimeRange = new DateRange(DateTime.UtcNow.AddHours(min), DateTime.UtcNow.AddHours(max));
                project.Published =( min + i )% 2  == 0;
                project.SetModified();


                var randomTags = db.Tags().ToList().OrderBy(t => Guid.NewGuid().ToString());

                project.Category = categories.ToList().Random();
                project.Tags = new List<Tag>();
            
                var numberTags = r.Next(2, (int)((int)tags.Count() * 0.8));
                var maxTags = 6;
                numberTags = numberTags > maxTags ? maxTags : numberTags;


                foreach (var tag in randomTags.Take(numberTags)) {
                    project.Tags.Add(tag);
                }


                // Meta Tags
                project.GenerateMissingMetaTags(db);


                db.Projects().Add(project);
                db.SaveChanges();

                var image = GetRandomImage(db);
                var thumb = GetRandomImage(db);
                {
                    var description = Core.Util.PlaceholderText.GetRandomLoremIpsumText();
                    project.Description = ContentNode.CreateContentNodeForEntityProperty(db, project, nameof(Project.Description), description);
                    project.Description.Summary = Core.Util.String.CreateSummarizedText(description, 100, false);
                }

                {
                    var summary = Core.Util.PlaceholderText.GetRandomLoremIpsumText();
                    summary = Core.Util.String.CreateSummarizedText(summary, 100, false);
                    project.Summary = ContentNode.CreateContentNodeForEntityProperty(db, project, nameof(Project.Summary), summary);
                    project.Summary.Summary = Core.Util.String.CreateSummarizedText(summary, 100, false);
                }

                project.Images = ContentNode.CreateContentNodeForEntityProperty(db, project, nameof(Project.Images), image, "image");
                project.Thumbnail = thumb;


                CreateVotes(project, r.Next(0, 200));

                // Fix wrong status based on votes
                if (project.ApprovalRateDecimal > 0.5m && project.Status == Project.ProjectStatuses.Rejected) {
                    project.Status = Project.ProjectStatuses.Accepted;
                }
                if (project.ApprovalRateDecimal < 0.5m && project.Status == Project.ProjectStatuses.Accepted) {
                    project.Status = Project.ProjectStatuses.Rejected;
                }


                projects.Add(project);

            }

            return projects;
        }

        private static void CheckTestSystem() {
            var isSitePublic = Core.Config.Dynamic.Bool("IsSitePublic", false);
            if (isSitePublic == true && Core.Config.IsTestEnvironment == false) {
                throw new BackendException("check-system-error","This is only allowed on the test environment");
            }
        }

        private static string GetRandomImage(ModelContext db) {
            return GetSampleImages(db).AsQueryable().OrderBy(e => Guid.NewGuid()).FirstOrDefault();
        }

        private static List<string> _sampleImages = null;

        private static IEnumerable<string> GetSampleImages(ModelContext db, string imagesPath ="/PseudoData/Project Images") {

            if (_sampleImages == null) {
                var imagesNode = ContentNode.GetByPath(db, imagesPath);

                if (imagesNode == null) {
                    throw new Exception($"Please add pseudo project images under {imagesPath} (in sub pages)");
                }

                var images = new List<string>();

                if (imagesNode != null) {
                    imagesNode.IncludeContent();
                    foreach (var node in imagesNode.ChildrenForType(ContentNode.NODE_TYPE_NODE)) {

                        node.IncludeContent();
                        foreach (var transes in node.ChildrenForType(ContentNode.NODE_TYPE_TRANSLATION)) {

                            foreach (var image in transes.ChildrenForType(ContentNode.NODE_TYPE_IMAGE)) {
                                images.Add(image.Value);

                            }
                        }

                    }
                }

                _sampleImages = images;
            }


            return _sampleImages;

        }
    }
}
