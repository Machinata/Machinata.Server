﻿using Machinata.Core.Model;
using Machinata.Core.Util;
using Machinata.Module.CrowdSource.Model;
using Machinata.Module.Reporting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.CrowdSource.Logic {
    public static class Statistics {


        /// <summary>
        /// Published Only
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static IQueryable<Project> RunningProjects(IQueryable<Project> projects) {
            return projects.Published();

        }


        public static int AvarageVotesPerProject(IQueryable<Project> projects) {
            return projects.SelectMany(p => p.Votes).Count() / projects.Count();

        }

        public static IQueryable<Project> ProjectsByCategory(IQueryable<Project> projects, string categoryShortURL) {
            return projects.Where(p => p.Category.ShortURL == categoryShortURL);
        }


        public static IQueryable<IGrouping<Category,Project>> ProjectsByCategories(IQueryable<Project> projects) {
            return projects.GroupBy(p => p.Category);
        }

        public static Dictionary<DateTime,IEnumerable<Vote>> VotesOverTime(IQueryable<Project> projects) {
            var votes = projects.SelectMany(p => p.Votes).ToList();

            var result = new Dictionary<DateTime, IEnumerable<Vote>>();

            var minDate = votes.Select(v => v.Changed).Min();
            var maxDate = votes.Select(v => v.Changed).Max();

            var minDateLocalDate = Core.Util.Time.ConvertToDefaultTimezone(minDate).Date;

            // TODO LOCAL TIME DATES
            for (int i = 0; i< (maxDate - minDate).TotalDays; i++) {
                var date = minDateLocalDate.AddDays(i);
                var votesDay = votes.Where(v => v.LocalChangedDate == date);
                result[date] = votesDay;
            }

            return result;

        }

        public static int TotalPublishedProjects(IQueryable<Project> projects) {
            return projects.Published().Count();
        }


        public static VBarNode VotesOverTimeChart(IQueryable<Project> projects) {

            var vbarnode = new VBarNode();
            vbarnode.Theme = "rating";
            vbarnode.SetTitle("Votes");
            vbarnode.SetSubTitle("over time");

            vbarnode.StackedSeries = true;
            vbarnode.Series = new List<CategorySerie>();


            var votes = projects.SelectMany(p => p.Votes).ToList();

            var result = new Dictionary<DateTime, IEnumerable<Vote>>();

            DateTime minDate = DateTime.UtcNow;
            DateTime maxDate = DateTime.UtcNow;

            if (votes.Any()) {
                minDate = votes.Select(v => v.Changed).Min();
                maxDate = votes.Select(v => v.Changed).Max();
            }

            var minDateLocalDate = Core.Util.Time.ConvertToDefaultTimezone(minDate).Date;

            var positiveSerie = new CategorySerie();

            positiveSerie.Id = nameof(positiveSerie);
            positiveSerie.Title = "Upvotes";
            positiveSerie.ColorShade = "positive2";
            positiveSerie.YAxis = new Axis();
            positiveSerie.YAxis.Format = "1f";
            vbarnode.Series.Add(positiveSerie);


            var negativeSerie = new CategorySerie();

            negativeSerie.Id = nameof(negativeSerie);
            negativeSerie.Title = "Downvotes";
            negativeSerie.ColorShade = "negative2";
            positiveSerie.YAxis.Format = "1f";
            vbarnode.Series.Add(negativeSerie);


            // TODO LOCAL TIME DATES
            for (int i = 0; i < (maxDate - minDate).TotalDays && votes.Any(); i++) {
                var date = minDateLocalDate.AddDays(i);
                var votesDay = votes.Where(v => v.LocalChangedDate == date);
                //  result[date] = votesDay;

                {
                    var posVotes = votesDay.Where(v => v.IsPositive).Count();
                    var posFact = new CategoryFact(posVotes, posVotes.ToString("0.##"), date.ToDateString());
                    positiveSerie.Facts.Add(posFact);
                }

                {
                    var negVotes = votesDay.Where(v => v.IsNegatvie).Count();
                    var negFact = new CategoryFact(negVotes, negVotes.ToString("0.##"), date.ToDateString());
                    negativeSerie.Facts.Add(negFact);
                }





            }

            return vbarnode;

        }


        public static IEnumerable<Vote> PositiveVotes(this Project project) {
            return project.Votes.Where(v => v.IsPositive);
        }

        public static IEnumerable<Vote> NegativeVotes(this Project project) {
            return project.Votes.Where(v => v.IsPositive);
        }

     
    }
}
