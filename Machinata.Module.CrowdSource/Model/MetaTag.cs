using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Text;
using Machinata.Core.Cards;


using Machinata.Core.Lifecycle;

namespace Machinata.Module.CrowdSource.Model {

    [Serializable()]
    [ModelClass]
    [Table("CrowdSourceMetaTags")]
    public class MetaTag : ModelObject {

        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////

        public enum RelationTypes {
            ToMany, // (at least one)
            ToSingle
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public MetaTag() {
            this.Infos = new Properties();
        }


        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////


        /// <summary>
        /// Title
        /// </summary>
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Title { get; set; }

      
        /// <summary>
        /// Type name for implementations
        /// </summary>
        [FormBuilder(Forms.System.JSON)]
        [Column]
        [Required]
        public string Type { get; set; }

       

        /// <summary>
        /// Additional infos
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [CascadeDelete]
        [Column]
        public Properties Infos { get; set; }


        /// <summary>
        /// This holds the json of the concrete content type.
        /// Properties of implementations are mapped to this.
        /// </summary>
        [Column]
        [DefaultPropertyMappingJSONStore]
        public string ImplementationJSON { get; set; }


        /// <summary>
        /// Sort in context of Type
        /// </summary>
        [Column]
        [FormBuilder()]
        public int Sort { get; set; }

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        

        [Column]
        public Project Project { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder()]
        public string TypeFriendlyName {
            get {
                return GetFriendlyTypeName(this.Type);
            }
        }

      
        [FormBuilder()]
        public string AdminTitle {
            get {
                if (this.RelationshipType == RelationTypes.ToSingle) {
                    return this.TypeFriendlyName;
                } else if (RelationshipType == RelationTypes.ToMany) {
                    if (string.IsNullOrWhiteSpace(this.Title) == false) {
                        return this.TypeFriendlyName + ": " + this.Title;
                    }
                    return this.TypeFriendlyName;
                }
                throw new Exception("Relationship title not implemented");
            }
        }
        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        public MetaTag LoadImplementation() {

            return ModelObjectTypeImplementer<MetaTag>(nameof(Type)).Load();

        }

        /// <summary>
        /// Can this meta tag be deleted
        /// </summary>
        /// <param name="project"></param>
        /// <param name="throwException"></param>
        /// <returns></returns>
        public bool TestCanDelete(Project project, bool throwException = true) {
            var allOfType = project.GetMetaTagsByTypeName(this.Type);

            var impl = this.LoadImplementation();
            if (impl.RelationshipType == MetaTag.RelationTypes.ToSingle) {
                if (throwException == true) {
                    throw new BackendException("metatag-delete-toone", "MetaType with RelationType Single cannot be deleted");
                }
                return false;
            }

            if (impl.RelationshipType == MetaTag.RelationTypes.ToMany) {
                if (allOfType.Count() <= 1) {
                    if (throwException == true) {
                        throw new BackendException("metatag-delete-tomany", "MetaType with RelationType Many cannot be deleted if there is only one left");
                    }
                    return false;
                }
            }

            return true;
        }

        public void SaveImplementation(MetaTag impl) {
            ModelObjectTypeImplementer<MetaTag>(nameof(Type)).Save(impl);
        }



        public static IEnumerable<Type> GetMetaTagTypes() {
            var types = Core.Reflection.Types.GetMachinataTypes(typeof(MetaTag));
            return types.Where(t=>t.IsAbstract == false && t != typeof(MetaTag));
        }

        /// <summary>
        /// This initiliazes a single MetaTagImpl 
        /// </summary>
        /// <param name="dB"></param>
        public virtual void Initialize(ModelContext dB) {
            // see implementations
        }


        /// <summary>
        /// If the relation is  Project to many MetaTagType, the first implementation has to act as the initializeer of all desired instances fo the MetaTagTypeImpl 
        /// </summary>
        /// <param name="dB"></param>
        /// <returns></returns>
        public virtual IEnumerable<MetaTag> InitializeMultiple(ModelContext dB) {
            return new List<MetaTag>();
        }


        public static string GetFriendlyTypeName(string typeName) {
           return typeName.ReplacePrefix("MetaTag", "");
        }


        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////

        public virtual RelationTypes RelationshipType { get; } = RelationTypes.ToSingle ;

        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////


    public static class ModelContexMetaTagExtensions {
        [ModelSet]
        public static DbSet<MetaTag> MetaTags(this Core.Model.ModelContext context) {
            return context.Set<MetaTag>();
        }
    }

    #endregion



}


