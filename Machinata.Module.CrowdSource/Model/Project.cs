using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Web;

namespace Machinata.Module.CrowdSource.Model {

    [Serializable()]
    [ModelClass]
    [Table("CrowdSourceProjects")]
    public partial class Project : ModelObject, IPublishedModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////

        public enum ProjectStatuses : short {
           // Draft = 10,
            Voting = 20,
            Accepted = 30,
            Rejected = 40
        }

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Project() {
            this.TimeRange = new DateRange();
            this.Votes = new List<Vote>();
            this.MetaTags = new List<MetaTag>();
            this.Tags = new List<Tag>();


        }

      
        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

       
        
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        [MinLength(3)]
        [Required]
        public string Name { get; set; }

        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        [MinLength(3)]
        [MaxLength(200)]
        public string ShortURL { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public bool Published { get; set; }


        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        [Required]
        public ProjectStatuses Status { get; set; }


        [FormBuilder(Forms.Admin.VIEW)]
        //[FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public DateTime Modified { get; set; }

        [Column]
        [FormBuilder()]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.VIEW)]
        [DataType("latlon")]
        public string Location { get; set; }


        [Column]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [DataType(DataType.Date)] 
        public DateRange TimeRange { get; set; }


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        [DataType(DataType.ImageUrl)]
        public string Thumbnail { get; set; }


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        [DataType(DataType.ImageUrl)]
        public string Icon { get; set; }


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [CascadeDelete]
        [Column]
        [ContentType("html")]
        public ContentNode Summary { get; set; }


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [CascadeDelete]
        [Column]
        [ContentType("html")]
        public ContentNode Description { get; set; }


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [CascadeDelete]
        [Column]
        [ContentType("image")]
        public ContentNode Images { get; set; }


       

        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        


        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        public User User { get; set; }
        
        
        [Column]
        public virtual ICollection<Tag> Tags { get; set; }

        [Column]
        public virtual ICollection<MetaTag> MetaTags { get; set; }

        [Column]
        public virtual ICollection<Vote> Votes { get; set; }


        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [Column]
        [Required]
        public virtual Category Category { get; set; }

        


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [FormBuilder(Forms.API.LISTING)]
        [NotMapped]
        public DateTime? Start {
            get {
                return this.TimeRange.Start;
            }
        }
        
        [FormBuilder(Forms.API.LISTING)]
        [NotMapped]
        public DateTime? End {
            get {
                return this.TimeRange.End;
            }
        }

        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [NotMapped]
        public string PublicURL {
            get {

                var path = Config.CrowdSourceProjectPath;
                var url = Core.Config.PublicURL;
                if (string.IsNullOrWhiteSpace(this.ShortURL) == false) {
                    path = path.Replace("{short-url}", this.ShortURL);
                }
                if (string.IsNullOrWhiteSpace(this.PublicId) == false) {
                    path = path.Replace("{publicid}", this.PublicId);
                }
                if (string.IsNullOrWhiteSpace(this.PublicId) == false) {
                    path = path.Replace("{public-id}",  this.PublicId);
                }
                return url + path;

            }

        }

        [FormBuilder]
        [NotMapped]
        public string PDFDownloadURL {
            get {
                // A3 x 4, in pixels (not really A3, but in mm the mode is totally different)
                var width = 297 * 4;
                var height = 420 * 4;
                var url = new UriBuilder(Core.Config.PublicURL + "/static/headless/screenshot");
                url.Query = "format=pdf&full-page=true&width="+width+"&height="+height+"&cache-version="+Core.Config.BuildGUID +"-"+ this.Modified.Ticks+"&url=" + HttpUtility.UrlEncode(this.PublicURL+ "?headless=true&cache-version=" + Core.Config.BuildGUID + "-" + this.Modified.Ticks + "&auth-secret={headless-auth-secret}");
                return url.ToString();
            }

        }


        [FormBuilder(Forms.API.LISTING)]
        [NotMapped]
       // [Decimal("0%")]
        public decimal? ApprovalRateDecimal {
            get {
                if (this.Votes.Any()) {
                    return Decimal.Round( (decimal)this.Votes.Count(v => v.IsPositive) / (decimal)this.Votes.Count(), 2);
                }
                return 0;

            }
        }

        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.LISTING)]
        [NotMapped]
        public string ApprovalRate {
            get {

                if (this.ApprovalRateDecimal.HasValue) {
                   return this.ApprovalRateDecimal.Value.ToString("0%");
                }
                return null;
            }
        }

        [FormBuilder(Forms.API.LISTING)]
        [NotMapped]
        public decimal Upvotes {
            get {
                return this.Votes.Count(v => v.IsPositive);
            }
        }

        [FormBuilder(Forms.API.LISTING)]
        [NotMapped]
        public decimal Downvotes {
            get {
                return this.Votes.Count(v => v.IsNegatvie);
            }
        }

        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [NotMapped]
        public decimal NumberVotes {
            get {
                return this.Votes.Count();
            }
        }

        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.LISTING)]
        [NotMapped]
        public bool HasVotes {
            get {
                return this.Votes.Count > 0;
            }
        }

        [FormBuilder(Forms.API.LISTING)]
        [NotMapped]
        public string APICallUpvote {
            get {
                return APIBaseURL() + "/upvote";
            }
        }

        private string APIBaseURL() {
            return Config.CrowdSourceProjectsAPIPath.Replace("{project-id}", this.PublicId);
        }

        [FormBuilder(Forms.API.LISTING)]
        [NotMapped]
        public string APICallDownvote {
            get {
                return APIBaseURL() + "/downvote";
            }
        }

        [NotMapped]
        public bool IsOpenForVoting {

            get {
                return (/*this.Status == ProjectStatuses.Voting &&*/ this.Published == true);
            }
        }

        [FormBuilder(Forms.API.LISTING)]
        [NotMapped]
        public string StatusId {
            get {
                return this.Status.ToString().ToDashedLower();
            }
        }

        [FormBuilder(Forms.API.LISTING)]
        [NotMapped]
        public string DescriptionShort {
            get {
                var rnd = new Random(this.Id);

                if (string.IsNullOrWhiteSpace(this.GetSummary()) == false) {
                    return this.GetSummary();
                }

                // TEMP FALLBACK TODO: auto summarize Description
                //if (string.IsNullOrWhiteSpace(this.GetDescriptionSummary()) == false) {
                //    return Core.Util.String.CreateSummarizedText(
                //        this.GetDescriptionSummary(),
                //        rnd.Next(80, 140),
                //        true,
                //        true,
                //        true
                //    );
                //}
                return null;
            }
        }

        [FormBuilder(Forms.API.LISTING)]
        [NotMapped]
        public bool IsNew {
            get {
                return (this.Created > DateTime.UtcNow.AddDays(-Config.CrowdSourceNewProjectTagMaxDays));
            }
        }

        [FormBuilder(Forms.API.LISTING)]
        [NotMapped]
        public bool IsPopular {
            get {
                return (this.ApprovalRateDecimal > (decimal)Config.CrowdSourcePopularProjectTagMinApprovalRate); 
            }
        }

        [NotMapped]
        public List<Tag> VirtualTags {
            get {
                var tags = new List<Tag>();
                //if (this.IsNew) {
                //    var tag = new Tag();
                //    tag.Name = Core.Localization.Text.GetTranslatedTextById("project-new");
                //    tag.ShortURL = tag.Name.ToDashedLower();
                //    tags.Add(tag);
                //}
                //if (this.IsPopular) {
                //    var tag = new Tag();
                //    tag.Name = Core.Localization.Text.GetTranslatedTextById("project-popular");
                //    tag.ShortURL = tag.Name.ToDashedLower();
                //    tags.Add(tag);
                //}

                // Custom Virtual Tags
                {
                    var methods = Core.Reflection.Methods.GetMachinataMethodsWithAttribute(typeof(ProjectMetaTagsProviderAttribute));
                    foreach (var method in methods) {
                        var results = method.Invoke(null, new object[] { this }) as List<Tag>;
                        foreach (var result in results) {
                            tags.Add(result);
                        }
                    }
                }
                return tags;
            }
        }

        [FormBuilder(Forms.API.LISTING)]
        [NotMapped]
        public string FilterGroups {
            get {
                var groups = new List<string>();
                if (this.Tags != null) {
                    var filterGroups = this.Tags.Select(t => t.FilterGroup);
                    foreach (var filterGroup in filterGroups) groups.Add(filterGroup);
                }
                {
                    var filterGroups = this.VirtualTags.Select(t => t.FilterGroup);
                    foreach (var filterGroup in filterGroups) groups.Add(filterGroup);
                }
                if (this.Category != null) {
                    groups.Add(this.Category.FilterGroup);
                }
                
                return string.Join(",", groups);
            }
        }

        /// <summary>
        /// Cached MetaTags (after first hit, if .MetaTags relation is loaded)
        /// </summary>
        public IEnumerable<MetaTag> MetaTagImplementations {
            get {
                if (this.MetaTags != null) {
                    if (_metaTagImplemntations == null) {
                        _metaTagImplemntations = this.MetaTags.Select(m => m.LoadImplementation());
                    }

                    return _metaTagImplemntations;
                }
                return this.MetaTags;
            }
        }

        private IEnumerable<MetaTag> _metaTagImplemntations = null;


        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Voting logic, with validations and saving/creating Model.Votes
        /// </summary>
        /// <param name="db"></param>
        /// <param name="decision"></param>
        /// <param name="user"></param>
        /// <param name="id">identifies the user, e.g. guid from cookie</param>
        /// <param name="context"></param>
        public void CastVote(ModelContext db, int? decision, User user, string id, HttpContext context, bool onlyUpdate = false) {

            if (decision.HasValue == false) {
                throw new LocalizedException(null, "cast-vote-no-value");
            }

            if (this.Published == false) {
                throw new LocalizedException(null, "project-not-found");
            }

            // Status checks have to be made in the Produt
            bool checkStatus = false;
            if (checkStatus && this.Status != ProjectStatuses.Voting) {
                throw new LocalizedException(null, "cast-vote-project-wrong-status");
            }

            // ID basic chck
            if (string.IsNullOrWhiteSpace(id) == true) {
                throw new LocalizedException(null, "cast-vote-no-userid");
            }

            // Fingerprint?
            var fingerprintProvider = Fingerprint.FingerprintProvider.Default();
            var fingerprintResult = fingerprintProvider.ValidateId(id);


            var vote = db.Votes()
                .FirstOrDefault(v => v.UserID == id && v.Project_Id != null  && v.Project_Id == this.Id);

            if (vote == null) {
                if (onlyUpdate == true) {
                    return;
                } else {
                    vote = new Vote();
                    this.Votes.Add(vote);
                }
            }

            vote.Changed = DateTime.UtcNow;
            vote.UserID = id;
            vote.User = user;
            vote.Value = decision.Value;

            if (fingerprintResult != null) {
                vote.Infos["fingerprint-result"] = fingerprintResult;
            }

            AddMetaDataFromRequest(vote.Infos, context);

           

        }

        public static void AddMetaDataFromRequest(Properties properties, HttpContext context) {
            properties["RemoteIP"] = Core.Util.HTTP.GetRemoteIP(context);
            properties["UserAgent"] = Core.Util.HTTP.GetUserAgent(context);
            properties["UserLanguages"] = Core.Util.HTTP.GetUserLanguagesString(context);
        }

        /// <summary>
        /// Full Richtext Descripintion
        /// </summary>
        /// <returns></returns>
        public string GetDescription() {
            if (this.Description != null) {
                this.Description.IncludeContent();
                
                var trans = this.Description.TranslationForLanguage("*");
                if (trans != null) {
                    var description = trans.ChildrenForType(ContentNode.NODE_TYPE_HTML).FirstOrDefault()?.Value;
                    return description;
                }
            }
            return null;
            
        }


        /// <summary>
        /// Short Summary from dedicated field
        /// </summary>
        /// <returns></returns>
        public string GetSummary() {
            //if (this.Summary != null) {
            //    this.Summary.IncludeContent();

            //    var trans = this.Summary.TranslationForLanguage("*");
            //    if (trans != null) {
            //        var summary = trans.ChildrenForType(ContentNode.NODE_TYPE_HTML).FirstOrDefault()?.Value;
            //        return summary;
            //    }
            //}

            if (this.Summary != null) {
                return this.Summary.Summary;
            }
            return null;

        }

        /// <summary>
        /// Summary of the description node (old projects)
        /// </summary>
        /// <returns></returns>
        //public string GetDescriptionSummary() {
        //    if (this.Description != null) {
        //        return this.Description.Summary;
        //    }
        //    return null;
        //}


        /// <summary>
        /// All images
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ModelObject> GetImages() {
            if (this.Images != null) {
                this.Images.IncludeContent();

                var trans = this.Images.TranslationForLanguage("*");
                if (trans != null) {
                    var images = trans.ChildrenForType(ContentNode.NODE_TYPE_IMAGE);
                    return images;
                }
            }
            return new List<ModelObject>();

        }

        /// <summary>
        /// This generates the missing tags which the project is supposed to have
        /// </summary>
        /// <param name="db"></param>
        public void GenerateMissingMetaTags(ModelContext db) {
            var types = MetaTag.GetMetaTagTypes();
            foreach (var type in types) {
                var baseImpl = new MetaTag();
                baseImpl.Type = type.Name;
                var impl = baseImpl.LoadImplementation();
                if (impl.RelationshipType == MetaTag.RelationTypes.ToSingle) {
                    impl.Initialize(db);
                    baseImpl.SaveImplementation(impl);
                    AddMetaTagIfMissing(baseImpl);
                } else {
                    var tags = impl.InitializeMultiple(db);
                    foreach (var tag in tags) {
                        AddMetaTagIfMissing(tag);
                    }
                }
            }
        }

        public void AddMetaTagIfMissing(MetaTag baseImpl) {
            var metaTags = this.MetaTags.Where(mt => mt.Type == baseImpl.Type && mt.Title == baseImpl.Title);
            if (metaTags.Count() == 0) {
                this.MetaTags.Add(baseImpl);
            }
        }


        /// <summary>
        /// Meta Tags from which type only one per project is used
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MetaTag> GetSingleMetaTags() {
            return this.MetaTags.Select(mt => mt.LoadImplementation()).Where(mt => mt.RelationshipType == MetaTag.RelationTypes.ToSingle);
        }

        /// <summary>
        /// MetaTag types with a Project to Many (MetaTags) relation
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MetaTag> GetMultiMetaTags() {
            return this.MetaTags.Select(mt => mt.LoadImplementation()).Where(mt => mt.RelationshipType == MetaTag.RelationTypes.ToMany);
        }

        /// <summary>
        /// Get all meta tags of a type
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MetaTag> GetMetaTagsByTypeName(string typeName) {
            return this.MetaTags.Select(mt => mt.LoadImplementation()).Where(mt => mt.Type == typeName);
        }



        /// <summary>
        /// Gets all projects orderered by the amount of common tags
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public IEnumerable<Project> GetRelatedProjects(ModelContext db) {

            var tagsIds = this.Tags.Select(t=>t.Id).ToList();
            var byTags = GetAllProjectTags(db);
              

            var inCommon = new Dictionary<Project, int>();
            foreach (var byTag in byTags) {
                if (byTag.Key.Id != this.Id) {
                    var count = byTag.Value.Count(c => tagsIds.Contains(c));
                    inCommon[byTag.Key] = count;
                } 
            }

            return inCommon.OrderByDescending(bt => bt.Value).Select(bt => bt.Key);
        }

        // Mark project as changed (UTC NOW)
        public void SetModified() {
            this.Modified = DateTime.UtcNow;
        }

        // TODO CACHE THIS shorter?
        public static Dictionary<Project, IEnumerable<int>> GetAllProjectTags(ModelContext db) {
            lock (_projectsByTagsLock) {

                if (_projectsByTags == null) {

                    _projectsByTags = db.Projects().Published()
                      .Include(nameof(Project.Description))
                      .Include(nameof(Project.Tags))
                      .Include(nameof(Project.Votes))
                      .ToDictionary(
                    p => p, p => p.Tags.Select(t => t.Id));
                }
                return _projectsByTags;
            }
        }

        private static Dictionary<Project, IEnumerable<int>> _projectsByTags = null;
        private static object _projectsByTagsLock = new object();


        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////


        [OnModelCreating]
        private static void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder) {
            //https://stackoverflow.com/questions/64919574/change-name-of-generated-join-table-many-to-many-ef-core-5
            //modelBuilder.Entity<Project>()
            // .HasMany(left => left.Tags)
            // .WithMany(right => right.Projects).ma
            // .UsingEntity(join => join.ToTable("TagProjects"));

        }


        public override void OnDelete(ModelContext db) {
            base.OnDelete(db);

            this.Include(nameof(Project.Votes));

            var votes = this.Votes.ToList();
            foreach(var vote in votes) {
                db.DeleteEntity(vote);
            }

            var metaTags = this.MetaTags.ToList();
            foreach (var metaTag in metaTags) {
                db.DeleteEntity(metaTag);
            }
        }


        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContextProjectExtensions {

        [ModelSet]
        public static DbSet<Project> Projects(this Core.Model.ModelContext context) {
            return context.Set<Project>();
        }


        /// <summary>
        /// This includes all relations including ContentNodes and their children
        /// </summary>
        /// <param name="set"></param>
        /// <returns></returns>
        public static IQueryable<Project> IncludeAll(this DbSet<Project> set) {
            var ret =  set.Include(nameof(Project.User))
                 
                      .Include(nameof(Project.Category))
                      .Include(nameof(Project.Description) /*+ "." + nameof(ContentNode.Children) + "." + nameof(ContentNode.Children) + "." + nameof(ContentNode.Children)*/)
                      .Include(nameof(Project.Images))
                 
                      .Include(nameof(Project.Votes)  + "." + nameof(Vote.User))
                      .Include(nameof(Project.Tags));

            // EF BUG: do this seperatly otherwise it will throw exceptions
            ret.Include(nameof(Project.Summary));
            ret.Include(nameof(Project.MetaTags));
            
            return ret;
        }

        /// <summary>
        /// This includes all relations including ContentNodes and their children
        /// </summary>
        /// <param name="set"></param>
        /// <returns></returns>
        public static IQueryable<Project> IncludeForListing(this DbSet<Project> set) {
            var ret = set.Include(nameof(Project.User))
                      .Include(nameof(Project.Category))
                      .Include(nameof(Project.Votes))
                      .Include(nameof(Project.Tags))
                      .Include(nameof(Project.MetaTags));

            // EF BUG: do this seperatly otherwise it will throw exceptions
            ret.Include(nameof(Project.Summary));
        

            return ret;
        }


        public static  Project GetProjectByShortURL(this IQueryable<Project> projects, string shortUrl) {
            var project =  projects.FirstOrDefault(p => p.ShortURL == shortUrl);

            if (project == null) {
                throw new Localized404Exception(null, "project-not-found");
            }

            return project;
        }
    }


    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class ProjectMetaTagsProviderAttribute : Attribute {

    }

    #endregion



}


