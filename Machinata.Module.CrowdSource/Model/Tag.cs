using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Text;
using Machinata.Core.Cards;


using Machinata.Core.Lifecycle;

namespace Machinata.Module.CrowdSource.Model {

    [Serializable()]
    [ModelClass]
    [Table("CrowdSourceTags")]
    public partial class Tag : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Enums /////////////////////////////////////////////////////////////////////////////

     

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Tag() {
            this.Infos = new Properties();
        }

      
        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////

        
        /// <summary>
        /// Name of tag
        /// </summary>
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string Name { get; set; }

        /// <summary>
        /// Short URL of tag
        /// </summary>
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [Column]
        public string ShortURL { get; set; }


        /// <summary>
        /// Additional infos
        /// </summary>
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [CascadeDelete]
        [Column]
        public Properties Infos { get; set; }



        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        
        
        [Column]
        public ICollection<Project> Projects { get; set; }


        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////


        [FormBuilder(Forms.API.LISTING)]
        [NotMapped]
        public string FilterGroup {
            get {
                return "tag-" + this.ShortURL;
            }
        }



        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////




        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////



        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////


    public static class ModelContexTagExtensions {

        [ModelSet]
        public static DbSet<Tag> Tags(this Core.Model.ModelContext context) {
            return context.Set<Tag>();
        }
    }

    #endregion



}


