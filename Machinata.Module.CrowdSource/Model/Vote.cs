using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Machinata.Core.Templates;
using Machinata.Core.Model;
using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Messaging;
using Machinata.Core.Util;
using System.Collections.Generic;
using System.Text;
using Machinata.Core.Cards;


using Machinata.Core.Lifecycle;
using Newtonsoft.Json;

namespace Machinata.Module.CrowdSource.Model {

    [Serializable()]
    [ModelClass]
    [Table("CrowdSourceVotes")]
    public partial class Vote : ModelObject {
        
        #region Class Logger
        /// <summary>
        /// The logger helper for this class. Use this logger instance anytime you want to
        /// log something from within this class.
        /// </summary>
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        #endregion

        #region Constants /////////////////////////////////////////////////////////////////////////////

        public const int VOTE_STANDARD_POSITIVE_VALUE = 1;
        public const int VOTE_STANDARD_NEGATIVE_VALUE = -1;
        public const int VOTE_STANDARD_CANCEL_VALUE = 0;

        #endregion

        #region Constructors //////////////////////////////////////////////////////////////////////

        public Vote() {
            this.Infos = new Properties();
        }


        #endregion

        #region Public Data Store Properties //////////////////////////////////////////////////////
       
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public DateTime Changed { get; set; }

        /// <summary>
        /// Positive or negative values
        /// </summary>
        [JsonProperty]
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public int Value { get; set; }

        /// <summary>
        /// ID of user, e.g. User.PublicId or GUID
        /// </summary>
        [JsonProperty]
        [FormBuilder(Forms.API.LISTING)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        [Column]
        public string UserID { get; set; }


        /// <summary>
        /// for User Agent infos etc
        /// </summary>
        [JsonProperty]
        [FormBuilder(Forms.Admin.VIEW)]
        [Column]
        public Properties Infos { get; set; }


      
        #endregion

        #region Public Navigation Properties //////////////////////////////////////////////////////        


        [Column]
        [FormBuilder(Forms.Admin.CREATE)]
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.EDIT)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.Admin.SELECTION)]
        public User User { get; set; }
        
        [Column]
        [ForeignKey("Project_Id")]
        public Project Project { get; set; }

        [Column]
        public int? Project_Id { get; set; }

        #endregion

        #region Public Properties (Derived or Not Mapped) /////////////////////////////////////////

        [NotMapped]
        public bool IsPositive {
            get {
                return this.Value > 0;
            }
        }

        [NotMapped]
        public bool IsNegatvie {
            get {
                return this.Value < 0;
            }
        }

        [NotMapped]
        public DateTime? LocalChangedDate {
            get {
                if (this.Changed != null) {
                    return Core.Util.Time.ConvertToDefaultTimezone(this.Changed).Date;
                }
                return null;
            }
        }

        [NotMapped]
        [FormBuilder(Forms.System.LISTING)]
        public string ProjectId {
            get {
                return this.Project?.PublicId;
            }
        }



        #endregion

        #region Model Creation ////////////////////////////////////////////////////////////////////

        #endregion

        #region Public Methods ////////////////////////////////////////////////////////////////////




        #endregion

        #region Virtual Methods ///////////////////////////////////////////////////////////////////








        #endregion

        #region Private Methods ///////////////////////////////////////////////////////////////////

        #endregion

    }

    #region Extensions ////////////////////////////////////////////////////////////////////////////

    public static class ModelContexVoteExtensions {
        [ModelSet]
        public static DbSet<Vote> Votes(this Core.Model.ModelContext context) {
            return context.Set<Vote>();
        }
    }

    #endregion



    public class Voter: ModelObject {

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public User User { get; set; }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        [FormBuilder(Forms.System.LISTING)]
        public string UserId { get; set; }

        public IEnumerable<Vote> Votes { get; set; }
        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public int? VotesCount {
            get {
                return this.Votes?.Count();
            }
        }

        [FormBuilder(Forms.Admin.VIEW)]
        [FormBuilder(Forms.Admin.LISTING)]
        public string Name {
            get {
                return this.User?.Name ?? UserId;
            }
        }

    }
}


