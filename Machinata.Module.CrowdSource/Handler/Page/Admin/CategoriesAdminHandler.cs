using System.Collections.Generic;
using System.Linq;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Module.CrowdSource.Model;
using Machinata.Module.CrowdSource.Extensions;

namespace Machinata.Module.CrowdSource.Handler {


    public class CategoriesAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("crowd-source");
        }

        #endregion




        [RequestHandler("/admin/crowd-source/categories")]
        public void Default() {

            // Paginate
            var entities = this.DB.Categories()
                .OrderByDescending(e => e.Created);
            entities = this.Template.Filter(entities, this, nameof(Tag.Name));
            entities = this.Template.Paginate(entities, this);

            // List
            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: entities,
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/crowd-source/categories/category/{entity.public-id}");

            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("categories");
        }

        [RequestHandler("/admin/crowd-source/categories/create")]
        public void Create() {
            this.RequireWriteARN();

            var form = new FormBuilder(Forms.Admin.CREATE);
         
            this.Template.InsertForm(
                variableName: "form",
                entity: new Category(),
                form: form,
                apiCall: "/api/admin/crowd-source/categories/create",
                onSuccess: "/admin/crowd-source/categories/category/{category.public-id}"
            );

            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("categories");
            this.Navigation.Add("create", "{text.create}");
        }



        [RequestHandler("/admin/crowd-source/categories/category/{publicId}")]
        public void View(string publicId) {

            // Grab 
            var entity = DB.Categories()
                .Include(nameof(Category.Projects))
                .GetByPublicId(publicId);

            this.Template.InsertVariables("entity", entity);
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity);

            // Projects
            this.Template.InsertProjectList(entity.Projects.AsQueryable());

            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("categories");
            this.Navigation.Add("category/" + entity.PublicId, entity.Name);
        }


        [RequestHandler("/admin/crowd-source/categories/category/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Categories()
               // .Include(nameof(BDEntity.Business))
                .GetByPublicId(publicId);
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/crowd-source/categories/category/{entity.PublicId}/edit",
                onSuccess: "{page.navigation.prev-path}"
            );
       

            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("categories");
            this.Navigation.Add("category/" + entity.PublicId, entity.Name);
          
            this.Navigation.Add($"edit", "{text.edit}");
        }



    }
}
