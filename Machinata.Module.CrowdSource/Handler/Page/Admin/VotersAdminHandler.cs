using System.Collections.Generic;
using System.Linq;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Module.CrowdSource.Model;
using Machinata.Core.Templates;
using System;
using Machinata.Module.CrowdSource.Extensions;
using Machinata.Core.Exceptions;

namespace Machinata.Module.CrowdSource.Handler {


    public class VotersAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("crowd-source");
        }

        #endregion

        [RequestHandler("/admin/crowd-source/voters")]
        public void Default() {
            var voters = GetAllVoters();

            var entities = this.Template.Filter(voters.AsQueryable(), this, nameof(Project.Name));
            entities = this.Template.Paginate(entities.AsQueryable(), this);

            // List
            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: entities,
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/crowd-source/voters/voter/{entity.user-id}");



            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("voters");
        }

        private IQueryable<Voter> GetAllVoters() {
            // Votes
            var votes = this.DB.Votes()
                .Include(nameof(Vote.Project))
                .Include(nameof(Vote.User)).ToList();
               

            var users = votes.Where(v => v.User != null).GroupBy(v => v.User);

            var nonUsers = votes.Where(v => v.User == null).GroupBy(v => v.UserID);


            var voters = new List<Voter>();

            foreach (var user in users) {
                voters.Add(new Voter() { User = user.Key, Votes = user.Where(u => true), UserId = user.Key.PublicId });
            }

            foreach (var nonUser in nonUsers) {
                voters.Add(new Voter() { User = null, Votes = nonUser.Where(u => true), UserId = nonUser.Key });
            }

            return voters.AsQueryable();
        }



        [RequestHandler("/admin/crowd-source/voters/voter/{userId}")]
        public void View(string userId) {
            Voter entity = GetVoterByUserId(userId);
          

            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity);



            this.Template.InsertEntityList(

               variableName: "votes",
               entities: entity.Votes.ToList(),
               link: "/admin/crowd-source/projects/project/{entity.project-id}/votes/vote/{entity.public-id}"
               );



            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("voters");
            this.Navigation.Add("voter/" + entity.PublicId, entity.UserId);
        }

        private Voter GetVoterByUserId(string userId) {

            // Grab 
            var voter =  GetAllVoters().FirstOrDefault(v => v.UserId == userId);

            if (voter == null) {
                throw new LocalizedException(this, "voter-not-found",null, 404);
            }

            return voter;
        }




    }
}
