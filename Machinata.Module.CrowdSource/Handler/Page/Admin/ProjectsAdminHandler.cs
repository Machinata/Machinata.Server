using System.Collections.Generic;
using System.Linq;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Module.CrowdSource.Model;
using Machinata.Core.Templates;
using System;
using Machinata.Module.CrowdSource.Extensions;

namespace Machinata.Module.CrowdSource.Handler {


    public class ProjectsAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("crowd-source");
        }

        #endregion



        [RequestHandler("/admin/crowd-source/projects")]
        public void Default() {

            // Paginate
            var entities = this.DB.Projects().IncludeAll().OrderByDescending(e => e.Created);
           
            entities = this.Template.Filter(entities, this, nameof(Project.Name));

            var count = entities.Count();
            entities = this.Template.Paginate(entities, this);

            // List
            this.Template.InsertVariable("entity-list.count", count);
            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: entities,
                form: new FormBuilder(Forms.Admin.LISTING).Include(nameof(Project.Created)),
                link: "/admin/crowd-source/projects/project/{entity.public-id}");

            

            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("projects");
        }

        [RequestHandler("/admin/crowd-source/projects/create")]
        public void Create() {
            this.RequireWriteARN();



            var form = new FormBuilder(Forms.Admin.CREATE);

            var entity = new Project();
            entity.User = this.User;

            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: form,
                apiCall: "/api/admin/crowd-source/projects/create",
                onSuccess: "/admin/crowd-source/projects/project/{project.public-id}"
            );

            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("projects");
            this.Navigation.Add("create", "{text.create}");
        }



        [RequestHandler("/admin/crowd-source/projects/project/{publicId}")]
        public void View(string publicId) {

            // Grab 
            var entity = DB.Projects().
                IncludeAll()
                .GetByPublicId(publicId);

            this.Template.InsertVariable("entity.has-votes", entity.HasVotes ? "true" : "false");
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity);

            // Tags
            this.Template.InsertTagCloud(entity, "tags.listing", "tag-cloud.tag", "tag-cloud.virtual-tag");

            // Meta Tags
            this.Template.InsertMetaTags(entity, "metatags");

        

            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("projects");
            this.Navigation.Add("project/" + entity.PublicId, entity.Name);
        }


        [RequestHandler("/admin/crowd-source/projects/project/{publicId}/votes")]
        public void ProjectVotes(string publicId) {

            // Grab 
            var entity = DB.Projects().
                IncludeAll()
                .GetByPublicId(publicId);

            this.Template.InsertVariable("entity.has-votes", entity.HasVotes ? "true" : "false");
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity);


            // Votes
            var entities = this.Template.Filter(entity.Votes.AsQueryable(), this, nameof(Project.Name));
            entities = this.Template.Paginate(entities, this/*,"Created","desc",1,10*/);

            this.Template.InsertEntityList(

               variableName: "entity-list",
               entities: entities,
               link: "{page.navigation.current-path}/vote/{entity.public-id}"
               );


            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("projects");
            this.Navigation.Add("project/" + entity.PublicId, entity.Name);
            this.Navigation.Add("votes");

        }



        [RequestHandler("/admin/crowd-source/projects/project/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Projects().
               IncludeAll()
                .GetByPublicId(publicId);
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/crowd-source/projects/project/{entity.PublicId}/edit",
                onSuccess: "{page.navigation.prev-path}"
            );
       

            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("projects");
            this.Navigation.Add("project/" + entity.PublicId, entity.Name);
          
            this.Navigation.Add($"edit", "{text.edit}");
        }

        [RequestHandler("/admin/crowd-source/projects/project/{publicId}/votes/vote/{voteId}")]
        public void VoteView(string publicId, string voteId) {
            var project = DB.Projects().IncludeAll().GetByPublicId(publicId);
            var entity = project.Votes.AsQueryable().GetByPublicId(voteId);
            this.Template.InsertVariables("project", project);

            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity);


            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("projects");
            this.Navigation.Add("project/" + project.PublicId, project.Name);
            this.Navigation.Add("votes");
            this.Navigation.Add($"vote/" + entity.PublicId, "{text.vote}: " + entity.PublicId);
        }

        [RequestHandler("/admin/crowd-source/projects/project/{publicId}/votes/vote/{voteId}/edit")]
        public void VoteEdit(string publicId, string voteId) {

            var project = DB.Projects().IncludeAll().GetByPublicId(publicId);
            var entity = project.Votes.AsQueryable().GetByPublicId(voteId);
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/crowd-source/projects/project/{project.PublicId}/votes/vote/{entity.PublicId}/edit",
                onSuccess: "{page.navigation.prev-path}"
            );


            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("projects");
            this.Navigation.Add("project/" + project.PublicId, project.Name);
            this.Navigation.Add("votes");
            this.Navigation.Add($"vote/" + entity.PublicId, "{text.vote}: " + entity.PublicId);
            this.Navigation.Add($"edit", "{text.edit}");
        }




        [RequestHandler("/admin/crowd-source/projects/project/{publicId}/metatags/metatag/{metatagId}/edit")]
        public void MetaTagEdit(string publicId, string metatagId) {

            var project = DB.Projects().IncludeAll().GetByPublicId(publicId);
            var entity = project.MetaTags.AsQueryable().GetByPublicId(metatagId);
            var impl = entity.LoadImplementation();


            this.Template.InsertVariables("project", project);
            this.Template.InsertVariables("entity", impl);

            this.Template.InsertVariable("entity.can-delete", impl.TestCanDelete(project, false) ? "true" : "false");

            var form = new FormBuilder(Forms.Admin.EDIT);
            if (impl.RelationshipType == MetaTag.RelationTypes.ToMany) {
                form.Include(nameof(MetaTag.Title));
            }

            this.Template.InsertForm(
                variableName: "form",
                entity: impl,
                form: form ,
                apiCall: $"/api/admin/crowd-source/projects/project/{project.PublicId}/metatags/metatag/{entity.PublicId}/edit",
                onSuccess: "{page.navigation.prev-path}"
            );


            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("projects");
            this.Navigation.Add("project/" + project.PublicId, project.Name);
          //  this.Navigation.Add($"metatags/metatag/" + entity.PublicId, entity.TypeName + ": " + entity.PublicId);
            this.Navigation.Add($"metatags/metatag/" + entity.PublicId  +"edit", entity.AdminTitle + ": {text.edit}");
        }

        [RequestHandler("/admin/crowd-source/projects/project/{publicId}/metatags/create")]
        public void MetaTagCreate(string publicId) {

            var project = DB.Projects().IncludeAll().GetByPublicId(publicId);
            var type = this.Params.String("type");


            if (type == null) {
                throw new Exception("No type has been defined");
            }
            var metaTag = new MetaTag();
            metaTag.Type = type;

            var form = new FormBuilder(Forms.Admin.CREATE);
            form.Hidden("type", type);


            var impl = metaTag.LoadImplementation();

            this.Template.InsertForm(
                variableName: "form",
                entity: impl,
                form: form,
                apiCall: $"/api/admin/crowd-source/projects/project/{project.PublicId}/metatags/create/",
                onSuccess: "{page.navigation.prev-path}"
            );

            metaTag.SaveImplementation(impl);

            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("projects");
            this.Navigation.Add("project/" + project.PublicId, project.Name);
            this.Navigation.Add($"metatags/create", impl.TypeFriendlyName + ": {text.create}");
        }



    }
}
