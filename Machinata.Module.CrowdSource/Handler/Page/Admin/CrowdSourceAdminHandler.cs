using System.Collections.Generic;
using System.Linq;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Module.CrowdSource.Model;
using Machinata.Module.Admin.Dashboard;
using Machinata.Module.Reporting.Model;

namespace Machinata.Module.CrowdSource.Handler {


    public class CrowdSourceAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("crowd-source");
        }

        #endregion


        #region Menu

        [MenuBuilder]
        public static void GetMenu(MenuBuilder menu) {
            menu.AddSection(new MenuSection {
                Icon = "connect",
                Path = "/admin/crowd-source",
                Title = "{text.crowd-source}",
                Sort = "500"
            });

            //menu.AddItem(new MenuItem {
            //    Icon = "project",
            //    Path = "/admin/crowd-source/projects",
            //    Title = "{text.projects}",
            //    Sort = "500"
            //});
        }

       

        #endregion

        [RequestHandler("/admin/crowd-source")]
        public void Default() {

            // Paginate
            
            var entities = this.DB.Projects().IncludeAll().Published()
                .OrderByDescending(e => e.Created);
            entities = this.Template.Filter(entities, this, nameof(Project.Name));
            var count = entities.Count();
            entities = this.Template.Paginate(entities, this);

            // List
            this.Template.InsertVariable("entity-list.count", count);
            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: entities, 
                form:new FormBuilder(Forms.Admin.LISTING).Include(nameof(Project.Created)),
                link: "/admin/crowd-source/projects/project/{entity.public-id}");

            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
        }
        
      

    }
}
