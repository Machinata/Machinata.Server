using System.Collections.Generic;
using System.Linq;

using Machinata.Module.Admin.Handler;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;

using Machinata.Core.Builder;
using Machinata.Module.CrowdSource.Model;
using Machinata.Module.CrowdSource.Extensions;

namespace Machinata.Module.CrowdSource.Handler {


    public class TagsAdminHandler : AdminPageTemplateHandler {
        
        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("crowd-source");
        }

        #endregion




        [RequestHandler("/admin/crowd-source/tags")]
        public void Default() {

            // Paginate
            var entities = this.DB.Tags()
                .OrderByDescending(e => e.Created);
            entities = this.Template.Filter(entities, this, nameof(Tag.Name));
            entities = this.Template.Paginate(entities, this);

            // List
            this.Template.InsertEntityList(
                variableName: "entity-list",
                entities: entities,
                form: new FormBuilder(Forms.Admin.LISTING),
                link: "/admin/crowd-source/tags/tag/{entity.public-id}");

            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("tags");
        }

        [RequestHandler("/admin/crowd-source/tags/create")]
        public void Create() {
            this.RequireWriteARN();



            var form = new FormBuilder(Forms.Admin.CREATE);
         

            this.Template.InsertForm(
                variableName: "form",
                entity: new Tag(),
                form: form,
                apiCall: "/api/admin/crowd-source/tags/create",
                onSuccess: "/admin/crowd-source/tags/tag/{tag.public-id}"
            );

            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("tags");
            this.Navigation.Add("create", "{text.create}");
        }



        [RequestHandler("/admin/crowd-source/tags/tag/{publicId}")]
        public void View(string publicId) {

            // Grab 
            var entity = DB.Tags()
                .Include(nameof(Tag.Projects))
                .GetByPublicId(publicId);



            this.Template.InsertVariables("entity", entity);
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertPropertyList("entity", entity);

            // Projects
            this.Template.InsertProjectList(entity.Projects.AsQueryable());


            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("tags");
            this.Navigation.Add("tag/" + entity.PublicId, entity.Name);
        }

      

        [RequestHandler("/admin/crowd-source/tags/tag/{publicId}/edit")]
        public void Edit(string publicId) {
            this.RequireWriteARN();

            var entity = DB.Tags()
               // .Include(nameof(BDEntity.Business))
                .GetByPublicId(publicId);
            this.Template.InsertVariables("entity", entity);
            this.Template.InsertForm(
                variableName: "form",
                entity: entity,
                form: new FormBuilder(Forms.Admin.EDIT),
                apiCall: $"/api/admin/crowd-source/tags/tag/{entity.PublicId}/edit",
                onSuccess: "{page.navigation.prev-path}"
            );
       

            // Navigation
            this.Navigation.Add("crowd-source", "{text.crowd-source}");
            this.Navigation.Add("tags");
            this.Navigation.Add("tag/" + entity.PublicId, entity.Name);
          
            this.Navigation.Add($"edit", "{text.edit}");
        }



    }
}
