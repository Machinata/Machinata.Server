using System.Collections.Generic;
using System.Linq;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Module.Admin.Handler;
using System.Reflection;
using Machinata.Module.CrowdSource.Model;
using System;
using Newtonsoft.Json.Linq;
using Machinata.Module.Reporting.Model;

namespace Machinata.Module.CrowdSource.Handler {


    public class ProjectsAdminChartsAPIHandler : AdminAPIHandler {

    

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("crowd-source");
        }

     

        #endregion

        [RequestHandler("/api/admin/crowd-source/projects/project/{projectId}/dashboard", "/admin/crowd-source/*")]
        public void ProjectViewDashboard(string projectId) {
            this.RequireWriteARN();


            var project = this.DB.Projects().IncludeAll().GetByPublicId(projectId);

            var projects = (new List<Project> { project }).AsQueryable();
            var votesDonuts = Dashboard.Dashboard.GetProjectVotesDonut(this.DB, project);

            var votesOvertime = Logic.Statistics.VotesOverTimeChart(projects);

            var report = new Report();
            var section = new SectionNode();
            //section.AddChild(votesDonuts);
            //section.AddChild(votesOvertime);
            report.AddToBody(section);


            var layoutNode = new LayoutNode();
            layoutNode.Style = "CS_W_Two_Columns";
            layoutNode.AddChildToSlot(votesOvertime, "{left}");
            layoutNode.AddChildToSlot(votesDonuts, "{right}");

            section.AddChild(layoutNode);


            this.SendAPIMessage("node-data", JObject.FromObject(report));

        }






    }
}
