using System.Collections.Generic;
using System.Linq;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Module.Admin.Handler;
using System.Reflection;
using Machinata.Module.CrowdSource.Model;
using Machinata.Module.Reporting.Model;

namespace Machinata.Module.CrowdSource.Handler {


    public class ReportsAdminAPIHandler : AdminAPIHandler {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("crowd-source");
        }

        #endregion

        #region REPORT

        [RequestHandler("/api/admin/crowd-source/reports/dashboard")]
        public void DashboardReport() {
            // Init report
            var report = new Report();
            report.SetTitle("Crowd Source");
           // report.SetSubtitle("Dashboard");
            // Chapter: Overview
            {
                var chapter = report.NewChapter("Overview");
                // TODO ADD DASHBOARD CHARTS


            }

            // Send
            this.SendJSON(report);

        }

       


        #endregion




    }
}
