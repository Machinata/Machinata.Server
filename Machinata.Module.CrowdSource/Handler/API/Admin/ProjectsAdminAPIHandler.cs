using System.Collections.Generic;
using System.Linq;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Module.Admin.Handler;
using System.Reflection;
using Machinata.Module.CrowdSource.Model;
using System;

namespace Machinata.Module.CrowdSource.Handler {


    public class ProjectsAdminAPIHandler : CRUDAdminAPIHandler<Model.Project> {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("crowd-source");
        }

        #endregion

        #region CRUD

        [RequestHandler("/api/admin/crowd-source/projects/create")]
        public void Create() {
            var project = CRUDCreate();
          

        }

        protected override void CreatePopulate(Model.Project entity) {
            base.CreatePopulate(entity);
            entity.Status = Model.Project.ProjectStatuses.Voting;

            if (string.IsNullOrWhiteSpace(entity.ShortURL) == true && string.IsNullOrWhiteSpace(entity.Name) == false) {
                entity.ShortURL = Core.Util.String.CreateShortURLForName(entity.Name);
            }

            if (this.DB.Projects().Any(p => p.ShortURL == entity.ShortURL)) {
                throw new BackendException("duplicated-shorturl", "The project name has been used already. Please choose another one");
            }

            // Meta Tags
            entity.GenerateMissingMetaTags(this.DB);

            entity.SetModified();

        }

      

        [RequestHandler("/api/admin/crowd-source/projects/project/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);

        }

        [RequestHandler("/api/admin/crowd-source/projects/project/{publicId}/edit")]
        public void Edit(string publicId) {

            var entity = this.DB.Projects().GetByPublicId(publicId);
            var oldName = entity.Name;
            var oldShortURL = entity.ShortURL;
            var oldIsDefaultShortURL = entity.ShortURL == Core.Util.String.CreateShortURLForName(oldName);

            CRUDEdit(publicId, new FormBuilder(Forms.Admin.EDIT));

            // Name has changed and short url is default, then update short url as well
            if (oldName != entity.Name && oldIsDefaultShortURL == true && oldShortURL == entity.ShortURL) {
                entity.ShortURL = Core.Util.String.CreateShortURLForName(entity.Name);
                this.DB.SaveChanges();
            }
            
            

        }

        protected override void EditPopulate(Project entity) {

            base.EditPopulate(entity);

            // make short url safe if existing
            if (string.IsNullOrWhiteSpace(entity.ShortURL) == false) {
                entity.ShortURL = Core.Util.String.CreateShortURLForName(entity.ShortURL);
            }
            // create short url from name
            else if (string.IsNullOrWhiteSpace(entity.Name) == false) {
                entity.ShortURL = Core.Util.String.CreateShortURLForName(entity.Name);
            }

            // Modified
            entity.SetModified();


            if (this.DB.Projects().Any(p => p.ShortURL == entity.ShortURL && p.Id != entity.Id)) {
                throw new BackendException("duplicated-shorturl", $"The short url '{entity.ShortURL}' been used already. Please choose another one.");
            }
        }


        [RequestHandler("/api/admin/crowd-source/projects/{entityId}/tags/missing")]
        public void MissingTags(string entityId) {

            var project = this.DB.Projects().IncludeAll().GetByPublicId(entityId);


            var selectedTags = project.Tags.ToList();

           var tags = this.DB.Tags().ToList().Where(bt => selectedTags.Contains(bt) == false).ToList();

            var form = new FormBuilder(Forms.API.LISTING)
                .Include(nameof(Tag.PublicId));

            var result = tags.Select(c => c.GetJSON(form)).ToList();

            // manually here otherwise enums will be integers
            var rawJson = Core.JSON.Serialize(new { Tags = result }, true, true, true);

            // Send raw to 
            this.SendRawAPIMessage("success", rawJson, false);

        }


        [RequestHandler("/api/admin/crowd-source/projects/{entityId}/tags/{tagId}/toggle")]
        public void ToggleEntity(string tagId, string entityId) {
            this.RequireWriteARN();
          
            var enable = this.Params.Bool("value", false);
            var tag = this.DB.Tags().GetByPublicId(tagId);

            var project = this.DB.Projects().IncludeAll().GetByPublicId(entityId);

            if (enable) {
                project.Tags.Add(tag);
            } else {
                project.Tags.Remove(tag);
            }

            // Modified
            project.SetModified();

            this.DB.SaveChanges();
            this.SendAPIMessage("success");

        }


        #endregion


        [RequestHandler("/api/admin/crowd-source/projects/project/{projectId}/votes/vote/{voteId}/edit")]
        public void ProjectVoteEdit(string projectId, string voteId) {
            this.RequireWriteARN();


            var project = this.DB.Projects().IncludeAll().GetByPublicId(projectId);
            var vote = project.Votes.AsQueryable().GetByPublicId(voteId);

            vote.Populate(this, new FormBuilder(Forms.Admin.EDIT));

            vote.Changed = DateTime.UtcNow;

            vote.Validate();

            this.DB.SaveChanges();
            this.SendAPIMessage("success");

        }

        [RequestHandler("/api/admin/crowd-source/projects/project/{projectId}/votes/vote/{voteId}/delete")]
        public void ProjectVoteDelete(string projectId, string voteId) {
            this.RequireWriteARN();


            var project = this.DB.Projects().IncludeAll().GetByPublicId(projectId);
            var vote = project.Votes.AsQueryable().GetByPublicId(voteId);


            this.DB.DeleteEntity(vote);

            this.DB.SaveChanges();
            this.SendAPIMessage("success");

        }


        [RequestHandler("/api/admin/crowd-source/projects/project/{projectId}/votes/create-random")]
        public void ProjectCreateRandomVotes(string projectId) {
            this.RequireWriteARN();


            var project = this.DB.Projects().IncludeAll().GetByPublicId(projectId);

            Data.PseudoData.CreateVotes(project, 100);

            this.DB.SaveChanges();
            this.SendAPIMessage("success");

        }

        [RequestHandler("/api/admin/crowd-source/projects/project/{projectId}/votes/purge")]
        public void ProjecPurgeVotes(string projectId) {
            this.RequireWriteARN();


            var project = this.DB.Projects().IncludeAll().GetByPublicId(projectId);

            var votes = project.Votes.ToList();

            foreach(var vote in votes) {
                this.DB.DeleteEntity(vote);
            }

            this.DB.SaveChanges();
            this.SendAPIMessage("success");

        }

        [RequestHandler("/api/admin/crowd-source/projects/project/{projectId}/metatags/generate-missing")]
        public void ProjecGenerateMissingMetaTags(string projectId) {
            this.RequireWriteARN();


            var project = this.DB.Projects().IncludeAll().GetByPublicId(projectId);

            project.GenerateMissingMetaTags(this.DB);

            // Modified
            project.SetModified();

            this.DB.SaveChanges();
            this.SendAPIMessage("success");

        }


        [RequestHandler("/api/admin/crowd-source/random/projects/create")]
        public void ProjectCreateRandomProjects() {
            this.RequireWriteARN();

            var amount = this.Params.Int("amount", 1);

            var projects = Data.PseudoData.CreateProjects(this.DB, amount);
                  

            this.DB.SaveChanges();
            this.SendAPIMessage("success");

        }



        [RequestHandler("/api/admin/crowd-source/projects/project/{projectId}/metatags/metatag/{metatagId}/edit")]
        public void ProjectMetaTagEdit(string projectId, string metatagId) {
            this.RequireWriteARN();


            var project = this.DB.Projects().IncludeAll().GetByPublicId(projectId);
            var metaTag = project.MetaTags.AsQueryable().GetByPublicId(metatagId);
            var impl = metaTag.LoadImplementation();


            var form = new FormBuilder(Forms.Admin.EDIT);
            if (impl.RelationshipType == MetaTag.RelationTypes.ToMany) {
                form.Include(nameof(MetaTag.Title));
            }

            impl.Populate(this, form);


            impl.Validate();
            metaTag.SaveImplementation(impl);

            // Modified
            project.SetModified();

            this.DB.SaveChanges();
            this.SendAPIMessage("success");

        }


        [RequestHandler("/api/admin/crowd-source/projects/project/{projectId}/metatags/create")]
        public void ProjectMetaTagCreate(string projectId) {
            this.RequireWriteARN();


            var project = this.DB.Projects().IncludeAll().GetByPublicId(projectId);

          

            var type = this.Params.String("type");



            if (type == null) {
                throw new Exception("No type has been defined");
            }

            var metaTags = project.MetaTags.Where(p => p.Type == type).AsQueryable();

            var newSort = 0;

            if (metaTags != null && metaTags.Any()) {
                newSort = metaTags.Max(mt => mt.Sort) + 1;
            }

            var metaTag = new MetaTag();
            metaTag.Type = type;
            metaTag.Sort = newSort;

            var impl = metaTag.LoadImplementation();

            impl.Populate(this, new FormBuilder(Forms.Admin.CREATE));


            impl.Validate();

            metaTag.SaveImplementation(impl);

            project.MetaTags.Add(metaTag);


            // Modified
            project.SetModified();

            this.DB.SaveChanges();
            this.SendAPIMessage("success");

        }


        [RequestHandler("/api/admin/crowd-source/projects/project/{projectId}/metatags/metatag/{metatagId}/delete")]
        public void ProjectMetaTagDelete(string projectId, string metatagId) {
            this.RequireWriteARN();


            var project = this.DB.Projects().IncludeAll().GetByPublicId(projectId);
            var metatag = project.MetaTags.AsQueryable().GetByPublicId(metatagId);
            metatag.TestCanDelete(project);

            this.DB.DeleteEntity(metatag);

            // Modified
            project.SetModified();

            this.DB.SaveChanges();
            this.SendAPIMessage("success");

        }

       

        [RequestHandler("/api/admin/crowd-source/projects/project/{projectId}/metatags/metatag/{typeName}/sort")]
        public void SortMetaTags(string projectId, string typeName) {

            this.RequireWriteARN();


            var project = this.DB.Projects().IncludeAll().GetByPublicId(projectId);

        

            if (typeName == null) {
                throw new Exception("No type has been defined");
            }

            var orderIds = this.Params.StringArray("order", new string[] { });
            var metaTags = project.MetaTags.Where(p => p.Type == typeName).AsQueryable();

            int order = 0;

            if (metaTags == null) {
                throw new System.Exception("No meta tags found defined");
            }

            if (metaTags.Count() != orderIds.Count()) {
                throw new System.Exception("Order ids doesnt match the number or metaTags");
            }


            // Set the order/sort
            foreach (var id in orderIds) {
                var metaTag = metaTags.GetByPublicId(id);
                metaTag.Sort = order;
                order++;
            }

            // Modified
            project.SetModified();


            // Save to DB...
            this.DB.SaveChanges();
            this.SendAPIMessage("success");

        }

    }
}
