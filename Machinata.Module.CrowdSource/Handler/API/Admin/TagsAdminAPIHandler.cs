using System.Collections.Generic;
using System.Linq;
using Machinata.Core.Handler;
using Machinata.Core.Util;
using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Core.Builder;
using Machinata.Module.Admin.Handler;
using System.Reflection;

namespace Machinata.Module.CrowdSource.Handler {


    public class TagsAdminAPIHandler : CRUDAdminAPIHandler<Model.Tag> {

        #region Handler Policies

        [PolicyProvider]
        public static List<AccessPolicy> PolicyProvider() {
            return AccessPolicy.GetDefaultAdminPolicies("crowd-source");
        }

        #endregion

        #region CRUD

        [RequestHandler("/api/admin/crowd-source/tags/create")]
        public void Create() {
            CRUDCreate();
        }
        protected override void CreatePopulate(Model.Tag entity) {
            base.CreatePopulate(entity);

            if (string.IsNullOrWhiteSpace(entity.ShortURL) == true && string.IsNullOrWhiteSpace(entity.Name) == false) {
                entity.ShortURL = Core.Util.String.CreateShortURLForName(entity.Name);
            }
        }

        [RequestHandler("/api/admin/crowd-source/tags/tag/{publicId}/delete")]
        public void Delete(string publicId) {
            CRUDDelete(publicId);

        }

        [RequestHandler("/api/admin/crowd-source/tags/tag/{publicId}/edit")]
        public void Edit(string publicId) {
            CRUDEdit(publicId, new FormBuilder(Forms.Admin.EDIT));

        }


        protected override void EditPopulate(Model.Tag entity) {
            base.EditPopulate(entity);

            if (string.IsNullOrWhiteSpace(entity.ShortURL) == false) {
                entity.ShortURL = Core.Util.String.CreateShortURLForName(entity.ShortURL);
            } else if (string.IsNullOrWhiteSpace(entity.Name) == false) {
                entity.ShortURL = Core.Util.String.CreateShortURLForName(entity.Name);
            }
        }



        #endregion




    }
}
