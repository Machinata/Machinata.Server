using Machinata.Core.Handler;
using System;

using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Module.CrowdSource.Model;
using Machinata.Core.Util;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace Machinata.Module.CrowdSource.Handler {

    public class StatisticsAPIHandler : APIHandler {


        [RequestHandler("/api/crowd-source/statistics/votes-over-time", AccessPolicy.PUBLIC_ARN)]
        public void VotesOverTime() {
            var result = Logic.Statistics.VotesOverTimeChart(this.DB.Projects());


            var data = JObject.FromObject(result);
            data["nodeType"] = "VBarNode";
            data["enabled"] = true;
            data["screen"] = true;
            data["theme"] = "default";

            this.SendAPIMessage("node-data", data);

        }

    }
}