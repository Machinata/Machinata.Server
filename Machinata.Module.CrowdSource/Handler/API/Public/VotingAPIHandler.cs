using Machinata.Core.Handler;
using System;

using Machinata.Core.Model;
using Machinata.Core.Exceptions;
using Machinata.Module.CrowdSource.Model;
using Machinata.Core.Util;
using Machinata.Core.Builder;

namespace Machinata.Module.CrowdSource.Handler {

    public class VotingAPIHandler : APIHandler {
              
     
        [RequestHandler("/api/crowd-source/projects/{projectId}/upvote", AccessPolicy.PUBLIC_ARN)]
        public void Upvote(string projectId) {
            try {

                var id = this.Params.String("id");

                // Check ID
                this.CheckId(id);

                // Mutex
                var mut = new System.Threading.Mutex(false, id);

                try {
                    // Wait until it is safe to enter.
                    mut.WaitOne();

                    var project = this.DB.Projects().IncludeAll().GetByPublicId(projectId);
                    project.CastVote(this.DB, Vote.VOTE_STANDARD_POSITIVE_VALUE, this.User, id, this.Context);

                    this.DB.SaveChanges();


                    var result = project.GetJSON(new Core.Builder.FormBuilder(Forms.API.LISTING));

                    this.SendAPIMessage("success", result);

                } finally {

                    mut.ReleaseMutex();

                }

            } catch (BackendException be) {
                Core.EmailLogger.SendMessageToAdminEmail("Expected Error Voting", "Machinata.Product.KantonSchaffhausen2030Website", this.Context, this, be);
                throw be;
            } catch (Exception e) {
                Core.EmailLogger.SendMessageToAdminEmail("Error Voting", "Machinata.Product.KantonSchaffhausen2030Website", this.Context, this, e);
                throw new BackendException("vote-error", "Voting error", e);
            }
        }

        private void CheckId(string id) {
            // ID basic chck
            if (string.IsNullOrWhiteSpace(id) == true) {
                throw new LocalizedException(null, "cast-vote-no-userid");
            }
        }

        [RequestHandler("/api/crowd-source/projects/{projectId}/downvote", AccessPolicy.PUBLIC_ARN)]
        public void Downvote(string projectId) {
            try {

                var id = this.Params.String("id");

                // Check ID
                this.CheckId(id);

                // Mutex
                var mut = new System.Threading.Mutex(false, id);

                try {
                    // Wait until it is safe to enter.
                    mut.WaitOne();

                    var project = this.DB.Projects().IncludeAll().GetByPublicId(projectId);
                    project.CastVote(this.DB, Vote.VOTE_STANDARD_NEGATIVE_VALUE, this.User, id, this.Context);

                    this.DB.SaveChanges();
               

                    var result = project.GetJSON(new Core.Builder.FormBuilder(Forms.API.LISTING));

                    this.SendAPIMessage("success", result);

                } finally {

                    mut.ReleaseMutex();

                }
                                
            } catch (BackendException be) {
                Core.EmailLogger.SendMessageToAdminEmail("Expected Error Voting", "Machinata.Product.KantonSchaffhausen2030Website", this.Context, this, be);
                throw be;
            } catch (Exception e) {
                Core.EmailLogger.SendMessageToAdminEmail("Error Voting", "Machinata.Product.KantonSchaffhausen2030Website", this.Context, this, e);
                throw new BackendException("vote-error", "Voting error", e);
            }
        }


        [RequestHandler("/api/crowd-source/projects/{projectId}/cancelvote", AccessPolicy.PUBLIC_ARN)]
        public void Cancelvote(string projectId) {
            try {

                var id = this.Params.String("id");

                // Check ID
                this.CheckId(id);

                // Mutex
                var mut = new System.Threading.Mutex(false, id);

                try {
                    // Wait until it is safe to enter.
                    mut.WaitOne();

                    var project = this.DB.Projects().IncludeAll().GetByPublicId(projectId);
                    project.CastVote(this.DB, Vote.VOTE_STANDARD_CANCEL_VALUE, this.User, id, this.Context, onlyUpdate: true);

                    this.DB.SaveChanges();

                    var result = project.GetJSON(new Core.Builder.FormBuilder(Forms.API.LISTING));

                    this.SendAPIMessage("success", result);
                } finally {

                    mut.ReleaseMutex();

                }
            } catch (BackendException be) {
                Core.EmailLogger.SendMessageToAdminEmail("Expected Error Voting", "Machinata.Product.KantonSchaffhausen2030Website", this.Context, this, be);
                throw be;
            } catch (Exception e) {
                Core.EmailLogger.SendMessageToAdminEmail("Error Voting", "Machinata.Product.KantonSchaffhausen2030Website", this.Context, this, e);
                throw new BackendException("vote-error", "Voting error", e);
            }
        }


    }
}