﻿using Machinata.Core.API;
using Machinata.Core.Exceptions;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace Machinata.Module.CrowdSource.Fingerprint {
    public class FingerprintJSProvider : FingerprintProvider {
        public override JObject ValidateId(string id ) {

            // FINGERPRINT ERROR
            if (id.StartsWith("FINGERPRINT_ERROR")) {
                throw new LocalizedException(null, "cast-vote-fingerprint");
            }

            // var server = "https://api.fpjs.io/visitors/7FRnA6BXTAaTnhUAJcRh?token=DEcJf6iNpa9DsCGkMDyM&limit=1";
            var server = "https://api.fpjs.io";
            var resource = "/visitors/" + id;

            var token = Config.CrowdSourceFingerprintProviderToken;
            if (string.IsNullOrWhiteSpace(token)) {
                throw new System.Exception("No ProjectsFingerprintProviderToken config found ");
            }

            resource += "?token=" + token;
            resource += "&limit=" + 1.ToString();

            var apiCall = new APICall(resource,server);
            apiCall.Send();
            var result = apiCall.JSON();

            if (result["visitorId"]?.ToString() != id) {
                throw new LocalizedException(null, "cast-vote-fingerprint");
            }

            if (result["visits"]?.Children().Any() == false) {
                throw new LocalizedException(null, "check-fingerprint");
            }

            return result;

        }
    }
}
