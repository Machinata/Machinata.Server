﻿using Machinata.Core.API;
using Machinata.Core.Exceptions;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace Machinata.Module.CrowdSource.Fingerprint {

    /// <summary>
    /// This one does nothing
    /// </summary>
    public class FingerprintNothingProvider : FingerprintProvider {
        public override JObject ValidateId(string id ) {

            return null;

        }
    }
}
