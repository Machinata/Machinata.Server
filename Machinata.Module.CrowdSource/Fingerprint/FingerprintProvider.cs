﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.CrowdSource.Fingerprint {
    public abstract class FingerprintProvider {

        /// <summary>
        /// Returns the validation result
        /// if not valid an exception has to be thrown
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public abstract JObject ValidateId(string id);

        public static FingerprintProvider Default() {

            var providerName = Config.CrowdSourceFingerprintProvider;

            if (providerName == "FingerprintJS") {
                return new FingerprintJSProvider();
            }
            else if (providerName == "Nothing") {
                return new FingerprintNothingProvider();
            }
            throw new Exception($"Fingerprint provider: {providerName} not found");


        }
    }
}
