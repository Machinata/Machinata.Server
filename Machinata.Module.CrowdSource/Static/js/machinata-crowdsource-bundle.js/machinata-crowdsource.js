
Machinata.CrowdSource = {};

Machinata.CrowdSource.idProvider = "CacheGUID";

Machinata.CrowdSource._votingStateCache = {};

Machinata.CrowdSource.loadCache = function () {
    Machinata.CrowdSource._votingStateCache = Machinata.getCache("CrowdSource_Cache");
    if (Machinata.CrowdSource._votingStateCache == null) Machinata.CrowdSource._votingStateCache = {};
};
Machinata.CrowdSource.persistCache = function () {
    Machinata.setCache("CrowdSource_Cache", Machinata.CrowdSource._votingStateCache);
};
Machinata.CrowdSource.registerVoteInCache = function (projectId, val) {
    Machinata.CrowdSource._votingStateCache[projectId] = {
        val: val
    };
    Machinata.CrowdSource.persistCache();
};
Machinata.CrowdSource.getVoteInCache = function (projectId) {
    return Machinata.CrowdSource._votingStateCache[projectId];
};


Machinata.CrowdSource.getVoterId = function (callback) {
    var provider = Machinata.CrowdSource.IDProviders[Machinata.CrowdSource.idProvider];
    if (provider == null) throw "Machinata.CrowdSource.getVoterId: invalid provider";
    provider.getId(function (id) {
        console.log("Machinata.CrowdSource.getVoterId",id);
        callback(id);
    });
};

Machinata.CrowdSource.cancelvote = function (projectId) {
    Machinata.CrowdSource.getVoterId(function (id) {
        var url = "/api/crowd-source/projects/" + projectId + "/cancelvote";
        var data = {
            id: id
        };
        Machinata.apiCall(url)
            .data(data)
            .success(function (response) {
                Machinata.CrowdSource.registerVoteInCache(projectId, 0);
                Machinata.CrowdSource.updateProjectUI(projectId, response.data);
            })
            .send();
    });
};

Machinata.CrowdSource.upvote = function (projectId) {
    Machinata.CrowdSource.getVoterId(function (id) {
        var url = "/api/crowd-source/projects/" + projectId + "/upvote";
        var data = {
            id: id
        };
        Machinata.apiCall(url)
            .data(data)
            .success(function (response) {
                Machinata.CrowdSource.registerVoteInCache(projectId, +1);
                Machinata.CrowdSource.updateProjectUI(projectId, response.data);
            })
            .send();
    });
};

Machinata.CrowdSource.downvote = function (projectId) {
    Machinata.CrowdSource.getVoterId(function (id) {
        var url = "/api/crowd-source/projects/" + projectId + "/downvote";
        var data = {
            id: id
        };
        Machinata.apiCall(url)
            .data(data)
            .success(function (response) {
                Machinata.CrowdSource.registerVoteInCache(projectId, -1);
                Machinata.CrowdSource.updateProjectUI(projectId, response.data);
            })
            .send();
    });
};

Machinata.CrowdSource.updateProjectUI = function (projectId, newState) {
    console.log(projectId,newState);
    var projects = $(".ma-crowdsource-project[data-project-id='" + projectId + "']");
    projects.each(function () {
        var projectElem = $(this);
        projectElem.find(".approval .value").text(newState["approval-rate"]);
        projectElem.find(".approval .ma-crowdsource-donut").attr("data-value", newState["approval-rate-decimal"]);
        Machinata.UI.microdonut(projectElem.find(".ma-crowdsource-donut"), {
            updateIfExists: true
        });
    });
};


Machinata.CrowdSource.follow = function (projectId) {
    return Machinata.genericError("not-implemented", "This is not implemented yet.");
};

Machinata.CrowdSource.share = function (projectId) {
    var opts = {};
    opts["facebook"] = "{text.dialog.share.facebook}";
    opts["twitter"] = "{text.dialog.share.twitter}";
    opts["whatsapp"] = "{text.dialog.share.whatsapp}";
    opts["email"] = "{text.dialog.share.email}";
    opts["copylink"] = "{text.dialog.share.copylink}";

    var projectElem = $(".ma-crowdsource-project[data-project-id='" + projectId + "']");
    var projectURL = projectElem.attr("data-project-url");

    Machinata.optionsDialog("{text.dialog.share-project.title}", "{text.dialog.share-project.message}", opts)
            .okay(function (id, val) {
                return Machinata.Social.openShareWindowForService(id, projectURL );
            })
            .show();
   
    return;
};

Machinata.CrowdSource.showDetails = function (projectId) {
    var projectElem = $(".ma-crowdsource-project[data-project-id='" + projectId + "']");
    Machinata.goToPage(projectElem.attr("data-project-url"));
};

Machinata.CrowdSource.notImplemented = function (projectId) {
    return Machinata.genericError("not-implemented", "This is not implemented yet.");
};

Machinata.CrowdSource.bindUI = function (elements) {
    var projects = elements.find(".ma-crowdsource-project");
    projects.each(function () {
        // Init
        var projectElem = $(this);
        var projectId = projectElem.attr("data-project-id");
        // Get cached state
        var vote = Machinata.CrowdSource.getVoteInCache(projectId);
        if (vote == null) vote = { val: 0 };
        var sliderValue = 0.5;
        if (vote.val == -1) sliderValue = 0.0;
        else if (vote.val == 1) sliderValue = 1.0;
        // Create donuts
        Machinata.UI.microdonut(projectElem.find(".ks-donut"));
        // Create voting slider
        projectElem.find(".voting-track").each(function () {
            Machinata.UI.createTrackSlider($(this), {
                snapToStartEnd: true,
                initialValue: sliderValue
            });
        });
        // Bind actions
        projectElem.find(".action-show-details").click(function () {
            Machinata.CrowdSource.showDetails(projectId);
        });
        projectElem.find(".action-share").click(function () {
            //Machinata.CrowdSource.notImplemented(projectId);
            Machinata.CrowdSource.share(projectId);
        });
        projectElem.find(".action-upvote").click(function () {
            Machinata.CrowdSource.upvote(projectId);
            projectElem.find(".voting-track").trigger("updateSliderValue", [1.0]);
        });
        projectElem.find(".action-downvote").click(function () {
            Machinata.CrowdSource.downvote(projectId);
            projectElem.find(".voting-track").trigger("updateSliderValue", [0.0]);
        });
        projectElem.find(".voting-track").on("sliderValueChanged", function (event, val) {
            console.log("sliderValueChanged", val);
            if (val == 0.0) {
                Machinata.CrowdSource.downvote(projectId);
            } else if (val == 1.0) {
                Machinata.CrowdSource.upvote(projectId);
            } else if (val == 0.5) {
                Machinata.CrowdSource.cancelvote(projectId);
            }
            projectElem.find(".voting-track").not($(this)).trigger("updateSliderValue", [val]);
        });
    });

    // Create shuffle layouts
    $(".ma-crowdsource-projects.option-filterable").each(function () {
        var shuffle = Machinata.Shuffle.createShuffleLayout($(this), {
            // Options
        });
        $(this).data("shuffle", shuffle);
        // Initial filter?
        if (Machinata.queryParameter("filter") != null) {
            $(".ma-crowdsource-filter[data-filter-group='" + Machinata.queryParameter("filter") + "']").addClass("selected");
            Machinata.CrowdSource.updateListingForSelectedFilters();
        }
    });
    
    // Bind filter actions
    {
        var filterButtons = $(".ma-crowdsource-filter");
        filterButtons.click(function () {
            var filterButton = $(this);
            filterButton.toggleClass("selected");
            if (filterButton.attr("data-filter-type") == "category" && filterButton.hasClass("selected")) {
                filterButtons.filter("[data-filter-type='category']").not(filterButton).removeClass("selected");
            }
            Machinata.CrowdSource.updateListingForSelectedFilters();
        });
    };

    // Bind sort actions
    {
        var sortButtons = $(".ma-crowdsource-sorter");
        sortButtons.click(function () {
            var sortButton = $(this);
            sortButton.toggleClass("selected");
            if (sortButton.hasClass("selected")) {
                sortButtons.not(sortButton).removeClass("selected");
            }
            Machinata.CrowdSource.updateListingForSelectedSorts();
        });
    };

    // Initial filter panel state
    Machinata.UI.microdonut($(".ma-crowdsource-filters .ma-crowdsource-donut"), {
        guide: true,
        guideIsBehindValue: true
    });
};

Machinata.CrowdSource.updateListingForSelectedSorts = function () {
    var property = $(".ma-crowdsource-sorter.selected").attr("data-sort-property");
    var reverse = $(".ma-crowdsource-sorter.selected").attr("data-sort-direction") != "ascending";
    var sortOptions = {};
    if (property != null) {
        sortOptions = {
            reverse: reverse,
            by: function (element) { return $(element).attr("data-sort-" + property); }
        };
    }
    $(".ma-crowdsource-projects.option-filterable").each(function () {
        var shuffle = $(this).data("shuffle");
        shuffle.sort(sortOptions);
    });
};

Machinata.CrowdSource.updateListingForSelectedFilters = function (containerElem) {
    // See https://vestride.github.io/Shuffle/compound-filters
    // Init
    if (containerElem == null) containerElem = $("body");
    var categories = [];
    var tags = [];
    var allFiltersTitles = [];
    containerElem.find(".ma-crowdsource-filter").each(function () {
        if ($(this).hasClass("selected")) {
            if ($(this).attr("data-filter-type") == "category") {
                categories.push($(this).attr("data-filter-group"));
            } else {
                tags.push($(this).attr("data-filter-group"));
            }
            allFiltersTitles.push($(this).attr("data-filter-title"));
        }
    });
    var allFilters = categories.concat(tags);
    //console.log("Machinata.CrowdSource.updateListingForSelectedFilters", categories,tags);
    containerElem.find(".ma-crowdsource-projects.option-filterable").each(function () {
        var shuffle = $(this).data("shuffle");
        var matches = [];
        var matchedFilterGroups = [];
        var totalCount = 0;
        var matchesCount = 0;
        function registerFilterGroups(groups) {
            for (var i = 0; i < groups.length; i++) {
                if (matchedFilterGroups.indexOf(groups[i]) == -1) {
                    matchedFilterGroups.push(groups[i]);
                }
            }
        };
        shuffle.filter(function (element, shuffle) {
            // Bookkeeping
            var countInTotal = $(element).hasClass("option-no-total-count") == false;
            if (countInTotal) totalCount++;
            // Init
            var itemFilterGroups = $(element).attr("data-filter-groups").split(",");
            // No filter?
            if (categories.length == 0 && tags.length == 0) {
                matches.push($(element));
                if (countInTotal) matchesCount++;
                registerFilterGroups(itemFilterGroups);
                return true;
            }
            // Test for category (must be, and)
            for (var i = 0; i < categories.length; i++) {
                var cat = categories[i];
                if (itemFilterGroups.indexOf(cat) == -1) return false;
            }
            // Not tags?
            if (tags.length == 0) {
                matches.push($(element));
                if (countInTotal) matchesCount++;
                registerFilterGroups(itemFilterGroups);
                return true;
            }
            // Test for tags (any, or)
            var itemMatchesFilters = false;
            for (var i = 0; i < tags.length; i++) {
                var tag = tags[i];
                if (itemFilterGroups.indexOf(tag) != -1) itemMatchesFilters = true;
            }
            if (itemMatchesFilters == true) {
                matches.push($(element));
                if (countInTotal) matchesCount++;
                registerFilterGroups(itemFilterGroups);
                return true;
            } else {
                return false;
            }
        });
        // Update results count
        containerElem.find(".ma-crowdsource-projects-filter-results-number").text(matchesCount);
        $(this).attr("data-num-matches", matchesCount);
        if (matches.length == 0) {
            $(".ma-crowdsource-projects-filter-no-results").show();
        } else {
            $(".ma-crowdsource-projects-filter-no-results").hide();
        }
        containerElem.find(".ma-crowdsource-donut-num-results").attr("data-value", matchesCount / totalCount);
        Machinata.UI.microdonut($(".ma-crowdsource-donut-num-results"), {
            updateIfExists: true,
            animate: true,
            guide: true,
            guideIsBehindValue: true
        });
        if (allFilters.length == 0) {
            containerElem.find(".ma-crowdsource-multiple-filter").hide();
            containerElem.find(".ma-crowdsource-single-filter").hide();
            containerElem.find(".ma-crowdsource-no-filter").show();
        } else if (allFilters.length == 1) {
            containerElem.find(".ma-crowdsource-no-filter").hide();
            containerElem.find(".ma-crowdsource-multiple-filter").hide();
            containerElem.find(".ma-crowdsource-single-filter").text(allFiltersTitles[0]).show();
        } else {
            containerElem.find(".ma-crowdsource-single-filter").hide();
            containerElem.find(".ma-crowdsource-no-filter").hide();
            containerElem.find(".ma-crowdsource-multiple-filter").show();
        }
        containerElem.find(".ma-crowdsource-donut")
        // Update filters UI based on if they are even included in matches
        console.log(matchedFilterGroups);
        containerElem.find(".ma-crowdsource-filter").addClass("option-inactive");
        for (var i = 0; i < matchedFilterGroups.length; i++) {
            containerElem.find(".ma-crowdsource-filter[data-filter-group='" + matchedFilterGroups[i] + "']").removeClass("option-inactive");
        }
    });
};
Machinata.CrowdSource.filterProjects = function (filterGroup) {
    $(".ma-crowdsource-projects.option-filterable").each(function () {
        var shuffle = $(this).data("shuffle");
        shuffle.filter(filterGroup);
    });
};


Machinata.ready(function () {

    // Load the cache
    Machinata.CrowdSource.loadCache();

    // Initial bind for everything on page
    Machinata.CrowdSource.bindUI($("body"));

});

