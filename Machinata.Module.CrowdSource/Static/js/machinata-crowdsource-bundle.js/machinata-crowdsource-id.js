
Machinata.CrowdSource.IDProviders = {};

Machinata.CrowdSource.IDProviders["CacheGUID"] = {
    getId: function (callback) {
        var cached = Machinata.getCache("CrowdSource_Id");
        if (cached != null) {
            callback(cached);
            return;
        }

        var guid = Machinata.guid();
        Machinata.setCache("CrowdSource_Id", guid);
        callback(guid);
    }
};

Machinata.CrowdSource.IDProviders["Fingerprint"] = {
    getId: function (callback) {
        Machinata.Fingerprint.getFingerprint(function (id) {
            callback(id);
        });
    }
};
