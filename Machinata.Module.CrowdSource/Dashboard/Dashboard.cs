﻿using Machinata.Core.Model;
using Machinata.Module.Admin.Dashboard;
using Machinata.Module.CrowdSource.Model;
using Machinata.Module.Reporting.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.CrowdSource.Dashboard {
    class Dashboard {

        [DashboardLoader]
        public static void LoadDashboard(DashboardLoader loader) {

            var section = new SectionNode();
            section.SetTitle("Crowd Source");

            // Projects by Category
            {

                var chart = new DonutNode();
                chart.Theme = "default";
                chart.SetTitle("Projects ");
                chart.SetSubTitle("by Category");
                var categories = loader.DB.Projects().IncludeAll().GroupBy(p => p.Category);

                
                foreach (var category in categories) {
                    chart.AddSlicePercentage(category.Count(), category.Key.Name);
                }


                chart.Series.First().CalculateFactValuesAsPercentages();
                          

                section.AddChild(chart);
            }

            // Votes over time, open    
            {

                var chart = Logic.Statistics.VotesOverTimeChart(loader.DB.Projects()/*.Where(p => p.Status >= Project.ProjectStatuses.Voting)*/);
                chart.SetTitle("Votes over time");
                chart.SetSubTitle("all projects");
                section.AddChild(chart);
            }

                     

            // Sunburst Categories / Approval
            {
                var sunburst = new SunburstNode();
                sunburst.Theme = "default";
                sunburst.SetTitle("Votes");
                sunburst.SetSubTitle("by Category");

                sunburst.SliceLabelsShowPercentage = false;
                var categories = loader.DB.Projects().IncludeAll().GroupBy(p => p.Category);


                foreach (var category in categories) {
                    var serie = sunburst.AddSerie(category.Key.Name);
                    serie.AddFact(category.Sum(c => c.Upvotes), category.Sum(c => c.Upvotes).ToString("0.##"), "Upvotes");
                    serie.AddFact(category.Sum(c => c.Downvotes), category.Sum(c => c.Downvotes).ToString("0.##"), "Downvotes");
                }
                section.AddChild(sunburst);

            }

            loader.AddItem(section,"/admin/crowd-source");
        }


        public static Node GetProjectVotesDonut(ModelContext db, Project project) {
            var chart = new DonutNode();
            chart.Theme = "rating";
            chart.SetTitle("Votes");
            chart.SetSubTitle("by type");
            var negatives = project.Votes.Where(v => v.IsNegatvie);
            var positives = project.Votes.Where(v => v.IsPositive);
            chart.AddSliceNumber(negatives.Count(),"Downvotes","negative2");
            chart.AddSliceNumber(positives.Count(), "Upvotes","positive2");
            return chart;
        }

       
    }
}
