﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.CrowdSource {
    class Config {

        /// <summary>
        /// Public project path: e.g. /projects/my-project
        /// </summary>
        public static string CrowdSourceProjectPath = Core.Config.GetStringSetting("CrowdSourceProjectPath", "/projects/{short-url}");

        public static string CrowdSourceProjectsAPIPath = Core.Config.GetStringSetting("CrowdSourceProjectsAPIPath", "/api/crowd-source/projects/{project-id}");



        public static string CrowdSourceFingerprintProvider = Core.Config.GetStringSetting("CrowdSourceFingerprintProvider", "Nothing");
        public static string CrowdSourceFingerprintProviderToken = Core.Config.GetStringSetting("CrowdSourceFingerprintProviderToken", null);
        
        public static int CrowdSourceNewProjectTagMaxDays = Core.Config.GetIntSetting("CrowdSourceNewProjectTagMaxDays", 10);
        public static double CrowdSourcePopularProjectTagMinApprovalRate = Core.Config.GetDoubleSetting("CrowdSourcePopularProjectTagMinApprovalRate");


    }
}
