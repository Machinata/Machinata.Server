﻿using Machinata.Core.Builder;
using Machinata.Core.Exceptions;
using Machinata.Core.Handler;
using Machinata.Core.Model;
using Machinata.Core.Templates;
using Machinata.Core.Util;
using Machinata.Module.CrowdSource.Logic;
using Machinata.Module.CrowdSource.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinata.Module.CrowdSource.Extensions {
    public static class ProjectTemplateExtensions {

        public static void InsertTagCloud(this PageTemplate template, Project entity, string variableName = "tags.listing", string templateName = "tag-cloud.tag", string virtualTemplateName = "tag-cloud.tag") {
            // Tags
            var tags = entity.Tags.ToList();
            tags.AddRange(entity.VirtualTags); // make sure to include the virtual tags

            // Has tags
            template.InsertVariable(variableName + ".has-tags", tags.Any());

            if (tags.Any() == false) {
                template.InsertVariable(variableName, "{text.no-tags}");
            }

            // Templates
            //template.InsertTemplates(
            //      variableName: variableName,
            //      entities: tags,
            //      templateName: templateName,
            //      forEachEntity: new Action<Tag, PageTemplate>(delegate (Tag tag, PageTemplate pt) {
            //          pt.InsertVariables("tag", tag);
            //          pt.InsertVariables("project", entity);
            //        })
            //      );


            var templates = new List<PageTemplate>();
            foreach (var tag in tags) {
                var isVirtual = tag.Id == 0;
                var templateNameUsed = isVirtual ? virtualTemplateName : templateName;
                var tagTemplate = template.LoadTemplate(templateNameUsed);
                tagTemplate.InsertVariables("tag", tag);
                tagTemplate.InsertVariables("project", entity);

                templates.Add(tagTemplate);

            }


            template.InsertTemplates(variableName, templates);


        }


        public static void InsertCategoryCloud(this PageTemplate template, Project entity, string variableName = "category.listing", string templateName = "tag-cloud.category") {
            // Tags
            var categories = new List<Category>();
            if (entity.Category != null) {
                categories.Add(entity.Category);
            }

            // Has tags
            template.InsertVariable(variableName + ".has-tags", categories.Any());

            if (categories.Any() == false) {
                template.InsertVariable(variableName, "{text.no-tags}");
            }

            // Templates
            template.InsertTemplates(
                  variableName: variableName,
                  entities: categories,
                  templateName: templateName,
                  forEachEntity: new Action<Category, PageTemplate>(delegate (Category tag, PageTemplate pt) {
                      pt.InsertVariables("category", tag);
                      pt.InsertVariables("project", entity);
                  })
                  );
        }

        public static void InsertProjectsFilters(this PageTemplate template, string variableName, string templateName, List<Project> projects, bool insertCategories = true, bool insertTags = true) {
            var wrappingTemplate = template.LoadTemplate(templateName);


            // Get all categories and insert
            if (insertCategories) {
                var categories = projects.Select(p => p.Category).Distinct().OrderBy(p=>p.Name);
                wrappingTemplate.InsertTemplates(variableName + ".categories", categories, templateName + ".category", delegate (Category c, PageTemplate t) {

                });
            }

            // Get all tags and insert
            if (insertTags) {
                var tags = projects.SelectMany(p => p.Tags).Distinct().OrderBy(t => t.Name);
                var vtags = projects.SelectMany(p => p.VirtualTags).DistinctBy(t => t.ShortURL).OrderBy(t => t.Name);
                var allTags = vtags.Concat(tags);
                wrappingTemplate.InsertTemplates(variableName + ".tags", allTags, templateName + ".tag", delegate (Tag tag, PageTemplate t) {

                });
            } else {
                wrappingTemplate.InsertVariable(variableName + ".tags", "");
            }

            template.InsertTemplate(variableName, wrappingTemplate);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentTemplate"></param>
        /// <param name="variableName"></param>
        /// <param name="templateName"></param>
        /// <param name="projects"></param>
        /// <param name="listingVariableName">will be added randomly into the project template stack</param>
        /// <param name="additionalElements"></param>
        public static void InsertProjectsOverview(this PageTemplate parentTemplate,
                                                        string variableName,
                                                        string templateName,
                                                        List<Project> projects,
                                                        string listingVariableName = "project",
                                                        IEnumerable<PageTemplate> additionalElements = null) {


            parentTemplate.InsertVariable("projects.count", projects.Count());

            var projectTemplates = new List<PageTemplate>();
            var destination = new List<PageTemplate>();
            var additional = additionalElements?.ToList();



            foreach (var project in projects) {
                try {
                    var template = parentTemplate.LoadTemplate(templateName);

                    // Removed because error with caching and .Description is not needed for listing
                    //if (project.Description != null) {
                    //    project.Description.IncludeContent();
                    //}


                    template.InsertTagCloud(project, listingVariableName + ".tags");
                    template.InsertVariables(listingVariableName, project);

                    projectTemplates.Add(template);
                } catch (Exception e) {
                    throw new BackendException("project-list-fail", "Could not generate project listing for " + project.Name + ": " + e.Message, e);
                }
            }

            // Only if the additional templates are less than the projects number
            if (additional != null && additional.Count() > 0 && additional.Count() < projects.Count()) {
                try {
                    var totalElements = projects.Count() + additional.Count();
                    var every = (int)System.Math.Ceiling( ((float)(totalElements) / ((float)additional.Count())) ); // We take the upper rounding int, since if we round down it could be that we don't have enough to insert (note: no longer needed because we just loop the additional

                    int current = (int)every / 2; // start off
                    int currentAdditional = 0;
                    foreach (var template in projectTemplates) {
                        destination.Add(template);
                        current++;
                        if (current % every == 0) {
                            if (additional.Count() == 0) throw new Exception("additional template (tip) was expected, but no more exist to insert. Most likely there is a problem with the insertion algorithm (ie every is not set correctly)...");
                            // Old method:
                            /*
                            var add = additional.First();
                            destination.Add(add); 
                            additional.Remove(add);
                            current++; // why increment current?
                            */
                            // New method: a safer alternative that just loops additionals
                            var additionalToAdd = additional[currentAdditional];
                            destination.Add(additionalToAdd);
                            current++; // why increment current?
                            currentAdditional++;
                            if (currentAdditional >= additional.Count()) currentAdditional = 0;
                        }
                    }
                } catch (Exception e) {
                    throw new BackendException("project-list-fail", "Could not insert additional templates into projects (check inseration algorithm in InsertProjectsOverview): " + e.Message, e);
                }
            } else {
                destination.AddRange(projectTemplates);
            }


            parentTemplate.InsertTemplates(variableName, destination);

        }


        /// <summary>
        /// Inserts Public Template for detailed view
        /// if preview=true and Admin User "non published" projects can be  previewed as well
        /// </summary>
        /// <param name="parentTemplate"></param>
        /// <param name="db"></param>
        /// <param name="shortURL"></param>
        public static Project InsertPublicProjectDetailView(this PageTemplate parentTemplate, PageTemplateHandler handler, string shortURL, out List<Project> projectsOut, User viewer) {

            // Project
            var projects = handler.DB.Projects().IncludeAll();



            var preview = handler.Params.Bool("preview", false);
            var isAdmin = viewer != null && viewer.IsAdminOrSuperUser;

            if (preview == false || isAdmin == false) {
                projects = projects.Published();
            }

            var project = projects.GetProjectByShortURL(shortURL);


            // View
            parentTemplate.InsertPublicProjectDetailView("project", "projects.project.details", project);


            projectsOut = projects.ToList();

            return project;
        }

        /// <summary>
        /// Frontend Detail View of Project
        /// </summary>
        /// <param name="parentTemplate"></param>
        /// <param name="variableName"></param>
        /// <param name="templateName"></param>
        /// <param name="project"></param>
        public static void InsertPublicProjectDetailView(this PageTemplate parentTemplate, string variableName, string templateName, Project project) {

            // Template
            var template = parentTemplate.LoadTemplate(templateName);

            // Tag Cloud
            template.InsertTagCloud(project, variableName + ".tags");

            // Category Cloud
            template.InsertCategoryCloud(project, variableName + ".category-tags", "tag-cloud.category");

            // Description
            template.InsertVariableUnsafe(variableName + ".description-html", project.GetDescription());

            // Images
            var images = project.GetImages();
            if (images.Any()) {
                template.InsertTemplates(variableName + ".images", images, variableName + ".image");

            } else {
                template.InsertVariableUnsafe(variableName + ".images", string.Empty);
            }


            // Vars
            template.InsertVariables(variableName, project);

            // Open is open for voting
            template.InsertVariable(variableName + ".open-for-voting", project.IsOpenForVoting);

            // Collect
            parentTemplate.InsertTemplate(variableName, template);
        }



        public static void InsertProjectList(this PageTemplate parentTemplate, IQueryable<Project> project, string variableName = "projects") {
            parentTemplate.InsertEntityList(
              variableName: variableName,
              entities: project,
              form: new FormBuilder(Forms.Admin.LISTING),
              link: "/admin/crowd-source/projects/project/{entity.public-id}");
        }


        /// <summary>
        /// Inserts data for a countown for the given dates
        /// HINTS: this function does nothing about local vs utc time
        /// </summary>
        /// <param name="parentTemplate"></param>
        /// <param name="variableName"></param>
        /// <param name="templateName"></param>
        /// <param name="votingStart"></param>
        /// <param name="votingEnd"></param>
        /// <param name="now"></param>
        public static void InsertCountdownTemplate(this PageTemplate parentTemplate, string variableName, string templateName, DateTime votingStart, DateTime votingEnd, DateTime now) {

            var template = parentTemplate.LoadTemplate(templateName);

            template.InsertTextVariables();

            // TODO LOCAL TIME
            var daysLeft = (now - votingEnd).TotalDays;
            var percentage = 1 - daysLeft / (votingStart - votingEnd).TotalDays;

            var daysLeftRounded = System.Math.Abs(System.Math.Round(daysLeft, 0));
            var percentageRounded = System.Math.Round(percentage, 2);

            template.InsertVariable("countdown.days-left", daysLeftRounded);
            template.InsertVariable("countdown.percentage", percentageRounded);

            parentTemplate.InsertTemplate(variableName, template);
        }


        /// <summary>
        /// Meta Tags for Admin View
        /// </summary>
        /// <param name="parentTemplate"></param>
        /// <param name="entity"></param>
        /// <param name="variableName"></param>
        public static void InsertMetaTags(this PageTemplate parentTemplate, Project entity, string variableName) {
            var metaTagTemplates = new List<PageTemplate>();
            var multis = new List<MetaTag>();
            foreach (var metaTag in entity.MetaTags) {
                var impl = metaTag.LoadImplementation();
                if (impl.RelationshipType == MetaTag.RelationTypes.ToSingle) {
                    var template = parentTemplate.LoadTemplate("metatags.metatag.single");
                    template.InsertVariables("entity", impl);
                    template.InsertPropertyList("properties", impl);
                    metaTagTemplates.Add(template);
                } else {
                    multis.Add(impl);
                }
            }

            // Mutli Relations
            foreach (var multi in multis.GroupBy(m => m.Type)) {
                var template = parentTemplate.LoadTemplate("metatags.metatag.multi");
                template.InsertVariable("type", multi.Key);
                template.InsertVariable("type-friendly-name", MetaTag.GetFriendlyTypeName(multi.Key));
                // template.InsertVariables("entity", impl);
                template.InsertSortableList(
                    variableName: "entities",
                    entities: multi.AsQueryable().OrderBy(m => m.Sort),
                    link: "{page.navigation.current-path}/metatags/metatag/{entity.public-id}/edit",
                    sortingAPICall: $"api/admin/crowd-source/projects/project/{entity.PublicId}/metatags/metatag/{multi.Key}/sort"
                    );

                metaTagTemplates.Add(template);
            }



            parentTemplate.InsertTemplates(variableName, metaTagTemplates);
        }


        /// <summary>
        /// Inserts single meta tags variables into {meta-tag-implename.variablename}
        /// For multi meta tags a callback function can be passed insertMultiMetaTags(MetaTags, PageTemplate, "TypeName")
        /// </summary>
        /// <param name="parentTemplate"></param>
        /// <param name="project"></param>
        /// <param name="insertMultiMetaTags"></param>
        public static void InsertMetaTagVariables(this PageTemplate parentTemplate, Project project, Action<IQueryable<MetaTag>, PageTemplate, string> insertMultiMetaTags = null) {
            var singleMetaTags = project.GetSingleMetaTags();
            foreach (var singleMetaTag in singleMetaTags) {
                parentTemplate.InsertVariables(singleMetaTag.Type.ToDashedLower(), singleMetaTag);
            }

            var multiMetaTags = project.GetMultiMetaTags().GroupBy(mt => mt.Type);
            foreach (var multiMetaTag in multiMetaTags) {
                if (insertMultiMetaTags != null) {
                    insertMultiMetaTags(multiMetaTag.AsQueryable(), parentTemplate, multiMetaTag.Key);
                }

            }
        }

    }
}
