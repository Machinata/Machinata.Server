using Machinata.Core.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Machinata.Core.Model;
using Machinata.Module.CrowdSource.Model;
using Machinata.Core.TaskManager;

namespace Machinata.Module.CrowdSource.Tasks {

    public class ProjectsGenerateMissingTagsTask : Task {

        public override void Process() {

          
            var projects = this.DB.Projects()
                .IncludeAll() // IMPORTANT (MetaTags)
                .OrderByDescending(e => e.Id).ToList(); // we must cache this unfortunately to iterate it safely...

            Log($"Found {projects.Count} projects");

            var progress = 0;

            // Generate Tags
            foreach(var project in projects) {

                Log($"Generate missing tags for {project.Name}");

                project.GenerateMissingMetaTags(this.DB);

                // Save
                if (this.TestMode == false) {
                    this.DB.SaveChanges();
                }

                // Progress
                RegisterProgress(progress++, projects.Count);

            }
            Log($"------------ Finished -----------");



        }

        public override ScheduledTaskConfig GetScheduledTaskConfig() {
            var config = new ScheduledTaskConfig();
            config.Enabled = false;
            config.Install = false;
            config.Interval = "1d";
            return config;
        }
    }
}
