#!/bin/bash

##############################################################
# Variables
##############################################################

MA_UTIL_VERSION=2023.01.31
MA_UTIL_DEBUG=false
MA_UTIL_CMD=$1

MA_UTIL_MACHINATA_SERVER_REMOTE=https://gitlab.com/Machinata/Machinata.Server.git
MA_UTIL_MACHINATA_SERVER_SUBDIR=Machinata.Server
#MA_UTIL_MACHINATA_SERVER_BRANCH=`git rev-parse --abbrev-ref HEAD` # gets current branch
MA_UTIL_MACHINATA_SERVER_BRANCH=master

MA_UTIL_MACHINATA_CONFIG_REMOTE=https://gitlab.com/Nerves/Products/Machinata.Config.git
MA_UTIL_MACHINATA_CONFIG_SUBDIR=Machinata.Config
MA_UTIL_MACHINATA_CONFIG_BRANCH=master



##############################################################
# Helper Functions
##############################################################
ma_util_debug () { 
  if [ "$MA_UTIL_DEBUG" = "true" ]; then
      echo "DEBUG: $@"; 
  fi
}
ma_util_validate_exit_code_0 () { 
    status=$?
    ma_util_debug "LAST EXIST CODE: $status"; 
    if [ $status -ne 0 ]; then
        echo "***Warning: exit code was not 0 (was $status), aborting..."
        exit 1
    fi
}



##############################################################
# Show Version
##############################################################

echo "ma-util v$MA_UTIL_VERSION"



##############################################################
# Debug Info
##############################################################

ma_util_debug "MA_UTIL_CMD: $MA_UTIL_CMD"
ma_util_debug "MA_UTIL_VERSION: $MA_UTIL_VERSION"
ma_util_debug "MA_UTIL_DEBUG: $MA_UTIL_DEBUG"
ma_util_debug "ARG0: $0"
ma_util_debug "ARG1: $1"
ma_util_debug "ARG2: $2"
ma_util_debug "ARG3: $3"
ma_util_debug "ARG4: $4"
ma_util_debug "ARG5: $5"




##############################################################
# Command: --help
##############################################################

ma_util_help () { 
    echo "Infos:"; 
    echo "  --help: show commands"; 
    echo "  --version: show version"; 
    echo "Tools:"; 
    echo "  add-upstream: add the upstream server and config subrepo"; 
    echo "  pull-upstream: pull changes from the upstream server and config based on current branch"; 
    echo "  push-upstream: push changes to the upstream server and config based on current branch"; 
    echo "  sync-upstream: clean, pull and push changes to and from upstream server and config"; 
    echo "  clean-subrepo: clean the subrepos so that they are ready to be pushed and pulled"; 
    echo "Utilities:"; 
    echo "  convert-subtree-to-subrepo: convert a old subtree-based upstream to subrepo-based upstream"; 
    echo "  remove-subtrees: remove old subtree-based upstream"; 
}

##############################################################
# Command: --version
##############################################################

ma_util_version () { 
    echo "v$MA_UTIL_VERSION"; 
}

##############################################################
# Command: convert-subtree-to-subrepo
##############################################################

ma_util_convert_subtree_to_subrepo () { 
    # Init
    branch=`git rev-parse --abbrev-ref HEAD`
    echo "Setting up $MA_UTIL_MACHINATA_SERVER_SUBDIR..."
    git subrepo init $MA_UTIL_MACHINATA_SERVER_SUBDIR -r $MA_UTIL_MACHINATA_SERVER_REMOTE -b $branch
    echo "Setting up $MA_UTIL_MACHINATA_CONFIG_SUBDIR..."
    git subrepo init $MA_UTIL_MACHINATA_CONFIG_SUBDIR -r $MA_UTIL_MACHINATA_CONFIG_REMOTE -b $branch
}



##############################################################
# Command: push-upstream / pull-upstream / sync-upstream
##############################################################

ma_util_push_upstream () { 
    # Init
    #branch=`git rev-parse --abbrev-ref HEAD`
    # Validate
    #if [ "$branch" = "master" ]; then 
    #    echo "You cannot push upstream subtrees to master!"
    #    exit 1
    #fi;
    # Do Push
    echo "Pushing $MA_UTIL_MACHINATA_CONFIG_SUBDIR to upstream using branch $MA_UTIL_MACHINATA_CONFIG_BRANCH..."
    git subrepo push "$MA_UTIL_MACHINATA_CONFIG_SUBDIR" -r "$MA_UTIL_MACHINATA_CONFIG_REMOTE" -b "$MA_UTIL_MACHINATA_CONFIG_BRANCH" --verbose
    ma_util_validate_exit_code_0
    echo "Pushing $MA_UTIL_MACHINATA_SERVER_SUBDIR to upstream using branch $MA_UTIL_MACHINATA_SERVER_BRANCH..."
    git subrepo push $MA_UTIL_MACHINATA_SERVER_SUBDIR -r $MA_UTIL_MACHINATA_SERVER_REMOTE -b $MA_UTIL_MACHINATA_SERVER_BRANCH --verbose
    ma_util_validate_exit_code_0
}

ma_util_pull_upstream () { 
    # Init
    #branch=`git rev-parse --abbrev-ref HEAD`
    # Do Pull
    echo "Pulling from $MA_UTIL_MACHINATA_CONFIG_SUBDIR upstream using branch $MA_UTIL_MACHINATA_CONFIG_BRANCH..."
    git subrepo pull "$MA_UTIL_MACHINATA_CONFIG_SUBDIR" -r "$MA_UTIL_MACHINATA_CONFIG_REMOTE" -b "$MA_UTIL_MACHINATA_CONFIG_BRANCH" --verbose
    ma_util_validate_exit_code_0
    echo "Pulling from $MA_UTIL_MACHINATA_SERVER_SUBDIR upstream using branch $MA_UTIL_MACHINATA_SERVER_BRANCH..."
    git subrepo pull "$MA_UTIL_MACHINATA_SERVER_SUBDIR" -r "$MA_UTIL_MACHINATA_SERVER_REMOTE" -b "$MA_UTIL_MACHINATA_SERVER_BRANCH" --verbose
    ma_util_validate_exit_code_0
}

ma_util_clean_subrepo () { 
    echo "Cleaning subrepo $MA_UTIL_MACHINATA_CONFIG_SUBDIR..."
    git subrepo clean "$MA_UTIL_MACHINATA_CONFIG_SUBDIR"
    ma_util_validate_exit_code_0
    echo "Cleaning subrepo $MA_UTIL_MACHINATA_SERVER_SUBDIR..."
    git subrepo clean "$MA_UTIL_MACHINATA_SERVER_SUBDIR"
    ma_util_validate_exit_code_0
}


ma_util_sync_upstream () { 
    ma_util_clean_subrepo
    ma_util_pull_upstream
    ma_util_push_upstream
}

ma_util_add_upstream () { 
    # Init
    #branch=`git rev-parse --abbrev-ref HEAD`
    # Clone
    echo "Cloning $MA_UTIL_MACHINATA_CONFIG_SUBDIR upstream from $MA_UTIL_MACHINATA_CONFIG_REMOTE using branch $MA_UTIL_MACHINATA_CONFIG_BRANCH..."
    git subrepo clone "$MA_UTIL_MACHINATA_CONFIG_REMOTE" "$MA_UTIL_MACHINATA_CONFIG_SUBDIR" -b "$MA_UTIL_MACHINATA_CONFIG_BRANCH" --verbose
    ma_util_validate_exit_code_0
    echo "Cloning $MA_UTIL_MACHINATA_SERVER_SUBDIR upstream from $MA_UTIL_MACHINATA_SERVER_REMOTE using branch $MA_UTIL_MACHINATA_SERVER_BRANCH..."
    git subrepo clone "$MA_UTIL_MACHINATA_SERVER_REMOTE" "$MA_UTIL_MACHINATA_SERVER_SUBDIR" -b "$MA_UTIL_MACHINATA_SERVER_BRANCH" --verbose
    ma_util_validate_exit_code_0
}

ma_util_remove_subtrees () { 
    echo "Please manually git remove $MA_UTIL_MACHINATA_CONFIG_SUBDIR and $MA_UTIL_MACHINATA_SERVER_SUBDIR followed by a commit."
    exit 1
    echo "Removing subtree $MA_UTIL_MACHINATA_CONFIG_SUBDIR..."
    git rm -r "$MA_UTIL_MACHINATA_CONFIG_SUBDIR"
    ma_util_validate_exit_code_0
    git commit . -m "Removed subtree $MA_UTIL_MACHINATA_CONFIG_SUBDIR"
    ma_util_validate_exit_code_0
}

##############################################################
# Switch on Main Command
##############################################################


if [ $# -eq 0 ]; then 
  ma_util_help
fi;
while [ $# -gt 0 ]; do
  arg=$1
  case $arg in
    "convert-subtree-to-subrepo")
      ma_util_convert_subtree_to_subrepo
    ;;
    "add-upstream")
      ma_util_add_upstream
    ;;
    "push-upstream")
      ma_util_push_upstream
    ;;
    "pull-upstream")
      ma_util_pull_upstream
    ;;
    "sync-upstream")
      ma_util_sync_upstream
    ;;
    "clean-subrepo")
      ma_util_clean_subrepo
    ;;
    "remove-subtrees")
      ma_util_remove_subtrees
    ;;
    "--help")
      ma_util_help
    ;;
    "")
      ma_util_help
    ;;
    "--version")
      ma_util_version
    ;;
    *)
     echo "Invalid command $arg. Available commands:"
     ma_util_help
    ;;
  esac
  shift
done