using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace Machinata.Module.Services.Tests {


    [TestClass]
    public class NLPTests {

        private void _validateData(
                Machinata.Module.Services.Providers.GeoIPProvider provider,
                string ip,
                string expectedCity
            ) {
            var data = provider.DataForIP(ip);
            Assert.AreEqual(data.Address.City,expectedCity);
        }

        [TestMethod]
        public void Services_NLP_Default_XXXXX() {
            //_validateData(Machinata.Module.Services.GeoIP.Default, "83.150.2.227", "Zurich");
            var provider = new NLP.Providers.OpenAINLPProvider();
            var ret = provider.GetTextCompletion("The following is a converstion between a patient and their very rude doctor:\n\nPatient: is salt unhealthy for me?\nDoctor:", "text-davinci-003", 0.5, 1000);
            var dbg = "";
        }

        [TestMethod]
        public void Services_NLP_Default_Moderation_XXXXX() {
            var provider = new NLP.Providers.OpenAINLPProvider();
            var ret = provider.CreateModeration("I love my live");
           
        }


    }

}
